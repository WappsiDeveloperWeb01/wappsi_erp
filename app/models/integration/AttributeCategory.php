<?php defined('BASEPATH') OR exit('No direct script access allowed');

class AttributeCategory extends CI_Model
{
    private $newDb;
    private $tableName = 'attribute_category';

    public function __construct()
    {
        parent::__construct();
        $this->load->integration_model('Connection');
        $this->load->integration_model('Translation');
    }

    public function open($connData)
    {
        $Connection = new Connection();

        $Connection->hostname = $connData->host;
        $Connection->username = $connData->username;
        $Connection->password = $connData->password;
        $Connection->dbname = $connData->databases;

        $this->newDb = $Connection->open();
    }

    public function find($data)
    {
        $this->newDb->where($data);
        $q = $this->newDb->get($this->tableName);
        return $q->row();
    }

    public function getAll()
    {
        $q = $this->newDb->get($this->tableName);
        return $q->result();
    }

    public function create($data)
    {
        if ($this->newDb->insert($this->tableName, $data)) {
            return true;
        }
        return false;
    }

    public function update($data, $id)
    {
        $this->newDb->where('id', $id);
        if ($this->newDb->update($this->tableName, $data)) {
            return true;
        }
        return false;
    }

    public function delete($id)
    {
        $this->newDb->where("id", $id);
        if ($this->newDb->delete($this->tableName)) {
            return true;
        }
        return false;
    }

    public function close()
    {
        $this->newDb->close();
    }
}