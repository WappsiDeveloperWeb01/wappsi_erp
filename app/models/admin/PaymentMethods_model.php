<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class PaymentMethods_model extends CI_Model
{
    public $tableName = "payment_methods";

	public function __construct()
	{
		parent::__construct();
	}

    public function find($data)
    {
        $this->db->where($data);
        $q = $this->db->get($this->tableName);
        return $q->row();
    }
}