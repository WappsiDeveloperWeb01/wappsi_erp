<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Creditors extends MY_Controller
{

    function __construct()
    {
        // $this->sma->print_arrays(array);
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('suppliers', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('pos_model');
        $this->load->admin_model('purchases_model');
    }

    function index($action = NULL)
    {
        $this->sma->checkPermissions('index', NULL, 'suppliers');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('creditors')));
        $meta = array('page_title' => lang('creditors'), 'bc' => $bc);
        $this->page_construct('creditors/index', $meta, $this->data);
    }

    function getCreditors()
    {
        $this->sma->checkPermissions('index', NULL, 'suppliers');

        $this->load->library('datatables');
        $this->datatables
            ->select("id, company, name, email, phone, city, country, vat_no")
            ->from("companies")
            ->where('group_name', 'creditor')
            ->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default new-button new-button-sm btn-xs dropdown-toggle\" data-toggle=\"dropdown\"  data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                                                <i class='fas fa-ellipsis-v fa-lg'></i>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li>
                                                    <a class=\"tip\" title='' href='" . admin_url('products?supplier=$1') . "'><i class=\"fa fa-list\"></i>" . $this->lang->line("list_products") . "</a> 
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('suppliers/deposits/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-money\"></i>
                                                        ".lang("list_deposits")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('suppliers/add_deposit/$1') . "' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-plus\"></i>
                                                        ".lang("add_deposit")."
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class=\"tip\" title='' href='" . admin_url('creditors/edit/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>" . $this->lang->line("edit_creditor") . "</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>", "id");
        //->unset_column('id');
        echo $this->datatables->generate();
    }

    function view($id = NULL)
    {
        $this->sma->checkPermissions('index', NULL, 'suppliers');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['creditor'] = $this->companies_model->getCompanyByID($id);
        $this->load_view($this->theme.'creditors/view',$this->data);
    }

    function add()
    {
        $this->sma->checkPermissions('add', NULL, 'suppliers');
        $this->form_validation->set_rules('email', $this->lang->line("email_address"), 'is_unique[companies.email]');
        if ($this->form_validation->run('companies/add') == true) {

            if ($this->input->post('type_person') == 1) {
                $nombreCompleto = $this->input->post('first_name')." ".$this->input->post('second_name')." ".$this->input->post('first_lastname')." ".$this->input->post('second_lastname');
            } else {
                $nombreCompleto = $this->input->post('name');
            }

            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => '9',
                'group_name' => 'creditor',
                'company' => $nombreCompleto,
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $nombreCompleto,
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'type_person' => $this->input->post('type_person'),
                'tipo_regimen' => $this->input->post('tipo_regimen'),
                'tipo_documento' => $this->input->post('tipo_documento'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "default_rete_fuente_id" => $this->input->post("default_rete_fuente_id"),
                "default_rete_iva_id" => $this->input->post("default_rete_iva_id"),
                "default_rete_ica_id" => $this->input->post("default_rete_ica_id"),
                "default_rete_other_id" => $this->input->post("default_rete_other_id"),
                'customer_validate_min_base_retention' => $this->input->post('customer_validate_min_base_retention') ? 1 : 0,
                "customer_payment_type" => $this->input->post("creditor_payment_type"),
                "customer_credit_limit" => $this->input->post("creditor_credit_limit"),
                "customer_payment_term" => $this->input->post("creditor_payment_term"),
            );

            if ($this->companies_model->validate_vat_no($data['vat_no'], 'creditor')) {
                $this->session->set_flashdata('error', 'Ya existe un tercero con el mismo documento y mismo perfil.');
                admin_redirect('creditors');
            }
        } elseif ($this->input->post('add_creditor')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('creditors');
        }

        if ($this->form_validation->run() == true && $sid = $this->companies_model->addCompany($data)) {
            $this->session->set_flashdata('message', $this->lang->line("creditor_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $rete_data = $this->purchases_model->getWithHolding(NULL, 'P');
            $retentions = [];
            if ($rete_data) {
                foreach ($rete_data as $row) {
                    $retentions[$row['type']][] = $row;
                }
            }
            $this->data['retentions'] = $retentions;
            $this->load_view($this->theme . 'creditors/add', $this->data);
        }
    }

    function edit($id = NULL)
    {
        $this->sma->checkPermissions(false, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $company_details = $this->companies_model->getCompanyByID($id);
        if ($this->input->post('email') != $company_details->email) {
            $this->form_validation->set_rules('code', lang("email_address"), 'is_unique[companies.email]');
        }

        if ($this->form_validation->run('companies/add') == true) {
           if ($this->input->post('type_person') == 1) {
                $nombreCompleto = $this->input->post('first_name')." ".$this->input->post('second_name')." ".$this->input->post('first_lastname')." ".$this->input->post('second_lastname');
            } else {
                $nombreCompleto = $this->input->post('name');
            }

            $data = array('name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'group_id' => '9',
                'group_name' => 'creditor',
                'company' => $nombreCompleto,
                'address' => $this->input->post('address'),
                'vat_no' => $this->input->post('vat_no'),
                'city' => $this->input->post('city'),
                'state' => $this->input->post('state'),
                'postal_code' => $this->input->post('postal_code'),
                'name' => $nombreCompleto,
                'country' => $this->input->post('country'),
                'phone' => $this->input->post('phone'),
                'type_person' => $this->input->post('type_person'),
                'tipo_regimen' => $this->input->post('tipo_regimen'),
                'tipo_documento' => $this->input->post('tipo_documento'),
                'digito_verificacion' => $this->input->post('digito_verificacion'),
                'first_name' => $this->input->post('first_name'),
                'second_name' => $this->input->post('second_name'),
                'first_lastname' => $this->input->post('first_lastname'),
                'second_lastname' => $this->input->post('second_lastname'),
                'city_code' => $this->input->post('city_code'),
                "default_rete_fuente_id" => $this->input->post("default_rete_fuente_id"),
                "default_rete_iva_id" => $this->input->post("default_rete_iva_id"),
                "default_rete_ica_id" => $this->input->post("default_rete_ica_id"),
                "default_rete_other_id" => $this->input->post("default_rete_other_id"),
                'customer_validate_min_base_retention' => $this->input->post('customer_validate_min_base_retention') ? 1 : 0,
                "customer_payment_type" => $this->input->post("creditor_payment_type"),
                "customer_credit_limit" => $this->input->post("creditor_credit_limit"),
                "customer_payment_term" => $this->input->post("creditor_payment_term"),
            );
        } elseif ($this->input->post('edit_creditor')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->companies_model->updateCompany($id, $data)) {
            $this->session->set_flashdata('message', $this->lang->line("creditor_updated"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['creditor'] = $company_details;
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['countries'] = $this->companies_model->getCountries();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
            $rete_data = $this->purchases_model->getWithHolding(NULL, 'P');
            $retentions = [];
            if ($rete_data) {
                foreach ($rete_data as $row) {
                    $retentions[$row['type']][] = $row;
                }
            }
            $this->data['retentions'] = $retentions;
            $this->load_view($this->theme . 'creditors/edit', $this->data);
        }
    }

    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->companies_model->deletecreditor($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("creditor_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("creditor_x_deleted_have_purchases")));
        }
    }

    function suggestions($term = NULL, $limit = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->companies_model->getcreditorCreditorSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }

    function getCreditor($id = NULL)
    {
        // $this->sma->checkPermissions('index');
        $row = $this->companies_model->getCompanyByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->company)));
    }

    function creditor_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $error = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->companies_model->deletecreditor($id)) {
                            $error = true;
                        }
                    }
                    if ($error) {
                        $this->session->set_flashdata('warning', lang('creditors_x_deleted_have_purchases'));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("creditors_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('email'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('phone'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('city'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('state'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('postal_code'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('country'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('vat_no'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('scf1'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('scf2'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('scf3'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('scf4'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('scf5'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('scf6'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $customer = $this->site->getCompanyByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $customer->company);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $customer->name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $customer->email);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $customer->phone);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $customer->address);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $customer->city);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $customer->state);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $customer->postal_code);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $customer->country);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $customer->vat_no);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $customer->cf1);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $customer->cf2);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $customer->cf3);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $customer->cf4);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $customer->cf5);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $customer->cf6);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'creditors_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_creditor_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function validate_vat_no($vat_no){
        $validate = $this->companies_model->validate_vat_no($vat_no, 'creditor');
        if ($validate) {
            echo "true";
        } else {
            echo "false";
        }
    }

    public function get_creditor_by_id($id, $biller_id = NULL){
        $creditor = $this->companies_model->getCompanyByID($id);
        $portfolio = $this->companies_model->get_portfolio($id, 'creditor', $biller_id);
        $creditor->portfolio = $portfolio->balance;
        echo json_encode($creditor);
    }

    public function deposit_balance($creditor_id, $amount = 0){

        $creditor = $this->site->getCompanyByID($creditor_id);
        $balance = $creditor->deposit_amount;

        if ($balance > $amount) {
            $type_msg = "primary";
        } else if ($balance < $amount) {
            $type_msg = "danger";
        } else {
            $type_msg = "default";
        }
        $amount_topay = $amount;
        $cdps = $this->companies_model->getDepositsWithBalance($creditor_id, 20);

        // exit(var_dump($cdps));

        $tabla = "<table class='table'>";
        $tabla .= "<tr>
                        <th class='text-center'> Número </th>
                        <th class='text-center'> Origen </th>
                        <th class='text-center'> Fecha </th>
                        <th class='text-center'> Valor </th>
                        <th class='text-center'> Aplicado </th>
                        <th class='text-center'> Balance </th>
                        <th class='text-center'> Valor a aplicar </th>
                   </tr>";
        if ($cdps != FALSE) {
            foreach ($cdps as $deposit) {

                if ($amount_topay == 0) {
                    break;
                }
                $amount_to_apply = 0;
                if ($deposit->balance < $amount_topay) {
                    $amount_topay -= $deposit->balance;
                    $amount_to_apply = $deposit->balance;
                } else if ($deposit->balance >= $amount_topay) {
                    $amount_to_apply = $amount_topay;
                    $amount_topay -= $amount_topay;
                }

                $tabla .= "<tr>
                                <td class='text-center'> ".$deposit->reference_no." </td>
                                <td class='text-center'> ".$deposit->origen_reference_no." </td>
                                <td class='text-center'> ".$deposit->date." </td>
                                <td class='text-center'> ".$this->sma->formatMoney($deposit->amount)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($deposit->amount - $deposit->balance)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($deposit->balance)." </td>
                                <td class='text-right'> ".$this->sma->formatMoney($amount_to_apply)." </td>
                           </tr>";
            }
        } else {
            $tabla .= "<tr>
                            <th colspan='6' class='text-center'> ".lang('creditor_without_balance')." </th>
                       </tr>";
        }

        $tabla .= "<tr>
                        <th colspan='5' class='text-right'> <span class='fa fa-exclamation-circle text-".$type_msg."'></span> <b>".lang('creditor_deposit_balance')."</b> </th>
                        <th class='text-right'> ".$this->sma->formatMoney($balance)." </th>
                   </tr>";

        $tabla .= "</table>";

        $msg_deposit =  '<div class="panel panel-'.$type_msg.' deposit_message" style="border-radius:5px;">
          <div class="panel-body text-center" >
            </hr>
            '.$tabla.'
          </div>
        </div>';

        echo $msg_deposit;
    }

    public function get_creditor_deposit_balance($creditor_id){
        $creditor = $this->companies_model->getCompanyByID($creditor_id);
        $data['deposit_amount'] = $creditor->deposit_amount;
        echo json_encode($data);
    }

    public function get_payment_type($customer_id){
        $customer = $this->companies_model->getCompanyByID($customer_id);
        $data['payment_type'] = $customer->customer_payment_type;
        $data['payment_term'] = $customer->customer_payment_term;
        $data['credit_limit'] = $customer->customer_credit_limit;
        echo json_encode($data);
    }

}
