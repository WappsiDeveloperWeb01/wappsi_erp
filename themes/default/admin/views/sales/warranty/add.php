<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="no-print notificationsContainer">
    <?php if ($error) { ?>
        <div class="alert alert-danger alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <?= is_array($error) ? print_r($error, true) : $error; ?>
        </div>
    <?php } ?>
</div>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <form class="wizard-big wizard" id="generalForm">
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= form_label(lang('date'), 'date') ?>
                                    <?= form_input(["name"=>"date", "id"=>"date", "class"=>"form-control datetime", "required"=>true], date("d/m/Y H:i")) ?>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= form_label(lang('biller'), 'billerId') ?>
                                    <select name="billerId" id="billerId" class="form-control" required>
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if (!empty($billers)) { ?>
                                            <?php $selected = (count($billers) == 1) ? 'selected': '' ?>
                                            <?php foreach ($billers as $biller) {?>
                                                <option value="<?= $biller->id ?>" <?= $selected ?>><?= $biller->name ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= form_label(lang('document_type'), 'document_type') ?>
                                    <select name="document_type" id="document_type" class="form-control" required>
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if (!empty($documentsTypes)) { ?>
                                            <?php foreach ($documentsTypes as $documentType) { ?>
                                            <option value="<?= $documentType->id ?>"><?= "$documentType->sales_prefix - $documentType->nombre" ?></option>
                                            <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <?= form_label(lang('customer'), 'customer_id') ?>
                                    <?= form_input(["name"=>"customer_id", "id"=>"customer_id", "class"=>"form-control", "required"=>true, "placeholder"=>lang("select")]) ?>
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= form_label(lang("customer_branch"), "customer_branch"); ?>
                                    <select name="customer_branch" id="customer_branch" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <?= form_label(lang('seller'), 'seller_id') ?>
                                <select name="seller_id" id="seller_id" class="form-control" required>
                                    <option value=""><?= lang('select') ?></option>
                                    <?php if (!empty($sellers)) { ?>
                                        <?php foreach ($sellers as $seller) { ?>
                                        <option value="<?= $seller->id ?>"><?= $seller->name ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-12">
                        <?= admin_form_open("warranty/create", ["class" => "wizard-big wizard", "id" => "addWarrantyform"]); ?>
                            <h1><?= lang("search_product") ?></h1>
                            <section>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang('reference_no'), 'reference_no') ?>
                                            <?= form_input(["name"=>"reference_no", "id"=>"reference_no", "class"=>"form-control", "placeholder"=>lang("select")]) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang('product'), 'sale_item_id') ?>
                                            <input type="text" name="sale_item_id" id="sale_item_id" class="form-control" required placeholder="<?= lang('select') ?>">
                                            <input type="hidden" name="reference_no_hidden" id="reference_no_hidden">
                                            <input type="hidden" name="sale_id" id="sale_id">
                                            <input type="hidden" name="product_id" id="product_id">
                                        </div>
                                    </div>
                                </div>

                                <hr>

                                <div class="row" id="tableContainer" style="display: none;">
                                    <div class="col-sm-12">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th><?= lang("code") ?></th>
                                                    <th><?= lang("name") ?></th>
                                                    <th><?= lang("price") ?></th>
                                                    <th><?= lang("amount") ?></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </div>
                            </section>

                            <h1><?= lang("detail") ?></h1>
                            <section>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang('warranty_reason'), 'warranty_reason') ?>
                                            <select name="warranty_reason" id="warranty_reason" class="form-control" required>
                                                <option value=""><?= lang('select') ?></option>
                                                <?php if (!empty($warranty_reasons)) { ?>
                                                    <?php foreach ($warranty_reasons as $warranty_reason) { ?>
                                                        <option value="<?= $warranty_reason->id ?>"><?= $warranty_reason->description ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang('contact_means'), 'contact_means') ?>
                                            <select name="contact_means" id="contact_means" class="form-control" required>
                                                <option value=""><?= lang('select') ?></option>
                                                <option value="1"><?= lang('email') ?></option>
                                                <option value="2"><?= lang('whatsapp') ?></option>
                                                <option value="3"><?= lang('call') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= form_label(lang('warranty_details'), 'warranty_details') ?>
                                            <textarea name="warranty_details" id="warranty_details" class="form-control skip" required></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= form_label(lang('product_status'), 'product_status') ?>
                                            <textarea name="product_status" id="product_status" class="form-control skip" required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= form_label(lang('customer_note'), 'customer_note') ?>
                                            <textarea name="customer_note" id="customer_note" class="form-control skip"></textarea>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <?= form_label(lang('internal_note'), 'internal_note') ?>
                                            <textarea name="internal_note" id="internal_note" class="form-control skip"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="">
                                                <input type="checkbox" name="has_it_been_intervened" id="has_it_been_intervened"> <?= form_label(lang('has_it_been_intervened'), 'has_it_been_intervened') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6" id="containerDescription" style="display: none">
                                        <div class="form-group">
                                            <?= form_label(lang('description'), 'description') ?>
                                            <textarea name="description" id="description" class="form-control skip"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </section>

                            <h1><?= lang("customer_data") ?></h1>
                            <section>
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= form_label(lang('customer_since'), 'customer_since') ?>
                                            <?= form_input(["name"=>"customer_since", "id"=>"customer_since", "class"=>"form-control", "type"=>"datetime", "readonly"=>true]) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= form_label(lang("customer_qualification"), "customer_qualification") ?>
                                            <select name="customer_qualification" id="customer_qualification" class="form-control" required>
                                                <option value=""><?= lang("select") ?></option>
                                                <?php if (!empty($customerQualifications)) { ?>
                                                    <?php foreach ($customerQualifications as $qualification) { ?>
                                                        <option value="<?= $qualification->id ?>"><?= $qualification->description ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= form_label(lang("number_credits"), "number_credits") ?>
                                            <?= form_input(["name"=>"number_credits", "id"=>"number_credits", "class"=>"form-control", "type"=>"number", "required"=>true]) ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= form_label(lang("customer_attitude"), "customer_attitude") ?>
                                            <select name="customer_attitude" id="customer_attitude" class="form-control" required>
                                                <option value=""><?= lang("select") ?></option>
                                                <?php if (!empty($customerAttitude)) { ?>
                                                    <?php foreach ($customerAttitude as $attitude) { ?>
                                                        <option value="<?= $attitude->id ?>"><?= $attitude->description ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>


                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= form_label(lang("maximum_purchase"), "maximum_purchase") ?>
                                            <?= form_input(["name"=>"maximum_purchase", "id"=>"maximum_purchase", "class"=>"form-control", "type"=>"number", "required"=>true]) ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        _generalForm = '';
        _warrantyform = '';
        _continueProcess = false;
        autocompleteCustomer();
        loadWizard();

        $(document).on('ifToggled', '#has_it_been_intervened', function() { enabledDisabledContainerDescription($(this)); });
        $(document).on('change', '#billerId', function() { loadDocumentsType($(this)); loadSellers($(this)); });
        $(document).on('change', '#customer_id', function() { loadCustomerBranch($(this)); loadcustomerData($(this)); });
        $(document).on('change', '#reference_no', function() { getReferenceNo($(this)) });
        $(document).on('change', '#sale_item_id', function() { showTable($(this)) });
        $(document).on('click', '#saveButton', function() { saveWarranty(); });

        $('[data-toggle="tooltip"]').tooltip();
    });

    function loadDocumentsType(element)
    {
        let billerId = element.val();
        let options = '<option value=""><?= lang('select') ?></option>';

        $('#document_type').select2('val', '');

        $.ajax({
            type: "post",
            url: "<?= admin_url("sales/getDocumentTypes") ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'billerId': element.val(),
                'asynchronous': 1,
                'electronicDocument': 0,
                'modules': [56],
            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(documentType => {
                        options += '<option value="'+documentType.id+'">'+documentType.sales_prefix+' - '+documentType.nombre+'</option>';
                    });
                    $('#document_type').html(options);
                } else {
                    swal({
                        title: "¡Advertencia!",
                        text: "No se ha creado el tipo de documento para garantías",
                        type: 'warning',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok",
                        closeOnClickOutside: false
                    });
                }
            }
        });
    }

    function loadSellers(element)
    {
        $('#seller_id').select2('val', '');

        $.ajax({
            type: "get",
            url: "<?= admin_url("sales/getSellers") ?>",
            data: {
                'biller_id': element.val(),
            },
            dataType: "html",
            success: function (response) {
                if (response != false) {
                    $('#seller_id').html(response);
                }
            }
        });
    }

    function autocompleteCustomer()
    {
        $('#customer_id').val('').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function loadCustomerBranch(element)
    {
        $('#customer_branch').select2('val', '');
        $('#sale_item_id').select2('val', '');
        showTable($('#sale_item_id'));

        if (element.val()) {
            $.ajax({
                type: "get",
                url: site.base_url+"sales/getCustomerAddresses/",
                data: {
                    customer_id: element.val()
                },
                dataType: "html",
                success: function (response) {
                    $('#customer_branch').html(response);
                }
            });
        }
    }

    function loadcustomerData(element)
    {
        $.ajax({
            type: "POST",
            url: site.base_url+'warranty/findCustomerAjax',
            data: {
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                customer_id : element.val()
            },
            dataType: "json",
            success: function (response) {
                console.log(response.registration_date);
                if (response) {
                    $('#customer_since').val(response.registration_date);
                }
            }
        });
    }

    function loadWizard()
    {
        _warrantyform = $("#addWarrantyform");
        _warrantyform.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: "Guardar",
                next: "Siguiente",
                previous: "Anterior",
                loading: "Loading ..."
            },
            onInit: function(event, currentIndex) {
                autocompleteInvoices();
                autocompleteProducts();
                hideShowStyleDanger();
                loadGeneralFormVallidate();
                loadWarrantyFormValidate();
            },
            onStepChanging: function (event, currentIndex, newIndex) {
                if (_generalForm.valid() && _warrantyform.valid()) {
                    if (!_continueProcess) {
                        validateProduct($('#sale_item_id'));
                    } else {
                        return true;
                    }
                } else {
                    return false;
                }
            },
            onStepChanged: function(event, currentIndex) {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();
            },
            onFinishing: function (event, currentIndex) {
                _warrantyform.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                return _warrantyform.valid();
            },
            onFinished: function (event, currentIndex) {
                _warrantyform.validate().settings.ignore = ":disabled";
                if(_warrantyform.valid()){
                    movingInput();

                    swal({
                        title: 'Creando nuevo garantía...',
                        text: 'Por favor, espere un momento.',
                        type: 'info',
                        showCancelButton: false,
                        showConfirmButton: false,
                        closeOnClickOutside: false,
                    });

                    _warrantyform.submit();
                }
            }
        });
    }

    function loadGeneralFormVallidate()
    {
        _generalForm = $("#generalForm");
        _generalForm.validate({
            ignore: [],
            rules: {
                customer_branch: "required",
                document_type: "required",
                customer_id: "required",
                billerId: "required",
                seller_id: "required",
            },
            messages: {
                customer_branch: "<?= sprintf(lang("required_input"),  lang('customer_branch')) ?>",
                document_type: "<?= sprintf(lang("required_input"),  lang('document_type')) ?>",
                customer_id: "<?= sprintf(lang("required_input"),  lang('customer')) ?>",
                billerId: "<?= sprintf(lang("required_input"),  lang('biller')) ?>",
                seller_id: "<?= sprintf(lang("required_input"),  lang('seller')) ?>",
            }
        });
    }

    function loadWarrantyFormValidate()
    {
        $.validator.addMethod("select2", function(value, element) {
            return $(element).val() !== null;
        }, "Por favor, selecciona una opción.");

        _warrantyform.validate({
            rules: {
                sale_item_id:"required"
            },
            messages: {
                sale_item_id: "<?= sprintf(lang("required_input"),  lang('product')) ?>"
            }
        }).settings.ignore = ":disabled,:hidden:not(.validate)";

        $("#sale_item_id").rules("add", {
            select2: true
        });
    }

    function hideShowStyleDanger()
    {
        $(document).on("select2-opening", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id")).hasClass("error")) {
                $(".select2-drop ul").addClass("error");
            } else {
                $(".select2-drop ul").removeClass("error");
            }
        });

        $(document).on("select2-close", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id")).hasClass("error") && $("#" + elem.attr("id")).val() != '') {
                $("#s2id_" + elem.attr("id")).removeClass('error');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');

                $("#" + elem.attr("id") + "-error").css('display', 'none');
            }
        });
    }

    function autocompleteInvoices()
    {
        $('#reference_no').val('').select2({
            minimumInputLength: 1,
            data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url+"warranty/getProduct/",
                        dataType: "json",
                        success: function (data) {
                            callback(data[0])
                        }
                    });
                },
            ajax: {
                url: site.base_url + "warranty/getInvoiceSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        customer_id : $('#customer_id').val(),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function autocompleteProducts()
    {
        $('#sale_item_id').val('').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"warranty/getProduct/",
                    dataType: "json",
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "warranty/getProductSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        reference_no : $('#reference_no').val(),
                        customer_id : $('#customer_id').val(),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (!$('#customer_id').val()) {
                        Command: toastr.error('<?= sprintf(lang("required_input"),  lang('customer')) ?>', '<?= lang("error") ?>', {
                            onHidden: function() { $('#sale_item_id').select2('close'); $('#customer_id').select2('open'); }
                        });
                        return {results: [{id: '', text: lang.no_match_found}]};
                    } else {
                        if (data.results) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            }
        });
    }

    function showTable(element)
    {
        let tableBody = '';
        if (element.val()) {
            $.ajax({
                type: "post",
                url: site.base_url+'warranty/getItem',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    itemId: element.val()
                },
                dataType: "json",
                success: function (response) {
                    if (response) {
                        tableBody = '<tr>'+
                            '<td>'+response.code+'</td>'+
                            '<td>'+response.product_name+'</td>'+
                            '<td class="right">'+response.unit_price+'</td>'+
                            '<td class="center">'+1+'</td>'+
                        '</tr>'
                        $('#tableContainer table tbody').html(tableBody);

                        $('#tableContainer').fadeIn();

                        $('#reference_no_hidden').val(response.reference_no);
                        $('#sale_id').val(response.sale_id);
                        $('#product_id').val(response.product_id);

                        validateProduct(element)
                    }
                }
            });
        } else {
            $('#tableContainer table tbody').html(tableBody);
            $('#tableContainer').fadeOut();
        }
    }

    function validateProduct(element)
    {
        if (element.val()) {
            $.ajax({
                type: "POST",
                url: site.base_url+"warranty/validate",
                data: {
                    "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                    productId: element.val(),
                    date: $('#date').val()
                },
                dataType: "json",
                success: function (response) {
                    if (response) {
                        let notification = '';
                        response.forEach(element => {
                            _continueProcess = element.continue;
                            notification += '<div class="alert alert-'+element.type+' alert-dismissable">'+
                                '<button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>'+
                                element.message +
                            '</div>';
                        });
                        $('.notificationsContainer').html(notification);
                    }
                }
            });
        } else {
            Command: toastr.error('<?= sprintf(lang("required_input"),  lang('product')) ?>', '<?= lang("error") ?>', {
                onHidden: function() {
                    $('#sale_item_id').select2('open');
                    $("#s2id_product_id").addClass('error');
                    $("#s2id_product_id span.select2-arrow, u.select2-results").addClass('mierroArrow');
                    $('#sale_item_id').after('<label id="sale_item_id-error" class="error" for="sale_item_id" style="">El campo Producto es obligatorio</label>');
                }
            });
        }
    }

    function getReferenceNo(element)
    {
        $('#sale_item_id').select2('val', '');
        $('#reference_no_hidden').val('');
        $('#product_id').val('');
        $('#sale_id').val('');
    }

    function enabledDisabledContainerDescription(element)
    {
        if (element.is(':checked')) {
            $('#containerDescription').fadeIn();
            $('#description').prop('required', true);
        } else {
            $('#containerDescription').fadeOut();
            $('#description').prop('required', false);
        }
    }

    function movingInput()
    {
        let reference_no_hidden = $("#generalForm [name='reference_no_hidden']");
        let customer_branch = $("#generalForm [name='customer_branch']");
        let document_type = $("#generalForm [name='document_type']");
        let reference_no = $("#generalForm [name='reference_no']");
        let customer_id = $("#generalForm [name='customer_id']");
        let product_id = $("#generalForm [name='prduct_id']");
        let seller_id = $("#generalForm [name='seller_id']");
        let biller_id = $("#generalForm [name='billerId']");
        let sale_id = $("#generalForm [name='sale_id']");
        let date = $("#generalForm [name='date']");

        $("#addWarrantyform").append(date, biller_id, document_type, customer_id, customer_branch, seller_id, product_id, reference_no, reference_no_hidden, sale_id);
    }
</script>