<?php
    $apppath = str_replace("\app", "", APPPATH);
    $apppath = str_replace("/app", "", $apppath);
    require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';

    class PDF extends HPDF
    {
        public $initialX = 0;
        public $initialY = 0;

        function Header()
        {
            $widthColumn = ($this->GetPageWidth() - ($this->marginWidth * 2)) / 3;

            $this->SetTitle($this->pageTitle, true);

            $this->Image(base_url($this->imagePath), 10, 10, 47);

            $this->SetTextColor(98);
            $this->SetDrawColor(180);
            $this->SetLineWidth(0.4);

            $this->initialX = $this->GetX();
            $this->initialY = $this->GetY();

            $this->SetXY($this->initialX +  $widthColumn, $this->initialY);
            $this->SetFont('Arial','B', 16);
            $this->Cell($widthColumn, 5, $this->sma->utf8Decode($this->pageTitle), 0, 0, 'C');

            $this->SetXY($this->initialX +  $widthColumn, $this->initialY + 5);
            $this->SetFont('Arial','', 10);
            $this->Cell($widthColumn, 5, $this->sma->utf8Decode('Periodo de liquidación'), 0, 0, 'C');

            $this->SetXY($this->initialX +  $widthColumn, $this->initialY + 10);
            $this->SetFont('Arial','', 10);
            $this->Cell($widthColumn, 5, $this->sma->utf8Decode($this->startDate .' - '. $this->endDate), 0, 0, 'C');

            $this->SetXY($this->initialX + ($widthColumn * 2), $this->initialY + 5);
            $this->Cell(($widthColumn / 2) + 10, 5, $this->sma->utf8Decode('Salario Mínimo Legal Vigente: '), 1, 0);
            $this->Cell(($widthColumn / 2) - 10, 5, $this->sma->utf8Decode('$ ' . number_format($this->minimumSalaryValue, 2)), 1, 0, 'R');

            $this->SetXY($this->initialX + ($widthColumn * 2), $this->initialY + 10);
            $this->Cell(($widthColumn / 2) + 10, 5, $this->sma->utf8Decode('Subsidio de transporte: '), 1, 0);
            $this->Cell(($widthColumn / 2) - 10, 5, $this->sma->utf8Decode('$ ' . number_format($this->transportationAllowanceValue, 2)), 1, 0, 'R');

            if ($this->statusPayroll == IN_PREPARATION) {
                $this->SetFont('Arial', 'B', 60);
                $this->SetTextColor(235);
                $this->RotatedText(60, 190, $this->sma->utf8Decode("PLANILLA NO APROBADA"), 33);
            }

            $this->Ln(10);
        }

        function Footer()
        {
            $this->SetY(-15);

            $widthColumnFooter = ($this->GetPageWidth() - ($this->marginWidth * 2)) / 2;
            $this->SetFont('Arial','',8);
            $this->Cell(0, 4 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS NIT 901.090.070-9  www.wappsi.com'), 0, 1, 'C');
            $this->Cell(0, 4, $this->sma->utf8Decode('Página '.$this->PageNo().' / {nb}'), 0, 0, 'C');
        }

        function generalTable($header, $widthHeader, $data = NULL, $headerMain = NULL, $widthHeaderMain = [])
        {
            $this->SetFont('Arial','B', 10);
            if (!empty($headerMain)) {
                for($i = 0; $i < count($headerMain); $i++) {
                    $this->Cell($widthHeaderMain[$i], 7, $this->sma->utf8Decode($headerMain[$i]), 1, 0, 'C');
                }
            }
            $this->Ln();

            $xOld = $this->GetX();
            $yOld = $this->GetY();

            $x = $this->GetX();
            $y = $this->GetY();
            $this->SetFont('Arial','B', 7);

            $this->SetTextColor(255);
            $higher = $yBefore = 0;
            for($i = 0; $i < count($header); $i++) {
                $this->MultiCell($widthHeader[$i], 3, $this->sma->utf8Decode($header[$i]), 0, 'R');
                $yBefore = $this->GetY() - $yOld;

                if ($yBefore > $higher) {
                    $higher = $yBefore;
                }

                $this->SetXY(($x + $widthHeader[$i]), $y);
                $x = $this->GetX();
            }

            $this->SetXY($xOld, $yOld);
            $this->SetTextColor(86);
            for($i = 0; $i < count($header); $i++) {
                $this->drawTextBox($this->sma->utf8Decode($header[$i]), $widthHeader[$i], $higher, 'C', 'M');
                $this->SetXY(($xOld + $widthHeader[$i]), $yOld);
                $xOld = $this->GetX();
            }
            $this->Ln($higher);


            $this->SetFont('Arial','', 7);
            $higher = $yBefore = $yBeforeRow = $yAfterRow = 0;
            $positionX = $this->marginWidth;
            for ($i=0; $i < count($data); $i++) {
                $higher = 0;
                // if ($i < 38) {
                    $this->SetTextColor(255);
                    $yBeforeRow = $this->GetY();
                    $this->MultiCell($widthHeader[0], 4, $this->sma->utf8Decode($data[$i]->documentNumber), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }

                    $positionColumn1 = $positionX + $widthHeader[0];
                    $this->SetXY($positionColumn1, $yBeforeRow);
                    $this->MultiCell($widthHeader[1], 4 , $this->sma->utf8Decode(ucwords(strtolower($data[$i]->name))), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }

                    $positionColumn2 = $positionColumn1 + $widthHeader[1];
                    $this->SetXY($positionColumn2, $yBeforeRow);
                    $this->MultiCell($widthHeader[2], 4 , $this->sma->utf8Decode(number_format($data[$i]->baseSalary, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn3 = $positionColumn2 + $widthHeader[2];
                    $this->SetXY($positionColumn3, $yBeforeRow);
                    $this->MultiCell($widthHeader[3], 4 , $this->sma->utf8Decode($data[$i]->daysQuantity), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn4 = $positionColumn3 + $widthHeader[3];
                    $this->SetXY($positionColumn4, $yBeforeRow);
                    $this->MultiCell($widthHeader[4], 4 , $this->sma->utf8Decode(number_format($data[$i]->salary, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn5 = $positionColumn4 + $widthHeader[4];
                    $this->SetXY($positionColumn5, $yBeforeRow);
                    $this->MultiCell($widthHeader[5], 4 , $this->sma->utf8Decode(number_format($data[$i]->transportSubsidy, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn6 = $positionColumn5 + $widthHeader[5];
                    $this->SetXY($positionColumn6, $yBeforeRow);
                    $this->MultiCell($widthHeader[6], 4 , $this->sma->utf8Decode(number_format($data[$i]->overtimeSurcharges, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn7 = $positionColumn6 + $widthHeader[6];
                    $this->SetXY($positionColumn6, $yBeforeRow);
                    $this->MultiCell($widthHeader[7], 4 , $this->sma->utf8Decode(number_format($data[$i]->salaryPayments, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn8 = $positionColumn7 + $widthHeader[7];
                    $this->SetXY($positionColumn8, $yBeforeRow);
                    $this->MultiCell($widthHeader[8], 4 , $this->sma->utf8Decode(number_format($data[$i]->notSalaryPayments, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }

                    $totalEarneds = ($data[$i]->salary + $data[$i]->transportSubsidy + $data[$i]->overtimeSurcharges + $data[$i]->salaryPayments + $data[$i]->notSalaryPayments);
                    $positionColumn9 = $positionColumn8 + $widthHeader[8];
                    $this->SetXY($positionColumn9, $yBeforeRow);
                    $this->MultiCell($widthHeader[9], 4 , $this->sma->utf8Decode(number_format($totalEarneds, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn10 = $positionColumn9 + $widthHeader[9];
                    $this->SetXY($positionColumn10, $yBeforeRow);
                    $this->MultiCell($widthHeader[10], 4 , $this->sma->utf8Decode(number_format($data[$i]->Health, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn11 = $positionColumn10 + $widthHeader[10];
                    $this->SetXY($positionColumn11, $yBeforeRow);
                    $this->MultiCell($widthHeader[11], 4 , $this->sma->utf8Decode(number_format($data[$i]->Pension, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn12 = $positionColumn11 + $widthHeader[11];
                    $this->SetXY($positionColumn12, $yBeforeRow);
                    $this->MultiCell($widthHeader[12], 4 , $this->sma->utf8Decode(number_format($data[$i]->otherDeductions, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }

                    $totalDeductions = $data[$i]->Health + $data[$i]->Pension + $data[$i]->otherDeductions;
                    $positionColumn13 = $positionColumn12 + $widthHeader[12];
                    $this->SetXY($positionColumn13, $yBeforeRow);
                    $this->MultiCell($widthHeader[13], 4 , $this->sma->utf8Decode(number_format($totalDeductions, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }

                    $total = $totalEarneds - $totalDeductions;
                    $positionColumn14 = $positionColumn13 + $widthHeader[13];
                    $this->SetXY($positionColumn14, $yBeforeRow);
                    $this->MultiCell($widthHeader[14], 4 , $this->sma->utf8Decode(number_format($total, 2)), 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);
                    }
                    $positionColumn15 = $positionColumn14 + $widthHeader[14];
                    $this->SetXY($positionColumn15, $yBeforeRow);
                    $this->MultiCell($widthHeader[15], 4 , "", 0, 'R');

                    $yAfterRow = $this->GetY();
                    if (($yAfterRow - $yBeforeRow) > $higher) {
                        $higher = ($yAfterRow - $yBeforeRow);

                    }

                    $this->SetTextColor(86);
                    $this->SetY($yBeforeRow);
                    $this->drawTextBox($data[$i]->documentNumber, $widthHeader[0], $higher, 'R', 'M');
                    $this->SetXY($positionColumn1, $yBeforeRow);
                    $this->drawTextBox($this->sma->utf8Decode(ucwords(strtolower($data[$i]->name))), $widthHeader[1], $higher, 'L', 'M');
                    $this->SetXY($positionColumn2, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->baseSalary)) ? number_format($data[$i]->baseSalary, 2) : $this->sma->utf8Decode(ucwords($data[$i]->baseSalary)), $widthHeader[2], $higher, 'R', 'M');
                    $this->SetXY($positionColumn3, $yBeforeRow);
                    $this->drawTextBox($this->sma->utf8Decode(ucwords($data[$i]->daysQuantity)), $widthHeader[3], $higher, 'C', 'M');
                    $this->SetXY($positionColumn4, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->salary)) ? number_format($data[$i]->salary, 2) : $this->sma->utf8Decode(ucwords($data[$i]->salary)), $widthHeader[4], $higher, 'R', 'M');
                    $this->SetXY($positionColumn5, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->transportSubsidy)) ? number_format($data[$i]->transportSubsidy, 2) : $this->sma->utf8Decode(ucwords($data[$i]->transportSubsidy)), $widthHeader[5], $higher, 'R', 'M');
                    $this->SetXY($positionColumn6, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->overtimeSurcharges)) ? number_format($data[$i]->overtimeSurcharges, 2) : $this->sma->utf8Decode(ucwords($data[$i]->overtimeSurcharges)), $widthHeader[6], $higher, 'R', 'M');
                    $this->SetXY($positionColumn7, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->salaryPayments)) ? number_format($data[$i]->salaryPayments, 2) : $this->sma->utf8Decode(ucwords($data[$i]->salaryPayments)), $widthHeader[7], $higher, 'R', 'M');
                    $this->SetXY($positionColumn8, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->notSalaryPayments)) ? number_format($data[$i]->notSalaryPayments, 2) : $this->sma->utf8Decode(ucwords($data[$i]->notSalaryPayments)), $widthHeader[8], $higher, 'R', 'M');
                    $this->SetXY($positionColumn9, $yBeforeRow);
                    $this->drawTextBox((is_numeric($totalEarneds)) ? number_format($totalEarneds, 2) : $this->sma->utf8Decode(ucwords($totalEarneds)), $widthHeader[9], $higher, 'R', 'M');
                    $this->SetXY($positionColumn10, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->Health)) ? number_format($data[$i]->Health, 2) : $this->sma->utf8Decode(ucwords($data[$i]->Health)), $widthHeader[10], $higher, 'R', 'M');
                    $this->SetXY($positionColumn11, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->Pension)) ? number_format($data[$i]->Pension, 2) : $this->sma->utf8Decode(ucwords($data[$i]->Pension)), $widthHeader[11], $higher, 'R', 'M');
                    $this->SetXY($positionColumn12, $yBeforeRow);
                    $this->drawTextBox((is_numeric($data[$i]->otherDeductions)) ? number_format($data[$i]->otherDeductions, 2) : $this->sma->utf8Decode(ucwords($data[$i]->otherDeductions)), $widthHeader[12], $higher, 'R', 'M');
                    $this->SetXY($positionColumn13, $yBeforeRow);
                    $this->drawTextBox((is_numeric($totalDeductions)) ? number_format($totalDeductions, 2) : $this->sma->utf8Decode(ucwords($totalDeductions)), $widthHeader[13], $higher, 'R', 'M');
                    $this->SetXY($positionColumn14, $yBeforeRow);
                    $this->drawTextBox((is_numeric($total)) ? number_format($total, 2) : $this->sma->utf8Decode(ucwords($total)), $widthHeader[14], $higher, 'R', 'M');
                    $this->SetXY($positionColumn15, $yBeforeRow);
                    $this->drawTextBox("", $widthHeader[15], $higher, 'R', 'M');

                    $ycurrent = $yBeforeRow + $higher;

                    if ($ycurrent > 198) {
                        $this->AddPage();
                    } else {
                        $this->SetY($yBeforeRow + $higher);
                    }

                // }
            }

            $this->Ln(10);
        }

        function secondaryTable($headerMain, $widthHeaderMain, $header, $widthColumnHeader, $data)
        {
            $xBefore = $this->GetX();

            $this->SetFont('Arial','B', 10);
            if (!empty($headerMain)) {
                for($i = 0; $i < count($headerMain); $i++) {
                    $this->Cell($widthHeaderMain[$i], 7, $this->sma->utf8Decode($headerMain[$i]), 1, 0, 'C');
                }
                $this->Ln();
            }

            $this->SetX($xBefore);

            if (!empty($header)) {
                for($i = 0; $i < count($header); $i++) {
                    $this->Cell($widthColumnHeader[$i], 7, $this->sma->utf8Decode($header[$i]), 1, 0, 'C');
                }
                $this->Ln();
            }

            $this->SetFont('Arial','', 7);
            foreach ($data as $value) {
                $this->SetX($xBefore);
                $this->Cell($widthColumnHeader[0], 4, $this->sma->utf8Decode($value->description), 1, 0);
                $this->Cell($widthColumnHeader[1], 4, number_format($value->percentage, 2), 1, 0, 'R');
                $this->Cell($widthColumnHeader[2], 4, number_format($value->amount, 2), 1, 0, 'R');
                $this->Ln();
            }

            $this->SetX($xBefore);
        }
    }

    $pdf = new PDF('L', 'mm', [220, 340]);

    $pdf->period = $period;
    $pdf->marginWidth = 10;
    $pdf->endDate = $endDate;
    $pdf->concepts = $concepts;
    $pdf->imagePath = $imagePath;
    $pdf->pageTitle = $pageTitle;
    $pdf->startDate = $startDate;
    $pdf->payrollItem = $payrollItems;
    $pdf->statusPayroll = $statusPayroll;
    $pdf->generationDate = $generationDate;
    $pdf->minimumSalaryValue = $minimumSalaryValue;
    $pdf->widthPageWithoutMargins = $pdf->GetPageWidth() - 20;
    $pdf->widthColumnHeader = $pdf->widthPageWithoutMargins / 2;
    $pdf->transportationAllowanceValue = $transportationAllowanceValue;

    $pdf->AliasNbPages();
    $pdf->sma = $this->sma;
$pdf->SetMargins(10, 10, 10);
    $pdf->SetAutoPageBreak(TRUE, 10);

    $pdf->AddPage();
    $pdf->SetTextColor(98);
    $pdf->SetDrawColor(180);
    $pdf->SetLineWidth(0.4);

    $widthHeader = [17,39,19,8,18,18,18,18,18,18,18,18,18,18,18,39];
    $headerMain = ['Datos empleado', 'Devengados', 'Deducidos', 'Totales'];
    $header = ['Documento','Nombre','Salario básico','Días','Salario','Subsidio transporte','Horas extras y recargos','Constitutivos de salario','No constitutivos de salario','Total','Salud','Pensión','Otros','Total','Neto a pagar','Firma'];
    $widthHeaderMain = [
        $widthHeader[0] + $widthHeader[1] + $widthHeader[2] + $widthHeader[3],
        $widthHeader[4] + $widthHeader[5] + $widthHeader[6] + $widthHeader[7] + $widthHeader[8] + $widthHeader[9],
        $widthHeader[10] + $widthHeader[11] + $widthHeader[12] + $widthHeader[13],
        $widthHeader[14] + $widthHeader[15],
    ];
    $pdf->generalTable($header, $widthHeader, $payrollItems, $headerMain, $widthHeaderMain);

    if ($hideShowParafiscalProvisions == TRUE) {
        $xBeforeGeneralTable = $pdf->GetX();
        $yBeforeGeneralTable = $pdf->GetY();

        $widthHeader = [45, 8, 20];
        $headerMain = ['Seguridal Social Empresa'];
        $widthHeaderMain = [$widthHeader[0] + $widthHeader[1] + $widthHeader[2]];
        $header = ['Concepto', '%', 'Valor'];
        $pdf->secondaryTable($headerMain, $widthHeaderMain, $header, $widthHeader, $socialSecurity);

        $xBeforeSecondTable = $pdf->GetX();

        $pdf->SetXY(($xBeforeSecondTable + $widthHeaderMain[0] + 10), $yBeforeGeneralTable);

        $widthHeader = [45, 8, 20];
        $headerMain = ['Seguridal Social Empleado'];
        $widthHeaderMain = [$widthHeader[0] + $widthHeader[1] + $widthHeader[2]];
        $header = ['Concepto', '%', 'Valor'];
        $pdf->secondaryTable($headerMain, $widthHeaderMain, $header, $widthHeader, $healthPension);

        $xAfterSecondTable = $pdf->GetX();

        $pdf->SetXY(($xAfterSecondTable + $widthHeaderMain[0] + 10), $yBeforeGeneralTable);

        $widthHeader = [43, 8, 20];
        $headerMain = ['Parafiscales'];
        $widthHeaderMain = [$widthHeader[0] + $widthHeader[1] + $widthHeader[2]];
        $header = ['Concepto', '%', 'Valor'];
        $pdf->secondaryTable($headerMain, $widthHeaderMain, $header, $widthHeader, $parafiscal);

        $xAfterSecondTable = $pdf->GetX();

        $pdf->SetXY(($xAfterSecondTable + $widthHeaderMain[0] + 10), $yBeforeGeneralTable);

        $widthHeader = [45, 8, 20];
        $headerMain = ['Prestaciones sociales'];
        $widthHeaderMain = [$widthHeader[0] + $widthHeader[1] + $widthHeader[2]];
        $header = ['Concepto', '%', 'Valor'];
        $pdf->secondaryTable($headerMain, $widthHeaderMain, $header, $widthHeader, $provisions);
    }

    $pdf->Output('I','Planilla de Nómina'.'.pdf');