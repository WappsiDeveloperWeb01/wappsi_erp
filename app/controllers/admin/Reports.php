<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Reports extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->lang->admin_load('reports', $this->Settings->user_language);
        $this->lang->admin_load('sma', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('reports_model');
        $this->load->admin_model('pos_model');
        $this->data['pb'] = array(
            'cash' => lang('cash'),
            'CC' => lang('CC'),
            'Cheque' => lang('Cheque'),
            'paypal_pro' => lang('paypal_pro'),
            'stripe' => lang('stripe'),
            'gift_card' => lang('gift_card'),
            'deposit' => lang('deposit'),
            'authorize' => lang('authorize'),
            );
    }
    function index()
    {
        $this->sma->checkPermissions();
        $data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['monthly_sales'] = $this->reports_model->getChartData();
        $this->data['stock'] = $this->reports_model->getStockValue();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reports')));
        $meta = array('page_title' => lang('reports'), 'bc' => $bc);
        $this->page_construct('reports/index', $meta, $this->data);
    }
    function warehouse_stock($warehouse = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);
        $data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->input->get('warehouse')) {
            $warehouse = $this->input->get('warehouse');
        }
        $this->data['stock'] = $warehouse ? $this->reports_model->getWarehouseStockValue($warehouse) : $this->reports_model->getStockValue();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse;
        $this->data['warehouse'] = $warehouse ? $this->site->getWarehouseByID($warehouse) : NULL;
        $this->data['totals'] = $this->reports_model->getWarehouseTotals($warehouse);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('reports')));
        $meta = array('page_title' => lang('reports'), 'bc' => $bc);
        $this->page_construct('reports/warehouse_stock', $meta, $this->data);
    }

    function expiry_alerts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('expiry_alerts');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $user = $this->site->getUser();
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $user->warehouse_id;
            $this->data['warehouse'] = $user->warehouse_id ? $this->site->getWarehouseByID($user->warehouse_id) : NULL;
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('product_expiry_alerts')));
        $meta = array('page_title' => lang('product_expiry_alerts'), 'bc' => $bc);
        $this->page_construct('reports/expiry_alerts', $meta, $this->data);
    }

    function getExpiryAlerts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('expiry_alerts', TRUE);
        $date = date('Y-m-d', strtotime('+3 months'));
        if (!$this->Owner && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("image, product_code, product_name, quantity_balance, warehouses.name, expiry")
                ->from('purchase_items')
                ->join('products', 'products.id=purchase_items.product_id', 'left')
                ->join('warehouses', 'warehouses.id=purchase_items.warehouse_id', 'left')
                ->where('warehouse_id', $warehouse_id)
                ->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')
                ->where('expiry <', $date);
        } else {
            $this->datatables
                ->select("image, product_code, product_name, quantity_balance, warehouses.name, expiry")
                ->from('purchase_items')
                ->join('products', 'products.id=purchase_items.product_id', 'left')
                ->join('warehouses', 'warehouses.id=purchase_items.warehouse_id', 'left')
                ->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')
                ->where('expiry <', $date);
        }
        echo $this->datatables->generate();
    }

    function quantity_alerts($warehouse_id = NULL)
    {
        $warehouse_id ? $warehouse_id : $this->session->userdata('warehouse_id');
        $this->sma->checkPermissions('quantity_alerts');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('product_quantity_alerts')));
        $meta = array('page_title' => lang('product_quantity_alerts'), 'bc' => $bc);
        $this->page_construct('reports/quantity_alerts', $meta, $this->data);
    }

    function getQuantityAlerts($warehouse_id = NULL, $pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('quantity_alerts', TRUE);
        $warehouse_id = !$warehouse_id ? $this->input->post('warehouse_id') : $warehouse_id;
        $biller_id = $this->input->post('biller_id');

        if ($pdf || $xls) {

            if ($warehouse_id) {
                $this->db
                    ->select('products.image as image, products.code, products.name, warehouses_products.quantity, alert_quantity')
                    ->from('products')->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
                    ->where('alert_quantity > warehouses_products.quantity')
                    ->where('warehouse_id', $warehouse_id)
                    // ->where('min_quantity > 0')
                    // ->where('min_quantity > warehouses_products.quantity')
                    ->where('track_quantity', 1)
                    ->order_by('products.code desc');
            } else {
                $this->db
                    ->select('image, code, name, quantity, alert_quantity')
                    ->from('products')
                    ->where('alert_quantity > quantity', NULL)
                    ->where('track_quantity', 1)
                    ->order_by('code desc');
            }
            if ($biller_id) {
                $this->db
                    ->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
                $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('product_quantity_alerts'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('quantity'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('alert_quantity'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->code);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->quantity);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->alert_quantity);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'product_quantity_alerts';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            if ($warehouse_id) {
                $this->datatables
                    ->select('image, code, name, warehouses_products.quantity, alert_quantity as alert_quantity')
                    ->from('products')
                    ->join("warehouses_products", 'products.id=warehouses_products.product_id', 'left')
                    ->where('track_quantity', 1)
                    ->where('alert_quantity > 0')
                    ->where('alert_quantity > warehouses_products.quantity')
                    ->where('warehouses_products.warehouse_id', $warehouse_id)
                    ->group_by('products.id');
            } else {
                $this->datatables
                    ->select('image, code, name, quantity, alert_quantity')
                    ->from('products')
                    ->where('alert_quantity > quantity', NULL)
                    ->where('track_quantity', 1);
            }

            if ($biller_id) {
                $this->datatables
                    ->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
                $this->datatables->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
            }

            echo $this->datatables->generate();

        }

    }

    function suggestions()
    {
        $term = $this->input->get('term', TRUE);
        $type = $this->input->get('type', TRUE);
        if (strlen($term) < 1) {
            die();
        }
        $rows = $this->reports_model->getProductNames($term, $type);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")");

            }
            $this->sma->send_json($pr);
        } else {
            echo FALSE;
        }
    }

    function suggestions_variant()
    {
        $term = $this->input->get('term', TRUE);
        $type = $this->input->get('type', TRUE);
        if (strlen($term) < 1) {
            die();
        }
        $rows = $this->reports_model->getVariantCodeName($term, $type);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")");

            }
            $this->sma->send_json($pr);
        } else {
            echo FALSE;
        }
    }

    public function best_sellers($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('products');

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $y1 = date('Y', strtotime('-1 month'));
        $m1 = date('m', strtotime('-1 month'));
        $m1sdate = $y1.'-'.$m1.'-01 00:00:00';
        $m1edate = $y1.'-'.$m1.'-'. days_in_month($m1, $y1) . ' 23:59:59';
        $this->data['m1'] = date('M Y', strtotime($y1.'-'.$m1));
        $this->data['m1bs'] = $this->reports_model->getBestSeller($m1sdate, $m1edate, $warehouse_id);
        $y2 = date('Y', strtotime('-2 months'));
        $m2 = date('m', strtotime('-2 months'));
        $m2sdate = $y2.'-'.$m2.'-01 00:00:00';
        $m2edate = $y2.'-'.$m2.'-'. days_in_month($m2, $y2) . ' 23:59:59';
        $this->data['m2'] = date('M Y', strtotime($y2.'-'.$m2));
        $this->data['m2bs'] = $this->reports_model->getBestSeller($m2sdate, $m2edate, $warehouse_id);
        $y3 = date('Y', strtotime('-3 months'));
        $m3 = date('m', strtotime('-3 months'));
        $m3sdate = $y3.'-'.$m3.'-01 23:59:59';
        $this->data['m3'] = date('M Y', strtotime($y3.'-'.$m3)).' - '.$this->data['m1'];
        $this->data['m3bs'] = $this->reports_model->getBestSeller($m3sdate, $m1edate, $warehouse_id);
        $y4 = date('Y', strtotime('-12 months'));
        $m4 = date('m', strtotime('-12 months'));
        $m4sdate = $y4.'-'.$m4.'-01 23:59:59';
        $this->data['m4'] = date('M Y', strtotime($y4.'-'.$m4)).' - '.$this->data['m1'];
        $this->data['m4bs'] = $this->reports_model->getBestSeller($m4sdate, $m1edate, $warehouse_id);
        // $this->sma->print_arrays($this->data['m1bs'], $this->data['m2bs'], $this->data['m3bs'], $this->data['m4bs']);
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('best_sellers')));
        $meta = array('page_title' => lang('best_sellers'), 'bc' => $bc);
        $this->page_construct('reports/best_sellers', $meta, $this->data);

    }

    function products()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('products_report')));
        $meta = array('page_title' => lang('products_report'), 'bc' => $bc);

        $this->page_construct('reports/products', $meta, $this->data);
    }

    function valued_products()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['price_groups'] = $this->site->getAllPriceGroups();
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        if ($this->input->post('warehouse')) {
            $this->session->set_flashdata('warning', lang('valued_products_report_wh_alert'));
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('valued_products_report')));
        $meta = array('page_title' => lang('valued_products_report'), 'bc' => $bc);
        $this->page_construct('reports/valued_products', $meta, $this->data);
    }

    function getProductsReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $brand = $this->input->get('brand') ? $this->input->get('brand') : NULL;
        $subcategory = $this->input->get('subcategory') ? $this->input->get('subcategory') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $cf1 = $this->input->get('cf1') ? $this->input->get('cf1') : NULL;
        $cf2 = $this->input->get('cf2') ? $this->input->get('cf2') : NULL;
        $cf3 = $this->input->get('cf3') ? $this->input->get('cf3') : NULL;
        $cf4 = $this->input->get('cf4') ? $this->input->get('cf4') : NULL;
        $cf5 = $this->input->get('cf5') ? $this->input->get('cf5') : NULL;
        $cf6 = $this->input->get('cf6') ? $this->input->get('cf6') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        $pp = "( SELECT product_id,
                SUM(CASE WHEN pi.purchase_id IS NOT NULL THEN quantity ELSE 0 END) as purchasedQty,
                    SUM(quantity_balance) as balacneQty, SUM( unit_cost * quantity_balance ) balacneValue,
                    SUM( (CASE WHEN pi.purchase_id IS NOT NULL THEN (pi.subtotal) ELSE 0 END) ) totalPurchase
                        from {$this->db->dbprefix('purchase_items')} pi LEFT JOIN {$this->db->dbprefix('purchases')} p on p.id = pi.purchase_id ";
        $sp = "( SELECT si.product_id, SUM( si.quantity ) soldQty, SUM( si.subtotal ) totalSale from " . $this->db->dbprefix('sales') . " s JOIN " . $this->db->dbprefix('sale_items') . " si on s.id = si.sale_id ";
        if ($start_date || $warehouse) {
            $pp .= " WHERE ";
            $sp .= " WHERE ";
            if ($start_date) {
                $start_date = $this->sma->fld($start_date);
                $end_date = $end_date ? $this->sma->fld($end_date) : date('Y-m-d');
                $pp .= " p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
            }
        }
        $pp .= " GROUP BY pi.product_id ) PCosts";
        $sp .= " GROUP BY si.product_id ) PSales";
        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('products') . ".code, " . $this->db->dbprefix('products') . ".name,
                COALESCE( PCosts.purchasedQty, 0 ) as PurchasedQty,
                COALESCE( PSales.soldQty, 0 ) as SoldQty,
                COALESCE( PCosts.balacneQty, 0 ) as BalacneQty,
                COALESCE( PCosts.totalPurchase, 0 ) as TotalPurchase,
                COALESCE( PCosts.balacneValue, 0 ) as TotalBalance,
                COALESCE( PSales.totalSale, 0 ) as TotalSales,
                (COALESCE( PSales.totalSale, 0 ) - COALESCE( PCosts.totalPurchase, 0 )) as Profit", FALSE)
                ->from('products')
                ->join($sp, 'products.id = PSales.product_id', 'left')
                ->join($pp, 'products.id = PCosts.product_id', 'left')
                ->order_by('products.name');

            if ($product) {
                $this->db->where($this->db->dbprefix('products') . ".id", $product);
            }
            if ($cf1) {
                $this->db->where($this->db->dbprefix('products') . ".cf1", $cf1);
            }
            if ($cf2) {
                $this->db->where($this->db->dbprefix('products') . ".cf2", $cf2);
            }
            if ($cf3) {
                $this->db->where($this->db->dbprefix('products') . ".cf3", $cf3);
            }
            if ($cf4) {
                $this->db->where($this->db->dbprefix('products') . ".cf4", $cf4);
            }
            if ($cf5) {
                $this->db->where($this->db->dbprefix('products') . ".cf5", $cf5);
            }
            if ($cf6) {
                $this->db->where($this->db->dbprefix('products') . ".cf6", $cf6);
            }
            if ($category) {
                $this->db->where($this->db->dbprefix('products') . ".category_id", $category);
            }
            if ($subcategory) {
                $this->db->where($this->db->dbprefix('products') . ".subcategory_id", $subcategory);
            }
            if ($brand) {
                $this->db->where($this->db->dbprefix('products') . ".brand", $brand);
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('products_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('purchased'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('sold'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('purchased_amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('sold_amount'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('profit_loss'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('stock_in_hand'));

                $row = 2;
                $sQty = 0;
                $pQty = 0;
                $sAmt = 0;
                $pAmt = 0;
                $bQty = 0;
                $bAmt = 0;
                $pl = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->code);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->PurchasedQty);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->SoldQty);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->BalacneQty);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->TotalPurchase);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->TotalSales);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->Profit);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->TotalBalance);
                    $pQty += $data_row->PurchasedQty;
                    $sQty += $data_row->SoldQty;
                    $bQty += $data_row->BalacneQty;
                    $pAmt += $data_row->TotalPurchase;
                    $sAmt += $data_row->TotalSales;
                    $bAmt += $data_row->TotalBalance;
                    $pl += $data_row->Profit;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("C" . $row . ":I" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $pQty);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sQty);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $bQty);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $pAmt);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $sAmt);
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $pl);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $bAmt);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'products_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                // ->select($this->db->dbprefix('products') . ".code, " . $this->db->dbprefix('products') . ".name,
                // CONCAT(COALESCE( PCosts.purchasedQty, 0 ), '__', COALESCE( PCosts.totalPurchase, 0 )) as purchased,
                // CONCAT(COALESCE( PSales.soldQty, 0 ), '__', COALESCE( PSales.totalSale, 0 )) as sold,
                // (COALESCE( PSales.totalSale, 0 ) - COALESCE( PCosts.totalPurchase, 0 )) as Profit,
                // CONCAT(COALESCE( PCosts.balacneQty, 0 ), '__', COALESCE( PCosts.balacneValue, 0 )) as balance, {$this->db->dbprefix('products')}.id as id", FALSE)
                ->select($this->db->dbprefix('products') . ".code, " . $this->db->dbprefix('products') . ".name,
                COALESCE( PCosts.purchasedQty, 0 ) as purchased,
                COALESCE( PSales.soldQty, 0 ) as sold,
                COALESCE( PCosts.balacneQty, 0 ) as balance, {$this->db->dbprefix('products')}.id as id", FALSE)
                ->from('products')
                ->join($sp, 'products.id = PSales.product_id', 'left')
                ->join($pp, 'products.id = PCosts.product_id', 'left')
                ->group_by('products.code, PSales.soldQty, PSales.totalSale, PCosts.purchasedQty, PCosts.totalPurchase, PCosts.balacneQty, PCosts.balacneValue');

            if ($product) {
                $this->datatables->where($this->db->dbprefix('products') . ".id", $product);
            }
            if ($cf1) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf1", $cf1);
            }
            if ($cf2) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf2", $cf2);
            }
            if ($cf3) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf3", $cf3);
            }
            if ($cf4) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf4", $cf4);
            }
            if ($cf5) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf5", $cf5);
            }
            if ($cf6) {
                $this->datatables->where($this->db->dbprefix('products') . ".cf6", $cf6);
            }
            if ($category) {
                $this->datatables->where($this->db->dbprefix('products') . ".category_id", $category);
            }
            if ($subcategory) {
                $this->datatables->where($this->db->dbprefix('products') . ".subcategory_id", $subcategory);
            }
            if ($brand) {
                $this->datatables->where($this->db->dbprefix('products') . ".brand", $brand);
            }

            echo $this->datatables->generate();

        }

    }

    function getProductsReport2($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $this->load->library('datatables');

        $show_only_invalid  = $this->input->get('show_only_invalid') ? $this->input->get('show_only_invalid') : NULL;
        $subcategory        = $this->input->get('subcategory') ? $this->input->get('subcategory') : NULL;
        $per_variant        = $this->input->get("per_variant") ? $this->input->get("per_variant") : NULL;
        $warehouse          = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        // $option_id          = $this->input->get('option_idº') ? $this->input->get('option_id') : NULL;
        $category           = $this->input->get('category') ? $this->input->get('category') : NULL;
        $product            = $this->input->get('product') ? $this->input->get('product') : NULL;
        $variant            = $this->input->get('option_id') ? $this->input->get('option_id') : NULL;
        $brand              = $this->input->get('brand') ? $this->input->get('brand') : NULL;

        $cantidad_actual = true;

        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
            $end_date = $this->input->get('end_date');
            $end_date = $this->sma->fld($end_date);
            $cantidad_actual = false;
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        $pp = "";
        $sp = "";
        $ap = "";
        $tp = "";

        $ppis = "";
        $spis = "";
        $apis = "";
        $tpis = "";
        $show_transfers = false;

        $purchase_variant = $sale_variant = $adjustment_variant = $purchase_variant_is = $sale_variant_is = $adjustment_variant_is = $purchase_variant_tranfer = $purchase_variant_tranfer_is = '';


        if ($start_date || $warehouse) {
            $pp .= " WHERE (PR.status = 'received' OR PR.status = 'returned') AND PI.status = 'received' ";
            $sp .= " WHERE (S.sale_status = 'completed' OR S.sale_status = 'returned') ";
            $ap .= " WHERE ";
            $tp .= " WHERE ";

            $ppis .= " WHERE (PRIS.status = 'received' OR PRIS.status = 'returned') AND PIIS.status = 'received' ";
            $spis .= " WHERE (SIS.sale_status = 'completed' OR SIS.sale_status = 'returned') ";
            $apis .= " WHERE ";
            $tpis .= " WHERE ";

            if ($start_date) {
                $start_date = $start_date;
                $end_date = $end_date;
                $pp .= " AND PR.date >= '{$start_date}' AND PR.date < '{$end_date}' ";
                $sp .= " AND S.date >= '{$start_date}' AND S.date < '{$end_date}' ";
                $ap .= " A.date >= '{$start_date}' AND A.date < '{$end_date}' ";
                $tp .= " TF.date >= '{$start_date}' AND TF.date < '{$end_date}' ";

                $ppis .= " AND PRIS.date < '{$start_date}' ";
                $spis .= " AND SIS.date < '{$start_date}' ";
                $apis .= " AIS.date < '{$start_date}' ";
                $tpis .= " TFIS.date < '{$start_date}' ";
                if ($warehouse) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                    $ap .= " AND ";
                    $tp .= " AND ";

                    $ppis .= " AND ";
                    $spis .= " AND ";
                    $apis .= " AND ";
                    $tpis .= " AND ";
                }
            } else {
                $pp .= " AND ";
                $sp .= " AND ";

                $ppis .= " AND ";
                $spis .= " AND ";
            }
            if ($warehouse) {
                $pp .= " PI.warehouse_id = '{$warehouse}' ";
                $sp .= " SI.warehouse_id = '{$warehouse}' ";
                $ap .= " AI.warehouse_id = '{$warehouse}' ";
                $tp .= " (TF.from_warehouse_id = '{$warehouse}' OR TF.to_warehouse_id = '{$warehouse}') ";

                $ppis .= " PIIS.warehouse_id = '{$warehouse}' ";
                $spis .= " SIIS.warehouse_id = '{$warehouse}' ";
                $apis .= " AIIS.warehouse_id = '{$warehouse}' ";
                $tpis .= " (TFIS.from_warehouse_id = '{$warehouse}' OR TFIS.to_warehouse_id = '{$warehouse}') ";
                $show_transfers = true;
                // la siguiente logica se cambia para poder obtener la cantidad desde la suma de las variantes de un producto
                $Pwarehouses = "(   SELECT product_id,
                                    SUM(quantity) AS warehouseQty
                                FROM sma_warehouses_products_variants
                                WHERE warehouse_id = '{$warehouse}'  ";
                if ($variant) {
                    $Pwarehouses .= " AND option_id = $variant ";
                }
                $Pwarehouses .= " GROUP BY product_id ) AS Pwarehouses";

                // se agrega la tabla warehouse products para products que no tengan variants
                $Pwarehouses_products = "( SELECT product_id,
                                SUM(quantity) AS warehouseQty
                            FROM sma_warehouses_products
                            WHERE warehouse_id = '{$warehouse}'  ";
                if ($product) {
                    $Pwarehouses_products .= " AND product_id = $product ";
                }
                $Pwarehouses_products .= " GROUP BY product_id ) AS Pwarehouses_products";
            }
        }

        if ($start_date == NULL) {
            if ($warehouse) {
                $ppis .= " AND ";
                $spis .= " AND ";
                $apis .= " AND ";
                $tpis .= " AND ";
            } else {
                $ppis .= " WHERE ";
                $spis .= " WHERE ";
                $apis .= " WHERE ";
                $tpis .= " WHERE ";
            }

            $ppis .= " 1 = 2 ";
            $spis .= " 1 = 2";
            $apis .= " 1 = 2";
            $tpis .= " 1 = 2 ";
        }

        // if ($variant) {
        //     $purchase_variant = ($pp != '') ? " AND PI.option_id = $variant " : " WHERE PI.option_id = $variant ";
        //     $sale_variant = ($sp != '') ? " AND SI.option_id = $variant " : " WHERE SI.option_id = $variant ";
        //     $adjustment_variant = ($ap != '') ? " AND AI.option_id = $variant " : " WHERE AI.option_id = $variant ";
        //     $purchase_variant_is = ($ppis != '') ? " AND PIIS.option_id = $variant " : " WHERE PIIS.option_id = $variant ";
        //     $sale_variant_is = ($spis != '') ? " AND SIIS.option_id = $variant " : " WHERE SIIS.option_id = $variant ";
        //     $adjustment_variant_is = ($apis != '') ? " AND AIIS.option_id = $variant " : " WHERE AIIS.option_id = $variant ";
        //     $purchase_variant_tranfer = ($tp != '') ? " AND TI.option_id = $variant " : " WHERE TI.option_id = $variant ";
        //     $purchase_variant_tranfer_is = ($tpis != '') ? " AND TIIS.option_id = $variant " : " WHERE TIIS.option_id = $variant ";
        // }



        $purchase_type = ($pp != '' || $purchase_variant != '') ? " AND PR.purchase_type = 1 " : " WHERE PR.purchase_type = 1 ";
        $purchase_type_2 = ($ppis != '' || $purchase_variant_is != '') ? " AND PRIS.purchase_type = 1 " : " WHERE PRIS.purchase_type = 1 ";

        if (!empty($per_variant)) {
            $pPurchasesGroupBy      = ", PI.option_id";
            $pSalesGroupBy          = ", SI.option_id";
            $pAdjustmentsGroupBy    = ", AI.option_id";
            $pPurchasesISGroupBy    = ", PIIS.option_id";
            $pSalesISGroupBy        = ", SIIS.option_id";
            $pAadjustmentsISGroupBy = ", AIIS.option_id";
            $pTransfersGroupBy      = ", TI.option_id";
            $pTransfersISGroupBy    = ", TIIS.option_id";
        } else {
            $pPurchasesGroupBy = $pSalesGroupBy = $pAdjustmentsGroupBy = $pPurchasesISGroupBy =
            $pSalesISGroupBy = $pAadjustmentsISGroupBy = $pTransfersGroupBy = $pTransfersISGroupBy = "";
        }

        $Ppurchases = "(SELECT PI.product_id, PI.option_id,
            SUM(IF((PI.status = 'received'), PI.quantity, 0)) AS purchaseQty ,
            SUM(IF((PI.status = 'pending'), PI.quantity, 0)) AS purchasePendingQty
        FROM sma_purchases PR
            INNER JOIN sma_purchase_items PI ON PR.id = PI.purchase_id {$pp} {$purchase_variant} {$purchase_type}
        GROUP BY PI.product_id $pPurchasesGroupBy) AS Ppurchases";


        $Psales = "(SELECT SI.product_id, SI.option_id,
            SUM(IF((S.sale_status = 'completed' OR S.sale_status = 'returned'), SI.quantity, 0)) AS soldQty ,
            SUM(IF((S.sale_status != 'completed' AND S.sale_status != 'returned'), SI.quantity, 0)) AS soldPendingQty
        FROM sma_sales S
            INNER JOIN sma_sale_items SI ON S.id = SI.sale_id {$sp} {$sale_variant}
        GROUP BY SI.product_id $pSalesGroupBy) AS Psales";

        $Padjustments = "(SELECT AI.product_id, AI.option_id,
            SUM(IF(AI.type = 'addition', AI.quantity, 0)) AS adjustmentQtyPositive,
            SUM(IF(AI.type = 'addition', 0, AI.quantity)) AS adjustmentQtyNegative
        FROM sma_adjustments A
            INNER JOIN sma_adjustment_items AI ON A.id = AI.adjustment_id {$ap} {$adjustment_variant}
        GROUP BY AI.product_id $pAdjustmentsGroupBy) AS Padjustments";

        $PpurchasesIS = "(SELECT PIIS.product_id, PIIS.option_id,
            SUM(IF((PIIS.status = 'received'), PIIS.quantity, 0)) AS purchaseQtyIS ,
            SUM(IF((PIIS.status = 'pending'), PIIS.quantity, 0)) AS purchasePendingQtyIS
        FROM sma_purchases PRIS
            INNER JOIN sma_purchase_items PIIS ON PRIS.id = PIIS.purchase_id {$ppis} {$purchase_variant_is} {$purchase_type_2}
        GROUP BY PIIS.product_id $pPurchasesISGroupBy) AS PpurchasesIS";

        $PsalesIS = "(SELECT SIIS.product_id, SIIS.option_id,
            SUM(IF((SIS.sale_status = 'completed' OR SIS.sale_status = 'returned'), SIIS.quantity, 0)) AS soldQtyIS ,
            SUM(IF((SIS.sale_status != 'completed' AND SIS.sale_status != 'returned'), SIIS.quantity, 0)) AS soldPendingQtyIS
        FROM sma_sales SIS
            INNER JOIN sma_sale_items SIIS ON SIS.id = SIIS.sale_id {$spis} {$sale_variant_is}
        GROUP BY SIIS.product_id $pSalesISGroupBy) AS PsalesIS";

        $PadjustmentsIS = "(SELECT AIIS.product_id, AIIS.option_id,
            SUM(IF(AIIS.type = 'addition', AIIS.quantity, 0)) AS adjustmentQtyPositiveIS,
            SUM(IF(AIIS.type = 'addition', 0, AIIS.quantity)) AS adjustmentQtyNegativeIS
        FROM sma_adjustments AIS
            INNER JOIN sma_adjustment_items AIIS ON AIS.id = AIIS.adjustment_id {$apis} {$adjustment_variant_is}
        GROUP BY AIIS.product_id $pAadjustmentsISGroupBy) AS PadjustmentsIS";

        if ($show_transfers) {
            $Ptransfers = "(SELECT TI.product_id, TI.option_id,
                SUM(IF(TF.to_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyIn,
                SUM(IF(TF.from_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyOut
            FROM sma_transfers TF
                INNER JOIN sma_purchase_items TI ON TF.id = TI.transfer_id {$tp} {$purchase_variant_tranfer}
            GROUP BY TI.product_id $pTransfersGroupBy)
                AS Ptransfers";

            $PtransfersIS = "(SELECT TIIS.product_id, TIIS.option_id,
                SUM(IF(TFIS.to_warehouse_id = '{$warehouse}', TIIS.quantity, 0)) AS transferQtyInIS,
                SUM(IF(TFIS.from_warehouse_id = '{$warehouse}', TIIS.quantity, 0)) AS transferQtyOutIS
            FROM sma_transfers TFIS
                INNER JOIN sma_purchase_items TIIS ON TFIS.id = TIIS.transfer_id {$tpis} {$purchase_variant_tranfer_is}
            GROUP BY TIIS.product_id $pTransfersISGroupBy) AS PtransfersIS";
        } else {
            $Ptransfers = "(SELECT TI.product_id, TI.option_id,
                SUM(0) AS transferQtyIn,
                SUM(0) AS transferQtyOut
            FROM sma_transfers TF
                INNER JOIN sma_purchase_items TI ON TF.id = TI.transfer_id {$tp} {$purchase_variant_tranfer}
            GROUP BY TI.product_id $pTransfersGroupBy) AS Ptransfers";

            $PtransfersIS = "(SELECT TIIS.product_id, TIIS.option_id,
                SUM(0) AS transferQtyInIS,
                SUM(0) AS transferQtyOutIS
            FROM sma_transfers TFIS
                INNER JOIN sma_purchase_items TIIS ON TFIS.id = TIIS.transfer_id {$tpis} {$purchase_variant_tranfer_is}
            GROUP BY TIIS.product_id $pTransfersISGroupBy) AS PtransfersIS";
        }

        $Pvariants = "(SELECT
                        PV.product_id,
                        COUNT(*) as num_variants
                       FROM sma_product_variants PV  GROUP BY PV.product_id)
                       AS Pvariants";

        if (!empty($per_variant)) {
            $pPurchasesCondition    = " AND Ppurchases.option_id = pv.id";
            $pSalesCondition        = " AND Psales.option_id = pv.id";
            $pAdjustmentsCondition  = " AND Psales.option_id = pv.id";
            $pTransfersConditions   = " AND Padjustments.option_id = pv.id";
            $pPurchasesISCondition  = " AND PpurchasesIS.option_id = pv.id";
            $pSalesISCondition      = " AND PsalesIS.option_id = pv.id";
            $pAdjustmentsISCondition= " AND PadjustmentsIS.option_id = pv.id";
            $pTransfersISCondition  = " AND PtransfersIS.option_id = pv.id";
        } else {
            $pPurchasesCondition = $pSalesCondition =  $pAdjustmentsCondition =
            $pTransfersConditions = $pPurchasesISCondition = $pSalesISCondition =
            $pAdjustmentsISCondition = $pTransfersISCondition = "";
        }

        if ($warehouse) {
            if ($cantidad_actual) {
                $qty = 'IF(
                    sma_products.attributes =0,
                        COALESCE(Pwarehouses_products.warehouseQty, 0),
                        COALESCE(Pwarehouses.warehouseQty, 0)
                )';
            } else {
                $qty = '(
                    COALESCE(Ppurchases.purchaseQty, 0) -
                    COALESCE(Psales.soldQty, 0) +
                    COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                    COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                    COALESCE(Ptransfers.transferQtyIn, 0) -
                    COALESCE(Ptransfers.transferQtyOut, 0) +
                    COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                    COALESCE(PsalesIS.soldQtyIS, 0) +
                    COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                    COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                    COALESCE(PtransfersIS.transferQtyInIS, 0) -
                    COALESCE(PtransfersIS.transferQtyOutIS, 0)
                )';
            }

            $this->datatables->select('
                ' . $this->db->dbprefix("products") . '.id,
                ' . $this->db->dbprefix("products") . '.code,
                ' . $this->db->dbprefix("products") . '.name,
                ' . $this->db->dbprefix("products") . '.reference,
                '. (!empty($per_variant) ? "pv.code as vCode, pv.name as vName," : "") .'
                c.name as category_name,
                sc.name as subcategory_name,
                ssc.name as second_level_subcategory_name,
                (
                    COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                    COALESCE(PsalesIS.soldQtyIS, 0) +
                    COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                    COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                    COALESCE(PtransfersIS.transferQtyInIS, 0) -
                    COALESCE(PtransfersIS.transferQtyOutIS, 0)
                ) as initial_stock,
                CONCAT(COALESCE(Ppurchases.purchaseQty, 0), "___", COALESCE(Ppurchases.purchasePendingQty, 0)) as purchaseQty,
                CONCAT(COALESCE(Psales.soldQty, 0), "___", COALESCE(Psales.soldPendingQty, 0)) as soldQty,
                COALESCE(Padjustments.adjustmentQtyPositive, 0) as adjustmentQtyPositive,
                COALESCE(Padjustments.adjustmentQtyNegative, 0) as adjustmentQtyNegative,
                COALESCE(Ptransfers.transferQtyIn, 0) as transferQtyIn,
                COALESCE(Ptransfers.transferQtyOut, 0) as transferQtyOut,
                '.$qty.' as quantity,
                IF(
                    (
                        COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                        COALESCE(PsalesIS.soldQtyIS, 0) +
                        COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                        COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                        COALESCE(PtransfersIS.transferQtyInIS, 0) -
                        COALESCE(PtransfersIS.transferQtyOutIS, 0) +
                        COALESCE(Ppurchases.purchaseQty, 0) -
                        COALESCE(Psales.soldQty, 0) +
                        COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                        COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                        COALESCE(Ptransfers.transferQtyIn, 0) -
                        COALESCE(Ptransfers.transferQtyOut, 0)
                    ) != IF(
                            sma_products.attributes = 0,
                            COALESCE(Pwarehouses_products.warehouseQty, 0),
                            COALESCE(Pwarehouses.warehouseQty, 0)
                            ),
                    0,
                    1
                    ) as correct_quantity,
                '.$this->db->dbprefix("products").'.type,
                Pvariants.num_variants
                ');
            $this->datatables->from('products');
            $this->datatables->join($this->db->dbprefix('categories') . " as c", 'c.id = '.$this->db->dbprefix('products').'.category_id', 'left');
            $this->datatables->join($this->db->dbprefix('categories') . " as sc", 'sc.id = '.$this->db->dbprefix('products').'.subcategory_id', 'left');
            $this->datatables->join($this->db->dbprefix('categories') . " as ssc", 'ssc.id = '.$this->db->dbprefix('products').'.second_level_subcategory_id', 'left');

            if (!empty($per_variant)) {
                $this->datatables->join($this->db->dbprefix('product_variants') . " as pv", 'pv.product_id = '.$this->db->dbprefix('products').'.id');
            }

            $this->datatables->join($Ppurchases, "products.id = Ppurchases.product_id $pPurchasesCondition", 'left');
            $this->datatables->join($Psales, "products.id = Psales.product_id $pSalesCondition", 'left');
            $this->datatables->join($Padjustments, "products.id = Padjustments.product_id $pAdjustmentsCondition", 'left');
            $this->datatables->join($Ptransfers, "products.id = Ptransfers.product_id $pTransfersConditions", 'left');
            $this->datatables->join($PpurchasesIS, "products.id = PpurchasesIS.product_id $pPurchasesISCondition", 'left');
            $this->datatables->join($PsalesIS, "products.id = PsalesIS.product_id $pSalesISCondition", 'left');
            $this->datatables->join($PadjustmentsIS, "products.id = PadjustmentsIS.product_id $pAdjustmentsISCondition", 'left');
            $this->datatables->join($PtransfersIS, "products.id = PtransfersIS.product_id $pTransfersISCondition", 'left');
            $this->datatables->join($Pvariants, 'products.id = Pvariants.product_id', 'left');
            $this->datatables->join($Pwarehouses, 'products.id = Pwarehouses.product_id', 'left');
            $this->datatables->join($Pwarehouses_products, 'products.id = Pwarehouses_products.product_id', 'left');
            $this->datatables->order_by($this->db->dbprefix("products") . '.code', 'asc');
        } else {
            $qty = '(
                COALESCE(Ppurchases.purchaseQty, 0) -
                COALESCE(Psales.soldQty, 0) +
                COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                COALESCE(Ptransfers.transferQtyIn, 0) -
                COALESCE(Ptransfers.transferQtyOut, 0) +
                COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                COALESCE(PsalesIS.soldQtyIS, 0) +
                COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                COALESCE(PtransfersIS.transferQtyInIS, 0) -
                COALESCE(PtransfersIS.transferQtyOutIS, 0)
            )';

            $query = $this->datatables->select('
                ' . $this->db->dbprefix("products") . '.id,
                ' . $this->db->dbprefix("products") . '.code,
                ' . $this->db->dbprefix("products") . '.name,
                ' . $this->db->dbprefix("products") . '.reference,
                '. (!empty($per_variant) ? "pv.code as vCode, pv.name as vName," : "") .'
                c.name as category_name,
                sc.name as subcategory_name,
                ssc.name as second_level_subcategory_name,
                (
                    COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                    COALESCE(PsalesIS.soldQtyIS, 0) +
                    COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                    COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                    COALESCE(PtransfersIS.transferQtyInIS, 0) -
                    COALESCE(PtransfersIS.transferQtyOutIS, 0)
                ) as initial_stock,
                CONCAT(COALESCE(Ppurchases.purchaseQty, 0), "___", COALESCE(Ppurchases.purchasePendingQty, 0)) as purchaseQty,
                CONCAT(COALESCE(Psales.soldQty, 0), "___", COALESCE(Psales.soldPendingQty, 0)) as soldQty,
                COALESCE(Padjustments.adjustmentQtyPositive, 0) as adjustmentQtyPositive,
                COALESCE(Padjustments.adjustmentQtyNegative, 0) as adjustmentQtyNegative,
                COALESCE(Ptransfers.transferQtyIn, 0) as transferQtyIn,
                COALESCE(Ptransfers.transferQtyOut, 0) as transferQtyOut,
                '.$qty.' as quantity,
                IF(
                    (
                        COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                        COALESCE(PsalesIS.soldQtyIS, 0) +
                        COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                        COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                        COALESCE(PtransfersIS.transferQtyInIS, 0) -
                        COALESCE(PtransfersIS.transferQtyOutIS, 0) +
                        COALESCE(Ppurchases.purchaseQty, 0) -
                        COALESCE(Psales.soldQty, 0) +
                        COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                        COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                        COALESCE(Ptransfers.transferQtyIn, 0) -
                        COALESCE(Ptransfers.transferQtyOut, 0)
                    ) != COALESCE(' . $this->db->dbprefix("products") . '.quantity, 0),
                    0,
                    1
                    ) as correct_quantity,
                '.$this->db->dbprefix("products").'.type,
                Pvariants.num_variants');
            $this->datatables->from('products');

            $this->datatables->join($this->db->dbprefix('categories') . " as c", 'c.id = '.$this->db->dbprefix('products').'.category_id', 'left');
            $this->datatables->join($this->db->dbprefix('categories') . " as sc", 'sc.id = '.$this->db->dbprefix('products').'.subcategory_id', 'left');
            $this->datatables->join($this->db->dbprefix('categories') . " as ssc", 'ssc.id = '.$this->db->dbprefix('products').'.second_level_subcategory_id', 'left');

            if (!empty($per_variant)) {
                $this->datatables->join($this->db->dbprefix('product_variants') . " as pv", 'pv.product_id = '.$this->db->dbprefix('products').'.id');
            }

            $this->datatables->join($Ppurchases, "products.id = Ppurchases.product_id $pPurchasesCondition", 'left');
            $this->datatables->join($Psales, "products.id = Psales.product_id $pSalesCondition", 'left');
            $this->datatables->join($Padjustments, "products.id = Padjustments.product_id $pAdjustmentsCondition", 'left');
            $this->datatables->join($Ptransfers, "products.id = Ptransfers.product_id $pTransfersConditions", 'left');
            $this->datatables->join($PpurchasesIS, "products.id = PpurchasesIS.product_id $pPurchasesISCondition", 'left');
            $this->datatables->join($PsalesIS, "products.id = PsalesIS.product_id $pSalesISCondition", 'left');
            $this->datatables->join($PadjustmentsIS, "products.id = PadjustmentsIS.product_id $pAdjustmentsISCondition", 'left');
            $this->datatables->join($PtransfersIS, "products.id = PtransfersIS.product_id $pTransfersISCondition", 'left');
            $this->datatables->join($Pvariants, "products.id = Pvariants.product_id", 'left');
            $this->datatables->order_by($this->db->dbprefix("products") . '.code', 'asc');

        }

        $this->datatables->where($this->db->dbprefix('products') . ".type !=", 'combo');

        if ($product) {
            $this->datatables->where($this->db->dbprefix('products') . ".id", $product);
        }
        if ($variant) {
            // $this->datatables->where($this->db->dbprefix('products').".id = (SELECT product_id FROM {$this->db->dbprefix('product_variants')} WHERE id = $variant)");
            $this->datatables->where("pv.name", $variant);
        }
        if ($category) {
            $parent_id = explode(",", $category);
            $sql_or = "";
            foreach ($parent_id as $key => $pid) {
                if ($sql_or != "") {
                    $sql_or.=" OR ";
                }
                $sql_or .= $this->db->dbprefix('products') . ".category_id = ".$pid;
            }
            $this->datatables->where("(".$sql_or.")");
        }
        if ($subcategory) {
            $this->datatables->where($this->db->dbprefix('products') . ".subcategory_id", $subcategory);
        }
        if (isset($second_level_subcategory_id) && $second_level_subcategory_id) {
            $this->datatables->where($this->db->dbprefix('products') . ".second_level_subcategory_id", $second_level_subcategory_id);
        }
        
        if ($brand) {
            $this->datatables->where($this->db->dbprefix('products') . ".brand", $brand);
        }
        $this->db->where($this->db->dbprefix('products') . ".type !=", 'service');

        if ($show_only_invalid) {
            $this->datatables->having("correct_quantity = 0");
        }

        if ($xls || $pdf) {
            $column = "A";
            $data = $this->datatables->get_display_result_2();

            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('products report');
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('product_code'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('product_name'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('reference'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('category'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('subcategory'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('second_level_subcategory_id'));

            if (!empty($per_variant)) {
                $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('variant_code'));
                $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('variant'));
            }

            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('initial_stock'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('purchased'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('purchased')." ".lang('pending'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('sold'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('sold')." ".lang('pending'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('adjustment_positive'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('adjustment_negative'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('transfer_in'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('transfer_out'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('stock_in_hand'));

            $row = 2;
            $isQty = 0;
            $pQty = 0;
            $pPQty = 0;
            $sQty = 0;
            $sPQty = 0;
            $apQty = 0;
            $anQty = 0;
            $tiQty = 0;
            $toQty = 0;
            $blqty = 0;

            $pl = 0;

            foreach ($data as $data_row) {
                $column = "A";
                $p = explode('___', $data_row->purchaseQty);
                $s = explode('___', $data_row->soldQty);

                $purchase_pending = $p[1];
                $purchase_completed = $p[0];
                $sale_pending = $s[1];
                $sale_completed= $s[0];

                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->code);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->name);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->reference);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->category_name);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->subcategory_name);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->second_level_subcategory_name);
                if (!empty($per_variant)) {
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->vCode);
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->vName);
                }
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->initial_stock);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $purchase_completed);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $purchase_pending);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $sale_completed);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $sale_pending);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->adjustmentQtyPositive);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->adjustmentQtyNegative);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->transferQtyIn);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->transferQtyOut);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $data_row->quantity);
                $isQty += $data_row->initial_stock;
                $pQty += $purchase_completed;
                $pPQty += $purchase_pending;
                $sQty += $sale_completed;
                $sPQty += $sale_pending;
                $apQty += $data_row->adjustmentQtyPositive;
                $anQty += $data_row->adjustmentQtyNegative;
                $tiQty += $data_row->transferQtyIn;
                $toQty += $data_row->transferQtyOut;
                $blqty += $data_row->quantity;

                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("E" . $row . ":K" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $isQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $pQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $pPQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $sQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $sPQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $apQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $anQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $tiQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $toQty);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $blqty);

                for ($col = 'A'; $col <= 'S'; $col++) {
                    $this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
                }

                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'products report';
                $this->load->helper('excel');

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'products',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('products_report'),
                ]);

                create_excel($this->excel, $filename);
        } else {
            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'products',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('products_report'),
            ]);

        }
    }

    function getProductsReport2Pos()
    {
        $this->sma->checkPermissions('products', TRUE);
        $this->load->library('datatables');

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $brand = $this->input->get('brand') ? $this->input->get('brand') : NULL;
        $subcategory = $this->input->get('subcategory') ? $this->input->get('subcategory') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $show_only_invalid = $this->input->get('show_only_invalid') ? $this->input->get('show_only_invalid') : NULL;

        $cantidad_actual = true;

        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
            $end_date = $this->input->get('end_date');
            $end_date = $this->sma->fld($end_date);
            $cantidad_actual = false;
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }


        $pp = "";
        $sp = "";
        $ap = "";
        $tp = "";

        $ppis = "";
        $spis = "";
        $apis = "";
        $tpis = "";
        $show_transfers = false;

        if ($start_date || $warehouse) {
            $pp .= " WHERE (PR.status = 'received' OR PR.status = 'returned') AND PI.status = 'received' ";
            $sp .= " WHERE (S.sale_status = 'completed' OR S.sale_status = 'returned') ";
            $ap .= " WHERE ";
            $tp .= " WHERE ";

            $ppis .= " WHERE (PRIS.status = 'received' OR PRIS.status = 'returned') AND PIIS.status = 'received' ";
            $spis .= " WHERE (SIS.sale_status = 'completed' OR SIS.sale_status = 'returned') ";
            $apis .= " WHERE ";
            $tpis .= " WHERE ";
            if ($start_date) {

                $start_date = $start_date;
                $end_date = $end_date;
                $pp .= " AND PR.date >= '{$start_date}' AND PR.date < '{$end_date}' ";
                $sp .= " AND S.date >= '{$start_date}' AND S.date < '{$end_date}' ";
                $ap .= " A.date >= '{$start_date}' AND A.date < '{$end_date}' ";
                $tp .= " TF.date >= '{$start_date}' AND TF.date < '{$end_date}' ";

                $ppis .= " AND PRIS.date < '{$start_date}' ";
                $spis .= " AND SIS.date < '{$start_date}' ";
                $apis .= " AIS.date < '{$start_date}' ";
                $tpis .= " TFIS.date < '{$start_date}' ";
                if ($warehouse) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                    $ap .= " AND ";
                    $tp .= " AND ";

                    $ppis .= " AND ";
                    $spis .= " AND ";
                    $apis .= " AND ";
                    $tpis .= " AND ";
                }

            } else {
                $pp .= " AND ";
                $sp .= " AND ";

                $ppis .= " AND ";
                $spis .= " AND ";
            }
            if ($warehouse) {
                $pp .= " PI.warehouse_id = '{$warehouse}' ";
                $sp .= " SI.warehouse_id = '{$warehouse}' ";
                $ap .= " AI.warehouse_id = '{$warehouse}' ";
                $tp .= " (TF.from_warehouse_id = '{$warehouse}' OR TF.to_warehouse_id = '{$warehouse}') ";

                $ppis .= " PIIS.warehouse_id = '{$warehouse}' ";
                $spis .= " SIIS.warehouse_id = '{$warehouse}' ";
                $apis .= " AIIS.warehouse_id = '{$warehouse}' ";
                $tpis .= " (TFIS.from_warehouse_id = '{$warehouse}' OR TFIS.to_warehouse_id = '{$warehouse}') ";
                $show_transfers = true;
                $Pwarehouses = "(SELECT product_id, SUM(quantity) AS warehouseQty FROM sma_warehouses_products WHERE warehouse_id = '{$warehouse}' GROUP BY product_id) AS Pwarehouses";
            }
        }

        if ($start_date == NULL) {


            if ($warehouse) {

                $ppis .= " AND ";
                $spis .= " AND ";
                $apis .= " AND ";
                $tpis .= " AND ";

            } else {

                $ppis .= " WHERE ";
                $spis .= " WHERE ";
                $apis .= " WHERE ";
                $tpis .= " WHERE ";
            }

            $ppis .= " 1 = 2 ";
            $spis .= " 1 = 2";
            $apis .= " 1 = 2";
            $tpis .= " 1 = 2 ";

        }

        $Ppurchases = "(SELECT PI.product_id,
                        SUM(IF((PI.status = 'received'), PI.quantity, 0)) AS purchaseQty ,
                        SUM(IF((PI.status = 'pending'), PI.quantity, 0)) AS purchasePendingQty
                        FROM sma_purchases PR INNER JOIN sma_purchase_items PI ON PR.id = PI.purchase_id {$pp} GROUP BY PI.product_id)
                        AS Ppurchases";

        $Psales = "(SELECT SI.product_id,
                    SUM(IF((S.sale_status = 'completed' OR S.sale_status = 'returned'), SI.quantity, 0)) AS soldQty ,
                    SUM(IF((S.sale_status != 'completed' AND S.sale_status != 'returned'), SI.quantity, 0)) AS soldPendingQty
                    FROM sma_sales S INNER JOIN sma_sale_items SI ON S.id = SI.sale_id {$sp} GROUP BY SI.product_id)
                    AS Psales";

        $Padjustments = "(SELECT AI.product_id,
                          SUM(IF(AI.type = 'addition', AI.quantity, 0)) AS adjustmentQtyPositive,
                          SUM(IF(AI.type = 'addition', 0, AI.quantity)) AS adjustmentQtyNegative
                          FROM sma_adjustments A INNER JOIN sma_adjustment_items AI ON A.id = AI.adjustment_id {$ap} GROUP BY AI.product_id)
                          AS Padjustments";

        $PpurchasesIS = "(SELECT PIIS.product_id,
                        SUM(IF((PIIS.status = 'received'), PIIS.quantity, 0)) AS purchaseQtyIS ,
                        SUM(IF((PIIS.status = 'pending'), PIIS.quantity, 0)) AS purchasePendingQtyIS
                        FROM sma_purchases PRIS INNER JOIN sma_purchase_items PIIS ON PRIS.id = PIIS.purchase_id {$ppis} GROUP BY PIIS.product_id)
                        AS PpurchasesIS";

        $PsalesIS = "(SELECT SIIS.product_id,
                    SUM(IF((SIS.sale_status = 'completed' OR SIS.sale_status = 'returned'), SIIS.quantity, 0)) AS soldQtyIS ,
                    SUM(IF((SIS.sale_status != 'completed' AND SIS.sale_status != 'returned'), SIIS.quantity, 0)) AS soldPendingQtyIS
                    FROM sma_sales SIS INNER JOIN sma_sale_items SIIS ON SIS.id = SIIS.sale_id {$spis} GROUP BY SIIS.product_id)
                    AS PsalesIS";

        $PadjustmentsIS = "(SELECT AIIS.product_id,
                          SUM(IF(AIIS.type = 'addition', AIIS.quantity, 0)) AS adjustmentQtyPositiveIS,
                          SUM(IF(AIIS.type = 'addition', 0, AIIS.quantity)) AS adjustmentQtyNegativeIS
                          FROM sma_adjustments AIS INNER JOIN sma_adjustment_items AIIS ON AIS.id = AIIS.adjustment_id {$apis} GROUP BY AIIS.product_id)
                          AS PadjustmentsIS";

        if ($show_transfers) {
            $Ptransfers = "(SELECT TI.product_id,
                            SUM(IF(TF.to_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyIn,
                            SUM(IF(TF.from_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyOut
                            FROM sma_transfers TF INNER JOIN sma_purchase_items TI ON TF.id = TI.transfer_id {$tp} GROUP BY TI.product_id)
                            AS Ptransfers";

            $PtransfersIS = "(SELECT TIIS.product_id,
                            SUM(IF(TFIS.to_warehouse_id = '{$warehouse}', TIIS.quantity, 0)) AS transferQtyInIS,
                            SUM(IF(TFIS.from_warehouse_id = '{$warehouse}', TIIS.quantity, 0)) AS transferQtyOutIS
                            FROM sma_transfers TFIS INNER JOIN sma_purchase_items TIIS ON TFIS.id = TIIS.transfer_id {$tpis} GROUP BY TIIS.product_id)
                            AS PtransfersIS";

        } else {
            $Ptransfers = "(SELECT TI.product_id,
                            SUM(0) AS transferQtyIn,
                            SUM(0) AS transferQtyOut
                            FROM sma_transfers TF INNER JOIN sma_purchase_items TI ON TF.id = TI.transfer_id {$tp} GROUP BY TI.product_id)
                            AS Ptransfers";

            $PtransfersIS = "(SELECT TIIS.product_id,
                            SUM(0) AS transferQtyInIS,
                            SUM(0) AS transferQtyOutIS
                            FROM sma_transfers TFIS INNER JOIN sma_purchase_items TIIS ON TFIS.id = TIIS.transfer_id {$tpis} GROUP BY TIIS.product_id)
                            AS PtransfersIS";
        }

        $Pvariants = "(SELECT
                        PV.product_id,
                        COUNT(*) as num_variants
                       FROM sma_product_variants PV GROUP BY PV.product_id)
                       AS Pvariants";

        if ($warehouse) {

            if ($cantidad_actual) {
                $qty = 'COALESCE(Pwarehouses.warehouseQty, 0)';
            } else {
                $qty = '(
                            COALESCE(Ppurchases.purchaseQty, 0) -
                            COALESCE(Psales.soldQty, 0) +
                            COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                            COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                            COALESCE(Ptransfers.transferQtyIn, 0) -
                            COALESCE(Ptransfers.transferQtyOut, 0) +
                            COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                            COALESCE(PsalesIS.soldQtyIS, 0) +
                            COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                            COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                            COALESCE(PtransfersIS.transferQtyInIS, 0) -
                            COALESCE(PtransfersIS.transferQtyOutIS, 0)
                        )';
            }

            $this->datatables->select('
                       ' . $this->db->dbprefix("products") . '.id,
                       ' . $this->db->dbprefix("products") . '.code,
                       ' . $this->db->dbprefix("products") . '.name,
                       ' . $this->db->dbprefix("categories") . '.name as cat_name,
                        (
                            COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                            COALESCE(PsalesIS.soldQtyIS, 0) +
                            COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                            COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                            COALESCE(PtransfersIS.transferQtyInIS, 0) -
                            COALESCE(PtransfersIS.transferQtyOutIS, 0)
                        ) as initial_stock,
                        CONCAT(COALESCE(Ppurchases.purchaseQty, 0), "___", COALESCE(Ppurchases.purchasePendingQty, 0)) as purchaseQty,
                        CONCAT(COALESCE(Psales.soldQty, 0), "___", COALESCE(Psales.soldPendingQty, 0)) as soldQty,
                        COALESCE(Padjustments.adjustmentQtyPositive, 0) as adjustmentQtyPositive,
                        COALESCE(Padjustments.adjustmentQtyNegative, 0) as adjustmentQtyNegative,
                        COALESCE(Ptransfers.transferQtyIn, 0) as transferQtyIn,
                        COALESCE(Ptransfers.transferQtyOut, 0) as transferQtyOut,
                        '.$qty.' as quantity,
                        IF(
                            (
                                COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                                COALESCE(PsalesIS.soldQtyIS, 0) +
                                COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                                COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                                COALESCE(PtransfersIS.transferQtyInIS, 0) -
                                COALESCE(PtransfersIS.transferQtyOutIS, 0) +
                                COALESCE(Ppurchases.purchaseQty, 0) -
                                COALESCE(Psales.soldQty, 0) +
                                COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                                COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                                COALESCE(Ptransfers.transferQtyIn, 0) -
                                COALESCE(Ptransfers.transferQtyOut, 0)
                            ) != '.$qty.',
                            0,
                            1
                          ) as correct_quantity,
                        '.$this->db->dbprefix("products").'.type,
                        Pvariants.num_variants
                        ')
                ->from('products')
                ->join($Ppurchases, 'products.id = Ppurchases.product_id', 'left')
                ->join($Psales, 'products.id = Psales.product_id', 'left')
                ->join($Padjustments, 'products.id = Padjustments.product_id', 'left')
                ->join($Ptransfers, 'products.id = Ptransfers.product_id', 'left')
                ->join($PpurchasesIS, 'products.id = PpurchasesIS.product_id', 'left')
                ->join($PsalesIS, 'products.id = PsalesIS.product_id', 'left')
                ->join($PadjustmentsIS, 'products.id = PadjustmentsIS.product_id', 'left')
                ->join($PtransfersIS, 'products.id = PtransfersIS.product_id', 'left')
                ->join($Pvariants, 'products.id = Pvariants.product_id', 'left')
                ->join($Pwarehouses, 'products.id = Pwarehouses.product_id', 'left')
                ->join('categories', 'products.category_id = categories.id', 'left')
                ->order_by($this->db->dbprefix("categories") . '.name', 'asc');

        } else {

            if ($cantidad_actual) {
                $qty = 'COALESCE(' . $this->db->dbprefix("products") . '.quantity, 0)';
            } else {
                $qty = '(
                            COALESCE(Ppurchases.purchaseQty, 0) -
                            COALESCE(Psales.soldQty, 0) +
                            COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                            COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                            COALESCE(Ptransfers.transferQtyIn, 0) -
                            COALESCE(Ptransfers.transferQtyOut, 0) +
                            COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                            COALESCE(PsalesIS.soldQtyIS, 0) +
                            COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                            COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                            COALESCE(PtransfersIS.transferQtyInIS, 0) -
                            COALESCE(PtransfersIS.transferQtyOutIS, 0)
                        )';
            }

            $this->datatables->select('
                       ' . $this->db->dbprefix("products") . '.id,
                       ' . $this->db->dbprefix("products") . '.code,
                       ' . $this->db->dbprefix("products") . '.name,
                       ' . $this->db->dbprefix("categories") . '.name as cat_name,
                        (
                            COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                            COALESCE(PsalesIS.soldQtyIS, 0) +
                            COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                            COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                            COALESCE(PtransfersIS.transferQtyInIS, 0) -
                            COALESCE(PtransfersIS.transferQtyOutIS, 0)
                        ) as initial_stock,
                        CONCAT(COALESCE(Ppurchases.purchaseQty, 0), "___", COALESCE(Ppurchases.purchasePendingQty, 0)) as purchaseQty,
                        CONCAT(COALESCE(Psales.soldQty, 0), "___", COALESCE(Psales.soldPendingQty, 0)) as soldQty,
                        COALESCE(Padjustments.adjustmentQtyPositive, 0) as adjustmentQtyPositive,
                        COALESCE(Padjustments.adjustmentQtyNegative, 0) as adjustmentQtyNegative,
                        COALESCE(Ptransfers.transferQtyIn, 0) as transferQtyIn,
                        COALESCE(Ptransfers.transferQtyOut, 0) as transferQtyOut,
                        '.$qty.' as quantity,
                        IF(
                            (
                                COALESCE(PpurchasesIS.purchaseQtyIS, 0) -
                                COALESCE(PsalesIS.soldQtyIS, 0) +
                                COALESCE(PadjustmentsIS.adjustmentQtyPositiveIS, 0) -
                                COALESCE(PadjustmentsIS.adjustmentQtyNegativeIS, 0) +
                                COALESCE(PtransfersIS.transferQtyInIS, 0) -
                                COALESCE(PtransfersIS.transferQtyOutIS, 0) +
                                COALESCE(Ppurchases.purchaseQty, 0) -
                                COALESCE(Psales.soldQty, 0) +
                                COALESCE(Padjustments.adjustmentQtyPositive, 0) -
                                COALESCE(Padjustments.adjustmentQtyNegative, 0) +
                                COALESCE(Ptransfers.transferQtyIn, 0) -
                                COALESCE(Ptransfers.transferQtyOut, 0)
                            ) != '.$qty.',
                            0,
                            1
                          ) as correct_quantity,
                        '.$this->db->dbprefix("products").'.type,
                        Pvariants.num_variants')
                ->from('products')
                ->join($Ppurchases, 'products.id = Ppurchases.product_id', 'left')
                ->join($Psales, 'products.id = Psales.product_id', 'left')
                ->join($Padjustments, 'products.id = Padjustments.product_id', 'left')
                ->join($Ptransfers, 'products.id = Ptransfers.product_id', 'left')
                ->join($PpurchasesIS, 'products.id = PpurchasesIS.product_id', 'left')
                ->join($PsalesIS, 'products.id = PsalesIS.product_id', 'left')
                ->join($PadjustmentsIS, 'products.id = PadjustmentsIS.product_id', 'left')
                ->join($Pvariants, 'products.id = Pvariants.product_id', 'left')
                ->join($PtransfersIS, 'products.id = PtransfersIS.product_id', 'left')
                ->join('categories', 'products.category_id = categories.id', 'left')
                ->order_by($this->db->dbprefix("categories") . '.name', 'asc');

        }

        $this->datatables->where($this->db->dbprefix('products') . ".type !=", 'combo');

        if ($product) {
            $this->datatables->where($this->db->dbprefix('products') . ".id", $product);
        }
        if ($category) {
            $parent_id = explode(",", $category);
            $sql_or = "";
            foreach ($parent_id as $key => $pid) {
                if ($sql_or != "") {
                    $sql_or.=" OR ";
                }
                $sql_or .= $this->db->dbprefix('products') . ".category_id = ".$pid;
            }
            $this->datatables->where("(".$sql_or.")");
        }
        if ($subcategory) {
            $this->datatables->where($this->db->dbprefix('products') . ".subcategory_id", $subcategory);
        }
        if ($brand) {
            $this->datatables->where($this->db->dbprefix('products') . ".brand", $brand);
        }
        $this->db->where($this->db->dbprefix('products') . ".type !=", 'service');
        if ($show_only_invalid) {
            $this->datatables->having("correct_quantity = 0");
        }
        $pdata = $this->datatables->get_display_result_2();

        $data['pdata'] = $pdata;
        $this->load_view($this->theme . 'reports/products_report_pos', $data);
    }

    function getValuedProductsReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $this->load->library('datatables');
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $brand = $this->input->get('brand') ? $this->input->get('brand') : NULL;
        $subcategory = $this->input->get('subcategory') ? $this->input->get('subcategory') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $price_group = $this->input->get('price_group') ? $this->input->get('price_group') : NULL;
        $report_date = $this->input->get('report_date') ? $this->input->get('report_date') : NULL;
        $include_products_discontinued = $this->input->get('include_products_discontinued') ? $this->input->get('include_products_discontinued') : NULL;
        $variants = $this->input->get('variants') ? $this->input->get('variants') : NULL;
        if ($variants) {
            $hasvariants = $this->reports_model->validate_variants();
            if (!$hasvariants) {
                return;
            }
        }
        $actual_avg_cost = false;
        if ($this->input->get('end_date')) {
            $end_date = $this->input->get('end_date')." 23:59";
            $end_date = $this->sma->fld($end_date);
            if (strpos($end_date, date('Y-m-d')) !== false) {
                $actual_avg_cost = true;
            }
        } else {
            $actual_avg_cost = true;
            $end_date =date('Y-m-d 23:59');
        }
        /*
            actual_avg_cost => si la fecha de corte es el día de hoy los costos se obtendran de la tabla sma_products
                            => si la fecha de corte es otro día diferente día calendario actual los costos se calcularan de los movimientos
        */
        $pp = "";
        $sp = "";
        $ap = "";
        $apsub = "";
        $apadd = "";
        $tp = "";

        $show_transfers = false; // show transfer pasa a true solo si se envia el formulario por bodega
        if ($end_date || $warehouse) {
            $pp .= " WHERE PI.status != 'service_received' AND ";
            $sp .= " WHERE ";
            $apadd .= " WHERE AI.type ='addition' AND "; // AI adjustment_items
            $apsub .= " WHERE AI.type = 'subtraction' AND "; // AI adjustment_items
            $tp .= " WHERE ";
            if ($end_date) {
                // end_date => fecha a tener en cuenta (encabezados)
                // PR->purchases, S->sales, A->adjustments, TF->transfers
                $pp .= ($report_date == 2 ? 'PR.registration_date' : 'PR.date')." <= '{$end_date}' ";
                $sp .= ($report_date == 2 ? 'S.registration_date' : 'S.date')." <= '{$end_date}' ";
                $apadd .= ($report_date == 2 ? 'A.registration_date' : 'A.date')." <= '{$end_date}' ";
                $apsub .= ($report_date == 2 ? 'A.registration_date' : 'A.date')." <= '{$end_date}' ";
                $tp .= ('TF.date')." <= '{$end_date}' ";
                if ($warehouse) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                    $ap .= " AND ";
                    $tp .= " AND ";
                    $apadd .= " AND ";
                    $apsub .= " AND ";
                }
            }
            if ($warehouse) {
                $show_transfers = true;
                $pp .= " PI.warehouse_id = '{$warehouse}' ";
                $sp .= " SI.warehouse_id = '{$warehouse}' ";
                $apadd .= " AI.warehouse_id = '{$warehouse}' ";
                $apsub .= " AI.warehouse_id = '{$warehouse}' ";
                $tp .= " (TF.from_warehouse_id = '{$warehouse}' OR TF.to_warehouse_id = '{$warehouse}') ";
                if ($variants) { // en caso de enviar filtrado por variantes se debe obtener los datos de warehouses_products_variants
                    $Pwarehouses = "(SELECT option_id, SUM(quantity) AS warehouseQty
                                    FROM ".$this->db->dbprefix."warehouses_products_variants
                                    WHERE warehouse_id = '{$warehouse}' GROUP BY option_id) AS Pwarehouses";
                }else{
                    $Pwarehouses = "(SELECT product_id, SUM(quantity) AS warehouseQty
                                    FROM ".$this->db->dbprefix."warehouses_products
                                    WHERE warehouse_id = '{$warehouse}' GROUP BY product_id) AS Pwarehouses";
                }
            }
        }
        if (!$actual_avg_cost) { // en caso que la fecha de corte sea diferente al día actual, entramos en este condicional
            if ($variants) { // agrupar las compras por variantes
                $Ppurchases = "(SELECT  PI.option_id AS product_id,
                                        MAX(PI.id) as max_pid,
                                        SUM(IF((PI.status = 'received' OR PI.status = 'returned'), PI.quantity, 0)) AS purchaseQty ,
                                        SUM(IF((PI.status = 'pending'), PI.quantity, 0)) AS purchasePendingQty
                                FROM ".$this->db->dbprefix."purchases PR
                                INNER JOIN ".$this->db->dbprefix."purchase_items PI ON PR.id = PI.purchase_id
                                {$pp}
                                GROUP BY PI.option_id) AS sma_Ppurchases ";
            }else { // agrupar las compras por productos
                $Ppurchases = "(SELECT  PI.product_id,
                                        MAX(PI.id) as max_pid,
                                        SUM(IF((PI.status = 'received' OR PI.status = 'returned'), PI.quantity, 0)) AS purchaseQty ,
                                        SUM(IF((PI.status = 'pending'), PI.quantity, 0)) AS purchasePendingQty
                                FROM ".$this->db->dbprefix."purchases PR
                                INNER JOIN ".$this->db->dbprefix."purchase_items PI ON PR.id = PI.purchase_id
                                {$pp}
                                GROUP BY PI.product_id) AS sma_Ppurchases ";
            }

            // ventas
            if ($variants) { // agrupar las ventas por variantes
                $Psales = "(SELECT  SI.option_id AS product_id,
                                    MAX(SI.id) as max_sale_item_id,
                                    avg_net_unit_cost,
                                    SUM(IF((S.sale_status = 'completed' OR S.sale_status = 'returned'), SI.quantity, 0)) AS soldQty ,
                                    SUM(IF((S.sale_status != 'completed'), SI.quantity, 0)) AS soldPendingQty
                            FROM ".$this->db->dbprefix."sales S
                            INNER JOIN ".$this->db->dbprefix."sale_items SI ON S.id = SI.sale_id
                            {$sp}
                            GROUP BY SI.option_id ) AS sma_Psales ";
            }else{  // agrupar las ventas por productos
                $Psales = "(SELECT  SI.product_id,
                                    MAX(SI.id) as max_sale_item_id,
                                    avg_net_unit_cost,
                                    SUM(IF((S.sale_status = 'completed' OR S.sale_status = 'returned'), SI.quantity, 0)) AS soldQty ,
                                    SUM(IF((S.sale_status != 'completed'), SI.quantity, 0)) AS soldPendingQty
                            FROM ".$this->db->dbprefix."sales S
                            INNER JOIN ".$this->db->dbprefix."sale_items SI ON S.id = SI.sale_id
                            {$sp}
                            GROUP BY SI.product_id ) AS sma_Psales ";
            }

            // ajustes de cantidades adiciones
            if ($variants) { // agrupar los ajustes por variantes
                $PadjustmentsADD = "(SELECT AI.option_id AS product_id,
                                            MAX(AI.id) as max_adjustment_item_id,
                                            SUM(AI.quantity) AS adjustmentQtyPositive
                                    FROM ".$this->db->dbprefix."adjustments A
                                    INNER JOIN ".$this->db->dbprefix."adjustment_items AI ON A.id = AI.adjustment_id
                                    {$apadd}
                                    GROUP BY AI.option_id ) AS sma_PadjustmentsADD ";
            }else{ // agrupar los ajustes por productos
                $PadjustmentsADD = "(SELECT AI.product_id,
                                            MAX(AI.id) as max_adjustment_item_id,
                                            SUM(AI.quantity) AS adjustmentQtyPositive
                                    FROM ".$this->db->dbprefix."adjustments A
                                    INNER JOIN ".$this->db->dbprefix."adjustment_items AI ON A.id = AI.adjustment_id
                                    {$apadd}
                                    GROUP BY AI.product_id ) AS sma_PadjustmentsADD ";
            }

            // ajustes de cantidades deducciones
            if ($variants) { // ajuste de cantidades por variantes
                $PadjustmentsSUB = "(SELECT AI.option_id AS product_id,
                                            SUM(AI.quantity) AS adjustmentQtyNegative
                                    FROM ".$this->db->dbprefix."adjustments A
                                    INNER JOIN ".$this->db->dbprefix."adjustment_items AI ON A.id = AI.adjustment_id
                                    {$apsub}
                                    GROUP BY AI.option_id) AS sma_PadjustmentsSUB ";
            }else{ // ajuste de cantidades por productos
                $PadjustmentsSUB = "(SELECT AI.product_id,
                                        SUM(AI.quantity) AS adjustmentQtyNegative
                                    FROM ".$this->db->dbprefix."adjustments A
                                    INNER JOIN ".$this->db->dbprefix."adjustment_items AI ON A.id = AI.adjustment_id
                                    {$apsub}
                                    GROUP BY AI.product_id) AS sma_PadjustmentsSUB ";
            }

            // traslados
            if ($show_transfers) {
                if ($variants) { // traslados por variantes
                    $Ptransfers = "(SELECT TI.option_id AS product_id,
                                            SUM(IF(TF.to_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyIn,
                                            SUM(IF(TF.from_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyOut
                                    FROM ".$this->db->dbprefix."transfers TF
                                    INNER JOIN ".$this->db->dbprefix."purchase_items TI ON TF.id = TI.transfer_id
                                    {$tp}
                                    GROUP BY TI.option_id) AS sma_Ptransfers ";
                }else{ // traslados por productos
                    $Ptransfers = "(SELECT  TI.product_id,
                                            SUM(IF(TF.to_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyIn,
                                            SUM(IF(TF.from_warehouse_id = '{$warehouse}', TI.quantity, 0)) AS transferQtyOut
                                    FROM ".$this->db->dbprefix."transfers TF
                                    INNER JOIN ".$this->db->dbprefix."purchase_items TI ON TF.id = TI.transfer_id
                                    {$tp}
                                    GROUP BY TI.product_id) AS sma_Ptransfers ";
                }

            } else {
                if ($variants) {
                    $Ptransfers = " (SELECT TI.option_id AS product_id,
                                            SUM(0) AS transferQtyIn,
                                            SUM(0) AS transferQtyOut
                                    FROM ".$this->db->dbprefix."transfers TF
                                    INNER JOIN ".$this->db->dbprefix."purchase_items TI ON TF.id = TI.transfer_id
                                    {$tp}
                                    GROUP BY TI.option_id) AS sma_Ptransfers ";
                }else{
                    $Ptransfers = "(SELECT  TI.product_id,
                                            SUM(0) AS transferQtyIn,
                                            SUM(0) AS transferQtyOut
                                    FROM ".$this->db->dbprefix."transfers TF
                                    INNER JOIN ".$this->db->dbprefix."purchase_items TI ON TF.id = TI.transfer_id
                                    {$tp}
                                    GROUP BY TI.product_id) AS sma_Ptransfers ";
                }
            }
        }

        if ($price_group) {
            $PriceGroupPrice = "(SELECT PPR.product_id,
                                     PPR.price
                              FROM ".$this->db->dbprefix."product_prices PPR
                              INNER JOIN ".$this->db->dbprefix."price_groups PG ON PG.id = PPR.price_group_id
                                AND PG.id = ".$price_group.")
                              AS PriceGroupPrice";
        } else {
            $PriceGroupPrice = "(SELECT PPR.product_id,
                                     PPR.price
                              FROM ".$this->db->dbprefix."product_prices PPR
                              INNER JOIN ".$this->db->dbprefix."price_groups PG ON PG.id = PPR.price_group_id
                                AND PG.price_group_base = 1)
                              AS PriceGroupPrice";
        }

        if (!$actual_avg_cost) {
            $qty = '(
                        COALESCE(sma_Ppurchases.purchaseQty, 0) -
                        COALESCE(sma_Psales.soldQty, 0) +
                        COALESCE(sma_PadjustmentsADD.adjustmentQtyPositive, 0) -
                        COALESCE(sma_PadjustmentsSUB.adjustmentQtyNegative, 0) +
                        COALESCE(sma_Ptransfers.transferQtyIn, 0) -
                        COALESCE(sma_Ptransfers.transferQtyOut, 0)
                    )';
            $last_cost = 'IF(
                            IF(pic.unit_cost > 0, pic.unit_cost, aic.adjustment_cost) > 0,
                            IF(pic.unit_cost > 0, pic.unit_cost, aic.adjustment_cost), ';
            if ($this->Settings->tax_method == 1) { // metodo antes de iva
                $last_cost .= '(
                                    ' .$this->db->dbprefix("products").'.cost +
                                    (' .$this->db->dbprefix("products"). '.cost *
                                        (
                                            (SELECT rate FROM sma_tax_rates WHERE id =' .$this->db->dbprefix('products'). '.tax_rate)/100
                                        )
                                    )
                                )
                            )';
            }else if ($this->Settings->tax_method == 0){ // metodo despues de iva
                $last_cost .= $this->db->dbprefix("products").'.cost)';
            }

        } else {
            if ($variants) {
                if ($warehouse) {
                    $qty = ' COALESCE(Pwarehouses.warehouseQty, 0)';
                } else {
                    $qty = 'IF('.$this->db->dbprefix('product_variants').'.`status` IS NULL,
                                COALESCE('.$this->db->dbprefix('products').'.quantity, 0),
                                COALESCE('.$this->db->dbprefix('product_variants').'.quantity, 0))';
                }
            }else {
                if ($warehouse) {
                    $qty = ' COALESCE(Pwarehouses.warehouseQty, 0)';
                } else {
                    $qty = 'COALESCE('.$this->db->dbprefix("products").'.quantity, 0)';
                }
            }
            if ($this->Settings->tax_method == 1) { // metodo antes de iva
                $last_cost = '(' .$this->db->dbprefix("products").'.cost +
                                    (' .$this->db->dbprefix("products"). '.cost *
                                        (
                                            (SELECT rate FROM sma_tax_rates WHERE id =' .$this->db->dbprefix('products'). '.tax_rate)/100
                                        )
                                    )
                                )';
            }else if ($this->Settings->tax_method == 0){ // metodo despues de iva
                $last_cost = $this->db->dbprefix("products").'.cost';
            }
        }
        if (!$actual_avg_cost) {

            $condicional = "
                            IF(
                                sale_date > purchase_date,
                                IF(
                                    sale_cost > 0,
                                    IF(
                                        purchase_date > adjustment_date,
                                        sale_cost,
                                        IF(
                                            adjustment_date > sale_date,
                                            IF(
                                                adjustment_cost > 0,
                                                adjustment_cost,
                                                product_cost
                                            ),
                                            sale_cost
                                        )
                                    ),
                                    IF(
                                        purchase_date > adjustment_date,
                                        IF(
                                            purchase_cost > 0,
                                            purchase_cost,
                                            IF(
                                                adjustment_cost > 0,
                                                adjustment_cost,
                                                product_cost
                                            )
                                        ),
                                        IF(
                                            adjustment_cost > 0,
                                            adjustment_cost,
                                            product_cost
                                        )
                                    )
                                ),
                                IF(
                                    sale_date > adjustment_date,
                                    IF(
                                        purchase_cost > 0,
                                        purchase_cost,
                                        IF(
                                            sale_cost > 0,
                                            sale_cost,
                                            IF(
                                                adjustment_cost > 0,
                                                adjustment_cost,
                                                product_cost
                                            )
                                        )
                                    ),
                                    IF(
                                        purchase_date > adjustment_date,
                                        IF(
                                            purchase_cost > 0,
                                            purchase_cost,
                                            IF(
                                                adjustment_cost > 0,
                                                adjustment_cost,
                                                product_cost
                                            )
                                        ),
                                        IF(
                                            adjustment_cost > 0,
                                            adjustment_cost,
                                            product_cost
                                        )
                                    )
                                )
                            )

                            ";

            $condicional1 = str_replace('sale_cost', 'sic.avg_net_unit_cost * (('.$this->db->dbprefix("tax_rates").'.rate / 100) + 1)', $condicional);
            $condicional1 = str_replace('purchase_cost', 'pic.calculated_avg_cost', $condicional1);
            $condicional1 = str_replace('adjustment_cost', 'aic.calculated_avg_cost', $condicional1);
            $condicional1 = str_replace('product_cost', $this->db->dbprefix("products").'.avg_cost', $condicional1);
            $condicional1 = str_replace('sale_date', $report_date == 2 ? 'sc.registration_date' : 'sc.date', $condicional1);
            $condicional1 = str_replace('purchase_date', $report_date == 2 ? 'pc.registration_date' : 'pc.date', $condicional1);
            $condicional1 = str_replace('adjustment_date', $report_date == 2 ? 'ac.registration_date' : 'ac.date', $condicional1);

            $avg_cost_select1 = $condicional1.' as avg_cost,';
            $avg_cost_select2 = '('.$condicional1.' * '.$qty.') as avg_cost_qty,';
            $avg_cost_select3 = '(('.$condicional1.' / (('.$this->db->dbprefix("tax_rates").'.rate / 100) + 1)) * '.$qty.') as avg_net_cost_qty,';
        } else {
            $avg_cost_select1 = 'COALESCE(' . $this->db->dbprefix("products") . '.avg_cost, 0) AS avg_cost,';
            $avg_cost_select2 = '(COALESCE(' . $this->db->dbprefix("products") . '.avg_cost, 0) * '.$qty.') AS avg_cost_qty,';
            $avg_cost_select3 = '((COALESCE(' . $this->db->dbprefix("products") . '.avg_cost, 0) / (('.$this->db->dbprefix("tax_rates").'.rate / 100) + 1)) * '.$qty.') AS avg_net_cost_qty,';
        }

        if ($warehouse) {
            $this->datatables->select($this->db->dbprefix("products").'.code');
            $this->datatables->select($this->db->dbprefix("products").'.name');
            if ($variants) {
                $this->datatables->select($this->db->dbprefix("product_variants").'.code AS codeV ');
                $this->datatables->select($this->db->dbprefix("product_variants").'.name AS nameV');
                $this->datatables->select($this->db->dbprefix("product_variants").'.status');
            }else{
                $this->datatables->select($this->db->dbprefix("products").'.discontinued');
            }
            $this->datatables->select($this->db->dbprefix("tax_rates").'.name as tax_rate_name');
            $this->datatables->select($qty. 'AS quantity');
            $this->datatables->select($last_cost. ' AS last_cost');
            $this->datatables->select($avg_cost_select1);
            $this->datatables->select('PriceGroupPrice.price');
            $this->datatables->select($avg_cost_select2);
            $this->datatables->select($avg_cost_select3);
            $this->datatables->select($this->db->dbprefix("categories").'.name AS category');
            $this->datatables->select($this->db->dbprefix("products").'.type');
            $this->datatables->select($this->db->dbprefix("products").'.reference');
            $this->datatables->select($this->db->dbprefix("products").'.subcategory_id');
            $this->datatables->select($this->db->dbprefix("products").'.second_level_subcategory_id');
            $this->datatables->select($this->db->dbprefix("products").'.brand');
            $this->datatables->select($this->db->dbprefix("products").'.color_id');
            $this->datatables->select($this->db->dbprefix("products").'.material_id');
            $this->datatables->select($this->db->dbprefix("products").'.unit');
            $this->datatables->select($this->db->dbprefix("products").'.discontinued');
            $this->datatables->select($this->db->dbprefix("products").'.tax_rate');
            $this->datatables->select($this->db->dbprefix("products").'.tax_method');
            $this->datatables->select($this->db->dbprefix("products").'.id');
            $this->datatables->select($this->db->dbprefix("products").'.cost');
            $this->datatables->select($this->db->dbprefix("product_variants").'.id AS opption_id');
            $this->datatables->from('products');
            $this->datatables->join('product_variants', 'products.id = product_variants.product_id', 'left');
            if (!$actual_avg_cost) {
                if ($variants) {
                    $this->datatables->join($Ppurchases, 'product_variants.id = Ppurchases.product_id', 'left');
                }else{
                    $this->datatables->join($Ppurchases, 'products.id = Ppurchases.product_id', 'left');
                }
                $this->datatables->join('purchase_items pic', 'pic.id = Ppurchases.max_pid', 'left');
                $this->datatables->join('purchases pc', 'pc.id = pic.purchase_id', 'left');
                if ($variants) {
                    $this->datatables->join($Psales, 'product_variants.id = Psales.product_id', 'left');
                }else{
                    $this->datatables->join($Psales, 'products.id = Psales.product_id', 'left');
                }
                $this->datatables->join('sale_items sic', 'sic.id = Psales.max_sale_item_id', 'left');
                $this->datatables->join('sales sc', 'sc.id = sic.sale_id', 'left');
                if ($variants) {
                    $this->datatables->join($PadjustmentsADD, 'product_variants.id = PadjustmentsADD.product_id', 'left');
                }else{
                    $this->datatables->join($PadjustmentsADD, 'products.id = PadjustmentsADD.product_id', 'left');
                }
                if ($variants) {
                    $this->datatables->join($PadjustmentsSUB, 'product_variants.id = PadjustmentsSUB.product_id', 'left');
                }else{
                    $this->datatables->join($PadjustmentsSUB, 'products.id = PadjustmentsSUB.product_id', 'left');
                }
                $this->datatables->join('adjustment_items aic', 'aic.id = PadjustmentsADD.max_adjustment_item_id', 'left');
                $this->datatables->join('adjustments ac', 'ac.id = aic.adjustment_id', 'left');
                if ($variants) {
                    $this->datatables->join($Ptransfers, 'product_variants.id = Ptransfers.product_id', 'left');
                }else{
                    $this->datatables->join($Ptransfers, 'products.id = Ptransfers.product_id', 'left');
                }
            }
            $this->datatables->join($PriceGroupPrice, 'products.id = PriceGroupPrice.product_id', 'left');

            if ($variants) {
                $this->datatables->join($Pwarehouses, 'product_variants.id = Pwarehouses.option_id', 'left');
            }else{
                $this->datatables->join($Pwarehouses, 'products.id = Pwarehouses.product_id', 'left');
            }
            $this->datatables->join('tax_rates', 'tax_rates.id = products.tax_rate');
            $this->datatables->join('categories', 'categories.id = products.category_id', 'left');
        }
        else {  /**** cuando el informe va sin bodega ****/
            $this->datatables->select($this->db->dbprefix("products").'.code');
            $this->datatables->select($this->db->dbprefix("products").'.name');
            if ($variants) {
                $this->datatables->select($this->db->dbprefix("product_variants").'.code AS codeV', '');
                $this->datatables->select($this->db->dbprefix("product_variants").'.name AS nameV', '');
                $this->datatables->select($this->db->dbprefix("product_variants").'.status', '');
            }else{
                $this->datatables->select($this->db->dbprefix("products").'.discontinued');
            }
            $this->datatables->select($this->db->dbprefix("tax_rates").'.name as tax_rate_name');
            $this->datatables->select($qty. 'AS quantity');
            $this->datatables->select($last_cost. ' AS last_cost');
            $this->datatables->select($avg_cost_select1);
            $this->datatables->select('PriceGroupPrice.price');
            $this->datatables->select($avg_cost_select2);
            $this->datatables->select($avg_cost_select3);
            $this->datatables->select($this->db->dbprefix("categories").'.name AS category');
            $this->datatables->select($this->db->dbprefix("products").'.type');
            $this->datatables->select($this->db->dbprefix("products").'.reference');
            $this->datatables->select($this->db->dbprefix("products").'.subcategory_id');
            $this->datatables->select($this->db->dbprefix("products").'.second_level_subcategory_id');
            $this->datatables->select($this->db->dbprefix("products").'.brand');
            $this->datatables->select($this->db->dbprefix("products").'.color_id');
            $this->datatables->select($this->db->dbprefix("products").'.material_id');
            $this->datatables->select($this->db->dbprefix("products").'.unit');
            $this->datatables->select($this->db->dbprefix("products").'.discontinued');
            $this->datatables->select($this->db->dbprefix("products").'.tax_rate');
            $this->datatables->select($this->db->dbprefix("products").'.tax_method');
            $this->datatables->select($this->db->dbprefix("products").'.id');
            $this->datatables->select($this->db->dbprefix("products").'.cost');
            $this->datatables->select($this->db->dbprefix("product_variants").'.id AS opption_id');
            $this->datatables->from('products');
            $this->datatables->join('product_variants', 'products.id = product_variants.product_id', 'left');
            if (!$actual_avg_cost) {
                if ($variants) {
                    $this->datatables->join($Ppurchases, 'product_variants.id = Ppurchases.product_id', 'left');
                }else{
                    $this->datatables->join($Ppurchases, 'products.id = Ppurchases.product_id', 'left');
                }
                $this->datatables->join('purchase_items pic', 'pic.id = Ppurchases.max_pid', 'left');
                $this->datatables->join('purchases pc', 'pc.id = pic.purchase_id', 'left');
                if ($variants) {
                    $this->datatables->join($Psales, 'product_variants.id = Psales.product_id', 'left');
                }else{
                    $this->datatables->join($Psales, 'products.id = Psales.product_id', 'left');
                }
                $this->datatables->join('sale_items sic', 'sic.id = Psales.max_sale_item_id', 'left');
                $this->datatables->join('sales sc', 'sc.id = sic.sale_id', 'left');
                if ($variants) {
                    $this->datatables->join($PadjustmentsADD, 'product_variants.id = PadjustmentsADD.product_id', 'left');
                }else{
                    $this->datatables->join($PadjustmentsADD, 'products.id = PadjustmentsADD.product_id', 'left');
                }
                if ($variants) {
                    $this->datatables->join($PadjustmentsSUB, 'product_variants.id = PadjustmentsSUB.product_id', 'left');
                }else{
                    $this->datatables->join($PadjustmentsSUB, 'products.id = PadjustmentsSUB.product_id', 'left');
                }
                $this->datatables->join('adjustment_items aic', 'aic.id = PadjustmentsADD.max_adjustment_item_id', 'left');
                $this->datatables->join('adjustments ac', 'ac.id = aic.adjustment_id', 'left');
                if ($variants) {
                    $this->datatables->join($Ptransfers, 'product_variants.id = Ptransfers.product_id', 'left');
                }else{
                    $this->datatables->join($Ptransfers, 'products.id = Ptransfers.product_id', 'left');
                }
            }
            $this->datatables->join($PriceGroupPrice, 'products.id = PriceGroupPrice.product_id', 'left');
            $this->datatables->join('categories', 'categories.id = products.category_id', 'left');
            $this->datatables->join('tax_rates', 'tax_rates.id = products.tax_rate');
            $this->datatables->order_by('quantity desc');
        }

        if ($product) {
            $this->datatables->where($this->db->dbprefix('products') . ".id", $product);
        }
        if ($category) {
            $this->datatables->where($this->db->dbprefix('products') . ".category_id", $category);
        }
        if ($subcategory) {
            $this->datatables->where($this->db->dbprefix('products') . ".subcategory_id", $subcategory);
        }
        if ($brand) {
            $this->datatables->where($this->db->dbprefix('products') . ".brand", $brand);
        }
        if (!$include_products_discontinued) {
            $this->datatables->where($this->db->dbprefix('products').'.discontinued', 0);
        }
        $this->datatables->group_by($this->db->dbprefix('products') . ".id");
        if ($variants) {
            $this->datatables->group_by($this->db->dbprefix('product_variants') . ".id");
        }
        if ($xls || $pdf) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $column = "A";
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('products report');
            $this->excel->getActiveSheet()->SetCellValue('A1', 'Fecha de corte : '.$end_date);
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('product_code'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('product_name'));
            if ($variants) {
                $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('code')." ".lang('variant'));
                $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('name')." ".lang('variant'));
            }
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('Reference'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('type'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('category'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('subcategory_first_level'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('subcategory_second_level'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('brand'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('color'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('material'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('base_unit_of_measure'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('inactive'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('tax_rate'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('unit_cost_before_tax'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('unit_cost_tax'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('avg_cost_before_tax')); //nueva
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('avg_cost_tax'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('base_price_before_tax'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('base_price_with_tax'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('total_quantity'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('total_last_cost_before_vat'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('total_last_cost_with_vat'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('total_avg_cost_before_vat')); //nueva
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('total_avg_cost_with_vat')); //nueva
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('total_sales_price_with_vat'));

            if ($warehouse) {
                $dataWarehouse = $this->reports_model->get_warehouses($warehouse);
            }else {
                $dataWarehouse = $this->reports_model->get_warehouses();
            }

            // $column = "W";
            $row = "2";
            foreach ($dataWarehouse as $keyW => $valueW) {
                $this->excel->getActiveSheet()->SetCellValue($column.$row, lang('quantity').' '.lang('warehouse').' ' .$valueW['name']);
                $column++;
                $warehouse_productsDataVariants[$valueW['id']] = $this->reports_model->get_warehouses_products_variants($valueW['id']);
                $warehouse_productsData[$valueW['id']] = $this->reports_model->get_warehouses_products($valueW['id']);
            }

            $subCategoriesData = $this->reports_model->get_subcategories();
            $brandsData = $this->reports_model->get_brands();
            $colorsData = $this->reports_model->get_colors();
            $materialsData = $this->reports_model->get_materials();
            $unitData = $this->reports_model->get_units();

            $row = 3;
            foreach ($data as $data_row) {
                $column = "A";
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->code);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->name);
                if ($variants) {
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->codeV);
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->nameV);
                }
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->reference);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, lang($data_row->type));
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->category);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, (($data_row->subcategory_id) && isset($subCategoriesData[$data_row->subcategory_id])) ? $subCategoriesData[$data_row->subcategory_id] : '' );
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, (($data_row->second_level_subcategory_id) && isset($subCategoriesData[$data_row->second_level_subcategory_id])) ? $subCategoriesData[$data_row->second_level_subcategory_id] : '' );
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, (($data_row->brand) && isset($brandsData[$data_row->brand])) ? $brandsData[$data_row->brand] : '' );
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, (($data_row->color_id) && isset($colorsData[$data_row->color_id])) ? $colorsData[$data_row->color_id] : '' );
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, (($data_row->material_id) && isset($materialsData[$data_row->material_id])) ? $materialsData[$data_row->material_id] : '' );
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, (($data_row->unit) && isset($unitData[$data_row->unit])) ? $unitData[$data_row->unit] : '' );
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, ($data_row->discontinued == 1) ? lang('inactive') : lang('active') );
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->tax_rate_name );
                // costo unitario antes de iva
                if ($data_row->tax_method == 1) { // parametrizado para que el costo este sin el iva incluido
                    $cost_without_vat = $data_row->cost;
                    $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $cost_without_vat );
                }elseif($data_row->tax_method == 0){ // parametrizado para que el costro este con el iva incluido
                    $cost_without_vat = $data_row->cost - $this->sma->calculate_tax($data_row->tax_rate, $data_row->cost, $data_row->tax_method);
                    $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $cost_without_vat );
                }
                // costo unitario con iva incluido
                if ($data_row->tax_method == 1) { // parametrizado para que el costo este sin el iva incluido
                    $cost_with_vat = $data_row->cost + $this->sma->calculate_tax($data_row->tax_rate, $data_row->cost, $data_row->tax_method);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $cost_with_vat );
                }elseif($data_row->tax_method == 0){ // parametrizado para que el costro este con el iva incluido
                    $cost_with_vat = $data_row->cost;
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $cost_with_vat );
                }
                // costo promedio antes de iva
                $costAvg_without_vat = $data_row->avg_cost - $this->sma->calculate_tax($data_row->tax_rate, $data_row->avg_cost, 0);
                $this->excel->getActiveSheet()->SetCellValue($column++ . $row, $costAvg_without_vat );
                // costo promedio con iva incluido
                $costAvg_with_vat = $data_row->avg_cost;
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $costAvg_with_vat );

                // precio unitario antes de iva
                if ($data_row->tax_method == 1) { // parametrizado para que el precio este sin el iva incluido
                    $price_without_vat = $data_row->price;
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $price_without_vat );
                }elseif($data_row->tax_method == 0){ // parametrizado para que el costro este con el iva incluido
                    $price_without_vat = $data_row->price - $this->sma->calculate_tax($data_row->tax_rate, $data_row->price, $data_row->tax_method);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $price_without_vat );
                }
                // precio unitario con iva incluido
                if ($data_row->tax_method == 1) { // parametrizado para que el precio este sin el iva incluido
                    $price_with_vat = $data_row->price + $this->sma->calculate_tax($data_row->tax_rate, $data_row->price, $data_row->tax_method);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $price_with_vat );
                }elseif($data_row->tax_method == 0){ // parametrizado para que el costro este con el iva incluido
                    $price_with_vat = $data_row->price;
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $price_with_vat );
                }

                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->quantity );
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->quantity * $cost_without_vat );
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->quantity * $cost_with_vat );
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->quantity * $costAvg_without_vat );
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->quantity * $costAvg_with_vat );
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->quantity * $price_with_vat );
                // $column = "W";

                foreach ($dataWarehouse as $keyW => $valueW) {
                    if ($variants) {
                        if ($data_row->opption_id) {
                            $this->excel->getActiveSheet()->SetCellValue($column.$row, (isset($warehouse_productsDataVariants[$valueW['id']][$data_row->opption_id]) ? $warehouse_productsDataVariants[$valueW['id']][$data_row->opption_id] : 0));
                        }else{
                            $this->excel->getActiveSheet()->SetCellValue($column.$row, (isset($warehouse_productsData[$valueW['id']][$data_row->id]) ? $warehouse_productsData[$valueW['id']][$data_row->id] : 0));
                        }
                    }else{
                        $this->excel->getActiveSheet()->SetCellValue($column.$row, (isset($warehouse_productsData[$valueW['id']][$data_row->id]) ? $warehouse_productsData[$valueW['id']][$data_row->id] : 0));
                    }
                    $column++;
                }
                $row++;
            }

            $this->excel->getActiveSheet()->getRowDimension('2')->setRowHeight(28);
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AA')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AB')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AC')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AD')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AE')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AF')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AG')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AH')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AI')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AJ')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AK')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AL')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AM')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AN')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AO')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AP')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('AQ')->setWidth(20);

            if ($variants) {
                $this->excel->getActiveSheet()->getStyle('P3:T'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('V3:X'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            }else{
                $this->excel->getActiveSheet()->getStyle('N3:R'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('T3:AF'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            }

            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A2:AZ' . $row)->getAlignment()->setWrapText(true);
            $filename = 'products report';
            $this->load->helper('excel');

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'products',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('valued_products_report'),
            ]);

            create_excel($this->excel, $filename);
        } else {
            $this->datatables->unset_column('category');
            $this->datatables->unset_column('type');
            echo $this->datatables->generate();
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'products',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('valued_products_report'),
            ]);
        }
    }

    function categories()
    {
        $this->sma->checkPermissions('products');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => sprintf(lang('sale_purchase_categories_report'), lang('category'))));
        $meta = array('page_title' => sprintf(lang('sale_purchase_categories_report'), lang('category')), 'bc' => $bc);
        $this->page_construct('reports/categories', $meta, $this->data);
    }

    function getCategoriesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $date_records_filter = $this->input->get('date_records_filter') ? $this->input->get('date_records_filter') : NULL;
        $visualization_day = $this->input->get('visualization_day') ? $this->input->get('visualization_day') : NULL;
        $visualization_month = $this->input->get('visualization_month') ? $this->input->get('visualization_month') : NULL;
        $visualization_month_year = $this->input->get('visualization_month_year') ? $this->input->get('visualization_month_year') : NULL;
        $exclude_returns = $this->input->get('exclude_returns') ? $this->input->get('exclude_returns') : NULL;

        if ($this->session->userdata('biller_id')) {
            $biller = $this->session->userdata('biller_id');
        }
        if ($this->session->userdata('warehouse_id')) {
            $warehouse = $this->session->userdata('warehouse_id');
        }

        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             // $start_date = $this->sma->fsd($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01');
            }
        }
        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             // $end_date = $this->sma->fsd($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d');
            }
        }
        if ($date_records_filter == 'daily') {
            $start_date = $visualization_day." 00:00:00";
            $end_date = $visualization_day." 23:59:59";
        } else if ($date_records_filter == 'monthly') {
            $start_date = $visualization_month_year."-".$visualization_month."-01 00:00:00";
            $end_date = $visualization_month_year."-".$visualization_month."-31 23:59:59";
        }
        if ($start_date) {
            $start_date = $start_date." 00:00:00";
        }
        if ($end_date) {
            $end_date = $end_date." 23:59:00";
        }
        $pp = "(
                SELECT
                    pp.category_id as category,
                    SUM( COALESCE(pi.quantity, 0) ) purchasedQty,
                    SUM( COALESCE(pi.subtotal, 0) ) totalPurchase
                FROM {$this->db->dbprefix('products')} pp
                LEFT JOIN " . $this->db->dbprefix('purchase_items') . " pi ON pp.id = pi.product_id
                INNER JOIN " . $this->db->dbprefix('purchases') . " p ON p.id = pi.purchase_id ";
        $sp = "(
                SELECT
                    sp.category_id as category,
                    SUM( COALESCE(si.quantity, 0) ) soldQty,
                    SUM( COALESCE(si.subtotal, 0) ) totalSale
                FROM {$this->db->dbprefix('products')} sp
                LEFT JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                INNER JOIN " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
        if ($start_date || $warehouse || $biller || (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right'))) {
            $pp .= " WHERE p.purchase_type = 1 AND ";
            $sp .= " WHERE ";
            if ($start_date) {
                $start_date = $start_date;
                $end_date = $end_date ? $end_date : date('Y-m-d H:i');
                $pp .= " p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse || $biller || (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) ) {
                    if ($warehouse || $biller ) {
                        $pp .= " AND ";
                    }
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
                if ($biller || (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right'))) {
                    if ($biller ) {
                        $pp .= " AND ";
                    }
                    $sp .= " AND ";
                }
            }

            if ($biller) {
                $pp .= " p.biller_id = '{$biller}' ";
                $sp .= " s.biller_id = '{$biller}' ";
                if ((!$this->Owner && !$this->Admin && !$this->session->userdata('view_right'))) {
                    $sp .= " AND ";
                }
            }

            if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                    $sp .= ' (s.created_by = '.$this->session->userdata('user_id').' OR s.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')';
                } else {
                    $sp .= ' s.created_by = '.$this->session->userdata('user_id');
                }
            }

        }
        $pp .= " GROUP BY pp.category_id ) PCosts";
        $sp .= " GROUP BY sp.category_id ) PSales";

        $this->db
            ->select(
                        $this->db->dbprefix('categories') . ".id as cid,
                        " .$this->db->dbprefix('categories') . ".code,
                        " . $this->db->dbprefix('categories') . ".name,
                        SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                        SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                        SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                        SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                        (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit
                    ", FALSE)
            ->from('categories')
            ->join($sp, 'categories.id = PSales.category', 'left')
            ->join($pp, 'categories.id = PCosts.category', 'left')
            ->where('categories.parent_id', 0)
            ;

        if ($category) {
            $this->db->where('categories.id', $category);
        }
        $this->db->group_by('categories.id, categories.code, categories.name, PSales.SoldQty, PSales.totalSale, PCosts.purchasedQty, PCosts.totalPurchase');
        $q = $this->db->get();

        $data = [];

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->cid]['data'] = $row;
            }
        }

        /* SIN CATEGORÍA */
        $q = $this->db->select('
                                "N/A" as code,
                                "N/A" as name,
                                PSales.category as cid,
                                PSales.soldQty as SoldQty,
                                PSales.totalSale as TotalSales
                                ')->from($sp)->where('PSales.category = 0')->get();
        $sale_na = [];
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $sale_na[$row->cid] = $row;
            }
        }
        $q = $this->db->select('
                                "N/A" as code,
                                "N/A" as name,
                                PCosts.category as cid,
                                PCosts.purchasedQty as PurchasedQty,
                                PCosts.totalPurchase as TotalPurchase
                                ')->from($pp)->where('PCosts.category = 0')->get();
        $purchase_na = [];
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $purchase_na[$row->cid] = $row;
            }
        }
        if (isset($sale_na[0]) && isset($purchase_na[0])) {
            $data[0]['data'] = array_merge((array)$sale_na[0], (array)$purchase_na[0]);
            $data[0]['data'] = (object) $data[0]['data'];
            $data[0]['data']->Profit = $data[0]['data']->TotalSales - $data[0]['data']->TotalPurchase;
        } else if (isset($sale_na[0]) || isset($purchase_na[0])) {
            if (isset($sale_na[0])) {
                $data[0]['data'] = $sale_na[0];
            }
            if (isset($purchase_na[0])) {
                $data[0]['data'] = $purchase_na[0];
            }
        }
        /* SIN CATEGORÍA */


        $pp = "(
                SELECT
                    pp.subcategory_id as subcategory,
                    SUM( COALESCE(pi.quantity, 0) ) purchasedQty,
                    SUM( COALESCE(pi.subtotal, 0) ) totalPurchase,
                    pp.category_id as subcategory_parent_id
                FROM {$this->db->dbprefix('products')} pp
                LEFT JOIN " . $this->db->dbprefix('purchase_items') . " pi ON pp.id = pi.product_id
                INNER JOIN " . $this->db->dbprefix('purchases') . " p ON p.id = pi.purchase_id ";
        $sp = "(
                SELECT
                    sp.subcategory_id as subcategory,
                    SUM( COALESCE(si.quantity, 0) ) soldQty,
                    SUM( COALESCE(si.subtotal, 0) ) totalSale,
                    sp.category_id as subcategory_parent_id
                FROM {$this->db->dbprefix('products')} sp
                LEFT JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                INNER JOIN " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
        if ($start_date || $warehouse || $biller) {
            $pp .= " WHERE p.purchase_type = 1 AND ";
            $sp .= " WHERE ";
            if ($start_date) {
                $start_date = $start_date;
                $end_date = $end_date ? $end_date : date('Y-m-d H:i');
                $pp .= " p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse || $biller) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
                if ($biller) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }

            if ($biller) {
                $pp .= " p.biller_id = '{$biller}' ";
                $sp .= " s.biller_id = '{$biller}' ";
            }
        }
        $pp .= " GROUP BY pp.subcategory_id ) PCosts";
        $sp .= " GROUP BY sp.subcategory_id ) PSales";

        $this->db
            ->select(
                        $this->db->dbprefix('categories') . ".id as cid,
                        " .$this->db->dbprefix('categories') . ".parent_id,
                        " .$this->db->dbprefix('categories') . ".code,
                        " . $this->db->dbprefix('categories') . ".name,
                        SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                        SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                        SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                        SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                        (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit
                    ", FALSE)
            ->from('categories')
            ->join($sp, 'categories.id = PSales.subcategory', 'left')
            ->join($pp, 'categories.id = PCosts.subcategory', 'left')
            ->where('categories.parent_id !=', 0)
            ;

        if ($category) {
            $this->db->where('categories.parent_id', $category);
        }
        $this->db->group_by('categories.id, categories.code, categories.name, PSales.SoldQty, PSales.totalSale, PCosts.purchasedQty, PCosts.totalPurchase');
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->parent_id]['subcategories'][] = $row;
            }
        }

        /* SIN CATEGORÍA */

        $pp = "(
                SELECT
                    pp.subcategory_id as subcategory,
                    SUM( COALESCE(pi.quantity, 0) ) purchasedQty,
                    SUM( COALESCE(pi.subtotal, 0) ) totalPurchase,
                    pp.category_id as subcategory_parent_id
                FROM {$this->db->dbprefix('products')} pp
                LEFT JOIN " . $this->db->dbprefix('purchase_items') . " pi ON pp.id = pi.product_id
                INNER JOIN " . $this->db->dbprefix('purchases') . " p ON p.id = pi.purchase_id ";
        $sp = "(
                SELECT
                    sp.subcategory_id as subcategory,
                    SUM( COALESCE(si.quantity, 0) ) soldQty,
                    SUM( COALESCE(si.subtotal, 0) ) totalSale,
                    sp.category_id as subcategory_parent_id
                FROM {$this->db->dbprefix('products')} sp
                LEFT JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                INNER JOIN " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
        if ($start_date || $warehouse || $biller) {
            $pp .= " WHERE p.purchase_type = 1 AND ";
            $sp .= " WHERE ";
            if ($start_date) {
                $start_date = $start_date;
                $end_date = $end_date ? $end_date : date('Y-m-d H:i');
                $pp .= " p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse || $biller) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
                if ($biller) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }

            if ($biller) {
                $pp .= " p.biller_id = '{$biller}' ";
                $sp .= " s.biller_id = '{$biller}' ";
            }
        } else {
            $pp .= " WHERE '1' = '1' ";
            $sp .= " WHERE '1' = '1' ";
        }
        $pp .= " GROUP BY pp.category_id, pp.subcategory_id ) PCosts";
        $sp .= " GROUP BY sp.category_id, sp.subcategory_id ) PSales";

        $q = $this->db->select('
                                "N/A" as code,
                                "N/A" as name,
                                PSales.subcategory as cid,
                                PSales.subcategory_parent_id,
                                PSales.soldQty as SoldQty,
                                PSales.totalSale as TotalSales
                                ')->from($sp)->where('PSales.subcategory = 0')->get();
        $category_no_subcategory = [];
        $sale_na = [];
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $sale_na[$row->subcategory_parent_id] = $row;
                if (!isset($category_no_subcategory[$row->subcategory_parent_id])) {
                    $category_no_subcategory[$row->subcategory_parent_id] = 1;
                }
            }
        }

        $q = $this->db->select('
                                "N/A" as code,
                                "N/A" as name,
                                PCosts.subcategory as cid,
                                PCosts.subcategory_parent_id,
                                PCosts.purchasedQty as PurchasedQty,
                                PCosts.totalPurchase as TotalPurchase
                                ')->from($pp)->where('PCosts.subcategory = 0')->get();
        $purchase_na = [];
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $purchase_na[$row->subcategory_parent_id] = $row;
                if (!isset($category_no_subcategory[$row->subcategory_parent_id])) {
                    $category_no_subcategory[$row->subcategory_parent_id] = 1;
                }
            }
        }

        foreach ($category_no_subcategory as $p_category_id => $setted) {
            if (isset($sale_na[$p_category_id]) && isset($purchase_na[$p_category_id])) {
                $c_data = array_merge((array) $sale_na[$p_category_id], (array) $purchase_na[$p_category_id]);
                $c_data = (object) $c_data;
                $c_data->Profit = $c_data->TotalSales - $c_data->TotalPurchase;
            } else if (isset($sale_na[$p_category_id])) {
                $c_data = $sale_na[$p_category_id];
            } else if (isset($purchase_na[$p_category_id])) {
                $c_data = $purchase_na[$p_category_id];
            }
            $data[$p_category_id]['subcategories'][] = $c_data;
        }

        /* SIN CATEGORÍA */

        if ($pdf || $xls) {

            if (!empty($data)) {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('categories_report_xls_title'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('category'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('subcategory'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('purchased'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('sold'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('purchased_amount'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('sold_amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('profit_loss'));

                $row = 2;
                foreach ($data as $data_row) {
                    if (isset($data_row['subcategories'])) {
                        foreach ($data_row['subcategories'] as $subcategory) {
                            // $this->sma->print_arrays($subcategory);
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, (isset($data_row['data']->name) ? $data_row['data']->name : ""));
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, $subcategory->name);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $subcategory->TotalPurchase);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $subcategory->TotalSales);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $subcategory->PurchasedQty);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $subcategory->SoldQty);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $row, $subcategory->Profit);
                            $row++;
                        }
                    } else {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row['data']->name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, ' - ');
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row['data']->TotalPurchase);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row['data']->TotalSales);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row['data']->PurchasedQty);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row['data']->SoldQty);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row['data']->Profit);
                        $row++;
                    }
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'categories_report';
                $this->load->helper('excel');

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales_purchases',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.sprintf(lang('sale_purchase_categories_report'), lang('category')),
                ]);

                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            $html = '';
            foreach ($data as $row) {
                if (isset($row['subcategories'])) {
                    foreach ($row['subcategories'] as $subcategory) {
                        $margin = ((($subcategory->TotalSales - $subcategory->TotalPurchase)/($subcategory->TotalSales > 0 ? $subcategory->TotalSales : 1))*100);
                        $html .='<tr>
                                    <td>'.$row['data']->name.'</td>
                                    <td>'.$subcategory->name.'</td>
                                    <td>'.$subcategory->TotalPurchase.'</td>
                                    <td>'.$subcategory->TotalSales.'</td>
                                    <td>'.$subcategory->PurchasedQty.'</td>
                                    <td>'.$subcategory->SoldQty.'</td>
                                    <td>'.($margin > 0 || $margin < 0 ? $margin : 0).'</td>
                                    <td>'.$subcategory->Profit.'</td>
                                  </tr>';
                    }
                } else {
                    $margin = ((($row['data']->TotalSales - $row['data']->TotalPurchase)/($row['data']->TotalSales > 0 ? $row['data']->TotalSales : 1))*100);
                    $html .='<tr>
                                <td>'.$row['data']->name.'</td>
                                <td> - </td>
                                <td>'.$row['data']->TotalPurchase.'</td>
                                <td>'.$row['data']->TotalSales.'</td>
                                <td>'.$row['data']->PurchasedQty.'</td>
                                <td>'.$row['data']->SoldQty.'</td>
                                <td>'.($margin > 0 || $margin < 0 ? $margin : 0).'</td>
                                <td>'.$row['data']->Profit.'</td>
                              </tr>';
                }
            }
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'sales_purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.sprintf(lang('sale_purchase_categories_report'), lang('category')),
            ]);
            echo $html;
        }

    }

    function brands()
    {
        $this->sma->checkPermissions('products');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => sprintf(lang('brands_report'), lang('brands'))));
        $meta = array('page_title' => sprintf(lang('brands_report'), lang('brands')), 'bc' => $bc);
        $this->page_construct('reports/brands', $meta, $this->data);
    }

    function getBrandsReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $brand = $this->input->get('brand') ? $this->input->get('brand') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        $pp = "( SELECT pp.brand as brand, SUM( pi.quantity ) purchasedQty, SUM( pi.subtotal ) totalPurchase from {$this->db->dbprefix('products')} pp
                left JOIN " . $this->db->dbprefix('purchase_items') . " pi ON pp.id = pi.product_id
                left join " . $this->db->dbprefix('purchases') . " p ON p.id = pi.purchase_id ";
        $sp = "( SELECT sp.brand as brand, SUM( si.quantity ) soldQty, SUM( si.subtotal ) totalSale from {$this->db->dbprefix('products')} sp
                left JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                left join " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
        if ($start_date || $warehouse) {
            $pp .= " WHERE ";
            $sp .= " WHERE ";
            if ($start_date) {
                $pp .= " p.date >= '{$start_date}' AND p.date < '{$end_date}' ";
                $sp .= " s.date >= '{$start_date}' AND s.date < '{$end_date}' ";
                if ($warehouse) {
                    $pp .= " AND ";
                    $sp .= " AND ";
                }
            }
            if ($warehouse) {
                $pp .= " pi.warehouse_id = '{$warehouse}' ";
                $sp .= " si.warehouse_id = '{$warehouse}' ";
            }
        }
        $pp .= " GROUP BY pp.brand ) PCosts";
        $sp .= " GROUP BY sp.brand ) PSales";

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('brands') . ".name,
                    SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                    SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                    SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                    SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                    (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit", FALSE)
                ->from('brands')
                ->join($sp, 'brands.id = PSales.brand', 'left')
                ->join($pp, 'brands.id = PCosts.brand', 'left')
                ->group_by('brands.id, brands.name')
                ->order_by('brands.code', 'asc');

            if ($brand) {
                $this->db->where($this->db->dbprefix('brands') . ".id", $brand);
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(sprintf(lang('brands_report'), lang('brands')));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('brands'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('purchased'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('sold'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('purchased_amount'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('sold_amount'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('profit_loss'));

                $row = 2; $sQty = 0; $pQty = 0; $sAmt = 0; $pAmt = 0; $pl = 0;
                foreach ($data as $data_row) {
                    $profit = $data_row->TotalSales - $data_row->TotalPurchase;
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->PurchasedQty);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->SoldQty);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->TotalPurchase);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->TotalSales);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $profit);
                    $pQty += $data_row->PurchasedQty;
                    $sQty += $data_row->SoldQty;
                    $pAmt += $data_row->TotalPurchase;
                    $sAmt += $data_row->TotalSales;
                    $pl += $profit;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("B" . $row . ":F" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $pQty);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sQty);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $pAmt);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $sAmt);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $pl);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'brands_report';
                $this->load->helper('excel');

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales_purchases',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.sprintf(lang('brands_report'), lang('brands')),
                ]);

                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {


            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('brands') . ".id as id, " . $this->db->dbprefix('brands') . ".name,
                    SUM( COALESCE( PCosts.purchasedQty, 0 ) ) as PurchasedQty,
                    SUM( COALESCE( PSales.soldQty, 0 ) ) as SoldQty,
                    SUM( COALESCE( PCosts.totalPurchase, 0 ) ) as TotalPurchase,
                    SUM( COALESCE( PSales.totalSale, 0 ) ) as TotalSales,
                    (SUM( COALESCE( PSales.totalSale, 0 ) )- SUM( COALESCE( PCosts.totalPurchase, 0 ) ) ) as Profit", FALSE)
                ->from('brands')
                ->join($sp, 'brands.id = PSales.brand', 'left')
                ->join($pp, 'brands.id = PCosts.brand', 'left');

            if ($brand) {
                $this->datatables->where('brands.id', $brand);
            }
            $this->datatables->group_by('brands.id, brands.name, PSales.SoldQty, PSales.totalSale, PCosts.purchasedQty, PCosts.totalPurchase');
            $this->datatables->unset_column('id');
            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'sales_purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.sprintf(lang('brands_report'), lang('brands')),
            ]);

        }

    }

    function profit($date = NULL, $biller_id = NULL, $re = NULL)
    {
        if (!($this->Owner || $this->Admin || $this->GP['reports-daily_sales']) ) {
            $this->session->set_flashdata('error', lang('access_denied'));
            $this->sma->md();
        }
        if ( ! $date) { $date = date('Y-m-d'); }

        $this->data['costing'] = $this->reports_model->getCosting($date, $biller_id);
        $this->data['discount'] = $this->reports_model->getOrderDiscount($date, $biller_id);
        $this->data['expenses'] = $this->reports_model->getExpenses($date, $biller_id);
        $this->data['returns'] = $this->reports_model->getReturns($date, $biller_id);
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['swh'] = $biller_id;
        $this->data['date'] = $date;
        if ($re) {
            echo $this->load_view($this->theme . 'reports/profit', $this->data, TRUE);
            exit();
        }
        $this->load_view($this->theme . 'reports/profit', $this->data);
    }
    function monthly_profit($year, $month, $biller_id = NULL, $re = NULL)
    {
        if (!($this->Admin || $this->Owner ||  $this->GP['reports-daily_purchases'])) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
        }

        $this->data['costing'] = $this->reports_model->getCosting(NULL, $biller_id, $year, $month);
        $this->data['discount'] = $this->reports_model->getOrderDiscount(NULL, $biller_id, $year, $month);
        $this->data['expenses'] = $this->reports_model->getExpenses(NULL, $biller_id, $year, $month);
        $this->data['returns'] = $this->reports_model->getReturns(NULL, $biller_id, $year, $month);
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['swh'] = $biller_id;
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $this->data['date'] = date('F Y', strtotime($year.'-'.$month.'-'.'01'));
        if ($re) {
            $this->load_view($this->theme . 'reports/monthly_profit', $this->data);
            return;
        }
        $this->load_view($this->theme . 'reports/monthly_profit', $this->data);
    }

    function daily_sales($biller_id = NULL, $year = NULL, $month = NULL, $pdf = NULL, $user_id = NULL)
    {
        $biller_id = ($biller_id == 0 ? NULL : $biller_id);
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
            $biller_id = $this->session->userdata('biller_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => admin_url('reports/daily_sales/'.($biller_id ? $biller_id : 0)),
            'month_type' => 'long',
            'day_type' => 'long'
        );

        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'sales',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('daily_sales'),
        ]);

        $config['template'] = '
        {table_open}<div class="table-responsive">
                        <table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable reports-table">{/table_open}
        {heading_row_start}
                        <thead>
                            <tr>{/heading_row_start}
        {heading_previous_cell}
                                <th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}
                                <th colspan="{colspan}" id="month_year" style="font-size:18px;">{heading}</th>{/heading_title_cell}
        {heading_next_cell}
                                <th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}
                            </tr>
                        </thead>{/heading_row_end}
        {week_row_start}
                        <tr>{/week_row_start}
        {week_day_cell}
                        <td class="cl_wday day_name">{week_day}</td>{/week_day_cell}
        {week_row_end}
                        </tr>{/week_row_end}
        {cal_row_start}
                        <tr class="days">{/cal_row_start}
        {cal_cell_start}
                            <td class="day">{/cal_cell_start}
        {cal_cell_content}
                                <div class="day_num" data-daynum="{day}">{day}</br>
                                    <div class="content">{content}</div>
                                </div>
        {/cal_cell_content}
        {cal_cell_content_today}
                                <div class="day_num highlight" data-daynum="{day}">{day}</br>
                                    <div class="content">{content}</div>
                                </div>
        {/cal_cell_content_today}
        {cal_cell_no_content}
                                <div class="day_num" data-daynum="{day}">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $sales = $user_id ? $this->reports_model->getStaffDailySales($user_id, $year, $month, $biller_id) : $this->reports_model->getDailySales($year, $month, $biller_id);

        if (!empty($sales)) {
            foreach ($sales as $sale) {
                $daily_sale[$sale->date] = "
                <table class='data' style='margin-top:6%;font-size: 85%; width:100%;'>
                    <tr ".($sale->sub_total > 0 ? "" : "style='display:none;'").">
                        <th>" . lang("sub_total") . "</th>
                        <td class='text-right'>" . $this->sma->formatMoney($sale->sub_total) . "</td>
                    </tr>
                    <tr ".($sale->discount > 0 ? "" : "style='display:none;'").">
                        <th>" . lang("discount") . "</th>
                        <td class='text-right'>" . $this->sma->formatMoney($sale->discount) . "</td>
                    </tr>
                    <tr ".($sale->shipping > 0 ? "" : "style='display:none;'").">
                        <th>" . lang("shipping") . "</th>
                        <td class='text-right'>" . $this->sma->formatMoney($sale->shipping) . "</td>
                    </tr>
                    <tr ".($sale->tax1 > 0 ? "" : "style='display:none;'").">
                        <th>" . lang("tax") . "</th>
                        <td class='text-right'>" . $this->sma->formatMoney($sale->tax1) . "</td>
                    </tr>
                    <tr ".($sale->tax2 > 0 ? "" : "style='display:none;'").">
                        <th>" . lang("order_tax") . "</th>
                        <td class='text-right'>" . $this->sma->formatMoney($sale->tax2) . "</td>
                    </tr>
                    <tr ".($sale->tip > 0 ? "" : "style='display:none;'").">
                        <th>" . lang("tip") . "</th>
                        <td class='text-right'>" . $this->sma->formatMoney($sale->tip) . "</td>
                    </tr>
                    <tr ".($sale->total > 0 ? "style='border-top-style: solid; border-top-width: 1px; mar'" : "style='display:none;'").">
                        <th style='font-size:107%;padding-top: 3%;'>" . lang("total") . "</th>
                        <th style='font-size:115%;padding-top: 3%;' class='text-right'>" . $this->sma->formatMoney($sale->total) . "</th>
                    </tr>
                    <tr ".($sale->returned_total != 0 ? "" : "style='display:none;'").">
                        <th>" . lang("returns") . "</th>
                        <td class='text-right'>" . $this->sma->formatMoney($sale->returned_total) . "</td>
                    </tr>
                </table>";
            }
        } else {
            $daily_sale = array();
        }

        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_sale);
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        if ($pdf) {
            $html = $this->load_view($this->theme . 'reports/daily', $this->data, true);
            $name = lang("daily_sales") . "_" . $year . "_" . $month . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['biller_id'] = $biller_id;
        $this->data['sel_biller'] = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('daily_sales_report')));
        $meta = array('page_title' => lang('daily_sales').' ('.($this->data['sel_biller'] ? $this->data['sel_biller']->name : lang('all_billers')).')', 'bc' => $bc);
        $this->page_construct('reports/daily', $meta, $this->data);

    }


    function monthly_sales($biller_id = NULL, $year = NULL, $pdf = NULL, $user_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
            $biller_id = $this->session->userdata('biller_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        $this->load->language('calendar');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['year'] = $year;
        $sales = $this->reports_model->getMonthlySales($year, $biller_id);
        $sales_order = [];
        foreach ($sales as $sale) {
            $sales_order[] = $sale->date;
        }
        array_multisort($sales_order, SORT_ASC, $sales);
        $this->data['sales'] = $sales;

        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'sales',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('monthly_sales'),
        ]);

        if ($pdf) {
            $html = $this->load_view($this->theme . 'reports/monthly', $this->data, true);
            $name = lang("monthly_sales") . "_" . $year . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        // $this->sma->print_arrays($this->data['sales']);
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['biller_id'] = $biller_id;
        $this->data['sel_biller'] = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('monthly_sales_report').' ('.($this->data['sel_biller'] ? $this->data['sel_biller']->name : lang('all_billers')).')'));
        $meta = array('page_title' => lang('monthly_sales_report').' ('.($this->data['sel_biller'] ? $this->data['sel_biller']->name : lang('all_billers')).')', 'bc' => $bc);
        $this->page_construct('reports/monthly', $meta, $this->data);

    }

    function sales()
    {
        $this->sma->checkPermissions('sales');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['sellers'] = $this->site->getAllCompanies('seller');
        $this->data['customers'] = $this->site->getAllCompanies('customer');
        $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['states'] = $this->site->get_states();
        $this->data['sale_document_types'] = $this->site->get_multi_module_document_types([1, 2, 3, 4]);

        $this->page_construct('reports/sales', ['page_title' => lang('sales_report')], $this->data);
    }

    function getSalesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('sales', TRUE);

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $seller = $this->input->get('seller') ? $this->input->get('seller') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $brand = $this->input->get('brand') ? $this->input->get('brand') : NULL;
        $address = $this->input->get('address') ? $this->input->get('address') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $detailed_sales = $this->input->get('detailed_sales') ? $this->input->get('detailed_sales') : NULL;
        $detailed_seller = $this->input->get('detailed_seller') ? $this->input->get('detailed_seller') : NULL;
        $per_variant = $this->input->get('per_variant') ? $this->input->get('per_variant') : NULL;
        $sale_document_type = $this->input->get('sale_document_type') ? $this->input->get('sale_document_type') : NULL;
        $detailed_sales = ($detailed_seller || $per_variant) ? 1 : $detailed_sales;
        $state = $this->input->get('state') ? $this->input->get('state') : NULL;
        $option_id = $this->input->get('option_id') ? $this->input->get('option_id') : NULL;
        $city = $this->input->get('city') ? $this->input->get('city') : NULL;
        $zone = $this->input->get('zone') ? $this->input->get('zone') : NULL;
        $subzone = $this->input->get('subzone') ? $this->input->get('subzone') : NULL;
        $detail_profitability = $this->input->get('detail_profitability');
        if ($this->input->get('start_date')) {
            $start_date = $this->input->get('start_date');
            $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;

        $si = "( SELECT
                    {$this->db->dbprefix('sale_items')}.quantity as quantity,
                    {$this->db->dbprefix('sale_items')}.net_unit_price as net_unit_price,
                    {$this->db->dbprefix('sale_items')}.unit_price as unit_price,
                    {$this->db->dbprefix('sale_items')}.item_discount as item_discount,
                    ({$this->db->dbprefix('sale_items')}.net_unit_price * {$this->db->dbprefix('sale_items')}.quantity) as total,
                    ({$this->db->dbprefix('sale_items')}.item_tax + {$this->db->dbprefix('sale_items')}.item_tax_2) as total_tax,
                    {$this->db->dbprefix('sale_items')}.avg_net_unit_cost as avg_cost,
                    {$this->db->dbprefix('sale_items')}.tax as tax,
                    {$this->db->dbprefix('sale_items')}.tax_rate_id as tax_rate_id,
                    {$this->db->dbprefix('products')}.cost as last_cost,
                    {$this->db->dbprefix('products')}.tax_method as tax_method,
                    {$this->db->dbprefix('products')}.reference as p_reference,
                    {$this->db->dbprefix('sale_items')}.sale_id,
                    {$this->db->dbprefix('sale_items')}.seller_id,
                    {$this->db->dbprefix('sale_items')}.product_id,
                    serial_no,
                    {$this->db->dbprefix('sale_items')}.product_name as item_nane,
                    {$this->db->dbprefix('sale_items')}.product_code as item_code,
                    {$this->db->dbprefix('brands')}.name as brand_name,
                    ".($per_variant ? "{$this->db->dbprefix('product_variants')}.name as variant_name," : "")."
                    ".($per_variant ? "{$this->db->dbprefix('product_variants')}.code as variant_code," : "")."
                    {$this->db->dbprefix('categories')}.name as category_name,
                    sc.name as subcategory_name,
                    ssc.name as second_level_subcategory_name
                FROM {$this->db->dbprefix('sale_items')} ";

        $si.="LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id = {$this->db->dbprefix('sale_items')}.product_id
              LEFT JOIN {$this->db->dbprefix('brands')} ON {$this->db->dbprefix('brands')}.id = {$this->db->dbprefix('products')}.brand
              LEFT JOIN {$this->db->dbprefix('categories')} ON {$this->db->dbprefix('categories')}.id = {$this->db->dbprefix('products')}.category_id
              LEFT JOIN {$this->db->dbprefix('categories')} sc ON sc.id = {$this->db->dbprefix('products')}.subcategory_id
              LEFT JOIN {$this->db->dbprefix('categories')} ssc ON ssc.id = {$this->db->dbprefix('products')}.second_level_subcategory_id
        ";

        // $this->sma->print_arrays($si);

        if ($per_variant) {
            $si.= "INNER JOIN {$this->db->dbprefix('product_variants')} ON ".($option_id ? "{$this->db->dbprefix('product_variants')}.name = '{$option_id}' AND " : "")." {$this->db->dbprefix('sale_items')}.option_id = {$this->db->dbprefix('product_variants')}.id ";
        }
        if ($start_date) {
            $si.="INNER JOIN {$this->db->dbprefix('sales')} ON {$this->db->dbprefix('sales')}.id = {$this->db->dbprefix('sale_items')}.sale_id AND ".$this->db->dbprefix('sales').".date BETWEEN '" . $start_date . "' and '".$end_date."' ";
        }

        if ($product || $serial || $category || $brand) { $si .= " WHERE "; }

        if ($product) {
            $si .= " {$this->db->dbprefix('sale_items')}.product_id = {$product} ";
            if ($serial || $category || $brand) { $si .= " AND "; }
        }
        if ($serial) {
            $si .= " {$this->db->dbprefix('sale_items')}.serial_no LIKe '%{$serial}%' ";
            if ($category || $brand) { $si .= " AND "; }
        }

        if ($category) {
            $si .= " {$this->db->dbprefix('products')}.category_id = {$category} ";
            if ($brand) { $si .= " AND "; }
        }

        if ($brand) {
            $si .= " {$this->db->dbprefix('products')}.brand = {$brand} ";
        }

        if ($detailed_sales) {
            $si .= " ORDER BY {$this->db->dbprefix('sale_items')}.sale_id DESC ".($detailed_seller ? ", {$this->db->dbprefix('sale_items')}.seller_id DESC" : "").") FSI";
        } else {
            $si .= " GROUP BY {$this->db->dbprefix('sale_items')}.sale_id ) FSI";
        }

        $this->load->library('datatables');

        $this->datatables
            ->select("
                        {$this->db->dbprefix('sales')}.id as id,
                        DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                        reference_no,
                        return_sale_ref as affects_to,
                        biller,
                        companies.name as customer_name,
                        ".
                        ($xls ? "
                                    companies.phone,
                                    companies.email,
                                    sales.payment_method,
                                    companies.vat_no as customer_vat_no,
                                    companies.company as customer_company,
                                    ".($detailed_sales ? ' (((FSI.unit_price * FSI.quantity) / ('.$this->db->dbprefix('sales').'.grand_total - '.$this->db->dbprefix('sales').'.shipping)) * '.$this->db->dbprefix('sales').'.shipping) as shipping ' : 'sales.shipping').",
                                    ".($detailed_sales && $per_variant ? ' FSI.variant_name as variant_name, ' : '')."
                                    ".($detailed_sales && $per_variant ? ' FSI.variant_code as variant_code, ' : '')."
                                    sales.tip_amount,
                                    seller.name as seller_name,
                            ".($detailed_sales ?
                                    "FSI.avg_cost,
                                    FSI.last_cost,
                                    FSI.net_unit_price,
                                    FSI.unit_price,
                                    FSI.item_discount,
                                    FSI.tax,
                                    FSI.tax_method,
                                    FSI.tax_rate_id,
                                    FSI.brand_name,
                                    FSI.category_name,
                                    FSI.subcategory_name,
                                    FSI.second_level_subcategory_name,
                                    ((FSI.net_unit_price * FSI.quantity) + FSI.item_discount) A1,
                                    (FSI.net_unit_price * FSI.quantity) A2,
                                    (FSI.unit_price * FSI.quantity) A3,
                                    (FSI.avg_cost * FSI.quantity) A4,
                                    ((FSI.net_unit_price - FSI.avg_cost) * FSI.quantity) A5,
                                    (((((FSI.net_unit_price * FSI.quantity) - (FSI.avg_cost * FSI.quantity)) / (IF(FSI.net_unit_price > 0 , FSI.net_unit_price , 1) * FSI.quantity)) * 100)) A6,
                                    FSI.item_code as icode," : ""
                            ).
                                    "addresses.sucursal as sucursal,
                                    addresses.city as sucursal_city,
                                    addresses.state as sucursal_state,
                                    addresses.country as sucursal_country,
                                    price_groups.name as price_group_name,
                                " : "")
                        ."
                        ".($detailed_sales ? 'FSI.item_nane as iname,' : '')."
                        ".($detailed_sales ? 'FSI.p_reference as p_reference,' : '')."
                        ".($detailed_seller ? 'si_seller.company as iseller_name,' : '')."
                        ".($detailed_sales ? 'FSI.quantity,' : '')."
                        ".($detailed_sales ? 'FSI.total' : 'sales.total').",
                        ".($detailed_sales ? 'FSI.total_tax' : 'sales.total_tax').",
                        ".($detailed_sales ? '(((((FSI.unit_price * FSI.quantity) / ( '.$this->db->dbprefix('sales').'.grand_total + '.$this->db->dbprefix('sales').'.order_discount )) * '.$this->db->dbprefix('sales').'.order_discount))) as order_discount' : 'sales.order_discount').",

                        (".
                            ($detailed_sales ?
                                'COALESCE(FSI.total, 0) + COALESCE(FSI.total_tax, 0) - (((((FSI.unit_price * FSI.quantity) / ( '.$this->db->dbprefix('sales').'.grand_total + COALESCE('.$this->db->dbprefix('sales').'.order_discount, 0) )) * '.$this->db->dbprefix('sales').'.order_discount)))'
                                : 'grand_total')
                        .") as grand_total,
                        paid,
                        (grand_total-paid) as balance,
                        sale_status, ".
                        ($xls ? "sales.note, " : "").
                        ($xls ? "sales.staff_note, " : "").
                        ($xls ? " (SELECT CONCAT(first_name, ' ', last_name) FROM {$this->db->dbprefix('users')} WHERE id = {$this->db->dbprefix('sales')}.created_by) AS created_by, " : "")
                        ."last_payments.lp_reference_no", FALSE)
            ->from('sales');
        if ($detailed_sales) {
            $this->datatables->join($si, 'FSI.sale_id=sales.id', 'INNER');
        }
        $this->datatables
            ->join('addresses', 'addresses.id = sales.address_id', 'left')
            ->join('companies', 'companies.id = sales.customer_id', 'left')
            ->join('price_groups', 'price_groups.id = companies.price_group_id', 'left')
            ->join('companies as seller', 'seller.id = sales.seller_id', 'left')
            ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
            ->join('(SELECT P1.id, P1.reference_no AS lp_reference_no, P1.sale_id
                        FROM '.$this->db->dbprefix('payments').' P1
                        JOIN (
                            SELECT sale_id, MAX(id) AS latest_id
                            FROM '.$this->db->dbprefix('payments').'
                            WHERE sale_id IS NOT NULL AND multi_payment = 1
                            GROUP BY sale_id
                    ) P2 ON P1.sale_id = P2.sale_id AND P1.id = P2.latest_id) AS last_payments', 'last_payments.sale_id = '.$this->db->dbprefix('sales').'.id', 'left');
        if ($detailed_seller) {
            $this->datatables->join('companies si_seller', 'si_seller.id = FSI.seller_id', 'left');
        }
        if ($user) {
            $this->datatables->where('sales.created_by', $user);
        }
        if ($product && $detailed_sales) {
            $this->datatables->where('FSI.product_id', $product);
        }
        if ($state) {
            $this->datatables->where('addresses.state', $state);
        }
        if ($city) {
            $this->datatables->where('addresses.city', $city);
        }
        if ($zone) {
            $this->datatables->where('addresses.location', $zone);
        }
        if ($subzone) {
            $this->datatables->where('addresses.subzone', $subzone);
        }
        if ($serial && $detailed_sales) {
            $this->datatables->like('FSI.serial_no', $serial);
        }
        if ($biller) {
            $this->datatables->where('sales.biller_id', $biller);
        }
        if ($seller) {
            $this->datatables->where('sales.seller_id', $seller);
        }
        if ($customer) {
            $this->datatables->where('sales.customer_id', $customer);
        }
        if ($warehouse) {
            $this->datatables->where('sales.warehouse_id', $warehouse);
        }
        if ($reference_no) {
            $this->datatables->like('sales.reference_no', $reference_no, 'both');
        }
        if ($start_date) {
            $this->datatables->where($this->db->dbprefix('sales').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }
        if ($address) {
            $this->datatables->where('sales.address_id', $address);
        }
        if ($sale_document_type) {
            $this->datatables->where('sales.document_type_id', $sale_document_type);
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
            }
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }
        if ($pdf || $xls) {
            $this->datatables->order_by('sales.date ASC');
        }
        if ($pdf || $xls) {
            if ($start_date) {
                $fechaInicio = DateTime::createFromFormat('Y-m-d H:i', $start_date);
                $fechaFin = DateTime::createFromFormat('Y-m-d H:i', $end_date);
                $diferencia = $fechaFin ? $fechaInicio->diff($fechaFin) : 0;
                $diasDiferencia = $diferencia ? $diferencia->days : 0;
            } else {
                $diasDiferencia = 9999;
            }
            $data = $this->datatables->get_display_result_2();
            if (!empty($data)) {
                if (!$detailed_sales) {
                    // $columns_paid_by = $this->get_sales_xls_payment_details($data);
                }
                $this->load->library('excel');
                $number_format_columns = [];
                $mes_recorrido = [];
                $sheet_num = NULL;
                foreach ($data as $data_row) {
                    $txtQtys = "";
                    $d_arr = explode(" ", $data_row->date);
                    $fecha = $d_arr[0];
                    $hora = $d_arr[1];
                    $letter = 'A';

                    $dia = date('j', strtotime($data_row->date));
                    $ultimoDiaMes = date('t', strtotime($data_row->date));
                    if ($dia == $ultimoDiaMes) {
                        $quincena = 2;
                    } else {
                        $quincena = ceil($dia / 15);
                    }
                    // $time_slice = date('M', strtotime($data_row->date))."-".$quincena;
                    if ($diasDiferencia > 31) {
                        $time_slice = lang(date('M', strtotime($data_row->date)))."_".date('Y', strtotime($data_row->date));
                    } else {
                        $time_slice = 'Hoja 1';
                    }

                    if (!isset($mes_recorrido[$time_slice])) {
                        if ($sheet_num === NULL) {
                            $sheet_num = 0;
                        } else {

                            for ($i='A'; $i <= 'Z'; $i++) {
                                $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                            }
                            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                            foreach ($number_format_columns as $column => $csetted) {
                                $this->excel->getActiveSheet()->getStyle($column.'1:'.$column.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                            }

                            $range = 'F1:F'.$row;
                            $this->excel->getActiveSheet()
                                ->getStyle($range)
                                ->getNumberFormat()
                                ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                            $range = 'D1:D'.$row;
                            $this->excel->getActiveSheet()
                                ->getStyle($range)
                                ->getNumberFormat()
                                ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                            $sheet_num++;
                        }
                        $mes_recorrido[$time_slice] = 1;

                        $show_cost = ($detail_profitability && ($this->Admin || $this->Owner || $this->GP['products-cost'])) ? true : false;

                        $this->excel->createSheet();
                        $this->excel->setActiveSheetIndex($sheet_num);
                        $this->excel->getActiveSheet()->setTitle($time_slice);
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('date'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('day'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('time'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('stripe'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('reference_no'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('affects_to'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('biller'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('vat_no'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('label_business_name'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('email'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('phone'));
                        $letter++;
                        if (!$detailed_sales) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('label_tradename'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('customer_branch'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('city'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('state'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('country'));
                            $letter++;
                        }
                        if ($detailed_sales) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('product_code'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('product'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('reference'));
                            $letter++;
                            if ($detailed_seller) {
                                $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('product_seller'));
                                $letter++;
                            }
                            if ($per_variant) {
                                $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('variant'));
                                $letter++;
                                $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('variant_code'));
                                $letter++;
                            }
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('product_qty'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('brand'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('category'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('subcategory'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('second_level_subcategory_id'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Vr Bruto');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Dcto Producto');
                            $letter++;
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Subtotal');
                        $letter++;
                        if ($detailed_sales) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', '% Impuestos');
                            $letter++;
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Impuestos');
                        $letter++;
                        if ($detailed_sales) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Precio Venta Unit');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Vr total');
                            $letter++;
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Dcto General');
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('shipping'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('tip'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Total');
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('original_payment_method'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('price_group'));
                        $letter++;
                        if (!$detailed_seller) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('seller'));
                            $letter++;
                        }
                        if ($detailed_sales && $show_cost) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Último Costo Unit. Antes de IVA');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Último Costo Unit. Con IVA');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Costo promedio Unit. Antes de IVA');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Costo promedio Unit con IVA');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Costo promedio total Antes de IVA');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Utilidad');
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', 'Rentabilidad');
                            $letter++;
                        }
                        if (!$detailed_sales) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('paid'));
                            $letter++;
                            // foreach ($columns_paid_by as $paid_by => $asd) {
                            //     $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang($paid_by));
                            //     $letter++;
                            // }
                        }
                        if (!$detailed_sales) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('balance'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('sale_status'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('last_payment'));
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('note'));
                            $letter++;
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('staff_note'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('created_by'));
                        $letter++;
                        $row = 2;
                    }
                    $letter = 'A';
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $fecha);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, lang(strtolower(date('l', strtotime($data_row->date)))));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $hora);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, date('H', strtotime($data_row->date)));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->reference_no);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->affects_to);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->biller);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->customer_vat_no);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->customer_name);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->email);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->phone);
                    $letter++;
                    if (!$detailed_sales) {
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->customer_company);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->sucursal);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->sucursal_city);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->sucursal_state);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->sucursal_country);
                        $letter++;
                    }
                    if ($detailed_sales) {
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->icode);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->iname);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->p_reference);
                        $letter++;
                        if ($detailed_seller) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->iseller_name);
                            $letter++;
                        }
                        if ($per_variant) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->variant_name);
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->variant_code);
                            $letter++;
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->quantity);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->brand_name);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->category_name);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->subcategory_name);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->second_level_subcategory_name);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->A1);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->item_discount);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $detailed_sales ? $data_row->A2 : $data_row->total);
                    $number_format_columns[$letter] = 1;
                    $letter++;
                    if ($detailed_sales) {
                        $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->tax);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $detailed_sales ? $data_row->total_tax : $data_row->total_tax);
                    $number_format_columns[$letter] = 1;
                    $letter++;
                    if ($detailed_sales) {
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, ($data_row->unit_price));
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->A3);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->order_discount);
                    $number_format_columns[$letter] = 1;
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->shipping);
                    $number_format_columns[$letter] = 1;
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->tip_amount);
                    $number_format_columns[$letter] = 1;
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->grand_total);
                    $number_format_columns[$letter] = 1;
                    $letter++;
                    //NUEVAS ANTES VENDEDOR
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, lang($data_row->payment_method));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->price_group_name);
                    $letter++;
                    //NUEVAS ANTES VENDEDOR
                    if (!$detailed_seller) {
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->seller_name);
                        $letter++;
                    }
                    if ($detailed_sales && $show_cost) {
                        $last_cost_tax_val = $this->sma->calculate_tax($data_row->tax_rate_id, $data_row->last_cost, $this->Settings->tax_method);
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->last_cost - ($this->Settings->tax_method == 0 ? $last_cost_tax_val : 0));
                        $number_format_columns[$letter] = 1; 
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->last_cost + ($this->Settings->tax_method == 1 ? $last_cost_tax_val : 0));
                        $number_format_columns[$letter] = 1;
                        $letter++;

                        $avg_cost_tax_val = $this->sma->calculate_tax($data_row->tax_rate_id, $data_row->avg_cost, 1);

                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->avg_cost);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->avg_cost + $avg_cost_tax_val);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->A4);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->A5);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->A6);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                    }
                    if (!$detailed_sales) {
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->paid);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        // foreach ($columns_paid_by as $paid_by => $pmnt_sales) {
                        //     $this->excel->getActiveSheet()->SetCellValue($letter.$row, isset($pmnt_sales[$data_row->id]) ? $pmnt_sales[$data_row->id] : 0);
                        $number_format_columns[$letter] = 1;
                        //     $letter++;
                        // }
                    }

                    if (!$detailed_sales) {
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->balance);
                        $number_format_columns[$letter] = 1;
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, lang($data_row->sale_status));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, ($data_row->lp_reference_no));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, strip_tags($this->sma->decode_html($data_row->note)));
                        $letter++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letter.$row, $data_row->staff_note);
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter.$row, strtoupper($data_row->created_by));
                    $letter++;
                    $row++;
                }
                for ($i='A'; $i <= 'Z'; $i++) {
                    $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                }
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                foreach ($number_format_columns as $column => $csetted) {
                    $this->excel->getActiveSheet()->getStyle($column.'1:'.$column.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                }

                $range = 'F1:F'.$row;
                $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                $range = 'D1:D'.$row;
                $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                $filename = lang('sales_report');
                $this->load->helper('excel');

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('sales_report'),
                ]);

                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'sales',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('sales_report'),
            ]);
        }
    }

    function getSalesSummaryReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('index', true, 'sales');
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $address = $this->input->get('address') ? $this->input->get('address') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $portfolio = $this->input->get('portfolio') ? $this->input->get('portfolio') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            $start_date = NULL;
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            $end_date = NULL;
        }
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;


        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        $si = "( SELECT
        GROUP_CONCAT(CONCAT({$this->db->dbprefix('sale_items')}.quantity) SEPARATOR '___') as quantity,
        sale_id,
        product_id,
        serial_no,

        GROUP_CONCAT(CONCAT({$this->db->dbprefix('sale_items')}.product_name) SEPARATOR '___') as item_nane

        from {$this->db->dbprefix('sale_items')} ";
        if ($product || $serial) { $si .= " WHERE "; }
        if ($product) {
            $si .= " {$this->db->dbprefix('sale_items')}.product_id = {$product} ";
        }
        if ($product && $serial) { $si .= " AND "; }
        if ($serial) {
            $si .= " {$this->db->dbprefix('sale_items')}.serial_no LIKe '%{$serial}%' ";
        }
        $si .= " GROUP BY {$this->db->dbprefix('sale_items')}.sale_id ) FSI";

        $this->load->library('datatables');

        $this->datatables
            ->select("
                        DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                        reference_no,
                        return_sale_ref as affects_to,
                        biller,
                        addresses.sucursal,
                        grand_total,
                        paid,
                        (grand_total-paid) as balance,
                        payment_status,
                        sale_status,
                        {$this->db->dbprefix('sales')}.id as id,
                        seller.company as seller_name,
                        sales.note", FALSE)
            ->from('sales')
            ->join($si, 'FSI.sale_id=sales.id', 'left')
            ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
            ->join('addresses', 'addresses.id = sales.address_id', 'left')
            ->join('companies as seller', 'seller.id=sales.seller_id', 'left')
            ;
        if ($product) {
            $this->datatables->where('FSI.product_id', $product);
        }
        if ($serial) {
            $this->datatables->like('FSI.serial_no', $serial);
        }
        if ($biller) {
            $this->datatables->where('sales.biller_id', $biller);
        }
        if ($customer) {
            $this->datatables->where('sales.customer_id', $customer);
        }
        if ($warehouse) {
            $this->datatables->where('sales.warehouse_id', $warehouse);
        }
        if ($reference_no) {
            $this->datatables->like('sales.reference_no', $reference_no, 'both');
        }
        if ($start_date) {
            $this->datatables->where($this->db->dbprefix('sales').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }

        if ($address) {
            $this->datatables->where('sales.address_id', $address);
        }
        if ($portfolio) {
            $this->datatables->where('('.$this->db->dbprefix('sales').'.grand_total - '.$this->db->dbprefix('sales').'.paid) > 0 AND '.$this->db->dbprefix('sales').'.sale_status != "returned" ');
        }

        if (!$this->Customer  && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
            }
        }

        if ($pdf || $xls) {
            $data = $this->datatables->get_display_result_2();

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer_branch'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('payment_status'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('seller'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('note'));

                $row = 2;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $qty = 0;
                foreach ($data as $data_row) {

                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->sucursal);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->paid);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, ($data_row->grand_total - $data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, lang($data_row->payment_status));
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->seller_name);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, strip_tags($this->sma->decode_html($data_row->note)));
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("E" . $row . ":G" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $balance);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(45);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(50);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('F1:F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('G1:G'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $filename = 'sales_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            echo $this->datatables->generate();
        }


    }

    function getOrderSalesSummaryReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('orders', TRUE, 'sales');
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $user = null;
        if ($this->input->get('start_date')) {
            $start_date = $this->input->get('start_date');
            $start_date = $this->sma->fld($start_date);
        } else {
            $start_date = NULL;
        }

        if ($this->input->get('end_date')) {
            $end_date = $this->input->get('end_date');
            $end_date = $this->sma->fld($end_date);
        } else {
            $end_date = NULL;
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }
        $this->load->library('datatables');
        $this->datatables->select("
                                    {$this->db->dbprefix('order_sales')}.id as id,
                                    DATE_FORMAT({$this->db->dbprefix('order_sales')}.date, '%Y-%m-%d %T') as date,
                                    CONCAT({$this->db->dbprefix('order_sales')}.reference_no, IF({$this->db->dbprefix('order_sales')}.notification_status = 0, ' <i class=\"fa fa-bell\" style=\"color:#428bca;\"></i>', '')) as reference_no,
                                    destination_reference_no,
                                    biller.name as biller_name,
                                    CONCAT(customer.name, ' (', customer.company, ')') AS customer_name,
                                    seller.name,
                                    sale_status,
                                    grand_total,
                                    order_sale_origin");
        $this->datatables->from('order_sales');
        $this->datatables->join('delivery_time', 'delivery_time.id = order_sales.delivery_time_id', 'left');
        $this->datatables->join('companies as seller', 'seller.id = order_sales.seller_id', 'left');
        $this->datatables->join('companies as customer', 'customer.id = order_sales.customer_id', 'left');
        $this->datatables->join('companies as biller', 'biller.id = order_sales.biller_id', 'left');

        if ($biller) {
            $this->datatables->where('order_sales.biller_id', $biller);
        }
        if ($user) {
            $this->datatables->where('order_sales.created_by', $user);
        }
        if ($customer) {
            $this->datatables->where('order_sales.customer_id', $customer);
        }
        if ($start_date) {
            $this->datatables->where('order_sales.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('order_sales.date <=', $end_date);
        }
        if (!$this->Customer  && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(order_sales.created_by = '.$this->session->userdata('user_id').' OR order_sales.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('order_sales.created_by', $this->session->userdata('user_id'));
            }
        }

        if ($pdf || $xls) {
            $data = $this->datatables->get_display_result_2();
            if (!empty($data)) {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('order_sales'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('destination_reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('seller'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('status'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('origin'));
                $row = 2;
                $total = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->destination_reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->biller_name);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->customer_name);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, lang($data_row->sale_status));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->order_origin($data_row->order_sale_origin));
                    $total += $data_row->grand_total;
                    $row++;
                }
                for ($i='A'; $i <= 'Z'; $i++) {
                    $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                }
                $this->excel->getActiveSheet()->getStyle('H1:H'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $filename = 'Order_sales';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            echo $this->datatables->generate();
        }
    }

    function getQuotesReport($pdf = NULL, $xls = NULL)
    {

        if ($this->input->get('product')) {
            $product = $this->input->get('product');
        } else {
            $product = NULL;
        }
        if ($this->input->get('user')) {
            $user = $this->input->get('user');
        } else {
            $user = NULL;
        }
        if ($this->input->get('customer')) {
            $customer = $this->input->get('customer');
        } else {
            $customer = NULL;
        }
        if ($this->input->get('biller')) {
            $biller = $this->input->get('biller');
        } else {
            $biller = NULL;
        }
        if ($this->input->get('warehouse')) {
            $warehouse = $this->input->get('warehouse');
        } else {
            $warehouse = NULL;
        }
        if ($this->input->get('reference_no')) {
            $reference_no = $this->input->get('reference_no');
        } else {
            $reference_no = NULL;
        }
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        if ($pdf || $xls) {

            $this->db
                ->select("date, reference_no, biller, customer, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('quote_items') . ".product_name, ' (', " . $this->db->dbprefix('quote_items') . ".quantity, ')') SEPARATOR '<br>') as iname, grand_total, quotes.status", FALSE)
                ->from('quotes')
                ->join('quote_items', 'quote_items.quote_id=quotes.id', 'left')
                ->join('warehouses', 'warehouses.id=quotes.warehouse_id', 'left')
                ->group_by('quotes.id');

            if ($user) {
                $this->db->where('quotes.created_by', $user);
            }
            if ($product) {
                $this->db->where('quote_items.product_id', $product);
            }
            if ($biller) {
                $this->db->where('quotes.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('quotes.customer_id', $customer);
            }
            if ($warehouse) {
                $this->db->where('quotes.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('quotes.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('quotes').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('quotes_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('status'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->customer);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatMoney($data_row->grand_total));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->status);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $filename = 'quotes_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $qi = "( SELECT quote_id, product_id, GROUP_CONCAT(CONCAT({$this->db->dbprefix('quote_items')}.product_name, '__', {$this->db->dbprefix('quote_items')}.quantity) SEPARATOR '___') as item_nane from {$this->db->dbprefix('quote_items')} ";
            if ($product) {
                $qi .= " WHERE {$this->db->dbprefix('quote_items')}.product_id = {$product} ";
            }
            $qi .= " GROUP BY {$this->db->dbprefix('quote_items')}.quote_id ) FQI";
            $this->load->library('datatables');
            $this->datatables
                ->select("date, reference_no, biller, customer, FQI.item_nane as iname, grand_total, quotes.status, {$this->db->dbprefix('quotes')}.id as id", FALSE)
                ->from('quotes')
                ->join($qi, 'FQI.quote_id=quotes.id', 'left')
                ->join('warehouses', 'warehouses.id=quotes.warehouse_id', 'left')
                ->group_by('quotes.id');

            if ($user) {
                $this->datatables->where('quotes.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FQI.product_id', $product, FALSE);
            }
            if ($biller) {
                $this->datatables->where('quotes.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('quotes.customer_id', $customer);
            }
            if ($warehouse) {
                $this->datatables->where('quotes.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('quotes.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('quotes').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();

        }

    }

    function getTransfersReport($pdf = NULL, $xls = NULL)
    {
        if ($this->input->get('product')) {
            $product = $this->input->get('product');
        } else {
            $product = NULL;
        }

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('transfers') . ".date, reference_no, (CASE WHEN " . $this->db->dbprefix('transfers') . ".status = 'completed' THEN  GROUP_CONCAT(" . $this->db->dbprefix('purchase_items') . ".product_name SEPARATOR '<br>') ELSE GROUP_CONCAT(" . $this->db->dbprefix('transfer_items') . ".product_name SEPARATOR '<br>') END) as iname, (CASE WHEN " . $this->db->dbprefix('transfers') . ".status = 'completed' THEN  GROUP_CONCAT(" . $this->db->dbprefix('purchase_items') . ".quantity SEPARATOR '<br>') ELSE GROUP_CONCAT(" . $this->db->dbprefix('transfer_items') . ".quantity SEPARATOR '<br>') END) as pquantity,  from_warehouse_name as fname, from_warehouse_code as fcode, to_warehouse_name as tname,to_warehouse_code as tcode, grand_total, " . $this->db->dbprefix('transfers') . ".status")
                ->from('transfers')
                ->join('transfer_items', 'transfer_items.transfer_id=transfers.id', 'left')
                ->join('purchase_items', 'purchase_items.transfer_id=transfers.id', 'left')
                ->group_by('transfers.id')->order_by('transfers.date desc');
            if ($product) {
                $this->db->where($this->db->dbprefix('purchase_items') . ".product_id", $product);
                $this->db->or_where($this->db->dbprefix('transfer_items') . ".product_id", $product);
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('transfers_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('warehouse') . ' (' . lang('from') . ')');
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('warehouse') . ' (' . lang('to') . ')');
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('status'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->pquantity);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->fname . ' (' . $data_row->fcode . ')');
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->tname . ' (' . $data_row->tcode . ')');
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatMoney($data_row->grand_total));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->status);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:C' . $row)->getAlignment()->setWrapText(true);
                $filename = 'transfers_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("{$this->db->dbprefix('transfers')}.date, reference_no, (CASE WHEN {$this->db->dbprefix('transfers')}.status = 'completed' THEN  GROUP_CONCAT({$this->db->dbprefix('purchase_items')}.product_name SEPARATOR '___') ELSE GROUP_CONCAT({$this->db->dbprefix('transfer_items')}.product_name SEPARATOR '___') END) as iname, (CASE WHEN {$this->db->dbprefix('transfers')}.status = 'completed' THEN  GROUP_CONCAT({$this->db->dbprefix('purchase_items')}.quantity SEPARATOR '___') ELSE GROUP_CONCAT({$this->db->dbprefix('transfer_items')}.quantity SEPARATOR '___') END) as pquantity,from_warehouse_name as fname, from_warehouse_code as fcode, to_warehouse_name as tname,to_warehouse_code as tcode, grand_total, {$this->db->dbprefix('transfers')}.status, {$this->db->dbprefix('transfers')}.id as id", FALSE)
                ->from('transfers')
                ->join('transfer_items', 'transfer_items.transfer_id=transfers.id', 'left')
                ->join('purchase_items', 'purchase_items.transfer_id=transfers.id', 'left')
                ->group_by('transfers.id');
            if ($product) {
                $this->datatables->where(" (({$this->db->dbprefix('purchase_items')}.product_id = {$product}) OR ({$this->db->dbprefix('transfer_items')}.product_id = {$product})) ", NULL, FALSE);
            }
            $this->datatables->edit_column("fname", "$1 ($2)", "fname, fcode")
                ->edit_column("tname", "$1 ($2)", "tname, tcode")
                ->unset_column('fcode')
                ->unset_column('tcode');
            echo $this->datatables->generate();

        }

    }

    function purchases()
    {
        $this->sma->checkPermissions('purchases');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('purchases_report')));
        $meta = array('page_title' => lang('purchases_report'), 'bc' => $bc);
        $this->page_construct('reports/purchases', $meta, $this->data);
    }

    function getPurchasesReport($pdf = NULL, $xls = NULL)
    {

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $detailed_purchases = $this->input->get('detailed_purchases') ? $this->input->get('detailed_purchases') : NULL;
        $purchase_evaluated = $this->input->get('purchase_evaluated');
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        $pi = "( SELECT
                    {$this->db->dbprefix('purchase_items')}.id,
                    {$this->db->dbprefix('purchase_items')}.quantity as quantity,
                    {$this->db->dbprefix('purchase_items')}.product_code as product_code,
                    {$this->db->dbprefix('purchase_items')}.net_unit_cost as net_unit_cost,
                    {$this->db->dbprefix('purchase_items')}.unit_cost as unit_cost,
                    ({$this->db->dbprefix('purchase_items')}.item_discount / {$this->db->dbprefix('purchase_items')}.quantity) as unit_item_discount,
                    {$this->db->dbprefix('purchase_items')}.item_discount as item_discount,
                    ({$this->db->dbprefix('purchase_items')}.net_unit_cost * {$this->db->dbprefix('purchase_items')}.quantity) as total,
                    ({$this->db->dbprefix('purchase_items')}.item_tax + {$this->db->dbprefix('purchase_items')}.item_tax_2) as total_tax,
                    ({$this->db->dbprefix('purchase_items')}.item_tax / {$this->db->dbprefix('purchase_items')}.quantity) as unit_tax,
                    {$this->db->dbprefix('purchase_items')}.tax as tax,
                    {$this->db->dbprefix('purchase_items')}.tax_rate_id as tax_rate_id,
                    {$this->db->dbprefix('products')}.cost as last_cost,
                    {$this->db->dbprefix('products')}.tax_method as tax_method,
                    {$this->db->dbprefix('purchase_items')}.purchase_id,
                    product_id,
                    serial_no,
                    {$this->db->dbprefix('purchase_items')}.product_name as item_nane,
                    {$this->db->dbprefix('purchase_items')}.product_code as item_code,
                    {$this->db->dbprefix('brands')}.name as brand_name,
                    {$this->db->dbprefix('categories')}.name as category_name
                FROM {$this->db->dbprefix('purchase_items')} ";

        // if ($category || $brand) {
            $pi.="LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id = {$this->db->dbprefix('purchase_items')}.product_id
                  LEFT JOIN {$this->db->dbprefix('brands')} ON {$this->db->dbprefix('brands')}.id = {$this->db->dbprefix('products')}.brand
                  LEFT JOIN {$this->db->dbprefix('categories')} ON {$this->db->dbprefix('categories')}.id = {$this->db->dbprefix('products')}.category_id
            ";
        // }

        if ($start_date) { //se relaciona encabezado a subconsulta para que en el del detalle no se traiga todos los registros de purchase_items, sólo se trae los que tienen que ver con la fecha seleccionada (mejora rendimiento)
            $pi.="INNER JOIN {$this->db->dbprefix('purchases')} ON {$this->db->dbprefix('purchases')}.id = {$this->db->dbprefix('purchase_items')}.purchase_id AND ".$this->db->dbprefix('purchases').".date BETWEEN '" . $start_date . "' and '".$end_date."'";
        }

        if ($product) { $pi .= " WHERE "; }

        if ($product) {
            $pi .= " {$this->db->dbprefix('purchase_items')}.product_id = {$product} ";
        }

        if ($detailed_purchases) {
            $pi .= " ORDER BY {$this->db->dbprefix('purchase_items')}.purchase_id DESC, {$this->db->dbprefix('purchase_items')}.id ASC) FSI";
        } else {
            $pi .= " GROUP BY {$this->db->dbprefix('purchase_items')}.purchase_id ) FSI";
        }


        $this->load->library('datatables');
        $this->datatables
            ->select("DATE_FORMAT({$this->db->dbprefix('purchases')}.date, '%Y-%m-%d %T') as date,
                      reference_no,
                      {$this->db->dbprefix('warehouses')}.name as wname,
                      supplier,
                        ".($detailed_purchases ? 'FSI.item_nane as iname,' : '')."
                        ".($detailed_purchases ? 'FSI.quantity,' : '')."
                        ".($detailed_purchases && $xls ? 'FSI.product_code,' : '')."
                        ".($detailed_purchases && $xls ? 'FSI.unit_cost,' : '')."
                        ".($detailed_purchases && $xls ? 'FSI.unit_item_discount,' : '')."
                        ".($detailed_purchases && $xls ? 'FSI.unit_tax,' : '')."
                        ".($detailed_purchases ? 'FSI.total' : 'purchases.total').",
                        ".($detailed_purchases ? 'FSI.total_tax' : 'purchases.total_tax').",
                        ".($detailed_purchases ? '(((((FSI.unit_cost * FSI.quantity) / ( '.$this->db->dbprefix('purchases').'.grand_total + '.$this->db->dbprefix('purchases').'.order_discount )) * '.$this->db->dbprefix('purchases').'.order_discount))) as order_discount' : 'purchases.order_discount').",

                        (".
                            ($detailed_purchases ?
                                'COALESCE(FSI.total, 0) + COALESCE(FSI.total_tax, 0) - (((((FSI.unit_cost * FSI.quantity) / ( '.$this->db->dbprefix('purchases').'.grand_total + COALESCE('.$this->db->dbprefix('purchases').'.order_discount, 0) )) * '.$this->db->dbprefix('purchases').'.order_discount)))'
                                : 'grand_total')
                        .") as grand_total,
                      paid,
                      (grand_total-paid) as balance,
                      {$this->db->dbprefix('purchases')}.status,
                      ".($this->Settings->manage_purchases_evaluation == 1 ? "{$this->db->dbprefix('purchases')}.purchase_evaluated," : "")."
                      ".($xls && $this->Settings->manage_purchases_evaluation == 1 ? "
                        {$this->db->dbprefix('purchases_evaluation')}.returns as returns_evaluation,
                        {$this->db->dbprefix('purchases_evaluation')}.delivery_supply as delivery_supply_evaluation,
                        {$this->db->dbprefix('purchases_evaluation')}.discount_variations as discount_variations_evaluation,
                        {$this->db->dbprefix('purchases_evaluation')}.pqrs as pqrs_evaluation,
                        {$this->db->dbprefix('purchases_evaluation')}.terms_of_payments as terms_of_payments_evaluation,
                        {$this->db->dbprefix('purchases_evaluation')}.warranty as warranty_evaluation,
                        " : "")."
                      {$this->db->dbprefix('purchases')}.id as id", FALSE)
            ->from('purchases')
            ->join($pi, 'FSI.purchase_id = purchases.id', 'INNER')
            ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');
            if ($xls && $this->Settings->manage_purchases_evaluation == 1) {
                $this->datatables->join('purchases_evaluation', 'purchases_evaluation.purchase_id = purchases.id', 'left');
            }
            $this->datatables->order_by('purchases.id DESC, FSI.id DESC');

        if ($user) {
            $this->datatables->where('purchases.created_by', $user);
        }
        if ($product) {
            $this->datatables->where('FSI.product_id', $product, FALSE);
        }
        if ($supplier) {
            $this->datatables->where('purchases.supplier_id', $supplier);
        }
        if ($warehouse) {
            $this->datatables->where('purchases.warehouse_id', $warehouse);
        }
        if ($reference_no) {
            $this->datatables->like('purchases.reference_no', $reference_no, 'both');
        }
        if ($start_date) {
            $this->datatables->where($this->db->dbprefix('purchases').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }
        if (!is_null($purchase_evaluated)) {
            $this->datatables->where('purchases.purchase_evaluated', $purchase_evaluated);
        }


        if ($pdf || $xls) {
            $data = $this->datatables->get_display_result_2();
            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('purchase_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('Hora'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('supplier'));
                $letra = "F";
                if ($detailed_purchases) {
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('product'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('product_code'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('product_qty'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('unit_cost'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('unit_discount'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('unit_tax'));$letra++;
                }
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('grand_total'));$letra++;
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('paid'));$letra++;
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('balance'));$letra++;
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('status'));$letra++;
                if ($this->Settings->manage_purchases_evaluation == 1) {
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('returns'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('delivery_supply'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('discount_variations'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('pqrs'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('terms_of_payments'));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('warranty'));$letra++;
                }
                $row = 2;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $qty = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrsd($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, date("H:i:s", strtotime($data_row->date)));
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->wname);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->supplier);
                    $letra = "F";
                    if ($detailed_purchases) {
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->iname);$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->product_code);$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->quantity);$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->unit_cost);$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->unit_item_discount);$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->unit_tax);$letra++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->grand_total);$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.$row, $data_row->paid);$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.$row, ($data_row->grand_total - $data_row->paid));$letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.$row, lang($data_row->status));$letra++;
                    if ($this->Settings->manage_purchases_evaluation == 1) {
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $this->sma->formatQuantity($data_row->returns_evaluation));$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $this->sma->formatQuantity($data_row->delivery_supply_evaluation));$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $this->sma->formatQuantity($data_row->discount_variations_evaluation));$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $this->sma->formatQuantity($data_row->pqrs_evaluation));$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $this->sma->formatQuantity($data_row->terms_of_payments_evaluation));$letra++;
                        $this->excel->getActiveSheet()->SetCellValue($letra.$row, $this->sma->formatQuantity($data_row->warranty_evaluation));$letra++;
                    }
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("H" . $row . ":J" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, number_format($qty,2,',','.')." ");
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $balance);
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('G2:G' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('H1:H'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('I1:I'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('J1:J'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $range = 'G1:G'.$row;
                $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                $filename = 'purchase_report';
                $this->load->helper('excel');
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'purchases',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('purchases_report'),
                ]);
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('purchases_report'),
            ]);
        }
    }

    function getPurchasesSummaryReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('purchases', TRUE);

        $product = $this->input->get('product_summary') ? $this->input->get('product_summary') : NULL;
        $user = $this->input->get('user_summary') ? $this->input->get('user_summary') : NULL;
        $supplier = $this->input->get('supplier_summary') ? $this->input->get('supplier_summary') : NULL;
        $warehouse = $this->input->get('warehouse_summary') ? $this->input->get('warehouse_summary') : NULL;
        $reference_no = $this->input->get('reference_no_summary') ? $this->input->get('reference_no_summary') : NULL;
        $purchase_summary_portfolio = $this->input->get('purchase_summary_portfolio') ? $this->input->get('purchase_summary_portfolio') : FALSE;
        if ($this->input->get('start_date_summary')) {
             $start_date = $this->input->get('start_date_summary');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date_summary')) {
             $end_date = $this->input->get('end_date_summary');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        $pi = "( SELECT
            purchase_id,
            product_id,
            (GROUP_CONCAT(CONCAT({$this->db->dbprefix('purchase_items')}.quantity) SEPARATOR '___')) as quantity,

            (GROUP_CONCAT(CONCAT({$this->db->dbprefix('purchase_items')}.product_name) SEPARATOR '___')) as item_nane

            from {$this->db->dbprefix('purchase_items')} ";
        if ($product) {
            $pi .= " WHERE {$this->db->dbprefix('purchase_items')}.product_id = {$product} ";
        }
        $pi .= " GROUP BY {$this->db->dbprefix('purchase_items')}.purchase_id ) FPI";



        $this->load->library('datatables');
        $this->datatables
            ->select("
                        DATE_FORMAT({$this->db->dbprefix('purchases')}.date, '%Y-%m-%d %T') as date,
                        reference_no,
                        {$this->db->dbprefix('warehouses')}.name as wname,
                        supplier,
                        grand_total,
                        paid,
                        (grand_total-paid) as balance,
                        {$this->db->dbprefix('purchases')}.status,
                        {$this->db->dbprefix('purchases')}.id as id
                    ", FALSE)
            ->from('purchases')
            ->join($pi, 'FPI.purchase_id=purchases.id', 'left')
            ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');


        if ($user) {
            $this->datatables->where('purchases.created_by', $user);
        }
        if ($product) {
            $this->datatables->where('FPI.product_id', $product, FALSE);
        }
        if ($supplier) {
            $this->datatables->where('purchases.supplier_id', $supplier);
        }
        if ($warehouse) {
            $this->datatables->where('purchases.warehouse_id', $warehouse);
        }
        if ($reference_no) {
            $this->datatables->like('purchases.reference_no', $reference_no, 'both');
        }
        if ($start_date) {
            $this->datatables->where($this->db->dbprefix('purchases').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }

        if ($purchase_summary_portfolio) {
            $this->datatables->where('('.$this->db->dbprefix('purchases').'.grand_total - '.$this->db->dbprefix('purchases').'.paid) > 0 AND '.$this->db->dbprefix('purchases').'.status != "returned" ');
        }

        if ($pdf || $xls) {

            $data = $this->datatables->get_display_result_2();

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('purchase_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('supplier'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('status'));

                $row = 2;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $qty = 0;
                foreach ($data as $data_row) {

                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->wname);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->supplier);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->paid);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, ($data_row->grand_total - $data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->status);
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $row++;

                }
                $this->excel->getActiveSheet()->getStyle("E" . $row . ":G" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $balance);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('F1:F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('G1:G'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $range = 'F1:F'.$row;
                $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                $filename = 'purchase_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            echo $this->datatables->generate();
        }

    }

    function payments()
    {
        $this->sma->checkPermissions('payments');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['pos_settings'] = POS ? $this->reports_model->getPOSSetting('biller') : FALSE;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('payments_report')));
        $meta = array('page_title' => lang('payments_report'), 'bc' => $bc);
        $this->page_construct('reports/payments', $meta, $this->data);
    }

    function getPaymentsReport($pdf = NULL, $xls = NULL)
    {
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;
        $biller_id = $this->input->get('biller_id') ? $this->input->get('biller_id') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $payment_ref = $this->input->get('payment_ref') ? $this->input->get('payment_ref') : NULL;
        $paid_by = $this->input->get('paid_by') ? $this->input->get('paid_by') : NULL;
        $sale_ref = $this->input->get('sale_ref') ? $this->input->get('sale_ref') : NULL;
        $purchase_ref = $this->input->get('purchase_ref') ? $this->input->get('purchase_ref') : NULL;
        $card = $this->input->get('card') ? $this->input->get('card') : NULL;
        $cheque = $this->input->get('cheque') ? $this->input->get('cheque') : NULL;
        $transaction_id = $this->input->get('tid') ? $this->input->get('tid') : NULL;
        $address = $this->input->get('address') ? $this->input->get('address') : NULL;
        $filter_by = $this->input->get('filter_by') ? $this->input->get('filter_by') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }


        if ($pdf || $xls) {

            $this->db
                ->select(
                        $this->db->dbprefix('payments') . ".date,
                       ".$this->db->dbprefix('payments') . ".comm_payment_date,
                       ".$this->db->dbprefix('payments') . ".reference_no as payment_ref,
                       ".$this->db->dbprefix('sales') . ".reference_no as sale_ref,
                       ".$this->db->dbprefix('purchases') . ".reference_no as purchase_ref,
                        paid_by,
                        amount,
                        type
                        ")
                ->from('payments')
                ->join('sales', 'payments.sale_id=sales.id', 'left')
                ->join('purchases', 'payments.purchase_id=purchases.id', 'left')
                ->group_by('payments.id')
                ->order_by('payments.date desc');

            if ($user) {
                if ($this->session->userdata('company_id')) {
                    $this->db->where("(payments.created_by = {$user} OR sales.seller_id = {$this->session->userdata('company_id')})");
                } else {
                    $this->db->where('payments.created_by', $user);
                }
            }
            if ($card) {
                $this->db->like('payments.cc_no', $card, 'both');
            }
            if ($cheque) {
                $this->db->where('payments.cheque_no', $cheque);
            }
            if ($transaction_id) {
                $this->db->where('payments.transaction_id', $transaction_id);
            }
            if ($customer) {
                $this->db->where('sales.customer_id', $customer);
            }
            if ($supplier) {
                $this->db->where('purchases.supplier_id', $supplier);
            }
            if ($biller) {
                $this->db->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('sales.customer_id', $customer);
            }
            if ($payment_ref) {
                $this->db->like('payments.reference_no', $payment_ref, 'both');
            }
            if ($paid_by) {
                $this->db->where('payments.paid_by', $paid_by);
            }
            if ($sale_ref) {
                $this->db->like('sales.reference_no', $sale_ref, 'both');
            }
            if ($purchase_ref) {
                $this->db->like('purchases.reference_no', $purchase_ref, 'both');
            }
            if ($start_date) {
                $date_field = "date";
                if ($filter_by == 2) {
                    $date_field = "payment_date";
                }
                $this->db->where($this->db->dbprefix('payments').'.'.$date_field.' BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
            if ($address) {
                $this->db->where('sales.address_id', $address);
            }
            if ($biller_id) {
                $this->db->where('sales.biller_id', $biller_id);
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('payments_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('payment_date'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('payment_reference'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('sale_reference'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('purchase_reference'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid_by'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('amount'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('type'));

                $row = 2;
                $total = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $this->sma->hrld($data_row->comm_payment_date));
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->payment_ref);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->sale_ref);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->purchase_ref);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($data_row->paid_by));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->amount);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->type);
                    if ($data_row->type == 'returned' || $data_row->type == 'sent') {
                        $total -= $data_row->amount;
                    } else {
                        $total += $data_row->amount;
                    }
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("F" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $total);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'payments_report';
                $this->load->helper('excel');
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'payments',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('payments_report'),
                ]);
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("
                    DATE_FORMAT({$this->db->dbprefix('payments')}.date, '%Y-%m-%d %T') as date,
                    DATE_FORMAT({$this->db->dbprefix('payments')}.comm_payment_date, '%Y-%m-%d %T') as comm_payment_date,
                    " . $this->db->dbprefix('payments') . ".reference_no as payment_ref,
                    " . $this->db->dbprefix('sales') . ".reference_no as sale_ref,
                    " . $this->db->dbprefix('purchases') . ".reference_no as purchase_ref,
                    paid_by,
                    amount,
                    type,
                    {$this->db->dbprefix('payments')}.id as id
                    ")
                ->from('payments')
                ->join('sales', 'payments.sale_id=sales.id', 'left')
                ->join('purchases', 'payments.purchase_id=purchases.id', 'left')
                ->where('payments.paid_by != "due"')
                ->group_by('payments.id');

            if ($user) {
                if ($this->session->userdata('company_id')) {
                    $this->db->where("(payments.created_by = {$user} OR sales.seller_id = {$this->session->userdata('company_id')})");
                } else {
                    $this->db->where('payments.created_by', $user);
                }
            }
            if ($card) {
                $this->datatables->like('payments.cc_no', $card, 'both');
            }
            if ($cheque) {
                $this->datatables->where('payments.cheque_no', $cheque);
            }
            if ($transaction_id) {
                $this->datatables->where('payments.transaction_id', $transaction_id);
            }
            if ($customer) {
                $this->datatables->where('sales.customer_id', $customer);
            }
            if ($supplier) {
                $this->datatables->where('purchases.supplier_id', $supplier);
            }
            if ($biller) {
                $this->datatables->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('sales.customer_id', $customer);
            }
            if ($payment_ref) {
                $this->datatables->like('payments.reference_no', $payment_ref, 'both');
            }
            if ($paid_by) {
                $this->datatables->where('payments.paid_by', $paid_by);
            }
            if ($sale_ref) {
                $this->datatables->like('sales.reference_no', $sale_ref, 'both');
            }
            if ($purchase_ref) {
                $this->datatables->like('purchases.reference_no', $purchase_ref, 'both');
            }
            if ($start_date) {
                $date_field = "date";
                if ($filter_by == 2) {
                    $date_field = "payment_date";
                }
                $this->datatables->where($this->db->dbprefix('payments').'.'.$date_field.' BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
            if ($address) {
                $this->datatables->where('sales.address_id', $address);
            }
            if ($biller_id) {
                $this->datatables->where('sales.biller_id', $biller_id);
            }

            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'payments',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('payments_report'),
            ]);

        }

    }

    function customers()
    {
        $this->sma->checkPermissions('customers');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('customers_report')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);
        $this->page_construct('reports/customers', $meta, $this->data);
    }

    function getCustomers($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('customers', TRUE);
        $start_date = (isset($_GET['start_date']) && $_GET['start_date'] != '') ? $this->sma->fld($_GET['start_date']) : NULL;
        $end_date = (isset($_GET['end_date']) && $_GET['end_date'] != '') ? $this->sma->fld($_GET['end_date']) : NULL;
        $usfilter = false;
        if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
            $usfilter = $this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id');
        }
        $s = "(
                SELECT customer_id, count(" . $this->db->dbprefix('sales') . ".id) as total, COALESCE(sum(grand_total), 0) as total_amount, COALESCE(sum(paid), 0) as paid, ( COALESCE(sum(grand_total), 0) - COALESCE(sum(paid), 0)) as balance
                from {$this->db->dbprefix('sales')}
                 ".((!$this->Customer  && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) ?
                     (($usfilter) ? "WHERE {$this->db->dbprefix('sales')}.created_by = ".$this->session->userdata('user_id')." OR {$this->db->dbprefix('sales')}.seller_id = ".$usfilter : "WHERE {$this->db->dbprefix('sales')}.created_by = {$this->session->userdata('user_id')}")
                    :
                        ""
                    )."
                GROUP BY {$this->db->dbprefix('sales')}.customer_id
            ) FS";
        $this->load->library('datatables');
        if ($start_date) {
            $this->datatables->where("{$this->db->dbprefix('companies')}.registration_date > ", $start_date);
        }
        if ($end_date) {
            $this->datatables->where("{$this->db->dbprefix('companies')}.registration_date < ", $end_date);
        }
        if ($pdf || $xls) {
            $this->db->select("{$this->db->dbprefix('companies')}.vat_no");
            $this->db->select("{$this->db->dbprefix('documentypes')}.abreviacion");
            $this->db->select("{$this->db->dbprefix('companies')}.name");
            $this->db->select("{$this->db->dbprefix('companies')}.address");
            $this->db->select("{$this->db->dbprefix('companies')}.city");
            $this->db->select("{$this->db->dbprefix('companies')}.phone");
            $this->db->select("RIGHT({$this->db->dbprefix('companies')}.city_code, 5) AS postal");
            $this->db->select("{$this->db->dbprefix('companies')}.status");
            $this->db->select("{$this->db->dbprefix('companies')}.country");
            $this->db->select("{$this->db->dbprefix('companies')}.email");
            $this->db->select("{$this->db->dbprefix('companies')}.phone");
            $this->db->select("{$this->db->dbprefix('companies')}.payment_term");
            $this->db->select("CONCAT({$this->db->dbprefix('users')}.first_name,' ', {$this->db->dbprefix('users')}.last_name) AS created_by");
        } else {  
            $this->datatables->select($this->db->dbprefix('companies') . ".id as id, companies.company, companies.name, companies.phone, companies.email, FS.total, FS.total_amount, FS.paid, FS.balance", FALSE);  
        }

        $this->datatables->from("companies")
            ->join($s, 'FS.customer_id=companies.id', 'left');
        if ($this->session->userdata('company_id')) {
            $this->datatables->join("(SELECT C.id FROM {$this->db->dbprefix('companies')} C
                        INNER JOIN {$this->db->dbprefix('companies')} S ON C.customer_seller_id_assigned = S.id
                    WHERE S.id = {$this->session->userdata('company_id')}) AS c_seller", "c_seller.id = companies.id",'inner');
        }
        $this->datatables->where('companies.group_name', 'customer')
            ->join('documentypes', 'companies.tipo_documento = documentypes.id', 'left')
            ->join('users', 'companies.created_by = users.id', 'left')
            ->group_by('companies.id')
            ->add_column("Actions", "<div class='text-center'><a class=\"tip\" title='" . lang("view_report") . "' href='" . admin_url('reports/customer_report/$1') . "'><span class='label label-primary'>" . lang("view_report") . "</span></a></div>", "id")
            ->unset_column('id');


        if ($pdf || $xls) {

            $data = $this->datatables->get_display_result_2();
            if (!empty($data)) {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('customers_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('label_document_number'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('type'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('name'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('address'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('city'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('phone'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('municipality'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('active'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('has_rut'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('country'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('email'));
                $this->excel->getActiveSheet()->SetCellValue('L1', lang('term'));
                $this->excel->getActiveSheet()->SetCellValue('M1', lang('economic_activity'));
                $this->excel->getActiveSheet()->SetCellValue('N1', lang('indicative'));
                $this->excel->getActiveSheet()->SetCellValue('O1', lang('nature'));
                $this->excel->getActiveSheet()->SetCellValue('P1', lang('created_by'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->vat_no);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->abreviacion);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->address);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->city);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->phone);
                    $this->excel->getActiveSheet()->setCellValueExplicit('G' . $row, $data_row->postal, PHPExcel_Cell_DataType::TYPE_STRING);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, ($data_row->status) == 1 ? 'S' : 'N');
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, '');
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->country);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, $data_row->email);
                    $this->excel->getActiveSheet()->SetCellValue('L' . $row, $data_row->payment_term);
                    $this->excel->getActiveSheet()->SetCellValue('M' . $row, '');
                    $this->excel->getActiveSheet()->SetCellValue('N' . $row, '');
                    $this->excel->getActiveSheet()->SetCellValue('O' . $row, '');
                    $this->excel->getActiveSheet()->SetCellValue('P' . $row, $data_row->created_by);
                    $row++;
                }

                for ($i='A'; $i <= 'Z'; $i++) {
                    $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                }

                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'customers_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            echo $this->datatables->generate();
        }

    }

    function customer_report($user_id = NULL)
    {
        $this->sma->checkPermissions('customers', TRUE);
        if (!$user_id) {
            $this->session->set_flashdata('error', lang("no_customer_selected"));
            admin_redirect('reports/customers');
        }

        $this->data['sales'] = $this->reports_model->getSalesTotals($user_id);
        $this->data['total_sales'] = $this->reports_model->getCustomerSales($user_id);
        $this->data['total_quotes'] = $this->reports_model->getCustomerQuotes($user_id);
        $this->data['total_returns'] = $this->reports_model->getCustomerReturns($user_id);
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');

        $this->data['customer'] = $this->site->getCompanyByID($user_id);
        $this->data['user_id'] = $user_id;
        $this->data['addresses'] = $this->site->getAddressByCustomer($user_id);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('customers_report')));
        $meta = array('page_title' => lang('customers_report'), 'bc' => $bc);

        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'companies',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó Informe de Clientes',
        ]);

        $this->page_construct('reports/customer_report', $meta, $this->data);

    }

    function suppliers()
    {
        $this->sma->checkPermissions('suppliers');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('suppliers_report')));
        $meta = array('page_title' => lang('suppliers_report'), 'bc' => $bc);
        $this->page_construct('reports/suppliers', $meta, $this->data);
    }

    function getSuppliers($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('suppliers', TRUE);

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('companies') . ".id as id, company, name, phone, email, count({$this->db->dbprefix('purchases')}.id) as total, COALESCE(sum(grand_total), 0) as total_amount, COALESCE(sum(paid), 0) as paid, ( COALESCE(sum(grand_total), 0) - COALESCE(sum(paid), 0)) as balance", FALSE)
                ->from("companies")
                ->join('purchases', 'purchases.supplier_id=companies.id')
                ->where('companies.group_name', 'supplier')
                ->order_by('companies.company asc')
                ->group_by('companies.id');

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('suppliers_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('company'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('phone'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('email'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('total_purchases'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('total_amount'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('balance'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->company);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->phone);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->email);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->formatDecimal($data_row->total));
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatDecimal($data_row->total_amount));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatDecimal($data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $this->sma->formatDecimal($data_row->balance));
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'suppliers_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $p = "( SELECT supplier_id, count(" . $this->db->dbprefix('purchases') . ".id) as total, COALESCE(sum(grand_total), 0) as total_amount, COALESCE(sum(paid), 0) as paid, ( COALESCE(sum(grand_total), 0) - COALESCE(sum(paid), 0)) as balance from {$this->db->dbprefix('purchases')} GROUP BY {$this->db->dbprefix('purchases')}.supplier_id ) FP";

            $this->load->library('datatables');
            $this->datatables
                ->select($this->db->dbprefix('companies') . ".id as id, company, name, phone, email, FP.total, FP.total_amount, FP.paid, FP.balance", FALSE)
                ->from("companies")
                ->join($p, 'FP.supplier_id=companies.id')
                ->where('(companies.group_name = "supplier" or companies.group_name = "creditor")')
                ->group_by('companies.id')
                ->add_column("Actions", "<div class='text-center'><a class=\"tip\" title='" . lang("view_report") . "' href='" . admin_url('reports/supplier_report/$1') . "'><span class='label label-primary'>" . lang("view_report") . "</span></a></div>", "id")
                ->unset_column('id');
            echo $this->datatables->generate();

        }

    }

    function supplier_report($user_id = NULL)
    {
        $this->sma->checkPermissions('suppliers', TRUE);
        if (!$user_id) {
            $this->session->set_flashdata('error', lang("no_supplier_selected"));
            admin_redirect('reports/suppliers');
        }

        $this->data['purchases'] = $this->reports_model->getPurchasesTotals($user_id);
        $this->data['total_purchases'] = $this->reports_model->getSupplierPurchases($user_id);
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['supplier'] = $this->site->getCompanyByID($user_id);
        $this->data['user_id'] = $user_id;

        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'companies',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó Informe de Proveedores',
        ]);


        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('suppliers_report')));
        $meta = array('page_title' => lang('suppliers_report'), 'bc' => $bc);
        $this->page_construct('reports/supplier_report', $meta, $this->data);

    }

    function users()
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('staff_report')));
        $meta = array('page_title' => lang('staff_report'), 'bc' => $bc);
        $this->page_construct('reports/users', $meta, $this->data);
    }

    function getUsers()
    {
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('users').".id as id, first_name, last_name, email, company, ".$this->db->dbprefix('groups').".name, active")
            ->from("users")
            ->join('groups', 'users.group_id=groups.id', 'left')
            ->group_by('users.id')
            ->where('company_id', NULL);
        if (!$this->Owner) {
            $this->datatables->where('group_id !=', 1);
        }
        $this->datatables
            ->edit_column('active', '$1__$2', 'active, id')
            ->add_column("Actions", "<div class='text-center'><a class=\"tip\" title='" . lang("view_report") . "' href='" . admin_url('reports/staff_report/$1') . "'><span class='label label-primary'>" . lang("view_report") . "</span></a></div>", "id")
            ->unset_column('id');
        echo $this->datatables->generate();
    }

    function staff_report($user_id = NULL, $year = NULL, $month = NULL, $pdf = NULL, $cal = 0)
    {

        if (!$user_id) {
            $this->session->set_flashdata('error', lang("no_user_selected"));
            admin_redirect('reports/users');
        }
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['purchases'] = $this->reports_model->getStaffPurchases($user_id);
        $this->data['sales'] = $this->reports_model->getStaffSales($user_id);
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['warehouses'] = $this->site->getAllWarehouses();

        if (!$year) {
            $year = date('Y');
        }
        if (!$month || $month == '#monthly-con') {
            $month = date('m');
        }
        if ($pdf) {
            if ($cal) {
                $this->monthly_sales($year, $pdf, $user_id);
            } else {
                $this->daily_sales($year, $month, $pdf, $user_id);
            }
        }
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => admin_url('reports/staff_report/'.$user_id),
            'month_type' => 'long',
            'day_type' => 'long'
        );

        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'users',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó Informe de Usuarios',
        ]);

        $config['template'] = '{table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable reports-table">{/table_open}
        {heading_row_start}<tr>{/heading_row_start}
        {heading_previous_cell}<th class="text-center"><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th class="text-center" colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th class="text-center"><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $sales = $this->reports_model->getStaffDailySales($user_id, $year, $month);

        if (!empty($sales)) {
            foreach ($sales as $sale) {
                $daily_sale[$sale->date] = "<table class='table table-bordered table-hover table-condensed data' style='margin:0;'><tr><td>" . lang("discount") . "</td><td>" . $this->sma->formatMoney($sale->discount) . "</td></tr><tr><td>" . lang("product_tax") . "</td><td>" . $this->sma->formatMoney($sale->tax1) . "</td></tr><tr><td>" . lang("order_tax") . "</td><td>" . $this->sma->formatMoney($sale->tax2) . "</td></tr><tr><td>" . lang("total") . "</td><td>" . $this->sma->formatMoney($sale->total) . "</td></tr></table>";
            }
        } else {
            $daily_sale = array();
        }
        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_sale);
        if ($this->input->get('pdf')) {

        }
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $this->data['msales'] = $this->reports_model->getStaffMonthlySales($user_id, $year);
        $this->data['user_id'] = $user_id;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('staff_report')));
        $meta = array('page_title' => lang('staff_report'), 'bc' => $bc);
        $this->page_construct('reports/staff_report', $meta, $this->data);

    }

    function getUserLogins($id = NULL, $pdf = NULL, $xls = NULL)
    {
        if ($this->input->get('start_date')) {
            $login_start_date = $this->input->get('start_date');
        } else {
            $login_start_date = NULL;
        }
        if ($this->input->get('end_date')) {
            $login_end_date = $this->input->get('end_date');
        } else {
            $login_end_date = NULL;
        }
        if ($login_start_date) {
            $login_start_date = $this->sma->fld($login_start_date);
            $login_end_date = $login_end_date ? $this->sma->fld($login_end_date) : date('Y-m-d H:i:s');
        }
        if ($pdf || $xls) {

            $this->db
                ->select("login, ip_address, time")
                ->from("user_logins")
                ->where('user_id', $id)
                ->order_by('time desc');
            if ($login_start_date) {
                $this->db->where("time BETWEEN '{$login_start_date}' and '{$login_end_date}'", NULL, FALSE);
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('staff_login_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('email'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('ip_address'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('time'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->login);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->ip_address);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->hrld($data_row->time));
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:C' . $row)->getAlignment()->setWrapText(true);
                $filename = 'staff_login_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("login, ip_address, DATE_FORMAT(time, '%Y-%m-%d %T') as time")
                ->from("user_logins")
                ->where('user_id', $id);
            if ($login_start_date) {
                $this->datatables->where("time BETWEEN '{$login_start_date}' and '{$login_end_date}'", NULL, FALSE);
            }
            echo $this->datatables->generate();

        }

    }

    function getCustomerLogins($id = NULL)
    {
        if ($this->input->get('login_start_date')) {
            $login_start_date = $this->input->get('login_start_date');
        } else {
            $login_start_date = NULL;
        }
        if ($this->input->get('login_end_date')) {
            $login_end_date = $this->input->get('login_end_date');
        } else {
            $login_end_date = NULL;
        }
        if ($login_start_date) {
            $login_start_date = $this->sma->fld($login_start_date);
            $login_end_date = $login_end_date ? $this->sma->fld($login_end_date) : date('Y-m-d H:i:s');
        }
        $this->load->library('datatables');
        $this->datatables
            ->select("login, ip_address, time")
            ->from("user_logins")
            ->where('customer_id', $id);
        if ($login_start_date) {
            $this->datatables->where('time BETWEEN "' . $login_start_date . '" and "' . $login_end_date . '"');
        }
        echo $this->datatables->generate();
    }

    function profit_loss($start_date = NULL, $end_date = NULL)
    {
        $this->sma->checkPermissions('profit_loss');
        if (!$start_date) {
            $start = $this->db->escape(date('Y-m') . '-1');
            $start_date = date('Y-m') . '-1';
        } else {
            $start = $this->db->escape(urldecode($start_date));
        }
        if (!$end_date) {
            $end = $this->db->escape(date('Y-m-d H:i'));
            $end_date = date('Y-m-d H:i');
        } else {
            $end = $this->db->escape(urldecode($end_date));
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');

        $this->data['total_purchases'] = $this->reports_model->getTotalPurchases($start, $end);
        $this->data['total_sales'] = $this->reports_model->getTotalSales($start, $end);
        $this->data['total_return_sales'] = $this->reports_model->getTotalReturnSales($start, $end);
        $this->data['total_expenses'] = $this->reports_model->getTotalExpenses($start, $end);
        $this->data['total_paid'] = $this->reports_model->getTotalPaidAmount($start, $end);
        $this->data['total_received'] = $this->reports_model->getTotalReceivedAmount($start, $end);
        $this->data['total_received_cash'] = $this->reports_model->getTotalReceivedCashAmount($start, $end);
        $this->data['total_received_cc'] = $this->reports_model->getTotalReceivedCCAmount($start, $end);
        $this->data['total_received_cheque'] = $this->reports_model->getTotalReceivedChequeAmount($start, $end);
        $this->data['total_received_ppp'] = $this->reports_model->getTotalReceivedPPPAmount($start, $end);
        $this->data['total_received_stripe'] = $this->reports_model->getTotalReceivedStripeAmount($start, $end);
        $this->data['total_returned'] = $this->reports_model->getTotalReturnedAmount($start, $end);
        $this->data['start'] = urldecode($start_date);
        $this->data['end'] = urldecode($end_date);

        $warehouses = $this->site->getAllWarehouses();
        foreach ($warehouses as $warehouse) {
            $total_purchases = $this->reports_model->getTotalPurchases($start, $end, $warehouse->id);
            $total_sales = $this->reports_model->getTotalSales($start, $end, $warehouse->id);
            $total_returns = $this->reports_model->getTotalReturnSales($start, $end, $warehouse->id);
            $total_expenses = $this->reports_model->getTotalExpenses($start, $end, $warehouse->id);
            $warehouses_report[] = array(
                'warehouse' => $warehouse,
                'total_purchases' => $total_purchases,
                'total_sales' => $total_sales,
                'total_returns' => $total_returns,
                'total_expenses' => $total_expenses,
                );
        }
        $this->data['warehouses_report'] = $warehouses_report;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('profit_loss')));
        $meta = array('page_title' => lang('profit_loss'), 'bc' => $bc);
        $this->page_construct('reports/profit_loss', $meta, $this->data);
    }

    function profit_loss_pdf($start_date = NULL, $end_date = NULL)
    {
        $this->sma->checkPermissions('profit_loss');
        if (!$start_date) {
            $start = $this->db->escape(date('Y-m') . '-1');
            $start_date = date('Y-m') . '-1';
        } else {
            $start = $this->db->escape(urldecode($start_date));
        }
        if (!$end_date) {
            $end = $this->db->escape(date('Y-m-d H:i'));
            $end_date = date('Y-m-d H:i');
        } else {
            $end = $this->db->escape(urldecode($end_date));
        }

        $this->data['total_purchases'] = $this->reports_model->getTotalPurchases($start, $end);
        $this->data['total_sales'] = $this->reports_model->getTotalSales($start, $end);
        $this->data['total_expenses'] = $this->reports_model->getTotalExpenses($start, $end);
        $this->data['total_paid'] = $this->reports_model->getTotalPaidAmount($start, $end);
        $this->data['total_received'] = $this->reports_model->getTotalReceivedAmount($start, $end);
        $this->data['total_received_cash'] = $this->reports_model->getTotalReceivedCashAmount($start, $end);
        $this->data['total_received_cc'] = $this->reports_model->getTotalReceivedCCAmount($start, $end);
        $this->data['total_received_cheque'] = $this->reports_model->getTotalReceivedChequeAmount($start, $end);
        $this->data['total_received_ppp'] = $this->reports_model->getTotalReceivedPPPAmount($start, $end);
        $this->data['total_received_stripe'] = $this->reports_model->getTotalReceivedStripeAmount($start, $end);
        $this->data['total_returned'] = $this->reports_model->getTotalReturnedAmount($start, $end);
        $this->data['start'] = urldecode($start_date);
        $this->data['end'] = urldecode($end_date);

        $warehouses = $this->site->getAllWarehouses();
        foreach ($warehouses as $warehouse) {
            $total_purchases = $this->reports_model->getTotalPurchases($start, $end, $warehouse->id);
            $total_sales = $this->reports_model->getTotalSales($start, $end, $warehouse->id);
            $warehouses_report[] = array(
                'warehouse' => $warehouse,
                'total_purchases' => $total_purchases,
                'total_sales' => $total_sales,
                );
        }
        $this->data['warehouses_report'] = $warehouses_report;

        $html = $this->load_view($this->theme . 'reports/profit_loss_pdf', $this->data, true);
        $name = lang("profit_loss") . "-" . str_replace(array('-', ' ', ':'), '_', $this->data['start']) . "-" . str_replace(array('-', ' ', ':'), '_', $this->data['end']) . ".pdf";
        $this->sma->generate_pdf($html, $name, false, false, false, false, false, 'L');
    }

    function register()
    {
        $this->sma->checkPermissions('register');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('register_report')));
        $meta = array('page_title' => lang('register_report'), 'bc' => $bc);
        $this->page_construct('reports/register', $meta, $this->data);
    }

    function getRrgisterlogs($pdf = NULL, $xls = NULL)
    {
        if ($this->input->get('user')) {
            $user = $this->input->get('user');
        } else {
            $user = NULL;
        }
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, closed_at, CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name, ' (', users.email, ')') as user, cash_in_hand, total_cc_slips, total_cheques, total_cash, total_cc_slips_submitted, total_cheques_submitted,total_cash_submitted, note, pos_register.id", FALSE)
                ->from("pos_register")
                ->join('users', 'users.id=pos_register.user_id', 'left')
                ->order_by('date desc');
            //->where('status', 'close');

            if ($user) {
                $this->db->where('pos_register.user_id', $user);
            }
            if ($start_date) {
                $this->db->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('register_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('open_time'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('close_time'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('user'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('cash_in_hand'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('cc_slips'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('cheques'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('total_cash'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('cc_slips_submitted'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('cheques_submitted'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('total_cash_submitted'));
                $this->excel->getActiveSheet()->SetCellValue('K1', lang('note'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->closed_at);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->user);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->cash_in_hand);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->total_cc_slips);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->total_cheques);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->total_cash);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->total_cc_slips_submitted);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->total_cheques_submitted);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->total_cash_submitted);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, $data_row->note);
                    if($data_row->total_cash_submitted < $data_row->total_cash || $data_row->total_cheques_submitted < $data_row->total_cheques || $data_row->total_cc_slips_submitted < $data_row->total_cc_slips) {
                        $this->excel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray(
                                array( 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'F2DEDE')) )
                                );
                    }
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(35);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'register_report';
                $this->load->helper('excel');
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('close_register_detailed'),
                ]);
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("
                            date,
                            closed_at,
                            CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name, '<br>', " . $this->db->dbprefix('users') . ".email) as user,
                            cash_in_hand,
                            CONCAT(total_cc_slips, ' (', total_cc_slips_submitted, ')'),
                            CONCAT(total_cheques, ' (', total_cheques_submitted, ')'),
                            CONCAT(total_cash, ' (', total_cash_submitted, ')'),
                            note,
                            " . $this->db->dbprefix('pos_register') . ".id as id,
                            " . $this->db->dbprefix('pos_register') . ".status as status
                            ",
                        FALSE)
                ->from("pos_register")
                ->join('users', 'users.id=pos_register.user_id', 'left')
                ->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs new-button new-button-sm dropdown-toggle\" data-toggle=\"dropdown\">
                                                <i class='fas fa-ellipsis-v fa-lg'></i>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li>
                                                    <a href='" . admin_url('pos/close_register/0/$1/1') . "' data-toggle='modal' data-target='#myModal' class='tip' title=''><i class=\"fa fa-rotate\"></i> " . lang('sync_closed_register') . "</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>", "id");

            if ($user) {
                $this->datatables->where('pos_register.user_id', $user);
            }
            if ($start_date) {
                $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $this->datatables->order_by('date DESC');

            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'sales',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('close_register_detailed'),
            ]);

        }

    }

    public function expenses($id = null)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['categories'] = $this->reports_model->getExpenseCategories();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('expenses')));
        $meta = array('page_title' => lang('expenses'), 'bc' => $bc);
        $this->page_construct('reports/expenses', $meta, $this->data);
    }

    public function getExpensesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('expenses');

        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $note = $this->input->get('note') ? $this->input->get('note') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $document_type_id = $this->input->get('document_type_id') ? $this->input->get('document_type_id') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
            ->from('expenses')
            ->join('users', 'users.id=expenses.created_by', 'left')
            ->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
            ->group_by('expenses.id');

            if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                $this->db->where('created_by', $this->session->userdata('user_id'));
            }

            if ($note) {
                $this->db->like('note', $note, 'both');
            }
            if ($reference_no) {
                $this->db->like('reference', $reference_no, 'both');
            }
            if ($category) {
                $this->db->where('category_id', $category);
            }
            // if ($warehouse) {
            //     $this->db->where('expenses.warehouse_id', $warehouse);
            // }
            if ($user) {
                $this->db->where('created_by', $user);
            }
            if ($start_date) {
                $this->db->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('expenses_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('category'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('amount'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('note'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('created_by'));

                $row = 2; $total = 0;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->category);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->amount);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->note);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->created_by);
                    $total += $data_row->amount;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("D" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $total);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'expenses_report';
                $this->load->helper('excel');
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'expenses',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('expenses_report'),
                ]);
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {

            $this->load->library('datatables');
            $this->datatables
            ->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference, {$this->db->dbprefix('expense_categories')}.name as category, amount, note, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user, attachment, {$this->db->dbprefix('expenses')}.id as id", false)
            ->from('expenses')
            ->join('users', 'users.id=expenses.created_by', 'left')
            ->join('expense_categories', 'expense_categories.id=expenses.category_id', 'left')
            ->group_by('expenses.id');

            if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                $this->datatables->where('created_by', $this->session->userdata('user_id'));
            }

            if ($note) {
                $this->datatables->like('note', $note, 'both');
            }
            if ($reference_no) {
                $this->datatables->like('reference', $reference_no, 'both');
            }
            if ($category) {
                $this->datatables->where('category_id', $category);
            }
            if ($warehouse) {
                $this->datatables->where('expenses.warehouse_id', $warehouse);
            }
            if ($user) {
                $this->datatables->where('created_by', $user);
            }
            if ($start_date) {
                $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
            if ($biller) {
                $this->datatables->where('expenses.biller_id', $biller);
            }
            if ($document_type_id) {
                $this->datatables->where('expenses.document_type_id', $document_type_id);
            }

            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'expenses',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('expenses_report'),
            ]);
        }
    }

    function daily_purchases($warehouse_id = NULL, $year = NULL, $month = NULL, $pdf = NULL, $user_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$month) {
            $month = date('m');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $config = array(
            'show_next_prev' => TRUE,
            'next_prev_url' => admin_url('reports/daily_purchases/'.($warehouse_id ? $warehouse_id : 0)),
            'month_type' => 'long',
            'day_type' => 'long'
        );

        $config['template'] = '
        {table_open}<div class="table-responsive"><table border="0" cellpadding="0" cellspacing="0" class="table table-bordered dfTable">{/table_open}
        {heading_row_start}<thead><tr>{/heading_row_start}
        {heading_previous_cell}<th><a href="{previous_url}">&lt;&lt;</a></th>{/heading_previous_cell}
        {heading_title_cell}<th colspan="{colspan}" id="month_year">{heading}</th>{/heading_title_cell}
        {heading_next_cell}<th><a href="{next_url}">&gt;&gt;</a></th>{/heading_next_cell}
        {heading_row_end}</tr></thead>{/heading_row_end}
        {week_row_start}<tr>{/week_row_start}
        {week_day_cell}<td class="cl_wday">{week_day}</td>{/week_day_cell}
        {week_row_end}</tr>{/week_row_end}
        {cal_row_start}<tr class="days">{/cal_row_start}
        {cal_cell_start}<td class="day">{/cal_cell_start}
        {cal_cell_content}
        <div class="day_num">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content}
        {cal_cell_content_today}
        <div class="day_num highlight">{day}</div>
        <div class="content">{content}</div>
        {/cal_cell_content_today}
        {cal_cell_no_content}<div class="day_num">{day}</div>{/cal_cell_no_content}
        {cal_cell_no_content_today}<div class="day_num highlight">{day}</div>{/cal_cell_no_content_today}
        {cal_cell_blank}&nbsp;{/cal_cell_blank}
        {cal_cell_end}</td>{/cal_cell_end}
        {cal_row_end}</tr>{/cal_row_end}
        {table_close}</table></div>{/table_close}';

        $this->load->library('calendar', $config);
        $purchases = $user_id ? $this->reports_model->getStaffDailyPurchases($user_id, $year, $month, $warehouse_id) : $this->reports_model->getDailyPurchases($year, $month, $warehouse_id);

        if (!empty($purchases)) {
            foreach ($purchases as $purchase) {
                $daily_purchase[$purchase->date] = "<table class='table table-bordered table-hover table-condensed data' style='margin:0;'><tr><td>" . lang("discount") . "</td><td>" . $this->sma->formatMoney($purchase->discount) . "</td></tr><tr><td>" . lang("shipping") . "</td><td>" . $this->sma->formatMoney($purchase->shipping) . "</td></tr><tr><td>" . lang("product_tax") . "</td><td>" . $this->sma->formatMoney($purchase->tax1) . "</td></tr><tr><td>" . lang("order_tax") . "</td><td>" . $this->sma->formatMoney($purchase->tax2) . "</td></tr><tr><td>" . lang("total") . "</td><td>" . $this->sma->formatMoney($purchase->total) . "</td></tr></table>";
            }
        } else {
            $daily_purchase = array();
        }

        $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('daily_purchases'),
            ]);

        $this->data['calender'] = $this->calendar->generate($year, $month, $daily_purchase);
        $this->data['year'] = $year;
        $this->data['month'] = $month;
        if ($pdf) {
            $html = $this->load_view($this->theme . 'reports/daily', $this->data, true);
            $name = lang("daily_purchases") . "_" . $year . "_" . $month . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('daily_purchases_report')));
        $meta = array('page_title' => lang('daily_purchases_report'), 'bc' => $bc);
        $this->page_construct('reports/daily_purchases', $meta, $this->data);

    }


    function monthly_purchases($warehouse_id = NULL, $year = NULL, $pdf = NULL, $user_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->Owner && !$this->Admin && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$year) {
            $year = date('Y');
        }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user_id = $this->session->userdata('user_id');
        }
        $this->load->language('calendar');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['year'] = $year;
        $this->data['purchases'] = $user_id ? $this->reports_model->getStaffMonthlyPurchases($user_id, $year, $warehouse_id) : $this->reports_model->getMonthlyPurchases($year, $warehouse_id);
        if ($pdf) {
            $html = $this->load_view($this->theme . 'reports/monthly', $this->data, true);
            $name = lang("monthly_purchases") . "_" . $year . ".pdf";
            $html = str_replace('<p class="introtext">' . lang("reports_calendar_text") . '</p>', '', $html);
            $this->sma->generate_pdf($html, $name, null, null, null, null, null, 'L');
        }

        $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('monthly_purchases'),
            ]);

        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['sel_warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('monthly_purchases_report')));
        $meta = array('page_title' => lang('monthly_purchases_report'), 'bc' => $bc);
        $this->page_construct('reports/monthly_purchases', $meta, $this->data);

    }

    function adjustments($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('products');

        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('adjustments_report')));
        $meta = array('page_title' => lang('adjustments_report'), 'bc' => $bc);
        $this->page_construct('reports/adjustments', $meta, $this->data);
    }

    function getAdjustmentReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $document_type_id = $this->input->get('document_type_id') ? $this->input->get('document_type_id') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }



        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }
            if ($pdf || $xls) {
                $ai = "(
                            SELECT
                                    {$this->db->dbprefix('adjustment_items')}.adjustment_id,
                                    {$this->db->dbprefix('adjustment_items')}.product_id,
                                    option_id,
                                    serial_no,
                                    {$this->db->dbprefix('products')}.code as item_code,
                                    {$this->db->dbprefix('products')}.name as item_nane,
                                    {$this->db->dbprefix('products')}.tax_rate,
                                    C.name as category_name,
                                    subcategories.name as subcategory_name,
                                    B.name as brand_name,
                                    PV.code as variant_code,
                                    PV.name as variant_name,
                                    (CASE
                                        WHEN {$this->db->dbprefix('adjustment_items')}.type  = 'subtraction'
                                        THEN (0-{$this->db->dbprefix('adjustment_items')}.quantity)
                                        ELSE {$this->db->dbprefix('adjustment_items')}.quantity
                                    END) as pquantity,
                                    {$this->db->dbprefix('adjustment_items')}.avg_cost
                            FROM {$this->db->dbprefix('adjustment_items')}
                                LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id={$this->db->dbprefix('adjustment_items')}.product_id
                                LEFT JOIN {$this->db->dbprefix('categories')} C ON C.id = {$this->db->dbprefix('products')}.category_id
                                LEFT JOIN {$this->db->dbprefix('categories')} AS subcategories ON subcategories.id = {$this->db->dbprefix('products')}.subcategory_id
                                LEFT JOIN {$this->db->dbprefix('brands')} B ON B.id = {$this->db->dbprefix('products')}.brand
                                LEFT JOIN {$this->db->dbprefix('product_variants')} PV ON PV.id = {$this->db->dbprefix('adjustment_items')}.option_id
                                ";
            } else {
                $ai = "(
                            SELECT
                                {$this->db->dbprefix('adjustment_items')}.adjustment_id,
                                {$this->db->dbprefix('adjustment_items')}.product_id,
                                option_id,
                                serial_no,
                                GROUP_CONCAT( {$this->db->dbprefix('products')}.name SEPARATOR '___') as item_nane,
                                GROUP_CONCAT(
                                    CASE
                                        WHEN {$this->db->dbprefix('adjustment_items')}.type  = 'subtraction'
                                        THEN (0-{$this->db->dbprefix('adjustment_items')}.quantity)
                                        ELSE {$this->db->dbprefix('adjustment_items')}.quantity
                                    END SEPARATOR '___') as pquantity
                            FROM {$this->db->dbprefix('adjustment_items')}
                                LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id={$this->db->dbprefix('adjustment_items')}.product_id
                                LEFT JOIN {$this->db->dbprefix('categories')} C ON C.id = {$this->db->dbprefix('products')}.category_id
                                LEFT JOIN {$this->db->dbprefix('categories')} AS subcategories ON subcategories.id = {$this->db->dbprefix('products')}.subcategory_id
                                LEFT JOIN {$this->db->dbprefix('brands')} B ON B.id = {$this->db->dbprefix('products')}.brand
                                LEFT JOIN {$this->db->dbprefix('product_variants')} PV ON PV.id = {$this->db->dbprefix('adjustment_items')}.option_id
                                GROUP BY {$this->db->dbprefix('adjustment_items')}.adjustment_id";

            }

            if ($product || $serial) { $ai .= " WHERE "; }
            if ($product) {
                $ai .= " {$this->db->dbprefix('adjustment_items')}.product_id = {$product} ";
            }
            if ($product && $serial) { $ai .= " AND "; }
            if ($serial) {
                $ai .= " {$this->db->dbprefix('adjustment_items')}.serial_no LIKe '%{$serial}%' ";
            }
            $ai .= ") FAI";
            $this->load->library('datatables');

            if ($pdf || $xls) {
                $this->datatables
                ->select("

                            {$this->db->dbprefix('adjustments')}.*,
                            DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                            reference_no,
                            warehouses.name as wh_name,
                            CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by,
                            adjustments.note,
                            FAI.item_code as icode,
                            FAI.item_nane as iname,
                            FAI.tax_rate AS tax_rate_id,
                            FAI.pquantity,
                            FAI.avg_cost,
                            {$this->db->dbprefix('adjustments')}.id as id,
                            biller.company as biller_name,
                            FAI.category_name as category_name,
                            FAI.subcategory_name as subcategory_name,
                            FAI.brand_name as brand_name,
                            FAI.variant_code as variant_code,
                            FAI.variant_name as variant_name,
                            tax_rates.rate as tax_rate_rate
                        ", FALSE);
            } else {
                $this->datatables
                ->select("
                            DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                            reference_no,
                            warehouses.name as wh_name,
                            CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by,
                            adjustments.note,
                            FAI.item_nane as iname,
                            FAI.pquantity,
                            {$this->db->dbprefix('adjustments')}.id as id
                        ", FALSE);
            }
            $this->datatables
            ->from('adjustments')
            ->join($ai, 'FAI.adjustment_id=adjustments.id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left')
            ->join('companies as biller', 'biller.id=adjustments.biller_id', 'left')
            ->join('products', 'products.id=FAI.product_id', 'left')
            ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
            ;

            if ($biller) {
                $this->datatables->where('adjustments.biller_id', $biller);
            }
            if ($user) {
                $this->datatables->where('adjustments.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FAI.product_id', $product);
            }
            if ($serial) {
                $this->datatables->like('FAI.serial_no', $serial);
            }
            if ($warehouse) {
                $this->datatables->where('adjustments.warehouse_id', $warehouse);
            }
            if ($document_type_id) {
                $this->datatables->where('adjustments.document_type_id', $document_type_id);
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('adjustments').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
            if ($pdf || $xls) {
                $data = $this->datatables->get_display_result_2();
                // exit(var_dump($data));
                if (!empty($data)) {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('adjustments_report'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('time'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('origin'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('warehouse'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('category'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('subcategory'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('brand'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('product'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('variant_code'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('variant'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('product_qty'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('unit_cost_before_tax'));
                    $this->excel->getActiveSheet()->SetCellValue('P1', lang('tax_rate'));
                    $this->excel->getActiveSheet()->SetCellValue('Q1', lang('unit_tax'));
                    $this->excel->getActiveSheet()->SetCellValue('R1', lang('unit_cost_with_tax'));
                    $this->excel->getActiveSheet()->SetCellValue('S1', lang('created_by'));
                    $this->excel->getActiveSheet()->SetCellValue('T1', lang('note'));
                    $row = 2;
                    foreach ($data as $data_row) {
                        $d_arr = explode(" ", $data_row->date);
                        $fecha = $d_arr[0];
                        $hora = $d_arr[1];
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, $fecha);
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, $hora);
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $data_row->reference_no);


                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, $data_row->origin_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $data_row->biller_name);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $data_row->wh_name);

                        $this->excel->getActiveSheet()->SetCellValue('G'.$row, $data_row->category_name);
                        $this->excel->getActiveSheet()->SetCellValue('H'.$row, $data_row->subcategory_name);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, $data_row->brand_name);
                        $this->excel->getActiveSheet()->SetCellValue('J'.$row, $data_row->icode);
                        $this->excel->getActiveSheet()->SetCellValue('K'.$row, $data_row->iname);
                        $this->excel->getActiveSheet()->SetCellValue('L'.$row, $data_row->variant_code);
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $data_row->variant_name);
                        $this->excel->getActiveSheet()->SetCellValue('N'.$row, $data_row->pquantity);
                        $avg_cost_tax_val = $this->sma->calculate_tax($data_row->tax_rate_id, $data_row->avg_cost, 0);
                        $this->excel->getActiveSheet()->SetCellValue('O'.$row, $this->sma->formatDecimal($data_row->avg_cost - $avg_cost_tax_val));
                        $this->excel->getActiveSheet()->SetCellValue('P'.$row, $this->sma->formatQuantity($data_row->tax_rate_rate)."%");
                        $this->excel->getActiveSheet()->SetCellValue('Q'.$row, $this->sma->formatDecimal($avg_cost_tax_val));
                        $this->excel->getActiveSheet()->SetCellValue('R'.$row, $this->sma->formatDecimal($data_row->avg_cost));
                        $this->excel->getActiveSheet()->SetCellValue('S'.$row, $data_row->created_by);
                        $this->excel->getActiveSheet()->SetCellValue('T'.$row, $this->sma->decode_html($data_row->note));
                        $row++;
                    }
                    for ($i='A'; $i <= 'Z'; $i++) {
                        $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                    }
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                    $range = 'J1:J'.$row;
                    $this->excel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                    $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                    $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                    $this->excel->getActiveSheet()->getStyle('G2:G' . $row)->getAlignment()->setWrapText(true);
                    $filename = 'adjustments_report';
                    $this->load->helper('excel');

                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 6,
                        'table_name' => 'adjustments',
                        'record_id' => null,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => $this->m,
                        'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('adjustments_report'),
                    ]);

                    create_excel($this->excel, $filename);
                }
            } else {
                echo $this->datatables->generate();

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 4,
                    'table_name' => 'adjustments',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('adjustments_report'),
                ]);
            }
    }

    function get_deposits($company_id = NULL)
    {
        $this->sma->checkPermissions('customers', TRUE);
        $this->load->library('datatables');
        $this->datatables
            ->select("date, amount, paid_by, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note", FALSE)
            ->from("deposits")
            ->join('users', 'users.id=deposits.created_by', 'left')
            ->where($this->db->dbprefix('deposits').'.company_id', $company_id);
        echo $this->datatables->generate();
    }

    function tax()
    {
        $this->sma->checkPermissions();

        $taxRate = $this->input->post('taxRate') ? $this->input->post('taxRate') : NULL;
        $biller = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $warehouse = $this->input->post('warehouse') ? $this->input->post('warehouse') : NULL;
        $sale_document_type = $this->input->post('sale_document_type') ? $this->input->post('sale_document_type') : NULL;
        $purchase_document_type = $this->input->post('purchase_document_type') ? $this->input->post('purchase_document_type') : NULL;
        if ($this->input->post('start_date')) {
             $start_date = $this->input->post('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['customers'] = $this->site->getAllCompanies('customer');
        $this->data['suppliers'] = $this->site->getAllCompanies('supplier');
        $this->data['sale_document_types'] = $this->site->get_multi_module_document_types([1, 2, 3, 4]);
        $this->data['purchase_document_types'] = $this->site->get_multi_module_document_types([5, 6, 21, 22, 35]);
        $this->data['taxRates'] = $this->site->getAllTaxRates();
        $this->data['sale_ipoconsumo_detail'] = $this->reports_model->getSalesIpoconsumoDetail($start_date, $end_date, $taxRate, $biller, $warehouse, $sale_document_type);
        $this->data['purchase_ipoconsumo_detail'] = $this->reports_model->getPurchasesIpoconsumoDetail($start_date, $end_date, $taxRate, $biller, $warehouse, $purchase_document_type);

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('tax_report')));
        $meta = array('page_title' => lang('tax_report'), 'bc' => $bc);
        $this->page_construct('reports/tax', $meta, $this->data);
    }

    function get_sale_taxes($xls = NULL)
    {
        $this->sma->checkPermissions('tax', TRUE);
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $taxRate = $this->input->get('taxRate') ? $this->input->get('taxRate') : NULL;
        $sale_document_type = $this->input->get('sale_document_type') ? $this->input->get('sale_document_type') : NULL;
        $totales = $this->input->get('totales') ? $this->input->get('totales') : NULL;

        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = ($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01');
            }
        }
        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = ($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d');
            }
        }

        if ($start_date) {
            $start_date .= " 00:00:00";
        }

        if ($end_date) {
            $end_date .= " 23:59:00";
        }

        $this->load->library('datatables');
        if ($taxRate) {

          $this->datatables
          ->select("  DATE({$this->db->dbprefix('sales')}.date) AS date,
                        {$this->db->dbprefix('sales')}.reference_no,
                        {$this->db->dbprefix('sales')}.return_sale_ref,
                        {$this->db->dbprefix('sales')}.sale_status,
                        {$this->db->dbprefix('sales')}.payment_method,
                        CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse,
                        biller,
                        customer,
                        sum({$this->db->dbprefix('sale_items')}.net_unit_price * {$this->db->dbprefix('sale_items')}.quantity) as total,
                        sum({$this->db->dbprefix('sale_items')}.item_tax) as product_tax,
                        sum({$this->db->dbprefix('sales')}.order_discount) as order_discount,
                        {$this->db->dbprefix('sales')}.rete_fuente_total,
                        {$this->db->dbprefix('sales')}.rete_iva_total,
                        {$this->db->dbprefix('sales')}.rete_ica_total,
                        {$this->db->dbprefix('sales')}.rete_other_total,
                        sum({$this->db->dbprefix('sale_items')}.subtotal) as grand_total,
                        {$this->db->dbprefix('sales')}.id as id,
                        {$this->db->dbprefix('sales')}.rete_fuente_percentage,
                        {$this->db->dbprefix('sales')}.rete_iva_percentage,
                        {$this->db->dbprefix('sales')}.rete_ica_percentage,
                        {$this->db->dbprefix('sales')}.rete_other_percentage,
                        {$this->db->dbprefix('companies')}.vat_no,
                        {$this->db->dbprefix('addresses')}.direccion,
                        {$this->db->dbprefix('addresses')}.city,
                        {$this->db->dbprefix('addresses')}.state,
                        {$this->db->dbprefix('sales')}.order_tax,
                        {$this->db->dbprefix('sales')}.shipping,
                        {$this->db->dbprefix('sales')}.total_discount
                        ", FALSE )
          ->from('sale_items')
          ->join('sales', 'sales.id = sale_items.sale_id', 'left')
          ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
          ->join('companies', 'companies.id=sales.customer_id', 'left')
          ->join('addresses', 'addresses.id=sales.address_id', 'left')
          ->join('tax_rates', 'tax_rates.id=sale_items.tax_rate_id', 'left');
          $this->datatables->where('sale_items.tax_rate_id', $taxRate);
          if ($biller) {
            $this->datatables->where('sales.biller_id', $biller);
          }
          if ($customer) {
            $this->datatables->where('sales.customer_id', $customer);
          }
          if ($warehouse) {
            $this->datatables->where('sales.warehouse_id', $warehouse);
          }
          if ($sale_document_type) {
            $this->datatables->where('sales.document_type_id', $sale_document_type);
          }
          if ($start_date) {
            $this->datatables->where('sales.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
          }
          if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                    $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
                } else {
                    $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
                }
            }
          $this->datatables->group_by("{$this->db->dbprefix('sale_items')}.sale_id");

        }else{
            $xls_sql = "";
            $this->datatables
              ->select("DATE({$this->db->dbprefix('sales')}.date) as date,
                        {$this->db->dbprefix('sales')}.reference_no,
                        {$this->db->dbprefix('sales')}.return_sale_ref,
                        {$this->db->dbprefix('sales')}.sale_status,
                        {$this->db->dbprefix('sales')}.payment_method,
                        CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse,
                        {$this->db->dbprefix('sales')}.biller,
                        {$this->db->dbprefix('sales')}.customer,
                        {$this->db->dbprefix('sales')}.total,
                        {$this->db->dbprefix('sales')}.product_tax,
                        {$this->db->dbprefix('sales')}.order_discount,
                        {$this->db->dbprefix('sales')}.rete_fuente_total,
                        {$this->db->dbprefix('sales')}.rete_iva_total,
                        {$this->db->dbprefix('sales')}.rete_ica_total,
                        {$this->db->dbprefix('sales')}.rete_other_total,
                        {$this->db->dbprefix('sales')}.grand_total,
                        {$this->db->dbprefix('sales')}.id as id,
                        {$this->db->dbprefix('sales')}.rete_fuente_percentage,
                        {$this->db->dbprefix('sales')}.rete_iva_percentage,
                        {$this->db->dbprefix('sales')}.rete_ica_percentage,
                        {$this->db->dbprefix('sales')}.rete_other_percentage,
                        {$this->db->dbprefix('companies')}.vat_no,
                        {$this->db->dbprefix('companies')}.first_name,
                        {$this->db->dbprefix('companies')}.second_name,
                        {$this->db->dbprefix('companies')}.first_lastname,
                        {$this->db->dbprefix('companies')}.second_lastname,
                        {$this->db->dbprefix('addresses')}.direccion,
                        {$this->db->dbprefix('addresses')}.city,
                        {$this->db->dbprefix('addresses')}.state,
                        {$this->db->dbprefix('sales')}.order_tax,
                        {$this->db->dbprefix('sales')}.shipping,
                        {$this->db->dbprefix('sales')}.tip_amount,
                        {$this->db->dbprefix('sales')}.total_discount,
                        {$this->db->dbprefix('types_person')}.description as type_person
                        ", FALSE)
              ->from('sales')
              ->join('companies', 'companies.id=sales.customer_id', 'left')
              ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
              ->join('types_person', 'types_person.id=companies.type_person', 'left')
              ->join('addresses', 'addresses.id=sales.address_id', 'left')
              ;
          if ($biller) {
            $this->datatables->where('biller_id', $biller);
          }
          if ($customer) {
            $this->datatables->where('customer_id', $customer);
          }
          if ($warehouse) {
            $this->datatables->where('warehouse_id', $warehouse);
          }
          if ($sale_document_type) {
            $this->datatables->where('document_type_id', $sale_document_type);
          }
          if ($start_date) {
            $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
          }
          if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
                if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                    $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
                } else {
                    $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
                }
            }
        }

        if ($xls) {

            $data = $this->datatables->get_display_result_2();
            $detail_tax_data = $this->get_xls_tax_inv_detail($data, 1);
            $tax_detail = $detail_tax_data['tax_detail'];

            $data_tax_detail = isset($detail_tax_data['data_tax_detail']['normal']) ? $detail_tax_data['data_tax_detail']['normal'] : NULL;
            $data_retention_detail = isset($detail_tax_data['data_retention_detail']['normal']) ? $detail_tax_data['data_retention_detail']['normal'] : NULL;
            $retention_detail = isset($detail_tax_data['retention_detail']['normal']) ? $detail_tax_data['retention_detail']['normal'] : NULL;
            $data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['normal']) ? $detail_tax_data['data_tax_detail_totals']['normal'] : NULL;

            $return_data_tax_detail = isset($detail_tax_data['data_tax_detail']['return']) ? $detail_tax_data['data_tax_detail']['return'] : NULL;
            $return_data_retention_detail = isset($detail_tax_data['data_retention_detail']['return']) ? $detail_tax_data['data_retention_detail']['return'] : NULL;
            $return_retention_detail = isset($detail_tax_data['retention_detail']['return']) ? $detail_tax_data['retention_detail']['return'] : NULL;
            $return_data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['return']) ? $detail_tax_data['data_tax_detail_totals']['return'] : NULL;

            if (!empty($data)) {
                if ($xls == 'xls') {
                    // $this->sma->print_arrays($data_retention_detail);
                      $this->load->library('excel');
                      $this->excel->setActiveSheetIndex(0);
                      $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                      $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                      $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                      $this->excel->getActiveSheet()->SetCellValue('C1', lang('affects_to'));
                      $this->excel->getActiveSheet()->SetCellValue('D1', lang('status'));
                      $this->excel->getActiveSheet()->SetCellValue('E1', lang('payment_method'));
                      $this->excel->getActiveSheet()->SetCellValue('F1', lang('warehouse'));
                      $this->excel->getActiveSheet()->SetCellValue('G1', lang('biller'));
                      $this->excel->getActiveSheet()->SetCellValue('H1', lang('customer_name'));
                      $this->excel->getActiveSheet()->SetCellValue('I1', lang('type_person'));
                      $this->excel->getActiveSheet()->SetCellValue('J1', 'Primer Nombre');
                      $this->excel->getActiveSheet()->SetCellValue('K1', 'Segundo Nombre');
                      $this->excel->getActiveSheet()->SetCellValue('L1', 'Primer Apellido');
                      $this->excel->getActiveSheet()->SetCellValue('M1', 'Segundo Apellido');
                      $this->excel->getActiveSheet()->SetCellValue('N1', lang('vat_no'));
                      $this->excel->getActiveSheet()->SetCellValue('O1', lang('address'));
                      $this->excel->getActiveSheet()->SetCellValue('P1', lang('city'));
                      $this->excel->getActiveSheet()->SetCellValue('Q1', lang('state'));
                      $this->excel->getActiveSheet()->SetCellValue('R1', lang('shipping'));
                      $this->excel->getActiveSheet()->SetCellValue('S1', lang('tip'));
                      $this->excel->getActiveSheet()->SetCellValue('T1', lang('total'));
                      $this->excel->getActiveSheet()->SetCellValue('U1', lang('sub_total'));
                      $this->excel->getActiveSheet()->SetCellValue('V1', lang('order_discount'));
                      $letra = 'T';
                        foreach ($tax_detail as $tax_rate_id => $tax_name) {
                            $this->excel->getActiveSheet()->SetCellValue($letra . '1', $tax_name." base");
                            $letra++;
                            $this->excel->getActiveSheet()->SetCellValue($letra . '1', $tax_name);
                            $letra++;
                        }
                        if (isset($retention_detail['fuente'])) {
                            foreach ($retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete fuente ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['fuente'])) {
                            foreach ($return_retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete fuente Dev ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($retention_detail['iva'])) {
                            foreach ($retention_detail['iva'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete iva ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['iva'])) {
                            foreach ($return_retention_detail['iva'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete iva Dev ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($retention_detail['ica'])) {
                            foreach ($retention_detail['ica'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete ica ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['ica'])) {
                            foreach ($return_retention_detail['ica'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete ica Dev ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($retention_detail['other'])) {
                            foreach ($retention_detail['other'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete other ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['other'])) {
                            foreach ($return_retention_detail['other'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete other Dev ".$retention_percentage);
                                $letra++;
                            }
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('total_to_pay'));

                      $row = 2;
                      $total = 0;
                      $sub_total = 0;
                      $order_discount = 0;
                      foreach ($data as $data_row) {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrsd($data_row->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->return_sale_ref);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, lang($data_row->sale_status));
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, lang($data_row->payment_method));
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->warehouse);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->biller);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->customer);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->type_person);

                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->first_name);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $data_row->second_name);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $data_row->first_lastname);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $data_row->second_lastname);

                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $data_row->vat_no);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $data_row->direccion);
                        $this->excel->getActiveSheet()->SetCellValue('P' . $row, $data_row->city);
                        $this->excel->getActiveSheet()->SetCellValue('Q' . $row, $data_row->state);
                        $this->excel->getActiveSheet()->SetCellValue('R' . $row, $data_row->shipping);
                        $this->excel->getActiveSheet()->SetCellValue('S' . $row, $data_row->tip_amount);
                        $this->excel->getActiveSheet()->SetCellValue('T' . $row, $data_row->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('U' . $row, $data_row->total);
                        $this->excel->getActiveSheet()->SetCellValue('V' . $row, $data_row->order_discount);
                        $letra = 'T';
                        foreach ($tax_detail as $tax_rate_id => $tax_name) {
                            if (isset($data_tax_detail[$data_row->id][$tax_rate_id])) {
                                $sale_tax_detail = $data_tax_detail[$data_row->id][$tax_rate_id];
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_tax_detail['tax_base']);
                                $letra++;
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_tax_detail['tax_amount']);
                                $letra++;
                            } else if (isset($return_data_tax_detail[$data_row->id][$tax_rate_id])) {
                                $sale_tax_detail = $return_data_tax_detail[$data_row->id][$tax_rate_id];
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_tax_detail['tax_base']);
                                $letra++;
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_tax_detail['tax_amount']);
                                $letra++;
                            } else {
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                $letra++;
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                $letra++;
                            }
                        }
                        if (isset($retention_detail['fuente'])) {
                            foreach ($retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['fuente'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $data_retention_detail['fuente'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        if (isset($return_retention_detail['fuente'])) {
                            foreach ($return_retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                if (isset($return_data_retention_detail['fuente'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $return_data_retention_detail['fuente'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        if (isset($retention_detail['iva'])) {
                            foreach ($retention_detail['iva'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['iva'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $data_retention_detail['iva'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        if (isset($return_retention_detail['iva'])) {
                            foreach ($return_retention_detail['iva'] as $retention_id => $retention_percentage) {
                                if (isset($return_data_retention_detail['iva'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $return_data_retention_detail['iva'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        if (isset($retention_detail['ica'])) {
                            foreach ($retention_detail['ica'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['ica'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $data_retention_detail['ica'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        if (isset($return_retention_detail['ica'])) {
                            foreach ($return_retention_detail['ica'] as $retention_id => $retention_percentage) {
                                if (isset($return_data_retention_detail['ica'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $return_data_retention_detail['ica'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        if (isset($retention_detail['other'])) {
                            foreach ($retention_detail['other'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['other'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $data_retention_detail['other'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        if (isset($return_retention_detail['other'])) {
                            foreach ($return_retention_detail['other'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['other'][$data_row->id][$retention_percentage])) {
                                    $sale_retention_detail = $data_retention_detail['other'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $sale_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }
                        $total_inv_retention = $data_row->rete_fuente_total + $data_row->rete_iva_total + $data_row->rete_ica_total + $data_row->rete_other_total;
                        $this->excel->getActiveSheet()->SetCellValue($letra . $row, $data_row->grand_total - ($data_row->grand_total > 0 ? $total_inv_retention : $total_inv_retention * -1));

                        $total += $data_row->grand_total;
                        $sub_total += $data_row->total;
                        $order_discount += $data_row->order_discount;
                        $row++;
                      }
                      $this->excel->getActiveSheet()->getStyle("M" . $row . ":O" . $row)->getBorders()
                      ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                      $this->excel->getActiveSheet()->SetCellValue('M' . $row, $this->sma->formatDecimal($total));
                      $this->excel->getActiveSheet()->SetCellValue('N' . $row, $this->sma->formatDecimal($sub_total));
                      $this->excel->getActiveSheet()->SetCellValue('O' . $row, $this->sma->formatDecimal($order_discount));
                      $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
                      $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
                      $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
                      $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                      $this->excel->getActiveSheet()->getStyle('A1:Z' . $row)->getAlignment()->setWrapText(true);
                } else if ($xls == 'xls_2') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('tax_name'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('net_price'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('tax'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('total'));

                    $table_total_base = 0;
                    $table_total_amount = 0;

                    $row = 2;
                    // $this->sma->print_arrays($data_tax_detail_totals);
                    if ($tax_detail) {
                        foreach ($tax_detail as $tax_rate_id => $tax_name) {
                            $base = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                            $amount = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                            $return_base = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                            $return_amount = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                            $table_total_base += ($base + $return_base);
                            $table_total_amount += ($amount + $return_amount);


                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, $tax_name);
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney($base));
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($amount));
                            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($base + $amount));

                            $row++;

                            if ($return_base != 0 || $return_amount != 0) {
                                $this->excel->getActiveSheet()->SetCellValue('A'.$row, $tax_name." Dev ");
                                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney($return_base));
                                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($return_amount));
                                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($return_base + $return_amount));
                                $row++;
                            }
                        }
                    }

                    if (isset($data_tax_detail_totals['ico'])) {
                        $table_total_amount += $data_tax_detail_totals['ico']['amount'];
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Impoconsumo');
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney(0));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($data_tax_detail_totals['ico']['amount']));
                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($data_tax_detail_totals['ico']['amount']));

                        $row++;
                    }

                    if (isset($return_data_tax_detail_totals['ico'])) {
                        $table_total_amount += $return_data_tax_detail_totals['ico']['amount'];
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Impoconsumo Dev ');
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney(0));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($return_data_tax_detail_totals['ico']['amount']));
                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($return_data_tax_detail_totals['ico']['amount']));

                        $row++;
                    }


                    $subtotal = 0;
                    $product_tax = 0;
                    $order_tax = 0;
                    $shipping = 0;
                    $total_discount = 0;
                    $total_sales = 0;
                    $order_discount = 0;
                    foreach ($data as $data_row) {
                        $subtotal += $data_row->total;
                        $product_tax += $data_row->product_tax;
                        $order_tax += $data_row->order_tax;
                        $shipping += $data_row->shipping;
                        $total_discount += $data_row->total_discount;
                        $total_sales += $data_row->grand_total;
                        $order_discount += $data_row->order_discount;
                    }

                    if ($order_discount > 0) {
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('order_discount'));
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney(0));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney(0));
                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($order_discount * -1));

                        $row++;
                    }


                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Total');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney($table_total_base));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($table_total_amount));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($table_total_base + $table_total_amount - $order_discount));
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                }
              $filename = 'sale_tax_report';
              // exit();
              $this->load->helper('excel');
              $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales_purchases',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('tax_report').' en ventas',
                ]);
              create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            if ($totales) {
                $data = $this->datatables->get_display_result_2();
                $detail_tax_data = $this->get_xls_tax_inv_detail($data, 1);
                $tax_detail = $detail_tax_data['tax_detail'];
                $data_tax_detail = isset($detail_tax_data['data_tax_detail']['normal']) ? $detail_tax_data['data_tax_detail']['normal'] : NULL;
                $data_retention_detail = isset($detail_tax_data['data_retention_detail']['normal']) ? $detail_tax_data['data_retention_detail']['normal'] : NULL;
                $retention_detail = isset($detail_tax_data['retention_detail']['normal']) ? $detail_tax_data['retention_detail']['normal'] : NULL;
                $data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['normal']) ? $detail_tax_data['data_tax_detail_totals']['normal'] : NULL;


                $return_data_tax_detail = isset($detail_tax_data['data_tax_detail']['return']) ? $detail_tax_data['data_tax_detail']['return'] : NULL;
                $return_data_retention_detail = isset($detail_tax_data['data_retention_detail']['return']) ? $detail_tax_data['data_retention_detail']['return'] : NULL;
                $return_retention_detail = isset($detail_tax_data['retention_detail']['return']) ? $detail_tax_data['retention_detail']['return'] : NULL;
                $return_data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['return']) ? $detail_tax_data['data_tax_detail_totals']['return'] : NULL;

                $table_html = "";

                $table_total_base = 0;
                $table_total_amount = 0;

                if ($tax_detail) {
                    foreach ($tax_detail as $tax_rate_id => $tax_name) {
                        $base = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                        $amount = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                        $return_base = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                        $return_amount = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                        $table_total_base += ($base + $return_base);
                        $table_total_amount += ($amount + $return_amount);

                        $table_html .= "<tr>
                                            <td>".$tax_name."</td>
                                            <td class='text-right'>".$this->sma->formatMoney($base)."</td>
                                            <td class='text-right'>".$this->sma->formatMoney($amount)."</td>
                                            <td class='text-right'>".$this->sma->formatMoney($base + $amount)."</td>
                                        </tr>";
                        if ($return_base != 0 || $return_amount != 0) {
                            $table_html .= "<tr>
                                                <td>".$tax_name." Devoluciones</td>
                                                <td class='text-right'>".$this->sma->formatMoney($return_base)."</td>
                                                <td class='text-right'>".$this->sma->formatMoney($return_amount)."</td>
                                                <td class='text-right'>".$this->sma->formatMoney($return_base + $return_amount)."</td>
                                            </tr>";
                        }
                    }
                }

                // if (isset($data_tax_detail_totals['ico'])) {
                //     $table_html .= "<tr>
                //                         <td>Impoconsumo</td>
                //                         <td class='text-right'>".$this->sma->formatMoney(0)."</td>
                //                         <td class='text-right'>".$this->sma->formatMoney($data_tax_detail_totals['ico']['amount'])."</td>
                //                         <td class='text-right'>".$this->sma->formatMoney($data_tax_detail_totals['ico']['amount'])."</td>
                //                     </tr>";
                //     $table_total_amount += $data_tax_detail_totals['ico']['amount'];
                // }

                // if (isset($return_data_tax_detail_totals['ico'])) {
                //     $table_html .= "<tr>
                //                         <td>Impoconsumo Devoluciones</td>
                //                         <td class='text-right'>".$this->sma->formatMoney(0)."</td>
                //                         <td class='text-right'>".$this->sma->formatMoney($return_data_tax_detail_totals['ico']['amount'])."</td>
                //                         <td class='text-right'>".$this->sma->formatMoney($return_data_tax_detail_totals['ico']['amount'])."</td>
                //                     </tr>";
                //     $table_total_amount += $return_data_tax_detail_totals['ico']['amount'];
                // }


                $subtotal = 0;
                $product_tax = 0;
                $order_tax = 0;
                $shipping = 0;
                $total_discount = 0;
                $total_sales = 0;
                $order_discount = 0;
                foreach ($data as $data_row) {
                    $subtotal += $data_row->total;
                    $product_tax += $data_row->product_tax;
                    $order_tax += $data_row->order_tax;
                    $shipping += $data_row->shipping;
                    $total_discount += $data_row->total_discount;
                    $total_sales += $data_row->grand_total;
                    $order_discount += $data_row->order_discount;
                }

                if ($order_discount > 0) {
                    $table_html .= "<tr>
                                    <td>".lang('order_discount')."</td>
                                    <td class='text-right'></td>
                                    <td class='text-right'></td>
                                    <td class='text-right'>".$this->sma->formatMoney($order_discount * -1)."</td>
                                </tr>";
                }

                $table_html .= "<tr>
                                    <td>Total</td>
                                    <td class='text-right text_sales_taxes_base'>".$this->sma->formatMoney($table_total_base)."</td>
                                    <td class='text-right text_sales_taxes_amount'>".$this->sma->formatMoney($table_total_amount)."</td>
                                    <td class='text-right text_sales_taxes_total'>".$this->sma->formatMoney($table_total_base + $table_total_amount - $order_discount)."</td>
                                </tr>";

                echo json_encode([
                                    'subtotal' => $subtotal,
                                    'product_tax' => $product_tax,
                                    'order_tax' => $order_tax,
                                    'shipping' => $shipping,
                                    'total_discount' => $total_discount,
                                    'total_sales' => $total_sales,
                                    'table_html' => $table_html,
                                ]);
            } else {
                echo $this->datatables->generate();


                if (!$totales) {
                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 4,
                        'table_name' => 'sales_purchases',
                        'record_id' => null,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => $this->m,
                        'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('tax_report'),
                    ]);
                }

            }
        }
    }

    function get_purchase_taxes($xls = NULL)
    {
        $this->sma->checkPermissions('tax', TRUE);
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $taxRate = $this->input->get('taxRate') ? $this->input->get('taxRate') : NULL;
        $purchase_document_type = $this->input->get('purchase_document_type') ? $this->input->get('purchase_document_type') : NULL;
        $totales = $this->input->get('totales') ? $this->input->get('totales') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = ($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = ($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d');
            }
        }

        if ($start_date) {
            $start_date .= " 00:00:00";
        }

        if ($end_date) {
            $end_date .= " 23:59:00";
        }

        $this->load->library('datatables');
        if ($taxRate) {

            $this->datatables
            ->select("
                        DATE({$this->db->dbprefix('purchases')}.date) AS date,
                        {$this->db->dbprefix('purchases')}.reference_no,
                        {$this->db->dbprefix('purchases')}.return_purchase_ref,
                        {$this->db->dbprefix('purchases')}.status,
                        CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse,
                        supplier,
                        sum({$this->db->dbprefix('purchase_items')}.net_unit_cost * {$this->db->dbprefix('purchase_items')}.quantity) as total,
                        sum({$this->db->dbprefix('purchase_items')}.item_tax) as product_tax,
                        sum({$this->db->dbprefix('purchases')}.order_discount) as order_discount,
                        sum({$this->db->dbprefix('purchases')}.rete_fuente_total) as rete_fuente_total,
                        sum({$this->db->dbprefix('purchases')}.rete_iva_total) as rete_iva_total,
                        sum({$this->db->dbprefix('purchases')}.rete_ica_total) as rete_ica_total,
                        sum({$this->db->dbprefix('purchases')}.rete_other_total) as rete_other_total,
                        sum({$this->db->dbprefix('purchase_items')}.unit_cost * {$this->db->dbprefix('purchase_items')}.quantity)  as grand_total,
                        {$this->db->dbprefix('purchases')}.id as id,
                        {$this->db->dbprefix('purchases')}.rete_fuente_percentage,
                        {$this->db->dbprefix('purchases')}.rete_iva_percentage,
                        {$this->db->dbprefix('purchases')}.rete_ica_percentage,
                        {$this->db->dbprefix('purchases')}.rete_other_percentage,
                        {$this->db->dbprefix('companies')}.vat_no,
                        {$this->db->dbprefix('companies')}.address,
                        {$this->db->dbprefix('companies')}.city,
                        {$this->db->dbprefix('companies')}.state,
                        {$this->db->dbprefix('purchases')}.order_tax,
                        {$this->db->dbprefix('purchases')}.shipping,
                        {$this->db->dbprefix('purchases')}.total_discount,
                        biller.name as biller_name
                    ", FALSE )
            ->from('purchase_items')
            ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'left')
            ->join('companies', 'companies.id = purchases.supplier_id', 'left')
            ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
            ->join('tax_rates', 'tax_rates.id=purchase_items.tax_rate_id', 'left')
            ->join('companies biller', 'biller.id=purchases.biller_id', 'left');
            $this->datatables->where('purchase_items.tax_rate_id', $taxRate);
            if ($biller) {
              $this->datatables->where('purchases.biller_id', $biller);
            }
            if ($supplier) {
              $this->datatables->where('purchases.supplier_id', $supplier);
            }
            if ($warehouse) {
              $this->datatables->where('purchases.warehouse_id', $warehouse);
            }
            if ($purchase_document_type) {
              $this->datatables->where('purchases.document_type_id', $purchase_document_type);
            }
            if ($start_date) {
              $this->datatables->where('purchases.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
            $this->datatables->group_by("{$this->db->dbprefix('purchase_items')}.purchase_id");
          } else {
            $this->datatables
            ->select("
                        DATE({$this->db->dbprefix('purchases')}.date) as date,
                        {$this->db->dbprefix('purchases')}.reference_no,
                        {$this->db->dbprefix('purchases')}.return_purchase_ref,
                        {$this->db->dbprefix('purchases')}.status,
                        CONCAT({$this->db->dbprefix('warehouses')}.name, ' (', {$this->db->dbprefix('warehouses')}.code, ')') as warehouse,
                        {$this->db->dbprefix('purchases')}.supplier,
                        {$this->db->dbprefix('purchases')}.total,
                        {$this->db->dbprefix('purchases')}.product_tax,
                        {$this->db->dbprefix('purchases')}.order_discount,
                        {$this->db->dbprefix('purchases')}.rete_fuente_total,
                        {$this->db->dbprefix('purchases')}.rete_iva_total,
                        {$this->db->dbprefix('purchases')}.rete_ica_total,
                        {$this->db->dbprefix('purchases')}.rete_other_total,
                        {$this->db->dbprefix('purchases')}.grand_total,
                        {$this->db->dbprefix('purchases')}.id as id,
                        {$this->db->dbprefix('purchases')}.rete_fuente_percentage,
                        {$this->db->dbprefix('purchases')}.rete_iva_percentage,
                        {$this->db->dbprefix('purchases')}.rete_ica_percentage,
                        {$this->db->dbprefix('purchases')}.rete_other_percentage,
                        {$this->db->dbprefix('companies')}.vat_no,
                        {$this->db->dbprefix('companies')}.address,
                        {$this->db->dbprefix('companies')}.city,
                        {$this->db->dbprefix('companies')}.state,
                        biller.name as biller_name,
                        {$this->db->dbprefix('purchases')}.order_tax,
                        {$this->db->dbprefix('purchases')}.shipping,
                        {$this->db->dbprefix('purchases')}.total_discount
                    ", FALSE)
            ->from('purchases')
            ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
            ->join('companies', 'companies.id=purchases.supplier_id', 'left')
            ->join('companies biller', 'biller.id=purchases.biller_id', 'left')
            ;
            if ($supplier) {
              $this->datatables->where('supplier_id', $supplier);
            }
            if ($warehouse) {
              $this->datatables->where('warehouse_id', $warehouse);
            }
            if ($purchase_document_type) {
              $this->datatables->where('document_type_id', $purchase_document_type);
            }
            if ($biller) {
              $this->datatables->where('purchases.biller_id', $biller);
            }
            if ($supplier) {
              $this->datatables->where('purchases.supplier_id', $supplier);
            }
            if ($start_date) {
              $this->datatables->where('date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
          }

        if ($xls) {

            $data = $this->datatables->get_display_result_2();
            $detail_tax_data = $this->get_xls_tax_inv_detail($data, 2);
            $tax_detail = $detail_tax_data['tax_detail'];

            $data_tax_detail = isset($detail_tax_data['data_tax_detail']['normal']) ? $detail_tax_data['data_tax_detail']['normal'] : NULL;
            $data_retention_detail = isset($detail_tax_data['data_retention_detail']['normal']) ? $detail_tax_data['data_retention_detail']['normal'] : NULL;
            $retention_detail = isset($detail_tax_data['retention_detail']['normal']) ? $detail_tax_data['retention_detail']['normal'] : NULL;
            $data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['normal']) ? $detail_tax_data['data_tax_detail_totals']['normal'] : NULL;

            $return_data_tax_detail = isset($detail_tax_data['data_tax_detail']['return']) ? $detail_tax_data['data_tax_detail']['return'] : NULL;
            $return_data_retention_detail = isset($detail_tax_data['data_retention_detail']['return']) ? $detail_tax_data['data_retention_detail']['return'] : NULL;
            $return_retention_detail = isset($detail_tax_data['retention_detail']['return']) ? $detail_tax_data['retention_detail']['return'] : NULL;
            $return_data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['return']) ? $detail_tax_data['data_tax_detail_totals']['return'] : NULL;
            if ($xls == 'xls') {
                if (!empty($data)) {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('purchases_report'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('affects_to'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('status'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('warehouse'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('supplier'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('vat_no')); //new
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('address')); //new
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('city')); //new
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('state')); //new
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('total'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('sub_total'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('order_discount'));
                      $letra = 'O';
                        foreach ($tax_detail as $tax_rate_id => $tax_name) {
                            $this->excel->getActiveSheet()->SetCellValue($letra . '1', $tax_name." base");
                            $letra++;
                            $this->excel->getActiveSheet()->SetCellValue($letra . '1', $tax_name);
                            $letra++;
                        }
                        if (isset($retention_detail['fuente'])) {
                            foreach ($retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete fuente ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['fuente'])) {
                            foreach ($return_retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete fuente Dev ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($retention_detail['iva'])) {
                            foreach ($retention_detail['iva'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete iva ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['iva'])) {
                            foreach ($return_retention_detail['iva'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete iva Dev ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($retention_detail['ica'])) {
                            foreach ($retention_detail['ica'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete ica ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['ica'])) {
                            foreach ($return_retention_detail['ica'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete ica Dev ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($retention_detail['other'])) {
                            foreach ($retention_detail['other'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete other ".$retention_percentage);
                                $letra++;
                            }
                        }

                        if (isset($return_retention_detail['other'])) {
                            foreach ($return_retention_detail['other'] as $retention_id => $retention_percentage) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . '1', "Rete other Dev ".$retention_percentage);
                                $letra++;
                            }
                        }
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('total_to_pay'));
                    $row = 2;
                    foreach ($data as $data_row) {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrsd($data_row->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->return_purchase_ref);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->status);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->warehouse);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->biller_name);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->supplier);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->vat_no);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->address);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->city);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $data_row->state);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $data_row->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $data_row->total);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $data_row->order_discount);
                        $letra = 'O';
                        foreach ($tax_detail as $tax_rate_id => $tax_name) {
                            if (isset($data_tax_detail[$data_row->id][$tax_rate_id])) {
                                $purchase_tax_detail = $data_tax_detail[$data_row->id][$tax_rate_id];
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_tax_detail['tax_base']);
                                $letra++;
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_tax_detail['tax_amount']);
                                $letra++;
                            } else if (isset($return_data_tax_detail[$data_row->id][$tax_rate_id])) {
                                $purchase_tax_detail = $return_data_tax_detail[$data_row->id][$tax_rate_id];
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_tax_detail['tax_base']);
                                $letra++;
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_tax_detail['tax_amount']);
                                $letra++;
                            } else {
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                $letra++;
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                $letra++;
                            }
                        }

                        if (isset($retention_detail['fuente'])) {
                            foreach ($retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['fuente'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $data_retention_detail['fuente'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }

                        if (isset($return_retention_detail['fuente'])) {
                            foreach ($return_retention_detail['fuente'] as $retention_id => $retention_percentage) {
                                if (isset($return_data_retention_detail['fuente'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $return_data_retention_detail['fuente'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }

                        if (isset($retention_detail['iva'])) {
                            foreach ($retention_detail['iva'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['iva'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $data_retention_detail['iva'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }

                        if (isset($return_retention_detail['iva'])) {
                            foreach ($return_retention_detail['iva'] as $retention_id => $retention_percentage) {
                                if (isset($return_data_retention_detail['iva'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $return_data_retention_detail['iva'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }

                        if (isset($retention_detail['ica'])) {
                            foreach ($retention_detail['ica'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['ica'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $data_retention_detail['ica'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }

                        if (isset($return_retention_detail['ica'])) {
                            foreach ($return_retention_detail['ica'] as $retention_id => $retention_percentage) {
                                if (isset($return_data_retention_detail['ica'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $return_data_retention_detail['ica'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }

                        if (isset($retention_detail['other'])) {
                            foreach ($retention_detail['other'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['other'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $data_retention_detail['other'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }

                        if (isset($return_retention_detail['other'])) {
                            foreach ($return_retention_detail['other'] as $retention_id => $retention_percentage) {
                                if (isset($data_retention_detail['other'][$data_row->id][$retention_percentage])) {
                                    $purchase_retention_detail = $data_retention_detail['other'][$data_row->id][$retention_percentage];
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, $purchase_retention_detail);
                                    $letra++;
                                } else {
                                    $this->excel->getActiveSheet()->SetCellValue($letra . $row, "0");
                                    $letra++;
                                }
                            }
                        }


                        $total_inv_retention = $data_row->rete_fuente_total + $data_row->rete_iva_total + $data_row->rete_ica_total + $data_row->rete_other_total;
                        $this->excel->getActiveSheet()->SetCellValue($letra . $row, $data_row->grand_total - ($data_row->grand_total > 0 ? $total_inv_retention : $total_inv_retention * -1));
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getStyle("E" . $row . ":K" . $row)->getBorders()
                        ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('Z')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                    $filename = 'purchase_tax_report';
                }
            } else if ($xls == 'xls_2') {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('purchases_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('tax_name'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('net_price'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('tax'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('total'));

                $table_total_base = 0;
                $table_total_amount = 0;

                $row = 2;
                // $this->sma->print_arrays($data_tax_detail_totals);
                if ($tax_detail) {
                    foreach ($tax_detail as $tax_rate_id => $tax_name) {
                        $base = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                        $amount = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                        $return_base = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                        $return_amount = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                        $table_total_base += ($base + $return_base);
                        $table_total_amount += ($amount + $return_amount);


                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, $tax_name);
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney($base));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($amount));
                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($base + $amount));

                        $row++;

                        if ($return_base != 0 || $return_amount != 0) {
                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, $tax_name." Dev ");
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney($return_base));
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($return_amount));
                            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($return_base + $return_amount));
                            $row++;
                        }
                    }
                }

                if (isset($data_tax_detail_totals['ico'])) {
                    $table_total_amount += $data_tax_detail_totals['ico']['amount'];
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Impoconsumo');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney(0));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($data_tax_detail_totals['ico']['amount']));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($data_tax_detail_totals['ico']['amount']));

                    $row++;
                }

                if (isset($return_data_tax_detail_totals['ico'])) {
                    $table_total_amount += $return_data_tax_detail_totals['ico']['amount'];
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Impoconsumo Dev ');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney(0));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($return_data_tax_detail_totals['ico']['amount']));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($return_data_tax_detail_totals['ico']['amount']));

                    $row++;
                }


                $subtotal = 0;
                $product_tax = 0;
                $order_tax = 0;
                $shipping = 0;
                $total_discount = 0;
                $total_sales = 0;
                $order_discount = 0;
                foreach ($data as $data_row) {
                    $subtotal += $data_row->total;
                    $product_tax += $data_row->product_tax;
                    $order_tax += $data_row->order_tax;
                    $shipping += $data_row->shipping;
                    $total_discount += $data_row->total_discount;
                    $total_sales += $data_row->grand_total;
                    $order_discount += $data_row->order_discount;
                }

                if ($order_discount > 0) {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('order_discount'));
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney(0));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney(0));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($order_discount * -1));

                    $row++;
                }


                $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Total');
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $this->sma->formatMoney($table_total_base));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $this->sma->formatMoney($table_total_amount));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $this->sma->formatMoney($table_total_base + $table_total_amount - $order_discount));
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $filename = 'sale_tax_report';
                // exit();
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            $this->load->helper('excel');
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'sales_purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('tax_report').' en compras',
            ]);
            create_excel($this->excel, $filename);
        } else {
            if ($totales) {
                $data = $this->datatables->get_display_result_2();
                $detail_tax_data = $this->get_xls_tax_inv_detail($data, 2);
                $tax_detail = $detail_tax_data['tax_detail'];

                $data_tax_detail = isset($detail_tax_data['data_tax_detail']['normal']) ? $detail_tax_data['data_tax_detail']['normal'] : NULL;
                $data_retention_detail = isset($detail_tax_data['data_retention_detail']['normal']) ? $detail_tax_data['data_retention_detail']['normal'] : NULL;
                $retention_detail = isset($detail_tax_data['retention_detail']['normal']) ? $detail_tax_data['retention_detail']['normal'] : NULL;
                $data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['normal']) ? $detail_tax_data['data_tax_detail_totals']['normal'] : NULL;

                $return_data_tax_detail = isset($detail_tax_data['data_tax_detail']['return']) ? $detail_tax_data['data_tax_detail']['return'] : NULL;
                $return_data_retention_detail = isset($detail_tax_data['data_retention_detail']['return']) ? $detail_tax_data['data_retention_detail']['return'] : NULL;
                $return_retention_detail = isset($detail_tax_data['retention_detail']['return']) ? $detail_tax_data['retention_detail']['return'] : NULL;
                $return_data_tax_detail_totals = isset($detail_tax_data['data_tax_detail_totals']['return']) ? $detail_tax_data['data_tax_detail_totals']['return'] : NULL;

                $table_html = "";

                $table_total_base = 0;
                $table_total_amount = 0;
                if ($tax_detail) {
                    foreach ($tax_detail as $tax_rate_id => $tax_name) {
                        $base = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                        $amount = isset($data_tax_detail_totals[$tax_rate_id]) ? ($data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                        $return_base = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['base']) : 0;
                        $return_amount = isset($return_data_tax_detail_totals[$tax_rate_id]) ? ($return_data_tax_detail_totals[$tax_rate_id]['amount']) : 0;

                        $table_total_base += ($base + $return_base);
                        $table_total_amount += ($amount + $return_amount);

                        $table_html .= "<tr>
                                            <td>".$tax_name."</td>
                                            <td class='text-right'>".$this->sma->formatMoney($base)."</td>
                                            <td class='text-right'>".$this->sma->formatMoney($amount)."</td>
                                            <td class='text-right'>".$this->sma->formatMoney($base + $amount)."</td>
                                        </tr>";
                        if ($return_base != 0 || $return_amount != 0) {
                            $table_html .= "<tr>
                                                <td>".$tax_name." Devoluciones</td>
                                                <td class='text-right'>".$this->sma->formatMoney($return_base)."</td>
                                                <td class='text-right'>".$this->sma->formatMoney($return_amount)."</td>
                                                <td class='text-right'>".$this->sma->formatMoney($return_base + $return_amount)."</td>
                                            </tr>";
                        }

                    }
                }

                // if (isset($data_tax_detail_totals['ico'])) {
                //     $table_html .= "<tr>
                //                         <td>Impoconsumo</td>
                //                         <td class='text-right'>".$this->sma->formatMoney(0)."</td>
                //                         <td class='text-right'>".$this->sma->formatMoney($data_tax_detail_totals['ico']['amount'])."</td>
                //                         <td class='text-right'>".$this->sma->formatMoney($data_tax_detail_totals['ico']['amount'])."</td>
                //                     </tr>";
                //     $table_total_amount += $data_tax_detail_totals['ico']['amount'];
                // }



                $subtotal = 0;
                $product_tax = 0;
                $order_tax = 0;
                $shipping = 0;
                $total_discount = 0;
                $total_sales = 0;
                $order_discount = 0;
                foreach ($data as $data_row) {
                    $subtotal += $data_row->total;
                    $product_tax += $data_row->product_tax;
                    $order_tax += $data_row->order_tax;
                    $shipping += $data_row->shipping;
                    $total_discount += $data_row->total_discount;
                    $total_sales += $data_row->grand_total;
                    $order_discount += $data_row->order_discount;
                }

                if ($order_discount > 0) {
                    $table_html .= "<tr>
                                    <td>".lang('order_discount')."</td>
                                    <td class='text-right'></td>
                                    <td class='text-right'></td>
                                    <td class='text-right'>".$this->sma->formatMoney($order_discount * -1)."</td>
                                </tr>";
                }

                $table_html .= "<tr>
                                    <td>Total</td>
                                    <td class='text-right text_purchases_taxes_base'>".$this->sma->formatMoney($table_total_base)."</td>
                                    <td class='text-right text_purchases_taxes_amount'>".$this->sma->formatMoney($table_total_amount)."</td>
                                    <td class='text-right text_purchases_taxes_total'>".$this->sma->formatMoney($table_total_base + $table_total_amount - $order_discount)."</td>
                                </tr>";

                echo json_encode([
                                    'subtotal' => $subtotal,
                                    'product_tax' => $product_tax,
                                    'order_tax' => $order_tax,
                                    'shipping' => $shipping,
                                    'total_discount' => $total_discount,
                                    'total_sales' => $total_sales,
                                    'table_html' => $table_html,
                                ]);
            } else {
                echo $this->datatables->generate();
            }
        }
    }

    public function portfolio()
    {
        $this->sma->checkPermissions('sales');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['countries'] = $this->site->getCountries();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('portfolio_report')));
        $meta = array('page_title' => lang('portfolio_report'), 'bc' => $bc);
        $this->page_construct('reports/portfolio', $meta, $this->data);
    }

    public function portfolio_report_pdf(){
        $this->sma->checkPermissions('sales', TRUE);
        $biller = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $seller = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:59" : date('Y-m-d H:i:s');
        if ($biller) {
            $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $biller);
        } else {
            $this->data['biller'] = NULL;
        }
        $this->data['print_logo'] = $this->site->biller_company_logo($biller);
        $customer = $this->input->post('customer') ? $this->input->post('customer') : NULL;
        $address = $this->input->post('address') ? $this->input->post('address') : NULL;
        $country = $this->input->post('country') ? $this->input->post('country') : NULL;
        $state = $this->input->post('state') ? $this->input->post('state') : NULL;
        $city = $this->input->post('city') ? $this->input->post('city') : NULL;
        $view_customer_address_total = $this->input->post('view_customer_address_total') ? $this->input->post('view_customer_address_total') : 0;
        $view_customer_total = $this->input->post('view_customer_total') ? $this->input->post('view_customer_total') : 0;
        $view_city_total = $this->input->post('view_city_total') ? $this->input->post('view_city_total') : 0;
        $expiration_days_from = $this->input->post('expiration_days_from') ? $this->input->post('expiration_days_from') : NULL;
        $group_by_biller = $this->input->post('group_by_biller') != 1 ? $this->input->post('group_by_biller') : NULL;
        $group_by_seller = $this->input->post('group_by_seller') != 1 ? $this->input->post('group_by_seller') : NULL;
        $generate_in = $this->input->post('generate_in');
        $this->site->create_temporary_sales_payments($end_date);
        $this->db->distinct();
        $this->db->select("
                            biller.id as biller_id,
                            biller.name as biller_name,
                            biller.vat_no as biller_vat_no,
                            biller.logo as biller_logo,
                            biller.digito_verificacion as biller_digito_verificacion,
                            seller_c.id as seller_id,
                            seller_c.name as seller_name,
                            customer.id as customer_id,
                            customer.name as customer_name,
                            customer.vat_no as customer_vat_no,
                            customer.digito_verificacion as customer_digito_verificacion,
                            customer.phone as customer_phone,
                            customer.deposit_amount as customer_deposit_amount,
                            price_groups.name as price_group_name,
                            addresses.id as address_id,
                            addresses.sucursal as address_name,
                            addresses.direccion as address_direccion,
                            addresses.state as address_state,
                            addresses.city as address_city,
                            addresses.phone as address_phone,
                            {$this->db->dbprefix('sales')}.reference_no,
                            {$this->db->dbprefix('sales')}.rete_fuente_total,
                            {$this->db->dbprefix('sales')}.rete_iva_total,
                            {$this->db->dbprefix('sales')}.rete_ica_total,
                            {$this->db->dbprefix('sales')}.rete_other_total,
                            DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date,
                            DATE_FORMAT({$this->db->dbprefix('sales')}.due_date, '%Y-%m-%d %T') as due_date,
                            (
                                {$this->db->dbprefix('sales')}.total +
                                {$this->db->dbprefix('sales')}.total_discount
                            ) as grand_total,
                            (
                                ({$this->db->dbprefix('sales')}.total_discount / ({$this->db->dbprefix('sales')}.total + {$this->db->dbprefix('sales')}.total_discount)) * 100
                            ) as percentage_discount,
                            {$this->db->dbprefix('sales')}.total_discount,
                            (
                                {$this->db->dbprefix('sales')}.total
                            ) as sub_total,
                            (
                                COALESCE(IF(tbl_pmnt.sale_id IS NULL, 0, tbl_pmnt.total_paid), 0) -
                                COALESCE({$this->db->dbprefix('sales')}.rete_fuente_total, 0) -
                                COALESCE({$this->db->dbprefix('sales')}.rete_iva_total, 0) -
                                COALESCE({$this->db->dbprefix('sales')}.rete_ica_total, 0) -
                                COALESCE({$this->db->dbprefix('sales')}.rete_other_total, 0)
                                -
                                (
                                    ({$this->db->dbprefix('sales')}.return_sale_total * -1) -
                                    COALESCE(return_sale.rete_fuente_total, 0) -
                                    COALESCE(return_sale.rete_iva_total, 0) -
                                    COALESCE(return_sale.rete_ica_total, 0) -
                                    COALESCE(return_sale.rete_other_total, 0)
                                )
                            ) as paid,
                            (
                                ({$this->db->dbprefix('sales')}.return_sale_total * -1) -
                                COALESCE(return_sale.rete_fuente_total, 0) -
                                COALESCE(return_sale.rete_iva_total, 0) -
                                COALESCE(return_sale.rete_ica_total, 0) -
                                COALESCE(return_sale.rete_other_total, 0)
                            ) as  credit_note,
                            {$this->db->dbprefix('sales')}.total_tax,
                            DATE_FORMAT(DATE_ADD({$this->db->dbprefix('sales')}.date, INTERVAL {$this->db->dbprefix('sales')}.payment_term DAY), '%Y-%m-%d %T') as expiration_date,
                            {$this->db->dbprefix('sales')}.payment_term,
                            (
                                {$this->db->dbprefix('sales')}.grand_total -
                                COALESCE(IF(tbl_pmnt.sale_id IS NULL, 0, tbl_pmnt.total_paid), 0)
                            ) as balance,
                            {$this->db->dbprefix('sales')}.id as id", FALSE)
            ->from('sales')
            ->join("temp_sales_payments tbl_pmnt", "tbl_pmnt.sale_id = {$this->db->dbprefix('sales')}.id", "left")
            ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
            ->join($this->db->dbprefix('companies').' as seller_c', 'seller_c.id=sales.seller_id', 'left')
            ->join($this->db->dbprefix('companies').' as customer', 'customer.id=sales.customer_id', 'left')
            ->join($this->db->dbprefix('companies').' as biller', 'biller.id=sales.biller_id', 'left')
            ->join('price_groups', 'customer.price_group_id=price_groups.id', 'left')
            ->join($this->db->dbprefix('sales').' as return_sale', 'return_sale.sale_id=sales.id', 'left')
            ->join('addresses', 'addresses.id=sales.address_id', 'left');
        $this->db->where('sales.date <=', $end_date)
                 ->where('(sales.portfolio_end_date > "'.$end_date.'" OR sales.portfolio_end_date IS NULL)');
        if ($biller) {
            $this->db->where('sales.biller_id', $biller);
        }
        if ($seller) {
            $this->db->where('sales.seller_id', $seller);
        } else {
            $this->db->where('sales.seller_id !=', NULL);
        }
        if ($customer) {
            $this->db->where('sales.customer_id', $customer);
        }
        if ($address) {
            $this->db->where('sales.address_id', $address);
        }
        if ($country) {
            $this->db->where('addresses.country', $country);
        }
        if ($state) {
            $this->db->where('addresses.state', $state);
        }
        if ($city) {
            $this->db->where('addresses.city', $city);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->db->where('sales.created_by', $this->session->userdata('user_id'));
            }
        }
        $this->db->where('sales.sale_status !=', 'returned');
        $this->db->group_by('sales.id');
        $this->db->having('balance > 0.1');
        $grouping = "";
        if ($group_by_biller) {
            $grouping .= "biller.name asc, ";
        }
        if ($group_by_seller) {
            $grouping .= "seller_c.name asc, ";
        }
        $grouping .= "addresses.state asc, addresses.city asc, customer.name asc, sales.address_id asc, sales.date asc, sales.reference_no asc";
        $this->db->order_by($grouping);
        $q = $this->db->get();
        $this->site->delete_temporary_sales_payments($end_date);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
        } else {
            $data = NULL;
        }
        if (empty($data)) {
            $this->session->set_flashdata('error', lang('nothing_found'));
            $this->session->set_userdata('portfolio_nothing_found', 1);
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['data'] = $data;
        $this->data['settings'] = $this->Settings;
        $this->data['view_customer_address_total'] = $view_customer_address_total;
        $this->data['view_customer_total'] = $view_customer_total;
        $this->data['view_city_total'] = $view_city_total;
        $this->data['expiration_days_from'] = $expiration_days_from;
        $this->data['group_by_biller'] = $group_by_biller;
        $this->data['group_by_seller'] = $group_by_seller;
        $this->data['default_biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
        $this->data['sma'] = $this->sma;
        if ($generate_in == 'pdf') {


            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'users',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('portfolio_report'),
            ]);

            $this->load_view($this->theme.'reports/pdf/portfolio', $this->data);
        } else if ($generate_in = 'xls') {
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle(lang('portfolio_report'));
            $this->excel->getActiveSheet()->SetCellValue('A3', 'Número');
            $this->excel->getActiveSheet()->SetCellValue('B3', 'Fecha factura');
            $this->excel->getActiveSheet()->SetCellValue('C3', 'Valor bruto factura');
            $this->excel->getActiveSheet()->SetCellValue('D3', '% Dscto');
            $this->excel->getActiveSheet()->SetCellValue('E3', 'Val Dscto (-)');
            $this->excel->getActiveSheet()->SetCellValue('F3', 'Subtotal (+)');
            $this->excel->getActiveSheet()->SetCellValue('G3', 'Retenciones (-)');
            $this->excel->getActiveSheet()->SetCellValue('H3', 'Abonos (-)');
            $this->excel->getActiveSheet()->SetCellValue('I3', 'Nota crédito (-)');
            $this->excel->getActiveSheet()->SetCellValue('J3', 'Valor Impuesto (+)');
            $this->excel->getActiveSheet()->SetCellValue('K3', 'Fecha Vence');
            $this->excel->getActiveSheet()->SetCellValue('L3', 'Días');
            $this->excel->getActiveSheet()->SetCellValue('M3', 'Saldo en factura');
            $row = 4;
            $sellers = [];
            $customers = [];
            $addresses = [];
            $cities = [];
            $states = [];
            $total_balance=0;
            $customer_balance = 0;
            $address_balance = 0;
            $customer_total_subtotal = 0;
            $address_total_subtotal = 0;
            $state_balance = 0;
            $city_balance = 0;
            $seller_balance = 0;
            $last_biller = null;
            if (!$group_by_biller) {
                $this->excel->getActiveSheet()->SetCellValue('A2', 'Sucursal :');
                $this->excel->getActiveSheet()->SetCellValue('B2', 'Todas las sucursales');
                $this->excel->getActiveSheet()->SetCellValue('D2', 'Vendedor : ');
                $this->excel->getActiveSheet()->SetCellValue('E2', 'Todos los vendedores');
            }
            foreach ($data as $sale) {
                if ($group_by_seller && !isset($sellers[$sale->seller_id])) {
                    if ($address_balance != 0 && $view_customer_address_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $address_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $address_balance);
                        $address_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($customer_balance != 0 && $view_customer_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $customer_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $customer_balance);
                        $customer_total_subtotal = 0;
                        $customer_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($city_balance != 0 && $view_city_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Ciudad ".(mb_strtoupper(end($cities)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $city_balance);
                        $city_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($state_balance != 0 && $view_city_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Dpto ".(mb_strtoupper(end($states)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $state_balance);
                        $state_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($seller_balance != 0) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del vendedor ".(mb_strtoupper(end($sellers)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $seller_balance);
                        $seller_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                    $sellers[$sale->seller_id] = $sale->seller_name;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Sucursal :');
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $sellers[$sale->seller_id]);
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, 'Vendedor : ');
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $sale->biller_name);
                    $customers = [];
                    $states = [];
                    $addresses = [];
                    $cities = [];
                    $row++;
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                }

                if (!isset($states[mb_strtolower($sale->address_state)])) {
                    if ($address_balance != 0 && $view_customer_address_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $address_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $address_balance);
                        $address_total_subtotal = 0;
                        $address_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($customer_balance != 0 && $view_customer_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $customer_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $customer_balance);
                        $customer_total_subtotal = 0;
                        $customer_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($city_balance != 0 && $view_city_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Ciudad ".(mb_strtoupper(end($cities)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $city_balance);
                        $city_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($state_balance != 0 && $view_city_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Dpto ".(mb_strtoupper(end($states)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $state_balance);
                        $state_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }

                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                    unset($cities);
                    unset($customers);
                    unset($addresses);
                    $states[mb_strtolower($sale->address_state)] = $sale->address_state;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, mb_strtoupper($sale->address_state));
                    $row++;
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                }

                if (!isset($cities[mb_strtolower($sale->address_city)])) {
                    if ($address_balance != 0 && $view_customer_address_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $address_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $address_balance);
                        $address_total_subtotal = 0;
                        $address_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($customer_balance != 0 && $view_customer_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $customer_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $customer_balance);
                        $customer_total_subtotal = 0;
                        $customer_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($city_balance != 0 && $view_city_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Ciudad ".(mb_strtoupper(end($cities)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $city_balance);
                        $city_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                    $cities[mb_strtolower($sale->address_city)] = $sale->address_city;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, mb_strtoupper($sale->address_city));
                    unset($customers);
                    unset($addresses);
                    $customers[$sale->customer_id] = $sale->customer_name;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, mb_strtoupper($sale->customer_name));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, "NIT/CC : ".$sale->customer_vat_no.($sale->customer_digito_verificacion != 0 ? "-".$sale->customer_digito_verificacion : ""));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, "Lista : ".(!empty($sale->price_group_name) ? ucfirst(mb_strtolower($sale->price_group_name)) : "Sin lista de precio asignada"));
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row,'Anticipos : '.$sale->customer_deposit_amount);
                    $row++;
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                }
                if (!isset($customers[$sale->customer_id])) {
                    if ($address_balance != 0 && $view_customer_address_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $address_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $address_balance);
                        $address_total_subtotal = 0;
                        $address_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($customer_balance != 0 && $view_customer_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $customer_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $customer_balance);
                        $customer_total_subtotal = 0;
                        $customer_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                    $customer_total_subtotal = 0;
                    $customers[$sale->customer_id] = $sale->customer_name;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, mb_strtoupper($sale->customer_name));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, "NIT/CC : ".$sale->customer_vat_no.($sale->customer_digito_verificacion != 0 ? "-".$sale->customer_digito_verificacion : ""));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, "Lista : ".(!empty($sale->price_group_name) ? ucfirst(mb_strtolower($sale->price_group_name)) : "Sin lista de precio asignada"));
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row,'Anticipos : '.$sale->customer_deposit_amount);
                    $row++;
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                }
                if (!isset($addresses[$sale->address_id])) {
                    if ($address_balance != 0 && $view_customer_address_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $address_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $address_balance);
                        $address_total_subtotal = 0;
                        $address_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                    $address_total_subtotal = 0;
                    $addresses[$sale->address_id] = $sale->address_name;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, "Sucursal : ".ucfirst(mb_strtolower($sale->address_name)));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, ucfirst(mb_strtolower($sale->address_direccion)).", ".ucfirst(mb_strtolower($sale->address_city)));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, "Télefono : ".$sale->address_phone);
                    $row++;
                    $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, $sale->reference_no);
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $sale->date);
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $sale->grand_total);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $sale->percentage_discount);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $sale->total_discount);
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, $sale->sub_total);
                $this->excel->getActiveSheet()->SetCellValue('G'.$row, ($sale->rete_fuente_total + $sale->rete_iva_total + $sale->rete_ica_total + $sale->rete_other_total));
                $this->excel->getActiveSheet()->SetCellValue('H'.$row, $sale->paid);
                $this->excel->getActiveSheet()->SetCellValue('I'.$row, $sale->credit_note);
                $this->excel->getActiveSheet()->SetCellValue('J'.$row, $sale->total_tax);
                $this->excel->getActiveSheet()->SetCellValue('K'.$row, $sale->expiration_date);
                if ($expiration_days_from == 1) {
                    $fecha = substr($sale->date,0,10);
                } else if ($expiration_days_from == 2) {
                    $fecha = substr($sale->due_date,0,10);
                }
                $fecha_now = date('Y-m-d');
                $dias   = (strtotime($fecha)-strtotime($fecha_now))/86400;
                $dias   = abs($dias);
                $dias   = floor($dias);
                $this->excel->getActiveSheet()->SetCellValue('L'.$row, $dias);
                $this->excel->getActiveSheet()->SetCellValue('M'.$row, $sale->balance);
                $customer_balance += $sale->balance;
                $customer_total_subtotal += $sale->sub_total;
                $address_balance += $sale->balance;
                $address_total_subtotal += $sale->sub_total;
                $state_balance += $sale->balance;
                $city_balance += $sale->balance;
                $seller_balance += $sale->balance;
                $total_balance += $sale->balance;
                $row++;
                if ($sale === end($data)) {
                    if ($address_balance != 0 && $view_customer_address_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $address_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Sucursal ".ucfirst(mb_strtolower(end($addresses)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $address_balance);
                        $address_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($customer_balance != 0 && $view_customer_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $customer_total_subtotal);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Cliente ".(mb_strtoupper(end($customers)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $customer_balance);
                        $customer_total_subtotal = 0;
                        $customer_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($city_balance != 0 && $view_city_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total de la Ciudad ".(mb_strtoupper(end($cities)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $city_balance);
                        $city_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($state_balance != 0 && $view_city_total) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del Dpto ".(mb_strtoupper(end($states)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $state_balance);
                        $state_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                    if ($seller_balance != 0) {
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Saldo total del vendedor ".(mb_strtoupper(end($sellers)))." : ");
                        $this->excel->getActiveSheet()->SetCellValue('M'.$row, $seller_balance);
                        $seller_balance = 0;
                        $row++;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(false);
                    }
                }
            }
            $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
            $this->excel->getActiveSheet()->SetCellValue('I'.$row, "Gran total del informe : ");
            $this->excel->getActiveSheet()->SetCellValue('M'.$row, $total_balance);
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
            $this->excel->getActiveSheet()->getStyle('A1:A'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('B1:B'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('C1:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('F1:F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('G1:G'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('H1:H'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('I1:I'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('J1:J'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('L1:L'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('M1:M'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $filename = lang('portfolio_report');
            $this->load->helper('excel');


            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'users',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('portfolio_report'),
            ]);

            create_excel($this->excel, $filename);
        }
    }

    //INFORMES JAIME INICIO

    //JAIME INFORME DE CARTERA

    public function portfolio_report() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('filter_action', lang('filter'), 'required');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['sellers'] = $this->reports_model->get_sales_seller();
        $this->data['customers'] = $this->reports_model->get_sales_customer();
        $can_filter = $this->Settings->big_data_limit_reports == 1 ? $this->reports_model->reset_portfolio_temp_tables() : true;
        $this->data['can_filter'] = $can_filter;
        $periodos = json_encode([
                                1 => [
                                        'start_month' => '-01-01',
                                        'end_month' => '-01-15',
                                    ],
                                2 => [
                                        'start_month' => '-01-16',
                                        'end_month' => '-01-31',
                                    ],
                                3 => [
                                        'start_month' => '-02-01',
                                        'end_month' => '-02-15',
                                    ],
                                4 => [
                                        'start_month' => '-02-16',
                                        'end_month' => '-03-01',
                                    ],
                                5 => [
                                        'start_month' => '-03-02',
                                        'end_month' => '-03-15',
                                    ],
                                6 => [
                                        'start_month' => '-03-16',
                                        'end_month' => '-03-31',
                                    ],
                                7 => [
                                        'start_month' => '-04-01',
                                        'end_month' => '-04-15',
                                    ],
                                8 => [
                                        'start_month' => '-04-16',
                                        'end_month' => '-04-30',
                                    ],
                                9 => [
                                        'start_month' => '-05-01',
                                        'end_month' => '-05-15',
                                    ],
                                10 => [
                                        'start_month' => '-05-16',
                                        'end_month' => '-05-31',
                                    ],
                                11 => [
                                        'start_month' => '-06-01',
                                        'end_month' => '-06-15',
                                    ],
                                12 => [
                                        'start_month' => '-06-16',
                                        'end_month' => '-06-30',
                                    ],
                                13 => [
                                        'start_month' => '-07-01',
                                        'end_month' => '-07-15',
                                    ],
                                14 => [
                                        'start_month' => '-07-16',
                                        'end_month' => '-07-31',
                                    ],
                                15 => [
                                        'start_month' => '-08-01',
                                        'end_month' => '-08-15',
                                    ],
                                16 => [
                                        'start_month' => '-08-16',
                                        'end_month' => '-08-31',
                                    ],
                                17 => [
                                        'start_month' => '-09-01',
                                        'end_month' => '-09-15',
                                    ],
                                18 => [
                                        'start_month' => '-09-16',
                                        'end_month' => '-09-30',
                                    ],
                                19 => [
                                        'start_month' => '-10-01',
                                        'end_month' => '-10-15',
                                    ],
                                20 => [
                                        'start_month' => '-10-16',
                                        'end_month' => '-10-31',
                                    ],
                                21 => [
                                        'start_month' => '-11-01',
                                        'end_month' => '-11-15',
                                    ],
                                22 => [
                                        'start_month' => '-11-16',
                                        'end_month' => '-11-30',
                                    ],
                                23 => [
                                        'start_month' => '-12-01',
                                        'end_month' => '-12-15',
                                    ],
                                24 => [
                                        'start_month' => '-12-16',
                                        'end_month' => '-12-31',
                                    ],
                                ]);
        $this->data['periodos'] = $periodos;
        if ($this->form_validation->run() === TRUE && $can_filter)
        {

            $xls = $this->input->post('action') == 'xls' ? true : false;
            $pdf = $this->input->post('action') == 'pdf' ? true : false;
            $biller = $this->input->post('Sucursal');
            $seller = $this->input->post('Vendedor');
            $customer = $this->input->post('Cliente');
            $data = [
                    'biller' => $biller,
                    'seller' => $seller,
                    'customer' => $customer,
                    ];
            if ($this->Settings->big_data_limit_reports == 1) {
                $this->data['sales'] = $this->reports_model->get_sales_big_data($data);
            } else {
                $this->data['sales'] = $this->reports_model->get_sales($data);
            }

            if ($xls) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó XLS '.lang('portfolio_by_ages_seller'),
                ]);
                $this->load_view($this->theme.'reports/xls/portfolio_report', $this->data);
            } else if ($pdf) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó PDF '.lang('portfolio_by_ages_seller'),
                ]);
                $this->load_view($this->theme.'reports/pdf/portfolio_report', $this->data);
            } else {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 4,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('portfolio_by_ages_seller'),
                ]);
                $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('portfolio_by_ages_seller')));
                $meta = array('page_title' => lang('portfolio_by_ages_seller'), 'bc' => $bc);
                $this->page_construct('reports/portfolio_report', $meta, $this->data);
            }
        } else {
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('portfolio_by_ages_seller')));
            $meta = array('page_title' => lang('portfolio_by_ages_seller'), 'bc' => $bc);
            $this->page_construct('reports/portfolio_report', $meta, $this->data);
        }
    }

    public function portfolio_report_2() {
        // $this->sma->print_arrays($this->filter_year_options);
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('end_date', lang('end_date'), 'required');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['sellers'] = $this->reports_model->get_sales_seller();
        $this->data['customers'] = $this->reports_model->get_sales_customer();
        $can_filter = $this->Settings->big_data_limit_reports == 1 ? $this->reports_model->reset_portfolio_temp_tables() : true;
        if (!$can_filter && $this->session->userdata('portfolio_temp_end_date')) {
            $this->session->unset_userdata('portfolio_temp_end_date');
        }
        $this->data['can_filter'] = $can_filter;
        $periodos = json_encode([
                                1 => [
                                        'start_month' => '-01-01',
                                        'end_month' => '-01-15',
                                    ],
                                2 => [
                                        'start_month' => '-01-16',
                                        'end_month' => '-01-31',
                                    ],
                                3 => [
                                        'start_month' => '-02-01',
                                        'end_month' => '-02-15',
                                    ],
                                4 => [
                                        'start_month' => '-02-16',
                                        'end_month' => '-03-01',
                                    ],
                                5 => [
                                        'start_month' => '-03-02',
                                        'end_month' => '-03-15',
                                    ],
                                6 => [
                                        'start_month' => '-03-16',
                                        'end_month' => '-03-31',
                                    ],
                                7 => [
                                        'start_month' => '-04-01',
                                        'end_month' => '-04-15',
                                    ],
                                8 => [
                                        'start_month' => '-04-16',
                                        'end_month' => '-04-30',
                                    ],
                                9 => [
                                        'start_month' => '-05-01',
                                        'end_month' => '-05-15',
                                    ],
                                10 => [
                                        'start_month' => '-05-16',
                                        'end_month' => '-05-31',
                                    ],
                                11 => [
                                        'start_month' => '-06-01',
                                        'end_month' => '-06-15',
                                    ],
                                12 => [
                                        'start_month' => '-06-16',
                                        'end_month' => '-06-30',
                                    ],
                                13 => [
                                        'start_month' => '-07-01',
                                        'end_month' => '-07-15',
                                    ],
                                14 => [
                                        'start_month' => '-07-16',
                                        'end_month' => '-07-31',
                                    ],
                                15 => [
                                        'start_month' => '-08-01',
                                        'end_month' => '-08-15',
                                    ],
                                16 => [
                                        'start_month' => '-08-16',
                                        'end_month' => '-08-31',
                                    ],
                                17 => [
                                        'start_month' => '-09-01',
                                        'end_month' => '-09-15',
                                    ],
                                18 => [
                                        'start_month' => '-09-16',
                                        'end_month' => '-09-30',
                                    ],
                                19 => [
                                        'start_month' => '-10-01',
                                        'end_month' => '-10-15',
                                    ],
                                20 => [
                                        'start_month' => '-10-16',
                                        'end_month' => '-10-31',
                                    ],
                                21 => [
                                        'start_month' => '-11-01',
                                        'end_month' => '-11-15',
                                    ],
                                22 => [
                                        'start_month' => '-11-16',
                                        'end_month' => '-11-30',
                                    ],
                                23 => [
                                        'start_month' => '-12-01',
                                        'end_month' => '-12-15',
                                    ],
                                24 => [
                                        'start_month' => '-12-16',
                                        'end_month' => '-12-31',
                                    ],
                                ]);
        $this->data['periodos'] = $periodos;
        if ($this->form_validation->run() === TRUE && $can_filter)
        {

            $xls = $this->input->post('action') == 'xls' ? true : false;
            $pdf = $this->input->post('action') == 'pdf' ? true : false;
            $filter_year = $this->input->post('filter_year');
            $biller = $this->input->post('Sucursal');
            $end_date = $this->input->post('end_date');
            $seller = $this->input->post('Vendedor');
            $customer = $this->input->post('Cliente');
            $group_by_biller = $this->input->post('group_by_biller') ? true : false;
            $group_by_seller = $this->input->post('group_by_seller') ? true : false;
            $group_by_city = $this->input->post('group_by_city') ? true : false;
            $group_by_customer = $this->input->post('group_by_customer') ? true : false;
            $group_by_customer_address = $this->input->post('group_by_customer_address') ? true : false;
            $order_by = $this->input->post('order_by');
            $days_by_end_date = false;
            if ($end_date != date('Y-m-d')) {
                $days_by_end_date = true;
            }
            $this->data['days_by_end_date'] = $days_by_end_date;
            $this->data['end_date'] = $end_date;
            $this->data['group_by_biller'] = $group_by_biller;
            $this->data['group_by_seller'] = $group_by_seller;
            $this->data['group_by_city'] = $group_by_city;
            $this->data['group_by_customer'] = $group_by_customer;
            $this->data['group_by_customer_address'] = $group_by_customer_address;
            $data = [
                     'filter_year' => $filter_year,
                     'biller' => $biller,
                     'seller' => $seller,
                     'customer' => $customer,
                     'order' => 'customer',
                     'group_by_biller' => $group_by_biller,
                     'group_by_seller' => $group_by_seller,
                     'group_by_city' => $group_by_city,
                     'group_by_customer' => $group_by_customer,
                     'group_by_customer_address' => $group_by_customer_address,
                     'order_by' => $order_by,
                     'end_date' => $end_date,
                    ];
            if ($this->Settings->big_data_limit_reports == 1) {
                $this->data['sales'] = $this->reports_model->get_sales_big_data($data);
            } else {
                $this->data['sales'] = $this->reports_model->get_sales($data);
            }
            $group_data = true;
            if ($order_by != "") {
                $group_data = false;
            } else {
                if ($group_by_seller == false && $group_by_city == false && $group_by_customer == false) {
                    $group_data = false;
                }
            }
            $this->data['group_data'] = $group_data;
            if ($xls) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó XLS'.lang('portfolio_by_ages_customer'),
                ]);
                $this->load_view($this->theme.'reports/xls/portfolio_report_2', $this->data);
            } else if ($pdf) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó PDF'.lang('portfolio_by_ages_customer'),
                ]);
                $this->load_view($this->theme.'reports/pdf/portfolio_report_2', $this->data);
            } else {

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 4,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('portfolio_by_ages_customer'),
                ]);
                $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('portfolio_by_ages_customer')));
                $meta = array('page_title' => lang('portfolio_by_ages_customer'), 'bc' => $bc);
                $this->page_construct('reports/portfolio_report_2', $meta, $this->data);
            }
        } else {
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('portfolio_by_ages_customer')));
            $meta = array('page_title' => lang('portfolio_by_ages_customer'), 'bc' => $bc);
            $this->page_construct('reports/portfolio_report_2', $meta, $this->data);
        }
    }

    public function search_sales_Seller($biller = '') {
        $this->data['sellers'] = $this->reports_model->get_sales_seller($biller);
        echo json_encode($this->data, true);
    }

    public function search_sale_customer($biller = '',$seller = '') {
        $this->data['sellers'] = $this->reports_model->get_sales_customer($biller,$seller);
        echo json_encode($this->data, true);
    }
    //JAIME INFORME DE CARTERA



    /* Informe Zeta */
    public function load_zeta()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->data['billers'] = $this->reports_model->get_zeta_sales_biller();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('zeta_report')));
        $meta = array('page_title' => lang('zeta_report'), 'bc' => $bc);
        $this->page_construct('reports/zeta_report', $meta, $this->data);
    }

    public function Zeta_billing_search() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->form_validation->set_rules('fech_ini', 'Fecha Inicial', 'required');
        if ($this->form_validation->run() === TRUE) {
            $Sucursal = $this->input->post('Sucursal');
            $fech_ini = $this->input->post('fech_ini');
            $Informe = $this->input->post('Informe');
            $module_id = $this->input->post('module_id');
            $document_type_id = $this->input->post('document_type_id');
            $Tipo = $this->input->post('Tipo');
            $fech_fin = date("Y-m-d",strtotime($fech_ini."+ 1 days"));
            $data['input'] = array('Sucursal' => $Sucursal,'Tipo' => $Tipo,'users' => 'Usuario de Impresion','fech_ini' => $fech_ini,'fech_fin' => $fech_fin,
                                    'nombre' => 'Juan Alberto Giraldo Aristizabal','documento'=>'1098649968' ,'direccion'=>' Calle 30 25 71 lc xxxxxxxx',
                                    'ciudad'=>'Floridablanca','telefono'=>'600 0000');
            $data['billings'] = $this->reports_model->get_billing($Sucursal,$fech_ini,$fech_fin, NULL, $module_id, $document_type_id);
            $data['returns'] = $this->reports_model->get_billing($Sucursal,$fech_ini,$fech_fin,1, $module_id, $document_type_id);
            $data['total_payments'] = $this->reports_model->get_total_payment($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            $data['categories_tax'] = $this->reports_model->get_total_categories_tax($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            $data['total_tax_rates'] = $this->reports_model->get_total_tax_rates($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            if ($this->Settings->ipoconsumo == 1) {
                $ico = $this->reports_model->get_total_ico($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
                $data['total_tax_rates'][] = $ico[0];
            }
            $data['total_users_taxs'] = $this->reports_model->get_total_users_tax($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            $data['total_sellers_taxs'] = $this->reports_model->get_total_sellers_tax($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            $data['total_customers_taxs'] = $this->reports_model->get_total_customers_tax($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            $data['register_movements'] = $this->reports_model->get_pos_register_movements($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            $data['deposits'] = $this->reports_model->get_deposits($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
            $biller = $Sucursal ? $Sucursal : $this->Settings->default_biller;

            $data['biller'] = $this->site->getAllCompaniesWithState('biller', $biller);
            if($Informe=='Pos') {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 4,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('zeta_report'),
                ]);
                $this->load_view($this->theme . 'reports/zeta_report_pos', $data);
            } else if ($Informe == 'Excel') {
                $this->load->library('excel');
                $text_center_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );
                $text_right_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    )
                );
                $row = 1;
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('zeta_report'));
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("initial_date"));
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $data['input']['fech_ini']);
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("end_date"));
                $this->excel->getActiveSheet()->getStyle("C".$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $data['input']['fech_fin']);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang('number_of_records'));
                $this->excel->getActiveSheet()->getStyle("E".$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, count($data['billings']));
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("initial_number"));
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, count($data['billings']) > 0 ? $data['billings'][0]['reference_no'] : '');
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("end_number"));
                $this->excel->getActiveSheet()->getStyle("C".$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, count($data['billings']) > 0 ? $data['billings'][count($data['billings'])-1]['reference_no'] : '');
                $row++;
                foreach ($data['total_payments'] as $payment)
                {
                    ${$payment['name']} = 0;
                    ${$payment['name'].'_debo'} = 0;
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("sales"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/' );
                $count = 0;
                $prefijo_old = $referencia_old = $prefijo = '';
                $array_prefijo = array();
                $cantidad = 0;
                foreach ($data['billings'] as $billing)
                {
                    $referencia = $billing['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);
                    if($prefijo_old!=$prefijo)
                    {
                        $count++;
                        if($count > 1)
                        {
                            $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                            $array_prefijo[$prefijo_old]['count'] = $cantidad;
                        }
                        $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old );
                        $prefijo_old = $prefijo;
                        $cantidad = 0;
                    }
                    $referencia_old = $referencia;
                    $cantidad++;
                }

                if(count($data['billings']) > 1)
                {
                    $array_prefijo[$prefijo]['Fin'] = $data['billings'][count($data['billings'])-1]['reference_no'];
                    $array_prefijo[$prefijo]['count'] = $cantidad;
                }
                else
                {
                    $array_prefijo[$prefijo]['count'] = 1;
                }

                $prefijo_old = $referencia_old = '';
                $total_ventas_subtotal = $total_ventas_iva  =  $total_ventas_total  = 0;
                $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
                $total_prefijo_propina = $total_prefijo_domicilio =  0;
                $total_propina = $total_domicilio = $Credito =  0;
                $prefijo = NULL;
                foreach ($data['billings'] as $billing)
                {
                    $referencia = $billing['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);
                    if($prefijo_old!==$prefijo) {
                        if($prefijo_old!='') {
                            // if ($this->pos_settings->apply_suggested_tip > 0) {
                            //     $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_propina),'T',0,'R');
                            // }
                            // if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                            //     $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_domicilio),'T',0,'R');
                            // }
                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Total');
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_prefijo_subtotal);
                            $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_prefijo_iva);
                            $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_prefijo_total);
                            $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                            $row++;
                            $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
                            $total_prefijo_propina = $total_prefijo_domicilio =  0;
                        }
                        $array_prefijo[$prefijo];
                        $Fin = $array_prefijo[$prefijo]['Fin'];
                        $Ini = $array_prefijo[$prefijo]['Ini'];
                        $count = $array_prefijo[$prefijo]['count'];
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Prefijo : '.$prefijo);
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, 'Registros : '.$count);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, 'Num. inicial : '.$Ini);
                        $this->excel->getActiveSheet()->SetCellValue('G'.$row, 'Num. fin : '.$Fin);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                        $row++;
                        if ($data['input']['Tipo'] == 'Detallado') {
                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Consecutivo');
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, 'Cliente');
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, 'Fecha');
                            $this->excel->getActiveSheet()->SetCellValue('D'.$row, 'Subtotal');
                            $this->excel->getActiveSheet()->SetCellValue('E'.$row, 'Iva');
                            $this->excel->getActiveSheet()->SetCellValue('F'.$row, 'Total');
                            $this->excel->getActiveSheet()->SetCellValue('G'.$row, 'Forma pago');
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                            $row++;
                        }
                        // if ($this->pos_settings->apply_suggested_tip > 0) {
                        //     $pdf->Cell($ancho_columnas,$height-1, 'Propina','T',0,'C');
                        // }
                        // if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                        //     $pdf->Cell($ancho_columnas,$height-1, 'Domicilio','T',0,'C');
                        // }
                        $prefijo_old = $prefijo;
                    }
                    if($billing['cantidad'] > 1 ) {
                        $Pago = $billing['name'].', '. lang("other");
                    } else {
                        $Pago = $billing['name'];
                    }
                    $fecha = substr($billing['fecha'],0,10);
                    $fecha_pay = substr($billing['fecha_pay'],0,10);
                    if($billing['saldo'] > 1)
                    {
                        if($billing['cantidad'] > 0 ) $Pago = lang("credit") .', '. lang("others");
                        else $Pago = lang("credit");
                        $Credito += $billing['grand_total'];
                    }
                    else
                    {
                        if($fecha!=$fecha_pay)
                        {
                            if($billing['cantidad'] > 0 ) $Pago = lang("credit") .', '. lang("others");
                            else $Pago = lang("credit");
                            $Credito += $billing['grand_total'];
                        } else {
                            ${$billing['name']} += $billing['grand_total'];
                        }
                    }
                    if ($data['input']['Tipo'] == 'Detallado') {
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, $billing['reference_no']);
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, $billing['customer']);
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, $billing['fecha']);
                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, $billing['total']);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, $billing['total_tax']);
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, $billing['grand_total']);
                        $this->excel->getActiveSheet()->SetCellValue('G'.$row, $Pago);
                        $row++;
                    }
                    $total_ventas_subtotal += $billing['total'];
                    $total_ventas_iva  += $billing['total_tax'];
                    $total_ventas_total  += $billing['grand_total'];
                    $total_prefijo_subtotal += $billing['total'];
                    $total_prefijo_iva  += $billing['total_tax'];
                    $total_prefijo_total  += $billing['grand_total'];
                    $total_prefijo_propina  += $billing['tip_amount'];
                    $total_prefijo_domicilio += $billing['shipping'];
                    $total_propina  += $billing['tip_amount'];
                    $total_domicilio += $billing['shipping'];
                }
                if ($total_prefijo_subtotal > 0) {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_prefijo_subtotal);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_prefijo_iva);
                    $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_prefijo_total);
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                    $row++;
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("sales_total"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_ventas_subtotal);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_ventas_iva);
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_ventas_total);
                $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;

                //deposits
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('deposits'));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $count = 0;
                $prefijo_old = $referencia_old = $prefijo = '';
                $array_prefijo = array();
                $cantidad = 0;
                $total_deposit = 0;
                foreach ($data['deposits'] as $deposit)
                {
                    $referencia = $deposit['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);
                    if ($deposit['code'] !== 'deposit') {
                        if (!isset($paids_deposits[$deposit['name']])) {
                            $paids_deposits[$deposit['name']] = $deposit['amount'];
                        }
                        else{
                            $paids_deposits[$deposit['name']] += $deposit['amount'];
                        }
                    }
                    if($prefijo_old!=$prefijo)
                    {
                        $count++;
                        if($count > 1)
                        {
                            $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                            $array_prefijo[$prefijo_old]['count'] = $cantidad;
                        }
                        $array_prefijo[$prefijo] = array("prefijo" => $prefijo,
                                                        "Ini" => $referencia ,
                                                        "Fin" => $referencia_old );
                        $prefijo_old = $prefijo;
                        $cantidad = 0;
                    }
                    $referencia_old = $referencia;
                    $cantidad++;
                }
                if(count($data['deposits']) > 1)
                {
                    $array_prefijo[$prefijo]['Fin'] = $data['deposits'][count($data['deposits'])-1]['reference_no'];
                    $array_prefijo[$prefijo]['count'] = $cantidad;
                }
                else
                {
                    $array_prefijo[$prefijo]['count'] = 1;
                }
                $prefijo_old = $referencia_old = '';
                $total_dev_subtotal = $total_dev_iva  =  $total_dev_total  = 0;
                $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
                $total_prefijo_propina = $total_prefijo_domicilio =  0;
                $total_propina = $total_domicilio = $Credito_debo = 0;
                $prefijo = NULL;
                foreach ($data['deposits'] as $deposit)
                {
                    $referencia = $deposit['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);
                    if($prefijo_old!==$prefijo) {
                        if($prefijo_old!='') {
                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                            $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_prefijo_total);
                            $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                            $row++;
                            $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
                            $total_prefijo_propina = $total_prefijo_domicilio =  0;
                        }
                        $array_prefijo[$prefijo];
                        $Fin = $array_prefijo[$prefijo]['Fin'];
                        $Ini = $array_prefijo[$prefijo]['Ini'];
                        $count = $array_prefijo[$prefijo]['count'];
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("prefix") .': '.$prefijo);
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("records") .' : '.$count);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang("initial_number") .' : '.$Ini);
                        $this->excel->getActiveSheet()->SetCellValue('G'.$row, lang("end_number") .' : '.$Fin);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                        $row++;
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("consecutive"));
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("customer"));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("date"));
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, lang("total"));
                        $this->excel->getActiveSheet()->SetCellValue('G'.$row, lang("payment_method"));
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                        $row++;
                        $prefijo_old = $prefijo;
                    }
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $deposit['reference_no']);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $deposit['customer']);
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $deposit['fecha']);
                    $this->excel->getActiveSheet()->SetCellValue('F'.$row, $deposit['amount']);
                    $Pago = $deposit['name'];
                    $fecha = substr($deposit['fecha'],0,10);
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row, $Pago);
                    $row++;
                    $total_deposit += $deposit['amount'];
                }
                if ($total_prefijo_subtotal != 0) {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                    $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_deposit);
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                    $row++;
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, 'Total '. lang('deposits'));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_deposit);
                $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;

                //RETURNS
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("returns"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $count = 0;
                $prefijo_old = $referencia_old = $prefijo = '';
                $array_prefijo = array();
                $cantidad = 0;
                foreach ($data['returns'] as $billing)
                {
                    $referencia = $billing['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);
                    if($prefijo_old!=$prefijo)
                    {
                        $count++;
                        if($count > 1)
                        {
                            $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                            $array_prefijo[$prefijo_old]['count'] = $cantidad;
                        }
                        $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old );
                        $prefijo_old = $prefijo;
                        $cantidad = 0;
                    }
                    $referencia_old = $referencia;
                    $cantidad++;
                }
                if(count($data['returns']) > 1)
                {
                    $array_prefijo[$prefijo]['Fin'] = $data['returns'][count($data['returns'])-1]['reference_no'];
                    $array_prefijo[$prefijo]['count'] = $cantidad;
                }
                else
                {
                    $array_prefijo[$prefijo]['count'] = 1;
                }
                $prefijo_old = $referencia_old = '';
                $total_dev_subtotal = $total_dev_iva  =  $total_dev_total  = 0;
                $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
                $total_prefijo_propina = $total_prefijo_domicilio =  0;
                $total_propina = $total_domicilio = $Credito_debo = 0;
                $prefijo = NULL;
                foreach ($data['returns'] as $billing)
                {
                    $referencia = $billing['reference_no'];
                    $prefijo = str_replace($arrayReplace,'',$referencia);
                    if($prefijo_old!==$prefijo) {
                        if($prefijo_old!='') {
                            // if ($this->pos_settings->apply_suggested_tip > 0) {
                            //     $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_propina),'T',0,'R');
                            // }
                            // if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                            //     $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_domicilio),'T',0,'R');
                            // }
                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_prefijo_subtotal);
                            $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_prefijo_iva);
                            $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_prefijo_total);
                            $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                            $row++;
                            $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
                            $total_prefijo_propina = $total_prefijo_domicilio =  0;
                        }
                        $array_prefijo[$prefijo];
                        $Fin = $array_prefijo[$prefijo]['Fin'];
                        $Ini = $array_prefijo[$prefijo]['Ini'];
                        $count = $array_prefijo[$prefijo]['count'];
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("prefix") .' : '.$prefijo);
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("records") .' : '.$count);
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang("initial_number") .' : '.$Ini);
                        $this->excel->getActiveSheet()->SetCellValue('G'.$row, lang("end_number") .' : '.$Fin);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                        $row++;
                        $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("consecutive"));
                        $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("customer"));
                        $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("date"));
                        $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("subtotal"));
                        $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang("vat"));
                        $this->excel->getActiveSheet()->SetCellValue('F'.$row, lang("total"));
                        $this->excel->getActiveSheet()->SetCellValue('G'.$row, lang("payment_method"));
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                        $row++;
                        // if ($this->pos_settings->apply_suggested_tip > 0) {
                        //     $pdf->Cell($ancho_columnas,$height-1, 'Propina','T',0,'C');
                        // }
                        // if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                        //     $pdf->Cell($ancho_columnas,$height-1, 'Domicilio','T',0,'C');
                        // }
                        $prefijo_old = $prefijo;
                    }
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $billing['reference_no']);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $billing['customer']);
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $billing['fecha']);
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $billing['total']);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $billing['total_tax']);
                    $this->excel->getActiveSheet()->SetCellValue('F'.$row, $billing['grand_total']);
                    if($billing['cantidad'] > 1 ) {
                        $Pago = $billing['name'].', '. lang("others");
                    } else {
                        $Pago = $billing['name'];
                    }
                    $fecha = substr($billing['fecha'],0,10);
                    $fecha_pay = substr($billing['fecha_pay'],0,10);
                    if($billing['saldo'] > 1)
                    {
                        if($billing['cantidad'] > 0 ) $Pago = lang("credits") .', '. lang("others");
                        else $Pago = lang("credit");
                        $Credito_debo += $billing['grand_total'];
                    }
                    else
                    {
                        if($fecha!=$fecha_pay)
                        {
                            if($billing['cantidad'] > 0 ) $Pago = 'Credito, Otros';
                            else $Pago = lang("credit");
                            $Credito_debo += $billing['grand_total'];
                        } else {
                            ${$billing['name'].'_debo'} += $billing['grand_total'];
                        }
                    }
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row, $Pago);
                    $row++;
                    $total_dev_subtotal += $billing['total'];
                    $total_dev_iva  += $billing['total_tax'];
                    $total_dev_total  += $billing['grand_total'];
                    $total_prefijo_subtotal += $billing['total'];
                    $total_prefijo_iva  += $billing['total_tax'];
                    $total_prefijo_total  += $billing['grand_total'];
                    $total_prefijo_propina  += $billing['tip_amount'];
                    $total_prefijo_domicilio += $billing['shipping'];
                    $total_propina  += $billing['tip_amount'];
                    $total_domicilio += $billing['shipping'];
                }
                if ($total_prefijo_subtotal != 0) {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_prefijo_subtotal);
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_prefijo_iva);
                    $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_prefijo_total);
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                    $row++;
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total_returns"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_dev_subtotal);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_dev_iva);
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_dev_total);
                $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("summary_general"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("sales_total"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_ventas_subtotal);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_ventas_iva);
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_ventas_total);
                $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total_returns"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_dev_subtotal);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_dev_iva);
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_dev_total);
                $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, '');
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $total_ventas_subtotal + $total_dev_subtotal);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $total_ventas_iva + $total_dev_iva);
                $this->excel->getActiveSheet()->SetCellValue('F'.$row, $total_ventas_total + $total_dev_total);
                $this->excel->getActiveSheet()->SetCellValue('G'.$row, '');
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("summary_payment_methods"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("payment_method"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("sales"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("returns"));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("total"));
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $p_total = $p_dev_total = 0;
                foreach ($data['total_payments'] as $payment)
                {
                    if (isset($paids_deposits[$payment['name']])) {
                        $tpaid = $payment['total_payment'] + $paids_deposits[$payment['name']];
                    }
                    else{
                        $tpaid = $payment['total_payment'];
                    }
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $payment['name']);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, $tpaid);
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, $payment['total_return_payment']);
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, $tpaid + $payment['total_return_payment']);
                    $row++;
                    $p_total += $tpaid;
                    $p_dev_total += $payment['total_return_payment'];
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("credit"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $Credito);
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $Credito_debo); //dev
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $Credito + $Credito_debo); // + dev
                $row++;
                $p_total += $Credito;
                $p_dev_total += $Credito_debo; //dev
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $p_total);
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $p_dev_total);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $p_total+$p_dev_total);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("summary_tax"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("rate"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("subtotal"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("vat"));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("total"));
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $unisubtotal = $item_tax = $subtotal = 0;
                foreach ($data['total_tax_rates'] as $tax_rate)
                {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $tax_rate['name']);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, ($tax_rate['unisubtotal']));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, ($tax_rate['item_tax']));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, ($tax_rate['subtotal']));
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $row++;
                    $unisubtotal += $tax_rate['unisubtotal'];
                    $item_tax += $tax_rate['item_tax'];
                    $subtotal += $tax_rate['subtotal'];
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, $unisubtotal);
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $item_tax);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $subtotal);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("summary_categories"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("category"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("product_qty2"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("subtotal"));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("vat"));
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang("total"));
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $unisubtotal = $item_tax = $subtotal = 0;
                // $this->sma->print_arrays($categories_tax);
                foreach ($data['categories_tax'] as $categori_tax)
                {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $categori_tax['Nombre']);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, ($categori_tax['total_items']));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, ($categori_tax['unisubtotal']));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, ($categori_tax['item_tax']));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, ($categori_tax['subtotal']));
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $row++;
                    $unisubtotal += $categori_tax['unisubtotal'];
                    $item_tax += $categori_tax['item_tax'];
                    $subtotal += $categori_tax['subtotal'];
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $unisubtotal);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $item_tax);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $subtotal);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("summary_users"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("name"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("sales_amount"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("subtotal"));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("vat"));
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang("total"));
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $unisubtotal = $item_tax = $subtotal = 0;
                foreach ($data['total_users_taxs'] as $total_users_tax)
                {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $total_users_tax['first_name'].' '.$total_users_tax['last_name']);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, ($total_users_tax['Cantidad']));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, ($total_users_tax['total']));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, ($total_users_tax['total_tax']));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, ($total_users_tax['grand_total']));
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $row++;
                    $unisubtotal += $total_users_tax['total'];
                    $item_tax += $total_users_tax['total_tax'];
                    $subtotal += $total_users_tax['grand_total'];
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $unisubtotal);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $item_tax);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $subtotal);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total_sales_by_seller"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("name"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("sales_amount"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("subtotal"));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("vat"));
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang("total"));
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $unisubtotal = $item_tax = $subtotal = 0;
                foreach ($data['total_sellers_taxs'] as $total_sellers_tax)
                {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, ($total_sellers_tax['first_name']. ' ' .$total_sellers_tax['first_lastname'] ));
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, ($total_sellers_tax['Cantidad']));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, ($total_sellers_tax['total']));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, ($total_sellers_tax['total_tax']));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, ($total_sellers_tax['grand_total']));
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $row++;
                    $unisubtotal += $total_sellers_tax['total'];
                    $item_tax += $total_sellers_tax['total_tax'];
                    $subtotal += $total_sellers_tax['grand_total'];
                }

                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $unisubtotal);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $item_tax);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $subtotal);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total_sales_by_customer"));
                $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row)->getFont()->setSize(13)->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("name"));
                $this->excel->getActiveSheet()->SetCellValue('B'.$row, lang("sales_amount"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, lang("subtotal"));
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, lang("vat"));
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, lang("total"));
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_center_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $unisubtotal = $item_tax = $subtotal = 0;
                foreach ($data['total_customers_taxs'] as $total_customers_tax)
                {
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, $total_customers_tax['name']);
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, ($total_customers_tax['Cantidad']));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, ($total_customers_tax['total']));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, ($total_customers_tax['total_tax']));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, ($total_customers_tax['grand_total']));
                    $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                    $row++;
                    $unisubtotal += $total_customers_tax['total'];
                    $item_tax += $total_customers_tax['total_tax'];
                    $subtotal += $total_customers_tax['grand_total'];
                }
                $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang("total"));
                $this->excel->getActiveSheet()->SetCellValue('C'.$row, $unisubtotal);
                $this->excel->getActiveSheet()->SetCellValue('D'.$row, $item_tax);
                $this->excel->getActiveSheet()->SetCellValue('E'.$row, $subtotal);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row)->getFont()->setBold(true);
                $row++;
                $this->excel->getActiveSheet()->getStyle('B5:B'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('C5:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('D5:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('E5:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('F5:F'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $filename = 'zeta_report';
                $this->load->helper('excel');

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó XLS '.lang('zeta_report'),
                ]);
                create_excel($this->excel, $filename);
            } else {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó PDF'.lang('zeta_report'),
                ]);
                $this->load_view($this->theme . 'reports/pdf/zeta_report', $data);
            }

        } else {
            $this->load->helper('form');
            $this->load->library('form_validation');
            $this->data['billers'] = $this->reports_model->get_zeta_sales_biller();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('zeta_report')));
            $meta = array('page_title' => lang('zeta_report'), 'bc' => $bc);
            $this->page_construct('reports/zeta_report', $meta, $this->data);
        }
    }
    /* End Informe Zeta */

    /* Informe Vendedor */
    public function load_bills() {
        $Sucursal = $this->input->post('Sucursal');
        $group_by_zone = $this->input->post('group_by_zone');
        $group_by_subzone = $this->input->post('group_by_subzone');
        $Vendedor = $this->input->post('Vendedor');
        $Cliente = $this->input->post('Cliente');
        $fech_ini = $this->input->post('start_date') ? $this->input->post('start_date')." 00:00:00" : date('Y-m-01 00:00:00');
        $fech_fin = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:00" : date('Y-m-d 23:59:00');
        $Tipo = $this->input->post('Tipo');
        $zone = $this->input->post('zone');
        $subzone = $this->input->post('subzone');
        // exit($fech_ini);
        $this->data['datos'] = array('Sucursal' => $Sucursal,'Vendedor' => $Vendedor,'Cliente' => $Cliente, 'inicial' => $fech_ini,'Final' => $fech_fin);
        $this->data['billers'] = $this->reports_model->get_biller();
        $this->data['sellers'] = $this->reports_model->get_seller();
        $this->data['tipo'] = $this->input->post('Tipo');
        $this->data['zones'] = $this->site->get_zones();
        $this->data['input'] = array('Tipo' => $Tipo,'users' => 'Usuario de Impresion','fech_ini' => $fech_ini,'fech_fin' => $fech_fin);
        $this->data['sales'] = $this->reports_model->get_seller_bills($Sucursal,$Vendedor,$Cliente,$fech_ini,$fech_fin,$zone,$subzone);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('vendedor')));
        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'sales',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('sales_seller'),
        ]);
        $meta = array('page_title' => lang('vendedor'), 'bc' => $bc);
        $this->page_construct('reports/sales_seller', $meta, $this->data);
    }

    public function search_Seller($biller = '') {
        $this->data['sellers'] = $this->reports_model->get_seller($biller);
        echo json_encode($this->data, true);
    }

    public function search_customer($biller = '',$seller = '') {
        $this->data['sellers'] = $this->reports_model->get_customer($biller,$seller);
        echo json_encode($this->data, true);
    }

    public function search_seller_bills() {
    }

    public function seller_bills_pdf($biller = '',$seller = '',$customer = '',$fech_ini = '',$fech_fin = '',$Tipo = '',$zone = '',$subzone = '',$group_by_zone = '',$group_by_subzone = '') {
        $fech_ini = $fech_ini." 00:00:00";
        $fech_fin = $fech_fin." 23:59:59";
        $this->data['input'] = array('Tipo' => $Tipo,'users' => 'Usuario de Impresion','fech_ini' => $fech_ini,'fech_fin' => $fech_fin,'group_by_zone' => $group_by_zone,'group_by_subzone' => $group_by_subzone);
        $this->data['tipo'] = $Tipo;
        $this->data['sales'] = $this->reports_model->get_seller_bills($biller,$seller,$customer,$fech_ini,$fech_fin,$zone,$subzone);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('vendedor')));
        $meta = array('page_title' => lang('vendedor'), 'bc' => $bc);
        $this->load_view($this->theme.'reports/pdf/sales_seller', $this->data);
    }

    public function seller_bills_xls($biller = '',$seller = '',$customer = '',$fech_ini = '',$fech_fin = '',$Tipo = '',$zone = '',$subzone = '',$group_by_zone = '',$group_by_subzone = '') {
        $fech_ini = $fech_ini." 00:00:00";
        $fech_fin = $fech_fin." 23:59:59";
        $this->data['input'] = array('Tipo' => $Tipo,'users' => 'Usuario de Impresion','fech_ini' => $fech_ini,'fech_fin' => $fech_fin,'group_by_zone' => $group_by_zone,'group_by_subzone' => $group_by_subzone);
        $this->data['tipo'] = $Tipo;
        $this->data['sales'] = $this->reports_model->get_seller_bills($biller,$seller,$customer,$fech_ini,$fech_fin,$zone,$subzone);
        // $this->sma->print_arrays($this->data['sales']);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('vendedor')));
        $meta = array('page_title' => lang('vendedor'), 'bc' => $bc);
        $this->load_view($this->theme.'reports/xls/sales_seller', $this->data);
    }
    /* End Informe Vendedor */

    /* Informe Rentabilidad documento */
    public function load_rentabilidad_doc() {
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_doct();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Rentabilidad_doc')));
        $meta = array('page_title' => lang('Rentabilidad_doc'), 'bc' => $bc);
        $this->page_construct('reports/Rentabilidad_documento', $meta, $this->data);
    }

    public function search_rentabilidad_doc() {
        $tipo_doc = $this->input->post('tipodoc');
        $fech_ini = $this->input->post('start_date') ? $this->sma->fld($this->input->post('start_date')) : '';
        $fech_fin = $this->input->post('end_date') ? $this->sma->fld($this->input->post('end_date')) : '';
        $biller = $this->input->post('biller');
        // exit(var_dump($fech_ini));
        $this->data['profitabilitys_tipo'] = $this->reports_model->get_profitability_doct();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['datos'] = array('tipodoc' => $tipo_doc, 'inicial' => $fech_ini,'Final' => $fech_fin,'biller' => $biller);
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_doct($tipo_doc,$fech_ini,$fech_fin,$biller);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Rentabilidad_doc')));

        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'costing',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('profitability_document'),
        ]);

        $meta = array('page_title' => lang('profitability_document'), 'bc' => $bc);
        $this->page_construct('reports/Rentabilidad_documento', $meta, $this->data);
    }

    public function profitabilitys_export_pdf($tipo_doc,$fech_ini,$fech_fin,$biller) {
        $tipo_doc = $tipo_doc != 'null' ? $tipo_doc : NULL;
        $fech_ini = $fech_ini != 'null' ? $fech_ini : NULL;
        $fech_fin = $fech_fin != 'null' ? $fech_fin : NULL;
        $biller = $biller != 'null' ? $biller : NULL;
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_doct($tipo_doc,$fech_ini,$fech_fin,$biller);
        $this->load_view($this->theme.'reports/pdf/Rentabilidad_documento', $this->data);
    }

    public function profitabilitys_export_xls($tipo_doc,$fech_ini,$fech_fin,$biller) {
        $tipo_doc = $tipo_doc != 'null' ? $tipo_doc : NULL;
        $fech_ini = $fech_ini != 'null' ? $fech_ini : NULL;
        $fech_fin = $fech_fin != 'null' ? $fech_fin : NULL;
        $biller = $biller != 'null' ? $biller : NULL;
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_doct($tipo_doc,$fech_ini,$fech_fin,$biller);
        $this->load_view($this->theme.'reports/xls/Rentabilidad_documento', $this->data);
    }
    /* End Rentabilidad documento */

    /* Informe Rentabilidad cliente */
    public function load_rentabilidad_customer() {
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_customer();
        $this->data['customers'] = $this->reports_model->get_cod_customer();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Rentabilidad_customer')));
        $meta = array('page_title' => lang('Rentabilidad_customer'), 'bc' => $bc);
        $this->page_construct('reports/Rentabilidad_customer', $meta, $this->data);
    }

    public function search_rentabilidad_customer() {
        $Cliente = $this->input->post('Cliente');
        $fech_ini = $this->input->post('start_date') ? $this->sma->fld($this->input->post('start_date')) : '';
        $fech_fin = $this->input->post('end_date') ? $this->sma->fld($this->input->post('end_date')) : '';
        $biller = $this->input->post('biller');
        // exit(var_dump($biller));
        $this->data['datos'] = array('Cliente' => $Cliente, 'inicial' => $fech_ini,'Final' => $fech_fin,'biller' => $biller);
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_customer($Cliente,$fech_ini,$fech_fin,$biller);
        $this->data['customers'] = $this->reports_model->get_cod_customer();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Rentabilidad_customer')));
        $meta = array('page_title' => lang('Rentabilidad_customer'), 'bc' => $bc);
        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'costing',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('profitability_customer'),
        ]);
        $this->page_construct('reports/Rentabilidad_customer', $meta, $this->data);
    }

    public function profitabilitys_customer_export_pdf($customer,$fech_ini,$fech_fin,$biller) {
        $customer = $customer != 'null' ? $customer : NULL;
        $fech_ini = $fech_ini != 'null' ? $fech_ini : NULL;
        $fech_fin = $fech_fin != 'null' ? $fech_fin : NULL;
        $biller = $biller != 'null' ? $biller : NULL;
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_detall_customer($customer,$fech_ini,$fech_fin,$biller);
         $this->load_view($this->theme.'reports/pdf/Rentabilidad_customer',$this->data);
    }

    public function profitabilitys_customer_export_xls($customer,$fech_ini,$fech_fin,$biller) {
        $customer = $customer != 'null' ? $customer : NULL;
        $fech_ini = $fech_ini != 'null' ? $fech_ini : NULL;
        $fech_fin = $fech_fin != 'null' ? $fech_fin : NULL;
        $biller = $biller != 'null' ? $biller : NULL;
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_detall_customer($customer,$fech_ini,$fech_fin,$biller);
         $this->load_view($this->theme.'reports/xls/Rentabilidad_customer',$this->data);
    }
    /* End Rentabilidad cliente */

    /* Informe Rentabilidad Producto */
    public function load_rentabilidad_producto() {
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_producto();
        $this->data['productos'] = $this->reports_model->get_cod_producto_sale();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Rentabilidad_doc')));
        $meta = array('page_title' => lang('profitability_product'), 'bc' => $bc);
        $this->page_construct('reports/Rentabilidad_producto', $meta, $this->data);
    }

    public function search_rentabilidad_producto() {
        $producto = $this->input->post('Producto');
        $fech_ini = $this->input->post('start_date') ? $this->sma->fld($this->input->post('start_date')) : '';
        $fech_fin = $this->input->post('end_date') ? $this->sma->fld($this->input->post('end_date')) : '';
        $biller = $this->input->post('biller');
        // exit(var_dump($fech_ini));
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['datos'] = array('Producto' => $producto, 'inicial' => $fech_ini,'Final' => $fech_fin,'biller' => $biller);
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_producto($producto, $fech_ini, $fech_fin, $biller);
        $this->data['productos'] = $this->reports_model->get_cod_producto_sale();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Rentabilidad_doc')));
        $meta = array('page_title' => lang('profitability_product'), 'bc' => $bc);
        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 4,
            'table_name' => 'costing',
            'record_id' => null,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => $this->m,
            'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('profitability_product'),
        ]);
        $this->page_construct('reports/Rentabilidad_producto', $meta, $this->data);
    }

    public function profitabilitys_producto_export_pdf($producto,$fech_ini,$fech_fin,$biller) {
        $producto = $producto != 'null' ? $producto : NULL;
        $fech_ini = $fech_ini != 'null' ? $fech_ini : NULL;
        $fech_fin = $fech_fin != 'null' ? $fech_fin : NULL;
        $biller = $biller != 'null' ? $biller : NULL;
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_detall_producto($producto,$fech_ini,$fech_fin,$biller);
         $this->load_view($this->theme.'reports/pdf/Rentabilidad_producto', $this->data);
    }

    public function profitabilitys_producto_export_xls($producto,$fech_ini,$fech_fin,$biller) {
        if ($fech_ini != 'null' && $fech_ini != 'NaN') {
            $fechaInicial = date('Y-m-d H:i:s', $fech_ini);
        }else{
            $fechaInicial = null;
        }

        if ($fech_fin != 'null' && $fech_fin != 'NaN') {
            $fechaFinal = date('Y-m-d H:i:s', $fech_fin);
        }else{
            $fechaFinal = null;
        }

        $producto = $producto != 'null' ? $producto : NULL;
        $biller = $biller != 'null' ? $biller : NULL;
        $this->data['profitabilitys'] = $this->reports_model->get_profitability_producto_xls($producto,$fechaInicial,$fechaFinal,$biller);
        $this->load_view($this->theme.'reports/xls/Rentabilidad_producto',$this->data);
    }
    /* End Rentabilidad Producto */


    //JAIME INFORME CUENTAS POR PAGAR

    public function debts_to_pay_report() {

        $xls = $this->input->post('action') == 'xls' ? true : false;
        $pdf = $this->input->post('action') == 'pdf' ? true : false;
        $provider = $this->input->post('Proveedor');
        $biller = $this->input->post('Sucursal');
        $end_date = $this->input->post('end_date');
        $select_due_days_date = $this->input->post('select_due_days_date');
        $this->data['view_summary'] = $this->input->post('view_summary') ? true : false;
        $this->data['datos'] = array('provider' => $provider,  'biller' => $biller);
        $this->data['providers'] = $this->reports_model->get_provider();
        $this->data['select_due_days_date'] = $select_due_days_date;
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['purchases'] = $this->reports_model->get_purchases($provider, $biller, $end_date);
        $this->data['default_biller'] = $this->site->getCompanyByID($this->Settings->default_biller);


        if ($xls) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó XLS '.lang('debts_to_pay_report'),
            ]);
            $this->load_view($this->theme.'reports/xls/debts_to_pay_report_xls', $this->data);
        } else if ($pdf) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó PDF '.lang('debts_to_pay_report'),
            ]);

            $this->load_view($this->theme.'reports/pdf/debts_to_pay_report_pdf', $this->data);
        } else {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'purchases',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('debts_to_pay_report'),
            ]);

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('debts_to_pay_report')));
            $meta = array('page_title' => lang('debts_to_pay_report'), 'bc' => $bc);
            $this->page_construct('reports/debts_to_pay_report', $meta, $this->data);
        }

    }

    public function get_provider($provider = '') {
        $data['providers'] = $this->reports_model->get_provider();
        echo json_encode($data, true);
    }

    //JAIME INFORME CUENTAS POR PAGAR


    //INFORMES JAIME FIN

    public function collection_commissions(){
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['sellers'] = $this->site->getAllCompanies('seller');
        $this->data['countries'] = $this->site->getCountries();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('collection_commissions_report')));
        $meta = array('page_title' => lang('collection_commissions_report'), 'bc' => $bc);
        $this->page_construct('reports/collection_commissions', $meta, $this->data);
    }

    public function getCollectionCommisions($pdf = NULL, $xls = NULL)
    {

        $biller = $this->input->post_get('biller') ? $this->input->post_get('biller') : NULL;
        $commision_status = $this->input->post_get('commision_status');
        $seller = $this->input->post_get('seller') ? $this->input->post_get('seller') : NULL;
        $date_select_collection_commissions = $this->input->post_get('date_select_collection_commissions') ? $this->input->post_get('date_select_collection_commissions') : NULL;

        if ($this->input->post_get('start_date')) {
             $start_date = $this->input->post_get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
                $start_date = date('Y-m-01 00:00');
        }

        if ($this->input->post_get('end_date')) {
             $end_date = $this->input->post_get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
                $end_date =date('Y-m-d H:i');
        }
        $this->load->library('datatables');
        if ($pdf || $xls) {
            $this->datatables
            ->select("
                        {$this->db->dbprefix('sales')}.*,
                        companies.name as name_seller,
                        GROUP_CONCAT({$this->db->dbprefix('payments')}.comm_payment_reference_no) as comm_payment_reference_no,
                        payments.comm_payment_status,
                        COALESCE(rtable.return_amount, 0) AS return_sale_total,
                        MAX({$this->db->dbprefix('payments')}.comm_perc) as comm_perc,
                        SUM(COALESCE({$this->db->dbprefix('payments')}.comm_base, 0)) as comm_base,
                        SUM(COALESCE({$this->db->dbprefix('payments')}.comm_amount, 0)) as comm_amount,
                        SUM(IF({$this->db->dbprefix('payments')}.paid_by != 'discount', {$this->db->dbprefix('payments')}.amount, 0)) as total_payment,
                        SUM(IF({$this->db->dbprefix('payments')}.paid_by = 'discount', {$this->db->dbprefix('payments')}.amount, 0)) as total_other_discounts
                    ");
        } else {
            $this->datatables
            ->select("
                        GROUP_CONCAT({$this->db->dbprefix('payments')}.id) as sale_id,
                        {$this->db->dbprefix('sales')}.reference_no,
                        companies.name as name_seller,
                        DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date,
                        CONCAT({$this->db->dbprefix('sales')}.id, '_', '".rawurlencode(date('Y-m-d H:i:s', strtotime($start_date)))."', '_', '".rawurlencode(date('Y-m-d H:i:s', strtotime($end_date)))."') as sale_id_2,
                        0 as days,
                        MAX({$this->db->dbprefix('payments')}.comm_perc) as comm_perc,
                        SUM(COALESCE({$this->db->dbprefix('payments')}.comm_base, 0)) as comm_base,
                        SUM(COALESCE({$this->db->dbprefix('payments')}.comm_amount, 0)) as comm_amount,
                        GROUP_CONCAT(DISTINCT {$this->db->dbprefix('payments')}.comm_payment_reference_no) as comm_payment_reference_no,
                        GROUP_CONCAT({$this->db->dbprefix('payments')}.comm_payment_status, '_') as payment_status,
                        {$this->db->dbprefix('sales')}.grand_total,
                        SUM(COALESCE({$this->db->dbprefix('payments')}.amount, 0)),
                        {$this->db->dbprefix('sales')}.rete_fuente_total,
                        {$this->db->dbprefix('sales')}.rete_iva_total,
                        {$this->db->dbprefix('sales')}.rete_ica_total,
                        {$this->db->dbprefix('sales')}.rete_other_total
                    ");
        }

            $this->datatables
                    ->from('sales')
                    ->join('payments', 'payments.sale_id=sales.id', 'inner')
                    ->join('companies', 'companies.id=sales.seller_id', 'inner')
                    ->join('(SELECT sale_id, SUM(COALESCE(grand_total, 0)) as return_amount FROM '.$this->db->dbprefix("sales").' WHERE sale_id IS NOT NULL GROUP BY sale_id) AS rtable', 'rtable.sale_id = sales.id', 'left')
                    // ->where('payments.paid_by != "due"')
                    // ->where('payments.paid_by != "retencion"')
                    // ->where('payments.comm_amount != 0')
                    // ->where('payments.comm_amount IS NOT NULL')
                    ;
            if ($pdf || $xls) {
                $this->datatables->order_by('sales.seller_id ASC')->order_by('sales.customer_id ASC');
            }
            $this->datatables->order_by('sales.date asc')->group_by('sales.id');

        if ($biller) {
            $this->datatables->where('sales.biller_id', $biller);
        }

        if ($seller) {
            $this->datatables->where('sales.seller_id', $seller);
        }

        if ($this->Settings->commision_payment_method == 1) {
            // $this->datatables->where('sales.payment_status', 'paid');
        }

        if ($date_select_collection_commissions == 4) {
            $this->datatables->where('payments.comm_payment_status = 1');
        } else {
            if ($commision_status == 'only_pending') {
                $this->datatables->where('payments.comm_payment_status = 0');
            } else if ($commision_status == 'only_paid') {
                $this->datatables->where('payments.comm_payment_status = 1');
            } else if ($commision_status == 'all') {
                // code...
            }
        }

        if ($start_date) {
            if ($date_select_collection_commissions == 1) {
                $this->datatables->where($this->db->dbprefix('payments').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            } else if ($date_select_collection_commissions == 2) {
                $this->datatables->where($this->db->dbprefix('payments').'.payment_date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            } else if ($date_select_collection_commissions == 3) {
                $this->datatables->where($this->db->dbprefix('sales').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            } else if ($date_select_collection_commissions == 4) {
                $this->datatables->where($this->db->dbprefix('payments').'.comm_payment_date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
        }

        $this->datatables->group_by($this->db->dbprefix('sales').'.id');

        if ($this->Settings->commision_payment_method == 1 && $date_select_collection_commissions != 4) {
            if ($commision_status == 'only_pending') {
                $this->datatables->having("SUM(COALESCE({$this->db->dbprefix('payments')}.amount, 0)) = COALESCE({$this->db->dbprefix('sales')}.grand_total, 0) AND SUM(COALESCE(IF({$this->db->dbprefix('payments')}.comm_payment_status = 0, {$this->db->dbprefix('payments')}.comm_amount, 0), 0)) != 0");
            } else {
                $this->datatables->having("SUM(COALESCE({$this->db->dbprefix('payments')}.amount, 0)) = {$this->db->dbprefix('sales')}.grand_total AND SUM(COALESCE({$this->db->dbprefix('payments')}.comm_amount, 0)) != 0");
            }
        }

        //PROBLEMA CUÁNDO SE HACE RETENCIONES EN LOS RC Y ESOS MONTOS SE UPDATEAN EN SMA_SALES, HAY QUE DIFERENCIAR CUÁNDO LA RETENCION ES POR RC Y NO RESTARLA AL GRAND TOTAL

        if ($pdf) {
            $sales = $this->datatables->get_display_result_2();
            $this->data['print_logo'] = true;

            if ($biller) {
                $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $biller);
                $this->data['print_logo'] = $this->site->biller_company_logo($biller);
            } else {
                $this->data['biller'] = false;
            }
            if ($seller) {
                $this->data['seller'] = $this->site->getAllCompaniesWithState('seller', $seller);
            } else {
                $this->data['seller'] = false;
            }
            $this->data['start_date'] = $start_date;
            $this->data['end_date'] = $end_date;
            $this->data['sales'] = $sales;
            $this->data['settings'] = $this->Settings;
            $this->data['sma'] = $this->sma;

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'payments',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó PDF '.lang('collection_commissions_report'),
            ]);
            $this->load_view($this->theme.'reports/pdf/collection_commissions', $this->data);
        } else if ($xls) {

            $data = $this->datatables->get_display_result_2();

            if (!empty($data)) {

                $total_base = 0;
                $total_comm = 0;
                // $this->sma->print_arrays($sales);
                $seller = 0;
                $seller_liq = 0;
                $customer = 0;
                $customer_liq = 0;

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', 'Número de Factura');
                $this->excel->getActiveSheet()->SetCellValue('B1', 'Valor sin descuento');
                $this->excel->getActiveSheet()->SetCellValue('C1', 'Valor del descuento');
                $this->excel->getActiveSheet()->SetCellValue('D1', 'Valor IVA');
                $this->excel->getActiveSheet()->SetCellValue('E1', 'Valor Factura');
                $this->excel->getActiveSheet()->SetCellValue('F1', 'Valor Recaudo');
                $this->excel->getActiveSheet()->SetCellValue('G1', 'Notas Crédito');
                $this->excel->getActiveSheet()->SetCellValue('H1', 'Otros descuentos');
                $this->excel->getActiveSheet()->SetCellValue('I1', 'Base liquidación');
                $this->excel->getActiveSheet()->SetCellValue('J1', 'Porc. liquidación');
                $this->excel->getActiveSheet()->SetCellValue('K1', 'Valor liquidado');
                $this->excel->getActiveSheet()->SetCellValue('L1', 'Estado liquidación');
                $row = 2;

                foreach ($data as $sale) {
                    if ($seller == 0 || ($seller != $sale->seller_id)) {
                        if ($seller == 0) {
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sale->name_seller);
                            $this->excel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray(
                                array( 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'a6deff')) )
                                );
                            $row++;
                        }
                        if ($customer_liq > 0) {
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, 'Liquidado cliente');
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($customer_liq));
                            $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                            $row++;
                            $customer_liq = 0;
                            $customer = 0;
                        }
                        if ($seller_liq > 0) {
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, 'Liquidado vendedor');
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($seller_liq));
                            $this->excel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray(
                                array( 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'a6deff')) )
                                );
                            $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                            $row++;
                            $seller_liq = 0;
                        }
                        if ($seller != 0) {
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sale->name_seller);
                            $this->excel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray(
                                array( 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'a6deff')) )
                                );
                            $row++;
                        }
                        $seller = $sale->seller_id;
                    }
                    if ($customer == 0 || ($customer != $sale->customer_id)) {
                        $customer = $sale->customer_id;
                        if ($customer_liq > 0) {
                            $this->excel->getActiveSheet()->SetCellValue('J' . $row, 'Liquidado cliente');
                            $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($customer_liq));
                            $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                            $row++;
                            $customer_liq = 0;
                        }
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sale->customer);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, ($sale->reference_no));
                    $this->excel->getActiveSheet()->SetCellValue('B'.$row, (($sale->total + $sale->total_discount)));
                    $this->excel->getActiveSheet()->SetCellValue('C'.$row, (($sale->total_discount)));
                    $this->excel->getActiveSheet()->SetCellValue('D'.$row, (($sale->total_tax)));
                    $this->excel->getActiveSheet()->SetCellValue('E'.$row, (($sale->grand_total)));
                    $this->excel->getActiveSheet()->SetCellValue('F'.$row, (($sale->total_payment + $sale->return_sale_total)));
                    $this->excel->getActiveSheet()->SetCellValue('G'.$row, (($sale->return_sale_total)));
                    $this->excel->getActiveSheet()->SetCellValue('H'.$row, (($sale->total_other_discounts)));
                    $this->excel->getActiveSheet()->SetCellValue('I'.$row, (($sale->comm_base)));
                    $this->excel->getActiveSheet()->SetCellValue('J'.$row, (($sale->comm_perc)));
                    $this->excel->getActiveSheet()->SetCellValue('K'.$row, (($sale->comm_amount)));
                    $this->excel->getActiveSheet()->SetCellValue('L'.$row, (($sale->comm_payment_status == 1 ? lang('paid') : lang('pending'))));
                    $row++;
                    $total_base += $sale->comm_base;
                    $total_comm += $sale->comm_amount;
                    $customer_liq += $sale->comm_amount;
                    $seller_liq += $sale->comm_amount;
                }
                if ($customer_liq > 0) {
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, 'Liquidado cliente');
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($customer_liq));
                    $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                    $row++;
                }

                if ($seller_liq > 0) {
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, 'Liquidado vendedor');
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($seller_liq));
                    $this->excel->getActiveSheet()->getStyle('A'.$row.':K'.$row)->applyFromArray(array( 'fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => array('rgb' => 'a6deff'))));
                    $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                    $row++;
                }

                $this->excel->getActiveSheet()->SetCellValue('H' . $row, 'Totales');
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, ($total_base));
                $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($total_comm));
                $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($total_comm));

                $this->excel->getActiveSheet()->getStyle("I" . $row . ":K" . $row)->getBorders()->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
                $this->excel->getActiveSheet()->getStyle('B2:K'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $range = 'A1:A'.$row;
                $this->excel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );

                $filename = 'sales_report';
                $this->load->helper('excel');

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'payments',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó XLS '.lang('collection_commissions_report'),
                ]);
                create_excel($this->excel, $filename);

            }
        } else {
            echo $this->datatables->generate();

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'payments',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('collection_commissions_report'),
            ]);
        }

    }

    function get_movements($company_id)
    {
        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->sma->fld($start_date);
        } else {
            $start_date = NULL;
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            $end_date = NULL;
        }

        $address = $this->input->post('address');
        $saldo_SI = $this->reports_model->get_movements_SI($company_id, $address, $start_date);
        $result = $this->reports_model->get_movements($company_id, $address, $start_date, $end_date);
        if ($result) {
            $html = '';
            $saldo = 0;
            foreach ($result as $row) {

                if ($row->incoming > 0) {
                    if ($row->outcoming > 0) {
                        $saldo -= $row->outcoming;
                    }
                    $saldo += $row->incoming;
                } else if($row->outcoming > 0) {
                    $saldo -= $row->outcoming;
                }

                $html .= '<tr>
                            <td>'.$row->date.'</td>
                            <td>'.$row->reference_no.'</td>
                            <td>'.$row->sucursal.'</td>
                            <td>'.$this->sma->formatMoney($row->incoming).'</td>
                            <td>'.$this->sma->formatMoney($row->outcoming).'</td>
                            <td>'.$this->sma->formatMoney($saldo).'</td>
                          </tr>';
            }
            $data = [
                        'balance' => $saldo_SI,
                        'html' => $html
                    ];
            echo json_encode($data);
        } else {
            $html = '<tr><td colspan="6">Sin resultados</td></tr>';
            $data = [
                        'balance' => 0,
                        'html' => $html
                    ];
            echo json_encode($data);
        }
    }

    function get_supplier_movements($company_id)
    {
        if ($this->input->post('start_date')) {
             $start_date = $this->input->post('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            // if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            // } else {
            //     $start_date = date('Y-m-01 00:00');
            // }
        }

        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            // if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            // } else {
            //     $end_date = date('Y-m-d H:i');
            // }
        }

        $saldo_SI = $this->reports_model->get_supplier_movements_SI($company_id, $start_date);
        $result = $this->reports_model->get_supplier_movements($company_id, $start_date, $end_date);
        if ($result) {
            $html = '';
            $saldo = 0;
            foreach ($result as $row) {

                if ($row->incoming > 0) {
                    $saldo += $row->incoming;
                } else if($row->outcoming > 0) {
                    $saldo -= $row->outcoming;
                }

                $html .= '<tr>
                            <td>'.$row->date.'</td>
                            <td>'.$row->reference_no.'</td>
                            <td>'.$this->sma->formatMoney($row->incoming).'</td>
                            <td>'.$this->sma->formatMoney($row->outcoming).'</td>
                            <td>'.$this->sma->formatMoney($saldo).'</td>
                          </tr>';
            }
            $data = [
                        'balance' => $saldo_SI,
                        'html' => $html
                    ];
            echo json_encode($data);
        } else {
            $html = '<tr><td colspan="5">Sin resultados</td></tr>';
            $data = [
                        'balance' => 0,
                        'html' => $html
                    ];
            echo json_encode($data);
        }
    }

    public function get_xls_tax_inv_detail($data, $type){
        $data_tax_detail = false;
        $data_tax_detail_totals = false;
        $tax_detail = false;
        $data_retention_detail = false;
        $retention_detail = false;
        foreach ($data as $inv) {
            $items  = [];
            if ($inv->rete_fuente_total > 0 && $inv->grand_total > 0) {
                if (!isset($data_retention_detail['normal']['fuente'][$inv->id][$inv->rete_fuente_percentage])) {
                    $data_retention_detail['normal']['fuente'][$inv->id][$inv->rete_fuente_percentage] = $inv->rete_fuente_total;
                } else {
                    $data_retention_detail['normal']['fuente'][$inv->id][$inv->rete_fuente_percentage] += $inv->rete_fuente_total;
                }

                if (!isset($retention_detail['normal']['fuente'][$inv->rete_fuente_percentage])) {
                    $retention_detail['normal']['fuente'][$inv->rete_fuente_percentage] = $inv->rete_fuente_percentage;
                }
            } else if ($inv->rete_fuente_total > 0 && $inv->grand_total < 0) {
                if (!isset($data_retention_detail['return']['fuente'][$inv->id][$inv->rete_fuente_percentage])) {
                    $data_retention_detail['return']['fuente'][$inv->id][$inv->rete_fuente_percentage] = $inv->rete_fuente_total;
                } else {
                    $data_retention_detail['return']['fuente'][$inv->id][$inv->rete_fuente_percentage] += $inv->rete_fuente_total;
                }

                if (!isset($retention_detail['return']['fuente'][$inv->rete_fuente_percentage])) {
                    $retention_detail['return']['fuente'][$inv->rete_fuente_percentage] = $inv->rete_fuente_percentage;
                }
            }

            if ($inv->rete_iva_total > 0 && $inv->grand_total > 0) {
                if (!isset($data_retention_detail['normal']['iva'][$inv->id][$inv->rete_iva_percentage])) {
                    $data_retention_detail['normal']['iva'][$inv->id][$inv->rete_iva_percentage] = $inv->rete_iva_total;
                } else {
                    $data_retention_detail['normal']['iva'][$inv->id][$inv->rete_iva_percentage] += $inv->rete_iva_total;
                }

                if (!isset($retention_detail['normal']['iva'][$inv->rete_iva_percentage])) {
                    $retention_detail['normal']['iva'][$inv->rete_iva_percentage] = $inv->rete_iva_percentage;
                }
            } else if ($inv->rete_iva_total > 0 && $inv->grand_total < 0) {
                if (!isset($data_retention_detail['return']['iva'][$inv->id][$inv->rete_iva_percentage])) {
                    $data_retention_detail['return']['iva'][$inv->id][$inv->rete_iva_percentage] = $inv->rete_iva_total;
                } else {
                    $data_retention_detail['return']['iva'][$inv->id][$inv->rete_iva_percentage] += $inv->rete_iva_total;
                }

                if (!isset($retention_detail['return']['iva'][$inv->rete_iva_percentage])) {
                    $retention_detail['return']['iva'][$inv->rete_iva_percentage] = $inv->rete_iva_percentage;
                }
            }

            if ($inv->rete_ica_total > 0 && $inv->grand_total > 0) {
                if (!isset($data_retention_detail['normal']['ica'][$inv->id][$inv->rete_ica_percentage])) {
                    $data_retention_detail['normal']['ica'][$inv->id][$inv->rete_ica_percentage] = $inv->rete_ica_total;
                } else {
                    $data_retention_detail['normal']['ica'][$inv->id][$inv->rete_ica_percentage] += $inv->rete_ica_total;
                }

                if (!isset($retention_detail['normal']['ica'][$inv->rete_ica_percentage])) {
                    $retention_detail['normal']['ica'][$inv->rete_ica_percentage] = $inv->rete_ica_percentage;
                }
            } else if ($inv->rete_ica_total > 0 && $inv->grand_total < 0) {
                if (!isset($data_retention_detail['return']['ica'][$inv->id][$inv->rete_ica_percentage])) {
                    $data_retention_detail['return']['ica'][$inv->id][$inv->rete_ica_percentage] = $inv->rete_ica_total;
                } else {
                    $data_retention_detail['return']['ica'][$inv->id][$inv->rete_ica_percentage] += $inv->rete_ica_total;
                }

                if (!isset($retention_detail['return']['ica'][$inv->rete_ica_percentage])) {
                    $retention_detail['return']['ica'][$inv->rete_ica_percentage] = $inv->rete_ica_percentage;
                }
            }

            if ($inv->rete_other_total > 0 && $inv->grand_total > 0) {
                if (!isset($data_retention_detail['normal']['other'][$inv->id][$inv->rete_other_percentage])) {
                    $data_retention_detail['normal']['other'][$inv->id][$inv->rete_other_percentage] = $inv->rete_other_total;
                } else {
                    $data_retention_detail['normal']['other'][$inv->id][$inv->rete_other_percentage] += $inv->rete_other_total;
                }

                if (!isset($retention_detail['normal']['other'][$inv->rete_other_percentage])) {
                    $retention_detail['normal']['other'][$inv->rete_other_percentage] = $inv->rete_other_percentage;
                }
            } else if ($inv->rete_other_total > 0 && $inv->grand_total < 0) {
                if (!isset($data_retention_detail['return']['other'][$inv->id][$inv->rete_other_percentage])) {
                    $data_retention_detail['return']['other'][$inv->id][$inv->rete_other_percentage] = $inv->rete_other_total;
                } else {
                    $data_retention_detail['return']['other'][$inv->id][$inv->rete_other_percentage] += $inv->rete_other_total;
                }

                if (!isset($retention_detail['return']['other'][$inv->rete_other_percentage])) {
                    $retention_detail['return']['other'][$inv->rete_other_percentage] = $inv->rete_other_percentage;
                }
            }

            if ($type == 1) {
                $items = $this->site->getAllSaleItems($inv->id);
            } else {
                $items = $this->site->getAllPurchaseItems($inv->id);
            }

            $items_ico = false;

            if ($items) {
                foreach ($items as $item) {
                    $net_amount = isset($item->net_unit_price) ? $item->net_unit_price : $item->net_unit_cost;
                    if ($item->quantity != 0) {

                        if (isset($data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id][$item->tax_rate_id])) {
                            $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id][$item->tax_rate_id]['tax_base'] += ($net_amount * $item->quantity);
                            $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id][$item->tax_rate_id]['tax_amount'] += $item->item_tax;
                        } else {
                            $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id][$item->tax_rate_id]['tax_base'] = ($net_amount * $item->quantity);
                            $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id][$item->tax_rate_id]['tax_amount'] = $item->item_tax;
                            $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id][$item->tax_rate_id]['tax_name'] = $item->name;
                        }
                        if (isset($data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')][$item->tax_rate_id])) {
                            $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')][$item->tax_rate_id]['base'] += ($net_amount * $item->quantity);
                            $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')][$item->tax_rate_id]['amount'] += $item->item_tax;
                        } else {
                            $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')][$item->tax_rate_id]['base'] = ($net_amount * $item->quantity);
                            $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')][$item->tax_rate_id]['amount'] = $item->item_tax;
                        }

                        if (isset($item->consumption_sales) && $item->consumption_sales && $item->consumption_sales > 0) {
                            $items_ico = true;
                            if (isset($data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico'])) {
                                $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico']['amount'] += ($item->consumption_sales * $item->quantity);
                            } else {
                                $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico']['amount'] = ($item->consumption_sales * $item->quantity);
                                $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico']['base'] = 0;
                            }

                            if (isset($data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico'])) {
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_amount'] += $item->item_tax_2;
                            } else {
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_base'] = 0;
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_amount'] = $item->item_tax_2;
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_name'] = 'ICO';
                            }
                        }

                        if (isset($item->consumption_purchase) && $item->consumption_purchase && $item->consumption_purchase > 0) {
                            $items_ico = true;
                            if (isset($data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico'])) {
                                $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico']['amount'] += ($item->consumption_purchase * $item->quantity);
                            } else {
                                $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico']['amount'] = ($item->consumption_purchase * $item->quantity);
                                $data_tax_detail_totals[($item->quantity > 0 ? 'normal' : 'return')]['ico']['base'] = 0;
                            }

                            if (isset($data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico'])) {
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_amount'] += $item->item_tax_2;
                            } else {
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_base'] = 0;
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_amount'] = $item->item_tax_2;
                                $data_tax_detail[($item->quantity > 0 ? 'normal' : 'return')][$inv->id]['ico']['tax_name'] = 'ICO';
                            }
                        }

                    }

                    if (!isset($tax_detail[$item->tax_rate_id])) {
                        $tax_detail[$item->tax_rate_id] = $item->name;
                    }
                    if ($items_ico) {
                        $tax_detail['ico'] = 'ICO';
                    }
                }
            }
        }
        // $this->sma->print_arrays($data_tax_detail);
        return [
                'data_tax_detail' => $data_tax_detail, 'tax_detail' => $tax_detail, 'data_tax_detail_totals' => $data_tax_detail_totals,
                'data_retention_detail' => $data_retention_detail, 'retention_detail' => $retention_detail
                ];
    }



    function categories2()
    {
        $this->sma->checkPermissions('products');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        if ($this->input->post('start_date')) {
            $dt = "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
        } else {
            $dt = "Till " . $this->input->post('end_date');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => sprintf(lang('categories_report'), lang('categories'))));
        $meta = array('page_title' => sprintf(lang('categories_report'), lang('categories')), 'bc' => $bc);
        $this->page_construct('reports/categories2', $meta, $this->data);
    }

    function getCategoriesReport2($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('products', TRUE);
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $category = $this->input->get('category') ? $this->input->get('category') : NULL;
        $detailed_by_invoice = $this->input->get('detailed_by_invoice') ? true : false;

        if ($this->session->userdata('biller_id')) {
            $biller = $this->session->userdata('biller_id');
        }
        if ($this->session->userdata('warehouse_id')) {
            $warehouse = $this->session->userdata('warehouse_id');
        }

        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d');
            }
        }
        $start_date = $start_date ? $start_date." 00:00:00" : NULL;
        $end_date = $end_date ? $end_date." 23:59:00" : NULL;
        if ($detailed_by_invoice) {
            $conditions = [];
            if ($start_date) {
                $conditions['sales.date >='] = $start_date;
            }
            if ($end_date) {
                $conditions['sales.date <='] = $end_date;
            }
            if ($biller) {
                $conditions['sales.biller_id'] = $biller;
            }
            if ($warehouse) {
                $conditions['sales.warehouse_id'] = $warehouse;
            }
            if ($category) {
                $conditions['products.category_id'] = $category;
            }
            $q = $this->db->select(
                            '
                            categories.name as category_name,
                            subcategory.name as subcategory_name,
                            sales.reference_no,
                            sales.date,
                            products.name as product_name,
                            sale_items.quantity,
                            ('.$this->db->dbprefix("sale_items").'.net_unit_price * '.$this->db->dbprefix("sale_items").'.quantity) as sub_total,
                            item_tax,
                            sale_items.subtotal
                            '
                            )
                    ->from('sale_items')
                    ->join('sales', 'sales.id = sale_items.sale_id', 'left')
                    ->join('products', 'products.id = sale_items.product_id', 'left')
                    ->join('categories', 'categories.id = products.category_id', 'left')
                    ->join('categories as subcategory', 'subcategory.id = products.subcategory_id', 'left')
                    ->where($conditions)
                    ->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            }
        } else {
            // ↓ ↓ ↓ valor por categoría principal ↓ ↓ ↓
            $sp = "(
                    SELECT
                        sp.category_id as category,
                        SUM( COALESCE(si.quantity, 0) ) quantity,
                        SUM( COALESCE(si.subtotal, 0) ) total_sale,
                        SUM( COALESCE(si.net_unit_price, 0) * COALESCE(si.quantity, 0) ) net_total_sale,
                        SUM( COALESCE(si.item_tax, 0) ) item_tax_total
                    FROM {$this->db->dbprefix('products')} sp
                    LEFT JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                    LEFT JOIN " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
            if ($start_date || $warehouse || $biller) {
                $sp .= " WHERE ";
                if ($start_date) {
                    $start_date = $start_date;
                    $end_date = $end_date ? $end_date : date('Y-m-d H:i');
                    $sp .= " s.date >= '{$start_date}' AND s.date <=  '{$end_date}' ";
                    if ($warehouse || $biller) {
                        $sp .= " AND ";
                    }
                }
                if ($warehouse) {
                    $sp .= " si.warehouse_id = '{$warehouse}' ";
                    if ($biller) {
                        $pp .= " AND ";
                        $sp .= " AND ";
                    }
                }
                if ($biller) {
                    $sp .= " s.biller_id = '{$biller}' ";
                }
            }
            $sp .= " GROUP BY sp.category_id ) PSales";
            $this->db
                ->select(
                            $this->db->dbprefix('categories') . ".id as cid,
                            " .$this->db->dbprefix('categories') . ".code,
                            " . $this->db->dbprefix('categories') . ".name,
                            SUM( COALESCE( PSales.quantity, 0 ) ) as quantity,
                            SUM( COALESCE( PSales.net_total_sale, 0 ) ) as net_total_sale,
                            SUM( COALESCE( PSales.item_tax_total, 0 ) ) as item_tax_total,
                            SUM( COALESCE( PSales.total_sale, 0 ) ) as total_sale
                        ", FALSE)
                ->from('categories')
                ->join($sp, 'categories.id = PSales.category', 'left')
                ->where('categories.parent_id', 0)
                ;
            if ($category) {
                $this->db->where('categories.id', $category);
            }
            $this->db->group_by('categories.id, categories.code, categories.name');
            $q = $this->db->get();
            $data = [];
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[$row->cid]['data'] = $row;
                }
            }
            // ↓ ↓ ↓ valor por categoría principal (Productos sin categoría asignada) ↓ ↓ ↓
            $q = $this->db->select('
                                    PSales.category as cid,
                                    "-" as code,
                                    "-" as name,
                                    PSales.quantity,
                                    PSales.net_total_sale,
                                    PSales.item_tax_total,
                                    PSales.total_sale
                                    ')->from($sp)->where('PSales.category = 0')->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[$row->cid]['data'] = $row;
                }
            }
            // ↓ ↓ ↓ valor por subcategoría ↓ ↓ ↓
            $sp = "(
                    SELECT
                        sp.subcategory_id subcategory,
                        SUM( COALESCE(si.quantity, 0) ) quantity,
                        SUM( COALESCE(si.subtotal, 0) ) total_sale,
                        SUM( COALESCE(si.net_unit_price, 0) * COALESCE(si.quantity, 0) ) net_total_sale,
                        SUM( COALESCE(si.item_tax, 0)) item_tax_total,
                        sp.category_id subcategory_parent_id
                    FROM {$this->db->dbprefix('products')} as sp
                    LEFT JOIN " . $this->db->dbprefix('sale_items') . " si ON sp.id = si.product_id
                    LEFT JOIN " . $this->db->dbprefix('sales') . " s ON s.id = si.sale_id ";
            if ($start_date || $warehouse || $biller) {
                $sp .= " WHERE sp.subcategory_id > 0 AND ";
                if ($start_date) {
                    $start_date = $start_date;
                    $end_date = $end_date ? $end_date : date('Y-m-d H:i');
                    $sp .= " s.date >= '{$start_date}' AND s.date <= '{$end_date}' ";
                    if ($warehouse || $biller) {
                        $sp .= " AND ";
                    }
                }
                if ($warehouse) {
                    $sp .= " si.warehouse_id = '{$warehouse}' ";
                    if ($biller) {
                        $sp .= " AND ";
                    }
                }

                if ($biller) {
                    $sp .= " s.biller_id = '{$biller}' ";
                }
            }
            $sp .= " GROUP BY sp.subcategory_id ) PSales";
            $this->db
                ->select(
                            $this->db->dbprefix('categories') . ".id as cid,
                            " .$this->db->dbprefix('categories') . ".parent_id,
                            " .$this->db->dbprefix('categories') . ".code,
                            " . $this->db->dbprefix('categories') . ".name,
                            SUM( COALESCE( PSales.quantity, 0 ) ) as quantity,
                            SUM( COALESCE( PSales.net_total_sale, 0 ) ) as net_total_sale,
                            SUM( COALESCE( PSales.item_tax_total, 0 ) ) as item_tax_total,
                            SUM( COALESCE( PSales.total_sale, 0 ) ) as total_sale
                        ", FALSE)
                ->from('categories')
                ->join($sp, 'categories.id = PSales.subcategory', 'left')
                ->where('categories.parent_id !=', 0)
                ;
            if ($category) {
                $this->db->where('categories.parent_id', $category);
            }
            $this->db->group_by('categories.id, categories.code, categories.name');
            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[$row->parent_id]['subcategories'][] = $row;
                    if ($data[$row->parent_id]['data']) {
                        $data[$row->parent_id]['data']->quantity -= $row->quantity;
                        $data[$row->parent_id]['data']->net_total_sale -= $row->net_total_sale;
                        $data[$row->parent_id]['data']->item_tax_total -= $row->item_tax_total;
                        $data[$row->parent_id]['data']->total_sale -= $row->total_sale;
                    }

                }
            }
            // ↓ ↓ ↓ valor por notas otros conceptos (debito, credito) ↓ ↓ ↓
            $sp = "(
                    SELECT
                        p.id AS product_table_id,
                        MAX( si.product_code ) AS product_code,
                        SUM( COALESCE(si.quantity, 0) ) AS quantity,
                        SUM( COALESCE(si.subtotal, 0) ) AS total_sale,
                        SUM( COALESCE(si.net_unit_price, 0) * COALESCE(si.quantity, 0) ) AS net_total_sale,
                        SUM( COALESCE(si.item_tax, 0) ) AS item_tax_total
                    FROM " . $this->db->dbprefix('sale_items') . " AS si
                    LEFT JOIN " . $this->db->dbprefix('sales') . " AS s ON s.id = si.sale_id
                    LEFT JOIN " . $this->db->dbprefix('products') . " AS p ON p.id = si.product_id
                    ";
            if ($start_date || $warehouse || $biller) {
                $sp .= " WHERE ";
                if ($start_date) {
                    $start_date = $start_date;
                    $end_date = $end_date ? $end_date : date('Y-m-d H:i');
                    $sp .= " s.date >= '{$start_date}' AND s.date <=  '{$end_date}' ";
                    if ($warehouse || $biller) {
                        $sp .= " AND ";
                    }
                }
                if ($warehouse) {
                    $sp .= " si.warehouse_id = '{$warehouse}' ";
                    if ($biller) {
                        $sp .= " AND ";
                    }
                }

                if ($biller) {
                    $sp .= " s.biller_id = '{$biller}' ";
                }
            }
            $sp .= " GROUP BY si.product_code) AS PSales";
            $q = $this->db->select('
                                    "-" as code,
                                    debit_credit_notes_concepts.description as name,
                                    PSales.quantity,
                                    PSales.net_total_sale,
                                    PSales.item_tax_total,
                                    PSales.total_sale
                                    ')
                        ->from($sp)
                        ->join('debit_credit_notes_concepts', 'debit_credit_notes_concepts.id = PSales.product_code')
                        ->where('PSales.product_table_id IS NULL')
                        ->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    if ($row->quantity > 0) {
                        $data['debit_other_concepts']['data'] = $row;
                    } else {
                        $data['return_other_concepts']['data'] = $row;
                    }
                }
            }
        }
        if ($pdf || $xls) {

            if (!empty($data)) {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('categories_report_xls_title'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('category'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('subcategory'));
                $letra = 'C';
                if ($detailed_by_invoice) {
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('reference_no'));
                    $letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('date'));
                    $letra++;
                    $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('product_name'));
                    $letra++;
                }
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('quantity'));
                $letra++;
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('subtotal'));
                $letra++;
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('total_tax_amount'));
                $letra++;
                $this->excel->getActiveSheet()->SetCellValue($letra.'1', lang('total'));
                $letra++;

                $row = 2;
                // $this->sma->print_arrays($data);
                foreach ($data as $data_row) {
                    if ($detailed_by_invoice) {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->category_name);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->subcategory_name);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->date);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->product_name);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->quantity);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->sub_total);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->item_tax);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->subtotal);
                        $row++;
                    } else {
                        if (isset($data_row['subcategories'])) {
                            foreach ($data_row['subcategories'] as $subcategory) {
                                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row['data']->name);
                                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $subcategory->name);
                                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $subcategory->quantity);
                                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $subcategory->net_total_sale);
                                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $subcategory->item_tax_total);
                                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $subcategory->total_sale);
                                $row++;
                            }
                        } else {
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row['data']->name);
                            $this->excel->getActiveSheet()->SetCellValue('B' . $row, 'N/A');
                            $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row['data']->quantity);
                            $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row['data']->net_total_sale);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row['data']->item_tax_total);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row['data']->total_sale);
                            $row++;
                        }
                    }
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'categories_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            $html = '';
            if ($detailed_by_invoice) {
                foreach ($data as $item) {
                    $html .='<tr>
                                <td>'.$item->category_name.'</td>
                                <td>'.$item->subcategory_name.'</td>
                                <td>'.$item->reference_no.'</td>
                                <td>'.$item->date.'</td>
                                <td>'.$item->product_name.'</td>
                                <td>'.$item->quantity.'</td>
                                <td>'.$item->sub_total.'</td>
                                <td>'.$item->item_tax.'</td>
                                <td>'.$item->subtotal.'</td>
                              </tr>';
                }
            } else {
                foreach ($data as $row) {
                    if ($row['data']->total_sale > 0 && isset($row['subcategories'])) {
                        $html .='<tr>
                                    <td>'.$row['data']->name.'</td>
                                    <td> - </td>
                                    <td>'.$row['data']->quantity.'</td>
                                    <td>'.$row['data']->net_total_sale.'</td>
                                    <td>'.$row['data']->item_tax_total.'</td>
                                    <td>'.$row['data']->total_sale.'</td>
                                  </tr>';
                    }
                    if (isset($row['subcategories'])) {
                        foreach ($row['subcategories'] as $subcategory) {
                            $html .='<tr>
                                        <td>'.$row['data']->name.'</td>
                                        <td>'.$subcategory->name.'</td>
                                        <td>'.$subcategory->quantity.'</td>
                                        <td>'.$subcategory->net_total_sale.'</td>
                                        <td>'.$subcategory->item_tax_total.'</td>
                                        <td>'.$subcategory->total_sale.'</td>
                                      </tr>';
                        }
                    } else {
                        $html .='<tr>
                                    <td>'.$row['data']->name.'</td>
                                    <td> - </td>
                                    <td>'.$row['data']->quantity.'</td>
                                    <td>'.$row['data']->net_total_sale.'</td>
                                    <td>'.$row['data']->item_tax_total.'</td>
                                    <td>'.$row['data']->total_sale.'</td>
                                  </tr>';
                    }
                }
            }
            echo $html;
        }

    }

    function closed_register_details()
    {
        $this->sma->checkPermissions('register');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('close_register_detailed')));
        $meta = array('page_title' => lang('close_register_detailed'), 'bc' => $bc);
        $this->page_construct('reports/closed_register_details', $meta, $this->data);
    }

    public function close_register_detailed($register_id = NULL)
    {
        $this->sma->checkPermissions('index');
        $user_register = $this->pos_model->registerData(NULL, $register_id);
        $user_id = $user_register ? $user_register->user_id : NULL;
        $this->data['cash_in_hand'] = $user_register ? $user_register->cash_in_hand : NULL;
        $this->data['register_open_time'] = $register_date = $user_register ? $user_register->date : NULL;
        $this->data['register_closed_time'] = $this->pos_model->close_register_end_date = $user_register && $user_register->closed_at ? $user_register->closed_at : date('Y-m-d H:i:s');

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $popts = $this->site->getPaidOpts(1,0, true);
        $this->data['popts'] = $popts;

        foreach ($popts as $popt) {
            $this->data['popts_data'][$popt->code]['Sales'] = $this->pos_model->getSalesByPaymentOption($popt->code, $register_date, $user_id, true);
            $this->data['popts_data'][$popt->code]['RC'] = $this->pos_model->getRegisterSalesRCByPaymentOption($popt->code, $register_date, $user_id, true);
            $this->data['popts_data'][$popt->code]['Returned'] = $this->pos_model->getRegisterReturnedSalesByPaymentOption($popt->code, $register_date, $user_id, true);
            $this->data['popts_data'][$popt->code]['payments_collections'] = $this->pos_model->getPaymentsCollectionsByPaymentOption($popt->code, $register_date, $user_id, true);
        }
        $this->data['expenses'] = $this->pos_model->getRegisterExpenses($register_date, $user_id, true);
        $this->data['purchases_expenses'] = $this->pos_model->getRegisterPurchasesExpenses($register_date, $user_id, true);
        $this->data['purchases_expenses_payments'] = $this->pos_model->getRegisterPurchasesExpensesPayments($register_date, $user_id, true);
        $this->data['ppayments'] = $this->pos_model->getPurchasesPayments($register_date, $user_id, true);
        $this->data['customer_deposits'] = $this->pos_model->getRegisterDeposits($register_date, $user_id, true);
        $this->data['supplier_deposits'] = $this->pos_model->getRegisterSupplierDeposits($register_date, $user_id, true);
        $this->data['pos_register_movements'] = $this->pos_model->getRegisterMovements($register_date, $user_id, true);
        $this->data['user_id'] = $user_id;
        $this->data['settings'] = $this->Settings;
        if ($this->session->userdata('biller_id')) {
            $this->data['biller'] = $this->site->getCompanyByID($this->session->userdata('biller_id'));
        } else {
            $this->data['biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
        }
        $this->load_view($this->theme . 'reports/close_register_detailed', $this->data);
    }

    function get_sales_xls_payment_details($data){

        $where_in = [];
        foreach ($data as $inv) {
            $where_in[] = $inv->id;
        }

        $inv_payments = [];
        $q_payments = $this->db->where_in('sale_id', $where_in)->get('payments');
        if ($q_payments->num_rows() > 0) {
            foreach (($q_payments->result()) as $payment) {
                if (!isset($inv_payments[$payment->sale_id])) {
                    $inv_payments[$payment->sale_id] = [];
                }
                $inv_payments[$payment->sale_id][] = $payment;
            }
        }

        $columns_paid_by = [];
        foreach ($data as $inv) {
            $payments = isset($inv_payments[$inv->id]) ? $inv_payments[$inv->id] : NULL;
            if ($payments) {
                foreach ($payments as $payment) {
                    if (!isset($columns_paid_by[$payment->paid_by][$inv->id])) {
                        $columns_paid_by[$payment->paid_by][$inv->id] = $payment->amount;
                    } else {
                        $columns_paid_by[$payment->paid_by][$inv->id] += $payment->amount;
                    }
                }
            }
            if ($inv->grand_total > $inv->paid) {
                $columns_paid_by['due'][$inv->id] = ($inv->grand_total - $inv->paid);
            }
        }
        return $columns_paid_by;
    }

    public function production_order_report()
    {
        $this->form_validation->set_rules('generate_report', 'Generar reporte', 'required');
        if ($this->form_validation->run() === true)
        {
            $data = $this->reports_model->production_order_report();
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('report');
            $this->excel->getActiveSheet()->SetCellValue('A1', lang('brand'));
            $this->excel->getActiveSheet()->SetCellValue('B1', lang('product'));
            $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse_quantity'));
            $this->excel->getActiveSheet()->SetCellValue('D1', lang('cutting_quantity'));
            $this->excel->getActiveSheet()->SetCellValue('E1', lang('assemble_quantity'));
            $this->excel->getActiveSheet()->SetCellValue('F1', lang('packing_quantity'));
            $this->excel->getActiveSheet()->SetCellValue('G1', lang('total_quantity'));
            $row = 2;
            $current_category_name = null;
            $text_center_align = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                )
            );
            // $this->sma->print_arrays($data);
            foreach ($data as $data_row) {
                if ($current_category_name == null || $current_category_name != $data_row->category_name) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->category_name);
                    $this->excel->getActiveSheet()->mergeCells('A'.$row.':G'.$row);
                    $this->excel->getActiveSheet()->getStyle("A".$row)->applyFromArray($text_center_align);
                    $current_category_name = $data_row->category_name;
                    $row++;
                }
                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->brand_name);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->product_code);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $this->sma->formatDecimalNoRound($data_row->wh_quantity, 0));

                $complete_cutting_quantity = ($data_row->complete_cutting_quantity_completed + $data_row->complete_cutting_quantity);
                $complete_assembling_quantity = ($data_row->complete_assembling_quantity_completed + $data_row->complete_assembling_quantity);
                $complete_packing_quantity = ($data_row->complete_packing_quantity_completed + $data_row->complete_packing_quantity);
                if ($complete_packing_quantity > 0) {
                    $complete_assembling_quantity -= $complete_packing_quantity;
                    $complete_cutting_quantity -= $complete_packing_quantity;
                }
                if ($complete_assembling_quantity > 0) {
                    $complete_cutting_quantity -= $complete_assembling_quantity;
                }
                $cutting_quantity = ($data_row->cutting_finished_quantity + $data_row->cutting_quantity);
                $assembling_quantity = ($data_row->assembling_finished_quantity + $data_row->assembling_quantity);
                $packing_quantity = ($data_row->packing_finished_quantity + $data_row->packing_quantity);
                if ($packing_quantity > 0) {
                    $assembling_quantity -= $packing_quantity;
                    $cutting_quantity -= $packing_quantity;
                }
                if ($assembling_quantity > 0) {
                    $cutting_quantity -= $assembling_quantity;
                }
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $this->sma->formatDecimalNoRound(
                    $complete_cutting_quantity
                    , 0)."(".$this->sma->formatDecimalNoRound(
                    $cutting_quantity
                    , 0).")");


                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->formatDecimalNoRound(
                    $complete_assembling_quantity
                    , 0)."(".$this->sma->formatDecimalNoRound(
                    $assembling_quantity
                    , 0).")");
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatDecimalNoRound(
                    ($data_row->complete_packing_quantity)
                    , 0)."(".$this->sma->formatDecimalNoRound(
                    ($data_row->packing_quantity)
                    , 0).")");
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatDecimalNoRound(
                    $data_row->wh_quantity +
                    (($data_row->complete_cutting_quantity_completed + $data_row->complete_cutting_quantity) -
                    ($data_row->complete_assembling_quantity_completed + $data_row->complete_assembling_quantity) -
                    ($data_row->complete_packing_quantity_completed + $data_row->complete_packing_quantity)) +
                    (($data_row->complete_assembling_quantity_completed + $data_row->complete_assembling_quantity) -
                    ($data_row->complete_packing_quantity_completed + $data_row->complete_packing_quantity)) +
                    (($data_row->complete_packing_quantity))
                    , 0));
                $row++;
            }
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $filename = 'production_order_report';
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        }
        $this->sma->checkPermissions('sales');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('production_order_report')));
        $meta = array('page_title' => lang('production_order_report'), 'bc' => $bc);
        $this->page_construct('reports/production_order_report', $meta, $this->data);
    }
    public function portfolio_report_2_generate_temp(){
        $end_date = $this->input->get('end_date');
        $end_month = $this->input->get('end_month');
        $start_month = $this->input->get('start_month');
        $year_to_execute = $this->input->get('year_to_execute');

        $start_date_to_execute = $year_to_execute.$start_month;
        if ($end_date < $start_date_to_execute) {
            echo json_encode(['success' => 1, 'message' => 'Correcto >> '.$end_date. " < ".$start_date_to_execute]);
            exit();
        }
        if ($this->reports_model->create_portfolio_temp_tables($end_date, $year_to_execute, $start_month, $end_month)) {
            echo json_encode(['success' => 1, 'message' => 'Correcto']);
            if (!$this->session->userdata('portfolio_temp_end_date')) {
                $this->session->set_userdata('portfolio_temp_end_date', $end_date);
                $q = $this->db->query("SHOW TABLES LIKE '%sma_portfolio_temp%';");
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row_key => $row_table) {
                        foreach ($row_table as $rt_key => $rt_table) {
                            $this->db->truncate($rt_table);
                        }
                    }
                }
            }
        } else {
            echo json_encode(['success' => 0, 'message' => 'Incorrecto']);
        }
        exit();
    }

    // INFORME DE CANTIDADES POR VARIANTES POR BODEGAS JERSON
    public function warehouse_inventory_variants(){
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['warehouses'] = $this->reports_model->getAllWarehouses();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('warehouse_inventory_variants')));
        $meta = array('page_title' => lang('warehouse_inventory_variants'), 'bc' => $bc);
        $this->page_construct('reports/warehouse_inventory_variants', $meta, $this->data);
    }

    public function getVariants($producto = NULL){
        if ($rows = $this->reports_model->getVariants($producto)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
    }

    public function getWarehouseProducts($bodega = NULL){
        if ($rows = $this->reports_model->getWarehouseProducts($bodega)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
    }

    public function getWarehousesInventoryVariants($xls = NULL){
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $variant = $this->input->get('variant') ? $this->input->get('variant') : NULL;
        $quantity_zero = $this->input->get('quantity_zero') ? $this->input->get('quantity_zero') : NULL;
        $include_products_without_variants = $this->input->get('include_products_without_variants') ? $this->input->get('include_products_without_variants') : NULL;
        $include_products_discontinued = $this->input->get('include_products_discontinued') ? $this->input->get('include_products_discontinued') : NULL;
        $this->load->library('datatables');
        $this->datatables
            ->select("  {$this->db->dbprefix('warehouses')}.name AS warehouseName,
                        {$this->db->dbprefix('products')}.code AS code,
                        UPPER({$this->db->dbprefix('products')}.name) AS name,
                        {$this->db->dbprefix('products')}.discontinued AS status,
                        {$this->db->dbprefix('product_variants')}.code AS codeVariant,
                        {$this->db->dbprefix('product_variants')}.name AS variant,
                        SUM( IF({$this->db->dbprefix('products')}.attributes != 0,
                                {$this->db->dbprefix('warehouses_products_variants')}.quantity,
                                (SELECT quantity FROM {$this->db->dbprefix('warehouses_products')} WHERE product_id = {$this->db->dbprefix('products')}.id AND warehouse_id = {$this->db->dbprefix('warehouses')}.id)
                            )
                        ) AS quantity ")
            ->from($this->db->dbprefix('products'))
            ->join($this->db->dbprefix('warehouses_products_variants'), $this->db->dbprefix('products').'.id = '.$this->db->dbprefix('warehouses_products_variants').'.product_id', 'inner')
            ->join($this->db->dbprefix('product_variants'), $this->db->dbprefix('product_variants').'.id = '.$this->db->dbprefix('warehouses_products_variants').'.option_id','inner')
            ->join($this->db->dbprefix('warehouses'),$this->db->dbprefix('warehouses_products_variants').'.warehouse_id = '.$this->db->dbprefix('warehouses').'.id','inner');
        if ($warehouse) {
            $this->datatables->where($this->db->dbprefix('warehouses_products_variants').'.warehouse_id',$warehouse);
        }
        if ($product) {
            $this->datatables->where($this->db->dbprefix('warehouses_products_variants').'.product_id',$product);
            if ($variant) {
                $this->datatables->where($this->db->dbprefix('warehouses_products_variants').'.option_id',$variant);
            }
        }
        if (!$include_products_without_variants) {
            $this->datatables->where($this->db->dbprefix('products').'.attributes', 1);
        }
        if (!$include_products_discontinued) {
            $this->datatables->where($this->db->dbprefix('products').'.discontinued', 0);
        }
        $this->datatables->group_by($this->db->dbprefix('warehouses_products_variants').'.warehouse_id');
        $this->datatables->group_by($this->db->dbprefix('warehouses_products_variants').'.option_id');
        if ($quantity_zero) {
            $this->datatables->having("quantity != 0");
        }

        if ($xls) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('Inventario bodega por variantes');
            $this->excel->getActiveSheet()->SetCellValue('A1', lang('warehouse'));
            $this->excel->getActiveSheet()->SetCellValue('B1', lang('code'));
            $this->excel->getActiveSheet()->SetCellValue('C1', lang('product'));
            $this->excel->getActiveSheet()->SetCellValue('D1', lang('status'));
            $this->excel->getActiveSheet()->SetCellValue('E1', lang('variant'));
            $this->excel->getActiveSheet()->SetCellValue('F1', lang('variant_code'));
            $this->excel->getActiveSheet()->SetCellValue('G1', lang('product_qty'));
            $row = 2;
            foreach ($data as $data_row) {
                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->warehouseName);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->code);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->name);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, ($data_row->status == 1) ? lang('inactive') : lang('active'));
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->variant);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->codeVariant);
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->quantity);
                $row++;
            }
            $titulos = [
                'font' => [
                    'bold' => true,
                    'size'  => 10,
                    'name' => 'calibrí',
                ],
                'alignment' => [
                    'wrapText' => true,
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ],
                'borders' => [
                    'diagonalDirection' => PHPExcel_Style_Borders::DIAGONAL_BOTH,
                    'allBorders' => [
                        'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
                    ],
                ],
            ];
            $this->excel->getActiveSheet()->getStyle("A1:E1")->applyFromArray($titulos);
            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(25);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('C2:G' . $row)->getAlignment()->setWrapText(true);
            $filename = 'warehouse inventory variants ' . date('Y_m_d_H_i_s');;
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        }
        else{
            echo $this->datatables->generate();
        }
    }

    // FIN INFORME DE CANTIDADES POR VARIANTES POR BODEGAS JERSON
    function purchases_request($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('products');
        $this->form_validation->set_rules('generate_report', 'Generar reporte', 'required');
        if ($this->form_validation->run() === true)
        {
            $this->data['biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
            $print_to = $this->input->post('print_to');
            $biller_id = $this->input->post('biller');
            $wh_data = FALSE;
            if ($biller_id) {
                $wh_data = $this->site->getCompanyByID($biller_id);
            }
            $group_by_category_sub_category = $this->input->post('group_by_category_sub_category');
            $group_by_preferences = $this->input->post('group_by_preferences');
            $print_additional_page = $this->input->post('print_additional_page');
            $data = $this->reports_model->get_purchases_request_report($biller_id, $group_by_preferences);
            if ($print_to == 'xls') {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle('report');
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('quantity_request'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('quantity_stock'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('quantity_to_purchase'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('supplier'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('cost'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('unit_of_measurement'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('quantity_purchased'));
                $this->excel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
                $row = 2;
                $current_category_name = null;
                $category_id = NULL;
                $subcategory_id = NULL;
                foreach ($data as $data_row) {
                    if ($category_id == NULL || $data_row->category_id != $category_id) {
                        $subcategory_id = NULL;
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->category_name);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $category_id = $data_row->category_id;
                        $row++;
                    }
                    if ($data_row->subcategory_id != NULL && ($subcategory_id == NULL || $data_row->subcategory_id != $subcategory_id)) {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->subcategory_name);
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $subcategory_id = $data_row->subcategory_id;
                        $row++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->product_code);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->product_name);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->quantity_request);
                    // $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->brand_name);
                    // $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->brand_name);
                    // $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->brand_name);
                    // $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->brand_name);
                    // $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->brand_name);
                    // $this->excel->getActiveSheet()->SetCellValue('I' . $row, $data_row->brand_name);
                    $row++;
                }
                // $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $range = 'A1:A'.$row;
                $this->excel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                $text_center_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );
                $text_right_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    )
                );
                $this->excel->getActiveSheet()->getStyle("B".$row.":I".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getStyle('C1:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $filename = 'production_order_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            } else if ($print_to == 'pdf') {
                $this->data['data'] = $data;
                $this->data['wh_data'] = $wh_data;
                $this->data['group_by_order'] = isset($group_by_order) ? $group_by_order : NULL;
                $this->data['group_by_preferences'] = $group_by_preferences;
                $this->data['print_additional_page'] = $print_additional_page;
                $this->load_view($this->theme.'reports/pdf/purchases_request_report', $this->data);
            }

        }
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('purchases_request_report')));
        $meta = array('page_title' => lang('purchases_request_report'), 'bc' => $bc);
        $this->page_construct('reports/purchases_request', $meta, $this->data);
    }

    function order_sales($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('products');
        $this->form_validation->set_rules('generate_report', 'Generar reporte', 'required');
        if ($this->form_validation->run() === true)
        {
            $this->data['biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
            $print_to = $this->input->post('print_to');
            $biller_id = $this->input->post('biller');
            $wh_data = FALSE;
            if ($biller_id) {
                $wh_data = $this->site->getCompanyByID($biller_id);
            }
            $filter_data['biller'] = $this->input->post('biller');
            $filter_data['warehouse'] = $this->input->post('warehouse');
            $filter_data['customer'] = $this->input->post('customer');
            $filter_data['seller'] = $this->input->post('seller');
            $filter_data['status'] = $this->input->post('status');
            $filter_data['country'] = $this->input->post('country');
            $filter_data['state'] = $this->input->post('state');
            $filter_data['city'] = $this->input->post('city');
            $filter_data['zone'] = $this->input->post('zone');
            $filter_data['subzone'] = $this->input->post('subzone');
            $filter_data['filter_date'] = $this->input->post('filter_date');
            $filter_data['start_date'] = $this->input->post('start_date') ? $this->sma->fld($this->input->post('start_date')) : NULL;
            $filter_data['end_date'] = $this->input->post('end_date') ? $this->sma->fld($this->input->post('end_date')) : NULL;
            $filter_data['group_by_order'] = $this->input->post('group_by_order');
            $filter_data['group_by_zone'] = $this->input->post('group_by_zone');
            $filter_data['reference_no'] = $this->input->post('reference_no');

            $data = $this->reports_model->get_order_sales_report($filter_data);

            if ($data && $print_to == 'xls') {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle('report');
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('category'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('product_code'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('product_name'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('unit_of_measurement'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('quantity_request'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('quantity_dispatch_billed'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('quantity_dispatch_pending'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('quantity_stock'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('quantity_to_purchase'));
                if ($this->input->post('warehouse')) {
                    $letter = 'J';
                    $warehouses = $this->site->getAllWarehouses();
                    foreach ($warehouses as $warehouse) {
                        if ($warehouse->id == $this->input->post('warehouse')) {
                            continue;
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', $warehouse->name);
                        $letter++;
                    }
                }
                $this->excel->getActiveSheet()->getStyle("A1:M1")->getFont()->setBold(true);
                $row = 2;
                $current_category_name = null;
                $category_id = NULL;
                $subcategory_id = NULL;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->category_name);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->product_code);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->product_name);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->main_unit_code);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->quantity_request);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->quantity_delivered);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->quantity_request - $data_row->quantity_delivered);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->product_quantity);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $this->get_order_sales_quantity_to_purchase($data_row)); //PENDIENTE
                    if ($this->input->post('warehouse')) {
                        $letter = 'J';
                        $wh_qty_data = $this->reports_model->get_other_wh($data_row->product_id, $this->input->post('warehouse'));
                        foreach ($warehouses as $warehouse) {
                            $this->excel->getActiveSheet()->SetCellValue($letter.$row, $wh_qty_data[$warehouse->id]);
                            $letter++;
                        }
                    }

                    $row++;
                }
                // $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $range = 'A1:A'.$row;
                $this->excel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                $text_center_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );
                $text_right_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    )
                );
                $this->excel->getActiveSheet()->getStyle("B".$row.":I".$row)->applyFromArray($text_right_align);
                // $this->excel->getActiveSheet()->getStyle('C1:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                // $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                // $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $filename = 'ordenes_de_pedido_'.date('Y_m_d_H_i_s');
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            } else if ($data && $print_to == 'pdf') {
                $this->data['data'] = $data;
                $this->data['wh_data'] = $wh_data;
                $this->data['group_by_order'] = $filter_data['group_by_order'];
                $this->data['group_by_zone'] = $filter_data['group_by_zone'];

                $this->load_view($this->theme.'reports/pdf/order_sales_report', $this->data);
            }
            if (!$data) {
                $this->session->set_flashdata('error', lang('nothing_found'));
            }

        }
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['sellers'] = $this->site->getAllCompanies('seller');
        $this->data['countries'] = $this->site->getCountries();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('order_sales_report')));
        $meta = array('page_title' => lang('order_sales_report'), 'bc' => $bc);
        $this->page_construct('reports/order_sales', $meta, $this->data);
    }

    function price_groups()
    {
        $this->sma->checkPermissions('products');
        $this->form_validation->set_rules('generate_report', 'Generar reporte', 'required');
        if ($this->form_validation->run() === true)
        {
            $this->data['biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
            $price_group = $this->input->post('price_group');
            $print_to = $this->input->post('print_to');
            $only_products_with_stock = $this->input->post('only_products_with_stock');
            $view_products_discontinued = $this->input->post('view_products_discontinued');
            $only_view_products_featured = $this->input->post('only_view_products_featured');
            $show_related_unit = $this->input->post('show_related_unit');
            $group_by_category_sub_category = $this->input->post('group_by_category_sub_category');
            $print_additional_page = $this->input->post('print_additional_page');
            $price_groups = $this->site->getAllPriceGroups(null, false, 6, $price_group);
            $this->data['show_related_unit'] = $show_related_unit;
            if ($this->Settings->prioridad_precios_producto == 10 && $show_related_unit) {
                $price_groups_units = $this->site->get_price_groups_units_relationed();
                $this->data['price_groups_units'] = $price_groups_units;
            }
            $products_price_groups = $this->site->getAllProductsPriceGroups();
            $products = $this->reports_model->get_all_products($view_products_discontinued, $only_products_with_stock, $group_by_category_sub_category, $only_view_products_featured);


            // $this->excel->getActiveSheet()->SetCellValue('A1', 'Producto');
            if ($print_to == 'xls') {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle('report');
                $row = 1;
                $current_category_name = null;
                $category_id = NULL;
                $subcategory_id = NULL;
                foreach ($products as $product) {
                    if ($group_by_category_sub_category && ($category_id == NULL || $product->category_id != $category_id)) {
                        $subcategory_id = NULL;
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->category_name);
                        $letra = "B";
                        foreach ($price_groups as $pg) {
                            $this->excel->getActiveSheet()->SetCellValue($letra . $row, $pg->name);
                            $letra++;
                        }
                        $category_id = $product->category_id;
                        $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
                        $row++;
                    } else {
                        if ($row == 1) {
                            $this->excel->getActiveSheet()->SetCellValue('A' . $row, 'Producto');
                            $letra = "B";
                            foreach ($price_groups as $pg) {
                                $this->excel->getActiveSheet()->SetCellValue($letra . $row, $pg->name);
                                $letra++;
                            }
                            $row++;
                        }
                    }
                    if ($group_by_category_sub_category && ($product->subcategory_id != NULL && ($subcategory_id == NULL || $product->subcategory_id != $subcategory_id))) {
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->subcategory_name);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->name);
                    $letra = "B";
                    foreach ($price_groups as $pg) {
                        if ($show_related_unit) {
                            $unit_name =(isset($price_groups_units[$pg->id][$product->id]) ? " ".$price_groups_units[$pg->id][$product->id] : (isset($products_price_groups[$product->id][$pg->id]) ? " ".$product->unit_name : ""));
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letra . $row, (isset($products_price_groups[$product->id][$pg->id]) ? ($this->sma->formatMoney($products_price_groups[$product->id][$pg->id])) : '-').($show_related_unit ? $unit_name : ""));
                        $letra++;
                    }
                    $row++;
                }

                // $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $range = 'A1:A'.$row;
                $this->excel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                $text_center_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    )
                );
                $text_right_align = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
                    )
                );
                $this->excel->getActiveSheet()->getStyle("B".$row.":I".$row)->applyFromArray($text_right_align);
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
                // $this->excel->getActiveSheet()->getStyle('C1:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                // $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                // $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                $filename = 'production_order_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            } else if ($print_to == 'pdf') {
                $categories = $this->site->getAllCategories();
                $this->data['products'] = $products;
                $this->data['price_groups'] = $price_groups;
                $this->data['products_price_groups'] = $products_price_groups;
                $this->data['group_by_category_sub_category'] = $group_by_category_sub_category;
                $this->data['print_additional_page'] = $print_additional_page;
                $this->load_view($this->theme.'reports/pdf/price_groups_report', $this->data);
            }

        }
        $this->data['price_groups'] = $this->site->getAllPriceGroups();
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('price_groups_report')));
        $meta = array('page_title' => lang('price_groups_report'), 'bc' => $bc);
        $this->page_construct('reports/price_groups', $meta, $this->data);
    }

    function get_customer_award_points_kardex($pdf = NULL, $xls = NULL)
    {
        if ($this->input->get('user')) {
            $user = $this->input->get('user');
        } else {
            $user = NULL;
        }
        if ($this->input->get('customer')) {
            $customer = $this->input->get('customer');
        } else {
            $customer = NULL;
        }
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }

        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        $this->load->library('datatables');
        $this->datatables
            ->select("
                        companies.name,
                        award_points.date,
                        award_points.type,
                        award_points.movement_points,
                        IF(".$this->db->dbprefix('sales').".reference_no IS NOT NULL, ".$this->db->dbprefix('sales').".reference_no, ".$this->db->dbprefix('gift_cards').".card_no) as reference_no
                    ")
            ->join('companies', 'companies.id = award_points.customer_id', 'inner')
            ->join('sales', 'sales.id = award_points.sale_id', 'left')
            ->join('gift_cards', 'gift_cards.id = award_points.gift_card_id', 'left')
            ->from('award_points');

        if ($user) {
            $this->datatables->where('award_points.created_by', $user);
        }
        if ($customer) {
            $this->datatables->where('award_points.customer_id', $customer);
        }
        if ($start_date) {
            $this->datatables->where($this->db->dbprefix('award_points').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }

        if ($pdf || $xls) {

            $data = $this->datatables->get_display_result_2();

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('quotes_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('status'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->customer);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatMoney($data_row->grand_total));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->status);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $filename = 'quotes_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            echo $this->datatables->generate();
        }

    }

    public function unit_prices()
    {
        $this->sma->checkPermissions('products');
        $this->form_validation->set_rules('generate_report', 'Generar reporte', 'required');
        if ($this->form_validation->run() === true)
        {
            $this->data['biller'] = $this->site->getCompanyByID($this->Settings->default_biller);
            $print_to = $this->input->post('print_to');
            $only_products_with_stock = $this->input->post('only_products_with_stock');
            $view_products_discontinued = $this->input->post('view_products_discontinued');
            $only_view_products_featured = $this->input->post('only_view_products_featured');
            $filters = [
                        'with_stock' => $only_products_with_stock,
                        'with_discontinued' => $view_products_discontinued,
                        'only_featured' => $only_view_products_featured,
                    ];
            // $this->sma->print_arrays($_POST);
            $results = $this->site->get_all_products_units($filters);
            // Obtener el número máximo de unidades asignadas a cualquier producto
            $max_units = 0;
            $up_setted=[];
            foreach ($results as $row) {
                $unit_id = $row->unit_id;
                if (!isset($units[$row->product_id])) {
                    $units[$row->product_id] = array();
                }
                $units[$row->product_id][] = $row;
                $max_units = max($max_units, count($units[$row->product_id]));
                if (count($units[$row->product_id]) >= 6) {
                    $this->sma->print_arrays($units[$row->product_id]);
                }
            }
            $max_units++;
            // $this->sma->print_arrays($units);
            // if ($print_to == 'xls') {
            //     $this->load->library('excel');
            //     $this->excel->setActiveSheetIndex(0);
            //     $this->excel->getActiveSheet()->setTitle('report');
            //     $row = 1;
            //     $current_category_name = null;
            //     $category_id = NULL;
            //     $subcategory_id = NULL;
            //     foreach ($products as $product) {
            //         if ($group_by_category_sub_category && ($category_id == NULL || $product->category_id != $category_id)) {
            //             $subcategory_id = NULL;
            //             $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->category_name);
            //             $letra = "B";
            //             foreach ($price_groups as $pg) {
            //                 $this->excel->getActiveSheet()->SetCellValue($letra . $row, $pg->name);
            //                 $letra++;
            //             }
            //             $category_id = $product->category_id;
            //             $this->excel->getActiveSheet()->getStyle("A".$row.":M".$row)->getFont()->setBold(true);
            //             $row++;
            //         }
            //         if ($group_by_category_sub_category && ($product->subcategory_id != NULL && ($subcategory_id == NULL || $product->subcategory_id != $subcategory_id))) {
            //             $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->subcategory_name);
            //             $row++;
            //         }
            //         $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product->name);
            //         $letra = "B";
            //         foreach ($price_groups as $pg) {
            //             if ($show_related_unit) {
            //                 $unit_name =(isset($price_groups_units[$pg->id][$product->id]) ? " ".$price_groups_units[$pg->id][$product->id] : (isset($products_price_groups[$product->id][$pg->id]) ? " ".$product->unit_name : ""));
            //             }
            //             $this->excel->getActiveSheet()->SetCellValue($letra . $row, (isset($products_price_groups[$product->id][$pg->id]) ? ($this->sma->formatMoney($products_price_groups[$product->id][$pg->id])) : '-').($show_related_unit ? $unit_name : ""));
            //             $letra++;
            //         }
            //         $row++;
            //     }

            //     // $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            //     $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            //     $range = 'A1:A'.$row;
            //     $this->excel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
            //     $text_center_align = array(
            //         'alignment' => array(
            //             'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            //         )
            //     );
            //     $text_right_align = array(
            //         'alignment' => array(
            //             'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            //         )
            //     );
            //     $this->excel->getActiveSheet()->getStyle("B".$row.":I".$row)->applyFromArray($text_right_align);
            //     $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(35);
            //     $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(35);
            //     $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(35);
            //     $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(35);
            //     $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
            //     $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(35);
            //     $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(35);
            //     // $this->excel->getActiveSheet()->getStyle('C1:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            //     // $this->excel->getActiveSheet()->getStyle('D1:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            //     // $this->excel->getActiveSheet()->getStyle('E1:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            //     $filename = 'production_order_report';
            //     $this->load->helper('excel');
            //     create_excel($this->excel, $filename);
            // } else
            if ($print_to == 'pdf') {
                $this->data['results'] = $results;
                $this->data['units'] = $units;
                $this->data['max_units'] = $max_units;
                $this->load_view($this->theme.'reports/pdf/unit_prices_report', $this->data);
            }

        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('unit_prices_report')));
        $meta = array('page_title' => lang('unit_prices_report'), 'bc' => $bc);
        $this->page_construct('reports/unit_prices', $meta, $this->data);
    }

    function get_order_sales_quantity_to_purchase($data_row){
        $quantity_to_purchase  = ($data_row->product_quantity - $data_row->quantity_request) * -1;
        $quantity_to_purchase = $quantity_to_purchase > 0 ? $quantity_to_purchase : 0;
        $quantity_to_purchase_process = $quantity_to_purchase > 0 ? $quantity_to_purchase : 0;
        $text_qty = "";
        if (isset($data_row->all_units)) {
            foreach ($data_row->all_units as $dr_unit) {
                if ($quantity_to_purchase_process > 0 && ($dr_unit->operation_value > 0 || $dr_unit->num == 1)) {
                    if ($dr_unit->num == 1) {
                        $dr_unit->operation_value = 1;
                    }
                    $dr_unit_qty = $quantity_to_purchase_process / $dr_unit->operation_value;
                    if ($dr_unit->operation_value != 1) {
                        $dr_unit_qty = floor($dr_unit_qty);
                    }
                    $quantity_to_purchase_process -= ($dr_unit_qty * $dr_unit->operation_value);
                    if ($dr_unit_qty > 0) {
                        $text_qty .= $this->sma->formatQuantity($dr_unit_qty)." ".$dr_unit->name." | ";
                    }
                }
            }
        }
        return trim($text_qty, "| ");
    }

    function biller_register()
    {
        $this->sma->checkPermissions();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('filter_action', lang('filter'), 'required');
        if ($this->form_validation->run() === TRUE)
        {
            $this->pos_model->biller_id = $this->input->post('biller_id');
            $this->pos_model->start_date = $this->sma->fld($this->input->post('start_date'));
            $this->pos_model->end_date = $this->sma->fld($this->input->post('end_date'));
            $register_data = $this->pos_model->get_register_details();
            $popts = $this->site->getPaidOpts(1,0);
            if ($register_data) {
                $this->data['popts'] = $popts;
                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['register'] = $register_data['register'];
                $this->data['register_details'] = $register_data['register_details'];
                $this->data['register_details_counted'] = $register_data['register_details_counted'];
                $this->data['register_details_categories'] = $register_data['register_details_categories'];
                $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $this->input->post('biller_id'));
                $this->load_view($this->theme . 'reports/biller_register_view', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('nothing_found'));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['users'] = $this->reports_model->getStaff();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('biller_register_report')));
            $meta = array('page_title' => lang('biller_register_report'), 'bc' => $bc);
            $this->page_construct('reports/biller_register', $meta, $this->data);
        }
    }

    public function user_activities()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->site->get_all_users();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('user_activities')));
        $meta = array('page_title' => lang('user_activities'), 'bc' => $bc);
        $this->page_construct('settings/user_activities', $meta, $this->data);
    }

    function serial_register()
    {
        $this->sma->checkPermissions();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('filter_action', lang('filter'), 'required');
        if ($this->form_validation->run() === TRUE)
        {
            $this->pos_model->serial = $this->input->post('serial');
            $this->pos_model->start_date = $this->sma->fld($this->input->post('start_date'));
            $this->pos_model->end_date = $this->sma->fld($this->input->post('end_date'));
            $register_data = $this->pos_model->get_register_details();
            $popts = $this->site->getPaidOpts(1,0);
            if ($register_data) {
                $this->data['popts'] = $popts;
                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['register'] = $register_data['register'];
                $this->data['register_details'] = $register_data['register_details'];
                $this->data['register_details_counted'] = $register_data['register_details_counted'];
                $this->data['register_details_categories'] = $register_data['register_details_categories'];
                $this->data['serial'] = $this->input->post('serial');
                $this->load_view($this->theme . 'reports/serial_register_view', $this->data);
            } else {
                $this->session->set_flashdata('error', lang('nothing_found'));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['users'] = $this->reports_model->getStaff();
            $this->data['serials'] = $this->site->get_users_serials();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('serial_register_report')));
            $meta = array('page_title' => lang('serial_register_report'), 'bc' => $bc);
            $this->page_construct('reports/serial_register', $meta, $this->data);
        }
    }

    function award_points()
    {
        $this->sma->checkPermissions();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('filter_action', lang('filter'), 'required');
        $this->form_validation->set_rules('start_date', lang('start_date'), 'required');
        $this->form_validation->set_rules('end_date', lang('end_date'), 'required');
        if ($this->form_validation->run() === TRUE) {
            $start_date = strtotime(str_replace('/', '-', $this->input->post('start_date')));
            $end_date = strtotime(str_replace('/', '-', $this->input->post('end_date')));
            if ($start_date > $end_date) {
                $this->session->set_flashdata('error', lang('date_validation'));
                $_POST['filter_action'] = 0;
            }
            $sales = $this->input->post('sales') ? $this->input->post('sales') : NULL;
            $returns = $this->input->post('returns') ? $this->input->post('returns') : NULL;
            $gift_cards = $this->input->post('gift_cards') ? $this->input->post('gift_cards') : NULL;
            if (!$sales && !$returns && !$gift_cards) {
                $this->session->set_flashdata('error', 'Debe seleccionar el tipo de movimiento');
                $_POST['filter_action'] = 0;
            }
        }
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('award_points')));
        $meta = array('page_title' => lang('award_points'), 'bc' => $bc);
        $this->page_construct('reports/award_points', $meta, $this->data);
    }

    function getAwardPoinstReport ($xls_detail = NULL, $xls = NULL, $xls_acumulated = NULL)
    {
        $this->load->library('datatables');
        $customer = $this->input->get('customer_id') ? $this->input->get('customer_id') : NULL;
        $biller = $this->input->get('biller_id') ? $this->input->get('biller_id') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        $informe = $this->input->get('Informe') ? $this->input->get('Informe') : NULL;
        $sales = $this->input->get('sales') ? $this->input->get('sales') : NULL;
        $returns = $this->input->get('returns') ? $this->input->get('returns') : NULL;
        $gift_cards = $this->input->get('gift_cards') ? $this->input->get('gift_cards') : NULL;

        $start_date_times = strtotime(str_replace('/', '-', $start_date));
        $start_date_times_format = date('Y-m-d', $start_date_times);
        $end_date_times = strtotime(str_replace('/', '-', $end_date));
        $end_date_times_format = date('Y-m-d', $end_date_times);

        #clientes
        $aCustomer = "(
                        SELECT
                            id,
                            vat_no,
                            UPPER(name) AS name
                        FROM {$this->db->dbprefix('companies')}
                        WHERE group_name = 'customer'
                    ) AS sma_aCustomer ";

        #ventas positivas
        $aSales = "(
                    SELECT
                        customer_id,
                        COALESCE( SUM(grand_total), 0) AS aVentas
                    FROM {$this->db->dbprefix('sales')}
                    WHERE grand_total > 0
                        AND date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59'
                    GROUP BY customer_id
                    ) AS sma_aSales ";

        #puntos por cada venta
        $pSales = "(
                    SELECT
                        customer_id,
                        COALESCE( SUM( FLOOR( ( grand_total / {$this->Settings->each_spent} ) * {$this->Settings->ca_point} ) ), 0) AS pVentas
                    FROM {$this->db->dbprefix('sales')}
                    WHERE grand_total > 0
                        AND date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59'
                    GROUP BY customer_id
                    ) AS sma_pSales ";

        #devoluciones
        $dSales = "(
                    SELECT
                        customer_id,
                        COALESCE( SUM(grand_total), 0) AS dVentas
                    FROM {$this->db->dbprefix('sales')}
                    WHERE grand_total < 0
                        AND date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59'
                    GROUP BY customer_id
                    ) AS sma_dSales ";

        #puntos negativos
        $nSales = "(
                    SELECT
                        customer_id,
                        COALESCE( SUM( CEIL( ( grand_total / {$this->Settings->each_spent} ) * {$this->Settings->ca_point} ) ), 0) AS nVentas
                    FROM {$this->db->dbprefix('sales')}
                    WHERE grand_total < 0
                        AND date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59'
                    GROUP BY customer_id
                    ) AS sma_nSales ";

        # gift cards
        $GitCard = "(
                        SELECT
                            customer_id,
                            (SUM( CEIL(ca_points_redeemed)) * -1) AS pRedeemed
                        FROM {$this->db->dbprefix('gift_cards')}
                        WHERE creation_type = 2 AND status != 3
                            AND date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59'
                        GROUP BY customer_id
                        ) AS sma_GitCard ";

        /** datatables **/
        if ($informe == 1) {
            $this->datatables->select("IF(
                                        vat_no IS NULL
                                        OR vat_no = '',
                                            '-',
                                            vat_no
                                        ) AS vat_no"
                                    );
            $this->datatables->select('name');
            $this->datatables->select('award_points AS totalPuntos');
            $this->datatables->from('companies');
            $this->datatables->where("group_name", "customer");
            if ($customer) {
                $this->datatables->where("id", $customer);
            }
            // $this->datatables->order_by("award_points desc");
        }

        if ($informe == 2) {
            $this->datatables->select('aCustomer.vat_no');
            $this->datatables->select('aCustomer.name');
            if ($sales) {
                $this->datatables->select("IF({$this->db->dbprefix('aSales')}.aVentas IS NULL,0, {$this->db->dbprefix('aSales')}.aVentas) AS totalVentas");
                $this->datatables->select("IF({$this->db->dbprefix('pSales')}.pVentas IS NULL,0, {$this->db->dbprefix('pSales')}.pVentas) AS totalPuntosVentas");
            }
            if ($returns) {
                $this->datatables->select("IF({$this->db->dbprefix('dSales')}.dVentas IS NULL,0, {$this->db->dbprefix('dSales')}.dVentas) AS totalDevoluciones ");
                $this->datatables->select("IF({$this->db->dbprefix('nSales')}.nVentas IS NULL,0, {$this->db->dbprefix('nSales')}.nVentas) AS totalPuntosDevoluciones");
            }
            if ($gift_cards) {
                $this->datatables->select("IF({$this->db->dbprefix('GitCard')}.pRedeemed IS NULL,0, {$this->db->dbprefix('GitCard')}.pRedeemed) AS totalPuntosRedimidos");
            }
            if ($sales && !$returns && !$gift_cards) {
                $this->datatables->select("IF({$this->db->dbprefix('pSales')}.pVentas IS NULL,0, {$this->db->dbprefix('pSales')}.pVentas) AS totalPuntos");
            }
            else if ($sales && $returns && !$gift_cards) {
                $this->datatables->select("IF({$this->db->dbprefix('pSales')}.pVentas IS NULL,0, {$this->db->dbprefix('pSales')}.pVentas) + IF({$this->db->dbprefix('nSales')}.nVentas IS NULL,0, {$this->db->dbprefix('nSales')}.nVentas) AS totalPuntos");
            }
            else if ($sales && !$returns && $gift_cards) {
                $this->datatables->select("IF({$this->db->dbprefix('pSales')}.pVentas IS NULL,0, {$this->db->dbprefix('pSales')}.pVentas) + IF({$this->db->dbprefix('GitCard')}.pRedeemed IS NULL,0, {$this->db->dbprefix('GitCard')}.pRedeemed) AS totalPuntos");
            }
            else if (!$sales && $returns && !$gift_cards) {
                $this->datatables->select("IF({$this->db->dbprefix('nSales')}.nVentas IS NULL,0, {$this->db->dbprefix('nSales')}.nVentas) AS totalPuntos");
            }
            else if (!$sales && $returns && $gift_cards) {
                $this->datatables->select(" IF({$this->db->dbprefix('nSales')}.nVentas IS NULL,0, {$this->db->dbprefix('nSales')}.nVentas) + IF({$this->db->dbprefix('GitCard')}.pRedeemed IS NULL,0, {$this->db->dbprefix('GitCard')}.pRedeemed) AS totalPuntos");
            }
            else if (!$sales && !$returns && $gift_cards) {
                $this->datatables->select("IF({$this->db->dbprefix('GitCard')}.pRedeemed IS NULL,0, {$this->db->dbprefix('GitCard')}.pRedeemed) AS totalPuntos");
            }
            else if ($sales && $returns && $gift_cards) {
                $this->datatables->select("IF({$this->db->dbprefix('pSales')}.pVentas IS NULL,0, {$this->db->dbprefix('pSales')}.pVentas) + IF({$this->db->dbprefix('nSales')}.nVentas IS NULL,0, {$this->db->dbprefix('nSales')}.nVentas) + IF({$this->db->dbprefix('GitCard')}.pRedeemed IS NULL,0, {$this->db->dbprefix('GitCard')}.pRedeemed) AS totalPuntos");
            }

            $this->datatables->from('sales');
            $this->datatables->join($aCustomer, 'sales.customer_id = aCustomer.id', 'inner');
            $this->datatables->join($aSales, 'sales.customer_id = aSales.customer_id', 'left');
            $this->datatables->join($pSales, 'sales.customer_id = pSales.customer_id', 'left');
            $this->datatables->join($dSales, 'sales.customer_id = dSales.customer_id', 'left');
            $this->datatables->join($nSales, 'sales.customer_id = nSales.customer_id', 'left');
            $this->datatables->join($GitCard, 'sales.customer_id = GitCard.customer_id', 'left');
            $this->datatables->where("{$this->db->dbprefix('sales')}.date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59' ");
            if ($biller) {
                $this->datatables->where("{$this->db->dbprefix('sales')}.biller_id", $biller);
            }
            if ($customer) {
                $this->datatables->where("{$this->db->dbprefix('sales')}.customer_id", $customer);
            }
            $this->datatables->group_by("{$this->db->dbprefix('sales')}.customer_id");
        }

        if ($xls_acumulated) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $column = "A";
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('award_points');
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('vat_no'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('customer'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('total'). ' ' .lang('points'));

            $row = 2;
            foreach ($data as $data_row) {
                $column = "A";
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->vat_no);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->name);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->totalPuntos);
                $row++;
            }

            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);

            # styles
            $this->excel->getActiveSheet()->getStyle("A1:C1")->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A2:A'.$row)->getNumberFormat()->setFormatCode("#,##0");$this->excel->getActiveSheet()->getStyle('C2:C'.$row)->getNumberFormat()->setFormatCode("#,##0");
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A2:AZ' . $row)->getAlignment()->setWrapText(true);
            $filename = 'Award Points Acumulated';
            $this->load->helper('excel');

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'reports',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('award_points'),
            ]);

            create_excel($this->excel, $filename);
        }

        if ($xls) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $column = "A";
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('award_points');
            $this->excel->getActiveSheet()->SetCellValue('A1', lang('start_date').  " : {$start_date_times_format} 00:00:00        " .lang('end_date'). " : {$end_date_times_format} 23:59:59 ");
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('vat_no'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('customer'));
            if ($sales) {
                $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('sales'));
                $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('points') .' '.lang('sales'));
            }
            if ($returns) {
                $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('returns'));
                $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('points'). ' ' .lang('returns'));
            }
            if ($gift_cards) {
                $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('points'). ' ' .lang('gift_cards'));
            }
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('total'). ' ' .lang('points'));
            $row = 3;
            foreach ($data as $data_row) {
                $column = "A";
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->vat_no);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->name);
                if ($sales) {
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->totalVentas);
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->totalPuntosVentas);
                }
                if ($returns) {
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->totalDevoluciones);
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->totalPuntosDevoluciones);
                }
                if ($gift_cards) {
                    $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->totalPuntosRedimidos);
                }
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->totalPuntos);
                $row++;
            }

            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

            # styles
            $this->excel->getActiveSheet()->getStyle("A1:H2")->getFont()->setBold(true);
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A2:AZ' . $row)->getAlignment()->setWrapText(true);
            $filename = 'Award Points Sumary';
            $this->load->helper('excel');

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'reports',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('award_points'),
            ]);

            create_excel($this->excel, $filename);
        }

        if ($xls_detail) {
            $data_sales = [];
            if ($sales) {
                $data_sales = $this->reports_model->get_award_points_detail_sales($biller, $customer, $start_date_times_format, $end_date_times_format);
            }
            $data_retuns = [];
            if ($returns) {
                $data_retuns = $this->reports_model->get_award_points_detail_returns($biller, $customer, $start_date_times_format, $end_date_times_format);
            }
            $data_giftCarts = [];
            if ($gift_cards) {
                $data_giftCarts = $this->reports_model->get_award_points_detail_gitCards($biller, $customer, $start_date_times_format, $end_date_times_format);
            }

            $data = array_merge($data_sales, $data_retuns, $data_giftCarts);
            function compararFechas($a, $b) {
                return strtotime($a['date']) - strtotime($b['date']);
            }
            usort($data, 'compararFechas');
            $this->load->library('excel');
            $column = "A";
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('award_points');
            $this->excel->getActiveSheet()->SetCellValue('A1', lang('start_date').  " : {$start_date_times_format} 00:00:00        " .lang('end_date'). " : {$end_date_times_format} 23:59:59 ");
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('vat_no'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('customer'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('movement_type'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('reference_no'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('value'));
            $this->excel->getActiveSheet()->SetCellValue($column++."2", lang('points'));

            $row = 3;
            foreach ($data as $data_row) {
                $column = "A";
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row['vat_no']);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row['name']);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, lang($data_row['type_movement']));
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row['reference_no']);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row['grand_total']);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row['total']);
                $row++;
            }

            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(40);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);

            # styles
            $this->excel->getActiveSheet()->getStyle("A1:H2")->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('A2:A'.$row)->getNumberFormat()->setFormatCode("#,##0");
            $this->excel->getActiveSheet()->getStyle('D2:D'.$row)->getNumberFormat()->setFormatCode("#,##0");
            $this->excel->getActiveSheet()->getStyle('E3:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $this->excel->getActiveSheet()->getStyle('A2:AZ' . $row)->getAlignment()->setWrapText(true);
            $filename = 'Award Points detail';

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'products',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('award_points'),
            ]);
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        }

        else {
            echo $this->datatables->generate();
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'reports',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('award_points'),
            ]);
        }
    }

    public function product_profitability() {

        $this->load->library('form_validation');
        $this->form_validation->set_rules('filter_action', lang('filter'), 'required');
        if ($this->form_validation->run() === TRUE) {
            $producto = $this->input->post('Producto');
            $year = $this->input->post('year') ? $this->input->post('year') : '';
            $month = $this->input->post('month') ? $this->input->post('month') : '';
            $biller = $this->input->post('biller');
            $ultimo_dia = cal_days_in_month(CAL_GREGORIAN, $month, $year);
            $fech_ini = $year."-".($month < 9 ? "0".$month : $month)."-01 00:00:00";
            $fech_fin = $year."-".($month < 9 ? "0".$month : $month)."-".$ultimo_dia." 23:59:59";
            // exit($start_date." >> ".$end_date);
            $this->data['selected_month'] = $month;
            $this->data['selected_year'] = $year;
            $this->data['datos'] = array('Producto' => $producto, 'inicial' => $fech_ini,'Final' => $fech_fin,'biller' => $biller);
            $this->data['profitabilitys'] = $this->reports_model->get_product_profitability($producto, $fech_ini, $fech_fin, $biller);
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'costing',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('profitability_product_purchase_cost'),
            ]);
        } else {
            $this->data['profitabilitys'] = false;
        }

        $this->data['productos'] = $this->reports_model->get_cod_producto_sale();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('Rentabilidad_doc')));
        $meta = array('page_title' => lang('profitability_product_purchase_cost'), 'bc' => $bc);
        $this->page_construct('reports/product_profitability', $meta, $this->data);
    }
    public function action_product_profitability($format, $product = null, $year = null, $month = null, $biller = null) {
        $ultimo_dia = cal_days_in_month(CAL_GREGORIAN, $month, $year);
        $fech_ini = $year."-".($month < 9 ? "0".$month : $month)."-01 00:00:00";
        $fech_fin = $year."-".($month < 9 ? "0".$month : $month)."-".$ultimo_dia." 23:59:59";
        $profitabilitys = $this->reports_model->get_product_profitability($product, $fech_ini, $fech_fin, $biller);
        if ($format == 'xls') {
            $this->load->library('excel');
            $column = "A";
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('product_profitability');
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('code'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('product'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('quantity'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('total'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('cost'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('profit'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('margin'));
            $row = 2;
            $total_cost = $total_sale = $total_quantity = 0;
            foreach ($profitabilitys as $data_row) {
                $column = "A";
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row['cod_product']);
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row['name']);
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, abs($data_row['quantity']) > 0 ? $data_row['quantity'] : 0);
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, abs($data_row['total']) > 0 ? $data_row['total'] : 0);
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, abs($data_row['costo']) > 0 ? $data_row['costo'] : 0);
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, abs($data_row['utilidad']) > 0 ? $data_row['utilidad'] : 0);
                $this->excel->getActiveSheet()->SetCellValue($column++.$row, abs($data_row['margen']) > 0 ? $data_row['margen'] : 0);
                $row++;
                $total_quantity += abs($data_row['quantity']) > 0 ? $data_row['quantity'] : 0;
                $total_sale += abs($data_row['total']) > 0 ? $data_row['total'] : 0;
                $total_cost += abs($data_row['costo']) > 0 ? $data_row['costo'] : 0;
            }


            $column = "A";
            $this->excel->getActiveSheet()->SetCellValue($column++.$row, lang('total'));
            $this->excel->getActiveSheet()->SetCellValue($column++.$row, '');
            $this->excel->getActiveSheet()->SetCellValue($column++.$row, $total_quantity);
            $this->excel->getActiveSheet()->SetCellValue($column++.$row, $total_sale);
            $this->excel->getActiveSheet()->SetCellValue($column++.$row, $total_cost);
            $this->excel->getActiveSheet()->SetCellValue($column++.$row, $total_sale - $total_cost);
            $this->excel->getActiveSheet()->SetCellValue($column++.$row, $this->sma->formatDecimal((($total_sale - $total_cost) / $total_sale) * 100));

            $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(40);
            $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
            $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
            # styles
            $this->excel->getActiveSheet()->getStyle("A1:G1")->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle("A".$row.":G".$row."")->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('B2:B'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('C2:C'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('D2:D'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getActiveSheet()->getStyle('E2:E'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            // $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            // $this->excel->getActiveSheet()->getStyle('A2:AZ' . $row)->getAlignment()->setWrapText(true);
            $filename = lang('profitability_product_purchase_cost');

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'products',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('valued_products_report'),
            ]);

            $this->load->helper('excel');
            create_excel($this->excel, $filename);


        } else{

        }

    }

    function getSalesReportCustomer($pdf = NULL, $xls = NULL)
    {
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $address = $this->input->get('address') ? $this->input->get('address') : NULL;
        if ($this->input->get('start_date')) {
             $start_date = $this->input->get('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            $start_date = NULL;
        }
        if ($this->input->get('end_date')) {
             $end_date = $this->input->get('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        $si = "( SELECT
                    {$this->db->dbprefix('sale_items')}.quantity as quantity,
                    {$this->db->dbprefix('sale_items')}.net_unit_price as net_unit_price,
                    {$this->db->dbprefix('sale_items')}.unit_price as unit_price,
                    {$this->db->dbprefix('sale_items')}.item_discount as item_discount,
                    ({$this->db->dbprefix('sale_items')}.net_unit_price * {$this->db->dbprefix('sale_items')}.quantity) as total,
                    ({$this->db->dbprefix('sale_items')}.item_tax + {$this->db->dbprefix('sale_items')}.item_tax_2) as total_tax,
                    {$this->db->dbprefix('sale_items')}.avg_net_unit_cost as avg_cost,
                    {$this->db->dbprefix('sale_items')}.tax as tax,
                    {$this->db->dbprefix('sale_items')}.tax_rate_id as tax_rate_id,
                    {$this->db->dbprefix('products')}.cost as last_cost,
                    {$this->db->dbprefix('products')}.tax_method as tax_method,
                    {$this->db->dbprefix('sale_items')}.sale_id,
                    {$this->db->dbprefix('sale_items')}.seller_id,
                    {$this->db->dbprefix('sale_items')}.product_id,
                    {$this->db->dbprefix('sale_items')}.subtotal,
                    serial_no,
                    {$this->db->dbprefix('sale_items')}.product_name as item_nane,
                    {$this->db->dbprefix('sale_items')}.product_code as item_code,
                    {$this->db->dbprefix('brands')}.name as brand_name,
                    {$this->db->dbprefix('categories')}.name as category_name
                FROM {$this->db->dbprefix('sale_items')}
                LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id = {$this->db->dbprefix('sale_items')}.product_id
                LEFT JOIN {$this->db->dbprefix('brands')} ON {$this->db->dbprefix('brands')}.id = {$this->db->dbprefix('products')}.brand
                LEFT JOIN {$this->db->dbprefix('categories')} ON {$this->db->dbprefix('categories')}.id = {$this->db->dbprefix('products')}.category_id ";
        if ($start_date) {
            $si.="INNER JOIN {$this->db->dbprefix('sales')} ON {$this->db->dbprefix('sales')}.id = {$this->db->dbprefix('sale_items')}.sale_id AND ".$this->db->dbprefix('sales').".date BETWEEN '" . $start_date . "' and '".$end_date."' ";
        }
        $si .= " GROUP BY {$this->db->dbprefix('sale_items')}.sale_id, {$this->db->dbprefix('sale_items')}.product_id ) FSI";
        $this->load->library('datatables');
        $this->datatables->select(" DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                                    reference_no,
                                    return_sale_ref as affects_to,
                                    biller,
                                    companies.name as customer_name,
                                    FSI.item_nane as iname,
                                    FSI.net_unit_price,
                                    (FSI.net_unit_price + COALESCE( FSI.item_discount, 0 )) AS price_x_discount,
                                    FSI.tax,
                                    FSI.total_tax,
                                    FSI.quantity,
                                    FSI.subtotal,
                                    sales.id" )
            ->from('sales')
            ->join($si, 'FSI.sale_id=sales.id', 'INNER')
            ->join('addresses', 'addresses.id = sales.address_id', 'left')
            ->join('companies', 'companies.id = sales.customer_id', 'left')
            ->join('price_groups', 'price_groups.id = companies.price_group_id', 'left')
            ->join('companies as seller', 'seller.id = sales.seller_id', 'left')
            ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');
        if ($customer) {
            $this->datatables->where('sales.customer_id', $customer);
        }
        if ($biller) {
            $this->datatables->where('sales.biller_id', $biller);
        }
        if ($start_date) {
            $this->datatables->where($this->db->dbprefix('sales').'.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
        }
        if ($address) {
            $this->datatables->where('sales.address_id', $address);
        }

        if (!$this->Customer  && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
            }
        }

        if ($xls) {
            $data = $this->datatables->get_display_result_2();
            $letter = 'A';
            if (!empty($data)) {
                $this->load->library('excel');
                $this->excel->createSheet();
                $this->excel->setActiveSheetIndex(1);
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('affects_to'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('address'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('net_unit_price'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('price_x_discount'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('tax'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('price_x_tax'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('quantity'));
                $this->excel->getActiveSheet()->SetCellValue($letter++.'1', lang('ST'));
                $row = 2;
                $total_cost = $total_sale = $total_quantity = 0;
                foreach ($data as $data_row) {
                    $column = "A";
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->date);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->affects_to);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->customer_name);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->net_unit_price);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->price_x_discount);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->tax);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->total_tax);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->quantity);
                    $this->excel->getActiveSheet()->SetCellValue($column++.$row, $data_row->subtotal);
                    $row++;
                }

                for ($i='A'; $i <= 'Z'; $i++) {
                    $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                }
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                $filename = lang('sales_report');
                $this->load->helper('excel');

                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 6,
                    'table_name' => 'sales',
                    'record_id' => null,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('sales_report'),
                ]);
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);

        } else {
            echo $this->datatables->generate();
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'sales',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('sales_report'),
            ]);
        }
    }

    public function get_depositsCustomer($company_id = NULL)
    {
        $this->load->library('datatables');
        $this->datatables->select("
                                    reference_no,
                                    date,
                                    amount,
                                    (amount - balance) as applied_amount,
                                    balance,
                                    paid_by,
                                    IF(origen = 1, 'Manual', 'Automático') as origen,
                                    IF(origen_reference_no IS NOT NULL, origen_reference_no, 'Manual'),
                                    CONCAT({$this->db->dbprefix('users')}.first_name,
                                    ' ',
                                    {$this->db->dbprefix('users')}.last_name) as created_by,
                                    companies.name ", false)
            ->from("deposits")
            ->join('users', 'users.id=deposits.created_by', 'left')
            ->join('companies', 'companies.id=deposits.company_id', 'left');

        if ($company_id != NULL) {
            $this->datatables->where($this->db->dbprefix('deposits').'.company_id', $company_id);
        }
        $this->datatables->order_by('deposits.date desc');
        echo $this->datatables->generate();
    }

    function transfers()
    {
        $this->sma->checkPermissions();
        $this->load->library('form_validation');
        $this->form_validation->set_rules('filter_action', lang('filter'), 'required');
        $this->form_validation->set_rules('start_date', lang('start_date'), 'required');
        $this->form_validation->set_rules('end_date', lang('end_date'), 'required');
        if ($this->form_validation->run() === TRUE) {
            $start_date = strtotime(str_replace('/', '-', $this->input->post('start_date')));
            $end_date = strtotime(str_replace('/', '-', $this->input->post('end_date')));
            if ($start_date > $end_date) {
                $this->session->set_flashdata('error', lang('date_validation'));
                $_POST['filter_action'] = 0;
            }
        }
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('reports'), 'page' => lang('reports')), array('link' => '#', 'page' => lang('transfers_report')));
        $meta = array('page_title' => lang('transfers_report'), 'bc' => $bc);
        $this->page_construct('reports/transfers', $meta, $this->data);
    }

    public function getTrasnfersMovements($xls=NULL)
    {
        $this->load->library('datatables');
        $from_warehouse = $this->input->get('warehouse_origin') ? $this->input->get('warehouse_origin') : NULL;
        $to_warehouse = $this->input->get('warehouse_destination') ? $this->input->get('warehouse_destination') : NULL;
        $start_date = $this->input->get('start_date') ? $this->input->get('start_date') : NULL;
        $end_date = $this->input->get('end_date') ? $this->input->get('end_date') : NULL;
        $start_date_times = strtotime(str_replace('/', '-', $start_date));
        $start_date_times_format = date('Y-m-d', $start_date_times);
        $end_date_times = strtotime(str_replace('/', '-', $end_date));
        $end_date_times_format = date('Y-m-d', $end_date_times);

        $this->datatables->select('transfers.date');
        $this->datatables->select('reference_no');
        $this->datatables->select("from_warehouse_name");
        $this->datatables->select("to_warehouse_name");
        $this->datatables->select("(SELECT CONCAT(first_name, ' ', last_name) FROM {$this->db->dbprefix('users')} WHERE id = {$this->db->dbprefix('transfers')}.created_by) AS created_by");
        $this->datatables->select("product_code");
        $this->datatables->select("product_name");
        $this->datatables->select("quantity");
        $this->datatables->select("(COALESCE(net_unit_cost) * quantity) AS base");
        $this->datatables->select("item_tax");
        $this->datatables->select("subtotal");
        $this->datatables->select("transfers.status");
        $this->datatables->from('transfers');
        $this->datatables->join('transfer_items', 'transfers.id = transfer_items.transfer_id', 'inner');
        $this->datatables->where("{$this->db->dbprefix('transfers')}.date BETWEEN '{$start_date_times_format} 00:00:00' AND '{$end_date_times_format} 23:59:59' ");
        if ($from_warehouse) {
            $this->datatables->where("{$this->db->dbprefix('transfers')}.from_warehouse_id", $from_warehouse);
        }
        if ($to_warehouse) {
            $this->datatables->where("{$this->db->dbprefix('transfers')}.to_warehouse_id", $to_warehouse);
        }

        if ($xls) {
            $data = $this->datatables->get_display_result_2();
            $this->load->library('excel');
            $column = "A";
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->setTitle('transfers_report');
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('date'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('reference_no'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('warehouse_origin'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('warehouse_destination'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('created_by'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('code'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('name'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('quantity'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('base'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('tax'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('grand_total'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", lang('status'));

            $row = 2;
            foreach ($data as $data_row) {
                $column = "A";
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->date);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->reference_no);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->from_warehouse_name);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->to_warehouse_name);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->created_by);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->product_code);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->product_name);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->quantity);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->base);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->item_tax);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->subtotal);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, lang($data_row->status));
                $row++;
            }

            # styles
            for ($i='A'; $i <= 'L'; $i++) {
                $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
            }
            $this->excel->getActiveSheet()->getStyle("A1:L1")->getFont()->setBold(true);
            $this->excel->getActiveSheet()->getStyle('F2:F'.$row)->getNumberFormat()->setFormatCode("0");
            $this->excel->getActiveSheet()->getStyle('H2:K'.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            $filename = 'Reports transfers';
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        }

        else {
            echo $this->datatables->generate();
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 4,
                'table_name' => 'reports',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' consultó '.lang('report') .' ' .lang('transfers'),
            ]);
        }
    }

    private function order_origin($x=NULL) {
        if($x == null) {
            return '';
        } else if($x == '1') {
            return 'Orden - Web ERP';
        } else if($x == '2') {
            return 'Orden - App Vendedor';
        } else if($x == '3') {
            return 'Orden - Tienda en Línea';
        } else if($x == '4') {
            return 'Orden - App Clientes';
        } else if($x == '5') {
            return 'POS';
        } else if($x == '6') {
            return 'Venta Suspendida';
        } else if($x == '7') {
            return 'Tienda Pro';
        } else if($x == '8') {
            return 'MarketPro';
        } else if($x == '9') {
            return 'Wshops';
        } else if($x == '10') {
            return 'Tienda B2B';
        }
    }

    function daily_incomes()
    {
        $this->sma->checkPermissions('sales');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['sellers'] = $this->site->getAllCompanies('seller');
        $this->page_construct('reports/daily_incomes', ['page_title' => lang('daily_incomes_report')], $this->data);
    }

    function getDayliIncomesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('sales', TRUE);
        $user = $this->input->post('user') ? $this->input->post('user') : NULL;
        $customer = $this->input->post('customer') ? $this->input->post('customer') : NULL;
        $biller = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $seller = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        $sql_deposits_where = $sql_admin_expenses_where = $sql_payment_inner_where = "";
        $sql_pos_expenses_where = $sql_credit_payments_where = $sql_payment_collection_where = $sql_where = false;
        if ($start_date) {
            $sql_deposits_where .= " AND D.date >= '{$start_date}' AND D.date <= '{$end_date}' ";
            $sql_admin_expenses_where .= " AND P.date >= '{$start_date}' AND P.date <= '{$end_date}' ";
            $sql_payment_inner_where .= " AND P.date >= '{$start_date}' AND P.date <= '{$end_date}' ";
            $sql_where = " WHERE S.date >= '{$start_date}' AND S.date <= '{$end_date}' ";
            $sql_pos_expenses_where = " WHERE E.date >= '{$start_date}' AND E.date <= '{$end_date}' ";
            $sql_credit_payments_where = " WHERE CF.payment_date >= '{$start_date}' AND CF.payment_date <= '{$end_date}' ";
            $sql_payment_collection_where = " WHERE PC.date >= '{$start_date}' AND PC.date <= '{$end_date}' ";


        }
        if ($user) {
            $sql_where .= ($sql_where ? " AND " : " WHERE ")." S.created_by = ".$user;
            $sql_payment_inner_where .= ($sql_payment_inner_where ? " AND " : " WHERE ")." S.created_by = ".$user;
            $sql_deposits_where .= " AND D.created_by = ".$user;
            $sql_admin_expenses_where .= " AND P.created_by = ".$user;
            $sql_pos_expenses_where = ($sql_pos_expenses_where ? $sql_pos_expenses_where." AND " : " WHERE ")." E.created_by = ".$user;
            $sql_credit_payments_where = ($sql_credit_payments_where ? $sql_credit_payments_where." AND " : " WHERE ")." CF.created_by = ".$user;
            $sql_payment_collection_where = ($sql_payment_collection_where ? $sql_payment_collection_where." AND " : " WHERE ")." PC.created_by = ".$user;
        }
        if ($customer) {
            $sql_where .= ($sql_where ? " AND " : " WHERE ")." S.customer_id = ".$customer;
            $sql_payment_inner_where .= ($sql_payment_inner_where ? " AND " : " WHERE ")." S.customer_id = ".$customer;
            $sql_deposits_where .= " AND D.company_id = ".$customer;
            $sql_credit_payments_where = ($sql_credit_payments_where ? $sql_credit_payments_where." AND " : " WHERE ")." S.customer_id = ".$customer;
            $sql_payment_collection_where = ($sql_payment_collection_where ? $sql_payment_collection_where." AND " : " WHERE ")." PC.customer_id = ".$customer;
        }
        if ($biller) {
            $sql_where .= ($sql_where ? " AND " : " WHERE ")." S.biller_id = ".$biller;
            $sql_payment_inner_where .= ($sql_payment_inner_where ? " AND " : " WHERE ")." S.biller_id = ".$biller;
            $sql_deposits_where .= " AND D.biller_id = ".$biller;
            $sql_admin_expenses_where .= " AND P.biller_id = ".$biller;
            $sql_pos_expenses_where = ($sql_pos_expenses_where ? $sql_pos_expenses_where." AND " : " WHERE ")." E.biller_id = ".$biller;
            $sql_credit_payments_where = ($sql_credit_payments_where ? $sql_credit_payments_where." AND " : " WHERE ")." CF.biller_id = ".$biller;
            $sql_payment_collection_where = ($sql_payment_collection_where ? $sql_payment_collection_where." AND " : " WHERE ")." PC.biller_id = ".$biller;
        }
        if ($seller) {
            $sql_where .= ($sql_where ? " AND " : " WHERE ")." S.seller_id = ".$seller;
            $sql_payment_inner_where .= ($sql_payment_inner_where ? " AND " : " WHERE ")." S.seller_id = ".$seller;
            $sql_credit_payments_where = ($sql_credit_payments_where ? $sql_credit_payments_where." AND " : " WHERE ")." S.seller_id = ".$seller;
        }

        $this->db->query("SET SESSION group_concat_max_len = 1048576;");
        $query = "SELECT 
                        daily_date, 
                        GROUP_CONCAT(sales_amount) as sales_amount,
                        GROUP_CONCAT(paid_amount) as paid_amount,
                        GROUP_CONCAT(paid_by_due_amount) as paid_by_due_amount,
                        GROUP_CONCAT(paid_by) as paid_by,
                        SUM(COALESCE(manual_deposit_amount, 0)) as manual_deposit_amount,
                        SUM(COALESCE(automatic_deposit_amount, 0)) as automatic_deposit_amount,
                        SUM(COALESCE(admin_expenses, 0)) as admin_expenses,
                        SUM(COALESCE(pos_expenses, 0)) as pos_expenses,
                        SUM(COALESCE(credit_payments, 0)) as credit_payments,
                        SUM(COALESCE(payment_collections, 0)) as payment_collections
                    FROM 
                    (
                    SELECT 
                        CONCAT(YEAR(S.date), '-', (IF(MONTH(S.date) < 10, CONCAT('0', MONTH(S.date)), MONTH(S.date))), '-', IF(DAY(S.date) < 10, CONCAT('0', DAY(S.date)), DAY(S.date))) AS daily_date,
                        SUM((S.grand_total)) AS sales_amount,
                        GROUP_CONCAT(IF(P.amount IS NULL, 0, P.amount)) AS paid_amount,
                        (SUM((S.grand_total)) - SUM(COALESCE(P.total_paid, 0))) AS paid_by_due_amount,
                        GROUP_CONCAT(IF(P.paid_by IS NULL, 'Credito', P.paid_by)) as paid_by,
                        NULL AS manual_deposit_amount,
                        NULL AS automatic_deposit_amount,
                        NULL AS admin_expenses,
                        NULL AS pos_expenses,
                        NULL AS credit_payments,
                        NULL AS payment_collections
                    FROM
                        sma_sales S
                            LEFT JOIN
                        (SELECT 
                            tbl.sale_id,
                            GROUP_CONCAT(tbl.paid_by) AS paid_by,
                            GROUP_CONCAT(tbl.amount) AS amount,
                            SUM(tbl.amount) AS total_paid
                        FROM
                            (SELECT 
                            P.sale_id, P.paid_by, SUM(P.amount) AS amount
                        FROM
                            sma_payments P
                            INNER JOIN sma_sales S ON S.id = P.sale_id
                        WHERE P.sale_id IS NOT NULL {$sql_payment_inner_where}
                        GROUP BY P.sale_id , P.paid_by) AS tbl
                        GROUP BY tbl.sale_id) P ON P.sale_id = S.id
                    {$sql_where}
                    GROUP BY IF(P.paid_by IS NULL, 'Credito', P.paid_by), CONCAT(YEAR(S.date), '-', (IF(MONTH(S.date) < 10, CONCAT('0', MONTH(S.date)), MONTH(S.date))), '-', IF(DAY(S.date) < 10, CONCAT('0', DAY(S.date)), DAY(S.date)))

                    UNION 

                    SELECT 
                        CONCAT(YEAR(D.date), '-', (IF(MONTH(D.date) < 10, CONCAT('0', MONTH(D.date)), MONTH(D.date))), '-', IF(DAY(D.date) < 10, CONCAT('0', DAY(D.date)), DAY(D.date))) AS daily_date,
                        NULL as sales_amount, 
                        NULL as paid_amount, 
                        NULL as paid_by_due_amount, 
                        NULL as paid_by,
                        SUM(COALESCE(D.amount, 0)) as  manual_deposit_amount,
                        NULL as automatic_deposit_amount,
                        NULL as admin_expenses,
                        NULL as pos_expenses,
                        NULL as credit_payments,
                        NULL as payment_collections
                    FROM sma_deposits D
                    WHERE D.third_type = 1 AND D.origen = 1 {$sql_deposits_where}
                    GROUP BY CONCAT(YEAR(D.date), '-', (IF(MONTH(D.date) < 10, CONCAT('0', MONTH(D.date)), MONTH(D.date))), '-', IF(DAY(D.date) < 10, CONCAT('0', DAY(D.date)), DAY(D.date)))

                    UNION 

                    SELECT 
                        CONCAT(YEAR(D.date), '-', (IF(MONTH(D.date) < 10, CONCAT('0', MONTH(D.date)), MONTH(D.date))), '-', IF(DAY(D.date) < 10, CONCAT('0', DAY(D.date)), DAY(D.date))) AS daily_date,
                        NULL as sales_amount, 
                        NULL as paid_amount, 
                        NULL as paid_by_due_amount, 
                        NULL as paid_by,
                        NULL as manual_deposit_amount,
                        SUM(COALESCE(D.amount, 0)) as automatic_deposit_amount,
                        NULL as admin_expenses,
                        NULL as pos_expenses,
                        NULL as credit_payments,
                        NULL as payment_collections
                    FROM sma_deposits D
                    WHERE D.third_type = 1 AND D.origen = 2 {$sql_deposits_where}
                    GROUP BY CONCAT(YEAR(D.date), '-', (IF(MONTH(D.date) < 10, CONCAT('0', MONTH(D.date)), MONTH(D.date))), '-', IF(DAY(D.date) < 10, CONCAT('0', DAY(D.date)), DAY(D.date)))

                    UNION

                    SELECT 
                        CONCAT(YEAR(P.date), '-', (IF(MONTH(P.date) < 10, CONCAT('0', MONTH(P.date)), MONTH(P.date))), '-', IF(DAY(P.date) < 10, CONCAT('0', DAY(P.date)), DAY(P.date))) AS daily_date,
                        NULL as sales_amount, 
                        NULL as paid_amount, 
                        NULL as paid_by_due_amount, 
                        NULL as paid_by,
                        NULL as manual_deposit_amount,
                        NULL as automatic_deposit_amount,
                        SUM(P.grand_total) as admin_expenses,
                        NULL as pos_expenses,
                        NULL as credit_payments,
                        NULL as payment_collections
                    FROM sma_purchases P
                    WHERE P.purchase_type = 2 {$sql_admin_expenses_where}
                    GROUP BY CONCAT(YEAR(P.date), '-', (IF(MONTH(P.date) < 10, CONCAT('0', MONTH(P.date)), MONTH(P.date))), '-', IF(DAY(P.date) < 10, CONCAT('0', DAY(P.date)), DAY(P.date)))

                    UNION

                    SELECT 
                        CONCAT(YEAR(E.date), '-', (IF(MONTH(E.date) < 10, CONCAT('0', MONTH(E.date)), MONTH(E.date))), '-', IF(DAY(E.date) < 10, CONCAT('0', DAY(E.date)), DAY(E.date))) AS daily_date,
                        NULL as sales_amount, 
                        NULL as paid_amount, 
                        NULL as paid_by_due_amount, 
                        NULL as paid_by,
                        NULL as manual_deposit_amount,
                        NULL as automatic_deposit_amount,
                        NULL as admin_expenses,
                        SUM(E.amount) as pos_expenses,
                        NULL as credit_payments,
                        NULL as payment_collections
                    FROM sma_expenses E
                        {$sql_pos_expenses_where}
                    GROUP BY CONCAT(YEAR(E.date), '-', (IF(MONTH(E.date) < 10, CONCAT('0', MONTH(E.date)), MONTH(E.date))), '-', IF(DAY(E.date) < 10, CONCAT('0', DAY(E.date)), DAY(E.date)))

                    UNION

                    SELECT 
                        CONCAT(YEAR(CF.payment_date), '-', (IF(MONTH(CF.payment_date) < 10, CONCAT('0', MONTH(CF.payment_date)), MONTH(CF.payment_date))), '-', IF(DAY(CF.payment_date) < 10, CONCAT('0', DAY(CF.payment_date)), DAY(CF.payment_date))) AS daily_date,
                        NULL as sales_amount, 
                        NULL as paid_amount, 
                        NULL as paid_by_due_amount, 
                        NULL as paid_by,
                        NULL as manual_deposit_amount,
                        NULL as automatic_deposit_amount,
                        NULL as admin_expenses,
                        NULL as pos_expenses,
                        SUM(CF.installment_paid_amount) as credit_payments,
                        NULL as payment_collections
                    FROM sma_credit_financing_installments_payment CF
                        INNER JOIN sma_sales S ON S.id = CF.sale_id
                        {$sql_credit_payments_where}
                    GROUP BY CONCAT(YEAR(CF.payment_date), '-', (IF(MONTH(CF.payment_date) < 10, CONCAT('0', MONTH(CF.payment_date)), MONTH(CF.payment_date))), '-', IF(DAY(CF.payment_date) < 10, CONCAT('0', DAY(CF.payment_date)), DAY(CF.payment_date)))

                    UNION 

                    SELECT 
                        CONCAT(YEAR(PC.date), '-', (IF(MONTH(PC.date) < 10, CONCAT('0', MONTH(PC.date)), MONTH(PC.date))), '-', IF(DAY(PC.date) < 10, CONCAT('0', DAY(PC.date)), DAY(PC.date))) AS daily_date,
                        NULL as sales_amount, 
                        NULL as paid_amount, 
                        NULL as paid_by_due_amount, 
                        NULL as paid_by,
                        NULL as manual_deposit_amount,
                        NULL as automatic_deposit_amount,
                        NULL as admin_expenses,
                        NULL as pos_expenses,
                        NULL as credit_payments,
                        SUM(PC.amount) AS payment_collections
                    FROM sma_payment_collection PC
                        {$sql_payment_collection_where}
                    GROUP BY CONCAT(YEAR(PC.date), '-', (IF(MONTH(PC.date) < 10, CONCAT('0', MONTH(PC.date)), MONTH(PC.date))), '-', IF(DAY(PC.date) < 10, CONCAT('0', DAY(PC.date)), DAY(PC.date)))

                    ORDER BY daily_date ASC
                    ) AS tbl
                    GROUP BY daily_date";
        // exit($query);
        $q = $this->db->query($query);
        $data_sales = $q->result();
        $payments_methods = [];
        $data = [];
        foreach ($data_sales as $data_key => $row) {
            $rpm = explode(",", $row->paid_by); //formas de pago
            $rsa = explode(",", $row->sales_amount); //ventas
            $rpa = explode(",", $row->paid_amount); //pagos
            $rdpa = explode(",", $row->paid_by_due_amount); //saldos
            $max_i = count($rpm) > count($rsa) ? count($rpm) : count($rsa);
            for ($i=0; $i < $max_i; $i++) { 
                $data[$data_key]['date'] = $row->daily_date;
                $data[$data_key]['manual_deposit_amount'] = $row->manual_deposit_amount;
                $data[$data_key]['automatic_deposit_amount'] = $row->automatic_deposit_amount;
                $data[$data_key]['admin_expenses'] = $row->admin_expenses;
                $data[$data_key]['pos_expenses'] = $row->pos_expenses;
                $data[$data_key]['credit_payments'] = $row->credit_payments;
                $data[$data_key]['payment_collections'] = $row->payment_collections;
                if (isset($rsa[$i])) {
                    if (isset($data[$data_key]['sales_amount'])) {
                        $data[$data_key]['sales_amount'] += $rsa[$i];
                    } else {
                        $data[$data_key]['sales_amount'] = $rsa[$i];
                    }
                }
                if (!empty($rpm[$i])) {
                    if (!isset($rpa[$i])) {
                        $this->sma->print_arrays($row->daily_date, $rpa, $rpm);
                    }
                    if (isset($data[$data_key]['paid_by'][$rpm[$i]])) {
                        $data[$data_key]['paid_by'][$rpm[$i]] += $rpa[$i];
                    } else {
                        $data[$data_key]['paid_by'][$rpm[$i]] = $rpa[$i];
                    }
                    if (!isset($payments_methods[$rpm[$i]])) {
                        $payments_methods[$rpm[$i]] = 1; //se genera array de pm encontrados
                    }
                }
                if (isset($rdpa[$i]) && $rdpa[$i] > 0) { //existen saldos
                    if (isset($data[$data_key]['paid_by']['Credito'])) {
                        $data[$data_key]['paid_by']['Credito'] += $rdpa[$i]; //se añaden a forma de pago credito
                    } else {
                        $payments_methods['Credito'] = 1;
                        $data[$data_key]['paid_by']['Credito'] = $rdpa[$i]; //se añaden a forma de pago credito
                    }
                }
            }
        }
        $data = json_decode(json_encode($data));
        // $this->sma->print_arrays($data);

        if (!empty($data)) {
            $pm_data = [];
            $pm_q = $this->db->get('payment_methods');
            if ($pm_q->num_rows() > 0) {
                $pm_q = $pm_q->result();
                foreach ($pm_q as $pm_q_row) {
                    $pm_data[$pm_q_row->code] = $pm_q_row->name;
                }
            }
            $biller_data = $this->site->getAllCompaniesWithState('biller', $biller);
            $this->load->library('excel');
            $number_format_columns = [];
            $mes_recorrido = [];
            $sheet_num = NULL;
            foreach ($data as $data_row) {
                // $this->sma->print_arrays($data_row);
                $letter = 'A';
                $dia = date('j', strtotime($data_row->date));
                $ultimoDiaMes = date('t', strtotime($data_row->date));
                if ($dia == $ultimoDiaMes) {
                    $quincena = 2;
                } else {
                    $quincena = ceil($dia / 15);
                }
                $fechaInicio = DateTime::createFromFormat('Y-m-d H:i', $start_date);
                $fechaFin = DateTime::createFromFormat('Y-m-d H:i', $end_date);
                $diferencia = $fechaFin ? $fechaInicio->diff($fechaFin) : 0;
                $diasDiferencia = $diferencia ? $diferencia->days : 0;
                if ($diasDiferencia > 31) {
                    $time_slice = lang(date('M', strtotime($data_row->date)));
                } else {
                    $time_slice = 'Hoja 1';
                }
                if (!isset($mes_recorrido[$time_slice])) {
                    if ($sheet_num === NULL) {
                        $sheet_num = 0;
                    } else {
                        for ($i='A'; $i <= 'Z'; $i++) {
                            $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                        }
                        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        foreach ($number_format_columns as $column => $csetted) {
                            $this->excel->getActiveSheet()->getStyle($column.'1:'.$column.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                        }
                        // $range = 'F1:F'.$row;
                        // $this->excel->getActiveSheet()
                        //     ->getStyle($range)
                        //     ->getNumberFormat()
                        //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                        // $range = 'D1:D'.$row;
                        // $this->excel->getActiveSheet()
                        //     ->getStyle($range)
                        //     ->getNumberFormat()
                        //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                        $sheet_num++;
                    }
                    $mes_recorrido[$time_slice] = 1;
                    $this->excel->createSheet();
                    $this->excel->setActiveSheetIndex($sheet_num);
                    $this->excel->getActiveSheet()->setTitle($time_slice);
                    $this->excel->getActiveSheet()->SetCellValue('A1', $biller_data->company);
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang(date('M', strtotime($data_row->date))));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('automatic_deposits'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('manual_deposits'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('credit_financing_installments_payment'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('payment_collections'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('total_sales'));
                    $letter++;
                    $endPMLetter = $letter;
                    foreach ($payments_methods as $pmrow => $pmkey) {
                        $payments_methods[$pmrow] = $letter;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'2', isset($pm_data[$pmrow]) ? $pm_data[$pmrow] : lang($pmrow));
                        $letter++;
                        $endPMLetter++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('admin_expenses'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('pos_expenses'));
                    $letter++;
                    $row = 3;
                }
                $this->excel->getActiveSheet()->getStyle("A1:Z2")->getFont()->setBold(true);
                $letter = 'A';
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->date);
                $letter++;
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->automatic_deposit_amount);
                $number_format_columns[$letter] = 1;
                $letter++;
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->manual_deposit_amount);
                $number_format_columns[$letter] = 1;
                $letter++;
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->credit_payments);
                $number_format_columns[$letter] = 1;
                $letter++;
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->payment_collections);
                $number_format_columns[$letter] = 1;
                $letter++;
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->sales_amount);
                $number_format_columns[$letter] = 1;
                $letter++;
                for ($i=$letter; $i <=$endPMLetter ; $i++) {
                    $this->excel->getActiveSheet()->SetCellValue($i . $row, 0);
                }
                if (isset($data_row->paid_by)) {
                    foreach ($data_row->paid_by as $row_paid_by => $row_paid_by_amount) {
                        $letterPM = $payments_methods[$row_paid_by];
                        $this->excel->getActiveSheet()->SetCellValue($letterPM . $row, $row_paid_by_amount);
                        $number_format_columns[$letterPM] = 1;
                    }
                }  
                $letter = $endPMLetter;
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->admin_expenses);
                $number_format_columns[$letter] = 1;
                $letter++;
                $this->excel->getActiveSheet()->SetCellValue($letter . $row, $data_row->pos_expenses);
                $number_format_columns[$letter] = 1;
                $letter++;
                $row++;
            }
            for ($i='A'; $i <= 'Z'; $i++) {
                $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
            }
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            foreach ($number_format_columns as $column => $csetted) {
                $this->excel->getActiveSheet()->getStyle($column.'1:'.$column.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            }

            // $range = 'F1:F'.$row;
            // $this->excel->getActiveSheet()
            //     ->getStyle($range)
            //     ->getNumberFormat()
            //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
            // $range = 'D1:D'.$row;
            // $this->excel->getActiveSheet()
            //     ->getStyle($range)
            //     ->getNumberFormat()
            //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
            $filename = lang('daily_incomes_report');
            $this->load->helper('excel');

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'daily_incomes',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('sales_report'),
            ]);
            create_excel($this->excel, $filename);
        }
        $this->session->set_flashdata('error', lang('nothing_found'));
        redirect($_SERVER["HTTP_REFERER"]);
    }

    function billers_monthly_sales()
    {
        $this->sma->checkPermissions('sales');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['users'] = $this->reports_model->getStaff();
        $this->data['billers'] = $this->site->getAllCompanies('biller');
        $this->data['sellers'] = $this->site->getAllCompanies('seller');
        $this->page_construct('reports/billers_monthly_sales', ['page_title' => lang('billers_monthly_sales')], $this->data);
    }

    function getBillersMonthlySales($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('sales', TRUE);
        $user = $this->input->post('user') ? $this->input->post('user') : NULL;
        $customer = $this->input->post('customer') ? $this->input->post('customer') : NULL;
        $biller = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $seller = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }

        $sql_where = "";
        $sql_sales_where = false;
        if ($start_date) {
            $sql_where = " AND P.date >= '{$start_date}' AND P.date <= '{$end_date}' ";
            $sql_sales_where = " WHERE S.date >= '{$start_date}' AND S.date <= '{$end_date}' ";
        }
        if ($user) {
            $sql_where = " AND P.created_by = ".$user;
            $sql_sales_where = ($sql_sales_where ? " AND " : " WHERE ")." S.created_by = ".$user;
        }
        if ($customer) {
            $sql_sales_where = ($sql_sales_where ? " AND " : " WHERE ")." S.customer_id = ".$customer;
        }
        if ($biller) {
            $sql_sales_where = ($sql_sales_where ? " AND " : " WHERE ")." S.biller_id = ".$biller;
        }
        if ($seller) {
            $sql_sales_where = ($sql_sales_where ? " AND " : " WHERE ")." S.seller_id = ".$seller;
        }


        $this->db->query("SET SESSION group_concat_max_len = 1048576;");
        $query = "SELECT 
                    tbl.monthly_date,
                    tbl.biller_id,
                    tbl.company,
                    GROUP_CONCAT(tbl.paid_by) as paid_by,
                    GROUP_CONCAT(tbl.amount) as amount,
                    SUM(tbl.total_sales) AS total_sales,
                    SUM(tbl.total_returns) AS total_returns,
                    SUM(tbl.total_items) AS total_items,
                    SUM(tbl.num_sales) AS num_sales
                FROM 
                (SELECT
                    tbl.monthly_date,
                    C.id as biller_id,
                    C.company,
                    GROUP_CONCAT(tbl.paid_by) as paid_by,
                    GROUP_CONCAT(tbl.amount) as amount,
                    SUM(IF(S.grand_total > 0, S.grand_total, 0)) AS total_sales,
                    SUM(IF(S.grand_total < 0, S.grand_total, 0)) AS total_returns,
                    SUM(nI.num_items) AS total_items,
                    COUNT(S.id) AS num_sales
                FROM sma_sales S
                    INNER JOIN sma_companies C ON C.id = S.biller_id
                    INNER JOIN (SELECT SI.sale_id, SUM(SI.quantity) AS num_items FROM sma_sale_items SI
                GROUP BY SI.sale_id) nI on nI.sale_id = S.id
                    LEFT JOIN (
                        SELECT  
                         CONCAT(YEAR(P.date),'_',LPAD(MONTH(P.date), 2, 0)) as monthly_date,
                         P.sale_id,
                         GROUP_CONCAT(COALESCE(P.amount, 0)) as amount,
                         GROUP_CONCAT(IF(P.paid_by IS NULL, 'Credito', P.paid_by)) as paid_by
                        FROM sma_payments P
                                WHERE P.sale_id IS NOT NULL {$sql_where}
                        GROUP BY P.sale_id, CONCAT(YEAR(P.date),'_',LPAD(MONTH(P.date), 2, 0))
                    ) tbl ON tbl.sale_id = S.id
                {$sql_sales_where}
                GROUP BY tbl.monthly_date, S.biller_id, tbl.paid_by) as tbl
                group by tbl.monthly_date, tbl.company
                ORDER BY tbl.monthly_date ASC, tbl.biller_id ASC";
        // exit($query);
        $q = $this->db->query($query);
        $data_sales = $q->result();
        $billers = [];
        $billers_letters = [];
        $payment_methods = [];
        $data = [];
        $dsNum = 0;
        foreach ($data_sales as $data_key => $row) {
            if (empty($row->monthly_date)) {
                continue;
            }
            $rpm = explode(",", $row->paid_by); //formas de pago
            $rpma = explode(",", $row->amount); //montos formas de pago
            $max_i = count($rpm);
            $rE = explode("_", $row->monthly_date);
            for ($i=0; $i < $max_i; $i++) { 
                $data[$dsNum]['date'] = $row->monthly_date;
                $data[$dsNum]['company'] = $row->company;
                $data[$dsNum]['biller_id'] = $row->biller_id;
                $data[$dsNum]['total_sales'] = $row->total_sales;
                $data[$dsNum]['total_returns'] = $row->total_returns;
                $data[$dsNum]['total_items'] = $row->total_items;
                $data[$dsNum]['num_sales'] = $row->num_sales;
                if (!isset($billers[$row->biller_id])) {
                    $billers[$row->biller_id] = $row->company;
                }
                if ($rpm[$i] != "") {
                    if (isset($data[$dsNum]['paid_by'][$rpm[$i]])) {
                        $data[$dsNum]['paid_by'][$rpm[$i]] += $rpma[$i];
                    } else {
                        $data[$dsNum]['paid_by'][$rpm[$i]] = $rpma[$i];
                    }
                    if (!isset($payment_methods[$rpm[$i]])) {
                        $payment_methods[$rpm[$i]] = 1;
                    }
                }
            }
            $dsNum++;
        }
        $data = json_decode(json_encode($data));
        // $this->sma->print_arrays($data);

        if (!empty($data)) {
            $pm_data = [];
            $pm_q = $this->db->get('payment_methods');
            if ($pm_q->num_rows() > 0) {
                $pm_q = $pm_q->result();
                foreach ($pm_q as $pm_q_row) {
                    $pm_data[$pm_q_row->code] = $pm_q_row->name;
                }
            }
            $this->load->library('excel');
            $number_format_columns = [];
            $mes_recorrido = [];
            $total_pms = [];
            $sheet_num = NULL;
            $monthly_date = NULL;
            $total_billers_pm = 0;
            $total_billers_pm_biller_id = NULL;
            foreach ($data as $data_row) {
                $letter = 'A';
                $dateE = explode("_", $data_row->date);
                $time_slice = $dateE[0];
                if (!isset($mes_recorrido[$time_slice])) {
                    if ($sheet_num === NULL) {
                        $sheet_num = 0;
                    } else {
                        for ($i='A'; $i <= 'Z'; $i++) {
                            $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                        }
                        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                        foreach ($number_format_columns as $column => $csetted) {
                            $this->excel->getActiveSheet()->getStyle($column.'1:'.$column.$row)->getNumberFormat()->setFormatCode("#,##0.00");
                        }
                        // $range = 'F1:F'.$row;
                        // $this->excel->getActiveSheet()
                        //     ->getStyle($range)
                        //     ->getNumberFormat()
                        //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                        // $range = 'D1:D'.$row;
                        // $this->excel->getActiveSheet()
                        //     ->getStyle($range)
                        //     ->getNumberFormat()
                        //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                        $sheet_num++;
                    }
                    $mes_recorrido[$time_slice] = 1;
                    $this->excel->createSheet();
                    $this->excel->setActiveSheetIndex($sheet_num);
                    $this->excel->getActiveSheet()->setTitle($time_slice);
                    $this->excel->getActiveSheet()->SetCellValue('A1', $time_slice);
                    if (!isset($dateE[1])) {
                        exit(var_dump($dateE));
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('months')[(Int) $dateE[1]]);
                    $letter++;
                    foreach ($billers as $biller_id => $biller_name) {
                        $this->excel->getActiveSheet()->SetCellValue($letter.'2', $biller_name);
                        $billers_letters[$biller_id] = $letter;
                        $letter++;
                    }
                    $end_billers_letter = $letter;
                    $this->excel->getActiveSheet()->SetCellValue($letter.'2', lang('total'));
                    $total_pm_letter = $letter;
                    $letter++;
                    $row = 3;
                }
                $this->excel->getActiveSheet()->getStyle("A1:Z2")->getFont()->setBold(true);
                $letter = 'A' ;
                $row = isset($endRow) ? $endRow : $row;

                if (!$total_billers_pm_biller_id || ($total_billers_pm_biller_id != $data_row->biller_id)) {
                    if ($total_billers_pm_biller_id != $data_row->biller_id && $total_billers_pm != 0) {
                        $this->excel->getActiveSheet()->SetCellValue($billers_letters[$total_billers_pm_biller_id ? $total_billers_pm_biller_id : $data_row->biller_id].$totales_pm_billers_row, $total_billers_pm);

                        if (isset($total_pms['totales_pm_billers_row'])) {
                            $total_pms['totales_pm_billers_row'] += $total_billers_pm;
                        } else {
                            $total_pms['totales_pm_billers_row'] = $total_billers_pm;
                        }
                        $total_billers_pm = 0;
                    }
                    $total_billers_pm_biller_id = $data_row->biller_id;
                }

                if (!$monthly_date || $monthly_date != $data_row->date) {
                    if (count($total_pms) > 0) {
                        foreach ($total_pms as $total_pm_paidBy => $total_pm_paidAmount) {
                            $this->excel->getActiveSheet()->SetCellValue($end_billers_letter.$payment_methods_row[$total_pm_paidBy], $total_pm_paidAmount);
                        }
                        $total_pms = [];
                    }
                    if ($row != 3) {
                        $drRE = explode("_", $data_row->date);
                        $this->excel->getActiveSheet()->SetCellValue("A".$row, lang('months')[(Int) $drRE[1]]);
                        foreach ($billers as $biller_id => $biller_name) {
                            $this->excel->getActiveSheet()->SetCellValue($billers_letters[$biller_id].$row, $biller_name);
                        }
                        $this->excel->getActiveSheet()->getStyle("A".$row.":Z".$row."")->getFont()->setBold(true);
                        $row++;
                    } 
                    $payment_methods_row = [];
                    $startRow = $row;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_returns'));
                    $payment_methods_row['total_returns'] = $row;
                    $row++;
                    foreach ($payment_methods as $pmPaid_by => $setted) {
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, isset($pm_data[$pmPaid_by]) ? $pm_data[$pmPaid_by] : lang($pmPaid_by));
                        $payment_methods_row[$pmPaid_by] = $row;
                        $row++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total'));
                    $payment_methods_row['totales_pm_billers_row'] = $row;
                    $this->excel->getActiveSheet()->getStyle("A".$row.":".$end_billers_letter.$row."")->getFont()->setBold(true);
                    $totales_pm_billers_row = $row;
                    $row++;

                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('units'));$payment_methods_row['pmbl_units_row'] = $row;
                    $row++;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('invoices'));$payment_methods_row['pmbl_invoices_row'] = $row;
                    $row++;
                    $this->excel->getActiveSheet()->SetCellValue('A'.$row, lang('total_invoices_crossed'));$payment_methods_row['pmbl_invoicescrossed_row'] = $row;
                    $this->excel->getActiveSheet()->getStyle("A".$row.":".$end_billers_letter.$row."")->getFont()->setBold(true);
                    $row++;

                    $endRow = $row;
                    for ($i=$startRow; $i <= $endRow; $i++) { 
                        foreach ($billers_letters as $biller_id => $biller_letter) {
                            $this->excel->getActiveSheet()->SetCellValue($biller_letter . $i, 0);
                        }
                    }
                    $monthly_date = $data_row->date;
                }
                $this->excel->getActiveSheet()->SetCellValue($billers_letters[$data_row->biller_id].$payment_methods_row['total_returns'], $data_row->total_returns);

                if (isset($total_pms['total_returns'])) {
                    $total_pms['total_returns'] += $data_row->total_returns;
                } else {
                    $total_pms['total_returns'] = $data_row->total_returns;
                }

                $total_billers_pm += $data_row->total_returns;
                foreach ($data_row->paid_by as $drPaid_by => $drPaid_amount) {
                    $this->excel->getActiveSheet()->SetCellValue($billers_letters[$data_row->biller_id].$payment_methods_row[$drPaid_by], $drPaid_amount);
                    $total_billers_pm += $drPaid_amount;
                    if (isset($total_pms[$drPaid_by])) {
                        $total_pms[$drPaid_by] += $drPaid_amount;
                    } else {
                        $total_pms[$drPaid_by] = $drPaid_amount;
                    }
                }

                $this->excel->getActiveSheet()->SetCellValue($billers_letters[$data_row->biller_id].$payment_methods_row['pmbl_units_row'], $data_row->total_items);
                if (isset($total_pms['pmbl_units_row'])) {
                    $total_pms['pmbl_units_row'] += $data_row->total_items;
                } else {
                    $total_pms['pmbl_units_row'] = $data_row->total_items;
                }
                $this->excel->getActiveSheet()->SetCellValue($billers_letters[$data_row->biller_id].$payment_methods_row['pmbl_invoices_row'], $data_row->num_sales);
                if (isset($total_pms['pmbl_invoices_row'])) {
                    $total_pms['pmbl_invoices_row'] += $data_row->num_sales;
                } else {
                    $total_pms['pmbl_invoices_row'] = $data_row->num_sales;
                }
                $this->excel->getActiveSheet()->SetCellValue($billers_letters[$data_row->biller_id].$payment_methods_row['pmbl_invoicescrossed_row'], $this->sma->formatQuantity($data_row->total_items / $data_row->num_sales));
                $total_pms['pmbl_invoicescrossed_row'] = $total_pms['pmbl_units_row'] / $total_pms['pmbl_invoices_row'];
                $this->excel->getActiveSheet()->getStyle("B".$startRow.':'."Z".$endRow)->getNumberFormat()->setFormatCode("#,##0.00");
            }
            // $this->sma->print_arrays($debugg_arr);
            $this->excel->getActiveSheet()->SetCellValue($billers_letters[$total_billers_pm_biller_id ? $total_billers_pm_biller_id : $data_row->biller_id].$totales_pm_billers_row, $total_billers_pm);
            if (isset($total_pms['totales_pm_billers_row'])) {
                $total_pms['totales_pm_billers_row'] += $total_billers_pm;
            } else {
                $total_pms['totales_pm_billers_row'] = $total_billers_pm;
            }
            if (count($total_pms) > 0) {
                foreach ($total_pms as $total_pm_paidBy => $total_pm_paidAmount) {
                    $this->excel->getActiveSheet()->SetCellValue($end_billers_letter.$payment_methods_row[$total_pm_paidBy], $total_pm_paidAmount);
                    
                }
                $total_pms = [];
            }
            for ($i='A'; $i <= 'Z'; $i++) {
                $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
            }
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

            foreach ($number_format_columns as $column => $csetted) {
                $this->excel->getActiveSheet()->getStyle($column.'1:'.$column.$row)->getNumberFormat()->setFormatCode("#,##0.00");
            }

            // $range = 'F1:F'.$row;
            // $this->excel->getActiveSheet()
            //     ->getStyle($range)
            //     ->getNumberFormat()
            //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
            // $range = 'D1:D'.$row;
            // $this->excel->getActiveSheet()
            //     ->getStyle($range)
            //     ->getNumberFormat()
            //     ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
            $filename = lang('billers_monthly_sales');
            $this->load->helper('excel');

            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 6,
                'table_name' => 'daily_incomes',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('sales_report'),
            ]);
            create_excel($this->excel, $filename);
        }
        $this->session->set_flashdata('error', lang('nothing_found'));
        redirect($_SERVER["HTTP_REFERER"]);
    }
}
