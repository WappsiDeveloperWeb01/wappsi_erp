<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .tooltip {
        z-index: : 9999999 !important;
    }
</style>
<script>
$(document).ready(function(){
    if (start_date = localStorage.getItem('auditor_start_date')) {
        $('#start_date').val(start_date);
    }
    if (end_date = localStorage.getItem('auditor_end_date')) {
        $('#end_date').val(end_date);
    }
    if (documents_types = JSON.parse(localStorage.getItem('auditor_documents_types'))) {
        $('#documents_types').val(documents_types);
        set_documents();
    }
});
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open("system_settings/auditor" . '', ['id' => 'auditor_form', 'target' => '_blank']); ?>
                                <input type="hidden" name="send_auditor" value="1">
                                <div class="row">
                                    <div class="form-group col-sm-4">
                                        <?= lang('start_date', 'start_date') ?>
                                        <input type="date" name="start_date" id="start_date" class="form-control" required>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <?= lang('end_date', 'end_date') ?>
                                        <input type="date" name="end_date" id="end_date" class="form-control" required>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <?= lang("documents_types_to_audit", "documents_types"); ?>
                                        <select name="documents_types[]" class="form-control" id="documents_types" multiple="true" required>
                                            <?php if ($documents_types): ?>
                                                <?php foreach ($documents_types as $sdt): ?>
                                                    <option value="<?= $sdt->id ?>"><?= $sdt->sales_prefix ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                        <p class="dt_selected" style="display: none;"></p>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 0.8%;">
                                        <h2><i class="fas fa-tasks"></i> <?= lang('to_audit') ?> <i class="fa fa-question-circle input-tip" data-tip="<?= lang('to_audit_description') ?>"></i> </h2>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 0.8%;">
                                        <h3 style="color: #428bca;"><i class="fas fa-angle-right"></i> <?= lang('to_audit_header') ?></h3>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_skipped_consecutives" id="audit_skipped_consecutives">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_skipped_consecutives_description') ?>"></i> <?= lang('audit_skipped_consecutives') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_company_relationed_exists" id="audit_company_relationed_exists">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_company_relationed_exists_description') ?>"></i> <?= lang('audit_company_relationed_exists') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_invoices_negative_balance" id="audit_invoices_negative_balance">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_invoices_negative_balance_description') ?>"></i> <?= lang('audit_invoices_negative_balance') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_invoice_paid_value" id="audit_invoice_paid_value">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_invoice_paid_value_description') ?>"></i> <?= lang('audit_invoice_paid_value') ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_returns_payments" id="audit_returns_payments">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_returns_payments_description') ?>"></i> <?= lang('audit_returns_payments') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_pending_invoices" id="audit_pending_invoices">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_pending_invoices_description') ?>"></i> <?= lang('audit_pending_invoices') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_invoices_commision" id="audit_invoices_commision">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_invoices_commision_description') ?>"></i> <?= lang('audit_invoices_commision') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_fe_invoice_pending_dian" id="audit_fe_invoice_pending_dian">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_fe_invoice_pending_dian_description') ?>"></i> <?= lang('audit_fe_invoice_pending_dian') ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_credit_customers_direct_payments" id="audit_credit_customers_direct_payments">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_credit_customers_direct_payments_description') ?>"></i> <?= lang('audit_credit_customers_direct_payments') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_invoice_subtotal_tax_grand_total" id="audit_invoice_subtotal_tax_grand_total">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_invoice_subtotal_tax_grand_total_description') ?>"></i> <?= lang('audit_invoice_subtotal_tax_grand_total') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_invoice_retentions_payments" id="audit_invoice_retentions_payments">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_invoice_retentions_payments_description') ?>"></i> <?= lang('audit_invoice_retentions_payments') ?>
                                        </label>
                                    </div>
                                    <?php if ($this->Settings->cost_center_selection != 2): ?>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_cost_center_relation" id="audit_cost_center_relation">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_cost_center_relation_description') ?>"></i> <?= lang('audit_cost_center_relation') ?>
                                            </label>
                                        </div>
                                    <?php endif ?>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 0.8%;margin-top :0.8%;">
                                        <h3 style="color: #428bca;"><i class="fas fa-angle-right"></i> <?= lang('to_audit_header_detail') ?></h3>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_header_detail" id="audit_header_detail">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_header_detail_description') ?>"></i> <?= lang('audit_header_detail') ?>
                                        </label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 0.8%;margin-top :0.8%;">
                                        <h3 style="color: #428bca;"><i class="fas fa-angle-right"></i> <?= lang('to_audit_detail') ?></h3>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_tax_retention_id_relation" id="audit_tax_retention_id_relation">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_tax_retention_id_relation_description') ?>"></i> <?= lang('audit_tax_retention_id_relation') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_tax_corresponds_product" id="audit_tax_corresponds_product">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_tax_corresponds_product_description') ?>"></i> <?= lang('audit_tax_corresponds_product') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_products_relation_in_movements" id="audit_products_relation_in_movements">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_products_relation_in_movements_description') ?>"></i> <?= lang('audit_products_relation_in_movements') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_products_option_relation_in_movements" id="audit_products_option_relation_in_movements">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_products_option_relation_in_movements_description') ?>"></i> <?= lang('audit_products_option_relation_in_movements') ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_sale_price_avg_costing" id="audit_sale_price_avg_costing">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_sale_price_avg_costing_description') ?>"></i> <?= lang('audit_sale_price_avg_costing') ?>
                                        </label>
                                    </div>
                                </div>
                                <?php if ($this->Settings->modulary == 1): ?>
                                    <div class="row">
                                        <div class="col-sm-12" style="margin-bottom: 0.8%; margin-top: 0.8%; text-align: center;">
                                            <h3 style="color: #428bca;"><i class="fas fa-cogs"></i> <?= lang('modulary') ?></h3>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_payments_concepts_ledger_id" id="audit_payments_concepts_ledger_id">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_payments_concepts_ledger_id_description') ?>"></i> <?= lang('audit_payments_concepts_ledger_id') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_movement_accounted" id="audit_movement_accounted">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_movement_accounted_description') ?>"></i> <?= lang('audit_movement_accounted') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_total_movement_payment_method" id="audit_total_movement_payment_method">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_total_movement_payment_method_description') ?>"></i> <?= lang('audit_total_movement_payment_method') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_retention_and_discounts_accounted" id="audit_retention_and_discounts_accounted">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_retention_and_discounts_accounted_description') ?>"></i> <?= lang('audit_retention_and_discounts_accounted') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_movement_accounting_third" id="audit_movement_accounting_third">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_movement_accounting_third_description') ?>"></i> <?= lang('audit_movement_accounting_third') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_movement_accounting_duplicated" id="audit_movement_accounting_duplicated">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_movement_accounting_duplicated_description') ?>"></i> <?= lang('audit_movement_accounting_duplicated') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_invoice_pending_accounted" id="audit_invoice_pending_accounted">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_invoice_pending_accounted_description') ?>"></i> <?= lang('audit_invoice_pending_accounted') ?>
                                            </label>
                                        </div>
                                        <?php if ($this->Settings->cost_center_selection != 2): ?>
                                            <div class="col-sm-3">
                                                <label>
                                                    <input type="checkbox" class="auditor_check auditor_with_filters" name="audit_movement_accounting_cost_center" id="audit_movement_accounting_cost_center">
                                                    <i class="fa fa-question-circle input-tip" <?= lang('<?=_description') ?>R SIT AMET"></i> <?= lang('audit_movement_accounting_cost_center') ?>
                                                </label>
                                            </div>
                                        <?php endif ?>
                                    </div>
                                <?php endif ?>

                            <hr>

                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 0.8%;">
                                        <h2><i class="fas fa-tasks"></i> <?= lang('audit_just_taking_filter_dates') ?>  <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_just_taking_filter_dates_description') ?>"></i></h2>
                                        <br>
                                        <span style="color: #428bca;"><i class="fa fa-exclamation-circle"></i> <?= lang('system_start_date') ?> : <?= $this->Settings->system_start_date ?></span>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_invalid_date" id="audit_invalid_date">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_invalid_date_description') ?>"></i> <?= lang('audit_invalid_date') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_header_document_type_relation" id="audit_header_document_type_relation">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_header_document_type_relation_description') ?>"></i> <?= lang('audit_header_document_type_relation') ?>
                                        </label>
                                    </div>
                                </div>

                                <hr>

                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 0.8%;">
                                        <h2><i class="fas fa-tasks"></i> <?= lang('general_audit') ?>   <i class="fa fa-question-circle input-tip" data-tip="<?= lang('general_audit_description') ?>"></i></h2>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_product_cost_and_avg_cost" id="audit_product_cost_and_avg_cost">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_product_cost_and_avg_cost_description') ?>"></i> <?= lang('audit_product_cost_and_avg_cost') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_product_cost_and_price_base" id="audit_product_cost_and_price_base">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_product_cost_and_price_base_description') ?>"></i> <?= lang('audit_product_cost_and_price_base') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <label>
                                            <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_products_without_movements" id="audit_products_without_movements">
                                            <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_products_without_movements_description') ?>"></i> <?= lang('audit_products_without_movements') ?>
                                        </label>
                                    </div>
                                </div>
                                <?php if ($this->Settings->modulary == 1): ?>
                                    <div class="row">
                                        <div class="col-sm-12" style="margin-bottom: 0.8%; margin-top: 0.8%; text-align: center;">
                                            <h3 style="color: #428bca;"><i class="fas fa-cogs"></i> <?= lang('modulary') ?></h3>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_document_types_modulary_parametrization" id="audit_document_types_modulary_parametrization">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_document_types_modulary_parametrization_description') ?>"></i> <?= lang('audit_document_types_modulary_parametrization') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_entries_debit_credit_total" id="audit_entries_debit_credit_total">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_entries_debit_credit_total_description') ?>"></i> <?= lang('audit_entries_debit_credit_total') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_entries_pending" id="audit_entries_pending">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_entries_pending_description') ?>"></i> <?= lang('audit_entries_pending') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_entries_invalid_ledger" id="audit_entries_invalid_ledger">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_entries_invalid_ledger_description') ?>"></i> <?= lang('audit_entries_invalid_ledger') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_tax_modulary_parametrization" id="audit_tax_modulary_parametrization">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_tax_modulary_parametrization_description') ?>"></i> <?= lang('audit_tax_modulary_parametrization') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_payment_methods_parametrization" id="audit_payment_methods_parametrization">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_payment_methods_parametrization_description') ?>"></i> <?= lang('audit_payment_methods_parametrization') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_expense_categories_modulary_parametrization" id="audit_expense_categories_modulary_parametrization">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_expense_categories_modulary_parametrization_description') ?>"></i> <?= lang('audit_expense_categories_modulary_parametrization') ?>
                                            </label>
                                        </div>
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_quantity_adjustments_modulary_parametrization" id="audit_quantity_adjustments_modulary_parametrization">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_quantity_adjustments_modulary_parametrization_description') ?>"></i> <?= lang('audit_quantity_adjustments_modulary_parametrization') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <label>
                                                <input type="checkbox" class="auditor_check auditor_whithout_filters" name="audit_movement_type_modulary_parametrization" id="audit_movement_type_modulary_parametrization">
                                                <i class="fa fa-question-circle input-tip" data-tip="<?= lang('audit_movement_type_modulary_parametrization_description') ?>"></i> <?= lang('audit_movement_type_modulary_parametrization') ?>
                                            </label>
                                        </div>
                                    </div>
                                <?php endif ?>

                            <hr>

                                <div class="row">
                                    <div class="col-sm-12" style="margin-bottom: 0.8%;">
                                        <label>
                                            <input type="checkbox" name="select_all" id="select_all">
                                            <?= lang('select_all') ?>
                                        </label>
                                    </div>

                                    <div class="col-sm-12">
                                        <button type="button" id="send_auditor" class="btn btn-primary"><?= lang('send') ?></button>
                                        <button type="button" onclick="reset_form();" class="btn btn-danger"><?= lang('reset') ?></button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->
<?= form_close() ?>
<script language="javascript">
    $(document).ready(function () {
        $("#auditor_form").validate({
            ignore: []
        });
    });

    $(document).on('ifChecked', '.auditor_check', function(){
        setFilter();
    });
    $(document).on('ifUnchecked', '.auditor_check', function(){
        setFilter();
    });

    function setFilter(){
        filter = false;
        $('.auditor_with_filters').each(function(index, check){
            if ($(check).is(':checked')) {
                filter = true;
            }
        });

        if (filter) {
            $('#start_date').prop('required', true).prop('disabled', false);
            $('#end_date').prop('required', true).prop('disabled', false);
            $('#documents_types').prop('required', true).prop('disabled', false);
        } else {
            $('#start_date').prop('required', false).prop('disabled', true);
            $('#end_date').prop('required', false).prop('disabled', true);
            $('#documents_types').prop('required', false).prop('disabled', true);
        }
    }

    $(document).on('click', '#send_auditor', function(){
        $('#send_auditor').prop('disabled', true);
        var checked = 0;
        $.each($('.auditor_check'), function(index, check){
            if ($(check).is(':checked')) {
                checked++;
            }
        });
        if (checked > 0) {
            if ($('#auditor_form').valid()) {
                $('#loading').fadeIn();
                command: toastr.warning('Se está generando el informe, por favor sea paciente.', 'Generando informe', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
                setTimeout(function() {
                    $('#auditor_form').submit();
                    set_documents();
                    $('#send_auditor').prop('disabled', false);
                }, 2000);
                $(window).on('focus', function(){
                    $('#loading').fadeOut();
                })
            } else {
                $('#send_auditor').prop('disabled', false);
            }
        } else {
            command: toastr.warning('Debe escoger al menos una opción para auditar.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            $('#send_auditor').prop('disabled', false);
        }
    });
    $(document).on('ifChecked', '#select_all', function(){
        $('.auditor_check').iCheck('check');
    });
    $(document).on('ifUnchecked', '#select_all', function(){
        $('.auditor_check').iCheck('uncheck');
    });

    function set_documents(){
        text = "Revisados : ";
        localStorage.setItem('auditor_start_date', $('#start_date').val());
        localStorage.setItem('auditor_end_date', $('#end_date').val());
        localStorage.setItem('auditor_documents_types', JSON.stringify($('#documents_types').val()));
        $('#documents_types option:selected').prop('disabled', true);
        $('#documents_types option:selected').each(function(index, option){
            text += (text != "Revisados : " ? ", " : "")+$(option).text();
        });
        $('.dt_selected').text(text).fadeIn();
        setTimeout(function() {
            $('#documents_types').select2('val', '');
        }, 550);
    }

    $(document).on('change', '#start_date', function(){
        clean_documents();
    });

    $(document).on('change', '#end_date', function(){
        clean_documents();
    });

    function clean_documents(){
        $('#documents_types option').each(function(index, option){
            $(option).prop('disabled', false);
        });
        $('#documents_types').select2();
        $('.dt_selected').text('').fadeOut();
        localStorage.removeItem('auditor_start_date');
        localStorage.removeItem('auditor_end_date');
        localStorage.removeItem('auditor_documents_types');
    }

    function reset_form(){
        localStorage.removeItem('auditor_start_date');
        localStorage.removeItem('auditor_end_date');
        localStorage.removeItem('auditor_documents_types');
        location.reload();
    }
</script>
