<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?= admin_form_open('system_settings/tax_actions', 'id="action-form"') ?>
    <div class="wrapper wrapper-content  animated fadeInRight">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="CURData" class="table table-hover reports-table">
                                        <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <!-- <input class="checkbox checkth" type="checkbox" name="check"/> -->
                                            </th>
                                            <th><?= $this->lang->line("name"); ?></th>
                                            <th><?= $this->lang->line("code"); ?></th>
                                            <th class="text-center"><?= $this->lang->line("tax_rate"); ?></th>
                                            <th><?= $this->lang->line("type"); ?></th>
                                            <th><?= $this->lang->line("synchronized"); ?></th>
                                            <th style="width:65px;"><?= $this->lang->line("actions"); ?></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /Body -->
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
    </div>
<?= form_close() ?>

<script>
    $(document).ready(function () {
        loadDataTables()

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $(document).on('click', '#syncTaxes', function(e) { e.preventDefault(); confirmSyncTaxes() });
    });

    function loadDataTables() 
    {
        oTable = $('#CURData').dataTable({
            aaSorting: [[1, "asc"]],
            aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true, 
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/getTaxRates') ?>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                {
                    "bSortable": false,
                    // "mRender": checkbox 
                    visible: false
                }, 
                null, 
                null, 
                { "mRender": decimalFormat }, 
                { 
                    mRender: function(data) {
                        return (data == 1) ? "<?=lang('percentage')?>" : "<?=lang('fixed')?>";
                    } 
                },
                {
                    className: 'text-center',
                    mRender: function(data) {
                        return (data == 1) ? '<i class="fas fa-check-circle fa-2x text-success"></i>' : '<i class="fa fa-ban fa-2x text-danger"></i>'
                    }
                }, 
                { 
                    "bSortable": false,
                    className: "text-center"
                }
            ],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html(`<a href="<?= admin_url('system_settings/add_tax_rate') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus fa-lg"></i></a>
                <div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#" id="delete" data-action="delete">
                                <i class="fa fa-trash-o"></i> <?= lang('delete_tax_rates') ?>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li><label style="padding-left: 20px; padding-top: 10px">Tienda PRO</label></li>
                        <li>   
                            <a href="${site.base_url + 'system_settings/syncTaxes'}" id="syncTaxes"> 
                                <i class="fas fa-sync fa-lg"></i> Sincronizar Todos
                            </a>
                        </li>
                    </ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });
    }

    function confirmSyncTaxes()
    {
        swal({
            title: '¡Sincronización en curso!',
            text: 'Por favor espere un momento... <br><br> <i class="fas fa-spinner fa-pulse fa-4x"></i>',
            html: true,
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        })

        window.location.href = $('#syncTaxes').attr('href');
    }
</script>
