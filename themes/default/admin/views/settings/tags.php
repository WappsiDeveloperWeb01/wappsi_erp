<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="tagsTable" class="table table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><?= lang("module"); ?></th>
                                            <th><?= lang("description"); ?></th>
                                            <th><?= lang("type"); ?></th>
                                            <th><?= lang("color"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th><?= lang("actions") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty">
                                                <?= lang('loading_data_from_server') ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadDataTables();
    });

    function loadDataTables() {
        oTable = $('#tagsTable').dataTable({
            aaSorting: [
                [2, "asc"]
            ],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/getAllTags') ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [{
                    visible: false
                },
                {
                    mRender: function(data, type, row) {
                        let modules = {
                            1 : "Producto",
                            2 : "Vental Detal",
                            3 : "Vental POS",
                            4 : "Compra",
                            5 : "Documento de Soporte",
                            6 : "Nota débito",
                            7 : "Nota crédito de Venta",
                            8 : "Nota crédito de Compra",
                            9 : "Nota crédito por Otros conceptos Venta",
                            10 : "Nota crédito por Otros conceptos Compra",
                            11 : "Cotización Venta",
                            12 : "Orden de pedido",
                        };
                        return modules[data];
                    }
                },
                null,
                {
                    mRender: function(data, type, row) {
                        let typeTag = { 1: "Normal", 2: "Urgente" }
                        return typeTag[data];
                    }
                },
                {
                    className: 'text-center',
                    sWidth: '65px',
                    mRender: function(data, type, row) {
                        return '<label class="color-option color-option-lg" style="background-color: '+data+'" data-color="'+data+'"></label>';
                    }
                },
                {
                    mRender: function(data, type, row) {
                        let statusTag = { 1: "Activo", 2: "Inactivo" }
                        return statusTag[data];
                    }
                },
                {
                    bSortable: false,
                    sWidth: '65px',
                    className: 'text-center'
                }
            ],
            fnDrawCallback: function(oSettings) {
                $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/addTag') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });
    }
</script>