<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .tooltip {
        z-index: : 9999999 !important;
    }
</style>
<script>
</script>
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $page_title ?></h2>
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open("sales/export_accountant_notes" . '', ['id' => 'export_accountant_notes']); ?>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <?= lang('module', 'module') ?>
                                        <select name="module" class="form-control module" required>
                                            <option value=""><?= lang('select') ?></option>
                                            <option value="1"><?= lang('modules')[1] ?></option>
                                            <option value="2"><?= lang('modules')[2] ?></option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <?= lang('date', 'date') ?>
                                        <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="right" title="<?= lang('export_accountant_notes_date_tooltip') ?>"></i>

                                        <input type="date" name="export_date" class="form-control export_date" required>
                                    </div>
                                    <div class="col-sm-12">
                                        <?= lang('export_format', 'export_format') ?>
                                        <br>
                                        <label>
                                            <input type="radio" name="export_format" id="helisa" class="form-control export_format" required value="helisa" checked>
                                            <?= lang('helisa', 'helisa') ?>
                                        </label>
                                        <label>
                                            <input type="radio" name="export_format" id="default" class="form-control export_format" required value="default">
                                            <?= lang('default', 'default') ?>
                                        </label>
                                    </div>
                                    <div class="col-sm-12">
                                        <button class="btn btn-primary validate" type="button"><?= lang('validate') ?></button>
                                        <button class="btn btn-success send" type="button" disabled><?= lang('send') ?></button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
        
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
        $("#export_accountant_notes").validate({
          ignore: []
        });
        $(document).on('click', '.validate', function(){
            if ($('#export_accountant_notes').valid()) {
                $.ajax({
                    url : site.base_url+"sales/validate_export_accountant_notes",
                    data : $('#export_accountant_notes').serialize(),
                    type : 'get',
                    dataType : 'JSON'
                }).done(function(data){
                    if (data.error == 0) {
                        $('.send').prop('disabled', false);
                        command: toastr.success('Validación con resultado correcto', 'Sin errores', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    } else {
                        $('.send').prop('disabled', true);
                        command: toastr.error(data.msg, '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                });
            }
        });
        $(document).on('click', '.send', function(){
            if ($('#export_accountant_notes').valid()) {
                $('#export_accountant_notes').submit();
            }
        });
        $(document).on('change', '.export_date, .module', function(){
            $('.send').prop('disabled', true);
        });
    });

</script>