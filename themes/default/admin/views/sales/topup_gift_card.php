<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= sprintf(lang('topup_gift_card'), lang('gift_card')).' ('.$card->card_no.')'; ?></h4>
        </div>
        <?php $attrib = ["id"=>"mv_add", 'target'=>'_blank'];
        echo admin_form_open("sales/topup_gift_card/".$card->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= lang("biller", "mv_biller"); ?>
                <?php
                $bl[""] = lang('select');
                foreach ($billers as $biller) {
                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                }
                echo form_dropdown('biller_id', $bl, $Settings->default_biller, 'id="mv_biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                ?>
            </div>
            <div class="form-group">
                <?= lang("reference", "mv_document_type_id"); ?>
                <select name="document_type_id" class="form-control" id="mv_document_type_id" required>

                </select>
            </div>
            <?php if (isset($cost_centers)): ?>
                <div class="form-group">
                    <?= lang('cost_center', 'mv_cost_center_id') ?>
                    <?php
                    $ccopts[''] = lang('select');
                    foreach ($cost_centers as $cost_center) {
                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                    }
                     ?>
                     <?= form_dropdown('cost_center_id', $ccopts, '', 'id="mv_cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                </div>
            <?php endif ?>

            <div class="form-group">
                <?= lang("paying_by", "paid_by"); ?>
                <select name="paid_by" id="paid_by" class="form-control" required>
                    <?= $this->sma->paid_opts(null, false, true, false, true); ?>
                </select>
            </div>
            <script type="text/javascript">
                $('#paid_by option').each(function(index, option){
                    if ($(option).val() == 'gift_card') {
                        $(option).remove();
                    }
                });
            </script>
            <div class="form-group">
                <?= lang("amount", "amount"); ?>
                <?php echo form_input('amount', '', 'class="form-control only_number" id="amount" required="required"'); ?>
            </div>
            <div class="form-group">
                <?= lang("expiry_date", "expiry"); ?>
                <input type="date" name="expiry" class="form-control" value="<?= date("Y-m-d", strtotime("+1 year")) ?>" required>
            </div>

        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary form_submit"><?= sprintf(lang('topup_gift_card'), lang('gift_card')) ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#mv_add").validate({
                ignore: []
            });

            $(document).on('click', '.form_submit', function(){
                if ($('#mv_add').valid()) {
                    $('#mv_add').submit();
                    $('.form_submit').prop('disabled', true);
                    setTimeout(function() {
                        location.reload();
                    }, 1200);
                }
            });

            $(document).on('change', '#mv_biller', function(){
                $.ajax({
                    url: site.base_url+'billers/getBillersDocumentTypes/53/'+$('#mv_biller').val(),
                    type:'get',
                    dataType:'JSON'
                }).done(function(data){
                    response = data;
                    $('#mv_document_type_id').html(response.options).select2();
                    if (response.not_parametrized != "") {
                        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                    "showDuration": "500",
                                    "hideDuration": "1000",
                                    "timeOut": "6000",
                                    "extendedTimeOut": "1000",
                                });
                    }
                    if (response.status == 0) {
                      $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                    }
                    $('#mv_document_type_id').trigger('change');
                });
            });
            $('#mv_biller').trigger('change');
        });
    </script>
</div>
<?= $modal_js ?>
