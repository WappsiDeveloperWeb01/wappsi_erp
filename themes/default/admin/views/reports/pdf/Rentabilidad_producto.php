<?php
/*
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'vendor/fpdf/fpdf.php';
*/

require_once APPPATH.'../vendor/fpdf/fpdf.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {     
        $this->SetFont('Arial','B',12);
        $this->Image($this->Logo,18,8,30,0);
        $this->Cell(70);        
        $this->Cell(80,4,'Informe de Rentabilidad por Producto',0,0,'C');
        $this->Ln();
        $this->Cell(70); 
        $this->SetFont('Arial','',6);
        $this->Cell(80,4,'Fecha : '.date('d-m-Y h:i A'),0,0,'C');
        $this->Ln(4);
        $this->SetFont('Arial','B',$this->fuente+1);

        $this->Cell(20,6, 'Fecha','TB',0,'C');
        $this->Cell(45,6, 'Producto','TB',0,'C');
        $this->Cell(28,6, 'Total Unitario','TB',0,'R');
        $this->Cell(28,6, 'Cantidad','TB',0,'R');
        $this->Cell(28,6, 'Total','TB',0,'R');        
        $this->Cell(28,6, 'Costo','TB',0,'R');
        $this->Cell(0,6, 'Utilidad','TB',0,'R');
        $this->Ln();
    }

    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica '.'            Usuario: '.$this->User),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

}

$fuente = 6;
$adicional_fuente = 2;

$height = 4;

$pdf = new PDF('P', 'mm', 'Legal');
//$pdf->Logo = base_url('assets/uploads/logos/'.$Settings->logo);
$pdf->Logo = 'https://static.wixstatic.com/media/a560e4_0525ed9cae7e4d42bd1ac3c8a7851bca~mv2.png';

$pdf->AliasNbPages();

$pdf->User = $this->session->userdata('username');
$pdf->fuente = 6;

$pdf->AddPage();

$pdf->SetFont('Arial','',$fuente);
$referencia_old =  $referencia = '' ; 

foreach ($profitabilitys as $profitability)
{
    $fecha = substr($profitability['date'],0,10);

    if($referencia_old!=$profitability['reference_no'])
    {
        $referencia = ($profitability['return_sale_ref']!='') ? $profitability['return_sale_ref'] : $profitability['reference_no'] ;
        $afec = ($profitability['return_sale_ref']!='') ? $profitability['reference_no'] : '' ;

        $pdf->SetFont('Arial','',$fuente);
        $pdf->Cell(50,$height+1, 'Referencia: '.$referencia ,'T',0,'L');
        $pdf->Cell(50,$height+1, 'Documento Afectado: '.$afec ,'T',0,'L');
        $pdf->SetFont('Arial','',$fuente);
        $pdf->Cell(40,$height+1, 'Total : $ '.number_format($profitability['total'],2,'','.'),'T',0,'R');
        $pdf->Cell(0,6, '','T',0,'C');
        $pdf->Ln();
        $referencia_old=$profitability['reference_no'];
    }

    $avg_net_unit_cost = number_format($profitability['avg_net_unit_cost']*$profitability['quantity'],2,',','.'); 
    $quantity = number_format($profitability['quantity'],2,',','.');
    $total_uni = number_format($profitability['net_unit_price'],2,',','.');
    $total = number_format(($profitability['net_unit_price']*$profitability['quantity']),2,',','.');
    $utilidad = number_format(($profitability['net_unit_price']*$profitability['quantity'])-($profitability['avg_net_unit_cost']*$profitability['quantity']),2,',','.');
    
    $pdf->Cell(20,$height, $fecha ,0,0,'C');
    $pdf->Cell(45,$height, substr($this->sma->utf8Decode($profitability['name']),0,40),0,0,'L');
    $pdf->Cell(28,$height, '$ '.$total_uni ,0,0,'R');
    $pdf->Cell(28,$height, $quantity ,0,0,'R');
    $pdf->Cell(28,$height, '$ '.$total ,0,0,'R');
    $pdf->Cell(28,$height, '$ '.$avg_net_unit_cost ,0,0,'R');
    $pdf->Cell(0,$height, '$ '.$utilidad ,0,0,'R');
    $pdf->Ln();

}


$pdf->Output();
?>