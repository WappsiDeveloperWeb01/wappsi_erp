<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    <?php if ($this->session->userdata('remove_imls')) { ?>
        <?php $this->sma->unset_data('remove_imls');
    } ?>
    var is_edit = false;
    $(document).ready(function () {

    });

</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                            <?php
                            $attrib = array('id' => 'submit_import_form');
                            echo admin_form_open_multipart("purchases/add_import", $attrib);
                            ?>
                            <div class="row">
                                <div class="col-md-4 same-height">
                                    <div class="form-group">
                                        <?= lang("date", "podate"); ?>
                                        <?php if ($this->Settings->purchase_datetime_management == 1): ?>
                                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : date('d/m/Y H:i')), 'class="form-control input-tip datetime" id="imdate" required="required"'); ?>
                                        <?php else: ?>
                                            <input type="date" name="date" class="form-control skip" id="imdate" required>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4  same-height form-group">
                                        <?= lang("biller", "pobiller"); ?>
                                        <select name="biller" class="form-control" id="imbiller" required="required">
                                            <?php foreach ($billers as $biller): ?>
                                              <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" selected><?= $biller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "pobiller"); ?>
                                            <select name="biller" class="form-control" id="imbiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-4 same-height">
                                    <div class="form-group">
                                        <?= lang("add_reference_no_label", "poref"); ?><br>
                                            <div style="display: flex;">
                                                <select name="document_type_id" class="form-control" id="document_type_id" required="required">
                                                </select>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 tag_div">
                                    <?= lang('tag', 'potag') ?>
                                    <select name="tag" id="imtag" class="form-control" required>
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($tags): ?>
                                            <?php foreach ($tags as $tag_row): ?>
                                                <option value="<?= $tag_row->id ?>" data-color="<?= $tag_row->color ?>" data-type="<?= $tag_row->type ?>"><?= $tag_row->description ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-4 same-height">
                                    <?= lang("add_purchase", "impurchase"); ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Buscar compra para añadir a la importación; Sólo se obtendrán compras con estado Pendiente y sin etiqueta asignada." style="cursor: pointer"></i>
                                    <input type="hidden" name="purchase" value="" id="impurchase" class="form-control" placeholder="<?= lang("select") . ' ' . lang("single_purchase") ?>">
                                </div>
                                <div class="col-md-4 same-height">
                                    <?= lang("add_expense", "imexpense"); ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Buscar gasto para añadir a la importación;  Sólo se obtendrán gastos con estado Pendiente y sin etiqueta asignada." style="cursor: pointer"></i>
                                    <input type="hidden" name="expense" value="" id="imexpense" class="form-control" placeholder="<?= lang("select") . ' ' . lang("expense") ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= lang('status', 'status') ?>
                                    <?php
                                    $post = array('1' => lang('completed'), '0' => lang('pending'));
                                    echo form_dropdown('status', $post, '', 'id="imstatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                                <div class="col-md-4 same-height">
                                    <div class="form-group">
                                        <?= lang("supplier_creditor", "imsupplier"); ?>
                                            <input type="hidden" name="supplier" value="" id="imsupplier"
                                                class="form-control"
                                                placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" required>
                                            <input type="hidden" name="supplier_id" value="" id="supplier_id"
                                                class="form-control">
                                    </div>
                                </div>
                            </div>
                            <hr class="col-sm-11">
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <th><h3><?= lang('purchases') ?></h3></th>
                                        </tr>
                                    </table>
                                    <div class="control-group table-group" id="accordion_purchases">
                                    </div>
                                    <table>
                                        <tr>
                                            <th><h3><?= lang('expenses') ?></h3> <em class="text-danger error_expenses" style="display:none;"><?= lang('expenses_required_for_import') ?></em> </th>
                                        </tr>
                                    </table>
                                    <div class="control-group table-group" id="accordion_expenses">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                            <div class="row">
                                <div class="col-md-12">
                                    <b class="text-danger error_debug" style="display: none;"></b>
                                    <div class="from-group">
                                        <button type="button" class="btn btn-primary" id="submit_import" style="padding: 6px 15px; margin:15px 0;"><?= lang('submit') ?></button>
                                        <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    
</script>