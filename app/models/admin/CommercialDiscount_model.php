<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CommercialDiscount_model extends CI_Model {

	public function get_by_id($id)
	{
		$this->db->select("*");
		$this->db->from("collection_discounts");
		$this->db->where("id", $id);

		$response = $this->db->get();
		return $response->row();
	}

	public function update($data, $id)
	{
		$this->db->where("id", $id);
		if ($this->db->update("collection_discounts", $data)) {
			return TRUE;
		}

		return FALSE;
	}
}

/* End of file CommercialDiscount_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/CommercialDiscount_model.php */