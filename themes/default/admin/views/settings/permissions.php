<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style>
    .table td:first-child {
        font-weight: bold;
    }

    label {
        margin-right: 10px;
    }
</style>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php if (!empty($p)) { ?>
                                <?php if ($p->group_id != 1) { ?>
                                    <?= admin_form_open("system_settings/permissions/" . $id); ?>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-hover reports-table">
                                            <thead>
                                                <tr>
                                                    <th colspan="6" class="text-center"><?= $group->description . ' ( ' . $this->lang->line($group->name) . ' ) ' . $this->lang->line("group_permissions"); ?></th>
                                                </tr>
                                                <tr>
                                                    <th rowspan="2" class="text-center"><?= lang("module_name"); ?>
                                                    </th>
                                                    <th colspan="5" class="text-center"><?= lang("permissions"); ?></th>
                                                </tr>
                                                <tr>
                                                    <th class="text-center"><?= lang("view"); ?></th>
                                                    <th class="text-center"><?= lang("add"); ?></th>
                                                    <th class="text-center"><?= lang("edit"); ?></th>
                                                    <th class="text-center"><?= lang("delete"); ?></th>
                                                    <th class="text-center"><?= lang("misc"); ?></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td><?= lang("products"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="products-index" <?= $p->{'products-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="products-add" <?= $p->{'products-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="products-edit" <?= $p->{'products-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="products-delete" <?= $p->{'products-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-cost" class="checkbox" name="products-cost" <?= $p->{'products-cost'} ? "checked" : ''; ?>>
                                                            <label for="products-cost" class="padding05"><?= lang('product_cost') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-price" class="checkbox" name="products-price" <?= $p->{'products-price'} ? "checked" : ''; ?>>
                                                            <label for="products-price" class="padding05"><?= lang('product_price') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-quantity" class="checkbox" name="products-quantity" <?= $p->{'products-quantity'} ? "checked" : ''; ?>>
                                                            <label for="products-quantity" class="padding05"><?= lang('products_quantity') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-adjustments" class="checkbox" name="products-adjustments" <?= $p->{'products-adjustments'} ? "checked" : ''; ?>>
                                                            <label for="products-adjustments" class="padding05"><?= lang('adjustments') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-production_orders" class="checkbox" name="products-production_orders" <?= $p->{'products-production_orders'} ? "checked" : ''; ?>>
                                                            <label for="products-production_orders" class="padding05"><?= lang('production_orders') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-add_production_order" class="checkbox" name="products-add_production_order" <?= $p->{'products-add_production_order'} ? "checked" : ''; ?>>
                                                            <label for="products-add_production_order" class="padding05"><?= lang('add_production_order') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-barcode" class="checkbox" name="products-barcode" <?= $p->{'products-barcode'} ? "checked" : ''; ?>>
                                                            <label for="products-barcode" class="padding05"><?= lang('print_barcodes') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-stock_count" class="checkbox" name="products-stock_count" <?= $p->{'products-stock_count'} ? "checked" : ''; ?>>
                                                            <label for="products-stock_count" class="padding05"><?= lang('stock_counts') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-product_transformations" class="checkbox" name="products-product_transformations" <?= $p->{'products-product_transformations'} ? "checked" : ''; ?>>
                                                            <label for="products-product_transformations" class="padding05"><?= lang('product_transformations') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-add_product_transformation" class="checkbox" name="products-add_product_transformation" <?= $p->{'products-add_product_transformation'} ? "checked" : ''; ?>>
                                                            <label for="products-add_product_transformation" class="padding05"><?= lang('add_product_transformation') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_kardex" class="checkbox" name="products-view_kardex" <?= $p->{'products-view_kardex'} ? "checked" : ''; ?>>
                                                            <label for="products-view_kardex" class="padding05"><?= lang('view_kardex') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-import_csv" class="checkbox" name="products-import_csv" <?= $p->{'products-import_csv'} ? "checked" : ''; ?>>
                                                            <label for="products-import_csv" class="padding05"><?= lang('import_products') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_graph" class="checkbox" name="products-view_graph" <?= $p->{'products-view_graph'} ? "checked" : ''; ?>>
                                                            <label for="products-view_graph" class="padding05"><?= lang('view_graph') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_sales" class="checkbox" name="products-view_sales" <?= $p->{'products-view_sales'} ? "checked" : ''; ?>>
                                                            <label for="products-view_sales" class="padding05"><?= lang('view_sales') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_order_sales" class="checkbox" name="products-view_order_sales" <?= $p->{'products-view_order_sales'} ? "checked" : ''; ?>>
                                                            <label for="products-view_order_sales" class="padding05"><?= lang('view_order_sales') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_quotes" class="checkbox" name="products-view_quotes" <?= $p->{'products-view_quotes'} ? "checked" : ''; ?>>
                                                            <label for="products-view_quotes" class="padding05"><?= lang('view_quotes') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_purchases" class="checkbox" name="products-view_purchases" <?= $p->{'products-view_purchases'} ? "checked" : ''; ?>>
                                                            <label for="products-view_purchases" class="padding05"><?= lang('view_purchases') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_transfers" class="checkbox" name="products-view_transfers" <?= $p->{'products-view_transfers'} ? "checked" : ''; ?>>
                                                            <label for="products-view_transfers" class="padding05"><?= lang('view_transfers') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-view_adjustments" class="checkbox" name="products-view_adjustments" <?= $p->{'products-view_adjustments'} ? "checked" : ''; ?>>
                                                            <label for="products-view_adjustments" class="padding05"><?= lang('view_adjustments') ?></label>
                                                        </span>
                                                        <hr>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-sync_pro_store_products" class="checkbox" name="products-sync_pro_store_products" <?= $p->{'products-sync_pro_store_products'} ? "checked" : ''; ?>>
                                                            <label for="products-sync_pro_store_products" class="padding05"><?= lang("Sincronizar Productos Tienda PRO") ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-sync_pro_store_prices_quantities" class="checkbox" name="products-sync_pro_store_prices_quantities" <?= $p->{'products-sync_pro_store_prices_quantities'} ? "checked" : ''; ?>>
                                                            <label for="products-sync_pro_store_prices_quantities" class="padding05"><?= lang("Sincronizar Precios y Cantidades Tienda PRO") ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="products-hide_products_without_photos" class="checkbox" name="products-hide_products_without_photos" <?= $p->{'products-hide_products_without_photos'} ? "checked" : ''; ?>>
                                                            <label for="products-hide_products_without_photos" class="padding05"><?= lang("Ocultar Productos sin Foto en Tienda PRO") ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <?= lang('making_orders') ?>
                                                    </th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="production_order-index" <?= $p->{'production_order-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="production_order-add" <?= $p->{'production_order-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="production_order-edit" <?= $p->{'production_order-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td></td>
                                                    <td>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-cutting_orders" <?= $p->{'production_order-cutting_orders'} ? "checked" : ''; ?>>
                                                            <?= lang('cutting_orders') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-add_cutting_order" <?= $p->{'production_order-add_cutting_order'} ? "checked" : ''; ?>>
                                                            <?= lang('add_cutting_order') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-change_cutting_order_items_status" <?= $p->{'production_order-change_cutting_order_items_status'} ? "checked" : ''; ?>>
                                                            <?= lang('change_cutting_order_items_status') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-assemble_orders" <?= $p->{'production_order-assemble_orders'} ? "checked" : ''; ?>>
                                                            <?= lang('assemble_orders') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-add_assemble_order" <?= $p->{'production_order-add_assemble_order'} ? "checked" : ''; ?>>
                                                            <?= lang('add_assemble_order') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-change_assemble_order_items_status" <?= $p->{'production_order-change_assemble_order_items_status'} ? "checked" : ''; ?>>
                                                            <?= lang('change_assemble_order_items_status') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-packing_orders" <?= $p->{'production_order-packing_orders'} ? "checked" : ''; ?>>
                                                            <?= lang('packing_orders') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-add_packing_order" <?= $p->{'production_order-add_packing_order'} ? "checked" : ''; ?>>
                                                            <?= lang('add_packing_order') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-change_packing_order_items_status" <?= $p->{'production_order-change_packing_order_items_status'} ? "checked" : ''; ?>>
                                                            <?= lang('change_packing_order_items_status') ?>
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" value="1" class="checkbox" name="production_order-production_order_report" <?= $p->{'production_order-production_order_report'} ? "checked" : ''; ?>>
                                                            <?= lang('production_order_report') ?>
                                                        </label>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("sales"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-index" <?= $p->{'sales-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-add" <?= $p->{'sales-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-edit" <?= $p->{'sales-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-delete" <?= $p->{'sales-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-email" class="checkbox" name="sales-email" <?= $p->{'sales-email'} ? "checked" : ''; ?>>
                                                            <label for="sales-email" class="padding05"><?= lang('email') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-pdf" class="checkbox" name="sales-pdf" <?= $p->{'sales-pdf'} ? "checked" : ''; ?>>
                                                            <label for="sales-pdf" class="padding05"><?= lang('pdf') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-payments" class="checkbox" name="sales-payments" <?= $p->{'sales-payments'} ? "checked" : ''; ?>>
                                                            <label for="sales-payments" class="padding05"><?= lang('payments') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-return_sales" class="checkbox" name="sales-return_sales" <?= $p->{'sales-return_sales'} ? "checked" : ''; ?>>
                                                            <label for="sales-return_sales" class="padding05"><?= lang('return_sales') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-return_sales_fe" class="checkbox" name="sales-return_sales_fe" <?= $p->{'sales-return_sales_fe'} ? "checked" : ''; ?>>
                                                            <label for="sales-return_sales_fe" class="padding05"><?= lang('return_sales_fe') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="payments-index" class="checkbox" name="payments-index" <?= $p->{'payments-index'} ? "checked" : ''; ?>>
                                                            <label for="payments-index" class="padding05"><?= lang('multi_payments') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="payments-add" class="checkbox" name="payments-add" <?= $p->{'payments-add'} ? "checked" : ''; ?>>
                                                            <label for="payments-add" class="padding05"><?= lang('add_multi_payment_sales') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="payments-cancel" class="checkbox" name="payments-cancel" <?= $p->{'payments-cancel'} ? "checked" : ''; ?>>
                                                            <label for="payments-cancel" class="padding05"><?= lang('cancel_payment') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-fe_index" class="checkbox" name="sales-fe_index" <?= $p->{'sales-fe_index'} ? "checked" : ''; ?>>
                                                            <label for="sales-fe_index" class="padding05"><?= lang('list_sales_fe') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="returns-credit_note_other_concepts" class="checkbox" name="returns-credit_note_other_concepts" <?= $p->{'returns-credit_note_other_concepts'} ? "checked" : ''; ?>>
                                                            <label for="returns-credit_note_other_concepts" class="padding05"><?= lang('credit_note_other_concepts_label_menu') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="reports-collection_commissions" class="checkbox" name="reports-collection_commissions" <?= $p->{'reports-collection_commissions'} ? "checked" : ''; ?>>
                                                            <label for="reports-collection_commissions" class="padding05"><?= lang('collection_commissions_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-pay_commissions" class="checkbox" name="sales-pay_commissions" <?= $p->{'sales-pay_commissions'} ? "checked" : ''; ?>>
                                                            <label for="sales-pay_commissions" class="padding05"><?= lang('pay_collection_commissions') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos-change_payment_method" class="checkbox" name="pos-change_payment_method" <?= $p->{'pos-change_payment_method'} ? "checked" : ''; ?>>
                                                            <label for="pos-change_payment_method" class="padding05"><?= lang('change_payment_method') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="budget-index" class="checkbox" name="budget-index" <?= $p->{'budget-index'} ? "checked" : ''; ?>>
                                                            <label for="budget-index" class="padding05"><?= lang('budget') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-print_server" class="checkbox" name="sales-print_server" <?= $p->{'sales-print_server'} ? "checked" : ''; ?>>
                                                            <label for="sales-print_server" class="padding05"><?= lang('print_server') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="debit_notes-add" class="checkbox" name="debit_notes-add" <?= $p->{'debit_notes-add'} ? "checked" : ''; ?>>
                                                            <label for="debit_notes-add" class="padding05"><?= lang('debit_notes_add') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <?= lang('pos_sales') ?>
                                                    </th>
                                                    <td class="text-center">
                                                        <?php if (POS) { ?>
                                                            <input type="checkbox" value="1" id="pos-sales" class="checkbox input-tip" name="pos-sales" <?= $p->{'pos-sales'} ? "checked" : ''; ?>>
                                                            <br>
                                                            <i class="fa fa-question-circle input-tip" data-tip="Acceder al listado de ventas POS" style="font-size: 100%;"></i>
                                                        <?php } ?>
                                                    </td>
                                                    <td class="text-center">
                                                        <?php if (POS) { ?>
                                                            <input type="checkbox" value="1" id="pos-index" class="checkbox" name="pos-index" <?= $p->{'pos-index'} ? "checked" : ''; ?>>
                                                            <br>
                                                            <i class="fa fa-question-circle input-tip" data-tip="Acceder al POS para agregar venta" style="font-size: 100%;"></i>
                                                        <?php } ?>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <?php if (POS) { ?>
                                                                <input type="checkbox" value="1" id="pos-add_wholesale" class="checkbox input-tip" data-tip="a" name="pos-add_wholesale" <?= $p->{'pos-add_wholesale'} ? "checked" : ''; ?>>
                                                                <label for="pos-add_wholesale" class="padding05"><?= lang('pos_wholesale') ?></label>
                                                            <?php } ?>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos-pos_register_movements" class="checkbox" name="pos-pos_register_movements" <?= $p->{'pos-pos_register_movements'} ? "checked" : ''; ?>>
                                                            <label for="pos-pos_register_movements" class="padding05"><?= lang('pos_register_movements') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos-pos_register_add_movement" class="checkbox" name="pos-pos_register_add_movement" <?= $p->{'pos-pos_register_add_movement'} ? "checked" : ''; ?>>
                                                            <label for="pos-pos_register_add_movement" class="padding05"><?= lang('pos_register_add_movement') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos_print_server" class="checkbox" name="pos_print_server" <?= $p->{'pos_print_server'} ? "checked" : ''; ?>>
                                                            <label for="pos_print_server" class="padding05"><?= lang('pos_print_server') ?></label>
                                                        </span>




                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos-order_preparation" class="checkbox" name="pos-order_preparation" <?= $p->{'pos-order_preparation'} ? "checked" : ''; ?>>
                                                            <label for="pos-order_preparation" class="padding05"><?= lang('order_preparation') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos-see_order_preparation" class="checkbox" name="pos-see_order_preparation" <?= $p->{'pos-see_order_preparation'} ? "checked" : ''; ?>>
                                                            <label for="pos-see_order_preparation" class="padding05"><?= lang('see_order_preparation') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos-close_register" class="checkbox" name="pos-close_register" <?= $p->{'pos-close_register'} ? "checked" : ''; ?>>
                                                            <label for="pos-close_register" class="padding05"><?= lang('close_register') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="pos-delete_table_order" class="checkbox" name="pos-delete_table_order" <?= $p->{'pos-delete_table_order'} ? "checked" : ''; ?>>
                                                            <label for="pos-delete_table_order" class="padding05"><?= lang('delete_table_order') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("deliveries"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-deliveries" <?= $p->{'sales-deliveries'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-add_delivery" <?= $p->{'sales-add_delivery'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-edit_delivery" <?= $p->{'sales-edit_delivery'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-delete_delivery" <?= $p->{'sales-delete_delivery'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="sales-pdf" class="checkbox" name="sales-pdf_delivery" <?= $p->{'sales-pdf_delivery'} ? "checked" : ''; ?>>
                                                            <label for="sales-pdf_delivery" class="padding05"><?= lang('pdf') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("gift_cards"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-gift_cards" <?= $p->{'sales-gift_cards'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-add_gift_card" <?= $p->{'sales-add_gift_card'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-edit_gift_card" <?= $p->{'sales-edit_gift_card'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-delete_gift_card" <?= $p->{'sales-delete_gift_card'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("quotes"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="quotes-index" <?= $p->{'quotes-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="quotes-add" <?= $p->{'quotes-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="quotes-edit" <?= $p->{'quotes-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="quotes-delete" <?= $p->{'quotes-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="quotes-email" class="checkbox" name="quotes-email" <?= $p->{'quotes-email'} ? "checked" : ''; ?>>
                                                            <label for="quotes-email" class="padding05"><?= lang('email') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="quotes-pdf" class="checkbox" name="quotes-pdf" <?= $p->{'quotes-pdf'} ? "checked" : ''; ?>>
                                                            <label for="quotes-pdf" class="padding05"><?= lang('pdf') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("orders"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-orders" <?= $p->{'sales-orders'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-add_order" <?= $p->{'sales-add_order'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="sales-edit_order" <?= $p->{'sales-edit_order'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("purchases"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="purchases-index" <?= $p->{'purchases-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="purchases-add" <?= $p->{'purchases-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="purchases-edit" <?= $p->{'purchases-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="purchases-delete_purchase" <?= $p->{'purchases-delete_purchase'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-email" class="checkbox" name="purchases-email" <?= $p->{'purchases-email'} ? "checked" : ''; ?>>
                                                            <label for="purchases-email" class="padding05"><?= lang('email') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-pdf" class="checkbox" name="purchases-pdf" <?= $p->{'purchases-pdf'} ? "checked" : ''; ?>>
                                                            <label for="purchases-pdf" class="padding05"><?= lang('pdf') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-payments" class="checkbox" name="purchases-payments" <?= $p->{'purchases-payments'} ? "checked" : ''; ?>>
                                                            <label for="purchases-payments" class="padding05"><?= lang('payments') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-expenses" class="checkbox" name="purchases-expenses" <?= $p->{'purchases-expenses'} ? "checked" : ''; ?>>
                                                            <label for="purchases-expenses" class="padding05"><?= lang('expenses') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-return_purchases" class="checkbox" name="purchases-return_purchases" <?= $p->{'purchases-return_purchases'} ? "checked" : ''; ?>>
                                                            <label for="purchases-return_purchases" class="padding05"><?= lang('return_purchases') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="payments-pindex" class="checkbox" name="payments-pindex" <?= $p->{'payments-pindex'} ? "checked" : ''; ?>>
                                                            <label for="payments-pindex" class="padding05"><?= lang('multi_payments_purchases') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="payments-padd" class="checkbox" name="payments-padd" <?= $p->{'payments-padd'} ? "checked" : ''; ?>>
                                                            <label for="payments-padd" class="padding05"><?= lang('add_multi_payment_purchases') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="payments-pcancel" class="checkbox" name="payments-pcancel" <?= $p->{'payments-pcancel'} ? "checked" : ''; ?>>
                                                            <label for="payments-pcancel" class="padding05"><?= lang('cancel_ppayment') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-return_other_concepts" class="checkbox" name="purchases-return_other_concepts" <?= $p->{'purchases-return_other_concepts'} ? "checked" : ''; ?>>
                                                            <label for="purchases-return_other_concepts" class="padding05"><?= lang('return_other_concepts') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-add_items_by_csv" class="checkbox" name="purchases-add_items_by_csv" <?= $p->{'purchases-add_items_by_csv'} ? "checked" : ''; ?>>
                                                            <label for="purchases-add_items_by_csv" class="padding05"><?= lang('add_items_by_csv') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="purchases-edition_delete_item" class="checkbox" name="purchases-edition_delete_item" <?= $p->{'purchases-edition_delete_item'} ? "checked" : ''; ?>>
                                                            <label for="purchases-edition_delete_item" class="padding05"><?= lang('edition_delete_item') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("transfers"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="transfers-index" <?= $p->{'transfers-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="transfers-add" <?= $p->{'transfers-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="transfers-edit" <?= $p->{'transfers-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="transfers-delete" <?= $p->{'transfers-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="transfers-email" class="checkbox" name="transfers-email" <?= $p->{'transfers-email'} ? "checked" : ''; ?>>
                                                            <label for="transfers-email" class="padding05"><?= lang('email') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="transfers-pdf" class="checkbox" name="transfers-pdf" <?= $p->{'transfers-pdf'} ? "checked" : ''; ?>>
                                                            <label for="transfers-pdf" class="padding05"><?= lang('pdf') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="transfers-orders" class="checkbox" name="transfers-orders" <?= $p->{'transfers-orders'} ? "checked" : ''; ?>>
                                                            <label for="transfers-orders" class="padding05"><?= lang('transfers_orders') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="transfers-add_order" class="checkbox" name="transfers-add_order" <?= $p->{'transfers-add_order'} ? "checked" : ''; ?>>
                                                            <label for="transfers-add_order" class="padding05"><?= lang('add_transfer_order') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("returns"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="returns-index" <?= $p->{'returns-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="returns-add" <?= $p->{'returns-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="returns-edit" <?= $p->{'returns-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="returns-delete" <?= $p->{'returns-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="returns-email" class="checkbox" name="returns-email" <?= $p->{'returns-email'} ? "checked" : ''; ?>>
                                                            <label for="returns-email" class="padding05"><?= lang('email') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="returns-pdf" class="checkbox" name="returns-pdf" <?= $p->{'returns-pdf'} ? "checked" : ''; ?>>
                                                            <label for="returns-pdf" class="padding05"><?= lang('pdf') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="returns-add_past_years_sale_return" class="checkbox" name="returns-add_past_years_sale_return" <?= $p->{'returns-add_past_years_sale_return'} ? "checked" : ''; ?>>
                                                            <label for="returns-add_past_years_sale_return" class="padding05"><?= lang('add_past_years_sale_return') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="returns-add_past_years_purchase_return" class="checkbox" name="returns-add_past_years_purchase_return" <?= $p->{'returns-add_past_years_purchase_return'} ? "checked" : ''; ?>>
                                                            <label for="returns-add_past_years_purchase_return" class="padding05"><?= lang('add_past_years_purchase_return') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("customers"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="customers-index" <?= $p->{'customers-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="customers-add" <?= $p->{'customers-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="customers-edit" <?= $p->{'customers-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="customers-delete" <?= $p->{'customers-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="customers-list_deposits" class="checkbox" name="customers-list_deposits" <?= $p->{'customers-list_deposits'} ? "checked" : ''; ?>>
                                                            <label for="customers-list_deposits" class="padding05"><?= lang('list_deposits') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="customers-add_deposit" class="checkbox" name="customers-add_deposit" <?= $p->{'customers-add_deposit'} ? "checked" : ''; ?>>
                                                            <label for="customers-add_deposit" class="padding05"><?= lang('add_deposit') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("suppliers"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="suppliers-index" <?= $p->{'suppliers-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="suppliers-add" <?= $p->{'suppliers-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="suppliers-edit" <?= $p->{'suppliers-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="suppliers-delete" <?= $p->{'suppliers-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="suppliers-list_deposits" class="checkbox" name="suppliers-list_deposits" <?= $p->{'suppliers-list_deposits'} ? "checked" : ''; ?>>
                                                            <label for="suppliers-list_deposits" class="padding05"><?= lang('list_deposits') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="suppliers-add_deposit" class="checkbox" name="suppliers-add_deposit" <?= $p->{'suppliers-add_deposit'} ? "checked" : ''; ?>>
                                                            <label for="suppliers-add_deposit" class="padding05"><?= lang('add_deposit') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("payments_collections"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="payments_collections-index" <?= $p->{'payments_collections-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="payments_collections-add" <?= $p->{'payments_collections-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("reports"); ?></td>
                                                    <td colspan="5" class="row">
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="product_quantity_alerts" name="reports-quantity_alerts" <?= $p->{'reports-quantity_alerts'} ? "checked" : ''; ?>>
                                                            <label for="product_quantity_alerts" class="padding05"><?= lang('product_quantity_alerts') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="Product_expiry_alerts" name="reports-expiry_alerts" <?= $p->{'reports-expiry_alerts'} ? "checked" : ''; ?>>
                                                            <label for="Product_expiry_alerts" class="padding05"><?= lang('product_expiry_alerts') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="products" name="reports-products" <?= $p->{'reports-products'} ? "checked" : ''; ?>><label for="products" class="padding05"><?= lang('products_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="daily_sales" name="reports-daily_sales" <?= $p->{'reports-daily_sales'} ? "checked" : ''; ?>>
                                                            <label for="daily_sales" class="padding05"><?= lang('daily_sales') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="monthly_sales" name="reports-monthly_sales" <?= $p->{'reports-monthly_sales'} ? "checked" : ''; ?>>
                                                            <label for="monthly_sales" class="padding05"><?= lang('monthly_sales') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="sales" name="reports-sales" <?= $p->{'reports-sales'} ? "checked" : ''; ?>>
                                                            <label for="sales" class="padding05"><?= lang('sales') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="payments" name="reports-payments" <?= $p->{'reports-payments'} ? "checked" : ''; ?>>
                                                            <label for="payments" class="padding05"><?= lang('payments') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="tax" name="reports-tax" <?= $p->{'reports-tax'} ? "checked" : ''; ?>>
                                                            <label for="tax" class="padding05"><?= lang('tax_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="expenses" name="reports-expenses" <?= $p->{'reports-expenses'} ? "checked" : ''; ?>>
                                                            <label for="expenses" class="padding05"><?= lang('expenses') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="daily_purchases" name="reports-daily_purchases" <?= $p->{'reports-daily_purchases'} ? "checked" : ''; ?>>
                                                            <label for="daily_purchases" class="padding05"><?= lang('daily_purchases') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="monthly_purchases" name="reports-monthly_purchases" <?= $p->{'reports-monthly_purchases'} ? "checked" : ''; ?>>
                                                            <label for="monthly_purchases" class="padding05"><?= lang('monthly_purchases') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="purchases" name="reports-purchases" <?= $p->{'reports-purchases'} ? "checked" : ''; ?>>
                                                            <label for="purchases" class="padding05"><?= lang('purchases') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="customers" name="reports-customers" <?= $p->{'reports-customers'} ? "checked" : ''; ?>>
                                                            <label for="customers" class="padding05"><?= lang('customers') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="suppliers" name="reports-suppliers" <?= $p->{'reports-suppliers'} ? "checked" : ''; ?>>
                                                            <label for="suppliers" class="padding05"><?= lang('suppliers') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="payment_term_expired" name="reports-payment_term_expired" <?= $p->{'reports-payment_term_expired'} ? "checked" : ''; ?>>
                                                            <label for="suppliers" class="padding05"><?= lang('payment_term_expired') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="best_sellers" name="reports-best_sellers" <?= $p->{'reports-best_sellers'} ? "checked" : ''; ?>>
                                                            <label for="best_sellers" class="padding05"><?= lang('best_sellers') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="valued_products" name="reports-valued_products" <?= $p->{'reports-valued_products'} ? "checked" : ''; ?>>
                                                            <label for="valued_products" class="padding05"><?= lang('valued_products_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="adjustments" name="reports-adjustments" <?= $p->{'reports-adjustments'} ? "checked" : ''; ?>>
                                                            <label for="adjustments" class="padding05"><?= lang('adjustments') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="categories" name="reports-categories" <?= $p->{'reports-categories'} ? "checked" : ''; ?>>
                                                            <label for="categories" class="padding05"><?= lang('categories') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="brands" name="reports-brands" <?= $p->{'reports-brands'} ? "checked" : ''; ?>>
                                                            <label for="brands" class="padding05"><?= lang('brands') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="users" name="reports-users" <?= $p->{'reports-users'} ? "checked" : ''; ?>>
                                                            <label for="users" class="padding05"><?= lang('users') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="portfolio" name="reports-portfolio" <?= $p->{'reports-portfolio'} ? "checked" : ''; ?>>
                                                            <label for="portfolio" class="padding05"><?= lang('portfolio_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="portfolio_report" name="reports-portfolio_report" <?= $p->{'reports-portfolio_report'} ? "checked" : ''; ?>>
                                                            <label for="portfolio_report" class="padding05"><?= lang('portfolio_by_ages') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="debts_to_pay_report" name="reports-debts_to_pay_report" <?= $p->{'reports-debts_to_pay_report'} ? "checked" : ''; ?>>
                                                            <label for="debts_to_pay_report" class="padding05"><?= lang('debts_to_pay_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="load_zeta" name="reports-load_zeta" <?= $p->{'reports-load_zeta'} ? "checked" : ''; ?>>
                                                            <label for="load_zeta" class="padding05"><?= lang('zeta_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="load_bills" name="reports-load_bills" <?= $p->{'reports-load_bills'} ? "checked" : ''; ?>>
                                                            <label for="load_bills" class="padding05"><?= lang('seller_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="load_rentabilidad_doc" name="reports-load_rentabilidad_doc" <?= $p->{'reports-load_rentabilidad_doc'} ? "checked" : ''; ?>>
                                                            <label for="load_rentabilidad_doc" class="padding05"><?= lang('profitability_document') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="load_rentabilidad_customer" name="reports-load_rentabilidad_customer" <?= $p->{'reports-load_rentabilidad_customer'} ? "checked" : ''; ?>>
                                                            <label for="load_rentabilidad_customer" class="padding05"><?= lang('profitability_customer') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="load_rentabilidad_producto" name="reports-load_rentabilidad_producto" <?= $p->{'reports-load_rentabilidad_producto'} ? "checked" : ''; ?>>
                                                            <label for="load_rentabilidad_producto" class="padding05"><?= lang('profitability_product') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="reports-register" name="reports-register" <?= $p->{'reports-register'} ? "checked" : ''; ?>>
                                                            <label for="reports-register" class="padding05"><?= lang('register_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="reports-biller_register" name="reports-biller_register" <?= $p->{'reports-biller_register'} ? "checked" : ''; ?>>
                                                            <label for="reports-biller_register" class="padding05"><?= lang('biller_register_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="reports-serial_register" name="reports-serial_register" <?= $p->{'reports-serial_register'} ? "checked" : ''; ?>>
                                                            <label for="reports-serial_register" class="padding05"><?= lang('serial_register_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="sales_reports-budget_execution"
                                                                name="sales_reports-budget_execution" <?= $p->{'sales_reports-budget_execution'} ? "checked" : ''; ?>>
                                                            <label for="sales_reports-budget_execution" class="padding05"><?= lang('sales_budget_execution') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="reports-transfers"
                                                                name="reports-transfers" <?= $p->{'reports-transfers'} ? "checked" : ''; ?>>
                                                            <label for="reports-transfers" class="padding05"><?= lang('transfers_report') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="reports-daily_incomes"
                                                                name="reports-daily_incomes" <?= $p->{'reports-daily_incomes'} ? "checked" : ''; ?>>
                                                            <label for="reports-daily_incomes" class="padding05"><?= lang('daily_incomes_report') ?></label>
                                                        </span>

                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("misc"); ?></td>
                                                    <td colspan="5">
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" class="checkbox" id="bulk_actions" name="bulk_actions" <?= $p->bulk_actions ? "checked" : ''; ?>>
                                                            <label for="bulk_actions" class="padding05"><?= lang('bulk_actions') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" class="checkbox" id="edit_price" name="edit_price" <?= $p->edit_price ? "checked" : ''; ?>>
                                                            <label for="edit_price" class="padding05"><?= lang('edit_price_on_sale') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th colspan="6" class="text-center"><?= lang('parameters') ?></th>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("categories"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-categories" <?= $p->{'system_settings-categories'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-add_category" <?= $p->{'system_settings-add_category'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-edit_category" <?= $p->{'system_settings-edit_category'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-delete_category" <?= $p->{'system_settings-delete_category'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("units"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-units" <?= $p->{'system_settings-units'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-add_unit" <?= $p->{'system_settings-add_unit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-edit_unit" <?= $p->{'system_settings-edit_unit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-delete_unit" <?= $p->{'system_settings-delete_unit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("brands"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-brands" <?= $p->{'system_settings-brands'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-add_brand" <?= $p->{'system_settings-add_brand'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-edit_brand" <?= $p->{'system_settings-edit_brand'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-delete_brand" <?= $p->{'system_settings-delete_brand'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("tax_rates"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-tax_rates" <?= $p->{'system_settings-tax_rates'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-add_tax_rate" <?= $p->{'system_settings-add_tax_rate'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-edit_tax_rate" <?= $p->{'system_settings-edit_tax_rate'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-delete_tax_rate" <?= $p->{'system_settings-delete_tax_rate'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("payment_methods"); ?></td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-payment_methods" <?= $p->{'system_settings-payment_methods'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-add_payment_method" <?= $p->{'system_settings-add_payment_method'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-edit_payment_method" <?= $p->{'system_settings-edit_payment_method'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="system_settings-delete_payment_method" <?= $p->{'system_settings-delete_payment_method'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?= lang("price_groups"); ?></td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td class="text-center">

                                                    </td>
                                                    <td>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" class="checkbox" name="system_settings-update_products_group_prices" id="update_products_group_prices" <?= $p->{'system_settings-update_products_group_prices'} ? "checked" : ''; ?>>
                                                            <label for="update_products_group_prices" class="padding05"><?= lang('update_products_group_prices') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" class="checkbox" name="system_settings-get_products_units_xls" id="get_products_units_xls" <?= $p->{'system_settings-get_products_units_xls'} ? "checked" : ''; ?>>
                                                            <label for="get_products_units_xls" class="padding05"><?= lang('get_products_units_xls') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <?= lang('shop_settings') ?>
                                                    </th>
                                                    <th colspan="5" class="row">
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-pages" name="shop_settings-pages" <?= $p->{'shop_settings-pages'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-pages" class="padding05"><?= lang('list_pages') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-add_page" name="shop_settings-add_page" <?= $p->{'shop_settings-add_page'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-add_page" class="padding05"><?= lang('add_page') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-edit_page" name="shop_settings-edit_page" <?= $p->{'shop_settings-edit_page'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-edit_page" class="padding05"><?= lang('edit_page') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-delete_page" name="shop_settings-delete_page" <?= $p->{'shop_settings-delete_page'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-delete_page" class="padding05"><?= lang('delete_page') ?></label>
                                                        </span>

                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-index" name="shop_settings-index" <?= $p->{'shop_settings-index'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-index" class="padding05"><?= lang('shop_settings') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-slider" name="shop_settings-slider" <?= $p->{'shop_settings-slider'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-slider" class="padding05"><?= lang('slider_settings') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-sms_settings" name="shop_settings-sms_settings" <?= $p->{'shop_settings-sms_settings'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-sms_settings" class="padding05"><?= lang('sms_settings') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-send_sms" name="shop_settings-send_sms" <?= $p->{'shop_settings-send_sms'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-send_sms" class="padding05"><?= lang('send_sms') ?></label>
                                                        </span>
                                                        <span style="display:inline-block;" class="col-xs-3">
                                                            <input type="checkbox" value="1" class="checkbox" id="shop_settings-sms_log" name="shop_settings-sms_log" <?= $p->{'shop_settings-sms_log'} ? "checked" : ''; ?>>
                                                            <label for="shop_settings-sms_log" class="padding05"><?= lang('sms_log') ?></label>
                                                        </span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line('payroll') ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="payroll_management-index" <?= $p->{'payroll_management-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="payroll_management-add" <?= $p->{'payroll_management-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="payroll_management-update" <?= $p->{'payroll_management-update'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="payroll_management-delete" <?= $p->{'payroll_management-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <th colspan="2">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="payroll_management-send" name="payroll_management-send" <?= $p->{'payroll_management-send'} ? "checked" : ''; ?>>
                                                            <label for="payroll_management-send" class="padding05"><?= $this->lang->line('send') ?></label>
                                                        </span>
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="payroll_management-approve" name="payroll_management-approve" <?= $p->{'payroll_management-approve'} ? "checked" : ''; ?>>
                                                            <label for="payroll_management-approve" class="padding05"><?= $this->lang->line('approve') ?></label>
                                                        </span>
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="payroll_management-add_novelty" name="payroll_management-add_novelty" <?= $p->{'payroll_management-add_novelty'} ? "checked" : ''; ?>>
                                                            <label for="payroll_management-add_novelty" class="padding05"><?= $this->lang->line('add_novelty') ?></label>
                                                        </span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line('documents_reception') ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="documentsReception-index" <?= $p->{'documentsReception-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <th colspan="2">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="documentsReception-acknowledgment" name="documentsReception-acknowledgment" <?= $p->{'documentsReception-acknowledgment'} ? "checked" : ''; ?>>
                                                            <label for="documentsReception-acknowledgment" class="padding05"><?= $this->lang->line('acknowledgment_receipt') ?></label>
                                                        </span>
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="documentsReception-claim" name="documentsReception-claim" <?= $p->{'documentsReception-claim'} ? "checked" : ''; ?>>
                                                            <label for="documentsReception-claim" class="padding05"><?= $this->lang->line('claim'); ?></label>
                                                        </span>
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="documentsReception-reception_good_service" name="documentsReception-reception_good_service" <?= $p->{'documentsReception-reception_good_service'} ? "checked" : ''; ?>>
                                                            <label for="documentsReception-reception_good_service" class="padding05"><?= $this->lang->line('reception_good_service'); ?></label>
                                                        </span>
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="documentsReception-express_acceptance" name="documentsReception-express_acceptance" <?= $p->{'documentsReception-express_acceptance'} ? "checked" : ''; ?>>
                                                            <label for="documentsReception-express_acceptance" class="padding05"><?= $this->lang->line('express_acceptance'); ?></label>
                                                        </span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line('supporting_document') ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="supportingDocument-index" <?= $p->{'supportingDocument-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="supportingDocument-add" <?= $p->{'supportingDocument-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="supportingDocument-edit" <?= $p->{'supportingDocument-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="supportingDocument-delete" <?= $p->{'supportingDocument-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <th colspan="2">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="supportingDocument-adjustmentNote" name="supportingDocument-adjustmentNote" <?= $p->{'supportingDocument-adjustmentNote'} ? "checked" : ''; ?>>
                                                            <label for="supportingDocument-adjustmentNote" class="padding05"><?= $this->lang->line('supportingDocumentAdjustmentNote') ?></label>
                                                        </span>
                                                    </th>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line('expense_category'); ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="expense_categories-index" <?= $p->{'expense_categories-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="expense_categories-add" <?= $p->{'expense_categories-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="expense_categories-edit" <?= $p->{'expense_categories-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line('guests'); ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="hotel-guests" <?= $p->{'hotel-guests'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="hotel-add_guest" <?= $p->{'hotel-add_guest'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="hotel-edit_guest" <?= $p->{'hotel-edit_guest'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line('guest_registers'); ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="hotel-guest_registers" <?= $p->{'hotel-guest_registers'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="hotel-add_guest_register" <?= $p->{'hotel-add_guest_register'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="hotel-edit_guest_register" <?= $p->{'hotel-edit_guest_register'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                    <td>

                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="hotel-close_guest_register" name="hotel-close_guest_register" <?= $p->{'hotel-close_guest_register'} ? "checked" : ''; ?>>
                                                            <label for="hotel-close_guest_register" class="padding05"><?= $this->lang->line('close_guest_register') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line('users'); ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="auth-users" <?= $p->{'auth-users'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="auth-create_user" <?= $p->{'auth-create_user'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="auth-profile" <?= $p->{'auth-profile'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                    <td>
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-user_activities" name="system_settings-user_activities" <?= $p->{'system_settings-user_activities'} ? "checked" : ''; ?>>
                                                            <label for="system_settings-user_activities" class="padding05"><?= $this->lang->line('user_activities') ?></label>
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <?= lang('variants') ?>
                                                    </th>
                                                    <td class="text-center">
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="system_settings-variants" class="checkbox" name="system_settings-variants" <?= $p->{'system_settings-variants'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-add_variant" name="system_settings-add_variant" <?= $p->{'system_settings-add_variant'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-edit_variant" name="system_settings-edit_variant" <?= $p->{'system_settings-edit_variant'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-delete_variant" name="system_settings-delete_variant" <?= $p->{'system_settings-delete_variant'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">



                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <?= lang('product_materials') ?>
                                                    </th>
                                                    <td class="text-center">
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="system_settings-materials" class="checkbox" name="system_settings-materials" <?= $p->{'system_settings-materials'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-addMaterial" name="system_settings-addMaterial" <?= $p->{'system_settings-addMaterial'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-editMaterial" name="system_settings-editMaterial" <?= $p->{'system_settings-editMaterial'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>
                                                        <?= lang('product_colors') ?>
                                                    </th>
                                                    <td class="text-center">
                                                        <span style="display:inline-block;">
                                                            <input type="checkbox" value="1" id="system_settings-colors" class="checkbox" name="system_settings-colors" <?= $p->{'system_settings-colors'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-addColor" name="system_settings-addColor" <?= $p->{'system_settings-addColor'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                        <span>
                                                            <input type="checkbox" value="1" class="checkbox" id="system_settings-editColor" name="system_settings-editColor" <?= $p->{'system_settings-editColor'} ? "checked" : ''; ?>>
                                                        </span>
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                    <td class="text-center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line("warranty") ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="warranty-index" <?= $p->{'warranty-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="warranty-add" <?= $p->{'warranty-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="warranty-edit" <?= $p->{'warranty-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="warranty-delete" <?= $p->{'warranty-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th><?= $this->lang->line("credit_financing_language") ?></th>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="financing-index" <?= $p->{'financing-index'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="financing-add" <?= $p->{'financing-add'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="financing-edit" <?= $p->{'financing-edit'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td class="text-center">
                                                        <input type="checkbox" value="1" class="checkbox" name="financing-delete" <?= $p->{'financing-delete'} ? "checked" : ''; ?>>
                                                    </td>
                                                    <td>
                                                        <input type="checkbox" value="1" class="checkbox" name="financing-make_collections" <?= $p->{'financing-make_collections'} ? "checked" : ''; ?>>
                                                        <label for="financing-make_collections" class="padding05"><?= $this->lang->line('make_collections') ?></label>

                                                        <input type="checkbox" value="1" class="checkbox" name="financing-make_collections_reported" <?= $p->{'financing-make_collections_reported'} ? "checked" : ''; ?>>
                                                        <label for="financing-make_collections_reported" class="padding05"><?= $this->lang->line('make_collections_reported') ?></label>

                                                        <input type="checkbox" value="1" class="checkbox" name="financing-installment_edit" <?= $p->{'financing-installment_edit'} ? "checked" : ''; ?>>
                                                        <label for="financing-installment_edit" class="padding05"><?= $this->lang->line('installment_edit') ?></label>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>

                                    <div class="form-actions">
                                        <button type="submit" class="btn btn-primary"><?= lang('update') ?></button>
                                    </div>
                            <?= form_close();
                                } else {
                                    echo $this->lang->line("group_x_allowed");
                                }
                            } else {
                                echo $this->lang->line("group_x_allowed");
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>