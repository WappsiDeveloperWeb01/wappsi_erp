<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
if ($this->session->userdata('detal_post_processing')) {
    $this->session->unset_userdata('detal_post_processing');
    $this->session->unset_userdata('detal_post_processing_model');
}
?>
<script type="text/javascript">
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
    var dias_vencimiento;
    var detal_reference_no;
    var biller;
    var filter_seller;
    var filter_user;
    var customer;
    var sale_status;
    var payment_status;
    var filtered = 0;
    var filtered_ini_date;
    if (localStorage.getItem('sl_filter_filtered_ini_date')) {
        filtered_ini_date = localStorage.getItem('sl_filter_filtered_ini_date');
    }
    <?php if (isset($_POST['start_date'])): ?>
        start_date = '<?= $_POST['start_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['end_date'])): ?>
        end_date = '<?= $_POST['end_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['dias_vencimiento'])): ?>
        dias_vencimiento = '<?= $_POST['dias_vencimiento'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['detal_reference_no'])): ?>
        detal_reference_no = '<?= $_POST['detal_reference_no'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['biller'])): ?>
        biller = '<?= $_POST['biller'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['customer'])): ?>
        customer = '<?= $_POST['customer'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['sale_status'])): ?>
        sale_status = '<?= $_POST['sale_status'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['payment_status'])): ?>
        payment_status = '<?= $_POST['payment_status'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['filtered'])): ?>
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php endif ?>
    <?php if (isset($_POST['filter_user'])): ?>
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php endif ?>

    <?php if (isset($_POST['filter_seller'])): ?>
        filter_seller = '<?= $_POST['filter_seller'] ?>';
    <?php endif ?>
</script>
<script>
    var oTable;
    if ($(window).width() < 1000) {
        var nums = [[10, 25], [10, 25]];
    } else {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }
    $(document).ready(function ()
    {
        var oTable = $('#SLData').dataTable({
            aaSorting: [[1, "desc"], [3, "desc"]],
            aLengthMenu: nums,
            iDisplayLength:  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?=admin_url('sales/getSales' . ($warehouse_id ? '/' . $warehouse_id : '') .'?v=1'. ($this->input->get('shop') ? '&shop='.$this->input->get('shop') : ''). ($this->input->get('attachment') ? '&attachment='.$this->input->get('attachment') : ''). ($this->input->get('delivery') ? '&delivery='.$this->input->get('delivery') : '').'&dias_vencimiento='.(isset($dias_vencimiento) ? $dias_vencimiento : '')); ?>',
            fnServerData: function (sSource, aoData, fnCallback)
            {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                }, {
                    "name": "start_date",
                    "value": start_date
                }, {
                    "name": "end_date",
                    "value": end_date
                }, {
                    "name": "dias_vencimiento",
                    "value": dias_vencimiento
                }, {
                    "name": "detal_reference_no",
                    "value": detal_reference_no
                }, {
                    "name": "biller",
                    "value": biller
                }, {
                    "name": "customer",
                    "value": customer
                }, {
                    "name": "sale_status",
                    "value": sale_status
                }, {
                    "name": "payment_status",
                    "value": payment_status
                }, {
                    "name": "user",
                    "value": filter_user
                }, {
                    "name": "seller",
                    "value": filter_seller
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex)
            {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.setAttribute('data-return-id', aData[13]);
                nRow.className = "invoice_link re"+aData[13];
                return nRow;
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fld},
                null,
                null,
                {"bSortable": true},
                {"bSortable": true},
                null,
                null,
                {"mRender": row_status},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": pay_status},
                {"bSortable": false,"mRender": attachment},
                {"bVisible": false},
                {"bVisible": false},
                {
                    mRender: function (data, type, row) {
                        is_invoice = (row[9] >= 0) ? true : false;
                        return renderActionsButton(data, row[14], row[0], is_invoice, row[8], row[14]);
                    },
                    bSortable: false
                }
            ],
            fnDrawCallback: function (oSettings) {
                <?php if ($this->Owner || $this->Admin || $this->GP['bulk_actions']): ?>
                    $('.actionsButtonContainer').html(`<div class="pull-right dropdown">
                        <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                        <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                            <?php if ($this->Owner || $this->Admin || $this->GP['sales-add']): ?>
                                <li>
                                    <a href="<?=admin_url('sales/add')?>">
                                        <i class="fa fa-plus-circle"></i> <?=lang('add_sale')?>
                                    </a>
                                </li>
                            <?php endif ?>
                                <li>
                                    <a href="#" id="excel" data-action="export_excel">
                                        <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" id="combine" data-action="combine">
                                        <i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" id="sync_payments" data-action="sync_payments">
                                        <i class="fa fa-dollar"></i> <?=lang('sync_payments')?>
                                    </a>
                                </li>
                            <?php if ($this->Owner): ?>
                                <li>
                                    <a href="#" id="post_sale" data-action="post_sale">
                                        <i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?>
                                    </a>
                                </li>
                            <?php endif ?>
                            <?php if ($this->Owner): ?>
                                <li>
                                    <a href="#" id="sync_returns" data-action="sync_returns">
                                        <i class="fa fa-retweet"></i> <?= lang('sync_returns') ?>
                                    </a>
                                </li>
                            <?php endif ?>
                            <li>
                                <a href="#" id="mark_printed_sale" data-action="mark_printed_sale">
                                    <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_printed_sale') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="mark_all_printed_sale" data-action="mark_all_printed_sale" data-electronic="0">
                                    <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_all_printed_sale') ?>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <?php if (!empty($warehouses)) { ?>
                        <div class="pull-right dropdown">
                            <button data-toggle="dropdown" class="btn btn-primary btn-outline new-button dropdown-toggle" title="Almacenes"><i class="fa fa-building"></i></button>
                            <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                <li><a href="<?=admin_url('sales')?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                                <li class="divider"></li>
                                <?php foreach ($warehouses as $warehouse) {
                                    echo '<li><a href="' . admin_url('sales/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                } ?>
                            </ul>
                        </div>
                    <?php } ?>
                    <?php if (SHOP) { ?>
                        <div class="pull-right dropdown">
                            <button data-toggle="dropdown" class="btn btn-primary btn-outline new-button dropdown-toggle" title="Ventas"><i class="fa fa-shopping-cart"></i></button>
                            <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                <li<?= $this->input->get('shop') == 'yes' ? ' class="active"' : ''; ?>><a href="<?=admin_url('sales?shop=yes')?>"><i class="fa fa-shopping-cart"></i> <?=lang('shop_sales')?></a></li>
                                <li<?= $this->input->get('shop') == 'no' ? ' class="active"' : ''; ?>><a href="<?=admin_url('sales?shop=no')?>"><i class="fa fa-heart"></i> <?=lang('staff_sales')?></a></li>
                                <li<?= !$this->input->get('shop') ? ' class="active"' : ''; ?>><a href="<?=admin_url('sales')?>"><i class="fa fa-list-alt"></i> <?=lang('all_sales')?></a></li>
                            </ul>
                        </div>
                    <?php } ?>`);
                <?php endif ?>


                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }/* ,
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    if (aaData[aiDisplay[i]] !== undefined) {
                        gtotal += parseFloat(aaData[aiDisplay[i]][9]);
                        paid += parseFloat(aaData[aiDisplay[i]][10]);
                        balance += parseFloat(aaData[aiDisplay[i]][11]);
                    }
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[9].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[10].innerHTML = currencyFormat(parseFloat(paid));
                nCells[11].innerHTML = currencyFormat(parseFloat(balance));
            } */
        })/* .fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('payment_term');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('reference_origin');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('affects_to');?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
            {column_number: 7, filter_default_label: "[<?=lang('customer_name');?>]", filter_type: "text", data: []},
            {column_number: 8, filter_default_label: "[<?=lang('sale_status');?>]", filter_type: "text", data: []},
            {column_number: 12, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
        ], "footer") */;

        if (localStorage.getItem('remove_slls') || localStorage.getItem('remove_resl')) {
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }
            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }
            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }
            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }
            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }
            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }
            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }
            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }
            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }
            if (localStorage.getItem('slseller')) {
                localStorage.removeItem('slseller');
            }
            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }
            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }
            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }
            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('othercurrency')) {
                localStorage.removeItem('othercurrency');
            }
            if (localStorage.getItem('othercurrencycode')) {
                localStorage.removeItem('othercurrencycode');
            }
            if (localStorage.getItem('othercurrencytrm')) {
                localStorage.removeItem('othercurrencytrm');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            if (localStorage.getItem('slretenciones')) {
                localStorage.removeItem('slretenciones');
            }
            if (localStorage.getItem('keep_prices')) {
                localStorage.removeItem('keep_prices');
            }
            if (localStorage.getItem('keep_prices_quote_id')) {
                localStorage.removeItem('keep_prices_quote_id');
            }
            if (localStorage.getItem('lock_submit_by_margin')) {
                localStorage.removeItem('lock_submit_by_margin');
            }
            if (localStorage.getItem('price_updated')) {
                localStorage.removeItem('price_updated');
            }
            if (localStorage.getItem('aiu_management')) {
                localStorage.removeItem('aiu_management');
            }
            localStorage.removeItem('remove_slls');
            localStorage.removeItem('remove_resl');
        }

        <?php if ($this->session->userdata('remove_slls') || $this->session->userdata('remove_resl')) {?>
            if (localStorage.getItem('slitems')) {
                localStorage.removeItem('slitems');
            }
            if (localStorage.getItem('sldiscount')) {
                localStorage.removeItem('sldiscount');
            }
            if (localStorage.getItem('sltax2')) {
                localStorage.removeItem('sltax2');
            }
            if (localStorage.getItem('slref')) {
                localStorage.removeItem('slref');
            }
            if (localStorage.getItem('slshipping')) {
                localStorage.removeItem('slshipping');
            }
            if (localStorage.getItem('slwarehouse')) {
                localStorage.removeItem('slwarehouse');
            }
            if (localStorage.getItem('slnote')) {
                localStorage.removeItem('slnote');
            }
            if (localStorage.getItem('slinnote')) {
                localStorage.removeItem('slinnote');
            }
            if (localStorage.getItem('slcustomer')) {
                localStorage.removeItem('slcustomer');
            }
            if (localStorage.getItem('slbiller')) {
                localStorage.removeItem('slbiller');
            }
            if (localStorage.getItem('slcurrency')) {
                localStorage.removeItem('slcurrency');
            }
            if (localStorage.getItem('sldate')) {
                localStorage.removeItem('sldate');
            }
            if (localStorage.getItem('slsale_status')) {
                localStorage.removeItem('slsale_status');
            }
            if (localStorage.getItem('slpayment_status')) {
                localStorage.removeItem('slpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            if (localStorage.getItem('othercurrencycode')) {
                localStorage.removeItem('othercurrencycode');
            }
            if (localStorage.getItem('othercurrencytrm')) {
                localStorage.removeItem('othercurrencytrm');
            }
            if (localStorage.getItem('slpayment_term')) {
                localStorage.removeItem('slpayment_term');
            }
            if (localStorage.getItem('slordersale')) {
                localStorage.removeItem('slordersale');
            }
            if (localStorage.getItem('slcustomerspecialdiscount')) {
                localStorage.removeItem('slcustomerspecialdiscount');
            }
            if (localStorage.getItem('slretenciones')) {
                localStorage.removeItem('slretenciones');
            }
            if (localStorage.getItem('keep_prices')) {
                localStorage.removeItem('keep_prices');
            }
            if (localStorage.getItem('keep_prices_quote_id')) {
                localStorage.removeItem('keep_prices_quote_id');
            }
            if (localStorage.getItem('price_updated')) {
                localStorage.removeItem('price_updated');
            }
            if (localStorage.getItem('aiu_management')) {
                localStorage.removeItem('aiu_management');
            }
        <?php
            $this->sma->unset_data('remove_slls');
            $this->sma->unset_data('remove_resl');
        }
        ?>

        <?php if ($this->session->userdata('remove_oslls')) {?>
            if (localStorage.getItem('oslitems')) {
                localStorage.removeItem('oslitems');
            }
            if (localStorage.getItem('osldiscount')) {
                localStorage.removeItem('osldiscount');
            }
            if (localStorage.getItem('osltax2')) {
                localStorage.removeItem('osltax2');
            }
            if (localStorage.getItem('oslref')) {
                localStorage.removeItem('oslref');
            }
            if (localStorage.getItem('oslshipping')) {
                localStorage.removeItem('oslshipping');
            }
            if (localStorage.getItem('oslwarehouse')) {
                localStorage.removeItem('oslwarehouse');
            }
            if (localStorage.getItem('oslnote')) {
                localStorage.removeItem('oslnote');
            }
            if (localStorage.getItem('oslinnote')) {
                localStorage.removeItem('oslinnote');
            }
            if (localStorage.getItem('oslcustomer')) {
                localStorage.removeItem('oslcustomer');
            }
            if (localStorage.getItem('oslbiller')) {
                localStorage.removeItem('oslbiller');
            }
            if (localStorage.getItem('oslcurrency')) {
                localStorage.removeItem('oslcurrency');
            }
            if (localStorage.getItem('osldate')) {
                localStorage.removeItem('osldate');
            }
            if (localStorage.getItem('oslsale_status')) {
                localStorage.removeItem('oslsale_status');
            }
            if (localStorage.getItem('oslpayment_status')) {
                localStorage.removeItem('oslpayment_status');
            }
            if (localStorage.getItem('paid_by')) {
                localStorage.removeItem('paid_by');
            }
            if (localStorage.getItem('amount_1')) {
                localStorage.removeItem('amount_1');
            }
            if (localStorage.getItem('paid_by_1')) {
                localStorage.removeItem('paid_by_1');
            }
            if (localStorage.getItem('pcc_holder_1')) {
                localStorage.removeItem('pcc_holder_1');
            }
            if (localStorage.getItem('pcc_type_1')) {
                localStorage.removeItem('pcc_type_1');
            }
            if (localStorage.getItem('pcc_month_1')) {
                localStorage.removeItem('pcc_month_1');
            }
            if (localStorage.getItem('pcc_year_1')) {
                localStorage.removeItem('pcc_year_1');
            }
            if (localStorage.getItem('pcc_no_1')) {
                localStorage.removeItem('pcc_no_1');
            }
            if (localStorage.getItem('cheque_no_1')) {
                localStorage.removeItem('cheque_no_1');
            }
            if (localStorage.getItem('oslpayment_term')) {
                localStorage.removeItem('oslpayment_term');
            }
            if (localStorage.getItem('oslcustomerbranch')) {
                localStorage.removeItem('oslcustomerbranch');
            }
            if (localStorage.getItem('oslseller')) {
                localStorage.removeItem('oslseller');
            }
            if (localStorage.getItem('oslcustomerspecialdiscount')) {
                localStorage.removeItem('oslcustomerspecialdiscount');
            }
            <?php $this->sma->unset_data('remove_oslls');
        }
        ?>

        if (localStorage.getItem('remove_rels')) {
            if (localStorage.getItem('reitems')) {
                localStorage.removeItem('reitems');
            }
            if (localStorage.getItem('rediscount')) {
                localStorage.removeItem('rediscount');
            }
            if (localStorage.getItem('retax2')) {
                localStorage.removeItem('retax2');
            }
            if (localStorage.getItem('reref')) {
                localStorage.removeItem('reref');
            }
            if (localStorage.getItem('rewarehouse')) {
                localStorage.removeItem('rewarehouse');
            }
            if (localStorage.getItem('renote')) {
                localStorage.removeItem('renote');
            }
            if (localStorage.getItem('reinnote')) {
                localStorage.removeItem('reinnote');
            }
            if (localStorage.getItem('recustomer')) {
                localStorage.removeItem('recustomer');
            }
            if (localStorage.getItem('rebiller')) {
                localStorage.removeItem('rebiller');
            }
            if (localStorage.getItem('redate')) {
                localStorage.removeItem('redate');
            }

            if (localStorage.getItem('resale')) {
                localStorage.removeItem('resale');
            }
            if (localStorage.getItem('reyear_database')) {
                localStorage.removeItem('reyear_database');
            }
            localStorage.removeItem('remove_rels');
        }

        <?php if ($this->session->userdata('remove_rels')) {?>
        if (localStorage.getItem('reitems')) {
                localStorage.removeItem('reitems');
            }
            if (localStorage.getItem('rediscount')) {
                localStorage.removeItem('rediscount');
            }
            if (localStorage.getItem('retax2')) {
                localStorage.removeItem('retax2');
            }
            if (localStorage.getItem('reref')) {
                localStorage.removeItem('reref');
            }
            if (localStorage.getItem('rewarehouse')) {
                localStorage.removeItem('rewarehouse');
            }
            if (localStorage.getItem('renote')) {
                localStorage.removeItem('renote');
            }
            if (localStorage.getItem('reinnote')) {
                localStorage.removeItem('reinnote');
            }
            if (localStorage.getItem('recustomer')) {
                localStorage.removeItem('recustomer');
            }
            if (localStorage.getItem('rebiller')) {
                localStorage.removeItem('rebiller');
            }
            if (localStorage.getItem('redate')) {
                localStorage.removeItem('redate');
            }
            if (localStorage.getItem('resale')) {
                localStorage.removeItem('resale');
            }
            if (localStorage.getItem('reyear_database')) {
                localStorage.removeItem('reyear_database');
            }
        <?php $this->sma->unset_data('remove_rels');}
        ?>

        $(document).on('click', '.sledit', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });
        $(document).on('click', '.slduplicate', function (e) {
            if (localStorage.getItem('slitems')) {
                e.preventDefault();
                var href = $(this).attr('href');
                bootbox.confirm("<?=lang('you_will_loss_sale_data')?>", function (result) {
                    if (result) {
                        window.location.href = href;
                    }
                });
            }
        });

    });

</script>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <?= admin_form_open('sales', ['id'=>'sales_filter']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-lg-11">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('reference_no', 'detal_reference_no') ?>
                                            <select name="detal_reference_no" id="detal_reference_no" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <?php if ($documents_types): ?>
                                                    <?php foreach ($documents_types as $dt): ?>
                                                        <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['detal_reference_no']) && $_POST['detal_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre." (".$dt->sales_prefix.")" ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                            <?php
                                                $biller_selected = '';
                                                $biller_readonly = false;
                                                if ($this->session->userdata('biller_id')) {
                                                    $biller_selected = $this->session->userdata('biller_id');
                                                    $biller_readonly = true;
                                                }

                                                $bl[""] = lang('select');
                                                foreach ($billers as $biller) {
                                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                }
                                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                            <?php
                                            echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="filter_customer" data-placeholder="' . lang("select") . '" class="form-control input-tip" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('sale_status', 'sale_status') ?>
                                            <select name="sale_status" id="sale_status" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <option value="completed" <?= isset($_POST['sale_status']) && $_POST['sale_status'] == 'completed' ? 'selected="selected"' : '' ?>><?= lang('completed') ?></option>
                                                <option value="pending" <?= isset($_POST['sale_status']) && $_POST['sale_status'] == 'pending' ? 'selected="selected"' : '' ?>><?= lang('pending') ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('payment_status', 'payment_status') ?>
                                            <select name="payment_status" id="payment_status" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <option value="pending" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'pending' ? 'selected="selected"' : '' ?>><?= lang('pending') ?></option>
                                                <option value="due" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'due' ? 'selected="selected"' : '' ?>><?= lang('due') ?></option>
                                                <option value="partial" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'partial' ? 'selected="selected"' : '' ?>><?= lang('partial') ?></option>
                                                <option value="paid" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'paid' ? 'selected="selected"' : '' ?>><?= lang('paid') ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= lang('payment_term') ?></label>
                                            <input type="number" name="dias_vencimiento" id="dias_vencimiento" class="form-control" value="<?= isset($dias_vencimiento) ? $dias_vencimiento : '' ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 text-right">
                                <div class="new-button-container">
                                    <?php $icon = (isset($advancedFiltersContainer) && $advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= (isset($advancedFiltersContainer) && $advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    <?php if ($this->Owner || $this->Admin): ?>
                                        <div class="col-sm-2">
                                            <label><?= lang('user') ?></label>
                                            <select name="filter_user" id="filter_user" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <?php if ($users): ?>
                                                    <?php foreach ($users as $user): ?>
                                                        <option value="<?= $user->id ?>"><?= $user->first_name." ".$user->last_name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-2">
                                        <label><?= lang('seller') ?></label>
                                        <select name="filter_seller" id="filter_seller" class="form-control">
                                            <?php if (!$this->sma->seller_restricted()): ?>
                                                <option value=""><?= lang('select') ?></option>
                                            <?php endif ?>
                                            <?php if ($sellers): ?>
                                                <?php foreach ($sellers as $seller): ?>
                                                    <?php if (!$this->sma->seller_restricted()||($this->sma->seller_restricted() && $seller->id == $this->sma->seller_restricted())): ?>
                                                        <option value="<?= $seller->id ?>" <?= ($this->sma->seller_restricted() && $seller->id == $this->sma->seller_restricted()) ? "selected='selected'" : "" ?>><?= $seller->company ?></option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>

                                    <div class="date_controls_dh">
                                        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                            <div class="col-sm-2">
                                                <div class="form-group">
                                                    <?= lang('filter_year', 'filter_year_dh') ?>
                                                    <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                        <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                            <option value="<?= $key ?>"><?= $key ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <?= lang('start_date', 'start_date') ?>
                                                <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                            </div>
                                        </div>
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <?= lang('end_date', 'end_date') ?>
                                                <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $this->sma->fld($_POST['end_date']) : $this->filtros_fecha_final ?>" class="form-control datetime">
                                            </div>
                                        </div>
                                    </div>

                                    <!-- <div class="col-sm-2">
                                        <div class="form-group">
                                        <input type="hidden" name="filtered" value="1">
                                            <button type="submit" class="btn btn-primary btn-outline" id="submit-sales-filter_dh"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                            <button type="button" class="btn btn-danger btn-outline new-button" data-toggle="tooltip" data-placement="bottom" title="<?= lang('reset') ?>" onclick="window.location.href = site.base_url+'sales';">
                                                <span class="fa fa-times"></span> 
                                            </button>
                                        </div>
                                    </div> -->
                                    <!-- <div class="col-sm-1 col-without-padding text-center">
                                        <div class="new-button-container">
                                            <button type="button" class="btn btn-danger btn-outline new-button pull-right" 
                                                style="top: -1em; position: relative; right: 0.8em;"
                                                data-toggle="tooltip" data-placement="bottom" title="<?= lang('reset') ?>" onclick="window.location.href = site.base_url+'sales';">
                                                <span class="fa fa-times"></span> 
                                            </button>
                                        </div>
                                    </div> -->

                                    <?= form_close() ?>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <hr>
                                        <div class="new-button-container">
                                            <button type="button" class="btn btn-danger btn-outline new-button pull-right" 
                                                data-toggle="tooltip" data-placement="left" title="<?= lang('reset') ?>" 
                                                onclick="window.location.href = site.base_url+'sales';">
                                                <i class="fa-solid fa-rotate-left"></i> 
                                            </button>
                                        </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
    	    echo admin_form_open('sales/sale_actions', 'id="action-form"');
    	}
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="SLData" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("date"); ?></th>
                                        <th style="min-width:30px; width: 30px; !important"><?= lang("payment_term"); ?></th>
                                        <th><?= lang("reference_origin"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("affects_to"); ?></th>
                                        <th><?= lang("biller"); ?></th>
                                        <th><?= lang("customer_name"); ?></th>
                                        <th><?= lang("sale_status"); ?></th>
                                        <th><?= lang("grand_total"); ?></th>
                                        <th><?= lang("paid"); ?></th>
                                        <th><?= lang("balance"); ?></th>
                                        <th><?= lang("payment_status"); ?></th>
                                        <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                        <th></th>
                                        <th></th>
                                        <th><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                    </tr>
                                    </tbody>
                                    <!-- <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th><?= lang("grand_total"); ?></th>
                                        <th><?= lang("paid"); ?></th>
                                        <th><?= lang("balance"); ?></th>
                                        <th></th>
                                        <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                        <th></th>
                                        <th></th>
                                        <th><?= lang("actions"); ?></th>
                                    </tr>
                                    </tfoot> -->
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $Admin || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <input type="hidden" name="electronic" value="" id="electronic"/>
        <?=form_submit('performAction', 'performAction', 'id="action-form-submit"')?>
    </div>
    <?=form_close()?>
<?php }
?>

<script type="text/javascript">

    $(document).ready(function () {
        if (customer != '') {
                $('#filter_customer').val(customer).select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site.base_url+"customers/getCustomer/" + $(element).val(),
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site.base_url + "customers/suggestions",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: lang.no_match_found}]};
                            }
                        }
                    }
                });

            $('#filter_customer').trigger('change');
        } else {
            $('#filter_customer').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        }
        if (filtered !== undefined) {
            setTimeout(function() {
                console.log('biller '+biller);
                $('#start_date').val(start_date);
                $('#end_date').val(end_date);
                $('#dias_vencimiento').val(dias_vencimiento);
                $('#detal_reference_no').select2('val', detal_reference_no);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                $('#filter_seller').select2('val', filter_seller);
                $('#sale_status').select2('val', sale_status);
                $('#payment_status').select2('val', payment_status);
                // $('.collapse-link').click();
            }, 900);
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);
        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>
    });

function calcularMinutos(start_date, end_date)
{
    var fecha1 = new Date(start_date);
    var fecha2 = new Date(end_date);
    var diff = fecha2.getTime() - fecha1.getTime();
    var minutos = diff/(1000 * 60);
    return minutos;
}

function setFilterText(){
    var reference_text = $('#detal_reference_no option:selected').data('dtprefix');
    var biller_text = $('#biller option:selected').text();
    var customer_text = $('#filter_customer').select2('data') !== null ? $('#filter_customer').select2('data').text : '';
    var sale_status_text = $('#sale_status option:selected').text();
    var payment_status_text = $('#payment_status option:selected').text();
    var start_date_text = $('#start_date_dh').val();
    var end_date_text = $('#end_date_dh').val();
    var text = "Filtros configurados : ";
    coma = false;
    if (detal_reference_no != '' && detal_reference_no !== undefined) {
        text+=" Tipo documento ("+reference_text+")";
        coma = true;
    }
    if (biller != '' && biller !== undefined) {
        text+= coma ? "," : "";
        text+=" Sucursal ("+biller_text+")";
        coma = true;
    }
    if (customer != '' && customer !== undefined) {
        text+= coma ? "," : "";
        text+=" Cliente ("+customer_text+")";
        coma = true;
    }
    if (sale_status != '' && sale_status !== undefined) {
        text+= coma ? "," : "";
        text+=" Estado de venta ("+sale_status_text+")";
        coma = true;
    }
    if (payment_status != '' && payment_status !== undefined) {
        text+= coma ? "," : "";
        text+=" Estado de pago ("+payment_status_text+")";
        coma = true;
    }
    if (start_date != '' && start_date !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha de inicio ("+start_date_text+")";
        coma = true;
    }
    if (end_date != '' && end_date !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha final ("+end_date_text+")";
        coma = true;
    }
    $('.text_filter').html(text);
}
$(document).ready(function(){
    setTimeout(function() {
        <?php if (!isset($_POST['date_records_filter'])): ?>
            $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            $('#sales_filter').submit();
        <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
            $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php endif ?>
    }, 150);
});

</script>