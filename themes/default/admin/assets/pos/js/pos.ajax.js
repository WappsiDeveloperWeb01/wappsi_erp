var type_pos = 1;
var time_for_unlock;
var customer_data;
if (remove_posls) {
    if (localStorage.getItem('positems')) {
        localStorage.removeItem('positems');
    }
    if (localStorage.getItem('posdiscount')) {
        localStorage.removeItem('posdiscount');
    }
    if (localStorage.getItem('postax2')) {
        localStorage.removeItem('postax2');
    }
    if (localStorage.getItem('posshipping')) {
        localStorage.removeItem('posshipping');
    }
    if (localStorage.getItem('posref')) {
        localStorage.removeItem('posref');
    }
    if (localStorage.getItem('poswarehouse')) {
        localStorage.removeItem('poswarehouse');
    }
    if (localStorage.getItem('posnote')) {
        localStorage.removeItem('posnote');
    }
    if (localStorage.getItem('posinnote')) {
        localStorage.removeItem('posinnote');
    }
    if (localStorage.getItem('poscustomer')) {
        localStorage.removeItem('poscustomer');
    }
    if (localStorage.getItem('poscurrency')) {
        localStorage.removeItem('poscurrency');
    }
    if (localStorage.getItem('posdate')) {
        localStorage.removeItem('posdate');
    }
    if (localStorage.getItem('posstatus')) {
        localStorage.removeItem('posstatus');
    }
    if (localStorage.getItem('posbiller')) {
        localStorage.removeItem('posbiller');
    }
    if (localStorage.getItem('posseller')) {
        localStorage.removeItem('posseller');
    }
    if (localStorage.getItem('posretenciones')) {
        localStorage.removeItem('posretenciones');
    }
    if (localStorage.getItem('restobar_mode_module')) {
        localStorage.removeItem('restobar_mode_module');
    }
    if (localStorage.getItem('tip_amount')) {
        localStorage.removeItem('tip_amount');
    }
    if (localStorage.getItem('delete_tip_amount')) {
        localStorage.removeItem('delete_tip_amount');
    }
    if (localStorage.getItem('poskeylog')) {
        localStorage.removeItem('poskeylog');
    }
    if (localStorage.getItem('poscustomerspecialdiscount')) {
        localStorage.removeItem('poscustomerspecialdiscount');
    }
    if (localStorage.getItem('pos_keep_prices')) {
        localStorage.removeItem('pos_keep_prices');
    }
    if (localStorage.getItem('pos_keep_prices_quote_id')) {
        localStorage.removeItem('pos_keep_prices_quote_id');
    }
    if (localStorage.getItem('restobar_mode_module')) {
        localStorage.removeItem('restobar_mode_module');
    }
    if (localStorage.getItem('prev_poscustomer')) {
        localStorage.removeItem('prev_poscustomer');
    }
    if (localStorage.getItem('prev_posbiller')) {
        localStorage.removeItem('prev_posbiller');
    }
}
if (localStorage.getItem('posshipping')) {
    localStorage.removeItem('posshipping');
}
if (localStorage.getItem('subtotal')) {
    localStorage.removeItem('subtotal');
}
if (localStorage.getItem('tax')) {
    localStorage.removeItem('tax');
}
if (localStorage.getItem('total')) {
    localStorage.removeItem('total');
}
if (localStorage.getItem('discount')) {
    localStorage.removeItem('discount');
}
if (localStorage.getItem('gtotal')) {
    localStorage.removeItem('gtotal');
}
if (localStorage.getItem('shipping')) {
    localStorage.removeItem('shipping');
}
if (localStorage.getItem('customer_validate_min_base_retention')) {
    localStorage.removeItem('customer_validate_min_base_retention');
}
if (localStorage.getItem('poskeylog')) {
    localStorage.removeItem('poskeylog');
}
if (localStorage.getItem('prev_poscustomer')) {
    localStorage.removeItem('prev_poscustomer');
}
if (localStorage.getItem('prev_posbiller')) {
    localStorage.removeItem('prev_posbiller');
}
if (localStorage.getItem('gram_current_price')) {
    localStorage.removeItem('gram_current_price');
}
var updated_items = 0;
var prices_checked = false;
/* RESTOBAR MODE */
var restobar_mode_module = false;
if (localStorage.getItem('restobar_mode_module')) {
    restobar_mode_module = localStorage.getItem('restobar_mode_module');
}
$('#restobar_mode_module').val(restobar_mode_module);
$(document).ready(function() {
    localStorage.removeItem('pos_subtotalwtax');
    localStorage.removeItem('pos_submitted');
    check_add_item_val();
    $('.select2-search').on('keydown', function (e) {
        if (e.keyCode == 13) {
            return false;
        }
    });
    $('#biller').val($('#posbiller').val());
    $('body a, body button').attr('tabindex', -1);
    $(".open-favorites").click(function () {
        setTimeout(function() {
            $('#favorites-slider').toggle('slide', { direction: 'up' }, 700);
        }, 450);
    });
    $(".open-featured").click(function () {
        setTimeout(function() {
            $('#featured-slider').toggle('slide', { direction: 'up' }, 700);
        }, 450);
    });
    $(".open-brands").click(function () {
        setTimeout(function() {
            $('#brands-slider').toggle('slide', { direction: 'up' }, 700);
        }, 450);
    });
    $(".open-category").click(function () {
        setTimeout(function() {
            $('#category-slider').toggle('slide', { direction: 'up' }, 700);
        }, 450);
    });
    $(".open-subcategory").click(function () {
        setTimeout(function() {
            $('#subcategory-slider').toggle('slide', { direction: 'up' }, 700);
        }, 450);
    });
    $(".open-promotions").click(function () {
        setTimeout(function() {
            $('#promotions-slider').toggle('slide', { direction: 'up' }, 700);
        }, 450);
    });
    $(document).on('click', '.menu-sobre-productos', function(e){
        hide_menu_sobre_productos();
    });
    $(document).on('keyup', 'body', function(e){
        if (e.keyCode == 27) {
            hide_menu_sobre_productos();
        }
    });
    $('.po').popover({html: true, placement: 'right', trigger: 'click'}).popover();
    // $('#inlineCalc').calculator({layout: ['_%+-CABS','_7_8_9_/','_4_5_6_*','_1_2_3_-','_0_._=_+'], showFormula:true});
    // $('.calc').click(function(e) { e.stopPropagation();});
    $(document).on('click', '[data-toggle="ajax"]', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $.get(href, function( data ) {
            $("#myModal").html(data).modal();
        });
    });
    $(document).on('click', '.sname', function(e) {
        var row = $(this).closest('tr');
        var positems_id = $(row).data('item-id');
        var itemid = row.find('.rid').val();
        if (positems[positems_id].preferences) {
            if (positems[positems_id].preferences) {
                $('#myModal').modal({remote: site.base_url + 'pos/product_preferences_selection/' + itemid + "/" + positems_id,
                backdrop: 'static',
                keyboard: false});
                $('#myModal').modal('show');
            }
        } else {
            if (site.settings.product_clic_action == 1) {
                $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + itemid});
                $('#myModal').modal('show');
            } else if (site.settings.product_clic_action == 2) {
                // if (positems[positems_id].options && positems[positems_id].row.variant_selected === undefined) {
                if (positems[positems_id].options && positems[positems_id].options.length > 1) {
                    $('#myModal').modal({remote: site.base_url + 'pos/product_variants_selection/' + itemid + "/" + positems_id + "/"+$('#poswarehouse').val()});
                    $('#myModal').modal('show');
                }
            } else if (site.settings.product_clic_action == 3) {
                row.find('.edit').trigger('click');
            }
        }

    });
    setTimeout(function() {
        $('.order_detail').fadeOut(100);
        var span = $('.delete_tip_amount > i');
        if (pos_settings.apply_suggested_tip > 0 && localStorage.getItem('delete_tip_amount')) {
                span.removeClass('fa-times');
                span.addClass('fa-refresh');
        }
        if (pos_settings.show_suspended_bills_automatically == 1 && pos_settings.restobar_mode != 1 && !localStorage.getItem('positems') && count_suspended_bills > 0) {
            $('.opened_bills_sup').click();
        }
        $("#add_item").focus();
    }, 1000);
    $('#left-middle').css('height', ($('.wrapper-content').height() * 0.46));
    $('.quick-menu').css('height', ($('.wrapper-content').height() * 0.4));
    if (posbiller = localStorage.getItem('posbiller')) {
        if (user_biller_id && posbiller != user_biller_id) {
            posbiller = user_biller_id;
        }
        $('#posbiller').select2('val', posbiller).trigger('change');
    } else {
        $('#posbiller').trigger('change');
    }
    // Order level shipping and discount localStorage
    if (posdiscount = localStorage.getItem('posdiscount')) {
        $('#posdiscount').val(posdiscount);
    }
    $(document).on('change', '#ppostax2', function () {
        localStorage.setItem('postax2', $(this).val());
        $('#postax2').val($(this).val());
    });
    if (postax2 = localStorage.getItem('postax2')) {
        $('#postax2').val(postax2);
    }
    $(document).on('blur', '#sale_note', function () {
        localStorage.setItem('posnote', $(this).val());
        $('#sale_note').val($(this).val());
    });
    if (posnote = localStorage.getItem('posnote')) {
        $('#sale_note').val(posnote);
    }
    $(document).on('blur', '#staffnote', function () {
        localStorage.setItem('staffnote', $(this).val());
        $('#staffnote').val($(this).val());
    });
    $(document).on('blur', '#input_locator_details', function () {
        localStorage.setItem('locator_details', $(this).val());
        $('#locator_details').val($(this).val());
    });
    if (staffnote = localStorage.getItem('staffnote')) {
        $('#staffnote').val(staffnote);
    }
    if (posshipping = localStorage.getItem('posshipping')) {
        $('#posshipping').val(posshipping);
        shipping = parseFloat(posshipping);
    }
    $("#pshipping").click(function(e) {
        e.preventDefault();
        shipping = $('#posshipping').val() ? $('#posshipping').val() : shipping;
        $('#shipping_input').val(shipping);
        $('#sModal').modal();
    });
    $('#sModal').on('shown.bs.modal', function() {
        $(this).find('#shipping_input').select().focus();
    });
    $(document).on('click', '#updateShipping', function() {
        var s = parseFloat($('#shipping_input').val() ? $('#shipping_input').val() : '0');
        if (is_numeric(s)) {
            $('#posshipping').val(s);
            localStorage.setItem('posshipping', s);
            shipping = s;
            loadItems();
            $('#sModal').modal('hide');
        } else {
            bootbox.alert(lang.unexpected_value);
        }
    });
    $("#ptip").click(function(e) {
        e.preventDefault();
        tip_amnt = $('#sale_tip_amount').val() ? $('#sale_tip_amount').val() : tip_amount;
        $('#tip_amount_input').val(tip_amnt);
        $('#tipModal').modal();
    });
    $('#tipModal').on('shown.bs.modal', function() {
        $(this).find('#tip_amount_input').select().focus();
    });
    $(document).on('click', '#updateTip', function() {
        var s = $('#tip_amount_input').val() ? $('#tip_amount_input').val() : '0';
        if (is_numeric(s)) {
            $('#sale_tip_amount').val(s);
            localStorage.setItem('tip_amount', s);
            localStorage.removeItem('delete_tip_amount');
            localStorage.setItem('manually_entered_tip', '1');
            tip_amount = s;
            loadItems();
            $('#tipModal').modal('hide');
        } else {
            bootbox.alert(lang.unexpected_value);
        }
    });
    $(document).on('click', '#recalculateTip', function() {
        localStorage.removeItem('delete_tip_amount');
        localStorage.setItem('tip_amount', 0);
        loadItems();
        $('#tipModal').modal('hide');
    });
    $(document).on('click', '#deleteTip', function() {
        localStorage.setItem('delete_tip_amount', 1);
        loadItems();
        $('#tipModal').modal('hide');
    });

    $(document).on('click', '.view_order_detail', function(){
        btn = $(this);
        if (btn.hasClass('detail_close')) {
            $('#product-list').css('height', '210px');
            $('#left-bottom').css('padding-top', 0);
            btn.removeClass('detail_close').addClass('detail_open');
            $('#icon_detail').removeClass('fa-bars').addClass('fa-minus');
            $('.order_detail').fadeIn();
        } else if (btn.hasClass('detail_open')) {
            $('#left-bottom').css('padding-top', 0);
            btn.removeClass('detail_open').addClass('detail_close');
            $('#icon_detail').removeClass('fa-minus').addClass('fa-bars');
            $('.order_detail').fadeOut(100);
            // hide_invoice_footer_items_values();
            $('#product-list').css('height', '');
        }
    });

    /* ----------------------
     * Order Discount Handler
     * ---------------------- */
    $("#ppdiscount").click(function(e) {
        e.preventDefault();
        var dval = $('#posdiscount').val() ? $('#posdiscount').val() : '0';
        $('#order_discount_input').val(dval);
        $('#dsModal').modal();
    });
    $('#dsModal').on('shown.bs.modal', function() {
        $(this).find('#order_discount_input').select().focus();
        $('#order_discount_input').bind('keypress', function(e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                var ds = $('#order_discount_input').val();
                if (is_valid_discount(ds)) {
                    $('#posdiscount').val(ds);
                    localStorage.removeItem('posdiscount');
                    localStorage.setItem('posdiscount', ds);
                    $('#payment').prop('disabled', true);
                    loadItems();
                } else {
                    bootbox.alert(lang.unexpected_value);
                }
                $('#dsModal').modal('hide');
            }
        });
    });
    $(document).on('click', '#updateOrderDiscount', function() {
        var ds = $('#order_discount_input').val() ? $('#order_discount_input').val() : '0';
        if (is_valid_discount(ds)) {
            $('#posdiscount').val(ds);
            localStorage.removeItem('posdiscount');
            localStorage.setItem('posdiscount', ds);
            $('#payment').prop('disabled', true);
            loadItems();
        } else {
            bootbox.alert(lang.unexpected_value);
        }
        $('#dsModal').modal('hide');
    });
    /* ----------------------
     * Order Tax Handler
     * ---------------------- */
     $("#pptax2").click(function(e) {
        e.preventDefault();
        var postax2 = localStorage.getItem('postax2');
        $('#order_tax_input').select2('val', postax2);
        $('#txModal').modal();
     });
     /* retención en la fuente */
     $("#pprete").click(function(e) {
        e.preventDefault();
        // var postax2 = localStorage.getItem('postax2');
        // $('#order_tax_input').select2('val', postax2);
        $('#rtModal').modal();
     });
     /* retención en la fuente */
     $('#txModal').on('shown.bs.modal', function() {
        $(this).find('#order_tax_input').select2('focus');
     });
     $('#txModal').on('hidden.bs.modal', function() {
        var ts = $('#order_tax_input').val();
        $('#postax2').val(ts);
        localStorage.setItem('postax2', ts);
        loadItems();
     });
     $(document).on('click', '#updateOrderTax', function () {
        var ts = $('#order_tax_input').val();
        $('#postax2').val(ts);
        localStorage.setItem('postax2', ts);
        loadItems();
        $('#txModal').modal('hide');
     });
     $(document).on('click', '#updateOrderRete', function () {
        $('#ajaxCall').fadeIn();
        var posretenciones = {
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            };
        posretenciones.total_discounted = formatMoney(localStorage.gtotal - parseFloat(formatDecimal($('#total_rete_amount').text())));
        setTimeout(function() {
            $('#gtotal').text(posretenciones.total_discounted);
            $('#ajaxCall').fadeOut();
        }, 1000);
        localStorage.setItem('posretenciones', JSON.stringify(posretenciones));
        if (parseFloat(formatDecimal(posretenciones.total_rete_amount)) > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(posretenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }
        $('#rete').text(posretenciones.total_rete_amount);
        $('#rtModal').modal('hide');
     });
     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('posretenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });
     $(document).on('change', '.rserial', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        positems[item_id].row.serial = $(this).val();
        localStorage.setItem('positems', JSON.stringify(positems));
     });
    // clear localStorage and reload
    localStorage.setItem('cancel_order', false);
    $(document).on('click', '#cancel_order', function(e) { e.preventDefault(); cancel_order(e); });
    function cancel_order(e)
    {
        localStorage.setItem('cancel_order', true);
        reset_pos_sale(e);
    }

    $(document).on('click', '#reset', function(e) {
        let restobarModeModule = $('#restobar_mode_module').val()
        let suspendSaleId = $('#suspend_sale_id').val()
        if (restobarModeModule == '1') {
            swal({
                title: "¿Está seguro de eliminar la Órden?",
                text: "Esta acción borrará completamente la órden y los cambios se perderán.",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "No",
                confirmButtonText: "¡Si, Cerrar!",
                closeOnConfirm: true
            }, function () {
                existing_products_dispatched()
            });
        } else {
            reset_pos_sale(e)
        }
    });

    function existing_products_dispatched() {
        let suspend_sale_id = $('#suspend_sale_id').val();

        $.ajax({
            url: site.base_url+"pos/existing_products_dispatched",
            type: 'GET',
            dataType: 'JSON',
            data: {
                'suspended_sale_id': suspend_sale_id
            },
        })
        .done(function(data) {
            if (data == false) {
                deleteOrder()
            } else {
                swal({
                    title: "¡Advertencia!",
                    text: "No es posible eliminar la orden de pedido debido a que ya existen productos despachados.",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    cancelButtonText: "No",
                    confirmButtonText: "Ok!",
                    closeOnConfirm: true
                })
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function deleteOrder () {
        let table_id = $('#table_id').val();
        let suspend_sale_id = $('#suspend_sale_id').val();

        clean_localstorage();
        $('#modal-loading').show();

        $.ajax({
            url: site.base_url+"pos/delete",
            type: 'GET',
            dataType: 'JSON',
            data: {
                'id': suspend_sale_id
            }
        })
        .done(function(data) {
            $.ajax({
                url: site.base_url+"pos/change_table_status",
                type: 'GET',
                dataType: 'JSON',
                data: {
                    'table_id': table_id
                }
            })
            .done(function(data) {
                window.location.href = site.base_url+"pos";
            })
            .fail(function(data) {
                console.log(data.responseText);
            });
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function reset_pos_sale(e) {
        if (protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 4 || protect_delete_pin_request == 2)) {
            $.ajax({
                url : site.base_url+'billers/get_random_pin_code',
                data : {
                    biller_id : $('#posbiller').val()
                },
                dataType : 'JSON',
                type : 'get',
                success : function(data){
                    if (data.pin_code) {
                        biller_pin_code = data.pin_code;
                        var boxd = bootbox.dialog({
                            title: "<i class='fa fa-key'></i> Código de autorización",
                            message: '<input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
                            buttons: {
                                success: {
                                    label: "<i class='fa fa-tick'></i> OK",
                                    className: "btn-success verify_pin",
                                    callback: function () {
                                        var pos_pin = md5($('#pos_pin').val());
                                        if(pos_pin == biller_pin_code) {
                                            clean_localstorage();
                                            $('#modal-loading').show();
                                            if (is_order) {
                                                window.location.href = site.base_url+"sales/orders";
                                            } else {
                                                window.location.href = site.base_url+"pos";
                                            }
                                        } else {
                                            bootbox.alert('Código de autorización incorrecto');
                                        }
                                    }
                                }
                            }
                        });
                    } else {
                        bootbox.alert('Código de autorización incorrecto');
                    }
                },
                fail : function(data){
                    bootbox.alert('Código de autorización incorrecto');
                }
            });
        } else {
            bootbox.confirm(lang.r_u_sure_suspend_sale, function (result) {
                if (result) {
                    clean_localstorage();
                    $('#modal-loading').show();
                    if (is_order) {
                        window.location.href = site.base_url+"sales/orders";
                    } else {
                        window.location.href = site.base_url+"pos";
                    }
                }
            });
        }
        localStorage.setItem('cancel_order', false);
    }


    // save and load the fields in and/or from localStorage
    $('#poswarehouse').change(function (e) {
        localStorage.setItem('poswarehouse', $(this).val());

        updateQuantityItems($(this).val());
    });
    if (poswarehouse = localStorage.getItem('poswarehouse')) {
        $('#poswarehouse').select2('val', poswarehouse);
    }
    //$(document).on('change', '#posnote', function (e) {
        $('#posnote').redactor('destroy');
        $('#posnote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('posnote', v);
            }
        });
        if (posnote = localStorage.getItem('posnote')) {
            $('#posnote').redactor('set', posnote);
        }

        localStorage.removeItem('prev_poscustomer');
        $(document).on('select2-open', '#poscustomer', function(){
            localStorage.setItem('prev_poscustomer', $(this).val());
        });

        $('#poscustomer').change(function (e) {
            var prev_customer = localStorage.getItem('prev_poscustomer');
            if (prev_customer == $(this).val()) {
                return false;
            } else {
                localStorage.setItem('prev_poscustomer', $(this).val());
            }
            if (prev_customer === null) {
                prev_customer = $(this).val();
            }
            get_customer_branches();
            var current_customer = $(this).val();
            if (localStorage.getItem('positems') != '') {
                positems = localStorage.getItem('positems') ? JSON.parse(localStorage.getItem('positems')) : [];
            } else {
                positems = '';
            }
            if (positems && prev_customer != current_customer && site.settings.prioridad_precios_producto != 1 && site.settings.prioridad_precios_producto != 2) {
                bootbox.confirm({
                    message: "Al cambiar el cliente, los precios se actualizarán de acuerdo con las condiciones del mismo. Que desea hacer ?",
                    buttons: {
                        confirm: {
                            label: 'Cambiar cliente',
                            className: 'btn-success send-submit-sale'
                        },
                        cancel: {
                            label: 'Cancelar',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.each(positems, function(id, arr){
                                wappsiPreciosUnidad(positems[id].row.id,id,positems[id].row.qty);
                            });
                            localStorage.setItem('poscustomer', current_customer);
                            customer_retentions();
                            customer_special_discount();
                        } else {
                            $('#poscustomer').select2('val', prev_customer);
                            customer_retentions();
                        }
                    }
                });
            } else {
                localStorage.setItem('poscustomer', current_customer);
                customer_retentions();
                customer_special_discount();
            }
            validate_fe_customer_data();
            if (pos_settings.show_client_modal_on_select == 1) {
                if (localStorage.getItem('poscustomer_manual_click')) {
                    $('#view-customer').trigger('click');
                    localStorage.removeItem('poscustomer_manual_click');
                }
            }
        }); //ACÁ POSCUSTOMER
    // prevent default action upon enter
    $('body').not('textarea').bind('keypress', function (e) {
        if (e.keyCode == 13) {}
    });

    // Order tax calculation
    if (site.settings.tax2 != 0) {
        $('#postax2').change(function () {
            localStorage.setItem('postax2', $(this).val());
            loadItems();
            return;
        });
    }

    // Order discount calculation
    var old_posdiscount;
    $('#posdiscount').focus(function () {
        old_posdiscount = $(this).val();
    }).change(function () {
        var new_discount = $(this).val() ? $(this).val() : '0';
        if (is_valid_discount(new_discount)) {
            localStorage.removeItem('posdiscount');
            localStorage.setItem('posdiscount', new_discount);
            loadItems();
            return;
        } else {
            $(this).val(old_posdiscount);
            bootbox.alert(lang.unexpected_value);
            return;
        }

    });

    /* ----------------------
     * Delete Row Method
     * ---------------------- */
     var pwacc = false;
     $(document).on('click', '.posdel', function () {
        if (!localStorage.getItem('pos_to_posfe_transformation')) {
            var row = $(this).closest('tr');
            var item_id = row.attr('data-item-id');
            if(protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 2)) {
                $.ajax({
                    url : site.base_url+'billers/get_random_pin_code',
                    data : {
                        biller_id : $('#posbiller').val()
                    },
                    dataType : 'JSON',
                    type : 'get',
                    success : function(data){
                        if (data.pin_code) {
                            var biller_pin_code = data.pin_code;
                            var boxd = bootbox.dialog({
                                title: "<i class='fa fa-key'></i> Código de autorización",
                                message: '<input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
                                buttons: {
                                    success: {
                                        label: "<i class='fa fa-tick'></i> OK",
                                        className: "btn-success verify_pin",
                                        callback: function () {
                                            var pos_pin = md5($('#pos_pin').val());
                                            if(pos_pin == biller_pin_code) {
                                                delete positems[item_id];
                                                row.remove();
                                                if(positems.hasOwnProperty(item_id)) { } else {
                                                    localStorage.setItem('positems', JSON.stringify(positems));
                                                    $('#payment').prop('disabled', true);
                                                    loadItems();
                                                }
                                            } else {
                                                bootbox.alert('Código de autorización incorrecto');
                                            }
                                        }
                                    }
                                }
                            });
                            boxd.on("shown.bs.modal", function() {
                                $( "#pos_pin" ).focus().keypress(function(e) {
                                    if (e.keyCode == 13) {
                                        e.preventDefault();
                                        $('.verify_pin').trigger('click');
                                        return false;
                                    }
                                });
                            });
                        } else {
                            bootbox.alert('Código de autorización incorrecto');
                        }
                    } ,
                    fail : function(data){
                        bootbox.alert('Código de autorización incorrecto');
                    }
                });

            } else {
                if (localStorage.getItem('restobar_mode') == '1' && typeof positems[item_id].state_readiness !== 'undefined') {
                    cancel_ordered_product(item_id);
                } else {
                    delete positems[item_id];
                    row.remove();
                    if(positems.hasOwnProperty(item_id)) { } else {
                        localStorage.setItem('positems', JSON.stringify(positems));
                        $('#payment').prop('disabled', true);
                        console.log('block 6');
                        loadItems();
                    }
                }

            }
            return false;
        }
     });

     $(document).on('change', '#posseller', function() {
        seller = $(this).val();
        $('#seller_id').val(seller);
        loadItems();
     });

    /* -----------------------
     * Edit Row Modal Hanlder
     ----------------------- */

    $(document).on('click', '.edit_gc', function () {
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = positems[item_id];
        $('#product_id_gift_card').val(item_id);
        if (item.gift_card !== undefined) {
            $('#gc_value').val(item.gift_card.gc_value);
            $('#gc_expiry').val(item.gift_card.gc_expiry);
            $('#card_no').val(item.gift_card.gc_card_no);
        } else {
            $('#gc_value').val('');
            $('#gc_expiry').val($('#gc_expiry').data('originaldate'));
            $('#card_no').val('');
        }
        $('#gc_modal').modal('show');
    });
    $(document).on('click', '.edit', function () {
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = positems[item_id];
        var qty = row.children().children('.rquantity').val(),
        product_option = row.children().children('.roption').val(),
        unit_price = formatDecimal(item.row.real_unit_price),
        discount = row.children().children('.rdiscount').val();

        // seccion para actualizar el precio de producto en el modal de editar producto, si el producto esta basado en peso de joyeria
        if (site.settings.handle_jewerly_products == "1") {
            if (item.row.based_on_gram_value == "1" || item.row.based_on_gram_value == '2' ) {
                unit_price = item.row.current_price_gram;
            }
        }
        // seccion para actualizar el precio de producto en el modal de editar producto, si el producto esta basado en peso de joyeria

        if (site.settings.management_weight_in_variants == "1") {
            if (item.row.based_on_gram_value == "1" || item.row.based_on_gram_value == '2' ) {
                unit_price = item.row.current_price_gram;
            }
        }
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                    unit_price = parseFloat(item.row.real_unit_price)+parseFloat(this.price);
                }
            });
        }
        var real_unit_price = item.row.real_unit_price;
        var net_price = unit_price;
        $('#prModalLabel').text(item.row.code + ' - ' + item.row.name);
        $('#pname').val(item.row.name);
        if (session_user_data && session_user_data.group_id != 1 && session_user_data.group_id != 2 && session_user_data.edit_right == 0) {
            $('#pname').attr("readonly", true);
        }
        if (site.settings.tax1) {
            $('#ptax_show').val(item.tax_rate.name);
            $('#ptax').val(item.row.tax_rate);
            $('#ptax').data('taxrate', item.tax_rate.rate);
            $('#ptax2').val(item.row.consumption_sale_tax);
            if (item.row.sale_tax_rate_2_percentage) {
                $('.ptax2_div').removeClass('col-sm-8').addClass('col-sm-4');
                $('#ptax2_percentage').val(item.row.sale_tax_rate_2_percentage);
                $('.ptax2_percentage_div').fadeIn();
            } else {
                $('.ptax2_div').removeClass('col-sm-4').addClass('col-sm-8');
                $('#ptax2_percentage').val('');
                $('.ptax2_percentage_div').fadeOut();
            }
            if (site.settings.allow_change_sale_iva == 0) {
                $('#ptax').select2('readonly', true);
            }
            $('#old_tax').val(item.row.tax_rate);
            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {

                            if (item.row.tax_method == 0) {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_price -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / 100));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }

                        } else if (this.type == 2) {

                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;

                        }
                    }
                });
            }
            var item_discount = 0, ds = discount ? discount : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal(parseFloat(((net_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
            net_price -= item_discount;
        }
        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.options !== false) {
            var o = 1;
            opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if(o == 1) {
                    if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        } else {
            $('.poptions_div').fadeOut();
            product_variant = 0;
        }
        var pref = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.preferences !== null && item.preferences !== undefined && item.preferences !== false) {
            pref = $("<select id=\"ppreferences\" name=\"ppreferences\" class=\"form-control select\" multiple />");
            $.each(item.preferences, function () {
                $("<option />", {value: this.id, text: "("+this.category_name+") "+this.name, "data-pfcatid" : this.preference_category_id}).appendTo(pref);
            });
        } else {
            $('.ppreferences_div').fadeOut();
        }
        if (item.units !== false) {
            uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
            $.each(item.units, function () {
                if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if(this.product_unit_id == item.row.product_unit_id_selected) {
                        $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                    }
                } else {
                    if(this.id == item.row.unit) {
                        $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                    }
                }
            });
            if (Object.keys(item.units).length == 1) {
                $('.punit_div').fadeOut();
            }
        } else {
            uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        }
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        $('#poptions-div').html(opt);
        $('#ppreferences-div').html(pref);
        if (item.preferences_selected !== undefined && item.preferences_selected !== null) {
            $('#ppreferences').val(item.preferences_selected);
        }
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        if(site.settings.handle_jewerly_products && item.row.based_on_gram_value != '0') {
            $('.jewel_grams_div').show(); 
            $('#jewel_grams').val(item.row.jewel_weight).attr('disabled', true);
            if (item.row.based_on_gram_value == '1') { // <- oro nacional
                $('label[for="pprice"]').text(lang.price_gram_national_gold);
            }else if(item.row.based_on_gram_value == '2'){ // <- oro italiano
                $('label[for="pprice"]').text(lang.price_gram_italian_gold);
            }
        }else{
            $('.jewel_grams_div').hide();
            $('label[for="pprice"]').text(lang.unit_price);
        }
        $('#old_qty').val(qty);
        $('#pprice').val(formatDecimals(unit_price));
        $('#pprice').data('prevprice', formatDecimals(unit_price));
        $('#pprice_ipoconsumo').val('');
        $('#punit_price').val(formatDecimal(parseFloat(unit_price)+parseFloat(pr_tax_val)+parseFloat(pr_tax_2_val)));
        $('#poption').select2('val', item.row.option);
        $('#under_cost_authorized').val(item.row.under_cost_authorized !== undefined ? item.row.under_cost_authorized : 0);
        $('#old_price').val(unit_price);
        $('#row_id').val(row_id);
        $('#item_id').val(item_id);
        $('#pserial').val(row.children().children('.rserial').val());
        $('#pdiscount').val(discount);
        $('#ptdiscount').val((formatDecimal(discount * qty)));
        $('#net_price').text(formatMoney(net_price));
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val)+parseFloat(pr_tax_2_val)));
        $('#prModal').appendTo("body").modal('show');
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            units = item.units;
            unit_selected = units[item.row.product_unit_id_selected !== undefined && parseFloat(item.row.product_unit_id_selected) > 0 ? item.row.product_unit_id_selected : item.row.unit];
            unit_selected_real_quantity = 1;
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty / unit_selected.operation_value;
                unit_selected_real_quantity = 1 * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty * unit_selected.operation_value;
                unit_selected_real_quantity = 1 / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            }
            $('#pquantity').val(unit_selected_quantity);
            $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
            $('#pproduct_unit_price').val((unit_price + parseFloat(pr_tax_2_val)) * unit_selected_real_quantity);
            $('#pproduct_unit_price').trigger('change');
            $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
        }
        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
        $('#psubtotal').val('');
    });

    $(document).on('click', '.comment', function () {
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = positems[item_id];
        $('#irow_id').val(row_id);
        $('#icomment').val(item.row.comment);
        $('#iordered').val(item.row.ordered);
        $('#iordered').select2('val', item.row.ordered);
        $('#cmModalLabel').text(item.row.code + ' - ' + item.row.name);
        $('#cmModal').appendTo("body").modal('show');
    });

    $(document).on('click', '#editComment', function () {
        var row = $('#' + $('#irow_id').val());
        var item_id = row.attr('data-item-id');
        positems[item_id].row.order = parseFloat($('#iorders').val()),
        positems[item_id].row.comment = $('#icomment').val() ? $('#icomment').val() : '';
        localStorage.setItem('positems', JSON.stringify(positems));
        $('#cmModal').modal('hide');
        loadItems();
        return;
    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });

    $(document).on('change', '#pprice, #pprice_ipoconsumo, #ptax, #pdiscount, #ptdiscount, #paddprice', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var unit_price = parseFloat($('#pprice').val());
        var pprice_ipoconsumo = parseFloat($('#pprice_ipoconsumo').val());
        var additional_price = parseFloat($('#paddprice').val());
        var pquantity = $('#pquantity').val();
        if (item_id == null) {
            return false;
        }
        var item = positems[item_id];
        $('#under_cost_authorized').val(0);
        if (item.row.sale_tax_rate_2_percentage) {
            ipoconsumo_percentage_calculated =  calculate_second_tax(item.row.sale_tax_rate_2_percentage, unit_price, item.row.tax_rate, item.row.tax_method, true);
            unit_price = unit_price - ipoconsumo_percentage_calculated[1];
            $('#ptax2').val(ipoconsumo_percentage_calculated[1]);
        }
        if (additional_price > 0) {
            $('#paddprice').val(0);
            unit_price += (additional_price / pquantity);
            $('#pprice').val(unit_price);
        }
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
        var dst = parseFloat($('#ptdiscount').val() ? $('#ptdiscount').val() : '0');

        if (parseFloat(dst) > 0) {
            item_discount = formatDecimal(dst / pquantity);
            $('#pdiscount').val(item_discount);
        } else {
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
        }
        unit_price -= item_discount;
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {
                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }
                    } else if (this.type == 2) {
                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;
                    }
                }
            });
        }

        if ((site.settings.disable_product_price_under_cost == 1 || parseFloat($('#pprice').val()) != parseFloat(item.row.price)) && protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 1)) {
            if (parseFloat($('#pprice').val()) < parseFloat(item.row.cost)) {
                $('#editItem').prop('disabled', true);
                $('.error_under_cost').fadeIn();
                $('.div_pprice_authorization_code').fadeIn();
                $('#pprice_authorization_code').val('');
                $('#pprice_authorization_code').focus();
            } else if (parseFloat($('#pprice').val()) != parseFloat(item.row.price)) {
                $('#editItem').prop('disabled', true);
                $('.div_pprice_authorization_code').fadeIn();
                $('#pprice_authorization_code').val('');
                $('#pprice_authorization_code').focus();
                $('.error_under_cost').fadeOut();
            } else {
                $('#editItem').prop('disabled', false);
                $('.error_under_cost').fadeOut();
                $('.div_pprice_authorization_code').fadeOut();
            }
        } else {
            if (site.settings.disable_product_price_under_cost == 1 && parseFloat(item.row.cost) > parseFloat($('#pprice').val())) {
                $('#editItem').prop('disabled', true);
                $('.error_under_cost').fadeIn();
            } else {
                $('#editItem').prop('disabled', false);
                $('.error_under_cost').fadeOut();
                $('.div_pprice_authorization_code').fadeOut();
            }
        }

        if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(unit_price));
        } else if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(pprice_ipoconsumo));
        } else if (parseFloat($('#pprice_ipoconsumo').val()) <= parseFloat(item.row.consumption_sale_tax)) {
            Command: toastr.error('Por favor ingrese un precio válido', 'Valores incorrectos', {onHidden : function(){}})
            $('#pprice_ipoconsumo').val(0);
        }
        $('#net_price').text(formatMoney(unit_price));
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
    });

    $(document).on('click', '#check_pprice_autoriz', function(){
        validation_under_cost();
    });
    $(document).on('keyup', '#pprice_authorization_code', function(e){
        if (e.keyCode == 13) {
            validation_under_cost();
        }
    });

    function validation_under_cost(){
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        pprice_pos_pin = md5($('#pprice_authorization_code').val());
        $.ajax({
            url : site.base_url+'billers/get_random_pin_code',
            data : {
                biller_id : $('#posbiller').val()
            },
            dataType : 'JSON',
            type : 'get',
            success : function(data){
                if (data.pin_code) {
                    if (pprice_pos_pin == data.pin_code) {
                        $('#editItem').prop('disabled', false);
                        $('.error_under_cost').fadeOut();
                        $('.div_pprice_authorization_code').fadeOut();
                        $('#under_cost_authorized').val(1);
                    }
                } else {
                    return false;
                }
            } ,
            fail : function(data){
                return false;
            }
        });
    }

    $(document).on('change', '#punit', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }

        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 0 && site.settings.precios_por_unidad_presentacion == 2) {
                console.log(unit_selected);
                valor = unit_selected.valor_unitario;
                var ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(unit_selected.valor_unitario) / parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) * parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(unit_selected.valor_unitario) * parseFloat(unit_selected.operation_value);
                    ipoconsumo_um =  parseFloat(pr_tax_2_val) / parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
                $('#pproduct_unit_price').val(parseFloat(unit_selected.valor_unitario) + parseFloat(ipoconsumo_um));
                $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
            } else {
                $('#pprice').val(unit_selected.valor_unitario);
                $('#pproduct_unit_price').val(unit_selected.valor_unitario);
            }
            $('#pprice').change();
        } else if(site.settings.prioridad_precios_producto == 11){

        } else {
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                        aprice = parseFloat(this.price);
                    }
                });
            }
            if(item.units && unit != positems[item_id].row.base_unit) {
                $.each(item.units, function(){
                    if (this.id == unit) {
                        base_quantity = unitToBaseQty($('#pquantity').val(), this);
                        $('#pprice').val(formatDecimal(((parseFloat(item.row.base_unit_price+aprice))*unitToBaseQty(1, this)))).change();
                    }
                });
            } else {
                $('#pprice').val(formatDecimal(item.row.base_unit_price+aprice)).change();
            }
        }
        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
    });

    $(document).on('change', '#pproduct_unit_price', function(){
        p_unit_price = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 0) {
                valor = p_unit_price;
                ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  (parseFloat(p_unit_price)) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  (parseFloat(p_unit_price)) * parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
            } else {
                $('#pprice').val(p_unit_price);
            }
            $('#pprice').change();
        }

    });

    $(document).on('click', '#calculate_unit_price', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        tax_rate = formatDecimal($('#ptax').data('taxrate')) / 100;
        tax_rate = tax_rate != 0 ? tax_rate + 1 : 1;
        var subtotal = parseFloat($('#psubtotal').val()),
        qty = parseFloat($('#pquantity').val());
        if (item.row.tax_method == 1) { tax_rate = 1; }
        $('#pprice').val(formatDecimal(((subtotal) * tax_rate))).change();
        return false;
    });

    /* -----------------------
     * Edit Row Method
     ----------------------- */
    $(document).on('click', '#editItem', function () {
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_2 = $('#ptax2').val(), new_pr_tax_rate = false, new_pr_tax_2_rate = false;
        if (new_pr_tax) {
            $.each(tax_rates, function () {
                if (this.id == new_pr_tax) {
                    new_pr_tax_rate = this;
                }
            });
        }
        var price = parseFloat($('#pprice').val());
        if(item.options !== false) {
            var opt = $('#poption').val();
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    price = price-parseFloat(this.price);
                }
            });
        }
        if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
            if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > price) {
                bootbox.alert(lang.unexpected_value);
                return false;
            }
        }
        if (!is_numeric($('#pquantity').val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var unit = $('#punit').val();
        var base_quantity = parseFloat($('#pquantity').val());
        if(unit != positems[item_id].row.base_unit) {
            $.each(positems[item_id].units, function(){
                if (this.id == unit) {
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        tpax2 = parseFloat($('#ptax2').val() ? $('#ptax2').val() : 0);

        // this section is for products with gram in calculate price
        if (site.settings.handle_jewerly_products == "1") {   
            if (positems[item_id].row.based_on_gram_value == "1" || positems[item_id].row.based_on_gram_value == "2" ) {
                positems[item_id].row.current_price_gram = (price-parseFloat(tpax2)) / trmrate;
            }
        }
        // this section is for products with gram in calculate price

        // this section is for varian with gram in calculate price
        if (site.settings.management_weight_in_variants == "1") {
            if (positems[item_id].row.based_on_gram_value == "1" || positems[item_id].row.based_on_gram_value == "2" ) {
                positems[item_id].row.current_price_gram = (price-parseFloat(tpax2)) / trmrate;
                price = 0;
            }
        }
        console.log(" price >> "+price);
        positems[item_id].row.fup = 1,
        positems[item_id].row.qty = parseFloat($('#pquantity').val()),
        positems[item_id].row.base_quantity = parseFloat(base_quantity),
        positems[item_id].row.under_cost_authorized = $('#under_cost_authorized').val(),
        positems[item_id].row.price_before_promo = ($('#under_cost_authorized').val() == 1 ? $('#pprice').data('prevprice') : 0),
        positems[item_id].row.consumption_sale_tax = tpax2,
        positems[item_id].row.real_unit_price = (price-parseFloat(tpax2)) / trmrate,
        positems[item_id].row.unit_price = (price-parseFloat(tpax2)) / trmrate,
        positems[item_id].row.unit = unit,
        positems[item_id].row.tax_rate = new_pr_tax,
        positems[item_id].tax_rate = new_pr_tax_rate,
        positems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '',
        positems[item_id].row.birthday_discount = 0,
        positems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '',
        positems[item_id].row.serial = $('#pserial').val();
        positems[item_id].row.name = $('#pname').val();
        prev_product_unit_id = positems[item_id].row.product_unit_id;
        positems[item_id].row.product_unit_id = $('#punit').val();
        positems[item_id].row.product_unit_id_selected = $('#punit').val();
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            var ditem = positems[item_id];
            unit_selected = ditem.units[unit];
            qty = parseFloat($('#pquantity').val());
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            }
            positems[item_id].row.qty = unit_selected_quantity;
        } else {
            positems[item_id].row.unit_price_id = $('#punit').val();
        }
        if (site.settings.prioridad_precios_producto == 11 && prev_product_unit_id !== undefined && prev_product_unit_id != positems[item_id].row.product_unit_id) {
            wappsiPreciosUnidad(positems[item_id].row.id,item_id,$('#pquantity').val(), true);
        }
        var preferences_text = '';
        var preferences_selected = [];
        var parr = [];
        $('#ppreferences option:selected').each(function(index, option){
            if (preferences_selected[$(option).data('pfcatid')] === undefined) {
                preferences_selected[$(option).data('pfcatid')] = [];
            }
            preferences_selected[$(option).data('pfcatid')].push(parseInt($(option).val()));
            psplit = $(option).text().split(')');
            psplit_index = psplit[0].replace("(", "");
            if (parr[psplit_index] === undefined) {
                parr[psplit_index] = [];
            }
            parr[psplit_index].push(psplit[1]);
        });
        if ($('#ppreferences option:selected').length > 0) {
            phtml = "";
            for (let preference_category in parr) {
                phtml += "<b>"+preference_category+" : </b>";
                for (let index in parr[preference_category]) {
                    phtml += parr[preference_category][index]+", ";
                }
            }
            positems[item_id].preferences_text = phtml;
            positems[item_id].preferences_selected = preferences_selected;
        }
        localStorage.setItem('positems', JSON.stringify(positems));
        $('#prModal').modal('hide');
        loadItems();
        return;
    });

    /* -----------------------
     * Product option change
     ----------------------- */
    $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = positems[item_id];
        var unit = $('#punit').val(), base_quantity = parseFloat($('#pquantity').val()), base_unit_price = item.row.base_unit_price;
        if(unit != positems[item_id].row.base_unit) {
            $.each(positems[item_id].units, function(){
                if (this.id == unit) {
                    base_unit_price = formatDecimal((parseFloat(item.row.base_unit_price)*(unitToBaseQty(1, this))))
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        $('#pprice').val(parseFloat(base_unit_price)).trigger('change');
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    $('#pprice').val(parseFloat(base_unit_price)+(parseFloat(this.price))).trigger('change');
                }
            });
        }
    });

    /* ------------------------------
    * Sell Gift Card modal
    ------------------------------- */
    $(document).on('click', '#sellGiftCard', function (e) {
        if (site.settings.gift_card_product_id != null) {
            $('#modal-loading').show();
            wh = $('#poswarehouse').val();
            bl = $('#posbiller').val();
            cu = $('#poscustomer').val();
            cub = $('#poscustomer_branch').val();
            $.ajax({
                type: "get",
                url: site.base_url+"pos/getProductDataByCode",
                data: {id : site.settings.gift_card_product_id, warehouse_id: wh, customer_id: cu, biller_id : bl, address_id : cub},
                dataType: "json",
                success: function (data) {
                    e.preventDefault();
                    if (data !== null) {
                        data.is_new = true;

                        var item_id = site.settings.item_addition == 1 ? data.item_id : data.id;
                        $('#gc_value').val('');
                        $('#card_no').val('');
                        $('#product_id_gift_card').val(item_id);
                        $('#gc_modal').modal('show');
                        var row = add_invoice_item(data);
                        $('#gc_expiry').val($('#gc_expiry').data('originaldate'));
                        if (row)
                            $(this).val('');

                    }
                }
            });
        } else {
            command: toastr.error('Aún no se ha definido el producto para las tarjetas de regalo.', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "50000",
                "extendedTimeOut": "1000",
            });
        }

    });

     $('#gccustomer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url+"customers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if(data.results != null) {
                    return { results: data.results };
                } else {
                    return { results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
     });

     $('#genNo').click(function(){
        var no = generateCardNo();
        $(this).parent().parent('.input-group').children('input').val(no);
        return false;
     });
     $('.date').datetimepicker({format: site.dateFormats.js_sdate, fontAwesome: true, language: 'sma', todayBtn: 1, autoclose: 1, minView: 2 });

    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
     $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            positems = {};
            if ($('#poswarehouse').val() && $('#poscustomer').val()) {
                $('#poscustomer').select2("readonly", true);
                $('#poswarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });

    $(document).on('click', '#addItemManually', function (e) {
        var mid = (new Date).getTime(),
        mcode = $('#mcode').val(),
        mname = $('#mname').val(),
        mtax = parseInt($('#mtax').val()),
        mqty = parseFloat($('#mquantity').val()),
        mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
        unit_price = parseFloat($('#mprice').val()),
        mtax_rate = {};
        if (mcode && mname && mqty && unit_price) {
            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });

            positems[mid] = {
                "id": mid,
                "item_id": mid,
                "label": mname + ' (' + mcode + ')',
                "row": {
                    "id": mid,
                    "code": mcode,
                    "name": mname,
                    "quantity": mqty,
                    "base_quantity": mqty,
                    "price": unit_price,
                    "unit_price": unit_price,
                    "real_unit_price": unit_price,
                    "tax_rate": mtax,
                    "tax_method": 0,
                    "qty": mqty,
                    "type": "manual",
                    "discount": mdiscount,
                    "serial": "",
                    "option":""
                },
                "tax_rate": mtax_rate,
                'units': false,
                "options":false
            };

            localStorage.setItem('positems', JSON.stringify(positems));
            $('#payment').prop('disabled', true);
            console.log('block 8');
            loadItems();
        }
        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mprice').val('');
        return false;
    });

    $(document).on('change', '#mprice, #mtax, #mdiscount', function () {
        var unit_price = parseFloat($('#mprice').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#mnet_price').text(formatMoney(unit_price));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
    --------------------------- */
    var old_row_qty;
    var controladorTiempo = "";

    $(document).on("focus", '.rquantity', function () {
      old_row_qty = $(this).val();
    }).on('keydown', '.rquantity', function(e){
        var elemento = this;
        if(document.all)
          tecla=event.keyCode;
        else
        {
          tecla=e.which;
        }
        if (tecla == 13) {
            localStorage.setItem('enter_pressed_for_quantity', true);
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
            clearTimeout(controladorTiempo);
            load_new_quantity(elemento);
            e.preventDefault();
            return false;
        } else {
          clearTimeout(controladorTiempo);
          clearTimeout(time_for_unlock);
          $('#payment').prop('disabled', true);
          controladorTiempo = setTimeout(function(){
            load_new_quantity(elemento);
            return false;
          }, (parseFloat(pos_settings.time_focus_quantity) * 1000));
        }
    });

    $(document).on('change', '.rquantity', function(){
        var elemento = this;
        if ($(window).width() <= 800) {
            localStorage.setItem('enter_pressed_for_quantity', true);
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
            clearTimeout(controladorTiempo);
            load_new_quantity(elemento);
            e.preventDefault();
            return false;
        }
    });

    $("#paid_by_1").select2({
        formatResult: payment_method_icons,
        formatSelection: payment_method_icons,
        escapeMarkup: function (m) {
          return m;
        }
    });
    setTimeout(function() {
        loadItems();
        if (site.settings.set_focus == 0 && site.pos_settings.balance_settings == 1) {

            navigator.permissions.query({ name: 'clipboard-read' })
              .then(permissionStatus => {
                if (permissionStatus.state == 'granted' || permissionStatus.state == 'prompt') {
                  navigator.clipboard.readText()
                    .then(text => {
                      console.log('Text from clipboard:', text);
                    })
                    .catch(err => {
                      console.log('Error reading from clipboard:', err);
                      // location.reload();
                    });
                } else {
                    bootbox.alert('El permiso para acceder al portapapeles fue denegado. Éste es necesario para el correcto funcionamiento, porfavor contactar a soporte');
                }
              })
              .catch(err => {
                console.log('Error requesting clipboard permission:', err);
              });

        }


    }, 1200);
    localStorage.removeItem('poscustomer_manual_click');
    if (is_quote) {
        setTimeout(function() {
            $('#posbiller').trigger('change');
        }, 1200);
    }

    $(document).on('click', '#table_change', function(e) { e.preventDefault(); table_change(); });

}); //FIN DOCUMENT READY


    $(document).on('keypress', '#reference_note', function(e){
        if (e.keyCode == 13 && !$('#suspend_sale').prop('disabled')) {
            $('#suspend_sale').click();
            $('#suspend_sale').prop('disabled', true);
        }
    });

    function load_new_quantity(elemento){
        var row = $(elemento).closest('tr');
        if (!is_numeric($(elemento).val()) || parseFloat($(elemento).val()) < 0) {
          $(elemento).val(1);
          bootbox.alert(lang.unexpected_value);
          return;
        }

        var new_qty = formatDecimal($(elemento).val()),
        item_id = row.attr('data-item-id');

        var expresion=/^\d*(\.\d{1})?\d{0,1}$/;
        if (expresion.test(new_qty) !== true) {
            new_qty = formatQuantity2(new_qty);
            $(elemento).val(new_qty);
        }

        positems[item_id].row.base_quantity = new_qty;
        if(positems[item_id].row.unit != positems[item_id].row.base_unit) {
          $.each(positems[item_id].units, function(){
            if (elemento.id == positems[item_id].row.unit) {
              // positems[item_id].row.base_quantity = unitToBaseQty(new_qty, elemento);
            }
          });
        }
        // console.log(" >> item_id load_new_quantity "+item_id);
        wappsiPreciosUnidad(positems[item_id].row.id,item_id,new_qty, true);
        if (site.settings.set_focus == 0 && site.pos_settings.balance_settings == 1) {
            navigator.clipboard.writeText(new_qty).then(() => {
                console.log("Text copied to clipboard");
              }).catch(error => {
                console.log("Error copying text to clipboard:", error);
              });
        }
        if (site.settings.set_focus != 1) {
            $('#add_item').focus();
        }
    }

// Wappsi Function
function wappsiPreciosUnidad(id,item_id,new_qty, load_items = false, new_item = false) {
    $('#payment').prop('disabled', true);
    positems = localStorage.getItem('positems') ? JSON.parse(localStorage.getItem('positems')) : [];
    var pos_keep_prices = localStorage.getItem('pos_keep_prices');
    if (new_item == false && (pos_keep_prices == 'true' || positems[item_id].row.fup == true)) {
        positems[item_id].row.qty = new_qty;
        localStorage.setItem('price_updated', true);
        localStorage.setItem('positems', JSON.stringify(positems));
        loadItems();
        return false;
    } else {
        var customer = $('#poscustomer').val();
        var biller_price_group = $('#posbiller option:selected').data('pricegroupdefault');
        var customer = $('#poscustomer').val();
        var address_id = $('#poscustomer_branch').val();
        var biller = $('#posbiller').val();
        var unit_price_id = null;
        if (site.settings.precios_por_unidad_presentacion == 2 || site.settings.prioridad_precios_producto == 11 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 7) {
            if (item_selected = positems[item_id]) {
                unit_price_id = item_selected.row.product_unit_id_selected;
            }
        }
        var datos = {
                        id : id,
                        new_qty : new_qty,
                        price_group : biller_price_group,
                        customer : customer,
                        address_id : address_id,
                        biller : biller,
                        prioridad : site.settings.prioridad_precios_producto,
                        unit_price_id : unit_price_id
                    };
        $.ajax({
            type: 'get',
            url: site.base_url+"sales/preciosUnidad",
            dataType: "json",
            data: datos,
            success: function (data) {
                $(this).removeClass('ui-autocomplete-loading');
                if (data !== null && data[0] !== null && data[0] != 0) {
                    var valor = data[0];
                    if (positems[item_id]) {
                        if (data[5] !== undefined && positems[item_id].row.price_before_promo === undefined) {
                            positems[item_id].row.price_before_promo = data[5];
                        }
                        positems[item_id].row.real_unit_price = valor;
                        if (is_suspend_sale) {
                            positems[item_id].row.unit_price = valor;
                        }
                        if (site.settings.precios_por_unidad_presentacion == 2) {
                            positems[item_id].row.unit_price_quantity = data[1];
                            if (data[1] !== undefined) {
                                positems[item_id].row.real_unit_price = valor / data[1];
                            }
                        }
                        if (data[2] !== undefined) {
                            positems[item_id].row.discount = data[2];
                        }
                        if (data[3] !== undefined) {
                            positems[item_id].row.tax_rate = data[3];
                            positems[item_id].tax_rate = data[4];
                            positems[item_id].row.tax_exempt_customer = true;
                        }
                    }
                } else {
                    positems[item_id].row.real_unit_price = positems[item_id].row.real_unit_price;
                    if (is_suspend_sale) {
                        positems[item_id].row.unit_price = positems[item_id].row.real_unit_price;
                    }
                    if (data[3] !== undefined) {
                        positems[item_id].row.tax_rate = data[3];
                        positems[item_id].tax_rate = data[4];
                        positems[item_id].row.tax_exempt_customer = true;
                    }
                }
                positems[item_id].row.qty = new_qty;
                // console.log(positems);
                localStorage.setItem('positems', JSON.stringify(positems));
                $('#payment').prop('disabled', true);
                updated_items++;
                if (updated_items == Object.keys(positems).length) {
                    updated_items = 0;
                    loadItems();
                    if (prices_checked == false) {
                        check_prices();
                        prices_checked = true;
                    }
                } else if (load_items == true) {
                    updated_items = 0;
                    loadItems();
                }
            }
        });
    }
}

/* -----------------------
 * Load all items
 aca loaditems
 ----------------------- */

function check_prices(){
    positems = localStorage.getItem('positems') ? JSON.parse(localStorage.getItem('positems')) : [];
    sortedItems = positems;
    var pos_keep_prices = localStorage.getItem('pos_keep_prices');
    if (!pos_keep_prices && (is_quote || is_suspend_sale || is_duplicate)) {
        var prices_changed = false;
        var prices_changed_html = '';
        $.each(sortedItems, function () {;
            var item = this;
            var actual_price = item.row.real_unit_price;
            var item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var key_log = localStorage.getItem('poskeylog');
            var poscustomerspecialdiscount = localStorage.getItem('poscustomerspecialdiscount');
            if (key_log == 1 && poscustomerspecialdiscount == 1) {
                item_price = item.row.real_unit_price;
                if (item_tax_method == 1) {
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 0))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                } else {
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                        pr_tax_val = ptax[0];
                        pr_tax_rate = ptax[1];
                    }
                    item_price -= pr_tax_val;
                }
                actual_price = item_price;
            }
            if (actual_price != parseFloat(item.row.prev_unit_price)) {
                prices_changed = true;
                prices_changed_html += '<tr>'+
                                            '<td>'+item_name+'</td>'+
                                            '<td class="text-right">'+formatMoney(item.row.prev_unit_price)+'</td>'+
                                            '<td class="text-right">'+formatMoney(actual_price)+'</td>'+
                                       '</tr>';
            }
        });
        if (prices_changed) {
            $('#prices_changed_table').html(prices_changed_html);
            $('#pricesChanged').modal('show');
            return false;
        }
    }
}
var is_sale_gc = false;
var product_total_qty = [];
function loadItems() {
    if (localStorage.getItem('positems')) {
        $('#birthday_year_applied').val('');
        total = 0;
        total_after_tax = 0;
        count = 1;
        an = 1;
        product_tax = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;
        order_data = {};
        bill_data = {};
        total_tax = 0;
        tip_amount = 0;
        var key_log = localStorage.getItem('poskeylog') > 0 ? localStorage.getItem('poskeylog') : 1;
        var poscustomerspecialdiscount = localStorage.getItem('poscustomerspecialdiscount');
        var restobar_mode_module = JSON.parse(localStorage.getItem('restobar_mode_module'));
        var pos_keep_prices = JSON.parse(localStorage.getItem('pos_keep_prices'));
        var price_updated = JSON.parse(localStorage.getItem('price_updated'));
        var actual_price = false;
        product_total_qty = [];
        if (pos_keep_prices && price_updated) {
            actual_price = true;
        }
        $("#posTable tbody").empty();
        var time = ((new Date).getTime())/1000;
        if (pos_settings.remote_printing != 1) {
            store_name = (biller && biller.company != '-' ? biller.company : biller.name);
            order_data.store_name = store_name;
            bill_data.store_name = store_name;
            order_data.header = "\n"+lang.order+"\n\n";
            bill_data.header = "\n"+lang.bill+"\n\n";
            var pos_customer = 'C: '+$('#select2-chosen-1').text()+ "\n";
            var hr = 'R: '+$('#reference_note').val()+ "\n";
            var user = 'U: '+username+ "\n";
            var pos_curr_time = 'T: '+date(site.dateFormats.php_ldate, time)+ "\n";
            var ob_info = pos_customer+hr+user+pos_curr_time+ "\n";
            order_data.info = ob_info;
            bill_data.info = ob_info;
            var o_items = '';
            var b_items = '';
        } else {
            $("#order_span").empty(); $("#bill_span").empty();
            var styles = '<style>table, th, td { border-collapse:collapse; border-bottom: 1px solid #CCC; } .no-border { border: 0; } .bold { font-weight: bold; }</style>';
            var pos_head1 = '<span style="text-align:center;"><h3>'+site.settings.site_name+'</h3><h4>';
            var pos_head2 = '</h4><p class="text-left">C: '+$('#select2-chosen-1').text()+'<br>R: '+$('#reference_note').val()+'<br>U: '+username+'<br>T: '+date(site.dateFormats.php_ldate, time)+'</p></span>';
            $("#order_span").prepend(styles + pos_head1+' '+lang.order+' '+pos_head2);
            $("#bill_span").prepend(styles + pos_head1+' '+lang.bill+' '+pos_head2);
            $("#order-table").empty(); $("#bill-table").empty();
        }
        positems = localStorage.getItem('positems') ? JSON.parse(localStorage.getItem('positems')) : [];
        if (pos_settings.item_order == 1) {
            sortedItems = _.sortBy(positems, function(o) { return [parseInt(o.category.id), parseInt(o.order)]; });
        } else {
            sortedItems = _.sortBy(positems, function(o) { return [parseInt(o.order)]; });
            if (site.settings.product_order == 2) {
                sortedItems.reverse();
            }
        }
        var category = 0, print_cate = false;
        var lock_submit = false;
        var lock_submit_by_margin = false;
        var except_category_taxes = false;
        var tax_exempt_customer = false;
        var diferent_tax_alert = false;
        $.each(sortedItems, function () {
            var item = this;
            var item_id = (site.settings.item_addition == 1 ? item.item_id : item.id) + (item.row.option && item.row.option > 0 ? item.row.option.toString() : '');
            var gc_incomplete_data = false;
            if (site.settings.gift_card_product_id == item.row.id) {
                is_sale_gc = true;
                if (item.gift_card === undefined || item.gift_card.gc_card_no == null) {
                    gc_incomplete_data = true;
                    lock_submit = true;
                }
            }
            positems[item_id] = item;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id;
            if (site.settings.overselling == 0) {
                if (product_total_qty[product_id+'A'] === undefined) {
                    product_total_qty[product_id+'A'] = item.row.qty;
                } else {
                    product_total_qty[product_id+'A'] += item.row.qty;
                }
            }
            var item_type = item.row.type;
            var combo_items = item.combo_items;
            var item_price = item.row.price;
            var item_qty = item.row.qty;
            var item_aqty = item.row.quantity;
            var item_tax_method = item.row.tax_method;
            if (its_customer_birthday && item.row.birthday_discount) {
                item.row.discount = item.row.birthday_discount;
                $('#birthday_year_applied').val(birthday_year_applied);
            }
            var item_ds = item.row.discount;
            var item_discount = 0;
            var item_option = item.row.option;
            var item_code = item.row.code;
            var item_serial = item.row.serial;
            var item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var item_state_readiness = (item.state_readiness === undefined ? '': item.state_readiness);
            if (item.prev_state_readiness === undefined) {
                item.prev_state_readiness = item.state_readiness;
            }
            var item_state_readiness_prev = (item.state_readiness === undefined ? '': item.state_readiness);
            var product_unit = item.row.product_unit_id_selected;
            var base_quantity = item.row.base_quantity;
            var unit_price = actual_price ? item.row.unit_price : item.row.real_unit_price;
            var item_comment = item.row.comment ? item.row.comment : '';
            var item_ordered = item.row.ordered ? item.row.ordered : 0;
            var ordered_product_id = item.ordered_product_id;
            if (site.settings.prioridad_precios_producto == 7 && item.row.product_unit_id_selected !== undefined && item.units[item.row.product_unit_id_selected] !== undefined && site.settings.validate_um_min_factor_qty == 1) {
                if ($.inArray(site.settings.prioridad_precios_producto, [7, 10, 11])) {
                    unit_selected = item.units[item.row.product_unit_id_selected];
                    if (formatDecimal(item_qty) < formatDecimal(unit_selected.operation_value)) {
                        item_qty = formatDecimal(unit_selected.operation_value);
                        command: toastr.warning('El valor ingresado está por debajo del mínimo de la unidad escogida.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "50000",
                            "extendedTimeOut": "1000",
                        });
                    }
                }
            }
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                        item_price = parseFloat(unit_price)+(parseFloat(this.price));
                        unit_price = item_price;
                    }
                });
            }
            if (item.row.except_category_taxes) {
                except_category_taxes = true;
            }
            if (item.row.tax_exempt_customer && site.settings.enable_customer_tax_exemption == 1) {
                tax_exempt_customer = true;
            }
            if (item.row.diferent_tax_alert) {
                diferent_tax_alert = true;
            }
            pr_t = {id : site.except_tax_rate_id, rate : 0};
            incorrect_tax_rate = false;
            if (item.tax_rate) {
                var pr_tax = item.tax_rate;
            } else {
                var pr_tax = pr_t;
                incorrect_tax_rate = true;
            }
            var pr_tax_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_price, item_tax_method))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }
            item_price = item_tax_method == 1 ? unit_price : unit_price - (pr_tax_val);

            var ds = item_ds ? item_ds : '0';
            item_discount = calculateDiscount(ds, item_price, pr_tax, item_tax_method);
            item_price -= item_discount; //precio sin IVA - Descuento
            if (item_discount > 0) {
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 1))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
            }
            if (pr_tax.type == 2 && item_price < 0) {
                item_price = 0;
            }
            unit_price = formatDecimal((unit_price+item_discount));
            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.name;
                }
            });
            if (key_log == 1 && poscustomerspecialdiscount == 1) {
                item_price = item.row.real_unit_price;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, (item_tax_method == 1 ? 0 : item_tax_method)))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                item_price -= pr_tax_val;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method == 0) {
                    item_price -= pr_tax_val;
                }
            } else if (key_log == 2) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (item_tax_method == 1) {
                    var pr_tax_val = 0;
                    if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, 1))) {
                        pr_tax_val = ptax[0];
                    }
                    item_price = parseFloat(formatDecimal(item_price)) + parseFloat(formatDecimal(pr_tax_val));
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            } else if (key_log == 3) {
                item_price = item.row.real_unit_price;
                var ds = item_ds ? item_ds : '0';
                item_discount = calculateDiscount(ds, item_price);
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method != 1) {
                    item_price -= pr_tax_val;
                }
                pr_tax_rate = 0;
                pr_tax_val = 0;
                pr_tax_2_rate = 0;
                product_tax = 0;
                product_tax_2 = 0;
                unit_price = item.row.real_unit_price;
            }

            // Sección para manejo de joyaeria con peso en productos sin variantes
            if (site.settings.handle_jewerly_products == "1") {
                if (item.row.based_on_gram_value == "1" || item.row.based_on_gram_value == "2") { // 1 valor_nacional, 2 valor_italiano
                    _jewel_weight =  parseFloat((item.row.jewel_weight !== undefined) ? item.row.jewel_weight : 0);
                    _current_price = parseFloat((item.row.current_price_gram !== undefined) ? item.row.current_price_gram : 0);
                    item_price = _jewel_weight * _current_price;
                }
            }
            // Sección para manejo de joyaeria con peso en productos sin variantes

            // this section is for varian with gram in calculate price
            if (site.settings.management_weight_in_variants == "1") {
                if (item.row.based_on_gram_value == "1" || item.row.based_on_gram_value == "2") { // 1 valor_nacional, 2 valor_italiano
                    _variant_weight = parseFloat((item.row.variant_weight !== undefined) ? item.row.variant_weight : 0);
                    _current_price = parseFloat((item.row.current_price_gram !== undefined) ? item.row.current_price_gram : 0);
                    item_price += (_variant_weight * _current_price);
                }
            }

            pr_tax_2_val = item.row.consumption_sale_tax > 0 ? item.row.consumption_sale_tax : 0;
            pr_tax_2_rate = item.row.sale_tax_rate_2_percentage ? item.row.sale_tax_rate_2_percentage : pr_tax_2_val;
            pr_tax_2_rate_id = item.row.sale_tax_rate_2_id;
            if (pos_settings.item_order == 1 && category != item.row.category_id) {
                category = item.row.category_id;
                print_cate = true;
                var newTh = $('<tr></tr>');
                newTh.html('<td colspan="100%"><strong>'+item.row.category_name+'</strong></td>');
                newTh.appendTo("#posTable");
            } else {
                print_cate = false;
            }
            cancelled_class = ''; //SE DEFINE VARIABLE POR ERROR DE UNDEFINED
            if (localStorage.getItem('restobar_mode') == '1') {
                cancelled_class = (item.state_readiness == 3) ? 'line-through' : (item.state_readiness == 2) ? 'success': '';
            }
            var msg_error = false;
            /* profitability_margin */
            $('.margin_alert').fadeOut();
            if (site.settings.alert_zero_cost_sale == 1 && formatDecimal(item.row.cost) == 0) {
                command: toastr.warning('El producto '+item_name+' tiene costo en 0.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "50000",
                    "extendedTimeOut": "1000",
                });
            }
            if (site.settings.profitability_margin_validation > 0) {
                var margin_cost = site.settings.which_cost_margin_validation == 1 ? item.row.cost : item.row.avg_cost; // ultmo costo o costro promedio
                var margin_price = parseFloat(item_price) + parseFloat(pr_tax_val);
                var tax_method = item_tax_method; // tax_method => 0 es impuesto incluido
                var margin_tax_rate = item.tax_rate.id; // margin_tax_rate => id del impuesto tabla tax_rates
                var margin = 0;
                let productNameE= item.row.name;
                if (site.settings.profitability_margin_validation == 3) { // cuando se valida listas minimas
                    if (item.row.based_on_gram_value == '0' || item.row.based_on_gram_value == null) { // cuando no esta basado en peso
                        if (item.row.variant_weight_selected) { // cuando no esta basado en peso pero tiene variantes, se validara el campo minimun_price
                            if (margin_price < item.row.variant_minimun_price_selected ) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }else { // cuando no esta basado en gramo y no tiene variantes se valida contra la lista marcada como minima
                            let price_minimum = validate_price_minimun(item.row.id);
                            if (!price_minimum) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor mínimo no definido para el producto ${productNameE}` );
                            }else if(price_minimum > margin_price){
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }
                    }
                    else if(item.row.based_on_gram_value == '1'){ // cuando esta basado en oro nacional
                        if (item.row.variant_weight_selected ) {
                            if (margin_price < parseFloat(item.row.variant_weight_selected * site.pos_settings.minimun_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }else{
                            if (margin_price < parseFloat(item.row.jewel_weight * site.pos_settings.minimun_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo para el producto ${productNameE}` );
                            }
                        }
                    }
                    else if(item.row.based_on_gram_value == '2'){ // cuando esta basado en oro italiano
                        if (item.row.variant_weight_selected) {
                            if (margin_price < parseFloat(item.row.variant_weight_selected * site.pos_settings.minimun_italian_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }else{
                            if (margin_price < parseFloat(item.row.jewel_weight * site.pos_settings.minimun_italian_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo para el producto ${productNameE}` );
                            }
                        }
                    }
                }else{
                    if (margin_cost > 0 && margin_price > 0 && margin_tax_rate > 0 && tax_method == 0) {
                        var margin_pr_tax = tax_rates[margin_tax_rate];
                        margin_pr_tax_val = 0;
                        if (ptax = calculateTax(margin_pr_tax, margin_price, tax_method)) {
                            margin_pr_tax_val = ptax[0];
                        }
                        var margin_net_price = margin_price - margin_pr_tax_val;
                        if (ptax = calculateTax(margin_pr_tax, margin_cost, tax_method)) {
                            margin_pr_tax_val = ptax[0];
                        }
                        var margin_net_cost = margin_cost - margin_pr_tax_val;
                    } else {
                        var margin_net_price = margin_price;
                        var margin_net_cost = margin_cost;
                    }
                    if (margin_net_price > 0 && margin_net_cost > 0) {
                        if (site.settings.profitability_margin_validation == 1 && item.category !== undefined) {
                            setted_margin = item.category.profitability_margin;
                        } else if(site.settings.profitability_margin_validation == 2 && item.subcategory !== undefined) {
                            setted_margin = item.subcategory.profitability_margin;
                        }
                        if (setted_margin == 0) {
                            if (item.subcategory !== undefined && item.subcategory.profitability_margin > 0) {
                                setted_margin = item.subcategory.profitability_margin;
                            } else if (item.category !== undefined && item.category.profitability_margin > 0) {
                                setted_margin = item.category.profitability_margin;
                            }
                        }
                        if (setted_margin > 0) {
                            margin = formatDecimals((((margin_net_price - margin_net_cost) / margin_net_price) * 100), 2);
                            if (parseFloat(margin) <= parseFloat(setted_margin)) {
                                lock_submit_by_margin = true;
                                msg_error = 'El margen mínimo establecido para el producto es de '+formatDecimals(setted_margin)+'%, margen actual '+margin+'%;';
                                $('.margin_alert').fadeIn();
                            }
                        }
                    }
                }
            }
            /* profitability_margin */
            if (item.row.ignore_hide_parameters == 0 && site.settings.invoice_values_greater_than_zero == 1 && formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty))) == 0) {
                lock_submit = true;
                if (msg_error !== false) {
                    msg_error += 'El total de producto no puede ser igual a 0';
                } else {
                    msg_error = 'El total de producto no puede ser igual a 0';
                }
            }
            if (site.settings.disable_product_price_under_cost == 1) {
                if (parseFloat((site.settings.cost_to_profit_and_validation == 1 ? item.row.cost : item.row.avg_cost)) > parseFloat(unit_price)) {
                    lock_submit = true;
                    if (msg_error !== false) {
                        msg_error += 'El precio del producto no puede estar por debajo del costo';
                    } else {
                        msg_error = 'El precio del producto no puede estar por debajo del costo';
                    }
                }
            }

            var row_no = (site.settings.item_addition == 1 ? item.item_id : item.id) + (item.row.option && item.row.option > 0 ? item.row.option.toString() : '');
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + ' '+ cancelled_class +'" data-item-id="' + item_id + '"></tr>');
            let style_edit = (localStorage.getItem('pos_to_posfe_transformation') == 1) ? 'display: none;' : "";
            tr_html = '<td>'+
                        (site.settings.gift_card_product_id == item.row.id ?
                            (gc_incomplete_data ?
                                " <i class='text-danger fa fa-credit-card edit_gc' style='cursor:pointer;' data-itemid='"+item_id+"' id='"+ row_no +"'></i>" : " <i class='text-success fa fa-credit-card edit_gc' style='cursor:pointer;' data-itemid='"+item_id+"' id='"+ row_no +"'></i>")
                            :
                                '<i class="pull-right fa fa-edit fa-bx tip pointer edit" id="'+ row_no +'" data-item="'+ item_id +'" title="Edit" style="cursor:pointer;'+style_edit +'"></i>'
                        )+
                        '</td>';
            tr_html += '<td class="'+ cancelled_class +'">'+
                        '<input name="product_ordered_product_id[]" type="hidden" value="'+ ordered_product_id +'">'+
                        '<input name="under_cost_authorized[]" type="hidden" value="'+ (item.row.under_cost_authorized !== undefined ? item.row.under_cost_authorized : 0) +'">'+
                        '<input name="product_seller_id[]" type="hidden" value="'+ (item.row.seller_id !== undefined && item.row.seller_id ? item.row.seller_id : $('#posseller').val()) +'">'+
                        '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                        '<input name="ignore_hide_parameters[]" type="hidden" class="ignore_hide_parameters" value="' + item.row.ignore_hide_parameters + '">'+
                        '<input name="product_type[]" type="hidden" class="rtype" value="'+ item_type +'">'+
                        '<input name="product_code[]" type="hidden" class="rcode" value="'+ item_code +'">'+
                        '<input name="product_name[]" type="hidden" class="rname" value="'+ item_name +'">'+
                        '<input name="product_is_new[]" type="hidden" class="is_new" value="'+ (item.is_new !== undefined || item.prev_state_readiness !== undefined && item.prev_state_readiness != item_state_readiness ? true : false) +'">'+
                        '<input name="product_option[]" type="hidden" class="roption" value="'+ item_option +'">'+
                        '<input name="product_comment[]" type="hidden" class="rcomment" value="'+ item_comment +'">'+
                        '<input name="product_current_gold_price[]" type="hidden" class="rcurrent_gold_price" value="'+ parseFloat((item.row.current_price_gram !== undefined) ? item.row.current_price_gram : 0) +'">'+

                        '<input name="product_gift_card_no[]" type="hidden" value="'+ (item.gift_card !== undefined && item.gift_card.gc_card_no ? item.gift_card.gc_card_no : "" ) +'">'+
                        '<input name="product_gift_card_value[]" type="hidden" value="'+ (item.gift_card !== undefined && item.gift_card.gc_value ? item.gift_card.gc_value : "" ) +'">'+
                        '<input name="product_gift_card_expiry[]" type="hidden" value="'+ (item.gift_card !== undefined && item.gift_card.gc_expiry ? item.gift_card.gc_expiry : "" ) +'">'+
                        '<input name="state_readiness[]" type="hidden" class="rstate_readiness" id="state_readiness'+ item_id +'" value="'+ item_state_readiness +'" />'+
                        '<input name="preparation_area[]" type="hidden" class="rstate_readiness" id="preparation_area_'+ item_id +'" value="'+ (item.category && item.category.preparation_area_id ? item.category.preparation_area_id : null) +'" />'+
                        '<span class="sname" id="name_'+ row_no +'">'+ item_name + (sel_opt != '' ? ' ('+sel_opt+')' : '')+(item.gift_card !== undefined && item.gift_card.gc_card_no ? " ("+item.gift_card.gc_card_no+")" : "" )+(its_customer_birthday && item.row.birthday_discount ? ' <i class="cambio_colores fa-solid fa-cake-candles"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+'Descuento aplicado por cumpleaños de cliente'+'"></i>' : '')+
                        (item.row.op_pending_quantity !== undefined && item.row.op_pending_quantity > 0 ? ' <i class="fa-solid fa-info-circle"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+
                                "En stock : "+formatDecimal((parseFloat(item.row.op_pending_quantity)+parseFloat(item.row.quantity)))+" | "+
                                "Pendientes orden de pedido : "+formatDecimal(item.row.op_pending_quantity)+" | "+
                                "Disponibles : "+formatDecimal(item.row.quantity)+
                            '"'+'"></i>': "")+
                        '</span>'+
                        (incorrect_tax_rate ? " <span class='text-warning' style='font-weight:600;'>"+lang.incorrect_tax+' <i class="fa fa-exclamation-triangle"></i></span>' : '')+
                        '<span class="lb"></span>'+
                        // '<i class="pull-right fa fa-comment fa-bx'+ (item_comment != '' ? '' :'-o') +' tip pointer comment" id="'+ row_no +'" data-item="'+ item_id +'" title="Comment" style="cursor:pointer;margin-right:5px;"></i>'+
                    '</td>';
            tr_html += '<td class="text-right '+ cancelled_class +'">';
            if (site.settings.product_serial == 1) {
                tr_html += '<input class="form-control input-sm rserial" name="serial[]" type="hidden" id="serial_' + row_no + '" value="'+item_serial+'">';
            }
            if (site.settings.product_discount == 1) {
                tr_html += '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                '<input class="form-control input-sm" name="product_discount_val[]" type="hidden" value="' + (item_discount * item_qty) + '">';
            }
            // console.log(" >> pr_tax_2_val "+pr_tax_2_val);
            // console.log(" >> key_log "+key_log);
            if (site.settings.tax1 == 1) {
                tr_html += '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax.id : site.except_tax_rate_id) + '">'+
                            '<input type="hidden" class="sproduct_tax" id="sproduct_tax_' + row_no + '" value="' + formatMoney(pr_tax_val * item_qty) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_rate[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 1) + '">'+
                            '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax_val : 0) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_val[]" type="hidden" value="' + (key_log == '1' ? pr_tax_val : 0) + '">';
                tr_html += '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_rate_id : 0) + '">'+
                            '<input type="hidden" class="sproduct_tax" id="sproduct_tax_' + row_no + '_2" value="' + formatMoney(pr_tax_2_val * item_qty) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_rate_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 0) + '">'+
                            '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_val : 0) + '">'+
                            '<input class="form-control input-sm text-right" name="product_tax_val_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_val : 0) + '">';
            }
            tr_html += '<input class="rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">'+
                        '<input class="ruprice" name="unit_price[]" type="hidden" value="' + unit_price + '">'+
                        '<input class="realuprice" name="real_unit_price[]" type="hidden" value="' + item.row.real_unit_price + '">'+
                        (item.row.price_before_promo !== undefined ? '<input name="price_before_promo[]" type="hidden" value="' + item.row.price_before_promo + '">' : '' );


            if (msg_error == false) {
                tr_html += '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) + '</span>';
            } else {
                tr_html += '<span class="text-right sprice text-danger danger bold " id="sprice_' + row_no + '" data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+msg_error+'">' + formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) + '</span>';
            }
            tr_html += '</td>';
            let quantityEdit = (localStorage.getItem('pos_to_posfe_transformation') == 1) ? 'readonly' : "";
            tr_html += '<td class="'+ cancelled_class +'">'+
                            '<input class="form-control input-sm kb-pad text-center rquantity" '+quantityEdit+' tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatQuantity2(item_qty) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_unit_id_selected[]" type="hidden" value="' + item.row.product_unit_id_selected + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + item_qty + '">'+
                            '<input name="product_aqty[]" type="hidden" class="item_aqty" value="' + item_aqty + '">'+
                        '</td>';

            tr_html += '<td class="text-right '+ cancelled_class +'">'+
                            '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))) +'</span>'+
                        '</td>';
            let style_trash = (localStorage.getItem('pos_to_posfe_transformation') == 1) ? 'style="display: none"' : "";
            tr_html += '<td class="text-center text-danger'+ cancelled_class +' posdel" id="' + row_no + '" title="Remove"  style="cursor:pointer;">'+
                            '<i class="fa fa-trash fa-lg tip pointer"'+style_trash+'></i>'+
                        '</td>';
            newTr.html(tr_html);
            newTr.appendTo("#posTable");
            if (item.preferences_text !== null && item.preferences_text !== undefined) {
                var newTr2 = $('<tr class="tr_preferences" style="font-size:112%;"></tr>');
                tr2_html = '<td colspan="6">'+
                            '<input type="hidden" name="product_preferences_text[]" value="'+JSON.stringify(item.preferences_selected)+'">' +
                            item.preferences_text +
                          '</td>';
                newTr2.html(tr2_html);
                newTr2.appendTo("#posTable");
            }
            if (localStorage.getItem('restobar_mode') == '1') {
                if (item.state_readiness != 3) {
                    total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
                    total_after_tax += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
                    total_tax += ((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty));
                    count += parseFloat(item_qty);
                }
            } else {
                total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)+ parseFloat(pr_tax_2_val)) * parseFloat(item_qty)));
                total_after_tax += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
                total_tax += ((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty));
                count += parseFloat(item_qty);
            }
            an++;

            if (site.settings.overselling == 0 && product_total_qty[product_id+'A'] > item.row.quantity && item.row.type != "service") {
                header_alert('error', 'El producto '+item_name+' no cuenta con cantidades disponibles para la venta');
                $('#row_' + row_no).addClass('danger');
                lock_submit = true;
            }
            if (item_type == 'standard' && item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item_option && item_qty > this.quantity) {
                        $('#row_' + row_no).addClass('danger');
                        if (site.settings.overselling == 0) {
                            lock_submit = true;
                        }
                    }
                });
            } else if(item_type == 'standard' && item_qty > item_aqty) {
                $('#row_' + row_no).addClass('danger');
                if (site.settings.overselling == 0) {
                    lock_submit = true;
                }
            } else if (item_type == 'combo') {
                if(combo_items === false) {
                    $('#row_' + row_no).addClass('danger');
                    if (site.settings.overselling == 0) {
                        lock_submit = true;
                    }
                } else {
                    $.each(combo_items, function(){
                        if(parseFloat(this.quantity) < (parseFloat(this.qty)*item_qty) && this.type == 'standard') {
                            $('#row_' + row_no).addClass('danger');
                            if (site.settings.overselling == 0) {
                                lock_submit = true;
                            }
                        }
                    });
                }
            }
            var comments = item_comment.split(/\r?\n/g);
            if (pos_settings.remote_printing != 1) {
                b_items += product_name("#"+(an-1)+" "+ item_code + " - " + item_name) + "\n";
                for (var i = 0, len = comments.length; i < len; i++) {
                    b_items += (comments[i].length > 0 ? "   * "+comments[i]+"\n" : "");
                }
                b_items += printLine("   "+formatDecimal(item_qty) + " x " + formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val))+": "+ formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty)))) + "\n";
                o_items += printLine(product_name("#"+(an-1)+" "+ item_code + " - " + item_name) + ": [ "+ (item_ordered != 0 ? 'xxxx' : formatDecimal(item_qty))) + " ]\n";
                for (var i = 0, len = comments.length; i < len; i++) {
                    o_items += (comments[i].length > 0 ? "   * "+comments[i]+"\n" : "");
                }
                o_items += "\n";
            } else {
                if (pos_settings.item_order == 1 && print_cate) {
                    var bprTh = $('<tr></tr>');
                    bprTh.html('<td colspan="100%" class="no-border"><strong>'+item.row.category_name+'</strong></td>');
                    var oprTh = $('<tr></tr>');
                    oprTh.html('<td colspan="100%" class="no-border"><strong>'+item.row.category_name+'</strong></td>');
                    $("#order-table").append(oprTh);
                    $("#bill-table").append(bprTh);
                }
                var bprTr = '<tr class="row_' + item_id + '" data-item-id="' + item_id + '"><td colspan="2" class="no-border">#'+(an-1)+' '+ item_code + " - " + item_name + '';
                for (var i = 0, len = comments.length; i < len; i++) {
                    bprTr += (comments[i] ? '<br> <b>*</b> <small>'+comments[i]+'</small>' : '');
                }
                bprTr += '</td></tr>';
                bprTr += '<tr class="row_' + item_id + '" data-item-id="' + item_id + '"><td>(' + formatDecimal(item_qty) + ' x ' + (item_discount != 0 ? '<del>'+formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val) + item_discount)+'</del>' : '') + formatMoney(parseFloat(item_price) + parseFloat(pr_tax_val))+ ')</td><td style="text-align:right;">'+ formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty))) +'</td></tr>';
                var oprTr = '<tr class="row_' + item_id + '" data-item-id="' + item_id + '"><td>#'+(an-1)+' ' + item_code + " - " + item_name + '';
                for (var i = 0, len = comments.length; i < len; i++) {
                    oprTr += (comments[i] ? '<br> <b>*</b> <small>'+comments[i]+'</small>' : '');
                }
                oprTr += '</td><td>[ ' + (item_ordered != 0 ? 'xxxx' : formatDecimal(item_qty)) +' ]</td></tr>';
                $("#order-table").append(oprTr);
                $("#bill-table").append(bprTr);
            }
            positems[item_id].end_price = formatDecimal(item_price) + formatDecimal(pr_tax_val);
            positems[item_id].end_tax = formatDecimal(pr_tax_val);
            positems[item_id].end_discount = formatDecimal(item_discount);
        });
        localStorage.setItem('positems', JSON.stringify(positems));
        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (postax2 = localStorage.getItem('postax2')) {
                $.each(tax_rates, function () {
                    if (this.id == postax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        }
                        if (this.type == 1) {
                            invoice_tax = formatDecimal((((total_after_tax - order_discount) * this.rate) / 100));
                        }
                    }
                });
            }
        }
        total = formatDecimal(total);
        product_tax = formatDecimal(product_tax);
        total_discount = formatDecimal(order_discount + product_discount);
        shipping = formatDecimal(localStorage.getItem('posshipping'));
        if (pos_settings.apply_suggested_home_delivery_amount > 0 && (restobar_mode_module == false || (restobar_mode_module != false && restobar_mode_module == 2))) {
            posshipping = pos_settings.apply_suggested_home_delivery_amount;
            $('#posshipping').val(posshipping);
            shipping = parseFloat(posshipping);
            localStorage.setItem('posshipping', posshipping)
        }
        if (pos_settings.home_delivery_in_invoice == 0 && (!restobar_mode_module || (restobar_mode_module != false && restobar_mode_module == 2))) {
            var setted_shipping = shipping;
            shipping = 0;
        }
        // Totals calculations after item addition
        subtotalwtax = total - total_tax;
        // Order level discount calculations
        if (posdiscount = localStorage.getItem('posdiscount')) {
            var ds = posdiscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    order_discount = formatDecimal((parseFloat(((subtotalwtax) * parseFloat(pds[0])) / 100)));
                } else {
                    order_discount = parseFloat(ds);
                }
            } else {
                order_discount = parseFloat(ds);
            }
        }
        if (site.pos_settings.max_net_sale > 0 && subtotalwtax > site.pos_settings.max_net_sale) {
            if (!localStorage.getItem('pos_subtotalwtax')) {

                bootbox.alert({
                    message: 'El subtotal de la venta ($ '+formatMoney(subtotalwtax)+') supera el máximo permitido ($ '+formatMoney(site.pos_settings.max_net_sale)+') para este tipo de venta',
                    callback: function () {
                        if ($('#pricesChanged').attr('aria-hidden') == 'false') {
                            setTimeout(function() {
                                $('#pricesChanged').modal('show');
                            }, 1000);
                        }
                    }
                });
                localStorage.setItem('pos_subtotalwtax', 1);
                //pricesChanged
            }
        }
        var gtotal = parseFloat(((total + invoice_tax) - order_discount) + parseFloat(shipping));
        $('#subtotal').text(formatMoney(subtotalwtax));
        $('.product_number').text("("+(an-1)+")");
        $('.pmnt_product_number').text((an-1));
        $('#ivaamount').text(formatMoney(total_tax));
        $('#total').text(formatMoney(total));
        $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        $('.pmnt_total_items').text(formatQuantity2(parseFloat(count) - 1));
        $('#tds').text(formatMoney(product_discount+order_discount));
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(invoice_tax));
        }
        $('#tship').text(parseFloat(shipping) > 0 ? formatMoney(shipping) : '');
        $('#shipping_in_grand_total').val(1);
        // if (pos_settings.home_delivery_in_invoice == 0 && restobar_mode_module != false) {
        if (pos_settings.home_delivery_in_invoice == 0) {
            $('#tship').text(formatMoney(setted_shipping));
            $('#shipping_in_grand_total').val(0);
        }
        $('#gtotal').text(formatMoney(gtotal));
        lock_submit_by_min_sale_amount = false;
        if (gtotal > 0 && localStorage.getItem('posbiller') && billers_data[localStorage.getItem('posbiller')].min_sale_amount > 0) {
            $('.text_min_sale_amount').text(formatMoney(billers_data[localStorage.getItem('posbiller')].min_sale_amount));
            if (gtotal < billers_data[localStorage.getItem('posbiller')].min_sale_amount) {
                $('.error_min_sale_amount').fadeIn();
                $('#payment').prop('disabled', true);
                // command: toastr.error('La venta no cumple con el monto mínimo establecido', 'Error', { onHidden : function(){} });
                lock_submit_by_min_sale_amount = true;
            } else if (gtotal >= billers_data[localStorage.getItem('posbiller')].min_sale_amount) {
                $('.error_min_sale_amount').fadeOut();
                $('#payment').prop('disabled', false);
            }
        } else if (gtotal < 0) {
            bootbox.alert(lang.unexpected_value);
        }
        /* SALE TIP */
        $('#tips').text(formatMoney(0));
        $('#sale_tip_amount').val(0);
        no_tip = false;
        if (localStorage.getItem('delete_tip_amount') || (pos_settings.except_tip_restobar == 1 && (localStorage.getItem('restobar_mode_module') && (localStorage.getItem('restobar_mode_module') == 2 || localStorage.getItem('restobar_mode_module') == 3)))) {
            no_tip = true;
        }
        if ((pos_settings.apply_suggested_tip > 0 && !no_tip) || (localStorage.getItem('manually_entered_tip') == 1 && formatDecimal(localStorage.getItem('tip_amount')) > 0)) {
            if (localStorage.getItem('tip_amount') && formatDecimal(localStorage.getItem('tip_amount')) > 0 && localStorage.getItem('manually_entered_tip') == 1) {
                tip_amount = formatDecimal(localStorage.getItem('tip_amount'));
            } else {
                tip_amount = gtotal * (pos_settings.apply_suggested_tip / 100);
                localStorage.setItem('tip_amount', tip_amount);
            }
            $('#tips').text(formatMoney(tip_amount));
            gtotal = gtotal + tip_amount;
            $('#gtotal').text(formatMoney(gtotal));
            $('#sale_tip_amount').val(formatDecimal(tip_amount)+0);
        } else {
            if (localStorage.getItem('tip_amount')) {
                localStorage.removeItem('tip_amount');
            }
        }
        /* SALE TIP */
        localStorage.setItem('subtotal', subtotalwtax);
        localStorage.setItem('tax', total_tax);
        localStorage.setItem('total', total);
        localStorage.setItem('discount', product_discount+order_discount);
        localStorage.setItem('gtotal', gtotal);
        localStorage.setItem('shipping', shipping);
        if (pos_settings.remote_printing != 1) {
            order_data.items = o_items;
            bill_data.items = b_items;
            var b_totals = '';
            b_totals += printLine(lang.total+': '+ formatMoney(total)) +"\n";
            if(order_discount > 0 || product_discount > 0) {
                b_totals += printLine(lang.discount+': '+ formatMoney(order_discount+product_discount)) +"\n";
            }
            if (site.settings.tax2 != 0 && invoice_tax != 0) {
                b_totals += printLine(lang.order_tax+': '+ formatMoney(invoice_tax)) +"\n";
            }
            b_totals += printLine(lang.grand_total+': '+ formatMoney(gtotal)) +"\n";
            if(pos_settings.rounding != 0) {
                round_total = roundNumber(gtotal, parseInt(pos_settings.rounding));
                var rounding = formatDecimal(round_total - gtotal);
                b_totals += printLine(lang.rounding+': '+ formatMoney(rounding)) +"\n";
                b_totals += printLine(lang.total_payable+': '+ formatMoney(round_total)) +"\n";
            }
            b_totals += "\n"+ lang.items+': '+ (an - 1) + ' (' + (parseFloat(count) - 1) + ')' +"\n";
            bill_data.totals = b_totals;
            bill_data.footer = "\n"+ lang.merchant_copy+"\n";
        } else {
            var bill_totals = '';
            bill_totals += '<tr class="bold"><td>'+lang.total+'</td><td style="text-align:right;">'+formatMoney(total)+'</td></tr>';
            if(order_discount > 0 || product_discount > 0) {
                bill_totals += '<tr class="bold"><td>'+lang.discount+'</td><td style="text-align:right;">'+formatMoney(order_discount+product_discount)+'</td></tr>';
            }
            if (site.settings.tax2 != 0 && invoice_tax != 0) {
                bill_totals += '<tr class="bold"><td>'+lang.order_tax+'</td><td style="text-align:right;">'+formatMoney(invoice_tax)+'</td></tr>';
            }
            bill_totals += '<tr class="bold"><td>'+lang.grand_total+'</td><td style="text-align:right;">'+formatMoney(gtotal)+'</td></tr>';
            if(pos_settings.rounding != 0) {
                round_total = roundNumber(gtotal, parseInt(pos_settings.rounding));
                var rounding = formatDecimal(round_total - gtotal);
                bill_totals += '<tr class="bold"><td>'+lang.rounding+'</td><td style="text-align:right;">'+formatMoney(rounding)+'</td></tr>';
                bill_totals += '<tr class="bold"><td>'+lang.total_payable+'</td><td style="text-align:right;">'+formatMoney(round_total)+'</td></tr>';
            }
            bill_totals += '<tr class="bold"><td>'+lang.items+'</td><td style="text-align:right;">'+(an - 1) + ' (' + (parseFloat(count) - 1) + ')</td></tr>';
            $('#bill-total-table').empty();
            $('#bill-total-table').append(bill_totals);
            $('#bill_footer').append('<p class="text-center"><br>'+lang.merchant_copy+'</p>');
        }
        if(count > 1) {
            $('#poscustomer').select2("readonly", true);
            // $('#poswarehouse').select2("readonly", true);
        } else {
            $('#poscustomer').select2("readonly", false);
            // $('#poswarehouse').select2("readonly", false);
        }
        // if (KB) { display_keyboards(); }
        if (localStorage.getItem('balance_label')) {
            $('#add_item').attr('tabindex', 1);
            $('#add_item').focus();
        } else {
            if (site.settings.product_order == 2) {
                an = 2;
            }
            set_page_focus(an);
        }
        recalcular_posretenciones();
        validate_fe_customer_data();
        $('[data-toggle="tooltip"]').tooltip();
        if (lock_submit !== false || lock_submit_by_margin !== false || lock_submit_by_min_sale_amount !== false) {
            $('.submit-sale').fadeOut();
        } else {
            $('.submit-sale').fadeIn();
        }
        localStorage.setItem('lock_submit', lock_submit);
        localStorage.setItem('lock_submit_by_margin', lock_submit_by_margin);
        localStorage.setItem('lock_submit_by_min_sale_amount', lock_submit_by_min_sale_amount);
        if (except_category_taxes) {
            $('#except_category_taxes').val(1);
        }
        if (tax_exempt_customer) {
            $('#tax_exempt_customer').val(1);
        }
        if (diferent_tax_alert) {
            fix_different_taxes();
        }
    } else {
        localStorage.getItem('positems', '');
    }
    if (is_sale_gc) {
        $('select.paid_by option').each(function(index, option){
            if ($(option).val() == 'gift_card') {
                $(option).remove();
            }
        });
    }
    hide_menu_sobre_productos();
    setTimeout(function() {
        if (site.pos_settings.express_payment == 1) {
            showHideAlertFE($('#subtotal').text());
        }
    }, 1200);
}

function fix_different_taxes(){
    if (localStorage.getItem('positems') && !localStorage.getItem('diferent_tax_alert')) {
        localStorage.setItem('diferent_tax_alert', 1);
        // bootbox.confirm({
        //     message: "<h3>¡Validación tarifa de impuestos!</h3></br>Se detectó que en algunos productos el registro de venta suspendida tiene el impuesto diferente al asignado orignalmente al producto, ¿Desea corregir el impuesto?",
        //     buttons: {
        //         confirm: {
        //             label: 'Asignar el del producto',
        //             className: 'btn-success btn-full'
        //         },
        //         cancel: {
        //             label: 'Conservar el del registro',
        //             className: 'btn-danger btn-full'
        //         }
        //     },
        //     callback: function (result) {
        //         if(result == true) {
                    positems = localStorage.getItem('positems') ? JSON.parse(localStorage.getItem('positems')) : [];
                    $.each(positems, function(index, item){
                        if (item.row.diferent_tax_alert) {
                            item.row.tax_rate = item.row.real_tax_rate;
                            item.tax_rate = item.real_tax_rate;
                            localStorage.setItem('diferent_tax_alert_fixed', 1);
                        }
                    });
                    localStorage.setItem('positems', JSON.stringify(positems));
                    loadItems();
                // }
            // }

        // });
    }
}

function printLine(str) {
    var size = pos_settings.char_per_line;
    var len = str.length;
    var res = str.split(":");
    var newd = res[0];
    for(i=1; i<(size-len); i++) {
        newd += " ";
    }
    newd += res[1];
    return newd;
}

/* -----------------------------
 * Add Purchase Iten Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */

 function add_invoice_item(item, modal = false, cont = false) {
    $('#payment').prop('disabled', true);
    if (count == 1) {
        positems = {};
        if ($('#poswarehouse').val() && $('#poscustomer').val()) {
            $('#poscustomer').select2("readonly", true);
            $('#poswarehouse').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }
    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    item_id = item_id + (item.row.option && item.row.option > 0 ? item.row.option.toString() : '');

    if (positems[item_id]) {
        var new_qty = parseFloat(positems[item_id].row.qty) + (item.row.qty ? parseFloat(item.row.qty) : (item.row.paste_balance_value == 1 ? 0 : 1));
        positems[item_id].row.base_quantity = new_qty;
        if(positems[item_id].row.unit != positems[item_id].row.base_unit) {
            $.each(positems[item_id].units, function(){
                if (this.id == positems[item_id].row.unit) {
                    positems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        positems[item_id].row.qty = new_qty;

    } else {
        positems[item_id] = item;
    }
    positems[item_id].order = new Date().getTime();
    //NUEVO

    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(positems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal_option_id').val('');
        $('#serialModal').modal('show');
        // console.log('>> add_invoice_item modal item_id '+item_id);
        localStorage.setItem('modalSerial_item', JSON.stringify(positems[item_id]));
        delete positems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
        }, 850);
    } else {
        if (site.settings.product_variant_per_serial == 1) {
            localStorage.removeItem('modalSerial_item');
            if (site.settings.product_variant_per_serial == 1 && cont == false) {
                $('#serialModal').modal('hide');
                $('#add_item').val('').focus();
            } else {
                item = positems[item_id];
                $.ajax({
                    url : site.base_url + "products/get_random_id"
                }).done(function(data){
                    item.id = data;
                    localStorage.setItem('modalSerial_item', JSON.stringify(item));
                    $('#serialModal_serial').val('');
                    $('#serialModal_meters').val('');
                });
            }
        }
        localStorage.setItem('id_item_focus', item_id);
        localStorage.setItem('positems', JSON.stringify(positems));
        // console.log('>> positems[item_id].row.qty '+positems[item_id].row.qty);

        if (site.pos_settings.balance_settings == 1 && item.row.paste_balance_value == 1) {
            navigator.clipboard.readText()
            .then(text => {
                value = parseFloat(text);
                positems[item_id].row.qty = (value > 0 ? value : 0);
                positems[item_id].row.base_quantity = (value > 0 ? value : 0);
                positems[item_id].row.quantity = (value > 0 ? value : 0);
                wappsiPreciosUnidad(positems[item_id].row.id,item_id,positems[item_id].row.qty, true);
                navigator.clipboard.writeText('').then(() => {
                    console.log("Text copied to clipboard");
                  }).catch(error => {
                    console.log("Error copying text to clipboard:", error);
                  });
            })
            .catch(err => {
              console.log('Error reading from clipboard:', err);
            });
        } else {
            wappsiPreciosUnidad(positems[item_id].row.id,item_id,positems[item_id].row.qty, true, true);
        }
        setTimeout(function() {
            if (site.pos_settings.show_variants_and_preferences == 1) {
                if (positems[item_id].row.variant_selected === undefined) {
                    $('#row_'+item_id).find('.sname').trigger('click');
                }
            }
        }, 1500);
        return true;
    }
    //NUEVO
 }
 if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
 }
function display_keyboards() {
    // $('.kb-text').keyboard({
    //     autoAccept: true,
    //     alwaysOpen: false,
    //     openOn: 'focus',
    //     usePreview: false,
    //     layout: 'custom',
    //     //layout: 'qwerty',
    //     display: {
    //         'bksp': "\u2190",
    //         'accept': 'return',
    //         'default': 'ABC',
    //         'meta1': '123',
    //         'meta2': '#+='
    //     },
    //     customLayout: {
    //         'default': [
    //         'q w e r t y u i o p {bksp}',
    //         'a s d f g h j k l {enter}',
    //         '{s} z x c v b n m , . {s}',
    //         '{meta1} {space} {cancel} {accept}'
    //         ],
    //         'shift': [
    //         'Q W E R T Y U I O P {bksp}',
    //         'A S D F G H J K L {enter}',
    //         '{s} Z X C V B N M / ? {s}',
    //         '{meta1} {space} {meta1} {accept}'
    //         ],
    //         'meta1': [
    //         '1 2 3 4 5 6 7 8 9 0 {bksp}',
    //         '- / : ; ( ) \u20ac & @ {enter}',
    //         '{meta2} . , ? ! \' " {meta2}',
    //         '{default} {space} {default} {accept}'
    //         ],
    //         'meta2': [
    //         '[ ] { } # % ^ * + = {bksp}',
    //         '_ \\ | &lt; &gt; $ \u00a3 \u00a5 {enter}',
    //         '{meta1} ~ . , ? ! \' " {meta1}',
    //         '{default} {space} {default} {accept}'
    //         ]}
    //     });
    // $('.kb-pad').keyboard({
    //     restrictInput: true,
    //     preventPaste: true,
    //     autoAccept: true,
    //     alwaysOpen: false,
    //     openOn: 'click',
    //     usePreview: false,
    //     layout: 'custom',
    //     display: {
    //         'b': '\u2190:Backspace',
    //     },
    //     customLayout: {
    //         'default': [
    //         '1 2 3 {b}',
    //         '4 5 6 . {clear}',
    //         '7 8 9 0 %',
    //         '{accept} {cancel}'
    //         ]
    //     }
    // });
    // var cc_key = (site.settings.decimals_sep == ',' ? ',' : '{clear}');
    // $('.kb-pad1').keyboard({
    //     restrictInput: true,
    //     preventPaste: true,
    //     autoAccept: true,
    //     alwaysOpen: false,
    //     openOn: 'click',
    //     usePreview: false,
    //     layout: 'custom',
    //     display: {
    //         'b': '\u2190:Backspace',
    //     },
    //     customLayout: {
    //         'default': [
    //         '1 2 3 {b}',
    //         '4 5 6 . '+cc_key,
    //         '7 8 9 0 %',
    //         '{accept} {cancel}'
    //         ]
    //     }
    // });

 }

/*$(window).bind('beforeunload', function(e) {
    if(count > 1){
    var msg = 'You will loss the sale data.';
        (e || window.event).returnValue = msg;
        return msg;
    }
});
*/
if(site.settings.auto_detect_barcode == 1) {
    $(document).ready(function() {
        var pressed = false;
        var chars = [];
        $(window).keypress(function(e) {
            if(e.key == '%') { pressed = true; }
            chars.push(String.fromCharCode(e.which));
            if (pressed == false) {
                setTimeout(function(){
                    if (chars.length >= 8) {
                        var barcode = chars.join("");
                        $( "#add_item" ).focus().autocomplete( "search", barcode );
                    }
                    chars = [];
                    pressed = false;
                },200);
            }
            pressed = true;
        });
    });
}

$(document).ready(function() {
    read_card();
});

function generateCardNo(x) {
    if(!x) { x = 16; }
    chars = "1234567890";
    no = "";
    for (var i=0; i<x; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        no += chars.substring(rnum,rnum+1);
    }
    return no;
}
function roundNumber(number, toref) {
    switch(toref) {
        case 1:
            var rn = formatDecimal(Math.round(number * 20)/20);
            break;
        case 2:
            var rn = formatDecimal(Math.round(number * 2)/2);
            break;
        case 3:
            var rn = formatDecimal(Math.round(number));
            break;
        case 4:
            var rn = formatDecimal(Math.ceil(number));
            break;
        default:
            var rn = number;
    }
    return rn;
}
function getNumber(x) {
    return accounting.unformat(x);
}
function formatQuantity(x) {
    return (x != null) ? '<div class="text-center">'+formatNumber(x, site.settings.qty_decimals)+'</div>' : '';
}
function formatQuantity2(x) {
    return (x != null) ? formatQuantityNumber(x, site.settings.qty_decimals) : '';
}
function formatQuantityNumber(x, d) {
    if (!d) { d = site.settings.qty_decimals; }
    return parseFloat(accounting.formatNumber(x, d, '', '.'));
}
function formatQty(x) {
    return (x != null) ? formatNumber(x, site.settings.qty_decimals) : '';
}
function formatNumber(x, d) {
    if(!d && d != 0) { d = site.settings.decimals; }
    if(site.settings.sac == 1) {
        return formatSA(parseFloat(x).toFixed(d));
    }
    return accounting.formatNumber(x, d, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep);
}
function formatMoney(x, symbol) {
    if(!symbol) { symbol = ""; }
    if(site.settings.sac == 1) {
        return symbol+''+formatSA(parseFloat(x).toFixed(site.settings.decimals));
    }
    return accounting.formatMoney(x, symbol, site.settings.decimals, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
}
function formatCNum(x) {
    if (site.settings.decimals_sep == ',') {
        var x = x.toString();
        var x = x.replace(",", ".");
        return parseFloat(x);
    }
    return x;
}
// function formatDecimal(x, d) {
//     if (!d) { d = site.settings.decimals; }
//     return parseFloat(accounting.formatNumber(x, d, '', '.'));
// }
function hrsd(sdate) {
    return moment().format(site.dateFormats.js_sdate.toUpperCase())
}

function hrld(ldate) {
    return moment().format(site.dateFormats.js_sdate.toUpperCase()+' H:mm')
}
function is_valid_discount(mixed_var) {
    return (is_numeric(mixed_var) || (/([0-9]%)/i.test(mixed_var))) ? true : false;
}
function is_numeric(mixed_var) {
    var whitespace =
    " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
        1)) && mixed_var !== '' && !isNaN(mixed_var);
}
function is_float(mixed_var) {
    return +mixed_var === mixed_var && (!isFinite(mixed_var) || !! (mixed_var % 1));
}
function currencyFormat(x) {
    return formatMoney(x != null ? x : 0);
}
function formatSA (x) {
    x=x.toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
       afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;
    return res;
}

function unitToBaseQty(qty, unitObj) {
    switch(unitObj.operator) {
        case '*':
            return parseFloat(qty)*parseFloat(unitObj.operation_value);
            break;
        case '/':
            return parseFloat(qty)/parseFloat(unitObj.operation_value);
            break;
        case '+':
            return parseFloat(qty)+parseFloat(unitObj.operation_value);
            break;
        case '-':
            return parseFloat(qty)-parseFloat(unitObj.operation_value);
            break;
        default:
            return parseFloat(qty);
    }
}

function baseToUnitQty(qty, unitObj) {
    switch(unitObj.operator) {
        case '*':
            return parseFloat(qty)/parseFloat(unitObj.operation_value);
            break;
        case '/':
            return parseFloat(qty)*parseFloat(unitObj.operation_value);
            break;
        case '+':
            return parseFloat(qty)-parseFloat(unitObj.operation_value);
            break;
        case '-':
            return parseFloat(qty)+parseFloat(unitObj.operation_value);
            break;
        default:
            return parseFloat(qty);
    }
}

function read_card() {
    var typingTimer;
    $('.swipe').keyup(function (e) {
        e.preventDefault();
        var self = $(this);
        clearTimeout(typingTimer);
        typingTimer = setTimeout(function() {
            var payid = self.attr('id');
            var id = payid.substr(payid.length - 1);
            var v = self.val();
            var p = new SwipeParserObj(v);

            if(p.hasTrack1) {
                var CardType = null;
                var ccn1 = p.account.charAt(0);
                if(ccn1 == 4)
                    CardType = 'Visa';
                else if(ccn1 == 5)
                    CardType = 'MasterCard';
                else if(ccn1 == 3)
                    CardType = 'Amex';
                else if(ccn1 == 6)
                    CardType = 'Discover';
                else
                    CardType = 'Visa';

                $('#pcc_no_'+id).val(p.account).change();
                $('#pcc_holder_'+id).val(p.account_name).change();
                $('#pcc_month_'+id).val(p.exp_month).change();
                $('#pcc_year_'+id).val(p.exp_year).change();
                $('#pcc_cvv2_'+id).val('');
                $('#pcc_type_'+id).val(CardType).change();
                self.val('');
                $('#pcc_cvv2_'+id).focus();
            } else {
                $('#pcc_no_'+id).val('');
                $('#pcc_holder_'+id).val('');
                $('#pcc_month_'+id).val('');
                $('#pcc_year_'+id).val('');
                $('#pcc_cvv2_'+id).val('');
                $('#pcc_type_'+id).val('');
            }
        }, 100);
    });
    $('.swipe').keydown(function (e) {
        clearTimeout(typingTimer);
    });
}

function check_add_item_val() {
    $('#add_item').bind('keypress', function (e) {
        if (e.keyCode == 13 || e.keyCode == 9) {
            e.preventDefault();
            $(this).autocomplete("search");
        }
    });
}
function nav_pointer() {
    var pp = p_page == 'n' ? 0 : p_page;
    (pp == 0) ? $('#previous').attr('disabled', true) : $('#previous').attr('disabled', false);
    (pp == 0) ? $('#first').attr('disabled', true) : $('#first').attr('disabled', false);
    ((pp+pro_limit) > tcp) ? $('#next').attr('disabled', true) : $('#next').attr('disabled', false);
    ((pp+pro_limit) > tcp) ? $('#last').attr('disabled', true) : $('#last').attr('disabled', false);
}

function product_name(name, size) {
    if (!size) { size = 42; }
    return name.substring(0, (size-7));
}

// $.extend($.keyboard.keyaction, {
//     enter : function(base) {
//         if (base.$el.is("textarea")){
//             base.insertText('\r\n');
//         } else {
//             base.accept();
//         }
//     }
// });

$(document).ajaxStart(function(){
  $('#ajaxCall').show();
}).ajaxStop(function(){
  $('#ajaxCall').hide();
});

$(document).ready(function(){
    nav_pointer();
    // $('#leftdiv').css('height', (screen.height*0.8)+"px");
    $('#myModal').on('show.bs.modal', function () {
    });
    $('#myModal').on('hidden.bs.modal', function() {
        $(this).find('.modal-dialog').empty();
        $(this).removeData('bs.modal');
    });
    $('#myModal2').on('hidden.bs.modal', function () {
        $(this).find('.modal-dialog').empty();
        $(this).removeData('bs.modal');
        $('#myModal').css('zIndex', '1050');
        $('#myModal').css('overflow-y', 'scroll');
    });
    $('#myModal2').on('show.bs.modal', function () {
        $('#myModal').css('zIndex', '1040');
    });
    $('.modal').on('hidden.bs.modal', function() {
        $(this).removeData('bs.modal');
    });
    $('.modal').on('show.bs.modal', function () {
        $('#modal-loading').show();
        $('.blackbg').css('zIndex', '1041');
        $('.loader').css('zIndex', '1042');
    }).on('hide.bs.modal', function () {
        $('#modal-loading').hide();
        $('.blackbg').css('zIndex', '3');
        $('.loader').css('zIndex', '4');
    });
    $('#clearLS').click(function(event) {
        bootbox.confirm("Are you sure?", function(result) {
        if(result == true) {
            localStorage.clear();
            location.reload();
        }
        });
        return false;
    });
    /* function for manage responsive in pos*/
    resizeWindow();
    $(window).resize(resizeWindow);
});
$(document).on('keyup', '.amount', function(){
    amount = $(this);
    payment = $(this).closest('.payment');
    paid_by = payment.find('select.paid_by');
    amount_to_pay = payment.find('.amount').val();
    customer = $('#poscustomer').val();
    if (paid_by.val() == "deposit") {
        $.ajax({
            url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    } else if (paid_by.val() == "gift_card") {
        if (amount.data('mingc') && parseFloat(amount.val()) > parseFloat(amount.data('mingc'))) {
            amount.val(formatDecimal(amount.data('mingc')));
            command: toastr.error('No puede pagar más del saldo de la tarjeta', 'Mensaje de validación', { onHidden : function(){} });
        }
    }
});
//PAYMENT TERM
$(document).on('change', '#cost_center', function(){
    $('#cost_center_id').val($(this).val());
});
//PAYMENT TERM
//$.ajaxSetup ({ cache: false, headers: { "cache-control": "no-cache" } });
if(pos_settings.focus_add_item != '') { shortcut.add(pos_settings.focus_add_item, function() { $("#add_item").focus(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.add_manual_product != '') { shortcut.add(pos_settings.add_manual_product, function() { window.open(site.base_url+'/products/add', '_blank'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.customer_selection != '') { shortcut.add(pos_settings.customer_selection, function() { $("#poscustomer").select2("open"); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.add_customer != '') { shortcut.add(pos_settings.add_customer, function() { $("#add-customer").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.toggle_category_slider != '') { shortcut.add(pos_settings.toggle_category_slider, function() { $("#open-category").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.toggle_brands_slider != '') { shortcut.add(pos_settings.toggle_brands_slider, function() { $("#open-brands").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.toggle_subcategory_slider != '') { shortcut.add(pos_settings.toggle_subcategory_slider, function() { $("#open-subcategory").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.cancel_sale != '') { shortcut.add(pos_settings.cancel_sale, function() { $("#reset").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.suspend_sale != '') { shortcut.add(pos_settings.suspend_sale, function() { $("#suspend").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.print_items_list != '') {
    shortcut.add(pos_settings.print_items_list, function() {
        window.open(site.base_url+`products`)
    },
    { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.finalize_sale != '') { shortcut.add(pos_settings.finalize_sale, function() { if ($('#paymentModal').is(':visible')) { $(".submit-sale").click(); } else { $("#payment").click(); } }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.today_sale != '') { shortcut.add(pos_settings.today_sale, function() { $("#today_sale").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.open_hold_bills != '') { shortcut.add(pos_settings.open_hold_bills, function() { $("#opened_bills").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.close_register != '') { shortcut.add(pos_settings.close_register, function() { $("#close_register").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.search_product_price != '') { shortcut.add(pos_settings.search_product_price, function() { $("#search_product_price").click(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.focus_last_product_quantity != '') { shortcut.add(pos_settings.focus_last_product_quantity, function() { focus_last_item_quantity(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
shortcut.add("ESC", function() { $("#cp").trigger('click'); }, { 'type':'keydown', 'propagate':false, 'target':document} );
if(pos_settings.open_cash_register != '') { shortcut.add(pos_settings.open_cash_register, function() { open_cash_register(); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if(pos_settings.show_list_produts != '') { shortcut.add(pos_settings.show_list_produts, function() { window.open(site.base_url+'/products', '_blank'); }, { 'type':'keydown', 'propagate':false, 'target':document} ); }
if (site.settings.set_focus != 1) {
    $(document).ready(function(){ $('#add_item').focus(); });
}
var sended = false;
$('#paymentModal').on('keypress', function(e){
    submit = $('.submit-sale').prop('disabled');
    lock_submit = JSON.parse(localStorage.getItem('lock_submit'));
    lock_submit_by_margin = JSON.parse(localStorage.getItem('lock_submit_by_margin'));
    lock_submit_by_min_sale_amount = JSON.parse(localStorage.getItem('lock_submit_by_min_sale_amount'));
    if (e.keyCode == 13 && !submit && !sended && lock_submit == false && lock_submit_by_margin == false && lock_submit_by_min_sale_amount == false) {
        sended = true;
        setTimeout(function() {
            $('.send-submit-sale').focus();
        }, 800);
        bootbox.confirm({
            message: "¿Está seguro que desea finalizar la venta?",
            buttons: {
                confirm: {
                    label: 'Si, finalizar',
                    className: 'btn-success btn-full send-submit-sale'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger btn-full btn-outline'
                }
            },
            callback: function (result) {
                if (result) {
                    $('.submit-sale').click();
                } else {
                    sended = false;
                }
            }
        });
    }
});
function cancel_ordered_product(item_id)
{
    // Esta funcion solo es invocada cuando se carga una venta suspendida
    if (session_user_data.edit_right == 0) { // Si el usuario no tiene permisos de edición
        header_alert('warning', 'No tiene permisos para editar este pedido');
        return;
    }
    $('#state_readiness'+item_id).val('3');
    positems[item_id].state_readiness = 3;
    localStorage.setItem('positems', JSON.stringify(positems));
    loadItems();
}
function hide_invoice_footer_items_values(show = false){
    $('.order_detail > td.text-right > span').each(function(index, span){
        // val = formatDecimal($(span).text());
        // if (show == false) {
        //     if (val < 1) {
        //         $('.order_detail').eq(index).fadeOut();
        //     }
        // } else {
        //     if (val > 0) {
        //         $('.order_detail').eq(index).fadeIn();
        //     }
        // }
    });
}
$(document).on('click', '.delete_tip_amount', function(){
    itip = $(this).find('i.fa');
    if (itip.hasClass('fa-times')) {
        itip.removeClass('fa-times');
        itip.addClass('fa-refresh');
        localStorage.setItem('delete_tip_amount', 1);
        localStorage.removeItem('tip_amount');
        loadItems();
    } else if (itip.hasClass('fa-refresh')) {
        itip.removeClass('fa-refresh');
        itip.addClass('fa-times');
        localStorage.removeItem('delete_tip_amount');
        loadItems();
    }
});
var its_customer_birthday = false;
var birthday_year_applied = false;
function customer_retentions(){
    var customer_validate_min_base_retention = false;
    $.ajax({
        url : site.base_url+"customers/get_customer_by_id/"+$('#poscustomer').val(),
        dataType : "JSON"
    }).done(function(data){
        $('#birthday_year_applied').val('');
        its_customer_birthday = data.apply_birthday_discount;
        if (its_customer_birthday) {
            birthday_year_applied = data.birthday_year_applied;
            header_alert('success', '¡Es cumpleaños del cliente!, se aplicarán descuentos configurados a productos');
        }
        customer_data = data;
        var key_log = localStorage.getItem('poskeylog');
        if (data.customer_validate_min_base_retention == 0) {
            customer_validate_min_base_retention = true;
        }
        localStorage.setItem('customer_validate_min_base_retention', customer_validate_min_base_retention);
        setTimeout(function() {
            prev_retention = JSON.parse(localStorage.getItem('posretenciones'));
            if (prev_retention === null || (prev_retention !== null && (prev_retention.total_rete_amount === null || prev_retention.total_rete_amount == 0))) {
                $('#cancelOrderRete').trigger('click');
                if((data.default_rete_fuente_id || data.default_iva_fuente_id || data.default_ica_fuente_id || data.default_other_fuente_id) && key_log == 1) {
                    var posretenciones = {
                        'gtotal' : $('#gtotal').text(),
                        'id_rete_fuente' : data.default_rete_fuente_id,
                        'id_rete_iva' : data.default_rete_iva_id,
                        'id_rete_ica' : data.default_rete_ica_id,
                        'id_rete_otros' : data.default_rete_other_id,
                        };
                    localStorage.setItem('posretenciones', JSON.stringify(posretenciones));
                    recalcular_posretenciones();
                }
            }
        }, 1600);
    });
}
function validate_key_log(){
    var documnet_type_id = $('#doc_type_id option:selected').data('kl');
    localStorage.removeItem('poskeylog');
    localStorage.setItem('poskeylog', documnet_type_id);
}
$(document).on('change', '#payment_reference_no', function(){
    var val = $(this).val();
    $('#payment_document_type_id').val(val);
});

function validate_fe_customer_data()
{
    var is_fe = $('#doc_type_id option:selected').data('fe');
    var customer_id = $('#poscustomer').val();
    if (is_fe) {
        $.ajax({
            url : site.base_url+"pos/validate_fe_customer_data/"+customer_id
        }).done(function(data){
            if (data != '') {
                localStorage.setItem('locked_for_customer_fe_data', 1);
                $('.txt-error').html(data).css('display', '');

                command: toastr.error(data, 'Mensaje de validación', { onHidden : function(){} });
                verify_locked_submit();
            } else if (data == ''){
                if (localStorage.getItem('locked_for_customer_fe_data')) {
                    localStorage.removeItem('locked_for_customer_fe_data');
                }
                $('.txt-error').html('').css('display', 'none');
                verify_locked_submit();
            }
        }).fail(function (data){ console.log(data.responseText); });
    } else {
        if (localStorage.getItem('locked_for_customer_fe_data')) {
            localStorage.removeItem('locked_for_customer_fe_data');
        }
        $('.txt-error').html('').css('display', 'none');
        verify_locked_submit();
    }
}

function verify_locked_submit(){
    clearTimeout(time_for_unlock);
    if (localStorage.getItem('locked_for_customer_fe_data') || localStorage.getItem('locked_for_biller_fe_data') || localStorage.getItem('locked_for_customer_credit_limit')) {
        $('#payment').attr('disabled', true);
        $('.submit-sale').fadeOut();
    } else {
        time_for_unlock = setTimeout(function() {
            $('#payment').attr('disabled', false);
            $('.submit-sale').fadeIn();
        }, 1600);
    }
}

function customer_special_discount(){
    //TAMBIÉN SE CONSULTA EL BALANCE DE ANTICIPOS
    var customer_id = $('#poscustomer').val();
    $.ajax({
        url : site.base_url+"customers/get_customer_special_discount/"+customer_id,
        dataType : "JSON"
    }).done(function(data){

        localStorage.setItem('poscustomerspecialdiscount', data.special_discount);
        if (data.special_discount == 1) {
                loadItems();
                command: toastr.warning('Cliente con descuento especial asignado', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        }
        localStorage.removeItem('poscustomerdepositamount');
        if (data.deposit_amount > 0) {
            localStorage.setItem('poscustomerdepositamount', parseFloat(data.deposit_amount));
        }
        localStorage.removeItem('poscustomergiftcardbalance');
        localStorage.removeItem('poscustomergiftcards');
        if (data.giftcard_balance > 0) {
            localStorage.setItem('poscustomergiftcardbalance', parseFloat(data.giftcard_balance));
            localStorage.setItem('poscustomergiftcards', JSON.stringify(data.gift_cards));
        }
        if (data.tax_exempt_customer == 1) {
            $('#staffnote').val(lang.tax_exempt_customer_text);
        }
    });

}

$(document).on('click', '#pos_keep_prices', function(){
    localStorage.setItem('pos_keep_prices', true);
    localStorage.setItem('pos_keep_prices_quote_id', $('#quote_id').val());
    localStorage.removeItem('prev_poscustomer');
    location.reload();
});

$(document).on('click', '#update_prices', function(){
    localStorage.setItem('pos_keep_prices', false);
    localStorage.setItem('pos_keep_prices_quote_id', $('#quote_id').val());
    localStorage.removeItem('prev_poscustomer');
    location.reload();
});

function enable_poscustomer_change(){
    var nst = $('#poscustomer').prop('readonly') ? false : true;
    $('#poscustomer').select2("readonly", nst);
    return false;
}
///SERIAL MODAL
$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});
$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});
function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        option_id = $('#serialModal_option_id').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.quantity = meters;
        item.row.base_quantity = meters;
        item.row.delivered_qty = 0;
        item.row.pending_qty = meters;
        item.row.bill_qty = 0;
        item.row.option = option_id;
        add_invoice_item(item, true, cont);
    }
}
$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});
$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});
$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});
function validate_serial(serialModal, e = null){
    if ( e == null || e != null && e.keyCode == 13) {
        poswarehouse = $('#poswarehouse').val();
        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        $.ajax({
            url : site.base_url + 'products/get_product_serial_variant_quantity/'+serial_item.row.id+'/'+serialModal+'/'+poswarehouse,
            dataType : 'JSON',
        }).done(function(data){
            if (data.id  !== undefined) {
                $('#serialModal_meters').val(data.quantity);
                $('#serialModal_meters').prop('max', data.quantity);
                $('#serialModal_option_id').val(data.option_id);
                $('#serialModal_meters').focus();
            } else {
                $('#serialModal_serial').val('');
                $('#serialModal_option_id').val('');
                 command: toastr.warning('El serial indicado no está registrado para este producto', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        });
    }
}
$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});
function clean_localstorage(){
    if (localStorage.getItem('positems')) {
        localStorage.removeItem('positems');
    }
    if (localStorage.getItem('posdiscount')) {
        localStorage.removeItem('posdiscount');
    }
    if (localStorage.getItem('postax2')) {
        localStorage.removeItem('postax2');
    }
    if (localStorage.getItem('posshipping')) {
        localStorage.removeItem('posshipping');
    }
    if (localStorage.getItem('posref')) {
        localStorage.removeItem('posref');
    }
    if (localStorage.getItem('poswarehouse')) {
        localStorage.removeItem('poswarehouse');
    }
    if (localStorage.getItem('posnote')) {
        localStorage.removeItem('posnote');
    }
    if (localStorage.getItem('posinnote')) {
        localStorage.removeItem('posinnote');
    }
    if (localStorage.getItem('poscustomer')) {
        localStorage.removeItem('poscustomer');
    }
    if (localStorage.getItem('poscurrency')) {
        localStorage.removeItem('poscurrency');
    }
    if (localStorage.getItem('posdate')) {
        localStorage.removeItem('posdate');
    }
    if (localStorage.getItem('posstatus')) {
        localStorage.removeItem('posstatus');
    }
    if (localStorage.getItem('posbiller')) {
        localStorage.removeItem('posbiller');
    }
    if (localStorage.getItem('posseller')) {
        localStorage.removeItem('posseller');
    }
    if (localStorage.getItem('posretenciones')) {
        localStorage.removeItem('posretenciones');
    }
    if (localStorage.getItem('restobar_mode_module')) {
        localStorage.removeItem('restobar_mode_module');
    }
    if (localStorage.getItem('tip_amount')) {
        localStorage.removeItem('tip_amount');
    }
    if (localStorage.getItem('delete_tip_amount')) {
        localStorage.removeItem('delete_tip_amount');
    }
    if (localStorage.getItem('poskeylog')) {
        localStorage.removeItem('poskeylog');
    }
    if (localStorage.getItem('poscustomerspecialdiscount')) {
        localStorage.removeItem('poscustomerspecialdiscount');
    }
    if (localStorage.getItem('pos_keep_prices')) {
        localStorage.removeItem('pos_keep_prices');
    }
    if (localStorage.getItem('pos_keep_prices_quote_id')) {
        localStorage.removeItem('pos_keep_prices_quote_id');
    }
    if (localStorage.getItem('restobar_mode_module')) {
        localStorage.removeItem('restobar_mode_module');
    }
    if (localStorage.getItem('locked_for_biller_fe_data')) {
        localStorage.removeItem('locked_for_biller_fe_data');
    }
}

// JS posretenciones


    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            reset_rete_bomberil();
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    $(document).on('click', '#rete_bomberil', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/BOMB"
            }).done(function(data){
                $('#rete_bomberil_option').html(data);
                $('#rete_bomberil_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_bomberil_option').select2('val', '').attr('disabled', true).select();
            $('#rete_bomberil_tax').val('');
            $('#rete_bomberil_valor').val('');
            setReteTotalAmount(rete_bomberil_amount, '-');
        }
    });

    $(document).on('click', '#rete_autoaviso', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/TABL"
            }).done(function(data){
                $('#rete_autoaviso_option').html(data);
                $('#rete_autoaviso_option').attr('disabled', false).select();
            }).fail(function(data){
                // console.log(data);
            });
        } else {
            $('#rete_autoaviso_option').select2('val', '').attr('disabled', true).select();
            $('#rete_autoaviso_tax').val('');
            $('#rete_autoaviso_valor').val('');
            setReteTotalAmount(rete_autoaviso_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;
    var rete_bomberil_amount = 0;
    var rete_autoaviso_amount = 0;

    $(document).on('change', '#rete_fuente_option, #rete_iva_option, #rete_ica_option, #rete_otros_option, #rete_bomberil_option, #rete_autoaviso_option', function(){
        rete_sale_option_changed($(this).prop('id'), getTrmRate());
    });

    function getReteAmount(apply){
        amount = 0;
        if (apply == "ST") {
            amount = $('#subtotal').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#total').text();
        } else if (apply == "IC") {
            amount = $('#rete_ica_valor').val();
        }
        return amount;
    }

    function setReteTotalAmount(amount, action){
        // // console.log('Monto : '+amount+' acción : '+action);
        rtotal_rete =
                    formatDecimal($('#rete_fuente_valor').val()) +
                    formatDecimal($('#rete_iva_valor').val()) +
                    formatDecimal($('#rete_ica_valor').val()) +
                    formatDecimal($('#rete_otros_valor').val())+
                    formatDecimal($('#rete_bomberil_valor').val())+
                    formatDecimal($('#rete_autoaviso_valor').val())
                    ;
        trmrate = getTrmRate();
        // $('#total_rete_amount_show').text(formatMoney(trmrate * rtotal_rete));
        $('#total_rete_amount').text(formatMoney(rtotal_rete));
    }


// JS posretenciones

    $(document).on('click', '#updateOrderRete', function () {
        $('#add_sale').prop('disabled', true);
        $('#ajaxCall').fadeIn();
        rete_prev = JSON.parse(localStorage.getItem('posretenciones'));
        var posretenciones = {
            'gtotal' : (rete_prev && rete_prev.gtotal > 0 ? rete_prev.gtotal : parseFloat(formatDecimal($('#gtotal').text()))),
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            'bomberil_option' : $('#rete_bomberil_option option:selected').data('percentage'),
            'id_rete_bomberil' : $('#rete_bomberil_option').val(),
            'autoaviso_option' : $('#rete_autoaviso_option option:selected').data('percentage'),
            'id_rete_autoaviso' : $('#rete_autoaviso_option').val(),
            };
        posretenciones.total_discounted = formatMoney(parseFloat(formatDecimal((rete_prev && rete_prev.gtotal > 0 ? rete_prev.gtotal : parseFloat(formatDecimal($('#gtotal').text()))))) - parseFloat(formatDecimal($('#total_rete_amount').text())));

        setTimeout(function() {
            $('#gtotal').text(posretenciones.total_discounted);
            if ($('#pospayment_status').val() == 'partial') {
                $('#amount_1').val(parseFloat(formatDecimal(posretenciones.total_discounted)));
            }
            $('#ajaxCall').fadeOut();
            verify_locked_submit();
            $('#paid_by_1').trigger('change');
        }, 1000);
        localStorage.setItem('posretenciones', JSON.stringify(posretenciones));
        if (parseFloat(formatDecimal(posretenciones.total_rete_amount)) > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(posretenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }
        $('#rete').val(posretenciones.total_rete_amount);
        $('#rtModal').modal('hide');
    });

    $('#rtModal').on('hidden.bs.modal', function () {
        // loadItems();
    });

     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('posretenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#rete_bomberil').prop('checked', true).trigger('click');
        $('#rete_autoaviso').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });

    function recalcular_posretenciones(){
        posretenciones = JSON.parse(localStorage.getItem('posretenciones'));
        if (posretenciones != null) {
            if (posretenciones.id_rete_fuente > 0) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').trigger('click');
                }
            }
            if (posretenciones.id_rete_iva > 0) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').trigger('click');
                }
            }
            if (posretenciones.id_rete_ica > 0) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').trigger('click');
                }
            }
            if (posretenciones.id_rete_otros > 0) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').trigger('click');
                }
            }
            if (posretenciones.id_rete_bomberil > 0) {
                if (!$('#rete_bomberil').is(':checked')) {
                    $('#rete_bomberil').trigger('click');
                }
            }
            if (posretenciones.id_rete_autoaviso > 0) {
                if (!$('#rete_autoaviso').is(':checked')) {
                    $('#rete_autoaviso').trigger('click');
                }
            }
            setTimeout(function() {
                $.each($('#rete_fuente_option option'), function(index, value){
                    if(posretenciones.id_rete_fuente != '' && value.value == posretenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_iva_option option'), function(index, value){
                    if(posretenciones.id_rete_iva != '' && value.value == posretenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_ica_option option'), function(index, value){
                    if(posretenciones.id_rete_ica != '' && value.value == posretenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_otros_option option'), function(index, value){
                    if(posretenciones.id_rete_otros != '' && value.value == posretenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_bomberil_option option'), function(index, value){
                    if(posretenciones.id_rete_bomberil != '' && value.value == posretenciones.id_rete_bomberil){
                        $('#rete_bomberil_option').select2('val', value.value).trigger('change');
                    }
                });
                $.each($('#rete_autoaviso_option option'), function(index, value){
                    if(posretenciones.id_rete_autoaviso != '' && value.value == posretenciones.id_rete_autoaviso){
                        $('#rete_autoaviso_option').select2('val', value.value).trigger('change');
                    }
                });
                $('#updateOrderRete').trigger('click');
            }, 2000);
        }
    }

    if ($('#rete_applied').val()) {
      total_rete_amount = $('#total_rete_amount').text();
      total_rete_amount = parseFloat(formatDecimal(total_rete_amount));
    } else {
      total_rete_amount = 0;
    }

// JS posretenciones

function getTrmRate(){
    trmrate = 1;

    if (othercurrency = localStorage.getItem('othercurrency')) {
        othercurrencytrm = localStorage.getItem('othercurrencytrm');
        othercurrencyrate = localStorage.getItem('othercurrencyrate');
        othercurrencycode = localStorage.getItem('othercurrencycode');

        if (othercurrencytrm !== null && othercurrencytrm !== undefined) {
            trmrate = othercurrencyrate / othercurrencytrm;
        }
    }

    return trmrate;
}

function reset_rete_bomberil(){
    $('#rete_bomberil').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_autoaviso').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_bomberil_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_autoaviso_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_bomberil_tax').val('');
    $('#rete_bomberil_valor').val('');
    $('#rete_autoaviso_tax').val('');
    $('#rete_autoaviso_valor').val('');
}

function get_biller_code(){

}

function set_customer_payment(){
    var customer_id = $('#poscustomer').val();
    $('#pospayment_status').select2('val', 'pending').trigger('change');
    $('#pospayment_term').val('');
    localStorage.removeItem('pos_customer_payment');
    localStorage.removeItem('locked_for_customer_credit_limit');
    if (is_quote_paid_by) {
        $('#paid_by_1').select2('val', is_quote_paid_by).trigger('change');
        $('#paid_by_1').select2('readonly', false);
    } else {
        $.ajax({
            url : site.base_url+"customers/get_payment_type/"+customer_id,
            dataType : "JSON"
        }).done(function(data){
            setTimeout(function() {
                if (data.payment_type == 0) {
                    localStorage.setItem('pos_customer_payment', JSON.stringify(data));
                    $('#pospayment_status').select2('val', 'pending').trigger('change');
                    $('#paid_by_1').val('Credito').trigger('change');
                    $('#payment_term').val(data.payment_term > 0 ? data.payment_term : 1);
                    $('.payment_term_val').val(data.payment_term > 0 ? data.payment_term : 1);
                    if (site.settings.control_customer_credit == 1) {
                        if (data.term_expired == 1) {
                            header_alert('error', 'Cliente con plazo de pago expirado en facturas, no se puede facturar');
                            localStorage.setItem('locked_for_customer_credit_limit', 1);
                        }
                        verify_locked_submit();
                    }
                } else {
                    if ($('.paid_by option[value="cash"]:not([disabled])').length == 0) {
                        $('.paid_by').select2('val', $('.paid_by option:not([disabled])').eq(1).val()).trigger('change');
                    } else {
                        $('#paid_by_1').select2('val', 'cash').trigger('change');
                    }
                    localStorage.removeItem('pos_customer_payment');
                }
                $('#paid_by_1').select2('readonly', false);
            }, 1200);
        }).fail(function(data){
            $('#paid_by_1').select2('val', 'cash').trigger('change');
        });
    }
        
}

function set_biller_warehouses_related(){
    warehouses_related = billers_data[$('#posbiller').val()].warehouses_related;
    if (warehouses_related) {
        $('#poswarehouse option').each(function(index, option){
            $(option).prop('disabled', false);
            if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                $(option).prop('disabled', true);
            }
        });
        setTimeout(function() {
            $('#poswarehouse').select2();
        }, 850);
    }
}

$(document).on('click', '.send_product_gift_card_data', function(){
    if ($('.product_gift_card_form').valid()) {
        var product_id_gift_card = $('#product_id_gift_card').val();
        positems = localStorage.getItem('positems') ? JSON.parse(localStorage.getItem('positems')) : [];
        positems[product_id_gift_card].gift_card = {
                            gc_card_no : $('#card_no').val(),
                            gc_value : $('#gc_value').val(),
                            gc_expiry : $('#gc_expiry').val(),
                        };
        positems[product_id_gift_card].row.price = formatDecimal($('#gc_value').val());
        positems[product_id_gift_card].row.unit_price = formatDecimal($('#gc_value').val());
        positems[product_id_gift_card].row.real_unit_price = formatDecimal($('#gc_value').val());
        localStorage.setItem('positems', JSON.stringify(positems));
        loadItems();
        $('#gc_modal').modal('hide');
    }
});

function set_giftcard_payments(gtotal){
    gc_balance = formatDecimal(localStorage.getItem('poscustomergiftcardbalance'));
    gc_data = JSON.parse(localStorage.getItem('poscustomergiftcards'));
    var gtotal_balance = gtotal;
    var pnum = 0;
    $('#paid_by_1').select2('val', 'cash').trigger('change');
    $.each(gc_data, function(index, gc){
        if (gtotal_balance > 0 && (gc.balance > gtotal_balance || gc.balance <= gtotal_balance)) {
            pnum++;
            if (pnum >= 2) {
                $('#paid_by_1').select2('destroy');
                add_payment_input(false);
            }
            $('#paid_by_'+pnum).select2('val', 'gift_card').trigger('change');
            $('#gift_card_no_'+pnum).val(gc.card_no).trigger('change');
            $('#amount_'+pnum).val((gc.balance > gtotal_balance) ? gtotal_balance : gc.balance).trigger('change');
            gtotal_balance -= gc.balance;
        }
        if ((index+1) == Object.keys(gc_data).length) {
            if (gtotal_balance > 0) {
                setTimeout(function() {
                    $('#paid_by_1').select2('destroy');
                    add_payment_input(false);
                    pnum++;
                    $('#amount_'+pnum).val(gtotal_balance).trigger('change');
                    $('#paid_by_'+pnum).val('cash').trigger('change');
                }, 1000);
            }
        }
    });
}

function focus_last_item_quantity() {
    if (site.settings.product_order == 1) {
        $('[tabindex='+($('.rquantity').length+1)+']').focus().select();
    } else {
        $('[tabindex='+2+']').focus().select();
    }
}


function hide_menu_sobre_productos(){
    if ($('#favorites-slider').is(':visible')) {
        $('#favorites-slider').toggle('slide', { direction: 'up' }, 700);
    }
    if ($('#brands-slider').is(':visible')) {
        $('#brands-slider').toggle('slide', { direction: 'up' }, 700);
    }
    if ($('#category-slider').is(':visible')) {
        $('#category-slider').toggle('slide', { direction: 'up' }, 700);
    }
    if ($('#subcategory-slider').is(':visible')) {
        $('#subcategory-slider').toggle('slide', { direction: 'up' }, 700);
    }
    if ($('#promotions-slider').is(':visible')) {
        $('#promotions-slider').toggle('slide', { direction: 'up' }, 700);
    }
    if ($('#featured-slider').is(':visible')) {
        $('#featured-slider').toggle('slide', { direction: 'up' }, 700);
    }
}

function set_submit_text(){
    $.each($('.submit-sale'), function(index, button){
        if ($(button).data('submittarget') == 1) {
            $(button).html(lang.finish_sale_print+' <i class="fas fa-print"></i>').attr('disabled', false).fadeIn();
        } else if ($(button).data('submittarget') == 2) {
            $(button).html(lang.finish_sale_no_print+' <i class="fas fa-angle-double-right"></i>').attr('disabled', false).fadeIn();
        } else {
            $(button).html(lang.finish_sale).attr('disabled', false).fadeIn();
        }
    });
}

function open_cash_register(){
    var newWin=window.open('','Print-Window');
    newWin.document.open();
    newWin.document.write('<body onload="window.print()" style="height:auto !important; min-height:50% !important;"></body>');
    newWin.document.close();
    setTimeout(function(){newWin.close();},1000);
}

function updateQuantityItems(warehouseId)
{
    let items = localStorage.getItem('positems')
    if (items) {
        items = JSON.parse(items)
        $.each(items, function () {
            productId = this.item_id
            options = this.options
            if (options) {
                $.each(options, function() {
                    this.quantity = getQuantitiesProductInWarehouse(warehouseId, productId, this.id)
                })
            } else {
                this.row.quantity = getQuantitiesProductInWarehouse(warehouseId, productId)
            }
        })
    }
    localStorage.setItem('positems', JSON.stringify(items));
    loadItems()
}

function getQuantitiesProductInWarehouse(warehouseId, productId, optionId = null)
{
    let quantity = 0
    $.ajax({
        type: "get",
        async: false,
        url: site.base_url + 'products/getQuantitiesProductInWarehouseAjax',
        data: {
            'warehouseId' : warehouseId,
            'productId' : productId,
            'optionId' : optionId,
        },
        dataType: "json",
        success: function (respuesta) {
            quantity = respuesta.quantity;
        }
    });
    return quantity;
}

function resizeWindow(){
    // .row_mobile -> flex-direction: column;
    // .input_mobile -> aplica 100% de ancho al campo
    // .div_hidden_movile -> oculta el div del input si dicho input contiene solo una opcion de seleccion
    if (user_permissions) {
        if ($(window).width() < 800) {
            $('#row_form_1').addClass('row_mobile');
            /**sucursal **/
            $('#div_posbiller').addClass('input_mobile');
            let count_billers = $('#posbiller').find('option').length;
            if (count_billers == 1) {
                $('#div_posbiller').addClass('div_hidden_movile');
            }
            /**sucursal **/

            /** document type **/
            $('#div_doc_type').addClass('input_mobile');
            let count_document_type = $('#doc_type_id').find('option').length;
            if (count_document_type == 1) {
                $('#div_doc_type').addClass('div_hidden_movile');
            }
            /** document type **/

            /** warehouse **/
            $('#div_warehouse').addClass('input_mobile');
            let count_warehouse = $('#poswarehouse').find('option').length;
            if (count_warehouse == 1) {
                $('#div_warehouse').addClass('div_hidden_movile');
            }
            /** warehouse **/

            $('#row_form_2').addClass('row_mobile');
            $("#div_customer").addClass('input_mobile'); /** en el caso del cliente si siempre se va a mostrar **/

            /** customer branch **/
            $("#div_customer_branch").addClass('input_mobile');
            let count_customer_branch = $('#poscustomer_branch').find('option').filter(function() {
                return $(this).val() !== ''; // Filtra las opciones que no tienen valor vacío
            }).length;
            if (count_customer_branch == 1) {
                $('#div_customer_branch').addClass('div_hidden_movile');
            }
            /** customer branch **/

            /** seller **/
            $("#div_seller").addClass('input_mobile');
            let sellerElement = $('#posseller');
            if (sellerElement.is('input')) {
                let validateSeller = $('#posseller').val();
                if (validateSeller != '') {
                    $('#div_seller').addClass('div_hidden_movile');
                }
            }
            else if(sellerElement.is('select')){
                let count_seller = $('#posseller').find('option').filter(function() {
                    return $(this).val() !== ''; // Filtra las opciones que no tienen valor vacío
                }).length;
                if (count_seller == 1) {
                    $('#div_seller').addClass('div_hidden_movile');
                }
            }
            /** seller **/

            $('#add_item').addClass('btn-pos-product_movil');
            $('#left-middle').addClass('div_container_item_movile');
            $('div#product-list.ps-container').css('min-height', '250px !important');
            $('.title_payment').removeClass('text-right');
            $('#sale_note').removeClass('text-right');
            $('.quick-cash').css('font-size','150%');
        } else {
            $('#row_form_1').removeClass('row_mobile');
            /** sucursal **/
            $('#div_posbiller').removeClass('input_mobile'); // clase para tomar todo el ancho de la pantalla
            $('#div_posbiller').removeClass('div_hidden_movile');  // clase para ocultar si solo existe uno
            /** sucursal **/

            /** document type **/
            $('#div_doc_type').removeClass('input_mobile');
            $('#div_doc_type').removeClass('div_hidden_movile');
            /** document type **/

            /** warehouse **/
            $('#div_warehouse').removeClass('input_mobile');
            $('#div_warehouse').removeClass('div_hidden_movile');
            /** warehouse **/

            $('#row_form_2').removeClass('row_mobile');
            $("#div_customer").removeClass('input_mobile');

            /** customer branch **/
            $("#div_customer_branch").removeClass('input_mobile');
            $("#div_customer_branch").removeClass('div_hidden_movile');
            /** customer branch **/

            /** customer branch **/
            $("#div_seller").removeClass('input_mobile');
            $("#div_seller").removeClass('div_hidden_movile');
            /** customer branch **/

            $('#add_item').removeClass('btn-pos-product_movil');
            $('#left-middle').removeClass('div_container_item_movile');
            $('#product-list').removeClass('div_container_item_movile');
        }
    }
}

function update_current_gram_price(current_price_gram, type){
    swal({
        title: "¡Actualización disponible!",
        text: "Se ha encontrado una actualización al valor del gramo. ¿Desea actualizar el precio de los productos seleccionados?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    }, function () {
        positems = JSON.parse(localStorage.getItem('positems'));
        for (const key in positems) {
            if (positems.hasOwnProperty(key)) {
                const innerObject = positems[key];
                for (const keyRow in innerObject) {
                    if (keyRow == 'row') {
                        if (innerObject.hasOwnProperty(keyRow)) {
                            const innerObjectRow = innerObject[keyRow];
                            for (const keyRowData in innerObjectRow) {
                                if (innerObjectRow.hasOwnProperty(keyRowData)) {
                                    const innerObjectRowData = innerObjectRow[keyRowData];
                                    if (keyRowData == 'based_on_gram_value') {
                                        if (positems[key][keyRow][keyRowData] == type) {
                                            positems[key][keyRow]['current_price_gram'] = current_price_gram;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        localStorage.setItem('positems', JSON.stringify(positems));
        loadItems();
    });
    localStorage.setItem('gram_current_price', current_price_gram);
}

function table_change() {
    let restobarModeModule  = JSON.parse(localStorage.getItem('restobar_mode_module'))
    let suspendSaleId       = $('#suspend_sale_id').val();
    let tableId             = $('#table_id').val();

    $('#myModal').modal({ remote: `${site.base_url}/pos/restobar_table_change/${suspendSaleId}`});
    $('#myModal').modal('show');
}

function validate_price_minimun(product_id){
    let price = false;
    $.ajax({
        type: "get",
        url : site.base_url + "pos/get_price_minimun/"+product_id,
        dataType: "json",
        async : false,
    })
    .done(function(data){
        price = data.price;
        price;
    });
    return price;
}

$(document).on('change', '.paid_by', function(){
    check_customer_credit_limit();
});

$(document).on('change', '.amount', function(){
    check_customer_credit_limit();
});

function check_customer_credit_limit(){
    if (site.settings.control_customer_credit == 1) {
        if (pos_customer_payment = JSON.parse(localStorage.getItem('pos_customer_payment'))) {
            if (pos_customer_payment.payment_type == 0) {
                pb_total_credit_to_pay = 0;
                $.each($('select.paid_by'), function(index, pb_paid_by){
                    pb_payment = $(pb_paid_by).closest('.row_payment');
                    pb_amount_to_pay = pb_payment.find('.amount').val();
                    if ($(pb_paid_by).val() != 'Credito') {
                        pb_total_credit_to_pay += parseFloat(pb_amount_to_pay);
                    }
                });
                pb_total_credit_amount = formatDecimal($('#twt').text()) - pb_total_credit_to_pay;
                if (parseFloat(pb_total_credit_amount) > parseFloat(pos_customer_payment.credit_limit)) {
                    header_alert('error', 'El valor crédito ('+formatMoney(pb_total_credit_amount)+') supera el límite de crédito definido al cliente ('+formatMoney(pos_customer_payment.credit_limit)+')');
                    localStorage.setItem('locked_for_customer_credit_limit', 1);
                    verify_locked_submit();
                } else {
                    localStorage.removeItem('locked_for_customer_credit_limit');
                    verify_locked_submit();
                }
            }
        }
    } 
}