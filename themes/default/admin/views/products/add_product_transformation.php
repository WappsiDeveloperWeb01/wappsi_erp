<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    $(document).ready(function () {
        if (localStorage.getItem('remove_ptrls')) {
            if (localStorage.getItem('ptritems')) {
                localStorage.removeItem('ptritems');
            }
            if (localStorage.getItem('ptrref')) {
                localStorage.removeItem('ptrref');
            }
            if (localStorage.getItem('ptrwarehouse')) {
                localStorage.removeItem('ptrwarehouse');
            }
            if (localStorage.getItem('ptrnote')) {
                localStorage.removeItem('ptrnote');
            }
            if (localStorage.getItem('ptrdate')) {
                localStorage.removeItem('ptrdate');
            }
            if (localStorage.getItem('trnf_companies_id')) {
                localStorage.removeItem('trnf_companies_id');
            }
            if (localStorage.getItem('trnf_document_type_id')) {
                localStorage.removeItem('trnf_document_type_id');
            }
            if (localStorage.getItem('reference_restricted')) {
                localStorage.removeItem('reference_restricted');
            }
            localStorage.removeItem('remove_ptrls');
        }
        <?php if ($adjustment_items) { ?>
        localStorage.setItem('ptritems', JSON.stringify(<?= $adjustment_items; ?>));
        <?php } ?>
        <?php if ($warehouse_id) { ?>
            localStorage.setItem('ptrwarehouse', '<?= $warehouse_id; ?>');
            $('#ptrwarehouseorigin').select2('readonly', true);
        <?php } ?>

        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('ptrdate')) {
            $("#ptrdate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#ptrdate', function (e) {
            localStorage.setItem('ptrdate', $(this).val());
        });
        <?php } ?>
        $("#add_item_origin").autocomplete({
            source: function (request, response) {
                if (!$('#companies_id').val() || !$('#document_type_id').val()) {
                    var msg = "";
                    if (!$('#companies_id').val()) {
                        msg = "</br><?= lang('employee') ?>";
                    }
                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }
                    $('#add_item_origin').val('').removeClass('ui-autocomplete-loading');
                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#add_item_origin').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('products/ptr_suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#ptrwarehouseorigin").val(),
                        biller_id: $("#ptrbiller").val(),
                        destination_biller_id: $("#ptrdestination_biller").val(),
                        search_type : 1
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item_origin').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item_origin").val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item_origin').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item_origin").val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    <?php if($this->Settings->overselling == 0) { ?>
                        // se va a entrar aca para manejar el evento de seleccion de un producto y validar las sobreventas en el caso que este parametrizado como No
                        let warehouseOriginCode = $('#ptrwarehouseorigin').val();
                        if (ui.item.row.wh_data.length == 0 || ui.item.row.wh_data[warehouseOriginCode].quantity <= 0) {  // si el stock en la tabla warehouse_products esta en 0 o no existe
                            $('#add_item_origin').val('').removeClass('ui-autocomplete-loading');
                            msg = "No puede agregar el producto " + ui.item.row.name + " debido a que no tiene inventario en la bodega seleccionada, parámetro de sobreventas desactivado.";
                            header_alert('warning', msg)
                            $('#add_item_origin').focus();
                            return false;
                        }
                    <?php } ?>

                    $.ajax({
                        url:"<?= admin_url('products/getProductAVGCostByWarehouse'); ?>/"+ui.item.item_id+"/"+$('#ptrwarehouseorigin').val(),
                    }).done(function(data){
                        if (data != false) {
                            ui.item.row.price = data;
                        }
                        var row = add_adjustment_origin_item(ui.item);
                        if (row){
                            $("#add_item_origin").val('');
                        }
                    }).fail(function(data){
                        console.log(data);
                    });
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $("#add_item_destination").autocomplete({
            source: function (request, response) {
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('products/ptr_suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        biller_id: $("#ptrbiller").val(),
                        destination_biller_id: $("#ptrdestination_biller").val(),
                        search_type : 2
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item_destination').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item_destination").val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item_destination').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item_destination").val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    $.ajax({
                        url:"<?= admin_url('products/getProductAVGCostByWarehouse'); ?>/"+ui.item.item_id+"/"+$('#ptrwarehouseorigin').val(),
                    }).done(function(data){
                        if (data != false) {
                            ui.item.row.price = data;
                        }
                        var row = add_adjustment_destination_item(ui.item);
                        if (row){
                            $("#add_item_destination").val('');
                        }
                    }).fail(function(data){
                        console.log(data);
                    });
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
        $('#ptrbiller').trigger('change');
        $('#ptrwarehouseorigin').change(function (e) {
            validateOversallingWarehouse($(this).val(), "<?= $this->Settings->overselling ?>");
        });

        $('#load_products').on('ifChanged', function(e){
            if ($(this).is(':checked')) {
                const from_warehouse = document.getElementById('ptrwarehouseorigin');
                load_warehouses_products(from_warehouse.value);
                validateOversallingWarehouse($('#ptrwarehouseorigin').val(), site.settings.overselling);
                loadItems();
            } 
        }); 
    });
</script>
<?php 
$show_cost = true;
if (!$this->Admin && !$this->Owner && !$this->GP['products-cost']) {
    $show_cost = false;
}
 ?>
 <script type="text/javascript">
     var show_cost = JSON.parse("<?= $show_cost ?>");
 </script>
<?= admin_form_open_multipart("products/add_product_transformation", ['id' => 'add_transformation']); ?>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <?php if ($Owner || $Admin) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("date", "ptrdate"); ?>
                                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : date('d/m/Y H:i')), 'class="form-control input-tip datetime" id="ptrdate" required="required"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                $bl[""] = "";
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                ?>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "ptrbiller"); ?>
                                            <select name="biller" class="form-control" id="ptrbiller" required="required">
                                                <?php foreach ($billers as $biller): ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"
                                                    <?= (isset($_POST['biller']) && $_POST['biller'] == $biller->id) ? 'selected' : ''  ?>
                                                    ><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "ptrbiller"); ?>
                                            <select name="biller" class="form-control" id="ptrbiller" readonly>
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>" selected><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                <?php } ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("destination_biller", "ptrdestination_biller"); ?>
                                        <select name="destination_biller" class="form-control" id="ptrdestination_biller" required="required">
                                            <?php foreach ($billers as $biller): ?>
                                                <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("reference_no", "document_type_id"); ?>
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                    </div>
                                </div>

                                <div class="col-md-4 form-group">
                                    <?= lang('employee', 'companies_id') ?>
                                    <?php
                                    $cOptions[''] = lang('select_third');
                                    if ($companies) {
                                        foreach ($companies as $row => $company) {
                                            $cOptions[$company->id] = $company->name;
                                        }
                                    }
                                        ?>
                                    <?= form_dropdown('companies_id', $cOptions, '',' id="companies_id" class="form-control select" required style="width:100%;"'); ?>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("adjustment_reference_no", "adjustment_document_type_id"); ?>
                                        <select name="adjustment_document_type_id" class="form-control" id="adjustment_document_type_id" required="required"></select>
                                    </div>
                                </div>
                                <?php if ($this->Settings->transformation_product_reference_restriction == 1): ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("restrict_by_product_reference", "restrict_by_product_reference"); ?>
                                            <select name="restrict_by_product_reference" class="form-control" id="restrict_by_product_reference" required="required">
                                                <option value="0"><?= lang('no') ?></option>
                                                <option value="1"><?= lang('yes') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4 div_restricted_reference" style="display:none;">
                                        <div class="form-group">
                                            <?= lang("restricted_reference", "restricted_reference"); ?>
                                            <input type="text" class="form-control" id="restricted_reference" readonly>
                                        </div>
                                    </div>
                                <?php else: ?>
                                    <select name="restrict_by_product_reference" id="restrict_by_product_reference" style="display: none;" >
                                        <option value="0" selected>No</option>
                                    </select>
                                <?php endif ?>


                                <?php if (isset($cost_centers)): ?>
                                    <div class="form-group col-md-4">
                                        <?= lang('cost_center', 'cost_center_id') ?>
                                        <?php
                                        $ccopts[''] = lang('select');
                                        foreach ($cost_centers as $cost_center) {
                                            $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                        }
                                            ?>
                                            <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('destination_cost_center', 'cost_center_id') ?>
                                        <?= form_dropdown('destination_cost_center_id', $ccopts, '', 'id="destination_cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= lang("warehouse", "ptrwarehouseorigin"); ?>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('ptrwarehouseorigin', $wh, (isset($_POST['ptrwarehouseorigin']) ? $_POST['ptrwarehouseorigin'] : ($warehouse_id ? $warehouse_id :$Settings->default_warehouse)), 'id="ptrwarehouseorigin" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <?= lang("warehouse", "ptrwarehouseorigin"); ?>
                                    <?php
                                    $wh[''] = '';
                                    foreach ($warehouses as $warehouse) {
                                        if ($this->session->userdata('warehouse_id') == $warehouse->id) {
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                    }
                                    echo form_dropdown('ptrwarehouseorigin', $wh, $this->session->userdata('warehouse_id'), 'id="ptrwarehouseorigin" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                    ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-12" id="sticker">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i>
                                        </div>
                                        <?php echo form_input('add_item_origin', '', 'class="form-control input-lg" id="add_item_origin" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <?php if($this->Settings->automatic_loading_of_existence_in_transformation): ?>
                            <div class="col-md-4">
                                <label for="load_products">
                                    <div class="checkbox">
                                        <input type="checkbox" id="load_products">
                                            <strong><?= lang('load_products') ?></strong> 
                                    </div>
                                </label>
                            </div>
                        <?php endif; ?>

                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("products_origin"); ?> *</label>

                                <div class="controls table-controls">
                                    <table id="ptrTableOrigin" class="table items  table-bordered table-condensed table-hover">
                                        <thead>
                                            <tr>
                                                <th style="width: 39.28%;"><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                <th style="width: 4.28%;"><?= lang("unit"); ?></th>
                                                <th style="width: 17.28%; <?= $show_cost ? '' : 'display:none;' ?>"><?= lang("unit_cost")." ".lang("with_tax"); ?></th>
                                                <th style="width: 10.28%;"><?= lang("type"); ?></th>
                                                <th style="width: 10.28%;"><?= lang("quantity"); ?></th>
                                                <th style="width: 14.28%; <?= $show_cost ? '' : 'display:none;' ?>"><?= lang("product_cost"); ?></th>
                                                <th style="width: 4.28%;">
                                                    <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("warehouse", "ptrwarehousedestination"); ?>
                                <?php
                                $wh[''] = '';
                                foreach ($warehouses as $warehouse) {
                                    $wh[$warehouse->id] = $warehouse->name;
                                }
                                echo form_dropdown('ptrwarehousedestination', $wh, (isset($_POST['ptrwarehousedestination']) ? $_POST['ptrwarehousedestination'] : ($warehouse_id ? $warehouse_id :$Settings->default_warehouse)), 'id="ptrwarehousedestination" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                ?>
                            </div>
                        </div>
                        <div class="col-md-12" id="sticker">
                            <div class="well well-sm">
                                <div class="form-group" style="margin-bottom:0;">
                                    <div class="input-group wide-tip">
                                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                            <i class="fa fa-2x fa-barcode addIcon"></i>
                                        </div>
                                        <?php echo form_input('add_item_destination', '', 'class="form-control input-lg" id="add_item_destination" placeholder="' . lang("add_product_to_order") . '"'); ?>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("products_destination"); ?> *</label>
                                <div class="controls table-controls">
                                    <table id="ptrTableDestination" class="table items  table-bordered table-condensed table-hover">
                                        <thead>
                                        <tr>
                                            <th style="width: 39.28%;"><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                            <th style="width: 4.28%;"><?= lang("unit"); ?></th>
                                            <th style="width: 17.28%;<?= $show_cost ? '' : 'display:none;' ?>"><?= lang("unit_cost")." ".lang("with_tax"); ?></th>
                                            <th style="width: 10.28%;"><?= lang("type"); ?></th>
                                            <th style="width: 10.28%;"><?= lang("quantity"); ?></th>
                                            <th style="width: 14.28%;<?= $show_cost ? '' : 'display:none;' ?>"><?= lang("product_cost"); ?></th>
                                            <th style="width: 4.28%;">
                                                <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody></tbody>
                                        <tfoot></tfoot>
                                    </table>
                                    <em class="bold text-danger cost_error" style="display: none;"><?= lang('total_costs_dont_match') ?></em>
                                    <br>
                                    <em class="bold text-danger quantity_error" style="display: none;"><?= lang('total_quantity_dont_match') ?></em>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-primary" id="equal_costs"><?= lang('equal_costs') ?></button>
                            <button type="button" class="btn btn-primary" id="reset_costs"><span class="fa fa-refresh"></span></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <?= lang("note", "ptrnote"); ?>
                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="ptrnote" style="margin-top: 10px; height: 100px;"'); ?>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="fprom-group">
                                <?php echo form_submit('add_adjustment', lang("submit"), 'class="btn btn-primary submit_form" style="display:none;"'); ?>
                                <button type="button" class="btn btn-primary" id="submit" name="add_adjustment"><?= lang("submit") ?></button>
                                <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo form_close(); ?>

<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <input type="hidden" id="serialModal_option_id">
                    <input type="hidden" id="serialModal_type">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="qtyFixModal" tabindex="-1" role="dialog" aria-labelledby="qtyFixModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_qtyFixModal"></h2>
                <h3><?= lang('set_quantity') ?></h3>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 form-group">
                    <?= lang('serial_no', 'serial') ?>
                    <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                </div>
                <div class="col-sm-12 form-group">
                    <?= lang('meters', 'meters') ?>
                    <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                </div>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">


    $('#ptrdestination_biller').on('change', function(){
        wh_default = $('#ptrdestination_biller option:selected').data('warehousedefault');
        $('#ptrwarehousedestination').select2('val', wh_default).trigger('change');
        localStorage.setItem('ptrdestination_biller', $(this).val());

        warehouses_related = billers_data[$('#ptrdestination_biller').val()].warehouses_related;
        if (warehouses_related) {
            $('#ptrwarehousedestination option').each(function(index, option){
                $(option).prop('disabled', false);
                if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                    $(option).prop('disabled', true);
                }
            });
            setTimeout(function() {
                $('#ptrwarehousedestination').select2();
            }, 850);
        }
    });

    $('#ptrbiller').on('change', function(){
        wh_default = $('#ptrbiller option:selected').data('warehousedefault');
        <?php if (!$this->session->userdata('warehouse_id')): ?>
            <?php if(!isset($_POST['ptrwarehouseorigin'])): ?>
                // $('#ptrwarehouseorigin').select2('val', wh_default).trigger('change');     
            <?php endif; ?>
            warehouses_related = billers_data[$('#ptrbiller').val()].warehouses_related;
            if (warehouses_related) {
                $('#ptrwarehouseorigin option').each(function(index, option){
                    $(option).prop('disabled', false);
                    if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                        $(option).prop('disabled', true);
                    }
                });
                setTimeout(function() {
                    $('#ptrwarehouseorigin').select2();
                }, 850);
            }
        <?php endif ?>
        localStorage.setItem('ptrbiller', $(this).val());
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/25/") ?>'+$('#ptrbiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/11/") ?>'+$('#ptrbiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#adjustment_document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#adjustment_document_type_id').trigger('change');
        });
    });
<?php if ($this->session->userdata('biller_id')): ?>
    setTimeout(function() {
        $('#ptrbiller').select2('readonly', true);
    }, 1000);
<?php endif ?>
<?php if ($this->session->userdata('warehouse_id')): ?>
    setTimeout(function() {
        $('#ptrwarehouseorigin').select2('readonly', true);
    }, 1000);
<?php endif ?>

    function validateOversallingWarehouse(warehouse, overselling){
        if (overselling == "0") {
            let ptrOriginitems = JSON.parse(localStorage.getItem('ptroriginitems')) || {};
            $.each(ptrOriginitems, function(clave, valor) {
                let whData = valor.row.wh_data;
                if (whData.length == 0) {
                    let objetoAEliminar = clave; 
                    delete ptrOriginitems[objetoAEliminar];
                    localStorage.setItem('ptroriginitems', JSON.stringify(ptrOriginitems));
                }
                if (whData[warehouse].quantity <= 0) {
                    let objetoAEliminar = clave; 
                    delete ptrOriginitems[objetoAEliminar];
                    localStorage.setItem('ptroriginitems', JSON.stringify(ptrOriginitems));   
                }
            });
        }
    }

    async function load_warehouses_products(warehouse_id){ 
        let ptrOriginitems = JSON.parse(localStorage.getItem('ptroriginitems')) || {};
        await   $.each(ptrOriginitems, function(clave, valor) {
                    let objetoAEliminar = clave; 
                    delete ptrOriginitems[objetoAEliminar];
                    localStorage.setItem('ptroriginitems', JSON.stringify(ptrOriginitems));
                });
        await loadItems();        
        $.ajax({
            url:'<?= admin_url("products/products_by_warehouse/") ?>',
            type:'GET',
            dataType:'JSON',
            async : false,
            data: {
                warehouse_id: warehouse_id,
                biller_id: $("#ptrbiller").val(),
                destination_biller_id: $("#ptrdestination_biller").val(),
                search_type : 1
            },
        }).done(function(data){ 
            response = data;
            if (response[0].value == '') {
                msg = "No puede agregar productos debido a que no tiene inventario en la bodega seleccionada, parámetro de sobreventas desactivado.";
                header_alert('warning', msg)
            }
            else{
                response.forEach(function(elemento) { 
                    $.ajax({
                        url:"<?= admin_url('products/getProductAVGCostByWarehouse'); ?>/"+elemento.item_id+"/"+warehouse_id,
                    }).done(function(data){
                        if (data != false) {
                            elemento.row.price = data;
                        }
                        if (!(typeof elemento.row.wh_data[warehouse_id] === "undefined")) {
                            // vamos a asignar la cantidad de la bodega al qty
                            elemento.row.qty = (elemento.row.wh_data[warehouse_id].quantity > 0) ? elemento.row.wh_data[warehouse_id].quantity : 1;
                            var row = add_adjustment_origin_item(elemento);
                        }
                        if (row){
                            $("#add_item_origin").val('');
                        }
                    }).fail(function(data){
                        console.log(data);
                    });
                });
            }
        });
    }

</script>