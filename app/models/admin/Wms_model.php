<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Wms_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_customer_order_sales($customer_id = NULL, $biller_id = NULL, $order_sale_reference = NULL){
        $conditions = [];

        if ($customer_id) {
            $conditions['order_sales.customer_id'] = $customer_id;
        }

        if ($biller_id && $this->Settings->wms_picking_filter_biller == 1) {
            $conditions['order_sales.biller_id'] = $biller_id;
        }

        if ($order_sale_reference) {
            $conditions['order_sales.id'] = $order_sale_reference;
        }

        $q = $this->db->select('
                                order_sale_items.*, 
                                order_sale_items.quantity as old_quantity, 
                                order_sales.reference_no, 
                                order_sales.customer_id, 
                                '.$this->db->dbprefix('product_variants').'.name as variant_name,
                                '.$this->db->dbprefix('product_variants').'.suffix as variant_suffix
                                ')
                    ->join('order_sales', 'order_sales.id = order_sale_items.sale_id', 'inner')
                    ->join('product_variants', 'product_variants.id = order_sale_items.option_id', 'left')
                    ->where('order_sales.sale_status !=', 'completed')
                    ->where('('.$this->db->dbprefix('order_sale_items').'.quantity - '.$this->db->dbprefix('order_sale_items').'.quantity_delivered) >', 0)
                    ->where($conditions)
                    ->order_by('order_sales.date ASC')
                    ->get('order_sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $row->cancelled = true;
                $data[$row->sale_id]['rows'][] = $row;
                $data[$row->sale_id]['customer'] = $row->customer_id;
                $data[$row->sale_id]['cancelled'] = true;
                $product_data = $this->site->getProductByID($row->product_id);
                if ($product_data->attributes == 1) {
                    $wh_pv = $this->db->select('warehouses_products_variants.*')
                            ->join('warehouses', 'warehouses.id = warehouses_products_variants.warehouse_id', 'join')
                            ->where('product_id', $row->product_id)
                            ->where('option_id', $row->option_id)
                            ->order_by('warehouses.area asc, warehouses.street asc, warehouses.module asc, warehouses.level asc')
                            ->get('warehouses_products_variants');
                    if ($wh_pv->num_rows() > 0) {
                        $order = 1;
                        foreach (($wh_pv->result()) as $wh_pv_row) {
                            if ($wh_pv_row->quantity > 0) {
                                $wh_pv_row->order = $order;
                                $row->warehouses_products[$order] = $wh_pv_row;
                                $order++;
                            }
                        }
                    }
                } else {
                    $wh = $this->db->select('warehouses_products.*')
                            ->join('warehouses', 'warehouses.id = warehouses_products.warehouse_id', 'join')
                            ->where('product_id', $row->product_id)
                            ->order_by('warehouses.area asc, warehouses.street asc, warehouses.module asc, warehouses.level asc')
                            ->get('warehouses_products');
                    if ($wh->num_rows() > 0) {
                        $order = 1;
                        foreach (($wh->result()) as $wh_row) {
                            if ($wh_row->quantity > 0) {
                                $wh_row->order = $order;
                                $row->warehouses_products[$order] = $wh_row;
                                $order++;
                            }
                        }
                    }
                }
            }
            return $data;
        }
        return false;
    }

    public function get_order_sale_suggestion($term, $biller_id, $limit){
        $this->db->select('id, reference_no as text')
                    ->where('sale_status !=', 'completed')
                    ->where('wms_picking_status !=', 'completed');
        if ($this->Settings->wms_picking_filter_biller == 1) {
            $this->db->where('biller_id', $biller_id);
        }     
        $q = $this->db->where('(reference_no LIKE "%'.$term.'%")')
                    ->get('order_sales');
      if ($q->num_rows() > 0) {
        foreach (($q->result()) as $row) {
            $data[] = $row;
        }
        return $data;
      }
      return false;
    }

    public function getOrderSaleByID($id)
    {
        $q = $this->db->get_where('order_sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_default_picking_notices($type){
        $q = $this->db->get_where('wms_default_notices', ['type'=>$type]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function add_picking($data, $products, $notices, $tf_data) {
        $os_id_sync = [];
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $ref = explode("-", $reference);
        $data['reference_no'] = $ref[0].$ref[1];
        $consecutive = $ref[1];
        if($this->db->insert('wms_picking', $data)){
            $picking_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($products as $product) {
                $product['picking_id'] = $picking_id;
                $this->db->insert('wms_picking_detail', $product);
                $os_id_sync[$product['order_sale_id']] = 1;
            }
            foreach ($notices as $notice) {
                $notice['picking_id'] = $picking_id;
                $this->db->insert('wms_notices', $notice);
            }
        }
        foreach ($tf_data as $tf) {
            $tf['data']['reference_no'] = $data['reference_no'];
            $this->transfers_model->addTransfer($tf['data'], $tf['rows']);
        }
        foreach ($os_id_sync as $os_id => $os_id_setted) {
            $this->sync_order_sale_picking_status($os_id);
        }
        return true;
    }

    public function get_picking($id){
        $q = $this->db->select('
                            wms_picking.*
                        ')
                ->where('wms_picking.id', $id)
                ->get('wms_picking');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_picking_detail($id){
        $q = $this->db->select('
                            wms_picking_detail.*,
                            CONCAT('.$this->db->dbprefix('product_variants').'.name, '.$this->db->dbprefix('product_variants').'.suffix) as variant
                        ')
                ->join('product_variants', 'product_variants.id = wms_picking_detail.option_id', 'left')
                ->where('wms_picking_detail.picking_id', $id)
                ->get('wms_picking_detail');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function sync_order_sale_picking_status($order_id){
        $q = $this->db->get_where('order_sale_items', ['sale_id' => $order_id]);
        $total_picked = 0;
        $total_ordered = 0; //CAMBIAR REVISAR ¡¡ OJO !!
        $status = 'pending';
        foreach (($q->result()) as $order_sale_item) {
            $total_ordered += $order_sale_item->quantity;
            $r = $this->db->get_where('wms_picking_detail', ['order_sale_item_id' => $order_sale_item->id]);
            foreach (($r->result()) as $picking_detail) {
                $total_picked += $picking_detail->quantity;
            }
        }

        if ($total_picked == $total_ordered) {
            $status = 'completed';
        } else if ($total_picked > 0) {
            $status = 'partial';
        }

        // exit(var_dump('total_picked '.$total_picked.' total_ordered '.$total_ordered));
        $this->db->update('order_sales', ['wms_picking_status' => $status], ['id' => $order_id]);
    }

    public function get_picking_notices($id){
        $q = $this->db->select(
                            '
                                wms_default_notices.id as notice_id,
                                wms_default_notices.text as notice,
                                warehouses.name as warehouse_name,
                                products.name as product_name,
                                products.code as product_code,
                                product_variants.name as product_variant,
                                product_variants.suffix as product_variant_suffix,
                                wms_notices.quantity
                            '
                                )
        ->join('wms_default_notices', 'wms_default_notices.id = wms_notices.default_notice_id', 'inner')
        ->join('warehouses', 'warehouses.id = wms_notices.warehouse_id', 'inner')
        ->join('products', 'products.id = wms_notices.product_id', 'inner')
        ->join('product_variants', 'product_variants.id = wms_notices.option_id', 'left')
        ->where(['wms_notices.type' => 1, 'wms_notices.picking_id' => $id])
        ->get('wms_notices');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_picking_suggestion($term, $biller_id, $limit){
        $this->db->select('id, reference_no as text')
                    ->where('packed_status !=', 'completed');
        if ($this->Settings->wms_picking_filter_biller == 1) {
            $this->db->where('biller_id', $biller_id);
        }     
        $q = $this->db->where('(reference_no LIKE "%'.$term.'%")')
                    ->get('wms_picking');
      if ($q->num_rows() > 0) {
        foreach (($q->result()) as $row) {
            $data[] = $row;
        }
        return $data;
      }
      return false;
    }

    public function get_customer_pickings($customer_id = NULL, $biller_id = NULL, $picking_reference = NULL){
        $conditions = [];

        if ($customer_id) {
            $conditions['wms_picking.customer_id'] = $customer_id;
        }

        if ($biller_id && $this->Settings->wms_picking_filter_biller == 1) {
            $conditions['wms_picking.biller_id'] = $biller_id;
        }

        if ($picking_reference) {
            $conditions['wms_picking.id'] = $picking_reference;
        }

        $q = $this->db->select('
                                wms_picking_detail.*, 
                                wms_picking_detail.quantity as old_quantity, 
                                wms_picking.reference_no, 
                                wms_picking.customer_id, 
                                wms_picking.id as picking_id, 
                                '.$this->db->dbprefix('product_variants').'.name as variant_name,
                                '.$this->db->dbprefix('product_variants').'.suffix as variant_suffix,
                                CONCAT('.$this->db->dbprefix('wms_picking_detail').'.product_code, '.$this->db->dbprefix('product_variants').'.suffix) as reading_code
                                ')
                    ->join('wms_picking', 'wms_picking.id = wms_picking_detail.picking_id', 'inner')
                    ->join('product_variants', 'product_variants.id = wms_picking_detail.option_id', 'left')
                    ->where('wms_picking.packed_status !=', 'completed')
                    ->where($conditions)
                    ->order_by('wms_picking.date ASC')
                    ->get('wms_picking_detail');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->picking_id]['reference_no']= $row->reference_no;
                $data[$row->picking_id]['customer']= $row->customer_id;
                $row->packed = false;
                $data[$row->picking_id]['rows'][] = $row;
            }
            return $data;
        }
        return false;
    }
    public function getPicking($id)
    {
        $q = $this->db->get_where('wms_picking', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
}