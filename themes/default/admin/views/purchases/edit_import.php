<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    <?php if ($this->session->userdata('remove_pols')) { ?>
        <?php $this->sma->unset_data('remove_pols');
    } ?>
    var is_edit = true;
        localStorage.removeItem('imtag');
        localStorage.removeItem('imbiller');
        localStorage.removeItem('imsupplier');
        localStorage.removeItem('impurchases');
        localStorage.removeItem('imexpenses');
        localStorage.removeItem('imstatus');
        localStorage.setItem('imbiller', "<?= $import->biller_id ?>");
        localStorage.setItem('imsupplier', "<?= $import->supplier_id ?>");
        localStorage.setItem('imtag', "<?= $import->tag_id ?>");
        localStorage.setItem('imstatus', "<?= $import->status ?>");
        localStorage.setItem('impurchases', '<?= json_encode($import_detail['purchases']) ?>');
        localStorage.setItem('imexpenses', '<?= json_encode($import_detail['expenses']) ?>');
    $(document).ready(function () {

        <?php if ($Owner || $Admin) { ?>
            <?php if ($this->Settings->purchase_datetime_management == 1): ?>
                $("#podate").datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    startDate: min_input_date,
                    endDate: max_input_date,
                    forceParse: 0
                }).datetimepicker('update', (localStorage.getItem('podate') ? localStorage.getItem('podate') : new Date()));
            <?php else: ?>
                $('#podate').val("<?= date('Y-m-d') ?>").prop('min', min_input_date).prop('max', max_input_date);
            <?php endif ?>
            $(document).on('change', '#podate', function (e) {
                localStorage.setItem('podate', $(this).val());
            });
        <?php } ?>
        if (!localStorage.getItem('potax2')) {
            localStorage.setItem('potax2', <?=$Settings->purchase_tax_rate;?>);
            setTimeout(function(){ $('#extras').iCheck('check'); }, 1000);
        }
        $("#add_item").autocomplete({
            // source: '<?= admin_url('purchases/suggestions'); ?>',
            source: function (request, response) {
                if (!$('#purchase_type').val() || !$('#posupplier').val() || !$('#document_type_id').val()) {
                    var msg = "";

                    if (!$('#purchase_type').val()) {
                        command: toastr.warning('<?= lang('select_above').' '.lang('purchase_type'); ?>  ', '¡Atención!', {
                            "showDuration": "1200",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }

                    if (!$('#posupplier').val()) {
                        command: toastr.warning('<?= lang('select_above').' '.lang('supplier'); ?>', '¡Atención!', {
                            "showDuration": "1200",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }

                    if (!$('#document_type_id').val()) {
                        command: toastr.warning('<?= lang('select_above').' '.lang('add_reference_no_label'); ?>', '¡Atención!', {
                            "showDuration": "1200",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    $('#add_item').focus();
                    return false;
                }

                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('purchases/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        supplier_id: $("#posupplier").val(),
                        biller_id: $("#pobiller").val(),
                        purchase_type: $("#purchase_type").val()
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if (($(this).val().length >= 16 && ui.content[0].id == 0) || (ui.content.length == 1 && ui.content[0].id == 0)) {
                    //audio_error.play();
                    if (site.pos_settings.use_barcode_scanner == 0) {
                        header_alert('error', 'Sin resultados', null, function(){ $('#producto').focus() });
                    } else {
                        header_alert('error', 'Sin resultados', null, function(){ $('#add_item').focus() });
                        $(this).val('');
                    }
                    if (site.settings.purchases_products_supplier_code == 1) {
                        header_alert('info', 'Está usando códigos de proveedor en compra, por favor revisar información diligenciada o buscar producto por nombre');
                    }

                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {

                    ui.item.is_new = true;
                    if (ui.item.row.cnt_units_prices !== undefined && ui.item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && ui.item.row.promotion != 1) {
                        var item_id = ui.item.item_id;
                        var warehouse_id = $('#powarehouse').val();
                        $('.product_name_spumodal').text(ui.item.label);
                        $('#sPUModal').appendTo("body").modal('show');
                        add_item_unit(item_id, warehouse_id, ui.item.row.purchase_unit);
                        $(this).val('');
                    } else {
                        var row = add_purchase_item(ui.item);
                        if (row)
                            $(this).val('');
                    }

                } else {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
    });

</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                            <?php
                            $attrib = array('id' => 'submit_import_form');
                            echo admin_form_open_multipart("purchases/edit_import/".$id, $attrib);
                            ?>
                            <input type="hidden" name="deleted_detail" id="deleted_detail">
                            <div class="row">
                                <div class="col-md-4 same-height">
                                    <div class="form-group">
                                        <?= lang("date", "podate"); ?>
                                        <?php if ($this->Settings->purchase_datetime_management == 1): ?>
                                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : date('d/m/Y H:i')), 'class="form-control input-tip datetime" id="imdate" required="required"'); ?>
                                        <?php else: ?>
                                            <input type="date" name="date" class="form-control skip" id="imdate" required>
                                        <?php endif ?>
                                    </div>
                                </div>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4  same-height form-group">
                                        <?= lang("biller", "pobiller"); ?>
                                        <select name="biller" class="form-control" id="imbiller" required="required">
                                            <?php foreach ($billers as $biller): ?>
                                              <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" selected><?= $biller->company ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "pobiller"); ?>
                                            <select name="biller" class="form-control" id="imbiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-4 same-height">
                                    <div class="form-group">
                                        <?= lang("add_reference_no_label", "poref"); ?><br>
                                            <div style="display: flex;">
                                                <select name="document_type_id" class="form-control" id="document_type_id" required="required">
                                                </select>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 tag_div">
                                    <?= lang('tag', 'potag') ?>
                                    <select name="tag" id="imtag" class="form-control" required>
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($tags): ?>
                                            <?php foreach ($tags as $tag_row): ?>
                                                <option value="<?= $tag_row->id ?>" data-color="<?= $tag_row->color ?>" data-type="<?= $tag_row->type ?>"><?= $tag_row->description ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-4 same-height">
                                    <?= lang("add_purchase", "impurchase"); ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Buscar compra para añadir a la importación; Sólo se obtendrán compras con estado Pendiente y sin etiqueta asignada." style="cursor: pointer"></i>
                                    <input type="hidden" name="purchase" value="" id="impurchase" class="form-control" placeholder="<?= lang("select") . ' ' . lang("single_purchase") ?>">
                                </div>
                                <div class="col-md-4 same-height">
                                    <?= lang("add_expense", "imexpense"); ?> <i class="fa fa-question-circle fa-lg" data-toggle="tooltip" data-placement="top" title="Buscar gasto para añadir a la importación;  Sólo se obtendrán gastos con estado Pendiente y sin etiqueta asignada." style="cursor: pointer"></i>
                                    <input type="hidden" name="expense" value="" id="imexpense" class="form-control" placeholder="<?= lang("select") . ' ' . lang("expense") ?>">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4">
                                    <?= lang('status', 'status') ?>
                                    <?php
                                    $post = array('1' => lang('completed'), '0' => lang('pending'));
                                    echo form_dropdown('status', $post, '', 'id="imstatus" class="form-control input-tip select" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("status") . '" required="required" style="width:100%;" ');
                                    ?>
                                </div>
                                <div class="col-md-4 same-height">
                                    <div class="form-group">
                                        <?= lang("supplier_creditor", "imsupplier"); ?>
                                            <input type="hidden" name="supplier" value="" id="imsupplier"
                                                class="form-control"
                                                placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" required>
                                            <input type="hidden" name="supplier_id" value="" id="supplier_id"
                                                class="form-control">
                                    </div>
                                </div>
                            </div>
                            <hr class="col-sm-11">
                            <div class="row">
                                <div class="col-md-12">
                                    <table>
                                        <tr>
                                            <th><h3><?= lang('purchases') ?></h3></th>
                                        </tr>
                                    </table>
                                    <div class="control-group table-group" id="accordion_purchases">
                                    </div>
                                    <table>
                                        <tr>
                                            <th><h3><?= lang('expenses') ?></h3><em class="text-danger error_expenses" style="display:none;"><?= lang('expenses_required_for_import') ?></em></th>
                                        </tr>
                                    </table>
                                    <div class="control-group table-group" id="accordion_expenses">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                            <div class="row">
                                <div class="col-md-12">
                                    <b class="text-danger error_debug" style="display: none;"></b>
                                    <div class="from-group">
                                        <button type="button" class="btn btn-primary" id="submit_import" style="padding: 6px 15px; margin:15px 0;"><?= lang('submit') ?></button>
                                        <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();
    });

    
</script>