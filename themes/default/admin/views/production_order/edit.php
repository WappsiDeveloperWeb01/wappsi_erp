<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1, an = 1;
    var type_opt = {'editition': '<?= lang('editition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    var max_qpr = "<?= isset($max_qpr) ? $max_qpr : 0 ?>";
    var edit = true;
    var last_employee = 'false';

    por_items = {};
    $(document).ready(function () {
        if (localStorage.getItem('poritems')) {
            localStorage.removeItem('poritems');
        }
        if (localStorage.getItem('porref')) {
            localStorage.removeItem('porref');
        }
        if (localStorage.getItem('pornote')) {
            localStorage.removeItem('pornote');
        }
        if (localStorage.getItem('pordate')) {
            localStorage.removeItem('pordate');
        }

        localStorage.setItem('poritems', '<?= $rows ?>');
        localStorage.setItem('poremployee', '<?= $inv->employee_id ?>');
        localStorage.setItem('porbiller', '<?= $inv->biller_id ?>');
        localStorage.setItem('pordate', '<?= date("d/m/Y H:i", strtotime($inv->date)) ?>');
        localStorage.setItem('porestimated_date', '<?= date("d/m/Y H:i", strtotime($inv->est_date)) ?>');
        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('pordate')) {
            $("#pordate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#pordate', function (e) {
            localStorage.setItem('pordate', $(this).val());
        });
        if (pordate = localStorage.getItem('pordate')) {
            $('#pordate').val(pordate);
        }
        $(document).on('change', '#porestimated_date', function (e) {
            localStorage.setItem('porestimated_date', $(this).val());
        });
        if (porestimated_date = localStorage.getItem('porestimated_date')) {
            $('#porestimated_date').val(porestimated_date);
        }
        <?php } ?>

        $("#edit_item").autocomplete({
            source: function (request, response) {
                if (!$('#document_type_id').val() || !$('#poremployee').val()) {
                    var msg = "";
                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }
                    if (!$('#poremployee').val()) {
                        msg += "</br><?= lang('employee') ?>";
                    }
                    $('#edit_item').val('').removeClass('ui-autocomplete-loading');
                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#edit_item').focus();
                    return false;
                }

                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('production_order/suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        limit : 10,
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });

            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#edit_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#edit_item").val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#edit_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#edit_item").val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = edit_order_item(ui.item);
                    if (row){
                        $("#edit_item").val('');
                    }
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
    });

$(document).on('change', '#porbiller', function (e) {
    localStorage.setItem('porbiller', $('#porbiller').val());
});

</script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('edit_making_order'); ?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="introtext"><?php echo lang('enter_info'); ?></p>
                            <?php
                            $attrib = array('id' => 'edit_production_order');
                            echo admin_form_open_multipart("production_order/edit/".$inv->id, $attrib);
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">

                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("creation_date", "pordate"); ?>
                                                <?php echo form_input('creation_date', (isset($_POST['creation_date']) ? $_POST['creation_date'] : ""), 'class="form-control input-tip datetime" id="pordate" required="required"'); ?>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("estimated_date", "porestimated_date"); ?>
                                                <?php echo form_input('estimated_date', (isset($_POST['estimated_date']) ? $_POST['estimated_date'] : date('d/m/Y H:i')), 'class="form-control input-tip datetime" id="porestimated_date" required="required"'); ?>
                                            </div>
                                        </div>

                                        <?php
                                            $bl[""] = "";
                                            $bldata = [];
                                            foreach ($billers as $biller) {
                                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                $bldata[$biller->id] = $biller;
                                            }
                                        ?>
                                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <?= lang("biller", "porbiller"); ?>
                                                    <select name="biller" class="form-control" id="porbiller" required="required">
                                                        <?php foreach ($billers as $biller): ?>
                                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <?= lang("biller", "porbiller"); ?>
                                                    <select name="biller" class="form-control" id="porbiller">
                                                        <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                            $biller = $bldata[$this->session->userdata('biller_id')];
                                                            ?>
                                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                        <?php endif ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } ?>
                                    </div>

                                    <div class="row">
                                        <!-- <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <?= lang("warehouse", "porwarehouse"); ?>
                                                    <?php
                                                    $wh[''] = '';
                                                    foreach ($warehouses as $warehouse) {
                                                        $wh[$warehouse->id] = $warehouse->name;
                                                    }
                                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ($Settings->default_warehouse)), 'id="porwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                            <?php } else {
                                                $warehouse_input = array(
                                                    'type' => 'hidden',
                                                    'name' => 'warehouse',
                                                    'id' => 'porwarehouse',
                                                    'value' => $this->session->userdata('warehouse_id'),
                                                    );

                                                echo form_input($warehouse_input);
                                            } ?> -->

                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("reference_no", "document_type_id"); ?>
                                                <input type="text" name="reference_no" class="form-control" value="<?= $inv->reference_no ?>" readonly>
                                            </div>
                                        </div>

                                        <div class="col-md-4 col-sm-4 form-group">
                                            <?= lang('responsable', 'poremployee') ?>
                                            <?php
                                            $cOptions[''] = lang('select')." ".lang('employee');
                                            if ($employees) {
                                                foreach ($employees as $row => $company) {
                                                    $cOptions[$company->id] = $company->name;
                                                }
                                            }
                                             ?>
                                            <?= form_dropdown('employee_id', $cOptions, '',' id="poremployee" class="form-control select" required style="width:100%;"'); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12 col-sm-12" id="sticker">
                                        <div class="form-group" style="margin-bottom:0;">
                                                <?php echo form_input('edit_item', '', 'class="form-control input-lg" id="edit_item" placeholder="' . lang("search_product_by_name_code") . '"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("products"); ?> *</label>

                                            <div class="controls table-controls">
                                                <table id="porTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th class="col-md-1"><?= lang("type"); ?></th>
                                                        <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                        <th style="max-width: 30px !important; text-align: center;">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("components"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="porCTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th style="width: 35%;"><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th style="width: 35%;"><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th style="width: 15%;"><?= lang("type"); ?></th>
                                                        <th style="width: 15%;"><?= lang("quantity"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <?= lang("note", "pornote"); ?>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="pornote" style="margin-top: 10px; height: 100px;"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="fprom-group">
                                            <button type="button" class="btn btn-primary" id="edit_production_order_submit"><?= lang('submit') ?></button>
                                            <button type="button" class="btn btn-primary" id="edit_production_order_submit_continue"><?= lang('submit_init_cutting') ?></button>
                                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="scontinue" name="continue" value="0">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <input type="hidden" id="serialModal_option_id">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <div class="input-group">
                            <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                            <span class="input-group-editon pointer" id="random_num" style="pediting: 1px 10px;">
                                <i class="fa fa-random"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $("#edit_production_order").validate({
            ignore: []
        });
    });

    $(document).on('click', '#edit_production_order_submit', function(){
        $('#scontinue').val(0);
        if ($('#edit_production_order').valid()) {
            $('#edit_production_order').submit();
        }
    });

    $(document).on('click', '#edit_production_order_submit_continue', function(){
        $('#scontinue').val(1);
        if ($('#edit_production_order').valid()) {
            $('#edit_production_order').submit();
        }
    });


</script>