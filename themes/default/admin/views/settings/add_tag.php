<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_tag'); ?></h4>
        </div>
        <?= admin_form_open_multipart("system_settings/createTag", ['data-toggle' => 'validator', 'role' => 'form']); ?>
        <div class="modal-body">
            <div class="form-group">
                <?= form_label($this->lang->line('module'), 'module'); ?>
                <?= form_dropdown(['name'=>'module', 'id'=>'module', 'class'=>'form-control', 'required'=>true], $modules); ?>
            </div>

            <div class="form-group">
                <?= form_label($this->lang->line('type'), 'type'); ?>
                <?= form_dropdown(['name'=>'type', 'id'=>'type', 'class'=>'form-control', 'required'=>true], $types); ?>
            </div>

            <div class="form-group">
                <?= form_label($this->lang->line('color'), 'color'); ?>
                <div class="text-align: justify">
                    <?php foreach ($colors as $color) { ?>
                        <label class="color-option color-option-lg" style="background-color: <?= $color ?>" data-color="<?= $color ?>"></label>
                    <?php } ?>
                    <?= form_input(["name"=>"color", "id"=>"color", "class"=>"form-control", "type"=>"hidden", "required"=>true]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= form_label($this->lang->line('status'), 'status'); ?>
                <?= form_dropdown(['name'=>'status', 'id'=>'status', 'class'=>'form-control', 'required'=>true], $status); ?>
            </div>

            <div class="form-group">
                <?= form_label($this->lang->line('description'), 'description'); ?>
                <?= form_input(['name'=>'description', 'id'=>'description', 'class'=>'form-control', 'required'=>true]); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_button(['name'=>'saveTag', 'id'=>'saveTag', 'type'=>'submit', 'class'=>'btn btn-primary new-button', 'data-toogle-second'=>'tooltip', 'title'=>$this->lang->line('save'), 'content'=>'<i class="fas fa-check"></i>']); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function () {
        $('[data-toogle-second="tooltip"]').tooltip();

        $(document).on('click', '.color-option', function() { select_color($(this)); });
    });

    function select_color(option_control)
    {
        let color = $(option_control).data('color')
        $('#color').val(color);

        $('.color-option').each(function(index, el) {
            $(this).removeClass('color-option-selected');
        });

        $(option_control).addClass('color-option-selected');
    }
</script>