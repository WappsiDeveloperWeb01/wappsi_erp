<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Sales_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('wappsi_invoicing_model');
    }

    public function get_sale_by_id($id)
    {
        $q = $this->db->where("id", $id)->get("sales");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductNames($term, $warehouse_id = NULL, $biller_id = NULL, $aiu_management = false, $enter = false)
    {
        if ($this->Settings->variant_code_search == 0) {
            return $this->get_products_names($term, $warehouse_id, $biller_id, $aiu_management, $enter);
        } else {
            if ($data = $this->get_products_names_variants($term, $warehouse_id, $biller_id, $aiu_management)) {
                return $data;
            } else {
                return $this->get_products_names($term, $warehouse_id, $biller_id, $aiu_management, $enter);
            }
        }
    }

    public function get_products_names($term, $warehouse_id, $biller_id, $aiu_management, $enter){

        $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($biller_id);
        $this->db->select('
                            products.*,
                            ('.($warehouse_id ? "FWP.quantity" : "{$this->db->dbprefix('products')}.quantity").($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') AS quantity,
                            '.($this->Settings->include_pending_order_quantity_stock == 1 ? "COALESCE(OP.pending_quantity, 0) AS op_pending_quantity, " : "").'
                            categories.id as category_id,
                            categories.name as category_name,
                            brands.name as brand_name
                        ', FALSE)
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
            ->join('brands', 'brands.id=products.brand', 'left');
        if ($warehouse_id) {
            $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";
            $this->db->join($wp, 'FWP.product_id=products.id  AND FWP.warehouse_id = ' . $warehouse_id, 'left');
        }
        if ($this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=products.id', 'left');
        }
        if ($this->Settings->exclude_overselling_on_sale_orders == 1 || ($this->Settings->exclude_overselling_on_sale_orders == 0 && $this->Settings->overselling == 1)) {
            if ($this->Settings->display_all_products == 0) {
                if ($enter) {
                    $this->db->where("
                        (
                            {$this->db->dbprefix('products')}.ignore_hide_parameters = 1 OR
                            {$this->db->dbprefix('products')}.type = 'service' OR
                            (".($warehouse_id ? "FWP.quantity > 0" : "{$this->db->dbprefix('products')}.quantity > 0").")
                        )
                        AND
                        (
                            {$this->db->dbprefix('products')}.code = '" . $term . "'
                        )");
                } else {
                    $this->db->where("
                        (
                            {$this->db->dbprefix('products')}.ignore_hide_parameters = 1 OR
                            {$this->db->dbprefix('products')}.type = 'service' OR
                            (".($warehouse_id ? "FWP.quantity > 0" : "{$this->db->dbprefix('products')}.quantity > 0").")
                        )
                        AND
                        (
                            {$this->db->dbprefix('products')}.name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            {$this->db->dbprefix('products')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            {$this->db->dbprefix('products')}.reference ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                        )");
                }
            } else {
                if ($enter) {
                    $this->db->where("
                        (
                            {$this->db->dbprefix('products')}.code = '" . $term . "'
                        )");
                } else {
                    $this->db->where("
                        (
                            {$this->db->dbprefix('products')}.name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            {$this->db->dbprefix('products')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            {$this->db->dbprefix('products')}.reference ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                        )");
                }

            }
        } else {
            if ($enter) {
                $this->db->where("
                        (
                            {$this->db->dbprefix('products')}.type = 'service' OR
                            {$this->db->dbprefix('products')}.type = 'combo' OR
                            (".($warehouse_id ? "FWP.quantity > 0" : "{$this->db->dbprefix('products')}.quantity > 0").")
                        )
                        AND
                        (
                            {$this->db->dbprefix('products')}.code = '" . $term . "'
                        )
                        ");
            } else {
                $this->db->where("
                        (
                            {$this->db->dbprefix('products')}.type = 'service' OR
                            {$this->db->dbprefix('products')}.type = 'combo' OR
                            (".($warehouse_id ? "FWP.quantity > 0" : "{$this->db->dbprefix('products')}.quantity > 0").")
                        )
                        AND
                        (
                            {$this->db->dbprefix('products')}.name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            {$this->db->dbprefix('products')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            {$this->db->dbprefix('products')}.reference ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                            concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                        )
                        ");
            }

        }
        if ($aiu_management) {
            $this->db->where('products.aiu_product', '1');
        }
        $this->db->where('products.type !=', 'raw');
        $this->db->where('categories.hide !=', '1');
        $this->db->where('products.discontinued', '0');
        $this->db->group_by('products.id');
        $this->db->order_by($this->db->dbprefix('products').'.code ASC, '.$this->db->dbprefix('products').'.name ASC');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
    }

    public function get_products_names_variants($term, $warehouse_id, $biller_id, $aiu_management){

        $limit = $this->Settings->max_num_results_display;
        $this->site->create_temporary_product_billers_assoc($biller_id);
        $this->db->select('
                            products.*,
                            ('.($warehouse_id ? "FWP.quantity" : "{$this->db->dbprefix('products')}.quantity").($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') AS quantity,
                            '.($this->Settings->include_pending_order_quantity_stock == 1 ? "COALESCE(OP.pending_quantity, 0) AS op_pending_quantity, " : "").'
                            categories.id as category_id,
                            categories.name as category_name,
                            brands.name as brand_name,
                            product_variants.id as variant_selected,
                            product_variants.weight AS variant_weight_selected,
                            product_variants.minimun_price AS variant_minimun_price_selected
                        ', FALSE)
            ->join('product_variants', 'product_variants.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
            ->join('brands', 'brands.id=products.brand', 'left');
        if ($warehouse_id) {
            $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";
            $this->db->join($wp, 'FWP.product_id=products.id  AND FWP.warehouse_id = ' . $warehouse_id, 'left');
        }
        if ($this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=products.id', 'left');
        }
        if ($this->Settings->overselling == 1) {
            if ($this->Settings->display_all_products == 0) {
                $this->db->where("
                    (
                        {$this->db->dbprefix('products')}.ignore_hide_parameters = 1 OR
                        {$this->db->dbprefix('products')}.type = 'service' OR
                        (".($warehouse_id ? "FWP.quantity > 0" : "{$this->db->dbprefix('products')}.quantity > 0").")
                    )
                    AND
                    (
                        {$this->db->dbprefix('product_variants')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                    )");
            } else {
                $this->db->where("
                    (
                        {$this->db->dbprefix('product_variants')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                    )");
            }
        } else {
            $this->db->where("
                    (
                        {$this->db->dbprefix('products')}.type = 'service' OR
                        {$this->db->dbprefix('products')}.type = 'combo' OR
                        (".($warehouse_id ? "FWP.quantity > 0" : "{$this->db->dbprefix('products')}.quantity > 0").")
                    )
                    AND
                    (
                        {$this->db->dbprefix('product_variants')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")."
                    )
                    ");
        }
        if ($aiu_management) {
            $this->db->where('products.aiu_product', '1');
        }
        $this->db->where('products.type !=', 'raw');
        $this->db->where('product_variants.status', 1);
        $this->db->where('categories.hide !=', '1');
        $this->db->where('products.discontinued', '0');
        $this->db->group_by('products.id');
        $this->db->order_by($this->db->dbprefix('products').'.code ASC, '.$this->db->dbprefix('products').'.name ASC');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
    }

    public function getProductNamesIU($product_id, $warehouse_id, $unit_price_id = NULL, $limit = 1)
    {
        $wp = "( SELECT product_id, warehouse_id, quantity as quantity from {$this->db->dbprefix('warehouses_products')} ) FWP";

        $this->db->select('products.*, 
            ('.($warehouse_id ? "FWP.quantity" : "{$this->db->dbprefix('products')}.quantity").($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') AS quantity,
            '.($this->Settings->include_pending_order_quantity_stock == 1 ? "COALESCE(OP.pending_quantity, 0) AS op_pending_quantity, " : "").' 
            categories.id as category_id, categories.name as category_name', FALSE)
            ->join($wp, 'FWP.product_id=products.id', 'left')
            // ->join('warehouses_products FWP', 'FWP.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id');
        if ($this->Settings->overselling) {
            $this->db->where("({$this->db->dbprefix('products')}.id = ".$product_id." )");
        } else {
            $this->db->where("(products.track_quantity = 0 OR FWP.quantity > 0) AND FWP.warehouse_id = '" . $warehouse_id . "' AND "
                . "({$this->db->dbprefix('products')}.id = ".$product_id." )");
        }

        if ($this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=products.id', 'left');
        }
        $this->db->where('products.discontinued', '0');
        // $this->db->order_by('products.name ASC');
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getOrderSaleProductNames($term, $warehouse_id)
    {
        $limit = $this->Settings->max_num_results_display;
        $wp = "( SELECT product_id, warehouse_id, quantity FROM {$this->db->dbprefix('warehouses_products')} ) FWP";
        $this->db->select('products.*,
                           FWP.quantity as quantity,
                           categories.id as category_id,
                           categories.name as category_name
                           ', FALSE)
            ->join($wp, 'FWP.product_id=products.id', 'left')
            ->join('categories', 'categories.id=products.category_id', 'left')
            ->group_by('products.id');
        $this->db->where("({$this->db->dbprefix('products')}.name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR {$this->db->dbprefix('products')}.code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR  concat({$this->db->dbprefix('products')}.name, ' (', {$this->db->dbprefix('products')}.code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")");
        $this->db->where('products.discontinued', '0');
        if ($this->Settings->display_all_products == 0) {
            $this->db->where(' (products.ignore_hide_parameters = 1 OR (products.ignore_hide_parameters = 0 AND products.quantity > 0)) ');
        }
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getProductComboItems($pid, $warehouse_id = NULL)
    {
        $this->db->select('products.id as id, combo_items.item_code as code, combo_items.unit as unit, combo_items.quantity as qty, products.name as name,products.type as type, warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $q = $this->db->get_where('combo_items', array('combo_items.product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductByCode($code)
    {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function syncQuantity($sale_id)
    {
        if ($sale_items = $this->getAllInvoiceItems($sale_id)) {
            foreach ($sale_items as $item) {
                $this->site->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->site->syncVariantQty($item->option_id, $item->warehouse_id);
                }
            }
        }
    }

    public function getProductQuantity($product_id, $warehouse)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse), 1);
        if ($q->num_rows() > 0) {
            return $q->row_array(); //$q->row();
        }
        return FALSE;
    }

    public function getProductOptions($product_id, $warehouse_id, $all = NULL, $option_id = NULL)
    {
        $this->db->select('
                            product_variants.id as id,
                            product_variants.weight as weight,
                            CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "") ) as name,
                            '.$this->db->dbprefix('product_variants').'.code,
                            product_variants.price as price,
                            product_variants.quantity as total_quantity,
                            ('.$this->db->dbprefix('warehouses_products_variants').'.quantity'.($this->Settings->include_pending_order_quantity_stock == 1 ? " - COALESCE(OP.pending_quantity, 0)" : "").') as quantity
                            '.($this->Settings->include_pending_order_quantity_stock == 1 ? ", COALESCE(OP.pending_quantity, 0) as op_pending_quantity" : ", 0 as op_pending_quantity").'
                    ', FALSE)
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id AND warehouses_products_variants.warehouse_id = '.$warehouse_id, 'left')
            ->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id');

        if ($this->Settings->include_pending_order_quantity_stock == 1) {
            $pending_order_sql = "(SELECT 
                                    OSI.product_id,
                                    OSI.option_id,
                                    SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
                                FROM sma_order_sale_items OSI
                                    INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
                                WHERE OS.sale_status != 'cancelled'
                                ".($warehouse_id ? ' AND OSI.warehouse_id = ' . $warehouse_id : '')."
                                GROUP BY OSI.product_id, OSI.option_id) OP";
            $this->db->join($pending_order_sql, 'OP.product_id=product_variants.product_id AND OP.option_id = product_variants.id', 'left');
        }

        if (!$this->Settings->overselling && !$all) {
            $this->db->where('warehouses_products_variants.warehouse_id', $warehouse_id);
            $this->db->where('warehouses_products_variants.quantity >', 0);
        }
        if ($option_id) {
            $this->db->where('product_variants.id', $option_id);
        }
        $q = $this->db->get('product_variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductPreferences($product_id){
        $q = $this->db->select('product_preferences.*, preferences_categories.name as category_name')
                ->join('preferences_categories', 'preferences_categories.id = product_preferences.preference_category_id')
                ->where('product_preferences.product_id', $product_id)
                ->get('product_preferences');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariants($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemByID($id)
    {
        $this->db->select("sale_items.*, p.code, s.reference_no");
        $this->db->join("products p", "p.id = sale_items.product_id");
        $this->db->join("sales s", "s.id = sale_items.sale_id");
        $q = $this->db->get_where('sale_items', array('sale_items.id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getAllInvoiceItems($sale_id, $return_id = NULL, $arr = false, $product_order = NULL)
    {
        $this->db->select('sale_items.*,
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            tax_rates.id as tax_rate_id,
            tax_rates.tax_indicator as tax_indicator,
            tr2ic.tax_indicator as tax_indicator2ic,
            tr2ic.code as tax_code2ic,
            products.image,
            products.details as details,
            CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as variant,
            products.hsn_code as hsn_code,
            products.reference as product_reference,
            products.second_name as second_name,
            products.tax_method as tax_method,
            brands.name as brand_name,
            products.reference as reference,
            units.code as product_unit_code,
            units.operation_value,
            units.operator')
            ->join('products', 'products.id = sale_items.product_id', 'left')
            ->join('brands', 'brands.id = products.brand', 'left')
            ->join('units', $this->db->dbprefix('units').'.id = IF('.$this->db->dbprefix('sale_items').'.product_unit_id_selected  > 0, '.$this->db->dbprefix('sale_items').'.product_unit_id_selected, IF('.$this->db->dbprefix('sale_items').'.product_unit_id > 0, '.$this->db->dbprefix('sale_items').'.product_unit_id, '.$this->db->dbprefix('products').'.sale_unit))', 'left')
            ->join('product_variants', 'product_variants.id = sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id = sale_items.tax_rate_id', 'left')
            ->join('tax_secondary tr2ic', 'tr2ic.id=sale_items.tax_rate_2_id', 'left')
            ->group_by('sale_items.id');
        if ($product_order == NULL || $product_order == 4) {

            $this->db->order_by('sale_items.id', 'asc');
        } else if ($product_order == 1) {
            $this->db->order_by('products.code', 'asc');
        } else if ($product_order == 2) {
            $this->db->order_by('products.name', 'asc');
        } else if ($product_order == 3) {
            $this->db->order_by('products.name', 'desc');
        } else if ($product_order == 5) {
            $this->db->order_by('sale_items.id', 'desc');
        }
        if ($sale_id && !$return_id) {
            $this->db->where('sale_id', $sale_id);
        } elseif ($return_id) {
            $this->db->where('sale_id', $return_id);
        }
        $q = $this->db->get('sale_items');
        if ($q->num_rows() > 0) {
                if ($arr == false) {
                    foreach (($q->result()) as $row) {
                        $data[] = $row;
                    }
                } else {
                    foreach (($q->result_array()) as $row) {
                        $row['sale_id'] = $sale_id;
                        $data[] = $row;
                    }
                }
            return $data;
        }
        return FALSE;
    }

    public function getAllOrderSaleItems($sale_id, $return_id = NULL, $product_order = NULL)
    {
        $this->db->select('
                            order_sale_items.*,
                            warehouses_products.quantity AS in_stock_quantity,
                            tax_rates.code as tax_code,
                            tax_rates.name as tax_name,
                            tax_rates.rate as tax_rate,
                            products.image,
                            products.details as details,
                            CONCAT('.$this->db->dbprefix('product_variants').'.name, COALESCE('.$this->db->dbprefix('product_variants').'.suffix, "")) as variant,
                            products.hsn_code as hsn_code,
                            products.second_name as second_name,
                            brands.name as brand_name,
                            products.reference as reference,
                            SUM(COALESCE('.$this->db->dbprefix('wms_picking_detail').'.quantity)) as picking_quantity,
                            SUM(COALESCE('.$this->db->dbprefix('wms_picking_detail').'.quantity)) as packing_quantity,
                            main_unit.code as main_unit_code,
                            main_unit.id as main_unit_id,
                            units.code as product_unit_code,
                            units.operation_value,
                            units.operator
                        ')
            ->join('products', 'products.id=order_sale_items.product_id', 'left')
            ->join('brands', 'brands.id=products.brand', 'left')
            ->join('product_variants', 'product_variants.id=order_sale_items.option_id', 'left')
            ->join('units', 'order_sale_items.product_unit_id=units.id', 'left')
            ->join('units main_unit', 'products.unit=main_unit.id', 'left')
            ->join('tax_rates', 'tax_rates.id=order_sale_items.tax_rate_id', 'left')
            ->join('wms_picking_detail', 'wms_picking_detail.order_sale_item_id=order_sale_items.id', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id = order_sale_items.product_id AND warehouses_products.warehouse_id = order_sale_items.warehouse_id', 'left')
            ->group_by('order_sale_items.id');
        if ($product_order == NULL || $product_order == 4) {
            $this->db->order_by('order_sale_items.sale_id', 'asc');
            $this->db->order_by('order_sale_items.id', 'asc');
        } else if ($product_order == 1) {
            $this->db->order_by('products.code', 'asc');
        } else if ($product_order == 2) {
            $this->db->order_by('products.name', 'asc');
        } else if ($product_order == 3) {
            $this->db->order_by('products.name', 'desc');
        } else if ($product_order == 5) {
            $this->db->order_by('order_sale_items.sale_id', 'desc');
            $this->db->order_by('order_sale_items.id', 'desc');
        }

        if ($sale_id && !$return_id) {
            $this->db->where('sale_id', $sale_id);
        } elseif ($return_id) {
            $this->db->where('sale_id', $return_id);
        }
        $q = $this->db->get('order_sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllInvoiceItemsWithDetails($sale_id)
    {
        $this->db->select('sale_items.*, products.details, product_variants.name as variant');
        $this->db->join('products', 'products.id=sale_items.product_id', 'left')
        ->join('product_variants', 'product_variants.id=sale_items.option_id', 'left')
        ->group_by('sale_items.id');
        $this->db->order_by('id', 'asc');
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getInvoiceByID($id)
    {
        $q = $this->db->select('
                        sales.*,
                        ('.$this->db->dbprefix("sales").'.rete_fuente_total - SUM(COALESCE('.$this->db->dbprefix("payments").'.rete_fuente_total))) rete_fuente_total,
                        ('.$this->db->dbprefix("sales").'.rete_iva_total - SUM(COALESCE('.$this->db->dbprefix("payments").'.rete_iva_total))) rete_iva_total,
                        ('.$this->db->dbprefix("sales").'.rete_ica_total - SUM(COALESCE('.$this->db->dbprefix("payments").'.rete_ica_total))) rete_ica_total,
                        ('.$this->db->dbprefix("sales").'.rete_other_total - SUM(COALESCE('.$this->db->dbprefix("payments").'.rete_other_total))) rete_other_total
                        ')
                 ->join('payments', 'payments.sale_id = sales.id', 'left')
                 ->group_by('sales.id')
                 ->where('sales.id', $id)
                 ->get('sales');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getOrderSaleByID($id)
    {
        $q = $this->db->get_where('order_sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getReturnBySID($sale_id)
    {
        $q = $this->db->get_where('sales', array('sale_id' => $sale_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function addOptionQuantity($option_id, $quantity)
    {
        if ($option = $this->getProductOptionByID($option_id)) {
            $nq = $option->quantity + $quantity;
            if ($this->db->update('product_variants', array('quantity' => $nq), array('id' => $option_id))) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function getProductWarehouseOptionQty($option_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateProductOptionQuantity($option_id, $warehouse_id, $quantity, $product_id)
    {
        if ($option = $this->getProductWarehouseOptionQty($option_id, $warehouse_id)) {
            $nq = $option->quantity - $quantity;
            if ($this->db->update('warehouses_products_variants', array('quantity' => $nq), array('option_id' => $option_id, 'warehouse_id' => $warehouse_id))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        } else {
            $nq = 0 - $quantity;
            if ($this->db->insert('warehouses_products_variants', array('option_id' => $option_id, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'quantity' => $nq))) {
                $this->site->syncVariantQty($option_id, $warehouse_id);
                return TRUE;
            }
        }
        return FALSE;
    }

    // Sobre esta función de seben aplicar las funciones de contabilidad como lo estoy haciendo en POS
    public function addSale($data = array(), $items = array(), $payment = array(), $si_return = array(), $data_tax_rate_traslate = array(), $quote_id = null, $order = false, $orders_ids = false)
    {
        $cost = $this->site->costing($items, (!empty($si_return) ? TRUE : FALSE), $data['sale_status']);
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        } else {
            $reference = $this->site->getReference('so');
        }
        $ref = explode("-", $reference);
        $hyphen = !empty($ref[0]) ? "-" : "";
        $data['reference_no'] = $ref[0].$hyphen.$ref[1];
        $consecutive = $ref[1];
        if ($this->site->check_if_register_exists('sales', $data)) {
            $this->session->set_flashdata('error', lang('process_interrupted_due_to_duplicate_records'));
            redirect($_SERVER["HTTP_REFERER"]);
            return false;
        }
        if (!$this->session->userdata('detal_post_processing_model')) {
            $this->session->set_userdata('detal_post_processing_model', 1);
        } else {
            $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->db->insert('sales', $data)) {
            $sale_id = $this->db->insert_id();
            $sale_id_returned = isset($data['sale_id']) ? $data['sale_id'] : NULL;
            $data['sale_id'] = $sale_id;
            $check_reference_data = [
                'table' => 'sales', 'record_id' => $sale_id, 'reference' => $data['reference_no'], 'biller_id' => $data['biller_id'], 'document_type_id' => $data['document_type_id']
                ];
            if ($new_reference = $this->site->check_reference($check_reference_data)) {
                $data['reference_no'] = $new_reference;
            }
            $item_costing_each = [];
            foreach ($items as $key_item => $item) {
                $item['sale_id'] = $sale_id;
                if ($this->Settings->ipoconsumo && isset($item['consumption_sales']) && $item['consumption_sales'] == 0) {
                    $product = $this->site->getProductByID($item['product_id']);
                    if ($product->consumption_purchase_tax > 0) {
                        $items[$key_item]['consumption_sales_costing'] = $product->consumption_purchase_tax * (!empty($si_return) ? -1 : 1);
                        $item['consumption_sales_costing'] = $product->consumption_purchase_tax * (!empty($si_return) ? -1 : 1);
                    }
                }
                $product_gift_card_no = $product_gift_card_value = $product_gift_card_expiry = NULL;
                if (isset($item['product_gift_card_no']) && $item['product_gift_card_no']) {
                    $product_gift_card_no = $item['product_gift_card_no'];
                    $product_gift_card_value = $item['product_gift_card_value'];
                    $product_gift_card_expiry = $item['product_gift_card_expiry'];
                    $item['product_name'] .= " (".$item['product_gift_card_no'].")";
                }
                unset($item['product_gift_card_no']);
                unset($item['product_gift_card_value']);
                unset($item['product_gift_card_expiry']);
                $this->db->insert('sale_items', $item);
                $sale_item_id = $this->db->insert_id();
                if ($product_gift_card_no) {
                    $gc_data = array(
                        'card_no' => $product_gift_card_no,
                        'value' => $product_gift_card_value,
                        'customer_id' => $data['customer_id'],
                        'customer' => $data['customer'],
                        'balance' => $product_gift_card_value,
                        'expiry' => $product_gift_card_expiry,
                        'creation_type' => 1,
                        'sale_item_id' => $sale_item_id,
                        'status' => 1,
                        'sale_reference_no' => $data['reference_no'],
                        'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    );
                    $this->db->insert('gift_cards', $gc_data);
                }
                if ($data['sale_status'] == 'completed' && empty($si_return)) {
                    $item_costs = $this->site->item_costing($item);
                    $item_costing_each[$item['product_id']] = $item_costs;
                    if (isset($item_costs[0]['purchase_net_unit_cost'])) {
                        $avg_net_unit_cost = (Double) $item_costs[0]['purchase_net_unit_cost'];
                        $this->db->update('sale_items', ['avg_net_unit_cost' => $avg_net_unit_cost], ['sale_id' => $sale_id, 'product_id' => $item['product_id']]);
                    }
                    foreach ($item_costs as $item_cost) {
                        if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $sale_id;
                            $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                            if(!isset($item_cost['pi_overselling'])) {
                                unset($item_cost['preparation_id_origin']);
                                $this->db->insert('costing', $item_cost);
                            }
                        } else {
                            foreach ($item_cost as $ic) {
                                $ic['sale_item_id'] = $sale_item_id;
                                $ic['sale_id'] = $sale_id;
                                $ic['date'] = date('Y-m-d', strtotime($data['date']));
                                unset($ic['preparation_id_origin']);
                                if(! isset($ic['pi_overselling'])) {
                                    $this->db->insert('costing', $ic);
                                }
                            }
                        }
                    }
                }
                // if (!empty($si_return)) {
                //     $inv = $this->site->getSaleByID($sale_id_returned);
                //     if ($inv->sale_origin_reference_no && $inv_order_sale = $this->get_order_sale_by_reference($inv->sale_origin_reference_no)) {
                //         $order = true;
                //         $quote_id = $inv_order_sale->id;
                //     }
                // }
                if ($quote_id && $order) {
                    $qty_del = 0;
                    $this->db->where("quantity_to_bill > ", 0);
                    $condicion = array(
                                        'sale_id' => $quote_id,
                                        'product_id' => $item['product_id']
                                    );
                    if ($this->sma->field_is_filled($item['product_unit_id'])) {
                        $condicion['product_unit_id'] = $item['product_unit_id'];
                    }
                    if ($this->sma->field_is_filled($item['option_id'])) {
                        $condicion['option_id'] = $item['option_id'];
                    }
                    $order_item = $this->db->where($condicion)->get('order_sale_items')->row_array();
                    $qty_del = $order_item['quantity_to_bill'] + $order_item['quantity_delivered'];
                    if ($qty_del > $order_item['quantity']) {
                        $qty_del = $order_item['quantity'];
                    }
                    $condicion['id'] = $order_item['id'];
                    $this->db->update('order_sale_items', array('quantity_delivered' => $qty_del, 'quantity_to_bill' => 0), ($condicion));
                }
            }
            $this->site->syncStoreProductsQuantityMovement($items, 2);
            if ($quote_id && !$order) {
                $this->db->update('quotes', array('status' => 'completed', 'destination_reference_no' => $data['reference_no']), array('id' => $quote_id));
            } else if ($quote_id && $order) {
                $this->site->updateOrderSaleStatus($quote_id);
                if (!isset($inv_order_sale) || !$inv_order_sale) {
                    $inv_order_sale = $this->getOrderSaleByID($quote_id);
                }
                $new_destination_reference = ($inv_order_sale->destination_reference_no ? $inv_order_sale->destination_reference_no.", " : "").$data['reference_no'];
                $this->db->update('order_sales', array('destination_reference_no' => $new_destination_reference), array('id' => $quote_id));
            }
            if ($data['sale_status'] == 'completed') {
                $this->site->syncPurchaseItems($cost);
            }
            if (!empty($si_return)) {
                foreach ($cost as $itc) {
                    $this->site->updateAVCO(array('product_id' => $itc[0]['product_id'], 'warehouse_id' => $data['warehouse_id'], 'quantity' => ($itc[0]['quantity'] * -1), 'cost' => $itc[0]['purchase_unit_cost'], 'shipping_unit_cost' => 0, 'sale_return'=>1));
                    $this->db->update('sale_items', ['avg_net_unit_cost' => $itc[0]['purchase_unit_cost']], ['sale_id'=>$sale_id, 'product_id'=>$itc[0]['product_id']]);
                }
                foreach ($si_return as $return_item) {
                    if ($return_item['product_id'] == $this->Settings->gift_card_product_id) {
                        $this->db->update('gift_cards', ['status'=>3], ['sale_item_id'=>$return_item['id']]);
                        $gc_return_data = $this->db->get_where('gift_cards', ['sale_item_id'=>$return_item['id']])->row();
                        $this->db->insert('gift_card_topups', [
                                                'date'=>date('Y-m-d H:i:s'),
                                                'card_id'=>$gc_return_data->id,
                                                'created_by'=>$this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                                'movement_type'=>2,
                                            ]);
                    }
                    $product = $this->site->getProductByID($return_item['product_id']);
                    if ($product != FALSE && $product->type == 'combo') {
                        $combo_items = $this->site->getProductComboItems($return_item['product_id'], $return_item['warehouse_id']);
                        foreach ($combo_items as $combo_item) {
                            $this->UpdateCostingAndPurchaseItem($return_item, $combo_item->id, ($return_item['quantity']*$combo_item->qty));
                        }
                    } else {
                        $this->UpdateCostingAndPurchaseItem($return_item, $return_item['product_id'], $return_item['quantity']);
                    }
                    $this->db->query("UPDATE {$this->db->dbprefix('sale_items')} SET {$this->db->dbprefix('sale_items')}.returned_quantity = COALESCE({$this->db->dbprefix('sale_items')}.returned_quantity, 0) + ".$return_item['quantity']." WHERE {$this->db->dbprefix('sale_items')}.id = ".$return_item['id']);
                }
                $this->db->update('sales', array('surcharge' => $data['surcharge'], 'return_id' => $sale_id), array('id' => $si_return[0]['sale_id']));
                $this->db->query("UPDATE {$this->db->dbprefix('sales')} SET {$this->db->dbprefix('sales')}.return_sale_total = COALESCE({$this->db->dbprefix('sales')}.return_sale_total, 0) + ".$data['grand_total']." WHERE {$this->db->dbprefix('sales')}.id = ".$sale_id_returned);

                $this->db->query("UPDATE {$this->db->dbprefix('sales')} SET {$this->db->dbprefix('sales')}.return_sale_ref = CONCAT(IF({$this->db->dbprefix('sales')}.return_sale_ref IS NOT NULL, CONCAT({$this->db->dbprefix('sales')}.return_sale_ref, ', '), ''), '".$data['reference_no']."') WHERE {$this->db->dbprefix('sales')}.id = ".$sale_id_returned);
                $this->db->insert('sales_returns', [
                                                        'sale_id'=> $si_return[0]['sale_id'],
                                                        'sale_reference_no'=> $data['return_sale_ref'],
                                                        'return_id'=> $sale_id,
                                                        'return_reference_no'=> $data['reference_no'],
                                                    ]);
            }
            if ($data['payment_status'] == 'pending' && !empty($payment)) {
                //TRATAMIENTO RETENCION SOBRE VENTA DETAL
                $payment[0]['sale_id'] = $sale_id;
                $this->db->insert('payments', $payment[0]);
                //TRATAMIENTO RETENCION SOBRE VENTA DETAL
            }
            $total_payment = 0;
            if (($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid') && !empty($payment)) {
                foreach ($payment as $pmnt_id => $pmnt) {
                    if (!empty($si_return) && $pmnt['paid_by'] == 'gift_card') {
                        if (isset($si_return[0]['paid_by_giftcard']) && $si_return[0]['paid_by_giftcard'] == 1) {
                            $gc_payment = $this->db->get_where('payments', ['sale_id'=>$data['sale_id'], 'paid_by'=>'gift_card'])->row();
                            $gc_data = $this->db->get_where('gift_cards', ['card_no'=>$gc_payment->cc_no])->row();
                            $this->db->insert('gift_card_topups', [
                                                'date'=>$data['date'],
                                                'card_id'=>$gc_data->id,
                                                'created_by'=>$this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                                'movement_type'=>1,
                                                'amount'=>($pmnt['amount']*-1),
                                                'biller_id'=>$data['biller_id'],
                                                'note'=>sprintf(lang('gift_card_top_up_by_sale_return'), $data['reference_no']),
                                            ]);
                            $this->db->update('gift_cards', ['balance'=>$gc_data->balance+($pmnt['amount']*-1)], ['id'=>$gc_data->id]);
                            $this->site->update_gift_card_balance_status($gc_data->id);
                        } else {
                            $new_gc_data = array(
                                'card_no' => $pmnt['cc_no'],
                                'value' => ($pmnt['amount']*-1),
                                'customer_id' => $data['customer_id'],
                                'customer' => $data['customer'],
                                'balance' => ($pmnt['amount']*-1),
                                'expiry' => date("Y-m-d", strtotime("+1 year")),
                                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                'creation_type' => 3,
                                'status' => 1,
                                'sale_reference_no' => $data['reference_no'],
                            );
                            $this->db->insert('gift_cards', $new_gc_data);
                        }
                    }
                    if ($pmnt['paid_by'] != 'retencion' && isset($pmnt['document_type_id'])) {
                        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt['document_type_id']);
                        if($referenceBiller){
                            $pay_reference = $referenceBiller;
                        }
                        $pmnt['reference_no'] = $pay_reference;
                        $ref = explode("-", $pay_reference);
                        $p_consecutive = $ref[1];

                        $pmnt_data = $this->site->getPaymentMethodByCode($pmnt['paid_by']);
                        $pmnt['pm_commision_value'] = $pmnt_data->commision_value;
                        $pmnt['pm_retefuente_value'] = $pmnt_data->retefuente_value;
                        $pmnt['pm_reteiva_value'] = $pmnt_data->reteiva_value;
                        $pmnt['pm_reteica_value'] = $pmnt_data->reteica_value;
                    } else {
                        $pmnt['reference_no'] = $data['reference_no'];
                    }
                    $pmnt['sale_id'] = $sale_id;
                    if ($pmnt['paid_by'] == 'gift_card' && empty($si_return)) {

                        $this->db->update('gift_cards', array('balance' => $pmnt['gc_balance']), array('card_no' => $pmnt['cc_no']));
                        unset($pmnt['gc_balance']);
                        $this->db->insert('payments', $pmnt);
                        $this->site->update_gift_card_balance_status(NULL, $pmnt['cc_no']);

                    } else {
                        if ($pmnt['paid_by'] == 'deposit') {
                            if (empty($si_return)) { //SI EL PAGO NO PROVIENE DESDE DEVOLUCIÓN
                                $p_arr[] = $pmnt;
                                $p_arr = $this->site->set_payments_affected_by_deposits($data['customer_id'], $p_arr);
                                $count = 0;
                                foreach ($p_arr as $pmnt2) {
                                    $pmnt2['pos_paid'] = $pmnt2['amount'];
                                    if ($count > 0) {
                                        $this->site->updateBillerConsecutive($pmnt2['document_type_id'], $p_consecutive+1);
                                        // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                                        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt2['document_type_id']);
                                        if($referenceBiller){
                                            $reference_payment = $referenceBiller;
                                        }else{
                                            $reference_payment = $this->site->getReference('rc');
                                        }
                                        $pmnt2['reference_no'] = $reference_payment;
                                        $ref = explode("-", $reference_payment);
                                        $p_consecutive = $ref[1];
                                        // NUEVA REFERENCIA POR REGISTRO DE OTRO DEPÓSITO
                                    }
                                    if (isset($pmnt['deposit_document_type_id'])) {
                                        unset($pmnt['deposit_document_type_id']);
                                    }
                                    $this->db->insert('payments', $pmnt2);
                                    $count++;
                                }
                            } else { //SI EL PAGO SI PROVIENE DESDE DEVOLUCIÓN
                                // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
                                $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt['deposit_document_type_id']);
                                if($referenceBiller){
                                    $reference_deposit = $referenceBiller;
                                }else{
                                    $reference_deposit = $this->site->getReference('rc');
                                }
                                $ref = explode("-", $reference_deposit);
                                $deposit_consecutive = $ref[1];
                                // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO

                                $deposit_data_amount = ($pmnt['amount'] * -1) + ($data['shipping']);
                                $deposit_data = array(
                                    'date' => $data['date'],
                                    'reference_no' => $reference_deposit,
                                    'amount' => $deposit_data_amount,
                                    'balance' => $deposit_data_amount,
                                    // 'paid_by' => 'cash',
                                    'paid_by' => $pmnt['paid_by'],
                                    'note' => 'Anticipo generado automáticamente por devolución',
                                    'company_id' => $data['customer_id'],
                                    'created_by' => $this->session->userdata('user_id'),
                                    'biller_id' => $data['biller_id'],
                                    'document_type_id' => $pmnt['deposit_document_type_id'],
                                    'origen_reference_no' => $data['reference_no'],
                                    'origen_document_type_id' => $data['document_type_id'],
                                    'origen' => '2',
                                );
                                if (isset($data['cost_center_id'])) {
                                    $deposit_data['cost_center_id'] = $data['cost_center_id'];
                                }
                                if ($this->db->insert('deposits', $deposit_data)) {
                                    $this->site->updateBillerConsecutive($pmnt['deposit_document_type_id'], $deposit_consecutive+1);
                                    $this->site->wappsiContabilidadDeposito($deposit_data);
                                    $customer = $this->site->getCompanyByID($data['customer_id']);
                                    $this->db->update('companies', ['deposit_amount' => ($customer->deposit_amount + $deposit_data_amount )], ['id'=> $customer->id]);
                                }
                                unset($pmnt['deposit_document_type_id']);
                                $this->db->insert('payments', $pmnt);
                            }
                        }
                        if ($pmnt['paid_by'] != 'deposit') {
                            $this->db->insert('payments', $pmnt);
                        }

                    }
                    if ($this->site->getReference('pay') == $pmnt['reference_no']) {
                        $this->site->updateReference('pay');
                    }
                    if ($pmnt['paid_by'] != 'due' && $pmnt['paid_by'] != 'retencion') {
                        $total_payment += $pmnt['amount'];
                    }
                    if (isset($p_consecutive) && isset($pmnt['document_type_id'])) {
                        $this->site->updateBillerConsecutive($pmnt['document_type_id'], $p_consecutive+1);
                    }
                    if (isset($pmnt_data)) {
                        $payment[$pmnt_id]['pmnt_data'] = $pmnt_data;
                    }
                }

            }
            if (!empty($si_return)) {
                $sale = $this->site->getWappsiSaleById($si_return[0]['sale_id']);
                $total_retenciones =
                (isset($data['rete_fuente_total']) ? $data['rete_fuente_total'] : 0) +
                (isset($data['rete_iva_total']) ? $data['rete_iva_total'] : 0) +
                (isset($data['rete_ica_total']) ? $data['rete_ica_total'] : 0) +
                (isset($data['rete_bomberil_total']) ? $data['rete_bomberil_total'] : 0) +
                (isset($data['rete_autoaviso_total']) ? $data['rete_autoaviso_total'] : 0) +
                (isset($data['rete_other_total']) ? $data['rete_other_total'] : 0);
                // $this->sma->print_arrays($sale, $data);
                if($data['payment_status'] == 'pending' || $data['payment_status'] == 'due'){

                    //Traer datos de la venta
                    // Actualizar el campo paid en la venta
                    $paid = $sale->paid + ($sale->return_sale_total * -1);
                    $tabla = 'sales';
                    $this->db->update($tabla, array('paid' => $paid), array('id' => $data['sale_id']));
                    $this->db->update($tabla, array('payment_status' => 'paid', 'paid' => $data['grand_total']), array('id' => $sale_id));
                    // Insertando registro en la tabla de payments
                    $tabla = 'payments';
                    $payment = array();
                    $payment['date'] = $data['date'];
                    $payment['sale_id'] = $si_return[0]['sale_id'];
                    $payment['amount'] = (($sale->return_sale_total - $total_retenciones) * -1) - $data['return_order_discount_not_applied'] - ($data['shipping']);
                    $payment['reference_no'] = $data['reference_no'];
                    $payment['paid_by'] = "due";
                    $payment['type'] = "devolución";
                    $payment['note'] = "Devolución factura Credito ".$data['reference_no'];
                    if ($total_retenciones > 0) {
                        $payment['rete_fuente_total'] = $data['rete_fuente_total'];
                        $payment['rete_iva_total'] = $data['rete_iva_total'];
                        $payment['rete_ica_total'] = $data['rete_ica_total'];
                        $payment['rete_other_total'] = $data['rete_other_total'];
                    }
                    $payment['created_by'] = $this->session->userdata('user_id');
                    $this->db->insert($tabla,$payment);
                } else if ($sale->payment_status == 'partial') {

                    $tabla = 'payments';
                    $payment_add = array();
                    $payment_add['date'] = $data['date'];
                    $payment_add['sale_id'] = $si_return[0]['sale_id'];
                    $payment_add['amount'] = ($data['grand_total'] * -1) - ($data['shipping']) - $total_retenciones - ($total_payment * -1) - $data['return_order_discount_not_applied'];
                    $payment_add['reference_no'] = $data['reference_no'];
                    $payment_add['paid_by'] = "due";
                    $payment_add['type'] = "devolución";
                    $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                    $payment_add['created_by'] = $this->session->userdata('user_id');
                    $this->db->insert($tabla,$payment_add);
                } else if ($data['payment_status'] == 'paid' && $sale->payment_status != 'paid') {
                    $tabla = 'payments';
                    $payment_add = array();
                    $payment_add['date'] = $data['date'];
                    $payment_add['sale_id'] = $si_return[0]['sale_id'];
                    $payment_add['amount'] = ($data['grand_total'] * -1) - ($data['shipping']) - $total_retenciones - $data['return_order_discount_not_applied'];
                    $payment_add['reference_no'] = $data['reference_no'];
                    $payment_add['paid_by'] = "due";
                    $payment_add['type'] = "returned";
                    $payment_add['note'] = "Saldo de devolución restante de crédito ".$data['reference_no'];
                    $payment_add['created_by'] = $this->session->userdata('user_id');
                    // $this->sma->print_arrays($payment_add);
                    $this->db->insert($tabla,$payment_add);
                }
            }
            $this->site->syncSalePayments($sale_id);
            $this->site->syncQuantity($sale_id);
            $biller_data = $this->site->getAllCompaniesWithState('biller', $data['biller_id']);
            if ($biller_data->default_customer_id != $data['customer_id']) {
                $pdsid = $data['sale_id'];
                $data['sale_id'] = $sale_id;
                $this->sma->update_award_points($data);
                $data['sale_id'] = $pdsid;
            }
            /* LLamado a la función wappsiContabilidad para alimentar las tablas del modulo de contabilidad */
            if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
                if (empty($si_return)) {
                    if ($data['sale_status'] == 'completed') {
                        $this->site->wappsiContabilidadVentas($sale_id, $data, $items, $payment, $cost, $data_tax_rate_traslate, $item_costing_each);
                    }
                }else{
                  $this->site->wappsiContabilidadVentasDevolucion($sale_id, $data, $items, $payment, $si_return, $cost);
                }
            }
            // Se realizo el proceso de inserción en las tablas de contabilidad.
            if ($orders_ids) {
                $orders_references = "";
                foreach ($orders_ids as $key => $oslid) {
                    $order_sale_data = $this->getOrderSaleByID($oslid);
                    $orders_references .= ($orders_references != "" ? ", " : "" )."".$order_sale_data->reference_no;
                    $this->db->update('order_sales', ['sale_status'=>'completed', 'destination_reference_no'=>$data['reference_no']], ['id'=>$oslid]);
                    $this->db->query('UPDATE sma_order_sale_items SET quantity_delivered = quantity WHERE sale_id = '.$oslid);
                }
                $this->db->update('sales', ['sale_origin_reference_no'=>$orders_references], ['id'=>$sale_id]);
            }
            if ($this->Settings->wappsi_invoice_issuer == 1) {
                $msg_arr = $this->wappsi_invoicing_model->sync_wappsi_invoicing($data['customer_id'], $data['address_id'], $sale_id);
            }
            // exit('A');
            return $sale_id;
        }
        return false;
    }

    // NUEVO MÓDULO ORDEN DE COMPRA

    public function restartOrderDelivered($order_id)
    {
        $this->db->where('sale_id', $order_id)->update('order_sale_items', array('quantity_to_bill' => 0));
    }

    public function addOrderSale($data = array(), $items = array())
    {
        if (!isset($data['reference_no'])) {
            $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
            if($referenceBiller){
                $reference = $referenceBiller;
            } else {
                $reference = $this->site->getReference('os');
            }
            $data['reference_no'] = $reference;
            $ref = explode("-", $reference);
            $consecutive = $ref[1];
        }
        if ($this->db->insert('order_sales', $data)) {
            $sale_id = $this->db->insert_id();
            if (isset($consecutive)) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            }
            foreach ($items as $item) {
                $item['sale_id'] = $sale_id;
                $this->db->insert('order_sale_items', $item);
                $sale_item_id = $this->db->insert_id();
            }

            if ($data['sale_status'] == 'completed') {
                $this->site->syncPurchaseItems($cost);
            }

            if ($this->site->updateOrderSaleStatus($sale_id)) {
                return $sale_id;
            }
        }
        return false;
    }

    public function updateSale($id, $data, $items = array(), $payment = array())
    {
        $sale = $this->getInvoiceByID($id);
        $this->db->update('payments', ['sale_id'=>NULL, 'note'=>'Pago anulado por edición de venta '.$sale->reference_no], ['sale_id'=>$id, 'multi_payment'=>0]);
        $oitems = $this->getAllInvoiceItems($id);
        $oitems_id = [];
        foreach ($oitems as $oidata) {
            $oitems_id[$oidata->id] = 1;
        }

        $this->resetSaleActions($id, FALSE, TRUE);
        if ($data['sale_status'] == 'completed') {
            $this->Settings->overselling = true;
            $cost = $this->site->costing($items, true);
        }


        $updateCons = false;
        if ($sale->document_type_id != $data['document_type_id']) {
            $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
            if($referenceBiller){
                $reference = $referenceBiller;
            }
            $data['reference_no'] = $reference;
            $ref = explode("-", $reference);
            $consecutive = $ref[1];
            $updateCons = true;
        }

        if ($this->db->update('sales', $data, array('id' => $id)) && $this->db->delete('costing', array('sale_id' => $id))) {

            if ($updateCons) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            }
            foreach ($items as $item) {

                if (isset($oitems_id[$item['id']])) {
                    unset($oitems_id[$item['id']]);
                }

                $item['sale_id'] = $id;

                if (!empty($item['id']) && is_numeric($item['id']) && $item['id'] > 0) {
                    $this->db->update('sale_items', $item, ['id' => $item['id']]);
                    $sale_item_id = $item['id'];
                } else {
                    unset($item['id']);
                    $this->db->insert('sale_items', $item);
                    $sale_item_id = $this->db->insert_id();
                }

                if ($data['sale_status'] == 'completed' && $this->site->getProductByID($item['product_id'])) {
                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $id;
                            $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        } else {
                            foreach ($item_cost as $ic) {
                                $ic['sale_item_id'] = $sale_item_id;
                                $ic['sale_id'] = $id;
                                $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                                if(! isset($ic['pi_overselling'])) {
                                    $this->db->insert('costing', $ic);
                                }
                            }
                        }
                    }
                }
            }


            if (isset($data['payment_status']) && ($data['payment_status'] == 'partial' || $data['payment_status'] == 'paid') && !empty($payment)) {
                foreach ($payment as $pmnt) {
                    if ($pmnt['paid_by'] == 'retencion') {
                        $rt = $this->db->get_where('payments', ['sale_id'=>$id, 'paid_by'=>'retencion']);
                        if ($rt->num_rows() > 0) {
                            $rt = $rt->row();
                            $this->db->update('payments', $pmnt, ['id'=>$rt->id]);
                        } else {
                            $this->db->insert('payments', $pmnt);
                        }
                    } else {
                        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $pmnt['document_type_id']);
                        if($referenceBiller){
                            $pay_reference = $referenceBiller;
                        }
                        $pmnt['reference_no'] = $pay_reference;
                        $ref = explode("-", $pay_reference);
                        $p_consecutive = $ref[1];
                        $this->db->insert('payments', $pmnt);
                        $this->site->updateBillerConsecutive($pmnt['document_type_id'], $p_consecutive+1);
                    }
                }
            }
            if ($data['sale_status'] == 'completed') {
                $this->site->syncPurchaseItems($cost);
            }

            $this->site->syncSalePayments($id);
            $this->site->syncQuantity($id);
            $this->sma->update_award_points($data);

            if ($data['sale_status'] == 'completed') {
                $this->recontabilizarVenta($id, true);
            }
            if ($sale->return_id) {
                $this->db->update('sales',
                                    [
                                        'customer_id' => $data['customer_id'],
                                        'customer' => $data['customer'],
                                    ],
                                    [
                                        'id' => $sale->return_id,
                                    ]
                                );
                $this->recontabilizarVenta($sale->return_id, true);
            }

            if (count($oitems_id) > 0) {
                foreach ($oitems_id as $oid => $setted) {
                    $this->db->delete('sale_items', ['id'=>$oid]);
                }
            }
            return true;
        }
        return false;
    }

    public function updateOrderSale($id, $data, $items = array())
    {
        $oitems = $this->sales_model->getAllOrderSaleItems($id);
        $oitems_ids = [];
        foreach ($oitems as $oitem) {
            $oitems_ids[$oitem->id] = 1;
        }
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->update('order_sales', $data, array('id' => $id))) {
            foreach ($items as $item) {
                $item['last_update'] = date('Y-m-d H:i:s');
                if ($item['sale_item_id'] &&  $item['sale_item_id'] != 'null') {
                    unset($oitems_ids[$item['sale_item_id']]);
                    $this->db->update('order_sale_items', $item, array('id' => $item['sale_item_id']));
                } else {
                    unset($item['sale_item_id']);
                    $item['sale_id'] = $id;
                    $this->db->insert('order_sale_items', $item);
                }
            }
            if ($this->site->updateOrderSaleStatus($id)) {
                if (count($oitems_ids) > 0) {
                    foreach ($oitems_ids as $oid => $oval) {
                        $this->db->delete('order_sale_items', ['id'=>$oid]);
                    }
                }
                return true;
            }
        }
        return false;
    }

    public function updateStatus($id, $status, $note)
    {

        $sale = $this->getInvoiceByID($id);
        $items = $this->getAllInvoiceItems($id);
        $cost = array();
        if ($status == 'completed' && $status != $sale->sale_status) {
            foreach ($items as $item) {
                $items_array[] = (array) $item;
            }
            $cost = $this->site->costing($items_array);
        }

        if ($this->db->update('sales', array('sale_status' => $status, 'note' => $note), array('id' => $id))) {

            if ($status == 'completed' && $status != $sale->sale_status) {

                foreach ($items as $item) {
                    $item = (array) $item;
                    if ($this->site->getProductByID($item['product_id'])) {
                        $item_costs = $this->site->item_costing($item);
                        foreach ($item_costs as $item_cost) {
                            $item_cost['sale_item_id'] = $item['id'];
                            $item_cost['sale_id'] = $id;
                            $item_cost['date'] = date('Y-m-d', strtotime($sale->date));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        }
                    }
                }

            } elseif ($status != 'completed' && $sale->sale_status == 'completed') {
                $this->resetSaleActions($id);
            }

            if (!empty($cost)) {
                $this->site->syncPurchaseItems($cost);
            }


            $this->site->syncQuantity(NULL, NULL, $items);
            return true;
        }
        return false;
    }

    public function deleteSale($id)
    {
        $sale_items = $this->resetSaleActions($id);
        if ($this->db->delete('sale_items', array('sale_id' => $id)) &&
        $this->db->delete('sales', array('id' => $id)) &&
        $this->db->delete('costing', array('sale_id' => $id))) {
            $this->db->delete('sales', array('sale_id' => $id));
            $this->db->delete('payments', array('sale_id' => $id));
            $this->site->syncQuantity(NULL, NULL, $sale_items);
            return true;
        }
        return FALSE;
    }

    public function resetSaleActions($id, $return_id = NULL, $check_return = NULL)
    {
        if ($sale = $this->getInvoiceByID($id)) {
            if ($check_return && $sale->sale_status == 'returned') {
                $this->session->set_flashdata('warning', lang('sale_x_action'));
                redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }

            if ($sale->sale_status == 'completed') {
                if ($costings = $this->getSaleCosting($id)) {
                    foreach ($costings as $costing) {
                        if ($pi = $this->getPurchaseItemByID($costing->purchase_item_id)) {
                            $this->site->setPurchaseItem(['id' => $pi->id, 'product_id' => $pi->product_id, 'option_id' => $pi->option_id], $costing->quantity);
                        } else {
                            $sale_item = $this->getSaleItemByID($costing->sale_item_id);
                            $pi = $this->site->getPurchasedItem(['product_id' => $costing->product_id, 'option_id' => $costing->option_id ? $costing->option_id : NULL, 'purchase_id' => NULL, 'transfer_id' => NULL, 'warehouse_id' => $sale_item->warehouse_id]);
                            $this->site->setPurchaseItem(['id' => $pi->id, 'product_id' => $pi->product_id, 'option_id' => $pi->option_id], $costing->quantity);
                        }
                    }
                }
                $items = $this->getAllInvoiceItems($id);
                $this->site->syncQuantity(NULL, NULL, $items);
                $data = [];
                foreach ($sale as $key => $value) {
                    $data[$key] = $value;
                }
                $this->sma->update_award_points($data, true);
                return $items;
            }
        }
    }

    public function getPurchaseItemByID($id)
    {
        $q = $this->db->get_where('purchase_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCostingLines($sale_item_id, $product_id, $sale_id = NULL)
    {
        if ($sale_id) { $this->db->where('sale_id', $sale_id); }
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->order_by('id', $orderby);
        $q = $this->db->get_where('costing', array('sale_item_id' => $sale_item_id, 'product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSaleItemByID($id)
    {
        $q = $this->db->get_where('sale_items', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByName($name)
    {
        $q = $this->db->get_where('products', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addDelivery($data = array())
    {
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $data['do_reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        // $this->sma->print_arrays($data);
        if ($this->db->insert('deliveries', $data)) {
            $del_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            return $del_id;
        }
        return false;
    }

    public function updateDelivery($id, $data = array())
    {
        if ($this->db->update('deliveries', $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getDeliveryByID($id)
    {
        $q = $this->db->get_where('deliveries', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDeliveryBySaleID($sale_id)
    {
        $q = $this->db->get_where('deliveries', array('sale_id' => $sale_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteDelivery($id)
    {
        if ($this->db->delete('deliveries', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getInvoicePayments($sale_id, $without_retention = false)
    {
        $this->db->order_by('id', 'asc');
        $conditions['sale_id'] = $sale_id;
        if ($without_retention) {
            $conditions['paid_by !='] = 'retencion';
        }
        $this->db->where($conditions);
        $q = $this->db->select('payments.*,
                                IF('.$this->db->dbprefix("payment_methods").'.name IS NOT NULL, '.$this->db->dbprefix("payment_methods").'.name, '.$this->db->dbprefix("payments").'.paid_by) AS paid_by
                                ')
                        ->join('payment_methods', 'payment_methods.code = payments.paid_by', 'left')
                        ->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getPaymentByID($id)
    {
        // $q = $this->db->get_where('payments', array('id' => $id), 1);
        // if ($q->num_rows() > 0) {
        //     return $q->row();
        // }
        // return FALSE;

        $q = $this->db->select('
                                payments.*,
                                IF('.$this->db->dbprefix("payment_methods").'.name IS NOT NULL, '.$this->db->dbprefix("payment_methods").'.name, '.$this->db->dbprefix("payments").'.paid_by) AS paid_by,
                                COALESCE(return_sale.rete_fuente_total, 0) as return_rete_fuente,
                                COALESCE(return_sale.rete_iva_total, 0) as return_rete_iva,
                                COALESCE(return_sale.rete_ica_total, 0) as return_rete_ica,
                                COALESCE(return_sale.rete_other_total, 0) as return_rete_other
                              ')
                ->join('sales', 'sales.id = payments.sale_id', 'left')
                ->join('payment_methods', 'payment_methods.code = payments.paid_by', 'left')
                ->join('sales as return_sale', 'return_sale.id = sales.return_id', 'left')
                ->where('payments.id', $id)
                ->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentsForSale($sale_id, $arr = false)
    {
        $this->db->select('payments.*,
            (
                COALESCE('.$this->db->dbprefix('payments').'.rete_fuente_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_iva_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_ica_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_other_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_autoaviso_total, 0) +
                COALESCE('.$this->db->dbprefix('payments').'.rete_bomberil_total, 0)
            ) as rc_total_retention
            , users.first_name, users.last_name, type, payment_methods.name')
            ->join('users', 'users.id=payments.created_by', 'left')
            ->join('payment_methods', 'payment_methods.code=payments.paid_by', 'left')
            ;
        // $this->db->where('(paid_by != "due" AND paid_by != "retencion" AND paid_by != "discount")');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result_array()) as $row) {
                    $row['sale_id'] = $sale_id;
                    $data[] = $row;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaymentsForSaleDev($sale_id, $arr = false)
    {
        $this->db->select('payments.date, payments.paid_by, payments.amount, payments.cc_no, payments.cheque_no, payments.reference_no, users.first_name, users.last_name, type')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id, 'type' => 'returned'));
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                $data = $q->row();
            } else {
                $data = $q->row_array();
            }
            return $data;
        }
        return FALSE;
    }

    public function getPaymentsForSaleReAccounting($sale_id, $arr = false)
    {
        $this->db->select('payments.*, users.first_name, users.last_name')
            ->join('users', 'users.id=payments.created_by', 'left');
        $q = $this->db->get_where('payments', array('sale_id' => $sale_id, 'type !=' => 'devolución'));
        if ($q->num_rows() > 0) {
            if ($arr == false) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                foreach (($q->result_array()) as $row) {
                    $row['sale_id'] = $sale_id;
                    $data[] = $row;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function addPayment($data = array(), $customer_id = null, $retencion = array(), $data_tax_rate_traslate = array())
    {

        //TRATAMIENTO DE PAGO INDIVIDUAL CON RETENCIÓN APLICADA
        if ($retencion != null) {

            $rete_payment = array(
                'date'         => $data['date'],
                'sale_id'      => $data['sale_id'],
                'amount'       => $retencion['total_retenciones'],
                'reference_no' => 'retencion',
                'paid_by'      => 'retencion',
                'cheque_no'    => '',
                'cc_no'        => '',
                'cc_holder'    => '',
                'cc_month'     => '',
                'cc_year'      => '',
                'cc_type'      => '',
                'created_by'   => $this->session->userdata('user_id'),
                'type'         => 'received',
                'note'         => 'Retenciones',
            );

            unset($retencion['total_retenciones']);



            if ($this->db->insert('payments', $rete_payment)) { //SE INSERTA EL PAGO DE RETENCIÓN
                if ($this->db->update('sales', $retencion, array('id' => $data['sale_id']))) { //SE ACTUALIZAN DATOS DE RETENCIÓN EN LA COMPRA
                    # code...
                } else {
                    exit('Error al actualizar base de datos : '.$this->db->last_query());
                }
            } else {
                return FALSE;
            }

        }
        //TRATAMIENTO DE PAGO INDIVIDUAL CON RETENCIÓN APLICADA

        if ($this->db->insert('payments', $data)) {
            if ($this->site->getReference('rc') == $data['reference_no']) {
                $this->site->updateReference('rc');
            }
            $this->site->syncSalePayments($data['sale_id']);
            if ($data['paid_by'] == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($data['cc_no']);
                $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
                $this->site->update_gift_card_balance_status(NULL, $data['cc_no']);
            } elseif ($customer_id && $data['paid_by'] == 'deposit') {
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
            }
            /* LLamado a la función wappsiContabilidad para pagos de ventas para alimentar las tablas del modulo de contabilidad */
            if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
                if($this->site->wappsiContabilidadPagoVentas($data, $customer_id, $retencion, $data_tax_rate_traslate)){}
            }
            return true;
        }
        return false;
    }

    public function updatePayment($id, $data = array(), $customer_id = null)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->update('payments', $data, array('id' => $id))) {
            $this->site->syncSalePayments($data['sale_id']);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
                $this->site->update_gift_card_balance_status(NULL, $opay->cc_no);
            } elseif ($opay->paid_by == 'deposit') {
                if (!$customer_id) {
                    $sale = $this->getInvoiceByID($opay->sale_id);
                    $customer_id = $sale->customer_id;
                }
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }
            if ($data['paid_by'] == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($data['cc_no']);
                $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
                $this->site->update_gift_card_balance_status(NULL, $data['cc_no']);
            } elseif ($customer_id && $data['paid_by'] == 'deposit') {
                $customer = $this->site->getCompanyByID($customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount-$data['amount'])), array('id' => $customer_id));
            }
            return true;
        }
        return false;
    }

    public function deletePayment($id)
    {
        $opay = $this->getPaymentByID($id);
        if ($this->db->delete('payments', array('id' => $id))) {
            $this->site->syncSalePayments($opay->sale_id);
            if ($opay->paid_by == 'gift_card') {
                $gc = $this->site->getGiftCardByNO($opay->cc_no);
                $this->db->update('gift_cards', array('balance' => ($gc->balance+$opay->amount)), array('card_no' => $opay->cc_no));
                $this->site->update_gift_card_balance_status(NULL, $opay->cc_no);
            } elseif ($opay->paid_by == 'deposit') {
                $sale = $this->getInvoiceByID($opay->sale_id);
                $customer = $this->site->getCompanyByID($sale->customer_id);
                $this->db->update('companies', array('deposit_amount' => ($customer->deposit_amount+$opay->amount)), array('id' => $customer->id));
            }
            return true;
        }
        return FALSE;
    }

    public function getWarehouseProductQuantity($warehouse_id, $product_id)
    {
        $q = $this->db->get_where('warehouses_products', array('warehouse_id' => $warehouse_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    /* ----------------- Gift Cards --------------------- */
    public function addGiftCard($data = array(), $ca_data = array(), $sa_data = array())
    {
        $data['date'] = date('Y-m-d H:i:s');
        if ($data['creation_type'] == 4) {
            $reference = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
            $data['reference_no'] = $reference;
            $ref = explode("-", $reference);
            $consecutive = $ref[1];
        }
        $data['status'] = 1;
        $data['ca_points_redeemed'] = isset($ca_data['used_points']) ? $ca_data['used_points'] : 0;
        if ($this->db->insert('gift_cards', $data)) {
            $gc_id = $this->db->insert_id();
            if ($data['creation_type'] == 4) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
                $this->site->wappsiContabilidadTarjetasRegalo($data);
            }
            if ($data['creation_type'] != 4 && !empty($ca_data)) {
                $this->db->update('companies', array('award_points' => $ca_data['points']), array('id' => $ca_data['customer']));
            } elseif (!empty($sa_data)) {
                $this->db->update('users', array('award_points' => $sa_data['points']), array('id' => $sa_data['user']));
            }
            if ($ca_data['used_points'] > 0) {
                $this->db->insert('award_points',
                    [
                        'date' => date('Y-m-d H:i:s'),
                        'type' => 2,
                        'created_by' => $this->session->userdata('user_id'),
                        'customer_id' => $data['customer_id'],
                        'biller_id' => NULL,
                        'sale_id' => NULL,
                        'gift_card_id' => $gc_id,
                        'movement_value' => $data['value'],
                        'movement_points' => ($ca_data['used_points'] * -1)
                    ]
                );
            }
            return true;
        }
        return false;
    }

    public function updateGiftCard($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update('gift_cards', $data)) {
            return true;
        }
        return false;
    }

    public function cancel_gift_card($id, $data = array())
    {
        $gc_data = $this->site->getGiftCardByID($id);
        $gc_data_arr = get_object_vars($gc_data);
        foreach ($gc_data_arr as $key => $value) {
            if (!in_array($key, ['id', 'document_type_id'])) {
                $data[$key] = $value;
            }
        }
        $customer_data = $this->site->getCompanyByID($gc_data->customer_id);
        $update_arr = ['status'=>($gc_data->creation_type == 4 ? 5 : 3)];
        $update_arr['balance'] = 0;
        $data['date'] = date('Y-m-d H:i:s');
        if ($gc_data->creation_type == 4) {
            $reference = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
            $data['reference_no'] = $reference;
            $ref = explode("-", $reference);
            $consecutive = $ref[1];

            $update_arr['balance'] = 0;
            $update_arr['return_reference_no'] = $data['reference_no'];
            $update_arr['return_document_type_id'] = $data['document_type_id'];
            $update_arr['return_date'] = date('Y-m-d H:i:s');
            $update_arr['return_by'] = $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id');

        }
        if ($this->db->update('gift_cards', $update_arr, ['id'=>$id])) {
            $this->db->insert('gift_card_topups', [
                                                'date'=>date('Y-m-d H:i:s'),
                                                'card_id'=>$id,
                                                'created_by'=>$this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                                'movement_type'=>2,
                                            ]);
            $this->site->wappsiContabilidadTarjetasRegalo($data, true);
            if ($gc_data->creation_type != 4) {
                $this->db->update('companies', ['award_points'=>$customer_data->award_points + $gc_data->ca_points_redeemed], ['id'=>$gc_data->customer_id]);
            } else if ($gc_data->creation_type == 4) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            }
            return true;
        }
        return false;
    }

    public function deleteGiftCard($id)
    {
        if ($this->db->delete('gift_cards', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getPaypalSettings()
    {
        $q = $this->db->get_where('paypal', array('id' => 1));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSkrillSettings()
    {
        $q = $this->db->get_where('skrill', array('id' => 1));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getQuoteByID($id)
    {
        $q = $this->db->get_where('quotes', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllQuoteItems($quote_id)
    {
        $q = $this->db->get_where('quote_items', array('quote_id' => $quote_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getOrderByID($id)
    {
        $this->db->select('order_sales.*,
                           addresses.sucursal as negocio,
                           addresses.direccion as direccion_negocio,
                           addresses.city as ciudad_negocio,
                           addresses.phone as phone_negocio,
                           tax_rates.rate as tax_rate,
                           tax_rates.type as type_tax_rate,
                           zones.zone_code as zone_code,
                           zones.zone_name as zone_name,
                           subzones.subzone_name as subzone_name,
                           delivery_time.time_1 as time_1,
                           delivery_time.time_2 as time_2,
                           documents_types.module_invoice_format_id,
                           companies.name as contact_name
                           ')
            ->where(array('order_sales.id' => $id))
            ->join('addresses', 'addresses.id = order_sales.address_id', 'left')
            ->join('zones', 'addresses.location = zones.id', 'left')
            ->join('subzones', 'addresses.subzone = subzones.id', 'left')
            ->join('delivery_time', 'order_sales.delivery_time_id = delivery_time.id', 'left')
            ->join('documents_types', 'documents_types.id = order_sales.document_type_id', 'left')
            ->join('tax_rates', 'tax_rates.id = order_sales.order_tax_id', 'left')
            ->join('companies', 'companies.id = order_sales.contact_id', 'left')
            ;
        $q = $this->db->get('order_sales', 1);

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSaleByID($id, $arr = false)
    {
        $this->db->select('sales.*,
                           addresses.sucursal as negocio,
                           addresses.direccion as direccion_negocio,
                           addresses.city as ciudad_negocio,
                           addresses.state as state_negocio,
                           addresses.country as country_negocio,
                           addresses.phone as phone_negocio,
                           addresses.email as email_negocio,
                           zones.zone_name as zone_negocio,
                           subzones.subzone_name as subzone_negocio,
                           tax_rates.rate as tax_rate,
                           tax_rates.type as type_tax_rate,
                           documents_types.module_invoice_format_id,
                           documents_types.factura_contingencia,
                           delivery_time.time_1 as time_1,
                           delivery_time.time_2 as time_2,
                           zones.zone_code as zone_code,
                           zones.zone_name as zone_name,
                           ('.$this->db->dbprefix("sales").'.rete_fuente_total -
                            SUM(
                                IF('.$this->db->dbprefix("payments").'.rete_fuente_id IS NOT NULL,
                                    COALESCE('.$this->db->dbprefix("payments").'.rete_fuente_total,0),
                                    0
                                )
                            )
                           ) rete_fuente_total,
                           ('.$this->db->dbprefix("sales").'.rete_iva_total -
                            SUM(
                                IF('.$this->db->dbprefix("payments").'.rete_iva_id IS NOT NULL,
                                    COALESCE('.$this->db->dbprefix("payments").'.rete_iva_total,0),
                                    0
                                )
                            )
                           ) rete_iva_total,
                           ('.$this->db->dbprefix("sales").'.rete_ica_total -
                            SUM(
                                IF('.$this->db->dbprefix("payments").'.rete_ica_id IS NOT NULL,
                                    COALESCE('.$this->db->dbprefix("payments").'.rete_ica_total,0),
                                    0
                                )
                            )
                           ) rete_ica_total,
                           ('.$this->db->dbprefix("sales").'.rete_other_total -
                            SUM(
                                IF('.$this->db->dbprefix("payments").'.rete_other_id IS NOT NULL,
                                    COALESCE('.$this->db->dbprefix("payments").'.rete_other_total,0),
                                    0
                                )
                            )
                           ) rete_other_total
                        ')
            ->where(array('sales.id' => $id))
            ->join('addresses', 'addresses.id = sales.address_id', 'left')
            ->join('documents_types', 'sales.document_type_id = documents_types.id', 'left')
            ->join('tax_rates', 'tax_rates.id = sales.order_tax_id', 'left')
            ->join('payments', 'payments.sale_id = sales.id', 'left')
            ->join('delivery_time', 'sales.delivery_time_id = delivery_time.id', 'left')
            ->join('zones', 'addresses.location = zones.id', 'left')
            ->join('subzones', 'addresses.subzone = subzones.id', 'left')
            ->group_by('sales.id');
        $q = $this->db->get('sales', 1);

        if ($arr == false) {
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        } else {
            if ($q->num_rows() > 0) {
                return $q->row_array();
            }
        }
        return FALSE;
    }

    public function getStaff()
    {
        if (!$this->Owner) {
            $this->db->where('group_id !=', 1);
        }
        $this->db->where('group_id !=', 3)->where('group_id !=', 4);
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductVariantByName($name, $product_id)
    {
        $q = $this->db->get_where('product_variants', array('name' => $name, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateByName($name)
    {
        $q = $this->db->get_where('tax_rates', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function topupGiftCard($data = array(), $card_data = NULL)
    {
        $reference = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        $data['reference_no'] = $reference;
        $ref = explode("-", $reference);
        $consecutive = $ref[1];
        if ($this->db->insert('gift_card_topups', $data)) {
            $insert_id = $this->db->insert_id();
            $this->db->update('gift_cards', $card_data, array('id' => $data['card_id']));
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            $this->site->wappsi_contabilidad_giftcard_topup($data);
            return $insert_id;
        }
        return false;
    }

    public function getAllGCTopups($card_id)
    {
        $this->db->select("{$this->db->dbprefix('gift_card_topups')}.*, {$this->db->dbprefix('users')}.first_name, {$this->db->dbprefix('users')}.last_name, {$this->db->dbprefix('users')}.email")
        ->join('users', 'users.id=gift_card_topups.created_by', 'left')
        ->order_by('id', 'desc')->limit(10);
        $q = $this->db->get_where('gift_card_topups', array('card_id' => $card_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getItemRack($product_id, $warehouse_id)
    {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            $wh = $q->row();
            return $wh->rack;
        }
        return FALSE;
    }

    public function getSaleCosting($sale_id)
    {
        $q = $this->db->get_where('costing', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function UpdateCostingAndPurchaseItem($return_item, $product_id, $quantity)
    {
        $bln_quantity = $quantity;
        if ($costings = $this->getCostingLines($return_item['id'], $product_id)) {
            foreach ($costings as $costing) {
                if ($costing->quantity > $bln_quantity && $bln_quantity != 0) {
                    $qty = $costing->quantity - $bln_quantity;
                    $bln = $costing->quantity_balance && $costing->quantity_balance >= $bln_quantity ? $costing->quantity_balance - $bln_quantity : 0;
                    $this->db->update('costing', array('quantity' => $qty, 'quantity_balance' => $bln), array('id' => $costing->id));
                    $bln_quantity = 0;
                    break;
                } elseif ($costing->quantity <= $bln_quantity && $bln_quantity != 0) {
                    // $this->db->delete('costing', array('id' => $costing->id));
                    $this->db->update('costing', array('quantity' => 0), array('id' => $costing->id));
                    $bln_quantity = ($bln_quantity - $costing->quantity);
                }
            }
        }
        $clause = ['product_id' => $product_id, 'warehouse_id' => $return_item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $return_item['option_id']];
        $this->site->setPurchaseItem($clause, $quantity);
        $this->site->syncQuantity(null, null, null, $product_id);
    }

    public function getPrecioUnidad($id, $new_qty, $unit_price_id = null)
    {

        if ($this->Settings->prioridad_precios_producto != 5) {
            if ($unit_price_id) {
                $Sql = 'SELECT
                            UP.*,
                            (IF(U.operator = "*", 1 * UP.cantidad, 1 / UP.cantidad)) AS cantidad
                        FROM '.$this->db->dbprefix("unit_prices").' UP
                            INNER JOIN '.$this->db->dbprefix("units").' U ON U.id = UP.unit_id
                        WHERE unit_id = '.$unit_price_id.' AND
                              id_product = '.$id;

                $q = $this->db->query($Sql);
                if (!$q) {
                    exit($this->db->last_query());
                }
                if ($q->num_rows() > 0) {
                    return $q->row();
                }
            }  
        } else {
            $this->db->select('(IF(unit.operator = "*", 1 * cantidad, 1 / cantidad)) AS cantidad');
            $this->db->select('valor_unitario');
            $this->db->join('units', 'units.id = unit_prices.unit_id');
            $this->db->where('id_product =', $id);
            $this->db->where('cantidad <=', $new_qty);
            $this->db->where('valor_unitario >', 0);
            $this->db->order_by('cantidad', 'desc');
            $q = $this->db->get('unit_prices', 1);
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }

        return FALSE;
    }

    //Retenciones

    public function getWithHolding($type, $affects){
        $ano = (substr($this->db->database, -2, 2));
        if ($this->site->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $ano = $contabilidadSufijo;
        } else {
            if (!is_numeric($ano)) {
                $year_actual = date('Y');
                $ano = substr($year_actual, -2, 2);
            }
        }
        $sufijo = "_con".$ano;
        $ledgers = 'ledgers'.$sufijo;
        $wholdings = 'withholdings';

        if ($this->Settings->modulary == 1) {
            $conditions[$wholdings.'.affects'] = $affects;
            if ($type) {
                $conditions[$wholdings.'.type'] = $type;
            }
            // $q = $this->db->where($conditions)->get('withholdings'.$sufijo);
            $q = $this->db->select($wholdings.".*, ".$ledgers.".name as ledger_name")
                    ->join($ledgers, $ledgers.'.id = '.$wholdings.'.account_id')
                    ->where($conditions)
                    ->get($wholdings);
            if ($q->num_rows() > 0) {
               return $q->result_array();
            }
        } else {

            $conditions['affects'] = $affects;
            if ($type) {
                $conditions['type'] = $type;
            }
            $q = $this->db->where($conditions)->get($wholdings);
            if ($q->num_rows() > 0) {
               return $q->result_array();
            }
        }
        return FALSE;
    }

    public function recontabilizarVenta($sale_id, $from_edit = false){

        $sale = $this->getSaleByID($sale_id, true);
        $items = $this->getAllInvoiceItems(($sale['sale_status'] == 'returned' ? $sale['sale_id'] : $sale['id']), ($sale['sale_status'] == 'returned' ? $sale['id'] : null), true);
        $payments = $this->getPaymentsForSaleReAccounting($sale_id, true);
        $msg = '';

        if ($mAccountSettings = $this->site->getSettingsCon($sale['date'])) {
            if ($mAccountSettings->year_closed == 1) {
                $this->session->set_userdata('reaccount_error', true);
                return "La venta (".$sale['reference_no'].") no se contabilizó, año contable cerrado. </br>";
            }
        }

        if (strpos($sale['reference_no'], "SI") !== false) {
            $this->session->set_userdata('reaccount_error', true);
            return "La venta (".$sale['reference_no'].") es saldo inicial, no se contabilizó. </br>";
        }
        if ($sale['sale_status'] == 'pending') {
            $this->session->set_userdata('reaccount_error', true);
            return "La venta (".$sale['reference_no'].") tiene estado pendiente, no se contabilizó. </br>";
        }
        if(!$this->site->getEntryTypeNumberExisting($sale, true, ($sale['sale_status'] == 'returned' ? true : false))){
            $msg .= "La venta (".$sale['reference_no'].") no estaba contabilizada y se contabilizó. </br>";
        } else {
            $msg .= "La venta (".$sale['reference_no'].") ya estaba contabilizada y se reprocesó la contabilización. </br>";
        }
        $payment_interno = [];
        $payments_externo = [];
        $payments_multi = [];
        $has_payment_retention = false;
        $si_return = [];
        if ($payments) {
            foreach ($payments as $pmnt) {
                if (strpos($pmnt['note'], 'Retención posterior') !== false) {
                    if ($pmnt['rete_fuente_total'] > 0) {
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_fuente_total'] = $pmnt['rete_fuente_total'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_fuente_base'] = $pmnt['rete_fuente_base'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_fuente_account'] = $pmnt['rete_fuente_account'];
                    }
                    if ($pmnt['rete_iva_total'] > 0) {
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_iva_total'] = $pmnt['rete_iva_total'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_iva_base'] = $pmnt['rete_iva_base'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_iva_account'] = $pmnt['rete_iva_account'];
                    }
                    if ($pmnt['rete_ica_total'] > 0) {
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_ica_total'] = $pmnt['rete_ica_total'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_ica_base'] = $pmnt['rete_ica_base'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_ica_account'] = $pmnt['rete_ica_account'];
                    }
                    if ($pmnt['rete_other_total'] > 0) {
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_other_total'] = $pmnt['rete_other_total'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_other_base'] = $pmnt['rete_other_base'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_other_account'] = $pmnt['rete_other_account'];
                    }
                    if ($pmnt['rete_autoaviso_total'] > 0) {
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_autoaviso_total'] = $pmnt['rete_autoaviso_total'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_autoaviso_base'] = $pmnt['rete_autoaviso_base'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_autoaviso_account'] = $pmnt['rete_autoaviso_account'];
                    }
                    if ($pmnt['rete_bomberil_total'] > 0) {
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_bomberil_total'] = $pmnt['rete_bomberil_total'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_bomberil_base'] = $pmnt['rete_bomberil_base'];
                        $si_return[0]['rc_retentions_data'][$pmnt['reference_no']]['rete_bomberil_account'] = $pmnt['rete_bomberil_account'];
                    }
                }
                if ($pmnt['multi_payment'] == 1) {
                    $payments_multi[] = $pmnt;
                    if ($pmnt['rete_fuente_total'] > 0 || $pmnt['rete_iva_total'] > 0 || $pmnt['rete_ica_total'] > 0 || $pmnt['rete_other_total'] > 0) {
                        $has_payment_retention = true;
                    }
                } else {
                    if ($pmnt['paid_by'] != 'retencion') {
                        if ($sale['sale_status'] != 'returned') {
                            $pmnt['pmnt_data'] = $this->site->getPaymentMethodByCode($pmnt['paid_by']);
                            $payment_interno[] = $pmnt;
                        } else {
                            $payments_externo[] = $pmnt;
                        }
                    }
                }
            }
        }
        if ($this->Settings->tax_rate_traslate && $sale['total_tax'] > 0 && count($payment_interno) > 0) {
            $inv_items = $items;
            $sale_taxes = [];
            $pagadoBB = $payment_interno['amount'];
            foreach ($inv_items as $row) {
                $proporcion_pago = ($pagadoBB * 100) / $sale['grand_total'];
                // $total_iva = ($row['item_tax'] + $row['item_tax_2']) * ($proporcion_pago / 100);
                $total_iva = ($row['item_tax']) * ($proporcion_pago / 100);
                if (!isset($sale_taxes[$row['tax_rate_id']])) {
                    $sale_taxes[$row['tax_rate_id']] = $total_iva;
                } else {
                    $sale_taxes[$row['tax_rate_id']] += $total_iva;
                }
            }
            $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
            $data_tax_rate_traslate = $data_taxrate_traslate = array(
                    'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->receipt_ledger_id,
                    'sale_taxes' => $sale_taxes,
                );
        }
        $cost = $this->site->costing($items, true);
        $item_costing_each = [];
        $nc_oc = false;
        foreach ($items as $item) {
            if ($item['product_id'] == '999999999') {
                $nc_oc = true;
            }
            $item_costs = $this->site->get_sale_item_costing_accounted($sale_id, $item['product_id'], true);
            $item_costing_each[$item['product_id']] = $item_costs;
            foreach ($item_costs as $item_cost) {
                if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                    $item_cost['sale_item_id'] = $item['id'];
                    $item_cost['sale_id'] = $sale_id;
                    $item_cost['date'] = date('Y-m-d', strtotime($sale['date']));
                } else {
                    foreach ($item_cost as $ic) {
                        $ic['sale_item_id'] = $item['id'];
                        $ic['sale_id'] = $sale_id;
                        $ic['date'] = date('Y-m-d', strtotime($sale['date']));
                        if(! isset($ic['pi_overselling'])) {
                            $this->db->insert('costing', $ic);
                        }
                    }
                }
            }
        }
        // $this->sma->print_arrays($items);
        if ($sale['sale_status'] == 'returned') {
            $payment_dev[0] = $this->getPaymentsForSaleDev($sale_id, true);
            if ($nc_oc) {
                $this->site->wappsiContabilidadVentasDevolucionOtherConcepts($sale['sale_id'], $sale, $items[0], $payments_externo);
            } else {
                $this->site->wappsiContabilidadVentasDevolucion($sale['sale_id'], $sale, $items, $payments_externo, $si_return, $cost, $from_edit);
            }
        } else {
            if ($nc_oc) {
                $this->site->wappsiContabilidadVentasDebitNotes($sale_id, $sale, $items[0], $payments_externo);
            } else {
                $this->site->wappsiContabilidadVentas($sale_id, $sale, $items, $payment_interno, $cost, (isset($data_taxrate_traslate) ? $data_taxrate_traslate : null), $item_costing_each, $has_payment_retention, $from_edit);
            }
        }
        if ($this->session->userdata('swal_flash_message')) {
            return $this->session->userdata('swal_flash_message');
        } else {
            return $msg;
        }
    }

    public function syncSaleReturns($sale_id){
        $sale_devs = $this->db->where('id', $sale_id)->get('sales');
        $actualizadas = 0;
        $insertadas = 0;
        if ($sale_devs->num_rows() > 0) {
            foreach (($sale_devs->result()) as $sd) {
                $sale_item_devs = $this->db->where('sale_id', $sd->id)->get('sale_items');
                if ($sale_item_devs->num_rows() > 0) {
                    foreach (($sale_item_devs->result()) as $sid) {
                        // $this->db->delete('costing', array('sale_item_id' => $sid->id, 'sale_id' => $sd->id));
                        $item_cost = $this->db->get_where('costing', array('sale_item_id' => $sid->id, 'sale_id' => $sd->id));
                        if ($item_cost->num_rows() > 0) {
                            $item_cost = $item_cost->row();
                            $data = array(
                                'date' => $sd->date,
                                // 'product_id' => $sid->product_id,
                                // 'sale_item_id' => $sid->id,
                                // 'sale_id' => $sid->sale_id,
                                // 'quantity' => $sid->quantity,
                                'sale_net_unit_price' => $sid->net_unit_price,
                                'sale_unit_price' => $sid->unit_price,
                                'overselling' => $this->Settings->overselling,
                                // 'purchase_item_id' => $sid->,
                                // 'purchase_net_unit_cost' => $sid->,
                                // 'purchase_unit_cost' => $sid->,
                                // 'quantity_balance' => $sid->,
                                // 'inventory' => $sid->,
                                // 'option_id' => NULL
                            );
                            $this->db->update('costing', $data, array('id' => $item_cost->id));
                            $actualizadas++;
                        } else  {
                            $data = array(
                                'date' => $sd->date,
                                'product_id' => $sid->product_id,
                                'sale_item_id' => $sid->id,
                                'sale_id' => $sid->sale_id,
                                'quantity' => 0,
                                'sale_net_unit_price' => $sid->net_unit_price,
                                'sale_unit_price' => $sid->unit_price,
                                'overselling' => $this->Settings->overselling,
                                // 'purchase_item_id' => $sid->,
                                // 'purchase_net_unit_cost' => $sid->,
                                // 'purchase_unit_cost' => $sid->,
                                // 'quantity_balance' => $sid->,
                                // 'inventory' => $sid->,
                                // 'option_id' => NULL
                            );

                            $this->db->insert('costing', $data);
                            $insertadas++;
                        }
                    }
                }
            }
            if ($actualizadas > 0 || $insertadas > 0) {
                return array('actualizadas' => $actualizadas, 'insertadas' => $insertadas);
            }
        } else {
            return false;
        }
    }


    public function getSalesPaymentPending($customer_id, $address_id = null, $biller_id = null, $sale_id = null, $rows_start = 0, $num_rows = 30){

        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
            $biller_id = $this->session->userdata('biller_id');
        }

        $this->db
              ->select('
                        id,
                        date,
                        reference_no,
                        customer_id,
                        customer,
                        biller_id,
                        biller,
                        warehouse_id,
                        total,
                        product_discount,
                        order_discount_id,
                        total_discount,
                        order_discount,
                        product_tax,
                        order_tax_id,
                        order_tax,
                        total_tax,
                        shipping,
                        grand_total,
                        sale_status,
                        payment_status,
                        payment_term,
                        due_date,
                        created_by,
                        updated_by,
                        updated_at,
                        total_items,
                        pos,
                        paid,
                        return_id,
                        surcharge,
                        attachment,
                        return_sale_ref,
                        sale_id,
                        return_sale_total,
                        rounding,
                        api,
                        shop,
                        address_id,
                        seller_id,
                        reserve_id,
                        hash,
                        manual_payment,
                        cgst,
                        sgst,
                        igst,
                        payment_method,
                        pay_partner,
                        rete_fuente_percentage,
                        rete_fuente_total,
                        rete_fuente_account,
                        rete_fuente_base,
                        rete_iva_percentage,
                        rete_iva_total,
                        rete_iva_account,
                        rete_iva_base,
                        rete_ica_percentage,
                        rete_ica_total,
                        rete_ica_account,
                        rete_ica_base,
                        rete_other_percentage,
                        rete_other_total,
                        rete_other_account,
                        rete_other_base,
                        sale_currency,
                        sale_currency_trm,
                        cost_center_id,
                        document_type_id,
                        tip_amount,
                        shipping_in_grand_total,
                        sale_origin,
                        sale_origin_reference_no,
                        rete_fuente_id,
                        rete_iva_id,
                        rete_ica_id,
                        rete_other_id,
                        sale_comm_perc,
                        collection_comm_perc,
                        sale_comm_amount,
                        sale_comm_payment_status,
                        debit_note_id,
                        reference_debit_note,
                        reference_invoice_id,
                        restobar_table_id,
                        consumption_sales,
                        self_withholding_amount,
                        self_withholding_percentage,
                        printed,
                        due_payment_method_id,
                        registration_date,
                        return_other_concepts,
                        rete_bomberil_percentage,
                        rete_bomberil_total,
                        rete_bomberil_account,
                        rete_bomberil_base,
                        rete_autoaviso_percentage,
                        rete_autoaviso_total,
                        rete_autoaviso_account,
                        rete_autoaviso_base,
                        rete_autoica_percentage,
                        rete_autoica_total,
                        rete_autoica_account,
                        rete_autoica_base,
                        rete_autoica_account_counterpart,
                        rete_autoaviso_account_counterpart,
                        rete_bomberil_account_counterpart,
                        rete_bomberil_id,
                        rete_autoaviso_id,
                        aiu_admin_total,
                        aiu_imprev_total,
                        aiu_utilidad_total
                        ')
                ->where('customer_id', $customer_id)
              ->where('payment_status !=', 'paid')
              ->where('sale_status !=', 'returned')
              ->where('sale_status !=', 'pending')
              ->where('grand_total > paid');
        if (isset($address_id)) {
              $this->db->where('address_id', $address_id);
        }
        if (isset($biller_id)) {
              $this->db->where('biller_id', $biller_id);
        }
        if (isset($sale_id)) {
              $this->db->where('id', $sale_id);
        }
        $sales_pending = $this->db->order_by('date asc')
                                  ->limit($num_rows, $rows_start)
                                  ->get('sales');
        if ($sales_pending->num_rows() > 0) {
            foreach (($sales_pending->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }

        return false;
    }


    public function updateSalesPayments($paid_by, $payments, $customer_id, $retenciones, $conceptos, $cost_center, $deposit, $manual_reference, $biller_id){
        $total_paid = 0;
        $reference_no = '';
        $note = '';
        $date = '';
        if (count($payments) > 0) {
            $ref_p = $payments[0]['reference_no'];
            $consecutive = explode("-", $ref_p);
            $consecutive = $consecutive[1];
            $document_type_id = $payments[0]['document_type_id'];
            $update_consecutive = true;
            if ($manual_reference) {
                $update_consecutive = false;
            }
        } else if (count($conceptos) > 0) {
            $ref_p = $conceptos[0]['reference_no'];
            $consecutive = explode("-", $ref_p);
            $consecutive = $consecutive[1];
            $document_type_id = $conceptos[0]['document_type_id'];
            $update_consecutive = true;
            if ($manual_reference) {
                $update_consecutive = false;
            }
        }

        if (count($payments) > 0) {
            if ($paid_by == 'deposit') {
                $payments = $this->site->set_payments_affected_by_deposits($customer_id, $payments);
                if ($conceptos && count($conceptos) > 0) {
                    foreach ($conceptos as $key => $concepto) {
                        $ordenar_conceptos[$key] = $concepto['amount'];
                    }
                    array_multisort($ordenar_conceptos, SORT_ASC, $conceptos);
                    $conceptos = $this->site->set_payments_affected_by_deposits($customer_id, $conceptos);
                }
            }
            foreach ($payments as $key => $data) { //SE RECORRE PARA INSERTAR EN BD Y SINCRONIZACIÓN DE FACTURAS$data['reference_no'] = $ref_p;
                $total_paid += $data['amount'];
                $reference_no = $data['reference_no'];
                $note = $data['note'];
                $date = $data['date'];
                if ($this->db->insert('payments', $data)) {
                    $payments[$key]['pmnt_insert_id'] = $this->db->insert_id();
                    $this->site->syncSalePayments($data['sale_id']);
                    if ($data['paid_by'] == 'gift_card') {
                        $gc = $this->site->getGiftCardByNO($data['cc_no']);
                        $this->db->update('gift_cards', array('balance' => ($gc->balance - $data['amount'])), array('card_no' => $data['cc_no']));
                        $this->site->update_gift_card_balance_status(NULL, $data['cc_no']);
                    }
                    //SE COMENTA PARA NO AFECTAR A VENTA, DATOS DE RETENCIÓN SÓLO AFECTAN RECIBO DE CAJA
                    // if (isset($retenciones[$data['sale_id']])) {
                    //     $retencion = $retenciones[$data['sale_id']];
                    //     $sale = $this->db->get_where('sales', array('id' => $data['sale_id']))->row();
                    //     $payments[$key]['cost_center_id'] = $cost_center;
                    //     foreach ($retencion as $rete => $valor) {
                    //         if ($rete != 'rete_fuente_percentage' && $rete != 'rete_iva_percentage' && $rete != 'rete_ica_percentage' && $rete != 'rete_other_percentage') {
                    //             $prev_rete = (Double) $sale->{$rete};
                    //             $prev_rete += (Double) $valor;
                    //             $retencion[$rete] = $prev_rete;
                    //         }
                    //     }
                    //     $this->db->update('sales', $retencion, array('id' => $data['sale_id']));
                    //     unset($retencion);
                    // }
                } else {
                    return false;
                }
            }

            foreach ($conceptos as $id => $concepto) {
                $ledger = $concepto['ledger_id'];
                $concepto['concept_ledger_id'] = $concepto['ledger_id'];
                $conceptos[$id]['concept_ledger_id'] = $concepto['ledger_id'];
                $reference_no = $concepto['reference_no'];
                unset($concepto['ledger_id']);
                $this->db->insert('payments', $concepto);
            }
        }

        if ($this->Settings->modulary == 1) { //Si se indicó que el POS es modular con Contabilidad.
            $data_tax_rate_traslate = [];
            $payment = array(
                            'reference_no' => $reference_no,
                            'biller_id' => $biller_id,
                            'amount' => $total_paid,
                            'date' => $date,
                            'payments' => $payments,
                            'companies_id' => $customer_id,
                            'note' => $note,
                            'document_type_id' => $document_type_id,
                            'created_by' => $payments[0]['created_by']
                            );
            if($this->site->wappsiContabilidadPagosMultiplesVentas($payment, $customer_id, $retenciones, $data_tax_rate_traslate, $conceptos, $cost_center)){}
        }
        if ($update_consecutive) {
            $this->site->updateBillerConsecutive($document_type_id, $consecutive+1);
        }
        if ($deposit) {
            // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
            $referenceBiller = $this->site->getReferenceBiller($deposit['biller_id'], $deposit['document_type_id']);
            if($referenceBiller){
                $reference_deposit = $referenceBiller;
            }else{
                $reference_deposit = $this->site->getReference('dp');
            }
            $deposit['reference_no'] = $reference_deposit;
            $deposit['origen_reference_no'] = $ref_p;
            $ref = explode("-", $reference_deposit);
            $deposit_consecutive = $ref[1];
            // NUEVA REFERENCIA POR REGISTRO DE DEPÓSITO AUTOMÁTICO
            $customer = $this->site->getCompanyByID($customer_id);
            if ($this->db->insert('deposits', $deposit)) {
                $this->site->updateBillerConsecutive($deposit['document_type_id'], $deposit_consecutive+1);
                $this->site->wappsiContabilidadDeposito($deposit);
                $this->db->update('companies', ['deposit_amount' => ($customer->deposit_amount + $deposit['amount'])], ['id'=> $customer->id]);
            }
        }
        return true;
    }

    public function complete_convert_order($order_id){
        $oitems = $this->db->get_where('order_sale_items', ['sale_id' => $order_id]);
        if ($oitems->num_rows() > 0) {
            foreach (($oitems->result()) as $oitem) {
                $this->db->update('order_sale_items', ['quantity_to_bill' => ($oitem->quantity_delivered > 0 ? $oitem->quantity - $oitem->quantity_delivered : $oitem->quantity )], ['id' => $oitem->id]);
            }
        }
        return true;
    }

    public function get_sale_multi_payments_by_reference($reference_no){
        $q = $this->db->where(['multi_payment' => 1, 'reference_no' => $reference_no])->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_order_sale_by_reference($reference_no){
        $q = $this->db->get_where('order_sales', ['reference_no' => $reference_no]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_email_customer($customer_id)
    {
        $this->db->select("email");

        if ($this->Settings->send_electronic_invoice_to == PRINCIPAL) {
            $this->db->from("companies");
            $this->db->where("id", $customer_id);
        } else {
            $this->db->from("addresses");
            $this->db->where("company_id", $customer_id);
        }

        $response = $this->db->get();
        return $response->row();
    }

    public function get_biller_document_type($biller_id, $module, $fe = false){
        $this->db->select('documents_types.*')
                      ->join('documents_types', 'documents_types.id = biller_documents_types.document_type_id AND documents_types.module = '.$module)
                      ->where('biller_documents_types.biller_id', $biller_id)
                      ->limit(1);
        if ($fe) {
            $this->db->where('documents_types.factura_electronica', 1);
        } else {
            $this->db->where('documents_types.factura_electronica !=', 1);
        }
        $q = $this->db->get('biller_documents_types');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function return_sale_for_fe($sale_id, $debbug = false){

        $inv = $this->site->getSaleByID($sale_id);
        $inv_items = $this->getAllInvoiceItems($sale_id);
        $payment = $products = $si_return = [];

        $data_document_type_id = $this->get_biller_document_type($inv->biller_id, 3);
        // $this->sma->print_arrays($data_document_type_id);
        $data = array(
                        'date' => date('Y-m-d H:i:s'),
                        'sale_id' => $inv->id,
                        'address_id' => $inv->address_id,
                        'customer_id' => $inv->customer_id,
                        'customer' => $inv->customer,
                        'biller_id' => $inv->biller_id,
                        'biller' => $inv->biller,
                        'warehouse_id' => $inv->warehouse_id,
                        'note' => $inv->note,
                        'total' => 0-$inv->total,
                        'product_discount' => $inv->product_discount,
                        'order_discount_id' => $inv->order_discount_id,
                        'order_discount' => $inv->order_discount,
                        'total_discount' => $inv->total_discount,
                        'product_tax' => 0-$inv->product_tax,
                        'order_tax_id' => $inv->order_tax_id,
                        'order_tax' => $inv->order_tax,
                        'total_tax' => $inv->total_tax,
                        'surcharge' => $inv->surcharge,
                        'grand_total' => 0-$inv->grand_total,
                        'created_by' => $inv->created_by,
                        'return_sale_ref' => $inv->return_sale_ref,
                        'sale_status' => 'returned',
                        'pos' => $inv->pos,
                        'seller_id' => $inv->seller_id,
                        'document_type_id' => $inv->document_type_id,
                        'shipping' => $inv->shipping,
                        'payment_status' => $inv->payment_status == 'pending' || $inv->payment_status == 'due' ? 'paid' : 'partial',
                        'rete_fuente_percentage' => $inv->rete_fuente_percentage,
                        'rete_fuente_total' => $inv->rete_fuente_total,
                        'rete_fuente_account' => $inv->rete_fuente_account,
                        'rete_fuente_base' => $inv->rete_fuente_base,
                        'rete_iva_percentage' => $inv->rete_iva_percentage,
                        'rete_iva_total' => $inv->rete_iva_total,
                        'rete_iva_account' => $inv->rete_iva_account,
                        'rete_iva_base' => $inv->rete_iva_base,
                        'rete_ica_percentage' => $inv->rete_ica_percentage,
                        'rete_ica_total' => $inv->rete_ica_total,
                        'rete_ica_account' => $inv->rete_ica_account,
                        'rete_ica_base' => $inv->rete_ica_base,
                        'rete_other_percentage' => $inv->rete_other_percentage,
                        'rete_other_total' => $inv->rete_other_total,
                        'rete_other_account' => $inv->rete_other_account,
                        'rete_other_base' => $inv->rete_other_base,
                        'rete_fuente_id' => $inv->rete_fuente_id,
                        'rete_iva_id' => $inv->rete_iva_id,
                        'rete_ica_id' => $inv->rete_ica_id,
                        'rete_other_id' => $inv->rete_other_id,
                        'document_type_id' => $data_document_type_id->id,
                        'return_sale_ref' => $inv->reference_no,
                        'tip_amount' => 0-$inv->tip_amount,
                        'unique_field' => $this->sma->unique_field(),
                    );
        if ($inv->payment_status == 'paid' || $inv->payment_status == 'partial') {
            $pay_ref = $this->site->getReference('pay');
            $payments = $this->getPaymentsForSale($sale_id);
            foreach ($payments as $spayment) {
                $data_payment = array(
                    'date' => date('Y-m-d H:i:s'),
                    'reference_no' =>  strpos($spayment->reference_no, '-') !== false ? $pay_ref : $spayment->reference_no,
                    'amount' => 0-$spayment->amount,
                    'paid_by' => $spayment->paid_by,
                    'cheque_no' => $spayment->cheque_no,
                    'cc_no' => $spayment->cc_no,
                    'cc_holder' => $spayment->cc_holder,
                    'cc_month' => $spayment->cc_month,
                    'cc_year' => $spayment->cc_year,
                    'cc_type' => $spayment->cc_type,
                    'created_by' => $spayment->created_by,
                    'type' => 'returned',
                    'comm_base' => 0-$spayment->comm_base,
                    'comm_amount' => 0-$spayment->comm_amount,
                    'comm_perc' => $spayment->comm_perc,
                    'seller_id' => $spayment->seller_id,
                    'mean_payment_code_fe' => $spayment->mean_payment_code_fe
                );
                $payment[] = $data_payment;
            }
            if ($inv->payment_status == 'partial') {
                $pay_ref = $this->site->getReference('pay');
                $data_payment = array(
                    'date' => date('Y-m-d H:i:s'),
                    'reference_no' => $pay_ref,
                    'amount' => $inv->paid - $inv->grand_total,
                    'paid_by' => 'due',
                    'cheque_no' => NULL,
                    'cc_no' => NULL,
                    'cc_holder' => NULL,
                    'cc_month' => NULL,
                    'cc_year' => NULL,
                    'cc_type' => NULL,
                    'created_by' => $this->session->userdata('user_id'),
                    'type' => 'returned',
                    'mean_payment_code_fe' => "ZZZ"
                );
                $payment[] = $data_payment;
            }
        } else {
            $pay_ref = $this->site->getReference('pay');
            $data_payment = array(
                'date' => date('Y-m-d H:i:s'),
                'reference_no' => $pay_ref,
                'amount' => $data['grand_total'],
                'paid_by' => 'due',
                'cheque_no' => NULL,
                'cc_no' => NULL,
                'cc_holder' => NULL,
                'cc_month' => NULL,
                'cc_year' => NULL,
                'cc_type' => NULL,
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'returned',
                'mean_payment_code_fe' => "ZZZ"
            );
            $payment[] = $data_payment;
        }

        foreach ($inv_items as $item) {
            $product = array(
                'product_id' => $item->product_id,
                'product_code' => $item->product_code,
                'product_name' => $item->product_name,
                'product_type' => $item->product_type,
                'option_id' => $item->option_id,
                'net_unit_price' => $item->net_unit_price,
                'unit_price' => $item->unit_price,
                'quantity' => 0-$item->quantity,
                'product_unit_id' => $item->product_unit_id,
                'product_unit_code' => $item->product_unit_code,
                'unit_quantity' => 0-$item->quantity,
                'warehouse_id' => $item->warehouse_id,
                'item_tax' => 0-$item->item_tax,
                'tax_rate_id' => $item->tax_rate_id,
                'tax' => $item->tax,
                'discount' => $item->discount,
                'item_discount' => 0-$item->item_discount,
                'subtotal' => 0-$item->subtotal,
                'serial_no' => $item->serial_no,
                'real_unit_price' => $item->real_unit_price,
                'price_before_tax' => $item->price_before_tax,
            );

            $products[] = $product;

            $si_return[] = array(
                'id' => $item->id,
                'sale_id' => $sale_id,
                'product_id' => $item->product_id,
                'option_id' => $item->option_id,
                'quantity' => $item->quantity,
                'warehouse_id' => $inv->warehouse_id,
            );
        }

        if ($debbug) {
            $this->sma->print_arrays($data, $payment, $products, $si_return);
        } else {
            $this->session->unset_userdata('detal_post_processing_model');
            $this->sales_model->addSale($data, $products, $payment, $si_return);
            $this->site->syncSalePayments($sale_id);
        }
    }

    public function sale_for_fe($sale_id, $data, $debbug = false){

        $customer_details = $this->site->getCompanyByID($data['customer_id']);
        $biller_details = $this->site->getCompanyByID($data['biller_id']);
        $customer = !empty($customer_details->name) && $customer_details->name != '-' ? $customer_details->name : $customer_details->company;
        $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;

        $inv = $this->site->getSaleByID($sale_id);
        $inv_items = $this->getAllInvoiceItems($sale_id);
        $payment = $products = [];
        $cur_date = date('Y-m-d H:i:s');
        $data_document_type_id = $this->get_biller_document_type($inv->biller_id, 3);
        $data = array(
                        'date' => $cur_date,
                        'sale_id' => $inv->id,
                        'address_id' => $data['address_id'],
                        'resolucion' => $data['resolucion'],
                        'customer_id' => $data['customer_id'],
                        'customer' => $customer,
                        'biller_id' => $data['biller_id'],
                        'biller' => $biller,
                        'warehouse_id' => $inv->warehouse_id,
                        'note' => $data['note'],
                        'staff_note' => $data['staff_note'],
                        'total' => $inv->total,
                        'product_discount' => $inv->product_discount,
                        'order_discount_id' => $inv->order_discount_id,
                        'order_discount' => $inv->order_discount,
                        'total_discount' => $inv->total_discount,
                        'product_tax' => $inv->product_tax,
                        'order_tax_id' => $inv->order_tax_id,
                        'order_tax' => $inv->order_tax,
                        'total_tax' => $inv->total_tax,
                        'surcharge' => $inv->surcharge,
                        'grand_total' => $inv->grand_total,
                        'created_by' => $inv->created_by,
                        'return_sale_ref' => NULL,
                        'sale_status' => $inv->sale_status,
                        'pos' => 0,
                        'seller_id' => $data['seller_id'],
                        'shipping' => $inv->shipping,
                        'tip_amount' => $inv->tip_amount,
                        'payment_status' => $inv->payment_status == 'pending' ? 'paid' : 'partial',
                        'rete_fuente_percentage' => $inv->rete_fuente_percentage,
                        'rete_fuente_total' => $inv->rete_fuente_total,
                        'rete_fuente_account' => $inv->rete_fuente_account,
                        'rete_fuente_base' => $inv->rete_fuente_base,
                        'rete_iva_percentage' => $inv->rete_iva_percentage,
                        'rete_iva_total' => $inv->rete_iva_total,
                        'rete_iva_account' => $inv->rete_iva_account,
                        'rete_iva_base' => $inv->rete_iva_base,
                        'rete_ica_percentage' => $inv->rete_ica_percentage,
                        'rete_ica_total' => $inv->rete_ica_total,
                        'rete_ica_account' => $inv->rete_ica_account,
                        'rete_ica_base' => $inv->rete_ica_base,
                        'rete_other_percentage' => $inv->rete_other_percentage,
                        'rete_other_total' => $inv->rete_other_total,
                        'rete_other_account' => $inv->rete_other_account,
                        'rete_other_base' => $inv->rete_other_base,
                        'rete_fuente_id' => $inv->rete_fuente_id,
                        'rete_iva_id' => $inv->rete_iva_id,
                        'rete_ica_id' => $inv->rete_ica_id,
                        'rete_other_id' => $inv->rete_other_id,

                        'rete_bomberil_total' => $inv->rete_bomberil_total,
                        'rete_autoaviso_total' => $inv->rete_autoaviso_total,

                        'document_type_id' => $data['document_type_id'],
                        'sale_origin' => $data['sale_origin'],
                        'sale_origin_reference_no' => $data['sale_origin_reference_no'],

                        'payment_method_fe' => $inv->payment_method_fe,
                        'payment_mean_fe' => $inv->payment_mean_fe,

                        'sale_comm_perc' => $inv->sale_comm_perc,
                        'collection_comm_perc' => $inv->collection_comm_perc,
                        'sale_comm_amount' => $inv->sale_comm_amount,
                        'sale_comm_payment_status' => $inv->sale_comm_payment_status,
                        'consumption_sales' => $inv->consumption_sales,
                        'sale_currency' => $data['sale_currency'],
                        'sale_currency_trm' => $data['sale_currency_trm'],
                        'unique_field' => $this->sma->unique_field(),
                    );

            $payments = $this->getPaymentsForSale($sale_id);
            $data_document_type_id = $this->get_biller_document_type($data['biller_id'], 14);
            if ($payments) {
                foreach ($payments as $spayment) {
                    $data_payment = array(
                        'date' => $cur_date,
                        'amount' => $spayment->amount,
                        'paid_by' => $spayment->paid_by,
                        'cheque_no' => $spayment->cheque_no,
                        'cc_no' => $spayment->cc_no,
                        'cc_holder' => $spayment->cc_holder,
                        'cc_month' => $spayment->cc_month,
                        'cc_year' => $spayment->cc_year,
                        'cc_type' => $spayment->cc_type,
                        'created_by' => $spayment->created_by,
                        'type' => $spayment->type,
                        'comm_base' => $spayment->comm_base,
                        'comm_amount' => $spayment->comm_amount,
                        'comm_perc' => $spayment->comm_perc,
                        'seller_id' => $spayment->seller_id,
                        'mean_payment_code_fe' => $spayment->mean_payment_code_fe,
                        'document_type_id' => $data_document_type_id->id,
                    );
                    $payment[] = $data_payment;
                }
            }


        foreach ($inv_items as $item) {
            $product = array(
                'product_id' => $item->product_id,
                'product_code' => $item->product_code,
                'product_name' => $item->product_name,
                'product_type' => $item->product_type,
                'option_id' => $item->option_id,
                'net_unit_price' => $item->net_unit_price,
                'unit_price' => $item->unit_price,
                'quantity' => $item->quantity,
                'product_unit_id' => $item->product_unit_id,
                'product_unit_code' => $item->product_unit_code,
                'unit_quantity' => $item->quantity,
                'warehouse_id' => $item->warehouse_id,
                'item_tax' => $item->item_tax,
                'tax_rate_id' => $item->tax_rate_id,
                'tax' => $item->tax,
                'item_tax_2' => $item->item_tax_2,
                'tax_rate_2_id' => $item->tax_rate_2_id,
                'tax_2' => $item->tax_2,
                'discount' => $item->discount,
                'item_discount' => $item->item_discount,
                'subtotal' => $item->subtotal,
                'serial_no' => $item->serial_no,
                'real_unit_price' => $item->real_unit_price,
                'price_before_tax' => $item->price_before_tax,
                'consumption_sales' => $item->consumption_sales,
            );

            $products[] = $product;
        }

        if ($debbug) {
            $this->sma->print_arrays($data, $payment, $products);
        } else {
            $this->session->unset_userdata('detal_post_processing_model');
            return $this->sales_model->addSale($data, $products, $payment);
        }
    }

    public function get_sale_by_term($term){
        $q = $this->db->like('reference_no', $term)
                      ->where('return_sale_ref', NULL)
                      ->limit(15)
                      ->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function update_payment_status_commisions($to_complete_payments_ids, $start_date, $end_date, $reference){
        // $this->sma->print_arrays($sales);
        $start_date = $this->sma->fld($start_date);
        $end_date = $this->sma->fld($end_date);
        foreach ($to_complete_payments_ids as $key => $payment_id) {
            $this->db->update('payments', ['comm_payment_status' => 1, 'comm_payment_date' => date('Y-m-d H:i:s'), 'comm_payment_reference_no' => $reference], ['id' => $payment_id]);
        }
    }

    public function delete_payment($reference, $data){
        $payment = $this->db->get_where('payments', ['reference_no' => $reference]);
        $payments = [];
        if ($payment->num_rows() > 0) {
            $entry_deleted = false;
            $reaccount_id = false;
            $reaccount_type = false;
            foreach (($payment->result()) as $row) {
                $payments[] = $row;
                if ($row->sale_id) {
                    $sale = $this->getSaleByID($row->sale_id);
                    if ($sale->return_id) {
                        return [
                                'response' => false,
                                'reaccount' => $reaccount_id,
                                'reaccount_type' => $reaccount_type,
                                'message' => sprintf(lang('payment_with_return'), $sale->reference_no),
                               ];
                    }
                }
                if ($row->paid_by == 'deposit') {
                    $deposit = $this->db->get_where('deposits', ['id' => $row->affected_deposit_id])->row();
                    $this->db->update('deposits', ['balance'=> $deposit->balance+($row->amount - ($row->rete_fuente_total + $row->rete_iva_total + $row->rete_ica_total + $row->rete_bomberil_total + $row->rete_autoaviso_total + $row->rete_other_total))], ['id'=>$deposit->id]);
                    $company = $this->db->get_where('companies', ['id' => $deposit->company_id])->row();
                    $this->db->update('companies', ['deposit_amount'=> $company->deposit_amount+($row->amount - ($row->rete_fuente_total + $row->rete_iva_total + $row->rete_ica_total + $row->rete_bomberil_total + $row->rete_autoaviso_total + $row->rete_other_total))], ['id'=>$company->id]);
                }

            }

            $multi_payments = [];
            $conceptos = [];
            $retenciones = [];
            $total_paid = 0;
            $referenceBiller = $this->site->getReferenceBiller($data['biller'], $data['document_type_id']);
            if($referenceBiller){
                $reference = $referenceBiller;
            }
            $reference_no = $reference;
            $ref = explode("-", $reference);
            $consecutive = $ref[1];
            foreach ($payments as $pmnt) {
                $txt = $this->site->text_delete_payment($pmnt);
                $updated = $this->db->insert('payments',
                                    [
                                        'date' => $data['date'],
                                        'note' => $txt,
                                        'reference_no' => $reference_no,
                                        'amount' => $pmnt->amount * -1,
                                        'sale_id' => $pmnt->sale_id,
                                        'rete_fuente_total' => $pmnt->rete_fuente_total * -1,
                                        'rete_fuente_percentage' => $pmnt->rete_fuente_percentage,
                                        'rete_fuente_base' => $pmnt->rete_fuente_base * -1,
                                        'rete_fuente_account' => $pmnt->rete_fuente_account,
                                        'rete_fuente_id' => $pmnt->rete_fuente_id,
                                        'rete_iva_total' => $pmnt->rete_iva_total * -1,
                                        'rete_iva_percentage' => $pmnt->rete_iva_percentage,
                                        'rete_iva_base' => $pmnt->rete_iva_base * -1,
                                        'rete_iva_account' => $pmnt->rete_iva_account,
                                        'rete_iva_id' => $pmnt->rete_iva_id,
                                        'rete_ica_total' => $pmnt->rete_ica_total * -1,
                                        'rete_ica_percentage' => $pmnt->rete_ica_percentage,
                                        'rete_ica_base' => $pmnt->rete_ica_base * -1,
                                        'rete_ica_account' => $pmnt->rete_ica_account,
                                        'rete_ica_id' => $pmnt->rete_ica_id,
                                        'rete_other_total' => $pmnt->rete_other_total * -1,
                                        'rete_other_percentage' => $pmnt->rete_other_percentage,
                                        'rete_other_base' => $pmnt->rete_other_base * -1,
                                        'rete_other_account' => $pmnt->rete_iva_account,
                                        'rete_other_id' => $pmnt->rete_other_id,
                                        'rete_bomberil_total' => $pmnt->rete_bomberil_total * -1,
                                        'rete_bomberil_percentage' => $pmnt->rete_bomberil_percentage,
                                        'rete_bomberil_base' => $pmnt->rete_bomberil_base * -1,
                                        'rete_bomberil_account' => $pmnt->rete_bomberil_account,
                                        'rete_bomberil_id' => $pmnt->rete_bomberil_id,
                                        'rete_autoaviso_total' => $pmnt->rete_autoaviso_total * -1,
                                        'rete_autoaviso_percentage' => $pmnt->rete_autoaviso_percentage,
                                        'rete_autoaviso_base' => $pmnt->rete_autoaviso_base * -1,
                                        'rete_autoaviso_account' => $pmnt->rete_autoaviso_account,
                                        'rete_autoaviso_id' => $pmnt->rete_autoaviso_id,
                                        'document_type_id' => $data['document_type_id'],
                                        'multi_payment' => $pmnt->multi_payment,
                                        'return_id' => $pmnt->id,
                                        'return_reference_no' => $pmnt->reference_no,
                                        'paid_by' => $pmnt->paid_by,
                                        'created_by' => $this->session->userdata('user_id'),
                                        'type' => 'cancelled',
                                        'affected_deposit_id' => $pmnt->affected_deposit_id,
                                        'concept_ledger_id' => $pmnt->concept_ledger_id,
                                        'company_id' => $pmnt->company_id,
                                        'pm_reteica_value' => $pmnt->pm_reteica_value,
                                        'pm_reteiva_value' => $pmnt->pm_reteiva_value,
                                        'pm_retefuente_value' => $pmnt->pm_retefuente_value,
                                        'pm_commision_value' => $pmnt->pm_commision_value,
                                        'seller_id' => $pmnt->seller_id,
                                        'concept_company_id' => $pmnt->concept_company_id,
                                        'concept_base' => $pmnt->concept_base,
                                    ]
                                );

                if ($updated) { //SI SE ACTUALIZÓ CORRECTAMENTE EL PAGO
                    $payment_return_id = $this->db->insert_id();
                    if ($pmnt->sale_id) {
                        $this->site->syncSalePayments($pmnt->sale_id); //SINCRONIZACIÓN DE VENTA CON LOS PAGOS QUE QUEDARON
                        $sale = $this->getSaleByID($pmnt->sale_id);

                        // $this->db->update('sales',
                        //                         [
                        //                             'rete_fuente_total' => $sale->rete_fuente_total - $pmnt->rete_fuente_total,
                        //                             'rete_fuente_base' => $sale->rete_fuente_base - $pmnt->rete_fuente_base,
                        //                             'rete_iva_total' => $sale->rete_iva_total - $pmnt->rete_iva_total,
                        //                             'rete_iva_base' => $sale->rete_iva_base - $pmnt->rete_iva_base,
                        //                             'rete_ica_total' => $sale->rete_ica_total - $pmnt->rete_ica_total,
                        //                             'rete_ica_base' => $sale->rete_ica_base - $pmnt->rete_ica_base,
                        //                             'rete_other_total' => $sale->rete_other_total - $pmnt->rete_other_total,
                        //                             'rete_other_base' => $sale->rete_other_base - $pmnt->rete_other_base,
                        //                             'rete_bomberil_total' => $sale->rete_bomberil_total - $pmnt->rete_bomberil_total,
                        //                             'rete_autoaviso_base' => $sale->rete_autoaviso_base - $pmnt->rete_autoaviso_base,
                        //                         ],
                        //                         [
                        //                             'id' => $sale->id
                        //                         ]);
                    }
                    if (!$pmnt->multi_payment) { //Si no es pago múltiple, si no uno directo de la factura, se activa bandera para recontabilizar la venta o compra según sea el caso
                        if ($pmnt->sale_id) {
                            if ($pmnt->date == $sale->date) {
                                $reaccount_id = $pmnt->sale_id;
                                $reaccount_type = 2;
                            }
                        }
                    }
                    $this->db->update('payments', ['return_id' => $payment_return_id, 'return_reference_no' => $reference_no], ['id' => $pmnt->id]);
                }

                if ($pmnt->sale_id) {
                    $payment = [];
                    $total_paid += $pmnt->amount;
                    foreach ($pmnt as $key => $value) {
                        $payment[$key] = $value;
                    }
                    $multi_payments[] = $payment;
                    if ($pmnt->rete_fuente_total > 0) {
                        $retenciones[$pmnt->sale_id]['rete_fuente_total'] = $pmnt->rete_fuente_total;
                        $retenciones[$pmnt->sale_id]['rete_fuente_percentage'] = $pmnt->rete_fuente_percentage;
                        $retenciones[$pmnt->sale_id]['rete_fuente_base'] = $pmnt->rete_fuente_base;
                        $retenciones[$pmnt->sale_id]['rete_fuente_account'] = $pmnt->rete_fuente_account;
                    }
                    if ($pmnt->rete_iva_total > 0) {
                        $retenciones[$pmnt->sale_id]['rete_iva_total'] = $pmnt->rete_iva_total;
                        $retenciones[$pmnt->sale_id]['rete_iva_percentage'] = $pmnt->rete_iva_percentage;
                        $retenciones[$pmnt->sale_id]['rete_iva_base'] = $pmnt->rete_iva_base;
                        $retenciones[$pmnt->sale_id]['rete_iva_account'] = $pmnt->rete_iva_account;
                    }
                    if ($pmnt->rete_ica_total > 0) {
                        $retenciones[$pmnt->sale_id]['rete_ica_total'] = $pmnt->rete_ica_total;
                        $retenciones[$pmnt->sale_id]['rete_ica_percentage'] = $pmnt->rete_ica_percentage;
                        $retenciones[$pmnt->sale_id]['rete_ica_base'] = $pmnt->rete_ica_base;
                        $retenciones[$pmnt->sale_id]['rete_ica_account'] = $pmnt->rete_ica_account;
                    }
                    if ($pmnt->rete_other_total > 0) {
                        $retenciones[$pmnt->sale_id]['rete_other_total'] = $pmnt->rete_other_total;
                        $retenciones[$pmnt->sale_id]['rete_other_percentage'] = $pmnt->rete_other_percentage;
                        $retenciones[$pmnt->sale_id]['rete_other_base'] = $pmnt->rete_other_base;
                        $retenciones[$pmnt->sale_id]['rete_other_account'] = $pmnt->rete_other_account;
                    }
                    if ($pmnt->rete_bomberil_total > 0) {
                        $retenciones[$pmnt->sale_id]['rete_bomberil_total'] = $pmnt->rete_bomberil_total;
                        $retenciones[$pmnt->sale_id]['rete_bomberil_percentage'] = $pmnt->rete_bomberil_percentage;
                        $retenciones[$pmnt->sale_id]['rete_bomberil_base'] = $pmnt->rete_bomberil_base;
                        $retenciones[$pmnt->sale_id]['rete_bomberil_account'] = $pmnt->rete_bomberil_account;
                    }
                    if ($pmnt->rete_autoaviso_total > 0) {
                        $retenciones[$pmnt->sale_id]['rete_autoaviso_total'] = $pmnt->rete_autoaviso_total;
                        $retenciones[$pmnt->sale_id]['rete_autoaviso_percentage'] = $pmnt->rete_autoaviso_percentage;
                        $retenciones[$pmnt->sale_id]['rete_autoaviso_base'] = $pmnt->rete_autoaviso_base;
                        $retenciones[$pmnt->sale_id]['rete_autoaviso_account'] = $pmnt->rete_autoaviso_account;
                    }
                } else {
                    $concepto = [];
                    $total_paid += $pmnt->amount;
                    foreach ($pmnt as $key => $value) {
                        if ($key == 'concept_ledger_id') {
                            $concepto['ledger_id'] = $value;
                            unset($pmnt->{$key});
                        }
                        $concepto[$key] = $value;
                    }
                    $conceptos[] = $concepto;
                }

            }

            $payment = array(
                            'reference_no' => $reference_no,
                            'amount' => $total_paid,
                            'date' => $data['date'],
                            'payments' => $multi_payments,
                            'companies_id' => (isset($data['company_id'])) ? $data['company_id'] : $data['customer_id'],
                            'document_type_id' => $data['document_type_id'],
                            'biller_id' => $data['biller'],
                            'created_by' => $data['created_by'],
                            'note' => '',
                            );
            $data_tax_rate_traslate = [];
            if($this->site->wappsiContabilidadPagosMultiplesVentasAnulacion($payment, $data['customer_id'], $retenciones, $data_tax_rate_traslate, $conceptos, $data['cost_center_id'])){}

            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);

            return [
                    'response' => true,
                    'reaccount' => $reaccount_id,
                    'reaccount_type' => $reaccount_type,
                    'message' => lang('payment_succesfully_cancelled'),
                   ];
        }
        return false;
    }

    public function getAllReturnsItems($sale_id, $arr = false)
    {
        $this->db->select('sale_items.*,
            tax_rates.code as tax_code,
            tax_rates.name as tax_name,
            tax_rates.rate as tax_rate,
            tax_rates.tax_indicator as tax_indicator,
            products.image,
            products.details as details,
            product_variants.name as variant,
            products.hsn_code as hsn_code,
            products.second_name as second_name,
            brands.name as brand_name,
            products.reference as reference,
            units.code as product_unit_code,
            units.operation_value,
            units.operator')
            ->join('sale_items', 'sale_items.sale_id = sales_returns.return_id', 'left')
            ->join('products', 'products.id = sale_items.product_id', 'left')
            ->join('brands', 'brands.id = products.brand', 'left')
            ->join('units', $this->db->dbprefix('units').'.id = IF('.$this->db->dbprefix('sale_items').'.product_unit_id_selected IS NOT NULL, '.$this->db->dbprefix('sale_items').'.product_unit_id_selected, '.$this->db->dbprefix('sale_items').'.product_unit_id)', 'left')
            ->join('product_variants', 'product_variants.id = sale_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id = sale_items.tax_rate_id', 'left')
            ->group_by('sale_items.id')
            ->order_by('id', 'desc');
        if ($sale_id) {
            $this->db->where('sales_returns.sale_id', $sale_id);
        }
        $q = $this->db->get('sales_returns');
        if ($q->num_rows() > 0) {
                if ($arr == false) {
                    foreach (($q->result()) as $row) {
                        $data[] = $row;
                    }
                } else {
                    foreach (($q->result_array()) as $row) {
                        $row['sale_id'] = $sale_id;
                        $data[] = $row;
                    }
                }
            return $data;
        }
        return FALSE;
    }

    public function getReturnsInvoicesBySaleID($id)
    {
        $q = $this->db->select("
                        SUM( {$this->db->dbprefix('sales')}.grand_total ) AS grand_total,
                        SUM( {$this->db->dbprefix('sales')}.surcharge ) AS surcharge,
                        SUM( {$this->db->dbprefix('sales')}.order_discount ) AS order_discount,
                        SUM( {$this->db->dbprefix('sales')}.order_tax ) AS order_tax,
                        SUM( {$this->db->dbprefix('sales')}.paid ) AS paid,
                        SUM( {$this->db->dbprefix('sales')}.product_tax ) AS product_tax
                        ")
                 ->join('sales', 'sales.id = sales_returns.return_id')
                 ->where('sales_returns.sale_id', $id)
                 ->get('sales_returns');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function recontabilizarPago($reference_no){

        $multi_payments = [];
        $conceptos = [];
        $retenciones = [];
        $mpayment_enc = $this->site->getMultiPaymentEnc($reference_no);
        $customer_id = $mpayment_enc[0]->company_id ? $mpayment_enc[0]->company_id : $mpayment_enc[0]->customer_id;
        $cost_center_id = $mpayment_enc[0]->cost_center_id;
        $details = $this->sales_model->get_sale_multi_payments_by_reference($reference_no);
        $total_paid = 0;
        // $this->sma->print_arrays($mpayment_enc);
        foreach ($details as $mpayment) {
            if ($mpayment->sale_id) {
                $payment = [];
                $total_paid += $mpayment->amount;
                foreach ($mpayment as $key => $value) {
                    $payment[$key] = $value;
                }
                $multi_payments[] = $payment;
                if ($mpayment->rete_fuente_total != 0) {
                    $retenciones[$mpayment->sale_id]['rete_fuente_total'] = $mpayment->rete_fuente_total;
                    $retenciones[$mpayment->sale_id]['rete_fuente_percentage'] = $mpayment->rete_fuente_percentage;
                    $retenciones[$mpayment->sale_id]['rete_fuente_base'] = $mpayment->rete_fuente_base;
                    $retenciones[$mpayment->sale_id]['rete_fuente_account'] = $mpayment->rete_fuente_account;
                }
                if ($mpayment->rete_iva_total != 0) {
                    $retenciones[$mpayment->sale_id]['rete_iva_total'] = $mpayment->rete_iva_total;
                    $retenciones[$mpayment->sale_id]['rete_iva_percentage'] = $mpayment->rete_iva_percentage;
                    $retenciones[$mpayment->sale_id]['rete_iva_base'] = $mpayment->rete_iva_base;
                    $retenciones[$mpayment->sale_id]['rete_iva_account'] = $mpayment->rete_iva_account;
                }
                if ($mpayment->rete_ica_total != 0) {
                    $retenciones[$mpayment->sale_id]['rete_ica_total'] = $mpayment->rete_ica_total;
                    $retenciones[$mpayment->sale_id]['rete_ica_percentage'] = $mpayment->rete_ica_percentage;
                    $retenciones[$mpayment->sale_id]['rete_ica_base'] = $mpayment->rete_ica_base;
                    $retenciones[$mpayment->sale_id]['rete_ica_account'] = $mpayment->rete_ica_account;
                }
                if ($mpayment->rete_other_total != 0) {
                    $retenciones[$mpayment->sale_id]['rete_other_total'] = $mpayment->rete_other_total;
                    $retenciones[$mpayment->sale_id]['rete_other_percentage'] = $mpayment->rete_other_percentage;
                    $retenciones[$mpayment->sale_id]['rete_other_base'] = $mpayment->rete_other_base;
                    $retenciones[$mpayment->sale_id]['rete_other_account'] = $mpayment->rete_other_account;
                }
                if ($mpayment->rete_bomberil_total != 0) {
                    $retenciones[$mpayment->sale_id]['rete_bomberil_total'] = $mpayment->rete_bomberil_total;
                    $retenciones[$mpayment->sale_id]['rete_bomberil_percentage'] = $mpayment->rete_bomberil_percentage;
                    $retenciones[$mpayment->sale_id]['rete_bomberil_base'] = $mpayment->rete_bomberil_base;
                    $retenciones[$mpayment->sale_id]['rete_bomberil_account'] = $mpayment->rete_bomberil_account;
                }
                if ($mpayment->rete_autoaviso_total != 0) {
                    $retenciones[$mpayment->sale_id]['rete_autoaviso_total'] = $mpayment->rete_autoaviso_total;
                    $retenciones[$mpayment->sale_id]['rete_autoaviso_percentage'] = $mpayment->rete_autoaviso_percentage;
                    $retenciones[$mpayment->sale_id]['rete_autoaviso_base'] = $mpayment->rete_autoaviso_base;
                    $retenciones[$mpayment->sale_id]['rete_autoaviso_account'] = $mpayment->rete_autoaviso_account;
                }
            } else {
                $concepto = [];
                $total_paid += $mpayment->amount;
                foreach ($mpayment as $key => $value) {
                    if ($key == 'concept_ledger_id') {
                        $concepto['ledger_id'] = $value;
                        unset($mpayment->{$key});
                    }
                    $concepto[$key] = $value;
                }
                $conceptos[] = $concepto;
            }
        }
        $payment = array(
                            'reference_no' => $mpayment_enc[0]->reference_no,
                            'amount' => $total_paid,
                            'date' => $mpayment_enc[0]->date,
                            'payments' => $multi_payments,
                            'companies_id' => $customer_id,
                            'customer_id' => $customer_id,
                            'biller_id' => $mpayment_enc[0]->biller_id,
                            'note' => $mpayment_enc[0]->note,
                            'document_type_id' => $mpayment_enc[0]->document_type_id,
                            );
        if ($mpayment_enc[0]->amount > 0) {
            $this->site->wappsiContabilidadPagosMultiplesVentas($payment, $customer_id, $retenciones, null, $conceptos, $cost_center_id);
        } else {
            foreach ($multi_payments as $cid => $mpayment) {
                $multi_payments[$cid]['amount'] = $multi_payments[$cid]['amount'] * -1;
            }
            foreach ($conceptos as $cid => $concepto) {
                $conceptos[$cid]['amount'] = $conceptos[$cid]['amount'] * -1;
            }
            $payment['amount'] = $payment['amount'] * -1;
            $payment['payments'] = $multi_payments;
            $this->site->wappsiContabilidadPagosMultiplesVentasAnulacion($payment, $customer_id, $retenciones, null, $conceptos, $cost_center_id);
        }
    }

    public function get_pending_sales_dian($pos = 0)
    {
        $this->db->select("count(*) AS quantity");
        $this->db->join('documents_types', 'documents_types.id = sales.document_type_id', 'left');
        $this->db->where("fe_aceptado !=", 2);
        $this->db->where("pos", $pos);
        $this->db->where('documents_types.factura_electronica', YES);
        $q = $this->db->get("sales");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function sale_has_paid_collection_commmissions($sale_id){
        $q = $this->db->get_where('payments', ['comm_payment_status' => 1, 'sale_id' => $sale_id]);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function order_delete_remaining_quantity($order_id){
        $rows = $this->db->get_where('order_sale_items', ['sale_id' => $order_id]);
        if ($rows->num_rows() > 0) {
            foreach (($rows->result()) as $row) {
                if ($row->quantity_delivered > 0) {
                    $new_quantity = $row->quantity - ($row->quantity - $row->quantity_delivered);
                    $this->db->update('order_sale_items', ['quantity'=>$new_quantity, 'quantity_to_bill'=>0, 'last_update' => date('Y-m-d H:i:s')], ['id'=>$row->id]);
                } else if ($row->quantity_delivered == 0) {
                    $this->db->delete('order_sale_items', ['id'=>$row->id]);
                }
            }
            $this->db->update('order_sales', ['sale_status'=>'completed', 'last_update' => date('Y-m-d H:i:s')], ['id'=>$order_id]);
        }
        return true;
    }

    public function cancel_order_sale($id){
        $order_sale = $this->getOrderSaleByID($id);
        if ($order_sale && $order_sale->sale_status != 'completed') {
            $this->db->update('order_sales', ['sale_status'=>'cancelled', 'last_update' => date('Y-m-d H:i:s')], ['id'=>$id]);
            return true;
        } else {
            return false;
        }
    }

    public function complete_os_wo_sale($id){
        $order_sale = $this->getOrderSaleByID($id);
        if ($order_sale && $order_sale->sale_status != 'completed' && $order_sale->sale_status != 'pending') {
            $this->db->update('order_sales', ['sale_status'=>'completed', 'last_update' => date('Y-m-d H:i:s')], ['id'=>$id]);
            $this->db->query("UPDATE sma_order_sale_items SI
                            SET SI.quantity = SI.quantity_delivered
                            WHERE SI.sale_id = {$id}");
            return true;
        } else {
            return false;
        }
    }

    public function change_order_sale_status_from_delivery($order_sale_reference){
        $q = $this->db->get_where('order_sales', ['reference_no'=>$order_sale_reference]);
        if ($q->num_rows() > 0) {
            $q = $q->row();
            if ($q->sale_status == 'completed') {
                $this->db->update('order_sales', ['sale_status'=>'sent'], ['reference_no'=>$order_sale_reference]);
            }
        }
    }

    public function get_gift_card_movements($gc_id){
        $q = $this->db->query("
                SELECT GC.id, NULL as top_up_id, 'Uso en venta' as movement_type, GC.card_no, CONCAT('Venta: ', S.reference_no, ', Pago: ', P.reference_no) as reference_no, P.date, (P.amount * -1) as value FROM {$this->db->dbprefix('gift_cards')} GC
                    INNER JOIN {$this->db->dbprefix('payments')} P ON P.paid_by = 'gift_card' AND P.cc_no = GC.card_no
                    INNER JOIN {$this->db->dbprefix('sales')} S ON S.id = P.sale_id
                WHERE GC.id = {$gc_id}

                    UNION

                SELECT GC.id, P.id as top_up_id, IF(movement_type = 1, 'Recarga', 'Anulación') as movement_type, GC.card_no, P.reference_no as reference_no, P.date, P.amount as value FROM {$this->db->dbprefix('gift_cards')} GC
                    INNER JOIN {$this->db->dbprefix('gift_card_topups')} P ON P.card_id = GC.id
                WHERE GC.id = {$gc_id}

                ORDER BY date ASC
                ");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function get_gift_card_topup($id){
        $q = $this->db->get_where('gift_card_topups', ['id'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_total_payments_for_sale($id){
        $q = $this->db->select('
            SUM(COALESCE(amount, 0)) as total_paid,
            SUM(COALESCE(rete_fuente_base, 0)) as fuente_base,
            SUM(COALESCE(rete_iva_base, 0)) as iva_base,
            SUM(COALESCE(rete_ica_base, 0)) as ica_base,
            SUM(COALESCE(rete_other_base, 0)) as other_base,
            SUM(COALESCE(rete_autoaviso_base, 0)) as autoaviso_base,
            SUM(COALESCE(rete_bomberil_base, 0)) as bomberil_base,
            SUM(COALESCE(rete_fuente_total, 0)) as fuente_total,
            SUM(COALESCE(rete_iva_total, 0)) as iva_total,
            SUM(COALESCE(rete_ica_total, 0)) as ica_total,
            SUM(COALESCE(rete_other_total, 0)) as other_total,
            SUM(COALESCE(rete_autoaviso_total, 0)) as autoaviso_total,
            SUM(COALESCE(rete_bomberil_total, 0)) as bomberil_total
            ')
            ->where('sale_id', $id)
            ->where('multi_payment', 1)
            ->group_by('sale_id')
            ->get('payments');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function findPaymentMethod($data)
    {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get('payment_methods');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function update_date($inv)
    {
        $old_date = $inv->date;
        $new_date = date('Y-m-d')." 00:00:01";
        $txt_note = $inv->note;
        $txt_note .= sprintf(lang('user_salenote_date_changed'), $old_date, $new_date);
        $msg = '';
        if ($this->db->update('sales', array('date' => $new_date, 'note' => $txt_note, 'date_updated' => 1), array('id' => $inv->id))) {
            $msg .= sprintf(lang('sale_date_updated'), $inv->reference_no, $old_date, $new_date);
            $payments = $this->getPaymentsForSale($inv->id);
            if ($payments) {
                foreach($payments as $payment => $value ){
                    $datePay = new DateTime($value->date);
                    $newDate = new DateTime($new_date);
                    if ($datePay < $newDate) {
                        $this->db->update('payments', ['date' => $new_date], ['id' => $value->id]);
                    }
                }
            }
            if ($this->Settings->modulary == 1) {
                $msg .= ", " .$this->recontabilizarVenta($inv->id);
            }
            return $msg;
        }
        return false;
    }

    public function selectUnprintedDocuments($userDocumentType)
    {
        $this->db->select("sales.id,
            date,
            reference_no,
            biller,
            customer,
            grand_total,
            printed,
            factura_electronica,
            cufe");
        $this->db->from("sales");
        $this->db->where("printed", "0");
        $this->db->where("CAST(date AS DATE) between date_sub(CURDATE(), interval 2 DAY) AND CURDATE()");
        $this->db->join("documents_types dt", "dt.id = sales.document_type_id");
        if (!empty($userDocumentType)) {
            $this->db->where_in("document_type_id", $userDocumentType);
        }

        $query1 = $this->db->get_compiled_select();

        $this->db->select("id,
            date,
            reference_no,
            biller,
            customer,
            grand_total,
            printed,
            0,
            cufe");
        $this->db->from("sales");
        $this->db->where("printed", "1");
        $this->db->where("CAST(date AS DATE) between date_sub(CURDATE(), interval 2 DAY) AND CURDATE()");
        if (!empty($userDocumentType)) {
            $this->db->where_in("document_type_id", $userDocumentType);
        }

        $this->db->order_by("printed", "ASC");
        $this->db->order_by("date", "DESC");
        $this->db->limit("20");
        $query2 = $this->db->get_compiled_select();

        $query = $this->db->query($query1 ." UNION ". $query2);

        return $query->result();
    }

    public function mark_all_sales_as_printed($electronic = 0)
    {
        $sql = "UPDATE 
                sma_sales
            INNER JOIN 
                sma_documents_types  ON sma_documents_types.id = sma_sales.document_type_id
            SET 
                sma_sales.printed = 1
            WHERE 
                sma_documents_types.factura_electronica = $electronic
                    AND sma_sales.pos = 0
                    AND sma_sales.printed != 1";

        $this->db->query($sql);

        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }


    public function create_recurring_orders(){

        if (!$this->Settings->osdt_recurring_sales > 0) {
            return false;
        }

        $return = false;
        /* Recurrencia mensual */
        $sql_Search = "SELECT S.id max_sale_id, GROUP_CONCAT(S.reference_no) AS reference_no, (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0))) as new_recurring_next_date FROM sma_sales S
                    LEFT JOIN sma_order_sales OS ON OS.recurring_next_date = (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0)))  AND S.customer_id = OS.customer_id
                WHERE S.recurring_sale = 1 AND S.recurring_type = 1 AND LPAD(MONTH(S.recurring_next_date), 2, 0) = ".date('m')." AND OS.id IS NULL AND S.grand_total > 0
                GROUP BY S.recurring_next_date, S.customer_id";
        $q = $this->db->query($sql_Search);
        if ($q->num_rows() > 0) {
            $sql1_1 = "SET @contador = (SELECT DT.sales_consecutive FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id);";
            $sql1_2 = "SET @reference = (SELECT DT.sales_prefix FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id);";
            $sql1_3 = "SET @dtid = (SELECT DT.id FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id);";
            $sql1_4 = "SET SQL_SAFE_UPDATES = 0;";

            $sql2 = "INSERT INTO sma_order_sales (date, reference_no, customer_id, customer, biller_id, biller, warehouse_id, note, total, product_discount, total_discount, order_discount, product_tax, order_tax, total_tax, grand_total, sale_status, payment_status, payment_term, due_date, created_by, seller_id, address_id, document_type_id, registration_date, recurring_sale, recurring_type, recurring_next_date)
                (
                SELECT CONCAT(S.recurring_next_date, ' 08:00:00') AS date, CONCAT(@reference, '-', (@contador := @contador + 1)), S.customer_id, S.customer, S.biller_id, S.biller, S.warehouse_id, CONCAT('Creada automáticamente por facturas recurrentes MENSUALES ', tbl.reference_no), 1, 0, 0, 0, 1, 1, 1, 1, 'automatic', 'pending', 0, NULL, S.created_by, S.seller_id, S.address_id, @dtid, CURRENT_TIMESTAMP(), S.recurring_sale, S.recurring_type, tbl.new_recurring_next_date FROM 
                (
                    SELECT S.id max_sale_id, GROUP_CONCAT(S.reference_no) AS reference_no, (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0))) as new_recurring_next_date FROM sma_sales S
                        LEFT JOIN sma_order_sales OS ON OS.recurring_next_date = (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0)))  AND S.customer_id = OS.customer_id
                    WHERE S.recurring_sale = 1 AND S.recurring_type = 1 AND LPAD(MONTH(S.recurring_next_date), 2, 0) = ".date('m')." AND OS.id IS NULL AND S.grand_total > 0
                    GROUP BY S.recurring_next_date, S.customer_id
                ) tbl
                INNER JOIN sma_sales S ON S.id = tbl.max_sale_id
                INNER JOIN sma_companies B ON B.id = S.biller_id
                WHERE B.status = 1
                GROUP BY S.customer_id
                );";
            // exit(var_dump($sql2));
            $sql3 = "INSERT INTO sma_order_sale_items 
                    (
                    sale_id, product_id, product_code, product_name, product_type, option_id, net_unit_price, current_gold_price, unit_price, quantity, warehouse_id, item_tax, tax_rate_id, tax, item_tax_2, tax_rate_2_id, tax_2, discount, item_discount, subtotal, serial_no, real_unit_price, product_unit_id, product_unit_code, unit_quantity, unit_order_discount, price_before_tax, preferences, registration_date
                    )

                    (
                    SELECT 
                    OS.id, SI.product_id, SI.product_code, SI.product_name, SI.product_type, SI.option_id, SI.net_unit_price, SI.current_gold_price, SI.unit_price, SI.quantity, SI.warehouse_id, SI.item_tax, SI.tax_rate_id, SI.tax, SI.item_tax_2, SI.tax_rate_2_id, SI.tax_2, SI.discount, SI.item_discount, SI.subtotal, SI.serial_no, SI.real_unit_price, SI.product_unit_id, SI.product_unit_code, SI.unit_quantity, SI.unit_order_discount, SI.price_before_tax, SI.preferences, SI.registration_date
                    FROM 
                    (
                        SELECT S.id max_sale_id, SI.id max_sale_item_id, (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0))) as new_recurring_next_date FROM sma_sales S
                            INNER JOIN sma_sale_items SI ON SI.sale_id = S.id
                            LEFT JOIN sma_order_sales OS ON OS.recurring_next_date = (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0)))  AND S.customer_id = OS.customer_id
                            LEFT JOIN sma_order_sale_items OSI ON OSI.sale_id = OS.id AND OSI.product_id = SI.product_id AND COALESCE(OSI.product_unit_id, 0) = COALESCE(SI.product_unit_id, 0) AND COALESCE(OSI.option_id, 0) = COALESCE(SI.option_id, 0)
                        WHERE S.recurring_sale = 1 AND S.recurring_type = 1 AND LPAD(MONTH(S.recurring_next_date), 2, 0) = ".date('m')." AND OSI.id IS NULL AND S.grand_total > 0
                        GROUP BY S.recurring_next_date, S.customer_id, SI.id
                    ) tbl
                    INNER JOIN sma_sales S ON S.id = tbl.max_sale_id
                    INNER JOIN sma_sale_items SI ON SI.id = tbl.max_sale_item_id
                    INNER JOIN sma_products P ON P.id = SI.product_id
                    INNER JOIN sma_order_sales OS ON OS.sale_status = 'automatic' AND OS.customer_id = S.customer_id AND OS.date = CONCAT(S.recurring_next_date, ' 08:00:00')
                    INNER JOIN sma_companies B ON B.id = S.biller_id
                    WHERE B.status = 1
                    );";
            $sql4 = "UPDATE sma_order_sales OS
                     INNER JOIN (SELECT OSI.sale_id, OS.reference_no, SUM(COALESCE(OSI.item_tax, 0)) as product_tax, SUM(COALESCE(OSI.net_unit_price * OSI.quantity)) AS subtotal, SUM(COALESCE(OSI.unit_price * OSI.quantity)) AS grand_total FROM sma_order_sales OS
                        INNER JOIN sma_order_sale_items OSI ON OSI.sale_id = OS.id
                        WHERE OS.sale_status = 'automatic'
                    GROUP BY OSI.sale_id) tbl ON tbl.sale_id = OS.id
                    SET OS.total = tbl.subtotal, OS.product_tax = tbl.product_tax, OS.total_tax = tbl.product_tax, OS.grand_total = tbl.grand_total, sale_status = 'pending';";
            $sql5 = "UPDATE sma_documents_types DT
                    INNER JOIN (
                        SELECT 
                            dt.id AS dt_id, 
                            MAX(CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(OS.reference_no, '-', 2), '-', -1) AS UNSIGNED))+1 AS consecutive 
                        FROM sma_order_sales OS
                        INNER JOIN (SELECT DT.id FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id) dt 
                        ON dt.id = OS.document_type_id
                    ) AS subquery 
                    ON subquery.dt_id = DT.id
                    SET DT.sales_consecutive = subquery.consecutive;";
            if ($this->db->query($sql1_1) && $this->db->query($sql1_2) && $this->db->query($sql1_3) && $this->db->query($sql1_4)) {
                if ($this->db->query($sql2)) {
                    if ($this->db->query($sql3)) {
                        $this->db->query($sql4);
                        if ($this->db->query($sql5)) {
                            $return = true;
                        }
                    }
                }
            }
        } 
        /* Recurrencia mensual */

        /* Recurrencia anual */
        $sql_Search = "SELECT S.id max_sale_id, GROUP_CONCAT(S.reference_no) AS reference_no, (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0))) as new_recurring_next_date FROM sma_sales S
                    LEFT JOIN sma_order_sales OS ON OS.recurring_next_date = (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0)))  AND S.customer_id = OS.customer_id
                WHERE S.recurring_sale = 1 AND S.recurring_type = 2 AND LPAD(MONTH(S.recurring_next_date), 2, 0) = ".date('m')." AND OS.id IS NULL AND S.grand_total > 0 AND YEAR(S.recurring_next_date)  = ".date('Y')."
                GROUP BY S.recurring_next_date, S.customer_id";
        // exit($sql_Search);
        $q = $this->db->query($sql_Search);
        if ($q->num_rows() > 0) {
            $sql1_1 = "SET @contador = (SELECT DT.sales_consecutive FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id);";
            $sql1_2 = "SET @reference = (SELECT DT.sales_prefix FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id);";
            $sql1_3 = "SET @dtid = (SELECT DT.id FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id);";
            $sql1_4 = "SET SQL_SAFE_UPDATES = 0;";

            $sql2 = "INSERT INTO sma_order_sales (date, reference_no, customer_id, customer, biller_id, biller, warehouse_id, note, total, product_discount, total_discount, order_discount, product_tax, order_tax, total_tax, grand_total, sale_status, payment_status, payment_term, due_date, created_by, seller_id, address_id, document_type_id, registration_date, recurring_sale, recurring_type, recurring_next_date)
                (
                SELECT CONCAT(S.recurring_next_date, ' 08:00:00') AS date, CONCAT(@reference, '-', (@contador := @contador + 1)), S.customer_id, S.customer, S.biller_id, S.biller, S.warehouse_id, CONCAT('Creada automáticamente por facturas recurrentes ANUALES ', tbl.reference_no), 1, 0, 0, 0, 1, 1, 1, 1, 'automatic', 'pending', 0, NULL, S.created_by, S.seller_id, S.address_id, @dtid, CURRENT_TIMESTAMP(), S.recurring_sale, S.recurring_type, tbl.new_recurring_next_date FROM 
                (
                    SELECT S.id max_sale_id, GROUP_CONCAT(S.reference_no) AS reference_no, (CONCAT(YEAR(S.recurring_next_date)+1, '-', LPAD((MONTH(S.recurring_next_date)), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0))) as new_recurring_next_date FROM sma_sales S
                        LEFT JOIN sma_order_sales OS ON OS.recurring_next_date = (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0)))  AND S.customer_id = OS.customer_id
                    WHERE S.recurring_sale = 1 AND S.recurring_type = 2 AND LPAD(MONTH(S.recurring_next_date), 2, 0) = ".date('m')." AND OS.id IS NULL AND S.grand_total > 0 AND YEAR(S.recurring_next_date)  = ".date('Y')."
                    GROUP BY S.recurring_next_date, S.customer_id
                ) tbl
                INNER JOIN sma_sales S ON S.id = tbl.max_sale_id
                INNER JOIN sma_companies B ON B.id = S.biller_id
                WHERE B.status = 1
                GROUP BY S.customer_id
                );";
            // exit(var_dump($sql2));
            $sql3 = "INSERT INTO sma_order_sale_items 
                    (
                    sale_id, product_id, product_code, product_name, product_type, option_id, net_unit_price, current_gold_price, unit_price, quantity, warehouse_id, item_tax, tax_rate_id, tax, item_tax_2, tax_rate_2_id, tax_2, discount, item_discount, subtotal, serial_no, real_unit_price, product_unit_id, product_unit_code, unit_quantity, unit_order_discount, price_before_tax, preferences, registration_date
                    )

                    (
                    SELECT 
                    OS.id, SI.product_id, SI.product_code, SI.product_name, SI.product_type, SI.option_id, SI.net_unit_price, SI.current_gold_price, SI.unit_price, SI.quantity, SI.warehouse_id, SI.item_tax, SI.tax_rate_id, SI.tax, SI.item_tax_2, SI.tax_rate_2_id, SI.tax_2, SI.discount, SI.item_discount, SI.subtotal, SI.serial_no, SI.real_unit_price, SI.product_unit_id, SI.product_unit_code, SI.unit_quantity, SI.unit_order_discount, SI.price_before_tax, SI.preferences, SI.registration_date
                    FROM 
                    (
                        SELECT S.id max_sale_id, SI.id max_sale_item_id, (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0))) as new_recurring_next_date FROM sma_sales S
                            INNER JOIN sma_sale_items SI ON SI.sale_id = S.id
                            LEFT JOIN sma_order_sales OS ON OS.recurring_next_date = (CONCAT(YEAR(S.recurring_next_date), '-', LPAD((MONTH(S.recurring_next_date)+1), 2, 0), '-', LPAD(DAY(S.recurring_next_date), 2, 0)))  AND S.customer_id = OS.customer_id
                            LEFT JOIN sma_order_sale_items OSI ON OSI.sale_id = OS.id AND OSI.product_id = SI.product_id AND COALESCE(OSI.product_unit_id, 0) = COALESCE(SI.product_unit_id, 0) AND COALESCE(OSI.option_id, 0) = COALESCE(SI.option_id, 0)
                        WHERE S.recurring_sale = 1 AND S.recurring_type = 2 AND LPAD(MONTH(S.recurring_next_date), 2, 0) = ".date('m')." AND OSI.id IS NULL  AND S.grand_total > 0 AND YEAR(S.recurring_next_date) = ".date('Y')."
                        GROUP BY S.recurring_next_date, S.customer_id, SI.id
                    ) tbl
                    INNER JOIN sma_sales S ON S.id = tbl.max_sale_id
                    INNER JOIN sma_sale_items SI ON SI.id = tbl.max_sale_item_id
                    INNER JOIN sma_products P ON P.id = SI.product_id
                    INNER JOIN sma_order_sales OS ON OS.sale_status = 'automatic' AND OS.customer_id = S.customer_id AND OS.date = CONCAT(S.recurring_next_date, ' 08:00:00')
                    INNER JOIN sma_companies B ON B.id = S.biller_id
                    WHERE B.status = 1
                    );";
            $sql4 = "UPDATE sma_order_sales OS
                     INNER JOIN (SELECT OSI.sale_id, OS.reference_no, SUM(COALESCE(OSI.item_tax, 0)) as product_tax, SUM(COALESCE(OSI.net_unit_price * OSI.quantity)) AS subtotal, SUM(COALESCE(OSI.unit_price * OSI.quantity)) AS grand_total FROM sma_order_sales OS
                        INNER JOIN sma_order_sale_items OSI ON OSI.sale_id = OS.id
                        WHERE OS.sale_status = 'automatic'
                    GROUP BY OSI.sale_id) tbl ON tbl.sale_id = OS.id
                    SET OS.total = tbl.subtotal, OS.product_tax = tbl.product_tax, OS.total_tax = tbl.product_tax, OS.grand_total = tbl.grand_total, sale_status = 'pending';";
            $sql5 = "UPDATE sma_documents_types DT
                    INNER JOIN (
                        SELECT 
                            dt.id AS dt_id, 
                            MAX(CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(OS.reference_no, '-', 2), '-', -1) AS UNSIGNED))+1 AS consecutive 
                        FROM sma_order_sales OS
                        INNER JOIN (SELECT DT.id FROM sma_documents_types DT INNER JOIN sma_settings S ON S.osdt_recurring_sales = DT.id) dt 
                        ON dt.id = OS.document_type_id
                    ) AS subquery 
                    ON subquery.dt_id = DT.id
                    SET DT.sales_consecutive = subquery.consecutive;";
            if ($this->db->query($sql1_1) && $this->db->query($sql1_2) && $this->db->query($sql1_3) && $this->db->query($sql1_4)) {
                if ($this->db->query($sql2)) {
                    if ($this->db->query($sql3)) {
                        $this->db->query($sql4);
                        if ($this->db->query($sql5)) {
                            $return = true;
                        }
                    }
                }
            }
        }
        /* Recurrencia anual */
        return $return;
    }
}