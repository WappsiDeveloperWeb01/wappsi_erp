<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<div class="modal-dialog">
  <div class="modal-content">
    <style type="text/css">
      .input-edit {
        color: black;
        border-color: #eaeaea;
        border-width: 1px;
      }
    </style>
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
      </button>
      <?php if ($type == 1): ?>
        <h4 class="modal-title" id="myModalLabel"><?= lang('add_country'); ?></h4>
      <?php endif ?>
      <?php if ($type == 2): ?>
        <h4 class="modal-title" id="myModalLabel"><?= lang('add_state'); ?></h4>
      <?php endif ?>
      <?php if ($type == 3): ?>
        <h4 class="modal-title" id="myModalLabel"><?= lang('add_city'); ?></h4>
      <?php endif ?>
      <?php if ($type == 4): ?>
        <h4 class="modal-title" id="myModalLabel"><?= lang('add_zone'); ?></h4>
      <?php endif ?>
      <?php if ($type == 5): ?>
        <h4 class="modal-title" id="myModalLabel"><?= lang('add_subzone'); ?></h4>
      <?php endif ?>

    </div>
    <?php $attrib = array('id' => 'add_ubication_form');
    echo admin_form_open("system_settings/add_ubication/" . $type, $attrib); ?>
    <div class="modal-body">

      <?php if ($type >= 2): ?>
        <div class="form-group">
          <?= lang("country", "country"); ?>
          <select class="form-control select" name="country" id="country" required>
            <option value=""><?= lang("select"); ?></option>
            <?php foreach ($countries as $row => $country): ?>
              <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>"><?= $country->NOMBRE ?></option>
            <?php endforeach ?>
          </select>
          <input type="hidden" name="country_code" id="country_code" <?= (isset($_POST['country_code'])) ? "value='" . $_POST['country_code'] . "'" : "" ?>>
        </div>
      <?php endif ?>

      <?php if ($type >= 3): ?>
        <div class="form-group">
          <?= lang("state", "state"); ?>
          <select class="form-control select" name="state" id="state" required>
            <option value=""><?= lang("select") ?></option>
          </select>
          <input type="hidden" name="state_code" id="state_code" <?= (isset($_POST['state_code'])) ? "value='" . $_POST['state_code'] . "'" : "" ?>>
        </div>
      <?php endif ?>

      <?php if ($type >= 4): ?>
        <div class="form-group">
          <?= lang("city", "city"); ?>
          <select class="form-control select" name="city" id="city" required>
            <option value=""><?= lang("select") ?></option>
          </select>
          <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='" . $_POST['city_code'] . "'" : "" ?>>
        </div>
      <?php endif ?>

      <?php if ($type >= 5): ?>
        <div class="form-group">
          <?= lang("zone", "zone"); ?>
          <select class="form-control select" name="zone" id="zone">
            <option value=""><?= lang("select") ?></option>
          </select>
          <input type="hidden" name="zone_code" id="zone_code" <?= (isset($_POST['zone_code'])) ? "value='" . $_POST['zone_code'] . "'" : "" ?>>
        </div>
      <?php endif ?>

      <div class="form-group">
        <?= lang('name', 'name') ?>
        <input type="text" name="name" id="name" class="form-control" required>
      </div>

      <div class="form-group">
        <input type="hidden" name="add_ubication" value="1">

        <?= lang('postal_code', 'postal_code') ?>
        <?php if ($type == 3): ?>
          <div class="input-group">
            <span class="input-group-addon" id="state_postal_code"></span>
          <?php endif ?>
          <input type="text" name="postal_code" id="postal_code" class="form-control" required>
          <?php if ($type == 3): ?>
          </div>
        <?php endif ?>
      </div>

      <?php if ($type == 1): ?>
        <?php if (count($currencies) > 1): ?>
          <div class="col-md-4 row">
            <div class="col-md-6 form-group">
              <?= lang('currency', 'currency') ?>
              <select name="currency" class="form-control" id="currency">
                <?php foreach ($currencies as $currency): ?>
                  <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $this->Settings->default_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-6 form-group trm-control" style="display: none;">
              <?= lang('trm', 'trm') ?>
              <input type="number" name="trm" id="trm" class="form-control" required="true" step="0.01">
            </div>
          </div>
        <?php else: ?>
          <input type="hidden" name="currency" value="<?= $this->Settings->default_currency ?>" id="currency">
        <?php endif ?>

        <div class="form-group">
          <?= lang('codigo_iso', 'codigo_iso') ?>
          <input type="text" name="codigo_iso" id="codigo_iso" maxlength="2" class="form-control" required>
        </div>
      <?php endif ?>

    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-primary" id="submit_ubication"><?= lang('submit_and_continue') ?></button>
    </div>
  </div>
  <?= form_close(); ?>
</div>

<script type="text/javascript">
  $(document).ready(function(e) {
    $("#add_ubication_form").validate({
      ignore: []
    });
  });

  $('#state').on('change', function() {
    dpto = $('#state option:selected').data('code');
    $('#state_code').val(dpto);
    $('#state_postal_code').text(dpto);
    $.ajax({
      url: "<?= admin_url() ?>customers/get_cities/" + dpto,
    }).done(function(data) {
      $('#city').html(data);
    }).fail(function(data) {
      console.log(data.responseText);
    });
  });

  $('#country').on('change', function() {
    dpto = $('#country option:selected').data('code');
    $('#country_code').val(dpto);
    $.ajax({
      url: "<?= admin_url() ?>customers/get_states/" + dpto,
    }).done(function(data) {
      $('#state').html(data);
    }).fail(function(data) {
      console.log(data.responseText);
    });
  });

  $('#city').on('change', function() {
    code = $('#city option:selected').data('code');
    $('.postal_code').val(code);
    $('#city_code').val(code);
    $.ajax({
      url: "<?= admin_url() . 'customers/get_zones/' ?>" + code
    }).done(function(data) {
      $('#zone').html(data);
    });

  });

  $('#zone').on('change', function() {
    code = $('#zone option:selected').data('code');
    $('.postal_code').val(code);
    $('#zone_code').val(code);
  });

  $('#submit_ubication').on('click', function() {
    if ($('#add_ubication_form').valid()) {
      $('#submit_ubication').prop('disabled', true);
      datos = $('#add_ubication_form').serialize();

      $.ajax({
        url: '<?= admin_url() ?>system_settings/add_ubication_ajax/<?= $type ?>',
        dataType: 'JSON',
        type: 'GET',
        data: datos
      }).done(function(response) {
        $messageSynchronize = ''

        if (response.success == 1) {
          if (Object.hasOwn(response, 'synchronyzed')) {
              if (response.synchronyzed == false) {
                $messageSynchronize = 'No se pudo sincronizar en Tienda Pro'
              }
          }
  
          swal({
              title: '<?= lang("toast_success_title") ?>',
              text: `Ubicación guardada con éxito. ${$messageSynchronize}`,
              type: 'success',
              showCancelButton: true,
              showConfirmButton: false,
              cancelButtonText: "Ok"
          });
          
          $('#name').val('');
          $('#postal_code').val('');
          $('#submit_ubication').prop('disabled', false);
  
          $('#myModal').modal('hide');

        } else {
          swal({
              title: '<?= lang(("toast_error_title")) ?>',
              text: `Ubicación no pudo ser guardada con éxito.`,
              type: 'error',
              showCancelButton: true,
              showConfirmButton: false,
              cancelButtonText: "Ok"
          });
        }
      });
    }
  });
</script>

<?= $modal_js ?>