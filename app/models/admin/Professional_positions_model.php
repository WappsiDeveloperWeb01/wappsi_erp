<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Professional_positions_model extends CI_Model {

	public function get_by_area_id($area_id)
	{
		$q = $this->db->where("area_id", $area_id)->get("payroll_professional_position");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

	public function insert($data)
	{
		if ($this->db->insert("payroll_professional_position", $data)) {
			return $this->db->insert_id();
		}
		return FALSE;
	}

	public function get_by_name($name)
	{
		$q = $this->db->where("name LIKE ", $name.'%')->get("payroll_professional_position");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return FALSE;
	}

}

/* End of file Professional_positions_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/Professional_positions_model.php */