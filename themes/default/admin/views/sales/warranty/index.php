<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <!-- <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?= admin_form_open('budget', ['id'=>'budgetFilterForm']) ?>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('year') ?></label>
                                    <?php $year = (isset($_POST["year"])) ? $_POST["year"] : ($this->session->userdata('year') ? $this->session->userdata('year') : date('Y')); ?>
                                    <input class="form-control" type="number" name="year" id="year" value="<?= $year ?>">
                                </div>
                            </div>

                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for=""><?= $this->lang->line('month') ?></label>
                                    <?php $monthId = (isset($_POST["month"])) ? $_POST["month"] : ($this->session->userdata('month') ? $this->session->userdata('month') : date('n')); ?>
                                    <select class="form-control" name="month" id="month">
                                        <?php if (!empty($months)) : ?>
                                            <?php foreach ($months as $month) : ?>
                                                <option value="<?= $month->id ?>" <?= ($month->id == $monthId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($month->name)) ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-8">
                                <div class="new-button-container">
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="top" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                    <a href="<?= admin_url("warranty/add") ?>" class="btn btn-primary new-button" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>
                                </div>
                            </div>
                        </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div> -->

    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover" id="warrantyTable">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><?= lang("date") ?></th>
                                            <th><?= lang("reference_no") ?></th>
                                            <th><?= lang("customer") ?></th>
                                            <th><?= lang("product") ?></th>
                                            <th><?= lang("status") ?></th>
                                            <th><?= lang("warranty_activity") ?></th>
                                            <th><?= lang("actions") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        loadDataTables();

        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle-second="tooltip"]').tooltip();

        $(document).on('change', '#biller_id', function() { loadSellers($(this).val())});
        $(document).on('click', '#saveBudgetButton', function() { saveBudget(); });
        $(document).on('click', '.deleteBudgetButton', function() { deleteBudget($(this)); });
        $(document).on('click', '#saveDoubleBudgetButton', function() { confirmSaveDoubleBudget(); });
        $(document).on('click', '.viewWarranty', function() { warrantyView($(this)); });
    });

    function loadDataTables()
    {
        var _tabFilterFill = false;
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#warrantyTable').dataTable({
            aaSorting: [[1, "desc"]],
            aLengthMenu: nums,
            iDisplayLength:  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: site.base_url+'warranty/getDataTables',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });

                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                { bVisible: false},
                null,
                null,
                null,
                null,
                {
                    mRender: function(data, type, row) {
                        let status = {
                            1: {"color": "warning", "name" : "Pendiente"},
                            2: {"color": "danger", "name": "Rechazada"},
                            3: {"color": "info", "name": "Revisión Proveedor"},
                            4: {"color": "danger", "name": "Rechazada por Proveedor"},
                            5: {"color": "primary", "name": "Aceptada por proveedor NC"},
                            6: {"color": "primary", "name": "Aceptada Proveedor Cambio"},
                            7: {"color": "primary", "name": "Aceptada"},
                            8: {"color": "success", "name": "Completada"},
                        };
                        return '<span class="badge badge-'+ status[data].color +'">'+ status[data].name +'</span>';
                    }
                },
                null,
                {
                    bSortable: false,
                    className: 'text-center',
                    mRender: function(data, type, row) {
                        let actions = $(data);
                        let status = row[5];

                        if (status == 2) {
                            actions.find('#addActivityLink').remove();
                        }

                        let str = actions.prop('outerHTML');
                        return str;
                    }
                }
            ],
            fnRowCallback: function (row, data, display) {
                row.setAttribute('warrantyId', data[0]);
                row.className = "viewWarranty";

                return row;
            },
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html('<div class="dropdown">'+
                    '<button class="btn btn-primary btn-outline new-button dropdown-toggle center-block" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                        '<?php if ($this->Owner || $this->Admin) { ?>'+
                            '<li>'+
                                '<a href="<?= admin_url("warranty/add") ?>">'+
                                    '<i class="fa-solid fa-plus"></i> <?= lang("add") ?>'+
                                '</a>'+
                            '</li>'+
                        '<?php } ?>'+
                    '</ul>'+
                '</div>');

                $('[data-toggle-second="tooltip"]').tooltip();
            }
        });
    }

    function warrantyView(element)
    {
        let warrantyId = element.attr('warrantyId');

        $('#myModal').modal({remote: site.base_url + 'warranty/view/' + warrantyId});
        $('#myModal').modal('show');
    }
</script>