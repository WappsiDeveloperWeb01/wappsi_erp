$(document).ready(function () {
$('body a, body button').attr('tabindex', -1);
check_add_item_val();
if (site.settings.set_focus != 1) {
    $('#add_item').focus();
}
// Order level shipping and discoutn localStorage

    if (qudiscount = localStorage.getItem('qudiscount')) {
        $('#qudiscount').val(qudiscount);
    }
    $('#qutax2').change(function (e) {
        localStorage.setItem('qutax2', $(this).val());
        $('#qutax2').val($(this).val());
    });
    if (qutax2 = localStorage.getItem('qutax2')) {
        $('#qutax2').select2("val", qutax2);
    }
    $('#qustatus').change(function (e) {
        localStorage.setItem('qustatus', $(this).val());
    });
    if (qustatus = localStorage.getItem('qustatus')) {
        $('#qustatus').select2("val", qustatus);
    }
    var old_shipping;
    $('#qushipping').focus(function () {
        old_shipping = $(this).val();
    }).change(function () {
        if (!is_numeric($(this).val())) {
            $(this).val(old_shipping);
            bootbox.alert(lang.unexpected_value);
            return;
        } else {
            shipping = $(this).val() ? parseFloat($(this).val()) : '0';
        }
        localStorage.setItem('qushipping', shipping);
        var gtotal = ((total + invoice_tax) - order_discount) + shipping;
        $('#gtotal').text(formatMoney(gtotal));
        $('#tship').text(formatMoney(shipping));
    });
    if (qushipping = localStorage.getItem('qushipping')) {
        shipping = parseFloat(qushipping);
        $('#qushipping').val(shipping);
    } else {
        shipping = 0;
    }

    $('#qusupplier').change(function (e) {
        localStorage.setItem('qusupplier', $(this).val());
        $('#supplier_id').val($(this).val());
    });
    if (qusupplier = localStorage.getItem('qusupplier')) {
        $('#qusupplier').val(qusupplier).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    } else {
        nsSupplier();
    }

    // If there is any item in localStorage
    if (localStorage.getItem('quitems')) {
        loadItems();
    }

    // clear localStorage and reload
    $('#reset').click(function (e) {
            bootbox.confirm(lang.r_u_sure, function (result) {
                if (result) {
                    if (localStorage.getItem('quitems')) {
                        localStorage.removeItem('quitems');
                    }
                    if (localStorage.getItem('qudiscount')) {
                        localStorage.removeItem('qudiscount');
                    }
                    if (localStorage.getItem('qutax2')) {
                        localStorage.removeItem('qutax2');
                    }
                    if (localStorage.getItem('qushipping')) {
                        localStorage.removeItem('qushipping');
                    }
                    if (localStorage.getItem('quref')) {
                        localStorage.removeItem('quref');
                    }
                    if (localStorage.getItem('quwarehouse')) {
                        localStorage.removeItem('quwarehouse');
                    }
                    if (localStorage.getItem('qunote')) {
                        localStorage.removeItem('qunote');
                    }
                    if (localStorage.getItem('quinnote')) {
                        localStorage.removeItem('quinnote');
                    }
                    if (localStorage.getItem('qucustomer')) {
                        localStorage.removeItem('qucustomer');
                    }
                    if (localStorage.getItem('qucurrency')) {
                        localStorage.removeItem('qucurrency');
                    }
                    if (localStorage.getItem('qudate')) {
                        localStorage.removeItem('qudate');
                    }
                    if (localStorage.getItem('qustatus')) {
                        localStorage.removeItem('qustatus');
                    }
                    if (localStorage.getItem('qubiller')) {
                        localStorage.removeItem('qubiller');
                    }
                    if (localStorage.getItem('qusupplier')) {
                        localStorage.removeItem('qusupplier');
                    }
                    if (localStorage.getItem('othercurrency')) {
                        localStorage.removeItem('othercurrency');
                    }
                    if (localStorage.getItem('othercurrencycode')) {
                        localStorage.removeItem('othercurrencycode');
                    }
                    if (localStorage.getItem('othercurrencytrm')) {
                        localStorage.removeItem('othercurrencytrm');
                    }
                    if (localStorage.getItem('retenciones')) {
                        localStorage.removeItem('retenciones');
                    }
                    $('#modal-loading').show();
                    location.reload();
                }
            });
    });

// save and load the fields in and/or from localStorage

    $('#quref').change(function (e) {
        localStorage.setItem('quref', $(this).val());
    });
    if (quref = localStorage.getItem('quref')) {
        $('#quref').val(quref);
    }
    $('#quwarehouse').change(function (e) {
        localStorage.setItem('quwarehouse', $(this).val());
    });
    if (quwarehouse = localStorage.getItem('quwarehouse')) {
        $('#quwarehouse').select2("val", quwarehouse);
    }

    $('#qunote').redactor('destroy');
    $('#qunote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('qunote', v);
        }
    });
    if (qunote = localStorage.getItem('qunote')) {
        $('#qunote').redactor('set', qunote);
    }
    var $customer = $('#qucustomer');
    $customer.change(function (e) {
        localStorage.setItem('qucustomer', $(this).val());
    });
    if (qucustomer = localStorage.getItem('qucustomer')) {
        $customer.val(qucustomer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    } else {
        nsCustomer();
    }


// prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });

// Order tax calculation
if (site.settings.tax2 != 0) {
    $('#qutax2').change(function () {
        localStorage.setItem('qutax2', $(this).val());
        loadItems();
        return;
    });
}

// Order discount calculation
var old_qudiscount;
$('#qudiscount').focus(function () {
    old_qudiscount = $(this).val();
}).change(function () {
    var new_discount = $(this).val() ? $(this).val() : '0';
    if (is_valid_discount(new_discount)) {
        localStorage.removeItem('qudiscount');
        localStorage.setItem('qudiscount', new_discount);
        loadItems();
        return;
    } else {
        $(this).val(old_qudiscount);
        bootbox.alert(lang.unexpected_value);
        return;
    }

});

/* ----------------------
 * Delete Row Method
 * ---------------------- */
$(document).on('click', '.qudel', function () {
    var row = $(this).closest('tr');
    var item_id = row.attr('data-item-id');
    delete quitems[item_id];
    row.remove();
    if(quitems.hasOwnProperty(item_id)) { } else {
        localStorage.setItem('quitems', JSON.stringify(quitems));
        loadItems();
        return;
    }
});

    /* -----------------------
     * Edit Row Modal Hanlder
     ----------------------- */
     $(document).on('click', '.edit', function () {
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }


        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        item_id = row.attr('data-item-id');
        item = quitems[item_id];
        var qty = row.children().children('.rquantity').val(),
        product_option = row.children().children('.roption').val(),
        unit_cost = formatDecimal(row.children().children('.rucost').val()),
        discount = row.children().children('.rdiscount').val();
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == item.row.option && this.cost != 0 && this.cost != '' && this.cost != null) {
                    unit_cost = parseFloat(item.row.real_unit_cost)+parseFloat(this.cost);
                }
            });
        }
        var real_unit_cost = item.row.real_unit_cost;
        var net_cost = unit_cost;
        $('#prModalLabel').text(item.row.name + ' (' + item.row.code + ')');
        if (site.settings.tax1) {
            $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax2').val(item.row.consumption_purchase_tax);
            $('#old_tax').val(item.row.tax_rate);
            var item_discount = 0, ds = discount ? discount : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal(parseFloat(((unit_cost) * parseFloat(pds[0])) / 100), 4);
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
            net_cost -= item_discount;
            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            if (quitems[item_id].row.tax_method == 0) {
                                pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_cost -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }
                        } else if (this.type == 2) {
                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;
                        }
                    }
                });
            }
        }
        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.options !== false) {
            var o = 1;
            opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if(o == 1) {
                    if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        } else {
            product_variant = 0;
        }

        uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if (item.units) {
            uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
            $.each(item.units, function () {
                if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if(this.product_unit_id == item.row.product_unit_id_selected) {
                        $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                    }
                } else {
                    if(this.id == item.row.unit) {
                        $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                    }
                }
            });
        }

        $('#poptions-div').html(opt);
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        $('#old_qty').val(qty);
        $('#pcost').val(trmrate * unit_cost);
        $('#pcost_ipoconsumo').val('');
        $('#punit_cost').val(formatDecimal(parseFloat(unit_cost)+parseFloat(pr_tax_val)));
        $('#poption').select2('val', item.row.option);
        $('#old_cost').val(unit_cost);
        $('#row_id').val(row_id);
        $('#item_id').val(item_id);
        $('#pserial').val(row.children().children('.rserial').val());
        $('#pdiscount').val(getDiscountAssigned(discount, trmrate, 1));
        $('#net_cost').text(formatMoney(trmrate * (net_cost-item_discount)));
        $('#pro_tax').text(formatMoney(trmrate * (parseFloat(pr_tax_val) + parseFloat(item.row.consumption_purchase_tax))));
        $('#prModal').appendTo("body").modal('show');
        $('#pname').val(item.row.name);

        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            units = item.units;
            unit_selected = units[item.row.product_unit_id_selected];
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty / unit_selected.operation_value;
                unit_selected_real_quantity = 1 * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty * unit_selected.operation_value;
                unit_selected_real_quantity = 1 / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            }
            $('#pquantity').val(unit_selected_quantity);
            $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
            $('#pproduct_unit_cost').val(unit_cost * unit_selected_real_quantity);
        }
        $('.purchase_form_edit_product_unit_name').text($('#punit option:selected').text());

    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });

    $(document).on('change', '#pcost, #pcost_ipoconsumo, #ptax, #pdiscount', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var unit_cost = parseFloat($('#pcost').val());
            var pcost_ipoconsumo = parseFloat($('#pcost_ipoconsumo').val());
        var item = quitems[item_id];
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';

        if (pcost_ipoconsumo > 0 && parseFloat(pcost_ipoconsumo) > parseFloat(item.row.consumption_purchase_tax)) {
            pcost_ipoconsumo = pcost_ipoconsumo - parseFloat(item.row.consumption_purchase_tax);
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            if (item.row.tax_method == 1 && pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            unit_cost = pcost_ipoconsumo / ((parseFloat(this.rate) / 100) + 1);
                        } else if (this.type == 2) {
                            unit_cost = pcost_ipoconsumo - parseFloat(this.rate);
                        }
                    }
                });
            } else if (item.row.tax_method == 0) {
                unit_cost = pcost_ipoconsumo;
            }
        }
        
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_cost) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_cost -= item_discount;
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_cost -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_cost) * parseFloat(this.rate)) / 100), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        if ($('#pcost_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pcost_ipoconsumo').val()) > parseFloat(item.row.consumption_purchase_tax)) {
            $('#pcost').val(formatDecimal(unit_cost));
        } else if ($('#pcost_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pcost_ipoconsumo').val()) > parseFloat(item.row.consumption_purchase_tax)) {
            $('#pcost').val(formatDecimal(pcost_ipoconsumo));
        } else if (parseFloat($('#pcost_ipoconsumo').val()) <= parseFloat(item.row.consumption_purchase_tax)) {
            Command: toastr.error('Por favor ingrese un costo válido', 'Valores incorrectos', {onHidden : function(){}})
            $('#pcost_ipoconsumo').val(0);
        }
        $('#net_cost').text(formatMoney(unit_cost));
        setTimeout(function() {
            $('#pcost_ipoconsumo').val('');
        }, 500);
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(item.row.consumption_purchase_tax)));
    });

    $(document).on('change', '#punit', function () {
        $('.purchase_form_edit_product_unit_name').text($('#punit option:selected').text());
    });

    $(document).on('change', '#pproduct_unit_cost', function(){
        p_unit_cost = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = quitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), acost = 0;

        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 1) {
                valor = unit_selected.operation_value;
                if (unit_selected.operator == "*") {
                    valor =  parseFloat(p_unit_cost) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  parseFloat(p_unit_cost) * parseFloat(unit_selected.operation_value);
                }
                $('#pcost').val(valor);
            } else {
                $('#pcost').val(p_unit_cost);
            }
            $('#pcost').change();
        }

    });

    /* -----------------------
     * Edit Row Method
     ----------------------- */
     $(document).on('click', '#editItem', function () {

        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }

        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = false;
        if (new_pr_tax) {
            $.each(tax_rates, function () {
                if (this.id == new_pr_tax) {
                    new_pr_tax_rate = this;
                }
            });
        }
        var cost = parseFloat($('#pcost').val());
        if(item.options !== false) {
            var opt = $('#poption').val();
            $.each(item.options, function () {
                if(this.id == opt && this.cost != 0 && this.cost != '' && this.cost != null) {
                    cost = cost-parseFloat(this.cost);
                }
            });
        }
        if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
            if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > cost) {
                bootbox.alert(lang.unexpected_value);
                return false;
            }
        }
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var unit = $('#punit').val();
        var base_quantity = parseFloat($('#pquantity').val());

        quitems[item_id].row.fup = 1,
        quitems[item_id].row.qty = parseFloat($('#pquantity').val()),
        quitems[item_id].row.base_quantity = parseFloat(base_quantity),
        quitems[item_id].row.real_unit_cost = cost / trmrate,
        quitems[item_id].row.unit = unit,
        quitems[item_id].row.tax_rate = new_pr_tax,
        quitems[item_id].tax_rate = new_pr_tax_rate,
        quitems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '',
        quitems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '',
        quitems[item_id].row.serial = $('#pserial').val();
        quitems[item_id].row.name = $('#pname').val();
        quitems[item_id].row.product_unit_id_selected = $('#punit').val();
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            var ditem = quitems[item_id];
            unit_selected = ditem.units[unit];
            qty = parseFloat($('#pquantity').val());
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            }
            quitems[item_id].row.qty = unit_selected_quantity;
            quitems[item_id].row.base_quantity = unit_selected_quantity;
        } else {
            quitems[item_id].row.unit_price_id = $('#punit').val();
        }
        localStorage.setItem('quitems', JSON.stringify(quitems));
        $('#prModal').modal('hide');

        loadItems();
        return;
    });

    /* -----------------------
     * Product option change
     ----------------------- */
     $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = quitems[item_id];
        var unit = $('#punit').val(), base_quantity = parseFloat($('#pquantity').val()), base_unit_cost = item.row.base_unit_cost;
        if(unit != quitems[item_id].row.base_unit) {
            $.each(quitems[item_id].units, function(){
                if (this.id == unit) {
                    base_unit_cost = formatDecimal((parseFloat(item.row.base_unit_cost)*(unitToBaseQty(1, this))), 4)
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == opt && this.cost != 0 && this.cost != '' && this.cost != null) {
                    $('#pcost').val(parseFloat(base_unit_cost)+(parseFloat(this.cost))).trigger('change');
                }
            });
        }
    });

    /* ------------------------------
     * Show manual item addition modal
     ------------------------------- */
     $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            quitems = {};
            if ($('#quwarehouse').val() && $('#qucustomer').val()) {
                $('#qucustomer').select2("readonly", true);
                $('#quwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#mnet_cost').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });

     $(document).on('click', '#addItemManually', function (e) {
        var mid = (new Date).getTime(),
        mcode = $('#mcode').val(),
        mname = $('#mname').val(),
        mtax = parseInt($('#mtax').val()),
        mqty = parseFloat($('#mquantity').val()),
        mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
        unit_cost = parseFloat($('#mcost').val()),
        mtax_rate = {};
        if (mcode && mname && mqty && unit_cost) {
            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });

            quitems[mid] = {"id": mid, "item_id": mid, "label": mname + ' (' + mcode + ')', "row": {"id": mid, "code": mcode, "name": mname, "quantity": mqty, "cost": unit_cost, "unit_cost": unit_cost, "real_unit_cost": unit_cost, "tax_rate": mtax, "tax_method": 0, "qty": mqty, "type": "manual", "discount": mdiscount, "serial": "", "option":""}, "tax_rate": mtax_rate, "options":false};
            localStorage.setItem('quitems', JSON.stringify(quitems));
            loadItems();
        }
        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mcost').val('');
        return false;
    });

    $(document).on('change', '#mcost, #mtax, #mdiscount', function () {
        var unit_cost = parseFloat($('#mcost').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_cost) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_cost -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_cost -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal(((unit_cost) * parseFloat(this.rate)) / 100);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#mnet_cost').text(formatMoney(unit_cost));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */
     var old_row_qty;
     $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        quitems[item_id].row.base_quantity = new_qty;
        if(quitems[item_id].row.unit != quitems[item_id].row.base_unit) {
            $.each(quitems[item_id].units, function(){
                if (this.id == quitems[item_id].row.unit) {
                    quitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        quitems[item_id].row.qty = new_qty;
        localStorage.setItem('quitems', JSON.stringify(quitems));
        loadItems();
    }).on('keydown', '.rquantity', function(e){
        if (e.keyCode == 13) {
            setTimeout(function() {
                $('#add_item').focus();
            }, 1200); //Se da un tiempo de espera para saltar todos las validaciones con parámetro set focus, en caso de formalizar la solicitud, se elimina la función completamente de dicho parámetro. By: Henry
        }
    });

    /* --------------------------
     * Edit Row cost Method
     -------------------------- */
    var old_cost;
    $(document).on("focus", '.rcost', function () {
        old_cost = $(this).val();
    }).on("change", '.rcost', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_cost);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_cost = parseFloat($(this).val()),
                item_id = row.attr('data-item-id');
        quitems[item_id].row.cost = new_cost;
        localStorage.setItem('quitems', JSON.stringify(quitems));
        loadItems();
    });

    $(document).on("click", '#removeReadonly', function () {
       $('#qucustomer').select2('readonly', false);
       //$('#quwarehouse').select2('readonly', false);
       return false;
    });

    setTimeout(function() {
        $('#paid_by_1 option').each(function(index, option){
            if($(option).val() == 'Credito'){
                $(option).remove();
            }
        });
    }, 850);


});
/* -----------------------
 * Misc Actions
 ----------------------- */

// hellper function for customer if no localStorage value
function nsCustomer() {
    $('#qucustomer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "customers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function nsSupplier() {
    $('#qusupplier').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
//aca load
//localStorage.clear();
function loadItems() {

    if (localStorage.getItem('quitems')) {

        $('#qusupplier').select2('readonly', true);
        $('#quwarehouse').select2('readonly', true);
        $('#currency').select2('readonly', true);
        $('#trm').prop('readonly', true);

        total = 0;
        subtotal = 0;
        count = 1;
        an = 1;
        product_tax = 0;
        product_tax_2 = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;
        var trmrate = 1;

        $("#quTable tbody").empty();
        quitems = JSON.parse(localStorage.getItem('quitems'));
        sortedItems =  _.sortBy(quitems, function(o) { return [parseInt(o.order)]; });
        if (site.settings.product_order == 2) {
            sortedItems.reverse();
        }
        $('#add_sale, #edit_sale').attr('disabled', false);
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, 
                item_type = item.row.type, 
                combo_items = item.combo_items, 
                item_cost = item.row.cost, 
                item_qty = item.row.qty, 
                item_aqty = item.row.quantity, 
                item_tax_method = item.row.tax_method, 
                item_ds = item.row.discount, 
                item_discount = 0, 
                item_option = item.row.option, 
                item_code = item.row.code, 
                item_serial = item.row.serial, 
                item_unit_id_selected = item.row.product_unit_id_selected, 
                item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");

            var unit_cost = item.row.real_unit_cost;
            var unit_cost = item.row.real_unit_cost;
            var product_unit = item.row.unit, base_quantity = item.row.base_quantity;
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item.row.option && this.cost != 0 && this.cost != '' && this.cost != null) {
                        item_cost = unit_cost+(parseFloat(this.cost));
                        unit_cost = item_cost;
                    }
                });
            }

            var pr_tax = item.tax_rate;
            var pr_ptev_tax_val = pr_tax_val = pr_tax_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_cost, item_tax_method))) {
                pr_ptev_tax_val = pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }

            var pr_tax_2 = item.tax_rate_2;
            var pr_ptev_tax_2_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax_2, unit_cost, item_tax_method))) {
                pr_ptev_tax_2_val = ptax[0];
            }

            var pr_tax = item.tax_rate;
            var pr_tax_val = pr_tax_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_cost - (item_tax_method == 1 ? 0 : pr_ptev_tax_val), 1))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
                product_tax += pr_tax_val * item_qty;
            }

            var pr_tax_2 = item.tax_rate_2;
            var pr_tax_2_val = pr_tax_2_rate = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax_2, unit_cost - (item_tax_method == 1 ? 0 : pr_ptev_tax_2_val), 1))) {
                pr_tax_2_val =  ptax[0];
                pr_tax_2_rate = ptax[1];
                product_tax_2 += pr_tax_2_val * item_qty;
            }

            item_cost = item_tax_method == 0 ? formatDecimal(unit_cost-(pr_ptev_tax_val+pr_ptev_tax_2_val)) : formatDecimal(unit_cost);
            unit_cost = formatDecimal(unit_cost+item_discount);

            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.name;
                }
            });

            if (othercurrency = localStorage.getItem('othercurrency')) {
                rate = localStorage.getItem('othercurrencyrate');
                if (trm = localStorage.getItem('othercurrencytrm')) {
                    trmrate = rate / trm;
                }
            }

            pr_tax_2_val = item.row.consumption_purchase_tax;
            pr_tax_2_rate = item.row.consumption_purchase_tax;
            product_tax_2 += pr_tax_2_val;

            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                            '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                            '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                            '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                            '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                            '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(sel_opt != '' ? ' ('+sel_opt+')' : '')+'</span> <i class="pull-right fa fa-edit tip pointer edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>'+
                        '</td>';
            if (site.settings.product_variant_per_serial == 1) {
                tr_html += '<td>'+
                                (item.row.serialModal_serial !== undefined ? item.row.serialModal_serial : '')+
                            '</td>';
            }
            tr_html += '<td class="text-right">'+
                            '<input class="form-control input-sm text-right rcost" name="net_cost[]" type="hidden" id="cost_' + row_no + '" value="' + formatDecimal(item_cost) + '">'+
                            '<input class="rucost" name="unit_cost[]" type="hidden" value="' + unit_cost + '">'+
                            '<input class="realucost" name="real_unit_cost[]" type="hidden" value="' + item.row.real_unit_cost + '">'+
                            '<span class="text-right scost" id="scost_' + row_no + '">' + formatMoney(trmrate * item_cost) + '</span>'+
                        '</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + (item_unit_id_selected != undefined ? item_unit_id_selected : product_unit) + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + base_quantity + '">'+
                        '</td>';
            if ((site.settings.product_discount == 1 && allow_discount == 1) || item_discount) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney(trmrate * (0 - (item_discount * item_qty))) + '</span>'+
                            '</td>';
            }
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + pr_tax.id + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_val[]" type="hidden" id="product_tax_' + row_no + '" value="' + pr_tax_val + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + (pr_tax_rate ? '(' + formatDecimal(pr_tax_rate) + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_val * item_qty)) + '</span>'+
                            '</td>';
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + pr_tax_2_rate + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_val_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + pr_tax_2_val + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '_2">' + (pr_tax_2_rate ? '(' + formatDecimal(pr_tax_2_rate) + ')' : '') + ' ' + formatMoney(trmrate * (pr_tax_2_val * item_qty)) + '</span>'+
                            '</td>';
            }
            tr_html += '<td class="text-right">'+
                            '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(trmrate * ((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))) + '</span>'+
                            (item.row.serialModal_serial ? '<input name="serialModal_serial[]" type="hidden" value="' + item.row.serialModal_serial+ '">' : '' )+
                        '</td>';
            tr_html += '<td class="text-center"><i class="fa fa-times tip pointer qudel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#quTable");
            subtotal += formatDecimal(((parseFloat(item_cost)) * parseFloat(item_qty)), 4);
            total += formatDecimal(((parseFloat(item_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)), 4);
            count += parseFloat(item_qty);
            an++;
            if (item_type == 'standard' && item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item_option && base_quantity > this.quantity) {
                        $('#row_' + row_no).addClass('danger');
                        if(site.settings.overselling != 1) { $('#add_sale, #edit_sale').attr('disabled', true); }
                    }
                });
            } else if(item_type == 'standard' && base_quantity > item_aqty) {
                // $('#row_' + row_no).addClass('danger');
            } else if (item_type == 'combo') {
                if(combo_items === false) {
                    $('#row_' + row_no).addClass('danger');
                } else {
                    $.each(combo_items, function() {
                       if(parseFloat(this.quantity) < (parseFloat(this.qty)*base_quantity) && this.type == 'standard') {
                           $('#row_' + row_no).addClass('danger');
                       }
                   });
                }
            }

        });

        var col = 1;
        if (site.settings.product_variant_per_serial == 1) {
            col++;
        }
        var tfoot = '<tr id="tfoot" class="tfoot active"><th colspan="'+col+'" >Total</th><th id="subtotal" class="text-right">'+formatMoney(trmrate * subtotal)+'</th><th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
        if ((site.settings.product_discount == 1 && allow_discount == 1) || product_discount) {
            tfoot += '<th class="text-right">'+formatMoney(trmrate * product_discount)+'</th>';
        }
        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax)+'</th>';
            tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax_2)+'</th>';
        }
        tfoot += '<th class="text-right" id="totalP">'+formatMoney(trmrate * total)+'</th><th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th></tr>';
        $('#quTable tfoot').html(tfoot);

        // Order level discount calculations
        if (qudiscount = localStorage.getItem('qudiscount')) {
            var ds = qudiscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    // order_discount = formatDecimal((((total) * parseFloat(pds[0])) / 100), 4);
                    order_discount = formatDecimal((((subtotal) * parseFloat(pds[0])) / 100), 4);
                } else {
                    order_discount = formatDecimal(ds);
                }
            } else {
                order_discount = formatDecimal(ds);
            }

            //total_discount += parseFloat(order_discount);
        }

        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (qutax2 = localStorage.getItem('qutax2')) {
                $.each(tax_rates, function () {
                    if (this.id == qutax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        }
                        if (this.type == 1) {
                            invoice_tax = formatDecimal((((subtotal - order_discount) * this.rate) / 100), 4);
                        }
                    }
                });
            }
        }

        // Retenciones
        if (retenciones = JSON.parse(localStorage.getItem('retenciones'))) {
            total_rete_amount = formatDecimal(retenciones.total_rete_amount);
            // console.log('Retención : '+total_rete_amount);
        } else {
            total_rete_amount = 0;
        }
        //Retenciones

        total_discount = parseFloat(order_discount + product_discount);
        // Totals calculations after item addition
        var gtotal = parseFloat(((total + invoice_tax) - order_discount) + shipping);
        $('#total').text(formatMoney(trmrate * total));
        $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        $('#tds').text(formatMoney(trmrate * order_discount));
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(trmrate * invoice_tax));
        }
        $('#tship').text(formatMoney(trmrate * shipping));
        $('#gtotal').text(formatMoney(trmrate * gtotal));
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        if (site.settings.product_order == 2) {
            an = 2;
        }
        set_page_focus(an);
    }
}

/* -----------------------------
 * Add Quotation Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
 function add_invoice_item(item, modal = false, cont = false) {

    if (count == 1) {
        quitems = {};
        if  (   $('#quwarehouse').val()
                && $('#qusupplier').val()
                && (($('#currency').val() == site.settings.default_currency && !$('#trm').val()) || ($('#currency').val() != site.settings.default_currency && $('#trm').val()))
            ) {
            $('#qucustomer').select2("readonly", true);
            $('#quwarehouse').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }
    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (quitems[item_id]) {

        var new_qty = parseFloat(quitems[item_id].row.qty) + 1;
        quitems[item_id].row.base_quantity = new_qty;
        if(quitems[item_id].row.unit != quitems[item_id].row.base_unit) {
            $.each(quitems[item_id].units, function(){
                if (this.id == quitems[item_id].row.unit) {
                    quitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        quitems[item_id].row.qty = new_qty;

    } else {
        quitems[item_id] = item;
    }

    quitems[item_id].order = new Date().getTime();

    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {

        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(quitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(quitems[item_id]));
        delete quitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
        }, 850);

    } else {

        //NUEVO

        localStorage.removeItem('modalSerial_item');
        if (site.settings.product_variant_per_serial == 1 && cont == false) {
            $('#serialModal').modal('hide');
            $('#add_item').val('').focus();
        } else {
            item = quitems[item_id];
            $.ajax({
                url : site.base_url + "products/get_random_id"
            }).done(function(data){
                item.id = data;
                console.log(item);
                localStorage.setItem('modalSerial_item', JSON.stringify(item));
                $('#serialModal_serial').val('');
                $('#serialModal_meters').val('');
            });
                
        }
        
        localStorage.setItem('quitems', JSON.stringify(quitems));
        loadItems();
        return true;

        //NUEVO

    }
        
}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}

$('#currency').on('change', function(){
    default_currency = site.settings.default_currency;
    actual_currency = $(this).val();
    actual_currency_rate = $('#currency option:selected').data('rate');

    if (default_currency != actual_currency) {
        $('.trm-control').css('display', '');
        $('#trm').prop('required', true);

        if (othercurrencytrm = localStorage.getItem('othercurrencytrm')) {
            $('#trm').val(othercurrencytrm);
        }

        localStorage.setItem('othercurrency', true);
        localStorage.setItem('othercurrencycode', actual_currency);
        localStorage.setItem('othercurrencyrate', actual_currency_rate);

        $('.currency').text(actual_currency);

    } else {
        $('.trm-control').css('display', 'none');
        $('#trm').prop('required', false);
        localStorage.removeItem('othercurrency');
        localStorage.removeItem('othercurrencycode');
        localStorage.removeItem('othercurrencyrate');
        localStorage.removeItem('othercurrencytrm');
        $('#trm').val('');
        $('.currency').text(default_currency);
    }


    loadItems();

});

$('#trm').on('change', function(){
    trm = $(this).val();

    if(trm != '' && trm != 0) {
        localStorage.setItem('othercurrencytrm', trm);
    } else {
        localStorage.removeItem('othercurrencytrm');
    }

    loadItems();

});

if (currencycode = localStorage.getItem('othercurrencycode')) {
    $('#currency').val(currencycode).trigger('change');
}

// JS Retenciones


    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_fuente_option').val('').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_iva_option').val('').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_ica_option').val('').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_otros_option').val('').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option', function(){

        trmrate = getTrmRate();

        prevamnt = $('#rete_fuente_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_fuente_option option:selected').data('percentage');
        apply = $('#rete_fuente_option option:selected').data('apply');
        account = $('#rete_fuente_option option:selected').data('account');

        $('#rete_fuente_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base fuente : '+amount);
        $('#rete_fuente_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_fuente_tax').val(percentage);
        $('#rete_fuente_valor_show').val(formatDecimal(trmrate * cAmount));
        $('#rete_fuente_valor').val(cAmount);

        rete_fuente_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_fuente_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });

    $(document).on('change', '#rete_iva_option', function(){

        trmrate = getTrmRate();

        prevamnt = $('#rete_iva_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_iva_option option:selected').data('percentage');
        apply = $('#rete_iva_option option:selected').data('apply');
        account = $('#rete_iva_option option:selected').data('account');

        $('#rete_iva_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base iva : '+amount);
        $('#rete_iva_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_iva_tax').val(percentage);
        $('#rete_iva_valor_show').val(formatDecimal(trmrate * cAmount));
        $('#rete_iva_valor').val(cAmount);

        rete_iva_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_iva_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });

    $(document).on('change', '#rete_ica_option', function(){

        trmrate = getTrmRate();

        prevamnt = $('#rete_ica_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_ica_option option:selected').data('percentage');
        apply = $('#rete_ica_option option:selected').data('apply');
        account = $('#rete_ica_option option:selected').data('account');

        $('#rete_ica_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base ica : '+amount);
        $('#rete_ica_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_ica_tax').val(percentage);
        $('#rete_ica_valor_show').val(formatDecimal(trmrate * cAmount));
        $('#rete_ica_valor').val(cAmount);

        rete_ica_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_ica_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });

    $(document).on('change', '#rete_otros_option', function(){

        trmrate = getTrmRate();

        prevamnt = $('#rete_otros_valor').val();
        setReteTotalAmount(prevamnt, '-');

        percentage = $('#rete_otros_option option:selected').data('percentage');
        apply = $('#rete_otros_option option:selected').data('apply');
        account = $('#rete_otros_option option:selected').data('account');

        $('#rete_otros_account').val(account);

        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));

        // console.log('Base otros : '+amount);
        $('#rete_otros_base').val(amount);

        cAmount = amount * (percentage / 100);

        cAmount = Math.round(cAmount);

        $('#rete_otros_tax').val(percentage);
        $('#rete_otros_valor_show').val(formatDecimal(trmrate * cAmount));
        $('#rete_otros_valor').val(cAmount);

        rete_otros_amount = formatMoney(cAmount);

        setReteTotalAmount(rete_otros_amount, '+');

        // console.log(apply+", "+amount+" * "+percentage+" = "+cAmount);
    });


    function getReteAmount(apply){

        amount = 0;

        if (apply == "ST") {
            amount = $('#subtotalP').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#totalP').text();
        }

        if (othercurrency = localStorage.getItem('othercurrency')) {
            othercurrencytrm = localStorage.getItem('othercurrencytrm');
            if (othercurrencytrm > 0) {
                amount = amount * othercurrencytrm;
            }
        }

        return amount;
    }

    function setReteTotalAmount(amount, action){

        // console.log('Monto : '+amount+' acción : '+action);

        if (action == "+") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) + formatDecimal(amount);
        } else if (action == "-") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) - formatDecimal(amount);
        }

        if (tra < 0) {
            tra = 0;
        }

        trmrate = getTrmRate();
        $('#total_rete_amount_show').text(formatMoney(trmrate * tra));
        $('#total_rete_amount').text(formatMoney(tra));
    }


// JS Retenciones

 $(document).on('click', '#updateOrderRete', function () {
        // var ts = $('#order_tax_input').val();
        // $('#postax2').val(ts);

        $('#ajaxCall').fadeIn();

        var retenciones = {
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            };

        total_rete_amount = parseFloat(formatDecimal($('#total_rete_amount').text()));
        trmrate = getTrmRate();
        total_rete_amount = trmrate * total_rete_amount;

        retenciones.total_discounted = formatMoney(parseFloat(formatDecimal($('#gtotal').text())) - total_rete_amount);


        setTimeout(function() {
            $('#gtotal').text(retenciones.total_discounted);
            $('#ajaxCall').fadeOut();
        }, 1000);

        localStorage.setItem('retenciones', JSON.stringify(retenciones));
        // loadItems();


        if (parseFloat(formatDecimal(retenciones.total_rete_amount)) > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(retenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }

        $('#rete').val(retenciones.total_rete_amount);
        $('#retencion_show').val(formatDecimal(total_rete_amount));
        $('#rtModal').modal('hide');
     });

     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('retenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });

function recalcular_retenciones(){

    // $('#rete_applied').val(1);
            // $('#total_rete_amount').text(retenciones.total_rete_amount);
            // $('#gtotal_rete_amount').val(parseFloat(formatDecimal(retenciones.total_rete_amount)));
            // $('#rete').text(retenciones.total_rete_amount);
    retenciones = JSON.parse(localStorage.getItem('retenciones'));

    if (retenciones != null) {
            if (retenciones.id_rete_fuente != '') {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').trigger('click');
                }
            }

            if (retenciones.id_rete_iva != '') {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').trigger('click');
                }
            }

            if (retenciones.id_rete_ica != '') {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').trigger('click');
                }
            }

            if (retenciones.id_rete_otros != '') {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').trigger('click');
                }
            }

            setTimeout(function() {

                $.each($('#rete_fuente_option option'), function(index, value){
                    if(retenciones.id_rete_fuente != '' && value.value == retenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_iva_option option'), function(index, value){
                    if(retenciones.id_rete_iva != '' && value.value == retenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_ica_option option'), function(index, value){
                    if(retenciones.id_rete_ica != '' && value.value == retenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_otros_option option'), function(index, value){
                    if(retenciones.id_rete_otros != '' && value.value == retenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', value.value).trigger('change');
                    }
                });

                $('#rete_otros_option').select2().trigger('change');

                $('#updateOrderRete').trigger('click');

            }, 2000);

    }

}

if ($('#rete_applied').val()) {
  total_rete_amount = $('#total_rete_amount').text();
  total_rete_amount = parseFloat(formatDecimal(total_rete_amount));
} else {
  total_rete_amount = 0;
}

// JS Retenciones


function getTrmRate(){
    trmrate = 1;

    if (othercurrency = localStorage.getItem('othercurrency')) {
        othercurrencytrm = localStorage.getItem('othercurrencytrm');
        othercurrencyrate = localStorage.getItem('othercurrencyrate');
        othercurrencycode = localStorage.getItem('othercurrencycode');

        trmrate = othercurrencyrate / othercurrencytrm;
    }

    return trmrate;
}


$(document).on('change', '#qu_payment_status', function(){
    if ($(this).val() == 1) {
        $('.div_paid_by').fadeIn();
        $('.div_payment_term').fadeOut();
    } else if ($(this).val() == 0) {
        $('.div_paid_by').fadeOut();
        $('.div_payment_term').fadeIn();
    }
});



$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});

$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});


function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.base_quantity = meters;
        item.row.type = 'addition';
        add_invoice_item(item, true, cont);
    }
}

$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});

$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});

$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});

function validate_serial(serialModal, e = null){
    if ( e == null || e != null && e.keyCode == 13) {

        quitems = JSON.parse(localStorage.getItem('quitems'));
        if (quitems) {
            $.each(quitems, function(index, item){
                if (item.row.serialModal_serial == $('#serialModal_serial').val()) {
                    setTimeout(function() {
                        command: toastr.warning('El serial ingresado ya se registró en este formulario.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                        $('#serialModal_serial').val('').focus();
                        }, 800);
                    return false;
                }
            });
        }

        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        $.ajax({
            url : site.base_url + 'products/get_product_serial_variant/'+serial_item.row.id+'/'+serialModal
        }).done(function(data){
            if (data == '1') {
                if (!localStorage.getItem('validated_serial')) {
                    localStorage.setItem('validated_serial', true);
                    bootbox.confirm('El serial ingresado ya existe para este producto. ¿Desea registrar más cantidades?', function (result) {
                        if (result) {
                            setTimeout(function() {
                                $('#serialModal').modal('show');
                            }, 400);
                            setTimeout(function() {
                                $('#serialModal_meters').focus();
                                localStorage.removeItem('validated_serial');
                            }, 1000);
                        } else {
                            setTimeout(function() {
                                $('#serialModal').modal('show');
                            }, 400);
                            setTimeout(function() {
                                $('#serialModal_serial').val('').focus();
                                localStorage.removeItem('validated_serial');
                            }, 1000);
                        }
                    });
                }
            } else {
                $('#serialModal_meters').focus();
            }
        });
    }
}

$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});