<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <?php if ($this->Settings->product_variant_per_serial == 1): ?>
                <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/adjustment_zebra_products_print/'.$inv->id) ?>" target="_blank">
                    <i class="fa fa-print"></i> <?= lang('print_zebra'); ?>
                </a>
            <?php endif ?>
           <!--  <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button> -->
            <a type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/view_adjustment_format/'.$inv->id) ?>" target="_blank">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </a>
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>" alt="<?= $Settings->site_name; ?>" style="width: 80px;">
            </div>
            <br>
            <div class="">
                <div class="row bold">
                    <div class="col-xs-5">
                    <h3><?= $document_type->nombre ?></h3>
                    <h3><?= $inv->reference_no ?></h3>
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("warehouse"); ?>: <?= $warehouse->name; ?><br>
                        <?= lang("created_by"); ?>: <?= $created_by->first_name." ".$created_by->last_name; ?><br>
                        <?= lang("employee"); ?>: <?= $employee->company; ?><br>
                        <?php if ($inv->origin_reference_no): ?>
                            <?= lang("origin"); ?>: <?= $inv->origin_reference_no; ?><br>
                        <?php endif ?>
                    </p>
                    </div>
                    <div class="col-xs-7 text-right order_barcodes">

                    </div>
                    <div class="clearfix"></div>
                    <?php if (isset($cost_center) && $cost_center): ?>
                        <div class="col-xs-6">
                            <p class="bold">
                                <?= lang("cost_center"); ?> : <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                            </p>
                        </div>
                    <?php endif ?>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover print-table order-table">

                    <thead>

                    <tr>
                        <th><?= lang("no"); ?></th>
                        <th><?= lang("code"); ?></th>
                        <th><?= lang("description"); ?></th>
                        <th><?= lang("variant"); ?></th>
                        <th><?= lang("type"); ?></th>
                        <th><?= lang("quantity"); ?></th>
                        <?php if ($this->Owner || $this->Admin): ?>
                            <th><?= lang("unit_cost"); ?></th>
                            <th><?= lang("total_cost"); ?></th>
                        <?php endif ?>
                    </tr>

                    </thead>

                    <tbody>

                    <?php $r = 1;

                    $total_qty = 0;
                    $total_cost = 0;

                    foreach ($rows as $row):
                    ?>
                        <tr>
                            <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                            <td><?= $row->product_code ?></td>
                            <td><?= $row->product_name ?></td>
                            <th><?= ($this->Settings->product_variant_per_serial == 1 ? ($row->serial_no ? $row->serial_no : '') : $row->variant); ?></th>
                            <th><?= ($row->type == 'addition') ? lang('entry') : lang('output') ?></th>
                            <td style="width: 80px; vertical-align:middle;" class="text-right"><?= $this->sma->formatNumber($row->quantity); ?></td>
                            <?php if ($this->Owner || $this->Admin): ?>
                                <td class="text-right"><?= $this->sma->formatMoney($row->adjustment_cost > 0 ? ($row->adjustment_cost /*/ $row->quantity*/) : $row->avg_cost); ?></td>
                                <td class="text-right"><?= $this->sma->formatMoney($row->adjustment_cost > 0 ? ($row->adjustment_cost * $row->quantity) : ($row->avg_cost * $row->quantity)); ?></td>
                            <?php endif ?>
                        </tr>
                        <?php
                        $total_qty += $row->quantity;
                        $total_cost += ($row->adjustment_cost > 0 ? ($row->adjustment_cost * $row->quantity) : ($row->avg_cost * $row->quantity));
                        $r++;
                    endforeach;
                    ?>
                    <tr>
                        <th colspan="5"></th>
                        <th class="text-right"><?= $this->sma->formatNumber($total_qty); ?></th>
                        <?php if ($this->Owner || $this->Admin): ?>
                            <th></th>
                            <th class="text-right"><?= $this->sma->formatMoney($total_cost); ?></th>
                        <?php endif ?>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="modal-footer">
            <div class="row">
                <div class="col-xs-12  text-left">
                    <?php if ($inv->note || $inv->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-xs-5 border no-right">
                  <p>&nbsp;</p>
                  <p>&nbsp;</p>
                  <p style="border-bottom: 1px solid #666;">&nbsp;</p>
                  <p style="text-align: center;">Recibido por</p>
                </div>
                <hr>
                <div class="col-xs-12 text-center">
                    <p>
                        <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                    </p>
                    <?php if ($inv->updated_by) { ?>
                    <p>
                        <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                        <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                    </p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</div>
