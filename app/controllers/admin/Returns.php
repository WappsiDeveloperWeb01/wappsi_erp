<?php defined('BASEPATH') or exit('No direct script access allowed');

class Returns extends MY_Controller {
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Supplier || $this->Customer) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('returns', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('returns_model');
        $this->load->admin_model('documentsTypes_model');
        $this->load->admin_model('Electronic_billing_model');
        $this->load->admin_model('pos_model');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('companies_model');
        if (!$this->session->userdata('register_id')) {
            if ($register = $this->pos_model->registerData($this->session->userdata('user_id'))) {
                $register_data = array('register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date);
                $this->session->set_userdata($register_data);
            }
        }

        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->data['logo'] = true;
    }

    public function index($warehouse_id = null)
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('returns')));
        $meta = array('page_title' => lang('returns'), 'bc' => $bc);
        $this->page_construct('returns/index', $meta, $this->data);
    }

    public function getReturns($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        $this->load->library('datatables');
        if ($warehouse_id) {
            $this->datatables
                ->select("{$this->db->dbprefix('returns')}.id as id, DATE_FORMAT({$this->db->dbprefix('returns')}.date, '%Y-%m-%d %T') as date, reference_no, biller, {$this->db->dbprefix('returns')}.customer, grand_total, {$this->db->dbprefix('returns')}.attachment")
                ->from('returns')
                ->where('warehouse_id', $warehouse_id);
        } else {
            $this->datatables
                ->select("{$this->db->dbprefix('returns')}.id as id, DATE_FORMAT({$this->db->dbprefix('returns')}.date, '%Y-%m-%d %T') as date, reference_no, biller, {$this->db->dbprefix('returns')}.customer, grand_total, {$this->db->dbprefix('returns')}.attachment")
                ->from('returns');
        }

        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('created_by', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", "<div class=\"text-center\"><a href='" . admin_url('returns/edit/$1') . "' class='tip' title='" . lang("edit_return") . "'><i class=\"fa fa-edit\"></i></a> <a href='#' class='tip po' title='<b>" . lang("delete_return") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('returns/delete/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a></div>", "id");
        echo $this->datatables->generate();
    }

    public function view($id = null)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->returns_model->getReturnByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->returns_model->getReturnItems($id);

        $this->load_view($this->theme . 'returns/view', $this->data);
    }

    public function add()
    {

        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');

        if ($this->form_validation->run() == true) {
            $date = ($this->Owner || $this->Admin) ? $this->sma->fld(trim($this->input->post('date'))) : date('Y-m-d H:i:s');
            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('re');
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->site->getProductByCode($item_code) : null;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax - $order_discount), 4);
            $data = array('date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
        }

        if ($this->form_validation->run() == true && $this->returns_model->addReturn($data, $products)) {
            $this->session->set_userdata('remove_rels', 1);
            $this->session->set_flashdata('message', lang("return_added"));
            admin_redirect("returns");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('returns'), 'page' => lang('returns')), array('link' => '#', 'page' => lang('add_return')));
            $meta = array('page_title' => lang('add_return'), 'bc' => $bc);
            $this->page_construct('returns/add', $meta, $this->data);
        }
    }

    public function edit($id = null)
    {

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->returns_model->getReturnByID($id);
        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');

        if ($this->form_validation->run() == true) {
            $date = ($this->Owner || $this->Admin) ? $this->sma->fld(trim($this->input->post('date'))) : $inv->date;
            $reference = $this->input->post('reference_no');
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->site->getProductByCode($item_code) : null;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);

                    $product = array(
                        'return_id' => $id,
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax - $order_discount), 4);
            $data = array('date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
        }

        if ($this->form_validation->run() == true && $this->returns_model->updateReturn($id, $data, $products)) {
            $this->session->set_userdata('remove_rels', 1);
            $this->session->set_flashdata('message', lang("return_updated"));
            admin_redirect("returns");
        } else {
            $this->data['inv'] = $inv;
            if ($this->Settings->disable_editing) {
                if ($this->data['inv']->date <= date('Y-m-d', strtotime('-'.$this->Settings->disable_editing.' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("return_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $inv_items = $this->returns_model->getReturnItems($id);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }

                $row->id = $item->product_id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->item_tax_method = $row->tax_method;
                $options = $this->returns_model->getProductOptions($row->id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->returns_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = false;
                }
                if ($row->promotion) {
                    $row->price = $row->promo_price;
                }
                $row->real_unit_price = $row->price;
                $row->base_quantity = $item->quantity;
                $row->base_unit = !empty($row->unit) ? $row->unit : $item->product_unit_id;
                $row->base_unit_price = !empty($row->price) ? $row->price : $item->unit_price;
                $row->unit = $item->product_unit_id;
                $row->qty = $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->serial = $item->serial_no;
                $row->option = $item->option_id;
                $row->tax_rate = $item->tax_rate_id;
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($row->id);
                }
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $ri = $this->Settings->item_addition ? $row->id : $c;

                $pr[$ri] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $c++;
            }
            $this->data['inv_items'] = json_encode($pr);
            $this->data['id'] = $id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('returns'), 'page' => lang('returns')), array('link' => '#', 'page' => lang('edit_return')));
            $meta = array('page_title' => lang('edit_return'), 'bc' => $bc);
            $this->page_construct('returns/edit', $meta, $this->data);
        }
    }

    public function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->returns_model->deleteReturn($id)) {
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("return_deleted")));
            }
            $this->session->set_flashdata('message', lang('return_deleted'));
            admin_redirect('welcome');
        }
    }

    public function suggestions()
    {
        $term = $this->input->get('term', true);

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];

        $rows = $this->returns_model->getProductNames($sr);
        if ($rows) {
            $r = 0;
            foreach ($rows as $row) {
                $c = uniqid(mt_rand(), true);
                $option = false;
                unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $row->item_tax_method = $row->tax_method;
                $options = $this->returns_model->getProductOptions($row->id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->returns_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = false;
                }
                $row->option = $option_id;
                if ($row->promotion) {
                    $row->price = $row->promo_price;
                }
                $row->real_unit_price = $row->price;
                $row->base_quantity = 1;
                $row->base_unit = $row->unit;
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->qty = 1;
                $row->discount = '0';
                $row->serial = '';
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($row->id);
                }
                $units = $this->site->getUnitsByBUID($row->base_unit);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);

                $pr[] = array('id' => sha1($c.$r), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function credit_note_other_concepts($sale_id = NULL)
    {
        $this->sma->checkPermissions();
        if (!$this->session->userdata('cash_in_hand')) {
            $this->session->set_flashdata('error', lang("no_data_pos_register"));
            admin_redirect('pos/index');
        }

        $reference_invoice = null;
        if (!empty($sale_id)) {
            $reference_invoice = $this->returns_model->get_sale($sale_id);
            $grand_total = $reference_invoice->grand_total;

            $total_value_credit_note = $this->returns_model->get_total_value_credit_notes_by_sale_id($sale_id);
            if ($total_value_credit_note !== FALSE) {
                $grand_total = $grand_total - abs($total_value_credit_note->total_return);
                $reference_invoice->grand_total = $grand_total;
            }

            $total_debit_notes = $this->returns_model->get_total_value_debit_notes_by_reference($reference_invoice->reference_debit_note);
            if ($total_debit_notes !== FALSE) {
                $grand_total = $grand_total + abs($total_debit_notes->total_debit);
                $reference_invoice->grand_total = $grand_total;
            }

            if ($grand_total == 0) {
                $this->session->set_flashdata('error', lang("La factura ya ha sido devuelta en su totalidad"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }

        $allow_payment = FALSE;
        if ($reference_invoice) {
            if (($reference_invoice->payment_status == 'paid' || $reference_invoice->payment_status == 'partial')) {
                $allow_payment = TRUE;
            }
        } else {
            $allow_payment = TRUE;
        }

        if ($this->session->userdata('detal_post_processing')) { $this->session->unset_userdata('detal_post_processing'); }

        $this->data["reference_invoice"] = $reference_invoice;
        $this->data['allow_payment'] = $allow_payment;
        $this->data['payment_ref'] = '';
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['tax_rates'] = $this->site->getAllTaxRates();
        $this->data["credit_note_concepts"] = $this->returns_model->get_credit_note_concepts();
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_centers'] = $this->site->getAllCostCenters();
        }

        $this->page_construct('returns/add_credit_note_other_concepts', ['page_title' => lang('credit_note_other_concepts_label_menu')], $this->data);
    }

    public function sale_reference_suggestions()
    {
        $term = $this->input->get("term");
        $module = $this->input->get("module");
        $customer_id = $this->input->get("customer_id");
        $is_electronic_document = $this->input->get("is_electronic_document");

        $result = $this->returns_model->get_sale_reference($term, $customer_id, $is_electronic_document, $module);

        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function get_sale_reference()
    {
        $term = $this->input->get("term");
        $module = $this->input->get("module");
        $customer_id = $this->input->get("customer_id");
        $is_electronic_document = $this->input->get("is_electronic_document");

        $result = $this->returns_model->get_sale_reference_by_id($term, $customer_id, $is_electronic_document, $module);

        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function get_sale_data()
    {
        $id = $this->input->post("id");

        $sale_data = $this->returns_model->get_sale($id);

        if (empty($sale_data)) {
            echo json_encode([
                "status" => FALSE,
                "error_message" => "No existe una venta con la referencia insertada"
            ]);
        } else {
            echo json_encode([
                "status" => TRUE,
                "sale_data" => $sale_data
            ]);
        }
    }

    public function add_credit_note_other_concepts()
    {
        $credit_note_data = $this->build_credit_note_data();
        $credit_note_item_data = $this->build_credit_note_item_data();
        $payment_data = $this->build_payment_data();
        $resolution_data = $this->site->getDocumentTypeById($this->input->post('document_type'));
        $externalInvoice = ($this->input->post("externalInvoice") || $this->input->post("documentWithoutReference"));

        if (!$this->session->userdata('detal_post_processing')) {
            $this->session->set_userdata('detal_post_processing', 1);
        } else {
            $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
            if ($resolution_data->factura_electronica == YES) {
                admin_redirect("sales/fe_index");
            } else {
                admin_redirect("sales/index");
            }
        }

        if (empty($credit_note_data)) {
            $this->session->set_flashdata("error", "No fue posible generar los datos para el encabezado de la nota crédito.");
            if ($resolution_data->factura_electronica == YES) {
                admin_redirect("sales/fe_index");
            } else {
                admin_redirect("sales/index");
            }
        }

        if (empty($credit_note_item_data)) {
            $this->session->set_flashdata("error", "No fue posible generar los datos para el detalle de la nota crédito.");
            if ($resolution_data->factura_electronica == YES) {
                admin_redirect("sales/fe_index");
            } else {
                admin_redirect("sales/index");
            }
        }
        if ($this->session->userdata('detal_post_processing')) { $this->session->unset_userdata('detal_post_processing'); }
        $credit_note_created = $this->returns_model->save_credit_note($credit_note_data, $credit_note_item_data, $payment_data, $externalInvoice);
        if ($credit_note_created !== FALSE) {

            if ($_SERVER['SERVER_NAME'] != 'localhost') {
                $credit_note_data = $this->site->getSaleByID($credit_note_created);
                if ($resolution_data->factura_electronica == YES) {
                    $invoice_data = [
                        'sale_id' => $credit_note_created,
                        'biller_id' => $credit_note_data->biller_id,
                        'customer_id' => $credit_note_data->customer_id,
                        'reference' => $credit_note_data->reference_no
                    ];

                    if ($resolution_data->factura_contingencia == NOT) {
                        $this->create_document_electronic($invoice_data);
                    }
                }
            }

            $this->session->set_flashdata("message", "La nota crédito fue creada exitosamente.");
            if ($credit_note_data->pos) {
                admin_redirect("pos/view/$credit_note_created");
            }
        } else {
            $this->session->set_flashdata("error", "No fue posible crear la nota crédito.");
        }

        if ($resolution_data->factura_electronica == YES) {
            admin_redirect("sales/fe_index");
        } else {
            admin_redirect("sales/index");
        }
    }

    private function build_credit_note_data()
    {
        $document_type_id = $this->input->post("document_type");
        $concept_value = $this->input->post("concept_value");
        $customerId = $this->input->post("customer_id");
        $billerId = $this->input->post("branch_id");
        $biller_cost_center = $this->site->getBillerCostCenter($billerId);
        $cost_center_id = NULL;
        if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
            $cost_center_id = $biller_cost_center->id;
        } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
            $cost_center_id = $this->input->post('cost_center_id');
        }

        if ($this->input->post("externalInvoice") == 'on' || $this->input->post("documentWithoutReference") == 'on') {
            $customerData = $this->site->getCompanyByID($customerId);
            $customerName = (!empty($customerData->name) && $customerData->name != '-') ? $customerData->name : $customerData->company;
            $billerId = $this->input->post("biller");
            $billerData = $this->site->getCompanyByID($billerId);
            $billerName = (!empty($billerData->company) && $billerData->company != '-') ? $billerData->company : $billerData->name;
            $sale_currency = $this->Settings->default_currency;
        } else {
            $customerName = $this->input->post("customer_name");
            $billerName = $this->input->post("branch_office_name");
            $sale_currency = $this->input->post("sale_currency");
        }

        $credit_note_data = [
            "date" => $this->input->post("date") ." ". date("H:i:s"),
            "customer_id" => $customerId,
            "customer" => $customerName,
            "biller_id" => $billerId,
            "biller" => $billerName,
            "warehouse_id"=>$this->input->post("warehouse_id"),
            "note" => $this->input->post("note"),
            "staff_note" => $this->input->post("staff_note"),
            "total" => ($concept_value * -1),
            "product_tax" => $this->input->post("concept_tax") * -1,
            "total_tax" => $this->input->post("concept_tax") * -1,
            "grand_total" => $this->input->post("total_value_concept") * -1,
            "sale_status" => "returned",
            "payment_status" => "paid",
            "paid" => $this->input->post("total_value_concept") * -1,
            "created_by" => $this->session->userdata("user_id"),
            "total_items" => 1,
            "return_sale_ref" => $this->input->post("invoice_reference"),
            "sale_id" => $this->input->post("reference_no"),
            "address_id" => $this->input->post("addresses_id"),
            "seller_id" => $this->input->post("seller_id"),
            "hash" => hash('sha256', microtime() . mt_rand()),
            "document_type_id" => $document_type_id,
            "payment_method_fe" => 2,
            "payment_mean_fe" => "ZZZ",
            "sale_currency" => $sale_currency,
            "fe_debit_credit_note_concept_dian_code" => $this->input->post("concept_dian_code"),
            "document_without_reference" => ($this->input->post("documentWithoutReference") == "on") ? 1 : 0,
            "document_without_reference_date" => ($this->input->post("documentWithoutReference") == "on") ? $this->input->post("document_without_reference_date") : NULL,
            "cost_center_id" => $cost_center_id,
        ];

        $credit_note_data = $this->calculateSelftRetention($credit_note_data);

        if ($this->input->post("externalInvoice") == 'on' && $this->Settings->fe_technology_provider == BPM) {
            $credit_note_data['external_invoice_reference'] = $this->input->post('external_invoice_reference');
            $credit_note_data['external_invoice_cufe'] = $this->input->post('external_invoice_cufe');
            $credit_note_data['external_invoice_date'] = $this->input->post('external_invoice_date');
        }

        $module_to_column_pos = 0; // cuando se crea una devolucion por otros conceptos y no se envía la referencia de la factura el modulo pos llega a este punto como '', la funcion de esta variable es asignar siempre un valor al modulo y asi tengamos un valor valido en la columna pos de la tabla sales
        if ($this->input->post('module') == '') {
            $module_to_column_pos = $this->returns_model->get_moduleById($this->input->post('document_type'))->module;
        }else if ($this->input->post('module') !== '') {
            $module_to_column_pos = $this->input->post('module');
        }

        if ($module_to_column_pos == FACTURA_DETAL || $module_to_column_pos == NOTA_CREDITO_DETAL || $module_to_column_pos == NOTA_DEBITO_DETAL) {
            $credit_note_data["pos"] = NOT;
        }else if ($module_to_column_pos == FACTURA_POS || $module_to_column_pos == NOTA_CREDITO_POS || $module_to_column_pos == NOTA_DEBITO_POS) {
            $credit_note_data["pos"] = YES;
        }

        if ($this->input->post('rete_applied') == 1) {
            $sale_id = $this->input->post("reference_no");
            $inv = $this->returns_model->get_sale($sale_id);
            $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
            $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
            $rete_fuente_account = $this->input->post('rete_fuente_account');
            $rete_fuente_base = $this->input->post('rete_fuente_base');

            $rete_iva_percentage = $this->input->post('rete_iva_tax');
            $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
            $rete_iva_account = $this->input->post('rete_iva_account');
            $rete_iva_base = $this->input->post('rete_iva_base');

            $rete_ica_percentage = $this->input->post('rete_ica_tax');
            $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
            $rete_ica_account = $this->input->post('rete_ica_account');
            $rete_ica_base = $this->input->post('rete_ica_base');

            $rete_other_percentage = $this->input->post('rete_otros_tax');
            $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
            $rete_other_account = $this->input->post('rete_otros_account');
            $rete_other_base = $this->input->post('rete_otros_base');

            $credit_note_data['rete_fuente_percentage'] = $rete_fuente_percentage;
            $credit_note_data['rete_fuente_total'] = $rete_fuente_total;
            $credit_note_data['rete_fuente_account'] = $rete_fuente_account;
            $credit_note_data['rete_fuente_base'] = $rete_fuente_base;
            $credit_note_data['rete_iva_percentage'] = $rete_iva_percentage;
            $credit_note_data['rete_iva_total'] = $rete_iva_total;
            $credit_note_data['rete_iva_account'] = $rete_iva_account;
            $credit_note_data['rete_iva_base'] = $rete_iva_base;
            $credit_note_data['rete_ica_percentage'] = $rete_ica_percentage;
            $credit_note_data['rete_ica_total'] = $rete_ica_total;
            $credit_note_data['rete_ica_account'] = $rete_ica_account;
            $credit_note_data['rete_ica_base'] = $rete_ica_base;
            $credit_note_data['rete_other_percentage'] = $rete_other_percentage;
            $credit_note_data['rete_other_total'] = $rete_other_total;
            $credit_note_data['rete_other_account'] = $rete_other_account;
            $credit_note_data['rete_other_base'] = $rete_other_base;

            if ($this->input->post("externalInvoice") == 'on' || $this->input->post("documentWithoutReference") == 'on') {
                $credit_note_data['rete_fuente_id'] = $this->input->post("rete_fuente_id");
                $credit_note_data['rete_iva_id'] =$this->input->post("rete_iva_id");
                $credit_note_data['rete_ica_id'] = $this->input->post("rete_ica_id");
                $credit_note_data['rete_other_id'] = $this->input->post("rete_otros_id");
            } else {
                $credit_note_data['rete_fuente_id'] = $inv->rete_fuente_id;
                $credit_note_data['rete_iva_id'] = $inv->rete_iva_id;
                $credit_note_data['rete_ica_id'] = $inv->rete_ica_id;
                $credit_note_data['rete_other_id'] = $inv->rete_other_id;
            }
        }

        return $credit_note_data;
    }

    private function calculateSelftRetention($data)
    {
        $selfRetentionAmount = 0;

        if (!empty($this->Settings->self_withholding_percentage)) {
            $concept = $this->returns_model->getCreditNoteConceptbyId($this->input->post("concept_id"));
            if (!empty($concept) && $concept->applied_selfretention == YES) {
                $selfRetentionAmount = (abs($data['total']) * $this->Settings->self_withholding_percentage) / 100;
            }
        }

        $data["self_withholding_amount"] = -abs($selfRetentionAmount);
        $data["self_withholding_percentage"] = $this->Settings->self_withholding_percentage;

        return $data;
    }

    private function build_credit_note_item_data()
    {
        $credit_note_item_data = [
            "product_id" => 999999999,
            "product_code" => $this->input->post("concept_id"),
            "product_name" => $this->input->post("concept_name"),
            "product_type" => "standar",
            "net_unit_price" => $this->sma->formatDecimal($this->input->post("concept_value")),
            "unit_price" => $this->sma->formatDecimal($this->input->post("concept_value") + $this->input->post("concept_tax")),
            "quantity" => $this->sma->formatDecimal(1) * -1,
            "item_tax" => $this->sma->formatDecimal($this->input->post("concept_tax")) * -1,
            "tax_rate_id" => $this->input->post("concept_tax_rate_id"),
            "tax" => $this->sma->formatDecimal($this->input->post("concept_tax_rate"), 0). "%",
            "subtotal" => ($this->sma->formatDecimal(($this->input->post("concept_value") * 1) + $this->input->post("concept_tax"))) * -1,
            "real_unit_price" => $this->sma->formatDecimal($this->input->post("concept_value") + $this->input->post("concept_tax")),
            "unit_quantity" => $this->sma->formatDecimal(1) * -1
        ];

        return $credit_note_item_data;
    }

    private function build_payment_data()
    {
        $payment = [];
        $deposit_document_type_id = $this->input->post("deposit_document_type_id");

        if (!($this->input->post("externalInvoice") == 'on' || $this->input->post("documentWithoutReference") == 'on')) {
            $sale_id = $this->input->post("reference_no");
            $inv = $this->returns_model->get_sale($sale_id);
        }

        $sale_return_balance = 0;
        $date = date('Y-m-d H:i:s');
        $total = $this->input->post("concept_value") * -1;
        $grand_total = -abs($this->input->post("total_value_concept"));

        $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
        $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
        $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
        $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
        $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;

        if ($total_retenciones > 0) {
            $retencion = array(
                'date'         => $date,
                'amount'       => ($total_retenciones*-1),
                'reference_no' => 'retencion',
                'paid_by'      => 'retencion',
                'cheque_no'    => '',
                'cc_no'        => '',
                'cc_holder'    => '',
                'cc_month'     => '',
                'cc_year'      => '',
                'cc_type'      => '',
                'created_by'   => $this->session->userdata('user_id'),
                'type'         => 'received',
                'note'         => 'Retenciones',
            );
            $payment[] = $retencion;
        }

        if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
            $base_commision = 0;
            $perc_commision = 0;
            $amount_commision = 0;
            $pay_ref = $this->input->post('payment_reference_no') ? $this->input->post('payment_reference_no') : $this->site->getReference('pay');

            if (isset($inv)) {
                $address_data = $this->site->getAddressByID($inv->address_id);
                if ($address_data) {
                    $base_commision = (($this->input->post('amount-paid') / $grand_total) * $total) * -1;
                    $perc_commision = $address_data->seller_collection_comision / 100;
                    $amount_commision = ($base_commision * $perc_commision);
                }
            }

            $data_payment = array(
                'date' => $date,
                'reference_no' => $pay_ref,
                'amount' => (0-$this->input->post('amount-paid')),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'returned',
                'comm_base' => $base_commision,
                'comm_amount' => $amount_commision,
                'comm_perc' => ($perc_commision * 100),
                'seller_id' => $this->input->post("seller_id"),
                'mean_payment_code_fe' =>$this->input->post('mean_payment_code_fe')
            );
            $sale_return_balance = abs($grand_total) - $this->input->post('amount-paid') - $total_retenciones;
            if ($this->input->post('paid_by') == 'deposit') {
                $data_payment['deposit_document_type_id'] = $deposit_document_type_id;
            }

            $payment[] = $data_payment;
        } else {
            $pay_ref = $this->site->getReference('pay');
            $data_payment = array(
                'date' => $date,
                'reference_no' => $pay_ref,
                'amount' => $grand_total + $total_retenciones,
                'paid_by' => 'due',
                'cheque_no' => NULL,
                'cc_no' => NULL,
                'cc_holder' => NULL,
                'cc_month' => NULL,
                'cc_year' => NULL,
                'cc_type' => NULL,
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'returned',
                'mean_payment_code_fe' => "ZZZ"
            );
            $data['payment_status'] = 'paid';
            $payment[] = $data_payment;
        }

        if ($sale_return_balance > 0) {
            $pay_ref = $this->site->getReference('pay');
            $data_payment = array(
                'date' => $date,
                'reference_no' => $pay_ref,
                'amount' => ($sale_return_balance * -1),
                'paid_by' => 'due',
                'cheque_no' => NULL,
                'cc_no' => NULL,
                'cc_holder' => NULL,
                'cc_month' => NULL,
                'cc_year' => NULL,
                'cc_type' => NULL,
                'created_by' => $this->session->userdata('user_id'),
                'type' => 'returned',
            );
            $data['payment_status'] = 'paid';
            $payment[] = $data_payment;
        }

        return $payment;
    }

    private function create_document_electronic($invoice_data)
    {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            return true;
        }
        $invoice_data = (object) $invoice_data;
        $filename = $this->site->getFilename($invoice_data->sale_id);
        $technologyProvider = $this->Settings->fe_technology_provider;
        $created_file = $this->Electronic_billing_model->send_document_electronic($invoice_data);

        if ($created_file->response == 0) {
            $this->session->set_flashdata('error', $created_file->message);
        } else {
            $email_delivery_response = '';

            if ($technologyProvider == CADENA || $technologyProvider == BPM || $technologyProvider == SIMBA) {
                if ($technologyProvider == SIMBA) {
                    $sale = $this->site->getSaleByID($invoice_data->sale_id);
                    $biller_data = $this->companies_model->getBillerData(['biller_id' => $sale->biller_id]); // <- biller data para validar el consumidor por defecto en la sucursal
                    if ($biller_data->default_customer_id == $sale->customer_id) { // <- Sí, el cliente de la factura es el mismo cliente por defecto de la sucursal, entra aca y no envia correo
                        $this->session->set_flashdata('message', 'Documento electrónico creado correctamente');
                        return true;
                    }
                }
                $this->download_fe_xml($invoice_data->sale_id, TRUE);
                $this->download_fe_pdf($invoice_data->sale_id, TRUE);

                if (file_exists('files/electronic_billing/'.$filename.'.xml') && file_exists('files/electronic_billing/'.$filename.'.pdf')) {
                    $zip = new ZipArchive();
                    $zip->open('files/electronic_billing/' . $filename . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
                    $zip->addFile("files/electronic_billing/" . $filename . '.xml', $filename . ".xml");
                    $zip->addFile("files/electronic_billing/" . $filename . '.pdf', $filename . ".pdf");

                    if ($invoice_data->attachment != '') {
                        $zip->addFile("files/" . $invoice_data->attachment, $invoice_data->attachment);
                    }

                    $zip->close();

                    unlink('files/electronic_billing/' . $filename . '.xml');
                    unlink('files/electronic_billing/' . $filename . '.pdf');

                    if ($_SERVER['SERVER_NAME'] != 'localhost') {
                        $email_delivery_response = $this->Electronic_billing_model->receipt_delivery($invoice_data, $filename);
                    }
                }

                $this->session->set_flashdata('message', 'Documento electrónico creado correctamente <br>'. $email_delivery_response);
            }
        }
    }

    public function download_fe_xml($sale_id, $internal_download = FALSE)
    {
        $sale = $this->site->getSaleByID($sale_id);
        $filename = $this->site->getFilename($sale_id);
        $technologyProvider = $this->Settings->fe_technology_provider;

        if ($technologyProvider == CADENA || $technologyProvider == BPM) {
            $xml_file = base64_decode($sale->fe_xml);

            if ($internal_download === TRUE) {
                $xml = new DOMDocument("1.0", "ISO-8859-15");
                $xml->loadXML($xml_file);
                $path = 'files/electronic_billing';
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }
                $xml->save('files/electronic_billing/'.$filename.'.xml');
            } else {
                header('Content-Type: application/xml;');
                header('Content-Disposition: attachment; filename="'.$filename.'.xml"');
                $xml = new DOMDocument("1.0", "ISO-8859-15");
                $xml->loadXML($xml_file);
                echo $xml->saveXML();
            }
        } elseif ($technologyProvider == SIMBA) {
            if (!empty($sale->fe_xml)){
                $fe_xml = $sale->fe_xml;
            }else{
                $typeDocument = $this->site->getTypeElectronicDocument($sale_id);
                // exit(var_dump($typeDocument));
                if ($typeDocument == INVOICE) {
                    sleep(10);
                }
                if ($typeDocument == CREDIT_NOTE) {
                    sleep(15);
                }
                $fe_xml = $this->Electronic_billing_model->getXMLSimba($sale_id);
            }
            if ($fe_xml) {
                $xml_file = base64_decode($fe_xml);
                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);
                    $path = 'files/electronic_billing';
                    if (!file_exists($path)) {
                        mkdir($path, 0777);
                    }
                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                }
            }
        }
    }

    public function download_fe_pdf($sale_id, $internal_download = FALSE)
    {
        $technologyProvider = $this->Settings->fe_technology_provider;
        if ($technologyProvider == CADENA || $technologyProvider == BPM || $technologyProvider == SIMBA) {
            $this->saleView($sale_id, $internal_download);
        }
    }

    public function add_past_years_sale_return()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        if ($this->form_validation->run() == true) {
            $date = ($this->Owner || $this->Admin) ? $this->sma->fld(trim($this->input->post('date'))) : date('Y-m-d H:i:s');
            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('re');
            $warehouse_id = $this->input->post('warehouse');
            $sale_reference_no = $this->input->post('sale_reference_no');
            $sale_pos = $this->input->post('pos');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $year_database = $this->input->post('year_database');
            $document_type_id = $this->input->post('document_type_id');
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->company) && $customer_details->company != '-' ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $sale_item_id = !empty($_POST['sale_item_id'][$r]) ? $_POST['sale_item_id'][$r] : NULL;
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_price = $this->sma->formatDecimal($_POST['real_unit_price'][$r]);
                $unit_price = $this->sma->formatDecimal($_POST['unit_price'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];

                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->site->getProductByCode($item_code) : null;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_price);
                    $unit_price = $this->sma->formatDecimal($unit_price - $pr_discount);
                    $item_net_price = $unit_price;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_price = $unit_price - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);

                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_tax),
                        'quantity' => ($item_quantity * -1),
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => ($item_unit_quantity * -1),
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => ($pr_item_tax * -1),
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal * -1),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'sale_item_id' => $sale_item_id,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax - $order_discount), 4);
            $data = array('date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => ($total * -1),
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => ($product_tax * -1),
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => ($order_tax * -1),
                'total_tax' => ($total_tax * -1),
                'grand_total' => ($grand_total * -1),
                'total_items' => $total_items,
                'sale_status' => 'returned',
                'payment_status' => 'paid',
                'paid' => ($grand_total * -1),
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'year_database' => $year_database,
                'document_type_id' => $document_type_id,
                'return_sale_ref' => $sale_reference_no,
                'pos' => $sale_pos,
                'pos' => $sale_pos,
                'sale_currency' => $this->Settings->default_currency,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            $sale_payment_method = NULL;
            $j = isset($_POST['amount-paid']) ? count($_POST['amount-paid']) : 0;
            $total_paid = 0;
            if ($j > 0) {
                $pay_ref = $this->input->post('payment_reference_no');
                for ($i = 0; $i < $j; $i++) {
                    if ($_POST['amount-paid'][$i] > 0) {
                        $base_commision = 0;
                        $perc_commision = 0;
                        $amount_commision = 0;
                        $total_paid+=$_POST['amount-paid'][$i];
                        $data_payment = array(
                            'date' => $date,
                            'document_type_id' => $pay_ref,
                            'amount' => -abs($_POST['amount-paid'][$i]),
                            'paid_by' => $_POST['paid_by'][$i],
                            'cheque_no' => $_POST['cheque_no'][$i],
                            'cc_no' => $_POST['pcc_no'][$i],
                            'cc_holder' => $_POST['pcc_holder'][$i],
                            'cc_month' => $_POST['pcc_month'][$i],
                            'cc_year' => $_POST['pcc_year'][$i],
                            'cc_type' => $_POST['pcc_type'][$i],
                            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                            'type' => 'returned',
                            'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i]
                        );

                        if ($_POST['paid_by'][$i] == 'deposit') {
                            if (!isset($_POST['deposit_document_type_id'][$i]) || empty($_POST['deposit_document_type_id'][$i]) || !$_POST['deposit_document_type_id'][$i]) {
                                $this->session->set_flashdata('error', lang("deposit_document_type_id"));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                            $data_payment['deposit_document_type_id'] = $_POST['deposit_document_type_id'][$i];
                        }
                        $payment[] = $data_payment;
                        $sale_payment_method = $_POST['paid_by'][$i];
                    }
                }
            }

            $total_retenciones = 0;
            if ($this->input->post('rete_applied') == 1) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->sma->formatDecimal($this->input->post('rete_bomberil_valor'));
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->sma->formatDecimal($this->input->post('rete_autoaviso_valor'));
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total + $rete_bomberil_total + $rete_autoaviso_total;
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_fuente_id'] = $this->input->post('rete_fuente_id');
                $data['rete_iva_id'] = $this->input->post('rete_iva_id');
                $data['rete_ica_id'] = $this->input->post('rete_ica_id');
                $data['rete_other_id'] = $this->input->post('rete_other_id');
                $data['rete_bomberil_id'] = $this->input->post('rete_bomberil_id');
                $data['rete_autoaviso_id'] = $this->input->post('rete_autoaviso_id');
                $retencion = array(
                    'date'         => $date,
                    'amount'       => ($total_retenciones * -1),
                    'reference_no' => 'retencion',
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'received',
                    'note'         => 'Retenciones',
                );
                $payment[] = $retencion;
            }


            if ($total_paid < $grand_total) {
                $data_payment = array(
                    'date' => $date,
                    'document_type_id' => $pay_ref,
                    'amount' => $total_paid - $grand_total + $total_retenciones,
                    'paid_by' => 'due',
                    'cheque_no' => NULL,
                    'cc_no' => NULL,
                    'cc_holder' => NULL,
                    'cc_month' => NULL,
                    'cc_year' => NULL,
                    'cc_type' => NULL,
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'type' => 'returned',
                );
                $payment[] = $data_payment;
            }
            if ($j > 1) {
                $sale_payment_method = 'mixed';
            }
            // $this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true && $creditNoteCreated = $this->returns_model->add_return_past_year($data, $products, $payment)) {
            $resolution_data = $this->site->getDocumentTypeById($this->input->post('document_type_id'));
            if ($resolution_data->factura_electronica == YES) {
                $creditNote = $this->site->getSaleByID($creditNoteCreated);
                $electronicDocument = [
                    'sale_id' => $creditNoteCreated,
                    'biller_id' => $creditNote->biller_id,
                    'customer_id' => $creditNote->customer_id,
                    'reference' => $creditNote->reference_no
                ];

                if ($resolution_data->factura_contingencia == NOT) {
                    $this->create_document_electronic($electronicDocument);
                }
            }

            $this->session->set_userdata('remove_rels', 1);
            $this->session->set_flashdata('message', lang("return_added"));
            if ($sale_pos) {
                if ($resolution_data->factura_electronica == YES) {
                    admin_redirect("pos/fe_index");
                }
                admin_redirect("pos/sales");
            } else {
                if ($resolution_data->factura_electronica == YES) {
                    admin_redirect("sales/fe_index");
                }
                admin_redirect("sales");
            }
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['last_years_databases'] = $this->site->get_last_years_databases();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('returns'), 'page' => lang('returns')), array('link' => '#', 'page' => lang('add_past_years_sale_return')));
            $meta = array('page_title' => lang('add_past_years_sale_return'), 'bc' => $bc);
            $this->page_construct('returns/add_past_years_sale_return', $meta, $this->data);
        }
    }

    public function past_year_sale_suggestion()
    {
        $term = $this->input->get('term');
        $database = $this->input->get('database');
        $sale = $this->site->get_past_year_sale($term, $database);

        if ($sale) {
            $items = $this->site->get_past_year_invoice_items($sale->id, $database);
            $payments = $this->site->get_past_year_invoice_payments($sale->id, $database);
            $pmnt = [];
            if ($payments) {
                foreach ($payments as $pt) {
                    $pmnt[$pt->paid_by] = $pt->amount;
                }
            }
                $total_quantity_available = 0;
            if ($items) {
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if ($this->Settings->gift_card_product_id == $item->product_id){
                        $si_gift_card = $this->site->get_giftcard_by_saleitem($item->id);
                        if ($this->sales_model->get_gift_card_movements($si_gift_card->id)) {
                            $invalid_gc_return = true;
                            continue;
                        }
                    }
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->sale_item_id = $item->sale_item_id;
                    $row->sale_item_id = $item->id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->returned_quantity = $item->returned_quantity;
                    $row->qty = $item->quantity - $item->returned_quantity;
                    $row->base_quantity = $item->quantity - $item->returned_quantity;
                    $row->base_unit = isset($row->unit) && $row->unit ? $row->unit : $item->product_unit_id;
                    if ($item->product_unit_id_selected) {
                        $row->product_unit_id_selected = $item->product_unit_id_selected;
                    }
                    $row->unit = $item->product_unit_id;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    $row->unit_price = $row->tax_method ? $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_price + ($item->item_discount / $item->quantity);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->base_unit_price = $row->unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = $item->serial_no;
                    $row->option = $item->option_id;
                    $options = $this->sales_model->getProductOptions($row->id, $sale->warehouse_id, true, $item->option_id);
                    $combo_items = false;
                    $row->price_before_tax = $item->price_before_tax;
                    $row->consumption_sale_tax = $item->tax_rate_2_id;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->item_tax = $item->item_tax / $item->quantity;
                    $row->net_unit_price = $item->net_unit_price;
                    $row->item_discount = $item->item_discount / $item->quantity;
                    $row->oqty = $item->quantity - $item->returned_quantity;
                    $total_quantity_available += $row->oqty;
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->get_product_units($row->id);
                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    if ($row->qty == 0) {
                        continue;
                    }
                    $pr[$ri] = array('id' => $ri, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'order' => $item->id);
                    $c++;
                }
            }
            if ($total_quantity_available <= 0) {
                $this->sma->send_json(array('response' => 0, 'msg' => lang('sale_totally_returned')));
            }
            $this->sma->send_json(['response' => 1, 'sale'=>$sale, 'sale_items'=>$pr,'payments'=>$pmnt]);
        } else {
            $this->sma->send_json(array('response' => 0, 'msg' => lang('sale_not_found')));
        }
    }

    public function add_past_years_purchase_return()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('supplier', lang("supplier"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        if ($this->form_validation->run() == true) {
            $date = ($this->Owner || $this->Admin) ? $this->sma->fld(trim($this->input->post('date'))) : date('Y-m-d H:i:s');
            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('re');
            $warehouse_id = $this->input->post('warehouse');
            $purchase_reference_no = $this->input->post('purchase_reference_no');
            $supplier_id = $this->input->post('supplier');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $year_database = $this->input->post('year_database');
            $document_type_id = $this->input->post('document_type_id');
            $supplier_details = $this->site->getCompanyByID($supplier_id);
            $supplier = !empty($supplier_details->company) && $supplier_details->company != '-' ? $supplier_details->company : $supplier_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $purchase_item_id = !empty($_POST['purchase_item_id'][$r]) ? $_POST['purchase_item_id'][$r] : NULL;
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $real_unit_cost = $this->sma->formatDecimal($_POST['real_unit_cost'][$r]);
                $unit_cost = $this->sma->formatDecimal($_POST['unit_cost'][$r]);
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                // exit(var_dump($item_unit_quantity));
                if (isset($item_code) && isset($real_unit_cost) && isset($unit_cost) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->site->getProductByCode($item_code) : null;
                    $pr_discount = $this->site->calculateDiscount($item_discount, $unit_cost);
                    $unit_cost = $this->sma->formatDecimal($unit_cost - $pr_discount);
                    $item_net_cost = $unit_cost;
                    $pr_item_discount = $this->sma->formatDecimal($pr_discount * $item_unit_quantity);
                    $product_discount += $pr_item_discount;
                    $pr_item_tax = $item_tax = 0;
                    $tax = "";

                    if (isset($item_tax_rate) && $item_tax_rate != 0) {

                        $tax_details = $this->site->getTaxRateByID($item_tax_rate);
                        $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_cost);
                        $item_tax = $ctax['amount'];
                        $tax = $ctax['tax'];
                        if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                            $item_net_cost = $unit_cost - $item_tax;
                        }
                        $pr_item_tax = $this->sma->formatDecimal(($item_tax * $item_unit_quantity), 4);
                        if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $supplier_details->state), $tax_details)) {
                            $total_cgst += $gst_data['cgst'];
                            $total_sgst += $gst_data['sgst'];
                            $total_igst += $gst_data['igst'];
                        }
                    }

                    $product_tax += $pr_item_tax;
                    $subtotal = (($item_net_cost * $item_unit_quantity) + $pr_item_tax);
                    $unit = $this->site->getUnitByID($item_unit);

                    $product = array(
                        'product_id' => $item_id,
                        'status' => 'received',
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        // 'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_cost' => $item_net_cost,
                        'unit_cost' => $this->sma->formatDecimal($item_net_cost + $item_tax),
                        'quantity' => ($item_quantity * -1),
                        'product_unit_id' => $unit ? $unit->id : null,
                        'product_unit_code' => $unit ? $unit->code : null,
                        'unit_quantity' => ($item_unit_quantity * -1),
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => ($pr_item_tax * -1),
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $tax,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal * -1),
                        'serial_no' => $item_serial,
                        'real_unit_cost' => $real_unit_cost,
                        'purchase_item_id' => $purchase_item_id,
                    );

                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_cost * $item_unit_quantity), 4);
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount), 4);
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax), 4);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax - $order_discount), 4);
            $data = array('date' => $date,
                'reference_no' => $reference,
                'supplier_id' => $supplier_id,
                'supplier' => $supplier,
                'biller_id' => $biller_id,
                // 'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                // 'staff_note' => $staff_note,
                'total' => ($total * -1),
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => ($product_tax * -1),
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => ($order_tax * -1),
                'total_tax' => ($total_tax * -1),
                'grand_total' => ($grand_total * -1),
                'paid' => ($grand_total * -1),
                // 'total_items' => $total_items,
                'status' => 'returned',
                'payment_status' => 'paid',
                'paid' => 0,
                'created_by' => $this->session->userdata('user_id'),
                // 'hash' => hash('sha256', microtime() . mt_rand()),
                'year_database' => $year_database,
                'document_type_id' => $document_type_id,
                'return_purchase_ref' => $purchase_reference_no,
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_id');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_id');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_id');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_id');

                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->sma->formatDecimal($this->input->post('rete_bomberil_valor'));
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_bomberil_id = $this->input->post('rete_bomberil_id');

                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->sma->formatDecimal($this->input->post('rete_autoaviso_valor'));
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $rete_autoaviso_id = $this->input->post('rete_autoaviso_id');

                $rete_fuente_assumed = $this->input->post('rete_fuente_assumed') ? 1 : 0;
                $rete_iva_assumed = $this->input->post('rete_iva_assumed') ? 1 : 0;
                $rete_ica_assumed = $this->input->post('rete_ica_assumed') ? 1 : 0;
                $rete_other_assumed = $this->input->post('rete_otros_assumed') ? 1 : 0;

                $rete_fuente_assumed_account = $this->input->post('rete_fuente_assumed_account');
                $rete_iva_assumed_account = $this->input->post('rete_iva_assumed_account');
                $rete_ica_assumed_account = $this->input->post('rete_ica_assumed_account');
                $rete_bomberil_assumed_account = $this->input->post('rete_bomberil_assumed_account');
                $rete_autoaviso_assumed_account = $this->input->post('rete_autoaviso_assumed_account');
                $rete_other_assumed_account = $this->input->post('rete_otros_assumed_account');

                $total_retenciones =
                    ($rete_fuente_assumed ? 0 : $rete_fuente_total) +
                    ($rete_iva_assumed ? 0 : $rete_iva_total) +
                    ($rete_ica_assumed ? 0 : $rete_ica_total) +
                    ($rete_ica_assumed ? 0 : $rete_bomberil_total) +
                    ($rete_ica_assumed ? 0 : $rete_autoaviso_total) +
                    ($rete_other_assumed ? 0 : $rete_other_total);

                if ($rete_fuente_assumed) {
                    $assumed_retentions_text .= "Rete Fuente asumida (" . $this->sma->formatMoney($rete_fuente_total) . "), ";
                }
                if ($rete_iva_assumed) {
                    $assumed_retentions_text .= "Rete IVA asumida(" . $this->sma->formatMoney($rete_iva_total) . "), ";
                }
                if ($rete_ica_assumed) {
                    $assumed_retentions_text .= "Rete ICA asumida(" . $this->sma->formatMoney($rete_ica_total) . "), ";
                    if ($rete_bomberil_total > 0) {
                        $assumed_retentions_text .= "Tasa Bomberil asumida(" . $this->sma->formatMoney($rete_bomberil_total) . "), ";
                    }
                    if ($rete_autoaviso_total > 0) {
                        $assumed_retentions_text .= "Tasa Auto Avisos y Tableros asumida(" . $this->sma->formatMoney($rete_autoaviso_total) . "), ";
                    }
                }
                if ($rete_other_assumed) {
                    $assumed_retentions_text .= "Rete OTRAS asumida(" . $this->sma->formatMoney($rete_other_total) . "), ";
                }
                // $assumed_retentions_text = trim($assumed_retentions_text, ", ");
                $rete_applied = TRUE;

                // exit('Total Fuente : '.$rete_fuente_total."\n Total IVA : ".$rete_iva_total."\n Total ICA : ".$rete_ica_total."\n Total Otros : ".$rete_other_total."\n Total Retención : ".$total_retenciones);
            } else {
                $rete_applied = FALSE;
                $total_retenciones = 0;
            }

            $purchase_payment_method = NULL;
            $j = isset($_POST['amount-paid']) ? count($_POST['amount-paid']) : 0;
            $total_paid = 0;
            if ($j > 0) {
                $pay_ref = $this->input->post('payment_reference_no');
                for ($i = 0; $i < $j; $i++) {
                    if ($_POST['amount-paid'][$i] > 0) {
                        $base_commision = 0;
                        $perc_commision = 0;
                        $amount_commision = 0;
                        $total_paid+=$_POST['amount-paid'][$i];
                        $data_payment = array(
                            'date' => $date,
                            'document_type_id' => $pay_ref,
                            'amount' => -abs($_POST['amount-paid'][$i]),
                            'paid_by' => $_POST['paid_by'][$i],
                            'cheque_no' => $_POST['cheque_no'][$i],
                            'cc_no' => $_POST['pcc_no'][$i],
                            'cc_holder' => $_POST['pcc_holder'][$i],
                            'cc_month' => $_POST['pcc_month'][$i],
                            'cc_year' => $_POST['pcc_year'][$i],
                            'cc_type' => $_POST['pcc_type'][$i],
                            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                            'type' => 'returned',
                            'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i]
                        );
                        // $this->sma->print_arrays($_POST['deposit_document_type_id']);
                        if ($_POST['paid_by'][$i] == 'deposit') {
                            if (!isset($_POST['deposit_document_type_id'][$i]) || empty($_POST['deposit_document_type_id'][$i]) || !$_POST['deposit_document_type_id'][$i]) {
                                $this->session->set_flashdata('error', lang("deposit_document_type_id"));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                            $data_payment['deposit_document_type_id'] = $_POST['deposit_document_type_id'][$i];
                        }
                        $payment[] = $data_payment;
                        $purchase_payment_method = $_POST['paid_by'][$i];
                    }
                }
            }
            if ($total_paid < $grand_total) {
                $data_payment = array(
                    'date' => $date,
                    'document_type_id' => $pay_ref,
                    'amount' => $total_paid - $grand_total + $total_retenciones,
                    'paid_by' => 'due',
                    'cheque_no' => NULL,
                    'cc_no' => NULL,
                    'cc_holder' => NULL,
                    'cc_month' => NULL,
                    'cc_year' => NULL,
                    'cc_type' => NULL,
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'type' => 'returned',
                );
                $payment[] = $data_payment;
            }

            if ($rete_applied) {
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;

                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_bomberil_id'] = $rete_bomberil_id;

                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_autoaviso_id'] = $rete_autoaviso_id;

                $data['rete_fuente_assumed'] = $rete_fuente_assumed;
                $data['rete_iva_assumed'] = $rete_iva_assumed;
                $data['rete_ica_assumed'] = $rete_ica_assumed;
                $data['rete_other_assumed'] = $rete_other_assumed;
                $data['rete_fuente_assumed_account'] = $rete_fuente_assumed ? $rete_fuente_assumed_account : NULL;
                $data['rete_iva_assumed_account'] = $rete_iva_assumed ? $rete_iva_assumed_account : NULL;
                $data['rete_ica_assumed_account'] = $rete_ica_assumed ? $rete_ica_assumed_account : NULL;
                $data['rete_bomberil_assumed_account'] = $rete_ica_assumed ? $rete_bomberil_assumed_account : NULL;
                $data['rete_autoaviso_assumed_account'] = $rete_ica_assumed ? $rete_autoaviso_assumed_account : NULL;
                $data['rete_other_assumed_account'] = $rete_other_assumed ? $rete_other_assumed_account : NULL;

                $retpayment = array(
                    'date'         => $date,
                    // 'reference_no' => $this->site->getReference('ppay'),
                    'amount'       => ($total_retenciones * -1),
                    'reference_no' => 'retencion',
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'sent',
                    'note'         => 'Retenciones',
                );
                $payment[] = $retpayment;
            }
            if ($j > 1) {
                $purchase_payment_method = 'mixed';
            }

            // $this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true && $creditNoteCreated = $this->returns_model->add_return_purchase_past_year($data, $products, $payment)) {
            $this->session->set_userdata('remove_rels', 1);
            $this->session->set_flashdata('message', lang("return_added"));
            admin_redirect("purchases");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['last_years_databases'] = $this->site->get_last_years_databases();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('returns'), 'page' => lang('returns')), array('link' => '#', 'page' => lang('add_past_years_purchase_return')));
            $meta = array('page_title' => lang('add_past_years_purchase_return'), 'bc' => $bc);
            $this->page_construct('returns/add_past_years_purchase_return', $meta, $this->data);
        }
    }

    public function past_year_purchase_suggestion()
    {
        $term = $this->input->get('term');
        $database = $this->input->get('database');
        $purchase = $this->site->get_past_year_purchase($term, $database);
        if ($purchase) {
            $items = $this->site->get_past_year_purchase_items($purchase->id, $database);
            $payments = $this->site->get_past_year_purchase_payments($purchase->id, $database);
            $pmnt = [];
            if ($payments) {
                foreach ($payments as $pt) {
                    $pmnt[$pt->paid_by] = $pt->amount;
                }
            }
                $total_quantity_available = 0;
            if ($items) {
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1cost, $row->supplier2cost, $row->cfsupplier3cost, $row->supplier4cost, $row->supplier5cost, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->purchase_item_id = $item->purchase_item_id;
                    $row->purchase_item_id = $item->id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->returned_quantity = $item->returned_quantity;
                    $row->qty = $item->quantity - $item->returned_quantity;
                    $row->base_quantity = $item->quantity - $item->returned_quantity;
                    $row->base_unit = isset($row->unit) && $row->unit ? $row->unit : $item->product_unit_id;
                    if ($item->product_unit_id_selected) {
                        $row->product_unit_id_selected = $item->product_unit_id_selected;
                    }
                    $row->unit = $item->product_unit_id;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->cost = $this->sma->formatDecimal($item->net_unit_cost + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    $row->unit_cost = $row->tax_method ? $item->unit_cost + $this->sma->formatDecimal($item->item_discount / $item->quantity) + $this->sma->formatDecimal($item->item_tax / $item->quantity) : $item->unit_cost + ($item->item_discount / $item->quantity);
                    $row->real_unit_cost = $item->real_unit_cost;
                    $row->base_unit_cost = $row->unit_cost;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = $item->serial_no;
                    $row->option = $item->option_id;
                    $options = $this->purchases_model->getProductOptions($row->id, $item->option_id);
                    if (!$options && $item->option_id) {
                        $options = [];
                        $options[] = $this->site->get_past_year_option_by_id($item->option_id, $database);
                    }
                    $combo_items = false;
                    $row->consumption_purchase_tax = $item->tax_rate_2_id;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->item_tax = $item->item_tax / $item->quantity;
                    $row->net_unit_cost = $item->net_unit_cost;
                    $row->item_discount = $item->item_discount / $item->quantity;
                    $row->oqty = $item->quantity - $item->returned_quantity;
                    $total_quantity_available += $row->oqty;
                    if ($row->type == 'combo') {
                        $combo_items = $this->pos_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->get_product_units($row->id);
                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    if ($row->qty == 0) {
                        continue;
                    }
                    $pr[$ri] = array('id' => $ri, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'order' => $item->id);
                    $c++;
                }
            }
            if ($total_quantity_available <= 0) {
                $this->sma->send_json(array('response' => 0, 'msg' => lang('purchase_totally_returned')));
            }
            $this->sma->send_json(['response' => 1, 'purchase'=>$purchase, 'purchase_items'=>$pr,'payments'=>$pmnt]);
        } else {
            $this->sma->send_json(array('response' => 0, 'msg' => lang('purchase_not_found')));
        }
    }
}