<?php
defined('BASEPATH') OR exit('No direct script access allowed');


$lang['sellers'] = "Vendedores";
$lang['add_seller']	= "Agregar Vendedor";
$lang['edit_seller'] = "Editar Vendedor";
$lang['seller_list'] = "Lista Vendedores";
$lang['delete_seller'] 	= "Eliminar Vendedor";
$lang['enable_seller']  = "Habilitar Vendedor";
$lang['disable_seller'] = "Inhabilitar Vendedor";
$lang['delete_sellers'] = "Eliminar Vendedores";
$lang['first_name_seller'] = "Primer nombre";
$lang['second_name_seller'] = "Segundo nombre";
$lang['first_lastname_seller'] = "Primer apellido";
$lang['second_lastname_seller'] = "Segundo apellido";

$lang['validation_message_required'] = "El campo %s es obligatorio.";
$lang['validation_message_valid_email'] = "El correo electrónico no es vállido.";

$lang['disabled_seller'] = "Vendedor deshabilitado.";
$lang['enabled_seller'] = "Vendedor habilitado.";
$lang['added_seller'] = "Vendedor agregado correctamente.";
$lang['seller_removed'] = "Vendedor eliminado correctamente.";
$lang['updated_seller'] = "Vendedor actualizado correctamente.";
$lang['enabled_not_seller'] = "Vendedor no ha sido habilitado.";
$lang['sellers_removed'] = "Vendedores eliminados correctamente.";
$lang['disabled_not_seller'] = "Vendedor no ha sido deshabilitado.";
$lang['seller_not_removed'] = "No fue posible emilinar el vendedor.";
$lang['existing_seller_sales'] = "No fue posible emilinar el vendedor, debido a que existen ventas asociadas al mismo.";
$lang['no_seller_selected'] = "No se ha seleccionado ningún vendedor.";
$lang['existing_seller_contracts'] = "No fue posible emilinar el vendedor, debido a que existe un contrato laboral asociado al mismo";