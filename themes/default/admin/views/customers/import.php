<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('import_by_csv'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("customers/import_csv", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="well well-small">
                <!-- <span class="text-warning"><?= lang("csv1"); ?></span><br/><?= lang("csv2"); ?> <span class="text-info">(<?= lang("company") . ', ' . lang("name") . ', ' . lang("email") . ', ' . lang("phone") . ', ' . lang("address") . ', ' . lang("city"); ?>
                    ,  <?= lang("state") . ', ' . lang("postal_code") . ', ' . lang("country") . ', ' . lang("vat_no") . ', ' . lang("ccf1") . ', ' . lang("ccf2") . ', ' . lang("ccf3") . ', ' . lang("ccf4") . ', ' . lang("ccf5") . ', ' . lang("ccf6"); ?>
                    )</span> <?= lang("csv3"); ?><br>
                <span class="text-success"><?= lang('first_6_required'); ?></span> -->
                <!-- <a href="<?php echo base_url(); ?>assets/csv/sample.csv" class="btn btn-primary pull-right"><i
                        class="fa fa-download"></i> Download Sample File</a> -->
                <div class="row">
                    <div class="col-sm-12">
                        <p>La primera línea debe ser igual al del CSV descargado, no modificar el orden ni el nombre de estos campos.</p>
                        <p>En los campos para especificar el tipo de documento, el grupo de clientes y el tipo de persona, se deben llenar con los ids abajo relacionados a cada uno.</p>
                        <p>En caso de que el cliente tenga tipo de documento NIT, el dígito de verificación es obligatorio llenarlo.</p>
                        <a href="<?php echo base_url(); ?>assets/csv/sample.csv" class="btn btn-primary pull-right"><i
                        class="fa fa-download"></i> <?= lang('download_sample_file') ?></a>
                    </div>
                    <div class="col-sm-6">
                        <table class="table">
                            <tr>
                                <th colspan="2" class="text-center">Tipo de documentos</th>
                            </tr>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                            </tr>
                            <?php foreach ($documentTypes as $row => $dT): ?>
                                <tr>
                                    <td><?= $dT->id ?></td>
                                    <td><?= $dT->nombre ?></td>
                                </tr>
                            <?php endforeach ?>
                        </table>
                    </div>
                    <div class="col-sm-6">
                        <table class="table">
                            <tr>
                                <th colspan="2" class="text-center">Grupo de clientes</th>
                            </tr>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                            </tr>
                            <?php foreach ($customerGroups as $row => $cG): ?>
                                <tr>
                                    <td><?= $cG->id ?></td>
                                    <td><?= $cG->name ?></td>
                                </tr>
                            <?php endforeach ?>
                        </table>
                        <table class="table">
                            <tr>
                                <th colspan="2" class="text-center">Tipo de personas</th>
                            </tr>
                            <tr>
                                <th>Id</th>
                                <th>Nombre</th>
                            </tr>
                            <tr>
                                <td>0</td>
                                <td>Jurídica</td>
                            </tr>
                            <tr>
                                <td>1</td>
                                <td>Natural</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <?= lang("upload_file", "csv_file") ?>
                <input id="csv_file" type="file" data-browse-label="<?= lang('browse'); ?>" name="csv_file" data-bv-notempty="true" data-show-upload="false"
                       data-show-preview="false" class="form-control file">
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('import', lang('import'), 'class="btn btn-primary"'); ?>
        </div>
        <?php echo form_close(); ?>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>