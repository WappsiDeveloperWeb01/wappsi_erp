<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style>

    .panel-default{
        margin-bottom : 1.5px;
        border-radius : 5px;
    }

    .panel-heading {
        background-color: #1c84c6; 
        color: black; 
        padding: 10px; 
        border-radius: 5px; 
        cursor: pointer; 
    }

    .table {
        width: 100%; 
        margin-top: 10px; 
        border-collapse: separate;
        border-spacing: 0 2.5px; 
    }

    .table th, .table td {
        padding: 10px;
        border: 1px solid #dee2e6; 
        text-align: center; 
        background-color: #ffffff;
    }

    .table th {
        color: black; 
    }

    .table tfoot th {
        font-weight: bold; 
    }

    .styleT{
        text-align: center;
    }

    .styleH3{
        text-align: center;
        margin-bottom : 5px !important;
    }

    .d-flex {
        display: flex;
        justify-content: space-between;
        align-items: center;
    }

    .widget{
        border: 1px solid #C2C2C2;
        transition: all 0.4s ease;
    }

    .widget:hover {
        border: 1.5px solid #000; 
        box-shadow: 0 4px 8px rgba(0, 0, 0, 0.2); 
    }
</style>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?= admin_form_open('wappsi_invoicing/electronic_hits', 'id="FilterForm"') ?>
                <div class="ibox">
                    <div class="ibox-content" style="margin-top:4px; margin-bottom:4px;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="col-md-2 col-sm-6">
                                    <?= lang('year', 'year') ?>
                                    <select name="year" id="year" class="form-control">
                                        <?php foreach ($years as $key => $value) : ?>
                                            <?php 
                                                $selected = ''; 
                                                if ($_POST['year'] == $value) {
                                                    $selected = 'selected';
                                                } 
                                            ?>
                                            <option value="<?= $key ?>" <?= $selected ?> > <?= $value ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-md-2 col-sm-6 divMonth">
                                    <?= lang('month', 'month') ?>
                                    <select name="month" id="month" class="form-control">
                                        <?php foreach ($months as $key => $month) : ?>
                                            <?php 
                                                $selected = '';
                                                if (isset($_POST['month']) && $_POST['month'] == $key) {
                                                    $selected = 'selected';
                                                }
                                            ?>
                                            <option value="<?= $key ?>" <?= $selected ?> > <?= $month ?> </option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="col-sm-1 col-without-padding text-center">
                                    <div class="new-button-container">
                                        <button class="btn btn-primary new-button" id="consultar" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>

        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title" style="min-height: 0px; padding: 10px 20px 7px;">
                    <h3 class="chart_titles" style="margin-bottom: 5px;">Resumen consumos electrónicos</h3>
                </div>
                <div class="ibox-content" style="min-height: 0px; padding: 10px 20px 7px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-sm-3">
                                <div class="widget styleT">
                                    <h3 class="font-bold styleH3"> <?=  number_format($total_consumed, 0, ',', '.') ?> </h3>
                                    <span> <?= lang('total_consumed') ?> </span>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="widget styleT">
                                    <h3 class="font-bold styleH3">  <?=  number_format($avg_month, 0, ',', '.') ?> </h3>
                                    <span> <?= lang('avg_month') ?> </span>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="widget styleT">
                                    <h3 class="font-bold styleH3"> <?= number_format($total_acq, 0, ',', '.') ?> </h3>
                                    <span> <?= lang('total_acquired') ?> </span>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <div class="widget styleT">
                                    <h3 class="font-bold styleH3 "> <?= (!$total_dispo) ? '-' : $total_dispo  ?>  </h3>
                                    <span> <?= lang('total_available') ?> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-title" style="min-height: 0px; padding: 10px 20px 7px;">
                    <h3 class="chart_titles" style="margin-bottom: 5px;"> Consumos electrónicos</h3>
                </div>
                <div class="ibox-content" style="padding: 1px 10px 7px;">
                    <div class="row" style="display: contents;">
                        <div class="col-lg-12">
                            <div id="faq" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="questionOne">
                                        <h5 class="panel-title d-flex justify-content-between">
                                            <a data-toggle="collapse" data-parent="#faq" href="#answerOne" aria-expanded="true" aria-controls="answerOne">
                                                Facturas electrónicas
                                            </a>
                                            <span class="total" id="titleFe"></span>
                                        </h5>
                                    </div>
                                    <div id="answerOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                                        <div class="panel-body">
                                            <table class='table'>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" ><?= lang('document_type_sales_prefix') ?></th>
                                                        <th class="text-center" ><?= lang('document_name') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_inicio_resolucion_wappsi') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_sales_consecutive') ?></th>
                                                        <th class="text-center" ><?= lang('consumed') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                    $factura_total_cons = 0;
                                                    $factura_total_por_cons = 0;
                                                    if (!empty($documents_types)) {
                                                        foreach ($documents_types as $key => $value) {
                                                            /* 
                                                                '2' => 'Factura',
                                                                '4' => 'Devolución Factura',
                                                                '27' => 'Nota Débito',
                                                            */
                                                            if ($value->module == 2 || $value->module == 4 || $value->module == 27) {
                                                                $inicioResolucionWappsi= $value->inicio_resolucion_wappsi;
                                                                $consumed = $value->total;
                                                                $consecutivoActual = $value->sales_consecutive-1; // menos uno por que el consecutivo actual siempre va por el proximo 
                                                                $factura_total_por_cons += $consumed;
                                                    ?>
                                                            <tr>
                                                                <td class="text-center"><?= $value->sales_prefix; ?></td>
                                                                <td class="text-center"><?= $value->name; ?></td>
                                                                <td class="text-center"><?= $inicioResolucionWappsi; ?></td>
                                                                <td class="text-center"><?= $consecutivoActual; ?></td>
                                                                <td class="text-center"><?= $consumed; ?></td>
                                                            </tr>
                                                    <?php        
                                                            }
                                                        }
                                                    }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center" colspan="4" ></th>
                                                        <th class="text-center" id="totalFe" ><?= $factura_total_por_cons ?></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="questionTwo">
                                        <h5 class="panel-title d-flex justify-content-between">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#faq" href="#answerTwo" aria-expanded="false" aria-controls="answerTwo">
                                                Facturas pos electrónicas
                                            </a>
                                            <span class="total" id="titlePoFe"></span>
                                        </h5>
                                    </div>
                                    <div id="answerTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionTwo">
                                        <div class="panel-body">
                                            <table class='table'>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" ><?= lang('document_type_sales_prefix') ?></th>
                                                        <th class="text-center" ><?= lang('document_name') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_inicio_resolucion_wappsi') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_sales_consecutive') ?></th>
                                                        <th class="text-center" ><?= lang('consumed') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                    $factura_total_cons = 0;
                                                    $factura_total_por_cons = 0;
                                                    /*
                                                        '1' => 'Factura Pos',
                                                        '3' => 'Devolución Factura POS',
                                                        '26' => 'Nota Débito POS',
                                                    */
                                                    if (!empty($documents_types)) {
                                                        foreach ($documents_types as $key => $value) {
                                                            if ($value->module == 1 || $value->module == 3 || $value->module == 26) {
                                                                $inicioResolucionWappsi= $value->inicio_resolucion_wappsi;
                                                                $consecutivoActual = $value->sales_consecutive; 
                                                                $consumed = $value->total;
                                                                $factura_total_por_cons += $consumed;
                                                    ?>
                                                            <tr>
                                                                <td class="text-center"><?= $value->sales_prefix; ?></td>
                                                                <td class="text-center"><?= $value->name; ?></td>
                                                                <td class="text-center"><?= $inicioResolucionWappsi; ?></td>
                                                                <td class="text-center"><?= $consecutivoActual; ?></td>
                                                                <td class="text-center"><?= $consumed; ?></td>
                                                            </tr>
                                                    <?php        
                                                            }
                                                        } 
                                                    }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center" colspan="4"></th>
                                                        <th class="text-center" id="totalPoFe"><?= $factura_total_por_cons ?></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="questionThree">
                                        <h5 class="panel-title d-flex justify-content-between">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#faq" href="#answerThree"  aria-expanded="false" aria-controls="answerThree">
                                                Documento soporte
                                            </a>
                                            <span class="total" id="titleDocSup"></span>
                                        </h5>
                                    </div>
                                    <div id="answerThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionThree">
                                        <div class="panel-body">
                                            <table class='table'>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" ><?= lang('document_type_sales_prefix') ?></th>
                                                        <th class="text-center" ><?= lang('document_name') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_inicio_resolucion_wappsi') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_sales_consecutive') ?></th>
                                                        <th class="text-center" ><?= lang('consumed') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                    $factura_total_cons = 0;
                                                    $factura_total_por_cons = 0;
                                                    /*
                                                        '35' => 'Documento Soporte',
                                                        "52" => "Nota de ajuste documento de soporte",
                                                    */
                                                    if (!empty($documents_types)) {
                                                        foreach ($documents_types as $key => $value) {
                                                            if ($value->module == 35 || $value->module == 52 ) {
                                                                $inicioResolucionWappsi= $value->inicio_resolucion_wappsi;
                                                                $consecutivoActual = $value->sales_consecutive; 
                                                                $consumed = $value->total;
                                                                $factura_total_por_cons += $consumed;
                                                    ?>
                                                            <tr>
                                                                <td class="text-center"><?= $value->sales_prefix; ?></td>
                                                                <td class="text-center"><?= $value->name; ?></td>
                                                                <td class="text-center"><?= $inicioResolucionWappsi; ?></td>
                                                                <td class="text-center"><?= $consecutivoActual; ?></td>
                                                                <td class="text-center"><?= $consumed; ?></td>
                                                            </tr>
                                                    <?php        
                                                            }
                                                        } 
                                                    }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center" colspan="4"></th>
                                                        <th class="text-center" id="totalDocSup"><?= $factura_total_por_cons ?></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="questionFour">
                                        <h5 class="panel-title d-flex justify-content-between">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#faq" href="#answerFour"  aria-expanded="false" aria-controls="answerFour">
                                                Recepción de documentos
                                            </a>
                                            <span class="total" id="titleRep"></span>
                                        </h5>
                                    </div>
                                    <div id="answerFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionFour">
                                        <div class="panel-body">
                                        <table class='table'>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" ><?= lang('document_type_sales_prefix') ?></th>
                                                        <th class="text-center" ><?= lang('document_name') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_inicio_resolucion_wappsi') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_sales_consecutive') ?></th>
                                                        <th class="text-center" ><?= lang('consumed') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                    $factura_total_cons = 0;
                                                    $factura_total_por_cons = 0;
                                                    /*
                                                        "48" => "Acuse de Recibo",
                                                        "49" => "Acuse del bien o servicio",
                                                        "50" => "Reclamo documento electrónico",
                                                        "51" => "Aceptación expresa documento electrónico",
                                                    */
                                                    if (!empty($documents_types)) {
                                                        foreach ($documents_types as $key => $value) {
                                                            if ($value->module == 48 || $value->module == 49 || $value->module == 50 || $value->module == 51) {
                                                                $inicioResolucionWappsi= $value->inicio_resolucion_wappsi;
                                                                $consecutivoActual = $value->sales_consecutive; 
                                                                $consumed = $value->total;
                                                                $for_consuming = $consumed - ($inicioResolucionWappsi-1);
                                                                $factura_total_por_cons += $consumed;
                                                    ?>
                                                            <tr>
                                                                <td class="text-center"><?= $value->sales_prefix; ?></td>
                                                                <td class="text-center"><?= $value->name; ?></td>
                                                                <td class="text-center"><?= $inicioResolucionWappsi; ?></td>
                                                                <td class="text-center"><?= $consecutivoActual; ?></td>
                                                                <td class="text-center"><?= $consumed; ?></td>
                                                            </tr>
                                                    <?php        
                                                            }
                                                        } 
                                                    }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center" colspan="4" ></th>
                                                        <th class="text-center" id="totalRep"><?= $factura_total_por_cons ?></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </br>
                            </br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-title" style="min-height: 0px; padding: 10px 20px 7px;">
                    <h3 class="chart_titles" style="margin-bottom: 5px;">Nómina electrónica</h3>
                </div>
                <div class="ibox-content" style="min-height: 0px; padding: 10px 20px 7px;">
                    <div class="row" style="display: contents;">
                        <div class="col-lg-12">
                        <div id="faq1" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default">
                                    <div class="panel-heading" role="tab" id="questionOne">
                                        <h5 class="panel-title d-flex justify-content-between">
                                            <a data-toggle="collapse" data-parent="#faq1" href="#answerOne5" aria-expanded="true" aria-controls="answerOne5">
                                                Resumen de transmisión
                                            </a>
                                            <span class='total' id='titleNe'></span>   
                                        </h5>
                                    </div>
                                    <div id="answerOne5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="questionOne">
                                        <div class="panel-body">
                                            <table class='table'>
                                                <thead>
                                                    <tr>
                                                        <th class="text-center" ><?= lang('document_type_sales_prefix') ?></th>
                                                        <th class="text-center" ><?= lang('document_name') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_inicio_resolucion_wappsi') ?></th>
                                                        <th class="text-center" ><?= lang('document_type_sales_consecutive') ?></th>
                                                        <th class="text-center" ><?= lang('consumed') ?></th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php 
                                                    $factura_total_cons = 0;
                                                    $factura_total_por_cons = 0;
                                                    if (!empty($documents_types)) {
                                                        foreach ($documents_types as $key => $value) {
                                                            /* 
                                                                '2' => 'Factura',
                                                                '4' => 'Devolución Factura',
                                                                '27' => 'Nota Débito',
                                                            */
                                                            if ($value->module == 43 || $value->module == 44 || $value->module == 45) {
                                                                $inicioResolucionWappsi= $value->inicio_resolucion_wappsi;
                                                                $consumed = $value->total;
                                                                $consecutivoActual = $value->sales_consecutive-1; // menos uno por que el consecutivo actual siempre va por el proximo 
                                                                $factura_total_por_cons += $consumed;
                                                    ?>
                                                            <tr>
                                                                <td class="text-center"><?= $value->sales_prefix; ?></td>
                                                                <td class="text-center"><?= $value->name; ?></td>
                                                                <td class="text-center"><?= $inicioResolucionWappsi; ?></td>
                                                                <td class="text-center"><?= $consecutivoActual; ?></td>
                                                                <td class="text-center"><?= $consumed; ?></td>
                                                            </tr>
                                                    <?php        
                                                            }
                                                        }
                                                    }
                                                ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th class="text-center" colspan="4" ></th>
                                                        <th class="text-center" id="totalNe" ><?= $factura_total_por_cons ?></th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </br>
                            </br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
