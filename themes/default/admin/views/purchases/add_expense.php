<?php defined('BASEPATH') OR exit('No direct script access allowed');
    // var_dump($_SESSION);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_expense'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("purchases/add_expense", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <?php if ($Owner || $Admin) { ?>
                <div class="form-group">
                    <?= lang("date", "date"); ?>
                    <?= form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="date" required="required" disabled'); ?>
                </div>
            <?php } ?>
            <div class="form-group">
                <?= lang("reference", "document_type_id"); ?>
                <select name="document_type_id" class="form-control" id="add_expense_document_type_id" required>

                </select>
            </div>
            <div class="form-group">
                <?= lang('category', 'category'); ?>
                <select name="category" id="category" class="form-control tip" required>
                    <option value=""><?= lang('select') ?></option>
                    <?php foreach ($categories as $category): ?>
                        <option value="<?= $category->id ?>" data-tax1="<?= $category->tax_rate_id ?>" data-tax2="<?= $category->tax_rate_2_id ?>"><?= $category->name ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <?= lang('supplier', 'posupplier'); ?>
                <input type="hidden" name="supplier" value="" id="posupplier" class="form-control" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>">
                <input type="hidden" name="supplier_id" value="" id="supplier_id"
                    class="form-control">
            </div>
            <?php
            $bl[""] = "";
            $bldata = [];
            foreach ($billers as $biller) {
                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                $bldata[$biller->id] = $biller;
            }
            ?>

            <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                <div class="form-group">
                    <div class="form-group">
                        <?= lang("biller", "ex_biller"); ?>
                        <select name="biller" class="form-control" id="ex_biller" required="required">
                            <?php foreach ($billers as $biller) : ?>
                                <option value="<?= $biller->id ?>"><?= $biller->company ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            <?php } else { ?>
                <div class="form-group">
                    <div class="form-group">
                        <?= lang("biller", "ex_biller"); ?>
                        <select name="biller" class="form-control" id="ex_biller">
                            <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                $biller = $bldata[$this->session->userdata('biller_id')];
                            ?>
                                <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                            <?php endif ?>
                        </select>
                    </div>
                </div>
            <?php } ?>
            <?php if (isset($cost_centers)): ?>
                <div class="form-group">
                    <?= lang('cost_center', 'cost_center_id') ?>
                    <?php
                    $ccopts[''] = lang('select');
                    foreach ($cost_centers as $cost_center) {
                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                    }
                     ?>
                     <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                </div>
            <?php endif ?>
            <div class="form-group">
                <?= lang("value", "amount"); ?>
                <input name="amount" type="text" id="amount" value="" class="pa form-control kb-pad amount"
                       required="required"/>
            </div>
            <div class="form-group">
                <label><?= lang('tax_1') ?></label>
                <div class="row">
                    <div class="col-xs-6">
                        <select name="exp_ptax" id="exp_ptax" class="form-control exp_ptax">
                            <option value=""><?= lang('n/a') ?></option>
                            <?php foreach ($tax_rates as $tax): ?>
                                <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <?= form_input('exp_ptax_value', '', 'id="exp_ptax_value" class="form-control"'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label><?= lang('tax_2') ?></label>
                <div class="row">
                    <div class="col-xs-6">
                        <select name="exp_ptax_2" id="exp_ptax_2" class="form-control exp_ptax_2">
                            <option value=""><?= lang('n/a') ?></option>
                            <?php foreach ($tax_rates as $tax): ?>
                                <option value="<?= $tax->id ?>" data-taxrate="<?= $tax->rate ?>"><?= $tax->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-xs-6">
                        <?= form_input('exp_ptax_2_value', '', 'id="exp_ptax_2_value" class="form-control"'); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <table class="table table-bordered">
                    <tr>
                        <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                        <th style="width:16.66%;"><span id="exp_net_cost"></span></th>
                        <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                        <th style="width:16.66%;"><span id="exp_total_tax"></span></th>
                        <th style="width:16.66%;"><?= lang('total'); ?></th>
                        <th style="width:16.66%;"><span id="exp_pro_total"></span></th>
                    </tr>
                </table>
            </div>
            <div class="form-group">
                <label onclick="showRetention()" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención</label>
                <input type="text" name="retencion" id="exp_rete" class="form-control text-right" readonly>
            </div>
            <div class="section-retentions" style="display: none;">
                  <div class="row">
                    <div class="col-sm-2">
                      <label>Retención</label>
                    </div>
                    <div class="col-sm-5">
                      <label>Opción</label>
                    </div>
                    <div class="col-sm-2">
                      <label>posexcentaje</label>
                    </div>
                    <div class="col-sm-3">
                      <label>Valor</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="exp_rete_fuente" class="skip" id="exp_rete_fuente"> Fuente
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="exp_rete_fuente_option" id="exp_rete_fuente_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="exp_rete_fuente_account" id="exp_rete_fuente_account">
                      <input type="hidden" name="exp_rete_fuente_base" id="exp_rete_fuente_base">
                      <input type="hidden" name="exp_rete_fuente_id" id="exp_rete_fuente_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="exp_rete_fuente_tax" id="exp_rete_fuente_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="exp_rete_fuente_valor" id="exp_rete_fuente_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="exp_rete_iva" class="skip" id="exp_rete_iva"> Iva
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="exp_rete_iva_option" id="exp_rete_iva_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="exp_rete_iva_account" id="exp_rete_iva_account">
                      <input type="hidden" name="exp_rete_iva_base" id="exp_rete_iva_base">
                      <input type="hidden" name="exp_rete_iva_id" id="exp_rete_iva_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="exp_rete_iva_tax" id="exp_rete_iva_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="exp_rete_iva_valor" id="exp_rete_iva_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="exp_rete_ica" class="skip" id="exp_rete_ica"> Ica
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="exp_rete_ica_option" id="exp_rete_ica_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="exp_rete_ica_account" id="exp_rete_ica_account">
                      <input type="hidden" name="exp_rete_ica_base" id="exp_rete_ica_base">
                      <input type="hidden" name="exp_rete_ica_id" id="exp_rete_ica_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="exp_rete_ica_tax" id="exp_rete_ica_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="exp_rete_ica_valor" id="exp_rete_ica_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="exp_rete_otros" class="skip" id="exp_rete_otros"> Otros
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="exp_rete_otros_option" id="exp_rete_otros_option" disabled='true'>
                        <option value="">Seleccione...</option>
                      </select>
                      <input type="hidden" name="exp_rete_otros_account" id="exp_rete_otros_account">
                      <input type="hidden" name="exp_rete_otros_base" id="exp_rete_otros_base">
                      <input type="hidden" name="exp_rete_otros_id" id="exp_rete_otros_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="exp_rete_otros_tax" id="exp_rete_otros_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="exp_rete_otros_valor" id="exp_rete_otros_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 text-right">
                      <label>Total : </label>
                    </div>
                    <div class="col-md-4 text-right">
                      <label id="total_exp_rete_amount"> 0.00 </label>
                    </div>
                    <input type="hidden" name="exp_rete_applied" id="exp_rete_applied" value="0">
                  </div>
            </div>
            <div class="form-group">
                <?= lang("attachment", "attachment") ?>
                <input id="attachment" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" data-show-upload="false" data-show-preview="false"
                       class="form-control file">
            </div>
            <div class="form-group">
                <?= lang("note", "note"); ?>
                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="note"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_expense', lang('add_expense'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
        nsSupplier();
        $(document).on('change', '#ex_biller', function(){
            $.ajax({
                url: site.base_url+'billers/getBillersDocumentTypes/36/'+$('#ex_biller').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){
                response = data;
                $('#add_expense_document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
                if (response.status == 0) {
                  $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                }
                $('#add_expense_document_type_id').trigger('change');
            });
        });
        $('#ex_biller').trigger('change');
    });

    var tax_rates = <?php echo json_encode($tax_rates); ?>;

    $('#category').on('change', function(){
        category = $(this);
        var tax_1 = $('#category option:selected').data('tax1');
        var tax_2 = $('#category option:selected').data('tax2');
        $.ajax({
            type:'POST',
            url:'<?= admin_url("purchases/getTaxInfoJson") ?>',
            data : {
                'tax_1' : tax_1,
                'tax_2' : tax_2,
                '<?=$this->security->get_csrf_token_name()?>': "<?=$this->security->get_csrf_hash()?>"
            }
        }).done(function(data){
            if (data != false) {
                tax_info = data;
                tax_rate = tax_info.tax_1;
                tax_rate_2 = tax_info.tax_2;
                net_cost = $('#amount').val() > 0 ? $('#amount').val() : 0;
                var tax_method = 1;
                $('#exp_ptax').select2('val', tax_1);
                $('#exp_ptax_2').select2('val', tax_2);
                if (tax_rate.type == 1) {
                    $('#exp_ptax_value').prop('readonly', 'readonly');
                }
                if (tax_rate_2.type == 1) {
                    $('#exp_ptax_2_value').prop('readonly', 'readonly');
                } //segundo iva
                if (tax_rate == false) {
                    $('#exp_ptax').select2('val', '').select2('readonly', 'readonly');
                    $('#exp_ptax_value').prop('readonly', 'readonly');
                } else {
                    $('#exp_ptax').select2('readonly', false);
                }
                if (tax_rate_2 == false) {
                    $('#exp_ptax_2').select2('val', '').select2('readonly', 'readonly');
                    $('#exp_ptax_2_value').prop('readonly', 'readonly');
                } else {
                    $('#exp_ptax_2').select2('readonly', false);
                }
                var pr_tax = tax_rate, pr_tax_val = 0;
                if (pr_tax !== null && pr_tax != 0) {
                    $.each(tax_rates, function () {
                        if(this.id == tax_1){
                            if (this.type == 1) {
                                if (tax_method == 0) {
                                    pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                    net_cost -= pr_tax_val;
                                } else {
                                    pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                                    pr_tax_rate = formatDecimal(this.rate) + '%';
                                }
                                $('#exp_ptax_value').prop('readonly', 'readonly');
                            } else if (this.type == 2) {
                                pr_tax_val = parseFloat(this.rate);
                                pr_tax_rate = this.rate;
                                $('#exp_ptax_value').removeAttr('readonly');
                            }
                            $('#exp_ptax_value').val(formatDecimal(pr_tax_val));
                        }
                    });
                }

                var pr_tax_2 = tax_rate_2, pr_tax_2_val = 0;
                if (pr_tax_2 !== null && pr_tax_2 != 0) {
                    $.each(tax_rates, function () {
                        if(this.id == tax_2){
                            if (this.type == 1) {
                                if (tax_method == 0) {
                                    pr_tax_2_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                                    pr_tax_2_rate = formatDecimal(this.rate) + '%';
                                    net_cost -= pr_tax_2_val;
                                } else {
                                    pr_tax_2_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                                    pr_tax_2_rate = formatDecimal(this.rate) + '%';
                                }
                                $('#exp_ptax_2_value').prop('readonly', 'readonly');
                            } else if (this.type == 2) {
                                pr_tax_2_val = parseFloat(this.rate);
                                pr_tax_2_rate = this.rate;
                                $('#exp_ptax_2_value').removeAttr('readonly');
                            }
                            $('#exp_ptax_2_value').val(formatDecimal(pr_tax_2_val));
                        }
                    });
                }
                $('#exp_net_cost').text(formatMoney(net_cost));
                $('#exp_total_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
                $('#exp_pro_total').text(formatMoney(parseFloat(net_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
                $('#exp_pro_total').data('originalamount', formatMoney(parseFloat(net_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));

            } else {
                console.log(data);
            }
        });
    });

    $(document).on('change', '#amount, #exp_ptax, #exp_ptax_2, #exp_ptax_value, #exp_ptax_2_value', function(){
        var tax_1 = $('#exp_ptax').val();
        var tax_2 = $('#exp_ptax_2').val();
        var item_tax_method = 1;
        var net_cost = $('#amount').val() > 0 ? $('#amount').val() : 0;
        var pr_tax_val = 0;
        var pr_tax_2_val = 0;
        $.each(tax_rates, function () {
            type_tax = this.type;
            if(this.id == tax_1){
                if (this.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_val = formatDecimal(((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)), 4);
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                        net_cost -= pr_tax_val;
                    } else {
                        pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                    }
                    $('#exp_ptax_value').val(pr_tax_val).prop('readonly', 'readonly');
                } else if (this.type == 2) {
                    $('#exp_ptax_value').removeAttr('readonly');
                    rate_tax_c = $('#exp_ptax_value').val();
                    pr_tax_val = parseFloat(rate_tax_c);
                    pr_tax_rate = rate_tax_c;
                }
            }

            if(this.id == tax_2){
                if (this.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_2_val = formatDecimal(((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)), 4);
                        pr_tax_2_rate = formatDecimal(this.rate) + '%';
                        net_cost -= pr_tax_2_val;
                    } else {
                        pr_tax_2_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100), 4);
                        pr_tax_2_rate = formatDecimal(this.rate) + '%';
                    }
                    $('#exp_ptax_2_value').val(pr_tax_2_val).prop('readonly', 'readonly');
                } else if (this.type == 2) {
                    $('#exp_ptax_2_value').removeAttr('readonly');
                    rate_tax_c = $('#exp_ptax_2_value').val();
                    pr_tax_2_val = parseFloat(rate_tax_c);
                    pr_tax_2_rate = rate_tax_c;
                }
            }
        });
        $('#exp_net_cost').text(formatMoney(net_cost));
        $('#exp_total_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
        $('#exp_pro_total').text(formatMoney(parseFloat(net_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
        $('#exp_pro_total').data('originalamount', formatMoney(parseFloat(net_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)));
        recalcular_posexetenciones();
    });


function nsSupplier() {
    $('#posupplier').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 2,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function showRetention(){
    sr = $('.section-retentions');
    if (sr.hasClass('active')) {
        $('.section-retentions').fadeOut().removeClass('active');
        $('#exp_rete_fuente').attr('checked', true).trigger('click');
        $('#exp_rete_iva').attr('checked', true).trigger('click');
        $('#exp_rete_ica').attr('checked', true).trigger('click');
        $('#exp_rete_otros').attr('checked', true).trigger('click');
    } else {
        $('.section-retentions').fadeIn().addClass('active');
    }
}

// JS posexetenciones

$(document).on('click', '#exp_rete_fuente', function(){
    if ($(this).is(':checked')) {
         $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/FUENTE"
        }).done(function(data){
            $('#exp_rete_fuente_option').html(data);
            $('#exp_rete_fuente_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#exp_rete_fuente_option').val('').attr('disabled', true).select();
        $('#exp_rete_fuente_tax').val('');
        $('#exp_rete_fuente_valor').val('');
        setReteTotalAmount(rete_fuente_amount, '-');
    }
});

$(document).on('click', '#exp_rete_iva', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/IVA"
        }).done(function(data){
            $('#exp_rete_iva_option').html(data);
            $('#exp_rete_iva_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#exp_rete_iva_option').val('').attr('disabled', true).select();
        $('#exp_rete_iva_tax').val('');
        $('#exp_rete_iva_valor').val('');
        setReteTotalAmount(rete_iva_amount, '-');
    }
});

$(document).on('click', '#exp_rete_ica', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/ICA"
        }).done(function(data){
            $('#exp_rete_ica_option').html(data);
            $('#exp_rete_ica_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#exp_rete_ica_option').val('').attr('disabled', true).select();
        $('#exp_rete_ica_tax').val('');
        $('#exp_rete_ica_valor').val('');
        setReteTotalAmount(rete_ica_amount, '-');
    }
});

$(document).on('click', '#exp_rete_otros', function(){
    if ($(this).is(':checked')) {
        $.ajax({
            url: site.base_url+"purchases/opcionesWithHolding/OTRA"
        }).done(function(data){
            $('#exp_rete_otros_option').html(data);
            $('#exp_rete_otros_option').attr('disabled', false).select();
        }).fail(function(data){
            console.log(data);
        });
    } else {
        $('#exp_rete_otros_option').val('').attr('disabled', true).select();
        $('#exp_rete_otros_tax').val('');
        $('#exp_rete_otros_valor').val('');
        setReteTotalAmount(rete_otros_amount, '-');
    }
});

var rete_fuente_amount = 0;
var rete_ica_amount = 0;
var rete_iva_amount = 0;
var rete_otros_amount = 0;

$(document).on('change', '#exp_rete_fuente_option', function(){
    rete_fuente_id = $(this).val();
    $('#exp_rete_fuente_id').val(rete_fuente_id);
    trmrate = 1;
    prevamnt = $('#exp_rete_fuente_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#exp_rete_fuente_option option:selected').data('percentage');
    apply = $('#exp_rete_fuente_option option:selected').data('apply');
    account = $('#exp_rete_fuente_option option:selected').data('account');
    $('#exp_rete_fuente_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#exp_rete_fuente_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#exp_rete_fuente_tax').val(percentage);
    $('#exp_rete_fuente_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#exp_rete_fuente_valor').val(cAmount);
    rete_fuente_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_fuente_amount, '+');
});

$(document).on('change', '#exp_rete_iva_option', function(){
    rete_iva_id = $(this).val();
    $('#exp_rete_iva_id').val(rete_iva_id);
    trmrate = 1;
    prevamnt = $('#exp_rete_iva_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#exp_rete_iva_option option:selected').data('percentage');
    apply = $('#exp_rete_iva_option option:selected').data('apply');
    account = $('#exp_rete_iva_option option:selected').data('account');
    $('#exp_rete_iva_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#exp_rete_iva_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#exp_rete_iva_tax').val(percentage);
    $('#exp_rete_iva_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#exp_rete_iva_valor').val(cAmount);
    rete_iva_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_iva_amount, '+');
});

$(document).on('change', '#exp_rete_ica_option', function(){
    rete_ica_id = $(this).val();
    $('#exp_rete_ica_id').val(rete_ica_id);
    trmrate = 1;
    prevamnt = $('#exp_rete_ica_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#exp_rete_ica_option option:selected').data('percentage');
    apply = $('#exp_rete_ica_option option:selected').data('apply');
    account = $('#exp_rete_ica_option option:selected').data('account');
    $('#exp_rete_ica_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#exp_rete_ica_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#exp_rete_ica_tax').val(percentage);
    $('#exp_rete_ica_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#exp_rete_ica_valor').val(cAmount);
    rete_ica_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_ica_amount, '+');
});

$(document).on('change', '#exp_rete_otros_option', function(){
    rete_otros_id = $(this).val();
    $('#exp_rete_otros_id').val(rete_otros_id);
    trmrate = 1;
    prevamnt = $('#exp_rete_otros_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#exp_rete_otros_option option:selected').data('percentage');
    apply = $('#exp_rete_otros_option option:selected').data('apply');
    account = $('#exp_rete_otros_option option:selected').data('account');
    $('#exp_rete_otros_account').val(account);
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    $('#exp_rete_otros_base').val(amount);
    cAmount = amount * (percentage / 100);
    cAmount = Math.round(cAmount);
    $('#exp_rete_otros_tax').val(percentage);
    $('#exp_rete_otros_valor_show').val(formatDecimal(trmrate * cAmount));
    $('#exp_rete_otros_valor').val(cAmount);
    rete_otros_amount = formatMoney(cAmount);
    setReteTotalAmount(rete_otros_amount, '+');
});

function getReteAmount(apply){
    amount = 0;
    if (apply == "ST") {
        amount = $('#exp_net_cost').text();
    } else if (apply == "TX") {
        amount = $('#exp_total_tax').text();
    } else if (apply == "TO") {
        amount = $('#exp_pro_total').text();
    }
    if (othercurrency = localStorage.getItem('othercurrency')) {
        othercurrencytrm = localStorage.getItem('othercurrencytrm');
        if (othercurrencytrm > 0) {
            amount = amount * othercurrencytrm;
        }
    }
    return amount;
}

function setReteTotalAmount(amount, action){
    if (action == "+") {
        tra =  formatDecimal(parseFloat(formatDecimal($('#total_exp_rete_amount').text()))) + formatDecimal(amount);
    } else if (action == "-") {
        tra =  formatDecimal(parseFloat(formatDecimal($('#total_exp_rete_amount').text()))) - formatDecimal(amount);
    }
    if (tra < 0) {
        tra = 0;
    }
    trmrate = 1;
    if (tra > 0) {
        $('#exp_rete_applied').val(1);
    } else {
        $('#exp_rete_applied').val(0);
    }
    $('#exp_rete').val(formatMoney(trmrate * tra));
    $('#total_exp_rete_amount').text(formatMoney(tra));
    exp_pro_total = formatDecimal(parseFloat(formatDecimal($('#exp_pro_total').data('originalamount'))));
    exp_pro_total = exp_pro_total - tra;
    $('#exp_pro_total').text(formatMoney(exp_pro_total));
}

function recalcular_posexetenciones(){
    $('#exp_rete_fuente_option').trigger('change');
    $('#exp_rete_iva_option').trigger('change');
    $('#exp_rete_ica_option').trigger('change');
    $('#exp_rete_otros_option').trigger('change');
}
</script>