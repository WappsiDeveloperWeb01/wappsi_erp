<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .dfTable th, .dfTable td {
        text-align: center;
        vertical-align: middle;
    }

    .dfTable td {
        padding: 2px;
    }

    .data tr:nth-child(odd) td {
        color: #2FA4E7;
    }

    .data tr:nth-child(even) td {
        text-align: right;
    }
</style>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                        <li class="dropdown"> <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>"> <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?> </a> </li>
                                    </ul>
                                </div>
                                <div class="pull-right dropdown">
                                    <?php if (!empty($billers) && !$this->session->userdata('biller_id')) { ?>
                                        <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('billers') ?> <span class="caret"></span> </button>
                                        <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                            <li><a href="<?=admin_url('reports/monthly_sales/0/'.$year)?>"><i class="fa fa-building-o"></i> <?=lang('all_billers')?></a></li>
                                            <li class="divider"></li>
                                            <?php
                                                foreach ($billers as $biller) {
                                                        echo '<li><a href="' . admin_url('reports/monthly_sales/'.$biller->id.'/'.$year) . '"><i class="fa fa-building"></i>' . $biller->name . '</a></li>';
                                                    }
                                                ?>
                                        </ul>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <br>
                                <div class="table-responsive">
                                    <table class="table table-bordered dfTable reports-table">
                                        <thead>
                                        <tr class="year_roller">
                                            <th><a href="<?= admin_url('reports/monthly_sales/'.(isset($biller_id) ? $biller_id : 0).'/'.($year-1)); ?>">&lt;&lt;</a></th>
                                            <th colspan="10" style="font-size: 18px;"> <?php echo $year; ?></th>
                                            <th><a href="<?= admin_url('reports/monthly_sales/'.(isset($biller_id) ? $biller_id : 0).'/'.($year+1)); ?>">&gt;&gt;</a></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="01">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/01'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_january"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="02">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/02'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_february"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="03">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/03'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_march"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="04">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/04'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_april"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="05">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/05'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_may"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="06">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/06'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_june"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="07">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/07'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_july"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="08">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/08'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_august"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="09">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/09'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_september"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="10">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/10'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_october"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="11">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/11'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_november"); ?>
                                                </a>
                                            </td>
                                            <td class="bold text-center monthly" data-year="<?= $year ?>" data-monthnum="12">
                                                <a href="<?= admin_url('reports/monthly_profit/'.$year.'/12'); ?>" data-toggle="modal" data-target="#myModal">
                                                    <?= lang("cal_december"); ?>
                                                </a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <?php
                                            $m_num = 0;
                                            if (!empty($sales)) {
                                                foreach ($sales as $value) {
                                                    $m_num++;
                                                    $array[$value->date] = "
                                                    <table class='monthly_".$value->date."' data-year='".$year."' data-monthnum='".($value->date > 9 ? $value->date : "0".$value->date)."' style='margin: 2%;'>
                                                        <tbody>
                                                            <tr ".($value->sub_total > 0 ? "" : "style='display:none;'").">
                                                                <th>" . $this->lang->line("sub_total") . "</th>
                                                            </tr>
                                                            <tr ".($value->sub_total > 0 ? "" : "style='display:none;'").">
                                                                <td>" . $this->sma->formatMoney($value->sub_total) . "</td>
                                                            </tr>
                                                            <tr ".($value->discount > 0 ? "" : "style='display:none;'").">
                                                                <th>" . $this->lang->line("discount") . "</th>
                                                            </tr>
                                                            <tr ".($value->discount > 0 ? "" : "style='display:none;'").">
                                                                <td>" . $this->sma->formatMoney($value->discount) . "</td>
                                                            </tr>
                                                            <tr ".($value->shipping > 0 ? "" : "style='display:none;'").">
                                                                <th>" . $this->lang->line("shipping") . "</th>
                                                            </tr>
                                                            <tr ".($value->shipping > 0 ? "" : "style='display:none;'").">
                                                                <td>" . $this->sma->formatMoney($value->shipping) . "</td>
                                                            </tr>
                                                            <tr ".($value->tax1 > 0 ? "" : "style='display:none;'").">
                                                                <th>" . $this->lang->line("tax") . "</th>
                                                            </tr>
                                                            <tr ".($value->tax1 > 0 ? "" : "style='display:none;'").">
                                                                <td>" . $this->sma->formatMoney($value->tax1) . "</td>
                                                            </tr>
                                                            <tr ".($value->tax2 > 0 ? "" : "style='display:none;'").">
                                                                <th>" . $this->lang->line("order_tax") . "</th>
                                                            </tr>
                                                            <tr ".($value->tax2 > 0 ? "" : "style='display:none;'").">
                                                                <td>" . $this->sma->formatMoney($value->tax2) . "</td>
                                                            </tr>
                                                            <tr ".($value->tip > 0 ? "" : "style='display:none;'").">
                                                                <th>" . $this->lang->line("tip") . "</th>
                                                            </tr>
                                                            <tr ".($value->tip > 0 ? "" : "style='display:none;'").">
                                                                <td>" . $this->sma->formatMoney($value->tip) . "</td>
                                                            </tr>
                                                            <tr ".($value->total > 0 ? "style='border-top-style: solid; border-top-width: 1px;'" : "style='display:none;'").">
                                                                <th style='font-size:107%;'>" . $this->lang->line("total") . "</th>
                                                            </tr>
                                                            <tr ".($value->total > 0 ? "" : "style='display:none;'").">
                                                                <th style='font-size:115%;'>" . $this->sma->formatMoney($value->total) . "</th>
                                                            </tr>
                                                            <tr ".($value->returned_total != 0 ? "" : "style='display:none;'").">
                                                                <th>" . $this->lang->line("returns") . "</th>
                                                            </tr>
                                                            <tr ".($value->returned_total != 0 ? "" : "style='display:none;'").">
                                                                <td>" . $this->sma->formatMoney($value->returned_total) . "</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>";
                                                }
                                                for ($i = 1; $i <= 12; $i++) {
                                                    echo '<td width="8.3%">';
                                                    if (isset($array[$i])) {
                                                        echo $array[$i];
                                                    } else {
                                                        echo '<strong>0</strong>';
                                                        $m_num++;
                                                    }
                                                    echo '</td>';
                                                }
                                            } else {
                                                for ($i = 1; $i <= 12; $i++) {
                                                    echo '<td width="8.3%"><strong>0</strong></td>';
                                                }
                                            }
                                            ?>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- /Body -->

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/monthly_sales/'.($biller_id ? $biller_id : 0).'/'.$year.'/pdf')?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });

        $('.monthly').click(function () {
            var monthnum = $(this).data('monthnum');
            var year = $(this).data('year');
            var href = '<?= admin_url('reports/monthly_profit'); ?>/'+year+'/'+monthnum;
            $.get(href, function( data ) {
                $("#myModal").html(data).modal();
            });

        });

    });
</script>
