<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
        </div>

        <?= admin_form_open("system_settings/update_commercial_discount", ["id"=>"edit_commercial_discount_form"]); ?>
        <?= form_hidden('id', $commercial_discounts_data->id); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= form_label(lang("commercial_discounts_apply_to")); ?>
                <?php $mopts = [
                    ""=>lang('select'),
                    "PST"=>"PST",
                    "ST"=>"ST",
                    "PTO"=>"PTO",
                    "TO"=>"TO",
                    "TX"=>"TX",
                    "MNL"=>"MNL"
                ]; ?>
                <?= form_dropdown(["name"=>"apply_to", "id"=>"apply_to", "class"=>"form-control select2", "required"=>TRUE], $mopts, $commercial_discounts_data->apply_to); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("commercial_discounts_sales_accounting_account", "ledger_id")); ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }
                ?>
                <?= form_dropdown(["name"=>"ledger_id", "id"=>"ledger_id", "class"=>"form-control select2", "required"=>TRUE], $lopts, $commercial_discounts_data->ledger_id); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("commercial_discounts_purchase_accounting_account", "purchase_ledger_id")); ?>
                <?php
                    $lopts[''] = lang('select');

                    foreach ($ledgers as $ledger) {
                        $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                    }
                ?>
                <?= form_dropdown(["name"=>"purchase_ledger_id", "id"=>"purchase_ledger_id", "class"=>"form-control select2", "required"=>TRUE], $lopts, $commercial_discounts_data->purchase_ledger_id); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("types_movement_document_type_name", "name")); ?>
                <?= form_input(["name"=>"name", "id"=>"name", "class"=>"form-control", "required"=>TRUE, "disabled"=>TRUE], lang($commercial_discounts_data->name)); ?>
            </div>

        </div>
        <div class="modal-footer">
            <?= form_button("commercial_discounts_edit", lang("save"), 'class="btn btn-primary" id="commercial_discounts_edit"'); ?>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $("#edit_types_movement_form").validate({ ignore: [] });
        $("#apply_to").select2("enable", false);

        $(document).on('click', '#commercial_discounts_edit', function() {
            if ($('#edit_commercial_discount_form').valid()) {
                $('#edit_commercial_discount_form').submit();
            }
        });
    });
</script>
