<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_provisions_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function get_by_payroll_id(int $payroll_id)
    {
        $this->db->select("payroll_provisions.id, payroll_provisions.payroll_id, payroll_provisions.employee_id, payroll_provisions.concept_id, payroll_provisions.percentage, SUM(base_salary) AS base_salary, SUM(amount) AS amount, payroll_concepts.name");
        $this->db->join("payroll_concepts", "payroll_concepts.id = payroll_provisions.concept_id", "inner");
        $this->db->where("payroll_id", $payroll_id);
		$this->db->group_by(["concept_id"]);
        $q = $this->db->get("payroll_provisions");
        return $q->result();
    }

	public function get_by_payroll_id_employee_id(int $payroll_id, int $employee_id, int $contract_id)
	{
		$this->db->select("payroll_id, employee_id, concept_id, SUM(base_salary) AS base_salary, payroll_provisions.percentage, SUM(amount) AS amount, payroll_concepts.name");
		$this->db->join("payroll_concepts", "payroll_concepts.id = payroll_provisions.concept_id", "inner");
		$this->db->where("payroll_id", $payroll_id);
		$this->db->where("employee_id", $employee_id);
		$this->db->where("contract_id", $contract_id);
		$this->db->group_by(["concept_id", "percentage"]);
		$q = $this->db->get("payroll_provisions");
		return $q->result();
	}

	public function insert($data)
	{
		if ($this->db->insert_batch('payroll_provisions', $data) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function delete(int $payroll_id, int $employee_id, int $contract_id)
	{
		$this->db->where("payroll_id", $payroll_id);
		$this->db->where("employee_id", $employee_id);
		if ($this->db->delete("payroll_provisions")) {
			return TRUE;
		}
		return FALSE;
	}
}

/* End of file Payroll_provisions_model.php */
/* Location: .//C/xampp/htdocs/wappsi_erp/app/models/admin/Payroll_provisions_model.php */