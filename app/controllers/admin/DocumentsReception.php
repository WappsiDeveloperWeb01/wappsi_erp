<?php

use function GuzzleHttp\json_encode;

 defined('BASEPATH') OR exit('No direct script access allowed');

class DocumentsReception extends MY_Controller
{
    public $settings = [];

    const TESTING = 1;
    const PRODUCTION = 2;

    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_NOT_MODIFIED = 304;
    const HTTP_BAD_REQUEST = 400;
    const HTTP_UNAUTHORIZED = 401;
    const HTTP_FORBIDDEN = 403;
    const HTTP_NOT_FOUND = 404;
    const HTTP_METHOD_NOT_ALLOWED = 405;
    const HTTP_NOT_ACCEPTABLE = 406;
    const HTTP_CONFLICT = 409;
    const HTTP_INTERNAL_ERROR = 500;
    const HTTP_GATEWAY_TIMEOUT = 504;

    const E_NOT_SENT = 0;
    const E_NOT_CREATED = 1;
    const E_RECEIVED = 2;
    const E_SEND_ERROR = 3;
    const E_DISABLED = 4;

    const NOT_SYNCHRONIZED = 0;
    const SYNCHRONIZED = 1;
    const SYNC_UP = 2;

    const ACKNOWLEDGMENT_RECEIPT =  '030';
    const RECEPTION_GOOD_SERVICE =  '032';
    const EXPRESS_ACCEPTANCE =  '033';
    const CLAIM =  '031';

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableDocumentsReception) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('documentReception_model');


        $this->viewPendingEvents = [
            ['id' => self::ACKNOWLEDGMENT_RECEIPT, 'name' => $this->lang->line('without_acknowledgment')],
            ['id' => self::RECEPTION_GOOD_SERVICE, 'name' => $this->lang->line('without_acceptance_goods_services')],
            ['id' => self::EXPRESS_ACCEPTANCE, 'name' => $this->lang->line('no_express_acceptance')],
            ['id' => self::CLAIM, 'name' => $this->lang->line('with_claim')]
        ];

        $this->settings = $this->documentReception_model->getSettings();
    }

    public function index()
    {
        $this->sma->checkPermissions('index', null, 'documentsReception');

        $this->validateSettingsData();

        $this->data['viewPendingEvents'] = json_decode(json_encode($this->viewPendingEvents));
        $this->data['ACKNOWLEDGMENT_RECEIPT'] = self::ACKNOWLEDGMENT_RECEIPT;
        $this->data['RECEPTION_GOOD_SERVICE'] = self::RECEPTION_GOOD_SERVICE;
        $this->data['EXPRESS_ACCEPTANCE'] = self::EXPRESS_ACCEPTANCE;
        $this->data['CLAIM'] = self::CLAIM;


        $this->page_construct('documents_reception/index', ['page_title' => lang('document_reception')], $this->data);
    }

    private function validateSettingsData()
    {
        if (empty($this->settings)) {
            $this->session->set_flashdata('error', $this->lang->line('Por favor complete los campos de ajustes de Documentos de recepción'));
            admin_redirect("documentsReception/settings");
        }

        $username = $this->settings->username;
        $workEnvironment = $this->settings->work_environment;
        $hostname = $this->settings->hostname;
        $hostUserName = $this->settings->hostUserName;
        $hostPassword = $this->settings->hostPassword;
        $hostnameDatabase = $this->settings->hostnameDatabase;

        if (empty($username) || empty($workEnvironment) || empty($hostname) || empty($hostUserName) || empty($hostPassword) || empty($hostnameDatabase)) {
            $this->session->set_flashdata('error', $this->lang->line('Por favor complete los campos de ajustes de Documentos de recepción'));
            admin_redirect("documentsReception/settings");
        }
    }

    public function getDocuments()
    {
        $this->load->library('datatables');

        $data = [];
        $viewPendingEvents = $this->input->post('viewPendingEvents');
        $end_date = ($this->input->post('end_date')) ? $this->sma->fld($this->input->post('end_date')) : '';
        $start_date = ($this->input->post('start_date')) ? $this->sma->fld($this->input->post('start_date')) : '';
        $supplier = ($this->input->post('supplier')) ? $this->input->post('supplier') : '';

        if ($documentsReceived = $this->documentReception_model->getDocumentsReceivedClient($start_date, $end_date, $supplier)) {
            foreach ($documentsReceived as $document) {
                $acknowledgmentEvent = $this->documentReception_model->getDocumentEventClientById($document->idAcknowledgment);
                $acknowledgmentGoodServiceEvent = $this->documentReception_model->getDocumentEventClientById($document->idAcknowledgmentGoodService);
                $expressAcceptanceEvent = $this->documentReception_model->getDocumentEventClientById($document->idExpressAcceptance);
                $claimEvent = $this->documentReception_model->getDocumentEventClientById($document->idClaim);

                if ($this->getAmountEvents($viewPendingEvents, $acknowledgmentEvent, $acknowledgmentGoodServiceEvent, $expressAcceptanceEvent, $claimEvent)) {
                    $document->id = $document->id;
                    $document->supplierName = ucwords(strtolower($document->supplierName));
                    $document->grandTotal = number_format($document->grandTotal, 2);
                    $document->pdfUrlLink = $this->getFileUrl($document->pdfUrl, $document->hoursDiff);
                    $document->xmlUrlLink = $this->getFileUrl($document->xmlUrl, $document->hoursDiff);
                    $document->attachedDocumentLink = $this->getFileUrl($document->attachedDocumentUrl, $document->hoursDiff);

                    $document->documentTypeName = $this->getDocumentTypeName($document->documentTypeCode);

                    $acknowledgmentStatus = $this->getAcknowledgment($acknowledgmentEvent);
                    $acknowledgmentGoodServiceStatus = $this->getAcknowledgmentGoodServices($acknowledgmentGoodServiceEvent, $acknowledgmentEvent);
                    $expressAcceptanceStatus = $this->getExpressAcceptance($expressAcceptanceEvent, $claimEvent, $acknowledgmentGoodServiceEvent);
                    $claimStatus = $this->getClaim($claimEvent, $expressAcceptanceEvent, $acknowledgmentGoodServiceEvent);

                    $document->acknowledgment = $acknowledgmentStatus->icon;
                    $document->acknowledgmentGoodService = $acknowledgmentGoodServiceStatus->icon;
                    $document->expressAcceptance = $expressAcceptanceStatus->icon;
                    $document->claim = $claimStatus->icon;

                    $document->actions = $this->getActionsButtons($document, $acknowledgmentStatus, $acknowledgmentGoodServiceStatus, $expressAcceptanceStatus, $claimStatus);
                    $document->otro = $this->input->post('sSearch');
                    $data[] = $document;
                }
            }
        }

        $output = [
            'sEcho' => 1,
            'iTotalRecords' => count($data),
            'iTotalDisplayRecords' => count($data),
            'aaData' => $data
        ];
        echo json_encode($output);
    }

    private function getAmountEvents($viewPendingEvents, $acknowledgmentEvent, $acknowledgmentGoodServiceEvent, $expressAcceptanceEvent, $claimEvent)
    {
        if (!empty($viewPendingEvents)) {
            if ($viewPendingEvents == self::ACKNOWLEDGMENT_RECEIPT) {
                if (empty($acknowledgmentEvent) || (!empty($acknowledgmentEvent) && $acknowledgmentEvent->eventStatus != self::E_RECEIVED)) {
                    return true;
                }
            }

            if ($viewPendingEvents == self::RECEPTION_GOOD_SERVICE) {
                if (empty($acknowledgmentGoodServiceEvent) || (!empty($acknowledgmentGoodServiceEvent) && $acknowledgmentGoodServiceEvent->eventStatus != self::E_RECEIVED)) {
                    return true;
                }
            }

            if ($viewPendingEvents == self::EXPRESS_ACCEPTANCE) {
                if (empty($expressAcceptanceEvent) || (!empty($expressAcceptanceEvent) && $expressAcceptanceEvent->eventStatus != self::E_RECEIVED)) {
                    return true;
                }
            }

            if ($viewPendingEvents == self::CLAIM) {
                if (!empty($claimEvent) && $claimEvent->eventStatus == self::E_RECEIVED) {
                    return true;
                }
            }

            return false;
        }

        return true;
    }

    private function getDocumentTypeName($documentTypeCode)
    {
        if ($documentTypeCode == INVOICE) {
            return 'Factura';
        } else if ($documentTypeCode == EXPORT_INVOICE) {
            return 'Factura de exportación';
        } else if ($documentTypeCode == BILLING_CONTINGENCY_INVOICE) {
            return 'Factura de exportación';
        } else if ($documentTypeCode == DIAN_CONTINGENCY_INVOICE) {
            return ' Facturas por contingencia de la DIAN';
        } else if ($documentTypeCode == CREDIT_NOTE) {
            return 'Nota crédito';
        } else if ($documentTypeCode == DEBIT_NOTE) {
            return 'Nota débito';
        }
    }

    private function getFileUrl($path, $hoursDiff)
    {
        if ($hoursDiff >= 6) {
            return "<i class='fa fa-minus-circle fa-lg'>";
        }

        return "<a href=\"$path\" target=\"_blank\"><i class=\"fa fa-file\"></i></a>";
    }

    private function getAcknowledgment($acknowledgment)
    {
        if ($acknowledgment) {
            if ($acknowledgment->eventStatus == self::E_RECEIVED) {
                return (object) [
                    'status'    => self::E_RECEIVED,
                    'id'        => $acknowledgment->id,
                    'statusCode'=> self::ACKNOWLEDGMENT_RECEIPT,
                    'icon'      => "<i class='fa fa-check-square-o fa-lg text-info'>"
                ];
            }

            if ($acknowledgment->eventStatus == self::E_SEND_ERROR) {
                return (object) [
                    'id'                => $acknowledgment->id,
                    'status'            => self::E_SEND_ERROR,
                    'statusCode'        => self::ACKNOWLEDGMENT_RECEIPT,
                    'statusDian'        => $acknowledgment->statusDian,
                    'errorReason'       => $acknowledgment->errorReason,
                    'warningsDian'      => $acknowledgment->warningsDian,
                    'statusCodeRequest' => $acknowledgment->statusCodeRequest,
                    'icon'              => "<i class='fa fa-exclamation-circle fa-lg text-danger errorMessage pointer' eventId='$acknowledgment->id'>"
                ];
            }

            if ($acknowledgment->eventStatus == self::E_NOT_SENT) {
                return (object) [
                    'status'            => self::E_NOT_SENT,
                    'id'                => $acknowledgment->id,
                    'statusCode'        => self::ACKNOWLEDGMENT_RECEIPT,
                    'statusDian'        => $acknowledgment->statusDian,
                    'errorReason'       => $acknowledgment->errorReason,
                    'warningsDian'      => $acknowledgment->warningsDian,
                    'statusCodeRequest' => $acknowledgment->statusCodeRequest,
                    'icon'              => "<i class='fa fa-minus-circle fa-lg'>"
                ];
            }
        }

        return (object) [
            'status'    => self::E_NOT_CREATED,
            'statusCode'=> self::ACKNOWLEDGMENT_RECEIPT,
            'icon'      => "<i class='fa fa-minus-circle fa-lg'>"
        ];
    }

    private function getAcknowledgmentGoodServices($acknowledgmentGoodService, $acknowledgment)
    {
        if (!empty($acknowledgment)) {
            if ($acknowledgment->eventStatus != self::E_RECEIVED) {
                return (object) [
                    'status'    => self::E_DISABLED,
                    'statusCode'=> self::RECEPTION_GOOD_SERVICE,
                    'icon'      => "<i class='fa fa-ban fa-lg'>",
                ];
            }

            if (!empty($acknowledgmentGoodService)) {
                if ($acknowledgmentGoodService->eventStatus == self::E_RECEIVED) {
                    return (object) [
                        'status'    => self::E_RECEIVED,
                        'statusCode'=> self::RECEPTION_GOOD_SERVICE,
                        'id'        => $acknowledgmentGoodService->id,
                        'icon'      => "<i class='fa fa-check-square-o fa-lg text-info'>"
                    ];
                }

                if ($acknowledgmentGoodService->eventStatus == self::E_SEND_ERROR) {
                    return (object) [
                        'status'            => self::E_SEND_ERROR,
                        'statusCode'        => self::RECEPTION_GOOD_SERVICE,
                        'id'                => $acknowledgmentGoodService->id,
                        'statusDian'        => $acknowledgmentGoodService->statusDian,
                        'errorReason'       => $acknowledgmentGoodService->errorReason,
                        'warningsDian'      => $acknowledgmentGoodService->warningsDian,
                        'statusCodeRequest' => $acknowledgmentGoodService->statusCodeRequest,
                        'icon'              => "<i class='fa fa-exclamation-circle fa-lg text-danger errorMessage pointer' eventId='$acknowledgmentGoodService->id''>"
                    ];
                }

                if ($acknowledgmentGoodService->eventStatus == self::E_NOT_SENT) {
                    return (object) [
                        'status'            => self::E_NOT_SENT,
                        'icon'              => "<i class='fa fa-ban fa-lg'>",
                        'statusCode'        => self::RECEPTION_GOOD_SERVICE,
                        'id'                => $acknowledgmentGoodService->id,
                        'statusDian'        => $acknowledgmentGoodService->statusDian,
                        'errorReason'       => $acknowledgmentGoodService->errorReason,
                        'warningsDian'      => $acknowledgmentGoodService->warningsDian,
                        'statusCodeRequest' => $acknowledgmentGoodService->statusCodeRequest
                    ];
                }
            }

            return (object) [
                'status'    => self::E_NOT_CREATED,
                'statusCode'=> self::RECEPTION_GOOD_SERVICE,
                'icon'      => "<i class='fa fa-minus-circle fa-lg'>"
            ];
        }

        return (object) [
            'status'    => self::E_DISABLED,
            'statusCode'=> self::RECEPTION_GOOD_SERVICE,
            'icon'      => "<i class='fa fa-ban fa-lg'>"
        ];
    }

    private function getExpressAcceptance($expressAcceptance, $claim, $acknowledgmentGoodService)
    {
        if (!empty($claim)) {
            return (object) [
                'status'    => self::E_DISABLED,
                'statusCode'=> self::EXPRESS_ACCEPTANCE,
                'icon'      => "<i class='fa fa-ban fa-lg'>"
            ];
        }

        if (!empty($acknowledgmentGoodService)) {
            if ($acknowledgmentGoodService->eventStatus != self::E_RECEIVED) {
                return (object) [
                    'status'    => self::E_DISABLED,
                    'statusCode'=> self::EXPRESS_ACCEPTANCE,
                    'icon'      => "<i class='fa fa-ban fa-lg'>"
                ];
            }

            if (!empty($expressAcceptance)) {
                if ($expressAcceptance->eventStatus == self::E_RECEIVED) {
                    return (object) [
                        'status'    => self::E_RECEIVED,
                        'id'        => $expressAcceptance->id,
                        'statusCode'=> self::EXPRESS_ACCEPTANCE,
                        'icon'      => "<i class='fa fa-check-square-o fa-lg text-info'>"
                    ];
                }

                if ($expressAcceptance->eventStatus == self::E_SEND_ERROR) {
                    return (object) [
                        'status'            => self::E_SEND_ERROR,
                        'id'                => $expressAcceptance->id,
                        'statusCode'        => self::EXPRESS_ACCEPTANCE,
                        'statusDian'        => $expressAcceptance->statusDian,
                        'errorReason'       => $expressAcceptance->errorReason,
                        'warningsDian'      => $expressAcceptance->warningsDian,
                        'statusCodeRequest' => $expressAcceptance->statusCodeRequest,
                        'icon'              => "<i class='fa fa-exclamation-circle fa-lg text-danger errorMessage pointer' eventId='$expressAcceptance->id'>"
                    ];
                }

                if ($expressAcceptance->eventStatus == self::E_NOT_SENT) {
                    return (object) [
                        'status'            => self::E_NOT_SENT,
                        'id'                => $expressAcceptance->id,
                        'statusCode'        => self::EXPRESS_ACCEPTANCE,
                        'icon'              => "<i class='fa fa-ban fa-lg'>",
                        'statusDian'        => $expressAcceptance->statusDian,
                        'errorReason'       => $expressAcceptance->errorReason,
                        'warningsDian'      => $expressAcceptance->warningsDian,
                        'statusCodeRequest' => $expressAcceptance->statusCodeRequest
                    ];
                }
            }

            return (object) [
                'status'    => self::E_NOT_CREATED,
                'statusCode'=> self::EXPRESS_ACCEPTANCE,
                'icon'      => "<i class='fa fa-minus-circle fa-lg'>"
            ];
        }

        return (object) [
            'status'    => self::E_DISABLED,
            'statusCode'=> self::EXPRESS_ACCEPTANCE,
            'icon'      => "<i class='fa fa-ban fa-lg'>"
        ];
    }

    private function getClaim($claim, $expressAcceptance, $acknowledgmentGoodService)
    {
        if (!empty($expressAcceptance)) {
            return (object) [
                'status'    => self::E_DISABLED,
                'statusCode'=> self::CLAIM,
                'icon'      => "<i class='fa fa-ban fa-lg'>"
            ];
        }

        if (!empty($acknowledgmentGoodService)) {
            if ($acknowledgmentGoodService->eventStatus != self::E_RECEIVED) {
                return (object) [
                    'statusCode'=> self::CLAIM,
                    'status'    => self::E_DISABLED,
                    'icon'      => "<i class='fa fa-ban fa-lg'>"
                ];
            }

            if (!empty($claim)) {
                if ($claim->eventStatus == self::E_RECEIVED) {
                    return (object) [
                        'id'        => $claim->id,
                        'statusCode'=> self::CLAIM,
                        'status'    => self::E_RECEIVED,
                        'statusDian'=> $claim->statusDian,
                        'icon'      => "<i class='fa fa-check-square-o fa-lg text-info'>"
                    ];
                }

                if ($claim->eventStatus == self::E_SEND_ERROR) {
                    return (object) [
                        'id'                => $claim->id,
                        'statusCode'        => self::CLAIM,
                        'status'            => self::E_SEND_ERROR,
                        'statusDian'        => $claim->statusDian,
                        'errorReason'       => $claim->errorReason,
                        'warningsDian'      => $claim->warningsDian,
                        'statusCodeRequest' => $claim->statusCodeRequest,
                        'icon'              => "<i class='fa fa-exclamation-circle fa-lg text-danger errorMessage pointer'  eventId='$claim->id'>"
                    ];
                }

                if ($claim->eventStatus == self::E_NOT_SENT) {
                    return (object) [
                        'id'                => $claim->id,
                        'statusCode'        => self::CLAIM,
                        'status'            => self::E_NOT_SENT,
                        'statusDian'        => $claim->statusDian,
                        'errorReason'       => $claim->errorReason,
                        'warningsDian'      => $claim->warningsDian,
                        'statusCodeRequest' => $claim->statusCodeRequest,
                        'icon'              => "<i class='fa fa-ban fa-lg'>"
                    ];
                }
            }

            return (object) [
                'status'    => self::E_NOT_CREATED,
                'statusCode'=> self::CLAIM,
                'icon'      => "<i class='fa fa-minus-circle fa-lg'>"
            ];
        }

        return (object) [
            'status'    => self::E_DISABLED,
            'statusCode'=> self::CLAIM,
            'icon'      => "<i class='fa fa-ban fa-lg'>"
        ];
    }

    private function getActionsButtons($document, $acknowledgmentStatus, $acknowledgmentGoodServiceStatus, $expressAcceptanceStatus, $claimStatus)
    {
        $documentId = $document->id;
        $uuid = $document->uuid;
        $acknowledgmentActions = $acknowledgmentGoodServiceActions = $expressAcceptanceActions = $claimActions = $disableButton = "";

        if($this->Admin || $this->Owner) {
            $acknowledgmentActions = $this->getEventActionsButton($acknowledgmentStatus, $documentId);
            $acknowledgmentGoodServiceActions = $this->getEventActionsButton($acknowledgmentGoodServiceStatus, $documentId);
            $expressAcceptanceActions = $this->getEventActionsButton($expressAcceptanceStatus, $documentId);
            $claimActions = $this->getEventActionsButton($claimStatus, $documentId);
        } else {
            if ($this->GP['documentsReception-acknowledgment']) {
                $acknowledgmentActions = $this->getEventActionsButton($acknowledgmentStatus, $documentId);
            }

            if ($this->GP['documentsReception-reception_good_service']) {
                $acknowledgmentGoodServiceActions = $this->getEventActionsButton($acknowledgmentGoodServiceStatus, $documentId);
            }

            if ($this->GP['documentsReception-express_acceptance']) {
                $expressAcceptanceActions = $this->getEventActionsButton($expressAcceptanceStatus, $documentId);
            }

            if ($this->GP['documentsReception-claim']) {
                $claimActions = $this->getEventActionsButton($claimStatus, $documentId);
            }
        }

        $consultDIAN = $this->getConsultDIANUrl($uuid);

        if (empty($acknowledgmentActions) && empty($acknowledgmentGoodServiceActions) && empty($expressAcceptanceActions) && empty($claimActions)) {
            $disableButton = "disabled";
        }

        return "<div>
                    <div class='btn-group text-left'>
                        <button type='button' class='btn btn-default new-button new-button-sm btn-xs dropdown-toggle $disableButton' data-toggle='dropdown' data-toggle-second='tooltip' data-placement='top' title='Acciones'><i class=fas fa-ellipsis-v fa-lg'></i></button>
                        <ul class='dropdown-menu pull-right' role='menu'>
                            $acknowledgmentActions
                            $acknowledgmentGoodServiceActions
                            $expressAcceptanceActions
                            $claimActions
                            $consultDIAN
                        </ul>
                    </div>
                </div>";
    }

    private function getEventActionsButton($event, $documentId)
    {
        $actions = '';

        if ($event->status == self::E_NOT_CREATED || $event->status == self::E_NOT_SENT || $event->status == self::E_SEND_ERROR) {
            if ($event->statusCode == self::ACKNOWLEDGMENT_RECEIPT) {
                $actionsButtonTitle = $this->lang->line('acknowledgment_receipt');
            } else if ($event->statusCode == self::RECEPTION_GOOD_SERVICE) {
                $actionsButtonTitle = $this->lang->line('reception_good_service');
            } else if ($event->statusCode == self::EXPRESS_ACCEPTANCE) {
                $actionsButtonTitle = $this->lang->line('express_acceptance');
            } else {
                $actionsButtonTitle = $this->lang->line('claim');
            }

            $actions .= "<li><label style='padding: 3px 10px;'>".$actionsButtonTitle."</label></li>";

            if ($event->status == self::E_NOT_CREATED) {
                if ($event->statusCode == self::CLAIM) {
                    $sendEventLink = admin_url("documentsReception/claimCode/".self::CLAIM."/$documentId");
                    $actions .= "<li><a href='$sendEventLink' eventType='".$event->statusCode."' documentId='$documentId' data-toggle='modal' data-target='#myModal'> <i class='fa fa-share-square-o fa-lg'></i> ".$this->lang->line('send_event')."</a></li>";
                } else {
                    $sendEventLink = admin_url("documentsReception/sendEvent/");
                    $actions .= "<li><a href='$sendEventLink' class='sendEvent' eventType='".$event->statusCode."' documentId='$documentId'> <i class='fa fa-share-square-o fa-lg'></i> ".$this->lang->line('send_event')."</a></li>";
                }
            } else {
                $checkStatus = admin_url('documentsReception/checkStatus/'.$event->id);
                $eventForwardLink = admin_url("documentsReception/resendEvent/");
                $viewJsonLink = admin_url("documentsReception/viewJSON/");

                $actions .= "<li><a href='$eventForwardLink' class='sendEvent' eventType='".$event->statusCode."' documentId='$documentId' eventId='$event->id'> <i class='fa fa-share-square-o fa-lg'></i> ".$this->lang->line('resend')."</a></li>";
                $actions .= "<li><a href='$viewJsonLink$documentId/$event->id/' target='_blank'> <i class='fa fa-code fa-lg'></i> ".$this->lang->line('see_JSON')."</a></li>";

                if ($event->status == self::E_SEND_ERROR) {
                    if ($event->statusCodeRequest == self::HTTP_BAD_REQUEST) {
                        if (strpos($event->errorReason, 'aceptacion expresa o reclamo') !== FALSE) {
                            $actions .= "<li><a href='$checkStatus' class='checkStatus' eventType='".$event->statusCode."'> <i class='fa fa-random fa-lg'></i> ".$this->lang->line('check_status')."</a></li>";
                        }

                        if (strpos($event->errorReason, 'recibo de bien y/o prestacion del servicio') !== FALSE) {
                            $actions .= "<li><a href='$checkStatus' class='checkStatus' eventType='".$event->statusCode."'> <i class='fa fa-random fa-lg'></i> ".$this->lang->line('check_status')."</a></li>";
                        }

                        if (strpos($event->errorReason, 'tiene un estado de aceptación expresa') !== FALSE) {
                            $actions .= "<li><a href='$checkStatus' class='checkStatus' eventType='".$event->statusCode."'> <i class='fa fa-random fa-lg'></i> ".$this->lang->line('check_status')."</a></li>";
                        }

                        if (strpos($event->errorReason, 'tiene un estado de reclamo') !== FALSE) {
                            $actions .= "<li><a href='$checkStatus' class='checkStatus' eventType='".$event->statusCode."'> <i class='fa fa-random fa-lg'></i> ".$this->lang->line('check_status')."</a></li>";
                        }
                    } else if ($event->statusDian == self::HTTP_CONFLICT) {
                        if (strpos($event->warningsDian, 'Documento procesado anteriormente') !== FALSE || strpos($event->warningsDian, 'Evento registrado previamente') !== FALSE) {
                            $actions .= "<li><a href='$checkStatus' class='checkStatus' eventType='".$event->statusCode."'> <i class='fa fa-random fa-lg'></i> ".$this->lang->line('check_status')."</a></li>";
                        }
                    }
                }
            }

            $actions .= "<li class='divider'></li>";
        }

        return $actions;
    }

    private function getConsultDIANUrl($uuid)
    {
        return "<li><a href=\"https://catalogo-vpfe.dian.gov.co/Document/ShowDocumentToPublic/$uuid\" target=\"_blank\"><i class=\"fa fa-search\"></i> Consulta DIAN</a></li>";
    }

    public function settings()
    {
        $this->sma->checkPermissions('index', null, 'documentsReception');

        $this->data['settings'] = $this->settings;
        $this->data['PRODUCTION'] = self::PRODUCTION;
        $this->data['TESTING'] = self::TESTING;
        $this->page_construct('documents_reception/settings', ['page_title' => lang('settings')], $this->data);
    }

    public function saveSettings()
    {
        $this->sma->checkPermissions('index', null, 'documentsReception');

        $this->form_validation->set_rules('username', $this->lang->line('username'), 'required|valid_email|trim');
        $this->form_validation->set_rules('hostname', $this->lang->line('hostname'), 'required|trim');
        $this->form_validation->set_rules('hostUserName', $this->lang->line('hostUserName'), 'required|trim');
        $this->form_validation->set_rules('hostPassword', $this->lang->line('hostPassword'), 'required|trim');
        $this->form_validation->set_rules('hostnameDatabase', $this->lang->line('hostnameDatabase'), 'required|trim');

        if ($this->form_validation->run() == true) {
            $id = $this->input->post('id');
            $data = [
                'username' => $this->input->post('username'),
                'work_environment' => $this->input->post('work_environment'),
                'hostname' => $this->input->post('hostname'),
                'hostUserName' => $this->input->post('hostUserName'),
                'hostPassword' => $this->input->post('hostPassword'),
                'hostnameDatabase' => $this->input->post('hostnameDatabase')
            ];

            if (!empty($id)) {
                if ($this->documentReception_model->updateSettings($data, $id)) {
                    $this->session->set_flashdata('message', $this->lang->line('settings_saved_successfully'));
                    admin_redirect('documentsReception/settings');
                }
            } else {
                if ($this->documentReception_model->saveSettings($data)) {
                    $this->session->set_flashdata('message', $this->lang->line('settings_saved_successfully'));
                    admin_redirect('documentsReception/settings');
                }
            }
        }

        $this->session->set_flashdata('error', $this->lang->line('document_reception_settings_have_not_been_saved'));
        $this->session->set_flashdata('error', validation_errors());
        admin_redirect('documentsReception/settings');
    }

    public function sync()
    {
        $sendEvent = false;
        $documentsReceived = $this->documentReception_model->getDocumentsReceived($this->Settings->numero_documento, self::NOT_SYNCHRONIZED);

        if (!empty($documentsReceived)) {
            foreach ($documentsReceived as $document) {
                $this->markPurchaseAsDocumentReception($document);

                $id = $document->id;
                unset($document->id);

                $documentCreated = $this->documentReception_model->saveDocumentClient($document);
                if ($documentCreated != false) {
                    $this->documentReception_model->updateDocumentReceived(['documentStatus' => self::SYNCHRONIZED], $id);
                }
            }

            $sendEvent = true;

            $response = (object) [
                "status"  => true,
                "sendEvent" => $sendEvent,
                "message"   => $this->lang->line('processCompletedSuccessfully')
            ];
        } else {
            $response = (object) [
                "status"  => false,
                "sendEvent" => $sendEvent,
                "message"   => "No existen documento para sincronizar."
            ];
        }

        echo json_encode($response);
    }

    private function markPurchaseAsDocumentReception($document)
    {
        if ($this->Settings->documents_reception == YES) {
            $purchase = $this->documentReception_model->getPurchaseByReferenceNo($document->documentId);

            if (!empty($purchase)) {
                $this->documentReception_model->updatePurchase(['existsInDocumentsReception' => YES], $purchase->id);
            }
        }
    }

    public function sendAutoAcknowledgmentEvent()
    {
        $documentsReceived = $this->documentReception_model->getDocumentReceivedWithOutAcknowledgmentClient();
        if (!empty($documentsReceived)) {
            foreach ($documentsReceived as $document) {
                $this->sendEvent(self::ACKNOWLEDGMENT_RECEIPT, $document->id, null, false);
            }
        }

        $response = (object) [
            "status"  => true,
            "message"   => $this->lang->line('processCompletedSuccessfully')
        ];
        echo json_encode($response);
    }

    public function sendEvent($eventType, $documentId, $claimCode = NULL, $manageResponse = true, $asynchronousResponse = false)
    {
        $document = $this->documentReception_model->getDocumentReceivedByIdClient($documentId);
        $eventDocument = $this->documentReception_model->getDocumentEventClient(['documentElectronic_documentId'=>$document->documentId, 'documentElectronic_supplierNit'=>$document->supplierNit, 'documentElectronic_receiverNit'=>$document->receiverNit, 'statusCode'=>$eventType]);

        if ($this->validatePreviousExistingEvent($eventType, $document))
        {
            if ($this->calculateTimeLimit($eventType, $document)) {
                if (empty($eventDocument)) {
                    $eventIdCreated = $this->createEvent($eventType, $document, $claimCode);
                    if ($eventIdCreated) {
                        if ($eventType == self::ACKNOWLEDGMENT_RECEIPT) {
                            $this->documentReception_model->updateDocumentClient(['acknowledgment_receipt' => YES], $documentId);
                        }

                        $this->resendEvent($documentId, $eventIdCreated, $manageResponse, $asynchronousResponse);
                    }
                } else {
                    if ($eventType == self::ACKNOWLEDGMENT_RECEIPT) {
                        $this->documentReception_model->updateDocumentClient(['acknowledgment_receipt' => YES], $documentId);
                    }

                    if ($eventDocument->eventStatus != ACCEPTED) {
                        $this->resendEvent($documentId, $eventDocument->id, $manageResponse, $asynchronousResponse);
                    }
                }
            } else {
                if ($asynchronousResponse == false) {
                    $this->session->set_flashdata('error', $this->lang->line("timeExceededSendEvent") . $this->lang->line('reception_good_service'));
                    admin_redirect('documentsReception');
                }
            }
        } else {
            if ($asynchronousResponse == false) {
                $this->session->set_flashdata('error', $this->lang->line("timeExceededSendEvent") . $this->lang->line('reception_good_service'));
                admin_redirect('documentsReception');
            }
        }
    }

    private function validatePreviousExistingEvent($eventType, $document)
    {
        if ($eventType == self::RECEPTION_GOOD_SERVICE) {
            $previousEvent = $this->documentReception_model->getDocumentEventClient(['documentElectronic_documentId'=>$document->documentId, 'documentElectronic_supplierNit'=>$document->supplierNit, 'documentElectronic_receiverNit'=>$document->receiverNit], self::ACKNOWLEDGMENT_RECEIPT);
            if (empty($previousEvent)) {
                return false;
            }

            if ($previousEvent->eventStatus != ACCEPTED) {
                return false;
            }
        }

        if ($eventType == self::EXPRESS_ACCEPTANCE) {
            $previousEvent = $this->documentReception_model->getDocumentEventClient(['documentElectronic_documentId'=>$document->documentId, 'documentElectronic_supplierNit'=>$document->supplierNit, 'documentElectronic_receiverNit'=>$document->receiverNit], self::RECEPTION_GOOD_SERVICE);
            if (empty($previousEvent)) {
                return false;
            }

            if ($previousEvent->eventStatus != ACCEPTED) {
                return false;
            }
        }

        return true;
    }

    private function calculateTimeLimit($eventType, $document)
    {
        if ($eventType == self::EXPRESS_ACCEPTANCE) {
            $previousEvent = $this->documentReception_model->getDocumentEventClient(['documentElectronic_documentId'=>$document->documentId, 'documentElectronic_supplierNit'=>$document->supplierNit, 'documentElectronic_receiverNit'=>$document->receiverNit], self::RECEPTION_GOOD_SERVICE);

            $date1 = new DateTime($previousEvent->statusDate);
            $date2 = new DateTime(date('Y-m-d H:i:s'));

            $diff = $date1->diff($date2);
            $days = $diff->format('%a');
            $hours = $diff->format('%h');

            if ($days > 0) {
                $hours += $days * 24;
            }

            if ($hours >= 72) {
                return false;
            }

            return true;
        }

        return true;
    }

    private function createEvent($eventType, $document, $claimCode)
    {
        $eventDocumentType = $this->getEventDocumentType($eventType);
        $data = [
            'documentElectronic_documentId'     => $document->documentId,
            'documentElectronic_supplierNit'    => $document->supplierNit,
            'documentElectronic_receiverNit'    => $document->receiverNit,
            'statusCode'                        => $eventType,
            'statusDescription'                 => $this->getEventTypeName($eventType),
            'statusDate'                        => date('Y-m-d H:i:s'),
            'eventId'                           => $eventDocumentType->referenceNo,
            'username'                          => $this->settings->username,
            'createdAt'                         => date('Y-m-d H:i:s'),
            'claimConcept'                      => ($eventType == self::CLAIM) ? $claimCode : null,
            'document_type_id'                  => $eventDocumentType->documentTypeId,
        ];

        $eventIdSaved = $this->documentReception_model->saveDocumentEventClient($data);
        if ($eventIdSaved == false) {
            $this->session->set_flashdata('error', $this->lang->line('event_has_not_been_saved'));
            admin_redirect('documentsReception');
        }

        $this->updateConsecutive($eventDocumentType);

        return $eventIdSaved;
    }

    private function getEventDocumentType($eventType)
    {
        $billerId = $this->Settings->default_biller;

        if ($eventType == self::ACKNOWLEDGMENT_RECEIPT) {
            $document_type_module = 48;
        } else if ($eventType == self::RECEPTION_GOOD_SERVICE) {
            $document_type_module = 49;
        } else if ($eventType == self::CLAIM) {
            $document_type_module = 50;
        } else if ($eventType == self::EXPRESS_ACCEPTANCE) {
            $document_type_module = 51;
        }

        $branchDocumentType = $this->companies_model->getBillerDocumentTypes($billerId, $document_type_module)[0];
        if (empty($branchDocumentType)) {
            $this->session->set_flashdata('error', $this->lang->line('document_type_must_be_configured'));
            admin_redirect('documentsReception');
        }

        $documentTypeId = $branchDocumentType->id;
        $eventId = $this->site->getReferenceBiller($billerId, $documentTypeId);

        return (object) ['referenceNo' => $eventId, 'documentTypeId' => $documentTypeId];
    }

    private function getEventTypeName($eventType)
    {
        if ($eventType ==  self::ACKNOWLEDGMENT_RECEIPT) {
            $evenTypeName =  $this->lang->line('acknowledgment_receipt');
        } else if ($eventType ==  self::RECEPTION_GOOD_SERVICE) {
            $evenTypeName =  $this->lang->line('reception_good_service');
        } else if ($eventType ==  self::CLAIM) {
            $evenTypeName =  $this->lang->line('claim');
        } else if ($eventType ==  self::EXPRESS_ACCEPTANCE) {
            $evenTypeName =  $this->lang->line('express_acceptance');
        }

        return $evenTypeName;
    }

    private function updateConsecutive($eventDocumentType)
    {
        $consecutive = substr($eventDocumentType->referenceNo, strpos($eventDocumentType->referenceNo, "-")+1);
        $this->site->updateBillerConsecutive($eventDocumentType->documentTypeId, ($consecutive+1));
    }

    public function resendEvent($documentId, $eventId, $manageResponse = true, $asynchronousResponse = false)
    {
        $this->validationElectronicInvoice($asynchronousResponse);

        $document = $this->documentReception_model->getDocumentReceivedByIdClient($documentId);
        $event = $this->documentReception_model->getDocumentEventClientById($eventId);
        $json = $this->createJSON($document, $event);

        $response = $this->consumeWebServices($json);

        $this->manageResponse((object) $response, $event, $documentId, $manageResponse);
    }

    private function createJSON($document, $event)
    {
        $json = [
            'supplierId'        => $document->supplierNit,
            'receiverId'        => $document->receiverNit,
            'partnershipId'     => '901090070',
            'documentTypeCode'  => $document->documentTypeCode,
            'documentId'        => $document->documentId,
            'username'          => $this->settings->username,
            'eventId'           => str_replace('-', '', $event->eventId),
            'documentStatus'    => [
                'statusCode' => $event->statusCode,
                'statusDate' => strtotime(date('Y-m-d H:i:s'))
            ]
        ];

        if ($event->statusCode == self::CLAIM) {
            $json['documentStatus']['claimCode'] = $event->claimConcept;
        }

        if ($event->statusCode == self::ACKNOWLEDGMENT_RECEIPT || $event->statusCode == self::RECEPTION_GOOD_SERVICE) {
            $json['documentStatus']['id'] = $this->Settings->numero_documento;

            if ($this->Settings->tipo_documento == 31) {
                $json['documentStatus']['idDv'] = $this->Settings->digito_verificacion;
            }

            $json['documentStatus']['idType'] = $this->Settings->tipo_documento;
            $json['documentStatus']['firstName'] = $this->Settings->razon_social;
            $json['documentStatus']['familyName'] = $this->Settings->razon_social;
        }

        return json_encode($json, JSON_UNESCAPED_UNICODE);
    }

    private function validationElectronicInvoice($asynchronousResponse = false)
    {
        $hits = $this->site->getHits();
        $invoicing = $this->site->getInvoicing(ELECTRONIC_DOCUMENTS);

        if ($this->Settings->electronic_hits_validation == YES) {
            if (intval($invoicing->amount) <= 0) {
                $response = (object) [
                    "status"  => FALSE,
                    "message"   => $this->lang->line('empty_electronic_packages')
                ];
            } else if (intval($hits->amount) > intval($invoicing->amount)) {
                $response = (object) [
                    "status"  => FALSE,
                    "message"   => $this->lang->line("past_max_attempts")
                ];
            }
        }

        if (isset($response) && $response->status === FALSE) {
            if ($asynchronousResponse) {
                echo json_encode($response);
                exit();
            } else {
                $this->session->set_flashdata('error', $response->message);
                admin_redirect('documentsReception');
            }
        }
    }

    public function consumeWebServices($json)
    {
        $curl = curl_init();
        $work_environment =  ($this->settings->work_environment == self::TESTING) ? 'staging' : 'v1';

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apivp.efacturacadena.com/$work_environment/recepcion/estados",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => $json,
            CURLOPT_HTTPHEADER => array(
                'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                'content-type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);

        log_message('debug', 'RD envio evento - Response: '. $response);
        log_message('debug', 'RD envio evento - Error: '. $error);

        return json_decode($response, JSON_UNESCAPED_UNICODE);
    }

    private function manageResponse($response, $event, $documentId, $manageResponse)
    {
        if ($manageResponse) {
            if (!isset($response->statusCode)) {
                $this->session->set_flashdata('error', $response->message);
                admin_redirect('documentsReception');
            }
        }

        if ($response->statusCode == self::HTTP_OK) {
            $data = [
                'eventStatus'           => self::E_SEND_ERROR,
                'statusCodeRequest'     => $response->statusCode,
                'statusMessageRequest'  => $response->statusMessage,
                'statusDian'            => $response->statusDian,
                'statusCode'            => $response->eventCode,
                'statusDescription'     => $response->eventDescription,
                'cude'                  => $response->cude,
                'statusMessageDian'     => $response->statusMessageDian,
                'statusDescriptionDian' => $response->statusDescriptionDian,
                'warningsDian'          => $response->warningsDian,
                'errorMessage'          => NULL,
                'errorReason'           => NULL
            ];

            if ($response->statusDian == self::HTTP_OK) {
                $data['attachedDocument'] = $response->attachedDocument;
                $data['eventStatus']      =  self::E_RECEIVED;

                $this->site->saveHits($event->id, DOCUMENTS_RECEPTION, ($response->statusDescriptionDian), SUCCESS, $event->statusDate);
            } else {
                $this->site->saveHits($event->id, DOCUMENTS_RECEPTION, ($event->statusDescriptionDian . ' ' . str_replace(';', ' ', $event->warningsDian)), ERROR, $event->statusDate);
            }
        }

        if ($response->statusCode == self::HTTP_UNAUTHORIZED || $response->statusCode == self::HTTP_BAD_REQUEST) {
            $data = [
                'eventStatus'           => self::E_SEND_ERROR,
                'statusCodeRequest'     => $response->statusCode,
                'errorMessage'          => $response->errorMessage,
                'errorReason'           => $response->errorReason
            ];
            $this->site->saveHits($event->id, DOCUMENTS_RECEPTION, ($response->errorReason), ERROR, $event->statusDate);
        }

        if ($response->statusCode == self::HTTP_NOT_FOUND) {
            $data = [
                'eventStatus'           => self::E_SEND_ERROR,
                'statusCodeRequest'     => $response->statusCode,
                'errorMessage'          => $response->errorMessage,
                'statusDescription'     => $response->statusDescription
            ];
            $this->site->saveHits($event->id, DOCUMENTS_RECEPTION, ($response->errorReason), ERROR, $event->statusDate);
        }

        if ($response->statusCode == self::HTTP_INTERNAL_ERROR || $response->statusCode == self::HTTP_GATEWAY_TIMEOUT) {
            $data = [
                'eventStatus'           => self::E_SEND_ERROR,
                'statusCodeRequest'     => $response->statusCode,
                'errorMessage'          => $response->errorMessage,
                'statusDescription'     => $response->statusDescription
            ];
            $this->site->saveHits($event->id, DOCUMENTS_RECEPTION, ($response->errorReason), ERROR, $event->statusDate);
        }

        $eventSaved = $this->documentReception_model->updateEventDocumentClient($data, $event->id);
        if ($eventSaved) {
            if ($manageResponse) {
                $response = $this->getMessageEvent($event->id);
                if ($response->status == self::HTTP_OK) {
                    if ($response->statusDian == self::HTTP_OK) {
                        $this->sendEmail($event->id, $documentId, false);

                        $this->session->set_flashdata('message', $response->title.' '.$response->message);
                    } else {
                        $this->session->set_flashdata('error', $response->title.' '.$response->message);
                    }
                } else {
                    $this->session->set_flashdata('error', $response->title.' '.$response->message);
                }

                admin_redirect('documentsReception');
            }
        } else {
            if ($manageResponse) {
                $this->session->set_flashdata('error', 'No fue posible enviar el evento');
                admin_redirect('documentsReception');
            }
        }
    }

    public function claimCode($eventType, $documentId)
    {
        $this->data['evenType'] = $eventType;
        $this->data['documentId'] = $documentId;
        $this->load_view($this->theme . 'documents_reception/typeClaim', $this->data);
    }

    public function getMessageEvent($eventId = NULL)
    {
        $eventId = (!empty($eventId)) ? $eventId : $this->input->post('eventId');
        $event = $this->documentReception_model->getDocumentEventClientById($eventId);

        if ($event->statusCodeRequest == self::HTTP_OK) {
            if ($event->statusDian == self::HTTP_CONFLICT) {
                $response = [
                    'title'     => $event->statusMessageDian,
                    'message'   => $event->statusDescriptionDian . ' ' . str_replace(';', ' ', $event->warningsDian),
                    'status'    => $event->statusCodeRequest,
                    'statusDian'=> $event->statusDian
                ];
            } else {
                $response = [
                    'title'     => $event->errorMessage,
                    'message'   => $event->statusDescriptionDian,
                    'status'    => $event->statusCodeRequest,
                    'statusDian'=> $event->statusDian
                ];
            }
        }

        if ($event->statusCodeRequest == self::HTTP_BAD_REQUEST) {
            $response = [
                'title'     => $event->errorMessage,
                'message'   => $event->errorReason,
                'status'    => $event->statusCodeRequest
            ];
        }

        if ($event->statusCodeRequest == self::HTTP_NOT_FOUND) {
            $response = [
                'title'     => $event->errorMessage,
                'message'   => $event->statusDescriptionDian,
                'status'    => $event->statusCodeRequest
            ];
        }

        if ($this->input->post('eventId')) {
            echo json_encode($response);
        } else {
            return (object) $response;
        }
    }

    public function viewJSON($documentId, $eventId)
    {
        $document = $this->documentReception_model->getDocumentReceivedByIdClient($documentId);
        $event = $this->documentReception_model->getDocumentEventClientById($eventId);

        echo $this->createJSON($document, $event, NULL);
    }

    public function sendEmail($eventId, $documentId, $manageResponse = true)
    {
        $billerId = $this->Settings->default_biller;
        $event = $this->documentReception_model->getDocumentEventClientById($eventId);
        $document = $this->documentReception_model->getDocumentReceivedByIdClient($documentId);
        $biller = $this->site->getCompanyByID($billerId);

        $this->getAttached($event);

        $template = file_get_contents($this->theme_path . 'documents_reception/templates/eventTemplate.php');
        $data = [
            'eventReference'    => $event->eventId,
            'supplierName'      => $document->electronicMail,
            'receiverName'      => $this->Settings->razon_social,
            'eventCreatedDate'  => $event->createdAt,
            'eventName'         => $this->getEventTypeName($event->statusCode),
            'logo'              => base_url("assets/uploads/logos/". $biller->logo)
        ];
        $body = $this->parser->parse_string($template, $data);
        $attachment = [FCPATH. 'files/documentsReception/'.$event->eventId.'.zip'];
        $subject = 'Evento;'.$event->documentElectronic_documentId.';'.$event->documentElectronic_receiverNit.';'.$this->Settings->razon_social.';'.$event->eventId.';'.$event->statusCode;

        $emailSent = $this->sma->send_email($document->electronicMail, $subject, $body, null, null, $attachment);
        if ($manageResponse) {
            if ($emailSent) {
                $this->session->set_flashdata('message', $this->lang->line("email_sent"));
                admin_redirect("documentsReception");
            }

            $this->session->set_flashdata('error', $this->lang->line('email_not_sent'));
            admin_redirect("documentsReception");
        }
    }

    private function getAttached($event)
    {
        $xmlFile = base64_decode($event->attachedDocument);
        $xml = new DOMDocument("1.0", "ISO-8859-15");
        $xml->loadXML($xmlFile);

        $path = 'files/documentsReception/';
        if (!file_exists($path)) {
            mkdir($path, 0777);
        }

        $xml->save($path . $event->eventId.'.xml');

        if (file_exists($path . $event->eventId.'.xml')) {
            $zip = new ZipArchive();

            $zip->open($path . $event->eventId . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
            $zip->addFile($path . $event->eventId . '.xml', $event->eventId . ".xml");
            $zip->close();

            unlink($path . $event->eventId . '.xml');
        }
    }

    public function syncEvents()
    {
        $this->documentReception_model->syncEvents(self::NOT_SYNCHRONIZED);
    }

    public function add()
    {
        $this->page_construct('documents_reception/add', ['page_title' => lang('add_document')], $this->data);
    }

    public function save()
    {
        $this->form_validation->set_rules("documentId", lang("document_number"), 'trim|required');
        $this->form_validation->set_rules("uuid", lang("uuid"), 'trim|required');
        $this->form_validation->set_rules("supplierNit", lang("supplierNit"), 'trim|required');
        $this->form_validation->set_rules("receiverNit", lang("receiverNit"), 'trim|required');
        $this->form_validation->set_rules("documentDate", lang("documentDate"), 'trim|required');
        $this->form_validation->set_rules("orderReference", lang("quote_purchase"), 'trim');
        $this->form_validation->set_rules("electronicMail", lang("email_address"), 'trim|required|valid_email');

        if ($this->form_validation->run()) {
            $data = [
                'documentId' => $this->input->post("documentId"),
                'documentTypeCode' => '01',
                'uuid' => $this->input->post("uuid"),
                'supplierNit' => $this->input->post("supplierNit"),
                'receiverNit' => $this->input->post("receiverNit"),
                'partnershipId' => '901090070',
                'documentDate' => $this->input->post("documentDate"),
                'createdAt' => date('Y-m-d H:i:s'),
                'electronicMail' => $this->input->post('electronicMail')
            ];

            if ($this->input->post("orderReference")) {
                $data['orderReference'] = $this->input->post("orderReference");
            }

            if ($this->documentReception_model->saveDocumentClient($data)) {
                $this->session->set_flashdata('message', $this->lang->line('document_saved_successfully'));
            } else {
                $this->session->set_flashdata('error', $this->lang->line('document_has_not_been_saved'));
            }

            admin_redirect('documentsReception');
        } else {
            $this->session->set_flashdata('error', validation_errors());
            $this->session->set_flashdata('post', $this->input->post());
            admin_redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function checkStatus($eventId)
    {
        $event = $this->documentReception_model->getDocumentEventClientById($eventId);

        $json = json_encode([
            "receiverId" => $event->documentElectronic_receiverNit,
            "supplierId" => $event->documentElectronic_supplierNit,
            "documentId" => $event->documentElectronic_documentId,
            "documentTypeCode" => "01",
            "eventCode" => $event->statusCode,
            "partnershipId" => "901090070",
            "eventId" => str_replace('-', '', $event->eventId)
        ], JSON_UNESCAPED_UNICODE);

        $response = $this->consumeEventStatusWebServices($json);

        $this->manageEventStatusResponse((object) $response, $eventId);
    }

    private function consumeEventStatusWebServices($json)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/eventos/documento',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>$json,
            CURLOPT_HTTPHEADER => array(
                'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                'content-type: application/json'
            ),
        ));

        $response = curl_exec($curl);
        $error = curl_error($curl);
        curl_close($curl);

        log_message('debug', 'RD consulta evento: '. $response);
        log_message('debug', 'RD consulta evento: '. $error);

        return json_decode($response, JSON_UNESCAPED_UNICODE);
    }

    private function manageEventStatusResponse($response, $eventId)
    {
        if ($response->statusCode == self::HTTP_OK) {
            $data = [
                'eventStatus' => self::E_RECEIVED,
                'statusDian' => self::HTTP_OK,
                'cude' => $response->cude,
                'statusMessageDian' => $response->statusMessageDian,
                'statusDescriptionDian' => $response->statusDescriptionDian,
                'warningsDian' => $response->warningsDian,
                'attachedDocument' => $response->attachedDocument
            ];
        }

        if ($response->statusCode == self::HTTP_CONFLICT) {
            $data = [
                'statusDescription' => $response->eventDescription,
                'cude' => $response->cude,
                'statusMessageDian' => $response->statusMessageDian,
                'statusDescriptionDian' => $response->statusDescriptionDian,
                'warningsDian' => $response->warningsDian
            ];
        }

        if (!empty($data)) {
            $updatedEvent = $this->documentReception_model->updateEventDocumentClient($data, $eventId);
            if ($updatedEvent) {
                echo json_encode([
                    "status" => true,
                    "message" => $response->statusMessageDian
                ]);
            } else {
                echo json_encode([
                    "status" => false,
                    "message" => $this->lang->line('event_has_not_been_updated')
                ]);
            }
        } else {
            echo json_encode([
                "status" => false,
                "message" => $response->errorMessage
            ]);
        }
    }

    public function sendEvents()
    {
        $documentIds = json_decode($_POST["documentIds"]);
        $eventType = $_POST["eventType"];

        foreach ($documentIds as $documentId) {
            $this->sendEvent($eventType, $documentId, null, false, true);
        }

        $response = (object) [
            "status"  => TRUE,
            "message"   => $this->lang->line('processCompletedSuccessfully')
        ];
        echo json_encode($response);
    }
}