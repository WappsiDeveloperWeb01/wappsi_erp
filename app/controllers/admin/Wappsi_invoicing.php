<?php defined('BASEPATH') or exit('No direct script access allowed');
class Wappsi_invoicing extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->library('form_validation');
        $this->load->admin_model('wappsi_invoicing_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '3072';
        $this->data['logo'] = true;
    }

    public function index()
    {
        if ($this->Settings->wappsi_invoice_issuer == 1) {
            $this->sma->checkPermissions();
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('wappsi_invoicing'), 'page' => lang('wappsi_invoicing')), array('link' => '#', 'page' => lang('wappsi_invoicing')));
            $meta = array('page_title' => lang('wappsi_invoicing'), 'bc' => $bc);
            $this->page_construct('wappsi_invoicing/index', $meta, $this->data);
        } else {
            admin_redirect('wappsi_invoicing/reception');
        }
        
    }

    public function download_customer_invoicing($customer_id, $address_id = NULL) {
        $msg_arr = $this->wappsi_invoicing_model->sync_wappsi_invoicing($customer_id, $address_id);
        $this->session->set_flashdata($msg_arr['msg_type'], $msg_arr['msg_text']);
        admin_redirect('customers');
    }

    public function reception()
    {
        if ($this->Settings->wappsi_invoice_issuer == 0) {
            if ($this->Owner) {
                $this->form_validation->set_rules('add', lang("add"), 'required');
                $succesfull = false;
                if ($this->form_validation->run() == true) {
                    if ($_FILES['csv_file']['size'] > 0) {
                        $this->load->library('upload');
                        $config['upload_path'] = $this->digital_upload_path;
                        $config['allowed_types'] = 'csv';
                        $config['max_size'] = $this->allowed_file_size;
                        $config['overwrite'] = false;
                        $config['encrypt_name'] = true;
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('csv_file')) {
                            $error = $this->upload->display_errors();
                            $this->session->set_flashdata('error', $error);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $csv = $this->upload->file_name;
                        $arrResult = array();
                        $handle = fopen($this->digital_upload_path . $csv, "r");
                        if ($handle) {
                            while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                                foreach ($row as $key => $value) {
                                    $row[$key] = $this->sma->utf8Decode($value);
                                }
                                $arrResult[] = $row;
                            }
                            fclose($handle);
                        }
                        $titles = array_shift($arrResult);
                        $keys = array('tipo_mov','descripcion','cantidad_facturada','cantidad_unidad','valor_neto','valor_iva','valor_total','frecuencia','reference_no','vencimiento','estado_pago','pago_reference_no','fecha_pago', 'fecha');
                        $final = array();
                        foreach ($arrResult as $key => $value) {
                            $final[] = array_combine($keys, $value);
                        }
                        if (count($final)>0) {
                            foreach ($final as $dt_row) {
                                $this->db->insert('wappsi_invoicing', $dt_row);
                            }
                        }
                        $succesfull = true;
                    }
                }
            } else {
                admin_redirect('wappsi_invoicing/invoices');
            }
            if ($this->form_validation->run() == true && $succesfull == true) {
                $this->session->set_flashdata('message', 'Se subió correctamente el contenido del CSV');
                admin_redirect('wappsi_invoicing/reception');
            } else{
                $this->sma->checkPermissions();
                $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
                $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('wappsi_invoicing'), 'page' => lang('wappsi_invoicing')), array('link' => '#', 'page' => lang('wappsi_invoicing')));
                $meta = array('page_title' => lang('wappsi_invoicing'), 'bc' => $bc);
                $this->page_construct('wappsi_invoicing/reception', $meta, $this->data);
            }
                
        } else {
            admin_redirect('wappsi_invoicing/index');
        }
        
    }

    public function invoices()
    {
        if ($this->Settings->wappsi_invoice_issuer == 1) {
            admin_redirect('wappsi_invoicing');
        } else {
            $this->sma->checkPermissions();
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('invoices'), 'page' => lang('wappsi_invoicing')), array('link' => '#', 'page' => lang('wappsi_invoicing')));
            $meta = array('page_title' => lang('wappsi_invoicing'), 'bc' => $bc);
            $this->page_construct('wappsi_invoicing/invoices', $meta, $this->data);
        }
        
    }

    public function get_invoices(){
        $this->load->library('datatables');
        $this->datatables
            ->select("
                    id,
                    fecha,
                    reference_no,
                    tipo_mov,
                    descripcion,
                    cantidad_facturada,
                    cantidad_unidad,
                    valor_total,
                    estado_pago
                    ")
            ->from("wappsi_invoicing")
            ->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                Acciones
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li>
                                                    <a href='" . admin_url('wappsi_invoicing/view_detail_group_by_reference/$1') . "' class='tip' title='' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-eye\"></i>
                                                        ".lang("view_detail_group_by_reference")."
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                        ", "id");
        $this->datatables->unset_column('faicon');
        $this->datatables->unset_column('detailaction');
        $this->datatables->unset_column('linkaction');
        echo $this->datatables->generate();
    }

    public function view_detail($invoice_id){
        $this->data['invoice'] = $this->wappsi_invoicing_model->get_invoice_by_id($invoice_id);
        $this->data['id'] = $invoice_id;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'wappsi_invoicing/view_detail', $this->data);
    }

    public function view_detail_group_by_reference($invoice_id){
        $this->data['rows'] = $this->wappsi_invoicing_model->get_invoice_by_reference_no($invoice_id);
        $this->data['id'] = $invoice_id;
        $this->data['modal_js'] = $this->site->modal_js();
        $this->load_view($this->theme . 'wappsi_invoicing/view_detail_group_by_reference', $this->data);
    }

    public function electronic_hits(){
        $this->sma->checkPermissions();
        
        $month = false;
        $year = 'all';
        if ($this->input->post('year')) $year = $this->input->post('year');
        if ($this->input->post('month')) $month = $this->input->post('month');

        $total_consumed = $this->get_total_consumed($year, $month);
        $avg_month = $this->wappsi_invoicing_model->get_avg_month($year, $month, $total_consumed);
        $total_acq = $this->wappsi_invoicing_model->get_total_acquired();
        $total_dispo = $this->get_total_dispo($year, $total_consumed, $total_acq);
        $documents = $this->wappsi_invoicing_model->get_documents_types();
        $document_types = $this->getCountByDocuments($documents, $year, $month);

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['documents_types'] = $document_types;
        $this->data['months'] = $this->wappsi_invoicing_model->get_months();
        $this->data['years'] = $this->wappsi_invoicing_model->get_years();
        $this->data['total_consumed'] = $total_consumed ? $total_consumed : 0;
        $this->data['avg_month'] = $avg_month ? $avg_month : 0;
        $this->data['total_acq'] = $total_acq ? $total_acq->cantidad_unidad : 0;
        $this->data['total_dispo'] = $total_dispo;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('invoices'), 'page' => lang('electronic_hits')), array('link' => '#', 'page' => lang('electronic_hits')));
        $meta = array('page_title' => lang('electronic_hits'), 'bc' => $bc);
        $this->page_construct('wappsi_invoicing/electronic_hits', $meta, $this->data);
    }

    public function get_electronic_hits(){
        $this->load->library('datatables');
        $this->datatables
            ->select($this->db->dbprefix('electronic_hits').".id,
                ".$this->db->dbprefix('electronic_hits').".fecha,
                IF(
                    ".$this->db->dbprefix('electronic_hits').".tipo_registro = 1,
                    S.reference_no,
                    IF(
                        ".$this->db->dbprefix('electronic_hits').".tipo_registro = 2,
                        PE.reference_no,
                        IF(
                            ".$this->db->dbprefix('electronic_hits').".tipo_registro = 3,
                            P.reference_no,
                            IF(
                                ".$this->db->dbprefix('electronic_hits').".tipo_registro = 4,
                                DR.orderReference ,
                                NULL
                                )
                            )
                        )
                    ) AS hit_reference,
                    IF(
                    ".$this->db->dbprefix('electronic_hits').".tipo_registro = 1,
                    'Factura electrónica',
                    IF(
                        ".$this->db->dbprefix('electronic_hits').".tipo_registro = 2,
                        'Nómina electrónica',
                        IF(
                            ".$this->db->dbprefix('electronic_hits').".tipo_registro = 3,
                            'Documento Soporte',
                            IF(
                                ".$this->db->dbprefix('electronic_hits').".tipo_registro = 4,
                                'Recepción documentos' ,
                                NULL
                                )
                            )
                        )
                    ) AS hit_type,
                CONCAT(U.first_name, ' ', U.last_name) AS usuario,
                ".$this->db->dbprefix('electronic_hits').".tipo_respuesta,
                ".$this->db->dbprefix('electronic_hits').".mensaje")
            ->from("electronic_hits")
            ->join('sales S', 'S.id = '.$this->db->dbprefix('electronic_hits').'.registro_id', 'left')
            ->join('payroll_electronic_employee PE', 'PE.id = '.$this->db->dbprefix('electronic_hits').'.registro_id', 'left')
            ->join('purchases P', 'P.id = '.$this->db->dbprefix('electronic_hits').'.registro_id', 'left')
            ->join('documents_reception_events DRE', 'DRE.id = '.$this->db->dbprefix('electronic_hits').'.registro_id', 'left')
            ->join('documents_reception DR', 'DR.id = DRE.documentElectronic_documentId', 'left')
            ->join('users U', 'U.id = '.$this->db->dbprefix('electronic_hits').'.usuario', 'inner');
        echo $this->datatables->generate();
    }

    public function validate_customer_database_valid($customer_id){
        $customer = $this->site->getCompanyByID($customer_id);
        $DBGLOBAL = $this->site->get_erp_db();
        if ($DBGLOBAL) {
            $db_exists = $DBGLOBAL->not_like('wappsi_customers_databases.database', 'prueba')->not_like('wappsi_customers_databases.database', 'demo')->where(['vat_no' => $customer->vat_no])->get('wappsi_customers_databases');
            if ($db_exists->num_rows() > 0) {
                echo json_encode(['valid'=>true]);
            } else {
                echo json_encode(['valid'=>false]);
            }
        }
    }

    private function get_total_consumed($year, $month){
        # Obtener cada tipo de documento de ventas y dev de ventas por el modulo
        $totalSales = 0;
        $totalPurchases = 0;
        $totalDocuments = 0;
        $totalRecep = 0;

        ################## sales ######################
        $salesModules = [1, 2, 3, 4, 26, 27];
        $sales = $this->get_numbering_by_count('sales', $salesModules, $year, $month);
        if (is_numeric($sales)) $totalSales = $sales;
        
        ################ purchases ####################
        $purchaseModules = [35, 52];
        $purchases = $this->get_numbering_by_count('purchases', $purchaseModules, $year, $month);
        if (is_numeric($purchases)) $totalPurchases = $purchases;

        ################ recepcion ####################
        $recepModules = [48, 49, 50, 51];
        $recep = $this->get_numbering_by_count('documents_reception_events', $recepModules, $year, $month);
        if (is_numeric($recep)) $totalRecep = $recep;
        // echo $totalSales ."<br/>". $totalPurchases ."<br/>". $totalRecep;
        return $totalSales + $totalPurchases + $totalRecep;
    }

    private function get_numbering_by_range($table, $mod,  $year, $month){ //<- diferencias entre el primero y el ultimo
        $total = 0;
        $docs = $this->wappsi_invoicing_model->get_documentsTypesByModule($mod);
        if ($docs) {
            foreach ($docs as $key => $doc) { // <- documentos 
                $document = $doc->id;
                $first_register = $this->wappsi_invoicing_model->get_firts_register($table, $document, $year, $month);
                if ($first_register) {
                    $first_number = explode('-', $first_register->reference_no)[1];
                    $last_record = $this->wappsi_invoicing_model->get_last_register($table, $document, $year, $month);
                    if ($last_record) {
                        $last_number = explode('-', $last_record->reference_no)[1] + 1;
                        // echo $last_record->reference_no.' * '. $last_number .' - '. $first_number. '<br>';
                        $diff = $last_number - $first_number;
                        $total += $diff;
                    }
                }
            }
        }
        return $total;
    }

    private function get_numbering_by_count($table, $mod, $year, $month){
        $total = 0;
        $docs = $this->wappsi_invoicing_model->get_documentsTypesByModule($mod);
        if ($docs) {
            foreach ($docs as $key => $doc) {
                $document = $doc->id;
                $total += $this->wappsi_invoicing_model->get_count_document($table, $document, $year, $month);
                // echo $doc->sales_prefix. '-'. $total ."<br />";
            }
        }
        return $total;
    }

    private function get_total_dispo($year, $total_consumed, $total_acq){
        if ($year == 'all') {
            $acq = $total_acq->cantidad_unidad;
            return $acq - $total_consumed;
        }
        return false;
    }

    private function getCountByDocuments($docs, $year, $month){
        foreach ($docs as $key => $value) {
            $table = 'sales';
            $document = $value->id;
            if (in_array($value->module, [35, 52])) {
                $table = 'purchases';
            }
            if (in_array($value->module,  [48, 49, 50, 51])) {
                $table = 'documents_reception_events';
            }
            if (in_array($value->module,  [43, 44, 45])) {
                $table = 'payroll_electronic_employee';
            }
            $total = $this->wappsi_invoicing_model->get_count_document($table, $document, $year, $month);
            $value->total = $total ? $total : 0;
        }
        return $docs;
    }
}