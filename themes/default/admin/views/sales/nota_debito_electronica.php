<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,2,36);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,12,36);
        }

        $cx = 53;
        $cy = 10;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->MultiCell(100, 3.5,  $this->sma->utf8Decode(mb_strtoupper($this->Settings->nombre_comercial)), '', 'L');
        $cy  = $this->getY();
        $cy+=1.3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->Settings->numero_documento.($this->Settings->digito_verificacion != "" ? '-'.$this->Settings->digito_verificacion : '')."  ".$this->tipo_regimen),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(isset($this->Settings->url_web) ? $this->Settings->url_web : ''),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+(6));
        $this->RoundedRect(138, 10, 71, 26, 3, '1234', '');
        $cx = 140;
        $cy = 13;
        $this->setXY($cx, $cy);
        $this->MultiCell(40, 5,  $this->sma->utf8Decode(ucfirst(mb_strtolower($this->document_type ? $this->document_type->nombre : 'FACTURA DE VENTA'))), '', 'L');
        $cy +=13;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'L');
        $this->Image(base_url().'themes/default/admin/assets/images/qr_code/'.$this->factura->reference_no.'.png', 183, 12, 22);

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 38, 125, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 43, 125, 27, 3, '4', '');
        $cx = 13;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('Información del cliente'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->phone),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $altura = 5;
        $adicional_altura = 1;
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(138, 38, 71, 5, 3, '2', 'DF');
        $this->RoundedRect(138, 43, 71, 27, 3, '3', '');
        $cx = 138;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Fecha Factura'),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Total'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, $this->factura->grand_total * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Fecha Vencimiento'),'LBR',1,'C',1);
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Forma de Pago'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode(lang('credit').", ".$this->factura->payment_term ." ".($this->factura->payment_term > 1 ? "Días" : "Día")),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(71, $altura , $this->sma->utf8Decode('Vendedor'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cy += $altura;
        $this->setXY($cx, $cy);
        if ($this->seller) {
            $this->Cell(71, $altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
        } else {
            $this->Cell(71, $altura , $this->sma->utf8Decode('Ventas Varias'),'',1,'C');
        }

        $this->ln(1.7);

        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $this->Cell(9.6 , 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(17.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(49.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(11.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(21.6, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        $this->Cell(9.6 , 8, $this->sma->utf8Decode('Ítem'),'',0,'C',0);
        $this->Cell(17.6, 8, $this->sma->utf8Decode('Código'),'',0,'C',0);
        $this->Cell(49.6, 8, $this->sma->utf8Decode('Nombre producto'),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode('Dcto'),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(lang('iva')),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(11.6, 8, $this->sma->utf8Decode('Cantidad'),'',0,'C',0);
        $this->Cell(21.6, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        $this->Cell(9.6 , 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(17.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(49.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Bruto'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode('Con dcto.'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode(lang('iva_included')),'',0,'C',0);
        $this->Cell(11.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(21.6, 4, $this->sma->utf8Decode(''),'',1,'C',0);
    }

    function Footer()
    {
        // Print centered page number
        if ($this->description2 != '') {
            // $this->SetXY(15, -70);
            $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
            $this->SetXY(13, -24);
            $this->RoundedRect(13, 254, 196, 10, 0.8, '1234', '');
            $this->Cell(196, 4 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            $this->Cell(196, 4 , $this->sma->utf8Decode('CUFE: '. $this->factura->cufe),'',1,'C');
        } else {
            $this->SetXY(13, -23);
            $this->RoundedRect(13, 255, 196, 10, 1, '1234', '');
            $this->Cell(196, 4 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            $this->Cell(196, 4 , $this->sma->utf8Decode("CUFE : ".$this->factura->cufe),'',1,'C');
        }

        $this->SetXY(13, -14);
        $img_x = $this->getX()+125;
        $img_y = $this->getY()+1;
        $this->Cell(125, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS NIT 901.090.070-9     www.wappsi.com'),'',1,'L');
        $this->Image(base_url().'assets/images/cadena_logo.jpeg', $img_x, $img_y, 45);
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Página N° '.$this->PageNo()),'',1,'C');
    }

    function reduceTextToDescription1($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        $max_size = 3500;
        if (strlen($text) > $max_size) {
            $text = substr($text, 0, ($max_size - 5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 1.5;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->value_decimals = $value_decimals;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

$maximo_footer = 240;

$pdf->SetFont('Arial','',$fuente);

$taxes = [];
$total_bruto = 0;
$references = [];

    foreach ($rows as $item) {

        $total_bruto += (($item->net_unit_price + ($item->item_discount / $item->quantity)) * $item->quantity) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }

        if (isset($references[$item->id])) {
            $references[$item->id]['pr_subtotal'] += $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] += $item->quantity;
        } else {
            $references[$item->id]['pr_code'] = ($show_code == 1 ? $item->product_code : $item->reference);
            $pr_name = $pdf->sma->reduce_text_length(
                                                $item->product_name.
                                                (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                                                ($pdf->Settings->show_brand_in_product_search && !is_null($item->brand_name) ? " - ".$item->brand_name : '').
                                                (!is_null($item->serial_no) ? " ".$item->serial_no : '')
                                            , 100);
            $references[$item->id]['pr_name'] = $pr_name;
            $references[$item->id]['pr_discount'] = $item->discount;
            $references[$item->id]['pr_individual_discount'] = $item->item_discount / $item->quantity;
            $references[$item->id]['pr_tax'] = $item->tax != '' ? $item->tax : 0;
            $references[$item->id]['pr_individual_tax'] = $item->item_tax / $item->quantity;
            $references[$item->id]['unit_price_1'] = ($item->net_unit_price + ($item->item_discount / $item->quantity)) * $trmrate;
            $references[$item->id]['unit_price_2'] = $item->net_unit_price * $trmrate;
            $references[$item->id]['unit_price_3'] = $item->unit_price * $trmrate;
            $references[$item->id]['pr_subtotal'] = $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] = $item->quantity;
        }
    }

    $cnt = 1;
// for ($i=0; $i < 15 ; $i++) {
    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }
        $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $columns[0]['inicio_x'] = $pdf->getX();
        $pdf->Cell( 9.6, 5, $this->sma->utf8Decode($cnt),'',0,'C',0);
        $columns[0]['fin_x'] = $pdf->getX();
        $columns[1]['inicio_x'] = $pdf->getX();
        $pdf->Cell(17.6, 5, $this->sma->utf8Decode($data['pr_code']),'',0,'C',0);
        $columns[1]['fin_x'] = $pdf->getX();
        $columns[2]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.5);
        $pdf->MultiCell(49.6,3, $this->sma->utf8Decode($data['pr_name']),'','L');
        $columns[2]['fin_x'] = $cX+49.6;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+49.6, $cY);
        $columns[3]['inicio_x'] = $pdf->getX();
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_1'])),'',0,'C',0);
        $columns[3]['fin_x'] = $pdf->getX();
        $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_discount']),'',0,'C',0);
        $columns[4]['fin_x'] = $pdf->getX();
        $columns[5]['inicio_x'] = $pdf->getX();
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_2'])),'',0,'C',0);
        $columns[5]['fin_x'] = $pdf->getX();
        $columns[6]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_tax']),'',0,'C',0);
        $columns[6]['fin_x'] = $pdf->getX();
        $columns[7]['inicio_x'] = $pdf->getX();
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_3'])),'',0,'C',0);
        $columns[7]['fin_x'] = $pdf->getX();
        $columns[8]['inicio_x'] = $pdf->getX();
        $pdf->Cell(11.6, 5, $this->sma->utf8Decode($this->sma->formatQuantity($data['pr_quantity'], $qty_decimals)),'',0,'C',0);
        $columns[8]['fin_x'] = $pdf->getX();
        $columns[9]['inicio_x'] = $pdf->getX();
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['pr_subtotal'])),'',0,'C',0);
        $columns[9]['fin_x'] = $pdf->getX();
            $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
            foreach ($columns as $key => $data) {
                $ancho_column = $data['fin_x'] - $data['inicio_x'];
                if ($altura_fila < 5) {
                    $altura_fila = 5;
                }
                $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
            }
            $pdf->ln($altura_fila);
        $cnt++;
    }
// }

if ($inv->order_tax > 0) {
    if (!isset($taxes[$inv->order_tax_id])) {
        $taxes[$inv->order_tax_id]['tax'] = $inv->order_tax;
        $taxes[$inv->order_tax_id]['base'] = $inv->total;
    } else {
        $taxes[$inv->order_tax_id]['tax'] += $inv->order_tax;
        $taxes[$inv->order_tax_id]['base'] += $inv->total;
    }
}

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

// $pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(115.6,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->SetFont('Arial','',$fuente);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente);
$pdf->MultiCell(115.6, 3, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
$pdf->setX(13);
$pdf->setXY($current_x, $current_y+8);
$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->Cell(113.4,0, $this->sma->utf8Decode(''),'B',1,'C');
$pdf->Cell(38.53,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
$pdf->Cell(38.53,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
$pdf->Cell(38.53,5, $this->sma->utf8Decode('Valor Impuesto'),'',1,'C');
// $pdf->Cell(38.53,5, $this->sma->utf8Decode('Total'),'',1,'C');
$pdf->SetFont('Arial','',$fuente+$adicional_fuente);
$total = $inv->total;

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $total+=$arr['tax'];

    $pdf->Cell(38.53,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');

    $pdf->setXY($current_x + 38.53, $current_y);
    $pdf->setX($current_x + 38.53);
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['base'] * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (38.53 * 2), $current_y);
    $pdf->setX($current_x + (38.53 * 2));
    $pdf->Cell(38.53,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['tax'] * $trmrate)),'',1,'C');

    // $pdf->setXY($current_x + (28.9 * 3), $current_y);
    // $pdf->setX($current_x + 28.9 * 3);
    // $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total * $trmrate)),'',1,'C');
}

$total+=$inv->order_tax;

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$total+=$arr['tax'];

if ($inv->order_tax > 0) {
    $pdf->Cell(28.9,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax * $trmrate)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total * $trmrate)),'',1,'C');
}

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;
$pdf->setXY($cX_items_finished+124, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Total bruto'), 'BLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_bruto)), 'BR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Descuento'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->total_discount > 0 ? "- " : "").$this->sma->formatValue($value_decimals, ($inv->total_discount) * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total_tax * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención en la fuente'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_fuente_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');

if ($inv->rete_iva_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención al IVA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_iva_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención al ICA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_ica_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode(' '.lang('rete_other')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_other_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  TOTAL A PAGAR'), 'TBLR', 1, 'L', 1);
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate)), 'TBR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente+1.5);

$pdf->Ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->ln(1);
$pdf->cell(1,3,"",0,'L');
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell(10, 3, $this->sma->utf8Decode('Observaciones'), '', 1, 'L');
$pdf->SetFont('Arial','',$fuente+1.5);
$pdf->setX($current_x+1);
$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

$footer_note_x = $pdf->getX();
$current_x = $pdf->getX();
$current_y = $pdf->getY()+3;
$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

if ($signature_root) {
    $pdf->Image($signature_root, $current_x+35,$current_y+1,35);
}


$pdf->setXY($footer_note_x+1, $current_y+19);
$square_initial_y = $pdf->getY()-2;
$pdf->SetFont('Arial','',$pdf->fuente+0.5);
if (isset($pdf->cost_center) && $pdf->cost_center) {
    $pdf->MultiCell(195,3,$this->sma->utf8Decode($pdf->reduceTextToDescription2( "Centro de costo : ".$pdf->cost_center->name." (".$pdf->cost_center->code.") \n".$pdf->description2)), 0, 'L');
} else {
    $pdf->MultiCell(194.5,3,$this->sma->utf8Decode($pdf->reduceTextToDescription2($pdf->description2)), 0, 'L');
}
$square_end_y = $pdf->getY();
$square_height = $square_end_y - $square_initial_y;
$pdf->RoundedRect($footer_note_x, $square_initial_y, 196, $square_height+2, 3, '1234', '');

if ($internal_download == TRUE) {
  $pdf->Output("files/electronic_billing/". $filename.".pdf", "F");
} else {
  $pdf->Output("{$filename}.pdf", "I");
}