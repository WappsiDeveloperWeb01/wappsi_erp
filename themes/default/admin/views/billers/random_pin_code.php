<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?= admin_form_open('system_settings/warehouse_actions', 'id="action-form"') ?>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <?php foreach ($billers as $biller):
                            if ($biller->pin_code_method == 1) {
                                continue;
                            }
                         ?>
                        <div class="col-sm-4">
                            <label><?= $biller->company ?></label>
                            <div class="input-group">
                                <input type="text" name="random_pin_code" class="form-control random_pin_code" placeholder="<?= sprintf(lang('biller_random_pin_code'), $biller->company) ?>" data-idbiller="<?= $biller->id ?>" value="<?= $biller->random_pin_code_date ? $biller->random_pin_code : '' ?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-primary generate_pin" data-idbiller="<?= $biller->id ?>" type="button"><i class="fa fa-hashtag"></i> Generar</button>
                                    <button class="btn btn-success copy_pin" data-idbiller="<?= $biller->id ?>" type="button"><i class="fa fa-copy"></i> Copiar</button>
                                </span>
                            </div>
                        </div>
                        <?php endforeach ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

<script type="text/javascript">
    $(document).on('click', '.generate_pin', function(){
        biller_id = $(this).data('idbiller');
        random_pin_code = generate_random_pin_code(8);
        $.ajax({
            url:site.base_url+"billers/update_random_pin_code",
            type:"get",
            dataType:"JSON",
            data : {
                biller_id : biller_id,
                random_pin_code : random_pin_code,
            }
        }).done(function(data){
            if (data.response == 1) {
                $('input[data-idbiller="'+biller_id+'"]').val(random_pin_code);
            }
        });
    });

    $(document).on('click', '.copy_pin', function(){
        biller_id = $(this).data('idbiller');
        $('input[data-idbiller="'+biller_id+'"]').select();
        document.execCommand("copy");
        command: toastr.success('El código ha sido copiado en el portapapeles.', 'Copiado', {
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
        });
    });
</script>