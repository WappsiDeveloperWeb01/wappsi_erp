<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    $boolean = [NOT => lang("not"), YES => lang("yes")];
    $genders = [1=>lang("male"), 2=>lang("female")];
    $blood_types = [1=>"A+", 2=>"A-", 3=>"B+", 4=>"B-", 5=>"O+", 6=>"O-", 7=>"AB+", 8=>"AB-"];
    $relationships = [1=>"Conyuge", 2=>"Padre", 3=>"Madre", 4=>"Hijo", 5=>"Hija", 6=>"Abuelo", 7=>"Abuela", 8=>"Otros"];
    $marital_statuses = [1=>lang("free_union"), 2=>lang("married"), 3=>lang("divorced"), 4=>lang("single"), 5=>lang("widower")];
    $withholding_methods = ["0"=>lang("n/a"), WITHHOLDING_PROCESS_1=>lang("payroll_contracts_process_1"), WITHHOLDING_PROCESS_2=>lang("payroll_contracts_process_2"), HAND_CALCULATION=>lang("hand_calculation")];
    $account_types = [1=>lang("payroll_contracts_savings_account"), 2=>lang("payroll_contracts_current_account"), 3=>lang("payroll_contracts_account_virtual_wallet")];
    $name_months = ["01"=>"Enero", "02"=>"Febrero", "03"=>"Marzo", "04"=>"Abril", "05"=>"Mayo", "06"=>"Junio", "07"=>"Julio", "08"=>"Agosto", "09"=>"Septiembre", "10"=>"Octubre", "11"=>"Noviembre", "12"=>"Diciembre"];
    $educational_levels = [1=>"Básica primaria", 2=>"Básica Secundaria", 3=>"Educación Media técnica", 4=>"Técnico profesional", 5=>"Tecnología", 6=>"Profesional Universitario", 7=>"Especialización", 8=>"Maestría", 9=>"Doctorado", 10=>"Postdoctorado"];
?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel">Contrato de <?= $employee->name && $employee->name != '-' ? $employee->name : $employee->name; ?></h4>
        </div>
        <div class="modal-body">
            <form id="see_contract_form">
                <h1><?= lang("contract") ?></h1>
                <section style="min-height: 317px !important;">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <dl class="dl-horizontal">
                                <dt><?= lang("contract_type") ?>: </dt>
                                <dd class="text-left">
                                    <?php
                                        $contract_type_name = "";
                                        foreach ($types_contracts as $type_contract) {
                                            if ($type_contract->id == $employee->contract_type) {
                                                $contract_type_name = $type_contract->description;
                                            }
                                        }
                                        echo $contract_type_name;
                                    ?>
                                </dd>
                                <br>
                                <dt><?= lang("internal_code") ?>: </dt>
                                <dd class="text-left"><?= $employee->internal_code; ?></dd>
                                <br>
                                <dt><?= lang("creation_date") ?>: </dt>
                                <dd class="text-left"><?= $employee->creation_date; ?></dd>
                                <br>
                                <dt><?= lang("contracts_start_date") ?>: </dt>
                                <dd class="text-left"><?= $employee->start_date; ?></dd>
                                <br>
                                <?php if (!empty($employee->end_date) && $employee->end_date != "0000-00-00") { ?>
                                    <dt><?= lang("contracts_end_date") ?>: </dt>
                                    <dd class="text-left"><?= $employee->end_date; ?></dd>
                                    <br>
                                <?php } ?>
                                <?php if (!empty($employee->settlement_date)  && $employee->settlement_date != "0000-00-00") { ?>
                                    <dt><?= lang("contracts_settlement_date") ?>: </dt>
                                    <dd class="text-left"><?= $employee->settlement_date; ?></dd>
                                    <br>
                                <?php } ?>
                                <dt><?= lang("branch") ?>: </dt>
                                <dd class="text-left"><?= $employee->branch_name; ?></dd>
                                <br>
                                <dt><?= lang("area") ?>: </dt>
                                <dd class="text-left"><?= $employee->area_name; ?></dd>
                                <br>
                                <dt><?= lang("professional_position") ?>: </dt>
                                <dd class="text-left"><?= $employee->professional_position_name; ?></dd>
                                <br>
                                <dt><?= lang("contracts_salary") ?>: </dt>
                                <dd class="text-left"><?= $employee->base_amount; ?></dd>
                                <br>
                                <dt><?= lang("withholding_method") ?>: </dt>
                                <dd class="text-left"><?= $withholding_methods[$employee->withholding_method]; ?></dd>
                                <br>
                                <?php if ($employee->withholding_method != NOT) { ?>
                                    <dt><?= lang("withholding_percentage") ?>: </dt>
                                    <dd class="text-left"><?= $employee->withholding_percentage; ?></dd>
                                    <br>
                                <?php } ?>

                                <dt><?= lang("payment_method") ?>: </dt>
                                <dd class="text-left">
                                    <?php
                                        if (!empty($payment_means)) {
                                            foreach ($payment_means as $payment_mean) {
                                                if ($payment_mean->code == $employee->payment_method) {
                                                    $contract_payment_method = $payment_mean->name;
                                                }
                                            }
                                            echo $contract_payment_method;
                                        }
                                    ?>
                                </dd>
                                <br>
                                <?php if (!empty($employee->bank_name)) { ?>
                                    <dt><?= lang("bank") ?>: </dt>
                                    <dd class="text-left"><?=$employee->bank_name; ?></dd>
                                    <br>
                                <?php } ?>
                                <?php if (!empty($employee->account_type)) { ?>
                                    <dt><?= lang("account_type") ?>: </dt>
                                    <dd class="text-left"><?= !empty($employee->account_type) ? $account_types[$employee->account_type] : 'No aplica'; ?></dd>
                                    <br>
                                <?php } ?>
                                <?php if (!empty($employee->account_no)) { ?>
                                    <dt><?= lang("account_no") ?>: </dt>
                                    <dd class="text-left"><?= $employee->account_no; ?></dd>
                                <?php } ?>
                            </dl>
                        </div>
                    </div>
                    <?php if (!empty($concepts_contract)) { ?>
                        <hr>
                    <?php } ?>
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1">
                            <?php if (!empty($concepts_contract)) { ?>
                                <h3><?= lang("payroll_concepts") ?></h3>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Concepto</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($concepts_contract as $concept_contract) { ?>
                                            <tr>
                                                <td><?= $concept_contract->name ?></td>
                                                <td><?= $concept_contract->amount ?></td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>
                    </div>
                </section>

                <h1><?= lang("social_security"); ?></h1>
                <section style="min-height: 317px !important;">
                    <div class="row">
                        <div class="col-sm-9 col-sm-offset-3">
                            <dl class="dl-horizontal">
                                <dt><?= lang("employee_type") ?>: </dt>
                                <dd class="text-left"><?= $employee->description_types_employees; ?></dd>
                                <br>
                                <dt><?= lang("afp") ?>: </dt>
                                <dd class="text-left"><?= $employee->afp_name; ?></dd>
                                <br>
                                <dt><?= lang("eps") ?>: </dt>
                                <dd class="text-left"><?= $employee->eps_name; ?></dd>
                                <br>
                                <dt><?= lang("cesantia") ?>: </dt>
                                <dd class="text-left"><?= $employee->cesantia_name; ?></dd>
                                <br>
                                <dt><?= lang("arl") ?>: </dt>
                                <dd class="text-left"><?= $employee->arl_name; ?></dd>
                                <br>
                                <dt><?= lang("arl_risk_classes") ?>: </dt>
                                <dd class="text-left"><?= $employee->arl_risk_classes_name ." (". $employee->arl_risk_classes_description .")"; ?></dd>
                                <br>
                                <dt><?= lang("caja") ?>: </dt>
                                <dd class="text-left"><?= $employee->caja_name; ?></dd>
                                <br>
                                <dt><?= lang("icbf") ?>: </dt>
                                <dd class="text-left"><?= $boolean[$employee->icbf_id]; ?></dd>
                                <br>
                                <dt><?= lang("sena") ?>: </dt>
                                <dd class="text-left"><?= $boolean[$employee->sena_id]; ?></dd>
                            </dl>
                        </div>
                    </div>
                </section>
            </form>
        </div>
    </div>
</div>

<script>
    $("#see_contract_form").show().steps({
        headerTag: "h1",
        bodyTag: "section",
        titleTemplate: "#title#",
        transitionEffect: "fade",
        enableAllSteps: true,
        enablePagination: false
    });
</script>