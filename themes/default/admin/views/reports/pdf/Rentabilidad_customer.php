<?php
/*
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'vendor/fpdf/fpdf.php';
*/

require_once APPPATH.'../vendor/fpdf/fpdf.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {     
        $this->SetFont('Arial','B',12);
        $this->Image($this->Logo,18,8,30,0);
        $this->Cell(70);        
        $this->Cell(80,4,'Informe de Rentabilidad por Cliente',0,0,'C');
        $this->Ln();
        $this->Cell(70); 
        $this->SetFont('Arial','',6);
        $this->Cell(80,4,'Fecha : '.date('d-m-Y h:i A'),0,0,'C');
        $this->Ln(4);
        $this->SetFont('Arial','B',$this->fuente+1);

        $this->Cell(20,6, 'Fecha','TB',0,'L');
        $this->Cell(20,6, 'Documento','TB',0,'L');
        $this->Cell(20,6, 'Afecta','TB',0,'L');
        $this->Cell(45,6, 'Cliente','TB',0,'L');
        $this->Cell(25,6, 'Total','TB',0,'L');
        $this->Cell(25,6, 'Costo','TB',0,'L');
        $this->Cell(25,6, 'Utilidad','TB',0,'L');      
        $this->Cell(0,6, 'Margen','TB',0,'L');      
        $this->Ln();
    }

    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica '.'            Usuario: '.$this->User),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

}

$fuente = 6;
$adicional_fuente = 2;

$height = 4;

$pdf = new PDF('P', 'mm', 'Legal');
//$pdf->Logo = base_url('assets/uploads/logos/'.$Settings->logo);
$pdf->Logo = 'https://static.wixstatic.com/media/a560e4_0525ed9cae7e4d42bd1ac3c8a7851bca~mv2.png';

$pdf->AliasNbPages();

$pdf->User = $this->session->userdata('username');
$pdf->fuente = 6;

$pdf->AddPage();

$pdf->SetFont('Arial','',$fuente);
$customer_old =  $referencia = '' ; 
$total_costo = $total_total = 0;
foreach ($profitabilitys as $profitability)
{
    if ($profitability['customer_id'] != $customer_old)
    {
        if($customer_old != '')
        {
            if ($total_total > 0) {
                $utilidad =  $total_total-$total_costo;
                $margen = ((($total_total-$total_costo)/$total_total)*100);
            } else {
                $utilidad =  0;
                $margen = 0;
            }
            $pdf->Cell(20,$height, '' ,'T',0,'C');
            $pdf->Cell(85,$height,'Total Cliente:  '.substr($this->sma->utf8Decode($customer),0,55),'T',0,'L');
            $pdf->Cell(25,$height, '$ '.number_format($total_total,2,',','.') ,'T',0,'R');
            $pdf->Cell(25,$height, '$ '.number_format($total_costo,2,',','.') ,'T',0,'R');
            $pdf->Cell(25,$height, '$ '.number_format($utilidad,2,',','.') ,'T',0,'R');
            $pdf->Cell(0,$height, number_format($margen,2,',','.').' %' ,'T',0,'R');
            $pdf->Ln();
            $total_costo = $total_total = 0;
        }
        $pdf->SetFont('Arial','B',$fuente+2);
        $pdf->Cell(0,$height+2, 'Cliente: '.$this->sma->utf8Decode($profitability['customer']." Nit : ".$profitability['vat_no']) ,0,0,'L');
        $pdf->Ln();
        $pdf->SetFont('Arial','',$fuente);
        $customer_old = $profitability['customer_id']; 
        $customer = $profitability['customer'];
    }

    $fecha = substr($profitability['date'],0,10);

    $utilidad = number_format($profitability['utilidad'],2,',','.');
    $avg_net_unit_cost = number_format($profitability['costo'],2,',','.'); 
    $margen = number_format($profitability['margen'],2,',',''); 
    $total = number_format($profitability['total'],2,',','.');

    $total_total +=  $profitability['total']; 
    $total_costo +=  $profitability['costo']; 
    
    $referencia = $profitability['reference_no'];
	$afec = $profitability['return_sale_ref'];

    $pdf->Cell(20,$height, $fecha ,0,0,'C');
    $pdf->Cell(20,$height, substr($referencia,0,12) ,0,0,'L');
    $pdf->Cell(20,$height, substr($afec,0,12) ,0,0,'L');
    $pdf->Cell(45,$height, substr($this->sma->utf8Decode($profitability['customer']),0,40),0,0,'L');
    $pdf->Cell(25,$height, '$ '.$total ,0,0,'R');
    $pdf->Cell(25,$height, '$ '.$avg_net_unit_cost ,0,0,'R');
    $pdf->Cell(25,$height, '$ '.$utilidad ,0,0,'R');
    $pdf->Cell(0,$height, $margen.' %' ,0,0,'R');
    $pdf->Ln();

}

$utilidad =  $total_total-$total_costo;
$margen = ((($total_total-$total_costo)/$total_total)*100) ;
$pdf->Cell(20,$height, '' ,'T',0,'C');
$pdf->Cell(85,$height,'Total Cliente:  '.substr($this->sma->utf8Decode($customer),0,55),'T',0,'L');
$pdf->Cell(25,$height, '$ '.number_format($total_total,2,',','.') ,'T',0,'R');
$pdf->Cell(25,$height, '$ '.number_format($total_costo,2,',','.') ,'T',0,'R');
$pdf->Cell(25,$height, '$ '.number_format($utilidad,2,',','.') ,'T',0,'R');
$pdf->Cell(0,$height, number_format($margen,2,',','.').' %' ,'T',0,'R');
$pdf->Ln();
$total_costo = $total_total = 0;



$pdf->Output();
?>