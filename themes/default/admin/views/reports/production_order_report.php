<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
?>
<script type="text/javascript">
    $(document).ready(function () {

        <?php if ($this->session->userdata('portfolio_nothing_found')) {
          $this->sma->unset_data('portfolio_nothing_found');
          ?>
          setTimeout(function() {
            window.close();
          }, 800);
        <?php } ?>

        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('production_order_report'); ?> <?php
            if ($this->input->post('start_date')) {
                echo "From " . $this->input->post('start_date') . " to " . $this->input->post('end_date');
            }
            ?></h2>
    </div>
</div>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <!-- <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a> -->
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/production_order_report", "id='production_order_report' type='POST' target='_blank'"); ?>
                                <input type="hidden" name="generate_report" value="1">
                                <div class="row">
                                </div>
                                <!-- <div class="row">
                                    <div class="form-group col-md-4">
                                      <label><?= lang('cut_off_date') ?></label>
                                      <input type="date" name="end_date" id="end_date" class="form-control" value="<?= date('Y-m-d') ?>">
                                    </div>
                                </div> -->
                                <div class="form-group">
                                    <div class="controls">
                                      <button class="btn btn-primary" type="button" id="submit_report"><?= lang('send') ?></button>
                                      <button class="btn btn-danger" type="button" onclick="location.href='<?= admin_url("reports/portfolio") ?>';"><?= lang('cancel') ?></button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#production_order_report").validate({
            ignore: []
        });
    });
    $(document).on('click', '#submit_report', function(){
      if ($("#production_order_report").valid()) {
        $("#production_order_report").submit();
      }
    });
</script>