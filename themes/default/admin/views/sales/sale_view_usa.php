<?php

/***********

FACTURACIÓN POR CONTINGENCIA SIN DATOS DE EMPRESA Y CON LOGO, NO SE DISCRIMINA IVA

************/
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
  {

    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Orden de remisión' ));


        //izquierda
        $this->RoundedRect(13, 7, 117, 29, 3, '1234', '');
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,60,8,20);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,18,16,50);
        }

        if ($this->show_biller) {
            $cx = 72;
            $cy = 13;
            $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
            $this->setXY($cx, $cy);
            $this->Cell(72, 5 , $this->sma->utf8Decode(ucfirst($this->biller->company != '-' ? $this->biller->company : $this->biller->name)),'',1,'L');
            $cy+=2;
            $this->setXY($cx, $cy);
            $this->SetFont('Arial','',$this->fuente+1.5);
            if ($this->Settings->great_contributor || $this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
                $cy+=3;
                $this->setXY($cx, $cy);
                $this->Cell(72, 3 , $this->sma->utf8Decode(
                                                    ($this->Settings->great_contributor ? 'Gran contribuyente '.
                                                        ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? ' - Auto retenedor de ' : '')
                                                        : '').
                                                    ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                    ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                    ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                                ),'',1,'L');
            }
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->phone),'',1,'L');
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->email),'',1,'L');
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode($this->Settings->url_web),'',1,'L');
        }
        
        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 29, 3, '1234', '');
        $cx = 134;
        $cy = 13;
        $this->setXY($cx, $cy);
        // $this->Cell(77, 5 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre.$this->document_type->nombre.$this->document_type->nombre : 'Orden de remisión'),'',1,'C');
        $this->MultiCell(77,3.5,$this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Orden de remisión'), 0, 'C');
        $cy +=4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*4));
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'C');

        //izquierda
        $cx = $this->getX();
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 38, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 43, 118, 31, 3, '4', '');
        $cx = 20.5;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(110, 5 , $this->sma->utf8Decode(ucfirst(lang('customer_information'))),'B',1,'C');
        $cx -= 7.4;
        $cy += 5.5;
        $this->setXY($cx, $cy);
        $this->Cell(118, 6.8 , $this->sma->utf8Decode(ucfirst($this->customer->name != '-' ? $this->customer->name : $this->customer->company)),'B',1,'C');
        $cy += 8;
        $this->SetFont('Arial','B',$this->fuente);
        $this->setXY($cx, $cy);
        $inicio_Y_info = $this->getY();
        $mitad_X = 59;
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode(ucfirst(lang('bill_to'))),'',1,'L');
        $cy += 3;
        $this->SetFont('Arial','',$this->fuente);
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode($this->customer->company),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $inicio_Y = $this->getY();
        $this->MultiCell($mitad_X,3,$this->sma->utf8Decode($this->customer->address), 0, 'L');
        $fin_Y = $this->getY();
        $cy += $fin_Y - $inicio_Y;
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode(ucwords(mb_strtolower($this->customer->city.", ".$this->customer->state))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode($this->customer->phone),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');
        //derecha
        $cy = $inicio_Y_info;
        $cx = $mitad_X + 13.5;
        $this->SetFont('Arial','B',$this->fuente);
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode(ucfirst(lang('ship_to'))),'',1,'L');
        $cy += 3;
        $this->SetFont('Arial','',$this->fuente);
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $inicio_Y = $this->getY();
        $this->MultiCell($mitad_X,3,$this->sma->utf8Decode($this->factura->direccion_negocio), 0, 'L');
        $fin_Y = $this->getY();
        $cy += $fin_Y - $inicio_Y;
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode(ucwords(mb_strtolower($this->factura->ciudad_negocio.", ".$this->factura->state_negocio))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell($mitad_X, 3 , $this->sma->utf8Decode($this->factura->email_negocio),'',1,'L');



        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 38, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 43, 78, 31, 3, '3', '');
        $cx = 131;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode(ucfirst(lang('date'))),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode(ucfirst(lang('total'))),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, ($this->factura->grand_total  - $this->factura->rete_fuente_total - $this->factura->rete_iva_total - $this->factura->rete_ica_total - $this->factura->rete_other_total - ($this->factura->rete_bomberil_id && $this->factura->rete_bomberil_total > 0 ? $this->factura->rete_bomberil_total : 0) - ($this->factura->rete_autoaviso_id && $this->factura->rete_autoaviso_total > 0 ? $this->factura->rete_autoaviso_total : 0)) * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode(ucfirst(lang('expiration'))),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode(ucfirst(lang('payment_method'))),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->payment_term ." ".($this->factura->payment_term > 1 ? "Días" : "Día")),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode(ucfirst(lang('seller'))),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cy += $altura+1;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name).($this->seller->phone != '' ? ", ".$this->seller->phone : ''))),'',1,'C');

        $this->ln();


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->Cell(22,5, $this->sma->utf8Decode(ucfirst(lang('code'))),'TBLR',0,'C',1);
        $this->Cell(79,5, $this->sma->utf8Decode(ucfirst(lang('product_description'))),'TBLR',0,'C',1);
        $this->Cell(13,5, $this->sma->utf8Decode(ucfirst(lang('quan.'))),'TBLR',0,'C',1);
        $this->Cell(12,5, $this->sma->utf8Decode(ucfirst(lang('unit'))),'TBLR',0,'C',1);
        $this->Cell(31.2,5, $this->sma->utf8Decode(ucfirst(lang('vr_unit'))),'TBLR',0,'C',1);
        $this->Cell(39.2,5, $this->sma->utf8Decode(ucfirst(lang('vr_total'))),'TBLR',1,'C',1);

    }

    function Footer()
    {
        // Print centered page number
        $this->RoundedRect(13, 248, 196, 17.5, 3, '1234', '');
        $this->SetXY(14, -29);
        // $this->Cell(196, 5 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
        if (isset($this->cost_center) && $this->cost_center) {
            $this->MultiCell(194,3,$this->sma->utf8Decode($this->reduceTextToDescription2( "Centro de costo : ".$this->cost_center->name." (".$this->cost_center->code.")".$this->description2)), 0, 'L');
        } else {
            $this->MultiCell(194,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        }
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode(lang('printing_software_wappsi_info')),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode(lang('page').' '.$this->PageNo()),'',1,'C');

    }

    function reduceTextToDescription1($text){
        $text= ucfirst(lang('notes')). " : ". $text;
        if (strlen($text) > 390) {
            $text = substr($text, 0, 385);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        if (strlen($text) > 750) {
            $text = substr($text, 0, 745);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

}


$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);


$number_convert = new number_convert();


$fuente = 8;
$adicional_fuente = 2;

$color_R = 200;
$color_G = 200;
$color_B = 200;

$pdf->setFillColor($color_R,$color_G,$color_B);

$pdf->biller = $biller;

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->biller_logo = $biller_logo;
$pdf->value_decimals = $value_decimals;
$pdf->show_biller = $document_type_invoice_format->show_biller;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}
$pdf->tipo_regimen = $tipo_regimen;

$pdf->AddPage();

$maximo_footer = 235;

// exit(var_dump($rows));

$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);
// for ($i=0; $i < 6; $i++) {

$taxes = [];

    foreach ($rows as $item) {

        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }

        $pdf->Cell(22, 5, $this->sma->utf8Decode($show_code == 1 ? $item->product_code : $item->reference),'BLR',0,'C');
        $pr_name = $this->sma->reduce_text_length(
                                                    ($item->under_cost_authorized == 1 ? '     ' : '').
                                                    $item->product_name.
                                                    (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                                                    ($this->Settings->show_brand_in_product_search ? " - ".$item->brand_name : '').
                                                    (!is_null($item->serial_no) ? " - ".$item->serial_no : '').
                                                    strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')))
                                                , 60);
        if ($item->under_cost_authorized == 1) {
            $unlock_icon_X = $pdf->getX();
            $unlock_icon_Y = $pdf->getY();
            $pdf->Image(base_url().'assets/images/unlock-icon.png',$unlock_icon_X+1, $unlock_icon_Y+1,2.6);
        }
        $pdf->Cell(79, 5, $this->sma->utf8Decode( $pr_name ),'BR',0,'L');

        $pr_quantity = $item->quantity;
        $pr_unit_code = NULL;

        if (isset($item->operator)) {
            if ($item->operator == "*") {
                $pr_quantity = $item->quantity / $item->operation_value;
            } else if ($item->operator == "/") {
                $pr_quantity = $item->quantity * $item->operation_value;
            } else if ($item->operator == "+") {
                $pr_quantity = $item->quantity - $item->operation_value;
            } else if ($item->operator == "-") {
                $pr_quantity = $item->quantity + $item->operation_value;
            }
        }
        $pr_unit_code = $item->product_unit_code;

        $pdf->Cell(13,5, $this->sma->utf8Decode($this->sma->formatQuantity($pr_quantity, $qty_decimals)),'BR',0,'C');
        $pdf->Cell(12,5, $this->sma->utf8Decode($pr_unit_code),'BR',0,'C');

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }

        if ($view_tax) {
            $pdf->Cell(31.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->unit_price * $trmrate)),'BR',0,'R');
            $pdf->Cell(39.2,5, $this->sma->utf8Decode(($item->item_discount > 0 ? "(-".$this->sma->formatValue($value_decimals, $item->item_discount * $item->quantity * $trmrate).")  " : "").$this->sma->formatValue($value_decimals, $item->unit_price * $item->quantity * $trmrate)),'BR',1,'R');
        } else {
            $pdf->Cell(31.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->net_unit_price * $trmrate)),'BR',0,'R');
            $pdf->Cell(39.2,5, $this->sma->utf8Decode(($item->item_discount > 0 ? "(-".$this->sma->formatValue($value_decimals, $item->item_discount * $item->quantity * $trmrate).")  " : "").$this->sma->formatValue($value_decimals, $item->net_unit_price * $item->quantity * $trmrate)),'BR',1,'R');
        }

    }

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 14, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(115.6,5, ($this->session->userdata['language'] != 'english') ? $this->sma->utf8Decode(ucfirst(lang('letter_value'))): '','',1,'L');

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente-1.2);
if ($this->session->userdata['language'] != 'english'){
    $pdf->MultiCell(115.6, 3, $this->sma->utf8Decode($number_convert->convertir(
        (($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate), 
        (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency),
        NULL,
        NULL,
        (($document_type_invoice_format->language == 'english') ? 'EN' : 'ES')
    )), 0, 'L');
}
$pdf->SetFont('Arial','',$fuente);
$pdf->setX(13);

$pdf->setXY($current_x, $current_y+8);

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

//derecha

$pdf->setXY($cX_items_finished+118, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('gross_total'))), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->total * $trmrate))), 'TBR', 1, 'R');


$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('total_tax'))), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total_tax * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('discount'))), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_discount * $trmrate)), 'TBR', 1, 'R');

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode('  Serv. Transporte'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Total'), 'TBLR', 1, 'R', 1);
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate)), 'TBRL', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente);

$pdf->Ln(5);

//FIRMAS

//izquierda
$pdf->ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;

$pdf->MultiCell(196,3, '   '.$this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode(lang('accept_signature_and_stamp_received')), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(lang('accept_document_type')), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode(lang('date')), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode(ucfirst(lang('accept_delivered'))), '', 1, 'L');

// $pdf->Image(( isset($this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : '' ) ,$current_x+35,$current_y+1,35);

// $descargar = false;
if ($inv->sale_status == 'pending') {
    $pdf->setXY(90, 90);
    $pdf->SetFont('Arial','B',50);
    $pdf->SetTextColor(125,125,125);
    if ($this->session->userdata['language'] != 'english') {
        $pdf->RotatedImage(base_url().'assets/images/not_approved.png',20,220,240,18,45);
    }else{
        $pdf->RotatedImage(base_url().'assets/images/not_approved_5.png',20,220,240,18,45);
    }
    $pdf->SetFont('Arial','',$fuente);
    $pdf->SetTextColor(0,0,0);
}

if ($for_email === true) {
    $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($download) {
      $pdf->Output($inv->reference_no.".pdf", "D");
    } else {
      $pdf->Output($inv->reference_no.".pdf", "I");
    }    
}
