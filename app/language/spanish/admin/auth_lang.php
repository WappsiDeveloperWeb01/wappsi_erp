<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 *
 */

$lang['add_user']                                              = "Agregar usuario";
$lang['notify_user_by_email']                                  = "Notificar al usuario por correo";
$lang['group']                                                 = "Grupo";
$lang['edit_user']                                             = "Editar usuario";
$lang['delete_user']                                           = "Eliminar usuario";
$lang['user_added']                                            = "Usuario agregado correctamente";
$lang['user_updated']                                          = "Usuario actualizado correctamente";
$lang['users_deleted']                                         = "Usuarios eliminados correctamente";
$lang['alert_x_user']                                          = "Está a punto de eliminar éste usuario permanentemente. Oprima OK para proceder o cancelar para regresar";
$lang['login_email']                                           = "Correo para inicio de sesión";
$lang['edit_profile']                                          = "Editar perfil";
$lang['website']                                               = "Website";
$lang['if_you_need_to_rest_password_for_user']                 = "Si necesita reiniciar la contraseña para este usuario";
$lang['user_options']                                          = "Opciones de usuario";
$lang['old_password']                                          = "Contraseña anterior";
$lang['new_password']                                          = "Nueva Contraseña";
$lang['change_avatar']                                         = "Cambiar imagen";
$lang['update_avatar']                                         = "Actualizar imagen";
$lang['avatar']                                                = "Imagen";
$lang['avatar_deleted']                                        = "Imagen actualizado correctamente";
$lang['captcha_wrong']                                         = "Captcha no es correcta o expiró. Por favor intente nuevamente";
$lang['captcha']                                               = "Captcha";
$lang['site_is_offline_plz_try_later']                         = "El Sitio no está en línea. Por favor visitenos en unos días.";
$lang['type_captcha']                                          = "Escriba el Captcha";
$lang['we_are_sorry_as_this_sction_is_still_under_development']= "Lo sentimos mucho esta sección está en construcción estamos esforzandonos para terminarlo lo mas pronto posible. Gracias enormes por su paciencia.";
$lang['confirm']                                               = "Confirmación";
$lang['error_csrf']                                            = "Se ha detectado una falsificación en la Solicitud de sitios cruzados o el token CSRF ha caducado. Por favor, vuelva a intentarlo.";
$lang['avatar_updated']                                        = "Avatar actualizado correctamente";
$lang['registration_is_disabled']                              = "El registro de cuentas está cerrado.";
$lang['login_to_your_account']                                 = "Ingrese a su cuenta";
$lang['pw']                                                    = "Contraseña";
$lang['remember_me']                                           = "Mantener sesión abierta";
$lang['forgot_your_password']                                  = "¿Olvidó su contraseña?";
$lang['dont_worry']                                            = "¡No hay que preocuparse!";
$lang['click_here']                                            = "haga clic aquí";
$lang['to_rest']                                               = "para restablecer";
$lang['forgot_password']                                       = "Olvidó la Contraseña";
$lang['login_successful']                                      = "Se inicia la sesión con éxito.";
$lang['back']                                                  = "Atrás";
$lang['dont_have_account']                                     = "¿No tiene cuenta?";
$lang['no_worry']                                              = "¡No hay que preocuparse!";
$lang['to_register']                                           = "para registrarse";
$lang['register_account_heading']                              = "Por favor complete el siguiente formulario para registrarse";
$lang['register_now']                                          = "Regístrese Ahora";
$lang['no_user_selected']                                      = "No hay ningún usuario seleccionado. Por favor, seleccione al menos un usuario";
$lang['delete_users']                                          = "Eliminar Usuarios";
$lang['delete_avatar']                                         = "Eliminar imagen";
$lang['deactivate_heading']                                    = "¿Está seguro de desactivar el usuario?";
$lang['deactivate']                                            = "Desactivar";
$lang['pasword_hint']                                          = "Al menos 1 mayúscula, 1 minúscula, 1 número y más de 8 caracteres de longitud";
$lang['pw_not_same']                                           = "La contraseña y la confirmación de la contraseña no son iguales";
$lang['reset_password']                                        = "Restablecer Contraseña";
$lang['reset_password_link_alt']                               = "Puede pegar el código de abajo en su url si el enlace anterior no funciona";
$lang['email_forgotten_password_subject']                      = "Detalles para cambiar la contraseña";
$lang['reset_password_email']                                  = "Restablecer contraseña %s";
$lang['back_to_login']                                         = "Volver a identificarse";
$lang['forgot_password_unsuccessful']                          = "Falla al restablecer contraseña";
$lang['forgot_password_successful']                            = "restablecimiento de contraseña éxitoso, por favor utilice la nueva contraseña para iniciar sesión";
$lang['password_change_unsuccessful']                          = "El cambio de contraseña falló";
$lang['password_change_successful']                            = "Cambio de contraseña correcto";
$lang['forgot_password_email_not_found']                       = "La dirección de correo electrónico introducida no pertenece a ninguna cuenta.";
$lang['login_unsuccessful']                                    = "Error de acceso, por favor intente de nuevo";
$lang['email_forgot_password_link']                            = "Enlace para restablecer contraseña";
$lang['reset_password_heading']                                = "Restablecer contraseña";
$lang['reset_password_new_password_label']                     = "Nueva contraseña";
$lang['reset_password_new_password_confirm_label']             = "Confirmar nueva contraseña";
$lang['register']                                              = "Regístrese";
$lang['reset_password_submit_btn']                             = "Restablecer contraseña";
$lang['error_csrf']                                            = 'Este proceso no superó el control de seguridad.';
$lang['account_creation_successful']                           = "Cuenta creada correctamente";
$lang['old_password_wrong']                                    = "La contraseña anterior no coincide";
$lang['sending_email_failed']                                  = "Por favor, escriba la contraseña antigua correctamente";
$lang['deactivate_successful']                                 = "No se puede enviar correo electrónico, por favor verifique la configuración del sistema.";
$lang['activate_successful']                                   = "Usuario activado correctamente";
$lang['login_timeout']                                         = "Se ha bloqueado su usuario por demasiados intentos fallados, por favor espere 10 minutos y reintente";
$lang['login_subheading']									   = "Ingreso a Wappsi";

//NO TRADUCIDAS

$lang['email_new_password_subject'] = 'La contraseña ha sido reseteada';
$lang['view_right'] = 'Permiso de ver';
$lang['edit_right'] = 'Permiso de editar';
$lang['all_records'] = 'Todos los registros';
$lang['own_records'] = 'Sus propios registros';
$lang['allow_discount'] = 'Permitir descuento';
$lang['new_user_created'] = 'Nuevo usuario creado';
$lang['type_email_to_reset'] = 'Por favor confirme cuenta de correo para instrucciones de reseteo';
$lang['account_exists'] = 'Ya existe una cuenta, por favor revise la cuenta o resetee su contraseña';

$lang['login_unsuccessful_not_active'] = 'El usuario con el que intenta loguear no está activo';
$lang['login_unsuccessful_already_logged'] = 'El usuario que intenta ingresar ya tiene sesión activa, ¿Desea cerrar la sesión que está abierta?';
$lang['login_unsuccessful_already_logged_yes'] = 'Si, cerrar sesion';
$lang['login'] = 'Iniciar sesión';
$lang['forgotten_password_recover'] = '¿Olvidó su contraseña?';