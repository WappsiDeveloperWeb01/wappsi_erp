<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 0"><i class="fa fa-lg">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('Agregar Presupuesto'); ?></h4>
        </div>
        <div class="modal-body">
            <?= admin_form_open('budget/save', ['id'=>'budgetForm']) ?>
                <input type="hidden" name="year" value="<?= $year ?>">
                <input type="hidden" name="month" value="<?= $month ?>">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="biller_id"><?= $this->lang->line('biller') ?></label>
                            <?= $billerId = ''; ?>
                            <select class="form-control select" name="biller_id" id="biller_id" <?= ($this->Settings->detailed_budget_by_seller == YES) ? 'required' : ''; ?>>
                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                <?php if (!empty($billers)) : ?>
                                    <?php foreach ($billers as $biller) : ?>
                                        <option value="<?= $biller->id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($biller->name)) ?></option>
                                    <?php endforeach ?>
                                <?php endif ?>
                            </select>
                        </div>
                    </div>

                    <?php if ($this->Settings->detailed_budget_by_seller == YES) : ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="seller"><?= $this->lang->line('seller') ?></label>
                                <?= $sellerId = ''; ?>
                                <select class="form-control select" name="seller_id" id="seller_id" required>
                                    <option value=""><?= $this->lang->line('select') ?></option>
                                </select>
                            </div>
                        </div>
                    <?php endif ?>

                    <?php if ($this->Settings->detailed_budget_by_seller == YES && $this->Settings->detailed_budget_by_category == YES) : ?>
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="category_id"><?= $this->lang->line('category') ?></label>
                                <?= $categoryId = ''; ?>
                                <select class="form-control select" name="category_id" id="category_id" required>
                                    <option value=""><?= $this->lang->line('select') ?></option>
                                    <?php if (!empty($categories)) : ?>
                                        <?php foreach ($categories as $category) : ?>
                                            <option value="<?= $category->id ?>" <?= ($category->id == $categoryId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($category->name)) ?></option>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>
                        </div>
                    <?php endif ?>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="amount"><?= $this->lang->line('value') ?></label>
                            <input class="form-control number_mask" name="amount" id="amount" min="0" placeholder="0" required>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="units"><?= $this->lang->line('units') ?></label>
                            <input class="form-control number_mask" name="units" id="units" min="0" placeholder="0">
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="new-button-container text-right">
                            <button class="btn btn-primary new-button" id="saveBudgetButton" type="button" data-toggle="tooltip" data-placement="top" title="Guardar"><i class="fas fa-check fa-lg"></i></button>
                            <button class="btn btn-default new-button" type="button" data-toggle="tooltip" data-placement="top" title="Cancelar" data-dismiss="modal"><i class="fas fa-times fa-lg"></i></button>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#biller_id').select2({placeholder: 'Seleccionar'});
        $('#seller_id').select2({placeholder: 'Seleccionar'});
        $('#category_id').select2({placeholder: 'Seleccionar'});
        $('[data-toggle="tooltip"]').tooltip();
        set_number_mask()
    });
</script>