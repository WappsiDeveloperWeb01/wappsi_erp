<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if(!empty($slider)) { ?>
<!-- <section class="slider-container">
    <div class="container-fluid">
        <div class="row">
            <div id="carousel-example-generic" class="carousel slide">
                <ol class="carousel-indicators margin-bottom-sm">
                    <?php
                    $sr = 0;
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<li data-target="#carousel-example-generic" data-slide-to="'.$sr.'" class="'.($sr == 0 ? 'active' : '').'"></li> ';
                            $sr++;
                        }
                    }
                    ?>
                </ol>

                <div class="carousel-inner" role="listbox">
                    <?php
                    $sr = 0;
                    // exit(var_dump($slider));
                    foreach ($slider as $slide) {
                        if (!empty($slide->image)) {
                            echo '<div class="carousel-item item'.($sr == 0 ? ' active' : '').'">';
                            if (!empty($slide->link)) {
                                echo '<a href="'.$slide->link.'">';
                            }
                            echo '<img src="'.base_url('assets/uploads/'.$slide->image).'" alt="">';
                            if (!empty($slide->caption)) {
                                echo '<div class="carousel-caption">'.$slide->caption.'</div>';
                            }
                            if (!empty($slide->link)) {
                                echo '</a>';
                            }
                            echo '</div>';
                            $sr++;
                        }
                    }
                    ?>
                </div>

                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="fa fa-chevron-left" aria-hidden="true"></span>
                    <span class="sr-only"><?= lang('prev'); ?></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="fa fa-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only"><?= lang('next'); ?></span>
                </a>
            </div>
        </div>
    </div>
</section> -->
<?php } ?>
<section class="page-contents">
    <div class="container">
        <div class="row"> 
            <div class="col-sm-12">
                <p >
                    <span class="greetings_name">
                        <?= sprintf(lang('shop_welcome_index'), (isset($loggedInUser->first_name) ? $loggedInUser->first_name : "")); ?>
                    </span>
                    <span class="greetings_message" style="font-size: 140%;padding-top: 1%;">
                        <?= lang('shop_welcome_index_question') ?>
                    </span>
                </p>
            </div>

            <div class="col-sm-12 row">
                <section class="slider-container">
                    <div class="container-fluid">
                        <div class="row">
                            <div id="carousel-example-generic" class="carousel slide">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="banners_images">
                                            <?php 
                                            $sr = 0;
                                            foreach ($slider as $slide) {
                                                if (!empty($slide->image)) {
                                                    echo '<img src="'.base_url('assets/uploads/'.$slide->image).'" class="img_banner '.($sr == 0 ? "first_banner_img" : "" ).'" alt="">';
                                                    $sr++;
                                                }
                                            }
                                             ?>
                                             <a class="left carousel-control" id="left_banner">
                                                <span class="fa fa-chevron-left" aria-hidden="true"></span>
                                                <span class="sr-only"><?= lang('prev'); ?></span>
                                            </a>
                                            <a class="right carousel-control" id="right_banner">
                                                <span class="fa fa-chevron-right" aria-hidden="true"></span>
                                                <span class="sr-only"><?= lang('next'); ?></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </div>
</section>

<script type="text/javascript">
$('#left_banner').click(function() {
  event.preventDefault();
  $('.banners_images').animate({
    scrollLeft: "-=625px"
  }, "slow");
});

$('#right_banner').click(function() {
  event.preventDefault();
  $('.banners_images').animate({
   scrollLeft: "+=625px"
  }, "slow");
});

$('.banners_images').scroll(function() {
    diff = ($(this)[0].scrollWidth - $(this).innerWidth()) - $(this).scrollLeft();
  if (diff <= 1) {
    setTimeout(function() {
        $('.banners_images').animate({
            scrollLeft: "0px"
        }, "slow");
    }, 850);
  }
});
$(document).ready(function() {
    var seconds_transition = "<?= $this->shop_settings->banners_transition_seconds * 1000 ?>"
    setInterval(function () {
        $('.banners_images').animate({
            scrollLeft: "+=625px"
        }, "slow");
    }, seconds_transition);
        setTimeout(function() {
            $('select').not('.skip').select2();
            // arreglar_tamaño_divs('product');
            $('.img-responsive').each(function(){
                if($(this)[0].naturalHeight == 0){
                     $(this).attr('src',site.base_url+"/assets/uploads/no_image.png");
                }
            });
            set_same_height('product-desc-promo');
            set_same_height('product-desc-featured');
            set_same_height('product-desc-best');
            set_same_height('product-price-promo');
            set_same_height('product-price-featured');
            set_same_height('product-price-best');
            
            $.each($('.product-image'), function(index, element){
                var scroll_pi_index = $('.product-image').index($(element));
                var scroll_img = $('.img-responsive').eq(scroll_pi_index);
                var scroll_img_width = parseFloat(scroll_img.width()) * 0.35;
                $(element).scrollLeft(scroll_img_width);
            });

        }, 550);
});
</script>
<section class="page-contents">
    <div class="container">


        <?php if ($this->shop_settings->show_categories_buttons == 1): ?>
            <div class="row">
                <div class="col-sm-12">
                    <p class="text-bold fp_title">
                        <span>
                            <?= lang('our_categories') ?>
                        </span>
                    </p>
                </div>
                <div class='col-sm-12'>
                    <div class="row cat_container">
                        <?php
                        $num = 1;
                        foreach($categories as $category) {
                            // if ($num == 1) {
                            //     echo "<div class='row cat_container'>";
                            // }
                            echo '<div '.'data-href="'.site_url('category/'.$category->slug).'" class="col-sm-1 category_link_square btn"><img class="category_img_square" src='.site_url('assets/uploads/'.(is_file("assets/uploads/".$category->image) ? $category->image : 'no_image.png')).'><br><p class="category_name">'.ucfirst(mb_strtolower($category->name)).'</p></div>';
                            
                            // if ($num%10 == 0) {
                            //     echo "</div>";
                            //     echo "<div class='row cat_container'>";
                            // }
                            $num++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <?php if ($this->shop_settings->show_brands_buttons == 1): ?>
            <div class="row">
                <div class="col-sm-12">
                    <p class="text-bold fp_title">
                        <span>
                            <?= lang('our_brands') ?>
                        </span>
                    </p>
                </div>
                <div class='row cat_container'>
                    <?php
                    $num = 1;
                    foreach($brands as $brand) {
                        // if ($num == 1) {
                        //     echo "<div class='row cat_container'>";
                        // }
                        echo '<div '.'data-href="'.site_url('brand/'.$brand->slug).'" class="col-sm-1 category_link_square btn"><img class="category_img_square" src='.site_url('assets/uploads/'.(is_file("assets/uploads/".$brand->image) ? $brand->image : 'no_image.png')).'><br><p class="category_name">'.ucfirst(mb_strtolower($brand->name)).'</p></div>';
                        
                        // if ($num%10 == 0) {
                        //     echo "</div>";
                        //     echo "<div class='row cat_container'>";
                        // }
                        $num++;
                    }
                    ?>
                </div>
            </div>
        <?php endif ?>

        <script type="text/javascript">
            
            $(document).on('click', '.category_link_square', function(){
                href = $(this).data('href');
                location.href = href;
            });

        </script>

        <script type="text/javascript">
            
            $(document).on('click', '.category_link_square', function(){
                href = $(this).data('href');
                location.href = href;
            });

        </script>

        <?php if ($this->shop_settings->show_promotion_products == 1 && count($promo_products) > 0): ?>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                    <p class="text-bold fp_title">
                        <?= lang('promo_products'); ?>
                        <span class="view_all" data-href="promo"><span class="no-mobile"><?= lang('view_all') ?></span> <i class="fa-solid fa-chevron-right"></i></span>
                    </p>
                </div>
                <?php
                if (count($promo_products) > 8) {
                    ?>
                    <div class="col-xs-3">
                        <div class="controls pull-right hidden-xs">
                            <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-promo_products"
                            data-slide="prev"></a>
                            <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-promo_products"
                            data-slide="next"></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="row">
                <div id="carousel-promo_products" class="carousel slide col-sm-12" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $r = 0;
                        foreach (array_chunk($promo_products, 6) as $fps) {
                            ?>
                            <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                <div class="featured-products-new">
                                    <?php
                                    foreach ($fps as $fp) {
                                        ?>
                                        <div class="product-container col-lg-2 col-md-2 col-sm-3 col-xs-6 ">
                                            <div class="product alt ">
                                                <?php
                                                if ($fp->promotion) {
                                                    ?>
                                                    <span class="badge promo_badge"><?= lang('promo'); ?></span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-top">
                                                    <div class="product-image">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <img class="img-responsive" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product-desc product-desc-promo">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <h2 class="product-name"><?= ucfirst(mb_strtolower($fp->name)); ?></h2>
                                                        </a>
                                                        <p><?= $this->sma->reduce_text_length(strip_tags($fp->details), 140); ?></p>
                                                        <?php if ($fp->variants && $this->shop_settings->use_product_variants == 1): ?>
                                                            <select class="variant-select form-control" style="height:46px;">
                                                                <?php foreach ($fp->variants as $variant): ?>
                                                                    <option value="<?= $variant->id ?>"><?= $variant->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                        <?php if ($fp->preferences && $this->shop_settings->use_product_preferences == 1): ?>
                                                            <div class='div_preferences' data-pid='<?= $fp->id ?>'>
                                                                Seleccione preferencias
                                                            </div>
                                                            <select class="preference-select form-control select skip" multiple placeholder="Seleccione preferencias" style="display:none;" data-pid="<?= $fp->id ?>">
                                                                <?php foreach ($fp->preferences as $preference): ?>
                                                                    <option value="<?= $preference->id ?>"><?= $preference->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="product-bottom">
                                                    <div class="row">
                                                        <?php if ($fp->units): ?>
                                                            <div class="col-md-6">
                                                                <select class="form-control unit-select" data-id="<?= $fp->id; ?>" title="" tabindex="-1">
                                                                    <?php foreach ($fp->units as $unit): ?>
                                                                        <option value="<?= $unit->product_unit_id ?>" data-unprice="<?= $unit->valor_unitario ?>"><?= $unit->code?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        <?php endif ?>
                                                        <div class="<?= $fp->units ? 'col-md-6' : 'col-md-12' ?>">
                                                            <div class="form-group product-rating " style="margin-bottom:0;">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                                                    <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required" data-id="<?= $fp->id; ?>">
                                                                    <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="product-price product-price-promo" style="height: 64px;">
                                                                <?php if ($fp->promotion): ?>
                                                                    <span class="prev_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                    <span class="promo_price"><?= $this->sma->convertMoney($fp->promo_price) ?></span>
                                                                <?php else: ?>
                                                                    <span class="product_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="product-cart-button">
                                                        <div class="row">
                                                            <button class="btn_wshlst btn btn-default add-to-wishlist wishlist_inactive" data-id="<?= $fp->id; ?>">
                                                                <span class="span_wshlst fa fa-heart-o"></span> 
                                                            </button>
                                                            <div class="col-xs-12 col-md-12 div_btn_add_to_cart">
                                                                <button class="btn btn-theme add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> Agregar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            $r++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php endif ?>

        <?php if ($this->shop_settings->show_featured_products == 1 && count($featured_products) > 0): ?>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                    <p class="text-bold fp_title">
                        <?= lang('featured_products'); ?>
                        <span class="view_all" data-href="featured"><span class="no-mobile"><?= lang('view_all') ?></span> <i class="fa-solid fa-chevron-right"></i></span>
                    </p>
                    
                </div>
                <?php
                if (count($featured_products) > 8) {
                    ?>
                    <div class="col-xs-3">
                        <div class="controls pull-right hidden-xs">
                            <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-featured_products"
                            data-slide="prev"></a>
                            <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-featured_products"
                            data-slide="next"></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="row">
                <div id="carousel-featured_products" class="carousel slide col-sm-12" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $r = 0;
                        foreach (array_chunk($featured_products, 6) as $fps) {
                            ?>
                            <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                <div class="featured-products-new">
                                    <?php
                                    foreach ($fps as $fp) {
                                        ?>
                                        <div class="product-container col-lg-2 col-md-2 col-sm-3 col-xs-6 ">
                                            <div class="product alt ">
                                                <?php
                                                if ($fp->promotion) {
                                                    ?>
                                                    <span class="badge promo_badge"><?= lang('promo'); ?></span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-top">
                                                    <div class="product-image">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <img class="img-responsive" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product-desc product-desc-featured">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <h2 class="product-name"><?= ucfirst(mb_strtolower($fp->name)); ?></h2>
                                                        </a>
                                                        <p><?= $this->sma->reduce_text_length(strip_tags($fp->details), 140); ?></p>
                                                        <?php if ($fp->variants && $this->shop_settings->use_product_variants == 1): ?>
                                                            <select class="variant-select form-control" style="height:46px;">
                                                                <?php foreach ($fp->variants as $variant): ?>
                                                                    <option value="<?= $variant->id ?>"><?= $variant->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                        <?php if ($fp->preferences && $this->shop_settings->use_product_preferences == 1): ?>
                                                            <div class='div_preferences' data-pid='<?= $fp->id ?>'>
                                                                Seleccione preferencias
                                                            </div>
                                                            <select class="preference-select form-control select skip" multiple placeholder="Seleccione preferencias" style="display:none;" data-pid="<?= $fp->id ?>">
                                                                <?php foreach ($fp->preferences as $preference): ?>
                                                                    <option value="<?= $preference->id ?>"><?= $preference->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="product-bottom">
                                                    <div class="row">
                                                        <?php if ($fp->units): ?>
                                                            <div class="col-md-6">
                                                                <select class="form-control unit-select" data-id="<?= $fp->id; ?>" title="" tabindex="-1">
                                                                    <?php foreach ($fp->units as $unit): ?>
                                                                        <option value="<?= $unit->product_unit_id ?>" data-unprice="<?= $unit->valor_unitario ?>"><?= $unit->code?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        <?php endif ?>
                                                        <div class="<?= $fp->units ? 'col-md-6' : 'col-md-12' ?>">
                                                            <div class="form-group product-rating " style="margin-bottom:0;">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                                                    <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required" data-id="<?= $fp->id; ?>">
                                                                    <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="product-price product-price-featured" style="height: 64px;">
                                                                <?php if ($fp->promotion): ?>
                                                                    <span class="prev_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                    <span class="promo_price"><?= $this->sma->convertMoney($fp->promo_price) ?></span>
                                                                <?php else: ?>
                                                                    <span class="product_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="product-cart-button">
                                                        <div class="row">
                                                            <button class="btn_wshlst btn btn-default add-to-wishlist wishlist_inactive" data-id="<?= $fp->id; ?>">
                                                                <span class="span_wshlst fa fa-heart-o"></span> 
                                                            </button>
                                                            <div class="col-xs-12 col-md-12 div_btn_add_to_cart">
                                                                <button class="btn btn-theme add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> Agregar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            $r++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php endif ?>
            
        <?php if ($this->shop_settings->show_most_selled_products == 1 && count($best_selling_products) > 0): ?>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                    <p class="text-bold fp_title">
                        <?= lang('best_selling_products'); ?>
                        <span class="view_all" data-href="best_selling"><span class="no-mobile"><?= lang('view_all') ?></span> <i class="fa-solid fa-chevron-right"></i></span>
                    </p>
                </div>
                <?php
                if (count($best_selling_products) > 8) {
                    ?>
                    <div class="col-xs-3">
                        <div class="controls pull-right hidden-xs">
                            <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-best_selling"
                            data-slide="prev"  role="button"></a>
                            <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-best_selling"
                            data-slide="next"  role="button"></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="row">
                <div id="carousel-best_selling" class="carousel slide col-sm-12" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $r = 0;
                        foreach (array_chunk($best_selling_products, 6) as $fps) {
                            ?>
                            <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                <div class="featured-products-new">
                                    <?php
                                    foreach ($fps as $fp) {
                                        ?>
                                        <div class="product-container col-lg-2 col-md-2 col-sm-3 col-xs-6 ">
                                            <div class="product alt ">
                                                <?php
                                                if ($fp->promotion) {
                                                    ?>
                                                    <span class="badge promo_badge"><?= lang('promo'); ?></span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-top">
                                                    <div class="product-image">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <img class="img-responsive" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product-desc product-desc-best">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <h2 class="product-name"><?= ucfirst(mb_strtolower($fp->name)); ?></h2>
                                                        </a>
                                                        <p><?= $this->sma->reduce_text_length(strip_tags($fp->details), 140); ?></p>
                                                        <?php if ($fp->variants && $this->shop_settings->use_product_variants == 1): ?>
                                                            <select class="variant-select form-control" style="height:46px;">
                                                                <?php foreach ($fp->variants as $variant): ?>
                                                                    <option value="<?= $variant->id ?>"><?= $variant->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                        <?php if ($fp->preferences && $this->shop_settings->use_product_preferences == 1): ?>
                                                            <div class='div_preferences' data-pid='<?= $fp->id ?>'>
                                                                Seleccione preferencias
                                                            </div>
                                                            <select class="preference-select form-control select skip" multiple placeholder="Seleccione preferencias" style="display:none;" data-pid="<?= $fp->id ?>">
                                                                <?php foreach ($fp->preferences as $preference): ?>
                                                                    <option value="<?= $preference->id ?>"><?= $preference->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="product-bottom">
                                                    <div class="row">
                                                        <?php if ($fp->units): ?>
                                                            <div class="col-md-6">
                                                                <select class="form-control unit-select" data-id="<?= $fp->id; ?>" title="" tabindex="-1">
                                                                    <?php foreach ($fp->units as $unit): ?>
                                                                        <option value="<?= $unit->product_unit_id ?>" data-unprice="<?= $unit->valor_unitario ?>"><?= $unit->code?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        <?php endif ?>
                                                        <div class="<?= $fp->units ? 'col-md-6' : 'col-md-12' ?>">
                                                            <div class="form-group product-rating " style="margin-bottom:0;">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                                                    <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required" data-id="<?= $fp->id; ?>">
                                                                    <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="product-price product-price-best" style="height: 64px;">
                                                                <?php if ($fp->promotion): ?>
                                                                    <span class="prev_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                    <span class="promo_price"><?= $this->sma->convertMoney($fp->promo_price) ?></span>
                                                                <?php else: ?>
                                                                    <span class="product_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="product-cart-button">
                                                        <div class="row">
                                                            <button class="btn_wshlst btn btn-default add-to-wishlist wishlist_inactive" data-id="<?= $fp->id; ?>">
                                                                <span class="span_wshlst fa fa-heart-o"></span> 
                                                            </button>
                                                            <div class="col-xs-12 col-md-12 div_btn_add_to_cart">
                                                                <button class="btn btn-theme add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> Agregar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            $r++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php endif ?>

        <?php if ($this->shop_settings->show_new_products == 1 && count($new_products) > 0): ?>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                    <p class="text-bold fp_title">
                        <?= lang('new_products'); ?>
                        <span class="view_all" data-href="new_products"><span class="no-mobile"><?= lang('view_all') ?></span> <i class="fa-solid fa-chevron-right"></i></span>
                    </p>
                </div>
                <?php
                if (count($new_products) > 8) {
                    ?>
                    <div class="col-xs-3">
                        <div class="controls pull-right hidden-xs">
                            <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-best_selling"
                            data-slide="prev"  role="button"></a>
                            <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-best_selling"
                            data-slide="next"  role="button"></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="row">
                <div id="carousel-best_selling" class="carousel slide col-sm-12" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $r = 0;
                        foreach (array_chunk($new_products, 6) as $fps) {
                            ?>
                            <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                <div class="featured-products-new">
                                    <?php
                                    foreach ($fps as $fp) {
                                        ?>
                                        <div class="product-container col-lg-2 col-md-2 col-sm-3 col-xs-6 ">
                                            <div class="product alt ">
                                                <?php
                                                if ($fp->promotion) {
                                                    ?>
                                                    <span class="badge promo_badge"><?= lang('promo'); ?></span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-top">
                                                    <div class="product-image">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <img class="img-responsive" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product-desc product-desc-best">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <h2 class="product-name"><?= ucfirst(mb_strtolower($fp->name)); ?></h2>
                                                        </a>
                                                        <p><?= $this->sma->reduce_text_length(strip_tags($fp->details), 140); ?></p>
                                                        <?php if ($fp->variants && $this->shop_settings->use_product_variants == 1): ?>
                                                            <select class="variant-select form-control" style="height:46px;">
                                                                <?php foreach ($fp->variants as $variant): ?>
                                                                    <option value="<?= $variant->id ?>"><?= $variant->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                        <?php if ($fp->preferences && $this->shop_settings->use_product_preferences == 1): ?>
                                                            <div class='div_preferences' data-pid='<?= $fp->id ?>'>
                                                                Seleccione preferencias
                                                            </div>
                                                            <select class="preference-select form-control select skip" multiple placeholder="Seleccione preferencias" style="display:none;" data-pid="<?= $fp->id ?>">
                                                                <?php foreach ($fp->preferences as $preference): ?>
                                                                    <option value="<?= $preference->id ?>"><?= $preference->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="product-bottom">
                                                    <div class="row">
                                                        <?php if ($fp->units): ?>
                                                            <div class="col-md-6">
                                                                <select class="form-control unit-select" data-id="<?= $fp->id; ?>" title="" tabindex="-1">
                                                                    <?php foreach ($fp->units as $unit): ?>
                                                                        <option value="<?= $unit->product_unit_id ?>" data-unprice="<?= $unit->valor_unitario ?>"><?= $unit->code?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        <?php endif ?>
                                                        <div class="<?= $fp->units ? 'col-md-6' : 'col-md-12' ?>">
                                                            <div class="form-group product-rating " style="margin-bottom:0;">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                                                    <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required" data-id="<?= $fp->id; ?>">
                                                                    <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="product-price product-price-best" style="height: 64px;">
                                                                <?php if ($fp->promotion): ?>
                                                                    <span class="prev_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                    <span class="promo_price"><?= $this->sma->convertMoney($fp->promo_price) ?></span>
                                                                <?php else: ?>
                                                                    <span class="product_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="product-cart-button">
                                                        <div class="row">
                                                            <button class="btn_wshlst btn btn-default add-to-wishlist wishlist_inactive" data-id="<?= $fp->id; ?>">
                                                                <span class="span_wshlst fa fa-heart-o"></span> 
                                                            </button>
                                                            <div class="col-xs-12 col-md-12 div_btn_add_to_cart">
                                                                <button class="btn btn-theme add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> Agregar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            $r++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php endif ?>
        <?php if ($this->shop_settings->show_favorite_products == 1 && $wl_products &&count($wl_products) > 0): ?>
            <div class="row">
                <div class="col-lg-9 col-md-9 col-xs-12 col-sm-12">
                    <p class="text-bold fp_title">
                        <?= lang('my_favorites'); ?>
                        <span class="view_all" data-href="favorites"><span class="no-mobile"><?= lang('view_all') ?></span> <i class="fa-solid fa-chevron-right"></i></span>
                    </p>
                </div>
                <?php
                if (count($wl_products) > 8) {
                    ?>
                    <div class="col-xs-3">
                        <div class="controls pull-right hidden-xs">
                            <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-best_selling"
                            data-slide="prev"  role="button"></a>
                            <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-best_selling"
                            data-slide="next"  role="button"></a>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
            <div class="row">
                <div id="carousel-best_selling" class="carousel slide col-sm-12" data-ride="carousel">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner">
                        <?php
                        $r = 0;
                        foreach (array_chunk($wl_products, 6) as $fps) {
                            ?>
                            <div class="item row <?= empty($r) ? 'active' : ''; ?>">
                                <div class="featured-products-new">
                                    <?php
                                    foreach ($fps as $fp) {
                                        ?>
                                        <div class="product-container col-lg-2 col-md-2 col-sm-3 col-xs-6 ">
                                            <div class="product alt ">
                                                <?php
                                                if ($fp->promotion) {
                                                    ?>
                                                    <span class="badge promo_badge"><?= lang('promo'); ?></span>
                                                    <?php
                                                }
                                                ?>
                                                <div class="product-top">
                                                    <div class="product-image">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <img class="img-responsive" src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="product-desc product-desc-best">
                                                        <a href="<?= site_url('product/'.$fp->slug); ?>">
                                                            <h2 class="product-name"><?= ucfirst(mb_strtolower($fp->name)); ?></h2>
                                                        </a>
                                                        <p><?= $this->sma->reduce_text_length(strip_tags($fp->details), 140); ?></p>
                                                        <?php if ($fp->variants && $this->shop_settings->use_product_variants == 1): ?>
                                                            <select class="variant-select form-control" style="height:46px;">
                                                                <?php foreach ($fp->variants as $variant): ?>
                                                                    <option value="<?= $variant->id ?>"><?= $variant->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                        <?php if ($fp->preferences && $this->shop_settings->use_product_preferences == 1): ?>
                                                            <div class='div_preferences' data-pid='<?= $fp->id ?>'>
                                                                Seleccione preferencias
                                                            </div>
                                                            <select class="preference-select form-control select skip" multiple placeholder="Seleccione preferencias" style="display:none;" data-pid="<?= $fp->id ?>">
                                                                <?php foreach ($fp->preferences as $preference): ?>
                                                                    <option value="<?= $preference->id ?>"><?= $preference->name ?></option>
                                                                <?php endforeach ?>
                                                            </select>
                                                        <?php endif ?>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="product-bottom">
                                                    <div class="row">
                                                        <?php if ($fp->units): ?>
                                                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                                                <select class="form-control unit-select" data-id="<?= $fp->id; ?>" title="" tabindex="-1">
                                                                    <?php foreach ($fp->units as $unit): ?>
                                                                        <option value="<?= $unit->product_unit_id ?>" data-unprice="<?= $unit->valor_unitario ?>"><?= $unit->code?></option>
                                                                    <?php endforeach ?>
                                                                </select>
                                                            </div>
                                                        <?php endif ?>
                                                        <div class="<?= $fp->units ? 'col-lg-6 col-md-6 col-sm-12 col-xs-12' : 'col-lg-12 col-md-12 col-sm-12 col-xs-12' ?>">
                                                            <div class="form-group product-rating " style="margin-bottom:0;">
                                                                <div class="input-group">
                                                                    <span class="input-group-addon pointer btn-minus"><span class="fa fa-minus"></span></span>
                                                                    <input type="text" name="quantity" class="form-control text-center quantity-input" value="1" required="required" data-id="<?= $fp->id; ?>">
                                                                    <span class="input-group-addon pointer btn-plus"><span class="fa fa-plus"></span></span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="product-price product-price-best" style="height: 64px;">
                                                                <?php if ($fp->promotion): ?>
                                                                    <span class="prev_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                    <span class="promo_price"><?= $this->sma->convertMoney($fp->promo_price) ?></span>
                                                                <?php else: ?>
                                                                    <span class="product_price"><?= $this->sma->convertMoney(isset($fp->special_price) && !empty(isset($fp->special_price)) ? $fp->special_price : $fp->price) ?></span>
                                                                <?php endif ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                    <div class="product-cart-button">
                                                        <div class="row">
                                                            <!-- <button class="btn_wshlst btn btn-default add-to-wishlist wishlist_inactive" data-id="<?= $fp->id; ?>">
                                                                <span class="span_wshlst fa fa-heart-o"></span> 
                                                            </button> -->
                                                            <div class="col-xs-12 col-md-12 div_btn_add_to_cart">
                                                                <button class="btn btn-theme add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart padding-right-md"></i> Agregar</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                            $r++;
                        }
                        ?>
                    </div>
                </div>
            </div>
        <?php endif ?>


    </div>
</section>

<script type="text/javascript">
    
</script>