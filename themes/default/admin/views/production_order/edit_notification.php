<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-md no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <div class="row">
                <div class="col-md-12 text-center" style="margin-bottom:20px;">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
                </div>
                <div class="col-md-12 text-center" style="margin-bottom:20px;">
                    <h3><?= lang('making_order') ?> N° <?= $inv->reference_no; ?></h3>
                </div>
                <div class="col-md-12">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("estimated_date"); ?>: <?= $this->sma->hrld($inv->est_date); ?><br>
                        <?= lang("responsable"); ?>: <?= $inv->responsable; ?><br>
                    </p>
                </div>
                <?php
                $attrib = array('id' => 'form_notification');
                echo admin_form_open_multipart("production_order/edit_notification/".$register_id."/".$notif_data->id, $attrib);
                ?>
                    <div class="col-md-12">
                        <hr>
                                <label><?= lang('description') ?></label>
                                <textarea name="description" class="form-control" placeholder="Detalle aquí el mensaje de la notificación" required><?= $notif_data->detail ?></textarea>
                    </div>
                    <div class="col-md-12">
                        <?= lang('status', 'status') ?>
                        <br>
                        <label>
                            <input type="radio" class="form-control" name="status" value="1" checked>
                            <?= lang('active') ?>
                        </label>
                        <label>
                            <input type="radio" class="form-control" name="status" value="0">
                            <?= lang('delete') ?>
                        </label>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <div class="modal-footer">
            <div class="row">
                <div class="col-md-12">
                    <button type="button" class="btn btn-primary" id="send_notification"><?= lang('send') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).on('click', '#send_notification', function(){
        if ($('#form_notification').valid()) {
            $('#form_notification').submit();
        }
    });
</script>