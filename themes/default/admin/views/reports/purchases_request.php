<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";
if ($this->input->post('product')) {
    $v .= "&product=" . $this->input->post('product');
}
if ($this->input->post('reference_no')) {
    $v .= "&reference_no=" . $this->input->post('reference_no');
}
if ($this->input->post('biller')) {
    $v .= "&biller=" . $this->input->post('biller');
}
if ($this->input->post('user')) {
    $v .= "&user=" . $this->input->post('user');
}
if ($this->input->post('serial')) {
    $v .= "&serial=" . $this->input->post('serial');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <?php echo admin_form_open("reports/purchases_request", 'id="form_filter"'); ?>
                            <input type="hidden" name="generate_report" value="1">
                            <div class="col-sm-4">
                                <div class="form-group">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                    $wh[""] = lang('select').' '.lang('biller');
                                    foreach ($billers as $biller) {
                                        $wh[$biller->id] = $biller->name;
                                    }
                                    echo form_dropdown('biller', $wh, (isset($_POST['biller']) ? $_POST['biller'] : ""), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                    ?>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="checkbox" name="group_by_category_sub_category">
                                    <?= lang('group_by_category_sub_category') ?>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="checkbox" name="group_by_preferences">
                                    <?= lang('group_by_preferences') ?>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <label>
                                    <input type="checkbox" name="print_additional_page">
                                    <?= lang('print_additional_page') ?>
                                </label>
                            </div>
                            <div class="col-md-12">
                                <!-- <label>
                                    <input type="radio" name="print_to" value="xls">
                                    <?= lang('Excel') ?>
                                </label> -->
                                <label>
                                    <input type="radio" name="print_to" value="pdf" checked>
                                    <?= lang('PDF') ?>
                                </label>
                            </div>
                            <div class="col-md-6">
                                <button class="btn btn-primary"><?= lang('submit') ?></button>
                            </div>

                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>