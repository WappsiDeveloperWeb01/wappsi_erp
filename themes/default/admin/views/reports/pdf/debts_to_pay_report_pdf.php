<?php
/*
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'vendor/fpdf/fpdf.php';
*/

require_once APPPATH.'../vendor/fpdf/fpdf.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {       
        $this->SetFont('Arial','B',12);
        $this->Image($this->Logo,18,8,30,0);
        $this->Cell(100);        
        $this->Cell(30,4,'ANALISIS DE CUENTAS POR PAGAR POR EDADES',0,0,'C');
        $this->Ln();
        $this->Cell(37); 
        $this->SetFont('Arial','',6);
        $this->Cell(0,4,'Fecha : '.date('d-m-Y h:i A'),0,0,'C');
        $this->Ln(8);
    }

    function Footer()
    {
        $this->SetFont('Arial','',6);
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica '.'            Usuario: '.$this->User),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

}

$fuente = 6;
$adicional_fuente = 2;

$height = 5;

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->sma = $this->sma;
//$pdf->Logo = base_url('assets/uploads/logos/'.$Settings->logo);
$pdf->Logo = base_url('assets/uploads/logos/'.$default_biller->logo);
$pdf->AliasNbPages();
$pdf->User = $this->session->userdata('username');
$pdf->AddPage();
$pdf->SetFont('Arial','B',$fuente);

$pdf->Cell(20,8, 'REFERENCIA COMPRA','TB',0,'C');
$pdf->Cell(20,8, 'FECHA','TB',0,'C');
$pdf->Cell(20,8, 'VALOR COMPRA','TB',0,'C');
$pdf->Cell(10,8, $this->sma->utf8Decode('DÍAS'),'TB',0,'C');
$pdf->SetXY($pdf->GetX(),$pdf->GetY()); 
$pdf->MultiCell(18,4, 'SALDO '.chr(10).' 0 - 30','TB','C');
$pdf->SetXY($pdf->GetX()+88,$pdf->GetY()-8); 
$pdf->MultiCell(18,4, 'SALDO '.chr(10).' 31 - 60','TB','C');
$pdf->SetXY($pdf->GetX()+106,$pdf->GetY()-8); 
$pdf->MultiCell(18,4, 'SALDO '.chr(10).' 61 - 90','TB','C');
$pdf->SetXY($pdf->GetX()+124,$pdf->GetY()-8); 
$pdf->MultiCell(18,4, 'SALDO '.chr(10).' 91 - 120','TB','C');
$pdf->SetXY($pdf->GetX()+142,$pdf->GetY()-8); 
$pdf->MultiCell(18,4, 'SALDO '.chr(10).' 121 o mas','TB','C');
$pdf->SetXY($pdf->GetX()+160,$pdf->GetY()-8); 
$pdf->Cell(20,8, 'SALDO TOTAL','TB',0,'C');
$pdf->MultiCell(15,8, 'VENCE','TB','C');
$pdf->SetXY($pdf->GetX()+195,$pdf->GetY()-8); 
$pdf->Ln();

$pdf->SetXY($pdf->GetX(),$pdf->GetY()); 

$pdf->SetFont('Arial','',$fuente);
$supplier =  '' ; 
$gran_total_venta = 0;
$gran_total_pago = 0;
$gran_total_saldo = 0;
$gran_total_30 = 0;
$gran_total_60 = 0;
$gran_total_90 = 0;
$gran_total_120 = 0;
$gran_total_mas = 0;
$total = $val_pago = $Saldo = 0;
$total_total = $total_val_pago = $total_Saldo = 0;			
$total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;
$saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';		
$i = 0;

foreach ($purchases as $purchase)
{
    $saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';	
    $i++;
    if ($purchase['cod_companie'] != $supplier) 
    {
        if ($i > 1) 
        {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(55,$height+1, 'SubTotal Proveedor' ,0,0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell(20,$height+1, '$ '.number_format($total_total,0,'','.'),0,0,'R');
            $pdf->Cell(20,$height+1, '$ '.number_format($total_30,0,'','.'),0,0,'R');
            $pdf->Cell(20,$height+1, '$ '.number_format($total_60,0,'','.'),0,0,'R');
            $pdf->Cell(20,$height+1, '$ '.number_format($total_90,0,'','.'),0,0,'R');
            $pdf->Cell(20,$height+1, '$ '.number_format($total_120,0,'','.'),0,0,'R');
            $pdf->Cell(20,$height+1, '$ '.number_format($total_mas,0,'','.'),0,0,'R');
            $pdf->Cell(20,$height+1, '$ '.number_format($total,0,'','.') ,0,0,'R');
            $pdf->Ln();

            $gran_total_30  += $total_30;
            $gran_total_60  += $total_60;
            $gran_total_90  += $total_90;
            $gran_total_120 += $total_120;
            $gran_total_mas += $total_mas;
            $total_total = $total_val_pago = $total_Saldo = 0;
            $total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;

        }
        $pdf->SetFont('Arial','B',$fuente+2);
        $pdf->Cell(0,$height+2, 'Proveedor: '.$this->sma->utf8Decode($purchase['supplier']) ,'T',0,'L');
        $pdf->Ln();
        $supplier = $purchase['cod_companie'];
    }

    $fecha = $select_due_days_date == 1 ? substr($purchase['Fecha'],0,10) : substr($purchase['due_date'],0,10);
    $fecha_now = date('Y-m-d');
    $dias	= (strtotime($fecha)-strtotime($fecha_now))/86400;

    if ($dias < 0) {
        $dias   = abs($dias); 
        $dias   = floor($dias);
    } else {
        $dias  = 0;
    }


    $Saldo = '$ '.number_format($purchase['Saldo'],0,'','.');

    if ($dias <=30) 
    {
        $saldo_30 = $Saldo;
        $total_30 += $purchase['Saldo'];
    }
    else if ($dias <=60)
    {
        $saldo_60 = $Saldo;
        $total_60 += $purchase['Saldo'];
    }
    else if ($dias <=90)
    {
        $saldo_90 = $Saldo;
        $total_90 += $purchase['Saldo'];
    }
    elseif ($dias <=120) 
    { 
        $saldo_120 = $Saldo;
        $total_120 += $purchase['Saldo'];
    }
    else
    {
        $saldo_mas = $Saldo;
        $total_mas += $purchase['Saldo'];
    }


    $total += $purchase['Saldo'];

    $pdf->SetFont('Arial','',$fuente);
    if (!$view_summary) {
        $pdf->Cell(20,$height,  $purchase['Referencia'],0,0,'L');
        $pdf->Cell(20,$height, substr($purchase['Fecha'],0,10) ,0,0,'C');
        $pdf->Cell(20,$height+1, '$ '.number_format($purchase['total'],0,'','.'),0,0,'R');
        $pdf->Cell(10,$height, $dias ,0,0,'R');
        $pdf->Cell(18,$height, $saldo_30 ,0,0,'R');
        $pdf->Cell(18,$height, $saldo_60 ,0,0,'R'); 
        $pdf->Cell(18,$height, $saldo_90 ,0,0,'R'); 
        $pdf->Cell(18,$height, $saldo_120 ,0,0,'R');
        $pdf->Cell(18,$height, $saldo_mas ,0,0,'R');
        $pdf->Cell(20,$height, $Saldo ,0,0,'R');
        $pdf->Cell(15,$height, substr($purchase['due_date'],0,10) ,0,0,'R');
        $pdf->Ln();
    }
    $total_total += $purchase['total'];
    $total_val_pago += $purchase['val_pago'];
    $total_Saldo += $purchase['Saldo'];
    $gran_total_venta += $purchase['total'];
    $gran_total_pago += $purchase['val_pago'];
    $gran_total_saldo += $purchase['Saldo'];
}

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(55,$height+1, 'SubTotal Proveedor' ,0,0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(20,$height+1, '$ '.number_format($total_total,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($total_30,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($total_60,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($total_90,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($total_120,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($total_mas,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($total,0,'','.') ,0,0,'R');
$pdf->Ln();

$gran_total_30  += $total_30;
$gran_total_60  += $total_60;
$gran_total_90  += $total_90;
$gran_total_120 += $total_120;
$gran_total_mas += $total_mas;

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(55,$height+1, 'Total Informe' ,0,0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(20,$height+1, '$ '.number_format($gran_total_venta,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($gran_total_30,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($gran_total_60,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($gran_total_90,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($gran_total_120,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($gran_total_mas,0,'','.'),0,0,'R');
$pdf->Cell(20,$height+1, '$ '.number_format($gran_total_saldo,0,'','.') ,0,0,'R');
$pdf->Ln();

$pdf->Output();

?>