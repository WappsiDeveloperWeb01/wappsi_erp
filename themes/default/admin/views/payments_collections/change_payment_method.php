<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <?= admin_form_open_multipart("payments_collections/change_payment_method/".$inv->id.'', ['id' => 'change_payment_method']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
                <span class="modal-title" style="font-size: 150%;"><?=lang('change_payment_method');?></span>
                <h3><?= lang('ref')." : ".$inv->reference_no ?></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <?php $cnt = 1; ?>
                    <?php foreach ($payments as $pmnt): ?>
                        <?php if ($pmnt->paid_by != 'retencion' && $pmnt->amount > 0): ?>
                            <div class="form-group col-sm-12">
                                <label class="pm_msg"><?= ucfirst(mb_strtolower(lang('payment_method')." N°.".$cnt)." por valor de ".$this->sma->formatMoney($pmnt->amount)) ?></label>
                                <select name="paid_by[]" id="paid_by" class="form-control paid_by">>
                                    <?= $this->sma->paid_opts($pmnt->paid_by); ?>
                                </select>
                                <em class="text-danger deposits_affected_error" style="display: none;">El valor afectaría dos o más anticipos, no se puede editar a esta forma de pago</em>
                                <input type="hidden" name="payment_id[]" value="<?= $pmnt->id ?>">
                                <input type="hidden" name="payment_amount[]" class="pmnt_amount" value="<?= $pmnt->amount ?>">
                                <input type="hidden" name="customer_id[]"  value="<?= $inv->customer_id ?>">
                                <div class="deposit_message"> </div>
                            </div>
                            <?php $cnt++; ?>
                        <?php endif ?>    
                    <?php endforeach ?>
                </div>
            </div>
            <div class="modal-footer">
                <input type="hidden" name="change_payment_method" value="1">
                <button class="btn btn-success submit" type="button"><?= lang('submit') ?></button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function(){
        $('.submit').prop('disabled', true);
        <?php if (!$pos_register_status): ?>
            location.href = site.base_url+'payments_collections/index';
        <?php endif ?>
        $.each($('select.paid_by'), function(index, select){
            supplier_name = $(select).find(':selected').data('suppliername');
            if (supplier_name != '') {
                $('.supplier_name').eq(index).val(supplier_name);
            }
        });
        _paid_selected = $('#paid_by').val();
    });

    $(document).on('click', '.submit', function(){
        if ($('#change_payment_method').valid()) {
            $('#change_payment_method').submit();
        }
    });

    $(document).on('change', 'select#paid_by', function(){
        if ($(this).val() == _paid_selected) {
            $('.submit').prop('disabled', true);
        }else{
            $('.submit').prop('disabled', false);
            $('.deposits_affected_error').fadeOut();
            index_p = $('select.paid_by').index($(this));
            amount_to_pay = $('.pmnt_amount').eq(index_p).val();
            $('.deposit_message').eq(index_p).empty();
            if ($(this).val() == 'deposit') {
                $.ajax({
                    url:  site.base_url+"customers/deposit_balance/<?= $inv->customer_id ?>/"+amount_to_pay,
                }).done(function(data) {
                    $('.deposit_message').eq(index_p).empty();
                    $('.deposit_message').eq(index_p).append(data);
                    $.ajax({
                        url:  site.base_url+"customers/get_deposits_will_be_affected/<?= $inv->customer_id ?>/"+amount_to_pay,
                        dataType: 'JSON',
                    }).done(function(data) {
                        if (data.num == 0 || data.num > 1) {
                            $('.submit').prop('disabled', true);
                            $('.deposits_affected_error').fadeIn();
                        } else if (data.error == 1) {
                            $('.submit').prop('disabled', true);
                        } else {
                            $('.submit').prop('disabled', false);
                            $('.deposits_affected_error').fadeOut();
                        }
                    });
                });
            } else {
                $('.deposit_message').eq(index_p).empty();
            }
        }
    });
</script>