<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
    $v = "";
    if ($this->input->post('customer')) {
        $v .= "&customer_id=" . $this->input->post('customer');
    }
    if ($this->input->post('biller')) {
        $v .= "&biller_id=" . $this->input->post('biller');
    }
    if ($this->input->post('start_date')) {
        $v .= "&start_date=" . $this->input->post('start_date');
    }
    if ($this->input->post('end_date')) {
        $v .= "&end_date=" . $this->input->post('end_date');
    }
    if ($this->input->post('Informe')) {
        $v .= "&Informe=" . $this->input->post('Informe');
    }
    if ($this->input->post('sales')) {
        $v .= "&sales=" . $this->input->post('sales');
    }
    if ($this->input->post('returns')) {
        $v .= "&returns=" . $this->input->post('returns');
    }
    if ($this->input->post('gift_cards')) {
        $v .= "&gift_cards=" . $this->input->post('gift_cards');
    }
?>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?php echo admin_form_open("reports/award_points", 'id="form_filter"'); ?>
                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-title">
                        <h5><?= lang('filter') ?></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">                      
                        <div class="row">
                            <div class="col-sm-3">
                                <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                <?php
                                    echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="filter_customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-sm-3 filter_movement">
                                <label for="biller" class="control-label"><?= lang('biller') ?></label>
                                <?php
                                    $biller_selected = '';
                                    $biller_readonly = false;
                                    if ($this->session->userdata('biller_id')) {
                                        $biller_selected = $this->session->userdata('biller_id');
                                        $biller_readonly = true;
                                    }

                                    $bl[""] = lang('select');
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control"  id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                ?>
                            </div>
                            <div class="col-sm-3 filter_movement">
                                <div class="form-group">
                                    <label for="start_date" class="control-label"> <?= lang('start_date') ?> </label>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : date('d/m/Y')), 'class="form-control date" id="start_date" 
                                    type="date" required="required" '); ?>
                                </div>
                            </div>
                            <div class="col-sm-3 filter_movement">
                                <div class="form-group">
                                    <label class="control-label" for="end_date"><?= lang("end_date"); ?> </label>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : date('d/m/Y')), 'class="form-control date" id="end_date" 
                                    type="date" required="required" '); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                        <label for="type" class="control-label"><?= lang('type')." ".lang('report') ?></label><br>
                                            <label class="radio-inline">
                                                <input class="icheck" type="radio" name="Informe" id="Informe1" value="1" <?= (!isset($_POST['Informe']) || $_POST['Informe'] == 1) ? 'checked' : '' ?>> <?= lang('accumulated_points') ?>
                                            </label>
                                            <label class="radio-inline">
                                                <input class="icheck" type="radio" name="Informe" id="Informe2" value="2" <?= (isset($_POST['Informe']) && $_POST['Informe'] == 2) ? 'checked' : '' ?> > <?= lang('movements_points') ?>
                                            </label>
								        </div>
                                    </div>

                                    <div class="col-sm-8 filter_movement">
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="sales" class="control-label"><?= lang('sales') ?></label>
                                                <div class="controls" id="filter_sales">
                                                    <?php
                                                        echo form_checkbox('sales', 'sales', ((!isset($_POST['sales']) && !isset($_POST['returns']) && !isset($_POST['gift_cards'])) ? 'checked' : '') || (isset($_POST['sales']) ? 'checked' : ''));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="returns" class="control-label"><?= lang('returns') ?></label>
                                                <div class="controls" id="filter_returns">
                                                    <?php
                                                        echo form_checkbox('returns', 'returns', (isset($_POST['returns']) ? 'checked' : ''));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <label for="gift_cards" class="control-label"><?= lang('gift_cards') ?></label>
                                                <div class="controls" id="filter_gift_cards">
                                                    <?php
                                                        echo form_checkbox('gift_cards', 'gift_cards', (isset($_POST['gift_cards']) ? 'checked' : ''));
                                                    ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" name="filter_action" id="filter_action" value="0">
                                <div class="form-group">
                                    <div class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_filter"'); ?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>
    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <?php if ($_POST['Informe'] && $_POST['Informe'] == 2): ?>
                                        <h3>
                                            <?= lang('start_date') ?> : <span><?= !empty($_POST['start_date']) ? $_POST['start_date'] : date('Y-m-d') ?></span>
                                            &nbsp;&nbsp;&nbsp;     
                                            <?= lang('end_date') ?> : <span><?= !empty($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d') ?></span> 
                                        </h3>
                                    <?php endif; ?>
                                    
                                    <table id="apData" class="table  table-bordered table-condensed table-hover dfTable reports-table" style="margin-bottom:5px;">
                                        <thead>
                                            <tr class="active">
                                                <th><?= lang("vat_no"); ?></th>
                                                <th><?= lang("customer"); ?></th>   
                                                <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['sales']) && $_POST['sales'] == 'sales')): ?>
                                                    <th><?= lang("sales"); ?></th>
                                                    <th><?= lang("points"). ' ' .lang('sales'); ?></th>
                                                <?php endif ?> 
                                                <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['returns']) && $_POST['returns'] == 'returns')): ?>
                                                    <th><?= lang("returns"); ?></th>
                                                    <th><?= lang("points"). ' ' .lang('returns'); ?></th>
                                                <?php endif ?> 
                                                <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['gift_cards']) && $_POST['gift_cards'] == 'gift_cards')): ?>
                                                    <th><?= lang("points"). ' ' .lang('gift_cards'); ?></th>
                                                <?php endif ?>         
                                                <th><?= lang('total'). ' ' .lang("points"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                            <tr class="active">
                                                <th><?= lang("vat_no"); ?></th>
                                                <th><?= lang("customer"); ?></th>   
                                                <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['sales']) && $_POST['sales'] == 'sales')): ?>
                                                    <th><?= lang("sales"); ?></th>
                                                    <th><?= lang("points"). ' ' .lang('sales'); ?></th>
                                                <?php endif ?> 
                                                <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['returns']) && $_POST['returns'] == 'returns')): ?>
                                                    <th><?= lang("returns"); ?></th>
                                                    <th><?= lang("points"). ' ' .lang('returns'); ?></th>
                                                <?php endif ?> 
                                                <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['gift_cards']) && $_POST['gift_cards'] == 'gift_cards')): ?>
                                                    <th><?= lang("points"). ' ' .lang('gift_cards'); ?></th>
                                                <?php endif ?>         
                                                <th><?= lang('total'). ' ' .lang("points"); ?></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

<script>
    $(document).ready(function() {
        <?php if (isset($_POST['customer'])) : ?>
            customer = '<?= $_POST['customer'] ?>';
        <?php endif; ?>

        if (typeof customer !== 'undefined' && customer != '') {
            $('#filter_customer').val(customer).select2({
                allowClear: true,
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });

            $('#filter_customer').trigger('change');
        } else {
            $('#filter_customer').select2({
                allowClear: true,
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "customers/getcustomer/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });
        }

        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
            var oTable = $('#apData').dataTable({
                "aaSorting": [
                    [2, "asc"]
                ],
                "aLengthMenu": [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
                ],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true,
                'bServerSide': true,
                "dom" : '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
                'sAjaxSource': '<?= admin_url('reports/getAwardPoinstReport/?v=1' . $v) ?>',
                'fnServerData': function(sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
                },
                'fnRowCallback': function(nRow, aData, iDisplayIndex) {

                },
                "fnDrawCallback": function (oSettings) {
                    $('.actionsButtonContainer').html('<div class="dropdown">'+
                        '<button class="btn btn-primary btn-outline new-button dropdown-toggle center-block" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                        '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                            '<?php if(isset($_POST['Informe']) && $_POST['Informe'] == 2){ ?>' +
                                '<li>'+
                                    '<a href="<?= admin_url('reports/getAwardPoinstReport/xls_detail/0/0/?v=1' . $v) ?>" id="xls_detail">'+
                                        '<i class="icon fa fa-file-excel-o"></i> <?= lang("download_xls") ?>'+
                                    '</a>'+
                                '</li>'+
                                '<li>'+
                                    '<a href="<?= admin_url('reports/getAwardPoinstReport/0/xls/0/?v=1' . $v) ?>" id="xls">'+
                                        '<i class="icon fa fa-file-excel-o"></i> <?= lang("download_xls_summary") ?>'+
                                    '</a>'+
                                '</li>'+
                            '<?php } ?>'+
                            '<?php if(isset($_POST['Informe']) && $_POST['Informe'] == 1){ ?>' +
                                '<li>'+
                                    '<a href="<?= admin_url('reports/getAwardPoinstReport/0/0/xls_acumulated/?v=1' . $v) ?>" id="xls_acumulated">'+
                                        '<i class="icon fa fa-file-excel-o"></i> <?= lang("download_xls") ?>'+
                                    '</a>'+
                                '</li>'+
                            '<?php } ?>'+
                        '</ul>'+
                    '</div>');
                                   
                    $('[data-toggle-second="tooltip"]').tooltip();
                },
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['sales']) && $_POST['sales'] == 'sales')): ?>
                        { "bSortable": false, "mRender": currencyFormat },
                        { "bSortable": false, "mRender" : formatStylePoint },
                    <?php endif ?>
                    <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['returns']) && $_POST['returns'] == 'returns')): ?>
                        { "bSortable": false, "mRender": currencyFormat },
                        { "bSortable": false, "mRender" : formatStylePoint },
                    <?php endif ?>
                    <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['gift_cards']) && $_POST['gift_cards'] == 'gift_cards')): ?>
                        { "bSortable": false, "mRender" : formatStylePoint },
                    <?php endif ?>
                    { "bSortable": false, "mRender" : formatStylePoint }
                ],
                "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                    c = 2;
                    <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['sales']) && $_POST['sales'] == 'sales')): ?>
                        c = c+2;
                    <?php endif ?>
                    <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['returns']) && $_POST['returns'] == 'returns')): ?>
                        c = c+2;
                    <?php endif ?>
                    <?php if((isset($_POST['Informe']) && $_POST['Informe'] == 2) && (isset($_POST['gift_cards']) && $_POST['gift_cards'] == 'gift_cards')): ?>
                        c = c+1;
                    <?php endif ?>
                    var tt1 = tt2 = 0;
                    for (var i = 0; i < aaData.length; i++) {
                        tt2 += parseFloat(aaData[aiDisplay[i]][c]);
                    }
                    var nCells = nRow.getElementsByTagName('th');
                    nCells[c].innerHTML = '<div class="text-right">' + formatQuantity2(tt2) + '</div>';
                }
            }).fnSetFilteringDelay().dtFilter([
                {
                    column_number: 0,
                    filter_default_label: "[<?= lang('vat_no'); ?>]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 1,
                    filter_default_label: "[<?= lang('customer'); ?>]",
                    filter_type: "text",
                    data: []
                },
            ], "footer");
        <?php endif ?>

        $('#submit_filter').on('click', function(e) {
            $('#filter_action').val(1);
        });

        $('.filter_movement').hide();
        change_forms();
        $('.icheck').on('ifChanged', function(){
            change_forms();
        });   
    });

    function change_forms(){
        var selectedOption = $('input[name="Informe"]:checked').val();
        if ($('input[name="Informe"]:checked').length > 0) {
            if (selectedOption == 1) {
                $('.filter_movement').hide();
            }else{
                $('.filter_movement').show();
            }
        } 
    }

    function formatStylePoint(x){
        return '<div class="text-center"><span class=""><strong>'+ x +'</strong></span></div>';
    }

</script>