<script>
    var oTable;
    $(document).ready(function () {



      function borrar(){
        //console.log('Funcionalidad para la alerta al borrar los conteos sequenciales');
        $('.confirm').click(function(event) {
          var a_href = $(this).attr('href');
          event.preventDefault();
          bootbox.confirm("Seguro que quiere eliminar este conteo secuencial?", function(result) {
            if(result){
              //alert("Confirm result: " + result);
              //window.location= site.base_url+'products/apply_sequential_count/<?//= $inv->id; ?>';

              console.log(a_href);
              window.location = a_href;
            }
          });
        });
      }



        function count_type(x) {
            if (x == 'full') {
                return '<div class="text-center"><label class="label label-success"><?= lang('full'); ?></label></div>';
            } else if (x == 'partial') {
                return '<div class="text-center"><label class="label label-primary"><?= lang('partial'); ?></label></div>';
            } else {
                return x;
            }
        }
        function count_apply(y) {
            if (y == '1') {
                return '<div class="text-center"><label class="label label-success"><?= lang('Aplicado'); ?></label></div>';

            } else if (y == '0') {
                return '<div class="text-center"><label class="label label-warning"><?= lang('No aplicado'); ?></label></div>';
            } else {
                return y;
            }
        }

        oTable = $('#STData').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('products/getSequentialCounts'.($warehouse_id ? '/'.$warehouse_id : '')) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false, "mRender": checkbox},
                {"mRender": fld},
                null,
                null,
                {"mRender": count_type},
                null,
                null,
                {"mRender": count_apply},
                {"bSortable": false}
            ],
            "fnInitComplete": function(oSettings, json) {
              borrar();
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[0];
                nRow.aplicado = aData[7];
                nRow.className = "sequential_count_link";
                //nRow.className = "adjustment_link";
                console.log(aData);
                console.log('<tr></tr>');
                //$(nRow).addClass( 'hell' );





                 if(nRow.aplicado == 1){
                //   $(nRow.td:last-child).html('');
                      $('td:eq(8)', nRow).html('');
                 }
                console.log(nRow);
                //7






                return nRow;

            },
            // "aoColumns": [
            //     {"bSortable": false, "mRender": checkbox},
            //     {"mRender": fld},
            //     null,
            //     null,
            //     {"mRender": count_type},
            //     null,
            //     null,
            //     {"bSortable": false, "mRender": attachment2},
            //     {"bSortable": false, "mRender": attachment},
            //     {"bSortable": false}
            // ]

        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('reference');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('type');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('brands');?>]", filter_type: "text", data: []},
            {column_number: 6, filter_default_label: "[<?=lang('categories');?>]", filter_type: "text", data: []},
        ], "footer");







    });
</script>
<?php
if ($Owner || $GP['bulk_actions']) { echo admin_form_open('products/count_actions'.($warehouse_id ? '/'.$warehouse_id : ''), 'id="action-form"'); }
?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right">
                                <a href="<?= admin_url('products/sequentialCount') ?>" class="btn btn-outline btn-success dropdown-toggle">Nuevo conteo secuencial</a>
                            </div>

                            <?php if (!empty($warehouses)) { ?>
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle">
                                        <?= lang("warehouses") ?> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                                        <li>
                                            <a href="<?= admin_url('products/sequential_counts') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a>
                                        </li>
                                        <li class="divider"></li>
                                        <?php
                                        foreach ($warehouses as $warehouse) {
                                            echo '<li><a href="' . admin_url('products/sequential_counts/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="STData" class="table table-bordered table-condensed table-hover">
                                <thead>
                                <tr class="primary">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkth" type="checkbox" name="check"/>
                                </th>
                                <th class="col-xs-2"><?= lang("date") ?></th>
                                <th class="col-xs-2"><?= lang("reference") ?></th>
                                <th class="col-xs-2"><?= lang("warehouse") ?></th>
                                <th class="col-xs-1"><?= lang("type") ?></th>
                                <th class="col-xs-2"><?= lang("brands") ?></th>
                                <th class="col-xs-2"><?= lang("categories") ?></th>
                                <th class="col-xs-1"><?= lang("Apply") ?></th>
                                <!-- <th style="max-width:30px; text-align:center;"><i class="fa fa-file-o"></i></th>
                                <th style="max-width:30px; text-align:center;"><i class="fa fa-chain"></i></th> -->
                                <th style="max-width:65px; text-align:center;"><?= lang("actions") ?></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                </tr>
                                </tbody>

                                <tfoot class="dtFilter">
                                <tr class="active">
                                <th style="min-width:30px; width: 30px; text-align: center;">
                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                </th>
                                <th></th><th></th><th></th><th></th><th></th><th></th><th></th>
                                <!-- <th style="max-width:30px; text-align:center;"><i class="fa fa-file-o"></i></th>
                                <th style="max-width:30px; text-align:center;"><i class="fa fa-chain"></i></th> -->
                                <th style="width:65px; text-align:center;"><?= lang("actions") ?></th>
                                </tr>
                                </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div><!-- /.ibox-content -->
            </div><!-- /.ibox -->
        </div>
    </div>
</div>


<?php if ($Owner || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>