<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Cron_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->lang->admin_load('cron');
        $this->load->admin_model('pos_model');
        $this->load->admin_model('reports_model');
    }

    public function run_cron()
    {
        $fecha_backup = date('Y-m-d-H-i-s');
        $m = [];
        $detail_html = "<table style='border:1px solid #DDD; padding:10px; margin:10px 0; font-size: 85%;'>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Fecha vencimiento</th>
                                <th>Plazo de pago</th>
                                <th>Total</th>
                            </tr>";
        $p = 0;
        if ($pendingInvoices = $this->get_due_sales()) {
            foreach ($pendingInvoices as $invoice) {
                if ($this->Settings->detail_due_sales == 1) {
                    $detail_html .= "<tr>
                                        <td>".$invoice->reference_no."</td>
                                        <td>".$invoice->date."</td>
                                        <td>".$invoice->due_date."</td>
                                        <td>".$invoice->payment_term."</td>
                                        <td style='text-align:right;'>".$this->sma->formatMoney($invoice->grand_total)."</td>
                                    </tr>";
                }
                $p++;
            }
            $m[] = sprintf(lang('x_due_sales'), $p);
        }
        $detail_html .= "</table>";
        if ($p>0 && $this->Settings->detail_due_sales == 1) {
            $m[] = $detail_html;
        }
        $detail_html = "<table style='border:1px solid #DDD; padding:10px; margin:10px 0; font-size: 85%;'>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Fecha vencimiento</th>
                                <th>Plazo de pago</th>
                                <th>Total</th>
                            </tr>";

        $pp = 0;
        if ($partialInvoices = $this->get_partial_sales()) {
            foreach ($partialInvoices as $invoice) {
                if ($this->Settings->detail_partial_sales == 1) {
                    $detail_html .= "<tr>
                                        <td>".$invoice->reference_no."</td>
                                        <td>".$invoice->date."</td>
                                        <td>".$invoice->due_date."</td>
                                        <td>".$invoice->payment_term."</td>
                                        <td style='text-align:right;'>".$this->sma->formatMoney($invoice->grand_total)."</td>
                                    </tr>";
                }
                $pp++;
            }
            $m[] = sprintf(lang('x_partial_sales'), $pp);
        }
        $detail_html .= "</table>";
        if ($pp>0 && $this->Settings->detail_partial_sales == 1) {
            $m[] = $detail_html;
        }
        $detail_html = "<table style='border:1px solid #DDD; padding:10px; margin:10px 0; font-size: 85%;'>
                            <tr>
                                <th>Número</th>
                                <th>Fecha</th>
                                <th>Fecha vencimiento</th>
                                <th>Plazo de pago</th>
                                <th>Total</th>
                            </tr>";
        $up = 0;
        if ($paid_sales = $this->get_paid_sales()) {
            foreach ($paid_sales as $invoice) {
                if ($this->Settings->detail_paid_sales == 1) {
                    $detail_html .= "<tr>
                                        <td>".$invoice->reference_no."</td>
                                        <td>".$invoice->date."</td>
                                        <td>".$invoice->due_date."</td>
                                        <td>".$invoice->payment_term."</td>
                                        <td style='text-align:right;'>".$this->sma->formatMoney($invoice->grand_total)."</td>
                                    </tr>";
                }
                $up++;
            }
            $m[] = sprintf(lang('x_paid_sales'), $up);
        }
        $detail_html .= "</table>";
        if ($up>0 && $this->Settings->detail_paid_sales == 1) {
            $m[] = $detail_html;
        }
        $detail_html = "<table style='border:1px solid #DDD; padding:10px; margin:10px 0; font-size: 85%;'>
                    <tr>
                        <th>Producto</th>
                        <th>Fecha expiración</th>
                    </tr>";
        $e = 0; $ep = 0;
        if ($pis = $this->get_expired_products()) {
            foreach($pis as $pi) {
                if ($this->Settings->detail_products_expirated == 1) {
                    $detail_html .= "<tr>
                                        <td>"."(".$pi->product_code.")".$pi->product_name."</td>
                                        <td>".$pi->expiry."</td>
                                    </tr>";
                }
                $this->db->update('purchase_items', array('quantity_balance' => 0), array('id' => $pi->id));
                $e++;
                $ep += $pi->quantity_balance;
            }
            $this->site->syncQuantity(NULL, NULL, $pis);
            $m[] = sprintf(lang('x_products_expired'), $e, $ep);
        }
        $detail_html .= "</table>";
        if ($e>0 && $this->Settings->detail_products_expirated == 1) {
            $m[] = $detail_html;
        }
        $detail_html = "<table style='border:1px solid #DDD; padding:10px; margin:10px 0; font-size: 85%;'>
                    <tr>
                        <th>Producto</th>
                        <th>Fecha promoción</th>
                    </tr>";
        $pro = 0;
        if ($promos = $this->getPromoProducts()) {
            foreach($promos as $pr) {
                if ($this->Settings->detail_products_promo_expirated == 1) {
                    $detail_html .= "<tr>
                                        <td>"."(".$pr->code.")".$pr->name."</td>
                                        <td>".$pr->start_date." - ".$pr->end_date."</td>
                                    </tr>";
                }
                $this->db->update('products', array('promotion' => 0), array('id' => $pr->id));
                $pro++;
            }
            $m[] = sprintf(lang('x_promotions_expired'), $pro);
        }
        $detail_html .= "</table>";
        if ($pro>0 && $this->Settings->detail_products_promo_expirated == 1) {
            $m[] = $detail_html;
        }
        if ($this->Settings->cron_job_db_backup == 1) {
            if ($this->db_backup($fecha_backup)) {
                $m[] = lang('backup_done');
            }
        }
        if ($this->files_backup($fecha_backup)) {
            $m[] = lang('backup_files_done');
        }
        $r = !empty($m) ? $m : false;
        if ($this->Settings->cron_job_send_mail == 1) {
            $this->send_email($r); 
        } 
        return $r;
    }

    private function get_due_sales()
    {
        $q = $this->db
                ->where('payment_status !=', 'paid')
                ->where('payment_status !=', 'partial')
                ->where('date >=', date('Y-m-d 00:00:00'))
                ->where('date <=', date('Y-m-d 23:59:59'))
                ->order_by('date ASC')
                ->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    private function get_partial_sales()
    {
        $q = $this->db->select('sales.*')
                      ->join('payments', 'payments.sale_id = sales.id')
                      ->where('(
                                    ( '.$this->db->dbprefix('payments').'.date >= "'.date('Y-m-d 00:00:00').'" 
                                        AND 
                                      '.$this->db->dbprefix('payments').'.date <= "'.date('Y-m-d 23:59:59').'" 
                                    ) OR 
                                    ('.$this->db->dbprefix('sales').'.date >= "'.date('Y-m-d 00:00:00').'" 
                                        AND '.$this->db->dbprefix('sales').'.date <= "'.date('Y-m-d 23:59:59').'" 
                                    )
                                )')
                      ->where('sales.payment_status', 'partial')
                      ->group_by('sales.id')->order_by('sales.date ASC')->get('sales');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    private function get_paid_sales()
    {
        $q = $this->db->select('sales.*')
                      ->join('payments', 'payments.sale_id = sales.id')
                      ->where('(
                                    ( '.$this->db->dbprefix('payments').'.date >= "'.date('Y-m-d 00:00:00').'" 
                                        AND 
                                      '.$this->db->dbprefix('payments').'.date <= "'.date('Y-m-d 23:59:59').'" 
                                    ) OR 
                                    ('.$this->db->dbprefix('sales').'.date >= "'.date('Y-m-d 00:00:00').'" 
                                        AND '.$this->db->dbprefix('sales').'.date <= "'.date('Y-m-d 23:59:59').'" 
                                    )
                                )')
                      ->where('sales.payment_status', 'paid')
                      ->group_by('sales.id')->order_by('sales.date ASC')->get('sales');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    private function updateInvoiceStatus($id)
    {
        if ($this->db->update('sales', array('payment_status' => 'due'), array('id' => $id))) {
            return TRUE;
        }
        return FALSE;
    }

    private function resetOrderRef()
    {
        if ($this->Settings->reference_format == 1 || $this->Settings->reference_format == 2) {
            $month = date('Y-m') . '-01';
            $year = date('Y') . '-01-01';
            if ($ref = $this->getOrderRef()) {
                $reset_ref = array('so' => 1, 'qu' => 1, 'po' => 1, 'to' => 1, 'pos' => 1, 'do' => 1, 'pay' => 1, 'ppay' => 1, 're' => 1, 'rep' => 1, 'ex' => 1, 'qa' => 1);
                if ($this->Settings->reference_format == 1 && strtotime($ref->date) < strtotime($year)) {
                    $reset_ref['date'] = $year;
                    $this->db->update('order_ref', $reset_ref, array('ref_id' => 1));
                    return TRUE;
                } elseif ($this->Settings->reference_format == 2 && strtotime($ref->date) < strtotime($month)) {
                    $reset_ref['date'] = $month;
                    $this->db->update('order_ref', $reset_ref, array('ref_id' => 1));
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    private function getOrderRef()
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSettings()
    {
        $q = $this->db->get_where('settings', array('setting_id' => 1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function deleteUserLgoins($date)
    {
        $this->db->where('time <', $date);
        if ($this->db->delete('user_logins')) {
            return true;
        }
        return FALSE;
    }

    // private function checkUpdate()
    // {
    //     $fields = array('version' => $this->Settings->version, 'code' => $this->Settings->purchase_code, 'username' => $this->Settings->envato_username, 'site' => base_url());
    //     $this->load->helper('update');
    //     $protocol = is_https() ? 'https://' : 'http://';
    //     $updates = get_remote_contents($protocol.'tecdiary.com/api/v1/update/', $fields);
    //     $response = json_decode($updates);
    //     if (!empty($response->data->updates)) {
    //         $this->db->update('settings', array('update' => 1), array('setting_id' => 1));
    //         return TRUE;
    //     }
    //     return FALSE;
    // }

    private function get_expired_products() {
        if ($this->Settings->remove_expired) {
            $date = date('Y-m-d');
            $this->db->where('expiry <=', $date)->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')->where('quantity_balance >', 0);
            $q = $this->db->get('purchase_items');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
        }
        return FALSE;
    }

    private function getPromoProducts()
    {
        $today = date('Y-m-d');
        $q = $this->db->get_where('products', array('promotion' => 1, 'end_date !=' => NULL, 'end_date <=' => $today));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    private function db_backup($fecha_backup) {
        $this->load->dbutil();
        $prefs = array(
            'format' => 'txt',
            'filename' => 'sma_db_backup.sql',
            'foreign_key_checks' => false
        );
        $this->db->query('
            DROP VIEW IF EXISTS `sma_sales_brands_view`;
            DROP VIEW IF EXISTS `sma_sales_categories_view`;
            DROP VIEW IF EXISTS `sma_sales_customers_view`;
            DROP VIEW IF EXISTS `sma_sales_document_type_view`;
            DROP VIEW IF EXISTS `sma_sales_mean_payments_view`;
            DROP VIEW IF EXISTS `sma_sales_products_view`;
            DROP VIEW IF EXISTS `sma_sales_sellers_view`;
            DROP VIEW IF EXISTS `sma_sales_view`;
                        ');
        if ($this->Settings->cron_job_db_backup_partitions > 1) {
            $qtables = $this->db->query('SHOW TABLES')->result();
            $tables = [];
            foreach ($qtables as $key => $qt) {
                $tables[] = $qt->Tables_in_wwwapp_telas_demo_20;
            }
            $num_tables = count($tables);
            $num_settings = $this->Settings->cron_job_db_backup_partitions;
            $num_foreach = $num_tables / $num_settings;
            $tables = array_chunk($tables, $num_foreach);
            foreach ($tables as $key => $btables) {
                // $this->sma->print_arrays($btables);
                $prefs['tables'] = $btables;
                $back = $this->dbutil->backup($prefs);
                $backup =& $back;
                $db_name = $this->db->database.'-00' . $key . '-'.$fecha_backup . '.sql';
                $db_save_path = './files/backups/'.$this->db->database.'-' . $fecha_backup.'/';
                $save = $db_save_path . $db_name;
                mkdir($db_save_path, 0777);
                $this->load->helper('file');
                write_file($save, $backup);
                $files = glob('./files/backups/*.zip', GLOB_BRACE);
                $now   = time();
                foreach ($files as $file) {
                    if (is_file($file)) {
                        if ($now - filemtime($file) >= 60 * 60 * 24 * 30) {
                            unlink($file);
                        }
                    }
                }
                $this->sma->zip($db_save_path, './files/backups/', $this->db->database.'-00' . $key . '-' . $fecha_backup);
                $this->eliminar_directorio($db_save_path);
                //DROPBOX UPLOAD
                $this->load->library('dropbox');
                $db_name = $this->db->database.'-00' . $key . '-' . $fecha_backup . '.zip';
                $tempfile = FCPATH."files/backups/".$db_name;
                $cli_name = $_SERVER["PHP_SELF"];
                $cli_name = mb_strtolower(str_replace("/", "", $cli_name));
                $cli_name = mb_strtolower(str_replace("index.php", "", $cli_name));
                $cli_host = mb_strtolower($_SERVER['HTTP_HOST']);
                $nombredropbox = "/".$cli_host."/".$cli_name."/".$fecha_backup."/".$db_name;
                $result_dropbox = $this->dropbox->upload($tempfile, $nombredropbox, $this->Settings->dropbox_key, $this->Settings->dropbox_secret, $this->Settings->dropbox_token);
                if ($result_dropbox !== true) {
                    $this->db->insert('system_logs', ['type'=>'Cron Job', 'description'=>$result_dropbox]);
                }
                //DROPBOX UPLOAD
            }
        } else {
            $back = $this->dbutil->backup($prefs);
            $backup =& $back;
            $db_name = $this->db->database.'-' . $fecha_backup . '.sql';
            $db_save_path = './files/backups/'.$this->db->database.'-' . $fecha_backup.'/';
            $save = $db_save_path . $db_name;
            mkdir($db_save_path, 0777);
            $this->load->helper('file');
            write_file($save, $backup);
            $files = glob('./files/backups/*.zip', GLOB_BRACE);
            $now   = time();
            foreach ($files as $file) {
                if (is_file($file)) {
                    if ($now - filemtime($file) >= 60 * 60 * 24 * 30) {
                        unlink($file);
                    }
                }
            }
            $this->sma->zip($db_save_path, './files/backups/', $this->db->database.'-' . $fecha_backup);
            $this->eliminar_directorio($db_save_path);
            //DROPBOX UPLOAD
            $this->load->library('dropbox');
            $db_name = $this->db->database.'-' . $fecha_backup . '.zip';
            $tempfile = FCPATH."files/backups/".$db_name;
            $cli_name = $_SERVER["PHP_SELF"];
            $cli_name = mb_strtolower(str_replace("/", "", $cli_name));
            $cli_name = mb_strtolower(str_replace("index.php", "", $cli_name));
            $cli_host = mb_strtolower($_SERVER['HTTP_HOST']);
            $nombredropbox = "/".$cli_host."/".$cli_name."/".$fecha_backup."/".$db_name;
            $result_dropbox = $this->dropbox->upload($tempfile, $nombredropbox, $this->Settings->dropbox_key, $this->Settings->dropbox_secret, $this->Settings->dropbox_token);
            if ($result_dropbox !== true) {
                $this->db->insert('system_logs', ['type'=>'Cron Job', 'description'=>$result_dropbox]);
            }
            //DROPBOX UPLOAD
        }

            
        return TRUE;
    }

    function send_email($details) {
        if ($details) {
            $table_html = '';
            $tables = $this->cron_model->yesterday_report();
            foreach ($tables as $table) {
                $table_html .= $table.'<div style="clear:both"></div>';
            }
            foreach ($details as $detail) {
                $table_html = $table_html.$detail;
            }

            if ($this->Settings->detail_pos_registers == 1) {
                $register_details = $this->generate_pos_register_details();
                if (count($register_details['open_registers']) > 0) {
                    $table_html .= "<hr><h4>".lang('open_registers')."</h4>";
                    foreach ($register_details['open_registers'] as $key => $op_view) {
                        $table_html .= $op_view;
                    }
                }
                if (count($register_details['closed_registers']) > 0) {
                    $table_html .= "<hr><h4>".lang('closed_registers')."</h4>";
                    foreach ($register_details['closed_registers'] as $key => $cl_view) {
                        $table_html .= $cl_view;
                    }
                }
            }

            if ($this->Settings->detail_zeta_report == 1) {
                $zeta_report_html = $this->generate_zeta_report();
                $table_html .= $zeta_report_html;
            }

            $msg_with_yesterday_report = $table_html;

            $owners = $this->db->get_where('users', array('group_id' => 2, 'main_administrator' => 1))->result();
            $this->load->library('email');
            $config['useragent'] = "Stock Manager Advance";
            $config['protocol'] = $this->Settings->protocol;
            $config['mailtype'] = "html";
            $config['crlf'] = "\r\n";
            $config['newline'] = "\r\n";
            if ($this->Settings->protocol == 'sendmail') {
                $config['mailpath'] = $this->Settings->mailpath;
            } elseif ($this->Settings->protocol == 'smtp') {
                $config['smtp_host'] = $this->Settings->smtp_host;
                $config['smtp_user'] = $this->Settings->smtp_user;
                $config['smtp_pass'] = $this->Settings->smtp_pass;
                $config['smtp_port'] = $this->Settings->smtp_port;
                if (!empty($this->Settings->smtp_crypto)) {
                    $config['smtp_crypto'] = $this->Settings->smtp_crypto;
                }
            }
            $this->email->initialize($config);
            if ($owners) {
                foreach ($owners as $owner) {
                    list($user, $domain) = explode('@', $owner->email);
                    if ($domain != 'tecdiary.com') {
                        $this->load->library('parser');
                        $parse_data = array(
                            'name' => $owner->first_name . ' ' . $owner->last_name,
                            'email' => $owner->email,
                            'msg' => $msg_with_yesterday_report,
                            'site_link' => base_url(),
                            'site_name' => $this->Settings->site_name,
                            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>',
                            'cron_job_has_successfully_run' => lang('cron_job_has_successfully_run')
                            );
                        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/cron.html');
                        $message = $this->parser->parse_string($msg, $parse_data);
                        $subject = lang('cron_job') . ' - ' . $this->Settings->site_name;

                        $this->email->from($this->Settings->default_email, $this->Settings->site_name);
                        $this->email->to($owner->email);
                        $this->email->subject($subject);
                        $this->email->message($message);
                        $this->email->send();
                    }
                }
            }
        }
    }

    private function yesterday_report() {
        // $date = date('Y-m-d', strtotime('-1 day'));
        $date = date('Y-m-d');
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        if ($this->Settings->detail_sales_per_biller == 1) {
            $billers = $this->db->get_where('companies', ['group_name'=>'biller'])->result();
            foreach ($billers as $biller) {
                $costing = $this->getCosting($date, $biller->id);
                $discount = $this->getOrderDiscount($sdate, $edate, $biller->id);
                $expenses = $this->getExpenses($sdate, $edate, $biller->id);
                $returns = $this->getReturns($sdate, $edate, $biller->id);
                $total_purchases = $this->getTotalPurchases($sdate, $edate, $biller->id);
                $total_sales = $this->getTotalSales($sdate, $edate, $biller->id);
                $html[] = $this->gen_html($costing, $discount, $expenses, $returns, $total_purchases, $total_sales, $biller);
            }
        }
        $costing = $this->getCosting($date);
        $discount = $this->getOrderDiscount($sdate, $edate);
        $expenses = $this->getExpenses($sdate, $edate);
        $returns = $this->getReturns($sdate, $edate);
        $total_purchases = $this->getTotalPurchases($sdate, $edate);
        $total_sales = $this->getTotalSales($sdate, $edate);
        $html[] = $this->gen_html($costing, $discount, $expenses, $returns, $total_purchases, $total_sales);

        return $html;
    }

    private function gen_html($costing, $discount, $expenses, $returns, $purchases, $sales, $biller = NULL) {
        $html = '<div style="border:1px solid #DDD; padding:10px; margin:10px 0;">
                    <h3>'.($biller ? $biller->company : lang('all_billers')).'</h3>
                    <table width="100%" class="stable">
                        <tr>
                            <td style="border-bottom: 1px solid #EEE;">'.lang('subtotal_before_tax').'</td>
                            <td style="text-align:right; border-bottom: 1px solid #EEE;">'.$this->sma->formatMoney($costing->net_sales).'</td>
                        </tr>';
                        $html .= '
                        <tr>
                            <td style="border-bottom: 1px solid #EEE;">'.lang('tax').'</td>
                            <td style="text-align:right; border-bottom: 1px solid #EEE;">'.$this->sma->formatMoney($sales->item_tax).'</td>
                        </tr>';
                        $html .= '
                        <tr>
                            <td style="border-bottom: 1px solid #DDD;">'.lang('order_discount').'</td>
                            <td style="text-align:right;border-bottom: 1px solid #DDD;">'. $this->sma->formatMoney($discount->order_discount).'</td>
                        </tr>';
                        $html .= '
                        <tr>
                            <td width="300px;" style="border-bottom: 1px solid #DDD;">'.lang('products_cost_before_tax').'</td>
                            <td style="text-align:right;border-bottom: 1px solid #DDD;">
                                '.$this->sma->formatMoney($costing->net_cost).'
                            </td>
                        </tr>';
                        $html .= '
                        <tr>
                            <td width="300px;" style="border-bottom: 1px solid #DDD;">'.lang('profit').'</td>
                            <td style="text-align:right;border-bottom: 1px solid #DDD;">
                                '.$this->sma->formatMoney($costing->net_sales - $costing->net_cost).'
                            </td>
                        </tr>';
                        $margin = (($costing->net_sales - $costing->net_cost) / ($costing->net_sales > 0 ? $costing->net_sales : 1)) * 100;
                        $html .= '
                        <tr>
                            <td width="300px;" style="border-bottom: 1px solid #DDD;">'.lang('profit_margin').'</td>
                            <td style="text-align:right;border-bottom: 1px solid #DDD;">'.$this->sma->formatDecimal($margin, 2).' %
                            </td>
                        </tr>';
                        $html .= '
                        <tr>
                            <td width="300px;" style="border-bottom: 2px solid #DDD;">'.lang('return_sales').'</td>
                            <td style="text-align:right;border-bottom: 2px solid #DDD;">'.$this->sma->formatMoney($returns->total).'</td>
                        </tr>';
                        $html .= '
                        <tr>
                            <td style="border-bottom: 1px solid #DDD;">'.lang('expenses').'</td>
                            <td style="text-align:right;border-bottom: 1px solid #DDD;">'. $this->sma->formatMoney($expenses ? $expenses->total : 0).'</td>
                        </tr>';
                    $html .= '</table>
                    <h4 style="margin-top:15px;">'. lang('general_ledger') .'</h4>
                    <table width="100%" class="stable">';
                    if ($sales) {
                        $html .= '
                        <tr>
                            <td width="33%" style="border-bottom: 1px solid #DDD;">'.lang('total_sales').': <strong>'.$this->sma->formatMoney($sales->total_amount).'('.$sales->total.')</strong></td>
                            <td width="33%" style="border-bottom: 1px solid #DDD;">'.lang('received').': <strong>'.$this->sma->formatMoney($sales->paid).'</strong></td>
                            <td width="33%" style="border-bottom: 1px solid #DDD;">'.lang('taxes').': <strong>'.$this->sma->formatMoney($sales->tax).'</strong></td>
                        </tr>';
                    }
                    if ($purchases) {
                        $html .= '
                        <tr>
                            <td width="33%">'.lang('total_purchases').': <strong>'.$this->sma->formatMoney($purchases->total_amount).'('.$purchases->total.')</strong></td>
                            <td width="33%">'.lang('paid').': <strong>'.$this->sma->formatMoney($purchases->paid).'</strong></td>
                            <td width="33%">'.lang('taxes').': <strong>'.$this->sma->formatMoney($purchases->tax).'</strong></td>
                        </tr>';
                    }
                    $html .= '</table></div>';
        return $html;
    }

    private function getCosting($date, $biller_id = NULL)
    {
        $this->db->select('SUM( COALESCE( purchase_unit_cost, 0 ) * quantity ) AS cost, SUM( COALESCE( sale_unit_price, 0 ) * quantity ) AS sales, SUM( COALESCE( purchase_net_unit_cost, 0 ) * quantity ) AS net_cost, SUM( COALESCE( sale_net_unit_price, 0 ) * quantity ) AS net_sales', FALSE);
        $this->db->where('costing.date', $date);
        if ($biller_id) {
            $this->db->join('sales', 'sales.id=costing.sale_id')
            ->where('sales.biller_id', $biller_id);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id')) {
                $this->db->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.$this->session->userdata('company_id').')');
            } else {
                $this->db->where('sales.created_by', $this->session->userdata('user_id'));
            }
        } 

        $q = $this->db->get('costing');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getOrderDiscount($sdate, $edate, $biller_id = NULL)
    {
        $this->db->select('SUM( COALESCE( order_discount, 0 ) ) AS order_discount', FALSE);
        $this->db->where('date >=', $sdate)->where('date <=', $edate);
        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getExpenses($sdate, $edate, $biller_id = NULL)
    {
        $this->db->select('SUM( COALESCE( amount, 0 ) ) AS total', FALSE);
        $this->db->where('date >=', $sdate)->where('date <=', $edate);
        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }

        $q = $this->db->get('expenses');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getReturns($sdate, $edate, $biller_id = NULL)
    {
        $this->db->select('SUM( COALESCE( grand_total, 0 ) ) AS total', FALSE)
        ->where('sale_status', 'returned');
        $this->db->where('date >=', $sdate)->where('date <=', $edate);
        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }

        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    private function getTotalPurchases($sdate, $edate, $biller_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax', FALSE)
            ->where('status !=', 'pending')
            ->where('date >=', $sdate)->where('date <=', $edate);
        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }
        $q = $this->db->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function getTotalSales($sdate, $edate, $biller_id = NULL)
    {
        $this->db->select('count(id) as total, sum(COALESCE(grand_total, 0)) as total_amount, SUM(COALESCE(paid, 0)) as paid, SUM(COALESCE(total_tax, 0)) as tax, SUM(COALESCE(product_tax, 0)) as item_tax', FALSE)
            ->where('sale_status !=', 'pending')
            ->where('date >=', $sdate)->where('date <=', $edate);
        if ($biller_id) {
            $this->db->where('biller_id', $biller_id);
        }
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    private function files_backup($fecha_backup){
        $main_file_path = "./assets/uploads/";
        $backup_path = "./files/backups/";
        $name = 'uploads-' . $fecha_backup;
        $this->sma->zip($main_file_path, $backup_path, $name);
        //DROPBOX UPLOAD
        $this->load->library('dropbox');
        $cli_name = $_SERVER["PHP_SELF"];
        $cli_name = mb_strtolower(str_replace("/", "", $cli_name));
        $cli_name = mb_strtolower(str_replace("index.php", "", $cli_name));
        $cli_host = mb_strtolower($_SERVER['HTTP_HOST']);
        $main_file_path = FCPATH."files/backups/";

        if ($this->Settings->cron_job_images_backup == 1) {
            $nombredropbox = "/".$cli_host."/".$cli_name."/".$fecha_backup."/".$name.".zip";
            $result_dropbox = $this->dropbox->upload($main_file_path.$name.".zip", $nombredropbox, $this->Settings->dropbox_key, $this->Settings->dropbox_secret, $this->Settings->dropbox_token);
            if ($result_dropbox !== true) {
                $this->db->insert('system_logs', ['type'=>'Cron Job', 'description'=>$result_dropbox]);
            }
        }

        if ($this->Settings->cron_job_ebilling_backup == 1) {
            $electronic_billing_file_path = "./files/electronic_billing/";
            $eb_name = 'ebilling-' . $fecha_backup;
            if (is_dir($electronic_billing_file_path)) {
                $this->sma->zip($electronic_billing_file_path, $backup_path, $eb_name);
            }
            if (is_dir($electronic_billing_file_path)) {
                $nombredropbox = "/".$cli_host."/".$cli_name."/".$fecha_backup."/".$eb_name.".zip";
                $result_dropbox = $this->dropbox->upload($main_file_path.$eb_name.".zip", $nombredropbox, $this->Settings->dropbox_key, $this->Settings->dropbox_secret, $this->Settings->dropbox_token);
                if ($result_dropbox !== true) {
                    $this->db->insert('system_logs', ['type'=>'Cron Job', 'description'=>$result_dropbox]);;
                }
            }
        }
        //DROPBOX UPLOAD
    }

    public static function eliminar_directorio($dirPath) {
       if (! is_dir($dirPath)) {
           throw new InvalidArgumentException("$dirPath must be a directory");
       }
       if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
           $dirPath .= '/';
       }
       $files = glob($dirPath . '*', GLOB_MARK);
       foreach ($files as $file) {
           if (is_dir($file)) {
               self::eliminar_directorio($file);
           } else {
               unlink($file);
           }
       }
       rmdir($dirPath);
    }

    public function generate_pos_register_details(){


        $open_registers_arr = [];
        $closed_registers_arr = [];

        $data['modal_js'] = null;

        $date = date('Y-m-d');
        $sdate = $date.' 00:00:00';
        $edate = $date.' 23:59:59';
        $closed_registers = $this->db->where('( (date >= "'.$sdate.'" AND date <= "'.$edate.'") OR (closed_at >= "'.$sdate.'" AND closed_at <= "'.$edate.'") OR (status = "open") )')->get('pos_register');
        if ($closed_registers->num_rows() > 0) {
            foreach (($closed_registers->result()) as $cr) {
                $data = [];
                $data['modal_js'] = null;
                $data['cron_job'] = true;
                if ($cr->status == 'close') {
                    $register_data = $this->pos_model->get_register_details($cr->id);
                    $data['register'] = $register_data['register'];
                    $data['register_details'] = $register_data['register_details'];
                    $data['register_details_counted'] = $register_data['register_details_counted'];
                    $data['register_details_categories'] = $register_data['register_details_categories'];
                    $data['close_user'] = $this->site->getUser($data['register']->user_id);
                    $view = $this->load->view($this->Settings->theme.'/admin/views/' . 'pos/register_details', $data, true);
                    $closed_registers_arr[] = $view;
                } else {
                    $user_register = $this->pos_model->registerData(NULL, $cr->id);
                    $register_open_time = $user_register ? $user_register->date : NULL;
                    $data['cash_in_hand'] = $user_register ? $user_register->cash_in_hand : NULL;
                    $data['register_open_time'] = $user_register ? $register_open_time : NULL;
                    $user_id = $user_register->user_id;

                    // //Credit Card Sales - Payments - Returns
                    $data['ccsales'] = $this->pos_model->getRegisterCCSales($register_open_time, $user_id);
                    // //Cheque Sales - Payments - Returns
                    $data['chsales'] = $this->pos_model->getRegisterChSales($register_open_time, $user_id);
                    $popts = $this->site->getPaidOpts(1,0);
                    $data['popts'] = $popts;
                    foreach ($popts as $popt) {
                        $data['popts_data'][$popt->code]['Sales'] = $this->pos_model->getSalesByPaymentOption($popt->code, $register_open_time, $user_id);
                        $data['popts_data'][$popt->code]['RC'] = $this->pos_model->getRegisterSalesRCByPaymentOption($popt->code, $register_open_time, $user_id);
                        $data['popts_data'][$popt->code]['Returned'] = $this->pos_model->getRegisterReturnedSalesByPaymentOption($popt->code, $register_open_time, $user_id);
                    }
                    //Payap Sales - Payments - Returns
                    $data['pppsales'] = $this->pos_model->getRegisterPPPSales($register_open_time, $user_id);
                    $data['pppsalesreturned'] = $this->pos_model->getRegisterReturnedPPPSales($register_open_time, $user_id);
                    //Stripe Sales - Payments - Returns
                    $data['stripesales'] = $this->pos_model->getRegisterStripeSales($register_open_time, $user_id);
                    $data['stripesalesreturned'] = $this->pos_model->getRegisterReturnedStripeSales($register_open_time, $user_id);
                    //Authorize Sales - Payments - Returns
                    $data['authorizesales'] = $this->pos_model->getRegisterAuthorizeSales($register_open_time, $user_id);
                    $data['totalsales'] = $this->pos_model->getRegisterSales($register_open_time, $user_id);
                    //Refunds & Returns
                    $data['refunds'] = $this->pos_model->getRegisterRefunds($register_open_time, $user_id);
                    $data['returns'] = $this->pos_model->getRegisterReturns($register_open_time, $user_id);
                    //Cash Refunds
                    $data['cashrefunds'] = $this->pos_model->getRegisterCashRefunds($register_open_time, $user_id);
                    //Due returns
                    $data['duesales'] = $this->pos_model->getRegisterDueSales($register_open_time, $user_id);
                    $data['duereturns'] = $this->pos_model->getRegisterDueReturns($register_open_time, $user_id);
                    //Expenses
                    $data['expenses'] = $this->pos_model->getRegisterExpenses($register_open_time, $user_id);
                    //Purchases payments
                    $data['ppayments'] = $this->pos_model->getPurchasesPayments($register_open_time, $user_id);
                    //Deposits
                    $data['customer_deposits'] = $this->pos_model->getRegisterDeposits($register_open_time, $user_id);
                    $data['customer_deposits_other_payments'] = $this->pos_model->getRegisterDepositsOtherPayments($register_open_time, $user_id);
                    $data['supplier_deposits'] = $this->pos_model->getRegisterSupplierDeposits($register_open_time, $user_id);
                    $data['pos_register_movements'] = $this->pos_model->getRegisterMovements($register_open_time, $user_id);
                    if ($this->pos_settings->detail_sale_by_category == 1) {
                        $categories = $this->pos_model->getSalesByCategory(NULL, $register_open_time, $user_id);
                        $data['categories'] = $categories;
                    }
                    $data['tips_cash'] = $this->pos_model->getTipsByCash($register_open_time, $user_id);
                    $data['tips_other_payments'] = $this->pos_model->getTipsByOtherPayments($register_open_time, $user_id);
                    $data['tips_due'] = $this->pos_model->getTipsByDue($register_open_time, $user_id);
                    $data['shipping_cash'] = $this->pos_model->getShippingByCash($register_open_time, $user_id);
                    $data['shipping_other_payments'] = $this->pos_model->getShippingByOtherPayments($register_open_time, $user_id);
                    $data['shipping_due'] = $this->pos_model->getShippingByDue($register_open_time, $user_id);
                    $data['totalsales'] = $this->pos_model->getRegisterSales($register_open_time, $user_id);
                    $data['users'] = $this->pos_model->getUsers($user_id);
                    $data['suspended_bills'] = $this->pos_model->getSuspendedsales($user_id);
                    $data['user_id'] = $user_id;
                    $data['pos_settings'] = $this->pos_settings;
                    $view = $this->load->view($this->Settings->theme.'/admin/views/' . 'pos/close_register', $data, true);
                    $open_registers_arr[] = $view;
                }
            }
        }
        return ['closed_registers' => $closed_registers_arr, 'open_registers' => $open_registers_arr];
    }

    public function generate_zeta_report(){
        $Sucursal = NULL;
        $Informe = "Pos";
        $document_type_id = NULL;
        $module_id = NULL;
        $Tipo = "Detallado";
        $date = date('Y-m-d');
        $fech_ini = $date.' 00:00:00';
        $fech_fin = $date.' 23:59:59';
        $data['input'] = array('Sucursal' => $Sucursal,'Tipo' => $Tipo,'users' => 'Usuario de Impresion','fech_ini' => $fech_ini,'fech_fin' => $fech_fin,
                                'nombre' => 'Juan Alberto Giraldo Aristizabal','documento'=>'1098649968' ,'direccion'=>' Calle 30 25 71 lc xxxxxxxx',
                                'ciudad'=>'Floridablanca','telefono'=>'600 0000');
        $data['billings'] = $this->reports_model->get_billing($Sucursal,$fech_ini,$fech_fin, NULL, $module_id, $document_type_id);
        $data['returns'] = $this->reports_model->get_billing($Sucursal,$fech_ini,$fech_fin,1, $module_id, $document_type_id);
        $data['total_payments'] = $this->reports_model->get_total_payment($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
        $data['categories_tax'] = $this->reports_model->get_total_categories_tax($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
        $data['total_tax_rates'] = $this->reports_model->get_total_tax_rates($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
        $data['total_users_taxs'] = $this->reports_model->get_total_users_tax($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
        $data['total_sellers_taxs'] = $this->reports_model->get_total_sellers_tax($Sucursal,$fech_ini,$fech_fin, $module_id, $document_type_id);
        $biller = $Sucursal ? $Sucursal : $this->Settings->default_biller;
        $data['biller'] = $this->site->getAllCompaniesWithState('biller', $biller);
        $data['cron_job'] = true;
        $view = $this->load->view($this->Settings->theme.'/admin/views/' . 'reports/zeta_report_pos', $data, true);
        return $view;
    }

}

        