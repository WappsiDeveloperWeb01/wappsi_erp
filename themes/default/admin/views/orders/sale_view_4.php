<?php

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

//$this->SetTextColor( 255, 0, 0); color rojo
//$this->SetTextColor( 0, 0, 0); color negro

// var_dump($POST);

class PDF extends FPDF
  {

    function Header()
    {

        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 10, 3, '1234', '');
        $cx = 132;
        $cy = 7;
        $this->setXY($cx, $cy);
        $this->Cell(38.5, 10 , $this->sma->utf8Decode($this->document_type->nombre),'',0,'C');
        $this->setXY($cx+38.5, $cy);
        $this->Cell(38.5, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'C');

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cx = 7;
        $cy = 19;
        $this->RoundedRect($cx, $cy, 118, 5, 3, '1', 'DF');
        $this->RoundedRect($cx, $cy+5, 118, 30, 3, '4', '');
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DEL CLIENTE'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->customer->company != '-' ? $this->customer->company : $this->customer->name)),'',1,'L');
        $cy += 5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Nit/Cc : ".$this->customer->vat_no.($this->customer->tipo_documento == 6 ? "-".$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Sucursal Cliente : ".$this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Dirección : ".$this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Teléfonos : ".$this->customer->phone),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode("Correo : ".$this->customer->email),'',1,'L');
        if ($this->factura->contact_name) {
            $cy += 3;
            $this->setXY($cx, $cy);
            $this->Cell(115, 3 , $this->sma->utf8Decode("Contacto : ".$this->factura->contact_name),'',1,'L');
        }
        //derecha


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $cx = 125;
        $cy = 19;
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect($cx, $cy, 84, 5, 3, '2', 'DF');
        $this->RoundedRect($cx, $cy+5, 84, 30, 3, '3', '');
        $this->setXY($cx, $cy);
        $this->Cell(42, $altura , $this->sma->utf8Decode('FECHA FACTURA'),'BR',1,'C');
        $cx += 42;
        $this->setXY($cx, $cy);
        $this->Cell(42, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente);
        $cx -= 42;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(42, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 42;
        $this->setXY($cx, $cy);
        $this->Cell(42, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatMoney($this->factura->grand_total)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 42;
        $this->setXY($cx, $cy);
        $this->Cell(42, $altura , $this->sma->utf8Decode('FECHA VENCE'),'LBR',1,'C',1);
        $cx += 42;
        $this->setXY($cx, $cy);
        $this->Cell(42, $altura , $this->sma->utf8Decode('PLAZO'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cx -= 42;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".$this->factura->payment_term." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(42, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 42;
        $this->setXY($cx, $cy);
        $this->Cell(42, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->payment_term ." ".($this->factura->payment_term > 1 ? "Días" : "Día")),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 42;
        $this->setXY($cx, $cy);
        $this->Cell(84, $altura , $this->sma->utf8Decode('VENDEDOR'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(84, $altura , $this->sma->utf8Decode(ucwords(mb_strtolower($this->seller && $this->seller->company != '-' ? $this->seller->company : ''))),'',1,'C');

        $this->ln();


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        if ($this->show_code) {
            $this->Cell(25,5, $this->sma->utf8Decode('Código'),'TBLR',0,'C',1);
        } else {
            $this->Cell(25,5, $this->sma->utf8Decode('Referencia'),'TBLR',0,'C',1);
        }
        $this->Cell(96,5, $this->sma->utf8Decode('Descripción'),'TBLR',0,'C',1);
        $this->Cell(17,5, $this->sma->utf8Decode('Cant.'),'TBLR',0,'C',1);
        $this->Cell(32.2,5, $this->sma->utf8Decode('Vr. unit.'),'TBLR',0,'C',1);
        $this->Cell(32.2,5, $this->sma->utf8Decode('Vr. total'),'TBLR',1,'C',1);

    }

    function Footer()
    {
        // Print centered page number
        if (!empty($this->description2)) {
            $this->RoundedRect(13, 248, 196, 17.5, 3, '1234', '');
            $this->SetXY(7, -30);
            $this->Cell(196, 5 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        }
            
        $this->SetXY(7, -14);
        // $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');

    }

    function reduceTextToDescription1($text){
        $text="Nota : ".$text;
        if (strlen($text) > 390) {
            $text = substr($text, 0, 385);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        if (strlen($text) > 380) {
            $text = substr($text, 0, 375);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function crop_text($text, $length = 50){
        if (strlen($text) > $length) {
            $text = substr($text, 0, ($length-5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }

}


$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);


$number_convert = new number_convert();

$fuente = 8;
$adicional_fuente = 2;

$color_R = 200;
$color_G = 200;
$color_B = 200;

$pdf->setFillColor($color_R,$color_G,$color_B);

$pdf->biller = $biller;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $sma;
$pdf->document_type = $document_type;
$description1 = $inv->note;
$pdf->description2 = $biller->invoice_footer;
$pdf->show_code = $show_code;

$pdf->AddPage();

$maximo_footer = 250;
$value_decimals = ($invoice_format ? $invoice_format->value_decimals : NULL);
$qty_decimals = ($invoice_format ? $invoice_format->qty_decimals : NULL);

// exit(var_dump($rows));
$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);
$total_qty = 0;
foreach ($rows as $item) { 
    if ($pdf->getY() > $maximo_footer) {
        $pdf->AddPage();
    }
    $columns = [];
        $inicio_fila_X = $pdf->getX();
        $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $pdf->getX();
    $pdf->Cell(25,5, $this->sma->utf8Decode($show_code ? $item->product_code : $item->reference),'',0,'C');
    $columns[0]['fin_x'] = $pdf->getX();
    $columns[1]['inicio_x'] = $pdf->getX();
        $cX = $pdf->getX();
        $cY = $pdf->getY();
        $inicio_altura_fila = $cY;                             
    $pdf->MultiCell(96,5, $this->sma->utf8Decode($item->product_name.
        strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')))),'','');
    $columns[1]['fin_x'] = $cX+96;
        $fin_altura_fila = $pdf->getY();
        $pdf->setXY($cX+96, $cY);
    $columns[2]['inicio_x'] = $pdf->getX();
    $pdf->Cell(17,5, $this->sma->utf8Decode($this->sma->formatQuantity($item->quantity, $qty_decimals)),'',0,'C');
    $total_qty += $item->quantity;
    $columns[2]['fin_x'] = $pdf->getX();
    $columns[3]['inicio_x'] = $pdf->getX();
    $pdf->Cell(32.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $item->unit_price)),'',0,'R');
    $columns[3]['fin_x'] = $pdf->getX();
    $columns[4]['inicio_x'] = $pdf->getX();
    $pdf->Cell(32.2,5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($item->unit_price * $item->quantity))),'',0,'R');
    $columns[4]['fin_x'] = $pdf->getX();
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    foreach ($columns as $key => $data) {
        $ancho_column = $data['fin_x'] - $data['inicio_x'];
        if ($altura_fila < 5) {
            $altura_fila = 5;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
    }
    $pdf->ln($altura_fila);
}

$pdf->SetFont('Arial','',$fuente);
if ($pdf->getY() > 193.6) {
    $pdf->AddPage();
}

if ($totalize_quantity) {
    $pdf->Cell(166 ,5, $this->sma->utf8Decode('Total cantidad'),'LBR',0,'R');
    $pdf->Cell(36.4,5, $this->sma->utf8Decode($this->sma->formatQuantity($total_qty, $qty_decimals)),'LBR',1,'R');
}


$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 121.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(121.6,6.125, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->setX(14);
$pdf->Cell(121.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total)),'',1,'L');
$pdf->setX(13);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

if ($invoice_format->view_item_tax) {
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Tipo Impuesto'),'',1,'C');
    $pdf->Cell(28.9,5, $this->sma->utf8Decode($inv->type_tax_rate == 1 ? $sma->formatDecimal($inv->tax_rate) : 0),'',1,'C');

    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Base'),'',1,'C');
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,5, $this->sma->utf8Decode($sma->formatMoney(($inv->total + $inv->product_tax - $inv->order_discount))),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Impuesto'),'',1,'C');
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,5, $this->sma->utf8Decode($sma->formatMoney($inv->order_tax)),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->Cell(28.9,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,5, $this->sma->utf8Decode($sma->formatMoney($inv->grand_total)),'',1,'C');
}


$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;


//derecha

$pdf->setXY($cX_items_finished+124, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->cell(39.2, 5, $this->sma->utf8Decode('TOTAL BRUTO'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney(($inv->total + $inv->product_tax))), 'TBR', 1, 'R');
$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('DESCUENTO'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney($inv->order_discount)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Sub Total'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney(($inv->total + $inv->product_tax - $inv->order_discount))), 'TBR', 1, 'R');

if ($invoice_format->view_item_tax) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode('Vr. Impuesto'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+39.2, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney($inv->order_tax)), 'TBR', 1, 'R');
}
    

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Retención en la fuente'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(0), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('TOTAL'), 'TBLR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente);
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($sma->formatMoney($inv->grand_total)), 'TBR', 1, 'R');


//FIRMAS

//izquierda
$pdf->ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 202, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;

$pdf->MultiCell(202,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 121.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

//derecha
$pdf->setXY($current_x+121.6, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();
$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

$descargar = false;

if ($for_email === true) {
  $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($descargar) {
      $pdf->Output($inv->reference_no.".pdf", "D");
    } else {
      $pdf->Output($inv->reference_no.".pdf", "I");
    }    
}



