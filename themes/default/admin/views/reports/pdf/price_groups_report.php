<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';

class PDF extends HPDF
  {
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Informe de listas de precios por unidad de medida'));
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->setY(7);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->Image($this->Logo,22,6,45,0);
        $this->setXY($cx+76.06, $cy);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Informe de listas de precios por unidad de medida'), '', 1, 'C');
        $this->setXY($cx, $cy+5);
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Fecha impresión : '.date('Y-m-d H:i:s')), '', 1, 'C');
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        $this->ln();
        $this->SetFont('Arial','B', $this->fontsize_title);
        if (!$this->group_by_category_sub_category) {
            $this->SetFont('Arial','B', $this->fontsize_title);
            $columns = [];
                $inicio_fila_X = $this->getX();
                $inicio_fila_Y = $this->getY();
            $columns[0]['inicio_x'] = $this->getX();
            $this->cell(60.8 , 8, $this->sma->utf8Decode('Producto'), '', 0, 'C');
            $columns[0]['fin_x'] = $this->getX();
            $width = (141.2 / count($this->price_groups));
            $n_column = 1;
            foreach ($this->price_groups as $pg) {
                $columns[$n_column]['inicio_x'] = $this->getX();
                    $cX = $this->getX();
                    $cY = $this->getY();
                    $inicio_altura_fila = $cY;
                $this->setXY($cX, $cY+0.7);
                $this->MultiCell($width, 4, $this->sma->utf8Decode($pg->name), '', 'C');
                $columns[$n_column]['fin_x'] = $cX+$width;
                    if (!isset($fin_altura_fila) || (isset($fin_altura_fila) && $fin_altura_fila < $this->getY())) {
                        $fin_altura_fila = $this->getY();
                    }
                    $this->setXY($cX+$width, $cY);
                $n_column++;
            }
            $fin_fila_X = $this->getX();
            $fin_fila_Y = $this->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $this->setXY($inicio_fila_X, $inicio_fila_Y);
            // exit(var_dump($columns));
            foreach ($columns as $key => $data) {
                $ancho_column = $data['fin_x'] - $data['inicio_x'];
                if ($altura_fila < 5) {
                    $altura_fila = 5;
                }
                $this->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'TLBR',0,'R');
            }
            $this->ln($altura_fila);
            $this->SetFont('Arial','', $this->fontsize_body);
        }
    }
    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina '.$this->PageNo()." de ".'{nb}'),'',1,'C');
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);
$pdf->fontsize_body = 8.5;
$pdf->fontsize_title = 8.5;
$pdf->SetFont('Arial','', $pdf->fontsize_body);
$pdf->Logo = base_url('assets/uploads/logos/'.$biller->logo);

$row = 2;
$current_category_name = null;
$category_id = NULL;
$subcategory_id = NULL;
$pdf->sma = $this->sma;

$pdf->group_by_category_sub_category = $group_by_category_sub_category;
$pdf->price_groups = $price_groups;

$pdf->AddPage();
foreach ($products as $product) {
    if ($pdf->getY() > 240) {
        $pdf->AddPage();
    }
    if ($group_by_category_sub_category && ($category_id == NULL || $product->category_id != $category_id)) {
        $subcategory_id = NULL;
        $pdf->setX(7);
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $columns[0]['inicio_x'] = $pdf->getX();
        $pdf->cell(60.8 , 8, $this->sma->utf8Decode($product->category_name), '', 0, 'C');
        $columns[0]['fin_x'] = $pdf->getX();
        $width = (141.2 / count($price_groups));
        $n_column = 1;
        $fin_altura_fila = 0;
        foreach ($price_groups as $pg) {
            $columns[$n_column]['inicio_x'] = $pdf->getX();
                $cX = $pdf->getX();
                $cY = $pdf->getY();
                $inicio_altura_fila = $cY;
            $pdf->setXY($cX, $cY+0.7);
            $pdf->MultiCell($width, 4, $this->sma->utf8Decode($pg->name), '', 'C');
            $columns[$n_column]['fin_x'] = $cX+$width;
                $fin_altura_fila = $pdf->getY() > $fin_altura_fila ? $pdf->getY() : $fin_altura_fila;
                $pdf->setXY($cX+$width, $cY);
            $n_column++;
        }
        $fin_fila_X = $pdf->getX();
        $fin_fila_Y = $pdf->getY();
        $ancho_fila = $fin_fila_X - $inicio_fila_X;
        $altura_fila = $fin_altura_fila - $inicio_altura_fila;
        $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
        // exit(var_dump($columns));
        foreach ($columns as $key => $data) {
            $ancho_column = $data['fin_x'] - $data['inicio_x'];
            if ($altura_fila < 5) {
                $altura_fila = 5;
            }
            $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'TLBR',0,'R');
        }
        $pdf->ln($altura_fila);
        $pdf->SetFont('Arial','', $pdf->fontsize_body);
        $category_id = $product->category_id;
    }
    if ($group_by_category_sub_category && ($product->subcategory_id != NULL && ($subcategory_id == NULL || $product->subcategory_id != $subcategory_id))) {
        $pdf->setX(7);
        $pdf->SetFont('Arial','B', $pdf->fontsize_title);
        $pdf->cell(29.44, 6, $this->sma->utf8Decode($product->subcategory_name), '', 1, 'C');
        $subcategory_id = $product->subcategory_id;
        $pdf->SetFont('Arial','', $pdf->fontsize_body);
    }
    $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $pdf->getX();
        $cX = $pdf->getX();
        $cY = $pdf->getY();
        $inicio_altura_fila = $cY;
    $pdf->setXY($cX, $cY+0.7);
    $pdf->MultiCell(60.8, 4, $this->sma->utf8Decode($product->name), '', 'L');
    $columns[0]['fin_x'] = $cX+60.8;
        $fin_altura_fila = $pdf->getY();
        $pdf->setXY($cX+60.8, $cY);
    $width = (141.2 / count($price_groups));
    $n_column = 1;
    foreach ($price_groups as $pg) {
        $class = '';
        $columns[$n_column]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.7);
        if ($show_related_unit) {
            $unit_name =(isset($price_groups_units[$pg->id][$product->id]) ? " ".$price_groups_units[$pg->id][$product->id] : (isset($products_price_groups[$product->id][$pg->id]) ? " ".$product->unit_name : ""));
        }

        $pdf->MultiCell($width, 4, $this->sma->utf8Decode((isset($products_price_groups[$product->id][$pg->id]) ? ($this->sma->formatMoney($products_price_groups[$product->id][$pg->id])) : '-').($show_related_unit ? $unit_name : "")), '', 'L');
        $columns[$n_column]['fin_x'] = $cX+$width;
            $fin_altura_fila = $pdf->getY() > $fin_altura_fila ? $pdf->getY() : $fin_altura_fila;
            $pdf->setXY($cX+$width, $cY);
        $n_column++;
    }
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    // exit(var_dump($columns));
    foreach ($columns as $key => $data) {
        $ancho_column = $data['fin_x'] - $data['inicio_x'];
        if ($altura_fila < 5) {
            $altura_fila = 5;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'TLBR',0,'R');
    }
    $pdf->ln($altura_fila);
}
// $pdf->cell(235.02, 4, $this->sma->utf8Decode("Gran total del informe : "), 'T', 0, 'R');
// $pdf->cell(30.8, 4, $this->sma->utf8Decode($this->sma->formatMoney($total_balance)), 'T', 1, 'R');
$descargar = false;
if ($descargar) {
  $pdf->Output("INFORME_LISTA_PRECIOS.pdf", "D");
} else {
  $pdf->Output("INFORME_LISTA_PRECIOS.pdf", "I");
}