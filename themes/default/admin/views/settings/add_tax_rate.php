<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-lg">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= lang('add_tax_rate'); ?></h3>
        </div>
        <?= admin_form_open("system_settings/add_tax_rate", ["id" => "add_tax_rate_form"]); ?>
            <div class="modal-body">
                <!-- <p><?= lang('enter_info'); ?></p> -->
                <div class="form-group">
                    <label for="type"><?= $this->lang->line("tax_rate_code_fe"); ?></label>
                    <div class="controls">
                        <select class="form-control" name="code_fe" id="code_fe" required="required">
                            <option value=""><?= lang("select"); ?></option>
                            <?php foreach ($tax_codes_fe as $tax_code_fe) { ?>
                                 <option value="<?= $tax_code_fe->codigo ?>"><?= $tax_code_fe->nombre ." - ". $tax_code_fe->descripcion; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="name"><?= $this->lang->line("name"); ?></label>
                    <div class="controls">
                        <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="code"><?= $this->lang->line("code"); ?></label>
                    <div class="controls">
                        <?= form_input('code', '', 'class="form-control" id="code"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="rate"><?= $this->lang->line("rate"); ?></label>
                    <div class="controls">
                        <?= form_input('rate', '', 'class="form-control" id="rate" required="required"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="type"><?= $this->lang->line("type"); ?></label>
                    <div class="controls">
                        <?php $type = array('1' => lang('percentage'), '2' => lang('fixed')); ?>
                        <?= form_dropdown('type', $type, '', 'class="form-control" id="type" required="required"'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="tax_indicator"><?= $this->lang->line("tax_indicator"); ?></label>
                    <div class="controls">
                        <?= form_input('tax_indicator', '', 'class="form-control" id="tax_indicator"'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label for="excluded">
                        <input type="checkbox" name="excluded" id="excluded"> &nbsp; Marcar como excluido
                    </label>
                </div>
                            
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary new-button" type="button" name="add_tax_rate" id="add_tax_rate" data-toggle="tooltip" data-placement="top" title="<?=lang('save') ?>">
                    <i class="fa fa-check"></i>
                </button>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('click', '#add_tax_rate', guardar_impuesto);

        $("#add_tax_rate_form").validate({
            ignore: []
        });

        $('[data-toggle="tooltip"]').tooltip();

        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });
    });

    function guardar_impuesto() {
        if ($('#add_tax_rate_form').valid()) {
            $('#add_tax_rate_form').submit();
        }
    }
</script>