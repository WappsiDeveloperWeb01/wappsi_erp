$(document).ready(function(){

    $("#add_guest_register_form").validate({
          ignore: []
    });
    $("#edit_guest_register_form").validate({
          ignore: []
    });
	if (gstbiller = localStorage.getItem('gstbiller')) {
		$('#gstbiller').select2('val', gstbiller).trigger('change');
	} else {
		$('#gstbiller').trigger('change');
	}
	if (gstcustomer = localStorage.getItem('gstcustomer')) {
		nsCustomer(gstcustomer);
	} else {
		nsCustomer();
	}
    if (gstroom = localStorage.getItem('gstroom')) {
        $('#gstroom').select2('val', gstroom).trigger('change');
    } else {
        $('#gstroom').trigger('change');
    }
    if (gstnights_no = localStorage.getItem('gstnights_no')) {
        if (edit) {
            $('#gstnights_no').val(gstnights_no);
        } else {
            $('#gstnights_no').val(gstnights_no).trigger('change');
        }
    } else {
        $('#gstnights_no').trigger('change');
    }
}); //fin document ready
$(document).on('change', '#gstcustomer', function(){
    localStorage.setItem('gstcustomer', $(this).val());
    set_customer_info();
});
$(document).on('change', '#gstcustomerbranch', function(){
    set_customer_branch_info();
});
var price = max_occupancy = nights_no = guests_no = null;
$(document).on('change', '#gstroom', function(){
    localStorage.setItem('gstroom', $(this).val());
    max_occupancy = $('#gstroom option:selected').data('maxoccupancy');
    price = $('#gstroom option:selected').data('price');
    if (max_occupancy > 0) {
        $('#gstguests_no').prop('max', max_occupancy).prop('readonly', false).val(max_occupancy).trigger('change');
        $('#gstguests_no').empty();
        for (var i = 1; i <= max_occupancy; i++) {
            $("<option />", {value: i, text: i}).appendTo($('#gstguests_no'));
        }
        if (localStorage.getItem('gstguests_no')) {
            $('#gstguests_no').select2('val', localStorage.getItem('gstguests_no'));
        }
        $('#gstguests_no').select2().trigger('change');
    }
    if (localStorage.getItem('gsttotal')) {
        price = formatDecimal(localStorage.getItem('gsttotal'));
        $('#gstnights_no').val(1);
        $('#gsttotal').val(formatDecimal( price ));
    } else if (price > 0) {
        $('#gstnights_no').val(1);
        $('#gsttotal').val(formatDecimal(price));
    }
});
$(document).on('keyup change', '#gstguests_no', function(){
    guests_no = $(this).val();
    max_guests_no = $('#gstguests_no').prop('max');
    if (guests_no > 0 && formatDecimal(max_guests_no) < formatDecimal(guests_no)) {
        $(this).val(max_guests_no);
        guests_no = max_guests_no;
    } else if (guests_no <= 0) {
        $(this).val(1);
        max_guests_no = 1;
    }
    loadGuests();
});
$(document).on('keyup change', '#gstnights_no', function(){
    localStorage.setItem('gstnights_no', $(this).val());
    nights_no = $(this).val();
    if (nights_no > 0) {
        $('#gsttotal').val(formatDecimal(price * nights_no));
    }
});
$(document).on('click', '#reset', function(){
    if (localStorage.getItem('gstbiller')) {
        localStorage.removeItem('gstbiller');
    }
    if (localStorage.getItem('gstcustomer')) {
        localStorage.removeItem('gstcustomer');
    }
    if (localStorage.getItem('gstroom')) {
        localStorage.removeItem('gstroom');
    }
    if (localStorage.getItem('gstnights_no')) {
        localStorage.removeItem('gstnights_no');
    }
    location.reload();
});

$(document).on('click', '#add_guest_register', function(){
    if ($('#add_guest_register_form').valid()) {
        $('#add_guest_register_form').submit();
    }
});

$(document).on('click', '#edit_guest_register', function(){
    if ($('#edit_guest_register_form').valid()) {
        $('#edit_guest_register_form').submit();
    }
});

$(document).on('change', '.vat_no', function(){
    if ($(this).val() != "") {
        index = $(this).index('.vat_no');
        console.log(validate_no_repeat_guest(index));
        console.log(validate_no_in_another_open_register(index));
        if (validate_no_repeat_guest(index) && validate_no_in_another_open_register(index)) {
            set_existing_guest(index, 1);
        } else {
            $(this).val('');
        }
    }
});

$(document).on('change', '.document_type', function(){
    if ($(this).val() != "") {
        index = $(this).index('select.document_type');
        console.log(validate_no_repeat_guest(index));
        console.log(validate_no_in_another_open_register(index));
        if (validate_no_repeat_guest(index) && validate_no_in_another_open_register(index)) {
            set_existing_guest(index, 2);
        } else {
            $('input[name="vat_no[]"]').eq(index).val('');
        }
    }
});
function validate_no_in_another_open_register(index){
    document_type = $('select[name="document_type[]"]').eq(index);
    vat_no = $('input[name="vat_no[]"]').eq(index);
    var error = 0;
    if (vat_no != "") {
        $.ajax({
            url : site.base_url+"hotel/validate_no_in_another_open_register",
            dataType : 'JSON',
            data : {
                document_type : document_type.val(),
                vat_no : vat_no.val(),
            }
        }).done(function(data){
            if (JSON.parse(data.exists) == true) {
                command: toastr.error('El huésped ya está asociado a otro registro activo.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
                error = 1;
            } else {
                error = 0;
            }
        });
    } else {
        error = 0;
    }
    if (error == 0) {
        return true;
    } else{
        return false;
    }
        
}
function validate_no_repeat_guest(index){
    prev_document_type = $('select[name="document_type[]"]').eq(index);
    prev_vat_no = $('input[name="vat_no[]"]').eq(index);
    var error = 0;
    $.each($('input[name="vat_no[]"]'), function(iindex, row){
        if (iindex != index) {
            document_type = $('select[name="document_type[]"]').eq(iindex);
            vat_no = $('input[name="vat_no[]"]').eq(iindex);
            if (document_type.val() == prev_document_type.val() && vat_no.val() == prev_vat_no.val()) {
                error++;
            }
        }
    });
    if (error == 0) {
        return true;
    } else {
        command: toastr.error('No puede diligenciar el huésped más de una vez.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
        return false;
    }
}
function set_existing_guest(index, type){
    document_type = $('select[name="document_type[]"]').eq(index);
    vat_no = $('input[name="vat_no[]"]').eq(index);
    names = $('input[name="names[]"]').eq(index);
    surnames = $('input[name="surnames[]"]').eq(index);
    nationality = $('input[name="nationality[]"]').eq(index);
    phone = $('input[name="phone[]"]').eq(index);
    email = $('input[name="email[]"]').eq(index);
    birth_date = $('input[name="birth_date[]"]').eq(index);
    action = $('input[name="action[]"]').eq(index);
    guest_id = $('input[name="guest_id[]"]').eq(index);
    $.ajax({
        url : site.base_url+"hotel/validate_existing_guest",
        type : "get",
        data : {
            document_type : document_type.val(),
            vat_no : vat_no.val(),
        },
        dataType : 'JSON',
    }).done(function(data){
        if (data.response) {
            names.val(data.data.names);
            surnames.val(data.data.surnames);
            nationality.val(data.data.nationality);
            phone.val(data.data.phone);
            email.val(data.data.email);
            birth_date.val(data.data.birth_date);
            action.val(2);
            guest_id.val(data.data.id);
        } else {
            names.val('');
            surnames.val('');
            nationality.val('');
            phone.val('');
            email.val('');
            birth_date.val('');
            action.val(1);
            guest_id.val('');
        }
    });
}
function verify_locked_submit(){
    // if (localStorage.getItem('locked_for_provider')) {
    //     $('#add_sale').attr('disabled', true);
    // } else {
    //     $('#add_sale').attr('disabled', false);
    // }
}
function nsCustomer(gstcustomer = null){
	if (gstcustomer) {
		$('#gstcustomer').val(gstcustomer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#gstcustomer').trigger('change');
	} else {
		$('#gstcustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
	}
}
function loadGuests(){
    $("#gstTable tbody").empty();
    for (var i = 1; i <= guests_no; i++) {
        var newTr = $('<tr></tr>');
        tr_html = "<td class='dts_"+i+"'>"+
                    ""+
                  "</td>";
        tr_html += "<td>"+
                    "<input name='vat_no[]' type='text' class='form-control vat_no'>"+
                    "<input name='action[]' type='hidden' value='1'>"+
                    "<input name='guest_id[]' type='hidden'>"+
                  "</td>";
        tr_html += "<td>"+
                    "<input name='names[]' type='text' class='form-control'>"+
                  "</td>";
        tr_html += "<td>"+
                    "<input name='surnames[]' type='text' class='form-control'>"+
                  "</td>";
        tr_html += "<td>"+
                    "<input name='nationality[]' type='text' class='form-control'>"+
                  "</td>";
        tr_html += "<td>"+
                    "<input name='phone[]' type='text' class='form-control'>"+
                  "</td>";
        tr_html += "<td>"+
                    "<input name='email[]' type='text' class='form-control'>"+
                  "</td>";
        tr_html += "<td>"+
                    "<input name='birth_date[]' type='date' class='form-control'>"+
                  "</td>";
        newTr.html(tr_html);
        newTr.appendTo("#gstTable");
        uopt = $("<select name=\"document_type[]\" class=\"form-control select document_type\" />");
        $.each(id_document_types, function () {
            $("<option />", {value: this.id, text: this.nombre}).appendTo(uopt);
        });
        $('.dts_'+i).html(uopt);
    }
    $('select.select').select2();
    if (localStorage.getItem('rows')) {
        rows = JSON.parse(localStorage.getItem('rows'));
        $(rows).each(function(index, guest){
            $('select.select[name="document_type[]"]').eq(index).select2('val', guest.document_type_id);
            $('input[name="vat_no[]"]').eq(index).val(guest.vat_no);
            $('input[name="names[]"]').eq(index).val(guest.names);
            $('input[name="surnames[]"]').eq(index).val(guest.surnames);
            $('input[name="nationality[]"]').eq(index).val(guest.nationality);
            $('input[name="phone[]"]').eq(index).val(guest.phone);
            $('input[name="email[]"]').eq(index).val(guest.email);
            $('input[name="birth_date[]"]').eq(index).val(formatDate(guest.birth_date));
            $('input[name="action[]"]').eq(index).val(2);
            $('input[name="guest_id[]"]').eq(index).val(guest.id);
        });
    }
}
function set_customer_info(){
    $.ajax({
        url: site.base_url+"sales/getCustomerAddresses",
        type: "get",
        data: {"customer_id" : $('#gstcustomer').val()}
    }).done(function(data){
        if (data != false) {
            $('#gstcustomerbranch').html(data);
            if (gstcustomerbranch = localStorage.getItem('gstcustomerbranch')) {
                $('#gstcustomerbranch option').each(function(index, option){
                    if ($(option).val() == gstcustomerbranch) {
                        $(option).prop('selected', 'selected');
                    }
                });
            }
        } else {
            $('#gstcustomerbranch').select2().select2('val', '');
        }
        $('#gstcustomerbranch').trigger('change');
    });
}

function set_customer_branch_info(){
    gstcustomerbranch = $('#gstcustomerbranch').val();
    $.ajax({
        url : site.base_url+"customers/get_customer_branch_details/"+gstcustomerbranch,
        dataType:"JSON"
    }).done(function(data){
        $('.customer_name').text(data.sucursal);
        $('.customer_address').text(data.direccion);
        $('.customer_phone').text(data.phone);
        $('.customer_city').text(data.city);
        $('.customer_info').fadeIn();
    });
}