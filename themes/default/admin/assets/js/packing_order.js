$(document).ready(function(){

	if (pckproduction_order = localStorage.getItem('pckproduction_order')) {
		nsProductionOrder(pckproduction_order);
	} else {
		nsProductionOrder();
	}

    if (pckcutting_order = localStorage.getItem('pckcutting_order')) {
        nsCuttingOrder(pckcutting_order);
    } else {
        nsCuttingOrder();
    }

    if (pckassemble_order = localStorage.getItem('pckassemble_order')) {
        nsAssembleOrder(pckassemble_order);
    } else {
        nsAssembleOrder();
    }

    if (pcksupplier = localStorage.getItem('pcksupplier')) {
        $('#pcksupplier').val(pcksupplier).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        ptype: 4,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#pcksupplier').trigger('change');
    } else {
        nsSupplier();
    }

	if (pckdate = localStorage.getItem('pckdate')) {
		$('#pckdate').val(pckdate);
	}
	if (pckest_date = localStorage.getItem('pckest_date')) {
		$('#pckest_date').val(pckest_date);
	}
	if (pckreference_no = localStorage.getItem('pckreference_no')) {
		$('#pckreference_no').val(pckreference_no);
	}
	if (assembles_id) {
        remove_local_storage();
        $.ajax({
            url : site.base_url+'production_order/get_assemble_order_items/0/0/0',
            data : {
                assembles_id : assembles_id.ids,
            },
            dataType : 'JSON'
        }).done(function(data){
            localStorage.setItem('pckitems', JSON.stringify(data.por_rows));
            localStorage.setItem('pckcomproducts', JSON.stringify(data.rows));
            localStorage.setItem('pckproduction_order', data.production_order_id);
            nsProductionOrder(data.production_order_id);
            loadItems();
        });
    }
});

$(document).on('change', '#pckproduction_order', function(){
    if (assembles_id == false && !$('#pckcutting_order').val() && !$('#pckassemble_order').val()) {
        $.ajax({
            url : site.base_url+'production_order/get_assemble_order_items/0/'+$(this).val(),
            dataType : 'JSON'
        }).done(function(data){
            localStorage.setItem('pckitems', JSON.stringify(data.por_rows));
            localStorage.setItem('pckcomproducts', JSON.stringify(data.rows));
            $('#pckwarehouse').select2('val', data.warehouse_id);
            $('#pckbiller').select2('val', data.biller_id).select2('readonly', true).trigger('change');
            loadItems();
        });
        localStorage.setItem('pckproduction_order', $(this).val());
    }
    $('#pckproduction_order_reference_no').val($('#pckproduction_order').select2('data').text);
});

$(document).on('change', '#pckcutting_order', function(){
    if (!$('#pckassemble_order').val()) {
        $.ajax({
            url : site.base_url+'production_order/get_assemble_order_items/0/0/'+$(this).val(),
            dataType : 'JSON'
        }).done(function(data){
            localStorage.setItem('pckitems', JSON.stringify(data.por_rows));
            localStorage.setItem('pckcomproducts', JSON.stringify(data.rows));
            localStorage.setItem('pckproduction_order', data.production_order_id);
            nsProductionOrder(data.production_order_id);
            $('#pckwarehouse').select2('val', data.warehouse_id);
            $('#pckbiller').select2('val', data.biller_id).select2('readonly', true).trigger('change');
            loadItems();
        });
        localStorage.setItem('pckcutting_order', $(this).val());
    }
    $('#pckcutting_order_reference_no').val($('#pckcutting_order').select2('data').text);
});

$(document).on('change', '#pckassemble_order', function(){
    $.ajax({
        url : site.base_url+'production_order/get_assemble_order_items/'+$(this).val(),
        dataType : 'JSON'
    }).done(function(data){
        localStorage.setItem('pckitems', JSON.stringify(data.por_rows));
        localStorage.setItem('pckcomproducts', JSON.stringify(data.rows));
        localStorage.setItem('pckproduction_order', data.production_order_id);
        nsProductionOrder(data.production_order_id);
        nsCuttingOrder(data.cutting_id);
        $('#pckwarehouse').select2('val', data.warehouse_id);
        $('#pckbiller').select2('val', data.biller_id).select2('readonly', true).trigger('change');
        loadItems();
    });
    localStorage.setItem('pckassemble_order', $(this).val());
    $('#pckassemble_order_reference_no').val($('#pckassemble_order').select2('data').text);
});


$(document).on('change', '#pcksupplier', function(){
	localStorage.setItem('pcksupplier', $(this).val());
});
$(document).on('change', '#pckest_date', function(){
	localStorage.setItem('pckest_date', $(this).val());
});
$(document).on('change', '#pckdate', function(){
	localStorage.setItem('pckdate', $(this).val());
});
$(document).on('change', '#pckreference_no', function(){
	localStorage.setItem('pckreference_no', $(this).val());
});

function nsProductionOrder(por_id = false){
    if (por_id != false) {
        $('#pckproduction_order').val(por_id).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"production_order/get_production_order/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        setTimeout(function() {
                            $("#pckdate").datetimepicker('remove').datetimepicker({
                                format: site.dateFormats.js_ldate,
                                fontAwesome: true,
                                language: 'sma',
                                weekStart: 1,
                                todayBtn: 1,
                                autoclose: 1,
                                todayHighlight: 1,
                                startView: 2,
                                forceParse: 0,
                                enableOnReadonly : false,
                                startDate : data[0].date,
                                endDate : data[0].est_date
                            });
                            // $("#pckdate").val(data[0].date);
                            $("#pckest_date").datetimepicker('remove').datetimepicker({
                                format: site.dateFormats.js_ldate,
                                fontAwesome: true,
                                language: 'sma',
                                weekStart: 1,
                                todayBtn: 1,
                                autoclose: 1,
                                todayHighlight: 1,
                                startView: 2,
                                forceParse: 0,
                                enableOnReadonly : false,
                                startDate : data[0].date,
                                endDate : data[0].est_date
                            });
                            $("#pckest_date").val(data[0].est_date);
                        }, 850);
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "production_order/production_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#pckproduction_order').trigger('change');
    } else {
        $('#pckproduction_order').select2({
            minimumInputLength: 1,
            width: '100%',
            ajax: {
                url: site.base_url + "production_order/production_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }
}

function nsCuttingOrder(cutting_id = false){
    if (cutting_id) {
        $('#pckcutting_order').val(cutting_id).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"production_order/get_cutting_order/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "production_order/cutting_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#pckcutting_order').trigger('change');
    } else {
        $('#pckcutting_order').select2({
            minimumInputLength: 1,
            width: '100%',
            ajax: {
                url: site.base_url + "production_order/cutting_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }
}

function nsAssembleOrder(assemble_id = false){
    if (assemble_id) {
        $('#pckassemble_order').val(assemble_id).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"production_order/get_assemble_order/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "production_order/assemble_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#pckassemble_order').trigger('change');
    } else {
        $('#pckassemble_order').select2({
            minimumInputLength: 1,
            width: '100%',
            ajax: {
                url: site.base_url + "production_order/assemble_order_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        POR_SUGGESTIONS_TYPE: POR_SUGGESTIONS_TYPE,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }
}

function nsSupplier(){
	$('#pcksupplier').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    ptype: 6,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function loadItems() {
    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }

    if (localStorage.getItem('pckitems')) {
        total_order_quantity = 0;
        total_assembled_quantity = 0;
        an = 1;
        Ttotal = 0;
        $("#pckTable tbody").empty();
        pckitems = JSON.parse(localStorage.getItem('pckitems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(pckitems, function(o){return [parseInt(o.order)];}) : pckitems;
        if (comproducts !== undefined) { delete comproducts; }
        var comproducts = [];
        $.each(sortedItems, function () {
            var item = this;
            var item_id = item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.product_id, item_qty = item.quantity, item_assembled_qty = item.assembled_quantity, item_code = item.product_code, item_name = item.product_name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><input name="product_name[]" type="hidden" value="' + item_name + '"><span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name +'</span>'+
                        '</td>';
            tr_html += '<td>'+lang[item.type]+'</td>';
            tr_html += '<td>'+(item.variant != null ? item.variant : '')+'</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_assembled_qty, decimals) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" readonly>'+
                        '</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_assembled_qty, decimals) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" readonly>'+
                        '</td>';
            tr_html += '<td class="text-center"><i class="fa fa-times tip pckdel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#pckTable");
            total_order_quantity += parseFloat(item_assembled_qty);
            total_assembled_quantity += parseFloat(item_assembled_qty);
            an++;
        });
        var col = 4;
        var tfoot = '<tr id="tfoot" class="tfoot active">'+(site.settings.set_adjustment_cost ? '<th></th>' : '')+
                        '<th colspan="2">Total</th>'+
                        '<th class="text-center">' + formatQuantity2(parseFloat(total_order_quantity)) + '</th>'+
                        '<th class="text-center">' + formatQuantity2(parseFloat(total_assembled_quantity)) + '</th>'+
                        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#pckTable tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        set_page_focus();
        loadCompositionItems();
    }
}

function loadCompositionItems(){
	comproducts = JSON.parse(localStorage.getItem('pckcomproducts'));
    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }
    $("#pckCTable tbody").empty();
    console.log(comproducts);
    count = 1;
    Ttotal = 0;
    var order_qty = 0;
    var finished_qty = 0;
    var process_qty = 0;
    var packing_qty = 0;
    $.each(comproducts, function (index, item) {
        if (item === undefined) { return; }
        var trclass = 'class="cproduct_'+item.id+'"';
        var newTr = $('<tr></tr>');
        tr_html = '<td '+trclass+'>'+
                        '<input name="cp_product_id[]" type="hidden" class="rid" value="' + item.product_id + '"><input name="cp_product_name[]" type="hidden" value="' + item.product_name + '"><span class="sname">' + item.product_code +' - ' + item.product_name +'</span>'+
                        '<input name="cp_assemble_detail_id[]" type="hidden" class="rid" value="' + (item.assemble_detail_id != "NULL" ? item.assemble_detail_id : null) + '">'+
                        '<input name="cp_production_order_pid[]" type="hidden" class="rid" value="' + item.production_order_pid + '">'+
                        '<input name="cp_production_order_option_id[]" type="hidden" class="rid" value="' + item.option_id + '">'+
                        '<input name="cp_pfinished_ciq_num[]" type="hidden" class="rid" value="' + item.ciq_num + '">'+
                    '</td>';
        tr_html += '<td>'+
                            '<span>' + item.por_product_name +' - ' + item.por_product_code + (item.por_variant != null ? " - "+item.por_variant : "") + ' </span>'+
                        '</td>';
        tr_html += '<td '+trclass+'>'+lang[item.type]+'</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center order_quantity" name="order_quantity[]" type="text" value="' + formatDecimal((item.assembling_quantity - item.fault_quantity), decimals) + '" readonly>'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center finished_quantity" name="finished_quantity[]" type="text" value="' + formatDecimal((item.packing_finished_quantity ? item.packing_finished_quantity : 0), decimals) + '" readonly>'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center in_process_quantity" name="in_process_quantity[]" type="text" value="' + formatDecimal((item.packing_in_process_quantity ? item.packing_in_process_quantity : 0) - (item.packing_finished_quantity ? item.packing_finished_quantity : 0), decimals) + '" readonly>'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input type="hidden" name="item_composition_qty[]" value="'+item.composition_quantity+'">'+
                        '<input type="hidden" name="item_pfinished_id[]" value="'+item.production_order_pfinished_item_id+'">'+
                        '<input type="hidden" name="item_product_id[]" value="'+item.product_id+'">'+
                        '<input type="hidden" name="item_pfinished_product_id[]" value="'+item.pfinished_product_id+'">'+
                        '<input type="hidden" name="item_pfinished_option_id[]" value="'+item.option_id+'">'+
                        '<input class="form-control text-center to_packing_quantity" name="to_packing_quantity[]" type="text" value="0" max="'+((item.finished_quantity) - (item.packing_in_process_quantity ? item.packing_in_process_quantity : 0))+'" id="cqty_'+item.id+'" '+(item.no_packing !== undefined ? 'readonly' : '')+
                        // value="' + (item.no_packing !== undefined ? 0 : formatDecimal((item.finished_quantity) - (item.packing_in_process_quantity ? item.packing_in_process_quantity : 0), decimals)) + '"
                        ' data-pfinishedpid="'+item.production_order_pfinished_item_id+'" data-optionid="'+item.option_id+'" data-compositionqty="'+item.composition_quantity+'"'+
                        '>'+
                        '<label class="error" for="cqty_'+item.id+'" style="display:none;"></label>'+
                    '</td>';
        newTr.html(tr_html);
        newTr.prependTo("#pckCTable");
        count += parseFloat(item.qty);
        order_qty += parseFloat((item.assembling_quantity - item.fault_quantity));
        finished_qty += parseFloat(item.packing_finished_quantity ? item.packing_finished_quantity : 0);
        process_qty += parseFloat((item.packing_in_process_quantity ? item.packing_in_process_quantity : 0) - (item.packing_finished_quantity ? item.packing_finished_quantity : 0));
        packing_qty += parseFloat((item.no_packing !== undefined ? 0 : formatDecimal((item.finished_quantity) - (item.packing_in_process_quantity ? item.packing_in_process_quantity : 0), decimals)));
    });
    var tfoot = '<tr id="tfoot" class="tfoot active">'+
                    '<th colspan="3">Total</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(order_qty)) + '</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(finished_qty)) + '</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(process_qty)) + '</th>'+
                    '<th class="text-center total_manual" ></th>'+
                '</tr>';
    $('#pckCTable tfoot').html(tfoot);
}

$('#reset').click(function (e) {
    bootbox.confirm(lang.r_u_sure, function (result) {
    if (result) {
        remove_local_storage();
        $('#modal-loading').show();
        location.reload();
    }
    });
});

function remove_local_storage(){
    if (localStorage.getItem('pckitems')) {
        localStorage.removeItem('pckitems');
    }
    if (localStorage.getItem('pckcomproducts')) {
        localStorage.removeItem('pckcomproducts');
    }
    if (localStorage.getItem('pckdate')) {
        localStorage.removeItem('pckdate');
    }
    if (localStorage.getItem('pckest_date')) {
        localStorage.removeItem('pckest_date');
    }
    if (localStorage.getItem('pckproduction_order')) {
        localStorage.removeItem('pckproduction_order');
    }
    if (localStorage.getItem('pckcutting_order')) {
        localStorage.removeItem('pckcutting_order');
    }
    if (localStorage.getItem('pckassemble_order')) {
        localStorage.removeItem('pckassemble_order');
    }
    if (localStorage.getItem('pckreference_no')) {
        localStorage.removeItem('pckreference_no');
    }
    if (localStorage.getItem('pcksupplier')) {
        localStorage.removeItem('pcksupplier');
    }
}
$(document).on('keyup', '.to_packing_quantity', function(){
    findex = $('.to_packing_quantity').index($(this));
    to_packing_quantity_input = $(this);
    pfinishedpid = to_packing_quantity_input.data('pfinishedpid');
    optionid = to_packing_quantity_input.data('optionid');
    modifiedcompositionqty = to_packing_quantity_input.data('compositionqty');
    if (parseFloat(to_packing_quantity_input.val()) > parseFloat(to_packing_quantity_input.prop('max'))) {
        to_packing_quantity_input.val(to_packing_quantity_input.prop('max'));
        command: toastr.error('El valor digitado está fuera del margen permitido.', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
    }
    $.each($('input[name="to_packing_quantity[]"][data-pfinishedpid="'+pfinishedpid+'"][data-optionid="'+optionid+'"]'), function(index, each_to_packing_quantity_input){
        f2index = $('.to_packing_quantity').index($(each_to_packing_quantity_input));
        if (findex != f2index) {
            compositionqty = parseFloat($(each_to_packing_quantity_input).data('compositionqty'));
            totalqty = compositionqty;
            if (modifiedcompositionqty > compositionqty) {
                totalqty = to_packing_quantity_input.val() / modifiedcompositionqty;
            } else if (modifiedcompositionqty < compositionqty) {
                totalqty = to_packing_quantity_input.val() * compositionqty;
            } else if (modifiedcompositionqty == compositionqty) {
                totalqty = to_packing_quantity_input.val();
            }
            $(each_to_packing_quantity_input).val(totalqty);
        }
    });
    var total_to_packing_quantity = 0;
    $.each($('.to_packing_quantity'), function(index, item){
        total_to_packing_quantity += parseFloat($(item).val());
    });
    $('.total_manual').html(formatQuantity2(total_to_packing_quantity));
});