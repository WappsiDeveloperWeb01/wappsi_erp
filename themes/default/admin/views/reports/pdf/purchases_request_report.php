<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Informe para solicitud de compras'));
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->setY(7);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->Image($this->Logo,22,6,30,0);
        $this->setXY($cx+76.06, $cy);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Informe para solicitud de compras'), '', ($this->wh_data ? 0 : 1), 'C');
        $this->setXY($cx, $cy+5);
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Fecha impresión : '.date('Y-m-d H:i:s')), '', ($this->wh_data ? 0 : 1), 'C');
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        if ($this->wh_data) {
            $this->setXY($cx+111.97, $cy);
            $this->cell(75.97, 5, $this->sma->utf8Decode('Sucursal : '.$this->wh_data->name), '', 1, 'C');
        }
        $this->ln();
        $this->SetFont('Arial','B', $this->fontsize_title);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->cell(20.44, 4, $this->sma->utf8Decode('Código'), 'TLR', 0, 'C');
        $this->cell(40.44, 8, $this->sma->utf8Decode('Nombre Producto'), 'TLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('Cantidad'), 'TLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('Cantidad'), 'TLR', 0, 'C');
        $this->cell(10   , 4, $this->sma->utf8Decode('UM'), 'TLR', 0, 'C');
        $this->cell(36.44, 4, $this->sma->utf8Decode('Cantidad'), 'TLR', 0, 'C');
        $this->cell(40.44, 8, $this->sma->utf8Decode('Proveedor'), 'TLR', 0, 'C');
        $this->cell(25.44, 8, $this->sma->utf8Decode('Valor'), 'TLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('Unidad'), 'TLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('Cantidad'), 'TLR', 0, 'C');
        $this->cell(15, 4, $this->sma->utf8Decode('Valor'), 'TLR', 0, 'C');
        $this->cell(15, 4, $this->sma->utf8Decode('Cantidad'), 'TLR', 1, 'C');
        $this->setXY($cx, $cy+4);
        $this->cell(20.44, 4, $this->sma->utf8Decode('Producto'), 'BLR', 0, 'C');
        $this->cell(40.44, 4, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('pedida'), 'BLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('existencia'), 'BLR', 0, 'C');
        $this->cell(10   , 4, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $this->cell(36.44, 4, $this->sma->utf8Decode('a comprar'), 'BLR', 0, 'C');
        $this->cell(40.44, 4, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $this->cell(25.44, 4, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('Medida'), 'BLR', 0, 'C');
        $this->cell(15.44, 4, $this->sma->utf8Decode('comprada'), 'BLR', 0, 'C');
        $this->cell(15, 4, $this->sma->utf8Decode('Total'), 'BLR', 0, 'C');
        $this->cell(15, 4, $this->sma->utf8Decode('Recibida'), 'BLR', 1, 'C');
    }
    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina '.$this->PageNo()." de ".'{nb}'),'',1,'C');
    }
    function get_quantity_to_purchase(){
        $quantity_to_purchase  = ($this->data_row->product_quantity - $this->data_row->quantity_request) * -1;
        $quantity_to_purchase = $quantity_to_purchase > 0 ? $quantity_to_purchase : 0;
        $quantity_to_purchase_process = $quantity_to_purchase > 0 ? $quantity_to_purchase : 0;
        $text_qty = "";
        if ($this->data_row->all_units) {
            foreach ($this->data_row->all_units as $dr_unit) {
                if ($quantity_to_purchase_process > 0 && ($dr_unit->operation_value > 0 || $dr_unit->num == 1)) {
                    if ($dr_unit->num == 1) {
                        $dr_unit->operation_value = 1;
                    }
                    $dr_unit_qty = $quantity_to_purchase_process / $dr_unit->operation_value;
                    if ($dr_unit->operation_value != 1) {
                        $dr_unit_qty = floor($dr_unit_qty);
                    }
                    $quantity_to_purchase_process -= ($dr_unit_qty * $dr_unit->operation_value);
                    if ($dr_unit_qty > 0) {
                        $text_qty .= $dr_unit_qty." ".$dr_unit->name." | ";
                    }
                }
            }
        }
        return trim($text_qty, "| ");
    }
}
$pdf = new PDF('L', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);
$pdf->fontsize_body = 8.5;
$pdf->fontsize_title = 8.5;
$pdf->SetFont('Arial','', $pdf->fontsize_body);
$pdf->Logo = base_url('assets/uploads/logos/'.$biller->logo);
$row = 2;
$current_product_name = null;
$current_category_name = null;
$current_preference = null;
$category_id = NULL;
$subcategory_id = NULL;
$pdf->sma = $this->sma;
$pdf->wh_data = $wh_data;
$pdf->AddPage();
$total_qtys = [];
if ($data) {
    foreach ($data as $data_row) {
        if (isset($total_qtys[$data_row->product_name])) {
            $total_qtys[$data_row->product_name] += $data_row->quantity_request;
        } else {
            $total_qtys[$data_row->product_name] = $data_row->quantity_request;
        }
    }
    foreach ($data as $data_row) {
        $data_row->preferences = $data_row->preferences ? $this->sma->print_preference_selection($data_row->preferences, true) : "";
        if ($pdf->getY() > 180) {
            $pdf->AddPage();
        }
        if (isset($group_by_category_sub_category) && $group_by_category_sub_category && ($category_id == NULL || $data_row->category_id != $category_id)) {
            $subcategory_id = NULL;
            $pdf->setX(7);
            $pdf->SetFont('Arial','B', $pdf->fontsize_title);
            $pdf->cell(29.44, 6, $this->sma->utf8Decode("Categoría : ".$data_row->category_name), '', 1, 'L');
            $category_id = $data_row->category_id;
            $row++;
        }
        if (isset($group_by_category_sub_category) && $group_by_category_sub_category && ($data_row->subcategory_id != NULL && ($subcategory_id == NULL || $data_row->subcategory_id != $subcategory_id))) {
            $pdf->setX(7);
            $pdf->SetFont('Arial','B', $pdf->fontsize_title);
            $pdf->cell(29.44, 6, $this->sma->utf8Decode("  ".$data_row->subcategory_name), '', 1, 'C');
            $subcategory_id = $data_row->subcategory_id;
            $row++;
        }
        if (($group_by_preferences && ($current_product_name == NULL || ($current_product_name != $data_row->product_name))) || !$group_by_preferences) {

            $pdf->SetFont('Arial','', $pdf->fontsize_body);
            $columns = [];
                    $inicio_fila_X = $pdf->getX();
                    $inicio_fila_Y = $pdf->getY();
                $columns[0]['inicio_x'] = $pdf->getX();
            $pdf->cell(20.44, 6, $this->sma->utf8Decode($data_row->product_code), '', 0, 'L');
                $columns[0]['fin_x'] = $pdf->getX();
                $columns[1]['inicio_x'] = $pdf->getX();
                    $cX = $pdf->getX();
                    $cY = $pdf->getY();
                    $inicio_altura_fila = $cY;
                $pdf->setXY($cX, $cY+0.7);
            $pdf->MultiCell(40.44, 4, $this->sma->utf8Decode($data_row->product_name), '', 'L');
                $columns[1]['fin_x'] = $cX+40.44;
                    $fin_altura_fila = $pdf->getY();
                    $pdf->setXY($cX+40.44, $cY);
                $columns[2]['inicio_x'] = $pdf->getX();
            $pdf->cell(15.44, 6, $this->sma->utf8Decode($this->sma->formatQuantity($total_qtys[$data_row->product_name])), '', 0, 'R');
                $qty_position_x_end = $columns[2]['fin_x'] = $pdf->getX();
                $columns[3]['inicio_x'] = $pdf->getX();
            $pdf->cell(15.44, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->product_quantity)), '', 0, 'R');
                $columns[3]['fin_x'] = $pdf->getX();
                $columns[4]['inicio_x'] = $pdf->getX();
            $pdf->cell(10, 6, $this->sma->utf8Decode($data_row->main_unit_code), '', 0, 'L');
                $columns[4]['fin_x'] = $pdf->getX();
            $pdf->data_row = $data_row;
                $columns[5]['inicio_x'] = $pdf->getX();
                    $cX = $pdf->getX();
                    $cY = $pdf->getY();
                    $inicio_altura_fila = $cY;
                $pdf->setXY($cX, $cY+0.7);
            $pdf->MultiCell(36.44, 4, $this->sma->utf8Decode($pdf->get_quantity_to_purchase()), '','L');
                $columns[5]['fin_x'] = $cX+36.44;
                    $fin_altura_fila = $pdf->getY() > $fin_altura_fila ? $pdf->getY() : $fin_altura_fila;
                    $pdf->setXY($cX+36.44, $cY);
                $columns[6]['inicio_x'] = $pdf->getX();
            $pdf->cell(40.44, 6, $this->sma->utf8Decode(''), '', 0, 'R');
                $columns[6]['fin_x'] = $pdf->getX();
                $columns[7]['inicio_x'] = $pdf->getX();
            $pdf->cell(25.44, 6, $this->sma->utf8Decode(''), '', 0, 'R');
                $columns[7]['fin_x'] = $pdf->getX();
                $columns[8]['inicio_x'] = $pdf->getX();
            $pdf->cell(15.44, 6, $this->sma->utf8Decode(''), '', 0, 'R');
                $columns[8]['fin_x'] = $pdf->getX();
                $columns[9]['inicio_x'] = $pdf->getX();
            $pdf->cell(15.44, 6, $this->sma->utf8Decode(''), '', 0, 'R');
                $columns[9]['fin_x'] = $pdf->getX();
                $columns[10]['inicio_x'] = $pdf->getX();
            $pdf->cell(15, 6, $this->sma->utf8Decode(''), '', 0, 'R');
                $columns[10]['fin_x'] = $pdf->getX();
                $columns[11]['inicio_x'] = $pdf->getX();
            $pdf->cell(15, 6, $this->sma->utf8Decode(''), '', 0, 'R');
                $columns[11]['fin_x'] = $pdf->getX();
                $fin_fila_X = $pdf->getX();
                $fin_fila_Y = $pdf->getY();
                $ancho_fila = $fin_fila_X - $inicio_fila_X;
                $altura_fila = $fin_altura_fila - $inicio_altura_fila;
                $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
                foreach ($columns as $key => $data) {
                    $ancho_column = $data['fin_x'] - $data['inicio_x'];
                    if ($altura_fila < 5) {
                        $altura_fila = 5;
                    }
                    $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'TLBR',0,'R');
                }
                $pdf->ln($altura_fila);
                $current_product_name = $data_row->product_name;
                $current_preference = NULL;
        }
        if ($group_by_preferences && !empty($data_row->preferences)) {
            $prfns = trim(strip_tags($this->sma->decode_html($data_row->preferences)), ", ");
            if ($current_preference == NULL || ($current_preference != $prfns)) {
                $pdf->setX(7);
                // $pdf->SetFont('Arial','B', $pdf->fontsize_title);
                $pdf->cell(60.88, 6, $this->sma->utf8Decode('Preferencia'), 'B', 0, 'L');
                $pdf->cell(15.44, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->quantity_request)), 'B', 0, 'R');
                $pdf->cell(188, 6, $this->sma->utf8Decode($prfns), 'B', 1, 'L');
                $current_preference = $prfns;
                $row++;
            }
        }
        $row++;
    }
}
if ($print_additional_page) {
    $pdf->AddPage();
    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    for ($i=1; $i < 28; $i++) {
        $pdf->cell(20.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(40.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(15.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(15.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(10   , 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(36.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(40.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(25.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(15.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(15.44, 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(15   , 6, $this->sma->utf8Decode(''), 'BLR', 0, 'C');
        $pdf->cell(15   , 6, $this->sma->utf8Decode(''), 'BLR', 1, 'C');
    }
}
$descargar = false;
if ($descargar) {
  $pdf->Output("INFORME_ORDEN_PEDIDOS.pdf", "D");
} else {
  $pdf->Output("INFORME_ORDEN_PEDIDOS.pdf", "I");
}