<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$v = "";
if ($this->input->post('product')) {
    $v .= "&product=" . $this->input->post('product');
}
if ($this->input->post('reference_no')) {
    $v .= "&reference_no=" . $this->input->post('reference_no');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('user')) {
    $v .= "&user=" . $this->input->post('user');
}
if ($this->input->post('serial')) {
    $v .= "&serial=" . $this->input->post('serial');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
?>
<script>
    $(document).ready(function () {
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        oTable = $('#dmpData').dataTable({
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reports/getAdjustmentReport/?v=1' . $v); ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{"mRender": fld}, null, null, null, {"mRender": decode_html}, {"bSortable": false, "mRender": pqFormat}, {"mRender": pqFormat}],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                nRow.id = aData[6];
                nRow.className = "adjustment_link2";
                return nRow;
            },
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('created_by');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang(' note');?>]", filter_type: "text", data: []},
        ], "footer");
        <?php endif ?>
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <?= admin_form_open("reports/unit_prices", 'id="form_filter"'); ?>
                        <div class="row">
                            <input type="hidden" name="generate_report" value="1">
                           <!--  <div class="col-md-3">
                                <label>
                                    <input type="checkbox" name="group_by_category_sub_category">
                                    <?= lang('group_by_category_sub_category') ?>
                                </label>
                            </div> -->
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" name="only_products_with_stock" value="1">
                                    <?= lang('only_products_with_stock') ?>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" name="view_products_discontinued" value="1">
                                    <?= lang('view_products_discontinued') ?>
                                </label>
                            </div>
                            <div class="col-md-3">
                                <label>
                                    <input type="checkbox" name="only_view_products_featured" value="1">
                                    <?= lang('only_view_products_featured') ?>
                                </label>
                            </div>
                            <div class="col-md-12">
                               <!--  <label>
                                    <input type="radio" name="print_to" value="xls">
                                    <?= lang('Excel') ?>
                                </label> -->
                                <label>
                                    <input type="radio" name="print_to" value="pdf" checked>
                                    <?= lang('PDF') ?>
                                </label>
                            </div>
                        </div>
                        <!-- <hr> -->
                        <!-- <div class="row">
                            <div class="col-md-12">
                                <h3><?= lang('price_groups') ?></h3>
                            </div>
                            <?php $num = 1; ?>
                            <?php foreach ($price_groups as $pg): ?>
                                <div class="col-md-3">
                                    <label>
                                        <input type="checkbox" class="chkpg" name="price_group[]" value="<?= $pg->id ?>" <?= $num <= 6 ? 'checked' : '' ?>>
                                        <?= $pg->name ?>
                                    </label>
                                </div>
                                <?php $num++; ?>
                                <?php endforeach ?>
                        </div> -->
                        <!-- <hr> -->
                        <div class="row" style="padding-top: 5%;">
                            <div class="col-md-6">
                                <button class="btn btn-primary"><?= lang('submit') ?></button>
                            </div>
                        </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>

<script type="text/javascript">
    $('.chkpg').on('ifToggled', function(){
        var check = $(this);
        var checkeds = $('.chkpg:checked').length;
        if (checkeds > 6) {
            command: toastr.error('No se pueden seleccionar más de 6 listas de precios', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
            setTimeout(function() {
                check.removeAttr('checked').iCheck('update');
            }, 850);
        }
    });
</script>