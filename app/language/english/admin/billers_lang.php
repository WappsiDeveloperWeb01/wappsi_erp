<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Billers
 *
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
*/


$lang['add_biller']                     = "Add Biller";
$lang['edit_biller']                    = "Edit Biller";
$lang['delete_biller']                  = "Delete Biller";
$lang['deactivate_biller']              = "Deactivate sucursal";
$lang['activate_biller']              	= "Activate sucursal";
$lang['delete_billers']                 = "Delete Billers";
$lang['biller_added']                   = "Biller successfully added";
$lang['biller_updated']                 = "Biller successfully updated";
$lang['biller_deleted']                 = "Biller successfully deleted";
$lang['billers_deleted']                = "Billers successfully deleted";
$lang['no_biller_selected']             = "No biller selected. Please select at least one biller.";
$lang['invoice_footer']                 = "Invoice Footer";
$lang['biller_x_deleted_have_sales']    = "Action failed! biller have sales";
$lang['billers_x_deleted_have_sales']   = "Some billers cannot be deleted as they have sales";

//NO TRADUCIDAS

$lang['default_customer'] = 'Default customer';
$lang['default_warehouse'] = 'Default warehouse';
$lang['default_group_price'] = 'Default group price';
$lang['default_seller'] = 'Default seller';
$lang["product_order"]	= "Product order";
$lang["default"]	= "Default";
$lang['label_location'] = "Location";
$lang['label_tradename'] = "Tradename";