<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PaymentMethod_model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
	}

    public function find($data)
    {
        $this->db->where($data);
        $q = $this->db->get('payment_methods');
        return $q->row();
    }

}