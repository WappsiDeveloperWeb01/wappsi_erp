<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Debit_notes_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getProductNames($term)
    {
        $limit = $this->Settings->max_num_results_display;
        $this->db->where("(name LIKE '%" . $term . "%' OR code LIKE '%" . $term . "%' OR  concat(name, ' (', code, ')') LIKE '%" . $term . "%')");
        $this->db->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getReturnByID($id)
    {
        $q = $this->db->get_where('returns', ['id' => $id], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getReturnItems($return_id)
    {
        $this->db->select('return_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code, products.second_name as second_name')
            ->join('products', 'products.id=return_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=return_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=return_items.tax_rate_id', 'left')
            ->where('return_id', $return_id)
            ->group_by('return_items.id')
            ->order_by('id', 'asc');

        $q = $this->db->get('return_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function addReturn($data = array(), $items = array())
    {
        if ($this->db->insert('returns', $data)) {
            $return_id = $this->db->insert_id();
            if ($this->site->getReference('re') == $data['reference_no']) {
                $this->site->updateReference('re');
            }

            foreach ($items as $item) {
                $item['return_id'] = $return_id;
                $this->db->insert('return_items', $item);
                if ($item['product_type'] == 'standard') {
                    $clause = ['product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                    $this->site->setPurchaseItem($clause, $item['quantity']);
                    $this->site->syncQuantity(null, null, null, $item['product_id']);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item['product_id']);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, ($combo_item->qty*$item['quantity']));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
            return true;
        }
        return false;
    }

    public function updateReturn($id, $data = array(), $items = array())
    {
        $this->resetSaleActions($id);

        if ($this->db->update('returns', $data, array('id' => $id)) && $this->db->delete('return_items', array('return_id' => $id))) {
            // $return_id = $id;
            foreach ($items as $item) {
                // $item['return_id'] = $return_id;
                $this->db->insert('return_items', $item);
                if ($item['product_type'] == 'standard') {
                    $clause = ['product_id' => $item['product_id'], 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item['option_id']];
                    $this->site->setPurchaseItem($clause, $item['quantity']);
                    $this->site->syncQuantity(null, null, null, $item['product_id']);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item['product_id']);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, ($combo_item->qty*$item['quantity']));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
            return true;
        }

        return false;
    }

    public function resetSaleActions($id)
    {
        if ($items = $this->getReturnItems($id)) {
            foreach ($items as $item) {
                if ($item->product_type == 'standard') {
                    $clause = ['product_id' => $item->product_id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => $item->option_id];
                    $this->site->setPurchaseItem($clause, (0-$item->quantity));
                    $this->site->syncQuantity(null, null, null, $item->product_id);
                } elseif ($item->product_type == 'combo') {
                    $combo_items = $this->site->getProductComboItems($item->product_id);
                    foreach ($combo_items as $combo_item) {
                        $clause = ['product_id' => $combo_item->id, 'purchase_id' => null, 'transfer_id' => null, 'option_id' => null];
                        $this->site->setPurchaseItem($clause, (0-($combo_item->qty*$item->quantity)));
                        $this->site->syncQuantity(null, null, null, $combo_item->id);
                    }
                }
            }
        }
    }

    public function deleteReturn($id)
    {
        $this->resetSaleActions($id);
        if ($this->db->delete('return_items', array('return_id' => $id)) && $this->db->delete('returns', array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getProductOptions($product_id)
    {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getProductOptionByID($id)
    {
        $q = $this->db->get_where('product_variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_sale_reference($reference, $customer_id, $is_electronic_document, $module)
    {
        $this->db->select("sales.id, reference_no AS text, sales.id AS value");
        $this->db->from("sales");
        $this->db->join("documents_types", "documents_types.id = sales.document_type_id", "inner");
        $this->db->where("customer_id", $customer_id);
        $this->db->like("reference_no", $reference);
        $this->db->where("debit_note_id IS NULL");

        if ($module == NOTA_DEBITO_POS) {
            $this->db->where("module", FACTURA_POS);
        } else if ($module == NOTA_DEBITO_DETAL) {
            $this->db->where("module", FACTURA_DETAL);
        }

        $this->db->order_by("id", "DESC");
        $response = $this->db->get();

        return $response->result();
    }

    public function get_sale_reference_by_id($id, $customer_id = NULL, $is_electronic_document = NULL, $module = NULL)
    {
        $this->db->select("sales.id, reference_no AS text, sales.id AS value");
        $this->db->from("sales");
        $this->db->join("documents_types", "documents_types.id = sales.document_type_id", "inner");
        $this->db->where("customer_id", $customer_id);
        $this->db->where("sales.id", $id);
        $this->db->where("debit_note_id IS NULL");
        $this->db->where("module", $module);
        $this->db->order_by("id", "DESC");

        $response = $this->db->get();

        return $response->result();
    }

    public function get_sale($id)
    {
        $this->db->select("sales.*,
                            sales.id AS reference_invoice_id,
                            sales.reference_no AS invoice_reference,
                            sales.customer_id,
                            sales.customer AS customer_name,
                            sales.grand_total,
                            sales.biller_id AS branch_id,
                            sales.biller AS biller_branch_name,
                            DATE(date) AS date,
                            sales.pos AS module,
                            sales.rete_fuente_total AS total_withholdings_fuente,
                            sales.rete_fuente_percentage AS fuente_withholdings_percentage,
                            sales.rete_iva_total AS total_withholdings_iva,
                            sales.rete_iva_percentage AS iva_withholdings_percentage,
                            sales.rete_ica_total AS total_withholdings_ica,
                            sales.rete_ica_percentage AS ica_withholdings_percentage,
                            sales.rete_other_total AS total_withholdings_other,
                            sales.rete_other_percentage AS other_withholdings_percentage,
                            sales.seller_id AS seller_id,
                            companies.name AS branch_name,
                            addresses.id AS addresses_id,
                            addresses.sucursal AS customer_branch,
                            documents_types.factura_electronica AS is_electronic_invoice,
                            sales.rete_fuente_id,
                            sales.rete_iva_id,
                            sales.rete_ica_id,
                            sales.rete_other_id,
                            documents_types.module");
        $this->db->join("companies", "companies.id = sales.biller_id", "inner");
        $this->db->join("addresses", "addresses.id = sales.address_id", "inner");
        $this->db->join("documents_types", "documents_types.id = sales.document_type_id", "inner");

        $q = $this->db->get_where("sales", ["sales.id" => $id]);

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function get_debit_note_concepts()
    {
        $this->db->select("debit_credit_notes_concepts.id,
                        debit_credit_notes_concepts.dian_code,
                        debit_credit_notes_concepts.description,
                        COALESCE(".$this->db->dbprefix("tax_rates").".rate, 0) AS concept_tax_rate,
                        tax_rates.id AS concept_tax_rate_id");
        $this->db->from("debit_credit_notes_concepts");
        $this->db->join("tax_rates", "tax_rates.id = debit_credit_notes_concepts.tax_id", "left");
        $this->db->where("debit_credit_notes_concepts.type", "ND");
        $this->db->where("debit_credit_notes_concepts.description IS NOT NULL");
        $this->db->where("debit_credit_notes_concepts.view", 1);
        $response = $this->db->get();

        return $response->result();
    }

    public function save_debit_note($data = [], $items = [], $payments =[])
    {
        if (empty($data) || empty($items)) {
            return FALSE;
        }

        $document_type = $this->get_document_type($data["document_type_id"]);
        $data["reference_no"] = $document_type->prefix ."-". $document_type->consecutive;

        if ($this->db->insert("sales", $data)) {
            $sale_id = $this->db->insert_id();

            $this->update_consecutive_document_type(["sales_consecutive" => ($document_type->consecutive + 1)], $data["document_type_id"]);
            
            $this->db->query("UPDATE {$this->db->dbprefix('sales')}
                            SET {$this->db->dbprefix('sales')}.reference_debit_note = CONCAT(IF({$this->db->dbprefix('sales')}.reference_debit_note IS NOT NULL, CONCAT({$this->db->dbprefix('sales')}.reference_debit_note, ', '), ''), '".$data['reference_no']."')
                            WHERE {$this->db->dbprefix('sales')}.id = ".$data['reference_invoice_id']);

            $this->db->insert('sales_debit_notes', [
                                                    'sale_id'=> $data['reference_invoice_id'],
                                                    'sale_reference_no'=> $data['reference_debit_note'],
                                                    'debit_note_id'=> $sale_id,
                                                    'debit_note_reference_no'=> $data['reference_no']
                                                ]);

            $items["sale_id"] = $sale_id;

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if (!isset($payment["paid_by"])) {
                        continue;
                    }
                    $payment["sale_id"] = $sale_id;

                    if ($payment["paid_by"] != 'retencion' && isset($payment["reference_no"])) {
                        $type_document_payment_id = $payment["reference_no"];

                        $document_type_pay = $this->get_document_type($type_document_payment_id);
                        $payment["reference_no"] = $document_type_pay->prefix ."-". $document_type_pay->consecutive;
                        $this->update_consecutive_document_type(["sales_consecutive" => ($document_type_pay->consecutive + 1)], $type_document_payment_id);
                    }

                    $this->db->insert('payments', $payment);
                }
            }

            if ($this->db->insert("sale_items", $items)) {
                $this->site->wappsiContabilidadVentasDebitNotes($sale_id, $data, $items, $payments);
                return $sale_id;
            }

            return FALSE;
        }

        return FALSE;
    }

    public function get_document_type($id)
    {
        $this->db->select("sales_consecutive AS consecutive, sales_prefix AS prefix");
        $this->db->from("documents_types");
        $this->db->where("id", $id);
        $response = $this->db->get();

        return $response->row();
    }

    public function update_consecutive_document_type($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("documents_types", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_reference_invoice($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("sales", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function getDebitNoteConceptbyId($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("debit_credit_notes_concepts");
        return $q->row();
    }
}
