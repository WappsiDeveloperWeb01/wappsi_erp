<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Apis
 * 
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 * 
 */


$lang['api_key']                        = "API Key";
$lang['api_keys']                       = "API Keys";
$lang['create_api_key']                 = "Crear API Key";
$lang['delete_api_keys']                = "Eliminar API Key";
$lang['key']                            = "Key";
$lang['level']                          = "Nivel";
$lang['ignore_limit']                   = "Ignorar el límite";
$lang['ip_addresses']                   = "Direcciones IP";
$lang['api_key_deleted']                = "API Key eliminada correctamente";
$lang['delete_failed']                  = "API key no se pudo eliminar";
$lang['api_key_added']                  = "API Key creada exitosamente";
