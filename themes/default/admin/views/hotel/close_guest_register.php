<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('close_guest_register')." ".$guest_register->reference_no; ?></h4>
      </div>
      <?= admin_form_open_multipart("hotel/close_guest_register/".$id, ['id' => 'add-customer-form']); ?>
        <?= form_hidden('document_code'); ?>
        <div class="modal-body">
          <div class="row">
            <div class="form-group col-md-12">
                <?= lang("date_out", "date_out"); ?>
                <input type="text" name="date_out" class="form-control" id="date_out" required="required">
            </div>
            <div class="form-group col-md-12">
                <?= lang("total", "total"); ?>
                <input type="number" name="total" class="form-control" id="total" required="required">
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="close_guest_register" value="1">
            <button class="btn btn-primary" id="submit_close_guest_register" type="button"><?= lang('close_guest_register') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
  $(document).ready(function (e) {
      $("#add-customer-form").validate({
          ignore: []
      });
      $(document).on('click', '#submit_close_guest_register', function(){
        form = $('#add-customer-form');
        if (form.valid()) {
          form.submit();
        }
      });
      $('select.select').select2({minimumResultsForSearch: 7});

      $('#total').val(formatDecimal("<?= $this->sma->formatDecimal($guest_register->total) ?>"));

      $("#date_out").datetimepicker({
          format: site.dateFormats.js_ldate,
          fontAwesome: true,
          language: 'sma',
          weekStart: 1,
          todayBtn: 1,
          startDate: min_input_date,
          endDate: max_input_date,
          autoclose: 1,
          todayHighlight: 1,
          startView: 2,
          forceParse: 0
      }).datetimepicker('update',  new Date());
    });

    $(document).on('keyup', '#vat_no', function(){
        
    });
</script>
<?= $modal_js ?>