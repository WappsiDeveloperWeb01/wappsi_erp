<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Electronic_billing_model extends CI_Model {
    public $amountResolutions = 0;

    public function send_document_electronic($documentData)
    {
        $document = $this->site->getSaleByID($documentData->sale_id);
        $technologyProvider = ($document->pos == YES) ? $this->pos_settings->technology_provider_id : $this->Settings->fe_technology_provider;
        $typeDocument = $this->site->getTypeElectronicDocument($documentData->sale_id);
        $priorValidation = $this->priorValidation($documentData, $typeDocument);

        if ($priorValidation->response === FALSE) {
            return $priorValidation;
        }

        if ($technologyProvider == CADENA) {
            if ($typeDocument == INVOICE) {
                $requestFile = $this->cadenaBuildXmlInvoice($documentData);
            } else if ($typeDocument == CREDIT_NOTE) {
                $requestFile = $this->cadenaBuildXmlCreditNote($documentData);
            } else if ($typeDocument == DEBIT_NOTE) {
                $requestFile = $this->cadenaBuildXmlDebitNote($documentData);
            }

            $responseDocumentSent = $this->cadenaWSSendDocument($requestFile, $documentData);
        } else if ($technologyProvider == DELCOP) {
            $authorizationToken = $this->delcopGetAuthorizationoken();
            if ($authorizationToken->success == FALSE) {
                $response = [
                    'response' => FALSE,
                    'message' => "Los campos de Usuario o Contraseña DELCOP no son correctos. ". $authorizationToken->error
                ];

                return (object) $response;
            }

            if ($typeDocument == INVOICE) {
                $xmlFile = $this->delcopBuildXmlInvoice($documentData);
            } else if ($typeDocument == CREDIT_NOTE) {
                $xmlFile = $this->delcopBuildXmlCreditNote($documentData);
            } else if ($typeDocument == DEBIT_NOTE) {
                $xmlFile = $this->delcopBuildXmlDebitNote($documentData);
            }

            $responseDocumentSent = $this->delcopWSSendDocument($xmlFile, $documentData, $authorizationToken->token);
        } else if ($technologyProvider == BPM) {
            $requestFile = $this->buildJsonInvoice($documentData);

            $response = $this->bpmWSSendDocument($requestFile, $document, $typeDocument);

            $responseDocumentSent = $this->manageSendResponse($response, $documentData);
        } else if ($technologyProvider == SIMBA){
            if ($typeDocument == INVOICE) {
                $requestFile = $this->simbaBuildJsonInvoice($documentData);
            }else if($typeDocument == CREDIT_NOTE){
                $requestFile = $this->simbaBuildJsonCreditNote($documentData);
            }else if ($typeDocument == DEBIT_NOTE) {
                $requestFile = $this->simbaBuildJsonDebitNote($documentData);
            }
            $response = $this->simbaWSSendDocument($requestFile, $document, $typeDocument);

            $responseDocumentSent = $this->manageSendResponseSimba($response, $documentData);
        }

        if ($responseDocumentSent->response === FALSE) {
            return $responseDocumentSent;
        } else {
            return (object) ["response" => 1];
        }
    }

    private function priorValidation($invoice_data, $type_electronic_document)
    {
        $hits = $this->site->getHits();
        $invoicing = $this->site->getInvoicing(ELECTRONIC_DOCUMENTS);
        $issuer_data = $this->site->get_setting();
        $sale = $this->site->getSaleByID($invoice_data->sale_id);
        $items_sale = $this->site->getAllSaleItems($invoice_data->sale_id);
        $payments_data = $this->site->getSalePayments($invoice_data->sale_id);
        $customer_data = $this->site->getCompanyByID($invoice_data->customer_id);
        $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
        $types_issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
        $type_customer_regimen = $this->get_type_of_electronic_billing_regime($customer_data->tipo_regimen)->codigo;
        $types_customer_obligations = $this->site->getTypesCustomerObligations($invoice_data->customer_id, ACQUIRER);
        $workEnvironment = ($sale->pos == YES) ? $this->pos_settings->work_environment : $this->Settings->fe_work_environment;

        if ($this->Settings->electronic_hits_validation == YES) {
            if ($invoicing->amount <= 0) {
                return (object) [
                    "response"  => FALSE,
                    "message"   => $this->lang->line('empty_electronic_packages')
                ];
            }

            if ($hits->amount > $invoicing->amount) {
                return (object) [
                    "response"  => FALSE,
                    "message"   => $this->lang->line("past_max_attempts")
                ];
            }
        }

        if (strpos($sale->fe_mensaje_soporte_tecnico, 'procesado anteriormente') != FALSE) {
            return (object) [
                "response"  => FALSE,
                "message"   => "Factura electrónica no enviada. El documento ya fue procesado ante la DIAN, por favor, cambiar el estado."
            ];
        }

        if ($workEnvironment == PRODUCTION && $resolution_data->fe_work_environment != PRODUCTION) {
            return (object) [
                "response" => FALSE,
                "message" => "Documento electrónico no enviado. La resolución de este documento está configurado para ambiente PRUEBAS."
            ];
        }

        if ($workEnvironment == TEST && $resolution_data->fe_work_environment != TEST) {
            return (object) [
                "response" => FALSE,
                "message" => "Documento electrónico no enviado. La resolución de este documento está configurado para ambiente PRODUCCIÓN."
            ];
        }

        if ($issuer_data->tipo_documento != NIT) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. El tipo de documento del emisor no es permitido."
            ];
        }

        if (empty($issuer_data->tipo_persona)) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. El Tipo de persona para el emisor no puede ser nulo."
            ];
        }

        if (empty($types_issuer_obligations) && $sale->pos == NOT) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. Las responsabilidades para el emisor no se han asignado."
            ];
        }

        if (empty($types_customer_obligations) && $sale->pos == NOT) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. Las responsabilidades para el cliente no se han asignado."
            ];
        }

        if (empty($type_customer_regimen) && $sale->pos == NOT) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. El código tipo de régimen del cliente no puede ser nulo."
            ];
        }

        if (strpos($customer_data->vat_no, ".") || strpos($customer_data->vat_no, "-")) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. El número del documento del adquiriente no puede llevar puntos (.) ni guiones (-)."
            ];
        }

        if (empty($items_sale)) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. No existen artículos relacionados a la factura."
            ];
        }

        if ($type_electronic_document == INVOICE) {
            if ($sale->sale_status != "completed") {
                return (object) [
                    "response" => FALSE,
                    "message" => "Factura electrónica no enviada. La factura no se encuentra aprobada."
                ];
            }

            if (!empty($payments_data)) {
                foreach ($payments_data as $payment) {
                    if ($payment->paid_by != 'retencion' || $payment->paid_by != 'discount') {
                    } else {
                        if (empty($payment->mean_payment_code_fe)) {
                            $paymentMethod = $this->site->getPaymentMethodByCode($payment->paid_by);
                            if (!empty($paymentMethod)) {
                                $paymentUpdated = $this->updatePayment(['mean_payment_code_fe'=>$paymentMethod->code_fe], $payment->id);
                            } else {
                                if ($payment->paid_by == 'due') {
                                    $paymentUpdated = $this->updatePayment(['mean_payment_code_fe'=>"ZZZ"], $payment->id);
                                }
                            }

                            if ($paymentUpdated == FALSE) {
                                return (object) [
                                    "response" => FALSE,
                                    "message" => "Factura electrónica no enviada. La factura no tiene asignado el código de medio de pago DIAN."
                                ];
                            }
                        }
                    }
                }
            }
        } else if ($type_electronic_document == CREDIT_NOTE) {
            if ($sale->sale_status != "returned") {
                return (object) [
                    "response" => FALSE,
                    "message" => "Nota electrónica no enviada. La Nota no se encuentra aprobada."
                ];
            }

            if (!empty($payments_data)) {
                foreach ($payments_data as $payment) {
                    if ($payment->paid_by != 'retencion') {
                        if (empty($payment->mean_payment_code_fe)) {
                            $paymentMethod = $this->site->getPaymentMethodByCode($payment->paid_by);
                            if (!empty($paymentMethod)) {
                                $paymentUpdated = $this->updatePayment(['mean_payment_code_fe'=>$paymentMethod->code_fe], $payment->id);
                            } else {
                                if ($payment->paid_by == 'due') {
                                    $paymentUpdated = $this->updatePayment(['mean_payment_code_fe'=>"ZZZ"], $payment->id);
                                }
                            }

                            if ($paymentUpdated == FALSE) {
                                return (object) [
                                    "response" => FALSE,
                                    "message" => "Factura electrónica no enviada. La factura no tiene asignado el código de medio de pago DIAN."
                                ];
                            }
                        }
                    }
                }
            }
        }
        
        foreach ($items_sale as $item) {
            if (empty($item->code_fe)) {
                return (object) [
                    "response" => FALSE,
                    "message" => "Factura electrónica no enviada. El código de impuesto para el impuesto ". $item->name ." dado por la DIAN no se encuentra diligenciado."
                ];

                break;
            }
        }

        if ($sale->fe_aceptado == 2) {
            return (object) [
                "response" => FALSE,
                "message" => "Factura electrónica no enviada. El documento ya ha sido procesado anteriormente."
            ];
        }

        return (object) ["response" => TRUE];
    }

    private function get_type_of_electronic_billing_regime($id)
    {
        $this->db->select('codigo');
        $this->db->from('types_vat_regime');
        $this->db->where('id', $id);
        $response = $this->db->get();
        return $response->row();
    }

    public function buildRequestFile($documentElectronicData, $toShow = FALSE)
    {
        $technologyProvider = $this->Settings->fe_technology_provider;
        $typeElectronicDocument = $this->site->getTypeElectronicDocument($documentElectronicData->sale_id);
        $this->priorValidation($documentElectronicData, $typeElectronicDocument);

        if ($technologyProvider == CADENA) {
            if ($typeElectronicDocument == INVOICE) {
                $this->cadenaBuildXmlInvoice($documentElectronicData, $toShow);
            } else if ($typeElectronicDocument == CREDIT_NOTE) {
                $this->cadenaBuildXmlCreditNote($documentElectronicData, $toShow);
            } else if ($typeElectronicDocument == DEBIT_NOTE) {
                $this->cadenaBuildXmlDebitNote($documentElectronicData, $toShow);
            }
        } else if ($technologyProvider == DELCOP) {
            if ($typeElectronicDocument == INVOICE) {
                $xml_file = $this->delcopBuildXmlInvoice($documentElectronicData, $toShow);
            } else if ($typeElectronicDocument == CREDIT_NOTE) {
                $xml_file = $this->delcopBuildXmlCreditNote($documentElectronicData, $toShow);
            } else if ($typeElectronicDocument == DEBIT_NOTE) {
                $xml_file = $this->delcopBuildXmlDebitNote($documentElectronicData, $toShow);
            }
        } else if ($technologyProvider == BPM) {
            $json = $this->buildJsonInvoice($documentElectronicData);
            $this->sma->print_arrays($json);
        } else if ($technologyProvider == SIMBA){ // modificacaiones para el proveedor tecnologico simba
            if ($typeElectronicDocument == INVOICE) {
                $json = $this->simbaBuildJsonInvoice($documentElectronicData);
            }else if($typeElectronicDocument == CREDIT_NOTE){
                $json = $this->simbaBuildJsonCreditNote($documentElectronicData);
            }else if($typeElectronicDocument == DEBIT_NOTE){
                $json = $this->simbaBuildJsonDebitNote($documentElectronicData);
            }
            $this->sma->print_arrays($json);
        }
    }

    /**************************** CADENA ****************************/
        public function cadenaBuildXmlInvoice($invoice_data, $print_xml = FALSE)
        {
            $sma = new Sma();
            $issuer_data = $this->site->get_setting();
            $sale = $this->site->getSaleByID($invoice_data->sale_id);
            $items_sale = $this->site->getAllSaleItems($invoice_data->sale_id);
            $biller_data = $this->site->getCompanyByID($invoice_data->biller_id);
            $customer_data = $this->site->getCompanyByID($invoice_data->customer_id);
            $payments_data = $this->site->getSalePayments($invoice_data->sale_id);
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $advance_payments = $this->get_advance_payments_sale($invoice_data->sale_id);
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER);
            $CUFE_code = $this->generate_CUFE($sale, $items_sale, $issuer_data, $customer_data, $resolution_data, $print_xml);

            if ($print_xml) { header("content-type: application/xml; charset=ISO-8859-15"); }

            $xml = new DOMDocument("1.0");
            $xml_invoice = $xml->createElement("Invoice");
                $xml_invoice->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
                $xml_invoice->setAttribute("xmlns", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2");
                $xml_invoice->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                $xml_invoice->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                $xml_invoice->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                $xml_invoice->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
                $xml_invoice->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
                $xml_invoice->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
                $xml_invoice->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                $xml_invoice->setAttribute("xsi:schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:Invoice-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-Invoice-2.1.xsd");

                /***************** Resolución de numeración *******************/
                    $this->addUBLExtensionsInvoice($xml, $xml_invoice, $sale);
                /**************************************************************/

                /************************* Encabezado *************************/
                    $this->addHeaderInvoice($xml, $xml_invoice, $sale, $items_sale, $resolution_data, $CUFE_code);
                /**************************************************************/

                /*************************** Emisor ***************************/
                    $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                        $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $issuer_data->tipo_persona));
                        $xml_cac_Party = $xml->createElement("cac:Party");
                            $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->nombre_comercial)));
                            $xml_cac_Party->appendChild($xml_cac_PartyName);

                            $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                $xml_cac_Address = $xml->createElement("cac:Address");
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($biller_data->city))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", $biller_data->DEPARTAMENTO));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                    $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", $biller_data->country);
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_Address->appendChild($xml_cac_Country);
                                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                            $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                            $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                    $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                $type_issuers_obligations = '';
                                foreach ($issuer_obligations as $issuer_obligation) {
                                    $type_issuers_obligations .= $issuer_obligation->types_obligations_id .';';
                                }

                                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_issuers_obligations, ";"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($biller_data->city))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", $biller_data->DEPARTAMENTO));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", $biller_data->country);
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                            $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                    $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);

                                $xml_cac_CorporateRegistrationScheme = $xml->createElement("cac:CorporateRegistrationScheme");
                                    $xml_cac_CorporateRegistrationScheme->appendChild($xml->createElement("cbc:ID", $resolution_data->sales_prefix));
                                $xml_cac_PartyLegalEntity->appendChild($xml_cac_CorporateRegistrationScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                            $xml_cac_Contact = $xml->createElement("cac:Contact");
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $issuer_data->default_email));
                            $xml_cac_Party->appendChild($xml_cac_Contact);
                        $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                    $xml_invoice->appendChild($xml_cac_AccountingSupplierParty);
                /**************************************************************/

                /************************ Adquiriente. ************************/
                    $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                        $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer_data->type_person));
                        $xml_cac_Party = $xml->createElement("cac:Party");
                            if ($customer_data->type_person == NATURAL_PERSON) {
                                $xml_cac_PartyIdentification = $xml->createElement("cac:PartyIdentification");
                                    $xml_cbc_ID = $xml->createElement("cbc:ID", $this->clear_document_number($customer_data->vat_no));
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_ID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }
                                        $xml_cbc_ID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cac_PartyIdentification->appendChild($xml_cbc_ID);
                                $xml_cac_Party->appendChild($xml_cac_PartyIdentification);
                            }
                            $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                            $xml_cac_Party->appendChild($xml_cac_PartyName);

                            $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                $xml_cac_Address = $xml->createElement("cac:Address");
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($customer_data->city))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(strtolower($customer_data->state))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                    $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(strtolower($customer_data->country)));
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_Address->appendChild($xml_cac_Country);
                                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                            $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                            $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clear_document_number($customer_data->vat_no));
                                    if ($customer_data->document_code == NIT) {
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                    }

                                    $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                $type_customer_obligations = '';
                                foreach ($customer_obligations as $customer_obligation) {
                                    $type_customer_obligations .= $customer_obligation->types_obligations_id .';';
                                }
                                $type_customer_obligations = empty($type_customer_obligations) ? 'R-99-PN' : $type_customer_obligations;

                                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_customer_obligations, ";"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($customer_data->city))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(strtolower($customer_data->state))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));

                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);

                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(strtolower($customer_data->country)));
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                            $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));

                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $customer_data->vat_no);
                                    if ($customer_data->document_code == NIT) {
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                    }

                                    $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);
                            $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                            $xml_cac_Contact = $xml->createElement("cac:Contact");
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $customer_data->email));
                            $xml_cac_Party->appendChild($xml_cac_Contact);
                        $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                    $xml_invoice->appendChild($xml_cac_AccountingCustomerParty);
                /**************************************************************/

                /************************ Medios pago *************************/
                    $payment_amount = 0;
                    $payments_clean_data = [];

                    if (!empty($payments_data)) {
                        foreach ($payments_data as $payment) {
                            $payment_amount += $payment->amount;

                            if ($payment->paid_by != 'retencion' || $payment->paid_by != 'discount') {
                            } else {
                                $payments_clean_data[] = $payment;
                            }
                        }
                    }

                    if ($sale->grand_total == $payment_amount) {
                        $method = CASH;
                    } else {
                        $method = CREDIT;
                    }

                    if (!empty($payments_clean_data)) {
                        foreach ($payments_clean_data as $payment) {
                            $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $payment->mean_payment_code_fe));
                                if ($method == CREDIT) {
                                    $fecha_creacion = date("Y-m-d", strtotime($sale->date));
                                    if (empty($sale->payment_term)) {
                                        $payment_term = 0;
                                    } else {
                                        $payment_term = $sale->payment_term;
                                    }

                                    $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                    if ($payment_term > 0) {
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                    }
                                }
                            $xml_invoice->appendChild($xml_cac_PaymentMeans);
                        }
                    } else {
                        $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                        if ($method == CREDIT) {
                            $fecha_creacion = date("Y-m-d", strtotime($sale->date));
                            if (empty($sale->payment_term)) {
                                $payment_term = 0;
                            } else {
                                $payment_term = $sale->payment_term;
                            }

                            $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                            if ($payment_term > 0) {
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                            }
                        }

                        $xml_invoice->appendChild($xml_cac_PaymentMeans);
                    }
                /**************************************************************/

                /**************************** TRM *****************************/
                    if ($sale->sale_currency != "COP") {
                        $xml_PaymentExchangeRate = $xml->createElement("cac:PaymentExchangeRate");
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyCode", "COP"));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyBaseRate", $sale->sale_currency_trm));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyCode", $sale->sale_currency));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyBaseRate", "1.00"));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:CalculationRate", $sale->sale_currency_trm));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:Date", $this->format_date($sale->date)->date));
                        $xml_invoice->appendChild($xml_PaymentExchangeRate);
                    }
                /**************************************************************/

                /******************** Descuentos y cargos *********************/
                    $charge_or_discount = 1;

                    if ($sale->order_discount > 0) {
                        $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "false"));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReasonCode", "11"));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReason", "Otro descuento"));

                            if (strpos($sale->order_discount_id, "%") !== FALSE) {
                                $order_discount_percentage = str_replace("%", "", $sale->order_discount_id);
                            } else {
                                $order_discount_percentage = ($sale->order_discount * 100) / ($sale->total + $sale->total_tax);
                            }

                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimalNoRound($order_discount_percentage, NUMBER_DECIMALS)));

                            $Amount = $sale->order_discount;
                            $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                $xml_cac_Amount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                            $BaseAmount = $sale->total + $sale->total_tax;
                            $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);
                        $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                    }

                    if ($sale->shipping > 0) {
                        $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                            $order_charge_percentage = ($sale->shipping * 100) / $sale->total;
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($order_charge_percentage, NUMBER_DECIMALS)));

                            $Amount = $sale->shipping;
                            $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                $xml_cac_Amount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                            $BaseAmount = $sale->total;
                            $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                        $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                    }

                    if (!empty($sale->aiu_management)) {
                        $baseAmountAIU = 0;
                        foreach ($items_sale as $item) {
                            if ($item->aiu_product == 1) {
                                $baseAmountAIU += $item->net_unit_price * $item->quantity;
                            }
                        }
                    }

                    if (!empty($sale->aiu_management) && $sale->aiu_admin_total > 0) {
                        $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($sale->aiu_admin_percentage, NUMBER_DECIMALS)));

                            $Amount = $sale->aiu_admin_total;
                            $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                $xml_cac_Amount->setAttribute("currencyID", $sale->sale_currency);
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                            $BaseAmount = $baseAmountAIU;
                            $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                        $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                    }

                    if (!empty($sale->aiu_management) && $sale->aiu_imprev_total > 0) {
                        $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($sale->aiu_imprev_percentage, NUMBER_DECIMALS)));

                            $Amount = $sale->aiu_imprev_total;
                            $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                $xml_cac_Amount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                            $BaseAmount = $baseAmountAIU;
                            $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                        $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                    }

                    if (!empty($sale->aiu_management) && $sale->aiu_utilidad_total > 0) {
                        $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($sale->aiu_utilidad_percentage, NUMBER_DECIMALS)));

                            $Amount = $sale->aiu_utilidad_total;
                            $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                $xml_cac_Amount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                            $BaseAmount = $baseAmountAIU;
                            $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                        $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                    }
                /**************************************************************/

                /********************** Importes totales. *********************/
                    /************************ Impuestos. **********************/
                        $productTaxIva = $productTaxInc = $productTaxBolsas = $subtotal = $totalTax = $exemptExisting = $nominalTax = $ivaUtilidad = $subtotalExclusive = $productTaxIbua = $productTaxIcui = $productTaxIcl = $productTaxIc = $grossValue = $baseBolsas = 0;
                        $taxRateAIU = '';

                        foreach ($items_sale as $item) {
                            $taxCode = $item->code_fe;
                            $i = (int) $item->rate;
                            if ($taxCode == IVA) {
                                $taxBaseIva[$i] = 0;
                                $taxValueIva[$i] = 0;
                                if ($item->rate == 0 && $item->excluded == NOT) { $exemptExisting++; }
                            }
                            if ($taxCode == INC) {
                                $taxBaseInc[$i] = 0;
                                $taxValueInc[$i] = 0;
                            }
                            if ($taxCode == BOLSAS) {
                                $i = (int) $item->tax;
                                $taxBaseBolsas[$i] = 0;
                                $taxValueBolsas[$i] = 0;
                            }

                            if (!empty($item->consumption_sales)) {
                                if ($item->codeTax2 == IBUA) {
                                    $i = (int) $item->nominal;
                                    $taxBaseIbua[$i] = 0;
                                    $taxValueIbua[$i] = 0;
                                }
                                if ($item->codeTax2 == ICUI) {
                                    $i = (int) str_replace('%', '', $item->tax_2);
                                    $taxBaseIcui[$i] = 0;
                                    $taxValueIcui[$i] = 0;
                                }
                                if ($item->codeTax2 == ICL) {
                                    $i = (int) $item->nominal;
                                    $taxBaseIcl[$i] = 0;
                                    $taxValueIcl[$i] = 0;
                                }
                                if ($item->codeTax2 == IC) {
                                    $i = (int) $item->nominal;
                                    $taxBaseIc[$i] = 0;
                                    $taxValueIc[$i] = 0;
                                }
                            }
                        }

                        foreach ($items_sale as $item) {
                            $taxRate = $item->rate;
                            $taxCode = $item->code_fe;
                            $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                            $netUnitPrice = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                            $grossValue += $sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);

                            if ($taxCode == IVA) {
                                $totalTax += $this->sma->formatDecimal(($netUnitPrice * $quantity * ($taxRate / 100)), NUMBER_DECIMALS);

                                if ($item->excluded == NOT) {
                                    $subtotalExclusive += $this->sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);
                                    $productTaxIva += $this->sma->formatDecimal(($netUnitPrice * $quantity * ($taxRate / 100)), NUMBER_DECIMALS);

                                    $i = (int) $taxRate;
                                    $taxBaseIva[$i] += $this->sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);
                                    $taxValueIva[$i] += $this->sma->formatDecimal(($netUnitPrice * $quantity * ($taxRate / 100)), NUMBER_DECIMALS);

                                    if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                                        if ($sale->order_tax_aiu_base_apply_to == 3) {
                                            $utilidad = (($this->sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS)) * ($sale->aiu_utilidad_percentage / 100));
                                            $taxRateAIU = $this->site->getTaxRateByID($sale->order_tax_aiu_id);
                                            $ivaUtilidad += $this->sma->formatDecimal(($utilidad * ($taxRateAIU->rate / 100)), NUMBER_DECIMALS);
                                        }
                                    }
                                } else {
                                    $subtotal += $sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);
                                    $productTaxIva += $this->sma->formatDecimal(($netUnitPrice * $quantity * ($taxRate / 100)), NUMBER_DECIMALS);
                                }
                            } else if ($taxCode == INC) {
                                $subtotal += $sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);
                                $productTaxInc += $sma->formatDecimal(($netUnitPrice * $quantity * ($taxRate / 100)), NUMBER_DECIMALS);

                                $i = (int) $taxRate;
                                $taxBaseInc[$i] += $sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);
                                $taxValueInc[$i] += $sma->formatDecimal(($netUnitPrice * $quantity * ($taxRate / 100)), NUMBER_DECIMALS);
                            } else if($taxCode == BOLSAS) {
                                $nominal = $item->tax;
                                $i = (int) $nominal;
                                $productTaxBolsas += $this->sma->formatDecimal($item->quantity * $nominal, NUMBER_DECIMALS);
                                $taxBaseBolsas[$i] += $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                                $baseBolsas += $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                                $taxValueBolsas[$i] += $this->sma->formatDecimal($item->quantity * $nominal, NUMBER_DECIMALS);
                            }

                            if (!empty($item->consumption_sales)) {
                                if ($item->codeTax2 == IBUA) {
                                    $i = (int) $item->nominal;
                                    $mililiters = $item->mililiters / 100;
                                    $nominal = $item->nominal;
                                    $productTaxIbua += $this->sma->formatDecimal(($mililiters * $nominal), NUMBER_DECIMALS);

                                    $taxBaseIbua[$i] += $item->mililiters;
                                    $taxValueIbua[$i] += $this->sma->formatDecimal(($mililiters * $nominal), NUMBER_DECIMALS);
                                }
                                if ($item->codeTax2 == ICUI) {
                                    $taxRate = str_replace('%', '', $item->tax_2);
                                    $i = (int) $taxRate;
                                    $rate = $this->sma->formatDecimal(($taxRate / 100), NUMBER_DECIMALS);

                                    $productTaxIcui += $this->sma->formatDecimal(($netUnitPrice * $quantity) * $rate, NUMBER_DECIMALS);

                                    $taxBaseIcui[$i] += $this->sma->formatDecimal(($netUnitPrice * $quantity), NUMBER_DECIMALS);
                                    $taxValueIcui[$i] += $this->sma->formatDecimal(($netUnitPrice * $quantity) * $rate, NUMBER_DECIMALS);
                                }
                                if ($item->codeTax2 == ICL) {
                                    $i = (int) $item->nominal;
                                    $degrees = $item->degrees;
                                    $nominal = $item->nominal;
                                    $mililiters = $item->mililiters;

                                    if ($mililiters != 750) {
                                        $tax =  intval(($mililiters * $nominal) / 750) * $degrees;
                                    } else {
                                        $tax = $nominal * $degrees;
                                    }

                                    $productTaxIcl += $this->sma->formatDecimal(($tax), NUMBER_DECIMALS);

                                    $taxBaseIcl[$i] += $item->degrees;
                                    $taxValueIcl[$i] += $this->sma->formatDecimal(($tax), NUMBER_DECIMALS);
                                }
                                if ($item->codeTax2 == IC) {
                                    $i = (int) $item->nominal;

                                    $productTaxIc  += $this->sma->formatDecimal($item->nominal * $item->quantity, NUMBER_DECIMALS);

                                    $taxBaseIc[$i] += $item->quantity;
                                    $taxValueIc[$i] += $this->sma->formatDecimal($item->nominal * $item->quantity, NUMBER_DECIMALS);
                                }
                            }
                        }

                        if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                            $totalTax += $ivaUtilidad;
                        }

                        if (isset($taxBaseIva)) {
                            $this->addTaxInvoice($xml, $xml_invoice, $sale, $items_sale, $productTaxIva, $taxBaseIva, $taxValueIva, IVA, 'IVA', $exemptExisting, $ivaUtilidad, $taxRateAIU);
                        }
                        if (isset($taxBaseInc)) {
                            $this->addTaxInvoice($xml, $xml_invoice, $sale, $items_sale, $productTaxInc, $taxBaseInc, $taxValueInc, INC, 'INC');
                        }
                        if (isset($taxBaseIbua)) {
                            $this->addTaxInvoice($xml, $xml_invoice, $sale, $items_sale, $productTaxIbua, $taxBaseIbua, $taxValueIbua, IBUA, 'IBUA');
                        }
                        if (isset($taxBaseIcui)) {
                            $this->addTaxInvoice($xml, $xml_invoice, $sale, $items_sale, $productTaxIcui, $taxBaseIcui, $taxValueIcui, ICUI, 'ICUI');
                        }
                        if (isset($taxBaseBolsas)) {
                            $this->addTaxInvoice($xml, $xml_invoice, $sale, $items_sale, $productTaxBolsas, $taxBaseBolsas, $taxValueBolsas, BOLSAS, 'INC Bolsas');
                        }
                        if (isset($taxBaseIcl)) {
                            $this->addTaxInvoice($xml, $xml_invoice, $sale, $items_sale, $productTaxIcl, $taxBaseIcl, $taxValueIcl, ICL, 'ICL');
                        }
                        if (isset($taxBaseIc)) {
                            $this->addTaxInvoice($xml, $xml_invoice, $sale, $items_sale, $productTaxIc, $taxBaseIc, $taxValueIc, IC, 'IC');
                        }

                    /***********************************************************/

                    /*********************** Retenciones. **********************/
                        if ($sale->rete_iva_total > 0) {
                            $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                $rete_iva_total = ($totalTax * $sale->rete_iva_percentage) / 100;
                                $TaxAmount = $rete_iva_total;
                                $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                    $TaxableAmount = $totalTax;
                                    $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                    $TaxAmount = $rete_iva_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                        $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($sale->rete_iva_percentage, 5)));

                                        $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "05"));
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteIVA"));
                                        $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                            $xml_invoice->appendChild($xml_cac_WithholdingTaxTotal);
                        }

                        if ($sale->rete_fuente_total > 0) {
                            $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                $rete_fuente_total = ($subtotal * $sale->rete_fuente_percentage) / 100;

                                $TaxAmount = $rete_fuente_total;
                                $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                    $TaxableAmount = $subtotal;
                                    $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);


                                    $TaxAmount = $rete_fuente_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                        $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($sale->rete_fuente_percentage, 5)));

                                        $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "06"));
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteFuente"));
                                        $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                            $xml_invoice->appendChild($xml_cac_WithholdingTaxTotal);
                        }

                        if ($sale->rete_ica_total > 0) {
                            $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                $rete_ica_total = ($subtotal * $sale->rete_ica_percentage) / 100;

                                $TaxAmount = $rete_ica_total;
                                $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                    $TaxableAmount = $subtotal;
                                    $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                    $TaxAmount = $rete_ica_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                        $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($sale->rete_ica_percentage, 5)));

                                        $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "07"));
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteICA"));
                                        $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                            $xml_invoice->appendChild($xml_cac_WithholdingTaxTotal);
                        }
                    /***********************************************************/
                /**************************************************************/

                /********************** Total impuestos ***********************/
                    $this->addTaxTotalInvoice($xml,$xml_invoice,$sale,$grossValue,$subtotal,$subtotalExclusive,$nominalTax,$totalTax,$productTaxIcl,$productTaxBolsas,$productTaxIcui,$productTaxIbua,$productTaxInc,$productTaxIc,$baseBolsas);
                /**************************************************************/

                /************************* Productos **************************/
                    $consecutive_product_identifier = 1;


                    foreach ($items_sale as $item) {
                        $xml_cac_InvoiceLine = $xml->createElement("cac:InvoiceLine");
                            $xml_cac_InvoiceLine->appendChild($xml->createElement("cbc:ID", $consecutive_product_identifier));

                            if (!empty($sale->aiu_management)) {
                                $xml_cac_InvoiceLine->appendChild($xml->createElement("cbc:Note", "Contrato de servicios AIU por concepto de:"));
                            }

                            $xml_cbc_InvoicedQuantity = $xml->createElement("cbc:InvoicedQuantity", $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS));
                                $xml_cbc_InvoicedQuantity->setAttribute("unitCode", "NIU");
                            $xml_cac_InvoiceLine->appendChild($xml_cbc_InvoicedQuantity);

                            $LineExtensionAmount = ($sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS) * $sma->formatDecimal($item->quantity, NUMBER_DECIMALS));

                            $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($LineExtensionAmount, NUMBER_DECIMALS));
                                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                            $xml_cac_InvoiceLine->appendChild($xml_cbc_LineExtensionAmount);

                            if ($item->code_fe == BOLSAS) {
                                $xml_cac_PricingReference = $xml->createElement("cac:PricingReference");
                                    $xml_cac_AlternativeConditionPrice = $xml->createElement("cac:AlternativeConditionPrice");
                                        $PriceAmount = (($item->net_unit_price * $item->quantity) <= 0) ? 1 : ($item->net_unit_price * $item->quantity);
                                        $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($PriceAmount, NUMBER_DECIMALS));
                                            $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_AlternativeConditionPrice->appendChild($xml_cbc_PriceAmount);


                                        $xml_cac_AlternativeConditionPrice->appendChild($xml->createElement("cbc:PriceTypeCode", "01"));
                                    $xml_cac_PricingReference->appendChild($xml_cac_AlternativeConditionPrice);
                                $xml_cac_InvoiceLine->appendChild($xml_cac_PricingReference);
                            }

                            $this->addTaxItem($xml, $xml_cac_InvoiceLine, $sale, $item);

                            $this->addTax2Item($xml, $xml_cac_InvoiceLine, $sale, $item);

                            $xml_cac_Item = $xml->createElement("cac:Item");
                                $xml_cac_Item->appendChild($xml->createElement("cbc:Description", htmlspecialchars($item->product_name)));

                                $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                                    $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                                        $xml_cbc_ID->setAttribute("schemeID", "999");
                                    $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                                $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                            $xml_cac_InvoiceLine->appendChild($xml_cac_Item);

                            $xml_cac_Price = $xml->createElement("cac:Price");
                                $PriceAmount = $item->net_unit_price;
                                $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($PriceAmount, NUMBER_DECIMALS));
                                    $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                $xml_cac_Price->appendChild($xml_cbc_PriceAmount);

                                $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS));
                                    $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                                $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                            $xml_cac_InvoiceLine->appendChild($xml_cac_Price);
                        $xml_invoice->appendChild($xml_cac_InvoiceLine);

                        $consecutive_product_identifier++;
                    }
                /**************************************************************/

                /************************* DATA *******************************/
                    if ($this->Settings->fe_technology_provider == CADENA) {
                        $xml_DATA = $xml->createElement("DATA");
                            $xml_DATA->appendChild($xml->createElement("UBL21", "true"));

                            $xml_Partnership = $xml->createElement("Partnership");
                                $xml_Partnership->appendChild($xml->createElement("ID", "901090070"));
                                $xml_Partnership->appendChild($xml->createElement("TechKey", $resolution_data->clave_tecnica));
                                if ($this->Settings->fe_work_environment == TEST) {
                                    $xml_Partnership->appendChild($xml->createElement("SetTestID", $resolution_data->fe_testid));
                                }
                            $xml_DATA->appendChild($xml_Partnership);
                        $xml_invoice->appendChild($xml_DATA);
                    }
                /**************************************************************/
            $xml->appendChild($xml_invoice);

            if (!$print_xml) { return base64_encode($xml->saveXML()); }

            echo $xml->saveXML();
        }

        private function addUBLExtensionsInvoice($xml, $xml_invoice, $sale)
        {
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);

            $xml_ext_UBLExtensions = $xml->createElement("ext:UBLExtensions");
                $xml_ext_UBLExtension = $xml->createElement("ext:UBLExtension");
                    $xml_ext_ExtensionContent = $xml->createElement("ext:ExtensionContent");
                        $xml_sts_DianExtensions = $xml->createElement("sts:DianExtensions");
                            $xml_sts_InvoiceControl = $xml->createElement("sts:InvoiceControl");
                                $xml_sts_InvoiceControl->appendChild($xml->createElement("sts:InvoiceAuthorization", trim($resolution_data->num_resolucion, " ")));
                                $xml_sts_AuthorizationPeriod = $xml->createElement("sts:AuthorizationPeriod");
                                    $xml_sts_AuthorizationPeriod->appendChild($xml->createElement("cbc:StartDate", $resolution_data->emision_resolucion));
                                    $xml_sts_AuthorizationPeriod->appendChild($xml->createElement("cbc:EndDate", $resolution_data->vencimiento_resolucion));
                                $xml_sts_InvoiceControl->appendChild($xml_sts_AuthorizationPeriod);
                                $xml_sts_AuthorizedInvoices = $xml->createElement("sts:AuthorizedInvoices");
                                    $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:Prefix', $resolution_data->sales_prefix));
                                    $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:From', $resolution_data->inicio_resolucion));
                                    $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:To', $resolution_data->fin_resolucion));
                                    $xml_sts_InvoiceControl->appendChild($xml_sts_AuthorizedInvoices);
                            $xml_sts_DianExtensions->appendChild($xml_sts_InvoiceControl);
                        $xml_ext_ExtensionContent->appendChild($xml_sts_DianExtensions);
                    $xml_ext_UBLExtension->appendChild($xml_ext_ExtensionContent);
                $xml_ext_UBLExtensions->appendChild($xml_ext_UBLExtension);
            $xml_invoice->appendChild($xml_ext_UBLExtensions);
        }

        private function addHeaderInvoice($xml, $xml_invoice, $sale, $itemsSale, $resolution, $cufe)
        {
            if (!empty($sale->aiu_management)) {
                $xml_invoice->appendChild($xml->createElement("cbc:CustomizationID", "09"));
            } else {
                $xml_invoice->appendChild($xml->createElement("cbc:CustomizationID", "10"));
            }

            $profileExecutionID = ($resolution->fe_work_environment == TEST) ? TEST : PRODUCTION;
            $xml_invoice->appendChild($xml->createElement("cbc:ProfileExecutionID", $profileExecutionID));
            $xml_invoice->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $sale->reference_no)));

            $xml_cbc_UUID = $xml->createElement("cbc:UUID", $cufe);
                $xml_cbc_UUID->setAttribute("schemeID", $profileExecutionID);
                if ($resolution->factura_contingencia == YES) {
                    $xml_cbc_UUID->setAttribute("schemeName", "CUDE-SHA384");
                } else {
                    $xml_cbc_UUID->setAttribute("schemeName", "CUFE-SHA384");
                }
            $xml_invoice->appendChild($xml_cbc_UUID);

            $xml_invoice->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($sale->date)->date));
            $xml_invoice->appendChild($xml->createElement("cbc:IssueTime", $this->format_date($sale->date)->time));

            if ($resolution->factura_contingencia == YES) {
                $xml_invoice->appendChild($xml->createElement("cbc:InvoiceTypeCode", "03"));
            } else {
                $xml_invoice->appendChild($xml->createElement("cbc:InvoiceTypeCode", "01"));
            }

            $xml_invoice->appendChild($xml->createElement("cbc:DocumentCurrencyCode", "COP"));
            $xml_invoice->appendChild($xml->createElement("cbc:LineCountNumeric", count($itemsSale)));

            if (!empty($sale->purchase_order)) {
                $xml_cac_OrderReference = $xml->createElement("cac:OrderReference");
                   $xml_cac_OrderReference->appendChild($xml->createElement("cbc:ID", $sale->purchase_order));
                $xml_invoice->appendChild($xml_cac_OrderReference);
            }
        }

        private function addTaxInvoice($xml, $xml_invoice, $sale, $itemsSale, $productTax, $taxBase, $taxValue, $tax, $codeTax, $exemptExisting = 0, $ivaUtilidad = 0, $taxRateAIU = '')
        {
            if ($productTax) {
                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                    $TaxAmount = $this->sma->formatDecimal($productTax, NUMBER_DECIMALS);
                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                    foreach ($taxBase as $percentage => $base) {
                        if (!empty($base)) {
                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                if (!in_array($codeTax, ['IBUA', 'ICL'])) {
                                    $TaxableAmount = $this->sma->formatDecimal($base, NUMBER_DECIMALS);
                                    $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                        $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);
                                }

                                $TaxAmount = $taxValue[$percentage];
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                if (in_array($codeTax, ['IBUA', 'INC Bolsas', 'ICL', 'IC'])) {
                                    $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($taxBase[$percentage], NUMBER_DECIMALS));
                                       $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", ($codeTax == 'IBUA') ? 'MLT' : '94');
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                    $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS));
                                        $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);
                                }

                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                    if (!in_array($codeTax, ['ICL', 'IC'])) {
                                        $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS)));
                                    }
                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $tax));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $codeTax));
                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                        }
                    }

                $xml_invoice->appendChild($xml_cac_TaxTotal);
            } else if (count($itemsSale) >= 1 && $exemptExisting > 0) {
                if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                    $productTax += $ivaUtilidad;
                }

                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                    $TaxAmount = $this->sma->formatDecimal($productTax, NUMBER_DECIMALS);
                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                    if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                            $TaxableAmount = $this->sma->formatDecimal($sale->order_tax_aiu_base, NUMBER_DECIMALS);
                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                            $TaxAmount = $ivaUtilidad;
                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($taxRateAIU->rate, NUMBER_DECIMALS)));
                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                    } else {
                        foreach ($taxBase as $percentage => $base) {
                            if (!empty($base)) {
                                $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                    $TaxableAmount = $this->sma->formatDecimal($base, NUMBER_DECIMALS);
                                    $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                        $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                    $TaxAmount = $taxValue[$percentage];
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                    $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                        $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS)));
                                        $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                        $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                            }
                        }
                    }

                $xml_invoice->appendChild($xml_cac_TaxTotal);
            }
        }

        private function addTaxTotalInvoice($xml,$xml_invoice,$sale,$grossValue,$subtotal,$subtotalExclusive,$nominalTax,$totalTax,$productTaxIcl,$productTaxBolsas,$productTaxIcui,$productTaxIbua,$productTaxInc,$productTaxIc, $baseBolsas)
        {
            $totalAmountOfCharges = 0;
            $orderDiscount = 0;
            $totalTax = $this->sma->formatDecimal($totalTax, NUMBER_DECIMALS);
            $productTaxInc = $this->sma->formatDecimal($productTaxInc, NUMBER_DECIMALS);
            $nominalTax = $this->sma->formatDecimal($nominalTax, NUMBER_DECIMALS);
            $productTaxIcui = $this->sma->formatDecimal($productTaxIcui, NUMBER_DECIMALS);

            $xml_cac_LegalMonetaryTotal = $xml->createElement("cac:LegalMonetaryTotal");
                $LineExtensionAmount = ($grossValue);
                $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($LineExtensionAmount, NUMBER_DECIMALS));
                    $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                if (!empty($sale->aiu_management)) {
                    $baseImponible = (in_array($sale->order_tax_aiu_base_apply_to, [2, 3])) ? $sale->order_tax_aiu_base : ($subtotal + $subtotalExclusive);
                } else {
                    $baseImponible = $subtotal + $subtotalExclusive;
                }
                $TaxExclusiveAmount = $baseImponible + $baseBolsas;
                $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimal($TaxExclusiveAmount, NUMBER_DECIMALS));
                    $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", "COP");
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                if (!empty($productTaxIcl)) {
                    $nominalTax += $productTaxIcl;
                }
                if (!empty($productTaxIbua)) {
                    $nominalTax += $productTaxIbua;
                }
                if (!empty($productTaxBolsas)) {
                    $nominalTax += $productTaxBolsas;
                }
                if (!empty($productTaxIc)) {
                    $nominalTax += $productTaxIc;
                }

                if (!empty($sale->aiu_management)) {
                    $baseImponible = (in_array($sale->order_tax_aiu_base_apply_to, [2, 3])) ? $grossValue : ($subtotal + $subtotalExclusive);
                }

                $TaxInclusiveAmount = ($baseImponible + $totalTax + $productTaxInc + $nominalTax + $productTaxIcui);
                $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimal($TaxInclusiveAmount, NUMBER_DECIMALS));
                    $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", "COP");
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                if ($sale->order_discount > 0) {
                    $orderDiscount = $this->sma->formatDecimal($sale->order_discount, NUMBER_DECIMALS);

                    $AllowanceTotalAmount = $orderDiscount;
                    $xml_cbc_AllowanceTotalAmount = $xml->createElement("cbc:AllowanceTotalAmount", $this->sma->formatDecimal($AllowanceTotalAmount, NUMBER_DECIMALS));
                        $xml_cbc_AllowanceTotalAmount->setAttribute("currencyID", "COP");
                    $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_AllowanceTotalAmount);
                }

                if (!empty($sale->aiu_management) && !empty($sale->aiu_amounts_affects_grand_total)) {
                    if (($sale->shipping > 0  || $sale->aiu_admin_total > 0 || $sale->aiu_imprev_total > 0 || $sale->aiu_utilidad_total > 0)) {
                        $totalAmountOfCharges = $sale->shipping + $sale->aiu_admin_total + $sale->aiu_imprev_total + $sale->aiu_utilidad_total;

                        $ChargeTotalAmount = $totalAmountOfCharges;
                        $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($ChargeTotalAmount, NUMBER_DECIMALS));
                            $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", "COP");
                        $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                    }
                } else {
                    if ($sale->shipping > 0) {
                        $totalAmountOfCharges = $sale->shipping;

                        $ChargeTotalAmount = $totalAmountOfCharges;
                        $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($ChargeTotalAmount, NUMBER_DECIMALS));
                            $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", "COP");
                        $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                    }
                }

                if (!empty($sale->aiu_management)) {
                    $baseImponible = (in_array($sale->order_tax_aiu_base_apply_to, [2, 3])) ? $grossValue : ($subtotal + $subtotalExclusive);
                }

                $PayableAmount = ($baseImponible + $totalTax + $productTaxInc + $nominalTax + $productTaxIcui + $totalAmountOfCharges - $orderDiscount);
                $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimal($PayableAmount, NUMBER_DECIMALS));
                    $xml_cbc_PayableAmount->setAttribute("currencyID", "COP");
                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_PayableAmount);

            $xml_invoice->appendChild($xml_cac_LegalMonetaryTotal);
        }

        private function addTaxItem($xml, $xml_cac_InvoiceLine, $sale, $item)
        {
            $netUnitPrice = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
            $rate         = $this->sma->formatDecimal($item->rate / 100, NUMBER_DECIMALS);
            $quantity     = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

            if ($item->excluded == NOT) {
                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                    if ($item->code_fe == BOLSAS) {
                        $TaxAmount = $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->tax, NUMBER_DECIMALS));
                    } else {
                        if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                            if ($sale->order_tax_aiu_base_apply_to == 3) {
                                $utilidad = (($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS)) * $sale->aiu_utilidad_percentage) / 100;
                                $taxRateAIU = $this->site->getTaxRateByID($sale->order_tax_aiu_id);
                                $ivaUtilidad = $this->sma->formatDecimal((($utilidad * $taxRateAIU->rate) / 100), NUMBER_DECIMALS);
                            }
                            $TaxAmount = $ivaUtilidad;
                        } else {
                            $TaxAmount = $this->sma->formatDecimal(($netUnitPrice * $quantity) * $rate, NUMBER_DECIMALS);
                        }
                    }

                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                    $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                    if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                        $TaxableAmount = ($item->net_unit_price * $item->quantity);
                        if ($sale->order_tax_aiu_base_apply_to == 3) {
                            $utilidad = ($TaxableAmount * $sale->aiu_utilidad_percentage) / 100;
                        }

                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($utilidad, NUMBER_DECIMALS));
                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                            $taxRateAIU = $this->site->getTaxRateByID($sale->order_tax_aiu_id);

                            $TaxAmount = (($utilidad * $taxRateAIU->rate) / 100);
                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($taxRateAIU->rate, NUMBER_DECIMALS)));
                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                    } else {
                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                            if ($item->code_fe == BOLSAS) {
                                $TaxableAmount = $this->sma->formatDecimal($quantity, NUMBER_DECIMALS);
                            } else {
                                $TaxableAmount = $this->sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);
                            }
                            $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                            if ($item->code_fe == BOLSAS) {
                                $TaxAmount = $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->tax, NUMBER_DECIMALS));
                            } else {
                                $TaxAmount = $this->sma->formatDecimal(($netUnitPrice * $quantity) * $rate, NUMBER_DECIMALS);
                            }

                            $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                            if ($item->code_fe == BOLSAS) {
                                $TaxableAmount = $item->quantity;
                                $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                    $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                $PerUnitAmount = $item->tax;
                                $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                    $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);
                            }

                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                if ($item->code_fe != BOLSAS) {
                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($item->rate, NUMBER_DECIMALS)));
                                }
                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                    }
                $xml_cac_InvoiceLine->appendChild($xml_cac_TaxTotal);
            }
        }

        private function addTax2Item($xml, $xml_cac_InvoiceLine, $sale, $item)
        {
            if (!empty($item->tax_rate_2_id)) {

                $netUnitPrice = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                $rate         = $this->sma->formatDecimal(str_replace('%', '', $item->tax_2) / 100, NUMBER_DECIMALS);
                $quantity     = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                    if (in_array($item->nameTax2, ['IC', 'ICL'])) {
                        if ($item->nameTax2 == 'ICL') {
                            $degrees = $item->degrees;
                            $nominal = $item->nominal;
                            $mililiters = $item->mililiters;

                            if ($mililiters != 750) {
                                $itemTax =  intval(($mililiters * $nominal) / 750) * $degrees;
                            } else {
                                $itemTax = $nominal * $degrees;
                            }
                        } else {
                            $itemTax = $item->nominal * $item->quantity;
                        }
                    } else if (in_array($item->nameTax2, ['IBUA'])) {
                        $mililiters = $item->mililiters / 100;
                        $nominal = $item->nominal;
                        $itemTax = $this->sma->formatDecimal(($mililiters * $nominal), NUMBER_DECIMALS);
                    } else {
                        $itemTax  = $this->sma->formatDecimal(($netUnitPrice * $quantity) * $rate, NUMBER_DECIMALS);
                    }
                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($itemTax, NUMBER_DECIMALS));
                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                    $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);
                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                            if (!in_array($item->nameTax2, ['IBUA', 'ICL'])) {
                                if ($item->nameTax2 == 'IC') {
                                    $TaxableAmount = $this->sma->formatDecimal($quantity, NUMBER_DECIMALS);
                                } else {
                                    $TaxableAmount = $this->sma->formatDecimal($netUnitPrice * $quantity, NUMBER_DECIMALS);
                                }
                                $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                    $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);
                            }

                            $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($itemTax, NUMBER_DECIMALS));
                                $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                            if (in_array($item->nameTax2, ['IBUA', 'INC Bolsas', 'ICL', 'IC'])) {
                                if ($item->nameTax2 == 'ICL') {
                                    $BaseUnitMeasure = $item->degrees;
                                    $unitCode = 'MLT';
                                } else  if ($item->nameTax2 == 'IC') {
                                    $BaseUnitMeasure = $item->quantity;
                                    $unitCode = '94';
                                } else {
                                    $BaseUnitMeasure = $item->mililiters;
                                    $unitCode = 'MLT';
                                }
                                $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($BaseUnitMeasure, NUMBER_DECIMALS));
                                   $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", $unitCode);
                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($item->nominal, NUMBER_DECIMALS));
                                    $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);
                            }

                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                if (!in_array($item->nameTax2, ['ICL', 'IC', 'IBUA'])) {
                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal(str_replace('%', '', $item->tax_2), NUMBER_DECIMALS)));
                                }
                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->codeTax2));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->nameTax2));
                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                $xml_cac_InvoiceLine->appendChild($xml_cac_TaxTotal);
            }
        }

        public function cadenaBuildXmlCreditNote($credit_note_data, $print_xml = FALSE)
        {
            $reference_invoice = '';
            $issuer_data = $this->site->get_setting();
            $credit_note = $this->site->getSaleByID($credit_note_data->sale_id);
            $credit_note_items = $this->site->getAllSaleItems($credit_note_data->sale_id);
            $biller_data = $this->site->getCompanyByID($credit_note_data->biller_id);
            $customer_data = $this->site->getCompanyByID($credit_note_data->customer_id);
            $resolution_data = $this->site->getDocumentTypeById($credit_note->document_type_id);

            if (!empty($credit_note->year_database)) {
                $reference_invoice = $this->site->get_past_year_sale($credit_note->return_sale_ref, $credit_note->year_database);
                $resolution_data_referenced_invoice = $this->site->get_past_year_document_type_by_id($reference_invoice->document_type_id, $credit_note->year_database);
            } else {
                if (!empty($credit_note->sale_id)) {
                    $reference_invoice = $this->site->getSaleByID($credit_note->sale_id);
                    $resolution_data_referenced_invoice = $this->site->getDocumentTypeById($reference_invoice->document_type_id);
                }
            }

            $issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $advance_payments = $this->get_advance_payments_sale($credit_note_data->sale_id);
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER);
            $CUDE_code = $this->generate_CUDE($credit_note, $credit_note_items, $issuer_data, $customer_data, $resolution_data, $print_xml);
            $payments_data = $this->site->getSalePayments($credit_note_data->sale_id);

            if ($print_xml) { header("content-type: application/xml; charset=ISO-8859-15"); }

            $xml = new DOMDocument("1.0");
            $xml_credit_note = $xml->createElement("CreditNote");
                $xml_credit_note->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
                $xml_credit_note->setAttribute("xmlns", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2");
                $xml_credit_note->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                $xml_credit_note->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                $xml_credit_note->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                $xml_credit_note->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
                $xml_credit_note->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
                $xml_credit_note->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
                $xml_credit_note->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                $xml_credit_note->setAttribute("xsi:schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:CreditNote-2     http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-CreditNote-2.1.xsd");

                /************************* Encabezado *************************/
                    if (!empty($reference_invoice)) {
                        $xml_credit_note->appendChild($xml->createElement("cbc:CustomizationID", "20"));
                    } else {
                        $xml_credit_note->appendChild($xml->createElement("cbc:CustomizationID", "22"));
                    }

                    $ProfileExecutionID = ($resolution_data->fe_work_environment == TEST) ? TEST : PRODUCTION;
                    $xml_credit_note->appendChild($xml->createElement("cbc:ProfileExecutionID", $ProfileExecutionID));
                    $xml_credit_note->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $credit_note->reference_no)));

                    $xml_cbc_UUID = $xml->createElement("cbc:UUID", $CUDE_code);
                        $xml_cbc_UUID->setAttribute("schemeID", $ProfileExecutionID);
                        $xml_cbc_UUID->setAttribute("schemeName", "CUDE-SHA384");
                    $xml_credit_note->appendChild($xml_cbc_UUID);

                    $xml_credit_note->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($credit_note->date)->date));
                    $xml_credit_note->appendChild($xml->createElement("cbc:IssueTime", $this->format_date($credit_note->date)->time));
                    $xml_credit_note->appendChild($xml->createElement("cbc:CreditNoteTypeCode", CREDIT_NOTE));
                    $xml_credit_note->appendChild($xml->createElement("cbc:DocumentCurrencyCode", "COP"));
                    $xml_credit_note->appendChild($xml->createElement("cbc:LineCountNumeric", count($credit_note_items)));

                    if (empty($reference_invoice)) {
                        $xml_cac_InvoicePeriod = $xml->createElement("cac:InvoicePeriod");
                            $date = !empty($credit_note->document_without_reference_date) ? $credit_note->document_without_reference_date : $credit_note->date;
                            $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:StartDate", date("Y-m-01", strtotime($date))));
                            $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:StartTime", "00:00:00-05:00"));
                            $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:EndDate", date("Y-m-t", strtotime($date))));
                            $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:EndTime", "00:00:00-05:00"));
                        $xml_credit_note->appendChild($xml_cac_InvoicePeriod);
                    }
                /**************************************************************/

                /********************* Factura referencia *********************/
                    if (!empty($reference_invoice)) {
                        if ($credit_note->fe_debit_credit_note_concept_dian_code != '') {
                            $conceptCorrection = $this->site->get_debit_credit_note_concept_by_id($credit_note->fe_debit_credit_note_concept_dian_code);

                            $xml_cac_DiscrepancyResponse = $xml->createElement("cac:DiscrepancyResponse");
                                $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ReferenceID", str_replace('-', '', $credit_note->return_sale_ref)));
                                // $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ResponseCode", $credit_note->fe_debit_credit_note_concept_dian_code));
                                $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ResponseCode", $conceptCorrection->dian_code));
                                $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:Description", $conceptCorrection->dian_name));
                            $xml_credit_note->appendChild($xml_cac_DiscrepancyResponse);
                        } else {
                            $conceptCorrection = $this->getConceptCorrectionNote($credit_note, $reference_invoice);
                            if (!empty($conceptCorrection)) {
                                $xml_cac_DiscrepancyResponse = $xml->createElement("cac:DiscrepancyResponse");
                                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ReferenceID", str_replace('-', '', $credit_note->return_sale_ref)));
                                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ResponseCode", $conceptCorrection->code));
                                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:Description", $conceptCorrection->description));
                                $xml_credit_note->appendChild($xml_cac_DiscrepancyResponse);
                            }
                        }

                        $xml_cac_BillingReference = $xml->createElement("cac:BillingReference");
                            $xml_cac_InvoiceDocumentReference = $xml->createElement("cac:InvoiceDocumentReference");
                                $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $credit_note->return_sale_ref)));
                                    $xml_cbc_UUID = $xml->createElement("cbc:UUID", $reference_invoice->cufe);
                                        $xml_cbc_UUID->setAttribute("schemeName", "CUFE-SHA384");
                                    $xml_cac_InvoiceDocumentReference->appendChild($xml_cbc_UUID);
                                $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($reference_invoice->date)->date));
                            $xml_cac_BillingReference->appendChild($xml_cac_InvoiceDocumentReference);
                        $xml_credit_note->appendChild($xml_cac_BillingReference);
                    }
                /**************************************************************/

                /*************************** Emisor ***************************/
                    $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                        $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $issuer_data->tipo_persona));
                        $xml_cac_Party = $xml->createElement("cac:Party");
                            $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->nombre_comercial)));
                            $xml_cac_Party->appendChild($xml_cac_PartyName);

                            $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                $xml_cac_Address = $xml->createElement("cac:Address");
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($biller_data->city))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", $biller_data->DEPARTAMENTO));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                    $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", $biller_data->country);
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_Address->appendChild($xml_cac_Country);
                                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                            $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                            $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                    $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                $type_issuers_obligations = '';
                                foreach ($issuer_obligations as $issuer_obligation) {
                                    $type_issuers_obligations .= $issuer_obligation->types_obligations_id .';';
                                }

                                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_issuers_obligations, ";"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($biller_data->city))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", $biller_data->DEPARTAMENTO));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", $biller_data->country);
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                            $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                    $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);

                                $xml_cac_CorporateRegistrationScheme = $xml->createElement("cac:CorporateRegistrationScheme");
                                    $xml_cac_CorporateRegistrationScheme->appendChild($xml->createElement("cbc:ID", $resolution_data->sales_prefix));
                                $xml_cac_PartyLegalEntity->appendChild($xml_cac_CorporateRegistrationScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                            $xml_cac_Contact = $xml->createElement("cac:Contact");
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $issuer_data->default_email));
                            $xml_cac_Party->appendChild($xml_cac_Contact);
                        $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                    $xml_credit_note->appendChild($xml_cac_AccountingSupplierParty);
                /**************************************************************/

                /************************ Adquiriente. ************************/
                    $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                        $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer_data->type_person));
                        $xml_cac_Party = $xml->createElement("cac:Party");
                            if ($customer_data->type_person == NATURAL_PERSON) {
                                $xml_cac_PartyIdentification = $xml->createElement("cac:PartyIdentification");
                                    $xml_cbc_ID = $xml->createElement("cbc:ID", $this->clear_document_number($customer_data->vat_no));
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_ID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }
                                        $xml_cbc_ID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cac_PartyIdentification->appendChild($xml_cbc_ID);
                                $xml_cac_Party->appendChild($xml_cac_PartyIdentification);
                            }

                            $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                            $xml_cac_Party->appendChild($xml_cac_PartyName);

                            $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                $xml_cac_Address = $xml->createElement("cac:Address");
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($customer_data->city))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(strtolower($customer_data->state))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                    $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(strtolower($customer_data->country)));
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_Address->appendChild($xml_cac_Country);
                                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                            $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                            $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clear_document_number($customer_data->vat_no));
                                    if ($customer_data->document_code == NIT) {
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                    }

                                    $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                $type_customer_obligations = '';
                                foreach ($customer_obligations as $customer_obligation) {
                                    $type_customer_obligations .= $customer_obligation->types_obligations_id .';';
                                }
                                $type_customer_obligations = empty($type_customer_obligations) ? 'R-99-PN' : $type_customer_obligations;

                                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_customer_obligations, ";"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($customer_data->city))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(strtolower($customer_data->state))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));

                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);

                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(strtolower($customer_data->country)));
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                            $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));

                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $customer_data->vat_no);
                                    if ($customer_data->document_code == NIT) {
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                    }

                                    $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);
                            $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                            $xml_cac_Contact = $xml->createElement("cac:Contact");
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $customer_data->email));
                            $xml_cac_Party->appendChild($xml_cac_Contact);
                        $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                    $xml_credit_note->appendChild($xml_cac_AccountingCustomerParty);
                /**************************************************************/

                /************************ Medios pago *************************/
                    $payment_amount = 0;
                    $payments_clean_data = [];

                    if (!empty($payments_data)) {
                        foreach ($payments_data as $payment) {
                            $payment_amount += $payment->amount;

                            if ($payment->paid_by != 'retencion') {
                                $payments_clean_data[] = $payment;
                            }
                        }
                    }

                    if ($this->sma->formatDecimal($credit_note->grand_total, 2) == $this->sma->formatDecimal($payment_amount, 2)) {
                        $method = CASH;
                    } else {
                        $method = CREDIT;
                    }

                    if (!empty($payments_clean_data)) {
                        foreach ($payments_clean_data as $payment) {
                            $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $payment->mean_payment_code_fe));
                                if ($method == CREDIT) {
                                    $fecha_creacion = date("Y-m-d", strtotime($credit_note->date));
                                    if (empty($credit_note->payment_term)) {
                                        $payment_term = 0;
                                    } else {
                                        $payment_term = $credit_note->payment_term;
                                    }

                                    $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                    if ($payment_term > 0) {
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                    }
                                }
                            $xml_credit_note->appendChild($xml_cac_PaymentMeans);
                        }
                    } else {
                        $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                        if ($method == CREDIT) {
                            $fecha_creacion = date("Y-m-d", strtotime($credit_note->date));
                            if (empty($credit_note->payment_term)) {
                                $payment_term = 0;
                            } else {
                                $payment_term = $credit_note->payment_term;
                            }

                            $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                            if ($payment_term > 0) {
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                            }
                        }

                        $xml_credit_note->appendChild($xml_cac_PaymentMeans);
                    }
                    // $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                    //     $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $credit_note->payment_method_fe));
                    //     $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $credit_note->payment_mean_fe));

                    //     if ($credit_note->payment_method_fe == CREDIT) {
                    //         $fecha_creacion = date("Y-m-d", strtotime($credit_note->date));
                    //         $payment_term = (!empty($credit_note->payment_term) ? $credit_note->payment_term : 0);
                    //         $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));

                    //         $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));
                    //     }
                    // $xml_credit_note->appendChild($xml_cac_PaymentMeans);
                /**************************************************************/

                /**************************** TRM *****************************/
                    if ($credit_note->sale_currency != "COP") {
                        $xml_PaymentExchangeRate = $xml->createElement("cac:PaymentExchangeRate");
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyCode", "COP"));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyBaseRate", $credit_note->sale_currency_trm));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyCode", $credit_note->sale_currency));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyBaseRate", "1.00"));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:CalculationRate", $credit_note->sale_currency_trm));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:Date", $this->format_date($credit_note->date)->date));
                        $xml_credit_note->appendChild($xml_PaymentExchangeRate);
                    }
                /**************************************************************/

                /********************** Importes totales. *********************/
                    /************************ Impuestos. **********************/
                        $tax_rates = $this->get_tax_rate_by_code_fe();

                        $product_tax_iva = $product_tax_ic = $product_tax_inc = $product_tax_bolsas  = $subtotal = $total_tax = $shipping = $order_discount = $exempt_existing = $nominal_tax = 0;

                        foreach ($tax_rates as $tax_rate) {
                            if ($tax_rate->code_fe == IVA) {
                                $index = (int) $tax_rate->rate;
                                $tax_value_iva_array[$index] = 0;
                                $tax_base_iva_array[$index] = 0;
                            } else if ($tax_rate->code_fe == INC) {
                                $index = (int) $tax_rate->rate;
                                $tax_value_inc_array[$index] = 0;
                                $tax_base_inc_array[$index] = 0;
                            } else if ($tax_rate->code_fe == BOLSAS) {
                                $index = (int) $tax_rate->rate;
                                $tax_base_bolsas_array[$index] = 0;
                                $tax_value_bolsas_array[$index] = 0;
                            }
                        }

                        foreach ($credit_note_items as $item) {
                            if ($item->consumption_sales > 0) {
                                $tax_base_ic_array[$item->consumption_sales] = 0;
                                $tax_value_ic_array[$item->consumption_sales] = 0;
                            }
                        }

                        foreach ($credit_note_items as $item) {
                            $subtotal += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                            $total_tax += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                            if ($item->code_fe == IVA) {
                                $product_tax_iva += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                foreach ($tax_rates as $tax_rate) {
                                    if ($item->rate == $tax_rate->rate) {
                                        if ($item->rate == 0) { $exempt_existing++; }

                                        $index = (int) $tax_rate->rate;
                                        $tax_base_iva_array[$index] += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                                        $tax_value_iva_array[$index] += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                        break;
                                    }
                                }
                            } else if ($item->code_fe == INC) {
                                $product_tax_inc += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                foreach ($tax_rates as $tax_rate) {
                                    if ($item->rate == $tax_rate->rate) {
                                        $index = (int) $tax_rate->rate;
                                        $tax_base_inc_array[$index] += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                                        $tax_value_inc_array[$index] += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                        break;
                                    }
                                }
                            } else if ($item->code_fe == BOLSAS) {
                                $index = (int) $item->rate;
                                $product_tax_bolsas += $this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS);
                                $tax_base_bolsas_array[$index] += $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                                $tax_value_bolsas_array[$index] += $this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS);
                            }

                            if ($item->consumption_sales > 0) {
                                $product_tax_ic += $this->sma->formatDecimal($item->consumption_sales * $item->quantity, NUMBER_DECIMALS);

                                $tax_base_ic_array[$item->consumption_sales] = $item->quantity;
                                $tax_value_ic_array[$item->consumption_sales] = $this->sma->formatDecimal($item->consumption_sales * $item->quantity, NUMBER_DECIMALS);
                            }
                        }

                        /******************** Descuentos y cargos *********************/
                            $charge_or_discount = 1;

                            if (($credit_note->order_discount * -1) > 0) {
                                $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "false"));
                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReasonCode", "01"));
                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReason", htmlspecialchars(str_replace(["&lt;p&gt;", "&lt;&sol;p&gt;", "&nbsp", "&comma;"], "", $credit_note->note))));

                                    if (strpos($credit_note->order_discount_id, "%") !== FALSE) {
                                        $order_discount_percentage = str_replace("%", "", $credit_note->order_discount_id);
                                    } else {
                                        $order_discount_percentage = (($credit_note->order_discount * -1) * 100) / ($subtotal * -1);
                                    }

                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimalNoRound($order_discount_percentage, NUMBER_DECIMALS)));

                                    $Amount = ($credit_note->order_discount * -1);
                                    $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                        $xml_cac_Amount->setAttribute("currencyID", "COP");
                                    $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                                    $BaseAmount = ($subtotal * -1);
                                    $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                        $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);
                                $xml_credit_note->appendChild($xml_cac_AllowanceCharge);
                            }

                            if (($credit_note->shipping * -1) > 0) {
                                $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                                    $order_charge_percentage = ($credit_note->shipping * 100) / $credit_note->total;
                                    $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($order_charge_percentage, NUMBER_DECIMALS)));

                                    $Amount = $credit_note->shipping;
                                    $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount * -1, NUMBER_DECIMALS));
                                        $xml_cac_Amount->setAttribute("currencyID", "COP");
                                    $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                                    $BaseAmount = $credit_note->total;
                                    $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount * -1, NUMBER_DECIMALS));
                                        $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                                $xml_credit_note->appendChild($xml_cac_AllowanceCharge);
                            }
                        /**************************************************************/

                        if ($product_tax_iva) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $this->sma->formatDecimal($product_tax_iva, NUMBER_DECIMALS);
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                    if (!empty($tax_base_iva)) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $this->sma->formatDecimal($tax_base_iva, NUMBER_DECIMALS);
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_iva_array[$percentage];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS)));
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                }

                            $xml_credit_note->appendChild($xml_cac_TaxTotal);
                        } else if (count($credit_note_items) >= 1 && $exempt_existing > 0) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $this->sma->formatDecimal($product_tax_iva, NUMBER_DECIMALS);
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                    if (!empty($tax_base_iva)) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $this->sma->formatDecimal($tax_base_iva, NUMBER_DECIMALS);
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_iva_array[$percentage];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS)));
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                }

                            $xml_credit_note->appendChild($xml_cac_TaxTotal);
                        }

                        if ($product_tax_ic) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $product_tax_ic;
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_ic_array as $nominal => $tax_base_ic) {
                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $tax_base_ic;
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $TaxAmount = $tax_value_ic_array[$nominal];
                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, NUMBER_DECIMALS));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $BaseUnitMeasure = $tax_base_ic;
                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($BaseUnitMeasure, NUMBER_DECIMALS));
                                            $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $PerUnitAmount = $nominal;
                                        $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                            $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);


                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($nominal, NUMBER_DECIMALS)));
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IC));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IC"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                }

                            $xml_credit_note->appendChild($xml_cac_TaxTotal);
                        }

                        if ($product_tax_inc) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $this->sma->formatDecimal($product_tax_inc, NUMBER_DECIMALS);
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_inc_array as $percentage => $tax_base_inc) {
                                    if (!empty($tax_base_inc)) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $tax_base_inc;
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_inc_array[$percentage];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS))) ;
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", INC));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                }

                            $xml_credit_note->appendChild($xml_cac_TaxTotal);
                        }

                        if ($product_tax_bolsas) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $product_tax_bolsas;
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);
                                foreach ($tax_base_bolsas_array as $nominal => $tax_base_bolsas) {
                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(0, NUMBER_DECIMALS));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $TaxAmount = $tax_value_bolsas_array[$nominal];
                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, NUMBER_DECIMALS));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $BaseUnitMeasure = $tax_base_bolsas;
                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal(abs($BaseUnitMeasure), NUMBER_DECIMALS));
                                            $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $PerUnitAmount = $nominal;
                                        $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                            $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", BOLSAS));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC Bolsas"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                }
                            $xml_credit_note->appendChild($xml_cac_TaxTotal);
                        }
                    /***********************************************************/
                /**************************************************************/

                /********************** Total impuestos ***********************/
                    $xml_cac_LegalMonetaryTotal = $xml->createElement("cac:LegalMonetaryTotal");
                        $LineExtensionAmount = abs($subtotal);
                        $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($LineExtensionAmount, NUMBER_DECIMALS));
                            $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                        $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                        $TaxExclusiveAmount = $subtotal;
                        $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimal(abs($TaxExclusiveAmount), NUMBER_DECIMALS));
                            $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", "COP");
                        $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                        if (!empty($product_tax_ic)) {
                            $nominal_tax += $product_tax_ic;
                        }

                        if (!empty($product_tax_bolsas)) {
                            $nominal_tax += $product_tax_bolsas;
                        }

                        $TaxInclusiveAmount = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($total_tax, NUMBER_DECIMALS) + $this->sma->formatDecimal($nominal_tax, NUMBER_DECIMALS));
                        $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimal(abs($TaxInclusiveAmount), NUMBER_DECIMALS));
                            $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", "COP");
                        $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                        $AllowanceTotalAmount = 0;
                        if (($credit_note->order_discount * -1) > 0) {
                            $order_discount = $credit_note->order_discount;

                            $AllowanceTotalAmount = abs($credit_note->order_discount);
                            $xml_cbc_AllowanceTotalAmount = $xml->createElement("cbc:AllowanceTotalAmount", $this->sma->formatDecimal($AllowanceTotalAmount, NUMBER_DECIMALS));
                                $xml_cbc_AllowanceTotalAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_AllowanceTotalAmount);
                        }

                        $ChargeTotalAmount = 0;
                        if (($credit_note->shipping * -1) > 0) {
                            $shipping = $credit_note->shipping;

                            $ChargeTotalAmount = abs($credit_note->shipping);
                            $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($ChargeTotalAmount, NUMBER_DECIMALS));
                                $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                        }

                        $PayableAmount = abs($TaxInclusiveAmount) + abs($ChargeTotalAmount) - abs($AllowanceTotalAmount);
                        $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimal($PayableAmount, NUMBER_DECIMALS));
                            $xml_cbc_PayableAmount->setAttribute("currencyID", "COP");
                        $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_PayableAmount);

                    $xml_credit_note->appendChild($xml_cac_LegalMonetaryTotal);
                /**************************************************************/

                /************************* Productos **************************/
                    $consecutive_product_identifier = 1;
                    foreach ($credit_note_items as $item) {
                        $xml_cac_CreditNoteLine = $xml->createElement("cac:CreditNoteLine");
                            $xml_cac_CreditNoteLine->appendChild($xml->createElement("cbc:ID", $consecutive_product_identifier));
                            $xml_cbc_CreditedQuantity = $xml->createElement("cbc:CreditedQuantity", $this->sma->formatDecimal(abs($item->quantity), NUMBER_DECIMALS));
                                $xml_cbc_CreditedQuantity->setAttribute("unitCode", "NIU");
                            $xml_cac_CreditNoteLine->appendChild($xml_cbc_CreditedQuantity);

                            $LineExtensionAmount = ($item->net_unit_price * $item->quantity);
                            $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal(abs($LineExtensionAmount), NUMBER_DECIMALS));
                                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                            $xml_cac_CreditNoteLine->appendChild($xml_cbc_LineExtensionAmount);

                            if ($item->code_fe == BOLSAS) {
                                $xml_cac_PricingReference = $xml->createElement("cac:PricingReference");
                                    $xml_cac_AlternativeConditionPrice = $xml->createElement("cac:AlternativeConditionPrice");
                                        $PriceAmount = (($item->net_unit_price * $item->quantity) <= 0) ? 1 : ($item->net_unit_price * $item->quantity);
                                        $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($PriceAmount, NUMBER_DECIMALS));
                                            $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_AlternativeConditionPrice->appendChild($xml_cbc_PriceAmount);

                                        $xml_cac_AlternativeConditionPrice->appendChild($xml->createElement("cbc:PriceTypeCode", "03"));
                                    $xml_cac_PricingReference->appendChild($xml_cac_AlternativeConditionPrice);
                                $xml_cac_CreditNoteLine->appendChild($xml_cac_PricingReference);
                            }

                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                if ($item->code_fe == BOLSAS) {
                                    $TaxAmount = $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS));
                                } else {
                                    $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                                }

                                $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, NUMBER_DECIMALS));
                                    $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                                $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                    $TaxableAmount = ($item->net_unit_price * $item->quantity);
                                    $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                        $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);


                                    if ($item->code_fe == BOLSAS) {
                                        $TaxAmount = $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS));
                                    } else {
                                        $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                                    }
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                    if ($item->code_fe == BOLSAS) {
                                        $TaxableAmount = $item->quantity;
                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($TaxableAmount * -1, NUMBER_DECIMALS));
                                            $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $PerUnitAmount = $item->rate;
                                        $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                            $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);
                                    }

                                    $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                        if ($item->code_fe != BOLSAS) {
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($item->rate, NUMBER_DECIMALS)));
                                        }
                                        $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                            $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                        $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                    $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                            $xml_cac_CreditNoteLine->appendChild($xml_cac_TaxTotal);

                            if ($item->consumption_sales > 0) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_ic;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                                    foreach ($tax_base_ic_array as $nominal => $tax_base_ic) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $tax_base_ic;
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_ic_array[$nominal];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $TaxableAmount = $tax_base_ic;
                                            $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                            $PerUnitAmount = $nominal;
                                            $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                                $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IC));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IC"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                $xml_cac_CreditNoteLine->appendChild($xml_cac_TaxTotal);
                            }

                            $xml_cac_Item = $xml->createElement("cac:Item");
                                $xml_cac_Item->appendChild($xml->createElement("cbc:Description", htmlspecialchars($item->product_name)));

                                $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                                    $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                                        $xml_cbc_ID->setAttribute("schemeID", "999");
                                    $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                                $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                            $xml_cac_CreditNoteLine->appendChild($xml_cac_Item);

                            $xml_cac_Price = $xml->createElement("cac:Price");
                                $PriceAmount = $item->net_unit_price;
                                $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($PriceAmount, NUMBER_DECIMALS));
                                    $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                $xml_cac_Price->appendChild($xml_cbc_PriceAmount);

                                $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $this->sma->formatDecimal(abs($item->quantity), NUMBER_DECIMALS));
                                    $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                                $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                            $xml_cac_CreditNoteLine->appendChild($xml_cac_Price);
                        $xml_credit_note->appendChild($xml_cac_CreditNoteLine);

                        $consecutive_product_identifier++;
                    }
                /**************************************************************/

                /************************* DATA *******************************/
                    if ($this->Settings->fe_technology_provider == CADENA) {
                        $xml_DATA = $xml->createElement("DATA");
                            $xml_DATA->appendChild($xml->createElement("UBL21", "true"));

                            $xml_Partnership = $xml->createElement("Partnership");
                                $xml_Partnership->appendChild($xml->createElement("ID", "901090070"));
                                if (!empty($reference_invoice)) {
                                    $xml_Partnership->appendChild($xml->createElement("TechKey", $resolution_data_referenced_invoice->clave_tecnica));
                                    if ($this->Settings->fe_work_environment == TEST) {
                                        $xml_Partnership->appendChild($xml->createElement("SetTestID", $resolution_data_referenced_invoice->fe_testid));
                                    }
                                } else {
                                    $xml_Partnership->appendChild($xml->createElement("TechKey", "N/A"));
                                }
                            $xml_DATA->appendChild($xml_Partnership);
                        $xml_credit_note->appendChild($xml_DATA);
                    }
                /**************************************************************/
            $xml->appendChild($xml_credit_note);

            if (!$print_xml) { return base64_encode($xml->saveXML()); }

            echo $xml->saveXML();
        }

        public function cadenaBuildXmlDebitNote($debit_note_data, $print_xml = FALSE)
        {
            $issuer_data = $this->site->get_setting();
            $debit_note = $this->site->getSaleByID($debit_note_data->sale_id);
            $items_sale = $this->site->getAllSaleItems($debit_note_data->sale_id);
            $biller_data = $this->site->getCompanyByID($debit_note->biller_id);
            $customer_data = $this->site->getCompanyByID($debit_note->customer_id);
            $reference_invoice = $this->site->getSaleByID($debit_note->reference_invoice_id);
            $resolution_data = $this->site->getDocumentTypeById($debit_note->document_type_id);
            $issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $advance_payments = $this->get_advance_payments_sale($debit_note->sale_id);
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER);
            $resolution_data_referenced_invoice = $this->site->getDocumentTypeById($reference_invoice->document_type_id);
            $CUFE_code = $this->generate_CUDE($debit_note, $items_sale, $issuer_data, $customer_data, $resolution_data);

            if ($print_xml) { header("content-type: application/xml; charset=ISO-8859-15"); }

            $xml = new DOMDocument("1.0");
            $xml_debit_note = $xml->createElement("DebitNote");
                $xml_debit_note->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
                $xml_debit_note->setAttribute("xmlns", "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2");
                $xml_debit_note->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                $xml_debit_note->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                $xml_debit_note->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                $xml_debit_note->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
                $xml_debit_note->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
                $xml_debit_note->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
                $xml_debit_note->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                $xml_debit_note->setAttribute("xsi:schemaLocation", "urn:oasis:names:specification:ubl:schema:xsd:DebitNote-2 http://docs.oasis-open.org/ubl/os-UBL-2.1/xsd/maindoc/UBL-DebitNote-2.1.xsd");

                /************************* Encabezado *************************/
                    $xml_debit_note->appendChild($xml->createElement("cbc:CustomizationID", "30"));

                    $ProfileExecutionID = ($resolution_data->fe_work_environment == TEST) ? TEST : PRODUCTION;
                    $xml_debit_note->appendChild($xml->createElement("cbc:ProfileExecutionID", $ProfileExecutionID));

                    $xml_debit_note->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $debit_note->reference_no)));

                    $xml_cbc_UUID = $xml->createElement("cbc:UUID", $CUFE_code);
                        $xml_cbc_UUID->setAttribute("schemeID", $ProfileExecutionID);
                        $xml_cbc_UUID->setAttribute("schemeName", "CUDE-SHA384");
                    $xml_debit_note->appendChild($xml_cbc_UUID);

                    $xml_debit_note->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($debit_note->date)->date));
                    $xml_debit_note->appendChild($xml->createElement("cbc:IssueTime", $this->format_date($debit_note->date)->time));

                    $xml_debit_note->appendChild($xml->createElement("cbc:DocumentCurrencyCode", "COP"));
                    $xml_debit_note->appendChild($xml->createElement("cbc:LineCountNumeric", count($items_sale)));
                /**************************************************************/

                /************************ Discrepancia ************************/
                    $xml_cac_DiscrepancyResponse = $xml->createElement("cac:DiscrepancyResponse");
                        $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ResponseCode", $debit_note->fe_debit_credit_note_concept_dian_code));
                        $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:Description", htmlspecialchars($items_sale[0]->product_name)));
                    $xml_debit_note->appendChild($xml_cac_DiscrepancyResponse);
                /**************************************************************/

                /********************* Factura referencia *********************/
                    $xml_cac_BillingReference = $xml->createElement("cac:BillingReference");
                        $xml_cac_InvoiceDocumentReference = $xml->createElement("cac:InvoiceDocumentReference");
                            $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $reference_invoice->reference_no)));
                            $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:UUID", $reference_invoice->cufe));
                            $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($reference_invoice->date)->date));
                        $xml_cac_BillingReference->appendChild($xml_cac_InvoiceDocumentReference);
                    $xml_debit_note->appendChild($xml_cac_BillingReference);
                /**************************************************************/

                /*************************** Emisor ***************************/
                    $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                        $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $issuer_data->tipo_persona));
                        $xml_cac_Party = $xml->createElement("cac:Party");
                            $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->nombre_comercial)));
                            $xml_cac_Party->appendChild($xml_cac_PartyName);

                            $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                $xml_cac_Address = $xml->createElement("cac:Address");
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($biller_data->city))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", $biller_data->DEPARTAMENTO));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                    $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", $biller_data->country);
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_Address->appendChild($xml_cac_Country);
                                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                            $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                            $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                    $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                $type_issuers_obligations = '';
                                foreach ($issuer_obligations as $issuer_obligation) {
                                    $type_issuers_obligations .= $issuer_obligation->types_obligations_id .';';
                                }

                                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_issuers_obligations, ";"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($biller_data->city))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", $biller_data->DEPARTAMENTO));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", $biller_data->country);
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                            $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                    $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);

                                $xml_cac_CorporateRegistrationScheme = $xml->createElement("cac:CorporateRegistrationScheme");
                                    $xml_cac_CorporateRegistrationScheme->appendChild($xml->createElement("cbc:ID", $resolution_data->sales_prefix));
                                $xml_cac_PartyLegalEntity->appendChild($xml_cac_CorporateRegistrationScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                            $xml_cac_Contact = $xml->createElement("cac:Contact");
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->razon_social)));
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $issuer_data->default_email));
                            $xml_cac_Party->appendChild($xml_cac_Contact);
                        $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                    $xml_debit_note->appendChild($xml_cac_AccountingSupplierParty);
                /**************************************************************/

                /************************ Adquiriente. ************************/
                    $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                        $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer_data->type_person));
                        $xml_cac_Party = $xml->createElement("cac:Party");
                            if ($customer_data->type_person == NATURAL_PERSON) {
                                $xml_cac_PartyIdentification = $xml->createElement("cac:PartyIdentification");
                                    $xml_cbc_ID = $xml->createElement("cbc:ID", $this->clear_document_number($customer_data->vat_no));
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_ID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }
                                        $xml_cbc_ID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cac_PartyIdentification->appendChild($xml_cbc_ID);
                                $xml_cac_Party->appendChild($xml_cac_PartyIdentification);
                            }

                            $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                            $xml_cac_Party->appendChild($xml_cac_PartyName);

                            $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                $xml_cac_Address = $xml->createElement("cac:Address");
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($customer_data->city))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(strtolower($customer_data->state))));
                                    $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));
                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                    $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(strtolower($customer_data->country)));
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_Address->appendChild($xml_cac_Country);
                                $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                            $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                            $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));
                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clear_document_number($customer_data->vat_no));
                                    if ($customer_data->document_code == NIT) {
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                    }

                                    $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                $type_customer_obligations = '';
                                foreach ($customer_obligations as $customer_obligation) {
                                    $type_customer_obligations .= $customer_obligation->types_obligations_id .';';
                                }
                                $type_customer_obligations = empty($type_customer_obligations) ? 'R-99-PN' : $type_customer_obligations;

                                $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_customer_obligations, ";"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(strtolower($customer_data->city))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(strtolower($customer_data->state))));
                                    $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));

                                    $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                        $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);

                                    $xml_cac_Country = $xml->createElement("cac:Country");
                                        $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                        $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(strtolower($customer_data->country)));
                                            $xml_cbc_Name->setAttribute("languageID", "es");
                                        $xml_cac_Country->appendChild($xml_cbc_Name);
                                    $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                            $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                            $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));

                                $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $customer_data->vat_no);
                                    if ($customer_data->document_code == NIT) {
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                    }

                                    $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                    $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);
                            $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                            $xml_cac_Contact = $xml->createElement("cac:Contact");
                                $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $customer_data->email));
                            $xml_cac_Party->appendChild($xml_cac_Contact);
                        $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                    $xml_debit_note->appendChild($xml_cac_AccountingCustomerParty);
                /**************************************************************/

                /************************ Medios pago *************************/
                    $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $debit_note->payment_method_fe));
                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $debit_note->payment_mean_fe));

                        if ($debit_note->payment_method_fe == CREDIT) {
                            $fecha_creacion = date("Y-m-d", strtotime($debit_note->date));
                            $payment_term = (!empty($debit_note->payment_term) ? $debit_note->payment_term : 0);

                            $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));

                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));
                        }
                    $xml_debit_note->appendChild($xml_cac_PaymentMeans);
                /**************************************************************/

                /************************* TRM **************************/
                    if ($debit_note->sale_currency != "COP") {
                        $xml_PaymentExchangeRate = $xml->createElement("cac:PaymentExchangeRate");
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyCode", "COP"));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyBaseRate", $debit_note->sale_currency_trm));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyCode", $debit_note->sale_currency));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyBaseRate", "1.00"));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:CalculationRate", $debit_note->sale_currency_trm));
                            $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:Date", $this->format_date($debit_note->date)->date));
                        $xml_debit_note->appendChild($xml_PaymentExchangeRate);
                    }
                /**************************************************************/

                /******************** Descuentos y cargos *********************/
                    $charge_or_discount = 1;

                    if ($debit_note->order_discount > 0) {
                        $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "false"));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReasonCode", "11"));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReason", "Otro descuento"));

                            if (strpos($debit_note->order_discount_id, "%") !== FALSE) {
                                $order_discount_percentage = str_replace("%", "", $debit_note->order_discount_id);
                            } else {
                                $order_discount_percentage = ($debit_note->order_discount * 100) / $debit_note->total + $debit_note->tax_total;
                            }

                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimalNoRound($order_discount_percentage, NUMBER_DECIMALS)));

                            $Amount = $debit_note->order_discount;
                            $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                $xml_cac_Amount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                            $BaseAmount = ($debit_note->total + $debit_note->tax_total);
                            $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);
                        $xml_debit_note->appendChild($xml_cac_AllowanceCharge);
                    }

                    if ($debit_note->shipping > 0) {
                        $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                            $order_charge_percentage = ($debit_note->shipping * 100) / $debit_note->total;
                            $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($order_charge_percentage, NUMBER_DECIMALS)));

                            $Amount = $debit_note->shipping;
                            $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, NUMBER_DECIMALS));
                                $xml_cac_Amount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                            $BaseAmount = $debit_note->total;
                            $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, NUMBER_DECIMALS));
                                $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                            $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                        $xml_debit_note->appendChild($xml_cac_AllowanceCharge);
                    }
                /**************************************************************/

                /********************** Importes totales. *********************/
                    /************************ Impuestos. **********************/
                        $product_tax_iva = $product_tax_ic = $product_tax_inc = $product_tax_bolsas = $subtotal = $total_tax = $shipping = $order_discount = $exempt_existing = $nominal_tax = 0;

                        $tax_rates = $this->get_tax_rate_by_code_fe();

                        foreach ($tax_rates as $tax_rate) {
                            if ($tax_rate->code_fe == IVA) {
                                $index = (int) $tax_rate->rate;
                                $tax_value_iva_array[$index] = 0;
                                $tax_base_iva_array[$index] = 0;
                            } else if ($tax_rate->code_fe == INC) {
                                $index = (int) $tax_rate->rate;
                                $tax_value_inc_array[$index] = 0;
                                $tax_base_inc_array[$index] = 0;
                            } else if ($tax_rate->code_fe == BOLSAS) {
                                $index = (int) $tax_rate->rate;
                                $tax_base_bolsas_array[$index] = 0;
                                $tax_value_bolsas_array[$index] = 0;
                            }
                        }

                        foreach ($items_sale as $item) {
                            $subtotal += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                            $total_tax += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                            if ($item->code_fe == IVA) {
                                $product_tax_iva += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                foreach ($tax_rates as $tax_rate) {
                                    if ($item->rate == $tax_rate->rate) {
                                        if ($item->rate == 0) { $exempt_existing++; }

                                        $index = (int) $tax_rate->rate;
                                        $tax_base_iva_array[$index] +=  $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                                        $tax_value_iva_array[$index] += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                        break;
                                    }
                                }
                            } else if ($item->code_fe == INC) {
                                $product_tax_inc += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                foreach ($tax_rates as $tax_rate) {
                                    if ($item->rate == $tax_rate->rate) {
                                        $index = (int) $tax_rate->rate;
                                        $tax_base_inc_array[$index] += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                                        $tax_value_inc_array[$index] += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                                        break;
                                    }
                                }
                            } else if ($item->code_fe == BOLSAS) {
                                $index = (int) $item->rate;
                                $product_tax_bolsas += $this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS);
                                $tax_base_bolsas_array[$index] += $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                                $tax_value_bolsas_array[$index] += $this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS);
                            }

                            if ($item->consumption_sales > 0) {
                                $product_tax_ic += $this->sma->formatDecimal($item->consumption_sales * $item->quantity, NUMBER_DECIMALS);

                                $tax_base_ic_array[$item->consumption_sales] = $item->quantity;
                                $tax_value_ic_array[$item->consumption_sales] = $this->sma->formatDecimal($item->consumption_sales * $item->quantity, NUMBER_DECIMALS);

                            }
                        }

                        if ($product_tax_iva) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $this->sma->formatDecimal($product_tax_iva, NUMBER_DECIMALS);
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                    if (!empty($tax_base_iva)) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $this->sma->formatDecimal($tax_base_iva, NUMBER_DECIMALS);
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_iva_array[$percentage];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS)));
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                }

                            $xml_debit_note->appendChild($xml_cac_TaxTotal);
                        } else if (count($items_sale) >= 1 && $exempt_existing > 0) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $this->sma->formatDecimal($product_tax_iva, NUMBER_DECIMALS);
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                    if (!empty($tax_base_iva)) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $this->sma->formatDecimal($tax_base_iva, NUMBER_DECIMALS);
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_iva_array[$percentage];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS)));
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                }

                            $xml_debit_note->appendChild($xml_cac_TaxTotal);
                        }

                        if ($product_tax_ic) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $product_tax_ic;
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_ic_array as $nominal => $tax_base_ic) {
                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $tax_base_ic;
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $TaxAmount = $tax_value_ic_array[$nominal];
                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $BaseUnitMeasure = $tax_base_ic;
                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($BaseUnitMeasure, NUMBER_DECIMALS));
                                            $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $PerUnitAmount = $nominal;
                                        $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                            $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IC));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IC"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                }

                            $xml_debit_note->appendChild($xml_cac_TaxTotal);
                        }

                        if ($product_tax_inc) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $this->sma->formatDecimal($product_tax_inc, NUMBER_DECIMALS);
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_inc_array as $percentage => $tax_base_inc) {
                                    if (!empty($tax_base_inc)) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $this->sma->formatDecimal($tax_base_inc, NUMBER_DECIMALS);
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_inc_array[$percentage];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, NUMBER_DECIMALS)));
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", INC));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                }

                            $xml_debit_note->appendChild($xml_cac_TaxTotal);
                        }

                        if ($product_tax_bolsas) {
                            $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                $TaxAmount = $product_tax_bolsas;
                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                foreach ($tax_base_bolsas_array as $nominal => $tax_base_bolsas) {
                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(0, NUMBER_DECIMALS));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $TaxAmount = $tax_value_bolsas_array[$nominal];
                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $BaseUnitMeasure = $tax_base_bolsas;
                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($BaseUnitMeasure, NUMBER_DECIMALS));
                                            $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $PerUnitAmount = $nominal;
                                        $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                            $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $Percentage = $nominal;
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($Percentage, NUMBER_DECIMALS)));
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", BOLSAS));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC Bolsas"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                }
                            $xml_debit_note->appendChild($xml_cac_TaxTotal);
                        }
                    /***********************************************************/
                /**************************************************************/

                /********************** Total impuestos ***********************/
                    $xml_cac_RequestedMonetaryTotal = $xml->createElement("cac:RequestedMonetaryTotal");
                        $LineExtensionAmount = $subtotal;
                        $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($LineExtensionAmount, NUMBER_DECIMALS));
                            $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                        $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                        $TaxExclusiveAmount = $subtotal;
                        $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimal($TaxExclusiveAmount, NUMBER_DECIMALS));
                            $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", "COP");
                        $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                        if (!empty($product_tax_ic)) {
                            $nominal_tax += $product_tax_ic;
                        }

                        if (!empty($product_tax_bolsas)) {
                            $nominal_tax += $product_tax_bolsas;
                        }

                        $TaxInclusiveAmount = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($total_tax, NUMBER_DECIMALS) + $this->sma->formatDecimal($nominal_tax, NUMBER_DECIMALS));
                        $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimal($TaxInclusiveAmount, NUMBER_DECIMALS));
                            $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", "COP");
                        $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                        if ($debit_note->order_discount > 0) {
                            $order_discount = $debit_note->order_discount;

                            $AllowanceTotalAmount = $order_discount;
                            $xml_cbc_AllowanceTotalAmount = $xml->createElement("cbc:AllowanceTotalAmount", $this->sma->formatDecimal($AllowanceTotalAmount, NUMBER_DECIMALS));
                                $xml_cbc_AllowanceTotalAmount->setAttribute("currencyID", "COP");
                            $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_AllowanceTotalAmount);
                        }

                        if ($debit_note->shipping > 0) {
                            $shipping = $debit_note->shipping;

                            $ChargeTotalAmount = $shipping;
                            $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($ChargeTotalAmount, NUMBER_DECIMALS));
                                $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", "COP");
                            $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                        }

                        $PayableAmount = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($total_tax, NUMBER_DECIMALS) + $this->sma->formatDecimal($nominal_tax, NUMBER_DECIMALS) + $shipping - $order_discount);
                        $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimal($PayableAmount, NUMBER_DECIMALS));
                            $xml_cbc_PayableAmount->setAttribute("currencyID", "COP");
                        $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_PayableAmount);
                    $xml_debit_note->appendChild($xml_cac_RequestedMonetaryTotal);
                /**************************************************************/

                /************************* Productos **************************/
                    $consecutive_product_identifier = 1;
                    foreach ($items_sale as $item) {
                        if ($item->code_fe != BOLSAS) {
                            $xml_cac_DebitNoteLine = $xml->createElement("cac:DebitNoteLine");
                                $xml_cac_DebitNoteLine->appendChild($xml->createElement("cbc:ID", $consecutive_product_identifier));
                                $xml_cbc_DebitedQuantity = $xml->createElement("cbc:DebitedQuantity", $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS));
                                    $xml_cbc_DebitedQuantity->setAttribute("unitCode", "NIU");
                                $xml_cac_DebitNoteLine->appendChild($xml_cbc_DebitedQuantity);

                                $LineExtensionAmount = ($item->net_unit_price * $item->quantity);
                                $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($LineExtensionAmount, NUMBER_DECIMALS));
                                    $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                                $xml_cac_DebitNoteLine->appendChild($xml_cbc_LineExtensionAmount);

                                if ($item->code_fe == BOLSAS) {
                                    $xml_cac_PricingReference = $xml->createElement("cac:PricingReference");
                                        $xml_cac_AlternativeConditionPrice = $xml->createElement("cac:AlternativeConditionPrice");
                                            $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS));
                                                $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_AlternativeConditionPrice->appendChild($xml_cbc_PriceAmount);

                                            $xml_cac_AlternativeConditionPrice->appendChild($xml->createElement("cbc:PriceTypeCode", "03"));
                                        $xml_cac_PricingReference->appendChild($xml_cac_AlternativeConditionPrice);
                                    $xml_cac_DebitNoteLine->appendChild($xml_cac_PricingReference);
                                }

                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    if ($item->code_fe == BOLSAS) {
                                        $TaxAmount = $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS));
                                    } else {
                                        $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                                    }

                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = ($item->net_unit_price * $item->quantity);
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                        if ($item->code_fe == BOLSAS) {
                                            $TaxAmount = $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS));
                                        } else {
                                            $TaxAmount = $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                                        }

                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        if ($item->code_fe == BOLSAS) {
                                            $TaxableAmount = $item->quantity;
                                            $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                            $PerUnitAmount = $item->rate;
                                            $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                                $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);
                                        }

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($item->rate, NUMBER_DECIMALS)));
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_cac_DebitNoteLine->appendChild($xml_cac_TaxTotal);

                                if ($item->consumption_sales > 0) {
                                    $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                        $TaxAmount = $product_tax_ic;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                                        foreach ($tax_base_ic_array as $nominal => $tax_base_ic) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_ic;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_ic_array[$nominal];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $TaxableAmount = $tax_base_ic;
                                                $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($TaxableAmount, NUMBER_DECIMALS));
                                                    $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                                $PerUnitAmount = $nominal;
                                                $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, NUMBER_DECIMALS));
                                                    $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IC));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IC"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    $xml_cac_DebitNoteLine->appendChild($xml_cac_TaxTotal);
                                }

                                $xml_cac_Item = $xml->createElement("cac:Item");
                                    $xml_cac_Item->appendChild($xml->createElement("cbc:Description", htmlspecialchars($item->product_name)));

                                    $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                                        $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                                            $xml_cbc_ID->setAttribute("schemeID", "999");
                                        $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                                    $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                                $xml_cac_DebitNoteLine->appendChild($xml_cac_Item);

                                $xml_cac_Price = $xml->createElement("cac:Price");
                                    $PriceAmount = $item->net_unit_price;
                                    $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($PriceAmount, NUMBER_DECIMALS));
                                        $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_Price->appendChild($xml_cbc_PriceAmount);

                                    $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS));
                                        $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                                    $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                                $xml_cac_DebitNoteLine->appendChild($xml_cac_Price);
                            $xml_debit_note->appendChild($xml_cac_DebitNoteLine);

                            $consecutive_product_identifier++;
                        }

                    }
                /**************************************************************/

                /************************* DATA *******************************/
                    if ($this->Settings->fe_technology_provider == CADENA) {
                        $xml_DATA = $xml->createElement("DATA");
                            $xml_DATA->appendChild($xml->createElement("UBL21", "true"));

                            $xml_Partnership = $xml->createElement("Partnership");
                                $xml_Partnership->appendChild($xml->createElement("ID", "901090070"));
                                $xml_Partnership->appendChild($xml->createElement("TechKey", $resolution_data_referenced_invoice->clave_tecnica));
                                if ($this->Settings->fe_work_environment == TEST) {
                                    $xml_Partnership->appendChild($xml->createElement("SetTestID", $resolution_data_referenced_invoice->fe_testid));
                                }
                            $xml_DATA->appendChild($xml_Partnership);
                        $xml_debit_note->appendChild($xml_DATA);
                    }
                /**************************************************************/
            $xml->appendChild($xml_debit_note);

            if (!$print_xml) { return base64_encode($xml->saveXML()); }

            echo $xml->saveXML();
        }

        private function generate_CUFE($sale, $items_sale, $issuer_data, $customer_data, $resolution_data, $decrypt = FALSE)
        {
            $sma = new Sma();

            $otherTax = 0;
            $ICvalue = 0.00;
            $IVA_value = 0.00;
            $INC_value = 0.00;
            $ICA_value = 0.00;
            $ICUI_value = 0.00;
            $IBUA_value = 0.00;
            $BOLSA_base = 0.00;
            $BOLSA_value = 0.00;
            $totalAmountOfCharges = $sale->shipping;
            $product_tax_iva = $subtotal = $total_tax = $ivaUtilidad = 0.00;
            $orderDiscount = $sma->formatDecimal($sale->order_discount, NUMBER_DECIMALS);

            if (!empty($sale->aiu_management) && !empty($sale->aiu_amounts_affects_grand_total)) {
                if (($sale->aiu_admin_total > 0 || $sale->aiu_imprev_total > 0 || $sale->aiu_utilidad_total > 0)) {
                    $totalAmountOfCharges += $sale->aiu_admin_total + $sale->aiu_imprev_total + $sale->aiu_utilidad_total;
                }
            }

            foreach ($items_sale as $item) {
                $netUnitPrice = $sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                $quantity = $sma->formatDecimal($item->quantity, NUMBER_DECIMALS);


                $subtotal += $sma->formatDecimal($sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS) * $sma->formatDecimal($item->quantity, NUMBER_DECIMALS), NUMBER_DECIMALS);
                $total_tax += $sma->formatDecimal(($sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS) * $sma->formatDecimal($item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                if ($item->code_fe == "01") {
                    $IVA_value += $sma->formatDecimal((($netUnitPrice * $quantity) * $item->rate) / 100, NUMBER_DECIMALS);
                } else if ($item->code_fe == "04") {
                    $INC_value += $sma->formatDecimal(($sma->formatDecimal(($netUnitPrice * $quantity), NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                    $otherTax += $sma->formatDecimal(($sma->formatDecimal(($netUnitPrice * $quantity), NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                } else if ($item->code_fe == "03") {
                    $ICA_value += $sma->formatDecimal((($netUnitPrice * $quantity) * $item->rate) / 100, NUMBER_DECIMALS);
                    $otherTax += $sma->formatDecimal((($netUnitPrice * $quantity) * $item->rate) / 100, NUMBER_DECIMALS);
                } else if ($item->code_fe == "22") {
                    $BOLSA_base += $quantity;
                    $BOLSA_value += $sma->formatDecimal($sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS), NUMBER_DECIMALS);
                    $otherTax += $sma->formatDecimal($sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS), NUMBER_DECIMALS);
                }

                if ($item->codeTax2 == ICUI) {
                    $taxRate = str_replace('%', '', $item->tax_2);
                    $rate = $this->sma->formatDecimal(($taxRate / 100), NUMBER_DECIMALS);

                    $ICUI_value += $this->sma->formatDecimal(($netUnitPrice * $quantity) * $rate, NUMBER_DECIMALS);
                } else if ($item->codeTax2 == IBUA) {
                    $mililiters = $item->mililiters / 100;
                    $nominal = $item->nominal;

                    $IBUA_value += $this->sma->formatDecimal(($mililiters * $nominal), NUMBER_DECIMALS);
                }

                if ($item->consumption_sales > 0) {
                    if ($item->nameTax2 == "IC") {
                        $ICvalue += $sma->formatDecimal($item->nominal * $item->quantity, NUMBER_DECIMALS);
                    }
                }

                if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                    if ($sale->order_tax_aiu_base_apply_to == 3) {
                        $utilidad = (($sma->formatDecimal(($netUnitPrice * $quantity), NUMBER_DECIMALS)) * $sale->aiu_utilidad_percentage) / 100;
                        $taxRateAIU = $this->site->getTaxRateByID($sale->order_tax_aiu_id);
                        $ivaUtilidad += $sma->formatDecimal((($utilidad * $taxRateAIU->rate) / 100), NUMBER_DECIMALS);
                    }
                }
            }

            if (!empty($sale->aiu_management) && $sale->order_tax_aiu_total > 0) {
                $IVA_value += $sma->formatDecimal($ivaUtilidad, NUMBER_DECIMALS);
                $total_tax += $ivaUtilidad;
            }

            $SUBTOTAL = $sma->formatDecimal($subtotal, NUMBER_DECIMALS);
            $IVA = $sma->formatDecimal($IVA_value, NUMBER_DECIMALS);
            $INC = $sma->formatDecimal($INC_value, NUMBER_DECIMALS);
            $ICA = $sma->formatDecimal($ICA_value, NUMBER_DECIMALS);
            $TOTAL = $sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $sma->formatDecimal($total_tax, NUMBER_DECIMALS) + $ICvalue + $ICUI_value + $IBUA_value + $BOLSA_value + $totalAmountOfCharges - $orderDiscount /*+ $BOLSA_base*/;
            $Work_environment = ($this->Settings->fe_work_environment == PRODUCTION) ? PRODUCTION : TEST;
            $Pin = $resolution_data->clave_tecnica;

            if ($decrypt == FALSE) {
                $CUFE_code_generated = hash('sha384',
                    str_replace('-', '', $sale->reference_no).
                    $this->format_date($sale->date)->date.
                    $this->format_date($sale->date)->time.
                    $sma->formatDecimalNoRound($SUBTOTAL, 2).
                    "01".
                    $sma->formatDecimalNoRound($IVA, 2).
                    "04".
                    $sma->formatDecimalNoRound($INC, 2).
                    "03".
                    $sma->formatDecimalNoRound($ICA, 2).
                    $sma->formatDecimalNoRound($TOTAL, 2).
                    str_replace("-", "", $issuer_data->numero_documento).
                    str_replace('-', '', $customer_data->vat_no).
                    $Pin.
                    $Work_environment
                );
            } else {
                $CUFE_code_generated =
                    str_replace('-', '', $sale->reference_no)." ".
                    $this->format_date($sale->date)->date." ".
                    $this->format_date($sale->date)->time." ".
                    $sma->formatDecimalNoRound($SUBTOTAL, 2)." ".
                    "01"." ".
                    $sma->formatDecimalNoRound($IVA, 2)." ".
                    "04"." ".
                    $sma->formatDecimalNoRound($INC, 2)." ".
                    "03"." ".
                    $sma->formatDecimalNoRound($ICA, 2)." ".
                    $sma->formatDecimalNoRound($TOTAL, 2)." ".
                    str_replace("-", "", $issuer_data->numero_documento)." ".
                    str_replace('-', '', $customer_data->vat_no)." ".
                    $Pin." ".
                    $Work_environment;
            }

            $string_CUFE = "NumFac: ".str_replace('-', '', $sale->reference_no)."\n".
                "FecFac: ". $this->format_date($sale->date)->date."\n".
                "HorFac: {$this->format_date($sale->date)->time} \n".
                "NitFac: ". str_replace("-", "", $issuer_data->numero_documento)."\n".
                "DocAdq: ". str_replace('-', '', $customer_data->vat_no)."\n".
                "ValFac: ". $sma->formatDecimalNoRound($SUBTOTAL, 2)."\n".
                "ValIva: ". $sma->formatDecimalNoRound($total_tax, 2)."\n".
                "ValOtroIm: ". $sma->formatDecimalNoRound($otherTax, 2)."\n".
                "ValTolFac: ". $sma->formatDecimalNoRound($TOTAL, 2)."\n".
                "CUFE: ". $CUFE_code_generated ."\n".
                "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey={$CUFE_code_generated}";

            if ($decrypt == FALSE) {
                if ($this->Settings->fe_technology_provider != DELCOP) {
                    $this->site->updateSale(["codigo_qr"=>$string_CUFE], $sale->id);
                }
            }

            return $CUFE_code_generated;
        }

        private function generate_CUDE($credit_note, $items_sale, $issuer_data, $customer_data, $resolution_data, $decrypt = FALSE)
        {
            $otherTax = 0;
            $IVA_value = 0.00;
            $INC_value = 0.00;
            $ICA_value = 0.00;
            $IC_value = 0.00;
            $BOLSA_value = 0.00;
            $product_tax_iva = $total_base_round = $subtotal = $total_tax = 0.00;

            foreach ($items_sale as $item) {
                $subtotal += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                $total_tax += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);

                if ($item->code_fe == "01") {
                    $IVA_value += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                } else if ($item->code_fe == "04") {
                    $INC_value += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                    $otherTax += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                } else if ($item->code_fe == "03") {
                    $ICA_value += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                    $otherTax += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
                } else if ($item->code_fe == "22") {
                    $BOLSA_value += $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS), NUMBER_DECIMALS);
                    $otherTax += $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS), NUMBER_DECIMALS);
                }

                if ($item->consumption_sales > 0) {
                    $IC_value += $this->sma->formatDecimal($item->consumption_sales * $item->quantity, NUMBER_DECIMALS);
                }
            }

            $SUBTOTAL = $this->sma->formatDecimal($subtotal, NUMBER_DECIMALS);
            $IVA = $this->sma->formatDecimal($IVA_value, NUMBER_DECIMALS);
            $INC = $this->sma->formatDecimal($INC_value, NUMBER_DECIMALS);
            $ICA = $this->sma->formatDecimal($ICA_value, NUMBER_DECIMALS);
            $shipping = $this->sma->formatDecimal($credit_note->shipping, NUMBER_DECIMALS);
            $order_discount = $this->sma->formatDecimal($credit_note->order_discount, NUMBER_DECIMALS);
            $Work_environment = ($this->Settings->fe_work_environment == PRODUCTION) ? PRODUCTION : TEST;

            $TOTAL = $this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($total_tax, NUMBER_DECIMALS) + $ICA_value + $BOLSA_value + $shipping - $order_discount;

            if ($decrypt == FALSE) {
                $CUDE_code_generated = hash('sha384',
                    str_replace('-', '', $credit_note->reference_no).
                    $this->format_date($credit_note->date)->date.
                    $this->format_date($credit_note->date)->time.
                    $this->sma->formatDecimalNoRound(abs($SUBTOTAL), 2).
                    "01".
                    $this->sma->formatDecimalNoRound(abs($IVA), 2).
                    "04".
                    $this->sma->formatDecimalNoRound(abs($INC), 2).
                    "03".
                    $this->sma->formatDecimalNoRound(abs($ICA), 2).
                    $this->sma->formatDecimalNoRound(abs($TOTAL), 2).
                    str_replace("-", "", $issuer_data->numero_documento).
                    str_replace('-', '', $customer_data->vat_no).
                    "75315".
                    $Work_environment
                );
            } else {
                $CUDE_code_generated =
                    str_replace('-', '', $credit_note->reference_no)." ".
                    $this->format_date($credit_note->date)->date." ".
                    $this->format_date($credit_note->date)->time." ".
                    $this->sma->formatDecimalNoRound(abs($SUBTOTAL), 2)." ".
                    "01"." ".
                    $this->sma->formatDecimalNoRound(abs($IVA), 2)." ".
                    "04"." ".
                    $this->sma->formatDecimalNoRound(abs($INC), 2)." ".
                    "03"." ".
                    $this->sma->formatDecimalNoRound(abs($ICA), 2)." ".
                    $this->sma->formatDecimalNoRound(abs($TOTAL), 2)." ".
                    str_replace("-", "", $issuer_data->numero_documento)." ".
                    str_replace('-', '', $customer_data->vat_no)." ".
                    "75315"." ".
                    $Work_environment;
            }

            $string_CUDE = "NumFac: ".str_replace('-', '', $credit_note->reference_no)."\n".
                "FecFac: ". $this->format_date($credit_note->date)->date."\n".
                "HorFac: {$this->format_date($credit_note->date)->time} \n".
                "NitFac: ". str_replace("-", "", $issuer_data->numero_documento)."\n".
                "DocAdq: ". str_replace('-', '', $customer_data->vat_no)."\n".
                "ValFac: ". $this->sma->formatDecimalNoRound(abs($SUBTOTAL), 2)."\n".
                "ValIva: ". ($this->sma->formatDecimalNoRound(abs($total_tax), 2))."\n".
                "ValOtroIm: ". $this->sma->formatDecimalNoRound($otherTax, 2)."\n".
                "ValTolFac: ". $this->sma->formatDecimalNoRound(abs($TOTAL), 2)."\n".
                "CUDE: ". $CUDE_code_generated."\n".
                "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey={$CUDE_code_generated}";

            if ($decrypt == FALSE) {
                if ($this->Settings->fe_technology_provider != DELCOP) {
                    $this->site->updateSale(["codigo_qr"=>$string_CUDE], $credit_note->id);
                }
            }

            return $CUDE_code_generated;
        }

        public function cadenaWSSendDocument($xml_file, $invoice_data)
        {
            $sale = $this->site->getSaleByID($invoice_data->sale_id);
            $feTechnologyProvider = ($sale->pos == YES) ? $this->pos_settings->technology_provider_id : $this->Settings->fe_technology_provider;
            $workEnvironment = ($sale->pos == YES) ? $this->pos_settings->work_environment : $this->Settings->fe_work_environment;
            $technologyProviderConfiguration = $this->site->getTechnologyProviderConfiguration($feTechnologyProvider, $workEnvironment);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $technologyProviderConfiguration->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "\"".$xml_file."\"",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: text/plain",
                    "efacturaAuthorizationToken: ". $technologyProviderConfiguration->security_token
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            log_message('debug', 'CADENA FE-Envio documento: '. $response);

            curl_close($curl);

            if ($err) {
                $response = [
                    'response' => FALSE,
                    'message' => "Información Técnica: No es posible establecer cominicación con el servicio. Esto puede deberse a una intermitencia. Por favor reintente nuevamente en unos minutos."
                ];

                $sale_data['fe_aceptado'] = 1;
                $sale_data['fe_mensaje'] = "La respuesta del servicio está tardando más de lo esperado.";
                $sale_data["fe_mensaje_soporte_tecnico"] = "Información Técnica: No es posible establecer cominicación con el servicio. Esto puede deberse a una intermitencia. Por favor reintente nuevamente en unos minutos.";
                $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data["fe_mensaje_soporte_tecnico"], ERROR, $sale->date);

                $this->site->updateSale($sale_data, $invoice_data->sale_id);
            } else {
                $response = json_decode($response);
                $message_technical_support = "";

                if (!empty($response)) {
                    if (isset($response->statusCode)) {
                        if ($response->statusCode == '200') {
                            $sale_data['fe_aceptado'] = 2;
                            $sale_data['cufe'] = $response->uuid;
                            $sale_data["fe_xml"] = $response->document;
                            $sale_data['fe_mensaje'] = "Documento aceptado por la DIAN";
                            $sale_data["fe_mensaje_soporte_tecnico"] = $response->statusMessage .". Código de estado: ". $response->statusCode .". ";

                            $response = [
                                'response' => TRUE,
                                'message' => "Documento aceptado por la DIAN"
                            ];

                            $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data['fe_mensaje'], SUCCESS, $sale->date);

                            $updated_sale = $this->site->updateSale($sale_data, $invoice_data->sale_id);
                        } else if ($response->statusCode == '400') {
                            $sale_data['fe_aceptado'] = 1;
                            $sale_data['fe_mensaje'] = $response->errorMessage;
                            $sale_data["fe_mensaje_soporte_tecnico"] = $response->errorReason. ". Código de estado: ". $response->statusCode .".";

                            $response = [
                                'response' => FALSE,
                                'message' => $response->errorMessage
                            ];

                            $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data['fe_mensaje'], ERROR, $sale->date);

                            $updated_sale = $this->site->updateSale($sale_data, $invoice_data->sale_id);
                        } else if ($response->statusCode == '401') {
                            $sale_data['fe_aceptado'] = 1;
                            $sale_data['fe_mensaje'] = "No autorizado";
                            $sale_data["fe_mensaje_soporte_tecnico"] = "El api key no está autorizado para enviar el documento con el nit contenido en este, o el encabezado no cumple con los requerimientos del endpoint. Código de estado: ". $response->statusCode;

                            $response = [
                                'response' => FALSE,
                                'message' => "No autorizado"
                            ];

                            $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data['fe_mensaje_soporte_tecnico'], ERROR, $sale->date);

                            $updated_sale = $this->site->updateSale($sale_data, $invoice_data->sale_id);
                        } else if ($response->statusCode == '409') {
                            if (isset($response->errorReason) && !empty($response->errorReason)) {
                                if (is_object($response->errorReason) || is_array($response->errorReason)) {
                                    foreach ($response->errorReason as $errorReasonKey => $errorReason) {
                                        $message_technical_support .= $errorReason ." ";
                                    }
                                } else {
                                    $message_technical_support .= $response->errorReason ." ";
                                }
                            }

                            $sale_data['fe_aceptado'] = 1;
                            $sale_data['fe_mensaje'] = $response->errorMessage;
                            $sale_data["fe_mensaje_soporte_tecnico"] = $message_technical_support;

                            if (strpos($message_technical_support, 'procesado anteriormente') === FALSE) {
                                $response = [
                                    'response' => FALSE,
                                    'message' => $response->errorMessage
                                ];

                                $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data["fe_mensaje_soporte_tecnico"], ERROR, $sale->date);

                                $updated_sale = $this->site->updateSale($sale_data, $invoice_data->sale_id);
                            } else {
                                $sale_data = [
                                    "fe_aceptado" => 3,
                                    "fe_mensaje" => $response->errorMessage,
                                    "fe_mensaje_soporte_tecnico" => $message_technical_support
                                ];
                                $this->site->updateSale($sale_data, $invoice_data->sale_id);
                                $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data["fe_mensaje_soporte_tecnico"], ERROR, $sale->date);
                                // $status_change = $this->change_status_accepted_cadena($invoice_data->sale_id);
                                $response = [
                                    'response' => FALSE,
                                    'message' => $response->errorMessage
                                ];
                            }
                        } else if ($response->statusCode == '502') {
                            // "Se confirmó proceso de contingencia DIAN. Abstenerse de enviar notas crédito o débito durante la contingencia
                            $sale_data['fe_aceptado'] = 1;
                            $sale_data['fe_mensaje'] = $response->errorMessage;
                            $sale_data["fe_mensaje_soporte_tecnico"] = $message_technical_support;

                            $response = [
                                'response' => FALSE,
                                'message' => $response->errorMessage
                            ];

                            $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data["fe_mensaje_soporte_tecnico"], ERROR, $sale->date);

                            $updated_sale = $this->site->updateSale($sale_data, $invoice_data->sale_id);
                        } else if ($response->statusCode == '503') {
                            // Posiblemente se declare contingencia DIAN. Si el documento es nota crédito o débito, intentar enviar más tarde.
                            $sale_data['fe_aceptado'] = 1;
                            $sale_data['fe_mensaje'] = $response->errorMessage;
                            $sale_data["fe_mensaje_soporte_tecnico"] = $message_technical_support;

                            $response = [
                                'response' => FALSE,
                                'message' => $response->errorMessage
                            ];

                            $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data["fe_mensaje_soporte_tecnico"], ERROR, $sale->date);

                            $updated_sale = $this->site->updateSale($sale_data, $invoice_data->sale_id);
                        } else if ($response->statusCode == '504') {
                            // El documento tardó más de lo esperado el consumo de la DIAN
                            $sale_data['fe_aceptado'] = 1;
                            $sale_data['fe_mensaje'] = $response->errorMessage;
                            $sale_data["fe_mensaje_soporte_tecnico"] = $message_technical_support;

                            $response = [
                                'response' => FALSE,
                                'message' => $response->errorMessage
                            ];

                            $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data["fe_mensaje_soporte_tecnico"], ERROR, $sale->date);

                            $updated_sale = $this->site->updateSale($sale_data, $invoice_data->sale_id);
                        }
                    } else  {
                        $response = [
                            'response' => FALSE,
                            'message' => "Se ha perdido la conexión con el Web Services."
                        ];
                    }
                } else {
                    $response = [
                        'response' => FALSE,
                        'message' => "No es posible realizar conexión con Web Services."
                    ];
                }
            }

            return (object) $response;
        }
    /****************************************************************/

    /**************************** DELCOP ****************************/
        public function delcopBuildXmlInvoice($invoice_data, $print_xml = FALSE)
        {
            $issuer_data = $this->site->get_setting();
            $sale = $this->site->getSaleByID($invoice_data->sale_id);
            $items_sale = $this->site->getAllSaleItems($invoice_data->sale_id);
            $biller_data = $this->site->getCompanyByID($invoice_data->biller_id);
            $customer_data = $this->site->getCompanyByID($invoice_data->customer_id);
            $payments_data = $this->site->getSalePayments($invoice_data->sale_id);
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $advance_payments = $this->get_advance_payments_sale($invoice_data->sale_id);
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER);
            $CUFE_code = $this->generate_CUFE_DELCOP($sale, $items_sale, $issuer_data, $customer_data, $resolution_data, $print_xml);

            if ($print_xml) { header("content-type: application/xml; charset=ISO-8859-15"); }

            $xml = new DOMDocument("1.0");
            $xml_Document = $xml->createElement("Document");
                $xml_Document->setAttribute("xmlns", "https://titanio.com.co/contratos/facturaelectronica/v2");
                $xml_Document->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                $xml_Document->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                $xml_Document->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
                $xml_Document->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                $xml_Document->setAttribute("xmlns:sac", "urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2");
                $xml_Document->setAttribute("xmlns:sbc", "urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2");
                $xml_Document->setAttribute("xmlns:sig", "urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2");
                $xml_Document->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
                $xml_Document->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
                $xml_Document->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
                $xml_Document->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                $xml_Document->setAttribute("xsi:schemaLocation", "https://titanio.com.co/contratos/facturaelectronica/v2 XSD/UBL_DIAN_2.1.xsd");

                $xml_invoice = $xml->createElement("Invoice");
                    /***************** Resolución de numeración *******************/
                        $xml_ext_UBLExtensions = $xml->createElement("ext:UBLExtensions");
                            $xml_ext_UBLExtension = $xml->createElement("ext:UBLExtension");
                                $xml_ext_ExtensionContent = $xml->createElement("ext:ExtensionContent");
                                    $xml_sts_DianExtensions = $xml->createElement("sts:DianExtensions");
                                        $xml_sts_InvoiceControl = $xml->createElement("sts:InvoiceControl");
                                            $xml_sts_InvoiceControl->appendChild($xml->createElement("sts:InvoiceAuthorization", $resolution_data->num_resolucion));
                                            $xml_sts_AuthorizationPeriod = $xml->createElement("sts:AuthorizationPeriod");
                                                $xml_sts_AuthorizationPeriod->appendChild($xml->createElement("cbc:StartDate", $resolution_data->emision_resolucion));
                                                $xml_sts_AuthorizationPeriod->appendChild($xml->createElement("cbc:EndDate", $resolution_data->vencimiento_resolucion));
                                            $xml_sts_InvoiceControl->appendChild($xml_sts_AuthorizationPeriod);
                                            $xml_sts_AuthorizedInvoices = $xml->createElement("sts:AuthorizedInvoices");
                                                $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:Prefix', $resolution_data->sales_prefix));
                                                $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:From', $resolution_data->inicio_resolucion));
                                                $xml_sts_AuthorizedInvoices->appendChild($xml->createElement('sts:To', $resolution_data->fin_resolucion));
                                                $xml_sts_InvoiceControl->appendChild($xml_sts_AuthorizedInvoices);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_InvoiceControl);
                                        $xml_sts_InvoiceSource = $xml->createElement("sts:InvoiceSource");
                                            $xml_cbc_IdentificationCode = $xml->createElement("cbc:IdentificationCode", "CO");
                                            $xml_cbc_IdentificationCode->setAttribute("listAgencyID", "6");
                                            $xml_cbc_IdentificationCode->setAttribute("listAgencyName", "United Nations Economic Commission for Europe");
                                            $xml_cbc_IdentificationCode->setAttribute("listSchemeURI", "urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1");
                                            $xml_sts_InvoiceSource->appendChild($xml_cbc_IdentificationCode);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_InvoiceSource);
                                        $xml_sts_SoftwareProvider = $xml->createElement("sts:SoftwareProvider");
                                            $xml_sts_ProviderID = $xml->createElement("sts:ProviderID", "860028581");
                                                $xml_sts_ProviderID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_ProviderID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                                $xml_sts_ProviderID->setAttribute("schemeID", "1");
                                                $xml_sts_ProviderID->setAttribute("schemeName", "31");
                                            $xml_sts_SoftwareProvider->appendChild($xml_sts_ProviderID);
                                            $xml_sts_SoftwareID = $xml->createElement("sts:SoftwareID", "04441877-5591-4df8-88b1-b880943551");
                                                $xml_sts_SoftwareID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_SoftwareID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                            $xml_sts_SoftwareProvider->appendChild($xml_sts_SoftwareID);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_SoftwareProvider);
                                        $xml_sts_SoftwareSecurityCode = $xml->createElement("sts:SoftwareSecurityCode");
                                            $xml_sts_SoftwareSecurityCode->setAttribute("schemeAgencyID", "195");
                                            $xml_sts_SoftwareSecurityCode->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                        $xml_sts_DianExtensions->appendChild($xml_sts_SoftwareSecurityCode);
                                        $xml_sts_AuthorizationProvider = $xml->createElement("sts:AuthorizationProvider");
                                            $xml_sts_AuthorizationProviderID = $xml->createElement("sts:AuthorizationProviderID", "800197268");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeID", "4");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeName", "31");
                                            $xml_sts_AuthorizationProvider->appendChild($xml_sts_AuthorizationProviderID);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_AuthorizationProvider);
                                        $xml_sts_QRCode = $xml->createElement("sts:QRCode", "");
                                        $xml_sts_DianExtensions->appendChild($xml_sts_QRCode);
                                    $xml_ext_ExtensionContent->appendChild($xml_sts_DianExtensions);
                                $xml_ext_UBLExtension->appendChild($xml_ext_ExtensionContent);
                            $xml_ext_UBLExtensions->appendChild($xml_ext_UBLExtension);
                        $xml_invoice->appendChild($xml_ext_UBLExtensions);
                    /**************************************************************/

                    /************************* Encabezado *************************/
                        $xml_invoice->appendChild($xml->createElement("cbc:UBLVersionID", "UBL 2.1"));
                        $xml_invoice->appendChild($xml->createElement("cbc:CustomizationID", "10"));
                        $xml_invoice->appendChild($xml->createElement("cbc:ProfileID", "DIAN 2.1: Factura Electrónica de Venta"));

                        $ProfileExecutionID = ($resolution_data->fe_work_environment == TEST) ? TEST : PRODUCTION;
                        $xml_invoice->appendChild($xml->createElement("cbc:ProfileExecutionID", $ProfileExecutionID));

                        $xml_invoice->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $sale->reference_no)));

                        $xml_cbc_UUID = $xml->createElement("cbc:UUID", $CUFE_code);
                            $xml_cbc_UUID->setAttribute("schemeID", $ProfileExecutionID);
                            if ($resolution_data->factura_contingencia == YES) {
                                $xml_cbc_UUID->setAttribute("schemeName", "CUDE-SHA384");
                            } else {
                                $xml_cbc_UUID->setAttribute("schemeName", "CUFE-SHA384");
                            }
                        $xml_invoice->appendChild($xml_cbc_UUID);

                        $xml_invoice->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($sale->date)->date));
                        $xml_invoice->appendChild($xml->createElement("cbc:IssueTime", $this->format_date($sale->date)->time));

                        if ($resolution_data->factura_contingencia == YES) {
                            $xml_invoice->appendChild($xml->createElement("cbc:InvoiceTypeCode", "03"));
                        } else {
                            $xml_invoice->appendChild($xml->createElement("cbc:InvoiceTypeCode", "01"));
                        }

                        $xml_invoice->appendChild($xml->createElement("cbc:DocumentCurrencyCode", "COP"));
                        $xml_invoice->appendChild($xml->createElement("cbc:LineCountNumeric", count($items_sale)));
                        $xml_invoice->appendChild($xml->createElement("cbc:Note", htmlspecialchars(str_replace(["&lt;p&gt;", "&lt;&sol;p&gt;", "&nbsp", "&comma;"], "", $sale->note))));
                    /**************************************************************/

                    /*************************** Emisor ***************************/
                        $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                            $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $issuer_data->tipo_persona));
                            $xml_cac_Party = $xml->createElement("cac:Party");
                                $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                    $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->nombre_comercial)));
                                $xml_cac_Party->appendChild($xml_cac_PartyName);

                                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                    $xml_cac_Address = $xml->createElement("cac:Address");
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($biller_data->city))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($biller_data->DEPARTAMENTO))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                        $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($biller_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_Address->appendChild($xml_cac_Country);
                                    $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                    $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                        $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                    $type_issuers_obligations = '';
                                    foreach ($issuer_obligations as $issuer_obligation) {
                                        $type_issuers_obligations .= $issuer_obligation->types_obligations_id .';';
                                    }

                                    if (empty($type_issuers_obligations)) {
                                        $type_issuers_obligations = "R-99-PN";
                                    }

                                    $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_issuers_obligations, ";"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                    $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($biller_data->city))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($biller_data->DEPARTAMENTO))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($biller_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                                $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                    $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                        $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);

                                    $xml_cac_CorporateRegistrationScheme = $xml->createElement("cac:CorporateRegistrationScheme");
                                        $xml_cac_CorporateRegistrationScheme->appendChild($xml->createElement("cbc:ID", $resolution_data->sales_prefix));
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cac_CorporateRegistrationScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);
                                $xml_cac_Contact = $xml->createElement("cac:Contact");
                                    $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $issuer_data->default_email));
                                $xml_cac_Party->appendChild($xml_cac_Contact);
                            $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                        $xml_invoice->appendChild($xml_cac_AccountingSupplierParty);
                    /**************************************************************/

                    /************************ Adquiriente. ************************/
                        $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                            $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer_data->type_person));
                            $xml_cac_Party = $xml->createElement("cac:Party");
                                if ($customer_data->type_person == NATURAL_PERSON) {
                                    $xml_cac_PartyIdentification = $xml->createElement("cac:PartyIdentification");
                                        $xml_cbc_ID = $xml->createElement("cbc:ID", $this->clear_document_number($customer_data->vat_no));
                                            if ($customer_data->document_code == NIT) {
                                                $xml_cbc_ID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                            }
                                            $xml_cbc_ID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cac_PartyIdentification->appendChild($xml_cbc_ID);
                                    $xml_cac_Party->appendChild($xml_cac_PartyIdentification);
                                }

                                $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                    $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                                $xml_cac_Party->appendChild($xml_cac_PartyName);

                                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                    $xml_cac_Address = $xml->createElement("cac:Address");
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($customer_data->city))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($customer_data->state))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                        $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($customer_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_Address->appendChild($xml_cac_Country);
                                    $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                    $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clear_document_number($customer_data->vat_no));
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }

                                        $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                    $type_customer_obligations = '';
                                    foreach ($customer_obligations as $customer_obligation) {
                                        $type_customer_obligations .= $customer_obligation->types_obligations_id .';';
                                    }

                                    if (empty($type_customer_obligations)) {
                                        $type_customer_obligations = "R-99-PN";
                                    }

                                    $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_customer_obligations, ";"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                    $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($customer_data->city))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($customer_data->state))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));

                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);

                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($customer_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                                $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                    $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));

                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $customer_data->vat_no);
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }

                                        $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);
                                $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                                if ($this->Settings->fe_work_environment == TEST) {
                                    $xml_cac_Contact = $xml->createElement("cac:Contact");
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", "Wappsi Innovation"));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Telephone", "301 794 6302"));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", "wappsi.desarrolloweb02@gmail.com"));
                                    $xml_cac_Party->appendChild($xml_cac_Contact);
                                } else {
                                    $xml_cac_Contact = $xml->createElement("cac:Contact");
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Telephone", $customer_data->phone));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $customer_data->email));
                                    $xml_cac_Party->appendChild($xml_cac_Contact);
                                }
                            $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                        $xml_invoice->appendChild($xml_cac_AccountingCustomerParty);
                    /**************************************************************/

                    /************************ Medios pago *************************/
                        $payment_amount = 0;
                        if (!empty($payments_data)) {
                            foreach ($payments_data as $payment) {
                                $payment_amount += $payment->amount;
                            }
                        }

                        if ($sale->grand_total == $payment_amount) {
                            $method = CASH;
                        } else {
                            $method = CREDIT;
                        }

                        if (!empty($payments_data)) {
                            foreach ($payments_data as $payment) {
                                if ($payment->paid_by != 'retencion') {
                                    $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $payment->mean_payment_code_fe));

                                        if ($method == CREDIT) {
                                            $fecha_creacion = date("Y-m-d", strtotime($sale->date));
                                            if (empty($sale->payment_term)) {
                                                $payment_term = 0;
                                            } else {
                                                $payment_term = $sale->payment_term;
                                            }

                                            $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                            if ($payment_term > 0) {
                                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                            }
                                        }
                                    $xml_invoice->appendChild($xml_cac_PaymentMeans);
                                } else {
                                    $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                                        if ($method == CREDIT) {
                                            $fecha_creacion = date("Y-m-d", strtotime($sale->date));
                                            if (empty($sale->payment_term)) {
                                                $payment_term = 0;
                                            } else {
                                                $payment_term = $sale->payment_term;
                                            }

                                            $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                            if ($payment_term > 0) {
                                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                            }
                                        }
                                    $xml_invoice->appendChild($xml_cac_PaymentMeans);
                                }
                            }
                        } else {
                            $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                            if ($method == CREDIT) {
                                $fecha_creacion = date("Y-m-d", strtotime($sale->date));
                                if (empty($sale->payment_term)) {
                                    $payment_term = 0;
                                } else {
                                    $payment_term = $sale->payment_term;
                                }

                                $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                if ($payment_term > 0) {
                                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                }
                            }

                            $xml_invoice->appendChild($xml_cac_PaymentMeans);
                        }
                    /**************************************************************/

                    /********************** Término de pago ***********************/
                        $xml_cac_PaymentTerms = $xml->createElement("cac:PaymentTerms");
                            $TaxAmount = 0;
                            $PayableAmount =  $sale->grand_total;

                            if ($sale->rete_fuente_total > 0) { $TaxAmount += $sale->rete_fuente_total; }
                            if ($sale->rete_iva_total > 0) { $TaxAmount += $sale->rete_iva_total; }
                            if ($sale->rete_ica_total > 0) { $TaxAmount += $sale->rete_ica_total; }

                            $xml_cac_PaymentTerms->appendChild($xml->createElement("cbc:Amount", ($PayableAmount - $TaxAmount)));
                        $xml_invoice->appendChild($xml_cac_PaymentTerms);
                    /**************************************************************/

                    /************************* TRM **************************/
                        if ($sale->sale_currency != "COP") {
                            $xml_PaymentExchangeRate = $xml->createElement("cac:PaymentExchangeRate");
                                $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyCode", "COP"));
                                $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:SourceCurrencyBaseRate", $sale->sale_currency_trm));
                                $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyCode", "COP"));
                                $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:TargetCurrencyBaseRate", "1.00"));
                                $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:CalculationRate", $sale->sale_currency_trm));
                                $xml_PaymentExchangeRate->appendChild($xml->createElement("cbc:Date", $this->format_date($sale->date)->date));
                            $xml_invoice->appendChild($xml_PaymentExchangeRate);
                        }
                    /**************************************************************/

                    /******************** Descuentos y cargos *********************/
                        $charge_or_discount = 1;

                        if ($sale->order_discount > 0) {
                            $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "false"));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReasonCode", "11"));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReason", "Otro descuento"));

                                if (strpos($sale->order_discount_id, "%") !== FALSE) {
                                    $order_discount_percentage = str_replace("%", "", $sale->order_discount_id);
                                } else {
                                    $order_discount_percentage = ($sale->order_discount * 100) / $sale->total;
                                }

                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($order_discount_percentage, 4)));

                                $Amount = $sale->order_discount;
                                $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, 4));
                                    $xml_cac_Amount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                                $BaseAmount = $sale->total;
                                $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, 4));
                                    $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);
                            $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                        }

                        if ($sale->shipping > 0) {
                            $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                                $order_charge_percentage = ($sale->shipping * 100) / $sale->total;
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($order_charge_percentage, 4)));

                                $Amount = $sale->shipping;
                                $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, 4));
                                    $xml_cac_Amount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                                $BaseAmount = $sale->total;
                                $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, 4));
                                    $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                            $xml_invoice->appendChild($xml_cac_AllowanceCharge);
                        }
                    /**************************************************************/

                    /********************** Importes totales. *********************/
                        /************************ Impuestos. **********************/
                            $product_tax_iva = $product_tax_ic = $product_tax_inc = $product_tax_bolsas = $subtotal = $total_tax = $total_tax_base = $exempt_existing = $shipping = $order_discount = $tax_base_bolsas = 0;

                            $tax_rates = $this->get_tax_rate_by_code_fe();

                            foreach ($tax_rates as $tax_rate) {
                                if ($tax_rate->code_fe == IVA) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_value_iva_array[$index] = 0;
                                    $tax_base_iva_array[$index] = 0;
                                } else if ($tax_rate->code_fe == INC) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_value_inc_array[$index] = 0;
                                    $tax_base_inc_array[$index] = 0;
                                } else if ($tax_rate->code_fe == BOLSAS) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_base_bolsas_array[$index] = 0;
                                    $tax_value_bolsas_array[$index] = 0;
                                }
                            }

                            foreach ($items_sale as $item) {
                                if ($item->consumption_sales > 0) {
                                    $tax_base_ic_array[$item->consumption_sales] = 0;
                                    $tax_value_ic_array[$item->consumption_sales] = 0;
                                }
                            }

                            foreach ($items_sale as $item) {
                                $net_unit_price = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                                $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

                                $subtotal += $this->sma->formatDecimal($net_unit_price * $quantity, NUMBER_DECIMALS);
                                $total_tax += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                                if ($item->code_fe == IVA) {
                                    if ($item->rate == 0) { $exempt_existing++; }

                                    $product_tax_iva += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                                    foreach ($tax_rates as $tax_rate) {
                                        if ($item->rate == $tax_rate->rate) {
                                            $index = (int) $tax_rate->rate;
                                            $tax_base_iva_array[$index] += $this->sma->formatDecimal($net_unit_price * $quantity, NUMBER_DECIMALS);
                                            $tax_value_iva_array[$index] += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                                            break;
                                        }
                                    }
                                } else if ($item->code_fe == INC) {
                                    $product_tax_inc += $item->item_tax;

                                    foreach ($tax_rates as $tax_rate) {
                                        if ($item->rate == $tax_rate->rate) {
                                            $index = (int) $tax_rate->rate;
                                            $tax_base_inc_array[$index] += $item->net_unit_price * $item->quantity;
                                            $tax_value_inc_array[$index] += $item->item_tax;
                                            $tax_base_bolsas += $item->quantity;
                                            $total_tax_base += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, 2);

                                            break;
                                        }
                                    }
                                } else if ($item->code_fe == BOLSAS) {
                                    $index = (int) $item->rate;
                                    $product_tax_bolsas += $item->quantity * $item->rate;
                                    $tax_base_bolsas_array[$index] += $item->quantity;
                                    $tax_value_bolsas_array[$index] += $item->quantity * $item->rate;
                                }

                                if ($item->consumption_sales > 0) {
                                    $product_tax_ic += $item->consumption_sales * $item->quantity;
                                    $tax_base_ic_array[$item->consumption_sales] = $item->quantity;
                                    $tax_value_ic_array[$item->consumption_sales] = $item->consumption_sales * $item->quantity;
                                }
                            }

                            if ($product_tax_iva) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_iva;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                        if (!empty($tax_base_iva)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_iva;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_iva_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_invoice->appendChild($xml_cac_TaxTotal);
                            } else if (count($items_sale) >= 1 && $exempt_existing > 0) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_iva;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                        if (!empty($tax_base_iva)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_iva;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_iva_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_invoice->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_ic) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_ic;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_ic_array as $nominal => $tax_base_ic) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $tax_base_ic;
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_ic_array[$nominal];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $BaseUnitMeasure = $tax_base_ic;
                                            $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($BaseUnitMeasure, 2));
                                                $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                            $PerUnitAmount = $nominal;
                                            $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, 2));
                                                $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IC));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IC"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }

                                $xml_invoice->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_inc) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_inc;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_inc_array as $percentage => $tax_base_inc) {
                                        if (!empty($tax_base_inc)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_inc;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_inc_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", INC));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_invoice->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_bolsas) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_bolsas;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);
                                    foreach ($tax_base_bolsas_array as $nominal => $tax_base_bolsas) {
                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            $TaxableAmount = $tax_base_bolsas;
                                            $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                                $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                            $TaxAmount = $tax_value_bolsas_array[$nominal];
                                            $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                                $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                            $BaseUnitMeasure = $tax_base_bolsas;
                                            $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($BaseUnitMeasure, 2));
                                                $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                            $PerUnitAmount = $nominal;
                                            $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, 2));
                                                $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", BOLSAS));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC Bolsas"));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    }
                                $xml_invoice->appendChild($xml_cac_TaxTotal);
                            }
                        /***********************************************************/

                        /*********************** Retenciones. **********************/
                            if ($sale->rete_iva_total > 0) {
                                $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                    $TaxAmount = $sale->rete_iva_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $sale->rete_iva_base;
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                        $TaxAmount = $sale->rete_iva_total;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($sale->rete_iva_percentage, 5)));

                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "05"));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteIVA"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_invoice->appendChild($xml_cac_WithholdingTaxTotal);
                            }

                            if ($sale->rete_fuente_total > 0) {
                                $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                    $TaxAmount = $sale->rete_fuente_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $sale->rete_fuente_base;
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);


                                        $TaxAmount = $sale->rete_fuente_total;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($sale->rete_fuente_percentage, 5)));

                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "06"));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteFuente"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_invoice->appendChild($xml_cac_WithholdingTaxTotal);
                            }

                            if ($sale->rete_ica_total > 0) {
                                $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                    $TaxAmount = $sale->rete_ica_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $sale->rete_ica_base;
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                        $TaxAmount = $sale->rete_ica_total;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($sale->rete_ica_percentage, 5)));

                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "07"));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteICA"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_invoice->appendChild($xml_cac_WithholdingTaxTotal);
                            }
                        /***********************************************************/
                    /**************************************************************/

                    /********************** Total impuestos ***********************/
                        $xml_cac_LegalMonetaryTotal = $xml->createElement("cac:LegalMonetaryTotal");
                            $LineExtensionAmount = $subtotal;
                            $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($LineExtensionAmount, 2));
                                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                            $TaxExclusiveAmount = ($subtotal + $tax_base_bolsas);
                            $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimal($TaxExclusiveAmount, 2));
                                $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                            $TaxInclusiveAmount = ($subtotal + $total_tax + $product_tax_bolsas);
                            $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimal($TaxInclusiveAmount, 2));
                                $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                            if ($sale->order_discount > 0) {
                                $order_discount = $this->sma->formatDecimal($sale->order_discount, 2);
                                $AllowanceTotalAmount = $order_discount;
                                $xml_cbc_AllowanceTotalAmount = $xml->createElement("cbc:AllowanceTotalAmount", $this->sma->formatDecimal($AllowanceTotalAmount, 2));
                                    $xml_cbc_AllowanceTotalAmount->setAttribute("currencyID", "COP");
                                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_AllowanceTotalAmount);
                            }

                            if ($sale->shipping > 0) {
                                $shipping = $this->sma->formatDecimal($sale->shipping, 2);
                                $ChargeTotalAmount = $shipping;
                                $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($ChargeTotalAmount, 2));
                                    $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", "COP");
                                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                            }

                            $PayableAmount = ($subtotal + $total_tax + $product_tax_bolsas + $shipping - $order_discount);
                            $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimal($PayableAmount, 2));
                                $xml_cbc_PayableAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_PayableAmount);

                        $xml_invoice->appendChild($xml_cac_LegalMonetaryTotal);
                    /**************************************************************/

                    /************************* Productos **************************/
                        $consecutive_product_identifier = 1;
                        foreach ($items_sale as $item) {
                            $net_unit_price = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                            $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                                $xml_cac_InvoiceLine = $xml->createElement("cac:InvoiceLine");
                                    $xml_cac_InvoiceLine->appendChild($xml->createElement("cbc:ID", $consecutive_product_identifier));
                                    $xml_cbc_InvoicedQuantity = $xml->createElement("cbc:InvoicedQuantity", $this->sma->formatDecimal($quantity, 2));
                                        $xml_cbc_InvoicedQuantity->setAttribute("unitCode", "NIU");
                                    $xml_cac_InvoiceLine->appendChild($xml_cbc_InvoicedQuantity);

                                    // if ($item->code_fe == BOLSAS) {
                                    //     $LineExtensionAmount = $item->quantity;
                                    // } else {
                                        $LineExtensionAmount = ($net_unit_price * $quantity);
                                    // }
                                    $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal($LineExtensionAmount, 2));
                                        $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_InvoiceLine->appendChild($xml_cbc_LineExtensionAmount);

                                    if ($item->code_fe == BOLSAS) {
                                        $xml_cac_InvoiceLine->appendChild($xml->createElement("cbc:FreeOfChargeIndicator", "true"));

                                        $xml_cac_PricingReference = $xml->createElement("cac:PricingReference");
                                            $xml_cac_AlternativeConditionPrice = $xml->createElement("cac:AlternativeConditionPrice");
                                                $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($item->item_tax, NUMBER_DECIMALS));
                                                    $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_AlternativeConditionPrice->appendChild($xml_cbc_PriceAmount);

                                                $xml_cac_AlternativeConditionPrice->appendChild($xml->createElement("cbc:PriceTypeCode", "01"));
                                            $xml_cac_PricingReference->appendChild($xml_cac_AlternativeConditionPrice);
                                        $xml_cac_InvoiceLine->appendChild($xml_cac_PricingReference);
                                    }

                                    $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                        if ($item->code_fe == BOLSAS) {
                                            $item_tax = $this->sma->formatDecimal($quantity * $item->rate, 2);
                                            $TaxAmount = $this->sma->formatDecimal($item_tax);
                                        } else {
                                            $item_tax = $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), 2);
                                            $TaxAmount = $item_tax;
                                        }
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                            if ($item->code_fe == BOLSAS) {
                                                $TaxableAmount = $item->quantity;
                                            } else {
                                                $TaxableAmount = ($net_unit_price * $quantity);
                                            }
                                            $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 2));
                                                $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                            $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 2));
                                                $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                            if ($item->code_fe == BOLSAS) {
                                                $TaxableAmount = $item->quantity;
                                                $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($TaxableAmount, 2));
                                                    $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                                $PerUnitAmount = $item->rate;
                                                $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, 2));
                                                    $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);
                                            }

                                            $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                if ($item->code_fe != BOLSAS) {
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($item->rate, 2)));
                                                }

                                                $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                                    $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                                $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                            $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                        $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                    $xml_cac_InvoiceLine->appendChild($xml_cac_TaxTotal);

                                    $xml_cac_Item = $xml->createElement("cac:Item");
                                        $xml_cac_Item->appendChild($xml->createElement("cbc:Description", $item->product_name . (!empty($item->nombre_variante) ? " (". $item->nombre_variante .")" : "") ));

                                        $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                                            $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                                                $xml_cbc_ID->setAttribute("schemeID", "999");
                                            $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                                        $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                                    $xml_cac_InvoiceLine->appendChild($xml_cac_Item);

                                    $xml_cac_Price = $xml->createElement("cac:Price");
                                        $PriceAmount = $item->net_unit_price;
                                        $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($PriceAmount, 2));
                                            $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_Price->appendChild($xml_cbc_PriceAmount);

                                        $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $this->sma->formatDecimal($item->quantity, 2));
                                            $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                                        $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                                    $xml_cac_InvoiceLine->appendChild($xml_cac_Price);
                                $xml_invoice->appendChild($xml_cac_InvoiceLine);
                            // }

                            $consecutive_product_identifier++;
                        }
                    /**************************************************************/
                $xml_Document->appendChild($xml_invoice);
            $xml->appendChild($xml_Document);

            if (!$print_xml) { return base64_encode($xml->saveXML()); }

            echo $xml->saveXML();
        }

        public function delcopBuildXmlCreditNote($credit_note_data, $print_xml = FALSE)
        {
            $reference_invoice = '';
            $issuer_data = $this->site->get_setting();
            $credit_note = $this->site->getSaleByID($credit_note_data->sale_id);
            $credit_note_items = $this->site->getAllSaleItems($credit_note_data->sale_id);
            $biller_data = $this->site->getCompanyByID($credit_note_data->biller_id);
            $customer_data = $this->site->getCompanyByID($credit_note_data->customer_id);
            $resolution_data = $this->site->getDocumentTypeById($credit_note->document_type_id);
            $issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $advance_payments = $this->get_advance_payments_sale($credit_note_data->sale_id);
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER);
            $CUDE_code = $this->generate_CUDE_DELCOP($credit_note, $credit_note_items, $issuer_data, $customer_data, $resolution_data, $print_xml);
            $payments_data = $this->site->getSalePayments($credit_note_data->sale_id);

            if (!empty($credit_note->year_database)) {
                $reference_invoice = $this->site->get_past_year_sale($credit_note->return_sale_ref, $credit_note->year_database);
            } else {
                if (!empty($credit_note->sale_id)) {
                    $reference_invoice = $this->site->getSaleByID($credit_note->sale_id);
                }
            }

            if ($print_xml) { header("content-type: application/xml; charset=ISO-8859-15"); }

            $xml = new DOMDocument("1.0");
            $xml_Document = $xml->createElement("Document");
                $xml_Document->setAttribute("xmlns", "https://titanio.com.co/contratos/facturaelectronica/v2");
                $xml_Document->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                $xml_Document->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                $xml_Document->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
                $xml_Document->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                $xml_Document->setAttribute("xmlns:sac", "urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2");
                $xml_Document->setAttribute("xmlns:sbc", "urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2");
                $xml_Document->setAttribute("xmlns:sig", "urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2");
                $xml_Document->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
                $xml_Document->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
                $xml_Document->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
                $xml_Document->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                $xml_Document->setAttribute("xsi:schemaLocation", "https://titanio.com.co/contratos/facturaelectronica/v2 XSD/UBL_DIAN_2.1.xsd");
                $xml_credit_note = $xml->createElement("CreditNote");
                    /************************* Encabezado *************************/
                        $xml_ext_UBLExtensions = $xml->createElement("ext:UBLExtensions");
                            $xml_ext_UBLExtension = $xml->createElement("ext:UBLExtension");
                                $xml_ext_ExtensionContent = $xml->createElement("ext:ExtensionContent");
                                    $xml_sts_DianExtensions = $xml->createElement("sts:DianExtensions");
                                        $xml_sts_InvoiceSource = $xml->createElement("sts:InvoiceSource");
                                            $xml_cbc_IdentificationCode = $xml->createElement("cbc:IdentificationCode", "CO");
                                                $xml_cbc_IdentificationCode->setAttribute("listAgencyID", "6");
                                                $xml_cbc_IdentificationCode->setAttribute("listAgencyName", "United Nations Economic Commission for Europe");
                                                $xml_cbc_IdentificationCode->setAttribute("listSchemeURI", "urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1");
                                            $xml_sts_InvoiceSource->appendChild($xml_cbc_IdentificationCode);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_InvoiceSource);
                                        $xml_sts_SoftwareProvider = $xml->createElement("sts:SoftwareProvider");
                                            $xml_sts_ProviderID = $xml->createElement("sts:ProviderID", "860028581");
                                                $xml_sts_ProviderID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_ProviderID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                                $xml_sts_ProviderID->setAttribute("schemeID", "1");
                                                $xml_sts_ProviderID->setAttribute("schemeName", "31");
                                            $xml_sts_SoftwareProvider->appendChild($xml_sts_ProviderID);
                                            $xml_sts_SoftwareID = $xml->createElement("sts:SoftwareID", "04441877-5591-4df8-88b1-b880943551");
                                                $xml_sts_SoftwareID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_SoftwareID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                            $xml_sts_SoftwareProvider->appendChild($xml_sts_SoftwareID);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_SoftwareProvider);
                                        $xml_sts_SoftwareSecurityCode = $xml->createElement("sts:SoftwareSecurityCode", "74926fc2f38d02dc3647d803b8058374a656cd12591c1d42b269c593e0d75ca3e8ebb274563f18dd813224b9fcd14bd4");
                                            $xml_sts_SoftwareSecurityCode->setAttribute("schemeAgencyID", "195");
                                            $xml_sts_SoftwareSecurityCode->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                        $xml_sts_DianExtensions->appendChild($xml_sts_SoftwareSecurityCode);
                                        $xml_sts_AuthorizationProvider = $xml->createElement("sts:AuthorizationProvider");
                                            $xml_sts_AuthorizationProviderID = $xml->createElement("sts:AuthorizationProviderID", "800197268");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeName", "31");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeID", "4");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                            $xml_sts_AuthorizationProvider->appendChild($xml_sts_AuthorizationProviderID);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_AuthorizationProvider);
                                        $xml_sts_QRCode = $xml->createElement("sts:QRCode", "");
                                        $xml_sts_DianExtensions->appendChild($xml_sts_QRCode);
                                    $xml_ext_ExtensionContent->appendChild($xml_sts_DianExtensions);
                                $xml_ext_UBLExtension->appendChild($xml_ext_ExtensionContent);
                            $xml_ext_UBLExtensions->appendChild($xml_ext_UBLExtension);
                        $xml_credit_note->appendChild($xml_ext_UBLExtensions);

                        $xml_credit_note->appendChild($xml->createElement("cbc:UBLVersionID", "UBL 2.1"));
                        if (!empty($reference_invoice)) {
                            $xml_credit_note->appendChild($xml->createElement("cbc:CustomizationID", "20"));
                        } else {
                            $xml_credit_note->appendChild($xml->createElement("cbc:CustomizationID", "22"));
                        }
                        $xml_credit_note->appendChild($xml->createElement("cbc:ProfileID", "DIAN 2.1: Nota Crédito de Factura Electrónica de Venta"));

                        $ProfileExecutionID = ($resolution_data->fe_work_environment == TEST) ? TEST : PRODUCTION;
                        $xml_credit_note->appendChild($xml->createElement("cbc:ProfileExecutionID", $ProfileExecutionID));
                        $xml_credit_note->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $credit_note->reference_no)));

                        $xml_cbc_UUID = $xml->createElement("cbc:UUID", $CUDE_code);
                            $xml_cbc_UUID->setAttribute("schemeID", $ProfileExecutionID);
                            $xml_cbc_UUID->setAttribute("schemeName", "CUDE-SHA384");
                        $xml_credit_note->appendChild($xml_cbc_UUID);

                        $xml_credit_note->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($credit_note->date)->date));
                        $xml_credit_note->appendChild($xml->createElement("cbc:IssueTime", $this->format_date($credit_note->date)->time));
                        $xml_credit_note->appendChild($xml->createElement("cbc:CreditNoteTypeCode", CREDIT_NOTE));
                        $xml_credit_note->appendChild($xml->createElement("cbc:DocumentCurrencyCode", "COP"));
                        $xml_credit_note->appendChild($xml->createElement("cbc:LineCountNumeric", count($credit_note_items)));
                        $xml_credit_note->appendChild($xml->createElement("cbc:Note", htmlspecialchars(str_replace(["&lt;p&gt;", "&lt;&sol;p&gt;", "&nbsp", "&comma;"], "", $credit_note->note))));

                        $rete_fuente_amount = $credit_note->rete_fuente_total;
                        $xml_credit_note->appendChild($xml->createElement("cbc:Note", $this->sma->formatDecimalNoRound($credit_note->rete_fuente_percentage, 2) ." | ". $rete_fuente_amount));

                        $rete_iva_amount = $credit_note->rete_iva_total;
                        $xml_credit_note->appendChild($xml->createElement("cbc:Note", $this->sma->formatDecimalNoRound($credit_note->rete_iva_percentage, 2) ." | ". $rete_iva_amount));

                        $rete_ica_amount = $credit_note->rete_ica_total;
                        $xml_credit_note->appendChild($xml->createElement("cbc:Note", $this->sma->formatDecimalNoRound($credit_note->rete_ica_percentage, 2) ." | ". $rete_ica_amount));
                    /**************************************************************/

                    /********************* Factura referencia *********************/
                        if (!empty($reference_invoice)) {
                            $xml_cac_BillingReference = $xml->createElement("cac:BillingReference");

                                $xml_cac_InvoiceDocumentReference = $xml->createElement("cac:InvoiceDocumentReference");
                                    $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $credit_note->return_sale_ref)));
                                    $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:UUID", $reference_invoice->cufe));
                                    $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($reference_invoice->date)->date));

                                $xml_cac_BillingReference->appendChild($xml_cac_InvoiceDocumentReference);
                            $xml_credit_note->appendChild($xml_cac_BillingReference);

                            $conceptCorrection = $this->getConceptCorrectionNote($credit_note, $reference_invoice);
                            if (!empty($conceptCorrection)) {
                                $xml_cac_DiscrepancyResponse = $xml->createElement("cac:DiscrepancyResponse");
                                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ReferenceID", str_replace('-', '', $credit_note->return_sale_ref)));
                                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:ResponseCode", $conceptCorrection->code));
                                    $xml_cac_DiscrepancyResponse->appendChild($xml->createElement("cbc:Description", $conceptCorrection->description));
                                $xml_credit_note->appendChild($xml_cac_DiscrepancyResponse);
                            }
                        } else {
                            $xml_cac_InvoicePeriod = $xml->createElement("cac:InvoicePeriod");
                                $date = !empty($credit_note->document_without_reference_date) ? $credit_note->document_without_reference_date : $credit_note->date;
                                $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:StartDate", date("Y-m-01", strtotime($date))));
                                $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:StartTime", "00:00:00-05:00"));
                                $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:EndDate", date("Y-m-t", strtotime($date))));
                                $xml_cac_InvoicePeriod->appendChild($xml->createElement("cbc:EndTime", "00:00:00-05:00"));
                            $xml_credit_note->appendChild($xml_cac_InvoicePeriod);
                        }
                    /**************************************************************/

                    /*************************** Emisor ***************************/
                        $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                            $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $issuer_data->tipo_persona));
                            $xml_cac_Party = $xml->createElement("cac:Party");
                                $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                    $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->nombre_comercial)));
                                $xml_cac_Party->appendChild($xml_cac_PartyName);

                                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                    $xml_cac_Address = $xml->createElement("cac:Address");
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($biller_data->city))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($biller_data->DEPARTAMENTO))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                        $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($biller_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_Address->appendChild($xml_cac_Country);
                                    $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                    $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                        $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                    $type_issuers_obligations = '';
                                    foreach ($issuer_obligations as $issuer_obligation) {
                                        $type_issuers_obligations .= $issuer_obligation->types_obligations_id .';';
                                    }

                                    if (empty($type_issuers_obligations)) {
                                        $type_issuers_obligations = "R-99-PN";
                                    }

                                    $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_issuers_obligations, ";"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                    $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($biller_data->city))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity",ucfirst(mb_strtolower($biller_data->DEPARTAMENTO))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($biller_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                                $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                    $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                        $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);

                                    $xml_cac_CorporateRegistrationScheme = $xml->createElement("cac:CorporateRegistrationScheme");
                                        $xml_cac_CorporateRegistrationScheme->appendChild($xml->createElement("cbc:ID", $resolution_data->sales_prefix));
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cac_CorporateRegistrationScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                                $xml_cac_Contact = $xml->createElement("cac:Contact");
                                    $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $issuer_data->default_email));
                                $xml_cac_Party->appendChild($xml_cac_Contact);
                            $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                        $xml_credit_note->appendChild($xml_cac_AccountingSupplierParty);
                    /**************************************************************/

                    /************************ Adquiriente. ************************/
                        $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                            $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer_data->type_person));
                            $xml_cac_Party = $xml->createElement("cac:Party");
                                if ($customer_data->type_person == NATURAL_PERSON) {
                                    $xml_cac_PartyIdentification = $xml->createElement("cac:PartyIdentification");
                                        $xml_cbc_ID = $xml->createElement("cbc:ID", $this->clear_document_number($customer_data->vat_no));
                                            if ($customer_data->document_code == NIT) {
                                                $xml_cbc_ID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                            }
                                            $xml_cbc_ID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cac_PartyIdentification->appendChild($xml_cbc_ID);
                                    $xml_cac_Party->appendChild($xml_cac_PartyIdentification);
                                }

                                $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                    $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                                $xml_cac_Party->appendChild($xml_cac_PartyName);

                                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                    $xml_cac_Address = $xml->createElement("cac:Address");
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($customer_data->city))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($customer_data->state))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                        $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($customer_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_Address->appendChild($xml_cac_Country);
                                    $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                    $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clear_document_number($customer_data->vat_no));
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }

                                        $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                    $type_customer_obligations = '';
                                    foreach ($customer_obligations as $customer_obligation) {
                                        $type_customer_obligations .= $customer_obligation->types_obligations_id .';';
                                    }

                                    if (empty($type_customer_obligations)) {
                                        $type_customer_obligations = "R-99-PN";
                                    }

                                    $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_customer_obligations, ";"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                    $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($customer_data->city))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($customer_data->state))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));

                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);

                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($customer_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                                $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                    $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));

                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $customer_data->vat_no);
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }

                                        $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);
                                $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                                if ($this->Settings->fe_work_environment == TEST) {
                                    $xml_cac_Contact = $xml->createElement("cac:Contact");
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", "Wappsi Innovation"));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Telephone", "301 794 6302"));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", "wappsi.desarrolloweb02@gmail.com"));
                                    $xml_cac_Party->appendChild($xml_cac_Contact);
                                } else {
                                    $xml_cac_Contact = $xml->createElement("cac:Contact");
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Telephone", $customer_data->phone));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $customer_data->email));
                                    $xml_cac_Party->appendChild($xml_cac_Contact);
                                }
                            $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                        $xml_credit_note->appendChild($xml_cac_AccountingCustomerParty);
                    /**************************************************************/

                    /************************ Medios pago *************************/
                        $payment_amount = 0;
                        if (!empty($payments_data)) {
                            foreach ($payments_data as $payment) {
                                $payment_amount += $payment->amount;
                            }
                        }

                        if ($credit_note->grand_total == $payment_amount) {
                            $method = CASH;
                        } else {
                            $method = CREDIT;
                        }

                        if (!empty($payments_data)) {
                            foreach ($payments_data as $payment) {
                                if ($payment->paid_by != 'retencion') {
                                    $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $payment->mean_payment_code_fe));

                                        if ($method == CREDIT) {
                                            $fecha_creacion = date("Y-m-d", strtotime($credit_note->date));
                                            if (empty($credit_note->payment_term)) {
                                                $payment_term = 0;
                                            } else {
                                                $payment_term = $credit_note->payment_term;
                                            }

                                            $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                            if ($payment_term > 0) {
                                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                            }
                                        }
                                    $xml_credit_note->appendChild($xml_cac_PaymentMeans);
                                } else {
                                    $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                                        $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                                        if ($method == CREDIT) {
                                            $fecha_creacion = date("Y-m-d", strtotime($credit_note->date));
                                            if (empty($credit_note->payment_term)) {
                                                $payment_term = 0;
                                            } else {
                                                $payment_term = $credit_note->payment_term;
                                            }

                                            $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                            if ($payment_term > 0) {
                                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                            }
                                        }
                                    $xml_credit_note->appendChild($xml_cac_PaymentMeans);
                                }
                            }
                        } else {
                            $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $method));
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", "ZZZ"));

                            if ($method == CREDIT) {
                                $fecha_creacion = date("Y-m-d", strtotime($credit_note->date));
                                if (empty($credit_note->payment_term)) {
                                    $payment_term = 0;
                                } else {
                                    $payment_term = $credit_note->payment_term;
                                }

                                $fecha_vencimiento = date("Y-m-d", strtotime("+". $payment_term ." day", strtotime($fecha_creacion)));
                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));

                                if ($payment_term > 0) {
                                    $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:InstructionNote", ($payment_term > 1) ? $payment_term ." días" : $payment_term ." día"));
                                }
                            }

                            $xml_credit_note->appendChild($xml_cac_PaymentMeans);
                        }
                    /**************************************************************/

                    /********************** Término de pago ***********************/
                        $xml_cac_PaymentTerms = $xml->createElement("cac:PaymentTerms");
                            $TaxAmount = 0;
                            $PayableAmount = $credit_note->grand_total;

                            if ($credit_note->rete_fuente_total > 0) { $TaxAmount += $credit_note->rete_fuente_total; }
                            if ($credit_note->rete_iva_total > 0) { $TaxAmount += $credit_note->rete_iva_total; }
                            if ($credit_note->rete_ica_total > 0) { $TaxAmount += $credit_note->rete_ica_total; }

                            $xml_cac_PaymentTerms->appendChild($xml->createElement("cbc:Amount", ($PayableAmount * -1) - $TaxAmount));
                        $xml_credit_note->appendChild($xml_cac_PaymentTerms);
                    /**************************************************************/

                    /******************** Descuentos y cargos *********************/
                        $charge_or_discount = 1;

                        if (abs($credit_note->order_discount) > 0) {
                            $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "false"));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReasonCode", "01"));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:AllowanceChargeReason", "Otro descuento"));

                                if (strpos($credit_note->order_discount_id, "%") !== FALSE) {
                                    $order_discount_percentage = str_replace("%", "", $credit_note->order_discount_id);
                                } else {
                                    $order_discount_percentage = (abs($credit_note->order_discount) * 100) / abs($credit_note->total);
                                }

                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($order_discount_percentage, 4)));

                                $Amount = abs($credit_note->order_discount);
                                $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, 4));
                                    $xml_cac_Amount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                                $BaseAmount = abs($credit_note->total);
                                $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, 4));
                                    $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);
                            $xml_credit_note->appendChild($xml_cac_AllowanceCharge);
                        }

                        if (abs($credit_note->shipping) > 0) {
                            $xml_cac_AllowanceCharge = $xml->createElement("cac:AllowanceCharge");
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ID", $charge_or_discount++));
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:ChargeIndicator", "true"));

                                $order_charge_percentage = (abs($credit_note->shipping) * 100) / abs($credit_note->total);
                                $xml_cac_AllowanceCharge->appendChild($xml->createElement("cbc:MultiplierFactorNumeric", $this->sma->formatDecimal($order_charge_percentage, 4)));

                                $Amount = abs($credit_note->shipping);
                                $xml_cac_Amount = $xml->createElement("cbc:Amount", $this->sma->formatDecimal($Amount, 4));
                                    $xml_cac_Amount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_Amount);

                                $BaseAmount = abs($credit_note->total);
                                $xml_cac_BaseAmount = $xml->createElement("cbc:BaseAmount", $this->sma->formatDecimal($BaseAmount, 4));
                                    $xml_cac_BaseAmount->setAttribute("currencyID", "COP");
                                $xml_cac_AllowanceCharge->appendChild($xml_cac_BaseAmount);

                            $xml_credit_note->appendChild($xml_cac_AllowanceCharge);
                        }
                    /**************************************************************/

                    /********************** Importes totales. *********************/
                        /************************ Impuestos. **********************/
                            $product_tax_iva = $product_tax_ic = $product_tax_inc = $product_tax_bolsas = $subtotal = $total_tax = $total_tax_base = $exempt_existing = $shipping = $order_discount = $tax_value_bolsas = $tax_base_bolsas = 0;

                            $tax_rates = $this->get_tax_rate_by_code_fe();

                            foreach ($tax_rates as $tax_rate) {
                                if ($tax_rate->code_fe == IVA) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_value_iva_array[$index] = 0;
                                    $tax_base_iva_array[$index] = 0;
                                } else if ($tax_rate->code_fe == INC) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_value_inc_array[$index] = 0;
                                    $tax_base_inc_array[$index] = 0;
                                } else if ($tax_rate->rate == BOLSAS) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_base_bolsas_array[$index] = 0;
                                    $tax_value_bolsas_array[$index] = 0;
                                }
                            }

                            foreach ($credit_note_items as $item) {
                                $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                                $net_unit_price = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);

                                $subtotal += $this->sma->formatDecimal($net_unit_price * $quantity, NUMBER_DECIMALS);
                                $total_tax += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                                if ($item->code_fe == IVA) {
                                    if ($item->rate == 0) { $exempt_existing++; }

                                    $product_tax_iva += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                                    foreach ($tax_rates as $tax_rate) {
                                        if ($item->rate == $tax_rate->rate) {
                                            $index = (int) $tax_rate->rate;
                                            $total_tax_base += $item->net_unit_price * $item->quantity;

                                            $tax_base_iva_array[$index] += $this->sma->formatDecimal($net_unit_price * $quantity, NUMBER_DECIMALS);
                                            $tax_value_iva_array[$index] += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                                            break;
                                        }
                                    }
                                } else if ($item->code_fe == INC) {
                                    $product_tax_inc += $item->item_tax;

                                    foreach ($tax_rates as $tax_rate) {
                                        if ($item->rate == $tax_rate->rate) {
                                            $index = (int) $tax_rate->rate;
                                            $tax_base_inc_array[$index] += $item->net_unit_price * ($item->quantity * -1);
                                            $tax_value_inc_array[$index] += $item->item_tax;
                                            $total_tax_base += $item->net_unit_price * $item->quantity;

                                            break;
                                        }
                                    }
                                } else if ($item->code_fe == BOLSAS) {
                                    $index = (int) $item->rate;
                                    $product_tax_bolsas += $item->quantity * $item->rate;
                                    $tax_base_bolsas += $item->quantity;
                                    $tax_value_bolsas += $item->quantity * $item->rate;
                                }
                            }

                            if ($product_tax_iva) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = abs($product_tax_iva);
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, NUMBER_DECIMALS));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                        if (!empty($tax_base_iva)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_iva;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_iva_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_credit_note->appendChild($xml_cac_TaxTotal);
                            } else if (count($credit_note_items) >= 1 && $exempt_existing > 0) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_iva;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount, 4));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                        if (!empty($tax_base_iva)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_iva;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_iva_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 4)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_credit_note->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_ic) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_ic;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, 4));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_ic_array as $percentage => $tax_base_ic) {
                                        if (!empty($tax_base_ic)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");

                                                $TaxableAmount = $tax_base_ic;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 4));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_ic_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, 4));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IC));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IC"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_credit_note->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_inc) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_inc;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, 4));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_inc_array as $percentage => $tax_base_inc) {
                                        if (!empty($tax_base_inc)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_inc;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal($TaxableAmount, 4));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_inc_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, 4));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", INC));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_credit_note->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_bolsas) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");

                                    $TaxAmount = $product_tax_bolsas;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $tax_base_bolsas;
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), 2));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $TaxAmount = $tax_value_bolsas;
                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal($TaxAmount * -1, 2));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", 1.00);
                                            $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "NIU");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $PerUnitAmount = $tax_value_bolsas;
                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal(abs($PerUnitAmount), 2));
                                            $xml_cbc_BaseUnitMeasure->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", BOLSAS));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC Bolsas"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_credit_note->appendChild($xml_cac_TaxTotal);
                            }
                        /***********************************************************/
                    /**************************************************************/

                    /********************** Total impuestos ***********************/
                        $xml_cac_LegalMonetaryTotal = $xml->createElement("cac:LegalMonetaryTotal");
                            $LineExtensionAmount = $subtotal;
                            $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal(abs($LineExtensionAmount), NUMBER_DECIMALS));
                                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                            $TaxExclusiveAmount = ($subtotal /*+ $tax_base_bolsas*/);
                            $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimal(abs($TaxExclusiveAmount), NUMBER_DECIMALS));
                                $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                            $TaxInclusiveAmount = (abs($subtotal) + abs($total_tax) + abs($product_tax_bolsas));
                            $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimal(abs($TaxInclusiveAmount), NUMBER_DECIMALS));
                                $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                            if (abs($credit_note->order_discount) > 0) {
                                $order_discount = $this->sma->formatDecimal($credit_note->order_discount, NUMBER_DECIMALS);
                                $AllowanceTotalAmount = abs($order_discount);
                                $xml_cbc_AllowanceTotalAmount = $xml->createElement("cbc:AllowanceTotalAmount", $this->sma->formatDecimal($AllowanceTotalAmount, 4));
                                    $xml_cbc_AllowanceTotalAmount->setAttribute("currencyID", "COP");
                                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_AllowanceTotalAmount);
                            }

                            if (abs($credit_note->shipping) > 0) {
                                $shipping = $this->sma->formatDecimal($credit_note->shipping, 2);
                                $ChargeTotalAmount = abs($shipping);
                                $xml_cbc_ChargeTotalAmount = $xml->createElement("cbc:ChargeTotalAmount", $this->sma->formatDecimal($ChargeTotalAmount, 4));
                                    $xml_cbc_ChargeTotalAmount->setAttribute("currencyID", "COP");
                                $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_ChargeTotalAmount);
                            }


                            $PayableAmount = (abs($subtotal) + abs($total_tax) + abs($product_tax_bolsas) + abs($shipping) - abs($order_discount));
                            $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimal($PayableAmount, NUMBER_DECIMALS));
                                $xml_cbc_PayableAmount->setAttribute("currencyID", "COP");
                            $xml_cac_LegalMonetaryTotal->appendChild($xml_cbc_PayableAmount);

                        $xml_credit_note->appendChild($xml_cac_LegalMonetaryTotal);
                    /**************************************************************/

                    /************************* Productos **************************/
                        $consecutive_product_identifier = 1;
                        foreach ($credit_note_items as $item) {
                            if ($item->code_fe == IVA) {
                                $tax_name = "IVA";
                            }

                            $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                            $net_unit_price = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);

                            $xml_cac_CreditNoteLine = $xml->createElement("cac:CreditNoteLine");
                                $xml_cac_CreditNoteLine->appendChild($xml->createElement("cbc:ID", $consecutive_product_identifier));
                                $xml_cbc_CreditedQuantity = $xml->createElement("cbc:CreditedQuantity", $this->sma->formatDecimal(abs($quantity), NUMBER_DECIMALS));
                                    $xml_cbc_CreditedQuantity->setAttribute("unitCode", "NIU");
                                $xml_cac_CreditNoteLine->appendChild($xml_cbc_CreditedQuantity);

                                $LineExtensionAmount = ($net_unit_price * $quantity);
                                $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimal(abs($LineExtensionAmount), NUMBER_DECIMALS));
                                    $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                                $xml_cac_CreditNoteLine->appendChild($xml_cbc_LineExtensionAmount);

                                if ($item->code_fe == BOLSAS) {
                                    $xml_cac_CreditNoteLine->appendChild($xml->createElement("cbc:FreeOfChargeIndicator", "true"));

                                    $xml_cac_PricingReference = $xml->createElement("cac:PricingReference");
                                        $xml_cac_AlternativeConditionPrice = $xml->createElement("cac:AlternativeConditionPrice");
                                            $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal(abs($item->item_tax), NUMBER_DECIMALS));
                                                $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_AlternativeConditionPrice->appendChild($xml_cbc_PriceAmount);

                                            $xml_cac_AlternativeConditionPrice->appendChild($xml->createElement("cbc:PriceTypeCode", "01"));
                                        $xml_cac_PricingReference->appendChild($xml_cac_AlternativeConditionPrice);
                                    $xml_cac_CreditNoteLine->appendChild($xml_cac_PricingReference);
                                }

                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    if ($item->code_fe == BOLSAS) {
                                        $item_tax = $this->sma->formatDecimal($quantity * $item->rate, 2);
                                        $TaxAmount = $this->sma->formatDecimal($item_tax);
                                    } else {
                                        $item_tax = $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), 2);
                                        $TaxAmount = $item_tax;
                                    }

                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        if ($item->code_fe == BOLSAS) {
                                            $TaxableAmount = $item->quantity;
                                        } else {
                                            $TaxableAmount = ($net_unit_price * $quantity);
                                        }
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimal(abs($TaxableAmount), NUMBER_DECIMALS));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimal(abs($TaxAmount), NUMBER_DECIMALS));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        if ($item->code_fe == BOLSAS) {
                                            $TaxableAmount = abs($item->quantity);
                                            $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", $this->sma->formatDecimal($TaxableAmount, 2));
                                                $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "94");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                            $PerUnitAmount = $item->rate;
                                            $xml_cbc_PerUnitAmount = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimal($PerUnitAmount, 2));
                                                $xml_cbc_PerUnitAmount->setAttribute("currencyID", "COP");
                                            $xml_cac_TaxSubtotal->appendChild($xml_cbc_PerUnitAmount);
                                        }

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            if ($item->code_fe != BOLSAS) {
                                                $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimal($item->rate, 2)));
                                            }
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_cac_CreditNoteLine->appendChild($xml_cac_TaxTotal);

                                $xml_cac_Item = $xml->createElement("cac:Item");
                                    $xml_cac_Item->appendChild($xml->createElement("cbc:Description", $item->product_name));
                                    $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                                        $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                                            $xml_cbc_ID->setAttribute("schemeID", "999");
                                        $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                                    $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                                $xml_cac_CreditNoteLine->appendChild($xml_cac_Item);

                                $xml_cac_Price = $xml->createElement("cac:Price");
                                    $PriceAmount = $item->net_unit_price;
                                    $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimal($PriceAmount, NUMBER_DECIMALS));
                                        $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_Price->appendChild($xml_cbc_PriceAmount);

                                    $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $this->sma->formatDecimal(abs($item->quantity), NUMBER_DECIMALS));
                                        $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                                    $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                                $xml_cac_CreditNoteLine->appendChild($xml_cac_Price);
                            $xml_credit_note->appendChild($xml_cac_CreditNoteLine);

                            $consecutive_product_identifier++;
                        }
                    /**************************************************************/
                $xml_Document->appendChild($xml_credit_note);
            $xml->appendChild($xml_Document);

            if (!$print_xml) { return base64_encode($xml->saveXML()); }

            echo $xml->saveXML();
        }

        public function delcopBuildXmlDebitNote($debit_note_data, $print_xml = FALSE)
        {
            $issuer_data = $this->site->get_setting();
            $debit_note = $this->site->getSaleByID($debit_note_data->sale_id);
            $items_sale = $this->site->getAllSaleItems($debit_note_data->sale_id);
            $biller_data = $this->site->getCompanyByID($debit_note_data->biller_id);
            $customer_data = $this->site->getCompanyByID($debit_note_data->customer_id);
            $reference_invoice = $this->site->getSaleByID($debit_note->reference_invoice_id);
            $resolution_data = $this->site->getDocumentTypeById($debit_note->document_type_id);
            $issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER);
            $advance_payments = $this->get_advance_payments_sale($debit_note_data->sale_id);
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER);
            $resolution_data_referenced_invoice = $this->site->getDocumentTypeById($reference_invoice->document_type_id);
            $CUDE_code = $this->generate_CUDE_DELCOP($debit_note, $items_sale, $issuer_data, $customer_data, $resolution_data);

            if ($print_xml) { header("content-type: application/xml; charset=ISO-8859-15"); }

            $xml = new DOMDocument("1.0");
            $xml_Document = $xml->createElement("Document");
                $xml_Document->setAttribute("xmlns", "https://titanio.com.co/contratos/facturaelectronica/v2");
                $xml_Document->setAttribute("xmlns:cac", "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2");
                $xml_Document->setAttribute("xmlns:cbc", "urn:oasis:names:specification:ubl:schema:xsd:CommonBasicComponents-2");
                $xml_Document->setAttribute("xmlns:ds", "http://www.w3.org/2000/09/xmldsig#");
                $xml_Document->setAttribute("xmlns:ext", "urn:oasis:names:specification:ubl:schema:xsd:CommonExtensionComponents-2");
                $xml_Document->setAttribute("xmlns:sac", "urn:oasis:names:specification:ubl:schema:xsd:SignatureAggregateComponents-2");
                $xml_Document->setAttribute("xmlns:sbc", "urn:oasis:names:specification:ubl:schema:xsd:SignatureBasicComponents-2");
                $xml_Document->setAttribute("xmlns:sig", "urn:oasis:names:specification:ubl:schema:xsd:CommonSignatureComponents-2");
                $xml_Document->setAttribute("xmlns:sts", "dian:gov:co:facturaelectronica:Structures-2-1");
                $xml_Document->setAttribute("xmlns:xades", "http://uri.etsi.org/01903/v1.3.2#");
                $xml_Document->setAttribute("xmlns:xades141", "http://uri.etsi.org/01903/v1.4.1#");
                $xml_Document->setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                $xml_Document->setAttribute("xsi:schemaLocation", "https://titanio.com.co/contratos/facturaelectronica/v2 XSD/UBL_DIAN_2.1.xsd");

                $xml_debit_note = $xml->createElement("DebitNote");
                    /************************* Encabezado *************************/
                        $xml_ext_UBLExtensions = $xml->createElement("ext:UBLExtensions");
                            $xml_ext_UBLExtension = $xml->createElement("ext:UBLExtension");
                                $xml_ext_ExtensionContent = $xml->createElement("ext:ExtensionContent");
                                    $xml_sts_DianExtensions = $xml->createElement("sts:DianExtensions");
                                        $xml_sts_InvoiceSource = $xml->createElement("sts:InvoiceSource");
                                            $xml_cbc_IdentificationCode = $xml->createElement("cbc:IdentificationCode", "CO");
                                                $xml_cbc_IdentificationCode->setAttribute("listAgencyID", "6");
                                                $xml_cbc_IdentificationCode->setAttribute("listAgencyName", "United Nations Economic Commission for Europe");
                                                $xml_cbc_IdentificationCode->setAttribute("listSchemeURI", "urn:oasis:names:specification:ubl:codelist:gc:CountryIdentificationCode-2.1");
                                            $xml_sts_InvoiceSource->appendChild($xml_cbc_IdentificationCode);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_InvoiceSource);
                                        $xml_sts_SoftwareProvider = $xml->createElement("sts:SoftwareProvider");
                                            $xml_sts_ProviderID = $xml->createElement("sts:ProviderID", "860028581");
                                                $xml_sts_ProviderID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_ProviderID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                                $xml_sts_ProviderID->setAttribute("schemeID", "1");
                                                $xml_sts_ProviderID->setAttribute("schemeName", "31");
                                            $xml_sts_SoftwareProvider->appendChild($xml_sts_ProviderID);
                                            $xml_sts_SoftwareID = $xml->createElement("sts:SoftwareID", "04441877-5591-4df8-88b1-b880943551");
                                                $xml_sts_SoftwareID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_SoftwareID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                            $xml_sts_SoftwareProvider->appendChild($xml_sts_SoftwareID);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_SoftwareProvider);
                                        $xml_sts_SoftwareSecurityCode = $xml->createElement("sts:SoftwareSecurityCode", "d117463c409aeeab179c465bca8d74b5cf4c02d0f4b17b1cb449f60abf328771b123f102e0af86d654121a48caa971fd");
                                            $xml_sts_SoftwareSecurityCode->setAttribute("schemeAgencyID", "195");
                                            $xml_sts_SoftwareSecurityCode->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                        $xml_sts_DianExtensions->appendChild($xml_sts_SoftwareSecurityCode);
                                        $xml_sts_AuthorizationProvider = $xml->createElement("sts:AuthorizationProvider");
                                            $xml_sts_AuthorizationProviderID = $xml->createElement("sts:AuthorizationProviderID", "800197268");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeName", "31");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeID", "4");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeAgencyID", "195");
                                                $xml_sts_AuthorizationProviderID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                            $xml_sts_AuthorizationProvider->appendChild($xml_sts_AuthorizationProviderID);
                                        $xml_sts_DianExtensions->appendChild($xml_sts_AuthorizationProvider);
                                        $xml_sts_QRCode = $xml->createElement("sts:QRCode", "");
                                        $xml_sts_DianExtensions->appendChild($xml_sts_QRCode);
                                    $xml_ext_ExtensionContent->appendChild($xml_sts_DianExtensions);
                                $xml_ext_UBLExtension->appendChild($xml_ext_ExtensionContent);
                            $xml_ext_UBLExtensions->appendChild($xml_ext_UBLExtension);
                        $xml_debit_note->appendChild($xml_ext_UBLExtensions);

                        $xml_debit_note->appendChild($xml->createElement("cbc:UBLVersionID", "UBL 2.1"));
                        $xml_debit_note->appendChild($xml->createElement("cbc:CustomizationID", "30"));
                        $xml_debit_note->appendChild($xml->createElement("cbc:ProfileID", "DIAN 2.1: Nota Débito de Factura Electrónica de Venta"));

                        $ProfileExecutionID = ($resolution_data->fe_work_environment == TEST) ? TEST : PRODUCTION;
                        $xml_debit_note->appendChild($xml->createElement("cbc:ProfileExecutionID", $ProfileExecutionID));
                        $xml_debit_note->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $debit_note->reference_no)));

                        $xml_cbc_UUID = $xml->createElement("cbc:UUID", $CUDE_code);
                            $xml_cbc_UUID->setAttribute("schemeID", $ProfileExecutionID);
                            $xml_cbc_UUID->setAttribute("schemeName", "CUDE-SHA384");
                        $xml_debit_note->appendChild($xml_cbc_UUID);

                        $xml_debit_note->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($debit_note->date)->date));
                        $xml_debit_note->appendChild($xml->createElement("cbc:IssueTime", $this->format_date($debit_note->date)->time));
                        $xml_debit_note->appendChild($xml->createElement("cbc:CreditNoteTypeCode", DEBIT_NOTE));
                        $xml_debit_note->appendChild($xml->createElement("cbc:DocumentCurrencyCode", "COP"));
                        $xml_debit_note->appendChild($xml->createElement("cbc:LineCountNumeric", count($items_sale)));
                        $xml_debit_note->appendChild($xml->createElement("cbc:Note", htmlspecialchars(str_replace(["&lt;p&gt;", "&lt;&sol;p&gt;", "&nbsp", "&comma;"], "", $debit_note->note))));

                        $rete_fuente_amount = $debit_note->rete_fuente_total;
                        $xml_debit_note->appendChild($xml->createElement("cbc:Note", $this->sma->formatDecimalNoRound($debit_note->rete_fuente_percentage, 2) ." | ". $rete_fuente_amount));

                        $rete_iva_amount = $debit_note->rete_iva_total;
                        $xml_debit_note->appendChild($xml->createElement("cbc:Note", $this->sma->formatDecimalNoRound($debit_note->rete_iva_percentage, 2) ." | ". $rete_iva_amount));

                        $rete_ica_amount = $debit_note->rete_ica_total;
                        $xml_debit_note->appendChild($xml->createElement("cbc:Note", $this->sma->formatDecimalNoRound($debit_note->rete_ica_percentage, 2) ." | ". $rete_ica_amount));
                    /**************************************************************/

                    /********************* Factura referencia *********************/
                        $xml_cac_BillingReference = $xml->createElement("cac:BillingReference");
                            $xml_cac_InvoiceDocumentReference = $xml->createElement("cac:InvoiceDocumentReference");
                                $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:ID", str_replace('-', '', $debit_note->return_sale_ref)));
                                $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:UUID", $reference_invoice->cufe));
                                $xml_cac_InvoiceDocumentReference->appendChild($xml->createElement("cbc:IssueDate", $this->format_date($reference_invoice->date)->date));
                            $xml_cac_BillingReference->appendChild($xml_cac_InvoiceDocumentReference);
                        $xml_debit_note->appendChild($xml_cac_BillingReference);
                    /**************************************************************/

                    /*************************** Emisor ***************************/
                        $xml_cac_AccountingSupplierParty = $xml->createElement("cac:AccountingSupplierParty");
                            $xml_cac_AccountingSupplierParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $issuer_data->tipo_persona));
                            $xml_cac_Party = $xml->createElement("cac:Party");
                                $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                    $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->nombre_comercial)));
                                $xml_cac_Party->appendChild($xml_cac_PartyName);

                                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                    $xml_cac_Address = $xml->createElement("cac:Address");
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($biller_data->city))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($biller_data->DEPARTAMENTO))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                        $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($biller_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_Address->appendChild($xml_cac_Country);
                                    $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                    $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                        $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                    $type_issuers_obligations = '';
                                    foreach ($issuer_obligations as $issuer_obligation) {
                                        $type_issuers_obligations .= $issuer_obligation->types_obligations_id .';';
                                    }

                                    if (empty($type_issuers_obligations)) {
                                        $type_issuers_obligations = "R-99-PN";
                                    }

                                    $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_issuers_obligations, ";"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                    $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($biller_data->codigo_municipio, -5)));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($biller_data->city))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($biller_data->DEPARTAMENTO))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($biller_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $biller_data->address));
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $issuer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($biller_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                                $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                    $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $issuer_data->numero_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeID", $issuer_data->digito_verificacion);
                                        $xml_cbc_CompanyID->setAttribute("schemeName", $issuer_data->tipo_documento);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);

                                    $xml_cac_CorporateRegistrationScheme = $xml->createElement("cac:CorporateRegistrationScheme");
                                        $xml_cac_CorporateRegistrationScheme->appendChild($xml->createElement("cbc:ID", $resolution_data->sales_prefix));
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cac_CorporateRegistrationScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                                $xml_cac_Contact = $xml->createElement("cac:Contact");
                                    $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($issuer_data->razon_social)));
                                    $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $issuer_data->default_email));
                                $xml_cac_Party->appendChild($xml_cac_Contact);
                            $xml_cac_AccountingSupplierParty->appendChild($xml_cac_Party);
                        $xml_debit_note->appendChild($xml_cac_AccountingSupplierParty);
                    /**************************************************************/

                    /************************ Adquiriente. ************************/
                        $xml_cac_AccountingCustomerParty = $xml->createElement("cac:AccountingCustomerParty");
                            $xml_cac_AccountingCustomerParty->appendChild($xml->createElement("cbc:AdditionalAccountID", $customer_data->type_person));
                            $xml_cac_Party = $xml->createElement("cac:Party");
                                if ($customer_data->type_person == NATURAL_PERSON) {
                                    $xml_cac_PartyIdentification = $xml->createElement("cac:PartyIdentification");
                                        $xml_cbc_ID = $xml->createElement("cbc:ID", $this->clear_document_number($customer_data->vat_no));
                                            if ($customer_data->document_code == NIT) {
                                                $xml_cbc_ID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                            }
                                            $xml_cbc_ID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cac_PartyIdentification->appendChild($xml_cbc_ID);
                                    $xml_cac_Party->appendChild($xml_cac_PartyIdentification);
                                }

                                $xml_cac_PartyName = $xml->createElement("cac:PartyName");
                                    $xml_cac_PartyName->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                                $xml_cac_Party->appendChild($xml_cac_PartyName);

                                $xml_cac_PhysicalLocation = $xml->createElement("cac:PhysicalLocation");
                                    $xml_cac_Address = $xml->createElement("cac:Address");
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($customer_data->city))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($customer_data->state))));
                                        $xml_cac_Address->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));
                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                        $xml_cac_Address->appendChild($xml_cac_AddressLine);
                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($customer_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_Address->appendChild($xml_cac_Country);
                                    $xml_cac_PhysicalLocation->appendChild($xml_cac_Address);
                                $xml_cac_Party->appendChild($xml_cac_PhysicalLocation);

                                $xml_cac_PartyTaxScheme = $xml->createElement("cac:PartyTaxScheme");
                                    $xml_cac_PartyTaxScheme->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));
                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $this->clear_document_number($customer_data->vat_no));
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }

                                        $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_CompanyID);

                                    $type_customer_obligations = '';
                                    foreach ($customer_obligations as $customer_obligation) {
                                        $type_customer_obligations .= $customer_obligation->types_obligations_id .';';
                                    }

                                    if (empty($type_customer_obligations)) {
                                        $type_customer_obligations = "R-99-PN";
                                    }

                                    $xml_cbc_TaxLevelCode = $xml->createElement("cbc:TaxLevelCode", trim($type_customer_obligations, ";"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cbc_TaxLevelCode);

                                    $xml_cac_RegistrationAddress = $xml->createElement("cac:RegistrationAddress");
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:ID", substr($customer_data->city_code, -5)));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CityName", ucfirst(mb_strtolower($customer_data->city))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentity", ucfirst(mb_strtolower($customer_data->state))));
                                        $xml_cac_RegistrationAddress->appendChild($xml->createElement("cbc:CountrySubentityCode", substr($customer_data->CODDEPARTAMENTO, -2)));

                                        $xml_cac_AddressLine = $xml->createElement("cac:AddressLine");
                                            $xml_cac_AddressLine->appendChild($xml->createElement("cbc:Line", $customer_data->address));
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_AddressLine);

                                        $xml_cac_Country = $xml->createElement("cac:Country");
                                            $xml_cac_Country->appendChild($xml->createElement("cbc:IdentificationCode", $customer_data->codigo_iso));
                                            $xml_cbc_Name = $xml->createElement("cbc:Name", ucfirst(mb_strtolower($customer_data->country)));
                                                $xml_cbc_Name->setAttribute("languageID", "es");
                                            $xml_cac_Country->appendChild($xml_cbc_Name);
                                        $xml_cac_RegistrationAddress->appendChild($xml_cac_Country);
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_RegistrationAddress);

                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "01"));
                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                    $xml_cac_PartyTaxScheme->appendChild($xml_cac_TaxScheme);
                                $xml_cac_Party->appendChild($xml_cac_PartyTaxScheme);

                                $xml_cac_PartyLegalEntity = $xml->createElement("cac:PartyLegalEntity");
                                    $xml_cac_PartyLegalEntity->appendChild($xml->createElement("cbc:RegistrationName", htmlspecialchars($customer_data->name)));

                                    $xml_cbc_CompanyID = $xml->createElement("cbc:CompanyID", $customer_data->vat_no);
                                        if ($customer_data->document_code == NIT) {
                                            $xml_cbc_CompanyID->setAttribute("schemeID", $customer_data->digito_verificacion);
                                        }

                                        $xml_cbc_CompanyID->setAttribute("schemeName", $customer_data->document_code);
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyID", "195");
                                        $xml_cbc_CompanyID->setAttribute("schemeAgencyName", "CO, DIAN (Dirección de Impuestos y Aduanas Nacionales)");
                                    $xml_cac_PartyLegalEntity->appendChild($xml_cbc_CompanyID);
                                $xml_cac_Party->appendChild($xml_cac_PartyLegalEntity);

                                if ($this->Settings->fe_work_environment == TEST) {
                                    $xml_cac_Contact = $xml->createElement("cac:Contact");
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", "Wappsi Innovation"));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Telephone", "301 794 6302"));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", "wappsi.desarrolloweb02@gmail.com"));
                                    $xml_cac_Party->appendChild($xml_cac_Contact);
                                } else {
                                    $xml_cac_Contact = $xml->createElement("cac:Contact");
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Name", htmlspecialchars($customer_data->name)));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:Telephone", $customer_data->phone));
                                        $xml_cac_Contact->appendChild($xml->createElement("cbc:ElectronicMail", $customer_data->email));
                                    $xml_cac_Party->appendChild($xml_cac_Contact);
                                }
                            $xml_cac_AccountingCustomerParty->appendChild($xml_cac_Party);
                        $xml_debit_note->appendChild($xml_cac_AccountingCustomerParty);
                    /**************************************************************/

                    /************************ Medios pago *************************/
                        $xml_cac_PaymentMeans = $xml->createElement("cac:PaymentMeans");
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:ID", $debit_note->payment_method_fe));
                            $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentMeansCode", $debit_note->payment_mean_fe));

                            if ($debit_note->payment_method_fe == CREDIT) {
                                $fecha_creacion = date("Y-m-d", strtotime($debit_note->date));
                                $fecha_vencimiento = date("Y-m-d", strtotime("+". $debit_note->payment_term ." day", strtotime($fecha_creacion)));

                                $xml_cac_PaymentMeans->appendChild($xml->createElement("cbc:PaymentDueDate", $fecha_vencimiento));
                            }
                        $xml_debit_note->appendChild($xml_cac_PaymentMeans);
                    /**************************************************************/

                    /********************** Término de pago ***********************/
                        $xml_cac_PaymentTerms = $xml->createElement("cac:PaymentTerms");
                            $PayableAmount = $debit_note->grand_total;
                            $xml_cbc_Amount = $xml->createElement("cbc:Amount", $PayableAmount);
                            $xml_cac_PaymentTerms->appendChild($xml_cbc_Amount);
                        $xml_debit_note->appendChild($xml_cac_PaymentTerms);
                    /**************************************************************/

                    /********************** Importes totales. *********************/
                        /************************ Impuestos. **********************/
                            $product_tax_iva = $product_tax_ic = $product_tax_inc = $product_tax_bolsas = $total_tax_base = $exempt_existing = 0;

                            $tax_rates = $this->get_tax_rate_by_code_fe();

                            foreach ($tax_rates as $tax_rate) {
                                if ($tax_rate->code_fe == IVA) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_value_iva_array[$index] = 0;
                                    $tax_base_iva_array[$index] = 0;
                                } else if ($tax_rate->code_fe == IC) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_value_ic_array[$index] = 0;
                                    $tax_base_ic_array[$index] = 0;
                                } else if ($tax_rate->code_fe == INC) {
                                    $index = (int) $tax_rate->rate;
                                    $tax_value_inc_array[$index] = 0;
                                    $tax_base_inc_array[$index] = 0;
                                } else if ($tax_rate->rate == BOLSAS) {
                                    $tax_value_bolsas = 0;
                                    $tax_base_bolsas = 0;
                                    $tax_value_bolsa = 0;
                                }
                            }

                            foreach ($items_sale as $item) {
                                if ($item->code_fe == IVA) {
                                    $product_tax_iva += $item->item_tax;

                                    foreach ($tax_rates as $tax_rate) {
                                        if ($item->rate == $tax_rate->rate) {
                                            if ($item->rate == 0) { $exempt_existing++; }

                                            $index = (int) $tax_rate->rate;
                                            $tax_base_iva_array[$index] += $item->net_unit_price * $item->quantity;
                                            $tax_value_iva_array[$index] += $item->item_tax;
                                            $total_tax_base += $item->net_unit_price * $item->quantity;

                                            break;
                                        }
                                    }
                                } else if ($item->code_fe == IC) {
                                    $product_tax_ic += $item->item_tax;

                                    foreach ($tax_rates as $tax_rate) {
                                        if ($item->rate == $tax_rate->rate) {
                                            $index = (int) $tax_rate->rate;
                                            $tax_base_ic_array[$index] += $item->net_unit_price * $item->quantity;
                                            $tax_value_ic_array[$index] += $item->item_tax;
                                            $total_tax_base += $item->net_unit_price * $item->quantity;

                                            break;
                                        }
                                    }
                                } else if ($item->code_fe == INC) {
                                    $product_tax_inc += $item->item_tax;

                                    foreach ($tax_rates as $tax_rate) {
                                        if ($item->rate == $tax_rate->rate) {
                                            $index = (int) $tax_rate->rate;
                                            $tax_base_inc_array[$index] += $item->net_unit_price * $item->quantity;
                                            $tax_value_inc_array[$index] += $item->item_tax;
                                            $total_tax_base += $item->net_unit_price * $item->quantity;

                                            break;
                                        }
                                    }
                                } else if ($item->code_fe == BOLSAS) {
                                    $product_tax_bolsas += $item->item_tax;

                                    $tax_value_bolsas += $item->item_tax;
                                    $tax_base_bolsas += 1;
                                    $tax_value_bolsa = $item->item_tax;

                                    $total_tax_base += $item->net_unit_price;
                                }
                            }

                            if ($product_tax_iva) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_iva;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                        if (!empty($tax_base_iva)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_iva;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_iva_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }
                                $xml_debit_note->appendChild($xml_cac_TaxTotal);
                            } else if (count($items_sale) >= 1 && $exempt_existing > 0) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_iva;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_iva_array as $percentage => $tax_base_iva) {
                                        if (!empty($tax_base_iva)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_iva;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_iva_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IVA));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IVA"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }
                                $xml_debit_note->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_ic) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_ic;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_ic_array as $percentage => $tax_base_ic) {
                                        if (!empty($tax_base_ic)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");

                                                $TaxableAmount = $tax_base_ic;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_ic_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", IC));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "IC"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_debit_note->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_inc) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $product_tax_inc;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    foreach ($tax_base_inc_array as $percentage => $tax_base_inc) {
                                        if (!empty($tax_base_inc)) {
                                            $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                                $TaxableAmount = $tax_base_inc;
                                                $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                                    $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                                $TaxAmount = $tax_value_inc_array[$percentage];
                                                $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                                    $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                                $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                                    $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($percentage, 2)));
                                                    $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", INC));
                                                        $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC"));
                                                    $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                                $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                            $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                        }
                                    }

                                $xml_debit_note->appendChild($xml_cac_TaxTotal);
                            }

                            if ($product_tax_bolsas) {
                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");

                                    $TaxAmount = $product_tax_bolsas;
                                    $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $tax_base_bolsas;
                                        $xml_cac_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                            $xml_cac_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxableAmount);

                                        $TaxAmount = $tax_value_bolsas;
                                        $xml_cac_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                            $xml_cac_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxAmount);

                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:BaseUnitMeasure", 1.00);
                                            $xml_cbc_BaseUnitMeasure->setAttribute("unitCode", "UNI");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $PerUnitAmount = $tax_value_bolsa;
                                        $xml_cbc_BaseUnitMeasure = $xml->createElement("cbc:PerUnitAmount", $this->sma->formatDecimalNoRound($PerUnitAmount, 2));
                                            $xml_cbc_BaseUnitMeasure->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", BOLSAS));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "INC Bolsas"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_BaseUnitMeasure);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_debit_note->appendChild($xml_cac_TaxTotal);
                            }
                        /***********************************************************/

                        /*********************** Retenciones. **********************/
                            if ($debit_note->rete_iva_total > 0) {
                                $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                    $TaxAmount = $debit_note->rete_iva_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $debit_note->rete_iva_base;
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                        $TaxAmount = $debit_note->rete_iva_total;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($debit_note->rete_iva_percentage, 5)));

                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "05"));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteIVA"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_debit_note->appendChild($xml_cac_WithholdingTaxTotal);
                            }

                            if ($debit_note->rete_fuente_total > 0) {
                                $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                    $TaxAmount = $debit_note->rete_fuente_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $debit_note->rete_fuente_base;
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);


                                        $TaxAmount = $debit_note->rete_fuente_total;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($debit_note->rete_fuente_percentage, 5)));

                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "06"));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteFuente"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_debit_note->appendChild($xml_cac_WithholdingTaxTotal);
                            }

                            if ($debit_note->rete_ica_total > 0) {
                                $xml_cac_WithholdingTaxTotal = $xml->createElement("cac:WithholdingTaxTotal");
                                    $TaxAmount = $debit_note->rete_ica_total;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");
                                        $TaxableAmount = $debit_note->rete_ica_base;
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);

                                        $TaxAmount = $debit_note->rete_ica_total;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($debit_note->rete_ica_percentage, 5)));

                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", "07"));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", "ReteICA"));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);

                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);

                                    $xml_cac_WithholdingTaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_debit_note->appendChild($xml_cac_WithholdingTaxTotal);
                            }
                        /***********************************************************/
                    /**************************************************************/

                    /********************** Total impuestos ***********************/
                        $xml_cac_RequestedMonetaryTotal = $xml->createElement("cac:RequestedMonetaryTotal");
                            $LineExtensionAmount = $debit_note->total;
                            $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimalNoRound($LineExtensionAmount, 2));
                                $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                            $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_LineExtensionAmount);

                            $TaxExclusiveAmount = $total_tax_base;
                            $xml_cbc_TaxExclusiveAmount = $xml->createElement("cbc:TaxExclusiveAmount", $this->sma->formatDecimalNoRound($TaxExclusiveAmount, 2));
                                $xml_cbc_TaxExclusiveAmount->setAttribute("currencyID", "COP");
                            $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_TaxExclusiveAmount);

                            $TaxInclusiveAmount = ($debit_note->total + $debit_note->product_tax);
                            $xml_cbc_TaxInclusiveAmount = $xml->createElement("cbc:TaxInclusiveAmount", $this->sma->formatDecimalNoRound($TaxInclusiveAmount, 2));
                                $xml_cbc_TaxInclusiveAmount->setAttribute("currencyID", "COP");
                            $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_TaxInclusiveAmount);

                            $PayableAmount = $debit_note->grand_total;
                            $xml_cbc_PayableAmount = $xml->createElement("cbc:PayableAmount", $this->sma->formatDecimalNoRound($PayableAmount, 2));
                                $xml_cbc_PayableAmount->setAttribute("currencyID", "COP");
                            $xml_cac_RequestedMonetaryTotal->appendChild($xml_cbc_PayableAmount);

                        $xml_debit_note->appendChild($xml_cac_RequestedMonetaryTotal);
                    /**************************************************************/

                    /************************* Productos **************************/
                        $consecutive_product_identifier = 1;
                        foreach ($items_sale as $item) {
                            if ($item->code_fe == IVA) {
                                $tax_name = "IVA";
                            }

                            $xml_cac_DebitNoteLine = $xml->createElement("cac:DebitNoteLine");
                                $xml_cac_DebitNoteLine->appendChild($xml->createElement("cbc:ID", $consecutive_product_identifier));
                                $xml_cbc_DebitedQuantity = $xml->createElement("cbc:DebitedQuantity", $this->sma->formatDecimalNoRound($item->quantity, 2));
                                    $xml_cbc_DebitedQuantity->setAttribute("unitCode", "NIU");
                                $xml_cac_DebitNoteLine->appendChild($xml_cbc_DebitedQuantity);

                                $LineExtensionAmount = ($item->net_unit_price * $item->quantity);
                                $xml_cbc_LineExtensionAmount = $xml->createElement("cbc:LineExtensionAmount", $this->sma->formatDecimalNoRound($LineExtensionAmount, 2));
                                    $xml_cbc_LineExtensionAmount->setAttribute("currencyID", "COP");
                                $xml_cac_DebitNoteLine->appendChild($xml_cbc_LineExtensionAmount);

                                $xml_cac_TaxTotal = $xml->createElement("cac:TaxTotal");
                                    $TaxAmount = $item->item_tax;
                                    $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                        $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_TaxTotal->appendChild($xml_cbc_TaxAmount);

                                    $xml_cac_TaxSubtotal = $xml->createElement("cac:TaxSubtotal");

                                        $TaxableAmount = ($item->net_unit_price * $item->quantity);
                                        $xml_cbc_TaxableAmount = $xml->createElement("cbc:TaxableAmount", $this->sma->formatDecimalNoRound($TaxableAmount, 2));
                                            $xml_cbc_TaxableAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxableAmount);


                                        $TaxAmount = $item->item_tax;
                                        $xml_cbc_TaxAmount = $xml->createElement("cbc:TaxAmount", $this->sma->formatDecimalNoRound($TaxAmount, 2));
                                            $xml_cbc_TaxAmount->setAttribute("currencyID", "COP");
                                        $xml_cac_TaxSubtotal->appendChild($xml_cbc_TaxAmount);

                                        $xml_cac_TaxCategory = $xml->createElement("cac:TaxCategory");
                                            $xml_cac_TaxCategory->appendChild($xml->createElement("cbc:Percent", $this->sma->formatDecimalNoRound($item->rate, 2)));
                                            $xml_cac_TaxScheme = $xml->createElement("cac:TaxScheme");
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:ID", $item->code_fe));
                                                $xml_cac_TaxScheme->appendChild($xml->createElement("cbc:Name", $item->code_fe_name));
                                            $xml_cac_TaxCategory->appendChild($xml_cac_TaxScheme);
                                        $xml_cac_TaxSubtotal->appendChild($xml_cac_TaxCategory);
                                    $xml_cac_TaxTotal->appendChild($xml_cac_TaxSubtotal);
                                $xml_cac_DebitNoteLine->appendChild($xml_cac_TaxTotal);

                                $xml_cac_Item = $xml->createElement("cac:Item");
                                    $xml_cac_Item->appendChild($xml->createElement("cbc:Description", $item->product_name));

                                    $xml_cac_StandardItemIdentification = $xml->createElement("cac:StandardItemIdentification");
                                        $xml_cbc_ID = $xml->createElement("cbc:ID", $item->product_code);
                                            $xml_cbc_ID->setAttribute("schemeID", "999");
                                        $xml_cac_StandardItemIdentification->appendChild($xml_cbc_ID);
                                    $xml_cac_Item->appendChild($xml_cac_StandardItemIdentification);
                                $xml_cac_DebitNoteLine->appendChild($xml_cac_Item);

                                $xml_cac_Price = $xml->createElement("cac:Price");
                                    $PriceAmount = $item->net_unit_price;
                                    $xml_cbc_PriceAmount = $xml->createElement("cbc:PriceAmount", $this->sma->formatDecimalNoRound($PriceAmount, 2));
                                        $xml_cbc_PriceAmount->setAttribute("currencyID", "COP");
                                    $xml_cac_Price->appendChild($xml_cbc_PriceAmount);

                                    $xml_cbc_BaseQuantity = $xml->createElement("cbc:BaseQuantity", $this->sma->formatDecimalNoRound($item->quantity, 2));
                                        $xml_cbc_BaseQuantity->setAttribute("unitCode", "NIU");
                                    $xml_cac_Price->appendChild($xml_cbc_BaseQuantity);
                                $xml_cac_DebitNoteLine->appendChild($xml_cac_Price);
                            $xml_debit_note->appendChild($xml_cac_DebitNoteLine);

                            $consecutive_product_identifier++;
                        }
                    /**************************************************************/

                    /************************* DATA *******************************/
                        if ($this->Settings->fe_technology_provider == CADENA) {
                            $xml_DATA = $xml->createElement("DATA");
                                $xml_DATA->appendChild($xml->createElement("UBL21", "true"));

                                $xml_Partnership = $xml->createElement("Partnership");
                                    $xml_Partnership->appendChild($xml->createElement("ID", "901090070"));
                                    $xml_Partnership->appendChild($xml->createElement("TechKey", $resolution_data->clave_tecnica));
                                    if ($this->Settings->fe_work_environment == TEST) {
                                        $xml_Partnership->appendChild($xml->createElement("SetTestID", $resolution_data->fe_testid));
                                    }
                                $xml_DATA->appendChild($xml_Partnership);
                            $xml_debit_note->appendChild($xml_DATA);
                        }
                    /**************************************************************/
                $xml_Document->appendChild($xml_debit_note);
            $xml->appendChild($xml_Document);

            if (!$print_xml) { return base64_encode($xml->saveXML()); }

            echo $xml->saveXML();
        }

        private function generate_CUFE_DELCOP($sale, $items_sale, $issuer_data, $customer_data, $resolution_data, $decrypt = FALSE)
        {
            $IVA_value = 0.00;
            $INC_value = 0.00;
            $ICA_value = 0.00;
            $IC_value = 0.00;
            $BOLSA_value = 0.00;
            $product_tax_iva = $subtotal = $total_tax = 0.00 ;

            foreach ($items_sale as $item) {
                $net_unit_price = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

                $subtotal += $this->sma->formatDecimal($net_unit_price * $quantity, NUMBER_DECIMALS);
                $total_tax += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                if ($item->code_fe == "01") {
                    $IVA_value += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);
                } else if ($item->code_fe == "04") {
                    $INC_value += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);
                } else if ($item->code_fe == "03") {
                    $ICA_value += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);
                } else if ($item->code_fe == "22") {
                    $BOLSA_value += $this->sma->formatDecimal($quantity * $item->rate, NUMBER_DECIMALS);
                }

                if ($item->consumption_sales > 0) {
                    $IC_value += $this->sma->formatDecimal($item->consumption_sales * $quantity, NUMBER_DECIMALS);
                }
            }

            $SUBTOTAL = $this->sma->formatDecimal($subtotal, NUMBER_DECIMALS);
            $IVA = $this->sma->formatDecimal($IVA_value, NUMBER_DECIMALS);
            $INC = $this->sma->formatDecimal($INC_value, NUMBER_DECIMALS);
            $ICA = $this->sma->formatDecimal($ICA_value, NUMBER_DECIMALS);
            $TOTAL = ($this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($total_tax, NUMBER_DECIMALS) + $IC_value + $BOLSA_value + $sale->shipping - $sale->order_discount);
            $Work_environment = ($this->Settings->fe_work_environment == PRODUCTION) ? PRODUCTION : TEST;
            $Pin = $resolution_data->clave_tecnica;

            if ($decrypt == FALSE) {
                $CUFE_code_generated = hash('sha384',
                    str_replace('-', '', $sale->reference_no).
                    $this->format_date($sale->date)->date.
                    $this->format_date($sale->date)->time.
                    $this->sma->formatDecimalNoRound($SUBTOTAL, 2).
                    "01".
                    $this->sma->formatDecimalNoRound($IVA, 2).
                    "04".
                    $this->sma->formatDecimalNoRound($INC, 2).
                    "03".
                    $this->sma->formatDecimalNoRound($ICA, 2).
                    $this->sma->formatDecimalNoRound($TOTAL, 2).
                    str_replace("-", "", $issuer_data->numero_documento).
                    str_replace('-', '', $customer_data->vat_no).
                    $Pin.
                    $Work_environment
                );
            } else {
                $CUFE_code_generated =
                    str_replace('-', '', $sale->reference_no)." ".
                    $this->format_date($sale->date)->date." ".
                    $this->format_date($sale->date)->time." ".
                    $this->sma->formatDecimalNoRound($SUBTOTAL, 2)." ".
                    "01"." ".
                    $this->sma->formatDecimalNoRound($IVA, 2)." ".
                    "04"." ".
                    $this->sma->formatDecimalNoRound($INC, 2)." ".
                    "03"." ".
                    $this->sma->formatDecimalNoRound($ICA, 2)." ".
                    $this->sma->formatDecimalNoRound($TOTAL, 2)." ".
                    str_replace("-", "", $issuer_data->numero_documento)." ".
                    str_replace('-', '', $customer_data->vat_no)." ".
                    $Pin." ".
                    $Work_environment;
            }

            $string_CUFE = "NumeroFactura: ".str_replace('-', '', $sale->reference_no)."\n".
                "FechaFactura: ". $this->format_date($sale->date)->date." ". $this->format_date($sale->date)->time ."\n".
                "ValorFactura: ". $this->sma->formatDecimalNoRound($SUBTOTAL, 2)."\n".
                "ValorIVA: ". $this->sma->formatDecimalNoRound($total_tax, 2)."\n".
                "ValorTotalFactura: ". $this->sma->formatDecimalNoRound($TOTAL, 2)."\n".
                "NitEmisor: ". str_replace("-", "", $issuer_data->numero_documento)."\n".
                "DocumentoAdquiriente: ". str_replace('-', '', $customer_data->vat_no)."\n".
                "CUFE: ". $CUFE_code_generated;

            if ($decrypt == FALSE) {
                if ($this->Settings->fe_technology_provider != DELCOP) {
                    $this->site->updateSale(["cufe"=>$CUFE_code_generated, "codigo_qr"=>$string_CUFE], $sale->id);
                }
            }

            return $CUFE_code_generated;
        }

        private function generate_CUDE_DELCOP($credit_note, $items_sale, $issuer_data, $customer_data, $resolution_data, $decrypt = FALSE)
        {
            $IVA_value = 0.00;
            $INC_value = 0.00;
            $ICA_value = 0.00;
            $IC_value = 0.00;
            $BOLSA_value = 0.00;
            $product_tax_iva = $total_base_round = $subtotal = $total_tax = 0.00;

            foreach ($items_sale as $item) {
                $net_unit_price = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);

                $subtotal += $this->sma->formatDecimal($net_unit_price * $quantity, NUMBER_DECIMALS);
                $total_tax += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);

                if ($item->code_fe == "01") {
                    $IVA_value += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);
                } else if ($item->code_fe == "04") {
                    $INC_value += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);
                } else if ($item->code_fe == "03") {
                    $ICA_value += $this->sma->formatDecimal(($net_unit_price * $quantity) * ($item->rate / 100), NUMBER_DECIMALS);
                } else if ($item->code_fe == "22") {
                    $BOLSA_value += $this->sma->formatDecimal($this->sma->formatDecimal($item->quantity * $item->rate, NUMBER_DECIMALS), NUMBER_DECIMALS);
                }

                if ($item->consumption_sales > 0) {
                    $IC_value += $this->sma->formatDecimal($item->consumption_sales * $item->quantity, NUMBER_DECIMALS);
                }
            }

            $SUBTOTAL = $this->sma->formatDecimal($subtotal, NUMBER_DECIMALS);
            $IVA = $this->sma->formatDecimal($IVA_value, NUMBER_DECIMALS);
            $INC = $this->sma->formatDecimal($INC_value, NUMBER_DECIMALS);
            $ICA = $this->sma->formatDecimal($ICA_value, NUMBER_DECIMALS);
            $shipping = $this->sma->formatDecimal($credit_note->shipping, NUMBER_DECIMALS);
            $order_discount = $this->sma->formatDecimal($credit_note->order_discount, NUMBER_DECIMALS);

            $TOTAL = $this->sma->formatDecimal($subtotal, NUMBER_DECIMALS) + $this->sma->formatDecimal($total_tax, NUMBER_DECIMALS) + $ICA_value + $BOLSA_value + $shipping - $order_discount;
            $Work_environment = ($this->Settings->fe_work_environment == PRODUCTION) ? PRODUCTION : TEST;

            if ($decrypt == FALSE) {
                $CUDE_code_generated = hash('sha384',
                    str_replace('-', '', $credit_note->reference_no).
                    $this->format_date($credit_note->date)->date.
                    $this->format_date($credit_note->date)->time.
                    $this->sma->formatDecimalNoRound(abs($SUBTOTAL), 2).
                    "01".
                    $this->sma->formatDecimalNoRound(abs($IVA), 2).
                    "04".
                    $this->sma->formatDecimalNoRound(abs($INC), 2).
                    "03".
                    $this->sma->formatDecimalNoRound(abs($ICA), 2).
                    $this->sma->formatDecimalNoRound(abs($TOTAL), 2).
                    str_replace("-", "", $issuer_data->numero_documento).
                    str_replace('-', '', $customer_data->vat_no).
                    "75315".
                    $Work_environment
                );
            } else {
                $CUDE_code_generated =
                    str_replace('-', '', $credit_note->reference_no)." ".
                    $this->format_date($credit_note->date)->date." ".
                    $this->format_date($credit_note->date)->time." ".
                    $this->sma->formatDecimalNoRound(abs($SUBTOTAL), 2)." ".
                    "01"." ".
                    $this->sma->formatDecimalNoRound(abs($IVA), 2)." ".
                    "04"." ".
                    $this->sma->formatDecimalNoRound(abs($INC), 2)." ".
                    "03"." ".
                    $this->sma->formatDecimalNoRound(abs($ICA), 2)." ".
                    $this->sma->formatDecimalNoRound(abs($TOTAL), 2)." ".
                    str_replace("-", "", $issuer_data->numero_documento)." ".
                    str_replace('-', '', $customer_data->vat_no)." ".
                    "75315"." ".
                    $Work_environment;
            }

            $string_CUDE = "NumeroFactura: ".str_replace('-', '', $credit_note->reference_no)."\n".
                "FechaFactura: ". $this->format_date($credit_note->date)->date." ". $this->format_date($credit_note->date)->time ."\n".
                "ValorFactura: ". $this->sma->formatDecimalNoRound(abs($SUBTOTAL), 2)."\n".
                "ValorIVA: ". ($this->sma->formatDecimalNoRound(abs($total_tax), 2))."\n".
                "ValorTotalFactura: ". $this->sma->formatDecimalNoRound(abs($TOTAL), 2)."\n".
                "NitEmisor: ". str_replace("-", "", $issuer_data->numero_documento)."\n".
                "DocumentoAdquiriente: ". str_replace('-', '', $customer_data->vat_no)."\n".
                "CUDE: ". $CUDE_code_generated;

            if ($decrypt == FALSE) {
                if ($this->Settings->fe_technology_provider != DELCOP) {
                    $this->site->updateSale(["cufe"=>$CUDE_code_generated, "codigo_qr"=>$string_CUDE], $credit_note->id);
                }
            }

            return $CUDE_code_generated;
        }

        private function getConceptCorrectionNote($document, $reference)
        {
            if (abs($document->grand_total) == abs($reference->grand_total)) {
                $conceptCorrection = $this->site->get_debit_credit_note_concept_by_id(2);
            } else {
                $conceptCorrection = $this->site->get_debit_credit_note_concept_by_id(1);
            }

            return (object) ["code" => $conceptCorrection->dian_code, "description" => $conceptCorrection->dian_name];
        }

        public function delcopGetAuthorizationoken()
        {
            $url = ($this->Settings->fe_work_environment == TEST) ? "https://www-prueba.titanio.com.co/PDE/public/api/auth/signin" : "https://www.titanio.com.co/PDE/public/api/auth/signin";

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>'{"NIT": '.$this->Settings->numero_documento.', "usuario": "'.$this->Settings->fe_user.'", "password": "'.$this->Settings->fe_password.'"}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            curl_close($curl);
            return json_decode($response);
        }

        public function delcopWSSendDocument($xml_file, $electronic_document_data, $token)
        {
            $sale = $this->site->getSaleByID($electronic_document_data->sale_id);

            $this->site->updateSale(['webservices_consumption_start_date' => date("Y-m-d H:i:s")], $electronic_document_data->sale_id);

            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $technologyProviderConfiguration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $technologyProviderConfiguration->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>'{"token": "'.$token.'", "tr_tipo_id": "'.$resolution_data->fe_transaction_id.'", "data": "'.$xml_file.'"}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            log_message('debug','DELCOP - envio: '. $response);

            curl_close($curl);
            $response = json_decode($response);

            if ($response->error_id == 0) {
                $sale_data['fe_mensaje_soporte_tecnico'] = "Error id: ". $response->error_id . ". Transacción id: ". $response->tr_id;
                $sale_data['fe_id_transaccion'] = $response->tr_id;
                $sale_data['cufe'] = $response->cufe;
                $sale_data['codigo_qr'] = $response->qr;
                $sale_data['webservices_consumption_end_date'] = date("Y-m-d H:i:s");
                if ($response->tr_id > 0) {
                    $sale_data["fe_aceptado"] = 3;
                } else {
                    $sale_data["fe_aceptado"] = 1;
                }

                $hitsSaved = $this->site->saveHits($sale->id, ELECTRONIC_BILL, $sale_data['fe_mensaje'], SUCCESS, $sale->date);

                $updated_sale = $this->site->updateSale($sale_data, $electronic_document_data->sale_id);

                sleep(8);

                $internal_response = $this->document_status($electronic_document_data->sale_id);
            } else {
                if ($sale->fe_aceptado != 2) {
                    $sale_data["fe_aceptado"] = 1;
                    $sale_data['fe_mensaje_soporte_tecnico'] = "Error id: ". $response->error_id. ". Transacción id: ". $response->tr_id. ". ".$response->mensaje;
                    $sale_data['webservices_consumption_end_date'] = date("Y-m-d H.i:S");
                    $updated_sale = $this->site->updateSale($sale_data, $electronic_document_data->sale_id);

                    if (!empty($response->tr_id)) {
                        sleep(8);
                        $internal_response = $this->document_status($electronic_document_data->sale_id);
                    } else {
                        $internal_response = [
                            'response' => FALSE,
                            'message' => "El documento no se ha podido procesar"
                        ];
                    }
                } else if ($sale->fe_aceptado == 2) {
                    $internal_response = [
                        'response' => FALSE,
                        'message' => "El documento ya ha sido enviado a la DIAN"
                    ];
                }
            }

            return (object) $internal_response;
        }

        private function document_status($document_id)
        {
            $sale = $this->site->getSaleByID($document_id);
            // $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);

            $response_token = $this->delcopGetAuthorizationoken();

            if ($response_token->success == FALSE) {
                $response = [
                    'response' => FALSE,
                    'message' => "Los campos de Usuario o Contraseña DELCOP no son correctos. ". $response_token->error
                ];

                return (object) $response;
            }

            if ($this->Settings->fe_work_environment == TEST) {
                $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/detalle";
            } else {
                $url = "https://www.titanio.com.co/PDE/public/api/PDE/detalle";
            }

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS =>'{"token": "'. $response_token->token .'", "transaccion_id": "'. $sale->fe_id_transaccion .'"}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json"
                ),
            ));

            $response = curl_exec($curl);
            log_message('debug','DELCOP - consulta estado: '. $response);
            $response = json_decode($response);

            curl_close($curl);

            if ($response->error_id == 0) {
                if ($response->transaccion_id != 0) {
                    $document_status = $response->detalleTransaccion->estado_id;
                    $acepted_DIAN = strstr($document_status, 'Validado por la DIAN');
                    $rejected_DIAN = strstr($document_status, 'Rechazado por la DIAN');
                    $sent_DIAN = strstr($document_status, 'Enviado a la DIAN');
                    $validated_DELCOP = strstr($document_status, 'Validado por la DIAN');
                    $transaction_received = strstr($document_status, 'Transacción recibida,');

                    if ($rejected_DIAN == "Rechazado por la DIAN") {
                        $update = $this->site->updateSale(["fe_aceptado"=>1, "fe_mensaje"=>"Transacción con errores detectados en la DIAN.", "fe_mensaje_soporte_tecnico"=>$response->error], $sale->id);
                        $internal_response = [
                            'response' => FALSE,
                            'message' => "Rechazado por la DIAN"
                        ];
                    } else if ($acepted_DIAN != "") {
                        $update = $this->site->updateSale(["fe_aceptado"=>2, "fe_mensaje"=>"Documento aceptado por la DIAN", "fe_mensaje_soporte_tecnico"=>""], $sale->id);
                        $internal_response = [
                            'response' => TRUE,
                            'message' => "Documento aceptado por la DIAN"
                        ];
                    } else if ($sent_DIAN != "") {
                        $update = $this->site->updateSale(["fe_aceptado"=>3, "fe_mensaje"=>"Documento aceptado y enviado a la DIAN", "fe_mensaje_soporte_tecnico"=>""], $sale->id);
                        $internal_response = [
                            'response' => TRUE,
                            'message' => "Documento aceptado y validado por DELCOP. En espera de respuesta de la DIAN."
                        ];
                    } else if ($validated_DELCOP != "") {
                        $update = $this->site->updateSale(["fe_aceptado"=>3, "fe_mensaje"=>"Documento aceptado y validado por DELCOP", "fe_mensaje_soporte_tecnico"=>""], $sale->id);
                        $internal_response = [
                            'response' => TRUE,
                            'message' => "Documento aceptado y validado por DELCOP. En espera de respuesta de la DIAN."
                        ];
                    } else if ($transaction_received != ""){
                        if ($response->error != ""){
                            $update = $this->site->updateSale(["fe_aceptado"=>1, "fe_mensaje"=>"Transacción recibida por DELCOP con posibles errores.", "fe_mensaje_soporte_tecnico"=>$response->error], $sale->id);
                            $internal_response = [
                                'response' => FALSE,
                                'message' => "Transacción recibida por DELCOP con posibles errores."
                            ];
                        } else {
                            $update = $this->site->updateSale(["fe_aceptado"=>3, "fe_mensaje"=>"Transacción recibida por DELCOP.", "fe_mensaje_soporte_tecnico"=>"Proceso de validación DELCOP pendiente"], $sale->id);
                            $internal_response = [
                                'response' => TRUE,
                                'message' => "Transacción recibida por DELCOP."
                            ];
                        }
                    }
                } else {
                    $update = $this->site->updateSale(["fe_aceptado"=>1, "fe_mensaje"=>"No existe registro del Documento en DELCOP"], $sale->id);
                    $internal_response = [
                        'response' => FALSE,
                        'message' => "No existe registro del Documento en DELCOP"
                    ];
                }
            } else {
                $update = $this->site->updateSale(["fe_mensaje"=>"Error al consumir: Estado del documentos en DELCOP"], $sale->id);
                $internal_response = [
                    'response' => FALSE,
                    'message' => "Error al consumir: Estado del documentos en DELCOP"
                ];
            }

            return $internal_response;
        }
    /****************************************************************/

    /****************************** BPM *****************************/
        public function buildJsonInvoice($documentElectronic)
        {
            $document = $this->site->getSaleByID($documentElectronic->sale_id);

            $json = [
                "TipoDocumento"         => $this->getDocumentTypeCode($document),
                "TipoConcepto"          => $this->getConceptType($document),
                "TipoOperacion"         => "10",
                "TipoNegociacion"       => $this->getPaymentMethod($document),
                "MedioPago"             => $this->getPaymentMean($document),
                "FechaExpedicion"       => $document->date,
                "FechaVencimiento"      => $this->getDueDate($document),
                "TotalImpuestos"        => floatval($this->getTotals($document, 'totalTax')),
                "TotalRetenciones"      => floatval($this->getTotalWithholdings($document)),
                "Retenciones"           => $this->getWithholdings($document),
                "InfoAdicional"         => $document->note,
                "Adquiriente"           => $this->getAcquirer($document),
                "Detalle"               => $this->getDetails($document)
            ];

            if (!empty($document->fe_generatedDocument)) {
                $json["DocumentoEditar"] = $document->fe_generatedDocument;
            }

            $json = $this->getTrm($json, $document);
            $json = $this->getCharge($json, $document);
            $json = $this->getDiscount($json, $document);
            $json = $this->getAdvances($json, $document);
            $json = $this->getResolution($json, $document);
            $json = $this->getExternalInvoice($json, $document);
            $json = $this->getMonthToAffect($json, $document);

            return json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        private function getDocumentTypeCode($document)
        {
            $typeElectronicDocument = $this->site->getTypeElectronicDocument($document->id);
            if ($typeElectronicDocument == INVOICE) {
                $typeDocument = 1;
            } else if ($typeElectronicDocument == CREDIT_NOTE) {
                $typeDocument = 3;
            } else if ($typeElectronicDocument == DEBIT_NOTE) {
                $typeDocument = 2;
            }
            return $typeDocument;
        }

        private function getConceptType($document)
        {
            $conceptType = "";
            $referenceDocument = $this->site->getSaleByID($document->sale_id);
            $typeElectronicDocument = $this->site->getTypeElectronicDocument($document->id);

            if ($typeElectronicDocument == CREDIT_NOTE) {
                if (!empty($document->fe_debit_credit_note_concept_dian_code)) {
                    $conceptType = $document->fe_debit_credit_note_concept_dian_code;
                } else {
                    if (abs($referenceDocument->grand_total) == abs($document->grand_total)) {
                        $conceptType = 2;
                    } else {
                        $conceptType = 1;
                        if (!empty($document->fe_debit_credit_note_concept_dian_code)) {
                            $conceptType = $document->fe_debit_credit_note_concept_dian_code;
                        }
                    }
                }
            } else if ($typeElectronicDocument == DEBIT_NOTE) {
                if (!empty($document->fe_debit_credit_note_concept_dian_code)) {
                    $conceptType = $document->fe_debit_credit_note_concept_dian_code;
                }
            }

            return $conceptType;
        }

        private function getPaymentMethod($document)
        {
            $payments = $this->site->getSalePayments($document->id);
            $paymentAmount = 0;

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    $paymentAmount += $payment->amount;
                }
            }

            return (abs($document->grand_total) == abs($paymentAmount)) ? CASH : CREDIT;
        }

        private function getPaymentMean($document)
        {
            $paymentMeanCode = "ZZZ";
            $paymentsWithoutWithholdings = [];
            $payments = $this->site->getSalePayments($document->id);

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($payment->paid_by != 'retencion') {
                        $paymentsWithoutWithholdings[] = $payment;
                    }
                }
            }

            if (!empty($paymentsWithoutWithholdings)) {
                foreach ($paymentsWithoutWithholdings as $payment) {
                    if (!empty($payment->mean_payment_code_fe)) {
                        $paymentMeanCode = $payment->mean_payment_code_fe;
                    }
                }
            }

            return $paymentMeanCode;
        }

        private function getDueDate($document)
        {
            $hours = date("H:i:s", strtotime($document->date));
            $paymentMethod = $this->getPaymentMethod($document);

            return ($paymentMethod == CREDIT) ? $document->due_date ." ". $hours: $document->date;
        }

        private function getTotals($document, $name)
        {
            $subtotal = 0;
            $totalTax = 0;
            $items = $this->site->getAllSaleItems($document->id);

            foreach ($items as $item) {
                $subtotal += $this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS);
                $totalTax += $this->sma->formatDecimal(($this->sma->formatDecimal($item->net_unit_price * $item->quantity, NUMBER_DECIMALS) * $item->rate) / 100, NUMBER_DECIMALS);
            }

            return $this->sma->formatDecimal(abs($$name), NUMBER_DECIMALS);
        }

        private function getTotalWithholdings($document)
        {
            $withHoldings = $document->rete_fuente_total + $document->rete_iva_total + $document->rete_ica_total;
            return $this->sma->formatDecimal($withHoldings, NUMBER_DECIMALS);
        }

        private function getWithholdings($document)
        {
            $withHoldings = [];

            if (!empty(floatval($document->rete_iva_total))) {
                $withHoldings[] = [
                    "Codigo"=> "05",
                    "Valor" => floatval($document->rete_iva_percentage),
                    "Total" => floatval($document->rete_iva_total)
                ];
            }

            if (!empty(floatval($document->rete_fuente_total))) {
                $withHoldings[] = [
                    "Codigo"=> "06",
                    "Valor" => floatval($document->rete_fuente_percentage),
                    "Total" => floatval($document->rete_fuente_total)
                ];
            }

            if (!empty(floatval($document->rete_ica_total))) {
                // $taxRate = $this->site->getTaxRateByID($document->rete_ica_id);
                $withHoldings[] = [
                    "Codigo"=> "07",
                    "Valor" => floatval($document->rete_ica_percentage),
                    "Total" => floatval($document->rete_ica_total)
                ];
            }

            return $withHoldings;
        }

        private function getAcquirer($document)
        {
            $customer = $this->site->getCompanyByID($document->customer_id);
            $typeRegimen = $this->site->get_types_vat_regime($customer->tipo_regimen);
            $typesObligations = $this->site->getTypesCustomerObligations($customer->id, ACQUIRER);
            $obligations = $this->getObligations($typesObligations);

            $acquirer = [
                "TipoIdentificacion"=> $customer->document_code,
                "TipoOrganizacion"  => $customer->tipo_regimen,
                "TipoRegimen"       => $typeRegimen->codigo,
                "ResponsabilidadesFiscales" => $obligations,
                "Identificacion"    => $customer->vat_no,
                "Nombre"            => htmlspecialchars($customer->name),
                "Apellido"          => "",
                "CorreoElectronico" => "",
                "Telefono"          => $customer->phone,
                "Direccion"         => $customer->address
            ];

            if ($customer->codigo_iso == "CO") {
                $acquirer["Ciudad"] = substr($customer->city_code, -5);
            }

            if ($document->sale_currency == "COP") {
                $acquirer["Pais"] = $customer->codigo_iso;
                $acquirer["NombreEstado"] = ucfirst(mb_strtolower($customer->state));
                $acquirer["NombreCiudad"] = ucfirst(mb_strtolower($customer->city));
            }

            return $acquirer;
        }

        private function getObligations($typesObligations)
        {
            $obligations = [];
            foreach ($typesObligations as $typesObligation) {
                $obligations[]['Codigo'] = $typesObligation->types_obligations_id;
            }
            return $obligations;
        }

        private function getDetails($document)
        {
            $items = $this->site->getAllSaleItems($document->id);
            $details = [];
            foreach ($items as $item) {
                $quantity = $this->sma->formatDecimal($item->quantity, NUMBER_DECIMALS);
                $netUnitPrice = $this->sma->formatDecimal($item->net_unit_price, NUMBER_DECIMALS);
                $subtotal = $this->sma->formatDecimal($quantity * $netUnitPrice, NUMBER_DECIMALS);
                $totalTax = $this->sma->formatDecimal(($subtotal * $item->rate) / 100, NUMBER_DECIMALS);

                $details[] = [
                    "Codigo"            => $item->product_code,
                    "Descripcion"       => htmlspecialchars(str_replace('"', '', $item->product_name)),
                    "Cantidad"          => floatval(abs($quantity)),
                    "Valor"             => floatval(abs($netUnitPrice)),
                    "TotalImpuestos"    => floatval(abs($totalTax)),
                    "TotalRetenciones"  => 0,
                    "Total"             => floatval(abs($this->sma->formatDecimal(($subtotal + $totalTax), NUMBER_DECIMALS))),
                    "Impuestos"         => [[
                        "Codigo"    => $item->code_fe,
                        "Valor"     => floatval($this->sma->formatDecimal($item->rate, NUMBER_DECIMALS)),
                        "Total"     => floatval(abs($totalTax))
                    ]]
                ];
            }
            return $details;
        }

        private function getTrm($json, $document)
        {
            if ($document->sale_currency != "COP") {
                $json["TasaMoneda"] = $document->sale_currency;
                $json["TasaValor"] = $document->sale_currency_trm;
                $json["TasaFecha"] = $document->date;
            }

            return $json;
        }

        private function getCharge($json, $document)
        {
            $chargePercentage = 0;

            if ($document->shipping > 0) {
                $chargePercentage = ($document->shipping * 100) / ($this->getTotals($document, "subtotal") + $this->getTotals($document, "totalTax"));

                $json["RecargoGeneralPorcentaje"] = floatval($this->sma->formatDecimal($chargePercentage, NUMBER_DECIMALS));
                $json["RecargoGeneralMotivo"] = "Recargo genral factura";
            }

            return $json;
        }

        private function getDiscount($json, $document)
        {
            if ($document->order_discount > 0) {
                $json["DescuentoGeneralValor"] = floatval($this->sma->formatDecimal($document->order_discount_id, NUMBER_DECIMALS));
                $json["DescuentoGeneralMotivo"] = "Descuento general factura";
            }

            return $json;
        }

        private function getAdvances($json, $document)
        {
            $payments = $this->site->getSalePayments($document->id);

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($payment->paid_by == 'deposit') {
                        $json["Anticipos"][] = [
                            "FechaHora" => $payment->date,
                            "Valor" => $this->sma->formatDecimal(abs($payment->amount), NUMBER_DECIMALS),
                            "Comentario" => $payment->note,
                        ];
                    }
                }
            }

            return $json;
        }

        private function getResolution($json, $document)
        {
            $typeElectronicDocument = $this->site->getTypeElectronicDocument($document->id);

            if ($this->Settings->fe_work_environment == TEST) {
                $resolutions = $this->bpmWsGetResolutions($typeElectronicDocument);

                if (!empty($resolutions) && $resolutions->Data->Summary->Success = YES) {
                    $resolutionsData = $resolutions->Data->DataResult;
                    $this->amountResolutions = count($resolutionsData);

                    foreach ($resolutionsData as $resolutionData) {
                        if ($typeElectronicDocument == INVOICE) {
                            $documentoRelacionado = $resolutionData->Prefijo.($resolutionData->UltimoConsecutivoGenerado + 1);
                        } else if ($typeElectronicDocument == CREDIT_NOTE) {
                            $externalInvoiceReference = $document->external_invoice_reference;
                            if (!empty($externalInvoiceReference)) {
                                $documentoRelacionado = $externalInvoiceReference;
                            } else {
                                $referenceDocument = $this->site->getSaleByID($document->sale_id);
                                $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                            }

                        } else if ($typeElectronicDocument == DEBIT_NOTE) {
                            $referenceDocument = $this->site->getSaleByID($document->reference_invoice_id);
                            $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                        }

                        $documentoConsecutivo = ($resolutionData->UltimoConsecutivoGenerado + 1);
                    }
                }
            } else {

                if ($typeElectronicDocument == INVOICE) {
                    $documentoRelacionado = str_replace("-", "", $document->reference_no);
                } else if ($typeElectronicDocument == CREDIT_NOTE) {
                    $externalInvoiceReference = $document->external_invoice_reference;
                    $documentWithoutReference = $document->document_without_reference;
                    if (empty($externalInvoiceReference) && empty($documentWithoutReference)) {
                        $referenceDocument = $this->site->getSaleByID($document->sale_id);
                        $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                    }
                } else if ($typeElectronicDocument == DEBIT_NOTE) {
                    $referenceDocument = $this->site->getSaleByID($document->reference_invoice_id);
                    $documentoRelacionado = $referenceDocument->fe_generatedDocument;
                }

                $documentoConsecutivo = explode("-", $document->reference_no)[1];

                if ($this->amountResolutions > 1) {
                    $resolutionWappsi = $this->site->getDocumentTypeById($document->document_type_id);
                    $json["Resolucion"]["Numero"]= $resolutionWappsi->num_resolucion;
                    $json["Resolucion"]["Prefijo"]= $resolutionWappsi->sales_prefix;
                }
            }

            if (isset($documentoRelacionado) && !empty($documentoRelacionado)) {
                $json["DocumentoRelacionado"] = $documentoRelacionado;
            }

            $json["DocumentoConsecutivo"] = $documentoConsecutivo;

            return $json;
        }

        public function bpmWsGetResolutions($typeElectronicDocument)
        {
            $technologyProviderConfiguration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

            if ($typeElectronicDocument == INVOICE) {
                $tipoDocumento = 'FA';
            } else if ($typeElectronicDocument == DEBIT_NOTE) {
                $tipoDocumento = 'ND';
            } else {
                $tipoDocumento = 'NC';
            }

            $curl = curl_init();

            curl_setopt_array($curl, [
                CURLOPT_URL => $technologyProviderConfiguration->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>'{
                    "Controller": "Documentos",
                    "Action" : "GetNumeracion",
                    "ParamValues" : {
                        "TokenIdentificador": "'. $this->Settings->security_token_bpm .'",
                        "TipoDocumento": "'. $tipoDocumento .'"
                    }
                }',
                CURLOPT_HTTPHEADER => [ 'Content-Type: application/json' ],
            ]);

            $response = json_decode(curl_exec($curl));

            curl_close($curl);

            return (object) $response;
        }

        private function getExternalInvoice($json, $document)
        {
            $externalInvoice = [];
            if (!empty($document->external_invoice_reference)) {
                $externalInvoice["DocFacturaElectronica"] = $document->external_invoice_reference;
                $externalInvoice["CUFE"] = $document->external_invoice_cufe;
                $externalInvoice["FechaExpedicion"] = $document->external_invoice_date;

                $json["FacturaExterna"] = $externalInvoice;
            }

            return $json;
        }

        private function getMonthToAffect($json, $document)
        {
            if (!empty($document->document_without_reference)) {
                $typeElectronicDocument = $this->site->getTypeElectronicDocument($document->id);

                if ($typeElectronicDocument == CREDIT_NOTE || $typeElectronicDocument == DEBIT_NOTE) {
                    $json["MesAfectarNota"] = date("Y-m", strtotime($document->date));
                }
            }

            return $json;
        }

        public function bpmWSSendDocument($json, $document, $typeDocument)
        {
            $technologyProvider = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
            $securityToken = $this->getSecurityTokenBPM($document, $typeDocument);

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $technologyProvider->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
                    "Controller"    : "Facturas",
                    "Action"        : "SendDocument",
                    "ParamValues"   : {
                        "TokenIdentificador": "'.$securityToken.'",
                        "DocumentoObject"   : '.$json.',
                        "ObtenerXml"        : true
                    }
                }',
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]);
            $response = curl_exec($curl);
            curl_close($curl);

            log_message('debug', 'FE - BPM envío documento: '. $response);

            return json_decode($response, false);
        }

        private function getSecurityTokenBPM(object $document, int $documentType)
        {
            if ($documentType == CREDIT_NOTE || $documentType == DEBIT_NOTE) {
                $resolution = $this->site->getDocumentTypeById($document->document_type_id);
            } else {
                $resolution = $this->site->getDocumentTypeById($document->document_type_id);
            }

            $intance = $this->site->getInstanceById($resolution->electronic_billing_instance_id);

            return $intance->security_token;
        }

        public function manageSendResponse($response, $eDocument)
        {
            $document = $this->site->getSaleByID($eDocument->sale_id);

            $summary = $response->Data->Summary;

            if ($summary->Success == YES) {
                $dataResult = $response->Data->DataResult[0];
                if ($dataResult->DocumentoEstadoDianIdentificador == 7200002) {
                    $data = [
                        "fe_aceptado"   => ACCEPTED,
                        "cufe"          => $dataResult->DocumentoCufe,
                        "codigo_qr"     => $dataResult->DocumentoQr,
                        "fe_mensaje"    => "Documento aceptado por la DIAN",
                        "fe_mensaje_soporte_tecnico" => "La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada.",
                        "fe_xml"        => $dataResult->InfoXML->AttachedDocumentArchivoBase64,
                        "fe_generatedDocument"  => $dataResult->DocumentoGenerado
                    ];

                    if ($this->site->updateSale($data, $document->id)) {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada."
                        ];
                    } else {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada. Pero no pudo ser actualizado en Wappsi. Por favor consulta el estado del documento."
                        ];
                    }

                    $this->site->saveHits($document->id, ELECTRONIC_BILL, $data["fe_mensaje"], SUCCESS, $document->date);
                }
            } else {
                $this->site->updateSale(["fe_mensaje_soporte_tecnico" => $summary->Message], $document->id);
                $this->site->saveHits($document->id, ELECTRONIC_BILL, "", ERROR, $document->date);

                $response = [
                    'response' => FALSE,
                    'message' => str_replace("'", "", $summary->Message)
                ];
            }

            return (object) $response;
        }

        public function manageSendResponseSimba($response, $eDocument)
        {
            $document = $this->site->getSaleByID($eDocument->sale_id);

            $respuestaUnitaria = isset($response->RespuestaUnitaria) ? $response->RespuestaUnitaria->EncabezadoRespuesta : false;
            if ($respuestaUnitaria) {
                $arrayIndicadores = $response->RespuestaUnitaria->EncabezadoRespuesta->EstadosGenerales->IndicadorProceso;
                $cufe = isset($response->RespuestaUnitaria->DocElectronicoExtendido->DatosBasicos->CUDE) ? $response->RespuestaUnitaria->DocElectronicoExtendido->DatosBasicos->CUDE : NULL;
                $msg = $response->RespuestaUnitaria->EncabezadoRespuesta->TextoResumenNovsCrit;
                $etapa = $response->RespuestaUnitaria->EncabezadoRespuesta->Etapa ?? [];
                $fechaValidDian = null;
                foreach ($etapa as $item) {
                    if ($item->Orden === "5" && $item->Codigo === "VALIDDIAN") {
                        $fechaValidDian = new DateTime($item->FechaHora);
                        $fechaValidDian = $fechaValidDian->format('Y-m-d H:i:s');
                        break;
                    }
                }

                $msg_sin_saltos = str_replace("\n", "", $msg);
                $status = 0;
                foreach ($arrayIndicadores as $keyAi => $valueAi) {
                    if ($valueAi->Nombre == 'IND_ENVIADO_DIAN' && $valueAi->Value == true) {
                        $status = 3;
                    }
                    if ($valueAi->Nombre == 'IND_ACEPTADO_DIAN' && $valueAi->Value == true) {
                        $status = 2;
                    }
                    if ($valueAi->Nombre == 'IND_ACEPTADO_DIAN' && $valueAi->Value == false) {
                        $status = 1;
                    }
                }
                if ($status == 2) {
                    $customer_data = $this->site->getCompanyByID($document->customer_id);
                    $codigo_qr =    "NumeroFactura: ".str_replace('-', '', $document->reference_no)."\n".
                                    "FechaFactura: ". $this->format_date($document->date)->date."\n".
                                    "ValorFactura: ". $this->sma->formatDecimalNoRound(abs($document->total), 2)."\n".
                                    "ValorIVA: ". ($this->sma->formatDecimalNoRound(abs($document->total_tax), 2))."\n".
                                    "ValorTotalFactura: ". $this->sma->formatDecimalNoRound(abs($document->grand_total), 2)."\n".
                                    "NitEmisor: ". str_replace("-", "", $this->Settings->numero_documento)."\n".
                                    "DocumentoAdquiriente: ". str_replace('-', '', $customer_data->vat_no)."\n".
                                    "CUFE:".$cufe."\n".
                                    "https://catalogo-vpfe.dian.gov.co/document/searchqr?documentkey={$cufe}";
                    $data = [
                        "fe_aceptado"   => $status,
                        "cufe"          => $cufe,
                        "codigo_qr"     => $codigo_qr,
                        "fe_mensaje"    => "Documento aceptado por la DIAN",
                        "fe_mensaje_soporte_tecnico" => $msg_sin_saltos,
                        "fe_validation_dian"    => $fechaValidDian,
                    ];

                    if ($this->site->updateSale($data, $document->id)) {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica, ha sido autorizada."
                        ];
                    } else {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica, ha sido autorizada. Pero no pudo ser actualizado en Wappsi. Por favor consulta el estado del documento."
                        ];
                    }
                    $this->site->saveHits($document->id, ELECTRONIC_BILL, $data["fe_mensaje"], SUCCESS, $document->date);
                } else {
                    $cude = $this->get_cude_simba($msg);
                    $this->site->updateSale(["cufe" => $cude, "fe_aceptado"   => $status, "fe_mensaje_soporte_tecnico" => $msg_sin_saltos], $document->id);
                    $response = [
                        'response' => FALSE,
                        'message' => str_replace("'", "", $msg_sin_saltos)
                    ];
                    $this->site->saveHits($document->id, ELECTRONIC_BILL, "", ERROR, $document->date);
                }
            }else{
                $response = [
                    'response' => FALSE,
                    'message' => "No es posible realizar conexión con Web Services."
                ];
            }
            return (object) $response;
        }

        function getXMLSimba($sale_id, $unity_test=null){
            $binarioField = NULL;
            $document = $this->site->getSaleByID($sale_id);
            $technologyProvider = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
            $json = $this->getDocumenSimba($document, $technologyProvider, $unity_test);

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://fe2.simba.co/api_nomina/api/FacturaElectronica/ConsultarDocumentos',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $json,
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]);
            $response = curl_exec($curl);
            curl_close($curl);
            log_message('debug', 'FE - SIMBA peticion xml: '. $response);
            
            try {
                $dataResponse = json_decode($response, false);
                if ($dataResponse === false) {
                    log_message('debug','FE SIMBA: '.$response);
                    return false;
                }

                $binarioField = $dataResponse->respuestaUnitariaField->docElectronicoExtendidoField->archivoPrincipalField[0]->binarioField;
                if ($binarioField) {
                    $this->site->updateSale(['fe_xml' => $binarioField], $document->id);
                }else{
                    log_message('debug','FE SIMBA: '.$response);
                }
            } catch (Exception $e) {
                log_message('debug','FE SIMBA: '. $e->getMessage());
            }
            return $binarioField;
        }

        public function getDocumenSimba($document,  $technologyProvider, $print=null)
        {
            $auxReference = explode('-', $document->reference_no);
            $typeDocument = $this->site->getTypeElectronicDocument($document->id);
            $typeDocumentPeticion = 1;
            if ($typeDocument == INVOICE) {
                $typeDocumentPeticion = 1;
            }
            if ($typeDocument == CREDIT_NOTE) {
                $typeDocumentPeticion = 3;
            }
            $peticion = [];
            $peticion["contenidoSolicitudField"]['solicitudIndividualField']['cUDEField'] = null;
            $peticion["contenidoSolicitudField"]['solicitudIndividualField']['fechaDocumentoField'] =date('Y-m-d', strtotime($document->date));
            $peticion["contenidoSolicitudField"]['solicitudIndividualField']['fechaDocumentoFieldSpecified'] = true;
            $peticion["contenidoSolicitudField"]['solicitudIndividualField']['numeroDocumentoField'] = $auxReference[1];
            $peticion["contenidoSolicitudField"]['solicitudIndividualField']['prefijoDocumentoField'] = $auxReference[0];
            $peticion["contenidoSolicitudField"]['solicitudIndividualField']['tipoDocumentoField'] = $typeDocumentPeticion;
            $peticion['encabezadoSolicitudField']['ambienteField'] = $this->Settings->fe_work_environment;
            $peticion['encabezadoSolicitudField']['nitEmpresaSolicitanteField'] = $this->Settings->numero_documento;
            $peticion['encabezadoSolicitudField']['nombreSistemaEmisorField'] = $this->Settings->razon_social;
            $peticion['encabezadoSolicitudField']['tipoAmbienteSMBField'] = "2.1";
            $peticion['encabezadoSolicitudField']['tokenEmpresaField'] = $technologyProvider->security_token;
            $peticion['encabezadoSolicitudField']['versionSistemaEmisorField'] = "1.0";
            $peticion['parametrosField']['modoSolicitudField'] = "Individual";
            $peticion['parametrosField']['parametroAvanzadoField'] = [["nombreField" => "MODO-DESCARGA", "valorField" => "IND_AB64"]];
            $peticion['parametrosField']['parametroBasicoField'] = [["nombreField" => "GET_ATD", "valorField" => "GET_ATD"]];
            return json_encode($peticion, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        public function getDocumentBPM($document)
        {
            $technologyProvider = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
            $typeDocument = $this->site->getTypeElectronicDocument($document->id);
            $securityToken = $this->getSecurityTokenBPM($document, $typeDocument);


            if ($typeDocument == CREDIT_NOTE) {
                $documentType = 3;
            } else if ($typeDocument == DEBIT_NOTE) {
                $documentType = 2;
            } else if ($typeDocument == INVOICE) {
                $documentType = 1;
            }

            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $technologyProvider->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 120,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => '{
                    "Controller"    : "Facturas",
                    "Action"        : "GetDocument",
                    "ParamValues"   : {
                        "TokenIdentificador": "'.$securityToken.'",
                        "TipoDocumento"   : '.$documentType.',
                        NumeroDocumento: "'. str_replace("-", "", $document->reference_no) .'",
                    }
                }',
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]);
            $response = curl_exec($curl);
            curl_close($curl);

            log_message('debug', 'FE - BPM consulta documento: '. $response);

            return json_decode($response, false);
        }

        public function manageResponseGetDocumentBPM($response, $eDocument)
        {
            $document = $this->site->getSaleByID($eDocument->sale_id);

            $summary = $response->Data->Summary;

            if ($summary->Success == YES) {
                $dataResult = $response->Data->DataResult[0];
                if ($dataResult->DocumentoEstadoDianIdentificador == 7200002) {
                    $data = [
                        "fe_aceptado"   => ACCEPTED,
                        "cufe"          => $dataResult->DocumentoCufe,
                        "codigo_qr"     => $dataResult->DocumentoQr,
                        "fe_mensaje"    => "Documento aceptado por la DIAN",
                        "fe_mensaje_soporte_tecnico" => "La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada.",
                        "fe_xml"        => $dataResult->InfoXML->AttachedDocumentArchivoBase64,
                        "fe_generatedDocument"  => $dataResult->DocumentoGenerado
                    ];

                    if ($this->site->updateSale($data, $document->id)) {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada."
                        ];
                    } else {
                        $response = [
                            'response'  => TRUE,
                            'message'   => "Documento aceptado por la DIAN. La Factura electrónica ".$dataResult->DocumentoGenerado.", ha sido autorizada. Pero no pudo ser actualizado en Wappsi. Por favor consulta el estado del documento."
                        ];
                    }

                    $this->site->saveHits($document->id, ELECTRONIC_BILL, $data["fe_mensaje"], SUCCESS, $document->date);
                }
            } else {
                $this->site->updateSale(["fe_mensaje_soporte_tecnico" => $summary->Message], $document->id);
                $this->site->saveHits($document->id, ELECTRONIC_BILL, "", ERROR, $document->date);

                $response = [
                    'response' => FALSE,
                    'message' => str_replace("'", "", $summary->Message)
                ];
            }

            return (object) $response;
        }
    /****************************************************************/

    /****************************** SIMBA *****************************************/
        public function simbaBuildJsonInvoice ($documentElectronic){
            $totImp2 = [];
            $issuer_data = $this->site->get_setting(); // variable que almacena los datos contenidos en el settings
            $security_token = $this->get_secuirty_token($issuer_data->fe_technology_provider, $issuer_data->fe_work_environment); // token que esta en la tabla configuration provider
            $typeDocument = $this->getTypeElectronicDocument($documentElectronic->sale_id); // tipo de factura
            $typeDocElectronic = $this->get_typeDocElectronic($typeDocument); // tipo de documento
            $sale = $this->site->getSaleByID($documentElectronic->sale_id); // datos de la venta
            $idCustomization = $this->get_idCustomizacion($sale->order_tax_aiu_id); // id de personalizacion segun tabla 13.1.5. Tipos de operación
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id); // document_types
            $consecutive_sale = $this->get_consecutiveSale($sale->reference_no); // metodo para obtener el consecutivo de la venta
            $fechaEmision = $this->current_date_format($sale->date);  // fecha hora de emision de factura en formato UTC
            $fechaDateDue = $this->current_date_format(($sale->due_date) ? $sale->due_date : $sale->date);
            $items_sale = $this->site->getAllSaleItems($documentElectronic->sale_id); // items de la factura
            $ciiu_code = $this->get_ciiuCode($this->Settings->ciiu_code); // codigo de actividad economica
            $biller_data = $this->site->getCompanyByID($documentElectronic->biller_id); // datos del emisor
            $customer_data = $this->site->getCompanyByID($documentElectronic->customer_id); // datos del adquiriente
            $types_issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER); // obligaciones del emisor
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER); // obligaciones del adquiriente
            $payments_data = $this->site->getSalePayments($documentElectronic->sale_id);
            $paymentMeanCode = $this->getPaymentMeanSimba($sale);
            $stringObligations = '';
            foreach ($types_issuer_obligations as $keyTo => $valueTo) {
                $stringObligations .= $valueTo->types_obligations_id . ";";
            }
            $stringObligations = trim($stringObligations, ';');

            $stringObligationAdquirer = '';
            foreach ($customer_obligations as $keyToc => $valueToc) {
                $stringObligationAdquirer .= $valueToc->types_obligations_id . ";";
            }
            $stringObligationAdquirer = trim($stringObligationAdquirer, ';');

            /******************************* NODO LINEAS ******************************/
            $n=1;
            /** variables para impuestos **/
            $totalTax = 0;
            $totalBase = 0;
            $totalTotal = 0;
            $totBaseImp = 0;

            foreach ($items_sale as $key => $value) {
                $totalBase += $value->net_unit_price * $value->quantity;
                $totalTotal += $totalTax + $totalBase;
                $taxArrayProduct = [];
                $netUnitPrice = $value->net_unit_price;
                if ($value->code_fe_name == 'Bolsas') {
                    $netUnitPrice = $value->unit_price;
                }

                /* excento con 0 excluido no se envia, bajo esta observación realizada por el proveedor tecnologico si la linea del producto esta excenta se envia con 0 y si esta excluido no se envia registro*/
                if ($value->excluded != 1) {
                    $taxArrayProduct = $this->get_tax_array_t($value, 'COP');
                }
                $arrayItems[] = [
                    "Id"    => [
                        "Value" => "".$n.""  //Número de Línea: Número de Línea
                    ],
                    "Nota"  => [
                        [
                            "Value" => "".$value->product_name."" // Información Adicional: Texto libre para añadir información adicional al artículo.
                        ]
                    ],
                    "Cantidad" => [
                        "CodUnidad" => "NIU",   // unidad de medida estandar
                        "Value"     => "$value->quantity"   // cantidad de unidades
                    ],
                    "ValorNeto" => [
                        "IdMoneda"  => 'COP',  // codigo moneda
                        "Value"     => "".$this->sma->formatDecimal($value->net_unit_price * $value->quantity, NUMBER_DECIMALS_SIMBA).""  // subtotal
                    ],
                    "FechaVigenciaImpuestoSpecified"    =>   false,
                    "IndicaEsGratisSpecified"   => false,
                    "TotalImpuesto" => $taxArrayProduct,
                    "Item"                  =>[
                        "Descripcion"           => [
                            [
                                "Value"                 =>$value->product_name,
                            ]
                        ],
                        "Nombre"                => [
                            "Value"                 => $value->product_name,
                        ],
                        "IdItemEstandar"        =>[
                            "Id"                    =>[
                                "SmaIdCodigo"           => "999",
                                "SmaIdNombre"           => $value->product_name,
                                "Value"                 => $value->product_name
                            ]
                        ]
                    ],
                    "Precio"                =>[
                        "ValorPrecio"           =>[
                            "IdMoneda"              => 'COP',
                            "Value"                 => "".$this->sma->formatDecimal($value->net_unit_price, NUMBER_DECIMALS_SIMBA)."",
                        ],
                        "CantidadBase"      =>[
                            "CodUnidad"         => "NIU",
                            "Value"             => "$value->quantity"
                        ]
                    ]
                ];

                // Condicional para agregar "ReferenciaPrecios"
                if ($value->code_fe_name == 'Bolsas') {
                    $arrayItems[count($arrayItems) - 1]["ReferenciaPrecios"] = [
                        "PrecioAlternativo" => [
                            [
                                "ValorPrecio" => [
                                    "IdMoneda" => "COP",
                                    "Value"    => "100.0000"
                                ],
                                "TipoPrecioCodigo" => [
                                    "Value" => "01"
                                ],
                                "TipoPrecioTexto" => [
                                    "Value" => "Valor comercial"
                                ],
                                "FactorConvAUnidadPedidoSpecified" => false
                            ]
                        ]
                    ];
                }

                if ($value->excluded != 1) {
                    if ($value->code_fe_name == 'Bolsas') {
                        $totBaseImp += 0;
                        $totalTax += ($netUnitPrice * $value->quantity);
                        if (isset($totImp2[$value->code_fe][$value->rate])) {
                            $totImp2[$value->code_fe][$value->rate]['base'] += 0;
                            $totImp2[$value->code_fe][$value->rate]['total'] += ($netUnitPrice * $value->quantity);
                        }else{
                            $totImp2[$value->code_fe][$value->rate] = [
                                'code_fe' => $value->code_fe,
                                'rate' => $value->rate,
                                'base' => 0,
                                'total' => ($netUnitPrice * $value->quantity),
                                'code_fe_name' => $value->code_fe_name,
                                'quantity' => $value->quantity,
                            ];
                        }
                    }else{
                        if ($value->code_fe_name != 'IC') {
                            $totBaseImp += ($netUnitPrice * $value->quantity);
                        }
                        $totalTax += (($netUnitPrice * $value->quantity) * $value->rate) / 100;
                        if (isset($totImp2[$value->code_fe][$value->rate])) {
                            $totImp2[$value->code_fe][$value->rate]['base'] += ($netUnitPrice * $value->quantity);
                            $totImp2[$value->code_fe][$value->rate]['total'] += (($netUnitPrice * $value->quantity) * $value->rate) /100;
                            $totImp2[$value->code_fe][$value->rate]['quantity'] += $value->quantity;
                        }else{
                            $totImp2[$value->code_fe][$value->rate] = [
                                'code_fe' => $value->code_fe,
                                'rate' => $value->rate,
                                'base' => $netUnitPrice * $value->quantity,
                                'total' => (($netUnitPrice * $value->quantity) * $value->rate) / 100,
                                'code_fe_name' => $value->code_fe_name,
                                'quantity' => $value->quantity,
                            ];
                        }
                    }
                }
                if ($value->tax_rate_2_id != 0) {
                    $totalTax += $value->item_tax_2;
                    $rateSecondTax = $value->item_tax_2;
                    $perSeconImp = '';
                    if ($value->mililiters == '0') {
                        $perSeconImp = intval(str_replace('%', '', $value->tax_2));
                    }else{
                        $perSeconImp = $value->tax_2;
                    }

                    if (isset($totImp2[$value->codeTax2][$value->rate])) {
                        $totImp2[$value->codeTax2][$value->rate]['base'] += $value->net_unit_price * $value->quantity;
                        $totImp2[$value->codeTax2][$value->rate]['total'] += $value->item_tax_2;
                    }else{
                        $totImp2[$value->codeTax2][$value->rate] = [
                            'code_fe'       => $value->codeTax2,
                            'rate'          => $perSeconImp,
                            'base'          => $value->net_unit_price * $value->quantity,
                            'total'         => $value->item_tax_2,
                            'code_fe_name'  => $value->nameTax2,
                            'nominal'       => $value->nominal,
                            'mililiters'    => $value->mililiters,
                            'degrees'       => $value->degrees,
                            'quantity'      => $value->quantity,
                            'is_second_tax' => true,
                        ];
                    }
                }
                $n++;
            }
            /******************************* NODO LINEAS ******************************/
            $totalBaseFormated = $this->sma->formatDecimal($totalBase, NUMBER_DECIMALS_SIMBA); // total Base ya va con formato
            $totalBaseImpFormated = $this->sma->formatDecimal($totBaseImp, NUMBER_DECIMALS_SIMBA); // total Impuesto con formato
            if ($sale->product_discount > 0) { // <- en este caso el descuento es por productos y si es por producto no se envia pues este ya afecto los items la factura ya se envia con el descuento implicito
                $totalDiscount = $this->sma->formatDecimal(0, NUMBER_DECIMALS_SIMBA); // total Descuentos
            }else{
                $totalDiscount = $this->sma->formatDecimal($sale->total_discount, NUMBER_DECIMALS_SIMBA); // total Descuentos
            }

            /*********************************** Total impuestos ******************************************************************/
            $totTax = $this->getTotalTax($totImp2, 'COP');
            $totalTaxArray = $totTax['totalTaxArray'];
            $totalTImpuesto = $totTax['totalTImpuesto'];
            /*********************************** Total impuestos ******************************************************************/

            /*********************************** total retenciones ****************************************************************/
            $withHoldingsTot = $sale->rete_fuente_total + $sale->rete_iva_total + $sale->rete_ica_total;
            $withHoldings = $this->getWithholdingsSimba($sale);
            $totalWithHolding = $this->sma->formatDecimal($withHoldingsTot, NUMBER_DECIMALS_SIMBA); // totalRetenciones ya va con formato
            $arrayWithAux = [];
            $totalWithHoldingsArray = [];

            if (count($withHoldings)> 0) {
                foreach ($withHoldings as $keyw => $valuew) {
                    $arrayWithAux[] = [
                        "BaseImponible"     => "".$this->sma->formatDecimal($valuew['Base'], NUMBER_DECIMALS_SIMBA)."",
                        "ValorImpuesto"     => "".$this->sma->formatDecimal($valuew['Total'], NUMBER_DECIMALS_SIMBA)."",
                        "CategoriaImpuesto" =>[
                            "Porcentaje"        => "". $this->sma->formatDecimal($valuew['Valor'], NUMBER_DECIMALS_SIMBA)  ."",
                            "EsquemaTributario" =>[
                                "Id"                => $valuew['Codigo'],
                                "Nombre"            => $valuew['Name']
                            ]
                        ]
                    ];
                }
                $totalWithHoldingsArray = [
                    "ValorImpuesto"     =>[
                        "IdMoneda"          =>  'COP',
                        "Value"             =>  "".$this->sma->formatDecimal($withHoldingsTot, NUMBER_DECIMALS_SIMBA).""
                    ],
                    "SubTotalImpuesto"  =>$arrayWithAux
                ];
            }
            /*********************************** total retenciones ****************************************************************/

            /*********************aca vamos a crear un array que va a rellenar el nodo Medios de pago del Json */
            $m=0;
            if ($payments_data) { // en el caso que existan los pagos
                foreach ($payments_data as $keyP => $valueP) { // iteramos en todos los pagos existentes
                    $payment_type = $this->getPaymentMethodSimba($sale);
                    $pagos[$m] = [
                        "Id"    =>   [
                            "Value"         => "$payment_type"
                        ],
                        "CodigoMedioDePago" => [
                            "Value"             => $paymentMeanCode
                        ]
                    ];
                    if ($payment_type == 2) {
                        $pagos[$m]["FechaLimitePago"] =  $fechaDateDue;
                    }
                    $pagos[$m]["IdPago"] = [
                        [
                            "Value"             => $valueP->id
                        ]
                    ];
                    $m++;
                }
            }else if(!$payments_data){ // si no existe ningun pago
                $payment_type = $this->getPaymentMethodSimba($sale);
                $pagos[$m] = [
                    "Id"    =>   [
                        "Value"         => "$payment_type"
                    ],
                    "CodigoMedioDePago" => [
                        "Value"             => "ZZZ"
                    ],
                    "FechaLimitePago"       => $fechaDateDue
                ];
                $pagos[$m]["IdPago"] = [
                    [
                        "Value"                 => '1'
                    ]
                ];
            }
            /*********************aca vamos a crear un array que va a rellenar el nodo Medios de pago del Json */

            $request = [
                "Parametros" => [
                    "VersionDocElectronico" => "4.0", // Es la versión del documento eletrónico que se utiliza. Debe contener el literal "4.0"
                    "NombreSistemaEmisor"   => $issuer_data->site_name, //Nombre del Software que envía, identificamos para nuestro control el sistema emisor (origen, ERP, contable)
                    "VersionSistemaEmisor"  => $issuer_data->version, //Es la versión del sistema que envía la petición. (Varía por cliente)
                    "ModoRespuesta"         => "1", // Es la forma en que se recibirá la respuesta. Por ahora dejar código en 1
                    "TipoAmbiente"          => $issuer_data->fe_work_environment, //Tipo ambiente (Podemos separar las facturas de producción y pruebas)
                    "TokenEmpresa"          => $security_token->security_token, //Token que identifica la empresa que esta enviando el documento ante el proveedor Tecnológico, mecanismo adicional de autenticación. Proporcionado por SIMBA para cada NIT, solicitado por el OFE a SIMBA
                    "TipoReporte"           => "1", //Identificador de la representacion grafica, tantas como desee o contrate el emisor, aquí identifica cuál representación usar. 1. Por Defecto
                    "Wildcards"             => "[NT_SK_VFA]",
                    "Personalizacion"       => "20",
                    // "PasswordEmpresa"       => "",  // Sin comentarios en el DEV-FEL-0.15 json especifica que en el momento no se usa
                    "ContactoReceptor"      => [
                        [
                            "CorreoElectronico"             => $biller_data->email,
                            "IdEtiquetaUbicacionCorreo"     => "1",
                        ]
                    ],
                    "IndicadoresAdicionales"    => [
                        [
                            "NombreIndicador"   =>  "SKIPVALIDDIANLOGI",
                            "Activado"          =>  true
                        ],
                        [
                            "NombreIndicador"   => "SKIPVALIDDIANREQU",
                            "Activado"  => true
                        ]
                    ]
                ],
                "Encabezado" => [
                    "TipoDocElectronico"    => "$typeDocElectronic", //Tipo de documento (Factura,N.crédito,N.débito). Se complementa con el campo Tipo de Factura o tipo de Nota para indicar FVenta, FContingencia, FExportación, otros
                    "IdPersonalizacion"     => [ // Tipo de Operación
                        "Value"                 => $idCustomization
                    ],
                    "PrefijoDocumento"      => $resolution_data->sales_prefix, // Prefijo de numeración de Documento Electrónico
                    "NumeroDocumento"       => $consecutive_sale, // consecutivo
                    "IndicaCopiaSpecified"  => false,
                    "FechaYHoraDocumento"   => $fechaEmision, // Fecha de emisión: Fecha de emisión de la factura a efectos fiscales. (Hora) de emisión
                    "FechaDeVencimientoSpecified" => false,
                    "TipoDeFactura"         => [
                        "Value"                 => $typeDocument,  //código: facturas de venta, y transcripciones
                    ],
                    "FechaTributariaSpecified"  => false,
                    "CodigoMoneda"          => [
                        "Value"                 => 'COP', //Divisa de la Factura: Divisa o moneda consolidada aplicable a toda la factura
                    ],
                    "CantidadLineas"        => "".count($items_sale)."", //Cantidad de líneas de la factura electrónica
                    "CantidadLineasSpecified" => true
                ],
                "Terceros" =>[
                    "TerceroProveedorContable" => [ //EMISOR - Obligado a Facturar, Tercero Proveedor, Facturador Electrónico Proveedor
                        "IdAdicional"           =>[
                                [
                                    "Value"                 => $this->Settings->tipo_persona   // tipo persona emisor
                                ]
                        ],
                        "Tercero"               =>[
                            "CodigoClasificacionIndustria"  => [
                                "Value"             => isset($ciiu_code->code) ? $ciiu_code->code : ''  //Actividad Economica (CIIU)
                            ],
                            "IdTercero"             =>[
                                [
                                    "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                    "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                    "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                ]
                            ],
                            "NombreTercero"         => [
                                [
                                    "Value"                     =>$this->Settings->razon_social  // razon social
                                ]
                            ],
                            "UbicacionFisica"       =>[
                                "Direccion"             =>[
                                    "Id"                    =>[
                                        "Value"                 => substr($biller_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad
                                    ],
                                    "Departamento"          =>[
                                        "Value"                 => $biller_data->state  // Departamento dentro de una Organización (Estructura Jerárquica)
                                    ],
                                    "Ciudad"                =>[
                                        "Value"                 => $biller_data->city  // Ciudad
                                    ],
                                    "ZonaPostal"            =>[
                                        "Value"                 => $biller_data->postal_code // codigo postal
                                    ],
                                    "SubdivisionPais"       =>[
                                        "Value"                 => $biller_data->state // Subdivisión del pais (Departamento, Estado, Provincia)
                                    ],
                                    "SubdivisionPaisCodigo" =>[
                                        "Value"                 => substr(substr($biller_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros
                                    ],
                                    "LineaDireccion"        =>[
                                        [
                                            "TextoLinea"        => [
                                                "Value"             =>$biller_data->address // Nombre de una calle
                                            ]
                                        ]
                                    ],
                                    "Pais"                  =>[
                                        "Codigo"                => [
                                            "Value"                 => $biller_data->codigo_iso // País de la dirección
                                        ],
                                        "Nombre"                =>[
                                            "IdLenguaje"            => "es",
                                            "Value"                 => ucfirst(mb_strtolower($biller_data->NOMBRE ))  // país del emisor
                                        ]
                                    ]
                                ]
                            ],
                            "EsquemaTributarioTercero" =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $this->Settings->razon_social
                                    ],
                                    "NumeroIdTributario"    =>[
                                        "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                        "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                        "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                    ],
                                    "NivelTributario"       =>[
                                        "ListaNombre"           => $this->get_name_regimen($biller_data->tipo_regimen),
                                        "Value"                 => $stringObligations
                                    ],
                                    "DireccionParaImpuestos"=>[
                                        "Id"                    =>[
                                            "Value"                 => substr($biller_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad
                                        ],
                                        "Departamento"          =>[
                                            "Value"                 => $biller_data->state  // Departamento dentro de una Organización (Estructura Jerárquica)
                                        ],
                                        "Ciudad"                =>[
                                            "Value"                 => $biller_data->city  // Ciudad
                                        ],
                                        "ZonaPostal"            =>[
                                            "Value"                 => $biller_data->postal_code // codigo postal
                                        ],
                                        "SubdivisionPais"       =>[
                                            "Value"                 => $biller_data->state // Subdivisión del pais (Departamento, Estado, Provincia)
                                        ],
                                        "SubdivisionPaisCodigo" =>[
                                            "Value"                 => substr(substr($biller_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros
                                        ],
                                        "LineaDireccion"        =>[
                                            [
                                                "TextoLinea"        => [
                                                    "Value"             =>$biller_data->address // Nombre de una calle
                                                ]
                                            ]
                                        ],
                                        "Pais"                  =>[
                                            "Codigo"                => [
                                                "Value"                 => $biller_data->codigo_iso // País de la dirección
                                            ],
                                            "Nombre"                =>[
                                                "IdLenguaje"            => "es",
                                                "Value"                 => ucfirst(mb_strtolower($biller_data->NOMBRE))    // país del emisor
                                            ]
                                        ]
                                    ],
                                    "EsquemaTributario"     =>[
                                        "Id"                    =>[
                                            "Value"                 => $this->get_id_tributo($biller_data->tipo_regimen)
                                        ],
                                        "Nombre"                =>[
                                            "Value"                 => $this->get_name_tributo($biller_data->tipo_regimen)
                                        ]
                                    ]
                                ]
                            ],
                            "EntidadLegalTercero"       =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $this->Settings->razon_social   // Razón Social, Nombre con el cual aparece registrado legalmente el tercero
                                    ],
                                    "NumeroIdLegal"         =>[
                                        "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                        "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                        "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                    ],
                                    "EsquemaRegistroCorporativo" =>[
                                        "Id"                        =>[
                                            "Value"                     => $resolution_data->sales_prefix,
                                        ],
                                        "Nombre"                    =>[
                                            "Value"                     => $this->Settings->matricula_mercantil
                                        ]
                                    ]
                                ]
                            ],
                            "Contacto"                  =>[
                                "Nombre"                    =>[
                                    "Value"                     => $biller_data->name
                                ],
                                "Telefono"                  =>[
                                    "Value"                     => $biller_data->phone
                                ],
                                "Email"                     =>[
                                    "Value"                     => $biller_data->email
                                ],
                                "Nota"                      => []
                            ]
                        ]
                    ],
                    "TerceroClienteContable" =>[  // RECEPTOR - Tercero Adquiriente, Tercero Cliente del Documento Electrónico
                        "IdAdicional"           =>[
                            [
                                "Value"             => $customer_data->type_person
                            ]
                        ],
                        "Tercero"               =>[
                            "IdTercero"             =>[
                                [
                                    "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                                    "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                    "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                ]
                            ],
                            "NombreTercero"         => [
                                [
                                    "Value"                     =>$customer_data->name  // nombre del adquiriente
                                ]
                            ],
                            "UbicacionFisica"       =>[
                                "Direccion"             =>[
                                    "Id"                    =>[
                                        "Value"                 => substr($customer_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad del adquiriente
                                    ],
                                    "Departamento"          =>[
                                        "Value"                 => $customer_data->state  // Departamento dentro de una Organización (Estructura Jerárquica) del adquiriente
                                    ],
                                    "Ciudad"                =>[
                                        "Value"                 => $customer_data->city  // Ciudad del adquiriente
                                    ],
                                    "ZonaPostal"            =>[
                                        "Value"                 => $customer_data->postal_code // codigo postal del adquiriente
                                    ],
                                    "SubdivisionPais"       =>[
                                        "Value"                 => $customer_data->state // Subdivisión del pais (Departamento, Estado, Provincia) del adquiriente
                                    ],
                                    "SubdivisionPaisCodigo" =>[
                                        "Value"                 => substr(substr($customer_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros  del adquiriente
                                    ],
                                    "LineaDireccion"        =>[
                                        [
                                            "TextoLinea"        => [
                                                "Value"             =>$customer_data->address // Nombre de una calle del adquiriente
                                            ]
                                        ]
                                    ],
                                    "Pais"                  =>[
                                        "Codigo"                => [
                                            "Value"                 => $customer_data->codigo_iso // País de la dirección del adquiriente
                                        ],
                                        "Nombre"                =>[
                                            "IdLenguaje"            => "es",
                                            "Value"                 => ucfirst(mb_strtolower($customer_data->NOMBRE))   // país del emisor del adquiriente
                                        ]
                                    ]
                                ]
                            ],
                            "EsquemaTributarioTercero" =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $customer_data->name  // nombre del adquiriente
                                    ],
                                    "NumeroIdTributario"    =>[
                                        "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                                        "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                        "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                    ],
                                    "NivelTributario"       =>[
                                        "ListaNombre"           => $this->get_name_regimen($customer_data->tipo_regimen),
                                        "Value"                 => $stringObligationAdquirer
                                    ],
                                    "DireccionParaImpuestos"=>[
                                        "Id"                    =>[
                                            "Value"                 => substr($customer_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad del aquiriente
                                        ],
                                        "Departamento"          =>[
                                            "Value"                 => $customer_data->state  // Departamento dentro de una Organización (Estructura Jerárquica) del adquiriente
                                        ],
                                        "Ciudad"                =>[
                                            "Value"                 => $customer_data->city  // Ciudad del adquiriente
                                        ],
                                        "ZonaPostal"            =>[
                                            "Value"                 => $customer_data->postal_code // codigo postal del adquiriente
                                        ],
                                        "SubdivisionPais"       =>[
                                            "Value"                 => $customer_data->state // Subdivisión del pais (Departamento, Estado, Provincia) del adquiriente
                                        ],
                                        "SubdivisionPaisCodigo" =>[
                                            "Value"                 => substr(substr($customer_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros del adquirente
                                        ],
                                        "LineaDireccion"        =>[
                                            [
                                                "TextoLinea"        => [
                                                    "Value"             =>$customer_data->address // Nombre de una calle del adquirente
                                                ]
                                            ]
                                        ],
                                        "Pais"                  =>[
                                            "Codigo"                => [
                                                "Value"                 => $customer_data->codigo_iso // País de la dirección del adquiriente
                                            ],
                                            "Nombre"                =>[
                                                "IdLenguaje"            => "es",
                                                "Value"                 => ucfirst(mb_strtolower($customer_data->NOMBRE))   // país del emisor del adquiriente
                                            ]
                                        ]
                                    ],
                                    "EsquemaTributario"     =>[
                                        "Id"                    =>[
                                            "Value"                 => $this->get_id_tributo($customer_data->tipo_regimen)
                                        ],
                                        "Nombre"                =>[
                                            "Value"                 => $this->get_name_tributo($customer_data->tipo_regimen)
                                        ]
                                    ]
                                ]
                            ],
                            "EntidadLegalTercero"       =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $customer_data->name   // Razón Social, Nombre con el cual aparece registrado legalmente el tercero
                                    ],
                                    "NumeroIdLegal"         =>[
                                        "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                                        "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                        "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                    ],
                                    "EsquemaRegistroCorporativo" =>[
                                        "Id"                        =>[
                                            "Value"                     => $resolution_data->sales_prefix,
                                        ]
                                    ]
                                ]
                            ],
                            "Contacto"                  =>[
                                "Nombre"                    =>[
                                    "Value"                     => $customer_data->name
                                ],
                                "Telefono"                  =>[
                                    "Value"                     => $customer_data->phone
                                ],
                                "Email"                     =>[
                                    "Value"                     => $customer_data->email
                                ],
                                "Nota"                      =>[]
                            ]
                        ]
                    ],
                    "TerceroRpteFiscal"     =>[
                        "IdTercero"             => [
                            [
                                "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                                "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                            ]
                        ]
                    ]
                ],
                "Lineas"    => $arrayItems,
                "AgregadoComercial" =>[
                    "MediosDePago"  => $pagos,
                    "CargoDescuento" => $this->get_general_discount($sale, 'COP'),
                ],
                "Totales"   =>[
                    "TotalImpuestos"    => $totalTaxArray,
                    "TotalRetenciones"  => $totalWithHoldingsArray,
                    "TotalMonetario"    =>[
                        "ValorBruto"            =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( $totalBaseFormated, NUMBER_DECIMALS_SIMBA).""
                        ],
                        "ValorBaseImpuestos"    =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( $totalBaseImpFormated, NUMBER_DECIMALS_SIMBA).""
                        ],
                        "TotalMasImpuestos"     =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( $totalTImpuesto + $totalBaseFormated, NUMBER_DECIMALS_SIMBA).""
                        ],
                        "TotalDescuentos"     =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( $totalDiscount, NUMBER_DECIMALS_SIMBA).""
                        ],
                        "ValorAPagar"           =>[
                            "IdMoneda"              => 'COP',
                            "Value"                 => "".$this->sma->formatDecimal( ($totalBaseFormated + $totalTImpuesto - $totalDiscount), NUMBER_DECIMALS_SIMBA).""
                        ]
                    ]
                ]
            ];
            $request = $this->get_documento_equivalente_pos($request, $sale, $resolution_data, $customer_data);
            $request = $this->get_another_currency($request, $sale, $fechaEmision);
            $request = $this->get_purchase_order($request, $sale);
            return json_encode($request, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
        }

        public function simbaBuildJsonCreditNote ($documentElectronic){
            $totImp2 = [];
            $issuer_data = $this->site->get_setting(); // variable que almacena los datos contenidos en el settings
            $security_token = $this->get_secuirty_token($issuer_data->fe_technology_provider, $issuer_data->fe_work_environment); // token que esta en la tabla configuration provider
            $typeDocument = $this->site->getTypeElectronicDocument($documentElectronic->sale_id); // tipo de factura
            $typeDocElectronic = $this->get_typeDocElectronic($typeDocument); // tipo de documento
            $sale = $this->site->getSaleByID($documentElectronic->sale_id); // datos de la venta
            $idCustomization = $this->get_idCustomizacion($sale->order_tax_aiu_id); // id de personalizacion segun tabla 13.1.5. Tipos de operación
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id); // document_types
            $consecutive_sale = $this->get_consecutiveSale($sale->reference_no); // metodo para obtener el consecutivo de la venta
            $fechaEmision = $this->current_date_format($sale->date);  // fecha hora de emision de factura en formato UTC
            $fechaDateDue = $this->current_date_format(($sale->due_date) ? $sale->due_date : $sale->date);
            $items_sale = $this->site->getAllSaleItems($documentElectronic->sale_id); // items de la factura
            $ciiu_code = $this->get_ciiuCode($this->Settings->ciiu_code); // codigo de actividad economica
            $biller_data = $this->site->getCompanyByID($documentElectronic->biller_id); // datos del emisor
            $customer_data = $this->site->getCompanyByID($documentElectronic->customer_id); // datos del adquiriente
            $types_issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER); // obligaciones del emisor
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER); // obligaciones del adquiriente
            $payments_data = $this->site->getSalePayments($documentElectronic->sale_id);
            $paymentMeanCode = $this->getPaymentMeanSimba($sale);
            $sale_origen = $this->get_sale_origin_from_other_year($sale);
            $stringObligations = '';
            foreach ($types_issuer_obligations as $keyTo => $valueTo) {
                $stringObligations .= $valueTo->types_obligations_id . ";";
            }
            $stringObligations = trim($stringObligations, ';');

            $stringObligationAdquirer = '';
            foreach ($customer_obligations as $keyToc => $valueToc) {
                $stringObligationAdquirer .= $valueToc->types_obligations_id . ";";
            }
            $stringObligationAdquirer = trim($stringObligationAdquirer, ';');

            /******************************* NODO LINEAS ******************************/
            $n=1;
            /** variables para impuestos **/
            $totalTax = 0;
            $totalBase = 0;
            $totalTotal = 0;
            $totBaseImp = 0;

            foreach ($items_sale as $key => $value) {
                $totalBase += $value->net_unit_price * $value->quantity;
                $totalTotal += $totalTax + $totalBase;
                $taxArrayProduct = [];
                $concept_product_id = $value->product_code;
                $netUnitPrice = $value->net_unit_price;
                if ($value->code_fe_name == 'Bolsas') {
                    $netUnitPrice = $value->unit_price;
                }

                /* excento con 0 excluido no se envia, bajo esta observación realizada por el proveedor tecnologico si la linea del producto esta excenta se envia con 0 y si esta excluido no se envia registro*/
                if ($value->excluded != 1) {
                    $taxArrayProduct = $this->get_tax_array_t($value, 'COP');
                }
                $arrayItems[] = [
                    "Id"    => [
                        "Value" => "".$n.""  //Número de Línea: Número de Línea
                    ],
                    "Nota"  => [
                        [
                            "Value" => "".$value->product_name."" // Información Adicional: Texto libre para añadir información adicional al artículo.
                        ]
                    ],
                    "Cantidad" => [
                        "CodUnidad" => "NIU",   // unidad de medida estandar
                        "Value"     => "" .abs($value->quantity). ""   // cantidad de unidades
                    ],
                    "ValorNeto" => [
                        "IdMoneda"  => 'COP',  // codigo moneda
                        "Value"     => "".$this->sma->formatDecimal($value->net_unit_price * abs($value->quantity), NUMBER_DECIMALS_SIMBA).""  // subtotal
                    ],
                    "FechaVigenciaImpuestoSpecified"    =>  false,
                    "IndicaEsGratisSpecified"           =>  false,
                    "ReferenciaLineaDocPedido"          =>  [],
                    "TotalImpuesto" => $taxArrayProduct,
                    "Item"                  =>[
                        "Descripcion"           => [
                            [
                                "Value"                 =>$value->product_name,
                            ]
                        ],
                        "CantidadEmpaque" => [
                            "CodUnidad" => "NIU",
                            "Value" => "" .abs($value->quantity). ""
                        ],
                        "ItemsPorEmpaqueSpecified" => false,
                        "IndicaDesdeCatalogoSpecified" => false,
                        "Marca" => [
                            [
                                "Value" => ""
                            ]
                        ],
                        "Modelo" => [
                            [
                                "Value" => ""
                            ]
                        ],
                        "Nombre"                => [
                            "Value"                 => $value->product_name,
                        ],
                        "IdItemComprador" => [
                            "Id" => [
                                "SmaIdCodigo" => "999",
                                "SmaIdNombre" => "Estándar de adopción del contribuyente",
                                "Value" => "9999999999999"
                            ],
                            "IdExtendida" => [
                                "Value" => "IdExtendido"
                            ],
                            "IdCodigoDeBarras" => [
                                "Value" => "Codigo de barras"
                            ],
                        ],
                        "IdItemVendedor" => [
                            "Id" => [
                                "SmaIdCodigo" => "999",
                                "SmaIdNombre" => "Estándar de adopción del contribuyente",
                                "Value" => "9999999999999"
                            ],
                            "IdExtendida" => [
                                "Value" => "IdExtendido"
                            ],
                            "IdCodigoDeBarras" => [
                                "Value" => "Codigo de barras"
                            ]
                        ],
                        "IndicadorDePeligroSpecified" => false,
                        "IdItemEstandar"        =>[
                            "Id"                    =>[
                                "SmaIdCodigo"           => "999",
                                "SmaIdNombre"           => $value->product_name,
                                "Value"                 => $value->product_name
                            ]
                        ],
                        "PropiedadesAdicionalesItem" => [
                            [
                                "Nombre" => [
                                    "Value" => "UnidadMedidaNombre"
                                ],
                                "Valor" => [
                                    "Value" => "NIU"
                                ]
                            ]
                        ],
                    ],
                    "Precio"                =>[
                        "ValorPrecio"           =>[
                            "IdMoneda"              => 'COP',
                            "Value"                 => "".$this->sma->formatDecimal($value->net_unit_price, NUMBER_DECIMALS_SIMBA)."",
                        ],
                        "CantidadBase"      =>[
                            "CodUnidad"         => "NIU",
                            "Value"             => "" .abs($value->quantity) .""
                        ],
                        "FactorConvAUnidadPedidoSpecified" => false,
                    ]
                ];

                // Condicional para agregar "ReferenciaPrecios"
                if ($value->code_fe_name == 'Bolsas') {
                    $arrayItems[count($arrayItems) - 1]["ReferenciaPrecios"] = [
                        "PrecioAlternativo" => [
                            [
                                "ValorPrecio" => [
                                    "IdMoneda" => "COP",
                                    "Value"    => "100.0000"
                                ],
                                "TipoPrecioCodigo" => [
                                    "Value" => "01"
                                ],
                                "TipoPrecioTexto" => [
                                    "Value" => "Valor comercial"
                                ],
                                "FactorConvAUnidadPedidoSpecified" => false
                            ]
                        ]
                    ];
                }
                

                if ($value->excluded != 1) {
                    if ($value->code_fe_name == 'Bolsas') {
                        $totBaseImp += 0;
                        $totalTax += ($netUnitPrice * $value->quantity);
                        if (isset($totImp2[$value->code_fe][$value->rate])) {
                            $totImp2[$value->code_fe][$value->rate]['base'] += 0;
                            $totImp2[$value->code_fe][$value->rate]['total'] += ($netUnitPrice * $value->quantity);
                        }else{
                            $totImp2[$value->code_fe][$value->rate] = [
                                'code_fe' => $value->code_fe,
                                'rate' => $value->rate,
                                'base' => 0,
                                'total' => ($netUnitPrice * $value->quantity),
                                'code_fe_name' => $value->code_fe_name,
                                'quantity'      => $value->quantity,
                            ];
                        }
                    }else{
                        if ($value->code_fe_name != 'IC') {
                            $totBaseImp += ($netUnitPrice * $value->quantity);
                        }
                        $totalTax += (($value->net_unit_price * $value->quantity) * $value->rate) / 100;
                        if (isset($totImp2[$value->code_fe][$value->rate])) {
                            $totImp2[$value->code_fe][$value->rate]['base'] += ($value->net_unit_price * $value->quantity);
                            $totImp2[$value->code_fe][$value->rate]['total'] += (($value->net_unit_price * $value->quantity) * $value->rate) /100;
                        }else{
                            $totImp2[$value->code_fe][$value->rate] = [
                                'code_fe' => $value->code_fe,
                                'rate' => $value->rate,
                                'base' => $value->net_unit_price * $value->quantity,
                                'total' => (($value->net_unit_price * $value->quantity) * $value->rate) / 100,
                                'code_fe_name' => $value->code_fe_name,
                                'quantity'      => $value->quantity,
                            ];
                        }
                    }
                }
                if ($value->tax_rate_2_id != 0) {
                    $totalTax += $value->item_tax_2;
                    $rateSecondTax = $value->item_tax_2;
                    $perSeconImp = '';
                    if ($value->mililiters == '0') {
                        $perSeconImp = intval(str_replace('%', '', $value->tax_2));
                    }else{
                        $perSeconImp = $value->tax_2;
                    }

                    if (isset($totImp2[$value->codeTax2][$value->rate])) {
                        $totImp2[$value->codeTax2][$value->rate]['base'] += $value->net_unit_price * $value->quantity;
                        $totImp2[$value->codeTax2][$value->rate]['total'] += $value->item_tax_2;
                    }else{
                        $totImp2[$value->codeTax2][$value->rate] = [
                            'code_fe'       => $value->codeTax2,
                            'rate'          => $perSeconImp,
                            'base'          => $value->net_unit_price * $value->quantity,
                            'total'         => $value->item_tax_2,
                            'code_fe_name'  => $value->nameTax2,
                            'quantity'       => $value->quantity,
                        ];
                    }
                }
                $n++;
            }
            /******************************* NODO LINEAS ******************************/
            $totalBaseFormated = $this->sma->formatDecimal($totalBase, NUMBER_DECIMALS_SIMBA); // total Base ya va con formato
            $totalTaxFormated = $this->sma->formatDecimal($totalTax, NUMBER_DECIMALS_SIMBA); // total Impuesto con formato
            $totalBaseImpFormated = $this->sma->formatDecimal($totBaseImp, NUMBER_DECIMALS_SIMBA); // total Impuesto con formato
            if ($sale->product_discount < 0) { // <- en este caso el descuento es por productos y si es por producto no se envia pues este ya afecto los items la factura ya se envia con el descuento implicito
                $totalDiscount = $this->sma->formatDecimal(0, NUMBER_DECIMALS_SIMBA); // total Descuentos
            }else{
                $totalDiscount = $this->sma->formatDecimal($sale->total_discount, NUMBER_DECIMALS_SIMBA); // total Descuentos
            }

            /*********************************** Total impuestos ******************************************************************/
            $totTax = $this->getTotalTax($totImp2, 'COP');
            $totalTaxArray = $totTax['totalTaxArray'];
            $totalTImpuesto = $totTax['totalTImpuesto'];
            /*********************************** Total impuestos ******************************************************************/

            /*********************************** total retenciones ****************************************************************/
            $withHoldingsTot = $sale->rete_fuente_total + $sale->rete_iva_total + $sale->rete_ica_total;
            $withHoldings = $this->getWithholdingsSimba($sale);
            $totalWithHolding = $this->sma->formatDecimal($withHoldingsTot, NUMBER_DECIMALS_SIMBA); // totalRetenciones ya va con formato
            $arrayWithAux = [];
            $totalWithHoldingsArray = [];

            if (count($withHoldings)> 0) {
                foreach ($withHoldings as $keyw => $valuew) {
                    $arrayWithAux[] = [
                        "BaseImponible"     => "".$this->sma->formatDecimal($valuew['Base'], NUMBER_DECIMALS_SIMBA)."",
                        "ValorImpuesto"     => "".$this->sma->formatDecimal($valuew['Total'], NUMBER_DECIMALS_SIMBA)."",
                        "CategoriaImpuesto" =>[
                            "Porcentaje"        => "". $this->sma->formatDecimal($valuew['Valor'], NUMBER_DECIMALS_SIMBA)  ."",
                            "EsquemaTributario" =>[
                                "Id"                => $valuew['Codigo'],
                                "Nombre"            => $valuew['Name']
                            ]
                        ]
                    ];
                }
                $totalWithHoldingsArray = [
                    "ValorImpuesto"     =>[
                        "IdMoneda"          =>  'COP',
                        "Value"             =>  "".$this->sma->formatDecimal($withHoldingsTot, NUMBER_DECIMALS_SIMBA).""
                    ],
                    "SubTotalImpuesto"  =>$arrayWithAux
                ];
            }
            /*********************************** total retenciones ****************************************************************/

            /*********************aca vamos a crear un array que va a rellenar el nodo Medios de pago del Json */
            $m=0;
            if ($payments_data) { // en el caso que existan los pagos
                foreach ($payments_data as $keyP => $valueP) { // iteramos en todos los pagos existentes
                    $payment_type = $this->getPaymentMethodSimba($sale);
                    $pagos[$m] = [
                        "Id"    =>   [
                            "Value"         => "$payment_type"
                        ],
                        "CodigoMedioDePago" => [
                            "Value"             => $paymentMeanCode
                        ],
                        "FechaLimitePagoSpecified" => false,
                        "IdInstruccion" => [
                          "Value" =>  ""
                        ],
                        "NotaInstruccion" => [
                          [
                            "Value" => "" .$valueP->note. ""
                          ]
                        ],
                        "CuentaFinancieraPagador"   => [
                            "Id"                        => [
                                "Value"                     => ""
                            ],
                            "Nombre"                    => [
                                "Value"                     => ""
                            ],
                            "TipoDeCuenta"              => [
                                "Value"                     => ""
                            ],
                            "CodigoMoneda"              => [
                                "Value"                     => "COP"
                            ],
                            "Nota"                      => [],
                            "Pais"                      => [
                                "Codigo"                    => [
                                    "Value"                     =>  "" .$biller_data->codigo_iso. ""
                                ],
                                "Nombre"                => [
                                    "IdLenguaje"            => "es",
                                    "Value"                 => "" .ucfirst(mb_strtolower($biller_data->NOMBRE)). ""
                                ]
                            ]
                        ]
                    ];
                    if ($payment_type == 2) {
                        $pagos[$m]["FechaLimitePago"] = $fechaDateDue;
                    }
                    $pagos[$m]["IdPago"] = [
                        [
                            "Value"             => $valueP->id
                        ]
                    ];
                    $m++;
                }
            }else if(!$payments_data){ // si no existe ningun pago
                $payment_type = $this->getPaymentMethodSimba($sale);
                $pagos[$m] = [
                    "Id"    =>   [
                        "Value"         => "$payment_type"
                    ],
                    "CodigoMedioDePago" => [
                        "Value"             => "ZZZ"
                    ],
                    "FechaLimitePago"       => $fechaDateDue
                ];
                $pagos[$m]["IdPago"] = [
                    [
                        "Value"                 => '1'
                    ]
                ];
            }

            $request = [
                "Parametros" => [
                    "VersionDocElectronico" => "4.0", // Es la versión del documento eletrónico que se utiliza. Debe contener el literal "4.0"
                    "NombreSistemaEmisor"   => $issuer_data->site_name, //Nombre del Software que envía, identificamos para nuestro control el sistema emisor (origen, ERP, contable)
                    "VersionSistemaEmisor"  => $issuer_data->version, //Es la versión del sistema que envía la petición. (Varía por cliente)
                    "ModoRespuesta"         => "1", // Es la forma en que se recibirá la respuesta. Por ahora dejar código en 1
                    "TipoAmbiente"          => $issuer_data->fe_work_environment, //Tipo ambiente (Podemos separar las facturas de producción y pruebas)
                    "TokenEmpresa"          => $security_token->security_token, //Token que identifica la empresa que esta enviando el documento ante el proveedor Tecnológico, mecanismo adicional de autenticación. Proporcionado por SIMBA para cada NIT, solicitado por el OFE a SIMBA
                    "TipoReporte"           => "1", //Identificador de la representacion grafica, tantas como desee o contrate el emisor, aquí identifica cuál representación usar. 1. Por Defecto
                    "Personalizacion"       => "20",
                    // "PasswordEmpresa"       => "",  // Sin comentarios en el DEV-FEL-0.15 json especifica que en el momento no se usa
                    "ContactoReceptor"      => [
                        [
                            "CorreoElectronico"             => $biller_data->email,
                            "IdEtiquetaUbicacionCorreo"     => "1",
                            "SoloEnvioCasoDeFalloSpecified" => false
                        ],
                        [
                            "CorreoElectronico"             => $biller_data->email,
                            "IdEtiquetaUbicacionCorreo"     => "2",
                            "SoloEnvioCasoDeFalloSpecified" => false
                        ]
                    ],
                    "IndicadoresAdicionales"    =>  [
                        [
                            "NombreIndicador"   =>   "SKIPVALIDDIANLOGI",
                            "Activado"          => true
                        ],
                        [
                            "NombreIndicador"   =>   "SKIPVALIDDIANREQU",
                            "Activado"          => true
                        ]
                    ],    
                ],
                "Encabezado" => [
                    "TipoDocElectronico"    => "$typeDocElectronic", //Tipo de documento (Factura,N.crédito,N.débito). Se complementa con el campo Tipo de Factura o tipo de Nota para indicar FVenta, FContingencia, FExportación, otros
                    "IdPersonalizacion"     => [ // Tipo de Operación
                        "Value"                 => ($sale_origen) ? "20" : "22"
                    ],
                    "PrefijoDocumento"      => $resolution_data->sales_prefix, // Prefijo de numeración de Documento Electrónico
                    "NumeroDocumento"       => $consecutive_sale, // consecutivo
                    "FechaYHoraDocumento"   => $fechaEmision, // Fecha de emisión: Fecha de emisión de la factura a efectos fiscales. (Hora) de emisión
                    "FechaDeVencimientoSpecified" => false,
                    "TipoDeFactura"         => [
                        "Value"                 => "3",  //código: facturas de venta, y transcripciones
                    ],
                    "CodigoMoneda"          => [
                        "Value"                 => 'COP', //Divisa de la Factura: Divisa o moneda consolidada aplicable a toda la factura
                    ],
                    "CantidadLineas"        => "".count($items_sale)."", //Cantidad de líneas de la factura electrónica
                    "CantidadLineasSpecified" => true,
                    // getPeriodFac($sale);
                    "RespuestaMotivoNota" => [
                        [
                            "CodRespuesta" => [
                                "Value" => 6
                            ],
                            "Descripcion" => [
                                ["Value" => $sale->note]
                            ],
                            "FechaEfectivaSpecified" => false,
                            "HoraEfectivaSpecified" => false
                        ]
                    ],
                    "TipoDeNotaCredito" => ["Value" => CREDIT_NOTE]
                ],
                "Terceros" =>[
                    "TerceroProveedorContable" => [ //EMISOR - Obligado a Facturar, Tercero Proveedor, Facturador Electrónico Proveedor
                        "IdAdicional"           =>[
                                [
                                    "Value"                 => $this->Settings->tipo_persona   // tipo persona emisor
                                ]
                        ],
                        "Tercero"               =>[
                            "IndicaATravesDeSpecified" => false,
                            "IndicaAtencionASpecified" => false,
                            "CodigoClasificacionIndustria"  => [
                                "Value"             => isset($ciiu_code->code) ? $ciiu_code->code : ''  //Actividad Economica (CIIU)
                            ],
                            "IdTercero"             =>[
                                [
                                    "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                    "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                    "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                ]
                            ],
                            "NombreTercero"         => [
                                [
                                    "Value"                     =>$this->Settings->razon_social  // razon social
                                ]
                            ],
                            "UbicacionFisica"       =>[
                                "Direccion"             =>[
                                    "Id"                    =>[
                                        "Value"                 => substr($biller_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad
                                    ],
                                    "Departamento"          =>[
                                        "Value"                 => $biller_data->state  // Departamento dentro de una Organización (Estructura Jerárquica)
                                    ],
                                    "Ciudad"                =>[
                                        "Value"                 => $biller_data->city  // Ciudad
                                    ],
                                    "ZonaPostal"            =>[
                                        "Value"                 => $biller_data->postal_code // codigo postal
                                    ],
                                    "SubdivisionPais"       =>[
                                        "Value"                 => $biller_data->state // Subdivisión del pais (Departamento, Estado, Provincia)
                                    ],
                                    "SubdivisionPaisCodigo" =>[
                                        "Value"                 => substr(substr($biller_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros
                                    ],
                                    "LineaDireccion"        =>[
                                        [
                                            "TextoLinea"        => [
                                                "Value"             =>$biller_data->address // Nombre de una calle
                                            ]
                                        ]
                                    ],
                                    "Pais"                  =>[
                                        "Codigo"                => [
                                            "Value"                 => $biller_data->codigo_iso // País de la dirección
                                        ],
                                        "Nombre"                =>[
                                            "IdLenguaje"            => "es",
                                            "Value"                 => ucfirst(mb_strtolower($biller_data->NOMBRE ))  // país del emisor
                                        ]
                                    ]
                                ]
                            ],
                            "EsquemaTributarioTercero" =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $this->Settings->razon_social
                                    ],
                                    "NumeroIdTributario"    =>[
                                        "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                        "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                        "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                    ],
                                    "NivelTributario"       =>[
                                        "ListaNombre"           => $this->get_name_regimen($biller_data->tipo_regimen),
                                        "Value"                 => $stringObligations
                                    ],
                                    "DireccionParaImpuestos"=>[
                                        "Id"                    =>[
                                            "Value"                 => substr($biller_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad
                                        ],
                                        "Departamento"          =>[
                                            "Value"                 => $biller_data->state  // Departamento dentro de una Organización (Estructura Jerárquica)
                                        ],
                                        "Ciudad"                =>[
                                            "Value"                 => $biller_data->city  // Ciudad
                                        ],
                                        "ZonaPostal"            =>[
                                            "Value"                 => $biller_data->postal_code // codigo postal
                                        ],
                                        "SubdivisionPais"       =>[
                                            "Value"                 => $biller_data->state // Subdivisión del pais (Departamento, Estado, Provincia)
                                        ],
                                        "SubdivisionPaisCodigo" =>[
                                            "Value"                 => substr(substr($biller_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros
                                        ],
                                        "LineaDireccion"        =>[
                                            [
                                                "TextoLinea"        => [
                                                    "Value"             =>$biller_data->address // Nombre de una calle
                                                ]
                                            ]
                                        ],
                                        "Pais"                  =>[
                                            "Codigo"                => [
                                                "Value"                 => $biller_data->codigo_iso // País de la dirección
                                            ],
                                            "Nombre"                =>[
                                                "IdLenguaje"            => "es",
                                                "Value"                 => ucfirst(mb_strtolower($biller_data->NOMBRE))    // país del emisor
                                            ]
                                        ]
                                    ],
                                    "EsquemaTributario"     =>[
                                        "Id"                    =>[
                                            "Value"                 => $this->get_id_tributo($biller_data->tipo_regimen)
                                        ],
                                        "Nombre"                =>[
                                            "Value"                 => $this->get_name_tributo($biller_data->tipo_regimen)
                                        ]
                                    ]
                                ]
                            ],
                            "EntidadLegalTercero"       =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $this->Settings->razon_social   // Razón Social, Nombre con el cual aparece registrado legalmente el tercero
                                    ],
                                    "NumeroIdLegal"         =>[
                                        "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                        "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                        "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                    ],
                                    "FechaRegistroSpecified" =>             false,
                                    "FechaExpiracionRegistroSpecified" =>   false,
                                    "IndicaPropietarioPersonaSpecified" =>  false,
                                    "IndicaAccionesPagadasSpecified" =>     false,
                                    "EsquemaRegistroCorporativo" =>[
                                        "Id"                        =>[
                                            "Value"                     => $resolution_data->sales_prefix,
                                        ],
                                        "Nombre"                    =>[
                                            "Value"                     => $this->Settings->matricula_mercantil
                                        ]
                                    ]
                                ]
                            ],
                            "Contacto"                  =>[
                                "Nombre"                    =>[
                                    "Value"                     => $biller_data->name
                                ],
                                "Telefono"                  =>[
                                    "Value"                     => $biller_data->phone
                                ],
                                "Email"                     =>[
                                    "Value"                     => $biller_data->email
                                ],
                                "Nota"                      => []
                            ]
                        ]
                    ],
                    "TerceroClienteContable" =>[  // RECEPTOR - Tercero Adquiriente, Tercero Cliente del Documento Electrónico
                        "IdAdicional"           =>[
                            [
                                "Value"             => $customer_data->type_person
                            ]
                        ],
                        "Tercero"               =>[
                            "IndicaATravesDeSpecified" =>    false,
                            "IndicaAtencionASpecified" =>    false,
                            "IdTercero"             =>[
                                [
                                    "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                                    "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                    "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                ]
                            ],
                            "NombreTercero"         => [
                                [
                                    "Value"                     =>$customer_data->name  // nombre del adquiriente
                                ]
                            ],
                            "UbicacionFisica"       =>[
                                "Direccion"             =>[
                                    "Id"                    =>[
                                        "Value"                 => substr($customer_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad del adquiriente
                                    ],
                                    "Departamento"          =>[
                                        "Value"                 => $customer_data->state  // Departamento dentro de una Organización (Estructura Jerárquica) del adquiriente
                                    ],
                                    "Ciudad"                =>[
                                        "Value"                 => $customer_data->city  // Ciudad del adquiriente
                                    ],
                                    "ZonaPostal"            =>[
                                        "Value"                 => $customer_data->postal_code // codigo postal del adquiriente
                                    ],
                                    "SubdivisionPais"       =>[
                                        "Value"                 => $customer_data->state // Subdivisión del pais (Departamento, Estado, Provincia) del adquiriente
                                    ],
                                    "SubdivisionPaisCodigo" =>[
                                        "Value"                 => substr(substr($customer_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros  del adquiriente
                                    ],
                                    "LineaDireccion"        =>[
                                        [
                                            "TextoLinea"        => [
                                                "Value"             =>$customer_data->address // Nombre de una calle del adquiriente
                                            ]
                                        ]
                                    ],
                                    "Pais"                  =>[
                                        "Codigo"                => [
                                            "Value"                 => $customer_data->codigo_iso // País de la dirección del adquiriente
                                        ],
                                        "Nombre"                =>[
                                            "IdLenguaje"            => "es",
                                            "Value"                 => ucfirst(mb_strtolower($customer_data->NOMBRE))   // país del emisor del adquiriente
                                        ]
                                    ]
                                ]
                            ],
                            "EsquemaTributarioTercero" =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $customer_data->name  // nombre del adquiriente
                                    ],
                                    "NumeroIdTributario"    =>[
                                        "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                                        "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                        "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                    ],
                                    "NivelTributario"       =>[
                                        "ListaNombre"           => $this->get_name_regimen($customer_data->tipo_regimen),
                                        "Value"                 => $stringObligationAdquirer
                                    ],
                                    "DireccionParaImpuestos"=>[
                                        "Id"                    =>[
                                            "Value"                 => substr($customer_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad del aquiriente
                                        ],
                                        "Departamento"          =>[
                                            "Value"                 => $customer_data->state  // Departamento dentro de una Organización (Estructura Jerárquica) del adquiriente
                                        ],
                                        "Ciudad"                =>[
                                            "Value"                 => $customer_data->city  // Ciudad del adquiriente
                                        ],
                                        "ZonaPostal"            =>[
                                            "Value"                 => $customer_data->postal_code // codigo postal del adquiriente
                                        ],
                                        "SubdivisionPais"       =>[
                                            "Value"                 => $customer_data->state // Subdivisión del pais (Departamento, Estado, Provincia) del adquiriente
                                        ],
                                        "SubdivisionPaisCodigo" =>[
                                            "Value"                 => substr(substr($customer_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros del adquirente
                                        ],
                                        "LineaDireccion"        =>[
                                            [
                                                "TextoLinea"        => [
                                                    "Value"             =>$customer_data->address // Nombre de una calle del adquirente
                                                ]
                                            ]
                                        ],
                                        "Pais"                  =>[
                                            "Codigo"                => [
                                                "Value"                 => $customer_data->codigo_iso // País de la dirección del adquiriente
                                            ],
                                            "Nombre"                =>[
                                                "IdLenguaje"            => "es",
                                                "Value"                 => ucfirst(mb_strtolower($customer_data->NOMBRE))   // país del emisor del adquiriente
                                            ]
                                        ]
                                    ],
                                    "EsquemaTributario"     =>[
                                        "Id"                    =>[
                                            "Value"                 => $this->get_id_tributo($customer_data->tipo_regimen)
                                        ],
                                        "Nombre"                =>[
                                            "Value"                 => $this->get_name_tributo($customer_data->tipo_regimen)
                                        ]
                                    ]
                                ]
                            ],
                            "EntidadLegalTercero"       =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $customer_data->name   // Razón Social, Nombre con el cual aparece registrado legalmente el tercero
                                    ],
                                    "NumeroIdLegal"         =>[
                                        "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                                        "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                        "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                    ],
                                    "FechaRegistroSpecified" =>  false,
                                    "FechaExpiracionRegistroSpecified" => false,
                                    "IndicaPropietarioPersonaSpecified" => false,
                                    "IndicaAccionesPagadasSpecified" => false,
                                    "EsquemaRegistroCorporativo" =>[
                                        "Id"                        =>[
                                            "Value"                     => $resolution_data->sales_prefix,
                                        ]
                                    ]
                                ]
                            ],
                            "Contacto"                  =>[
                                "Nombre"                    =>[
                                    "Value"                     => $customer_data->name
                                ],
                                "Telefono"                  =>[
                                    "Value"                     => $customer_data->phone
                                ],
                                "Email"                     =>[
                                    "Value"                     => $customer_data->email
                                ],
                                "Nota"                      =>[]
                            ],
                            "Personas"                  =>[
                                [
                                    "PrimerNombre" => [
                                        "Value" => $customer_data->first_name ." ".$customer_data->first_lastname
                                ],
                                "FechaNacimientoSpecified" => false
                                ]
                            ]
                        ]
                    ],
                    // "TerceroRpteFiscal"     =>[
                    //     "IdTercero"             => [
                    //         [
                    //             "SmaIdCodigo"               => $customer_data->digito_verificacion, //Digito de verificación del adquiriente
                    //             "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                    //             "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                    //         ]
                    //     ]
                    // ]
                ],
                "Lineas"    => $arrayItems,
                "Referencias" => $this->get_reference_returns($sale_origen, $sale),
                "AgregadoComercial" =>[
                    "MediosDePago"  => $pagos,
                    "CargoDescuento" => $this->get_general_discount($sale, 'COP', true),
                ],
                "Notas"     => [],
                "Totales"   =>[
                    "TotalImpuestos"    => $totalTaxArray,
                    "TotalRetenciones"  => $totalWithHoldingsArray,
                    "TotalMonetario"    =>[
                        "ValorBruto"            =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( abs($totalBaseFormated), NUMBER_DECIMALS_SIMBA).""
                        ],
                        "ValorBaseImpuestos"    =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( abs($totalBaseImpFormated), NUMBER_DECIMALS_SIMBA).""
                        ],
                        "TotalMasImpuestos"     =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( abs($totalTImpuesto) + abs($totalBaseFormated), NUMBER_DECIMALS_SIMBA).""
                        ],
                        "TotalDescuentos"     =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( abs($totalDiscount), NUMBER_DECIMALS_SIMBA).""
                        ],
                        "ValorAPagar"           =>[
                            "IdMoneda"              => 'COP',
                            "Value"                 => "".$this->sma->formatDecimal( abs($totalBaseFormated) + abs($totalTImpuesto) - abs($totalDiscount), NUMBER_DECIMALS_SIMBA).""
                        ]
                    ]
                ]
            ];
            $request = $this->get_billing_period_nc_noReference($request, $sale, $concept_product_id);
            $request = $this->get_params_with_other_technology_provider($request, $sale_origen);
            $request = $this->get_another_currency($request, $sale, $fechaEmision);
            return json_encode($request, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        public function simbaBuildJsonDebitNote ($documentElectronic){
            $totImp2 = [];
            $issuer_data = $this->site->get_setting(); // variable que almacena los datos contenidos en el settings
            $security_token = $this->get_secuirty_token($issuer_data->fe_technology_provider, $issuer_data->fe_work_environment); // token que esta en la tabla configuration provider
            $typeDocument = $this->site->getTypeElectronicDocument($documentElectronic->sale_id); // tipo de factura
            $typeDocElectronic = $this->get_typeDocElectronic($typeDocument); // tipo de documento
            $sale = $this->site->getSaleByID($documentElectronic->sale_id); // datos de la venta
            $idCustomization = $this->get_idCustomizacion($sale->order_tax_aiu_id); // id de personalizacion segun tabla 13.1.5. Tipos de operación
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id); // document_types
            $consecutive_sale = $this->get_consecutiveSale($sale->reference_no); // metodo para obtener el consecutivo de la venta
            $fechaEmision = $this->current_date_format($sale->date);  // fecha hora de emision de factura en formato UTC
            $fechaDateDue = $this->current_date_format(($sale->due_date) ? $sale->due_date : $sale->date);
            $items_sale = $this->site->getAllSaleItems($documentElectronic->sale_id); // items de la factura
            $ciiu_code = $this->get_ciiuCode($this->Settings->ciiu_code); // codigo de actividad economica
            $biller_data = $this->site->getCompanyByID($documentElectronic->biller_id); // datos del emisor
            $customer_data = $this->site->getCompanyByID($documentElectronic->customer_id); // datos del adquiriente
            $types_issuer_obligations = $this->site->getTypesCustomerObligations(1, TRANSMITTER); // obligaciones del emisor
            $customer_obligations = $this->site->getTypesCustomerObligations($customer_data->id, ACQUIRER); // obligaciones del adquiriente
            $payments_data = $this->site->getSalePayments($documentElectronic->sale_id);
            $paymentMeanCode = $this->getPaymentMeanSimba($sale);
            $sale_origen = $this->site->getSaleByID($sale->reference_invoice_id); // datos de la venta de origen
            $stringObligations = '';
            foreach ($types_issuer_obligations as $keyTo => $valueTo) {
                $stringObligations .= $valueTo->types_obligations_id . ";";
            }
            $stringObligations = trim($stringObligations, ';');

            $stringObligationAdquirer = '';
            foreach ($customer_obligations as $keyToc => $valueToc) {
                $stringObligationAdquirer .= $valueToc->types_obligations_id . ";";
            }
            $stringObligationAdquirer = trim($stringObligationAdquirer, ';');

            /******************************* NODO LINEAS ******************************/
            $n=1;
            /** variables para impuestos **/
            $totalTax = 0;
            $totalBase = 0;
            $totalTotal = 0;
            $totBaseImp = 0;

            foreach ($items_sale as $key => $value) {
                $totalBase += $value->net_unit_price * $value->quantity;
                $totalTotal += $totalTax + $totalBase;
                $taxArrayProduct = [];
                $motivoNota = $value->product_name;
                $concept_note_id = $value->product_code;

                /* excento con 0 excluido no se envia, bajo esta observación realizada por el proveedor tecnologico si la linea del producto esta excenta se envia con 0 y si esta excluido no se envia registro*/
                if ($value->excluded != 1) {
                    $taxArrayProduct = $this->get_tax_array_t($value, 'COP');
                }
                $arrayItems[] = [
                    "Id"    => [
                        "Value" => "".$n.""  //Número de Línea: Número de Línea
                    ],
                    "Nota"  => [
                        [
                            "Value" => "".$value->product_name."" // Información Adicional: Texto libre para añadir información adicional al artículo.
                        ]
                    ],
                    "Cantidad" => [
                        "CodUnidad" => "NIU",   // unidad de medida estandar
                        "Value"     => "" .abs($value->quantity). ""   // cantidad de unidades
                    ],
                    "ValorNeto" => [
                        "IdMoneda"  => 'COP',  // codigo moneda
                        "Value"     => "".$this->sma->formatDecimal($value->net_unit_price * abs($value->quantity), NUMBER_DECIMALS_SIMBA).""  // subtotal
                    ],
                    "FechaVigenciaImpuestoSpecified" => false,
                    "IndicaEsGratisSpecified" => false,
                    "ReferenciaLineaDocPedido" => [],
                    "Item"                  =>[
                        "Descripcion"           => [
                            [
                                "Value"                 =>$value->product_name,
                            ]
                        ],
                        "CantidadEmpaque" => [
                            "CodUnidad" => "NIU",
                            "Value" => "$value->quantity"
                        ],
                        "ItemsPorEmpaqueSpecified" => false,
                        "IndicaDesdeCatalogoSpecified" => false,
                        "Nombre"                => [
                            "Value"                 => $value->product_name,
                        ],
                        "IndicadorDePeligroSpecified" => false,
                        "IdItemEstandar"        =>[
                            "Id"                    =>[
                                "SmaIdCodigo"           => "999",
                                "SmaIdNombre"           => $value->product_name,
                                "Value"                 => $value->product_name
                            ]
                        ],
                        "PropiedadesAdicionalesItem" => [
                            [
                                "Nombre" => [
                                    "Value" => "UnidadMedidaNombre"
                                ],
                                "Valor" => [
                                    "Value" => "NIU"
                                ]
                            ]
                        ],
                    ],
                    "Precio"                =>[
                        "ValorPrecio"           =>[
                            "IdMoneda"              => 'COP',
                            "Value"                 => "".$this->sma->formatDecimal($value->net_unit_price, NUMBER_DECIMALS_SIMBA)."",
                        ],
                        "CantidadBase"      =>[
                            "CodUnidad"         => "NIU",
                            "Value"             => "" .abs($value->quantity) .""
                        ],
                        "FactorConvAUnidadPedidoSpecified" => false,
                    ],
                    "TotalImpuesto" => $taxArrayProduct,
                ];

                if ($value->excluded != 1) {
                    $totBaseImp += ($value->net_unit_price * $value->quantity);
                    $totalTax += (($value->net_unit_price * $value->quantity) * $value->rate) / 100;
                    if (isset($totImp2[$value->code_fe][$value->rate])) {
                        $totImp2[$value->code_fe][$value->rate]['base'] += ($value->net_unit_price * $value->quantity);
                        $totImp2[$value->code_fe][$value->rate]['total'] += (($value->net_unit_price * $value->quantity) * $value->rate) /100;
                    }else{
                        $totImp2[$value->code_fe][$value->rate] = [
                            'code_fe' => $value->code_fe,
                            'rate' => $value->rate,
                            'base' => $value->net_unit_price * $value->quantity,
                            'total' => (($value->net_unit_price * $value->quantity) * $value->rate) / 100,
                            'code_fe_name' => $value->code_fe_name,
                        ];
                    }
                }
                if ($value->tax_rate_2_id != 0) {
                    $totalTax += $value->item_tax_2;
                    $rateSecondTax = $value->item_tax_2;
                    $perSeconImp = '';
                    if ($value->mililiters == '0') {
                        $perSeconImp = intval(str_replace('%', '', $value->tax_2));
                    }else{
                        $perSeconImp = $value->tax_2;
                    }

                    if (isset($totImp2[$value->codeTax2][$value->rate])) {
                        $totImp2[$value->codeTax2][$value->rate]['base'] += $value->net_unit_price * $value->quantity;
                        $totImp2[$value->codeTax2][$value->rate]['total'] += $value->item_tax_2;
                    }else{
                        $totImp2[$value->codeTax2][$value->rate] = [
                            'code_fe' => $value->codeTax2,
                            'rate' => $perSeconImp,
                            'base' => $value->net_unit_price * $value->quantity,
                            'total' => $value->item_tax_2,
                            'code_fe_name' => $value->nameTax2,
                        ];
                    }
                }
                $n++;
            }
            /******************************* NODO LINEAS ******************************/
            $totalBaseFormated = $this->sma->formatDecimal($totalBase, NUMBER_DECIMALS_SIMBA); // total Base ya va con formato
            $totalTaxFormated = $this->sma->formatDecimal($totalTax, NUMBER_DECIMALS_SIMBA); // total Impuesto con formato
            $totalBaseImpFormated = $this->sma->formatDecimal($totBaseImp, NUMBER_DECIMALS_SIMBA); // total Impuesto con formato
            $totalDiscount = $this->sma->formatDecimal($sale->total_discount, NUMBER_DECIMALS_SIMBA); // total Descuentos

            /*********************************** Total impuestos ******************************************************************/
            $totalTaxArray = [];
            $totalTImpuesto = 0;
            foreach ($totImp2 as $keyti2 => $valueti2) {
                $totalTaxT = 0;
                $totalTaxAux = [];
                foreach ($valueti2 as $keytii2 => $valuetii2) {
                    if ($valuetii2['code_fe_name'] == 'ICL') {
                        $percen = ($valuetii2['total'] / $valuetii2['base']) * 100;
                        if ($valuetii2['mililiters'] != 750) {
                            $tax_2 = (($valuetii2['mililiters'] * $valuetii2['nominal']) / 750) * $valuetii2['degrees'];
                        }else{
                            $tax_2 = $valuetii2['mililiters'] * $valuetii2['degrees'];
                        }
                        // $totalTaxT += $tax_2;
                        $totalTaxT += $valuetii2['total'];

                        $totalTaxAux[] = [
                            "BaseImponible"     => [
                                "IdMoneda"          => 'COP',
                                "Value"             => "".$this->sma->formatDecimal($valuetii2['base'], NUMBER_DECIMALS_SIMBA).""
                            ],
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => 'COP',
                                "Value"             => "".$this->sma->formatDecimal($valuetii2['total'], NUMBER_DECIMALS_SIMBA).""
                            ],
                            // "BaseUnitMeasure" => [
                            //     "Value" => "" .$valuetii2['degrees']. ""
                            // ],
                            // "PerUnitAmount" => [
                            //     "Value" => "" .$valuetii2['nominal']. ""
                            // ],
                            "CategoriaImpuesto" =>[
                                "Porcentaje"        => "" .$this->sma->formatDecimal($percen, NUMBER_DECIMALS). "",
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $valuetii2['code_fe_name']
                                    ]
                                ]
                            ]
                        ];
                    }
                    if ($valuetii2['code_fe_name'] == 'IBUA') {
                        $percen = ($valuetii2['total'] / $valuetii2['base']) * 100;
                        if ($valuetii2['mililiters'] != 750) {
                            $tax_2 = (($valuetii2['mililiters'] * $valuetii2['nominal']) / 750) * $valuetii2['degrees'];
                        }else{
                            $tax_2 = $valuetii2['mililiters'] * $valuetii2['degrees'];
                        }
                        // $totalTaxT += $tax_2;
                        $totalTaxT += $valuetii2['total'];

                        $totalTaxAux[] = [
                            "BaseImponible"     => [
                                "IdMoneda"          => 'COP',
                                "Value"             => "".$this->sma->formatDecimal($valuetii2['base'], NUMBER_DECIMALS_SIMBA).""
                            ],
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => 'COP',
                                "Value"             => "".$this->sma->formatDecimal($valuetii2['total'], NUMBER_DECIMALS_SIMBA).""
                            ],
                            // "BaseUnitMeasure" => [
                            //     "Value" => "" .$valuetii2['degrees']. ""
                            // ],
                            // "PerUnitAmount" => [
                            //     "Value" => "" .$valuetii2['nominal']. ""
                            // ],
                            "CategoriaImpuesto" =>[
                                "Porcentaje"        => "" .$this->sma->formatDecimal($percen, NUMBER_DECIMALS). "",
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $valuetii2['code_fe_name']
                                    ]
                                ]
                            ]
                        ];
                    }
                    if ($valuetii2['code_fe_name'] != 'ICL' && $valuetii2['code_fe_name'] != 'IBUA'){
                        $totalTaxT += abs($valuetii2['total']);
                        $totalTaxAux[] = [
                            "BaseImponible"     => [
                                "IdMoneda"          => 'COP',
                                "Value"             => "".$this->sma->formatDecimal(abs($valuetii2['base']), NUMBER_DECIMALS_SIMBA).""
                            ],
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => 'COP',
                                "Value"             => "".$this->sma->formatDecimal(abs($valuetii2['total']), NUMBER_DECIMALS_SIMBA).""
                            ],
                            "CategoriaImpuesto" =>[
                                "Porcentaje"        => "" .$this->sma->formatDecimal($valuetii2['rate'], NUMBER_DECIMALS). "",
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $valuetii2['code_fe_name']
                                    ]
                                ]
                            ]
                        ];
                    }
                }
                // if ($totalTax > 0) {
                    $totalTaxArray[] = [
                        "ValorImpuesto"     =>[
                            "IdMoneda"          =>  'COP',
                            "Value"             =>  "".$this->sma->formatDecimal($totalTaxT, NUMBER_DECIMALS_SIMBA)."",
                        ],
                        "ValorAjusteRedondeo" => [
                            "IdMoneda"          => "COP",
                            "Value"             =>  0.0
                        ],
                        "IndicaEsSoloEvidenciaSpecified"    => false,
                        "IndicaImpuestoIncluidoSpecified"   => false,
                        "SubTotalImpuesto" => $totalTaxAux
                    ];
                // }
                $totalTImpuesto += $totalTaxT;
            }
            /*********************************** Total impuestos ******************************************************************/

            /*********************************** total retenciones ****************************************************************/
            $withHoldingsTot = $sale->rete_fuente_total + $sale->rete_iva_total + $sale->rete_ica_total;
            $withHoldings = $this->getWithholdingsSimba($sale);
            $totalWithHolding = $this->sma->formatDecimal($withHoldingsTot, NUMBER_DECIMALS_SIMBA); // totalRetenciones ya va con formato
            $arrayWithAux = [];
            $totalWithHoldingsArray = [];

            if (count($withHoldings)> 0) {
                foreach ($withHoldings as $keyw => $valuew) {
                    $arrayWithAux[] = [
                        "BaseImponible"     => "".$this->sma->formatDecimal($valuew['Base'], NUMBER_DECIMALS_SIMBA)."",
                        "ValorImpuesto"     => "".$this->sma->formatDecimal($valuew['Total'], NUMBER_DECIMALS_SIMBA)."",
                        "CategoriaImpuesto" =>[
                            "Porcentaje"        => "". $this->sma->formatDecimal($valuew['Valor'], NUMBER_DECIMALS_SIMBA)  ."",
                            "EsquemaTributario" =>[
                                "Id"                => $valuew['Codigo'],
                                "Nombre"            => $valuew['Name']
                            ]
                        ]
                    ];
                }
                $totalWithHoldingsArray = [
                    "ValorImpuesto"     =>[
                        "IdMoneda"          =>  'COP',
                        "Value"             =>  "".$this->sma->formatDecimal($withHoldingsTot, NUMBER_DECIMALS_SIMBA).""
                    ],
                    "SubTotalImpuesto"  =>$arrayWithAux
                ];
            }
            /*********************************** total retenciones ****************************************************************/

            /*********************aca vamos a crear un array que va a rellenar el nodo Medios de pago del Json */
            $m=0;
            if ($payments_data) { // en el caso que existan los pagos
                foreach ($payments_data as $keyP => $valueP) { // iteramos en todos los pagos existentes
                    $payment_type = $this->getPaymentMethodSimba($sale);
                    $pagos[$m] = [
                        "Id"    =>   [
                            "Value"         => "$payment_type"
                        ],
                        "CodigoMedioDePago" => [
                            "Value"             => $paymentMeanCode
                        ],
                        "FechaLimitePagoSpecified" => false,
                        "IdInstruccion" => [
                          "Value" =>  ""
                        ],
                        "NotaInstruccion" => [
                          [
                            "Value" => "" .$valueP->note. ""
                          ]
                        ],
                    ];
                    if ($payment_type == 2) {
                        $pagos[$m]["FechaLimitePago"] = $fechaDateDue;
                    }
                    $pagos[$m]["IdPago"] = [
                        [
                            "Value"             => $valueP->id
                        ]
                    ];
                    $m++;
                }
            }else if(!$payments_data){ // si no existe ningun pago
                $payment_type = $this->getPaymentMethodSimba($sale);
                $pagos[$m] = [
                    "Id"    =>   [
                        "Value"         => "$payment_type"
                    ],
                    "CodigoMedioDePago" => [
                        "Value"             => "ZZZ"
                    ],
                    "FechaLimitePago"       => $fechaDateDue
                ];
                $pagos[$m]["IdPago"] = [
                    [
                        "Value"                 => '1'
                    ]
                ];
            }

            $request = [
                "Parametros" => [
                    "VersionDocElectronico" => "4.0", // Es la versión del documento eletrónico que se utiliza. Debe contener el literal "4.0"
                    "NombreSistemaEmisor"   => $issuer_data->site_name, //Nombre del Software que envía, identificamos para nuestro control el sistema emisor (origen, ERP, contable)
                    "VersionSistemaEmisor"  => $issuer_data->version, //Es la versión del sistema que envía la petición. (Varía por cliente)
                    "ModoRespuesta"         => "1", // Es la forma en que se recibirá la respuesta. Por ahora dejar código en 1
                    "TipoAmbiente"          => $issuer_data->fe_work_environment, //Tipo ambiente (Podemos separar las facturas de producción y pruebas)
                    "TokenEmpresa"          => $security_token->security_token, //Token que identifica la empresa que esta enviando el documento ante el proveedor Tecnológico, mecanismo adicional de autenticación. Proporcionado por SIMBA para cada NIT, solicitado por el OFE a SIMBA
                    "TipoReporte"           => "1", //Identificador de la representacion grafica, tantas como desee o contrate el emisor, aquí identifica cuál representación usar. 1. Por Defecto
                    "Personalizacion"       => "20",
                    "ContactoReceptor"      => [
                        [
                            "CorreoElectronico"             => $biller_data->email,
                            "IdEtiquetaUbicacionCorreo"     => "1",
                            "SoloEnvioCasoDeFalloSpecified" => false,
                        ],
                        [
                            "CorreoElectronico"             => $biller_data->email,
                            "IdEtiquetaUbicacionCorreo"     => "2",
                            "SoloEnvioCasoDeFalloSpecified" => false,
                        ],
                    ],
                ],
                "Encabezado" => [
                    "TipoDocElectronico"    => "$typeDocElectronic", //Tipo de documento (Factura,N.crédito,N.débito). Se complementa con el campo Tipo de Factura o tipo de Nota para indicar FVenta, FContingencia, FExportación, otros
                    "IdPersonalizacion"     => [ // Tipo de Operación
                        "Value"                 => "30"
                    ],
                    "PrefijoDocumento"      => $resolution_data->sales_prefix, // Prefijo de numeración de Documento Electrónico
                    "NumeroDocumento"       => $consecutive_sale, // consecutivo
                    "IndicaCopiaSpecified"  => false,
                    "FechaYHoraDocumento"   => $fechaEmision, // Fecha de emisión: Fecha de emisión de la factura a efectos fiscales. (Hora) de emisión
                    "FechaDeVencimientoSpecified" => false,
                    "TipoDeNotaCredito"     => [
                        "Value"             => "92"
                    ],
                    "FechaTributariaSpecified" => false,
                    "CodigoMoneda"          => [
                        "Value"                 => 'COP', //Divisa de la Factura: Divisa o moneda consolidada aplicable a toda la factura
                    ],
                    "CantidadLineas"        => "".count($items_sale)."", //Cantidad de líneas de la factura electrónica
                    "CantidadLineasSpecified" => true,
                    "RespuestaMotivoNota"   => [
                        [
                            "IdReferencia"      => [
                                "Value"             => $sale_origen->reference_no
                            ],
                            "CodRespuesta"      => [
                                "Value"             => $this->site->get_debit_credit_note_concept_by_id($concept_note_id)->dian_code,
                            ],
                            "Descripcion"       => [
                                [
                                    "Value"         => "".$motivoNota."",
                                ]
                            ],
                            "FechaEfectivaSpecified"        => false,
                            "HoraEfectivaSpecified"         => false
                        ]
                    ]
                ],
                "Terceros" =>[
                    "TerceroProveedorContable" => [ //EMISOR - Obligado a Facturar, Tercero Proveedor, Facturador Electrónico Proveedor
                        "IdAdicional"           =>[
                                [
                                    "Value"                 => $this->Settings->tipo_persona   // tipo persona emisor
                                ]
                        ],
                        "Tercero"               =>[
                            "IndicaATravesDeSpecified" => false,
                            "IndicaAtencionASpecified" => false,
                            "CodigoClasificacionIndustria"  => [
                                "Value"             => isset($ciiu_code->code) ? $ciiu_code->code : ''  //Actividad Economica (CIIU)
                            ],
                            "IdTercero"             =>[
                                [
                                    "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                    "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                    "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                ]
                            ],
                            "NombreTercero"         => [
                                [
                                    "Value"                     =>$this->Settings->razon_social  // razon social
                                ]
                            ],
                            "UbicacionFisica"       =>[
                                "Direccion"             =>[
                                    "Id"                    =>[
                                        "Value"                 => substr($biller_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad
                                    ],
                                    "Departamento"          =>[
                                        "Value"                 => $biller_data->state  // Departamento dentro de una Organización (Estructura Jerárquica)
                                    ],
                                    "Ciudad"                =>[
                                        "Value"                 => $biller_data->city  // Ciudad
                                    ],
                                    "ZonaPostal"            =>[
                                        "Value"                 => $biller_data->postal_code // codigo postal
                                    ],
                                    "SubdivisionPais"       =>[
                                        "Value"                 => $biller_data->state // Subdivisión del pais (Departamento, Estado, Provincia)
                                    ],
                                    "SubdivisionPaisCodigo" =>[
                                        "Value"                 => substr(substr($biller_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros
                                    ],
                                    "LineaDireccion"        =>[
                                        [
                                            "TextoLinea"        => [
                                                "Value"             =>$biller_data->address // Nombre de una calle
                                            ]
                                        ]
                                    ],
                                    "Pais"                  =>[
                                        "Codigo"                => [
                                            "Value"                 => $biller_data->codigo_iso // País de la dirección
                                        ],
                                        "Nombre"                =>[
                                            "IdLenguaje"            => "es",
                                            "Value"                 => ucfirst(mb_strtolower($biller_data->NOMBRE ))  // país del emisor
                                        ]
                                    ]
                                ]
                            ],
                            "EsquemaTributarioTercero" =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $this->Settings->razon_social
                                    ],
                                    "NumeroIdTributario"    =>[
                                        "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                        "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                        "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                    ],
                                    "NivelTributario"       =>[
                                        "ListaNombre"           => $this->get_name_regimen($biller_data->tipo_regimen),
                                        "Value"                 => $stringObligations
                                    ],
                                    "DireccionParaImpuestos"=>[
                                        "Id"                    =>[
                                            "Value"                 => substr($biller_data->city_code, -5) // cinco ultimos numeros del codigo de la ciudad
                                        ],
                                        "Departamento"          =>[
                                            "Value"                 => $biller_data->state  // Departamento dentro de una Organización (Estructura Jerárquica)
                                        ],
                                        "Ciudad"                =>[
                                            "Value"                 => $biller_data->city  // Ciudad
                                        ],
                                        "ZonaPostal"            =>[
                                            "Value"                 => $biller_data->postal_code // codigo postal
                                        ],
                                        "SubdivisionPais"       =>[
                                            "Value"                 => $biller_data->state // Subdivisión del pais (Departamento, Estado, Provincia)
                                        ],
                                        "SubdivisionPaisCodigo" =>[
                                            "Value"                 => substr(substr($biller_data->city_code, -5), 0, 2)  // Subdivisión del país (código DANE) ISO, entre otros
                                        ],
                                        "LineaDireccion"        =>[
                                            [
                                                "TextoLinea"        => [
                                                    "Value"             =>$biller_data->address // Nombre de una calle
                                                ]
                                            ]
                                        ],
                                        "Pais"                  =>[
                                            "Codigo"                => [
                                                "Value"                 => $biller_data->codigo_iso // País de la dirección
                                            ],
                                            "Nombre"                =>[
                                                "IdLenguaje"            => "es",
                                                "Value"                 => ucfirst(mb_strtolower($biller_data->NOMBRE))    // país del emisor
                                            ]
                                        ]
                                    ],
                                    "EsquemaTributario"     =>[
                                        "Id"                    =>[
                                            "Value"                 => $this->get_id_tributo($biller_data->tipo_regimen)
                                        ],
                                        "Nombre"                =>[
                                            "Value"                 => $this->get_name_tributo($biller_data->tipo_regimen)
                                        ]
                                    ]
                                ]
                            ],
                            "EntidadLegalTercero"       =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $this->Settings->razon_social   // Razón Social, Nombre con el cual aparece registrado legalmente el tercero
                                    ],
                                    "NumeroIdLegal"         =>[
                                        "SmaIdCodigo"               => $this->Settings->digito_verificacion, //Digito de verificación
                                        "SmaIdNombre"               => $this->Settings->tipo_documento, //Tipo de identificación, según Lista DIAN
                                        "Value"                     => $this->Settings->numero_documento //Documento de Identificación - Número
                                    ],
                                    "FechaRegistroSpecified" => false,
                                    "FechaExpiracionRegistroSpecified" => false,
                                    "IndicaPropietarioPersonaSpecified" => false,
                                    "IndicaAccionesPagadasSpecified" => false,
                                    "EsquemaRegistroCorporativo" =>[
                                        "Id"                        =>[
                                            "Value"                     => $resolution_data->sales_prefix,
                                        ],
                                        "Nombre"                    =>[
                                            "Value"                     => $this->Settings->matricula_mercantil
                                        ]
                                    ]
                                ]
                            ],
                            "Contacto"                  =>[
                                "Nombre"                    =>[
                                    "Value"                     => $biller_data->name
                                ],
                                "Telefono"                  =>[
                                    "Value"                     => $biller_data->phone
                                ],
                                "Email"                     =>[
                                    "Value"                     => $biller_data->email
                                ],
                                "Nota"                      => []
                            ]
                        ]
                    ],
                    "TerceroClienteContable" =>[  // RECEPTOR - Tercero Adquiriente, Tercero Cliente del Documento Electrónico
                        "IdAdicional"           =>[
                            [
                                "Value"             => $customer_data->type_person
                            ]
                        ],
                        "Tercero"               =>[
                            "IndicaATravesDeSpecified" => false,
                            "IndicaAtencionASpecified" => false,
                            "IdTercero"             =>[
                                [
                                    "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                    "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                ]
                            ],
                            "NombreTercero"         => [
                                [
                                    "Value"                     =>$customer_data->name  // nombre del adquiriente
                                ]
                            ],
                            "EsquemaTributarioTercero" =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $customer_data->name  // nombre del adquiriente
                                    ],
                                    "NumeroIdTributario"    =>[
                                        "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                        "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                    ],
                                    "NivelTributario"       =>[
                                        "ListaNombre"           => $this->get_name_regimen($customer_data->tipo_regimen),
                                        "Value"                 => $stringObligationAdquirer
                                    ],
                                    "EsquemaTributario"     =>[
                                        "Id"                    =>[
                                            "Value"                 => $this->get_id_tributo($customer_data->tipo_regimen)
                                        ],
                                        "Nombre"                =>[
                                            "Value"                 => $this->get_name_tributo($customer_data->tipo_regimen)
                                        ]
                                    ]
                                ]
                            ],
                            "EntidadLegalTercero"       =>[
                                [
                                    "NombreRegistrado"      =>[
                                        "Value"                 => $customer_data->name   // Razón Social, Nombre con el cual aparece registrado legalmente el tercero
                                    ],
                                    "NumeroIdLegal"         =>[
                                        "SmaIdNombre"               => $customer_data->document_code, //Tipo de identificación, según Lista DIAN  del adquiriente
                                        "Value"                     => $customer_data->vat_no //Documento de Identificación - Número del adquiriente
                                    ],
                                    "FechaRegistroSpecified" =>  false,
                                    "FechaExpiracionRegistroSpecified" => false,
                                    "IndicaPropietarioPersonaSpecified" => false,
                                    "IndicaAccionesPagadasSpecified" => false,
                                    "EsquemaRegistroCorporativo" =>[
                                        "Id"                        =>[
                                            "Value"                     => $resolution_data->sales_prefix,
                                        ]
                                    ]
                                ]
                            ],
                            "Contacto"                  =>[
                                "Nombre"                    =>[
                                    "Value"                     => $customer_data->name
                                ],
                                "Telefono"                  =>[
                                    "Value"                     => $customer_data->phone
                                ],
                                "Email"                     =>[
                                    "Value"                     => $customer_data->email
                                ],
                                "Nota"                      =>[]
                            ],
                            "Personas"                  =>[
                                [
                                    "PrimerNombre" => [
                                        "Value" => $customer_data->first_name ." ".$customer_data->first_lastname
                                ],
                                "FechaNacimientoSpecified" => false
                                ]
                            ]
                        ]
                    ],
                ],
                "Lineas"    => $arrayItems,
                "AgregadoComercial" =>[
                    "MediosDePago"  => $pagos
                ],
                "Referencias" => $this->get_reference_returns($sale_origen, $sale),
                "Notas" => [],
                "Totales"   =>[
                    "TotalImpuestos"    => $totalTaxArray,
                    "TotalRetenciones"  => $totalWithHoldingsArray,
                    "TotalMonetario"    =>[
                        "ValorBruto"            =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( abs($totalBaseFormated), NUMBER_DECIMALS_SIMBA).""
                        ],
                        "ValorBaseImpuestos"    =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( abs($totalBaseImpFormated), NUMBER_DECIMALS_SIMBA).""
                        ],
                        "TotalMasImpuestos"     =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( abs($totalTImpuesto) + abs($totalBaseFormated), NUMBER_DECIMALS_SIMBA).""
                        ],
                        "TotalDescuentos"     =>[
                            "IdMoneda"              =>  'COP',
                            "Value"                 =>  "".$this->sma->formatDecimal( $totalDiscount, NUMBER_DECIMALS_SIMBA).""
                        ],
                        "ValorAPagar"           =>[
                            "IdMoneda"              => 'COP',
                            "Value"                 => "".$this->sma->formatDecimal( abs($totalBaseFormated) + abs($totalTImpuesto) - abs($totalDiscount), NUMBER_DECIMALS_SIMBA).""
                        ]
                    ]
                ]
            ];
            return json_encode($request, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);
        }

        public function simbaWSSendDocument($json, $document, $typeDocument)
        {
            $technologyProvider = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
            $security_token = $this->get_secuirty_token($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $technologyProvider->url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $json,
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]);
            $response = curl_exec($curl);
            curl_close($curl);

            log_message('debug', 'FE - SIMBA envío documento: '. $response);

            return json_decode($response, false);
        }

    /******************************************************************************/

    public function get_advance_payments_sale($sales_id)
    {
        $this->db->select("payments.amount, payments.date");
        $this->db->join("payments", "payments.sale_id = sales.id AND payments.date = sales.date", "INNER");
        $this->db->where("sales.id", $sales_id);
        $this->db->where("payments.paid_by", "deposit");
        $response = $this->db->get("sales");
        return $response->result();
    }

    private function format_date($date)
    {
        $formatted_date = explode(' ', $date);
        $array_date = ['date'=>$formatted_date[0]];
        if (isset($formatted_date[1])) {
            $array_date["time"] = $formatted_date[1] ."-05:00";
        }
        return (object) $array_date;
    }

    private function clear_document_number($document_number)
    {
        $document_number = str_replace('.', '', $document_number);
        $document_number = explode('-', $document_number);
        return $document_number[0];
    }

    private function get_tax_rate_by_code_fe()
    {
        $this->db->select("code_fe, rate");
        $this->db->order_by("code_fe", "asc");
        $this->db->order_by("rate", "asc");
        $response = $this->db->get("tax_rates");
        return $response->result();
    }

    private function change_status_accepted_cadena($document_id)
    {
        $sale_data = $this->site->getSaleByID($document_id);
        $prefix = strstr($sale_data->reference_no, '-', TRUE);
        $reference = str_replace('-','', $sale_data->reference_no);
        $document_type_code = ($sale_data->total > 0) ? '01' : '91';
        $technologyProviderConfiguration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor='.$this->Settings->numero_documento.'&id_documento='.$reference.'&codigo_tipo_documento='.$document_type_code.'&prefijo='.$prefix,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                'Content-Type: application/json',
                'Partnership-Id: 901090070'
            ],
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        $response = json_decode($response);

        if (!empty($response)) {
            if (isset($response->statusCode)) {
                if ($response->statusCode == 200) {
                    $updated_sale = $this->site->updateSale([
                        'fe_aceptado'=>2,
                        'fe_mensaje'=>'Documento aceptado por la DIAN',
                        'fe_mensaje_soporte_tecnico'=>'La Factura electrónica '.$reference.', ha sido autorizada. Código es estado '. $response->statusCode,
                        'fe_xml'=>$response->document
                    ], $document_id);

                    if ($updated_sale == TRUE) {
                        $response_request=[
                            'response'=>TRUE,
                            'message'=>'Documento aceptado por la DIAN. El cambio de estado del Documento fue realizado correctamente.'
                        ];
                    } else {
                        $response_request=[
                            'response'=>FALSE,
                            'message'=>'No fue posible cambiar el estado del Documento. Error al actualizar el estado del documento.'
                        ];
                    }
                } else if ($response->statusCode == 400) {
                    $response_request=[
                        'response'=>FALSE,
                        'message'=>'No fue posible cambiar el estado del Documento. El prefijo no coincide con el prefijo del id del documento.'
                    ];
                } else if ($response->statusCode == 404) {
                    $response_request=[
                        'response'=>FALSE,
                        'message'=>'No fue posible cambiar el estado del Documento. Registro no encontrado con los parámetros de búsqueda enviados.'
                    ];
                }
            } else {
                $response_request=[
                    'response'=>FALSE,
                    'message'=>'No fue posible cambiar el estado del Documento. Se ha perdido la conexión con el Web Services.'
                ];
            }
        } else {
            $response_request=[
                'response'=>FALSE,
                'message'=>'No fue posible cambiar el estado del Documento. No es posible realizar conexión con Web Services.'
            ];
        }

        return (object) $response_request;
    }

    public function receipt_delivery($invoice_data, $filename)
    {
        $sale = $this->site->getSaleByID($invoice_data->sale_id);
        $resolution_data = $this->site->site->getDocumentTypeById($sale->document_type_id);
        $typeElectronicDocument = $this->site->getTypeElectronicDocument($sale->id);
        $customer_data = $this->site->getCompanyByID($invoice_data->customer_id);
        $biller_data = $this->site->getCompanyByID($invoice_data->biller_id);
        $issuer_data = $this->site->get_setting();

        $sale_currency = "COP";
        $grand_total = (!empty($sale->sale_id)) ? $sale->grand_total * -1: $sale->grand_total ;

        if (isset($invoice_data->email) && !empty($invoice_data->email)) {
            $to = mb_strtolower($invoice_data->email);
        } else {
            $email_customer = $this->get_email_customer($customer_data->id);
            $to = mb_strtolower($email_customer->email);
        }

        if (empty($to)) {
            return "No fue posible enviar el correo electrónico. No se ha asignado el receptor del correo.";
        }

        $company_name = ($issuer_data->tipo_persona == NATURAL_PERSON) ? $issuer_data->razon_social . ' / '. $issuer_data->nombre_comercial : $company_name = $issuer_data->razon_social;
        $subject = $issuer_data->numero_documento .';'. htmlspecialchars_decode($issuer_data->razon_social) .';'. str_replace('-', '', $sale->reference_no) .';'. $typeElectronicDocument .';'. htmlspecialchars_decode($issuer_data->nombre_comercial);
        $message = '<!DOCTYPE html>
                    <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <title>Document</title>
                        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
                    </head>
                    <body style="color: #4e4e4e; font-family: Roboto, sans-serif;">
                        <table style="width: 800px" align="center">
                            <tbody style="width: 100%">
                                <tr style="text-align: left; width: 100%;">
                                    <th style="width: 40%;">
                                        <img src="'. base_url("assets/uploads/logos/". $biller_data->logo) .'" width="70%">
                                    </th>
                                    <th style=" text-align: right; width: 60%">
                                        <h2>Comprobante de documento electrónico</h2>
                                    </th>
                                </tr>
                                <tr>
                                    <td colspan="2" style="border-bottom:1px solid #979a9b"></td>
                                </tr>
                                <tr style="width: 100%;">
                                    <td style="font-weight:lighter; text-align: justify; width: 100%;" colspan="2">
                                        <p style="padding: 0 10px;">
                                            <strong>
                                            Señores <br>
                                            <label style="text-transform: uppercase;">'. $customer_data->name .'</label>
                                            </strong>
                                            <br>
                                            <br>
                                            Cordial saludo,
                                        </p>
                                        <br>
                                        <p style="padding: 0 10px">
                                            <strong><label style="text-transform: uppercase;">'. $company_name .'</label></strong>, mediante Resolución de numeración DIAN '. $resolution_data->num_resolucion .' hace entrega de la '. $resolution_data->nombre .' número <strong>'. $sale->reference_no .'</strong> adjunta a este correo con fecha <strong>'. $sale->date .'</strong> por un valor total de <strong>'. $sale_currency .' '. $this->sma->formatMoney($grand_total).'</strong>
                                        </p>
                                        <p style="padding: 0 10px">Para dar cumpliento a los términos del Decreto 2242, lo invitamos a realizar aceptación o rechazo del documento.</p>
                                    </td>
                                </tr>
                                <tr style="width: 100%;">
                                    <td colspan="2">
                                        <p style="padding: 0 10px;">
                                            De acuerdo con los términos del Decreto 1349 de 22 de agosto de 2016, si usted es obligado o voluntario para recibir documentos electrónicamente, debe realizar la confirmación o rechazo de los mismos, de lo contrario luego de tres días hábiles de recibido el correo, se entenderá como aceptados tácitamente y podrá iniciarse un proceso de factoring electrónico.
                                        </p>
                                    </td>
                                </tr>
                                <tr><td colspan="2" style="border-bottom:1px solid #979a9b"></td></tr>
                                <tr>
                                    <td colspan="2" style="text-align: center;">
                                        <p style="margin-bottom: 30px;">Documento electrónico generado desde Wappsi ERP</p>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <center>
                                            <img src="'. base_url("assets/uploads/logos/logo_wappsife.png") .'" width="45%">
                                        </center>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="middle">
                                        <p>
                                            Si desea implementar nuestra solucion de facturacion electrónica, contáctenos en: <br>
                                            <a href="mailto:fe@wappsi.com" style="text-decoration: none;">fe@wappsi.com</a>
                                        </p>
                                        <p>
                                            <a href="https://www.wappsi.com.co/" style="font-size: 18px; text-decoration: none;">wappsi.com</a>
                                        </p>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </body>
                    </html>';

        // if ($this->Settings->add_individual_attachments == YES) {
        //     $attachment = [
        //         FCPATH. 'files/electronic_billing/'.$filename.'.zip',
        //         FCPATH. 'files/electronic_billing/'.$filename.'.xml',
        //         FCPATH. 'files/electronic_billing/'.$filename.'.pdf'
        //     ];
        // } else {
            $attachment = [FCPATH. 'files/electronic_billing/'.$filename.'.zip'];
        // }

        // if ($sale->attachment != '') {
        //     $attachment[] = FCPATH. 'files/'.$sale->attachment;
        // }

        $cc = ($this->Settings->copy_mail_to_sender == TRUE) ? $issuer_data->default_email : NULL;

        if ($this->sma->send_email($to, $subject, $message, $issuer_data->default_email, $issuer_data->razon_social, $attachment, $cc)) {
            $this->site->updateSale(["fe_correo_enviado" => 1], $invoice_data->sale_id);

            if (file_exists(FCPATH. 'files/electronic_billing/'.$filename.'.xml')) {
                unlink(FCPATH. 'files/electronic_billing/'.$filename.'.xml');
            }
            if (file_exists(FCPATH. 'files/electronic_billing/'.$filename.'.pdf')) {
                unlink(FCPATH. 'files/electronic_billing/'.$filename.'.pdf');
            }
            if (file_exists(FCPATH. 'files/electronic_billing/'.$filename.'.zip')) {
                unlink(FCPATH. 'files/electronic_billing/'.$filename.'.zip');
            }

            return "Correo electrónico enviado al adquiriente.";
        } else {
            return "No fue posible enviar el correo electrónico.";
        }
    }

    public function get_email_customer($customer_id)
    {
        $this->db->select("email");
        if ($this->Settings->send_electronic_invoice_to == PRINCIPAL) {
            $this->db->from("companies");
            $this->db->where("id", $customer_id);
        } else {
            $this->db->from("addresses");
            $this->db->where("company_id", $customer_id);
        }

        $response = $this->db->get();
        return $response->row();
    }

    public function getDefaultBranchesCustomer($data)
    {
        $this->db->select("default_customer_id");
        $this->db->where($data);
        $q = $this->db->get("biller_data");
        return $q->row();
    }

    public function get_sale_by_cufe($cufe)
    {
        $this->db->where("cufe", $cufe);
        $response = $this->db->get("sales");
        return $response->row();
    }

    public function updatePayment($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("payments", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    /*************************************************************************** METODOS CREADOS PARA EL REQUEST DE SIMBA ***********************************************************************************/
        private function get_secuirty_token($id_provider, $ambiente)
        {
            $this->db->select('security_token');
            $this->db->from('technology_provider_configuration');
            $this->db->where('technology_provider_id', $id_provider);
            $this->db->where('work_enviroment', $ambiente);
            $response = $this->db->get();
            return $response->row();
        }

        private function get_typeDocElectronic($invoice_type)
        {
            // criterios tipo de factura a partir del cual se returna el tipo de documento
            // Tipo de factura	01	Factura de Venta Nacional
            // Tipo de factura	02	Factura de Exportación
            // Tipo de factura	03	Factura por Contingencia Facturador
            // Tipo de factura	04	Factura por Contingencia DIAN
            // Tipo de factura	91	Nota Crédito
            // Tipo de factura	92	Nota Débito

            // [1] FacturaV, [2] Nota Debito,[3] Nota Credito. Documentacion DEV-FEL-0.15
            if ($invoice_type == '01' || $invoice_type == DOCUMENT_EQUIVALENT) { // factura
                return 1;
            }
            if ($invoice_type == '91') { // nota credito
                return 3;
            }
            if ($invoice_type == '92') { // nota debito
                return 2;
            }
            return false;
        }

        private function get_idCustomizacion ($order_tax_aiu_id)
        {  // segun documentacion Anexo tecnico 13.1.5. Tipos de operación

            // Tipos de operación	01	Combustibles
            // Tipos de operación	02	Emisor es Autorretenedor
            // Tipos de operación	03	Excluidos y Exentos
            // Tipos de operación	04	Exportación
            // Tipos de operación	05	Genérica
            // Tipos de operación	06	Genérica con pago anticipado
            // Tipos de operación	07	Genérica con periodo de facturación
            // Tipos de operación	08	Consorcio
            // Tipos de operación	09	Servicios AIU
            // Tipos de operación	10	Estándar
            // Tipos de operación	11	Mandatos bienes
            // Tipos de operación	12	Mandatos Servicios

            // por ahora solo manejamos la 10 que es la estandar y la 09 segun el campo order_tax_aiu_id de la tabla sma_sales en caso de agregar mas agregar al siguiente flujo de condiciones
            if (!is_null($order_tax_aiu_id) && $order_tax_aiu_id != 0) {
                return "09";
            }
            return "10";
        }

        private function get_consecutiveSale($reference_no)
        {
            $aux = explode('-', $reference_no);
            return $aux[1];
        }

        private function current_date_format($date)
        {
            // Zona horaria de Colombia
            $zonaHorariaColombia = new DateTimeZone('America/Bogota');

            // Obtenemos la fecha y hora actual
            $fechaHoraActual = new DateTime($date, $zonaHorariaColombia); // Se crea un objeto DateTime con la hora actual y la zona horaria UTC

            // Formateamos la fecha y hora en el formato deseado
            $fechaHoraFormateada = $fechaHoraActual->format('Y-m-d\TH:i:sP'); // Formato: YYYY-mm-dd'T'HH:mm:ss+HH:mm

            return $fechaHoraFormateada;
        }

        private function get_ciiuCode($id_ciiu_code){
            $this->db->select('code');
            $this->db->from('ciiu_codes');
            $this->db->where('id', $id_ciiu_code);
            $response = $this->db->get();
            return $response->row();
        }

        private function getPaymentMethodSimba($document)
        {
            $payments = $this->site->getSalePayments($document->id);
            $paymentAmount = 0;

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    $paymentAmount += $payment->amount;
                }
                return (abs($document->grand_total) == abs($paymentAmount)) ? CASH : CREDIT;
            }
            return CREDIT;
        }

        private function getWithholdingsSimba($document)
        {
            $withHoldings = [];

            if (!empty(floatval($document->rete_iva_total))) {
                $withHoldings[] = [
                    "Codigo"=> "05",
                    "Base"  => floatval($document->rete_iva_base),
                    "Valor" => floatval($document->rete_iva_percentage),
                    "Total" => floatval($document->rete_iva_total),
                    "Name"  => "ReteIva"
                ];
            }

            if (!empty(floatval($document->rete_fuente_total))) {
                $withHoldings[] = [
                    "Codigo"=> "06",
                    "Base"  => floatval($document->rete_fuente_base),
                    "Valor" => floatval($document->rete_fuente_percentage),
                    "Total" => floatval($document->rete_fuente_total),
                    "Name"  => "ReteFuente"
                ];
            }

            if (!empty(floatval($document->rete_ica_total))) {
                // $taxRate = $this->site->getTaxRateByID($document->rete_ica_id);
                $withHoldings[] = [
                    "Codigo"=> "07",
                    "Base"  => floatval($document->rete_ica_base),
                    "Valor" => floatval($document->rete_ica_percentage),
                    "Total" => floatval($document->rete_ica_total),
                    "Name"  => "ReteIca"
                ];
            }

            return $withHoldings;
        }

        private function get_tax_array_t($sale_item, $currency){ // <- este metodo se invoca en cada linea
            // $this->sma->print_arrays($sale_item);
            $netUnitPrice = abs($sale_item->net_unit_price);
            $itemQuantity = abs($sale_item->quantity);
            $itemRate = abs($sale_item->rate);
            $totalImp2 = [];
            $value_imp1 = $this->sma->formatDecimal(((( $netUnitPrice * $itemQuantity) * $itemRate) / 100), NUMBER_DECIMALS_SIMBA);
            if ($sale_item->code_fe_name == "Bolsas") {
                $unitPrice = $sale_item->unit_price;
                $codeFe = $sale_item->code_fe;
                $totalImp[] = [
                    // desde aca lo que queremos es armar un array con los diferentes impuestos que tenga esta linea lo vamos a hacer con una funcion
                    "ValorImpuesto"    => [
                        "IdMoneda"          => "" .$currency. "", // codigo moneda
                        "Value"             => "".  $this->sma->formatDecimal( $unitPrice * $itemQuantity, NUMBER_DECIMALS_SIMBA) ."" // impuesto del producto sumando los dos impuestos
                    ],
                    "ValorAjusteRedondeo" => [
                        "IdMoneda"      =>   "COP",
                        "Value"         =>  0.0,
                    ],
                    "IndicaEsSoloEvidencia" => false,
                    "IndicaEsSoloEvidenciaSpecified"    => true,
                    "IndicaImpuestoIncluidoSpecified"   => false,
                    "SubTotalImpuesto" =>[
                        [
                            "ValorImpuesto" =>[
                                "IdMoneda"      =>  "". $currency ."",  // codigo moneda
                                "Value"         =>  "".$this->sma->formatDecimal( $unitPrice * $itemQuantity, NUMBER_DECIMALS_SIMBA).""
                            ],
                            "SecuenciaNumericaSpecified"    =>  false,
                            "PorcentajeSpecified"           =>  false,
                            "UnidadDeMedida"    => [
                                "CodUnidad"         => "NIU",
                                "Value"             => "1.0",
                            ],
                            "ValorPorUnidad"    => [
                                "IdMoneda"          => "". $currency ."",
                                "Value"             => "". $this->sma->formatDecimal( $unitPrice, NUMBER_DECIMALS_SIMBA) .""
                            ],
                            "PorcentajeRangoSpecified"  =>  false,
                            "CategoriaImpuesto" =>[
                                "PorcentajeSpecified"   => false,
                                "PorcentajeRangoSpecified"  => false,
                                "EsquemaTributario" => [
                                    "Id"    => [
                                        "Value" => "" .$codeFe. ""
                                    ],
                                    "Nombre"    => [
                                        "Value" =>  "INC Bolsas"
                                    ]
                                ]
                            ]
                        ]
                    ],
                ];
            } else if($sale_item->code_fe_name == "IC"){
                $totalImp[] = [
                    "ValorImpuesto"    => [
                        "IdMoneda"          => $currency, // codigo moneda
                        "Value"             => "". $value_imp1 ."" // impuesto del producto sumando los dos impuestos
                    ],
                    "ValorAjusteRedondeo"   =>[
                        "IdMoneda"          => "" .$currency. "",
                        "Value"             => 0.0
                    ],
                    "IndicaEsSoloEvidencia" => false,
                    "IndicaEsSoloEvidenciaSpecified"    => true,
                    "IndicaImpuestoIncluidoSpecified"   => false,
                    "SubTotalImpuesto" =>[
                        [
                            "ValorImpuesto" =>[
                                "IdMoneda"      =>  $currency,  // codigo moneda
                                "Value"         =>  "". $value_imp1 .""
                            ],
                            "SecuenciaNumericaSpecified"=> false,
                            "PorcentajeSpecified" => false,
                            "UnidadDeMedida"             => [
                                "CodUnidad"         => "NIU",
                                "Value"             => "" .$itemQuantity. "",
                            ],
                            "ValorPorUnidad"       =>[
                                "IdMoneda"      => "" .$currency. "",
                                "Value"         =>  "" .$value_imp1 / $itemQuantity. ""
                            ],
                            "PorcentajeRangoSpecified" => false,
                            "CategoriaImpuesto" =>[
                                "PorcentajeSpecified"   => false,
                                "PorcentajeRangoSpecified"  => false,
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $sale_item->code_fe,  // codigo del impuesto
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $sale_item->code_fe_name // nombre del impuesto
                                    ]
                                ]
                            ]
                        ]
                    ],
                ];
            }
            else{
                $totalImp[] = [
                    // desde aca lo que queremos es armar un array con los diferentes impuestos que tenga esta linea lo vamos a hacer con una funcion
                    "ValorImpuesto"    => [
                        "IdMoneda"          => $currency, // codigo moneda
                        "Value"             => "".  $this->sma->formatDecimal(((( $netUnitPrice * $itemQuantity) * $itemRate) / 100), NUMBER_DECIMALS_SIMBA) ."" // impuesto del producto sumando los dos impuestos
                    ],
                    "ValorAjusteRedondeo"   =>[
                        "IdMoneda"          => "" .$currency. "",
                        "Value"             => "0.0",
                    ],
                    "IndicaEsSoloEvidencia" => false,
                    "IndicaEsSoloEvidenciaSpecified"    => true,
                    "IndicaImpuestoIncluidoSpecified"   => false,
                    "SubTotalImpuesto" =>[
                        [
                            "BaseImponible" =>[
                                "IdMoneda"      => $currency, // codigo moneda
                                "Value"         => "". $this->sma->formatDecimal($netUnitPrice * $itemQuantity, NUMBER_DECIMALS_SIMBA) .""  // subtotal
                            ],
                            "ValorImpuesto" =>[
                                "IdMoneda"      =>  $currency,  // codigo moneda
                                "Value"         =>  "".((($netUnitPrice * $itemQuantity) * $itemRate) / 100).""
                            ],
                            "SecuenciaNumericaSpecified"=> false,
                            "PorcentajeSpecified" => false,
                            "PorcentajeRangoSpecified"  => false,
                            "CategoriaImpuesto" =>[
                                "Porcentaje"        =>  "".$this->sma->formatDecimal($itemRate, NUMBER_DECIMALS)."",  // porcentaje impuesto
                                "PorcentajeSpecified"   => true,
                                "PorcentajeRangoSpecified"  => false,
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $sale_item->code_fe,  // codigo del impuesto
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $sale_item->code_fe_name // nombre del impuesto
                                    ]
                                ]
                            ]
                        ]
                    ],
                ];
            }
            
            if ($sale_item->tax_rate_2_id > 0) {
                // esta seccion es para darle logica a cada tipo de impuesto por aparte segun la documentacion de la Dian 13.3.11 tributos.xlsx
                if ($sale_item->nameTax2 == 'ICL') {
                    $percen = abs($sale_item->item_tax_2 / $sale_item->net_unit_price) * 100;

                    if ($sale_item->mililiters != 750) {
                        $tax_2 = (($sale_item->mililiters * $sale_item->nominal) / 750) * $sale_item->degrees;
                    }else{
                        $tax_2 = $sale_item->mililiters * $sale_item->degrees;
                    }
                    $totalImp2 = [
                        "ValorImpuesto"    => [
                            "IdMoneda"          => $currency, // codigo moneda
                            // "Value"             => "".  $this->sma->formatDecimal($tax_2 , NUMBER_DECIMALS_SIMBA) .""
                            "Value"             => "".  $this->sma->formatDecimal($sale_item->item_tax_2 , NUMBER_DECIMALS_SIMBA) .""
                        ],
                        "SubTotalImpuesto" =>[
                            [
                                "BaseImponible" =>[
                                    "IdMoneda"      => $currency, // codigo moneda
                                    "Value"         => "". $this->sma->formatDecimal($sale_item->net_unit_price * $itemQuantity, NUMBER_DECIMALS_SIMBA) .""  // subtotal
                                ],
                                "ValorImpuesto" =>[
                                    "IdMoneda"      =>  $currency,  // codigo moneda
                                    // "Value"         =>  "".$this->sma->formatDecimal($tax_2, NUMBER_DECIMALS).""
                                    "Value"         =>  "".$this->sma->formatDecimal($sale_item->item_tax_2, NUMBER_DECIMALS).""
                                ],
                                // "BaseUnitMeasure" => [
                                //     "Value" => "" .$sale_item->degrees. ""
                                // ],
                                // "PerUnitAmount" => [
                                //     "Value" => "" .$sale_item->nominal. ""
                                // ],
                                "CategoriaImpuesto" =>[
                                    "Porcentaje"        => "" .$this->sma->formatDecimal($percen, NUMBER_DECIMALS). "",
                                    "EsquemaTributario" =>[
                                        "Id"                => [
                                            "Value"             => "" .$sale_item->codeTax2. "",  // codigo del impuesto
                                        ],
                                        "Nombre"            =>  [
                                            "Value"             => "". $sale_item->nameTax2 .""  // nombre del impuesto
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
                if ($sale_item->nameTax2 == 'IBUA') {
                    $percen = ($sale_item->item_tax_2 / $sale_item->net_unit_price) * 100;

                    if ($sale_item->mililiters != 750) {
                        $tax_2 = (($sale_item->mililiters * $sale_item->nominal) / 750) * $sale_item->degrees;
                    }else{
                        $tax_2 = $sale_item->mililiters * $sale_item->degrees;
                    }
                    $totalImp2 = [
                        "ValorImpuesto"    => [
                            "IdMoneda"          => $currency, // codigo moneda
                            // "Value"             => "".  $this->sma->formatDecimal($tax_2 , NUMBER_DECIMALS_SIMBA) .""
                            "Value"             => "".  $this->sma->formatDecimal($sale_item->item_tax_2 , NUMBER_DECIMALS_SIMBA) .""
                        ],
                        "SubTotalImpuesto" =>[
                            [
                                "BaseImponible" =>[
                                    "IdMoneda"      => $currency, // codigo moneda
                                    "Value"         => "". $this->sma->formatDecimal($sale_item->net_unit_price * abs($sale_item->quantity), NUMBER_DECIMALS_SIMBA) .""  // subtotal
                                ],
                                "ValorImpuesto" =>[
                                    "IdMoneda"      =>  $currency,  // codigo moneda
                                    // "Value"         =>  "".$this->sma->formatDecimal($tax_2, NUMBER_DECIMALS).""
                                    "Value"         =>  "".$this->sma->formatDecimal($sale_item->item_tax_2, NUMBER_DECIMALS).""
                                ],
                                // "BaseUnitMeasure" => [
                                //     "Value" => "" .$sale_item->degrees. ""
                                // ],
                                // "PerUnitAmount" => [
                                //     "Value" => "" .$sale_item->nominal. ""
                                // ],
                                "CategoriaImpuesto" =>[
                                    "Porcentaje"        => "" .$this->sma->formatDecimal($percen, NUMBER_DECIMALS). "",
                                    "EsquemaTributario" =>[
                                        "Id"                => [
                                            "Value"             => "" .$sale_item->codeTax2. "",  // codigo del impuesto
                                        ],
                                        "Nombre"            =>  [
                                            "Value"             => "". $sale_item->nameTax2 .""  // nombre del impuesto
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
                if ($sale_item->nameTax2 == 'IC') {
                    $item_tax_2 = abs($sale_item->item_tax_2);
                    $secondImpValue = $this->sma->formatDecimal($item_tax_2, NUMBER_DECIMALS_SIMBA);
                    $totalImp2 = [
                        "ValorImpuesto"    => [
                            "IdMoneda"          => $currency, // codigo moneda
                            "Value"             => "". $secondImpValue ."" // impuesto del producto sumando los dos impuestos
                        ],
                        "ValorAjusteRedondeo" => [
                            "IdMoneda"          =>  "".$currency."" ,
                            "Value"             =>  "0.0"
                        ],
                        "IndicaEsSoloEvidencia"     => false,
                        "IndicaEsSoloEvidenciaSpecified"    => true,
                        "IndicaImpuestoIncluidoSpecified"   => false,
                        "SubTotalImpuesto" =>[
                            [
                                "ValorImpuesto" =>[
                                    "IdMoneda"      =>  $currency,  // codigo moneda
                                    "Value"         =>  "" .$secondImpValue. ""
                                ],
                                "SecuenciaNumericaSpecified" => false,
                                "PorcentajeSpecified"        => false,
                                "UnidadDeMedida"             => [
                                    "CodUnidad"         => "NIU",
                                    "Value"             => "" .$itemQuantity. "",
                                ],
                                "ValorPorUnidad"       =>[
                                    "IdMoneda"      => "" .$currency. "",
                                    "Value"         =>  "" .$secondImpValue / $itemQuantity. ""
                                ],
                                "PorcentajeRangoSpecified" => false,    
                                "CategoriaImpuesto" =>[
                                    "PorcentajeSpecified"       => false,
                                    "PorcentajeRangoSpecified"  => false,
                                    "EsquemaTributario" =>[
                                        "Id"                => [
                                            "Value"             => "" .$sale_item->codeTax2. "",  // codigo del impuesto
                                        ],
                                        "Nombre"            =>  [
                                            "Value"             => "". $sale_item->nameTax2 .""  // nombre del impuesto
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
                if ($sale_item->nameTax2 != 'ICL' && $sale_item->nameTax2 != 'IBUA' && $sale_item->nameTax2 != 'IC') {
                    $rate = intval($sale_item->tax_2);
                    $perSeconImp = '';
                    if ($sale_item->mililiters == '0') {
                        $perSeconImp = intval(str_replace('%', '', $sale_item->tax_2));
                    }else{
                        $perSeconImp = $sale_item->tax_2;
                    }
                    $totalImp2 = [
                        "ValorImpuesto"    => [
                            "IdMoneda"          => $currency, // codigo moneda
                            "Value"             => "".  $this->sma->formatDecimal( $sale_item->item_tax_2 , NUMBER_DECIMALS_SIMBA) ."" // impuesto del producto sumando los dos impuestos
                        ],
                        "SubTotalImpuesto" =>[
                            [
                                "BaseImponible" =>[
                                    "IdMoneda"      => $currency, // codigo moneda
                                    "Value"         => "".$this->sma->formatDecimal($sale_item->net_unit_price * $itemQuantity, NUMBER_DECIMALS_SIMBA).""  // subtotal
                                ],
                                "ValorImpuesto" =>[
                                    "IdMoneda"      =>  $currency,  // codigo moneda
                                    "Value"         =>  "".$this->sma->formatDecimal($sale_item->item_tax_2, NUMBER_DECIMALS).""
                                ],
                                "CategoriaImpuesto" =>[
                                    "Porcentaje"        =>  "".$this->sma->formatDecimal(($perSeconImp), NUMBER_DECIMALS)."",  // porcentaje impuesto
                                    "EsquemaTributario" =>[
                                        "Id"                => [
                                            "Value"             => "" .$sale_item->codeTax2. "",  // codigo del impuesto
                                        ],
                                        "Nombre"            =>  [
                                            "Value"             => "". $sale_item->nameTax2 .""  // nombre del impuesto
                                        ]
                                    ]
                                ]
                            ]
                        ]
                    ];
                }
            }
            if (!empty($totalImp2)) {
                array_push($totalImp, $totalImp2);
            }
            return $totalImp;
        }

        private function getPaymentMeanSimba($document)
        {
            $paymentMeanCode = "ZZZ";
            $paymentsWithoutWithholdings = [];
            $payments = $this->site->getSalePayments($document->id);

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    if ($payment->paid_by != 'retencion') {
                        $paymentsWithoutWithholdings[] = $payment;
                    }
                }
            }

            if (!empty($paymentsWithoutWithholdings)) {
                foreach ($paymentsWithoutWithholdings as $payment) {
                    if (!empty($payment->mean_payment_code_fe)) {
                        $paymentMeanCode = $payment->mean_payment_code_fe;
                    }
                }
            }

            return $paymentMeanCode;
        }

        private function get_id_tributo($person_type){
            // 01 IVA
            // 04 INC
            // ZA IVA e INC
            // ZZ No aplica *

            /** regimen simplificado    -> No responsable de IVA
             *  regimen comun           -> Responsable de IVA
            **/
            /***** Se establece por ahora que si es persona responsable como IVA y si es No responsable ZZ  *****/
            $id = "ZZ";
            if ($person_type == 2) {
                $id = '01';
            }
            return $id;
        }

        private function get_name_tributo($person_type){
            // 01 IVA
            // 04 INC
            // ZA IVA e INC
            // ZZ No aplica *

            /** regimen simplificado    -> No responsable de IVA
             *  regimen comun           -> Responsable de IVA
            **/
            /***** Se establece por ahora que si es persona responsable como IVA y si es No responsable ZZ  *****/
            $name = "No aplica";
            if ($person_type == 2) {
                $name = 'IVA';
            }
            return $name;
        }

        private function get_name_regimen($code){
            // codigos sistema
            // 1 simplificado, 2 comun, 0 no aplica
            $name = "04";
            $this->db->select('list_name');
            $this->db->from('types_vat_regime');
            $this->db->where('id =', $code);
            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                $res = $q->row();
                $name = $res->list_name;
                return $name;
            }
            return $name;
        }

        private function get_reference_returns($sale_origin, $nc){
            if ($sale_origin) {
                $_reference =  [
                    "ReferenciaFacturacion" => [
                        [
                            "ReferenciaDocFactura" => [
                                "Id" => [
                                    "Value" => str_replace("-", "", $sale_origin->reference_no),
                                ],
                                "UID" => [
                                    "Value" => $sale_origin->cufe,
                                ],
                                "Fecha" => date('Y-m-d', strtotime($sale_origin->date)),
                            ]
                        ]
                    ]
                ];
            }else{
                $_reference = [
                    "ReferenciaDocOrden" => [
                        "Id" => [
                            "Value" => "11111"
                        ],
                        "IdPedido" => [
                            "Value" => "222222"
                        ],
                        "IndicadorCopiaSpecified" => false,
                        "FechaOrden" =>  $this->current_date_format($nc->document_without_reference_date),
                        "FechaOrdenSpecified" => true,
                        "HoraOrdenSpecified" => false,
                        "ReferenciaADocumento" => [
                            "Id" => [
                                "Value" => "222222"
                            ],
                            "EsCopiaSpecified" => false,
                            "FechaSpecified" => false,
                            "HoraSpecified" => false,
                            "CodigoTipoDoc" => [
                                "Value" => "",
                            ],
                            "NombreTipoDoc" => [
                                "Value" => "",
                            ],
                            "XPath" => [],
                            "Adjunto" => [
                                "ReferenciaExterna" => [
                                    "URL" => [
                                        "Value" => ""
                                    ],
                                    "HashDocumento" =>[
                                        "Value" => ""
                                    ],
                                    "FechaExpiracionSpecified" => false,
                                    "HoraExpiracionSpecified" => false
                                ]
                            ]
                        ]
                    ]
                ];
            }
            return $_reference;
        }

        private function get_billing_period_nc_noReference($request, $nc, $product_id){
            if ($nc->document_without_reference == 1) {
                $concepts = $this->site->get_debit_credit_note_concept_by_id($product_id);
                // ajustes en nodo parametros
                $arrayEnca = [
                    [
                        "NombreIndicador" => "SKIPVALIDDIANLOGI",
                        "Activado" => true
                    ],
                    [
                        "NombreIndicador" => "SKIPVALIDDIANREQU",
                        "Activado" => true
                    ]
                ];
                $request["Parametros"]["Wildcards"] = "[NT_SK_VFA]";
                $request["Parametros"]["IndicadoresAdicionales"] = $arrayEnca;
                $request['Parametros']['Personalizacion'] = "20";

                // ajustes en nodo encabezado
                $array = [
                         [
                            "FechaInicial" => $this->current_date_format($nc->document_without_reference_date),
                            "FechaInicialSpecified" => true,
                            "HoraInicialSpecified" => false,
                            "FechaFinal" => $this->current_date_format($nc->document_without_reference_date),
                            "FechaFinalSpecified" => true,
                            "HoraFinalSpecified" => false
                        ],
                ];
                $respuestaMotivoNota = [
                    [
                        "CodRespuesta" => [
                            "Value" =>  "" .$concepts->dian_code. ""
                        ],
                        "Descripcion" => [
                            ["Value" => "" .$concepts->dian_name. ""]
                        ],
                        "FechaEfectivaSpecified"  => false,
                        "HoraEfectivaSpecified"  => false,
                    ],
                ];
                $request['Encabezado']['IndicaCopiaSpecified'] = false;
                $request['Encabezado']['FechaTributariaSpecified'] = false;
                $request['Encabezado']['PeriodoFacturacion'] = $array;
                $request['Encabezado']['RespuestaMotivoNota'] = $respuestaMotivoNota;
            }
            return $request;
        }

        private function get_documento_equivalente_pos($request, $sale, $document_type, $customer, $sale_origin = null){
            if ($document_type->mode == '2') {
                $user = $this->site->getUser($sale->created_by);
                // Información del fabricante del software
                $InformacionDelFabricanteDelSoftware = [
                    [
                        "Name"  => "NombreApellido",
                        "Value" => "Simba Software Sas"
                    ],
                    [
                        "Name"  => "RazonSocial",
                        "Value" => "Simba Software Sas"
                    ],
                    [
                        "Name"  => "NombreSoftware",
                        "Value" => "Simba Factura Web4.0.0.92"
                    ]
                ];

                // Información de beneficios para el comprador
                $InformacionBeneficiosComprador = [
                    [
                        "Name"  => "Codigo",
                        "Value" => "" .$customer->vat_no. ""
                    ],
                    [
                        "Name"  => "NombresApellidos",
                        "Value" => "" .$customer->name. ""
                    ],
                    [
                        "Name"  => "Puntos",
                        "Value" => "" .$customer->award_points. ""
                    ]
                ];

                // Información de la caja de venta
                $InformacionCajaVenta = [
                    [
                        "Name"  => "PlacaCaja",
                        "Value" => ($user->user_pc_serial != '') ? $user->user_pc_serial : "N/A"
                    ],
                    [
                        "Name"  => "UbicaciónCaja",
                        "Value" => "N/A"
                    ],
                    [
                        "Name"  => "Cajero",
                        "Value" => $user->first_name . ' ' . $user->last_name,
                    ],
                    [
                        "Name"  => "TipoCaja",
                        "Value" => "N/A"
                    ],
                    [
                        "Name"  => "CódigoVenta",
                        "Value" => "N/A"
                    ],
                    [
                        "Name"  => "SubTotal",
                        "Value" => "N/A"
                    ]
                ];

                // Construcción del array de request
                $request['Extensiones'][] = [
                    'ContenidoExtension' => [
                        'FabricanteSoftware' => [
                            'InformacionDelFabricanteDelSoftware' => $InformacionDelFabricanteDelSoftware
                        ]
                    ]
                ];

                $request['Extensiones'][] = [
                    'ContenidoExtension' => [
                        'BeneficiosComprador' => [
                            'InformacionBeneficiosComprador' => $InformacionBeneficiosComprador
                        ]
                    ]
                ];

                $request['Extensiones'][] = [
                    'ContenidoExtension' => [
                        'PuntoVenta' => [
                            'InformacionCajaVenta' => $InformacionCajaVenta
                        ]
                    ]
                ];
            }
            return $request;
        }

        private function get_params_with_other_technology_provider($request, $sale_origen){
            if (($sale_origen) && $sale_origen->technology_provider != '3') {
                $request["Parametros"]["Wildcards"] = "[NT_SK_VFA]";
            }
            return $request;
        }

        public function getStatusSimba($sale){
            $codigoField = 0;
            $document = $this->site->getSaleByID($sale->id);
            $technologyProvider = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);
            $json = $this->getDocumenSimba($document, $technologyProvider);
            // $this->sma->print_arrays($json);
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => 'https://fe2.simba.co/api_nomina/api/FacturaElectronica/ConsultarDocumentos',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $json,
                CURLOPT_HTTPHEADER => ['Content-Type: application/json'],
            ]);
            $response = curl_exec($curl);
            curl_close($curl);

            try {
                $dataResponse = json_decode($response, false);
                if ($dataResponse === false) {
                    log_message('debug','FE SIMBA peticion status: '.$response);
                    $response = [
                        "error"     => true,
                        "message"   => "Error en el procedimiento"
                    ];
                    return $response;
                }
                $estadoDocumento = $dataResponse->respuestaUnitariaField->docElectronicoExtendidoField->estadoDocumentoField ?? null;
                if (!$estadoDocumento || $estadoDocumento != 'Exito' ) {
                    $mensajeField = 'No se encontraron Registros';
                    if (isset($dataResponse->respuestaUnitariaField->encabezadoRespuestaField->novedadField[0])) {
                        $error_message = $dataResponse->respuestaUnitariaField->encabezadoRespuestaField->novedadField[0];
                        if ($error_message->mensajeField[0]) {
                            $mensajeField = $error_message->mensajeField[0]->valueField;
                        }
                    }
                    $response = [
                        "error"     => true,
                        "message"   => $mensajeField,
                    ];
                    return $response;
                }

                if (isset($dataResponse->respuestaUnitariaField->docElectronicoExtendidoField->archivoPrincipalField[0])) {
                    $fe_xml = $dataResponse->respuestaUnitariaField->docElectronicoExtendidoField->archivoPrincipalField[0]->binarioField;
                } else {
                    $fe_xml = '';
                }
                
                $fe_cufe = $dataResponse->respuestaUnitariaField->docElectronicoExtendidoField->datosBasicosField->cUDEField;

                $etapa = $dataResponse->respuestaUnitariaField->encabezadoRespuestaField->etapaField ?? [];
                $fechaValidDian = null;
                foreach ($etapa as $item) {
                    if ($item->nombreField === "Validado por Dian") {
                        $fechaValidDian = new DateTime($item->fechaHoraField);
                        $fechaValidDian = $fechaValidDian->format('Y-m-d H:i:s');
                        $codigoField = $item->codigoField;
                        break;
                    }
                }

                if ($codigoField == '100') {
                    $customer_data = $this->site->getCompanyByID($document->customer_id);
                    $codigo_qr =    "NumeroFactura: ".str_replace('-', '', $document->reference_no)."\n".
                                    "FechaFactura: ". $this->format_date($document->date)->date."\n".
                                    "ValorFactura: ". $this->sma->formatDecimalNoRound(abs($document->total), 2)."\n".
                                    "ValorIVA: ". ($this->sma->formatDecimalNoRound(abs($document->total_tax), 2))."\n".
                                    "ValorTotalFactura: ". $this->sma->formatDecimalNoRound(abs($document->grand_total), 2)."\n".
                                    "NitEmisor: ". str_replace("-", "", $this->Settings->numero_documento)."\n".
                                    "DocumentoAdquiriente: ". str_replace('-', '', $customer_data->vat_no)."\n".
                                    "CUFE:".$fe_cufe."" ;
                   
                    $data = [
                        "fe_aceptado"   => 2,
                        "cufe"          => $fe_cufe,
                        "fe_xml"        => $fe_xml,
                        "codigo_qr"     => $codigo_qr,
                        "fe_mensaje"    => "Documento aceptado por la DIAN",
                        "fe_validation_dian"    => $fechaValidDian,
                    ];
                    
                    if ($this->site->updateSale($data, $document->id)) {
                        $response = [
                            "error"     => false,
                            "message"   => lang('successful_state_change')
                        ];
                        return $response;
                    } 
                }

                $txtRes = ($codigoField == '90') ? 'Documento procesado anteriormente' : 'Rechazado';
                $mensajeField = "Documento enviado a Simba, pero pendiente en la DIAN. Su estado actual es: $txtRes";

                if (isset($dataResponse->respuestaUnitariaField->encabezadoRespuestaField->novedadField[1])) {
                    $error_message = $dataResponse->respuestaUnitariaField->encabezadoRespuestaField->novedadField[1];
                    if ($error_message->mensajeField[0]) {
                        $mensajeField = $error_message->mensajeField[0]->valueField;
                    }
                }
                $response = [
                    "error"     => true,
                    "message"   => $mensajeField,
                ];
                return $response;    
            } catch (Exception $e) {
                log_message('debug','FE SIMBA get Status: '. $e->getMessage());
            }
        }

        private function get_general_discount($sale, $currency, $return = false){
            if ($sale->total_discount == 0) { // <- cuando la factura no tiene descuento
                return [];
            }
            if ($sale->product_discount != 0) { // <- En este caso el descuento es por producto
                return [];
            }
            $percentage = str_replace('%', '', $sale->order_discount_id);
            if (!strpos($sale->order_discount_id, '%') !== false ) {
                $percentage = ($sale->total_discount / ($sale->total + $sale->total_tax)) * 100;
            }
            $total_discount = ($return) ? abs($sale->total_discount) : $sale->total_discount ;
            $total = ($return) ? abs($sale->total) : $sale->total;

            return [
                [
                    "Id" => [
                        "Value" => "5"
                    ],
                    "IndicaCargoODescuento"     => false,
                    "RazonCargoDescuentoCod"    => [
                        "Value"                     => "09"
                    ],
                    "RazonCargoDescuentoTexto"  => [
                        [
                            "Value"             => "Descuento General"
                        ]
                    ],
                    "Porcentaje"            => "" .$percentage. "",
                    "PorcentajeSpecified"   => true,
                    "IndicaPagoAdelantado"  => false,
                    "SecuenciaNumericaSpecifies" => false,
                    "Valor" => [
                        "IdMoneda"  => "" .$currency. "",
                        "Value"     => "" .$this->sma->formatDecimal( $total_discount , NUMBER_DECIMALS_SIMBA). "",
                    ],
                    "Base"  => [
                        "IdMoneda"  => "" .$currency. "",
                        "Value"     => "" .$this->sma->formatDecimal( $total , NUMBER_DECIMALS_SIMBA). "", 
                    ]   
                ]
            ];
        }

        private function get_sale_origin_from_other_year($return){
            if ($return->sale_id) {
                return $this->site->getSaleByID($return->sale_id); // datos de la venta de origen
            }
            if ($return->year_database) {
                $term = $return->return_sale_ref;
                $database = $return->year_database;
                return $this->site->get_past_year_sale($term, $database);
            }
            return false;
        }

        private function get_cude_simba($msg){
            preg_match('/\[Cude=([^\]]+)\]/', $msg, $matches);
            if (isset($matches[1])) {
                return $matches[1];
            } else {
                return NULL;
            }
        }

        public function getTypeElectronicDocument($documentId, $module = NULL)
        {
            $document = ($module == DOCUMENTO_SOPORTE) ? $this->site->getPurchaseByID($documentId) : $this->site->getSaleByID($documentId);
            $resolution = $this->site->getDocumentTypeById($document->document_type_id);
    
            if ($resolution->module == FACTURA_POS || $resolution->module == FACTURA_DETAL) {
                if ($resolution->mode == "2") {
                    return DOCUMENT_EQUIVALENT;
                }
                return INVOICE;
            } elseif ($resolution->module == NOTA_CREDITO_POS || $resolution->module == NOTA_CREDITO_DETAL) {
                return CREDIT_NOTE;
            } elseif ($resolution->module == NOTA_DEBITO_POS || $resolution->module == NOTA_DEBITO_DETAL) {
                return DEBIT_NOTE;
            } elseif ($resolution->module == DOCUMENTO_SOPORTE) {
                return DOCUMENT_SUPPORT;
            }
        }

        private function getTotalTax ($totImp2, $currency)
        {
            // $this->sma->print_arrays($totImp2);
            $totalTaxArray = [];
            $totalTImpuesto = 0;
            foreach ($totImp2 as $keyti2 => $valueti2) {
                $totalTaxT = 0;
                $totalTaxAux = [];
                foreach ($valueti2 as $keytii2 => $valuetii2) {      
                    $value_total = $this->sma->formatDecimal(abs($valuetii2['total']), NUMBER_DECIMALS_SIMBA);
                    $value_base = $this->sma->formatDecimal(abs($valuetii2['base']), NUMBER_DECIMALS_SIMBA);
                    $value_rate = $this->sma->formatDecimal(abs($valuetii2['rate']), NUMBER_DECIMALS);
                    $value_quantity = abs($valuetii2['quantity']);            
                    if ($valuetii2['code_fe_name'] == 'Bolsas') {
                        $totalTaxT += $value_total;
                        $totalTaxAux[] = [
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => $currency,
                                "Value"             => "".$value_total.""
                            ],
                            "SecuenciaNumericaSpecified"    =>  false,
                            "PorcentajeSpecified"           =>  false,
                            "PorcentajeRangoSpecified"      =>  false,
                            "UnidadDeMedida"    => [
                                "CodUnidad" => "NIU",
                                "Value" => "1.0",
                            ],
                            "ValorPorUnidad" => [
                                "IdMoneda" => "" .$currency. "",
                                "Value" => "" .$value_rate. "",
                            ],
                            "PorcentajeRangoSpecified" => false,
                            "CategoriaImpuesto" =>[
                                "PorcentajeSpecified"   => false,
                                "PorcentajeRangoSpecified" => false,
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => "INC Bolsas"
                                    ]
                                ]
                            ]
                        ];
                    }
                    if ($valuetii2['code_fe_name'] == 'ICL') {
                        $percen = ($valuetii2['total'] / $valuetii2['base']) * 100;
                        if ($valuetii2['mililiters'] != 750) {
                            $tax_2 = (($valuetii2['mililiters'] * $valuetii2['nominal']) / 750) * $valuetii2['degrees'];
                        }else{
                            $tax_2 = $valuetii2['mililiters'] * $valuetii2['degrees'];
                        }
                        $totalTaxT += $value_total;
                        $totalTaxAux[] = [
                            "BaseImponible"     => [
                                "IdMoneda"          => $currency,
                                "Value"             => "" .$value_base. ""
                            ],
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => $currency,
                                "Value"             => "" .$value_total. ""
                            ],
                            "CategoriaImpuesto" =>[
                                "Porcentaje"        => "" .$this->sma->formatDecimal($percen, NUMBER_DECIMALS). "",
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $valuetii2['code_fe_name']
                                    ]
                                ]
                            ]
                        ];
                    }
                    if ($valuetii2['code_fe_name'] == 'IBUA') {
                        $percen = ($valuetii2['total'] / $valuetii2['base']) * 100;
                        if ($valuetii2['mililiters'] != 750) {
                            $tax_2 = (($valuetii2['mililiters'] * $valuetii2['nominal']) / 750) * $valuetii2['degrees'];
                        }else{
                            $tax_2 = $valuetii2['mililiters'] * $valuetii2['degrees'];
                        }
                        $totalTaxT += $value_total;
                        $totalTaxAux[] = [
                            "BaseImponible"     => [
                                "IdMoneda"          => $currency,
                                "Value"             => "".$value_base.""
                            ],
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => $currency,
                                "Value"             => "".$value_total.""
                            ],
                            "CategoriaImpuesto" =>[
                                "Porcentaje"        => "" .$this->sma->formatDecimal($percen, NUMBER_DECIMALS). "",
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $valuetii2['code_fe_name']
                                    ]
                                ]
                            ]
                        ];
                    }
                    if($valuetii2['code_fe_name'] == 'IC'){
                        $secondImpValue = $this->sma->formatDecimal(abs($value_total), NUMBER_DECIMALS_SIMBA);
                        $totalTaxT += $secondImpValue;
                        $totalTaxAux[] = [
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => $currency,
                                "Value"             => "". $secondImpValue .""
                            ],
                            "SecuenciaNumericaSpecified"    =>  false,
                            "PorcentajeSpecified"           =>  false,
                            "UnidadDeMedida"                => [
                                "CodUnidad"                     => "NIU",
                                "Value"                         => "" .$value_quantity. ""  ,
                            ],
                            "ValorPorUnidad" => [
                                "IdMoneda"      => "" .$currency. "",
                                "Value"         => "" .$secondImpValue / $value_quantity. "",
                            ],
                            "PorcentajeRangoSpecified"  => false,
                            "CategoriaImpuesto" =>[
                                "PorcentajeSpecified"       => false,
                                "PorcentajeRangoSpecified"  => false,
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $valuetii2['code_fe_name']
                                    ]
                                ]
                            ]
                        ];
                    }
                    if($valuetii2['code_fe_name'] != 'ICL' && $valuetii2['code_fe_name'] != 'IBUA' && $valuetii2['code_fe_name'] != 'IC' && $valuetii2['code_fe_name'] != 'Bolsas'){
                        $totalTaxT += $value_total;
                        $totalTaxAux[] = [
                            "BaseImponible"     => [
                                "IdMoneda"          => $currency,
                                "Value"             => "".$value_base.""
                            ],
                            "ValorImpuesto"     =>  [
                                "IdMoneda"          => $currency,
                                "Value"             => "".$value_total.""
                            ],
                            "SecuenciaNumericaSpecified"    =>  false,
                            "PorcentajeSpecified"           =>  false,
                            "PorcentajeRangoSpecified"      =>  false,
                            "CategoriaImpuesto" =>[
                                "Porcentaje"        => "" .$value_rate. "",
                                "PorcentajeSpecified"   => true,
                                "PorcentajeRangoSpecified" => false,
                                "EsquemaTributario" =>[
                                    "Id"                => [
                                        "Value"             => $valuetii2['code_fe'],
                                    ],
                                    "Nombre"            => [
                                        "Value"             => $valuetii2['code_fe_name']
                                    ]
                                ]
                            ]
                        ];
                    }
                }
                $totalTaxArray[] = [
                    "ValorImpuesto"     =>[
                        "IdMoneda"          =>  $currency,
                        "Value"             =>  "".$this->sma->formatDecimal($totalTaxT, NUMBER_DECIMALS_SIMBA)."",
                    ],
                    "ValorAjusteRedondeo"   => [
                        "IdMoneda"          => "" .$currency. "",
                        "Value"             => "0.0",
                    ],
                    "IndicaEsSoloEvidenciaSpecified"  =>  false,
                    "IndicaImpuestoIncluidoSpecified"   => false,
                    "SubTotalImpuesto" => $totalTaxAux
                ];
                $totalTImpuesto += $totalTaxT;
            }

            return [
                "totalTaxArray" => $totalTaxArray,
                "totalTImpuesto" => $totalTImpuesto
            ];
        }

        private function get_another_currency($request, $sale, $fechaEmision){
            if ($sale->sale_currency !== 'COP') {
                $request['AgregadoComercial']['TasaDeCambioPago']['OrigenCodigoMoneda']['Value'] = 'COP';
                $request['AgregadoComercial']['TasaDeCambioPago']['OrigenFactorBase'] = $sale->sale_currency_trm;
                $request['AgregadoComercial']['TasaDeCambioPago']['OrigenFactorBaseSpecified'] = true;
                $request['AgregadoComercial']['TasaDeCambioPago']['DestinoCodigoMoneda']["Value"] = $sale->sale_currency;
                $request['AgregadoComercial']['TasaDeCambioPago']['DestinoFactorBase'] = "1.00";
                $request['AgregadoComercial']['TasaDeCambioPago']['DestinoFactorBaseSpecified'] = true;
                $request['AgregadoComercial']['TasaDeCambioPago']['FactorDeCalculo'] = $sale->sale_currency_trm;
                $request['AgregadoComercial']['TasaDeCambioPago']['FactorDeCalculoSpecified'] = true;
                $request['AgregadoComercial']['TasaDeCambioPago']['Fecha'] = "".$fechaEmision."";
                $request['AgregadoComercial']['TasaDeCambioPago']['FechaSpecified'] = true;
                     
            }
            return $request;
        }
        private function get_purchase_order($request, $sale){
            if ($sale->purchase_order && $sale->purchase_order !== '') {
                $request['Referencias']['ReferenciaDocOrden']['Id']['Value'] = $sale->purchase_order;
                $request['Referencias']['ReferenciaDocOrden']['IdPedido']['Value'] = $sale->purchase_order;
                $request['Referencias']['ReferenciaDocOrden']['IndicadorCopiaSpecified'] = false;
                $request['Referencias']['ReferenciaDocOrden']['FechaOrdenSpecified'] = false;
                $request['Referencias']['ReferenciaDocOrden']['HoraOrdenSpecified'] = false;
                $request['Referencias']['ReferenciaDocOrden']['ReferenciaADocumento']['Id']['Value'] = $sale->purchase_order;
                $request['Referencias']['ReferenciaDocOrden']['ReferenciaADocumento']['EsCopiaSpecified'] = false;
                $request['Referencias']['ReferenciaDocOrden']['ReferenciaADocumento']['FechaSpecified'] = false;
                $request['Referencias']['ReferenciaDocOrden']['ReferenciaADocumento']['HoraSpecified'] = false;
                $request['Referencias']['ReferenciaDocOrden']['ReferenciaADocumento']['CodigoTipoDoc']['Value'] = 'RPEDI';
                $request['Referencias']['ReferenciaDocOrden']['ReferenciaADocumento']['NombreTipoDoc']['Value'] = 'RPEDI';
                $request['Referencias']['ReferenciaDocOrden']['ReferenciaADocumento']['XPath']= [];        
            }
            return $request;
        }
    /*************************************************************************** METODOS CREADOS PARA EL REQUEST DE SIMBA ***********************************************************************************/
}

/* End of file Electronic_billing_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/Electronic_billing_model */