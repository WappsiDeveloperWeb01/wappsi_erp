<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="withholding_tax_table" class="table table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th></th>
                                            <th><?= lang('withholding_tax_description') ?></th>
                                            <th><?= lang('withholding_tax_minimum_base') ?></th>
                                            <th><?= lang('withholding_tax_percentage') ?></th>
                                            <th><?= lang('withholding_tax_basis_calculation') ?></th>
                                            <th><?= lang('withholding_tax_transaction_type') ?></th>
                                            <th><?= lang('withholding_tax_type') ?></th>
                                            <th><?= lang('actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th><?= lang('withholding_tax_description') ?></th>
                                            <th><?= lang('withholding_tax_minimum_base') ?></th>
                                            <th><?= lang('withholding_tax_percentage') ?></th>
                                            <th><?= lang('withholding_tax_basis_calculation') ?></th>
                                            <th><?= lang('withholding_tax_transaction_type') ?></th>
                                            <th><?= lang('withholding_tax_type') ?></th>
                                            <th><?= lang('actions'); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        load_withholding_tax_table();

        $(document).on('change', '#percentage', function() {
            validate_allowed_percentages()
        });
        $(document).on('change', '#type', function() {
            load_retention_rates()
        });

        $('[data-toggle-second="tooltip"]').tooltip();
    });

    function load_withholding_tax_table() {
        if ($(window).width() < 1000) {
            var nums = [
                [10, 25],
                [10, 25]
            ];
        } else {
            var nums = [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ];
        }

        oTable = $('#withholding_tax_table').dataTable({
            "aaSorting": [
                [1, "asc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/get_withholding_tax_datatables') ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex) {},
            drawCallback: function(settings) {
                $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/add_withholding_tax') ?>" class="btn btn-primary new-button" data-toggle-second="tooltip" data-placement="top" title="Agregar" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus fa-lg"></i></a>');

                $('[data-toggle-second="tooltip"]').tooltip();
            },
            aoColumns: [{
                    'bVisible': false
                },
                null,
                {
                    className: 'text-right'
                },
                {
                    className: 'text-right'
                },
                null,
                null,
                {
                    // mRender:  function ( data, type, row ) {
                    //     if (data == 0) {
                    //         return '<i class="fa fa-ban text-danger"></i>';
                    //     } else {
                    //         return '<i class="fa fa-check text-success"></i>';
                    //     }
                    // },
                    className: 'text-center'
                },
                null,
            ],
            fnFooterCallback: function(nRow, aaData, iStart, iEnd, aiDisplay) {
                var nCells = nRow.getElementsByTagName('th');
            }
        }).fnSetFilteringDelay().dtFilter([], "footer");
    }

    function validate_allowed_percentages() {
        var type = $('#type').val();
        var percentage = $('#percentage').val()

        if (type != 'ICA' && percentage != '') {
            if (type == 'IVA') {
                var rates_array = [15, 100];
            } else if (type != 'BOMB' && type != 'TABL') {
                var rates_array = [0.1, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5, 4.0, 6.0, 7.0, 10.0, 11.0, 20.0];
            }

            if (rates_array.includes(parseFloat(percentage)) == false) {
                command: toastr.error('El dato ingresado no coincide con los porcentajes dados por la DIAN.', '¡Error validación', {
                    onHidden: function() {
                        $('#percentage').val('');
                    }
                });
                return false;
            }
        }
    }

    function load_retention_rates() {
        var options = '';
        var type = $('#type').val();

        if (type != 'ICA') {
            if (type == 'IVA') {
                $('#percentage').prop('placeholder', 'Permitidos: 15%, 100%');
            } else if (type != 'BOMB' && type != 'TABL') {
                $('#percentage').prop('placeholder', 'Permitidos: 0.1%, 0.5%, 1%, 1.5%, 2%, 2.5%, 3%, 3.5%, 4%, 6%, 7%, 10%, 11%, 20%');
            }
        } else {
            $('#percentage').prop('placeholder', '');
        }
    }
</script>