<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 *
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */

$lang['add_customer']                       = "Añadir cliente";
$lang['edit_customer']                      = "Editar clientes";
$lang['deactivate_customer']                    = "Inactivar cliente";
$lang['deactivate_customers']                   = "Inactivar clientes";
$lang['activate_customer']                   = "Activar cliente";
$lang['activate_customers']                   = "Activar clientes";
$lang['customer_added']                     = "Cliente agregado correctamente";
$lang['customer_updated']                   = "Cliente actualizado correctamente";
$lang['customer_deleted']                   = "Cliente eliminado correctamente";
$lang['customer_deactivated']                   = "Cliente desactivado correctamente";
$lang['customer_activated']                   = "Cliente activado correctamente";
$lang['customers_deactivated']                  = "Clientes desactivados correctamente";
$lang['import_by_csv']                      = "Agregar clientes por archivo CSV";
$lang['edit_profile']                       = "Editar usuario";
$lang['delete_user']                        = "Desactivar usuario";
$lang['no_customer_selected']               = "Ningún cliente seleccionado. Por favor seleccione al menos uno";
$lang['pw_not_same']                        = "La contraseña y la confirmación de la contraseña no son iguales";
$lang['user_added']                         = "Usuario agregado correctamente";
$lang['user_deleted']                       = "Usuario eliminado correctamente";
$lang['customer_x_deactivated_have_sales']      = "No se puede desactivar, el cliente tiene ventas";
$lang['customers_x_deactivated_have_sales']     = "Algunos clientes no pueden ser desactivados ya que tienen ventas";
$lang['check_customer_email']               = "Por Favor verifique correo electronico";
$lang['customer_already_exist']             = "Cliente ya existe previamente con este correo";
$lang['line_no']                            = "Numero de linea";
$lang['first_6_required']                   = "Las primeras 6 columnas son necesarias.";
$lang['customer_deposit_balance']			= "Saldo de anticipos";
$lang['customer_without_balance']			= "No hay anticipos con saldos registrados";

//NO TRADUCIDAS

$lang['customers_added'] = 'Clientes agregados correctamente';
$lang['customer_x_deleted_have_sales'] = 'El cliente tiene ventas o anticipos, no se puede eliminar';
$lang['customer_x_deleted'] = 'Advertencia, este cliente no puede ser eliminado';
$lang['customer_x_deactivated'] = 'Advertencia, este cliente no puede ser desactivado';
$lang['customer_x_activated'] = 'Advertencia, este cliente no puede ser desactivado';
$lang['paid_by'] = 'Tipo';
$lang['deposits'] = 'Anticipos';
$lang['list_deposits'] = 'Lista de anticipos';
$lang['add_deposit'] = 'Crear nuevo anticipo';
$lang['edit_deposit'] = 'Editar anticipo';
$lang['delete_deposit'] = 'Eliminar anticipo';
$lang['deposit_added'] = 'Anticipo agregado correctamente';
$lang['deposit_updated'] = 'Anticipo actualizado correctamente';
$lang['deposit_deleted'] = 'Anticipo eliminado correctamente';
$lang['deposits_subheading'] = 'Por favor utilice la tabla de abajo o realice una búsqueda';
$lang['deposit_note'] = 'Nota de anticipo';
$lang['list_addresses'] = 'Lista de sucursales';
$lang['addresses'] = 'Sucursales';
$lang['line1'] = 'Sucursal línea 1';
$lang['line2'] = 'Sucursal línea 2';
$lang['add_address'] = 'Agregar Sucursal';
$lang['edit_address'] = 'Editar Sucursal';
$lang['delete_address'] = 'Eliminar Sucursal';
$lang['address_added'] = 'Sucursal agregada correctamente';
$lang['address_updated'] = 'Sucursal actualizada correctamente';
$lang['address_deleted'] = 'Sucursal eliminada correctamente';