<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $_post = $this->session->flashdata("post") ? $this->session->flashdata("post") : ""; ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row" id="sortable-view">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <?= admin_form_open_multipart("payroll_contracts/save", ["class" => "wizard-big wizard", "id" => "add_contract_form"]); ?>
                        <?= form_hidden('module', $module); ?>
                        <h1><?= lang("contract") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("payroll_contracts_employee"), "company_id"); ?>
                                        <?php
                                            $emopts[""] = lang('select');
                                            if (!empty($employees)):
                                                foreach ($employees as $employee2):
                                                    $emopts[$employee2->id] = ucwords($employee2->name);
                                                endforeach;
                                            endif;
                                        ?>
                                        <?php $company_id = !empty($employee_id) ? $employee_id : "" ?>
                                        <?= form_dropdown(["name"=>"company_id", "id"=>"company_id", "class"=>"form-control", "required"=>TRUE], $emopts, $company_id, !empty($employee_id) ? "disabled" : ""); ?>
                                        <?= form_hidden('employee_id', ''); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("internal_code"), "internal_code"); ?>
                                        <?= form_input(["name"=>"internal_code", "id"=>"internal_code", "class"=>"form-control", "required"=>TRUE], (isset($_post->internal_code) ? $_post->internal_code : "")); ?>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contract_type"), "contract_type"); ?>
                                        <?php
                                            $topts[""] = lang('select');
                                            foreach ($types_contracts as $type_contract):
                                                $topts[$type_contract->id] = $type_contract->description;
                                            endforeach
                                        ?>
                                        <?= form_dropdown(["name"=>"contract_type", "id"=>"contract_type", "class"=>"form-control not_select", "required"=>TRUE], $topts); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contracts_start_date"), "start_date"); ?>
                                        <?= form_input(["name"=>"start_date", "id"=>"start_date", "type"=>"date", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contracts_end_date"), "end_date"); ?>
                                        <?= form_input(["name"=>"end_date", "id"=>"end_date", "type"=>"date", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("contracts_settlement_date"), "settlement_date"); ?>
                                        <?= form_input(["name"=>"settlement_date", "id"=>"settlement_date", "type"=>"date", "class"=>"form-control", "disabled"=>TRUE, "max"=>date('Y-m-d')]); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("workday"), "workday"); ?>
                                        <?php
                                            $wopts[""] = lang('select');
                                            foreach ($workdays as $workday) {
                                                if ($workday->id == 1 || $workday->id == 2) {
                                                    $wopts[$workday->id] = $workday->name;
                                                }
                                            }
                                        ?>
                                        <?= form_dropdown(["name"=>"workday", "id"=>"workday", "class"=>"form-control not_select", "required"=>TRUE], $wopts); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("payroll_contracts_biller"), "biller_id"); ?>
                                        <?php
                                            $bopts[""] = lang('select');
                                            $selected = '';
                                            $number_billers = count($billers);
                                            foreach ($billers as $key => $biller):
                                                $bopts[$biller->id] = "$biller->name ($biller->company)";
                                                if ($number_billers == 1 && $key == 0) {
                                                    $selected = $biller->id;
                                                }
                                            endforeach
                                        ?>
                                        <?= form_dropdown(["name"=>"biller_id", "id"=>"biller_id", "class"=>"form-control not_select", "required"=>TRUE], $bopts, $selected); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("retired_risk"), 'retired_risk'); ?>
                                        <?php $mopts = [0=>lang("no"), 1=>lang("yes")]; ?>
                                        <?= form_dropdown(["name"=>"retired_risk", "id"=>"retired_risk", "class"=>"form-control not_select", "required"=>TRUE], $mopts); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("area"), 'area'); ?>
                                        <?php
                                            $aopts[""] = lang("select");
                                            foreach ($areas as $area):
                                                $aopts[$area->id] = $area->name;
                                            endforeach
                                        ?>
                                        <?= form_dropdown(["name"=>"area", "id"=>"area", "class"=>"form-control not_select", "required"=>TRUE], $aopts); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("professional_position"), "professional_position"); ?>
                                        <?php $popts = ["" => lang("select")] ?>
                                        <div class="input-group">
                                            <?= form_dropdown(["name"=>"professional_position", "id"=>"professional_position", "class"=>"form-control not_select", "required"=>TRUE], $popts); ?>
                                            <span class="input-group-btn" style="vertical-align: top;">
                                                <?= form_button(["id"=>"add_professional_position", "class"=>"btn btn-primary", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>"Agregar cargo"], '<i class="fa fa-plus tol" aria-hidden="true"></i>'); ?>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h1><?= lang("contracts_salary") ?></h1>
                        <section>
                            <div id="contact_data_container">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("employee_type"), "employee_type"); ?>
                                            <select name="employee_type" id="employee_type" class="form-control not_select" required="required">
                                                <option value=""><?= lang("select"); ?></option>
                                                <?php foreach ($types_employees as $type_employee): ?>
                                                    <option value="<?= $type_employee->code; ?>"
                                                        data-afp="<?= $type_employee->afp; ?>"
                                                        data-eps="<?= $type_employee->eps; ?>"
                                                        data-arl="<?= $type_employee->arl; ?>"
                                                        data-caja="<?= $type_employee->caja; ?>"p
                                                        data-icbf="<?= $type_employee->icbf; ?>"
                                                        data-sena="<?= $type_employee->sena; ?>"
                                                        data-cesantias="<?= $type_employee->cesantias; ?>"><?= $type_employee->code ." - ". $type_employee->description; ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payroll_contracts_base_amount"), "base_amount"); ?>
                                            <div class="input-group">
                                                <?= form_input(["name"=>"base_amount", "id"=>"base_amount", "type"=>"number", "class"=>"form-control", "min"=>0, "required"=>TRUE], ''); ?>
                                                <span class="input-group-addon btn-success" id="get_minimum_salary" data-toggle="tooltip" title="Si desea obtener el valor del Salario míinimo haga clic aquí"><i class="fa fa-clipboard" aria-hidden="true"></i></span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payroll_contracts_integral_salary"), "integral_salary"); ?>
                                            <?php $sopts = [ NOT => lang("no"), YES => lang("yes")]; ?>
                                            <?= form_dropdown(["name"=>"integral_salary", "id"=>"integral_salary", "class"=>"form-control", "disabled"=>TRUE, "required"=>TRUE], $sopts); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("trans_allowance"), "trans_allowance"); ?>
                                            <?php $aopts = [ NOT => lang("no"), YES => lang("yes")]; ?>
                                            <?= form_dropdown(["name"=>"trans_allowance", "id"=>"trans_allowance", "class"=>"form-control"], $aopts); ?>
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payroll_contracts_payment_frequency"), "payment_frequency"); ?>
                                            <?php $fopts = [""=>lang("select")]; ?>
                                            <?php if (!empty($payment_frequencies)): ?>
                                                <?php foreach ($payment_frequencies as $payment_frequency) {
                                                    $fopts[$payment_frequency->id] = $payment_frequency->period;
                                                } ?>
                                            <?php endif; ?>
                                            <?php
                                                if ($this->Payroll_settings->payment_frequency != DEFINED_IN_CONTRACT) {
                                                    $payment_frequency = $this->Payroll_settings->payment_frequency;
                                                    $disable_payment_frequency = 'disabled="disabled"';
                                                } else {
                                                    $disable_payment_frequency = '';
                                                    $payment_frequency = '';
                                                }
                                            ?>
                                            <?= form_dropdown(["name"=>"payment_frequency", "id"=>"payment_frequency", "class"=>"form-control", "required"=>TRUE], $fopts, $payment_frequency, $disable_payment_frequency); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("withholding_method"), "withholding_method"); ?>
                                            <?php $mopts = ["0"=>lang("n/a"), WITHHOLDING_PROCESS_1=>lang("payroll_contracts_process_1"), WITHHOLDING_PROCESS_2=>lang("payroll_contracts_process_2")]; ?>
                                            <?= form_dropdown(["name"=>"withholding_method", "id"=>"withholding_method", "class"=>"form-control not_select", "required"=>TRUE], $mopts); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("withholding_percentage"), "withholding_percentage"); ?>
                                            <div class="input-group">
                                                <?= form_input(["name"=>"withholding_percentage", "id"=>"withholding_percentage", "type"=>"number", "class"=>"form-control", "min"=>0, "disabled"=>TRUE, "required"=>TRUE], ''); ?>
                                                <span class="input-group-addon">%</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("payment_method"), "payment_method"); ?>
                                            <?php
                                                $popts = [""=>lang("select")];
                                                foreach ($payment_means as $payment_mean) {
                                                    $popts[$payment_mean->code] = $payment_mean->name;
                                                }
                                            ?>
                                            <?= form_dropdown(["name"=>"payment_method", "id"=>"payment_method", "class"=>"form-control not_select", "required"=>TRUE], $popts); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("bank"), "bank"); ?>
                                            <?php
                                                $bopts = [""=>lang("select")];
                                                foreach ($banks as $bank) {
                                                    $bopts[$bank->id] = ucwords(mb_strtolower($bank->company));
                                                }
                                            ?>
                                            <?= form_dropdown(["name"=>"bank", "id"=>"bank", "class"=>"form-control not_select", "disabled"=>TRUE, "required"=>TRUE], $bopts); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("account_type"), "account_type"); ?>
                                            <?php $aopts = [""=>lang("select"), 1=>lang("payroll_contracts_savings_account"), 2=>lang("payroll_contracts_current_account")/*, 3=>lang("payroll_contracts_account_virtual_wallet")*/]; ?>
                                            <?= form_dropdown(["name"=>"account_type", "id"=>"account_type", "class"=>"form-control not_select", "disabled"=>TRUE, "required"=>TRUE], $aopts); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("account_no"), "account_no"); ?>
                                            <?= form_input(["name"=>"account_no", "id"=>"account_no", "type"=>"number", "class"=>"form-control", "disabled"=>TRUE, "required"=>TRUE]); ?>
                                        </div>
                                    </div>
                                </div>

                                <?php if (!empty($concepts)) : ?>
                                    <hr>
                                    <h3>Bonificaciones</h3>
                                    <br>

                                    <div class="row">
                                        <?php foreach ($concepts as $key => $concept) : ?>
                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="form-label"><?= $concept->name; ?></label>
                                                    <div class="checkbox" style="margin-left: 10px; margin-top: 6px;">
                                                        <input class="concepts" type="checkbox" name="concept_check[<?= $concept->id ?>]" id="concept_check_<?= $concept->id ?>" data-concept="<?= $concept->id ?>" value="<?= $concept->id ?>"/> Activar
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-3">
                                                <div class="form-group">
                                                    <label class="form-label">Monto</label>
                                                    <input class="form-control" type="number" name="concept[<?= $concept->id ?>]" id="concept_<?= $concept->id ?>" disabled/>
                                                    <input type="hidden" name="concept_type[<?= $concept->id ?>]" id="concept_type_<?= $concept->id ?>" value="<?= $concept->concept_type_id; ?>"/>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </section>

                        <h1><?= lang("payroll_contracts_social_security") ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("afp"), "afp"); ?>
                                        <?php $afopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($afps)):
                                                foreach ($afps as $afp) :
                                                    $afopts[$afp->id] = ucwords(mb_strtolower($afp->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?= form_dropdown(["name"=>"afp", "id"=>"afp", "class"=>"form-control not_select", "required"=>TRUE], $afopts); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("eps"), "eps"); ?>
                                        <?php $eopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($epss)):
                                                foreach ($epss as $eps) :
                                                    $eopts[$eps->id] = ucwords(mb_strtolower($eps->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?= form_dropdown(["name"=>"eps", "id"=>"eps", "class"=>"form-control not_select", "required"=>TRUE], $eopts); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("cesantia"), "cesantia"); ?>
                                        <?php $csopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($cesantias)):
                                                foreach ($cesantias as $cesantia) :
                                                    $csopts[$cesantia->id] = ucwords(mb_strtolower($cesantia->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?= form_dropdown(["name"=>"cesantia", "id"=>"cesantia", "class"=>"form-control not_select", "required"=>TRUE], $csopts); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("caja"), "caja"); ?>
                                        <?php $copts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($cajas)):
                                                foreach ($cajas as $caja) :
                                                    $copts[$caja->id] = ucwords(mb_strtolower($caja->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?= form_dropdown(["name"=>"caja", "id"=>"caja", "class"=>"form-control not_select", "required"=>TRUE], $copts); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("arl"), "arl"); ?>
                                        <?php $rlopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($arls)):
                                                foreach ($arls as $arl) :
                                                    $rlopts[$arl->id] = ucwords(mb_strtolower($arl->company));
                                                endforeach;
                                            endif;
                                        ?>
                                        <?= form_dropdown(["name"=>"arl", "id"=>"arl", "class"=>"form-control not_select", "required"=>TRUE], $rlopts); ?>
                                    </div>
                                </div>

                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("arl_risk_classes"), "arl_risk_classes"); ?>
                                        <?php $arcopts[""] = lang("select"); ?>
                                        <?php
                                            if (!empty($arl_risk_classes)):
                                                foreach ($arl_risk_classes as $arl_risk_class) :
                                                    $arcopts[$arl_risk_class->id] = $arl_risk_class->name;
                                                endforeach;
                                            endif;
                                        ?>
                                        <?= form_dropdown(["name"=>"arl_risk_classes", "id"=>"arl_risk_classes", "class"=>"form-control not_select", "required"=>TRUE], $arcopts); ?>
                                    </div>
                                </div>

                                <!-- <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("icbf"), "icbf"); ?>
                                        <?php $iopts = [0=>lang("no"), 1=>lang("yes")]; ?>
                                        <?= form_dropdown(["name"=>"icbf", "id"=>"icbf", "class"=>"form-control not_select", "required"=>TRUE], $iopts); ?>
                                    </div>
                                </div> -->
                            </div>
                            <!-- <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <?= form_label(lang("sena"), "sena"); ?>
                                        <?php $sopts = [0=>lang("no"), 1=>lang("yes")]; ?>
                                        <?= form_dropdown(["name"=>"sena", "id"=>"sena", "class"=>"form-control not_select", "required"=>TRUE], $sopts); ?>
                                    </div>
                                </div>
                            </div> -->
                        </section>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade"  id="add_professional_position_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog  modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Agregar Cargo <small id="area_name"></small></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label("Cargo", "new_professional_position"); ?>
                            <?= form_input(["name"=>"new_professional_position", "id"=>"new_professional_position", "class"=>"form-control"]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_button(["class"=>"btn btn-default", "data-dismiss"=>"modal"], lang("cancel")); ?>
                <?= form_button(["id"=>"save_new_professional_position", "class"=>"btn btn-primary"], lang("save")); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip();

        $(document).on('change', '#base_amount', function() { enable_disable_integral_salary_fields(); calculate_withholding_method(); });
        $(document).on('change', '#internal_code', function(){ validate_existing_internal_code(); });
        $(document).on('change', '#contract_type', function(){ enable_disable_end_date_input(); });
        $(document).on('change', '#start_date', function() { set_minimum_date_contract_end_date() });
        $(document).on('change', '#end_date', function() { set_maximun_date_contract_end_date() });
        $(document).on('change', '#area', function(){ load_professional_position(); });
        $(document).on('click', '#add_professional_position', function(){ add_professional_position(); });
        $(document).on('click', '#save_new_professional_position', function(){ save_new_professional_position(); });
        $(document).on('change', '#employee_type', function() { /*calculateApprenticeSalaryPercentage();*/ enable_disable_social_security_fields(); });
        $(document).on('change', '#withholding_method', function() { enable_disable_withholding_percentage_fields(); });
        $(document).on('keydown', '#withholding_percentage', function(e) { validate_number_comma(e); });
        $(document).on('change', '#payment_method', function() { enable_disable_bank_data_fields(); });
        $(document).on('change', '#company_id', function() { enable_disable_employee_input(); });
        $(document).on('click', '#get_minimum_salary', function(event) { get_minimum_salary() });

        <?php if (!empty($employee_id)) : ?>
            $('#company_id').trigger('change');
        <?php endif; ?>

        load_wizard_configuration();
    });

    function load_wizard_configuration()
    {
        var form = $("#add_contract_form");
        wizard = form.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onInit: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();

                $('.concepts').on('ifChecked', function(event) {
                    var concept_id = $(this).data('concept');
                    console.log(concept_id);
                    $('#concept_'+concept_id).attr({'disabled': false, 'required': true});
                });

                $('.concepts').on('ifUnchecked', function(event) {
                    var concept_id = $(this).data('concept');
                    console.log(concept_id);
                    $('#concept_'+concept_id).attr({'disabled': true, 'required': false});
                });

                load_temporary_data();
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                form.validate({
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                        } else {
                            elem.addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                        } else {
                            elem.removeClass(errorClass);
                        }
                    }
                }).settings.ignore = ":disabled,:hidden:not(.validate)";

                create_temporary_data(currentIndex);

                return form.valid();
            },
            onStepChanged: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                if(form.valid()){
                    $(form).submit();
                }
            }
        });

        $(document).on("select2-opening", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror")) {
                $(".select2-drop ul").addClass("mierror");
            } else {
                $(".select2-drop ul").removeClass("mierror");
            }
        });

        $(document).on("select2-close", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror") && $("#" + elem.attr("id")).val() != '') {
                $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');

                $("#" + elem.attr("id") + "-error").css('display', 'none');
            }
        });
    }

    function load_temporary_data()
    {
        if (sessionStorage.getItem('company_id') != null) {
            $('#company_id').select2('val', sessionStorage.getItem('company_id'));
            $('#company_id').trigger('change');
        }
        $('#internal_code').val(sessionStorage.getItem('internal_code'));
        $('#start_date').val(sessionStorage.getItem('start_date'));
        $('#end_date').val(sessionStorage.getItem('end_date'));
        $('#settlement_date').val(sessionStorage.getItem('settlement_date'));
        $('#biller_id').val(sessionStorage.getItem('biller_id'));
        $('#retired_risk').val(sessionStorage.getItem('retired_risk'));
        $('#workday').select2('val', sessionStorage.getItem('workday'));

        if (sessionStorage.getItem('contract_type') != '') {
            $('#contract_type').select2('val', sessionStorage.getItem('contract_type'));
            $('#contract_type').trigger('change');
        }
        if (sessionStorage.getItem('area') != '') {
            $('#area').select2('val', sessionStorage.getItem('area'));
            $('#area').trigger('change');

            setTimeout(function () {
                $('#professional_position').select2('val', sessionStorage.getItem('professional_position'));
            }, 1000);
        }

        $('#payment_frequency').select2('val', sessionStorage.getItem('payment_frequency'));
        $('#withholding_method').select2('val', sessionStorage.getItem('withholding_method'));
        if (sessionStorage.getItem('base_amount') > 0) {
            $('#base_amount').val(sessionStorage.getItem('base_amount'));
            $('#base_amount').trigger('change');

            $('#integral_salary').select2('val', sessionStorage.getItem('integral_salary'));
            $('#trans_allowance').select2('val', sessionStorage.getItem('trans_allowance'));
        }
        $('#payment_frequency').select2('val', sessionStorage.getItem('payment_frequency'));
        $('#withholding_method').select2('val', sessionStorage.getItem('withholding_method'));
        $('#withholding_percentage').val(sessionStorage.getItem('withholding_percentage'));

        if (sessionStorage.getItem('payment_method') != '') {
            $('#payment_method').val(sessionStorage.getItem('payment_method'));
            $('#payment_method').trigger('change');

            setTimeout(function () {
                $('#bank').select2('val', sessionStorage.getItem('bank'));
                $('#account_type').val(sessionStorage.getItem('account_type'));
                $('#account_no').val(sessionStorage.getItem('account_no'));
            }, 300);
        }
    }

    function create_temporary_data(tab_index)
    {

        if (tab_index == 0) {
            sessionStorage.setItem("company_id", $('#company_id').val());
            sessionStorage.setItem("internal_code", $('#internal_code').val());
            sessionStorage.setItem("contract_type", $('#contract_type').val());
            sessionStorage.setItem("start_date", $('#start_date').val());
            sessionStorage.setItem("end_date", $('#end_date').val());
            sessionStorage.setItem("settlement_date", $('#settlement_date').val());
            sessionStorage.setItem("workday", $('#workday').val());
            sessionStorage.setItem("biller_id", $('#biller_id').val());
            sessionStorage.setItem("retired_risk", $('#retired_risk').val());
            sessionStorage.setItem("area", $('#area').val());
            sessionStorage.setItem("professional_position", $('#professional_position').val());
        }

        if (tab_index == 1) {
            sessionStorage.setItem("base_amount", $('#base_amount').val());
            sessionStorage.setItem("integral_salary", $('#integral_salary').val());
            sessionStorage.setItem("trans_allowance", $('#trans_allowance').val());
            sessionStorage.setItem("payment_frequency", $('#payment_frequency').val());
            sessionStorage.setItem("withholding_percentage", $('#withholding_method').val());
            sessionStorage.setItem("withholding_percentage", $('#withholding_percentage').val());
            sessionStorage.setItem("payment_method", $('#payment_method').val());
            sessionStorage.setItem("bank", $('#bank').val());
            sessionStorage.setItem("account_type", $('#account_type').val());
            sessionStorage.setItem("account_no", $('#account_no').val());
        }
    }

    function enable_disable_integral_salary_fields()
    {
        var minimumSalaryValue = <?= $this->Payroll_settings->minimum_salary_value; ?>;
        var APPRENTICES_SENA_PRODUCTIVE = <?= APPRENTICES_SENA_PRODUCTIVE ?>;
        var APPRENTICES_SENA_LECTIVA = <?= APPRENTICES_SENA_LECTIVA ?>;
        var employeeType = $('#employee_type').val();
        var baseAmount = $('#base_amount').val();
        var workday = $('#workday').val();
        var HALFTIME = <?= HALFTIME ?>;

        if (employeeType == APPRENTICES_SENA_LECTIVA || employeeType == APPRENTICES_SENA_PRODUCTIVE) {
            var percentageSalaryApprenticeTeaching = <?= $this->Payroll_settings->percentage_salary_apprentice_teaching; ?>;
            var percentageSalaryApprenticeProductive = <?= $this->Payroll_settings->percentage_salary_apprentice_productive; ?>;

            if (employeeType == APPRENTICES_SENA_LECTIVA) {
                minimumValueApprenticeSalary = minimumSalaryValue * (percentageSalaryApprenticeTeaching / 100);
            } else {
                minimumValueApprenticeSalary = minimumSalaryValue * (percentageSalaryApprenticeProductive / 100);
            }

            minimumSalaryValue = minimumValueApprenticeSalary;
        } else if (workday == HALFTIME) {
            minimumSalaryValue = minimumSalaryValue / 2;
        }

        if (baseAmount > 0 && baseAmount < minimumSalaryValue) {
            command: toastr.error('El valor del salario no puede ser menor al Salario mínimo estipulado por ley', '¡Error!', {onHidden: function() { $('#base_amount').val(''); }});
        }

        if (baseAmount > <?= $this->Payroll_settings->integral_salary_value; ?>) {
            $('#integral_salary').prop('disabled', false);
        } else {
            $('#integral_salary').select2('val', '<?= NOT ?>');
            $('#integral_salary').prop('disabled', true);
        }

        var minimumSalaryValueToTransAllowance = <?= $this->Payroll_settings->minimum_salary_value * 2 ?>;
        if (baseAmount > minimumSalaryValueToTransAllowance) {
            $('#trans_allowance').select2('val', '<?= NOT ?>');
            $('#trans_allowance').prop('disabled', true);
        } else {
            $('#trans_allowance').prop('disabled', false);
            $('#trans_allowance').select2('val', '<?= YES ?>');
        }
    }

    function get_minimum_salary() {
        $('#base_amount').val(<?= $this->Payroll_settings->minimum_salary_value; ?>);
        $('#base_amount').trigger('change');
    }

    function validate_existing_internal_code()
    {
        var internal_code = $('#internal_code').val();
        $.ajax({
            url: '<?= admin_url("payroll_contracts/validate_existing_internal_code"); ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                'internal_code': internal_code
            },
        })
        .done(function(data) {
            if (data == true) {
                command:toastr.error('<?= lang("payroll_contracts_existing_internal_code"); ?>', '<?= lang("toast_error_title") ?>', {onHidden:function() { $("#internal_code").val(''); $("#internal_code").focus(); }});
            }
        })
        .fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function enable_disable_end_date_input()
    {
        var type_contract = $('#contract_type').val();

        if (type_contract == '<?= UNDEFINED_TERM ?>') {
            $('#end_date').prop('disabled', true);
            $('#end_date').prop('required', false);
        } else {
            $('#end_date').prop('disabled', false);
            $('#end_date').prop('required', true);
        }
    }

    function set_minimum_date_contract_end_date()
    {
        var start_date = $('#start_date').val();
        $('#end_date').prop('min', start_date);
        $('#settlement_date').prop('min', start_date);
    }

    function set_maximun_date_contract_end_date()
    {
        var end_date = $('#end_date').val();
        $('#start_date').prop('max', end_date);
    }

    function load_professional_position()
    {
        var area = $('#area').val();

        $.ajax({
            url: '<?= admin_url("payroll_contracts/get_professional_position") ?>',
            type: 'GET',
            dataType: 'html',
            data: {
                'area': area
            },
        })
        .done(function(data) {
            $('#professional_position').html(data);
            $('#professional_position').select2('val', "");
        })
        .fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function add_professional_position()
    {
        if ($('#area').val() != '') {
            $('#add_professional_position_modal').modal({backdrop: 'static', keyboard: false});
            $('#area_name').html($('#area option:selected').text());
        } else {
            $('#area').select2('focus');
        }
    }

    function save_new_professional_position()
    {
        $.ajax({
            url: '<?= admin_url("professional_positions/save") ?>',
            type: 'GET',
            dataType: 'json',
            data: {
                "area_id": $('#area').val(),
                "new_professional_position": $('#new_professional_position').val()
            },
        })
        .done(function(response) {
            if (response.status == true) {
                command:toastr.success(response.message, '<?= lang("toast_success_title"); ?>', {onHidden: function(){
                    $('#professional_position').html(response.data);
                    $('#professional_position').select2('val', response.option_selected);

                    $('#add_professional_position_modal').modal('hide');
                }});
            } else {
                command:toastr.error(response.message, '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
            }
        })
        .fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function validate_number_comma(event)
    {
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 190 || event.keyCode == 110 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode == 67 && event.ctrlKey === true) || (event.keyCode == 86 && event.ctrlKey === true) || (event.keyCode == 88 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 40) ) {

            return;
        }
        else {
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    }

    // function calculateApprenticeSalaryPercentage()
    // {
    //     var employeeType = $('#employee_type').val();
    //     var APPRENTICES_SENA_LECTIVA = <?= APPRENTICES_SENA_LECTIVA ?>;
    //     var APPRENTICES_SENA_PRODUCTIVE = <?= APPRENTICES_SENA_PRODUCTIVE ?>;

    //     if (employeeType == APPRENTICES_SENA_LECTIVA || employeeType == APPRENTICES_SENA_PRODUCTIVE) {
    //         var minimumSalaryValue = <?= $this->Payroll_settings->minimum_salary_value; ?>;
    //         var percentageSalaryApprenticeTeaching = <?= $this->Payroll_settings->percentage_salary_apprentice_teaching; ?>;
    //         var percentageSalaryApprenticeProductive = <?= $this->Payroll_settings->percentage_salary_apprentice_productive; ?>;

    //         if (employeeType == APPRENTICES_SENA_LECTIVA) {
    //             minimumValueApprenticeSalary = minimumSalaryValue * (percentageSalaryApprenticeTeaching / 100);
    //             $('#base_amount').val(minimumValueApprenticeSalary);
    //             $('#base_amount').attr('min', minimumValueApprenticeSalary);
    //         } else {
    //             minimumValueApprenticeSalary = minimumSalaryValue * (percentageSalaryApprenticeProductive / 100);
    //             $('#base_amount').val(minimumValueApprenticeSalary);
    //             $('#base_amount').attr('min', minimumValueApprenticeSalary);
    //         }
    //     } else {
    //         $('#base_amount').val('');
    //         $('#base_amount').attr('min', 0);
    //     }
    // }

    function enable_disable_social_security_fields()
    {
        var afp = $('#employee_type option:selected').data('afp');
        if (afp == '<?= YES ?>') { $('#afp').prop('disabled', false); } else { $('#afp').prop('disabled', true); }
        var eps = $('#employee_type option:selected').data('eps');
        if (eps == '<?= YES ?>') { $('#eps').prop('disabled', false); } else { $('#eps').prop('disabled', true); }
        var arl = $('#employee_type option:selected').data('arl');
        if (arl == '<?= YES ?>') { $('#arl').prop('disabled', false); } else { $('#arl').prop('disabled', true); }
        var caja = $('#employee_type option:selected').data('caja');
        if (caja == '<?= YES ?>') { $('#caja').prop('disabled', false); } else { $('#caja').prop('disabled', true); }
        var icbf = $('#employee_type option:selected').data('icbf');
        if (icbf == '<?= YES ?>') { $('#icbf').prop('disabled', false); } else { $('#icbf').prop('disabled', true); }
        var sena = $('#employee_type option:selected').data('sena');
        if (sena == '<?= YES ?>') { $('#sena').prop('disabled', false); } else { $('#sena').prop('disabled', true); }
        var cesantia = $('#employee_type option:selected').data('cesantias');
        if (cesantia == '<?= YES ?>') { $('#cesantia').prop('disabled', false); } else { $('#cesantia').prop('disabled', true); }
    }

    function calculate_withholding_method()
    {
        var base_amount = $('#base_amount').val();
        var withholding_base_value = '<?= $this->Payroll_settings->withholding_base_value * $this->Payroll_settings->UVT_value; ?>';

        if (parseFloat(base_amount) >= parseFloat(withholding_base_value)) {
            $("#withholding_method option[value='0']").prop("disabled", true);
            $("#withholding_method option[value='1']").prop("disabled", false);
            $("#withholding_method option[value='2']").prop("disabled", false);

            $('#withholding_method').select2('val', '1');
        } else {
            $("#withholding_method option[value='0']").prop("disabled", false);
            $("#withholding_method option[value='1']").prop("disabled", true);
            $("#withholding_method option[value='2']").prop("disabled", true);

            $('#withholding_method').select2('val', '0');
        }
    }

    function enable_disable_withholding_percentage_fields()
    {
        var withholding_method = $('#withholding_method').val();
        if (withholding_method == '<?= WITHHOLDING_PROCESS_2 ?>') {
            $('#withholding_percentage').prop('disabled', false);
        } else {
            $('#withholding_percentage').val('');
            $('#withholding_percentage').prop('disabled', true);
        }
    }

    function enable_disable_bank_data_fields()
    {
        var paymentMethod = $('#payment_method').val();
        if (paymentMethod == '<?= WIRE_TRANSFER; ?>') {
            $('#bank').prop('disabled', false);
            $('#account_type').prop('disabled', false);
            $('#account_no').prop('disabled', false);
        } else {
            $('#bank').prop('disabled', true);
            $('#account_type').prop('disabled', true);
            $('#account_no').prop('disabled', true);
        }
    }

    function enable_disable_employee_input()
    {
        var company_id = $('#company_id').val();
        if (company_id != '') {
            $('input[name="employee_id"]').val(company_id);
        }
    }
</script>
