<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('sales/order_actions', 'id="action-form"');
    } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="installmentsTable" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><?= lang("credit"); ?></th>
                                            <th><?= lang("customer"); ?></th>
                                            <th><?= lang("quota_number"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("fee"); ?></th>
                                            <th><?= lang("capital"); ?></th>
                                            <th><?= lang("current_interest"); ?></th>
                                            <th><?= lang("vat_current_interest"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <?php if ($this->Admin || $this->Owner) { ?>
                                                <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                            <?php } else if ($this->GP["financing-installment_edit"]) { ?>
                                                <th style="width:80px; text-align:center;"><?= lang("actions"); ?></th>
                                            <?php } ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        // openPrintWindow()
        loadDataTables()

        $(document).on('click', '.details td:not(:last-child)', function() {
            let reference = $(this).attr('reference')
            $('#myModal').modal({
                remote: site.base_url + 'financing/details/' + reference
            });
            $('#myModal').modal('show');
        })
    });

    function loadDataTables() {
        if ($(window).width() < 1000) {
            var nums = [
                [10, 25],
                [10, 25]
            ];
        } else {
            var nums = [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ];
        }

        $('#installmentsTable').dataTable({
            "aaSorting": [
                [1, "asc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('financing/getInstallments') ?>',
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            // fnRowCallback: function(row, data, displayIndex) {
            //     let paymentReference = data[1]

            //     $(row).addClass('details')
            //     $(row).attr('reference', paymentReference)

            //     return row
            // },
            drawCallback: function(settings) {

                $('[data-toggle-second="tooltip"]').tooltip();
            },
            "aoColumns": [{
                    visible: false
                },
                null,
                null,
                {
                    className: 'text-center'
                },
                {
                    className: 'text-right'
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    render: function(data) {
                        let labelColor = 'label-danger'
                        let labelText = 'Pendiente'

                        if (data == 1) {
                            labelColor = 'label-success'
                            labelText = 'Completado'
                        }

                        return `<div class="text-center"><span class="payment_status pending_errors label ${labelColor}" data-toggle="tooltip" data-placement="top">${labelText}</span></div>`;
                    }
                },
                <?php if ($this->Admin || $this->Owner) { ?>
                    null,
                <?php } else if ($this->GP["financing-installment_edit"]) { ?>
                    null,
                <?php } ?>
            ]
        });
    }

    function openPrintWindow() {
        <?php if (isset($installmentNo)) { ?>
            installmentNo = '<?= $installmentNo ?>'
            window.open(site.base_url + `financing/printReceipt/${installmentNo}`, '_blank');
        <?php } ?>
    }
</script>