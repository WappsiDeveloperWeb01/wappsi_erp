<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight no-print">
    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('payments/payment_actions', 'id="action-form"');
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <h4 class="text_filter"></h4>
                                <table id="SLData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("credit"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("vat_no"); ?></th>
                                            <th><?= lang("customer"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("initial_value"); ?></th>
                                            <th><?= lang("overdue"); ?></th>
                                            <th><?= lang("notified"); ?></th>
                                            <th><?= lang("reported"); ?></th>
                                            <!-- <th><?= lang("actions") ?></th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php }
?>

<script>
    $(document).ready(function() {
        loadDataTables()

        $(document).on('click', '#notifyCredits', notifyCredits)
        $(document).on('click', '#reportCredits', reportCredits)
    });

    function loadDataTables()
    {
        let _tabFilterFill = false;
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];

        if ($(window).width() < 1000) {
            var nums = [
                [10, 25],
                [10, 25]
            ];
        }

        oTable = $('#SLData').dataTable({
            aaSorting: [[1, "desc"]],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            sAjaxSource: site.base_url + 'financing/getFinancing',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    }, {
                        "name": "optionFilter",
                        "value": option_filter
                    }

                );
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });

                swal({
                    title: `<?= lang('toast_alert_title') ?>`,
                    text: `<?= lang('Cargando Datos, por favor espere...') ?>`,
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                })
            },
            fnDrawCallback: function() {
                if (!_tabFilterFill) {
                    $('.additionalControlsContainer').html('<div class="wizard">' +
                        '<div class="steps clearfix">' +
                            '<ul role="tablist">' +
                                '<li role="tab" class="first current index_list" aria-disabled="false" aria-selected="true" style="width:16.6%;">' +
                                    '<a class="wizard_index_step" data-optionfilter="all" href="#">'+
                                        '<span class="all_span">0</span><br><?= lang('all') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="to_notify" href="#">' +
                                        '<span class="to_notify_span">0</span><br><?= lang('to_notify') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="notified" href="#">'+
                                        '<span class="notified_span">0</span><br><?= lang('notified') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="to_report" href="#">'+
                                        '<span class="to_report_span">0</span><br><?= lang('to_report') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="reported" href="#">'+
                                        '<span class="reported_span">0</span><br><?= lang('reported') ?>' +
                                    '</a>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>').addClass('col-without-padding');

                    _tabFilterFill = true;
                }

                $('.actionsButtonContainer').html(`<div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">
                        <i class="fas fa-ellipsis-v fa-lg"></i>
                    </button>
                    <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a id="notifyCredits">
                                <i class="fa fa-exclamation-triangle fa-lg"></i> Notificar Créditos
                            </a>
                        </li>
                        <li>
                            <a id="reportCredits">
                                <i class="fa fa-exclamation-circle"></i> Reportar Créditos
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="${site.base_url}financing/initialReport" target="_blank">
                                <i class="fa fa-file-text-o fa-lg" aria-hidden="true"></i> Informe Procrédito
                            </a>
                        </li>
                    </ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue',
                    increaseArea: '20%'
                });

                loadDataTabFilters();
            },
            aoColumns: [
                { mRender: checkbox, bSortable: false },
                null,
                null,
                null,
                {
                    mRender: function (data) {
                        return data.toLowerCase().split(' ').map(function(word) {
                            return word.charAt(0).toUpperCase() + word.slice(1);
                        }).join(' ');
                    }
                },
                null,
                {
                    mRender: function (data) {
                        return formatMoney(data)
                    },
                    className: `text-right`
                },
                {
                    mRender: function(data, row) {
                        return (data == 1) ? `<i class="fa fa-check text-success" aria-hidden="true"></i>` : `<i class="fa fa-ban" aria-hidden="true"></i>`
                    },
                    className: `text-center`
                },
                {
                    mRender: function(data, row) {
                        return (data == 1) ? `<i class="fa fa-check text-success" aria-hidden="true"></i>` : `<i class="fa fa-ban" aria-hidden="true"></i>`
                    },
                    className: `text-center`
                },
                {
                    mRender: function(data, row) {
                        return (data == 2) ? `<i class="fa fa-check text-success" aria-hidden="true"></i>` : `<i class="fa fa-ban" aria-hidden="true"></i>`
                    },
                    className: `text-center`
                }
            ]
        });
    }

    function loadDataTabFilters() {
        $.ajax({
            type: 'post',
            url: site.base_url + 'financing/getFinancing',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                tabFilterData: 1,
                optionFilter: option_filter
            },
            dataType: 'json'
        }).done(function(data) {
            $('.all_span').text(data.all);
            $('.to_notify_span').text(data.toNotify);
            $('.notified_span').text(data.notified);
            $('.to_report_span').text(data.toReport);
            $('.reported_span').text(data.reported);

            swal.close()
        });
    }

    function setFilterText() {

        var reference_text = $('#p2payment_reference_no option:selected').data('dtprefix');
        var p2payment_method_text = $('#p2payment_method option:selected').text();
        var biller_text = $('#biller option:selected').text();
        var customer_text = $('#filter_customer').select2('data') !== null ? $('#filter_customer').select2('data').text : '';
        var start_date_text = $('#start_date_dh').val();
        var end_date_text = $('#end_date_dh').val();
        var text = "Filtros configurados : ";

        coma = false;

        if (p2payment_reference_no != '' && p2payment_reference_no !== undefined) {
            text += " Tipo documento (" + reference_text + ")";
            coma = true;
        }
        if (p2payment_method != '' && p2payment_method !== undefined) {
            text += "Medio de pago (" + p2payment_method_text + ")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text += coma ? "," : "";
            text += " Sucursal (" + biller_text + ")";
            coma = true;
        }
        if (customer != '' && customer !== undefined) {
            text += coma ? "," : "";
            text += " Cliente (" + customer_text + ")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha de inicio (" + start_date_text + ")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha final (" + end_date_text + ")";
            coma = true;
        }
    }

    function notifyCredits() {
        if (validateSelectedCredits()) {
            let selectedCredits = getSelectedCredits()
            $.ajax({
                type: 'post',
                url: site.base_url+'financing/notifyCredits',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'credits': selectedCredits
                },
                dataType: 'json',
                success: function (response) {
                    message = (response.message != '') ? response.message: ``
                    swal({
                        title: `<?= lang('toast_warning_title') ?>`,
                        text: `<?= lang('Proceso de Notificación exitoso.') ?> ${message}`,
                        type: `success`,
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: `Ok`
                    }, function(result) {
                        window.location.reload()
                    })
                }
            });
        }
    }

    function reportCredits() {
        if (validateSelectedCredits()) {
            let selectedCredits = getSelectedCredits()
            $.ajax({
                type: 'post',
                url: site.base_url+'financing/reportCredits',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'credits': selectedCredits
                },
                dataType: 'json',
                success: function (response) {
                    message = (response.message != '') ? response.message: ``
                    swal({
                        title: `<?= lang('toast_warning_title') ?>`,
                        text: `<?= lang('Proceso de Reporte Exitoso.') ?> ${message}`,
                        type: `success`,
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: `Ok`
                    }, function(result) {
                        window.location.reload()
                    })
                }
            });
        }

    }

    function getSelectedCredits() {
        let selected = []

        $('.multi-select').each(function(index, check) {
            if ($(check).is(':checked')) {
                selected.push($(this).val())
            }
        })

        return selected
    }

    function validateSelectedCredits() {
        let checksSelected = 0

        $('.multi-select').each(function(index, check) {
            if ($(check).is(':checked')) {
                checksSelected++
            }
        })

        if (checksSelected == 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('No se ha seleccionado ningún Crédito para realizar el proceso.') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                return false
            })
        } else {
            return true
        }
    }

    function settlementInstallments() {
        swal({
            title: `<?= lang("toast_warning_title") ?>`,
            text: `<?= lang("Calculando Liquidación de Cuotas al día de Hoy") ?>`,
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
        })
    }
</script>