<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . " " . lang("no") . " " . $inv->id;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3, h4, p, span, table, table > tbody > tr > td, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > th {
                padding: 0px !important;
                margin: 0px !important;
            }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>

    <body>
    <div id="wrapper">
        <div id="receiptData">

            <div class="no-print">
                <?php
                if ($message) {
                    ?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=is_array($message) ? print_r($message, true) : $message;?>
                    </div>
                    <?php
                } ?>

            </div>
            <div>
                <div class="text-center">
                    <h3 style="text-transform:uppercase;"><?=$biller->company != '-' ? $biller->company : $biller->name;?></h3>
                    <?php
                    echo "NIT : ".$biller->vat_no.($biller->digito_verificacion != "" ? '-'.$biller->digito_verificacion : '');
                    ?>
                </div>
                <?php
                if ($Settings->invoice_view == 1 || $Settings->indian_gst) {
                    ?>
                    <div class="col-sm-12 text-center">
                        <h4 style="font-weight:bold;"><?=lang('sale_invoice');?> N° <?= $inv->reference_no ?></h4>
                    </div>
                    <?php
                } ?>

                <table class="table">
                    <tbody>
                        <tr>
                            <th>
                                <?= lang('grand_total') ?>
                            </th>
                            <td class="text-right">
                                <?= $this->sma->formatMoney($inv->grand_total) ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <?= lang("customer") ?>
                            </th>
                            <td class="text-right">
                                <?= ucwords(mb_strtolower($customer->company && $customer->company != '-' ? $customer->company : $customer->name)) ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <?= lang("vat_no") ?>
                            </th>
                            <td class="text-right">
                                <?= ($customer->tipo_documento == 6) ?
                                        $customer->vat_no."-".$customer->digito_verificacion." <br>"
                                    :
                                        $customer->vat_no." <br>"
                                    ;
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <?= lang("seller") ?>
                            </th>
                            <td class="text-right">
                                <?php if ($seller): ?>
                                    <?= $seller->company != '-' ? $seller->company : $seller->name; ?>
                                <?php endif ?>
                            </td>
                        </tr>
                        <tr>
                            <th>
                                <?= lang("date") ?>
                            </th>
                            <td class="text-right">
                                <?= $this->sma->hrld($inv->date); ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <?= $inv->staff_note ? '<p><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>

                <?= "<b>".lang("sales_person") . "</b>: " . $created_by->first_name." ".$created_by->last_name ?>


                    <a class="btn btn-block btn-warning no-print" href="<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>"><?= lang("back_to_pos"); ?></a>
                </div>
            </div>
        </div>
    </body>

        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#email').click(function () {
                bootbox.prompt({
                    title: "<?= lang("email_address"); ?>",
                    inputType: 'email',
                    value: "<?= $customer->email; ?>",
                    callback: function (email) {
                        if (email != null) {
                            $.ajax({
                                type: "post",
                                url: "<?= admin_url('pos/email_receipt') ?>",
                                data: {<?= $this->security->get_csrf_token_name(); ?>: "<?= $this->security->get_csrf_hash(); ?>", email: email, id: <?= $inv->id; ?>},
                                dataType: "json",
                                success: function (data) {
                                    bootbox.alert({message: data.msg, size: 'small'});
                                },
                                error: function () {
                                    bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                                    return false;
                                }
                            });
                        }
                    }
                });
                return false;
            });

        <?php if ($redirect_to_pos && $print_directly == 1) { ?>

            window.scrollTo(0,document.body.scrollHeight);
            setTimeout(function() {
                $.ajax({
                    url:"<?= admin_url('pos/get_direct_printers_jobs/'.$direct_print_job_id.'/'.$printer->google_cloud_print_id) ?>",
                }).done(function(data){
                    $('.loading_print_status').fadeOut();
                    window.scrollTo(0,document.body.scrollHeight);
                    if (data == 'DONE') {
                        $('.loading_print_status_success').fadeIn();
                    } else {
                        $('.loading_print_status_warning').fadeIn();
                    }
                });
            }, 8000);

            setTimeout(function() {
                location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
            }, 8500);
        <?php } else if ($redirect_to_pos) { ?>
            imprimir_factura_pos();
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && site.pos_settings.mobile_print_automatically_invoice == 2) {
                setTimeout(function() {
                    location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                }, (site.pos_settings.mobile_print_wait_time * 1000));
            } else {
                setTimeout(function() {
                    location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                }, 850);
            }
        <?php } else { ?>
            <?php
            if ($print_directly == 1) { ?>

                window.scrollTo(0,document.body.scrollHeight);
                setTimeout(function() {
                    $.ajax({
                        url:"<?= admin_url('pos/get_direct_printers_jobs/'.$direct_print_job_id.'/'.$printer->google_cloud_print_id) ?>",
                    }).done(function(data){
                        $('.loading_print_status').fadeOut();
                        window.scrollTo(0,document.body.scrollHeight);
                        if (data == 'DONE') {
                            $('.loading_print_status_success').fadeIn();
                        } else {
                            $('.loading_print_status_warning').fadeIn();
                        }
                    });
                }, 8000);
            <?php } else if ($this->pos_settings->remote_printing == 1 && !$print_directly) { ?>
                $(window).load(function () {
                    imprimir_factura_pos();
                    return false;
                });
                <?php
            }
            ?>
        <?php } ?>
        });
    </script>
    <script>
        //redirect_to_pos
        function imprimir_factura_pos()
        {
          var divToPrint=document.getElementById('receiptData');

          var newWin=window.open('','Print-Window');

          var header = $('head').html();

          newWin.document.open();

          newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');

          newWin.document.close();

          setTimeout(function(){newWin.close();},10);
        }
    </script>

    <?php /* include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'pos'.DIRECTORY_SEPARATOR.'remote_printing.php'; */ ?>
    <?php include 'remote_printing.php'; ?>

</body>
</html>
