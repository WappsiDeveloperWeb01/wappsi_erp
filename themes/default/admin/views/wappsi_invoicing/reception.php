<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12 ">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Recepción de archivo CSV facturación Wappsi
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <?= admin_form_open_multipart("wappsi_invoicing/reception", ['id' => 'invoicing_form']); ?>
                                            <input type="hidden" name="add" value="1">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="csv_file"><?= lang("upload_file"); ?></label>
                                                    <input type="file" data-browse-label="<?= lang('browse'); ?>" name="csv_file" class="form-control file" data-show-upload="false" data-show-preview="false" id="csv_file" required="required"/>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <button class="btn btn-primary" id="send" type="button">Enviar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script language="javascript">
    $(document).ready(function () {
        $("#invoicing_form").validate({
            ignore: []
        });
        $('[data-toggle="tooltip"]').tooltip();
    });
    $(document).on('click', '#send', function(){
        if ($('#invoicing_form').valid()) {
            $('#invoicing_form').submit();
        }
    });
</script>
