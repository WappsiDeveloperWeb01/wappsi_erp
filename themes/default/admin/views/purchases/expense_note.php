<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">

</style>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <div class="col-xs-12 no-printable text-right">
                <button onclick="window.print();"> <span class="fa fa-print"></span> <?= lang("print") ?></button>
            </div>
            <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                     alt="<?= $Settings->site_name; ?>">
            <div class="clearfix"></div>
            <div class="row padding10">
                <div class="col-xs-5">
                    <h2 class=""><?= $Settings->site_name; ?></h2>

                    <div class="clearfix"></div>
                </div>
            </div>
            <div style="clear: both;"></div>
            <p>&nbsp;</p>

            <div class="well">
                <table class="table table-borderless" style="margin-bottom:0;">
                    <tbody>
                    <tr>
                        <td><strong><?= lang("date"); ?></strong></td>
                        <td><strong class="text-right"><?php echo $this->sma->hrld($expense->date); ?></strong></td>
                    </tr>
                    <tr>
                        <td><strong><?= lang("reference"); ?></strong></td>
                        <td><strong class="text-right"><?php echo $expense->reference; ?></strong></td>
                    </tr>
                    <?php if ($category) { ?>
                    <tr>
                        <td><strong><?= lang("category"); ?></strong></td>
                        <td><strong class="text-right"><?php echo $category->name; ?></strong></td>
                    </tr>
                    <?php } ?>
                    <?php if ($biller) { ?>
                    <tr>
                        <td><strong><?= lang("biller"); ?></strong></td>
                        <td><strong class="text-right"><?php echo $biller->name; ?></strong></td>
                    </tr>
                    <?php } ?>
                    <?php if ($supplier) { ?>
                    <tr>
                        <td><strong><?= lang("supplier"); ?></strong></td>
                        <td><strong class="text-right"><?php echo $supplier->name; ?></strong></td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td><strong><?= lang("amount"); ?></strong></td>
                        <td><strong class="text-right"><?php echo $this->sma->formatMoney($expense->amount); ?></strong>
                        </td>
                    </tr>
                    <?php if (isset($cost_center) && $cost_center): ?>
                        <tr>
                            <td><strong><?= lang("cost_center"); ?></strong></td>
                            <td><strong class="text-right"><?= $cost_center->name ?> ( <?= $cost_center->code ?> )</strong>
                            </td>
                        </tr>
                    <?php endif ?>
                    <?php if ($expense->rete_fuente_total > 0): ?>
                        <tr>
                            <td><strong><?= lang("rete_fuente"); ?></strong></td>
                            <td><strong class="text-right"><?=  $this->sma->formatMoney($expense->rete_fuente_total) ?> </strong>
                            </td>
                        </tr>
                    <?php endif ?>
                    <?php if ($expense->rete_iva_total > 0): ?>
                        <tr>
                            <td><strong><?= lang("rete_iva"); ?></strong></td>
                            <td><strong class="text-right"><?=  $this->sma->formatMoney($expense->rete_iva_total) ?> </strong>
                            </td>
                        </tr>
                    <?php endif ?>
                    <?php if ($expense->rete_ica_total > 0): ?>
                        <tr>
                            <td><strong><?= lang("rete_ica"); ?></strong></td>
                            <td><strong class="text-right"><?=  $this->sma->formatMoney($expense->rete_ica_total) ?> </strong>
                            </td>
                        </tr>
                    <?php endif ?>
                    <?php if ($expense->rete_other_total > 0): ?>
                        <tr>
                            <td><strong><?= lang("rete_other"); ?></strong></td>
                            <td><strong class="text-right"><?=  $this->sma->formatMoney($expense->rete_other_total) ?> </strong>
                            </td>
                        </tr>
                    <?php endif ?>
                    <tr>
                        <td colspan="2"><?php echo $expense->note; ?></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div style="clear: both;"></div>
            <div class="row">
                <div class="col-xs-4 pull-left">
                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p>&nbsp;</p>

                    <p style="border-bottom: 1px solid #666;">&nbsp;</p>

                    <p><?= lang("stamp_sign"); ?></p>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>