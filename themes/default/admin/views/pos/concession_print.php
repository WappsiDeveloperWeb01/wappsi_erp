<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . " " . lang("no") . " " . $inv->id;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3, h4, p, span, table, table > tbody > tr > td, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > th {
                padding: 0px !important;
                margin: 0px !important;
            }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
        </style>
    </head>

    <body>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                <table class="table">
                    <thead>
                        <tr>
                            <th colspan="4"  class="text-center"><h3><?= $concession_category ?> <?= $this->sma->formatMoney($total_prc) ?></h3></th>
                        </tr>
                        <tr>
                            <th class="text-center">Cantidad</th>
                            <th class="text-center">UM</th>
                            <th class="text-center">Producto</th>
                            <th class="text-center">Valor</th>
                        </tr>
                    </thead>
                    <?php foreach ($products_categories[$categories[$curr_category]->id] as $product): ?>
                        <tr>
                            <td class="text-center"><?= $this->sma->formatQuantity($product->quantity) ?></td>
                            <td class="text-center"><?= $product->product_unit_code ?></td>
                            <td class="text-center"><?= $product->product_name ?></td>
                            <td class="text-right"><?= $this->sma->formatMoney($product->subtotal) ?></td>
                        </tr>
                    <?php endforeach ?>
                </table>
                </div>
            </div>
        </div>
    </body>

        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
        <?php if ($curr_category != NULL && $categories[$curr_category] != end($categories)): ?>
                imprimir_factura_pos();
            <?php $curr_category++; ?>
            setTimeout(function() {
                location.href = '<?= admin_url("pos/concession_print/").$sid.'/FALSE/'.$curr_category ?>';
            }, 2000);
        <?php else: ?>
            <?php if ($redirect_to_pos) { ?>
                imprimir_factura_pos();
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && site.pos_settings.mobile_print_automatically_invoice == 2) {
                    setTimeout(function() {
                        location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                    }, (site.pos_settings.mobile_print_wait_time * 1000));
                } else {
                    setTimeout(function() {
                        location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                    }, 850);
                }
            <?php } else { ?>
                $(window).load(function () {
                    imprimir_factura_pos();
                    return false;
                });
            <?php } ?>
        <?php endif ?>

            
        });
    </script>
    <script>
        //redirect_to_pos
        function imprimir_factura_pos()
        {
          var divToPrint=document.getElementById('receiptData');
          var newWin=window.open('','Print-Window');
          var header = $('head').html();
          newWin.document.open();
          newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
          newWin.document.close();
          setTimeout(function(){newWin.close();},10);
        }
    </script>

</body>
</html>
