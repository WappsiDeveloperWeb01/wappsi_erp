<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 

// if ($inv->sale_status != 'cancelled') {
//     if (
//         ($new_order_sale_status == 'enlistment' && $inv->sale_status == 'pending') ||
//         ($new_order_sale_status == 'pending' && $inv->sale_status == 'enlistment') ||
//         (
//             ($new_order_sale_status == 'sent' || $new_order_sale_status == 'delivered') &&
//             ($inv->sale_status == 'completed' || $inv->sale_status == 'sent' || $inv->sale_status == 'delivered')
//         ) ||
//         ($new_order_sale_status == 'completed' && ($inv->sale_status == 'sent' || $inv->sale_status == 'delivered'))
//     ) {
//         $this->db->update('order_sales', ['sale_status' => $new_order_sale_status, 'last_update' => date('Y-m-d H:i:s')], ['id' => $id]);
//     }
// }

$css_pending = 'disabled="true"';
$css_enlistment = 'disabled="true"';
$css_completed = 'disabled="true"';
$css_sent = 'disabled="true"';
$css_delivered = 'disabled="true"';

if ($inv->sale_status == 'pending') {
    $css_enlistment = '';
}

if ($inv->sale_status == 'enlistment') {
    $css_pending = '';
}

if ($inv->sale_status == 'completed' || $inv->sale_status == 'sent' || $inv->sale_status == 'delivered') {
    $css_sent = '';
    $css_delivered = '';
}

if ($inv->sale_status == 'sent' || $inv->sale_status == 'delivered') {
    $css_completed = '';
}


?>
<div class="modal-dialog modal-md">
    <?= admin_form_open("sales/change_order_sale_status/" . $id); ?>
    <div class="modal-content">
        <div class="modal-header">
            <h3><?= lang('change_order_status') ?></h3>
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= lang('order_status', 'order_status') ?>
                    <select name="order_status" class="form-control" id="order_status">
                        <option value=""><?= lang('select') ?></option>
                        <option value="pending" <?= $css_pending ?>><?= lang('pending') ?></option>
                        <option value="enlistment" <?= $css_enlistment ?>><?= lang('enlistment') ?></option>
                        <option value="completed" <?= $css_completed ?>><?= lang('invoiced') ?></option>
                        <option value="sent" <?= $css_sent ?>><?= lang('sent') ?></option>
                        <option value="delivered" <?= $css_delivered ?>><?= lang('delivered') ?></option>
                    </select>
                </div>
                <div class="col-lg-12">
                    <h5 style="cursor: pointer;" class="show_info"> <i class="fa fa-exclamation-circle"></i> Clic para información</h5>
                    <div class="change_os_info" style="display: none;">
                        <p>
                                1. Se puede cambiar de "Pendiente" sólo a "En alistamiento" o viceversa
                            <br>
                                2. El estado cambia automáticamente a "Parcial" cuándo se facture uno o unos productos de la orden, o a "Facturado" si se facturó la orden completamente
                            <br>
                                3. si la orden de pedido está en estado "parcial", no deja cambiar estado
                            <br>
                                4. si la orden está en estado "Facturado", se puede cambiar a "Enviado" o "Entregado"
                            <br>
                                5. se puede cambiar de "Enviado" o de "Entregado" sólo a "Facturado"
                            <br>
                                6. Si la orden ya está anulada, no se puede cambiar el estado
                            <br>
                            <br>
                            <b>Importante </b>: Se ignorarán las ordenes que no cumplan con estas condiciones según el estado escogido a cambiar; Por lo tanto, sólo se actualizarán las que sí cumplen las condiciones.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-sm-12">
            <?= form_submit('submit', lang('send'), 'class="btn btn-primary"'); ?>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
