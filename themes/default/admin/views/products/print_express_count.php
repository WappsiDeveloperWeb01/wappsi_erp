<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . " " . lang("no") . " " . $stock_count->reference_no;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            table { width: 100% ; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
        </style>
    </head>
    <body>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                <div style="text-align:center;">
                    <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    <h3><?= lang('express_count_result') ?></h3>
                    <h4 style="text-transform:uppercase;"><?=$biller->name;?></h4>
                </div>
                <?php
                echo "<p>" .lang("date") . ": " . $this->sma->hrld($stock_count->date) . "<br>";
                echo lang("reference_no") . ": " . $stock_count->reference_no . "<br>";
                echo "<b>".lang("warehouse") . ": </b>" . $warehouse->name . "<br>";
                // echo lang("sales_person") . ": " . $created_by->first_name." ".$created_by->last_name . "</p>";
                echo "<p>";
                ?>

                <div style="clear:both;"></div>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align: center;"><?= lang('product') ?></th>
                            <th style="text-align: center;"><?= lang('counted') ?></th>
                            <th style="text-align: center;"><?= lang('expected') ?></th>
                            <th style="text-align: center;"><?= lang('adjusted') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($stock_count_items as $row): ?>
                            <tr>
                                <td><?= $row->product_code." - ".$row->product_name ?></td>
                                <td class="text-right"><?= $this->sma->formatQuantity($row->counted) ?></td>
                                <td class="text-right"><?= $this->sma->formatQuantity($row->expected) ?></td>
                                <td class="text-right"><?= $this->sma->formatQuantity($row->counted  - $row->expected ) ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
 

                <p class="text-center"><?= lang('printing_software_wappsi_info') ?></p>
                <p class="text-center"><?= lang('created_by')." : ".ucwords(mb_strtolower($created_by->first_name." ".$created_by->last_name)) ?></p>
            </div>

        </div>

        <div id="buttons" style="padding-top:10px;" class="no-print">

            <div class="alert alert-info loading_print_status" <?= $this->pos_settings->remote_printing != 4 ? 'style="display:none;"' : '' ?> >
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status') ?>
            </div>

            <div class="alert alert-success loading_print_status_success" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_success') ?>
            </div>

            <div class="alert alert-warning loading_print_status_warning" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_warning') ?>
            </div>

            <hr>
                <span class="pull-right col-xs-12">
                    <button onclick="return imprimir_factura_pos()" class="btn btn-block btn-primary"><?= lang("print") ?></button>
                    <button onclick="window.close();" class="btn btn-block btn-default"><?= lang("close") ?></button>
                </span>
        </div>
    </div>



        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
    <script type="text/javascript">
        
        function imprimir_factura_pos()
        {
            if ("<?= $this->session->userdata('print_pos_finalized_invoice') ?>" == 0 || ("<?= isset($submit_target) && $submit_target == 2 ?>")) {
                return false;
            }
            if ("<?= $this->pos_settings->mobile_print_automatically_invoice ?>" == 0) {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    $('.btn_print_html').fadeOut();
                    return false;
                }
            }
            setTimeout(function() {
                window.print();
            }, 350);
        }

        $(document).ready(function(){
            imprimir_factura_pos();
        });
    </script>
</body>
</html>
