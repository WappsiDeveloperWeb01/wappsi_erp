<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog  modal-lg">
    <div class="modal-content">
        <?= admin_form_open_multipart("system_settings/change_logo", ['data-toggle' => 'validator', 'role' => 'form']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabel"><?php echo lang('change_logo'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-4">
                        <?= lang("site_logo", "site_logo") ?>
                        <input id="site_logo" type="file" data-browse-label="<?= lang('browse'); ?>" name="site_logo" data-show-upload="false" data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('site_logo_tip'); ?></small>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang("biller_logo_rectangular", "biller_logo_rectangular") ?>
                        <input id="biller_logo_rectangular" type="file" data-browse-label="<?= lang('browse'); ?>" name="biller_logo_rectangular" data-show-upload="false"
                            data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('biller_logo_tip_rectangular'); ?></small>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang("biller_logo_square", "biller_logo_square") ?>
                        <input id="biller_logo_square" type="file" data-browse-label="<?= lang('browse'); ?>" name="biller_logo_square" data-show-upload="false"
                            data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('biller_logo_tip_square'); ?></small>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <?= lang("logo_app", "logo_app") ?>
                        <input id="logo_app" type="file" data-browse-label="<?= lang('browse'); ?>" name="logo_app" data-show-upload="false" data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('biller_logo_app'); ?></small>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang("logo_symbol", "logo_symbol") ?>
                        <input id="logo_symbol" type="file" data-browse-label="<?= lang('browse'); ?>" name="logo_symbol" data-show-upload="false" data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('biller_logo_symbol'); ?></small>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang("logo_zebra_32_25_variants", "logo_zebra_32_25_variants") ?>
                        <input id="logo_zebra_32_25_variants" type="file" data-browse-label="<?= lang('browse'); ?>" name="logo_zebra_32_25_variants" data-show-upload="false" data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('logo_zebra_32_25_variants_symbol'); ?></small>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-4">
                        <?= lang("entry_image_1", "entry_image_1") ?>
                        <input id="entry_image_1" type="file" data-browse-label="<?= lang('browse'); ?>" name="entry_image_1" data-show-upload="false" data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('entry_image_symbol'); ?></small>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang("entry_image_2", "entry_image_2") ?>
                        <input id="entry_image_2" type="file" data-browse-label="<?= lang('browse'); ?>" name="entry_image_2" data-show-upload="false" data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('entry_image_symbol'); ?></small>
                    </div>
                    <div class="form-group col-md-4">
                        <?= lang("entry_image_3", "entry_image_3") ?>
                        <input id="entry_image_3" type="file" data-browse-label="<?= lang('browse'); ?>" name="entry_image_3" data-show-upload="false" data-show-preview="false" class="form-control file">
                        <small class="help-block"><?= lang('entry_image_symbol'); ?></small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_submit('upload_logo', lang('upload_logo'), 'class="btn btn-primary"'); ?>
            </div>
        <?= form_close(); ?>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>