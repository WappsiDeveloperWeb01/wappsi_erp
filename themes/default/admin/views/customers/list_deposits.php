<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<script type="text/javascript">


    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
    var cdeppayment_reference_no;
    var cdeppayment_method;
    var biller;
    var filter_user;
    var customer;
    var filtered = 0;
    var filtered_ini_date;


    if (localStorage.getItem('cdep_filter_filtered_ini_date')) {
        filtered_ini_date = localStorage.getItem('cdep_filter_filtered_ini_date');
    }
    <?php if (isset($_POST['start_date'])): ?>
        localStorage.setItem('cdep_filter_start_date', '<?= $_POST['start_date'] ?>');
        start_date = '<?= $_POST['start_date'] ?>';
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_start_date')) {
            start_date = localStorage.getItem('cdep_filter_start_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['end_date'])): ?>
        localStorage.setItem('cdep_filter_end_date', '<?= $_POST['end_date'] ?>');
        end_date = '<?= $_POST['end_date'] ?>';
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_end_date')) {
            end_date = localStorage.getItem('cdep_filter_end_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['cdeppayment_reference_no'])): ?>
        localStorage.setItem('cdep_filter_cdeppayment_reference_no', '<?= $_POST['cdeppayment_reference_no'] ?>');
        cdeppayment_reference_no = '<?= $_POST['cdeppayment_reference_no'] ?>';
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_cdeppayment_reference_no')) {
            ppayment_reference_no = localStorage.getItem('cdep_filter_ppayment_reference_no');
        }
    <?php endif ?>

    <?php if (isset($_POST['cdeppayment_method'])): ?>
        localStorage.setItem('cdep_filter_cdeppayment_method', '<?= $_POST['cdeppayment_method'] ?>');
        cdeppayment_method = '<?= $_POST['cdeppayment_method'] ?>';
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_cdeppayment_method')) {
            cdeppayment_method = localStorage.getItem('cdep_filter_cdeppayment_method');
        }
    <?php endif ?>

    <?php if (isset($_POST['biller'])): ?>
        localStorage.setItem('cdep_filter_biller', '<?= $_POST['biller'] ?>');
        biller = '<?= $_POST['biller'] ?>';
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_biller')) {
            biller = localStorage.getItem('cdep_filter_biller');
        }
    <?php endif ?>

    <?php if (isset($_POST['customer'])): ?>
        localStorage.setItem('cdep_filter_customer', '<?= $_POST['customer'] ?>');
        customer = '<?= $_POST['customer'] ?>';
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_customer')) {
            customer = localStorage.getItem('cdep_filter_customer');
        }
    <?php endif ?>

    <?php if (isset($_POST['filtered'])): ?>
        localStorage.setItem('cdep_filter_filtered', '<?= $_POST['filtered'] ?>');
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            localStorage.setItem('cdep_filter_filtered_ini_date', '<?= date("Y-m-d H:i:s") ?>');
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_filtered')) {
            filtered = localStorage.getItem('cdep_filter_filtered');
        }
    <?php endif ?>

    <?php if (isset($_POST['filter_user'])): ?>
        localStorage.setItem('cdep_filter_filter_user', '<?= $_POST['filter_user'] ?>');
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php else: ?>
        if (localStorage.getItem('cdep_filter_filter_user')) {
            filter_user = localStorage.getItem('cdep_filter_filter_user');
        }
    <?php endif ?>

    <?php if (isset($_POST['date_records_filter'])): ?>
        localStorage.setItem('cdep_filter_date_records_filter', '<?= $_POST['date_records_filter'] ?>');
    <?php endif ?>
</script>


<div class="wrapper wrapper-content  animated fadeInRight no-print">


<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins border-bottom">
            <div class="ibox-title">
                <h5><?= lang('filter') ?></h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-down"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content" style="display: none;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <?= admin_form_open('customers/list_deposits', ['id'=>'ppayments_filter']) ?>
                            <div class="col-sm-4">
                                <?= lang('reference_no', 'cdeppayment_reference_no') ?>
                                <select name="cdeppayment_reference_no" id="cdeppayment_reference_no" class="form-control">
                                    <option value=""><?= lang('select') ?></option>
                                    <?php if ($documents_types): ?>
                                        <?php foreach ($documents_types as $dt): ?>
                                            <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['cdeppayment_reference_no']) && $_POST['cdeppayment_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre." (".$dt->sales_prefix.")" ?></option>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </select>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                <?php
                                $biller_selected = '';
                                $biller_readonly = false;
                                if ($this->session->userdata('biller_id')) {
                                    $biller_selected = $this->session->userdata('biller_id');
                                    $biller_readonly = true;
                                }

                                $bl[""] = lang('select');
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                }
                                echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                ?>
                            </div>
                            <div class="col-sm-4">
                                <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                <?php
                                echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="filter_customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                                ?>
                            </div>
                            <?php if ($this->Owner || $this->Admin): ?>
                                <div class="col-sm-4">
                                    <label><?= lang('user') ?></label>
                                    <select name="filter_user" id="filter_user" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($users): ?>
                                            <?php foreach ($users as $user): ?>
                                                <option value="<?= $user->id ?>"><?= $user->first_name." ".$user->last_name ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            <?php endif ?>


                            <div class="col-sm-4 form-group">
                                <?= lang("paying_by", "cdeppayment_method"); ?>
                                <select name="cdeppayment_method" id="cdeppayment_method" class="form-control">
                                    <?= $this->sma->paid_opts(null, false, true, true, true); ?>
                                </select>
                            </div>
                            <hr class="col-sm-11">

                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                    <?= $this->sma->get_filter_options(); ?>
                                </select>
                            </div>


                            <div class="date_controls_dh">
                                <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                    <div class="col-sm-4 form-group">
                                        <?= lang('filter_year', 'filter_year_dh') ?>
                                        <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                            <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                <option value="<?= $key ?>"><?= $key ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                <?php endif ?>
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('start_date', 'start_date') ?>
                                    <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                </div>
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('end_date', 'end_date') ?>
                                    <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control datetime">
                                </div>
                            </div>

                            <div class="col-sm-12" style="margin-top: 2%;">
                                <input type="hidden" name="filtered" value="1">
                                <button type="submit" id="submit-purchases-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                <button type="button" id="submit-purchases-filter-clean" class="btn btn-danger"><span class="fa fa-times"></span> <?= lang('reset') ?></button>
                            </div>
                            <?= form_close() ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php if ($Owner || $Admin || $GP['bulk_actions']) {
    echo admin_form_open('customers/deposit_actions', 'id="action-form"');
} ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"><?= lang("actions") ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('customers/add_deposit') ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i> <?= lang("add_deposit"); ?>
                                        </a>
                                    </li>
                                    <?php if ($Owner || $Admin) { ?>
                                    <li>
                                        <a href="#" id="excel" data-action="reaccount">
                                            <i class="fa fa-file-pdf-o"></i> <?= lang('post_deposit') ?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <h4 class="text_filter"></h4>
                                <table id="DepData" class="table table-hover">
                                    <thead>
                                    <tr class="primary">
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkth" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("reference"); ?></th>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("value"); ?></th>
                                        <th><?= lang("applied_valued"); ?></th>
                                        <th><?= lang("balance"); ?></th>
                                        <th><?= lang("paid_by"); ?></th>
                                        <th><?= lang("origin"); ?></th>
                                        <th><?= lang("origin_reference_no"); ?></th>
                                        <th><?= lang("created_by"); ?></th>
                                        <th><?= lang("customers"); ?></th>
                                        <th><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php if ($Owner || $Admin  || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.tip').tooltip();
            oTable = $('#DepData').dataTable({
                "aaSorting": [[1, "asc"]],
                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true, 'bServerSide': true,
                'sAjaxSource': '<?= admin_url('customers/get_deposits/') ?>',
                'fnServerData': function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    }, {
                        "name": "start_date",
                        "value": start_date
                    }, {
                        "name": "end_date",
                        "value": end_date
                    }, {
                        "name": "cdeppayment_reference_no",
                        "value": cdeppayment_reference_no
                    }, {
                        "name": "cdeppayment_method",
                        "value": cdeppayment_method
                    }, {
                        "name": "biller",
                        "value": biller
                    }, {
                        "name": "customer",
                        "value": customer
                    }, {
                        "name": "user",
                        "value": filter_user
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                    var oSettings = oTable.fnSettings();
                    nRow.id = aData[0];
                    nRow.className = "deposit_link";
                    return nRow;
                },
                "aoColumns": [
                                {
                                    "bSortable": false,
                                    "mRender": checkbox
                                },
                                null,
                                {"mRender": fld},
                                {"mRender": currencyFormat},
                                {"mRender": currencyFormat},
                                {"mRender": currencyFormat},
                                {"mRender": paid_by},
                                null,
                                null,
                                null,
                                null,
                                {"bSortable": false}
                            ]
            });
            $('div.dataTables_length select').addClass('form-control');
            $('div.dataTables_length select').addClass('select2');
            $('div.dataTables_filter input').attr('placeholder', 'Buscar...');
            $('select.select2').select2({minimumResultsForSearch: 7});
        });
    </script>


<script type="text/javascript">

    $(document).ready(function () {
        if (customer != '') {
                $('#filter_customer').val(customer).select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site.base_url+"customers/getCustomer/" + $(element).val(),
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site.base_url + "customers/suggestions",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: lang.no_match_found}]};
                            }
                        }
                    }
                });

            $('#filter_customer').trigger('change');
        } else {
            $('#filter_customer').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        }
        if (filtered !== undefined) {
            setTimeout(function() {
                $('#start_date_dh').val(start_date);
                $('#end_date_dh').val(end_date);
                $('#cdeppayment_reference_no').select2('val', cdeppayment_reference_no);
                $('#cdeppayment_method').select2('val', cdeppayment_method);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                // $('.collapse-link').click();
            }, 900);
        }
        if (filtered_ini_date !== undefined) {
            minutos = calcularMinutos(filtered_ini_date, '<?= date("Y-m-d H:i:s") ?>');
            if (minutos >= 10) {
                localStorage.removeItem('cdep_filter_start_date');
                localStorage.removeItem('cdep_filter_end_date');
                localStorage.removeItem('cdep_filter_biller');
                localStorage.removeItem('cdep_filter_filter_user');
                localStorage.removeItem('cdep_filter_customer');
                localStorage.removeItem('cdep_filter_cdeppayment_reference_no');
                localStorage.removeItem('cdep_filter_cdeppayment_method');
                localStorage.removeItem('cdep_filter_filtered');
                localStorage.removeItem('cdep_filter_filtered_ini_date');
                localStorage.removeItem('cdep_filter_date_records_filter');
                location.href = '<?= admin_url("customers/list_deposits") ?>';
            }
        }
        setTimeout(function() {
            setFilterText();
        }, 1500);
        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>
    });

    $(document).on('click', '#submit-purchases-filter-clean', function(){
        localStorage.removeItem('cdep_filter_start_date');
        localStorage.removeItem('cdep_filter_end_date');
        localStorage.removeItem('cdep_filter_biller');
        localStorage.removeItem('cdep_filter_filter_user');
        localStorage.removeItem('cdep_filter_customer');
        localStorage.removeItem('cdep_filter_cdeppayment_reference_no');
        localStorage.removeItem('cdep_filter_cdeppayment_method');
        localStorage.removeItem('cdep_filter_filtered');
        localStorage.removeItem('cdep_filter_filtered_ini_date');
        localStorage.removeItem('cdep_filter_date_records_filter');
        location.href = '<?= admin_url("customers/list_deposits") ?>';
    });

function calcularMinutos(start_date, end_date)
{
    var fecha1 = new Date(start_date);
    var fecha2 = new Date(end_date);
    var diff = fecha2.getTime() - fecha1.getTime();
    var minutos = diff/(1000 * 60);
    return minutos;
}

function setFilterText(){

    var reference_text = $('#cdeppayment_reference_no option:selected').data('dtprefix');
    var cdeppayment_method_text = $('#cdeppayment_method option:selected').text();
    var biller_text = $('#biller option:selected').text();
    var customer_text = $('#filter_customer').select2('data') !== null ? $('#filter_customer').select2('data').text : '';
    var start_date_text = $('#start_date_dh').val();
    var end_date_text = $('#end_date_dh').val();
    var text = "Filtros configurados : ";

    coma = false;

    if (cdeppayment_reference_no != '' && cdeppayment_reference_no !== undefined) {
        text+=" Tipo documento ("+reference_text+")";
        coma = true;
    }
    if (cdeppayment_method != '' && cdeppayment_method !== undefined) {
        text+="Medio de pago ("+cdeppayment_method_text+")";
        coma = true;
    }
    if (biller != '' && biller !== undefined) {
        text+= coma ? "," : "";
        text+=" Sucursal ("+biller_text+")";
        coma = true;
    }
    if (customer != '' && customer !== undefined) {
        text+= coma ? "," : "";
        text+=" Cliente ("+customer_text+")";
        coma = true;
    }
    if (start_date != '' && start_date !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha de inicio ("+start_date_text+")";
        coma = true;
    }
    if (end_date != '' && end_date !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha final ("+end_date_text+")";
        coma = true;
    }

    $('.text_filter').html(text);

}


$(document).ready(function(){
    setTimeout(function() {
        <?php if (!isset($_POST['date_records_filter'])): ?>
            $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            $('#ppayments_filter').submit();
        <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
            $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php endif ?>
    }, 150);
});

</script>
