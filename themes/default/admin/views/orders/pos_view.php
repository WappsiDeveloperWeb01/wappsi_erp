<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
    p {
        margin: 0 0 3px !important;
    }

    .table {
        margin-bottom: 0px !important;
    }
</style>
<!doctype html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><?=lang('order_sale') . " " . lang("no") . " " . $inv->id;?></title>
        <base href="<?=base_url()?>"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
        </style>
    </head>

    <body>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                <div class="text-center">
                    <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    <h4 style="text-transform:uppercase;"><?=$biller->company;?></h4>
                    <?php
                    echo "<p>" . $biller->address . " " . $biller->city . " " . $biller->postal_code . " " . $biller->state . " " . $biller->country .
                    "<br>" . lang("tel") . ": " . $biller->phone;
                    echo  "<br>" . lang("email") . ": " . $biller->email;
                    echo "<br>";
                    echo "NIT : ".$biller->vat_no.($biller->digito_verificacion > 0 ? '-'.$biller->digito_verificacion : '');
                    if ($ciiu_code) {
                        echo "</br> Código CIIU : ".$ciiu_code."</br>";
                    }
                    echo "<br>";
                    if ($this->pos_settings->cf_title1 != "" && $this->pos_settings->cf_value1 != "") {
                        echo $this->pos_settings->cf_title1 . ": " . $this->pos_settings->cf_value1 . "<br>";
                    }
                    if ($this->pos_settings->cf_title2 != "" && $this->pos_settings->cf_value2 != "") {
                        echo $this->pos_settings->cf_title2 . ": " . $this->pos_settings->cf_value2 . "<br>";
                    }
                    echo '</p>';
                    ?>
                    <p class="text-center"><b><?= $tipo_regimen ?></b></p>
                    <?php 
                    if ($this->Settings->url_web != "") {
                        echo $this->Settings->url_web."<br>";
                    } ?>
                </div>
                <?php
                if ($Settings->invoice_view == 1 || $Settings->indian_gst) {
                    ?>
                    <div class="col-sm-12 text-center">
                        <?php if ($inv->sale_status == 'returned'): ?>
                            <h4 style="font-weight:bold;">Devolución de venta</h4>
                        <?php endif ?>
                    </div>
                    <?php
                }
                echo "<p>" .lang("date") . ": " . $this->sma->hrld($inv->date) . "<br>";
                if ($inv->payment_term > 0) {
                    $fecha_exp = strtotime("+".$inv->payment_term." day", strtotime($inv->date));
                    $fecha_exp = date('d/m/Y', $fecha_exp);
                    echo lang("expiration_date")." : ".$fecha_exp." <br>";
                }
                echo "<b style='font-size:115%;'>".($document_type ? $document_type->nombre : lang('sale_invoice')) . " N° " . $inv->reference_no . "</b><br>";
                // echo lang("sales_person") . ": " . $created_by->first_name." ".$created_by->last_name . "</p>";
                echo "<p>";
                echo lang("customer") . ": " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name) . "<br>";
                if ($this->pos_settings->customer_details && $customer->vat_no != "222222222222") {
                    if ($customer->vat_no != "-") {

                        if ($customer->tipo_documento == 6) {
                            echo  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion." <br>";
                        } else {
                            echo  lang("vat_no") . ": " . $customer->vat_no." <br>";
                        }

                    }
                    if ($address) {
                        echo lang("tel") . ": " . $address->phone . "<br>";
                        echo lang("address") . ": " . $address->direccion . "<br>";
                        echo $address->city ." ".$address->state." ".$address->country ."<br>";
                    } else {
                        echo lang("tel") . ": " . $customer->phone . "<br>";
                        echo lang("address") . ": " . $customer->address . "<br>";
                        echo $customer->city ." ".$customer->state." ".$customer->country ."<br>";
                    }
                        echo $customer->email ."<br>";
                }
                echo "</p>";
                ?>
                <?php if ($seller): ?>
                    <p><b><?= lang('seller') ?> :</b> <?= $seller->company != '-' ? $seller->company : $seller->name; ?></p>
                <?php endif ?>
                <?php if (!empty($inv->sale_origin_reference_no)): ?>
                    <p><b><?= lang('reference_origin') ?> :</b> <?= $inv->sale_origin_reference_no?></p>
                <?php endif ?>

                <?php if (isset($cost_center) && $cost_center): ?>
                    <div>
                        <label><?= lang('cost_center') ?></label>
                        <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                    </div>
                <?php endif ?>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align: center;">Cant</th>
                            <th style="text-align: center;">U.M.</th>
                            <th style="text-align: center;">Producto</th>
                            <th style="text-align: center;">Valor</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $r = 1; $category = 0;
                        // Variable para identificar las clasificación de marcas
                        $brand = 0;
                        $tax_summary = array();
                        foreach ($rows as $row) {
                            $qty = $row->quantity;
                            if (isset($row->operator)) {
                                if ($row->operator == "*") {
                                    $qty = $row->quantity / $row->operation_value;
                                } else if ($row->operator == "/") {
                                    $qty = $row->quantity * $row->operation_value;
                                } else if ($row->operator == "+") {
                                    $qty = $row->quantity - $row->operation_value;
                                } else if ($row->operator == "-") {
                                    $qty = $row->quantity + $row->operation_value;
                                }
                            }
                            echo "<tr>
                                    <td>".$this->sma->formatQuantity($qty, $qty_decimals)."</td>
                                    <td>".$row->product_unit_code."</td>
                                    <td>".$row->product_name." ".
                                        ($row->variant && ($invoice_format && $invoice_format->show_product_variants == 1) ? ' (' . $row->variant . ')' : '') .
                                        ($tax_indicator ? '('.$row->tax_indicator.')' : '').
                                        ($tax_indicator && $row->tax_2 > 0 ? ' (LC)' : '').
                                        ($show_product_preferences == 1 && $row->preferences ? ' (' . $this->sma->print_preference_selection($row->preferences) . ')' : '') .
                                        ($invoice_format && ($invoice_format->product_detail_discount) && $row->item_discount > 0 ? ", ".lang('product_discount_included')." (-".$row->discount.")" : "").
                                        ($this->Settings->show_brand_in_product_search ? " - ".$row->brand_name : '').
                                    "</td>
                                    <td>".$this->sma->formatValue($value_decimals, $row->subtotal)."</td>
                                </tr>";
                            $r++;
                        }

                        ?>
                    </tbody>
                    <tfoot>
                        <?php
                            $rete_total = 0;
                        ?>
                        <?php if ($rete_total != 0 || $inv->order_discount != 0): ?>
                            <tr>
                                <th colspan="2"><?=lang("total_before_discount");?></th>
                                <th colspan="2" class="text-right"><?=$this->sma->formatValue($value_decimals, ($inv->total + $inv->product_tax));?></th>
                            </tr>
                        <?php endif ?>
                        <?php
                        if ($inv->order_tax != 0) {
                            echo '<tr><th colspan="2">' . lang("tax") . '</th><th colspan="2" class="text-right">' . $this->sma->formatValue($value_decimals, $inv->order_tax) . '</th></tr>';
                        }

                        if ($rete_total != 0) {
                            echo '<tr>
                                    <th colspan="2">'.lang('retention').'</th>
                                    <th colspan="2" class="text-right"> -' . $this->sma->formatValue($value_decimals, $rete_total) . '</th>
                                  </tr>';
                        }

                        if ($inv->order_discount != 0) {
                            echo '<tr><th colspan="2">' . lang("order_discount") . '</th><th  colspan="2" class="text-right"> -' . $this->sma->formatValue($value_decimals, $inv->order_discount) . '</th></tr>';
                        }

                        if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 1) {
                            echo '<tr><th colspan="2">' . lang("shipping") . '</th><th colspan="2" class="text-right">' . $this->sma->formatValue($value_decimals, $inv->shipping) . '</th></tr>';
                        }

                        if ($Settings->indian_gst) {
                            if ($inv->cgst > 0) {
                                $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                                echo '<tr><td>' . lang('cgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $cgst) : $cgst) . '</td></tr>';
                            }
                            if ($inv->sgst > 0) {
                                $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                                echo '<tr><td>' . lang('sgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $sgst) : $sgst) . '</td></tr>';
                            }
                            if ($inv->igst > 0) {
                                $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                                echo '<tr><td>' . lang('igst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $igst) : $igst) . '</td></tr>';
                            }
                        }

                        if ($this->pos_settings->rounding || $inv->rounding != 0) {
                            ?>
                            <tr>
                                <th colspan="2"><?=lang("rounding");?></th>
                                <th colspan="2" class="text-right"><?= $this->sma->formatValue($value_decimals, $inv->rounding);?></th>
                            </tr>
                            <tr>
                                <th colspan="2"><?=lang("total_to_pay");?></th>
                                <th colspan="2" class="text-right"><?=$this->sma->formatValue($value_decimals, ($inv->grand_total + $inv->rounding)  - $rete_total );?></th>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <th colspan="2"><?=lang("total_to_pay");?></th>
                                <th colspan="2" class="text-right"><?=$this->sma->formatValue($value_decimals, $inv->grand_total  - $rete_total );?></th>
                            </tr>
                            <?php
                        } ?>
                    </tfoot>
                </table>

                <?= $Settings->invoice_view > 0 && (!isset($invoice_format->view_item_tax) || (isset($invoice_format->view_item_tax) && $invoice_format->view_item_tax)) ? $this->gst->summary($rows, $return_rows, ($inv->product_tax), FALSE, FALSE, $tax_indicator, false, $value_decimals, $qty_decimals) : ''; ?>

                <?php
                    if ($biller_data->product_order == "1" || $biller_data->product_order == "2")
                    {
                        echo $this->gscat->summary($rows, $return_rows, ($inv->product_tax), FALSE, $biller_data->product_order, $tax_indicator);
                    }
                ?>
                <?php if ($show_award_points == 1): ?>
                    <?= $customer->award_points != 0 && $Settings->each_spent > 0 ? '<p style="text-align:center;">'.lang('this_sale_points').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                    .'<br>'.
                    lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>' : ''; ?>
                <?php endif ?>
                <?= $inv->note ? '<p><strong>' . lang('sale_note') . ':</strong>  ' . $this->sma->decode_html($inv->note) . '</p>' : ''; ?>
                <?= $inv->staff_note ? '<p><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>

                <?php if ($inv->resolucion): ?>
                    <p class="text-center"><?= $inv->resolucion ?></p>
                <?php endif ?>

                <p class="text-center"><?= $this->sma->decode_html($invoice_footer ? $invoice_footer : '') ?></p>

                <?php if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 0): ?>
                    <hr>
                    <h4><?= sprintf(lang('shipping_not_include_in_grand_total'), $this->sma->formatValue($value_decimals, $inv->shipping)) ?></h4>
                <?php endif ?>

                <?php if ($biller_data->concession_status == 1): ?>
                    <?php 
                        $cc_data = [];
                        foreach ($biller_categories_concession as $bcc) {
                            $cc_data[$bcc->category_id] = $bcc;
                        }
                     ?>
                    <?php foreach ($ccategories as $category_id => $category_total): ?>
                        <?php if (isset($cc_data[$category_id]) && $cc_data[$category_id]->concession_code): ?>
                            <?php 
                            $total_factura_barcode = $this->sma->zero_left_consecutive(ceil($category_total), '000000000');
                            $concession_name = $cc_data[$category_id]->concession_name;
                            $concession_code = $cc_data[$category_id]->concession_code;
                             ?>
                            <div class="col-xs-12 text-center" style="padding-top: 10%">
                                <span><?= lang('concession_name') ?> : <?= $concession_name ?></span><br>
                                <span><?= lang('total') ?> : <?= $this->sma->formatMoney(ceil($category_total)) ?></span><br>
                                <img src="<?= admin_url('misc/barcode/'.($concession_code.$total_factura_barcode).'/code128/40/0/0'); ?>" alt="<?= ($concession_code.$total_factura_barcode); ?>" class="bcimg" style="width: 70%;" />
                                <br>
                                <span><?= $concession_code.$total_factura_barcode ?></span>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php endif ?>
                <p class="text-center">Software Wappsi POS desarrollado por Web Apps Innovation SAS  www.wappsi.com</p>
            </div>

            <button onclick="window.print();" class="btn btn-block btn-primary btn_print_html no-print"><?= lang("print") ?></button>
            <button style="with:100%" type="button" class="btn btn-block btn btn-warning btn_print_html no-print" onclick="window.location.href = '<?= admin_url().'sales/orders' ?>';">
                <i class="fa fa-arrow-circle-left"></i> <?= lang('go_back'); ?>
            </button>               
        </div>
    </div>

        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

        <script type="text/javascript">
            $(window).ready(function(){
                setTimeout(function() {
                    window.print();
                }, 350);
            });
        </script>

</body>
</html>