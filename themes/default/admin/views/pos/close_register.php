<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
   $definir_color = function($amount){
        if ($amount > 0) {
            $color = "color:green;";
        } else if ($amount < 0) {
            $color = "color:#ef5b50;";
        } else {
            $color = "";
        }
        return $color;
    }
?>
<style type="text/css">
    .amount_cash {
        font-weight: 700;
    }
</style>
<script>
    $(document).ready(function(){
        <?php if (!$this->Admin && !$this->Owner): ?>
            if ((site.settings.pos_register_hide_closing_detail == 0 || site.settings.pos_register_hide_closing_detail == 1)) {
                // $('.tr_payment_method').fadeOut();
                $('.tr_payment_method_show').css('display', '');
                $('.tr_payment_method').css('display', 'none');
            }
            if ((site.settings.pos_register_hide_closing_detail == 1 || site.settings.pos_register_hide_closing_detail == 3)) {
                // $('.tr_payment_method_cash').fadeOut();
                $('.tr_payment_method_cash').css('display', 'none');
            }
        <?php endif ?>
            
    });
</script>
<div class="modal-dialog modal-xs" <?= isset($cron_job) ? "style='border:1px solid #DDD; padding:10px; margin:10px 0;'" : "" ?>>
    <div class="modal-content"  id="closeRegisterPrint">
        <div class="modal-header">
            <?php if (!isset($cron_job)): ?>
                <button type="button" class="close close_close_register" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
            <?php endif ?>

            <h4 class="modal-title" id="myModalLabel"><?= lang('close_register') . ' (' . $this->sma->hrld($register_open_time ? $register_open_time : $this->session->userdata('register_open_time')) . ' - ' . $this->sma->hrld(date('Y-m-d H:i:s')) . ')'; ?></h4>
            <h4><?= lang('company') ?> : <?= $this->Settings->nombre_comercial ?></h4>
            <h4><?= lang('vat_no') ?> : <?= $this->Settings->numero_documento ?></h4>
            <h4><?= lang('close_register_user') ?> : <?= $this->session->userdata('username'); ?></h4>
            <?php if ($this->session->userdata('user_pc_serial')): ?>
                <h4><?= lang('user_pc_serial') ?> : <?= $this->session->userdata('user_pc_serial'); ?></h4>
            <?php endif ?>
            <?php if (isset($biller_name)): ?>
                <h4><?= lang('biller') ?> : <?= $biller_name; ?></h4>
            <?php endif ?>
            <h4><?= lang('cash_in_hand') ?> : <span class="amount_cash" style="<?= $definir_color($cash_in_hand) ?>"><?= $this->sma->formatMoney($cash_in_hand); ?></span></h4>
            <h4 class="without_close" style="display: none;">Impresión estado de caja sin cierre definitivo.</h4>
            <?php if (!isset($cron_job)): ?>
                <label class="no-print tr_payment_method">
                    <input type="checkbox" name="tirilla" class="tirilla skip"> <?= lang('pos_ticket') ?>
                </label>
            <?php endif ?>
        </div>
        <?php $attrib = array('class' => 'close_register_form');
        echo admin_form_open_multipart("pos/close_register" , $attrib);
        ?>
        <input type="hidden" name="sync" value="<?= isset($sync) ? true : false ?>">
        <input type="hidden" name="rid" value="<?= isset($rid) ? $rid : null ?>">
        <div class="modal-body">
            <div id="alerts"></div>
            <table width="100%" class="stable tableView">
                <tr>
                    <th style="width: 40%;"><?= lang('close_register_concept') ?></th>
                    <th style="text-align: center; display: none;" class="tr_payment_method_show"><?= lang('transactions_number') ?></th>
                    <th style="text-align: center;" class="tr_payment_method"><?= lang('payments') ?></th>
                    <th style="text-align: center;" class="tr_hide_tirilla tr_payment_method"><?= lang('sale') ?></th>
                    <th style="text-align: center; display: none;" class="tr_show_tirilla tr_payment_method"><?= lang('sale')." + ".lang('return') ?></th>
                    <th style="text-align: center;" class="tr_hide_tirilla tr_payment_method"><?= lang('return') ?></th>
                    <th style="text-align: center;" class="tr_payment_method"><?= lang('total') ?></th>
                </tr>
                <?php $total_cash = 0;
                      $total_sales = 0;
                      $total_total = 0;
                      $total_returns = 0;
                      $total_rete = 0;
                      $total_return_rete = 0;
                      $total_rc_rete = 0;
                      $total_payments_collections = 0;
                      $total_sold_gift_cards = 0;
                      $total_paid_installments = 0;
                    if (isset($popts) && $popts): ?>
                    <?php foreach ($popts as $popt): ?>
                        <?php
                            $RC_paid = $popts_data[$popt->code]['RC'] ? $popts_data[$popt->code]['RC']->paid : 0;
                            $total_payments_collections += $popts_data[$popt->code]['payments_collections'] ? $popts_data[$popt->code]['payments_collections']->paid : 0;
                            $total_sold_gift_cards += ($popts_data[$popt->code]['sold_gift_cards'] ? $popts_data[$popt->code]['sold_gift_cards']->paid : 0) + ($popts_data[$popt->code]['returned_gift_cards'] ? $popts_data[$popt->code]['returned_gift_cards']->paid : 0);
                            $total_paid_installments += $popts_data[$popt->code]['paid_installments'] ? $popts_data[$popt->code]['paid_installments']->paid : 0;
                            $Sales_paid = ($popts_data[$popt->code]['Sales'] ? $popts_data[$popt->code]['Sales']->paid : 0) - (isset($payment_method_shipping[$popt->code]) ? $payment_method_shipping[$popt->code] : 0) - (isset($payment_method_tipping[$popt->code]) ? $payment_method_tipping[$popt->code] : 0);
                            $Returned_paid = $popts_data[$popt->code]['Returned'] ? $popts_data[$popt->code]['Returned']->paid : 0;
                            $display_tr = '';
                            $total_rete += $popts_data[$popt->code]['Sales'] ? $popts_data[$popt->code]['Sales']->total_rete : 0;
                            $total_return_rete += $popts_data[$popt->code]['Returned'] ? ($popts_data[$popt->code]['Returned']->total_rete * -1) : 0;
                            $total_rc_rete += $popts_data[$popt->code]['RC'] ? ($popts_data[$popt->code]['RC']->total_rete) : 0;
                            if ($RC_paid == 0 && $Sales_paid == 0 && $Returned_paid == 0) {
                                $display_tr = "style='display:none;'";
                            }
                         ?>
                        <input type="hidden" name="popt_id[]" value="<?= $popt->id ?>">
                        <input type="hidden" name="popt_cash_counted[]" value="<?= $popt->cash_payment ?>">
                        <input type="hidden" name="popt_description[]" value="<?= $popt->name ?>">
                        <input type="hidden" name="popt_payments[]" value="<?= $popts_data[$popt->code]['RC'] ? $popts_data[$popt->code]['RC']->paid : 0 ?>">
                        <input type="hidden" name="popt_sales[]" value="<?= $Sales_paid ?>">
                        <input type="hidden" name="popt_returns[]" value="<?= $popts_data[$popt->code]['Returned'] ? $popts_data[$popt->code]['Returned']->paid : 0 ?>">
                        <input type="hidden" name="popt_payments_collections[]" value="<?= $popts_data[$popt->code]['payments_collections'] ? $popts_data[$popt->code]['payments_collections']->paid : 0 ?>">
                        <input type="hidden" name="popt_sold_gift_cards[]" value="<?= $popts_data[$popt->code]['sold_gift_cards'] ? $popts_data[$popt->code]['sold_gift_cards']->paid : 0 ?>">
                        <input type="hidden" name="popt_returned_gift_cards[]" value="<?= $popts_data[$popt->code]['returned_gift_cards'] ? $popts_data[$popt->code]['returned_gift_cards']->paid : 0 ?>">
                        <input type="hidden" name="popt_paid_installments[]" value="<?= $popts_data[$popt->code]['paid_installments'] ? $popts_data[$popt->code]['paid_installments']->paid : 0 ?>">
                        <tr <?= $display_tr ?> class="<?= $popt->code == "cash" ? '' : '' ?>">
                            <td style="border-bottom: 1px solid #DDD;"><h4><?= $popt->name ?></h4></td>
                            <td style="border-bottom: 1px solid #DDD; display: none;" class="tr_payment_method_show text-right"><h4><?= $popts_data[$popt->code]['num_transactions'] ?></h4></td>
                            <?php
                            $Total_paid = $RC_paid + $Sales_paid + $Returned_paid;
                            $total_sales += $popt->code == "cash" && ($this->Settings->pos_register_hide_closing_detail == 1 || $this->Settings->pos_register_hide_closing_detail == 3) ? 0 : $Sales_paid;
                            $total_total += $popt->code == "cash" && ($this->Settings->pos_register_hide_closing_detail == 1 || $this->Settings->pos_register_hide_closing_detail == 3) ? 0 : $RC_paid + $Sales_paid;
                            $total_returns += $popt->code == "cash" && ($this->Settings->pos_register_hide_closing_detail == 1 || $this->Settings->pos_register_hide_closing_detail == 3) ? 0 : $Returned_paid;
                            if ($popt->code == "cash") {
                                $color_RC = $definir_color($RC_paid);
                                $color_Sales = $definir_color($Sales_paid);
                                $color_Sales_Returned = $definir_color($Sales_paid+$Returned_paid);
                                $color_Returned = $definir_color($Returned_paid);
                                $color_total = $definir_color($Total_paid);
                                $total_cash += $Total_paid;
                            } else {
                                $color_RC = "";
                                $color_Sales = "";
                                $color_Sales_Returned = "";
                                $color_Returned = "";
                                $color_total = "";
                            }
                            ?>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_RC ?>"  class="tr_payment_method"><?= $this->sma->formatMoney($RC_paid) ?></span></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_Sales ?>" class="tr_hide_tirilla tr_payment_method"><h4><span><?= $this->sma->formatMoney($Sales_paid) ?></span></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;<?= $color_Sales_Returned ?>" class="tr_show_tirilla tr_payment_method"><h4><span><?= $this->sma->formatMoney($Sales_paid+$Returned_paid); ?></span></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_Returned ?>" class="tr_hide_tirilla tr_payment_method"><h4><span><?= $this->sma->formatMoney($Returned_paid); ?></span></h4></td>
                            <td style="text-align:right; border-bottom: 1px solid #DDD; <?= $color_total ?>" class="tr_payment_method"><h4><span><?= $this->sma->formatMoney($Total_paid) ?></span></h4></td>
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
                <?php $credit_amount = ($totalsales->total - $totalsales->paid) + $duesales->paid;
                      $total_sales += $credit_amount;
                      $total_total += $credit_amount;

                    $display_tr = '';
                    if ($credit_amount == 0 && $duereturns->paid == 0) {
                        $display_tr = "style='display:none;'";
                    }
                                ?>
                <tr <?= $display_tr ?> >
                    <td width="300px;" style="font-weight:bold; border-bottom: 1px solid #EEE;"><h4>Ventas a crédito:</h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;" class="tr_hide_tirilla">
                        <h4><span><?= $this->sma->formatMoney($credit_amount) ?></span>
                        </h4>
                    </td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE; display: none;" class="tr_show_tirilla">
                        <h4><span><?= $this->sma->formatMoney($credit_amount+($duereturns->paid ? $duereturns->paid : 0)) ?></span>
                        </h4>
                    </td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;" width="200px;" style="font-weight:bold;text-align:right; border-bottom: 1px solid #EEE;" class="tr_hide_tirilla"><h4><span>
                                <?=  $this->sma->formatMoney($duereturns->paid ? $duereturns->paid : '0.00'); ?>
                                <?php $total_returns += $duereturns->paid; ?>
                            </span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;">
                        <h4>
                            <span>
                                <?= $this->sma->formatMoney($credit_amount + ($duereturns->paid ? $duereturns->paid : 0)) ?>
                            </span>
                        </h4>
                    </td>
                        <input type="hidden" name="popt_id[]" value="false">
                        <input type="hidden" name="popt_cash_counted[]" value="0">
                        <input type="hidden" name="popt_description[]" value="Ventas a crédito">
                        <input type="hidden" name="popt_payments[]" value="<?= 0 ?>">
                        <input type="hidden" name="popt_sales[]" value="<?= $credit_amount ?>">
                        <input type="hidden" name="popt_returns[]" value="<?= $duereturns->paid ? $duereturns->paid : 0 ?>">
                        <input type="hidden" name="popt_payments_collections[]" value="0">
                        <input type="hidden" name="popt_sold_gift_cards[]" value="0">
                        <input type="hidden" name="popt_returned_gift_cards[]" value="0">
                        <input type="hidden" name="popt_paid_installments[]" value="0">
                </tr>
<!-- ACA -->
                <?php
                $total_shipping = 0;
                $total_tipping = 0;
                $display_tr = '';
                if ($tips_cash && $tips_cash->amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('tips_by_cash'), lang('tip')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($tips_cash ? $tips_cash->amount : 0) ?>"><?= $this->sma->formatMoney($tips_cash ? $tips_cash->amount : 0) ?></span></h4></td>
                    <input type="hidden" name="tips_cash" value="<?= $tips_cash ? $tips_cash->amount : 0 ?>">
                    <?php $total_total += ($tips_cash ? $tips_cash->amount : 0); ?>
                    <?php $total_tipping += ($tips_cash ? $tips_cash->amount : 0); ?>
                </tr>
                <?php
                $display_tr = '';
                if ($tips_other_payments && $tips_other_payments->amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('tips_by_other_payments'), lang('tip')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($tips_other_payments ? $tips_other_payments->amount : 0) ?></span></h4></td>
                    <input type="hidden" name="tips_other_methods" value="<?= $tips_other_payments ? $tips_other_payments->amount : 0 ?>">
                    <?php $total_total += ($tips_other_payments ? $tips_other_payments->amount : 0); ?>
                    <?php $total_tipping += ($tips_other_payments ? $tips_other_payments->amount : 0); ?>
                </tr>
                <?php
                $display_tr = '';
                if ($tips_due && $tips_due->amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('tips_by_due'), lang('tip')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($tips_due ? $tips_due->amount : 0) ?></span></h4></td>
                    <input type="hidden" name="tips_due" value="<?= $tips_due ? $tips_due->amount : 0 ?>">
                    <?php $total_total += ($tips_due ? $tips_due->amount : 0); ?>
                    <?php $total_tipping += ($tips_due ? $tips_due->amount : 0); ?>
                </tr>
                <?php
                $display_tr = '';
                if ($shipping_cash && $shipping_cash->amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('shipping_by_cash'), lang('shipping')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span class="amount_cash" style="<?= $definir_color($shipping_cash ? $shipping_cash->amount : 0) ?>"><?= $this->sma->formatMoney($shipping_cash ? $shipping_cash->amount : 0) ?></span></h4></td>
                    <input type="hidden" name="shipping_cash" value="<?= $shipping_cash ? $shipping_cash->amount : 0 ?>">
                    <?php $total_total += ($shipping_cash ? $shipping_cash->amount : 0); ?>
                    <?php $total_shipping += ($shipping_cash ? $shipping_cash->amount : 0); ?>
                </tr>
                <?php
                $display_tr = '';
                if ($shipping_other_payments && $shipping_other_payments->amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('shipping_by_other_payments'), lang('shipping')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($shipping_other_payments ? $shipping_other_payments->amount : 0) ?></span></h4></td>
                    <input type="hidden" name="shipping_other_methods" value="<?= $shipping_other_payments ? $shipping_other_payments->amount : 0 ?>">
                    <?php $total_total += ($shipping_other_payments ? $shipping_other_payments->amount : 0); ?>
                    <?php $total_shipping += ($shipping_other_payments ? $shipping_other_payments->amount : 0); ?>
                </tr>
                <?php
                $display_tr = '';
                if ($shipping_due && $shipping_due->amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= sprintf(lang('shipping_by_due'), lang('shipping')); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"><h4><span><?= $this->sma->formatMoney($shipping_due ? $shipping_due->amount : 0) ?></span></h4></td>
                    <input type="hidden" name="shipping_due" value="<?= $shipping_due ? $shipping_due->amount : 0 ?>">
                    <?php $total_total += ($shipping_due ? $shipping_due->amount : 0); ?>
                    <?php $total_shipping += ($shipping_due ? $shipping_due->amount : 0); ?>
                </tr>
<!-- ACA FIN -->
                <tr class="tr_payment_method">
                    <td width="300px;" style="font-weight:bold;"><h4><?= lang('total'); ?></h4></td>
                    <td></td>
                    <td style="text-align:right;" class="tr_hide_tirilla"><h4><span><?= $this->sma->formatMoney($total_sales); ?></span></h4></td>
                    <td style="font-weight:bold;text-align:right;display: none;" class="tr_show_tirilla"><h4><span><?= $this->sma->formatMoney($total_total + $total_returns) ?></span></h4></td>
                    <td width="200px;" style="font-weight:bold;text-align:right;" class="tr_hide_tirilla"><h4><span><?= $this->sma->formatMoney($total_returns); ?></span></h4></td>
                    <td style="text-align:right;"><h4><span><?= $this->sma->formatMoney($total_total + $total_returns) ?></span></h4></td>
                </tr>

                <?php if (isset($expense_categories) && $expense_categories): ?>
                    <tr>
                        <th colspan="6"><?= lang('expenses_detail_by_category') ?></th>
                    </tr>
                    <?php foreach ($expense_categories as $expensec): ?>
                        <?php
                        $display_tr = '';
                        $total_expenses =  $expensec->total;
                        if ($total_expenses == 0) {
                            $display_tr = "style='display:none;'";
                        }
                         ?>
                        <tr <?= $display_tr ?>>
                            <td style=""><?= $expensec->expense_name; ?></td>
                            <td style="" class="tr_payment_method_show text-right"><?= $expensec->num_transactions; ?></td>
                            <td style="text-align:right;" class="tr_payment_method"></td>
                            <td style="text-align:right;" class="tr_hide_tirilla tr_payment_method"></td>
                            <td style="text-align:right; display: none;" class="tr_show_tirilla tr_payment_method"></td>
                            <td style="text-align:right;" class="tr_hide_tirilla tr_payment_method">
                                    <span><?= $this->sma->formatMoney($total_expenses); ?></span>
                                </td>
                            <td style="text-align:right;border-bottom: 1px solid #DDD;"></td>
                            <input type="hidden" name="expense_category_id[]" value="<?= $expensec->id ?>">
                            <input type="hidden" name="expense_category_description[]" value="<?= $expensec->expense_name ?>">
                            <input type="hidden" name="expense_category_amount[]" value="<?= $expensec->total ?>">
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
                <?php
                $display_tr = '';
                $total_expenses =  ($expenses ? $expenses->total : 0) + ($purchases_expenses ? $purchases_expenses->total : 0) + ($purchases_expenses_payments ? $purchases_expenses_payments->total : 0);
                if ($total_expenses == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style=""><h4><?= (isset($expense_categories) && $expense_categories) ? lang('total')." " : ""  ?><?= lang('expenses'); ?></h4></td>
                    <td style="text-align:right;"></td>
                    <td style="text-align:right;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right;" class="tr_hide_tirilla"><h4>
                            <span class="amount_cash" style="<?= $definir_color($total_expenses) ?>"><?= $this->sma->formatMoney($total_expenses); ?></span>
                        </h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"></td>
                    <input type="hidden" name="expenses" value="<?= $total_expenses ?>">
                </tr>
                <?php
                $display_tr = '';
                if ($ppayments->ppayments == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('ppayments'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"><h4>
                            <!-- <span><?php $expense = $expenses ? $expenses->total : 0; echo $this->sma->formatMoney($expense) . ' (' . $this->sma->formatMoney($expense) . ')'; ?></span> -->
                            <span class="amount_cash" style="<?= $definir_color($ppayments->ppayments) ?>"><?= $this->sma->formatMoney($ppayments ? $ppayments->ppayments : "0.00"); ?></span>
                        </h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"></td>
                    <input type="hidden" name="ppayments" value="<?= $ppayments ? $ppayments->ppayments : 0 ?>">
                </tr>
                <?php
                $display_tr = '';
                if ($customer_deposits && $customer_deposits->total == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('deposits_registered'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"><h4><span class="amount_cash" style="<?= $definir_color($customer_deposits->total) ?>"><?= $this->sma->formatMoney($customer_deposits ? $customer_deposits->total : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <input type="hidden" name="deposits" value="<?= $customer_deposits ? $customer_deposits->total : 0 ?>">
                </tr>
                <?php
                $display_tr = '';
                if ($customer_deposits_other_payments && $customer_deposits_other_payments->total == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('deposits_registered_other_payments'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"><h4><span><?= $this->sma->formatMoney($customer_deposits_other_payments ? $customer_deposits_other_payments->total : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <input type="hidden" name="deposits_other_methods" value="<?= $customer_deposits_other_payments ? $customer_deposits_other_payments->total : 0 ?>">
                </tr>
                <?php
                $display_tr = '';
                if ($supplier_deposits && $supplier_deposits->total == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('deposits_paid'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"><h4><span class="amount_cash" style="<?= $definir_color($supplier_deposits->total) ?>"><?= $this->sma->formatMoney($supplier_deposits ? $supplier_deposits->total : '0.00') ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <input type="hidden" name="suppliers_deposits" value="<?= $supplier_deposits ? $supplier_deposits->total : 0 ?>">
                </tr>
                <?php
                $display_tr = '';
                if ($pos_register_movements && $pos_register_movements->amount_in == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('pos_register_movements').": ".lang('movement_type_in'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"><h4><span class="amount_cash" style="<?= $definir_color($pos_register_movements ? $pos_register_movements->amount_in : 0) ?>"><?= $this->sma->formatMoney($pos_register_movements ? $pos_register_movements->amount_in : 0) ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <input type="hidden" name="movements_in" value="<?= $pos_register_movements ? $pos_register_movements->amount_in : 0 ?>">
                </tr>
                <?php
                $display_tr = '';
                if ($pos_register_movements && $pos_register_movements->amount_out == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('pos_register_movements').": ".lang('movement_type_out'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;" class="tr_hide_tirilla"><h4><span class="amount_cash" style="<?= $definir_color($pos_register_movements ? $pos_register_movements->amount_out : 0) ?>"><?= $this->sma->formatMoney($pos_register_movements ? $pos_register_movements->amount_out : 0) ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #DDD;"></td>
                    <input type="hidden" name="movements_out" value="<?= $pos_register_movements ? $pos_register_movements->amount_out : 0 ?>">
                </tr>

                <?php 
                $display_tr = '';
                if ($gc_topups_cash && $gc_topups_cash->payment_amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td width="300px;"><h4>Recargas tarjetas regalo (efectivo):</h4></td>
                    <td style="text-align:right; <?= $definir_color($gc_topups_cash ? $gc_topups_cash->payment_amount : 0) ?>"  class="amount_cash"><?= $this->sma->formatMoney($gc_topups_cash ? $gc_topups_cash->payment_amount : 0) ?></td>
                    <td class="tr_hide_tirilla"></td>
                    <td class="tr_show_tirilla" style="display:none;"></td>
                    <td class="tr_hide_tirilla"></td>
                    <td>
                        <input type="hidden" name="gc_topups_cash" value="<?= $gc_topups_cash ? $gc_topups_cash->payment_amount : 0 ?>">
                    </td>
                </tr>
                <?php 
                $display_tr = '';
                if ($gc_topups_other && $gc_topups_other->payment_amount == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td width="300px;"><h4>Recargas tarjetas regalo:</h4></td>
                    <td style="text-align:right;"><?= $this->sma->formatMoney($gc_topups_other ? $gc_topups_other->payment_amount : 0) ?></td>
                    <td class="tr_hide_tirilla"></td>
                    <td class="tr_show_tirilla" style="display:none;"></td>
                    <td class="tr_hide_tirilla"></td>
                    <td><input type="hidden" name="gc_topups_other" value="<?= $gc_topups_other ? $gc_topups_other->payment_amount : 0 ?>"></td>
                </tr>
                <?php if ($total_payments_collections > 0): ?>
                    <tr>
                        <th colspan="6" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4><?= lang('payments_collections') ?></h4></th>
                    </tr>
                    <?php if (isset($popts) && $popts): ?>
                        <?php foreach ($popts as $popt): ?>
                            <?php if (isset($popts_data[$popt->code]['payments_collections']) && $popts_data[$popt->code]['payments_collections']->paid > 0): ?>
                                <?php 
                                if ($popt->code == 'cash') {
                                    $total_cash += $popts_data[$popt->code]['payments_collections']->paid;
                                }
                                 ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popts_data[$popt->code]['payments_collections']->paid)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popts_data[$popt->code]['payments_collections']->paid) ?></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td></td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endif ?>
                <?php if ($total_sold_gift_cards != 0): ?>
                    <tr>
                        <th colspan="6" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4><?= sprintf(lang('sold_gift_cards'), lang('gift_cards')) ?></h4></th>
                    </tr>
                    <tr>
                        <th><?= lang('close_register_concept') ?></th>
                        <th><?= lang('value') ?></th>
                        <th><?= lang('return') ?></th>
                    </tr>
                    <?php if (isset($popts) && $popts): ?>
                        <?php foreach ($popts as $popt): ?>
                            <?php if ((isset($popts_data[$popt->code]['sold_gift_cards']) && $popts_data[$popt->code]['sold_gift_cards']->paid > 0) || (isset($popts_data[$popt->code]['returned_gift_cards']) && $popts_data[$popt->code]['returned_gift_cards']->paid != 0)): ?>
                                <?php 
                                if ($popt->code == 'cash') {
                                    $total_cash += ($popts_data[$popt->code]['sold_gift_cards'] ? $popts_data[$popt->code]['sold_gift_cards']->paid : 0) + ($popts_data[$popt->code]['returned_gift_cards'] ? $popts_data[$popt->code]['returned_gift_cards']->paid : 0);
                                }
                                 ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popts_data[$popt->code]['sold_gift_cards']->paid)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popts_data[$popt->code]['sold_gift_cards']->paid) ?></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popts_data[$popt->code]['returned_gift_cards']->paid)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popts_data[$popt->code]['returned_gift_cards']->paid) ?></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td></td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endif ?>
                
                
                <?php if ($total_paid_installments > 0): ?>
                    <tr>
                        <th colspan="6" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;"><h4><?= lang('payment_financing_fee') ?></h4></th>
                    </tr>
                    <?php if (isset($popts) && $popts): ?>
                        <?php foreach ($popts as $popt): ?>
                            <?php if (isset($popts_data[$popt->code]['paid_installments']) && $popts_data[$popt->code]['paid_installments']->paid > 0): ?>
                                <?php 
                                if ($popt->code == 'cash') {
                                    $total_cash += $popts_data[$popt->code]['paid_installments']->paid;
                                }
                                 ?>
                                <tr>
                                    <td width="300px;"><h4><?= lang($popt->code) ?></h4></td>
                                    <td style="text-align:right; <?= $popt->code == 'cash' ? $definir_color($popts_data[$popt->code]['paid_installments']->paid)."font-weight: 800;"  : '' ?>"><?= $this->sma->formatMoney($popts_data[$popt->code]['paid_installments']->paid) ?></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td class="tr_show_tirilla" style="display:none;"></td>
                                    <td class="tr_hide_tirilla"></td>
                                    <td></td>
                                </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                    <?php endif ?>
                <?php endif ?>
                
                
                <tr class="tr_payment_method_cash" style="border-top-width: 1px;border-top-style: solid;">
                    <td width="300px;" style="font-weight:bold;"><h4><strong><?= lang('total_cash'); ?></strong>:</h4></td>
                    <td></td>
                    <td style="text-align:right;"><h4>
                    <?php
                    $total_cash_amount = 
                                    $cash_in_hand + 
                                    $total_cash + 
                                    ($expenses ? $expenses->total : 0) + 
                                    ($purchases_expenses ? $purchases_expenses->total : 0) + 
                                    ($purchases_expenses_payments ? $purchases_expenses_payments->total : 0) + 
                                    $customer_deposits->total + 
                                    ($ppayments->ppayments ? $ppayments->ppayments : 0) + 
                                    ($supplier_deposits ? $supplier_deposits->total : 0) + 
                                    ($tips_cash ? $tips_cash->amount : 0) +
                                    ($shipping_cash ? $shipping_cash->amount : 0) +
                                    ($pos_register_movements ? $pos_register_movements->amount_in : 0) +
                                    ($pos_register_movements ? $pos_register_movements->amount_out : 0) +
                                    ($gc_topups_cash ? $gc_topups_cash->payment_amount : 0)
                                    ;
                                    
                        if ($this->Settings->pos_register_hide_closing_detail == 1 || $this->Settings->pos_register_hide_closing_detail == 3) {
                            $total_cash_amount = 0;
                        }
                    ?>
                            <span class="amount_cash" style="<?= $definir_color($total_cash_amount) ?>"><?= $this->sma->formatMoney($total_cash_amount); ?></span>
                        </h4></td>
                    <td style="text-align:right; display: none;" class="tr_show_tirilla"></td>
                    <td style="text-align:right;" class="tr_hide_tirilla"></td>
                    <td class="tr_hide_tirilla"></td>
                </tr>
                <?php if (isset($categories) && $categories): ?>
                    <tr>
                        <th colspan="7" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;" class="tr_payment_method"><h4>Resumen de Ventas por categoría </h4></th>
                    </tr>
                        <tr>
                            <th class="tr_hide_tirilla tr_payment_method"></th>
                            <th class="tr_show_tirilla tr_payment_method" style="display:none;"></th>
                            <th class="tr_hide_tirilla tr_payment_method"></th>
                            <th class="tr_payment_method">Cantidad</th>
                            <th class="tr_payment_method">Total</th>
                        </tr>
                        <?php
                        $c_subtotal = 0;
                        $c_descuento = 0;
                         ?>
                        <?php foreach ($categories as $category): ?>
                                <tr class="tr_payment_method">
                                   <th class="tr_hide_tirilla"><?= $category->name ?></th>
                                   <th class="tr_show_tirilla" style="display:none;"><?= $category->name ?></th>
                                   <th class="tr_hide_tirilla"></th>
                                   <th class="text-center"><?= $this->sma->formatQuantity($category->total_quantity) ?></th> 
                                   <th class="text-right"><?= $this->sma->formatMoney($category->total_sales - $category->order_discount) ?></th> 
                                    <?php 
                                    $c_subtotal += $category->total_sales;
                                    $c_descuento += $category->order_discount;
                                    ?>
                                </tr>
                                <input type="hidden" name="category_id[]" value="<?= $category->id ?>">
                                <input type="hidden" name="category_description[]" value="<?= $category->name ?>">
                                <input type="hidden" name="category_payments[]" value="<?= 0 ?>">
                                <input type="hidden" name="category_sales[]" value="<?= $category->total_sales ?>">
                                <input type="hidden" name="category_returns[]" value="<?= $category->total_devolutions ?>">
                                <input type="hidden" name="category_quantity[]" value="<?= $category->total_quantity ?>">
                        <?php endforeach ?>
                        <?php if ($total_tipping > 0): ?>
                                <tr class="tr_payment_method">
                                    <th class="tr_hide_tirilla"><?= lang('tips') ?></th>
                                    <th class="tr_show_tirilla" style="display:none;"><?= lang('tips') ?></th>
                                    <th class="tr_hide_tirilla"></th>
                                   <th class="text-right"><?= $this->sma->formatMoney(0) ?></th> 
                                   <th class="text-right"><?= $this->sma->formatMoney(0) ?></th> 
                                   <th class="text-right"><?= $this->sma->formatMoney($total_tipping) ?></th> 
                                </tr>
                        <?php endif ?>
                        <?php if ($total_shipping > 0): ?>
                                <tr class="tr_payment_method">
                                    <th class="tr_hide_tirilla"><?= lang('shipping') ?></th>
                                    <th class="tr_show_tirilla" style="display:none;"><?= lang('shipping') ?></th>
                                    <th class="tr_hide_tirilla"></th>
                                   <th class="text-right"><?= $this->sma->formatMoney(0) ?></th> 
                                   <th class="text-right"><?= $this->sma->formatMoney(0) ?></th> 
                                   <th class="text-right"><?= $this->sma->formatMoney($total_shipping) ?></th> 
                                </tr>
                        <?php endif ?>
                        <tr class="tr_payment_method">
                            <th class="tr_hide_tirilla">Total</th>
                            <th class="tr_show_tirilla" style="display:none;">Total</th>
                            <th class="tr_hide_tirilla"></th>
                            <th class="tr_hide_tirilla"></th>
                            <th class="tr_hide_tirilla"></th>
                            <th class="text-right"><?= $this->sma->formatMoney($c_subtotal - $c_descuento + $total_tipping + $total_shipping) ?></th>
                        </tr>
                <?php endif ?>
                <?php if (isset($r_products) && $r_products): ?>
                    <tr>
                        <th colspan="5" style="border-bottom: 1px solid #DDD; border-top: 1px solid #DDD; background-color:#e8e8e8; text-align: center;" class="tr_payment_method"><h4>Resumen de Ventas por producto </h4></th>
                    </tr>
                        <tr>
                            <th class="tr_hide_tirilla tr_payment_method"></th>
                            <th class="tr_show_tirilla tr_payment_method" style="display:none;"></th>
                            <th class="tr_hide_tirilla tr_payment_method"></th>
                            <th class="tr_payment_method">Cantidad</th>
                            <th class="tr_payment_method">Total</th>
                        </tr>
                        <?php
                        $c_subtotal = 0;
                        $c_descuento = 0;
                         ?>
                        <?php foreach ($r_products as $r_product): ?>
                                <tr class="tr_payment_method">
                                   <th class="tr_hide_tirilla" colspan="2"><?= $r_product->name ?></th>
                                   <th class="tr_show_tirilla" style="display:none;"><?= $r_product->name ?></th>
                                   <th class="tr_hide_tirilla"></th>
                                   <th class="text-center"><?= $this->sma->formatQuantity($r_product->total_quantity) ?></th> 
                                   <th class="text-right"><?= $this->sma->formatMoney($r_product->total_sales - $r_product->order_discount) ?></th> 
                                    <?php 
                                    $c_subtotal += $r_product->total_sales;
                                    $c_descuento += $r_product->order_discount;
                                    ?>
                                </tr>
                                <input type="hidden" name="category_id[]" value="<?= $r_product->id ?>">
                                <input type="hidden" name="category_description[]" value="<?= $r_product->name ?>">
                                <input type="hidden" name="category_payments[]" value="<?= 0 ?>">
                                <input type="hidden" name="category_sales[]" value="<?= $r_product->total_sales ?>">
                                <input type="hidden" name="category_returns[]" value="<?= $r_product->total_devolutions ?>">
                                <input type="hidden" name="category_quantity[]" value="<?= $r_product->total_quantity ?>">
                        <?php endforeach ?>
                        <?php if ($total_tipping > 0): ?>
                                <tr class="tr_payment_method">
                                    <th class="tr_hide_tirilla"><?= lang('tips') ?></th>
                                    <th class="tr_show_tirilla" style="display:none;"><?= lang('tips') ?></th>
                                    <th class="tr_hide_tirilla"></th>
                                    <th class="tr_hide_tirilla"></th>
                                    <th class="tr_hide_tirilla"></th>
                                   <th class="text-right"><?= $this->sma->formatMoney($total_tipping) ?></th> 
                                </tr>
                        <?php endif ?>
                        <?php if ($total_shipping > 0): ?>
                                <tr class="tr_payment_method">
                                    <th class="tr_hide_tirilla"><?= lang('shipping') ?></th>
                                    <th class="tr_show_tirilla" style="display:none;"><?= lang('shipping') ?></th>
                                    <th class="tr_hide_tirilla"></th>
                                    <th class="tr_hide_tirilla"></th>
                                    <th class="tr_hide_tirilla"></th>
                                   <th class="text-right"><?= $this->sma->formatMoney($total_shipping) ?></th> 
                                </tr>
                        <?php endif ?>
                        <tr class="tr_payment_method">
                            <th class="tr_hide_tirilla">Total</th>
                            <th class="tr_show_tirilla text-right" style="display:none;">Total</th>
                            <th class="tr_hide_tirilla"></th>
                            <th class="tr_hide_tirilla"></th>
                            <th class="tr_hide_tirilla"></th>
                            <th class="text-right"><?= $this->sma->formatMoney($c_subtotal - $c_descuento + $total_tipping + $total_shipping) ?></th>
                        </tr>
                <?php endif ?>
                <?php
                $display_tr = '';
                if ($total_rete == 0 && $total_return_rete == 0 && $total_rc_rete == 0) {
                    $display_tr = "style='display:none;'";
                }
                 ?>
                <tr <?= $display_tr ?> class="tr_payment_method">
                    <td width="300px;" style="font-weight:bold; border-bottom: 1px solid #EEE;" colspan="2"><h4><?= lang('retentions') ?>:</h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"><?= $this->sma->formatMoney($total_rc_rete) ?></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;" class="tr_hide_tirilla"><h4><span><?= $this->sma->formatMoney($total_rete) ?></span></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE; display: none;" class="tr_show_tirilla">
                        <h4>
                            <span>
                                <?= $this->sma->formatMoney($total_rete + $total_return_rete) ?>
                            </span>
                        </h4>
                        <input type="hidden" name="total_retention" value="<?= $total_rete ?>">
                        <input type="hidden" name="total_return_retention" value="<?= $total_return_rete ?>">
                        <input type="hidden" name="total_rc_retention" value="<?= $total_rc_rete ?>">
                    </td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;" width="200px;" style="font-weight:bold;text-align:right; border-bottom: 1px solid #EEE;" class="tr_hide_tirilla"><h4><span><?=  $this->sma->formatMoney($total_return_rete); ?></span></h4></td>
                </tr>
                <?php if ($register_movements && $this->Settings->show_pos_movements_close_register == 1): ?>
                    <tr class="tr_register_movements">
                        <td colspan="6" style="font-weight:bold; border-bottom: 1px solid #EEE; text-align: center;"><h4><?= lang('pos_register_movements') ?>:</h4></td>
                    </tr>
                    <?php foreach ($register_movements as $rm): ?>
                        <tr  class="tr_register_movements">
                            <td style="font-weight:bold; border-bottom: 1px solid #EEE;"><h4><?= $rm->reference_no ?></h4></td>
                            <td style="font-weight:bold; border-bottom: 1px solid #EEE;" colspan="3"><h4><?= $rm->movement_type == 1 ? lang('movement_type_in') : lang('movement_type_out') ?></h4></td>
                            <td style="font-weight:bold; border-bottom: 1px solid #EEE;"><h4><?= lang($rm->movement_type == 1 ? $rm->destination_paid_by : $rm->origin_paid_by) ?></h4></td>
                            <td style="font-weight:bold; border-bottom: 1px solid #EEE; text-align: right;"><h4><?= $this->sma->formatMoney($rm->movement_type == 1 ? $rm->amount : $rm->amount * -1) ?></h4></td>
                    
                            <input type="hidden" name="popt_id[]" value="false">
                            <input type="hidden" name="popt_cash_counted[]" value="0">
                            <input type="hidden" name="popt_description[]" value="<?= $rm->reference_no." ".($rm->movement_type == 1 ? lang('movement_type_in') : lang('movement_type_out'))." ".lang($rm->movement_type == 1 ? $rm->destination_paid_by : $rm->origin_paid_by) ?>">
                            <input type="hidden" name="popt_payments[]" value="<?= 0 ?>">
                            <input type="hidden" name="popt_sales[]" value="<?= ($rm->movement_type == 1 ? $rm->amount : $rm->amount * -1) ?>">
                            <input type="hidden" name="popt_returns[]" value="<?= 0 ?>">
                        </tr>
                    <?php endforeach ?>
                <?php endif ?>
                    
            </table>

            <?php if ($this->Settings->pos_register_hide_closing_detail == 0 || $this->Settings->pos_register_hide_closing_detail == 1): ?>
                <div class="row">
                    <div class="col-sm-12">
                        <?= lang("total_cash_submitted", "total_cash_submitted"); ?>
                        <?= form_hidden('total_cash', $total_cash_amount); ?>
                        <?= form_input('total_cash_submitted', (isset($_POST['total_cash_submitted']) ? $_POST['total_cash_submitted'] : $total_cash_amount), 'class="form-control input-tip" id="total_cash_submitted" required="required"'); ?>
                    </div>
                </div>
            <?php endif ?>

            <div class="no-print">
                <?php
                if ($suspended_bills) {
                    echo '<hr>
                            <h3>' . lang('opened_bills') . '</h3>
                            <table class="table table-hovered table-bordered no-print">
                                <thead>
                                    <tr>
                                        <th>' . lang('customer') . '</th>
                                        <th>' . lang('date') . '</th>
                                        <th>' . lang('total_items') . '</th>
                                        <th>' . lang('amount') . '</th>
                                        <th>' . lang('restobar') . '</th>
                                        <th><i class="fa fa-trash-o"></i></th>
                                    </tr>
                                </thead>
                                <tbody>';
                    foreach ($suspended_bills as $bill) {
                        echo '<tr>
                                <td>' . $bill->customer . '</td>
                                <td>' . $this->sma->hrld($bill->date) . '</td>
                                <td class="text-center">' . $bill->count . '</td>
                                <td class="text-right">' . $bill->total . '</td>
                                <td class="text-center">' . $bill->numero . '</td>
                                <td class="text-center"><a href="#" class="tip po" title="<b>' . $this->lang->line("delete_bill") . '</b>" data-content="<p>' . lang('r_u_sure') . '</p><a class=\'btn btn-danger po-delete\' href=\'' . admin_url('pos/delete/' . $bill->id) . '\'>' . lang('i_m_sure') . '</a> <button class=\'btn po-close\'>' . lang('no') . '</button>"  rel="popover"><i class="fa fa-trash-o"></i></a>
                                </td>
                            </tr>';
                    }
                            echo '</tbody>
                            </table>';
                }

                ?>
                <hr>

            </div>
            <div class="row no-print">
                <?php if ($this->Settings->pos_register_hide_closing_detail == 2 || $this->Settings->pos_register_hide_closing_detail == 3): ?>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <?php
                            // $total_cash_amount = $total_cash_amount;
                            ?>
                            <?= lang("total_cash_submitted", "total_cash_submitted"); ?>
                            <?= form_hidden('total_cash', $total_cash_amount); ?>
                            <?= form_input('total_cash_submitted', (isset($_POST['total_cash_submitted']) ? $_POST['total_cash_submitted'] : $total_cash_amount), 'class="form-control input-tip" id="total_cash_submitted" required="required"'); ?>
                        </div>
                    </div>
                <?php endif ?>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("total_cheques", "total_cheques_submitted"); ?>
                        <?= form_hidden('total_cheques', $chsales->total_cheques); ?>
                        <?= form_input('total_cheques_submitted', (isset($_POST['total_cheques_submitted']) ? $_POST['total_cheques_submitted'] : $chsales->total_cheques), 'class="form-control input-tip" id="total_cheques_submitted" required="required"'); ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?php if ($suspended_bills) { ?>
                        <div class="form-group">
                            <?= lang("transfer_opened_bills", "transfer_opened_bills"); ?>
                            <?php $u = $user_id ? $user_id : $this->session->userdata('user_id');
                            if ($Owner || $Admin) {
                                $usrs[-1] = lang('delete_all');
                            }
                            $usrs[0] = lang('leave_opened');
                            foreach ($users as $user) {
                                if ($user->id != $u) {
                                    $usrs[$user->id] = $user->first_name . ' ' . $user->last_name;
                                }
                            }
                            ?>
                            <?= form_dropdown('transfer_opened_bills', $usrs, (isset($_POST['transfer_opened_bills']) ? $_POST['transfer_opened_bills'] : 0), 'class="form-control input-tip" id="transfer_opened_bills" required="required"'); ?>
                        </div>
                    <?php } ?>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <?= lang("total_cc_slips", "total_cc_slips_submitted"); ?>
                        <?= form_hidden('total_cc_slips', $ccsales->total_cc_slips); ?>
                        <?= form_input('total_cc_slips_submitted', (isset($_POST['total_cc_slips_submitted']) ? $_POST['total_cc_slips_submitted'] : $ccsales->total_cc_slips), 'class="form-control input-tip" id="total_cc_slips_submitted" required="required"'); ?>
                    </div>
                </div>
            </div>
            <?php if (!isset($cron_job)): ?>
                <div class="form-group no-print">
                    <label for="note"><?= lang("note"); ?></label>
                    <?php 
                    
                    $note = isset($note) ? $note : null;
                    
                     ?>
                    <div
                        class="controls"> <?= form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : $note), 'class="form-control" id="note" style="margin-top: 10px; height: 100px;"'); ?> </div>
                </div>
            <?php endif ?>

        </div>
        <div class="modal-footer no-print">
            <em class="text-danger bold advertisement_close" style="display:none;">
                Por favor no cierre la ventana hasta que se complete el proceso de cierre
            </em>
            <?php if (!isset($cron_job) && ($user_id == NULL && $v == 'index' && $m == 'pos') || $user_id != NULL): ?>
                <button type="button" class="btn btn-wappsi-pagar close_register_btn" onclick="imprimir_factura_pos();">
                    <i class="fa fa-print"></i> <?= lang('print_without_close'); ?>
                </button>
                <button type="button" class="btn btn-primary close_register_btn close_register_submit" >
                    <?= lang('submit_close_register') ?>
                </button>
            <?php endif ?>
            <?php if ($user_id == NULL && ($v != 'index' || $m != 'pos')): ?>
                <em>Porfavor, diríjase a la pantalla del POS para poder hacer el cierre</em>
            <?php endif ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

</div>
<?= $modal_js ?>
<script type="text/javascript">
    $(document).ready(function () {

        $(".close_register_submit").click(function( event ){

            if (site.settings.pos_register_hide_closing_detail == 1) {
                if ($('#total_cash_submitted').val() == 0) {
                    command: toastr.error('Debe indicar el total de efectivo del cierre.', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#total_cash_submitted').select();
                    return false;
                }
            }
            // window.print();
            $('.close_close_register').fadeOut();
            $('.advertisement_close').fadeIn();
            $('.close_register_btn').fadeOut();
            event.preventDefault();
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && site.pos_settings.mobile_print_automatically_invoice == 2)
            {
                setTimeout(function() {
                    $('.close_register_form').submit();
                }, (site.pos_settings.mobile_print_wait_time * 1000));
            } else {

                window.onafterprint = function(e){
                    $(window).off('mousemove', window.onafterprint);
                    $('.close_register_form').submit();
                };
                setTimeout(function(){
                    $('.close_register_form').submit();
                }, 850);
            }
        });

        $(document).on('click', '.po', function (e) {
            e.preventDefault();
            $('.po').popover({
                html: true,
                placement: 'left',
                trigger: 'manual'
            }).popover('show').not(this).popover('hide');
            return false;
        });
        $(document).on('click', '.po-close', function () {
            $('.po').popover('hide');
            return false;
        });
        $(document).on('click', '.po-delete', function (e) {
            var row = $(this).closest('tr');
            e.preventDefault();
            $('.po').popover('hide');
            var link = $(this).attr('href');
            $.ajax({
                type: "get", url: link,
                success: function (data) {
                    row.remove();
                    addAlert(data, 'success');
                },
                error: function (data) {
                    addAlert('Failed', 'danger');
                }
            });
            return false;
        });
    });
    function addAlert(message, type) {
        $('#alerts').empty().append(
            '<div class="alert alert-' + type + '">' +
            '<button type="button" class="close" data-dismiss="alert">' +
            '&times;</button>' + message + '</div>');
    }


    $(document).on('change', '.tirilla', function(){
        check = $(this);

        if (check.is(':checked')) {
            $('.tr_hide_tirilla').css('display', 'none');
            $('.tr_show_tirilla').css('display', '');
            $('.modal-dialog h4').css('font-size', '95%');
        } else {
            $('.tr_hide_tirilla').css('display', '');
            $('.tr_show_tirilla').css('display', 'none');
            $('.modal-dialog h4').css('font-size', '');
        }

    });

var newWin;
    function imprimir_factura_pos()
    {
        var boxd = bootbox.dialog({
                title: "<i class='fa fa-exclamation-circle'></i> Cierre informativo",
                message: 'Este informe se visualizará para efectos informativos y no generará cierre de la caja',
                buttons: {
                    success: {
                        label: "<i class='fa fa-tick'></i> OK",
                        className: "btn-success",
                        callback: function () {
                            $('.without_close').css('display', '');
                            var divToPrint=document.getElementById('closeRegisterPrint');
                            newWin=window.open('','Print-Window');
                            var header = $('head').html();
                            newWin.document.open();
                            newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
                            newWin.document.close();
                            setTimeout(function(){
                                newWin.close();
                                $('.without_close').css('display', 'none');
                            },1000);
                            $('#myModal').modal('hide');
                        }
                    }
                }
            });
    }

</script>


<?php

    unset($def_color);

 ?>