<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    if ($this->session->userdata('purchase_processing')) {
        $this->session->unset_userdata('purchase_processing');
    }
?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link"><i class="fa fa-chevron-down"></i></a>
                    </div>
                </div>

                <div class="ibox-content" style="display: none;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <?= admin_form_open('purchases/supporting_document_index', ['id'=>'purchases_filter']) ?>
                                <?php echo form_hidden('support_document', 1); ?>
                                <div class="col-sm-4">
                                    <?= lang('reference_no', 'purchase_reference_no') ?>
                                    <select name="purchase_reference_no" id="purchase_reference_no" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($documents_types): ?>
                                            <?php foreach ($documents_types as $dt): ?>
                                                <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['purchase_reference_no']) && $_POST['purchase_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre." (".$dt->sales_prefix.")" ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                        $biller_selected = '';
                                        $biller_readonly = false;
                                        if ($this->session->userdata('biller_id')) {
                                            $biller_selected = $this->session->userdata('biller_id');
                                            $biller_readonly = true;
                                        }

                                        $bl[""] = lang('select');
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        }
                                        echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                    ?>
                                </div>

                                <div class="col-sm-4">
                                    <label class="control-label" for="supplier"><?= lang("supplier"); ?></label>
                                    <?= form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'id="filter_supplier" data-placeholder="' . lang("select") . ' ' . lang("supplier") . '" class="form-control input-tip" style="width:100%;"'); ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= lang('status', 'status') ?>
                                    <select name="status" id="status" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="completed" <?= isset($_POST['status']) && $_POST['status'] == 'completed' ? 'selected="selected"' : '' ?>><?= lang('completed') ?></option>
                                        <option value="pending" <?= isset($_POST['status']) && $_POST['status'] == 'pending' ? 'selected="selected"' : '' ?>><?= lang('pending') ?></option>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <?= lang('payment_status', 'payment_status') ?>
                                    <select name="payment_status" id="payment_status" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="pending" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'pending' ? 'selected="selected"' : '' ?>><?= lang('pending') ?></option>
                                        <option value="due" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'due' ? 'selected="selected"' : '' ?>><?= lang('due') ?></option>
                                        <option value="partial" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'partial' ? 'selected="selected"' : '' ?>><?= lang('partial') ?></option>
                                        <option value="paid" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'paid' ? 'selected="selected"' : '' ?>><?= lang('paid') ?></option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label><?= lang('payment_term') ?></label>
                                    <input type="number" name="dias_vencimiento" id="dias_vencimiento" class="form-control" value="<?= isset($dias_vencimiento) ? $dias_vencimiento : '' ?>">
                                </div>

                                <?php if ($this->Owner || $this->Admin): ?>
                                    <div class="col-sm-4">
                                        <label><?= lang('user') ?></label>
                                        <select name="filter_user" id="filter_user" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($users): ?>
                                                <?php foreach ($users as $user): ?>
                                                    <option value="<?= $user->id ?>"><?= $user->first_name." ".$user->last_name ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                <?php endif ?>

                                <hr class="col-sm-11">

                                <?php if ($this->Settings->purchase_datetime_management == 1) : ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                    <div class="date_controls_dh">
                                        <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_dh') ?>
                                                <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('start_date', 'start_date') ?>
                                            <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                        </div>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('end_date', 'end_date') ?>
                                            <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control datetime">
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <?= lang('date_records_filter', 'date_records_filter') ?>
                                        <select name="date_records_filter" id="date_records_filter_h" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>

                                    <div class="date_controls_h">
                                        <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_h') ?>
                                                <select name="filter_year" id="filter_year_h" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('start_date', 'start_date') ?>
                                            <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                        </div>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('end_date', 'end_date') ?>
                                            <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                        </div>
                                    </div>
                                <?php endif ?>

                                <div class="col-sm-12" style="margin-top: 2%;">
                                    <input type="hidden" name="filtered" value="1">
                                    <button type="submit" id="submit-purchases-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    <button type="button" onclick="window.location.href = site.base_url+'purchases';" class="btn btn-danger"><span class="fa fa-times"></span> <?= lang('reset') ?></button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
        <?= admin_form_open('purchases/purchase_actions', 'id="action-form"'); ?>
    <?php } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <h4 class="text_filter"></h4>
                                <table id="supportDocumentTable" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><input class="checkbox checkft" type="checkbox" name="check"/></th>
                                            <th><?= lang("id"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_origin"); ?></th>
                                            <th><?= lang("purchase_type"); ?></th>
                                            <th><?= lang("ref_no"); ?></th>
                                            <th><?= lang("affects"); ?></th>
                                            <?php if ($this->Settings->management_consecutive_suppliers == 1): ?>
                                                <th><?= lang("consecutive_supplier"); ?></th>
                                            <?php endif ?>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <th><?= lang("purchase_status"); ?></th>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("payment_status"); ?></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                            <th><?= lang("acepted_DIAN"); ?></th>
                                            <th><?= lang("acepted_DIAN"); ?></th>
                                            <th style="width:100px;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="12" class="dataTables_empty"><?=lang('loading_data_from_server');?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) {?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"'); ?>
    </div>
    <?=form_close()?>
<?php } ?>

<div class="modal fade" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Lista de errores de Documento de soporte</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang("close"); ?></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        start_date = "<?= $this->filtros_fecha_inicial ?>";
        filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        end_date = "<?= $this->filtros_fecha_final ?>";
        purchase_reference_no = '';
        dias_vencimiento = '';
        payment_status = '';
        filtered = 0;
        filter_user = '';
        supplier = '';
        biller = '';
        status = '';

        support_document = 1;

        <?php if (isset($_POST['purchase_reference_no'])): ?> purchase_reference_no = '<?= $_POST['purchase_reference_no'] ?>'; <?php endif ?>
        <?php if (isset($_POST['dias_vencimiento'])): ?> dias_vencimiento = '<?= $_POST['dias_vencimiento'] ?>'; <?php endif ?>
        <?php if (isset($_POST['payment_status'])): ?> payment_status = '<?= $_POST['payment_status'] ?>'; <?php endif ?>
        <?php if (isset($_POST['filter_user'])): ?> filter_user = '<?= $_POST['filter_user'] ?>'; <?php endif ?>
        <?php if (isset($_POST['start_date'])): ?> start_date = '<?= $_POST['start_date'] ?>'; <?php endif ?>
        <?php if (isset($_POST['supplier'])): ?> supplier = '<?= $_POST['supplier'] ?>'; <?php endif ?>
        <?php if (isset($_POST['filtered'])): ?> filtered = '<?= $_POST['filtered'] ?>'; <?php endif ?>
        <?php if (isset($_POST['end_date'])): ?> end_date = '<?= $_POST['end_date'] ?>'; <?php endif ?>
        <?php if (isset($_POST['biller'])): ?> biller = '<?= $_POST['biller'] ?>'; <?php endif ?>
        <?php if (isset($_POST['status'])): ?> status = '<?= $_POST['status'] ?>'; <?php endif ?>

        <?php if ($this->session->userdata('remove_pols')) { ?>
            if (localStorage.getItem('poorderdiscounttoproducts')) { localStorage.removeItem('poorderdiscounttoproducts'); }
            if (localStorage.getItem('poorderdiscountmethod')) { localStorage.removeItem('poorderdiscountmethod'); }
            if (localStorage.getItem('othercurrencycode')) { localStorage.removeItem('othercurrencycode'); }
            if (localStorage.getItem('othercurrencytrm')) { localStorage.removeItem('othercurrencytrm'); }
            if (localStorage.getItem('popayment_status')) { localStorage.removeItem('popayment_status'); }
            if (localStorage.getItem('popayment_term')) { localStorage.removeItem('popayment_term'); }
            if (localStorage.getItem('poretenciones')) { localStorage.removeItem('poretenciones'); }
            if (localStorage.getItem('othercurrency')) { localStorage.removeItem('othercurrency'); }
            if (localStorage.getItem('purchase_type')) { localStorage.removeItem('purchase_type'); }
            if (localStorage.getItem('powarehouse')) { localStorage.removeItem('powarehouse'); }
            if (localStorage.getItem('podiscount')) { localStorage.removeItem('podiscount'); }
            if (localStorage.getItem('poshipping')) { localStorage.removeItem('poshipping'); }
            if (localStorage.getItem('posupplier')) { localStorage.removeItem('posupplier'); }
            if (localStorage.getItem('pocurrency')) { localStorage.removeItem('pocurrency'); }
            if (localStorage.getItem('pobiller')) { localStorage.removeItem('pobiller'); }
            if (localStorage.getItem('poextras')) { localStorage.removeItem('poextras'); }
            if (localStorage.getItem('postatus')) { localStorage.removeItem('postatus'); }
            if (localStorage.getItem('poitems')) { localStorage.removeItem('poitems'); }
            if (localStorage.getItem('potax2')) { localStorage.removeItem('potax2'); }
            if (localStorage.getItem('ponote')) { localStorage.removeItem('ponote'); }
            if (localStorage.getItem('podate')) { localStorage.removeItem('podate'); }
            if (localStorage.getItem('poref')) { localStorage.removeItem('poref'); }
            <?php $this->sma->unset_data('remove_pols'); ?>
        <?php } ?>

        if (filtered !== undefined) {
            setTimeout(function() {
                $('#start_date').val(start_date);
                $('#end_date').val(end_date);
                $('#dias_vencimiento').val(dias_vencimiento);
                $('#purchase_reference_no').select2('val', purchase_reference_no);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                $('#status').select2('val', status);
                $('#payment_status').select2('val', payment_status);
            }, 900);
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);

        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>

        <?php if ($this->Settings->purchase_datetime_management == 1): ?>
            setTimeout(function() {
                <?php if (!isset($_POST['date_records_filter'])): ?>
                    $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                    $('#purchases_filter').submit();
                <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                    $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
                <?php endif ?>
            }, 150);
        <?php else: ?>
            setTimeout(function() {
                <?php if (!isset($_POST['date_records_filter'])): ?>
                    $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                    $('#sales_filter').submit();
                <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                    $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
                <?php endif ?>
            }, 150);
        <?php endif ?>


        loadTable();
        loadFilterSupplier();

        $(document).on('click', '.purchase_link3 td:not(:first-child, :nth-child(12), :nth-last-child(2), :last-child)', function() {
            $('#myModal').modal({remote: site.base_url + 'purchases/modal_view/' + $(this).parent('.purchase_link3').attr('id')});
            $('#myModal').modal('show');
        });

        $(document).on('click', '.sendElectronicDocument', function (event) { confirmSendElectronicDocument(event, $(this)); });
        $(document).on('click', '.checkStatusElectronicDocument', function (event) { confirmCheckStatusElectronicDocument(event, $(this)); });
        $(document).on('click', '.pendingErrors', function () { openErrorModal($(this).attr('purchaseId')); });
        $(document).on('click', '.sendEmail', function (event) { confirmSendEmail(event, $(this)); });

    });

    function loadTable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }
        oTable = $('#supportDocumentTable').dataTable({
            "aaSorting": [[1, "desc"], [2, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength":  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?=admin_url('purchases/getPurchasesSupportingDocument' . ($warehouse_id ? '/' . $warehouse_id : ''))?>',
            responsive: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                }, {
                    "name": "start_date",
                    "value": start_date
                }, {
                    "name": "end_date",
                    "value": end_date
                }, {
                    "name": "dias_vencimiento",
                    "value": dias_vencimiento
                }, {
                    "name": "purchase_reference_no",
                    "value": purchase_reference_no
                }, {
                    "name": "biller",
                    "value": biller
                }, {
                    "name": "supplier",
                    "value": supplier
                }, {
                    "name": "status",
                    "value": status
                }, {
                    "name": "payment_status",
                    "value": payment_status
                }, {
                    "name": "user",
                    "value": filter_user
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                {"bSortable": false,"mRender": checkbox},
                null,
                {"mRender": fld},
                null,
                null,
                null,
                null,
                <?php if ($this->Settings->management_consecutive_suppliers == 1): ?>
                    null,
                <?php endif ?>
                null,
                {"mRender": row_status},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": pay_status},
                {"bSortable": false,"mRender": attachment},
                {
                    mRender:  function(data, type, row) {
                        purchaseId = row[0];
                        documentStatus = data;
                        if (documentStatus == <?= NOT_SENT ?>) {
                            return '<div class="text-center"><span class="documentStatus label label-danger">'+lang["not_sent"]+'</span></div>';
                        } else if(documentStatus == <?= PENDING ?>) {
                            return '<div class="text-center"><span class="documentStatus pendingErrors label label-warning" purchaseId="'+ purchaseId +'">'+lang["pending"]+'</span></div>';
                        } else if(documentStatus == <?= ACCEPTED ?>) {
                            return '<div class="text-center"><span class="documentStatus label label-primary">'+lang["accepted"]+'</span></div>';
                        } else if (documentStatus == <?= SEND ?>) {
                            return '<div class="text-center"><span class="documentStatus label label-success" '+lang["sent"]+'>'+lang["sent"]+'</span></div>';
                        } else {
                            return '';
                        }
                    }
                },
                {"bVisible":false},
                {
                    mRender: function (data, type, row) {
                        actionsButton = $(data);
                        var documentId = row[0];
                        var reference = row[5];
                        var affect = row[6];
                        var status = row[8];
                        var supportingDocumentType = (status == 'returned') ? 52 : 35;
                        documentStatus = <?php if ($this->Settings->management_consecutive_suppliers == 1){ ?> row[15] <?php } else { ?> row[14] <?php } ?>;
                        electronicDocument = <?php if ($this->Settings->management_consecutive_suppliers == 1){ ?> row[16] <?php } else { ?> row[15] <?php } ?>;
                        actionsButton.find('.dropdown-menu').prepend('<li class="divider"></li>');
                        actionsButton.find('.dropdown-menu').prepend('<li><a href="'+ site.base_url + 'purchases/showPreviousRequest/'+documentId +'" target="_blank"><i class="fa fa-file-code-o"></i> <?= lang('see_previous_request') ?></a></li>');
                        if (electronicDocument == 1 && (documentStatus == <?= NOT_SENT ?> || documentStatus == <?= PENDING ?>)) {
                            actionsButton.find('.dropdown-menu').prepend('<li class="checkStatusElectronicDocument" reference="'+reference+'"><a href="admin/purchases/checkStatus/'+ documentId +'"><i class="fa fa-search"></i> Consultar estado</a></li>');
                            actionsButton.find('.dropdown-menu').prepend('<li class="sendElectronicDocument" reference="'+reference+'" supportingDocumentType="'+supportingDocumentType+'"><a href="admin/purchases/sendElectronicDocument/'+ documentId +'"><i class="far fa-paper-plane"></i> Enviar Documento Electrónico</a></li>');

                            actionsButton.find('.dropdown-menu').find('.returnPurchase').remove();
                        } else if (documentStatus == <?= ACCEPTED ?>) {
                            actionsButton.find('.dropdown-menu').prepend('<li class="sendEmail" reference="'+reference+'"><a href="admin/purchases/sendEmail/'+ documentId +'"><i class="fa fa-envelope-o"></i> Enviar E-mail</a></li>');
                            if (affect != null) {
                                actionsButton.find('.dropdown-menu').find('.returnPurchase').remove();
                            }

                        } else if (documentStatus == <?= SEND ?>) {
                            actionsButton.find('.dropdown-menu').prepend('<li class="checkStatusElectronicDocument" reference="'+reference+'"><a href="admin/purchases/checkStatus/'+ documentId +'"><i class="fa fa-search"></i> Consultar estado</a></li>');
                        }

                        actionsButton.find('.dropdown-menu').prepend('<li><label style="padding-left: 20px; padding-top: 10px">Documento Soporte Electrónico</label></li>');

                        if (documentStatus == <?= PENDING ?> || documentStatus == <?= ACCEPTED ?>) {
                            actionsButton.find('.dropdown-menu').find('.editPurchase').remove();
                        }

                        actionsButtonString = actionsButton.prop('outerHTML');
                        return actionsButtonString;
                    },
                    bSortable: false
                }
            ],
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "purchase_link3";
                return nRow;
            },
            fnDrawCallback: function (oSettings) {
                loadActionsButtons();

                $('input[type="checkbox"]').not('.skip').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%'
                });

                $('[data-toggle="tooltip"]').tooltip();
                $('[data-toggle-second="tooltip"]').tooltip();
            }
        });
    }

    function loadActionsButtons()
    {
        $warehauseActionButton = '';
        <?php if ($this->Owner || $this->Admin) { ?>
            addSupportDocument = '<a href="<?= admin_url('purchases/add_support_document') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>';
        <?php } else { ?>
            addSupportDocument = '';
            <?php if ($GP['supportingDocument-add']) { ?>
                addSupportDocument = '<a href="<?= admin_url('purchases/add_support_document') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>';
            <?php } ?>
        <?php } ?>

        <?php if (!empty($warehouses)) { ?>
            warehouseList = '';
            <?php foreach ($warehouses as $warehouse) { ?>
                warehouseList += '<li <?= ($warehouse_id && $warehouse_id == $warehouse->id) ? 'class="active"' : '' ?>><a href="<?= admin_url('purchases/'. $warehouse->id) ?>"><i class="fa fa-building"></i><?= $warehouse->name ?></a></li>';
            <?php } ?>

            $warehauseActionButton = '<div class="pull-right dropdown" style="margin-top: 5px; margin-right: 1px;">'+
                                        '<button data-toggle="dropdown" class="btn btn-outline btn-success"> Almacenes <span class="caret"></span> </button>'+
                                        '<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">'+
                                            '<li><a href="<?=admin_url('purchases')?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>'+
                                            '<li class="divider"></li>'+
                                            warehouseList +
                                        '</ul>'+
                                    '</div>';
        <?php } ?>

        $('.actionsButtonContainer').html(addSupportDocument +
        '<div class="pull-right dropdown">'+
            '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="left" title="<?= lang('actions') ?>"><i class="fas fa-magic fa-lg"></i></button>'+
            '<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">'+
                '<li><a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?></a></li>'+
                '<li><a href="#" id="combine" data-action="combine"><i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?></a></li>'+
                '<li><a href="#" id="sync_payments" data-action="sync_payments"><i class="fa fa-dollar"></i> <?=lang('sync_payments')?></a></li>'+
                '<li><a href="#" id="post_sale" data-action="post_purchase"><i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?></a></li>'+
                '<li class="divider"></li>'+
            '</ul>'+
        '</div>');

        $('[data-toggle-second="tooltip"]').tooltip();
    }

    function loadFilterSupplier()
    {
        if (supplier != '') {
            $('#filter_supplier').val(supplier).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"suppliers/getsupplier/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                            support_document: support_document,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });

            $('#filter_supplier').trigger('change');
        } else {
            $('#filter_supplier').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"suppliers/getsupplier/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10,
                            support_document: support_document,
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        }
    }

    function calcularMinutos(start_date, end_date)
    {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff/(1000 * 60);
        return minutos;
    }

    function setFilterText()
    {
        var reference_text = $('#purchase_reference_no option:selected').data('dtprefix');
        var biller_text = $('#biller option:selected').text();
        var supplier_text = $('#filter_supplier').select2('data') !== null ? $('#filter_supplier').select2('data').text : '';
        var status_text = $('#status option:selected').text();
        var payment_status_text = $('#payment_status option:selected').text();
        var start_date_text = $('<?= $this->Settings->purchase_datetime_management == 1 ? "#start_date_dh" : "#start_date_h" ?>').val();
        var end_date_text = $('<?= $this->Settings->purchase_datetime_management == 1 ? "#end_date_dh" : "#end_date_h" ?>').val();
        var text = "Filtros configurados : ";

        coma = false;

        if (purchase_reference_no != '' && purchase_reference_no !== undefined) {
            text+=" Tipo documento ("+reference_text+")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text+= coma ? "," : "";
            text+=" Sucursal ("+biller_text+")";
            coma = true;
        }
        if (supplier != '' && supplier !== undefined) {
            text+= coma ? "," : "";
            text+=" Proveedor ("+supplier_text+")";
            coma = true;
        }
        if (status != '' && status !== undefined) {
            text+= coma ? "," : "";
            text+=" Estado de compra ("+status_text+")";
            coma = true;
        }
        if (payment_status != '' && payment_status !== undefined) {
            text+= coma ? "," : "";
            text+=" Estado de pago ("+payment_status_text+")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha de inicio ("+start_date_text+")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text+= coma ? "," : "";
            text+=" Fecha final ("+end_date_text+")";
            coma = true;
        }

        $('.text_filter').html(text);
    }

    function confirmSendElectronicDocument(event, element)
    {
        event.preventDefault();
        var reference = element.attr('reference');
        var href = element.find('a').attr('href');
        var supportingDocumentType = element.attr('supportingDocumentType');
        var text = (supportingDocumentType == 52) ? 'la nota de ajuste de' : 'el';

        swal({
            title: 'Enviar documento electrónico',
            text: '¿Está seguro de enviar '+ text +' documento de soporte: ' + reference + '?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Enviando documento soporte...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            window.location.href = href;
        });
    }

    function openErrorModal(documentId)
    {
        $.ajax({
            type: "post",
            url: "<?= admin_url('purchases/getMessage') ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'documentId': documentId
            },
            dataType: "json",
            success: function (response) {
                swal({
                    title: response.title,
                    text: response.text,
                    type: 'info',
                    showCancelButton: true,
                    showConfirmButton: false,
                    cancelButtonText: "Ok"
                });
            }
        });
    }

    function confirmSendEmail(event, element)
    {
        event.preventDefault();

        var reference = element.attr('reference');
        var href = element.find('a').attr('href');

        swal({
            title: 'Enviar correo electrónico',
            text: '¿Está seguro de enviar el correo electrónico del documento: ' + reference + '?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Enviando correo electrónico...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            window.location.href = href;
        });
    }

    function confirmCheckStatusElectronicDocument(event, element)
    {
        event.preventDefault();

        var reference = element.attr('reference');
        var href = element.find('a').attr('href');

        swal({
            title: 'Consultar estado del documento',
            text: '¿Está seguro de realizar la consulta del documento de soporte: ' + reference + '?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Consultando documento...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            window.location.href = href;
        });
    }
</script>7y