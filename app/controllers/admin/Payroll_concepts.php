<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_concepts extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableElectronicPayroll) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->Payroll_settings = $this->site->get_payroll_setting();

        $this->lang->admin_load('payroll', $this->Settings->user_language);

        $this->load->library('form_validation');
        $this->load->admin_model('Payroll_concepts_model');
        $this->load->admin_model("UserActivities_model");
	}

	public function index()
	{
		$this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->data['action'] = "";
        $this->data["concepts"] = $this->Payroll_concepts_model->get();

        $this->page_construct('payroll_concepts/index', ["page_title"=>lang("payroll_concepts")], $this->data);
	}

	public function get_datatables()
	{
        $this->load->library('datatables');
        $this->datatables->select("payroll_concepts.id AS id, payroll_concepts.name, payroll_concept_type.name AS concept_type_name, payroll_concept_type.type, payroll_concepts.percentage, payroll_concepts.status, payroll_concept_type.fixed");
        $this->datatables->from("payroll_concepts");
        $this->datatables->join("payroll_concept_type", "payroll_concept_type.id = payroll_concepts.concept_type_id", "left");
        $this->datatables->add_column("Actions", '<div class="text-center"><div class="btn-group text-left">
								                    <button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
								                    	. lang("actions") .'<span class="caret"></span>
								                    </button>
								                        <ul class="dropdown-menu pull-right" role="menu">
								                            <li class="print_document">
					                            				<a href="'. admin_url('payroll_concepts/edit/$1') .'" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> '. lang('edit') .'</a>
			                                                </li>
								                            <li class="divider"></li>
								                        </ul>
							                      </div>', "id");
        echo $this->datatables->generate();
	}

	public function add()
	{
		$this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->data["page_title"] = lang("payroll_concepts_add");
        $this->data["types_concepts"] = $this->Payroll_concepts_model->get_not_fixed();
        $this->data["payment_frequencies_concept"] = $this->Payroll_concepts_model->get_frequency_options($this->Payroll_settings->payment_frequency);

        $this->load_view($this->theme . "payroll_concepts/add", $this->data);
	}

	public function save()
	{
		$this->sma->checkPermissions('index', NULL, 'payroll_management');

		$this->form_validation->set_rules("concept_type_id", lang("payroll_concepts_type"), 'trim|required');
		$this->form_validation->set_rules("name", lang("payroll_concepts_name"), 'trim|required');
		$this->form_validation->set_rules("status", lang("payroll_concepts_status"), 'trim|required');

		if ($this->form_validation->run())
		{
			$data = [
				"name"=>$this->input->post("name"),
				"concept_type_id"=>$this->input->post("concept_type_id"),
				"status"=>$this->input->post("status")
			];
			$type_concept_saved = $this->Payroll_concepts_model->insert($data);
			if ($type_concept_saved == TRUE) {
				$this->session->set_flashdata("message", lang("payroll_concepts_saved"));
            	admin_redirect("payroll_concepts");
			} else {
				$this->session->set_flashdata("error", lang("payroll_concepts_not_saved"));
            	admin_redirect("payroll_concepts");
			}
		} else {
			$this->session->set_flashdata("error", validation_errors());
            admin_redirect("payroll_concepts");
		}
	}

	public function edit($id)
	{
		$this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->data["page_title"] = lang("payroll_concepts_edit");

        $this->data["types_concepts"] = $this->Payroll_concepts_model->get_not_fixed();
        $this->data["type_concept_data"] = $this->Payroll_concepts_model->get_by_id($id);
        $this->data["payment_frequencies_concept"] = $this->Payroll_concepts_model->get_frequency_options($this->Payroll_settings->payment_frequency);

        $this->load_view($this->theme . "payroll_concepts/edit", $this->data);
	}

	public function update()
	{
		$this->sma->checkPermissions('index', NULL, 'payroll_management');

		$this->form_validation->set_rules("name", lang("payroll_concepts_name"), 'trim|required');
		$this->form_validation->set_rules("status", lang("payroll_concepts_status"), 'trim|required');

		if ($this->form_validation->run())
		{
			$id = $this->input->post("id");
			$concept_data = $this->Payroll_concepts_model->get_by_id($id);

			$data = [
				"name"=>$this->input->post("name"),
				"status"=>$this->input->post("status"),
			];
			$type_concept_updated = $this->Payroll_concepts_model->update($data, $id);
			if ($type_concept_updated == TRUE) {
				$this->log_user_activity($concept_data, $id);

				$this->session->set_flashdata("message", lang("payroll_concepts_updated"));
            	admin_redirect("payroll_concepts");
			} else {
				$this->session->set_flashdata("error", lang("payroll_concepts_not_updated"));
            	admin_redirect("payroll_concepts");
			}
		} else {
			$this->session->set_flashdata("error", validation_errors());
            admin_redirect("payroll_concepts");
		}
	}

	public function get_ajax_nature_concept()
	{
		$id = $this->input->get("id");
		$concept_type_data = $this->Payroll_concepts_model->get_concept_type($id);

		echo $concept_type_data->type;
	}

	public function log_user_activity($concept_data, $id)
	{
		$changes_detected = 0;
		$description_changes_detected = "";
		$update_concept_data = $this->Payroll_concepts_model->get_by_id($id);
        $description = 'El usuario '. $this->session->userdata('first_name') .' '. $this->session->userdata('last_name') .' hizo lo siguiente cambios en conceptos de nómina: ';

        $diff = array_diff_assoc((array) $concept_data, (array) $update_concept_data);
        if (!empty($diff)) {
            foreach ($diff as $field_name => $field_data) {
                $data_from = $field_data;
                $data_to = $update_concept_data->$field_name;

                $description_changes_detected .= lang($field_name). ' de "'. $data_from .'" a "'. $data_to. '", ';
            }

            $log_user_data = [
                'date'=>date('Y-m-d H:m:i'),
                'type_id'=>EDITION,
                'user_id'=>$this->session->userdata('user_id'),
                'module_name'=>'payroll',
                'table_name'=>'payroll_concepts',
                'record_id'=> $id,
                'description'=>trim($description . $description_changes_detected, ', ')
            ];
            $this->UserActivities_model->insert($log_user_data);
        }
	}
}

/* End of file payroll_concepts.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/controllers/admin/payroll_concepts.php */