<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
    if ($this->session->userdata('detal_post_processing')) { $this->session->unset_userdata('detal_post_processing'); }
?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= admin_form_open_multipart("debit_notes/save", ["id"=>"add_debit_note_form"]); ?>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label for="date"><?= lang("date"); ?></label>
                                            <input class="form-control" type="date" name="date" id="date" value="<?= isset($_POST['date']) ? $_POST['date'] : date('Y-m-d'); ?>" required>
                                        </div>
                                    </div>

                                    <?php
                                        $bl[""] = "";
                                        $bldata = [];
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                            $bldata[$biller->id] = $biller;
                                        }
                                    ?>

                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("biller", "branch_office"); ?>
                                                <select name="biller" class="form-control" id="branch_office" required="required">
                                                    <?php foreach ($billers as $biller): ?>
                                                      <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" <?= (isset($reference_invoice) && $biller->id == $reference_invoice->branch_id) ? "selected": "" ?>><?= $biller->company ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <input type="hidden" name="branch_id" id="branch_id">
                                        </div>
                                    <?php } else { ?>
                                        <div class="col-md-4" style="display: none;">
                                            <div class="form-group">
                                                <?= lang("biller", "branch_office"); ?>
                                                <select name="biller" class="form-control" id="branch_office">
                                                    <?php
                                                        if (isset($bldata[$this->session->userdata('biller_id')])):
                                                            $biller = $bldata[$this->session->userdata('biller_id')];
                                                    ?>
                                                        <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                    <?php
                                                        endif
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="col-md-4">
                                        <?= form_label(lang("credit_note_other_concepts_label_document"), 'document_type'); ?>
                                        <select class="form-control" name="document_type" id="document_type" required="required">
                                            <option value=""><?= lang("select"); ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= form_label(lang("customer"), "customer_id"); ?>
                                            <?= form_input('customer_id', (isset($_POST['customer_id']) ? $_POST['customer_id'] : ""), 'id="customer_id" data-placeholder="'. lang("select") .'" required="required" class="form-control  input-tip" style="width:100%;"'); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= form_label(lang("credit_note_other_concepts_label_invoice_reference"), 'reference_no'); ?>
                                            <?= form_input("reference_no", (isset($_POST['reference_no']) ? $_POST['reference_no'] : ""), 'class="form-control" id="reference_no" placeholder="'. lang("select") .'" required="required"'); ?>
                                            <input type="hidden" name="invoice_reference" id="invoice_reference">
                                            <input type="hidden" name="customer_name" id="customer_name">
                                            <input type="hidden" name="grand_total" id="grand_total">
                                            <input type="hidden" name="dian_code" id="dian_code">
                                            <input type="hidden" name="module" id="module">
                                            <input type="hidden" name="addresses_id" id="addresses_id">
                                            <input type="hidden" name="seller_id" id="seller_id">
                                            <input type="hidden" name="withholdings_data" id="withholdings_data">
                                            <input type="hidden" name="warehouse_id" id="warehouse_id">
                                            <input type="hidden" name="sale_currency" id="sale_currency">
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <?= form_label(lang("credit_note_other_concepts_label_customer_branch"), 'customer_branch'); ?>
                                        <?= form_input("customer_branch", (isset($_POST['customer_branch']) ? $_POST['customer_branch'] : ""), 'class="form-control" id="customer_branch" readonly'); ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <?= form_label(lang("credit_note_other_concepts_label_concept"), 'concept'); ?>
                                        <div class="input-group">
                                            <select class="form-control" name="concept" id="concept">
                                                <option value=""><?= lang("select"); ?></option>
                                                <?php foreach ($debit_note_concepts as $debit_note_concept) { ?>
                                                    <option value="<?= $debit_note_concept->id; ?>" data-concept_tax_rate="<?= $debit_note_concept->concept_tax_rate; ?>" data-concept_dian_code="<?= $debit_note_concept->dian_code; ?>" data-concept_tax_rate_id="<?= $debit_note_concept->concept_tax_rate_id; ?>"><?= $debit_note_concept->description; ?></option>
                                                <?php } ?>
                                            </select>
                                            <span class="input-group-btn">
                                                <button class="btn btn-success" type="button" name="add_concept" id="add_concept" data-toggle="tooltip" data-placement="top" title="Agregar Concepto"><i class="fa fa-plus"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("credit_note_other_concepts_label_concepts"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="credit_note_table" class="table items  table-bordered table-condensed table-hover sortable_table">
                                                    <thead>
                                                        <tr>
                                                            <th width="5%"><?= lang("credit_note_other_concepts_label_delete"); ?></th>
                                                            <th ><?= lang('credit_note_other_concepts_label_concept'); ?></th>
                                                            <th width="10%"><?= lang("credit_note_other_concepts_label_value"); ?></th>
                                                            <th width="10%"><?= lang("credit_note_other_concepts_label_tax"); ?></th>
                                                            <th width="10%"><?= lang("credit_note_other_concepts_label_total"); ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div id="bottom-total" class="well well-sm">
                                            <table class="table table-bordered table-condensed" style="margin-bottom:0;">
                                                <tr class="warning">
                                                    <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                                    <td width="10%"><?= lang('total') ?> <span class="totals_val pull-right" id="total_sum_concept_value">0.00</span></td>
                                                    <td width="10%"><?= lang('grand_total') ?> <span class="totals_val pull-right" id="total_sum_concept_tax">0.00</span></td>
                                                    <td width="9.5%"><?= lang('grand_total') ?> <span class="totals_val pull-right" id="total_sum">0.00</span></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="open_modal_retention_window"> Retención</label>
                                        <input class="form-control text-right" type="text" name="withholdings" id="withholdings" style="padding-right: 10px" readonly>
                                    </div>

                                    <?php if ($Owner || $Admin || $GP['sales-payments']) { ?>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("payment_status", "slpayment_status"); ?>
                                                <?php $pst = array('pending' => lang('credit'), 'partial' => lang('payment_type_cash'));
                                                echo form_dropdown('payment_status', $pst, '', 'class="form-control input-tip" required="required" id="slpayment_status"'); ?>

                                            </div>
                                        </div>
                                    <?php } else {
                                        echo form_hidden('payment_status', 'pending');
                                    } ?>

                                    <div class="col-sm-4" id="slpayment_term_container">
                                        <div class="form-group">
                                            <?= lang("payment_term", "slpayment_term"); ?>
                                            <?= form_input('payment_term', '0', 'class="form-control tip" data-trigger="focus" data-placement="top" title="' . lang('payment_term_tip') . '" id="slpayment_term"'); ?>
                                            <input type="hidden" name="customerpaymentterm" id="customerpaymentterm">
                                        </div>
                                    </div>
                                </div>


                                <div class="row" id="payments" style="display: none;">
                                    <div class="col-md-12">
                                        <div class="well well-sm well_1">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                                            <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="payment">
                                                            <div class="form-group ngc">
                                                                <?= lang("amount", "amount_1"); ?>
                                                                <input name="amount-paid" type="text" id="amount_1"class="pa form-control kb-pad amount only_number"/>
                                                            </div>
                                                            <div class="form-group gc" style="display: none;">
                                                                <?= lang("gift_card_no", "gift_card_no"); ?>
                                                                <input name="gift_card_no" type="text" id="gift_card_no"class="pa form-control kb-pad"/>
                                                                <div id="gc_details"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <div class="form-group">
                                                            <?= lang("paying_by", "paid_by_1"); ?>
                                                            <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                                <?= $this->sma->paid_opts(); ?>
                                                            </select>
                                                        </div>
                                                        <input type="hidden" name="mean_payment_code_fe" id="mean_payment_code_fe">
                                                    </div>

                                                </div>
                                                <div class="clearfix"></div>
                                                <div class="pcc_1" style="display:none;">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <input name="pcc_no" type="text" id="pcc_no_1"
                                                                    class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <input name="pcc_holder" type="text" id="pcc_holder_1"
                                                                    class="form-control"
                                                                    placeholder="<?= lang('cc_holder') ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <select name="pcc_type" id="pcc_type_1" class="form-control pcc_type" placeholder="<?= lang('card_type') ?>">
                                                                    <option value="Visa"><?= lang("Visa"); ?></option>
                                                                    <option value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                    <option value="Amex"><?= lang("Amex"); ?></option>
                                                                    <option value="Discover"><?= lang("Discover"); ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">
                                                                <input name="pcc_month" type="text" id="pcc_month_1"
                                                                    class="form-control" placeholder="<?= lang('month') ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">

                                                                <input name="pcc_year" type="text" id="pcc_year_1"
                                                                    class="form-control" placeholder="<?= lang('year') ?>"/>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <div class="form-group">

                                                                <input name="pcc_ccv" type="text" id="pcc_cvv2_1"
                                                                    class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="pcheque_1" style="display:none;">
                                                    <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                        <input name="cheque_no" type="text" id="cheque_no_1"
                                                            class="form-control cheque_no"/>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <?= lang('payment_note', 'payment_note_1'); ?>
                                                    <textarea name="payment_note" id="payment_note_1"
                                                            class="pa form-control kb-text payment_note"></textarea>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div id="bt">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang("debit_note_label_note", "renote"); ?>
                                                        <?= form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="renote" style="margin-top: 10px; height: 100px;"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <?= lang("staff_note", "reinnote"); ?>
                                                        <?= form_textarea('staff_note', (isset($_POST['staff_note']) ? $_POST['staff_note'] : ""), 'class="form-control" id="reinnote" style="margin-top: 10px; height: 100px;"'); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="fprom-group">
                                            <button class="btn btn-primary" type="button" name="add_debit_note" id="add_debit_note"><?= lang("submit"); ?></button>
                                            <button class="btn btn-danger" type="button" id="reset"><?= lang('reset') ?></button>
                                        </div>
                                    </div>
                                </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="withholding_modal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="rtModalLabel"><?= lang('credit_note_other_concepts_label_withholdings_tax');?></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th class="col-sm-2">Retención</th>
                                    <th class="col-sm-5">Opción</th>
                                    <th class="col-sm-2">Porcentaje</th>
                                    <th class="col-sm-3">Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_fuente" id="rete_fuente" data-type_retention="fuente"> Fuente
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_fuente_option" id="rete_fuente_option" data-type_retention="fuente" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                        <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                        <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_fuente_valor" id="rete_fuente_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_iva" id="rete_iva" data-type_retention="iva"> Iva
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_iva_option" id="rete_iva_option" data-type_retention="iva" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                        <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                        <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_iva_valor" id="rete_iva_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_ica" id="rete_ica" data-type_retention="ica"> Ica
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_ica_option" id="rete_ica_option" data-type_retention="ica" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                        <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                        <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_ica_valor" id="rete_ica_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_otros" id="rete_otros" data-type_retention="otros"> Otros
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_otros_option" id="rete_otros_option" data-type_retention="otros" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                        <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                        <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_otros_valor" id="rete_otros_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td class="text-right">
                                        <label>Total : </label>
                                    </td>
                                    <td class="text-right">
                                        <label id="total_rete_amount"> 0.00 </label>
                                        <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="save_debit_note_withholdings" class="btn btn-primary"><?=lang('update')?></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        _retenciones = [];
        _existing_concept = false;

        $('[data-toggle="tooltip"]').tooltip();

        $(document).on('change', '#branch_office', function() { get_document_type_debit_note(); });
        $(document).on('change', '#customer_id', function() { clean_form(); });
        $(document).on('change', '#reference_no', function() { get_sale_data(); });
        $(document).on('click', '#add_concept', function() { add_concept(); });
        $(document).on('change', '#concept_value', function() { calculate_taxes($(this)); });
        $(document).on('click', '.delete_row', function() { delete_concept_row($(this)); });
        $(document).on('click', '#reset', function() { location.reload(); });
        $(document).on('click', '#add_debit_note', function() { add_debit_note(); });
        $(document).on('change', '#paid_by_1', function () { cargar_codigo_forma_pago_fe(); });
        $(document).on('click', '#withholdings', function() { open_modal_withholdings(); });
        $(document).on('click', '#save_debit_note_withholdings', function() { save_debit_note_withholdings(); });

        $('#customer_id').val('<?= (isset($reference_invoice)) ? $reference_invoice->customer_id : ""; ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: '<?= admin_url("customers/suggestions"); ?>',
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                    };
                },
                results: function (data, page) {
                    if(data.results != null) {
                        return { results: data.results };
                    } else {
                        return { results: [{id: '', text: 'No se encontraron resultados'}]};
                    }
                }
            }
        });

        $('#reference_no').val('<?= (isset($reference_invoice)) ? $reference_invoice->reference_invoice_id : ""; ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    async: false,
                    type: "get",
                    url: '<?= admin_url("debit_notes/get_sale_reference"); ?>',
                    data: {
                        term: $(element).val(),
                        customer_id: $('#customer_id').val(),
                        module: '<?= (isset($reference_invoice)) ? $reference_invoice->module: ""; ?>',
                        is_electronic_document: $('#document_type option:selected').data('is_electronic_document')
                    },
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);

                        $('#reference_no').trigger('change');
                    }
                });
            },
            ajax: {
                url: '<?= admin_url("debit_notes/sale_reference_suggestions"); ?>',
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        customer_id: $('#customer_id').val(),
                        module: $('#document_type option:selected').data('module'),
                        is_electronic_document: $('#document_type option:selected').data('is_electronic_document')
                    };
                },
                results: function (data, page) {
                    if(data.results != null) {
                        return { results: data.results };
                    } else {
                        return { results: [{id: '', text: 'No se encontraron resultados'}]};
                    }
                }
            }
        });

        $(document).on('ifToggled', '.type_retention_check', function(){
            var type_retention = $(this).data('type_retention');

            if ($(this).is(':checked')) {
                 $.ajax({
                    url: site.base_url+"sales/opcionesWithHolding/"+type_retention.toUpperCase()
                }).done(function(data){
                    $('#rete_'+type_retention+'_option').html(data);
                    $('#rete_'+type_retention+'_option').attr('disabled', false).select();
                }).fail(function(data){
                    console.log(data);
                });
            } else {
                $('#rete_'+type_retention+'_option').select2('val', '').attr('disabled', true).select();
                $('#rete_'+type_retention+'_tax').val('');
                $('#rete_'+type_retention+'_valor').val('');

                total_value_withholding_type();
            }
        });

        $(document).on('change', '.retention_type_option', function() {
            var type_retention = $(this).data('type_retention');
            var percentage = $('#rete_'+ type_retention +'_option option:selected').data('percentage');
            var apply = $('#rete_'+ type_retention +'_option option:selected').data('apply');
            var minbase = $('#rete_'+ type_retention +'_option option:selected').data('minbase')

            $('#rete_'+type_retention+'_tax').val(percentage);

            if (apply == 'ST') {
                var total_withholding = 0;
                $('#rete_'+type_retention+'_base').val($('#concept_value').val());

                if ($('#concept_value').val() >= minbase) {
                    total_withholding = ($('#concept_value').val() * percentage) / 100;
                }
            } else if (apply == 'TX') {
                var total_withholding = 0;
                $('#rete_'+type_retention+'_base').val($('#concept_tax').val());

                if ($('#concept_tax').val() >= 0) {
                    total_withholding = ($('#concept_tax').val() * percentage) / 100;
                }
            } else if (apply == 'TO') {
                var total_withholding = 0;
                $('#rete_'+type_retention+'_base').val($('#total_value_concept').val());

                if ($('#total_value_concept').val() >= 0) {
                    total_withholding = ($('#total_value_concept').val() * percentage) / 100;
                }
            }


            $('#rete_'+type_retention+'_valor').val(formatDecimal(parseFloat(total_withholding)));

            total_value_withholding_type();
        });

        $('#branch_office').trigger('change');

        $(document).on('change', '#slpayment_status', function () { show_hide_payment_container(); });
    });

    function get_document_type_debit_note()
    {
        $.ajax({
            url: '<?= admin_url("documents_types/get_document_type_debit_note"); ?>',
            type: 'post',
            dataType: 'json',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash() ?>',
                'branch_id': $('#branch_office').val(),
                'pos': '<?= isset($reference_invoice) ? $reference_invoice->pos : "" ?>'
            },
        })
        .done(function(data) {
            if (data != '') {
                var document_types_options = '';

                $(data).each(function(i, document_type) {
                    document_types_options += '<option value="'+ document_type.document_type_id +'" '+ (i == 0 ? 'selected' : '') +' data-is_electronic_document="'+document_type.is_electronic_document+'" data-module="'+ document_type.module +'">'+ document_type.document_type_prefix +'</option>';

                    if (i == 0) { selected = document_type.document_type_id; }
                });

                $('#document_type').html(document_types_options);
                $('#document_type').select2('val', selected);
            } else {
                document_types_options = '<option value="">No existe tipos de documentos asociados</option>';
            }
        })
        .fail(function(data) { console.log(data.responseText); });
    }

    function clean_form()
    {
        $('#reference_no').select2('val', '');
        $('#reference_no').trigger('change');
    }

    function get_sale_data()
    {
        $.ajax({
            url: '<?= admin_url("debit_notes/get_sale_data"); ?>',
            type: 'POST',
            dataType: 'JSON',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                id: $('#reference_no').val()
            },
        })
        .done(function(data) {
            console.log(data);
            if (data.status) {
                var sale_data = data.sale_data;

                if (sale_data.rete_fuente_id || sale_data.rete_iva_id || sale_data.rete_ica_id || sale_data.rete_other_id) {
                    invoice_withholdings = {
                        'gtotal' : $('#gtotal').text(),
                        'id_rete_fuente' : sale_data.rete_fuente_id,
                        'id_rete_iva' : sale_data.rete_iva_id,
                        'id_rete_ica' : sale_data.rete_ica_id,
                        'id_rete_otros' : sale_data.rete_other_id,
                    };

                    localStorage.setItem('invoice_withholdings', JSON.stringify(invoice_withholdings));
                }

                $('#customer_branch').val(sale_data.customer_branch);
                $('#branch_id').val(sale_data.branch_id);
                $('#invoice_reference').val(sale_data.invoice_reference);
                $('#customer_name').val(sale_data.customer_name);
                $('#grand_total').val(sale_data.grand_total);
                $('#module').val(sale_data.module);
                $('#addresses_id').val(sale_data.addresses_id);
                $('#seller_id').val(sale_data.seller_id);
                $('#date').attr('min', sale_data.date);
                $('#warehouse_id').val(sale_data.warehouse_id);
                $('#sale_currency').val(sale_data.sale_currency);
            } else {
                $('#customer_branch').val('');
                $('#branch_id').val('');
                $('#invoice_reference').val('');
                $('#customer_name').val('');
                $('#grand_total').val('');
                $('#module').val('');
                $('#addresses_id').val('');
                $('#seller_id').val('');
                $('#date').attr('min', '');
                $('#warehouse_id').val('');
                $('#sale_currency').val('');
            }

            cancel_withholding_calculation();
            delete_concept_row();
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function add_concept()
    {
        var concept = $('#concept').val();
        var reference = $('#reference_no').val();
        var description = $('#concept option:selected').text();
        var tax_rate_id = $('#concept option:selected').data('concept_tax_rate_id');
        var tax_rate = $('#concept option:selected').data('concept_tax_rate');
        var concept_dian_code = $('#concept option:selected').data('concept_dian_code');

        if (validate_add_concept(reference, concept)) {
            var table_row = '<tr>'+
                                '<td class="text-center">'+
                                    '<i class="fa fa-trash fa-2x text-danger delete_row" data-toggle="tooltip" data-placement="top" title="Eliminar" style="cursor: pointer;"></i>'+
                                '</td>'+
                                '<td>'+ description +'</td>'+
                                '<td>'+
                                    '<input class="form-control text-right concept_value" type="number" name="concept_value" id="concept_value">'+
                                    '<input type="hidden" name="concept_id" id="concept_id" value="'+ concept +'">'+
                                    '<input type="hidden" name="concept_name" id="concept_name" value="'+ description +'">'+
                                    '<input type="hidden" name="concept_tax_rate_id" id="concept_tax_rate_id" value="'+ tax_rate_id +'">'+
                                    '<input type="hidden" name="concept_tax_rate" id="concept_tax_rate" value="'+ tax_rate +'">'+
                                    '<input type="hidden" name="concept_dian_code" id="concept_dian_code" value="'+ concept_dian_code +'">'+
                                '</td>'+
                                '<td><input class="form-control text-right concept_tax" type="number" name="concept_tax" id="concept_tax" readonly></td>'+
                                '<td><input class="form-control text-right total_value_concept" type="number" name="total_value_concept" id="total_value_concept" readonly></td>'+
                            '</tr>';

            $('#credit_note_table tbody').append(table_row);
            $('[data-toggle="tooltip"]').tooltip();

            // _allow_withholding_calculation = concept_withholdings;
        }
    }

    function validate_add_concept(reference, concept)
    {
        if (reference == "") {
            Command: toastr.error('Debe seleccionar la factura que se quiere afectar', '<?= lang("error"); ?>');
            return false;
        }

        if (concept == "") {
            Command: toastr.error('Debe seleccionar el concepto a agregar', '<?= lang("error"); ?>');
            return false;
        }

        if (_existing_concept == true) {
            Command: toastr.error('El concepto ya se encuentra agregado', '<?= lang("error"); ?>');
            return false;
        }

        _existing_concept = true;
        return true;
    }

    function calculate_taxes(concept_value_input)
    {
        var tax_calculated_from_concept_note = calculate_tax_concept(concept_value_input);
        concept_value_input.parents('tr').find('#concept_tax').val(tax_calculated_from_concept_note);

        var total_calculated_from_concept_note = calculate_total_value_concept(concept_value_input);
        concept_value_input.parents('tr').find('#total_value_concept').val(total_calculated_from_concept_note);

        total_sums();

        recalculate_withholdings();
    }

    function calculate_tax_concept(concept_value_input)
    {
        var concept_tax_rate = $('#concept option:selected').data('concept_tax_rate');
        var concept_value = concept_value_input.val();
        var calculate_tax_note_concept = 0;

        if (concept_tax_rate > 0) {
            calculate_tax_note_concept = (concept_value * concept_tax_rate) / 100;
        }

        return formatDecimalNoRound(calculate_tax_note_concept);
    }

    function calculate_total_value_concept(concept_value_input)
    {
        var concept_value = concept_value_input.val();
        var concept_tax = concept_value_input.parents('tr').find('#concept_tax').val();

        var total_value_concept = parseFloat(concept_value) + parseFloat(concept_tax);

        return total_value_concept;
    }

    function total_sums()
    {
        var total_sum_concept_value = total_sum_concept_tax = total_sum_withholdings = total_sum = 0;
        $('.concept_value').each(function() {
            total_sum_concept_value += parseFloat($(this).val());
        });
        $('#total_sum_concept_value').text(total_sum_concept_value);

        $('.concept_tax').each(function() {
            total_sum_concept_tax += parseFloat($(this).val());
        });
        $('#total_sum_concept_tax').text(total_sum_concept_tax);

        $('.total_value_concept').each(function() {
            total_sum += parseFloat($(this).val());
        });
        $('#total_sum').text(total_sum);
    }

    function open_modal_withholdings()
    {
        if ($('#concept').val() == '') {
            Command: toastr.error('No se ha seleccionado el concepto.', '¡Error!', {onHidden: function() { $('#concept').select2('focus'); }});
        } else if ($('#concept_value').val() == null) {
            Command: toastr.error('No se ha ingresado valor al concepto seleccionado.', '¡Error!', {onHidden: function() { $('#concept_value').focus(); }});
        } else {
            $('#withholding_modal').modal('show');
        }
    }

    function add_debit_note()
    {
        if (validate_form_inputs()) {
            $('#add_debit_note_form').submit();
        }
    }

    function validate_form_inputs()
    {
        if ($('#document_type').val() == '') {
            header_alert('Error', 'El campo Número es Obligatorio.');
            $('#document_type').select2('focus');
            return false;
        }

        var total_sum = $('#total_sum').text();
        var maximum_total_allowed = $('#grand_total').val();
        if (parseFloat(total_sum) == 0) {
            Command: toastr.error('No se puede crear La Nota Débito. Por favor diligencie el formulario.', '<?= lang("error"); ?>');
            return false;
        }

        return true;
    }

    function delete_concept_row(row_control)
    {
        if (row_control === undefined) {
            $('#credit_note_table tbody tr').remove();
        } else {
            row_control.parents('tr').remove();
        }

        cancel_withholding_calculation();

        $('#total_sum_concept_value').text('0.00');
        $('#total_sum_concept_tax').text('0.00');
        $('#total_sum').text('0.00');
        $('#withholdings').val(0);

        _existing_concept = false;
    }

    function recalculate_withholdings()
    {
        re_retenciones = JSON.parse(localStorage.getItem('invoice_withholdings'));

        if (re_retenciones != null) {
            if (re_retenciones.id_rete_fuente > 0 && re_retenciones.id_rete_fuente != null) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').iCheck('check');
                }
            }

            if (re_retenciones.id_rete_ica > 0 && re_retenciones.id_rete_ica != null) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').iCheck('check');
                }
            }

            if (re_retenciones.id_rete_iva > 0 && re_retenciones.id_rete_iva != null) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').iCheck('check');
                }
            }

            if (re_retenciones.id_rete_otros > 0 && re_retenciones.id_rete_otros != null) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').iCheck('check');
                }
            }

            setTimeout(function() {

                $.each($('#rete_fuente_option option'), function(index, value){
                    if(re_retenciones.id_rete_fuente != '' && value.value == re_retenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', value.value).trigger('change');
                    }
                });


                $.each($('#rete_iva_option option'), function(index, value){
                    if(re_retenciones.id_rete_iva != '' && value.value == re_retenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_ica_option option'), function(index, value){
                    if(re_retenciones.id_rete_ica != '' && value.value == re_retenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_otros_option option'), function(index, value){
                    if(re_retenciones.id_rete_otros != '' && value.value == re_retenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', value.value).trigger('change');
                    }
                });

                save_debit_note_withholdings();
            }, 2000);
        }
    }

    function total_value_withholding_type()
    {
        var total_sum_withholdings = 0;

        $('.total_value_withholding_type').each(function () {
            if ($(this).val() != '') {
                total_sum_withholdings += parseFloat($(this).val());
            }
        });

        $('#total_rete_amount').text(total_sum_withholdings);

        _retenciones = {
            'retention_id_fuente': $('#rete_fuente_option').val(),
            'retention_percentage_fuente': $('#rete_fuente_option option:selected').data('percentage'),
            'retention_total_fuente': $('#rete_fuente_valor').val(),
            'retention_base_fuente': $('#rete_fuente_base').val(),
            'retention_account_fuente': $('#rete_fuente_option option:selected').data('account'),

            'retention_id_iva': $('#rete_iva_option').val(),
            'retention_percentage_iva': $('#rete_iva_option option:selected').data('percentage'),
            'retention_total_iva': $('#rete_iva_valor').val(),
            'retention_base_iva': $('#rete_iva_base').val(),
            'retention_account_iva': $('#rete_iva_option option:selected').data('account'),

            'retention_id_ica': $('#rete_ica_option').val(),
            'retention_percentage_ica': $('#rete_ica_option option:selected').data('percentage'),
            'retention_total_ica': $('#rete_ica_valor').val(),
            'retention_base_ica': $('#rete_ica_base').val(),
            'retention_account_ica': $('#rete_ica_option option:selected').data('account'),

            'retention_id_other': $('#rete_otros_option').val(),
            'retention_percentage_other': $('#rete_otros_option option:selected').data('percentage'),
            'retention_total_other': $('#rete_otros_valor').val(),
            'retention_base_other': $('#rete_otros_base').val(),
            'retention_account_other': $('#rete_otros_option option:selected').data('account'),
        };

        $('#withholdings_data').val(JSON.stringify(_retenciones));
    }

    function save_debit_note_withholdings()
    {
        var total_withholding = $('#total_rete_amount').text();

        $('#withholdings').val(formatDecimalNoRound(total_withholding, 4));

        $('#withholding_modal').modal('hide');
    }

    function cancel_withholding_calculation()
    {
        $('.type_retention_check').iCheck('uncheck');

        total_value_withholding_type();
    }

    function show_hide_payment_container()
    {
        var payment_status = $('#slpayment_status').val();

        if (payment_status == 'pending' || payment_status == 'due') {
            $('#mean_payment_code_fe').val('');
            $('#payments').slideUp();
            $('#amount_1').val(0);
            $('#slpayment_term_container').fadeIn();
        } else {
            $('#payments').slideDown();
            $('#slpayment_term_container').fadeOut();

            get_payment_reference_no();

            cargar_codigo_forma_pago_fe();

            total_value_debit_note();
        }
    }

    function get_payment_reference_no()
    {
        var biller_id = $('#branch_office').val();
        var module_debit_note = $('#document_type option:selected').data('module');

        if (module_debit_note == 27) {
            modulo = 29;
        } else if (module_debit_note == 26) {
            modulo = 28;
        }

        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/") ?>'+ modulo +'/'+ biller_id,
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#payment_reference_no').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }

            if (response.status == 0) {

            }

            $('#payment_reference_no').trigger('change');
        });
    }

    function cargar_codigo_forma_pago_fe()
    {
        var codigo_fe_forma_pago = $('#paid_by_1 option:selected').data('code_fe');

        $('#mean_payment_code_fe').val(codigo_fe_forma_pago);
    }

    function total_value_debit_note()
    {
        var total_value_concept = $('#total_value_concept').val();
        var total_withholding = $('#withholdings').val();

        $('#amount_1').val(parseFloat(total_value_concept) - parseFloat(total_withholding));
    }
</script>
