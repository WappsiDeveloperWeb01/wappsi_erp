$(document).ready(function(){

});

$('#factura_electronica').on('change', function(){
	if ($(this).val() == 1) {
        if (show_hide_electronic_billing_fields()) {
            validate_assigned_technology_provider();
        }
	}
});

$('#sales_consecutive').on('keyup', function(){
	var val = $(this).val();

	$('#inicio_resolucion').val(val).prop('max', val);
	$('#fin_resolucion').prop('min', val);

});

$('#emision_resolucion').on('change', function(){
	var inicio = $(this).val();
	var fin = $('#vencimiento_resolucion').val();

	$('#vencimiento_resolucion').prop('min', inicio);

});

$('#vencimiento_resolucion').on('change', function(){
	var inicio = $('#emision_resolucion').val();
	var fin = $(this).val();

    var fecha = new Date(fin);
    var ano = fecha.getFullYear();

    if (ano > 1999 && ano < 2099) {
	   $('#emision_resolucion').prop('max', fin);
       // $('#emision_resolucion').trigger('change');
    }


});

function validate_assigned_technology_provider()
{
    $.ajax({
        url: site.base_url+"sales/validate_assigned_technology_provider",
        dataType: 'JSON',
    })
    .done(function(response) {
        if (response == false) {
            if ($('#electronic_billing').val() == 0) {
                Command: toastr.error('Módulo Facturación Electrónica deshabilitado', 'Error');
                $('#myModal').modal('hide');
            } else {
                Command: toastr.error('El Proveedor tecnológico no se ha asignado', 'Error');
                localStorage.setItem('locked_for_provider', 1);
                verify_locked_submit();
            }
        } else {
            if (localStorage.getItem('locked_for_provider')) {
                localStorage.removeItem('locked_for_provider');
            }
            verify_locked_submit();
        }
    })
    .fail(function(response) {
        console.log(response.responseText);
    });
}

function verify_locked_submit(){
    if (localStorage.getItem('locked_for_provider')) {
        $('.submit').attr('disabled', true);
    } else {
        $('.submit').attr('disabled', false);
    }
}