<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
  <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
      <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_seller'); ?></h4>
    </div>

    <?= admin_form_open_multipart("seller/add", array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add-seller-form')); ?>
      <div class="modal-body">
        <p><?= lang('enter_info'); ?></p>
        <div class="row">
          <div class="form-group col-md-6">
            <?= lang("id_document_type", "document_type"); ?>
            <?php
              $document_type_options[''] = lang('select');
              foreach ($id_document_types as $document_type)
              {
                if ($document_type->id != 6)
                {
                  $document_type_options[$document_type->id] = lang($document_type->nombre);
                }
              }
            ?>
            <?= form_dropdown('document_type', $document_type_options, '', 'id="document_type" class="form-control select" style="width:100%;" required="required"'); ?>
          </div>

          <div class="form-group col-md-6">
            <?= lang("vat_no", "document_number"); ?>
            <?= form_input('document_number', '', 'id="document_number" class="form-control" required="required"'); ?>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <?= lang('first_name_seller', 'first_name') ?>
            <?= form_input('first_name', '', 'id="first_name" class="form-control" required="required"'); ?>
          </div>

          <div class="form-group col-md-6">
            <?= lang('second_name_seller', 'second_name') ?>
            <?= form_input('second_name', '', 'id="second_name" class="form-control"'); ?>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <?= lang('first_lastname_seller', 'first_lastname') ?>
            <?= form_input('first_lastname', '', 'id="first_lastname" class="form-control" required="required"'); ?>
          </div>

          <div class="form-group col-md-6">
            <?= lang('second_lastname_seller', 'second_lastname') ?>
            <?= form_input('second_lastname', '', 'id="second_lastname" class="form-control"'); ?>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <?= lang("email_address", "email"); ?>
            <input type="email" name="email" class="form-control" required="required" id="email"/>
          </div>

          <div class="form-group col-md-6">
            <?= lang("phone", "phone"); ?>
            <input type="tel" name="phone" class="form-control" required="required" id="phone"/>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <?= lang("address", "address"); ?>
            <?php echo form_input('address', '', 'class="form-control" id="address" required="required"'); ?>
          </div>

          <div class="form-group col-md-6">
            <?= lang("postal_code", "postal_code"); ?>
            <?php echo form_input('postal_code', '', 'class="form-control postal_code" id="postal_code"'); ?>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <?= lang("country", "country"); ?>
            <select class="form-control select" name="country" id="country" required>
              <option value="">Seleccione...</option>
              <?php foreach ($countries as $row => $country): ?>
                <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>"><?= $country->NOMBRE ?></option>
              <?php endforeach ?>
            </select>
          </div>

          <div class="form-group col-md-6">
            <?= lang("state", "state"); ?>
            <select class="form-control select" name="state" id="state" required>
              <option value="">Seleccione...</option>
            </select>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <?= lang("city", "city"); ?>
            <select class="form-control select" name="city" id="city" required>
              <option value="">Seleccione...</option>
            </select>
            <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
          </div>

          <div class="form-group col-md-6">
            <?= lang('biller', 'billers'); ?>
            <select class="form-control select" name="billers[]" id="billers" multiple="multiple" required="required">
              <option vaue="">Seleccione...</option>
              <?php foreach ($billers as $biller) : ?>
                <option value="<?= $biller->id ?>"><?= $biller->name_biller; ?></option>
              <?php endforeach ?>
            </select>
          </div>
        </div>

        <div class="row">
          <div class="form-group col-md-6">
            <?= lang('customer_branch', 'addresses'); ?>
            <?= form_input('addresses[]', '', 'id="addresses" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="max-width:325px;"'); ?>
          </div>
        </div>
      </div>
      <div class="modal-footer">
          <?= form_submit('add_seller', lang('add_seller'), 'class="btn btn-primary" id="add_seller"'); ?>
      </div>
      </div>
    <?= form_close(); ?>
</div>

<script type="text/javascript">
    $(document).ready(function (e) {
      $('select.select').select2({minimumResultsForSearch: 7});

      $('#add-seller-form').bootstrapValidator({
        feedbackIcons: {
            valid: 'fa fa-check',
            invalid: 'fa fa-times',
            validating: 'fa fa-refresh'
        }, excluded: [':disabled']
      });

      fields = $('.modal-content').find('.form-control');
      $.each(fields, function () {
          var id = $(this).attr('id');
          var iname = $(this).attr('name');
          var iid = '#' + id;
          if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
              $("label[for='" + id + "']").append(' *');
              $(document).on('change', iid, function () {
                  $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
              });
          }
      });

        $('#state').on('change', function(){
          dpto = $('#state option:selected').data('code');

          // $('.postal_code').val($('.postal_code').val()+dpto);

          $.ajax({
            url:"<?= admin_url() ?>customers/get_cities/"+dpto,
          }).done(function(data){
            $('#city').html(data);
            console.log(data);
          }).fail(function(data){
            console.log(data);
          });

        });

        $('#country').on('change', function(){
          dpto = $('#country option:selected').data('code');

          // $('.postal_code').val(dpto);

          $.ajax({
            url:"<?= admin_url() ?>customers/get_states/"+dpto,
          }).done(function(data){
            $('#state').html(data);
            console.log(data);
          }).fail(function(data){
            console.log(data);
          });

        });

        $('#city').on('change', function(){
          code = $('#city option:selected').data('code');
          $('.postal_code').val(code);
          $('#city_code').val(code);
        });
        
        nsCustomer();
        
    });

    $('#document_number').on('keyup', function(){
        var vat_no = $(this).val();
        $.ajax({
          url:'<?= admin_url("seller/validate_vat_no/") ?>'+vat_no
        }).done(function(data){
          if (data == "true") {
            Command: toastr.error('Ya existe el vendedor', '¡Error!', {onHidden : function(){}})
            $('#add_seller').prop('disabled', true);
          } else {
            $('#add_seller').prop('disabled', false);
          }
        });
    });
    
    
      
      function nsCustomer() {
        $('#addresses').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions_addresses",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            },
            multiple: true
        });
      }
</script>
