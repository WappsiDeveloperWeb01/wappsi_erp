$(document).ready(function (e) {
    if (localStorage.getItem('oslsale_status')) {
        localStorage.removeItem('oslsale_status');
    }
    if (localStorage.getItem('amount_ordered')) {
        localStorage.removeItem('amount_ordered');
    }
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }

    $('#oslcustomer').on('change', function(){
        oslitems = JSON.parse(localStorage.getItem('oslitems'));
        if (oslitems && Object.keys(oslitems).length > 0) {
            current_customer = $(this).val();
            prev_customer = localStorage.getItem('oslcustomer');
            if (prev_customer != current_customer) {
                bootbox.confirm({
                    message: "Al cambiar el cliente, los precios se actualizarán de acuerdo con las condiciones del mismo. Que desea hacer ?",
                    buttons: {
                        confirm: {
                            label: 'Cambiar cliente',
                            className: 'btn-success send-submit-sale'
                        },
                        cancel: {
                            label: 'Cancelar',
                            className: 'btn-danger'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $.each(oslitems, function(id, arr){
                                wappsiPreciosUnidad(oslitems[id].row.id,id,oslitems[id].row.qty);
                            });
                            localStorage.setItem('oslcustomer', current_customer);
                            customer_special_discount();
                        } else {
                            $('#oslcustomer').select2('val', prev_customer);
                        }
                    }
                });
            }
        } else {
            localStorage.setItem('oslcustomer', $(this).val());
        }
    });

    var $customer = $('#oslcustomer');
    $("#form_edit_order_sale").validate({
          ignore: []
      });
    $("#form_add_order_sale").validate({
          ignore: []
      });

    if (oslcustomer = localStorage.getItem('oslcustomer')) {
        $customer.val(oslcustomer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        $('#oslcustomer').trigger('change');

        if (oslcustomerbranch = localStorage.getItem('oslcustomerbranch')) {
            setTimeout(function() {
                // console.log('cb '+oslcustomerbranch);
                $('#oslcustomerbranch').select2('val', oslcustomerbranch).trigger('change');

            }, 2000);
        }

    } else {
        nsCustomer();
    }
    if (osldiscount = localStorage.getItem('osldiscount')) {
        $('#osldiscount').val(osldiscount);
    }
    if (oslseller = localStorage.getItem('oslseller')) {
        $('#oslseller').select2('val', oslseller);
    }
    $('#osltax2').change(function (e) {
        localStorage.setItem('osltax2', $(this).val());
        $('#osltax2').val($(this).val());
    });
    $('#oslseller').change(function (e) {
        localStorage.setItem('oslseller', $(this).val());
    });
    $('#oslcustomerbranch').change(function (e) {
        localStorage.setItem('oslcustomerbranch', $(this).val());
        set_customer_payment();
        setTimeout(function() {
            $('#day_delivery_time').trigger('change');
        }, 1300);
    });
    if (osltax2 = localStorage.getItem('osltax2')) {
        $('#osltax2').select2("val", osltax2);
    }
    $('#oslsale_status').change(function (e) {
        localStorage.setItem('oslsale_status', $(this).val());
    });
    if (oslsale_status = localStorage.getItem('oslsale_status')) {
        $('#oslsale_status').select2("val", oslsale_status);
    }
    $('#document_type_id').change(function (e) {
        localStorage.setItem('document_type_id', $(this).val());
    });
    if (document_type_id = localStorage.getItem('document_type_id')) {
        $('#document_type_id').select2("val", document_type_id);
    }
    $('#oslpayment_status').change(function (e) {
        var ps = $(this).val();
        localStorage.setItem('oslpayment_status', ps);
        if (ps == 'partial' || ps == 'paid') {
            if(ps == 'paid') {
                $('#amount_1').val(formatDecimal(parseFloat(((total + invoice_tax) - order_discount) + shipping)));
            }
            $('#payments').oslideDown();
            $('#pcc_no_1').focus();
        } else {
            $('#payments').slideUp();
        }
    });
    if (oslpayment_status = localStorage.getItem('oslpayment_status')) {
        $('#oslpayment_status').select2("val", oslpayment_status);
        var ps = oslpayment_status;
        if (ps == 'partial' || ps == 'paid') {
            $('#payments').oslideDown();
            $('#pcc_no_1').focus();
        } else {
            $('#payments').slideUp();
        }
    }
    $(document).on('change', '.paid_by', function () {
        var p_val = $(this).val();
        localStorage.setItem('paid_by', p_val);
        $('#rpaidby').val(p_val);
        if (p_val == 'cash' ||  p_val == 'other') {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').show();
            $('#payment_note_1').focus();
        } else if (p_val == 'CC') {
            $('.pcheque_1').hide();
            $('.pcash_1').hide();
            $('.pcc_1').show();
            $('#pcc_no_1').focus();
        } else if (p_val == 'Cheque') {
            $('.pcc_1').hide();
            $('.pcash_1').hide();
            $('.pcheque_1').show();
            $('#cheque_no_1').focus();
        } else {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').hide();
        }
        if (p_val == 'gift_card') {
            $('.gc').show();
            $('.ngc').hide();
            $('#gift_card_no').focus();
        } else {
            $('.ngc').show();
            $('.gc').hide();
            $('#gc_details').html('');
        }
    });
    if (paid_by = localStorage.getItem('paid_by')) {
        var p_val = paid_by;
        $('.paid_by').select2("val", paid_by);
        $('#rpaidby').val(p_val);
        if (p_val == 'cash' ||  p_val == 'other') {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').show();
            $('#payment_note_1').focus();
        } else if (p_val == 'CC') {
            $('.pcheque_1').hide();
            $('.pcash_1').hide();
            $('.pcc_1').show();
            $('#pcc_no_1').focus();
        } else if (p_val == 'Cheque') {
            $('.pcc_1').hide();
            $('.pcash_1').hide();
            $('.pcheque_1').show();
            $('#cheque_no_1').focus();
        } else {
            $('.pcheque_1').hide();
            $('.pcc_1').hide();
            $('.pcash_1').hide();
        }
        if (p_val == 'gift_card') {
            $('.gc').show();
            $('.ngc').hide();
            $('#gift_card_no').focus();
        } else {
            $('.ngc').show();
            $('.gc').hide();
            $('#gc_details').html('');
        }
    }
    if (gift_card_no = localStorage.getItem('gift_card_no')) {
        $('#gift_card_no').val(gift_card_no);
    }
    $('#gift_card_no').change(function (e) {
        localStorage.setItem('gift_card_no', $(this).val());
    });
    if (amount_1 = localStorage.getItem('amount_1')) {
        $('#amount_1').val(amount_1);
    }
    $('#amount_1').change(function (e) {
        localStorage.setItem('amount_1', $(this).val());
    });
    if (paid_by_1 = localStorage.getItem('paid_by_1')) {
        $('#paid_by_1').select2('val', paid_by);
    }
    $('#paid_by_1').change(function (e) {
        localStorage.setItem('paid_by_1', $(this).val());
    });
    if (pcc_holder_1 = localStorage.getItem('pcc_holder_1')) {
        $('#pcc_holder_1').val(pcc_holder_1);
    }
    $('#pcc_holder_1').change(function (e) {
        localStorage.setItem('pcc_holder_1', $(this).val());
    });
    if (pcc_type_1 = localStorage.getItem('pcc_type_1')) {
        $('#pcc_type_1').select2("val", pcc_type_1);
    }
    $('#pcc_type_1').change(function (e) {
        localStorage.setItem('pcc_type_1', $(this).val());
    });
    if (pcc_month_1 = localStorage.getItem('pcc_month_1')) {
        $('#pcc_month_1').val( pcc_month_1);
    }
    $('#pcc_month_1').change(function (e) {
        localStorage.setItem('pcc_month_1', $(this).val());
    });
    if (pcc_year_1 = localStorage.getItem('pcc_year_1')) {
        $('#pcc_year_1').val(pcc_year_1);
    }
    $('#pcc_year_1').change(function (e) {
        localStorage.setItem('pcc_year_1', $(this).val());
    });
    if (pcc_no_1 = localStorage.getItem('pcc_no_1')) {
        $('#pcc_no_1').val(pcc_no_1);
    }
    $('#pcc_no_1').change(function (e) {
        var pcc_no = $(this).val();
        localStorage.setItem('pcc_no_1', pcc_no);
        var CardType = null;
        var ccn1 = pcc_no.charAt(0);
        if(ccn1 == 4)
            CardType = 'Visa';
        else if(ccn1 == 5)
            CardType = 'MasterCard';
        else if(ccn1 == 3)
            CardType = 'Amex';
        else if(ccn1 == 6)
            CardType = 'Discover';
        else
            CardType = 'Visa';
        $('#pcc_type_1').select2("val", CardType);
    });
    if (cheque_no_1 = localStorage.getItem('cheque_no_1')) {
        $('#cheque_no_1').val(cheque_no_1);
    }
    $('#cheque_no_1').change(function (e) {
        localStorage.setItem('cheque_no_1', $(this).val());
    });
    if (payment_note_1 = localStorage.getItem('payment_note_1')) {
        $('#payment_note_1').redactor('set', payment_note_1);
    }
    $('#payment_note_1').redactor('destroy');
    $('#payment_note_1').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('payment_note_1', v);
        }
    });
    var old_payment_term;
    $('#oslpayment_term').focus(function () {
        old_payment_term = $(this).val();
    }).change(function (e) {
        var new_payment_term = $(this).val() ? parseFloat($(this).val()) : 0;
        if (!is_numeric($(this).val())) {
            $(this).val(old_payment_term);
            bootbox.alert(lang.unexpected_value);
            return;
        } else {
            localStorage.setItem('oslpayment_term', new_payment_term);
            $('#oslpayment_term').val(new_payment_term);
        }
    });
    if (oslpayment_term = localStorage.getItem('oslpayment_term')) {
        $('#oslpayment_term').val(oslpayment_term);
    }
    var old_shipping;
    $('#oslshipping').focus(function () {
        old_shipping = $(this).val();
    }).change(function () {
        var oslsh = $(this).val() ? $(this).val() : 0;
        if (!is_numeric(oslsh)) {
            $(this).val(old_shipping);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        shipping = parseFloat(oslsh);
        localStorage.setItem('oslshipping', shipping);
        var gtotal = ((total + invoice_tax) - order_discount) + shipping;
        $('#gtotal').text(formatMoney(gtotal));
        $('#tship').text(formatMoney(shipping));
    });
    if (oslshipping = localStorage.getItem('oslshipping')) {
        shipping = parseFloat(oslshipping);
        $('#oslshipping').val(shipping);
    } else {
        shipping = 0;
    }
    $('#add_sale, #edit_sale').attr('disabled', true);
    $(document).on('change', '.rserial', function () {
        var item_id = $(this).closest('tr').attr('data-item-id');
        oslitems[item_id].row.serial = $(this).val();
        localStorage.setItem('oslitems', JSON.stringify(oslitems));
    });
    // If there is any item in localStorage
    if (localStorage.getItem('oslitems')) {
        loadItems();
    }
    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('oslitems')) {
                    localStorage.removeItem('oslitems');
                }
                if (localStorage.getItem('osldiscount')) {
                    localStorage.removeItem('osldiscount');
                }
                if (localStorage.getItem('osltax2')) {
                    localStorage.removeItem('osltax2');
                }
                if (localStorage.getItem('oslshipping')) {
                    localStorage.removeItem('oslshipping');
                }
                if (localStorage.getItem('oslref')) {
                    localStorage.removeItem('oslref');
                }
                if (localStorage.getItem('oslwarehouse')) {
                    localStorage.removeItem('oslwarehouse');
                }
                if (localStorage.getItem('oslnote')) {
                    localStorage.removeItem('oslnote');
                }
                if (localStorage.getItem('oslinnote')) {
                    localStorage.removeItem('oslinnote');
                }
                if (localStorage.getItem('oslcustomer')) {
                    localStorage.removeItem('oslcustomer');
                }
                if (localStorage.getItem('oslcurrency')) {
                    localStorage.removeItem('oslcurrency');
                }
                if (localStorage.getItem('osldate')) {
                    localStorage.removeItem('osldate');
                }
                if (localStorage.getItem('oslstatus')) {
                    localStorage.removeItem('oslstatus');
                }
                if (localStorage.getItem('oslbiller')) {
                    localStorage.removeItem('oslbiller');
                }
                if (localStorage.getItem('gift_card_no')) {
                    localStorage.removeItem('gift_card_no');
                }
                if (localStorage.getItem('oslkeylog')) {
                    localStorage.removeItem('oslkeylog');
                }
                if (localStorage.getItem('oslcustomerbranch')) {
                    localStorage.removeItem('oslcustomerbranch');
                }
                if (localStorage.getItem('oslseller')) {
                    localStorage.removeItem('oslseller');
                }
                if (localStorage.getItem('oslcustomerspecialdiscount')) {
                    localStorage.removeItem('oslcustomerspecialdiscount');
                }
                if (localStorage.getItem('paid_by')) {
                    localStorage.removeItem('paid_by');
                }

                $('#modal-loading').show();
                location.reload();
            }
        });
    });
    // save and load the fields in and/or from localStorage
    $('#oslref').change(function (e) {
        localStorage.setItem('oslref', $(this).val());
    });
    if (oslref = localStorage.getItem('oslref')) {
        $('#oslref').val(oslref);
    }
    $('#oslwarehouse').change(function (e) {
        localStorage.setItem('oslwarehouse', $(this).val());
        updateQuantityItems($(this).val());
    });
    if (oslwarehouse = localStorage.getItem('oslwarehouse')) {
        $('#oslwarehouse').select2("val", oslwarehouse);
    }
    $('#oslnote').redactor('destroy');
    $('#oslnote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('oslnote', v);
        }
    });
    if (oslnote = localStorage.getItem('oslnote')) {
        $('#oslnote').redactor('set', oslnote);
    }
    $('#oslinnote').redactor('destroy');
    $('#oslinnote').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function (e) {
            var v = this.get();
            localStorage.setItem('oslinnote', v);
        }
    });
    if (oslinnote = localStorage.getItem('oslinnote')) {
        $('#oslinnote').redactor('set', oslinnote);
    }
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });
    if (site.settings.tax2 != 0) {
        $('#osltax2').change(function () {
            localStorage.setItem('osltax2', $(this).val());
            loadItems();
            return;
        });
    }
    var old_osldiscount;
    $('#osldiscount').focus(function () {
        old_osldiscount = $(this).val();
    }).change(function () {
        var new_discount = $(this).val() ? $(this).val() : '0';
        if (is_valid_discount(new_discount)) {
            localStorage.removeItem('osldiscount');
            localStorage.setItem('osldiscount', new_discount);
            loadItems();
            return;
        } else {
            $(this).val(old_osldiscount);
            bootbox.alert(lang.unexpected_value);
            return;
        }
    });
    $(document).on('click', '.osldel', function () {
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        delete oslitems[item_id];
        row.remove();
        if(oslitems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('oslitems', JSON.stringify(oslitems));
            loadItems();
            return;
        }
    });
    $(document).on('change', '#document_type_id', function(){
        validate_key_log();
        loadItems();
    });
    $(document).on('click', '.edit', function () {
        var trmrate = 1;
        var key_log = localStorage.getItem('oslkeylog');
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $(this).closest('tr');
        var row_id = row.attr('id');
        $('#row_id').val(row_id);
        // console.log('TR '+row_id);
        item_id = row.attr('data-item-id');
        item = oslitems[item_id];        var qty = row.children().children('.rquantity').val(),
        product_option = row.children().children('.roption').val(),
        unit_price = formatDecimal(row.children().children('.ruprice').val()),
        discount = row.children().children('.rdiscount').val();

        // seccion para actualizar el precio de producto en el modal de editar producto, si el producto esta basado en peso de joyeria
        if (site.settings.handle_jewerly_products == "1") {
            if (item.row.based_on_gram_value == "1" || item.row.based_on_gram_value == '2' ) {
                unit_price = item.row.current_price_gram;
            }
        }
        // seccion para actualizar el precio de producto en el modal de editar producto, si el producto esta basado en peso de joyeria
        
        if(item.options !== 'undefined' && item.options !== false && site.settings.product_variant_per_serial == 0) {
            $.each(item.options, function () {
                if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                    unit_price = parseFloat(item.row.real_unit_price)+parseFloat(this.price);
                }
            });
        }
        var real_unit_price = item.row.real_unit_price;
        var net_price = unit_price;
        $('#prModalLabel').text(item.row.name + ' (' + item.row.code + ')');
        $('#pname').val(item.row.name);
        if (site.settings.tax1) {
            // $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax').select2('val', item.row.tax_rate);
            $('#ptax2').val(item.row.consumption_sale_tax);

            if(site.settings.allow_change_sale_iva == 0){
                $('#ptax').select2('readonly', true);
            }
            if (item.row.sale_tax_rate_2_percentage) {
                $('.ptax2_div').removeClass('col-sm-8').addClass('col-sm-4');
                $('#ptax2_percentage').val(item.row.sale_tax_rate_2_percentage);
                $('.ptax2_percentage_div').fadeIn();
            } else {
                $('.ptax2_div').removeClass('col-sm-4').addClass('col-sm-8');
                $('#ptax2_percentage').val('');
                $('.ptax2_percentage_div').fadeOut();
            }
            $('#old_tax').val(item.row.tax_rate);
            var item_discount = 0, ds = discount ? discount : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal(parseFloat(((unit_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = parseFloat(ds);
                }
            } else {
                item_discount = parseFloat(ds);
            }
            net_price -= item_discount;
            var pr_tax = item.row.tax_rate, pr_tax_val = 0;
            if (pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            if (oslitems[item_id].row.tax_method == 0) {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                                net_price -= pr_tax_val;
                            } else {
                                pr_tax_val = formatDecimal((((net_price) * parseFloat(this.rate)) / 100));
                                pr_tax_rate = formatDecimal(this.rate) + '%';
                            }
                        } else if (this.type == 2) {
                            pr_tax_val = parseFloat(this.rate);
                            pr_tax_rate = this.rate;
                        }
                    }
                });
            }
        } else {
            $('#ptax').select2('val', 1).trigger('change');
        }
        if (site.settings.product_serial !== 0) {
            $('#pserial').val(row.children().children('.rserial').val());
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.options !== false && site.settings.product_variant_per_serial == 0) {
            var o = 1;
            opt = $("<select id=\"poption\" name=\"poption\" class=\"form-control select\" />");
            $.each(item.options, function () {
                if(o == 1) {
                    if(product_option == '') { product_variant = this.id; } else { product_variant = product_option; }
                }
                $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                o++;
            });
        } else {
            $('.poptions_div').fadeOut();
            product_variant = 0;
        }

        uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        var pref = '<p style="margin: 12px 0 0 0;">n/a</p>';
        if(item.preferences !== null && item.preferences !== undefined && item.preferences !== false) {
            pref = $("<select id=\"ppreferences\" name=\"ppreferences\" class=\"form-control select\" multiple />");
            $.each(item.preferences, function () {
                $("<option />", {value: this.id, text: "("+this.category_name+") "+this.name, "data-pfcatid" : this.preference_category_id}).appendTo(pref);
            });
        } else {
            $('.ppreferences_div').fadeOut();
        }
        if (item.units) {
            uopt = $("<select id=\"punit\" name=\"punit\" class=\"form-control select\" />");
            $.each(item.units, function () {
                if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) {
                    if(this.product_unit_id == item.row.product_unit_id_selected) {
                        $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                    } else {
                        $("<option />", {value: this.product_unit_id, text: this.name}).appendTo(uopt);
                    }
                } else {
                    if (Object.keys(item.units).length == 1) { // Asegurar que no exista mas de una unidad 
                        if ((item.units?.[1]?.product_unit_id ?? item.units?.[1]?.id) === this.product_unit_id) {      
                            $("<option />", {value: this.product_unit_id, text: this.name, selected:true}).appendTo(uopt);
                        }
                    }else{
                        if(this.id == item.row.unit) {
                            $("<option />", {value: this.id, text: this.name, selected:true}).appendTo(uopt);
                        } else {
                            $("<option />", {value: this.id, text: this.name}).appendTo(uopt);
                        }
                    }
                }
            });
            if (Object.keys(item.units).length == 1) {
                $('.punit_div').fadeOut();
            }
        }
        $('#poptions-div').html(opt);
        $('#ppreferences-div').html(pref);
        if (item.preferences_selected !== undefined && item.preferences_selected !== null) {
            $('#ppreferences').val(item.preferences_selected);
        }
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#pquantity').val(qty);
        $('#old_qty').val(qty);
        $('#pprice').val(trmrate != 1 ? formatDecimal(trmrate * unit_price, 6) : trmrate * unit_price);
        $('#pprice').data('prevprice', trmrate != 1 ? formatDecimal(trmrate * unit_price, 6) : trmrate * unit_price);
        $('#pprice_ipoconsumo').val('');
        $('#punit_price').val(formatDecimal(parseFloat(unit_price)+parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
        $('#poption').select2('val', item.row.option);
        $('#old_price').val(unit_price);
        $('#item_id').val(item_id);
        $('#pserial').val(row.children().children('.rserial').val());
        $('#ptdiscount').val((formatDecimal(discount * qty) * trmrate));
        $('#pdiscount').val(getDiscountAssigned(discount, trmrate, 1));
        $('#net_price').text(trmrate != 1 ? formatDecimal(trmrate * net_price, 6) : formatMoney(trmrate * net_price));
        $('#pro_tax').text(formatMoney(trmrate * (parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax))));
        $('#under_cost_authorized').val(item.row.under_cost_authorized !== undefined ? item.row.under_cost_authorized : 0);

        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            units = item.units;
            unit_selected = units[item.row.product_unit_id_selected !== undefined && parseFloat(item.row.product_unit_id_selected) > 0 ? item.row.product_unit_id_selected : item.row.unit];
            unit_selected_real_quantity = 1;
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty / unit_selected.operation_value;
                unit_selected_real_quantity = 1 * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty * unit_selected.operation_value;
                unit_selected_real_quantity = 1 / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            }
            $('#pquantity').val(unit_selected_quantity);
            $('#punit').select2('val', item.row.product_unit_id_selected).trigger('change');
            $('#pproduct_unit_price').val(formatDecimal((unit_price + parseFloat(item.row.consumption_sale_tax)) * unit_selected_real_quantity));
            $('#pproduct_unit_price').trigger('change');
            $('#punit_quantity').val(formatDecimal(unit_selected.operation_value));
        }

        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
        $('#prModal').appendTo("body").modal('show');
        $('#psubtotal').val('');

    });

    $('#prModal').on('shown.bs.modal', function (e) {
        if($('#poption').select2('val') != '') {
            $('#poption').select2('val', product_variant);
            product_variant = 0;
        }
    });
    $(document).on('change', '#pprice, #pprice_ipoconsumo, #ptax, #pdiscount', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var unit_price = parseFloat($('#pprice').val());
        var pprice_ipoconsumo = parseFloat($('#pprice_ipoconsumo').val());
        var item = oslitems[item_id];
        var ds = $('#pdiscount').val() ? $('#pdiscount').val() : '0';
        if (pprice_ipoconsumo > 0 && parseFloat(pprice_ipoconsumo) > parseFloat(item.row.consumption_sale_tax)) {
            pprice_ipoconsumo = pprice_ipoconsumo - parseFloat(item.row.consumption_sale_tax);
            var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
            if (item.row.tax_method == 1 && pr_tax !== null && pr_tax != 0) {
                $.each(tax_rates, function () {
                    if(this.id == pr_tax){
                        if (this.type == 1) {
                            unit_price = pprice_ipoconsumo / ((parseFloat(this.rate) / 100) + 1);
                        } else if (this.type == 2) {
                            unit_price = pprice_ipoconsumo - parseFloat(this.rate);
                        }
                    }
                });
            } else if (item.row.tax_method == 0) {
                unit_price = pprice_ipoconsumo;
            }
        }
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#ptax').val(), item_tax_method = item.row.tax_method;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal(((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / 100));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }
        if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 1  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(unit_price));
        } else if ($('#pprice_ipoconsumo').val() > 0 && item.row.tax_method == 0  && parseFloat($('#pprice_ipoconsumo').val()) > parseFloat(item.row.consumption_sale_tax)) {
            $('#pprice').val(formatDecimal(pprice_ipoconsumo));
        } else if (parseFloat($('#pprice_ipoconsumo').val()) <= parseFloat(item.row.consumption_sale_tax)) {
            Command: toastr.error('Por favor ingrese un precio válido', 'Valores incorrectos', {onHidden : function(){}})
            $('#pprice_ipoconsumo').val(0);
        }
        $('#net_price').text(formatMoney(unit_price));
        setTimeout(function() {
            $('#pprice_ipoconsumo').val('');
        }, 500);
        $('#pro_tax').text(formatMoney(parseFloat(pr_tax_val) + parseFloat(item.row.consumption_sale_tax)));
    });
    $(document).on('change', '#punit', function () {
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = oslitems[item_id];
        if (!is_numeric($('#pquantity').val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        if (site.settings.prioridad_precios_producto == 5 || site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10) {
            unit_selected = item.units[unit];
            if (unit_selected) {
                if (unit_selected.operation_value > 0 && site.settings.precios_por_unidad_presentacion == 2) {
                    valor = unit_selected.operation_value;
                    if (unit_selected.operator == "*") {
                        valor =  parseFloat(unit_selected.valor_unitario) / parseFloat(unit_selected.operation_value);
                    } else if (unit_selected.operator == "/") {
                        valor =  parseFloat(unit_selected.valor_unitario) * parseFloat(unit_selected.operation_value);
                    }
                    $('#pprice').val(valor);
                    $('#pproduct_unit_price').val(unit_selected.valor_unitario);
                } else {
                    $('#pprice').val(unit_selected.valor_unitario);
                    $('#pproduct_unit_price').val(unit_selected.valor_unitario);
                }
                $('#pprice').change();
            }
        } else if(site.settings.prioridad_precios_producto == 11){

        } else {
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                        aprice = parseFloat(this.price);
                    }
                });
            }
            if(item.units && unit != oslitems[item_id].row.base_unit) {
                $.each(item.units, function(){
                    if (this.id == unit) {
                        base_quantity = unitToBaseQty($('#pquantity').val(), this);
                        $('#pprice').val(formatDecimal(((parseFloat(item.row.base_unit_price+aprice))*unitToBaseQty(1, this)))).change();
                    }
                });
            } else {
                $('#pprice').val(formatDecimal(item.row.base_unit_price+aprice)).change();
            }
        }
        $('.sale_form_edit_product_unit_name').text($('#punit option:selected').text());
    });
    $(document).on('change', '#pproduct_unit_price', function(){
        p_unit_price = $(this).val();
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id');
        var item = oslitems[item_id];
        if (!is_numeric($('#pquantity').val()) || parseFloat($('#pquantity').val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var opt = $('#poption').val(), unit = $('#punit').val(), base_quantity = $('#pquantity').val(), aprice = 0;
        pr_tax_2_val = formatDecimal(parseFloat(item.row.consumption_sale_tax));
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            unit_selected = item.units[unit];
            if (unit_selected.operation_value > 0) {
                valor = unit_selected.operation_value;
                ipoconsumo_um = 0;
                if (unit_selected.operator == "*") {
                    valor =  (parseFloat(p_unit_price)) / parseFloat(unit_selected.operation_value);
                } else if (unit_selected.operator == "/") {
                    valor =  (parseFloat(p_unit_price)) * parseFloat(unit_selected.operation_value);
                }
                $('#pprice').val(valor);
            } else {
                $('#pprice').val(p_unit_price);
            }
            $('#pprice').change();
        }

    });
    $(document).on('click', '#editItem', function () {
        var trmrate = 1;
        if (othercurrency = localStorage.getItem('othercurrency')) {
            rate = localStorage.getItem('othercurrencyrate');
            if (trm = localStorage.getItem('othercurrencytrm')) {
                trmrate = rate / trm;
            }
        }
        var row = $('#' + $('#row_id').val());
        var item_id = row.attr('data-item-id'), new_pr_tax = $('#ptax').val(), new_pr_tax_rate = false;
        if (new_pr_tax) {
            $.each(tax_rates, function () {
                if (this.id == new_pr_tax) {
                    new_pr_tax_rate = this;
                }
            });
        }
        var price = parseFloat($('#pprice').val());
        if(item.options !== false && site.settings.product_variant_per_serial == 0) {
            var opt = $('#poption').val();
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    price = price-parseFloat(this.price);
                }
            });
        }
        if (site.settings.product_discount == 1 && $('#pdiscount').val()) {
            if(!is_valid_discount($('#pdiscount').val()) || $('#pdiscount').val() > price) {
                bootbox.alert(lang.unexpected_value);
                return false;
            }
        }
        if (!is_numeric($('#pquantity').val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var unit = $('#punit').val();
        if (Object.keys(oslitems[item_id].units).length != 1) { // <- Si tiene mas de una unidad de medida, envia a la funcion unitToBaseQty la seleccion del select punit, si solo tiene una continua el flujo normal
            var base_quantity = unitToBaseQty($('#pquantity').val(),oslitems[item_id].units[unit]);
        }

        // this section is for products with gram in calculate price
        if (site.settings.handle_jewerly_products == "1") {   
            if (oslitems[item_id].row.based_on_gram_value == "1" || oslitems[item_id].row.based_on_gram_value == "2" ) {
                oslitems[item_id].row.current_price_gram = price / trmrate;
            }
        }
        // this section is for products with gram in calculate price

        if(unit != oslitems[item_id].row.base_unit) {
            $.each(oslitems[item_id].units, function(){
                if (this.id == unit) {
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        if (base_quantity === undefined) { // <- base quantity si pasa hasta aca indefinido se envia con la unidad base 0
            const units = oslitems[item_id].units;
            var firstUnitKey = Object.keys(units)[0];
            var base_quantity = unitToBaseQty($('#pquantity').val(),oslitems[item_id].units[firstUnitKey]);
        }
        if (site.settings.product_variant_per_serial == 0) {
            oslitems[item_id].row.option = $('#poption').val() ? $('#poption').val() : '';
        }

        tpax2 = parseFloat($('#ptax2').val() ? $('#ptax2').val() : 0);
        oslitems[item_id].row.fup = 1,
        oslitems[item_id].row.qty = parseFloat($('#pquantity').val()),
        oslitems[item_id].row.base_quantity = parseFloat(base_quantity),
        oslitems[item_id].row.consumption_sale_tax = tpax2,
        oslitems[item_id].row.real_unit_price = (price-parseFloat(tpax2)) / trmrate,
        oslitems[item_id].row.unit_price = (price-parseFloat(tpax2)) / trmrate,
        oslitems[item_id].row.unit = unit,
        oslitems[item_id].row.under_cost_authorized = $('#under_cost_authorized').val(),
        oslitems[item_id].row.price_before_promo = ($('#under_cost_authorized').val() == 1 ? $('#pprice').data('prevprice') : 0),
        oslitems[item_id].row.tax_rate = new_pr_tax,
        oslitems[item_id].tax_rate = new_pr_tax_rate,
        oslitems[item_id].row.discount = $('#pdiscount').val() ? getDiscountAssigned($('#pdiscount').val(), trmrate, 2) : '',
        oslitems[item_id].row.serial = $('#pserial').val();
        oslitems[item_id].row.name = $('#pname').val();
        prev_product_unit_id = oslitems[item_id].row.product_unit_id;
        oslitems[item_id].row.product_unit_id = $('#punit').val();
        oslitems[item_id].row.product_unit_id_selected = $('#punit').val();
        if (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 5) {
            var ditem = oslitems[item_id];
            unit_selected = ditem.units[unit];
            qty = parseFloat($('#pquantity').val());
            unit_selected_quantity = qty;
            if (unit_selected.operator == '*') {
                unit_selected_quantity = qty * unit_selected.operation_value;
            } else if (unit_selected.operator == '/') {
                unit_selected_quantity = qty / unit_selected.operation_value;
            } else if (unit_selected.operator == '+') {
                unit_selected_quantity = qty + unit_selected.operation_value;
            } else if (unit_selected.operator == '-') {
                unit_selected_quantity = qty - unit_selected.operation_value;
            }
            oslitems[item_id].row.qty = unit_selected_quantity;
        } else {
            oslitems[item_id].row.unit_price_id = $('#punit').val();
        }
        if (site.settings.prioridad_precios_producto == 11 && prev_product_unit_id !== undefined && prev_product_unit_id != oslitems[item_id].row.product_unit_id) {
            wappsiPreciosUnidad(oslitems[item_id].row.id,item_id,$('#pquantity').val(), true);
        }
        var preferences_text = '';
        var preferences_selected = [];
        var parr = [];
        $('#ppreferences option:selected').each(function(index, option){
            if (preferences_selected[$(option).data('pfcatid')] === undefined) {
                preferences_selected[$(option).data('pfcatid')] = [];
            }
            preferences_selected[$(option).data('pfcatid')].push(parseInt($(option).val()));
            psplit = $(option).text().split(')');
            psplit_index = psplit[0].replace("(", "");
            if (parr[psplit_index] === undefined) {
                parr[psplit_index] = [];
            }
            parr[psplit_index].push(psplit[1]);
        });
        if ($('#ppreferences option:selected').length > 0) {
            phtml = "";
            for (let preference_category in parr) {
                phtml += "<b>"+preference_category+" : </b>";
                for (let index in parr[preference_category]) {
                    phtml += parr[preference_category][index]+", ";
                }
            }
            oslitems[item_id].preferences_text = phtml;
            oslitems[item_id].preferences_selected = preferences_selected;
        }
        localStorage.setItem('oslitems', JSON.stringify(oslitems));
        $('#prModal').modal('hide');
        loadItems();
        return;
    });
    $(document).on('change', '#poption', function () {
        var row = $('#' + $('#row_id').val()), opt = $(this).val();
        var item_id = row.attr('data-item-id');
        var item = oslitems[item_id];
        var unit = $('#punit').val(), base_quantity = parseFloat($('#pquantity').val()), base_unit_price = item.row.base_unit_price;
        if(unit != oslitems[item_id].row.base_unit) {
            $.each(oslitems[item_id].units, function(){
                if (this.id == unit) {
                    base_unit_price = formatDecimal((parseFloat(item.row.base_unit_price)*(unitToBaseQty(1, this))), 4)
                    base_quantity = unitToBaseQty($('#pquantity').val(), this);
                }
            });
        }
        $('#pprice').val(parseFloat(base_unit_price)).trigger('change');
        if(item.options !== false) {
            $.each(item.options, function () {
                if(this.id == opt && this.price != 0 && this.price != '' && this.price != null) {
                    $('#pprice').val(parseFloat(base_unit_price)+(parseFloat(this.price))).trigger('change');
                }
            });
        }
    });
     $(document).on('click', '#sellGiftCard', function (e) {
        if (count == 1) {
            oslitems = {};
            if ($('#oslwarehouse').val() && $('#oslcustomer').val()) {
                $('#oslcustomer').select2("readonly", true);
                $('#oslwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#gcModal').appendTo("body").modal('show');
        return false;
    });
     $(document).on('click', '#addGiftCard', function (e) {
        var mid = (new Date).getTime(),
        gccode = $('#gccard_no').val(),
        gcname = $('#gcname').val(),
        gcvalue = $('#gcvalue').val(),
        gccustomer = $('#gccustomer').val(),
        gcexpiry = $('#gcexpiry').val() ? $('#gcexpiry').val() : '',
        gcprice = parseFloat($('#gcprice').val());
        if(gccode == '' || gcvalue == '' || gcprice == '' || gcvalue == 0 || gcprice == 0) {
            $('#gcerror').text('Please fill the required fields');
            $('.gcerror-con').show();
            return false;
        }
        var gc_data = new Array();
        gc_data[0] = gccode;
        gc_data[1] = gcvalue;
        gc_data[2] = gccustomer;
        gc_data[3] = gcexpiry;
        $.ajax({
            type: 'get',
            url: site.base_url+'sales/sell_gift_card',
            dataType: "json",
            data: { gcdata: gc_data },
            success: function (data) {
                if(data.result === 'success') {
                    oslitems[mid] = {"id": mid, "item_id": mid, "label": gcname + ' (' + gccode + ')', "row": {"id": mid, "code": gccode, "name": gcname, "quantity": 1, "base_quantity": 1, "price": gcprice, "real_unit_price": gcprice, "tax_rate": 0, "qty": 1, "type": "manual", "discount": "0", "serial": "", "option":""}, "tax_rate": false, "options":false, "units":false};
                    localStorage.setItem('oslitems', JSON.stringify(oslitems));
                    loadItems();
                    $('#gcModal').modal('hide');
                    $('#gccard_no').val('');
                    $('#gcvalue').val('');
                    $('#gcexpiry').val('');
                    $('#gcprice').val('');
                } else {
                    $('#gcerror').text(data.message);
                    $('.gcerror-con').show();
                }
            }
        });
        return false;
    });
     $(document).on('click', '#addManually', function (e) {
        if (count == 1) {
            oslitems = {};
            if ($('#oslwarehouse').val() && $('#oslcustomer').val()) {
                $('#oslcustomer').select2("readonly", true);
                $('#oslwarehouse').select2("readonly", true);
            } else {
                bootbox.alert(lang.select_above);
                item = null;
                return false;
            }
        }
        $('#mnet_price').text('0.00');
        $('#mpro_tax').text('0.00');
        $('#mModal').appendTo("body").modal('show');
        return false;
    });
    $(document).on('click', '#addItemManually', function (e) {
        var mid = (new Date).getTime(),
        mcode = $('#mcode').val(),
        mname = $('#mname').val(),
        mtax = parseInt($('#mtax').val()),
        mqty = parseFloat($('#mquantity').val()),
        mdiscount = $('#mdiscount').val() ? $('#mdiscount').val() : '0',
        unit_price = parseFloat($('#mprice').val()),
        mtax_rate = {};
        if (mcode && mname && mqty && unit_price) {
            $.each(tax_rates, function () {
                if (this.id == mtax) {
                    mtax_rate = this;
                }
            });
            oslitems[mid] = {"id": mid, "item_id": mid, "label": mname + ' (' + mcode + ')', "row": {"id": mid, "code": mcode, "name": mname, "quantity": mqty, "base_quantity": mqty, "price": unit_price, "unit_price": unit_price, "real_unit_price": unit_price, "tax_rate": mtax, "tax_method": 0, "qty": mqty, "type": "manual", "discount": mdiscount, "serial": "", "option":""}, "tax_rate": mtax_rate, 'units': false, "options":false};
            localStorage.setItem('oslitems', JSON.stringify(oslitems));
            loadItems();
        }
        $('#mModal').modal('hide');
        $('#mcode').val('');
        $('#mname').val('');
        $('#mtax').val('');
        $('#mquantity').val('');
        $('#mdiscount').val('');
        $('#mprice').val('');
        return false;
    });
    $(document).on('change', '#mprice, #mtax, #mdiscount', function () {
        var unit_price = parseFloat($('#mprice').val());
        var ds = $('#mdiscount').val() ? $('#mdiscount').val() : '0';
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = parseFloat(((unit_price) * parseFloat(pds[0])) / 100);
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        unit_price -= item_discount;
        var pr_tax = $('#mtax').val(), item_tax_method = 0;
        var pr_tax_val = 0, pr_tax_rate = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {

                        if (item_tax_method == 0) {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            unit_price -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((unit_price) * parseFloat(this.rate)) / 100), 4);
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }

                    } else if (this.type == 2) {

                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;

                    }
                }
            });
        }

        $('#mnet_price').text(formatMoney(unit_price));
        $('#mpro_tax').text(formatMoney(pr_tax_val));
    });

    /* --------------------------
     * Edit Row Quantity Method
    --------------------------- */
    var old_row_qty;
    $(document).on("focus", '.rquantity', function () {
        old_row_qty = $(this).val();
    }).on("change", '.rquantity', function () {
        var row = $(this).closest('tr');
        min = parseFloat($(this).prop('min'));
        if (!is_numeric($(this).val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        if (new_qty < oslitems[item_id].row.delivered_qty) {
            new_qty = oslitems[item_id].row.delivered_qty;
        }
        if (min > 0 && new_qty < min) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        oslitems[item_id].row.base_quantity = new_qty;
        if(oslitems[item_id].row.unit != oslitems[item_id].row.base_unit) {
            $.each(oslitems[item_id].units, function(){
                if (this.id == oslitems[item_id].row.unit) {
                    oslitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        oslitems[item_id].row.qty = new_qty;
        oslitems[item_id].row.delivered_qty = 0;
        oslitems[item_id].row.pending_qty = 0;
        oslitems[item_id].row.bill_qty = oslitems[item_id].row.pending_qty;
        oslitems2 = JSON.parse(localStorage.getItem('oslitems'));
        wappsiPreciosUnidad(oslitems[item_id].row.id, item_id, new_qty, $('.rquantity').index($(this))+1, false);
        if (delivered_qty = oslitems2[item_id].row.delivered_qty) {
            if (new_qty >= delivered_qty) {
                oslitems[item_id].row.pending_qty = new_qty - delivered_qty;
                oslitems[item_id].row.bill_qty = new_qty - delivered_qty;
                oslitems[item_id].row.delivered_qty = delivered_qty;
                localStorage.setItem('oslitems', JSON.stringify(oslitems));
            }
        } else {
            localStorage.setItem('oslitems', JSON.stringify(oslitems));
        }
    }).on('keydown', '.rquantity', function(e){
        if (e.keyCode == 13) {
            localStorage.setItem('enter_pressed_for_quantity', true);
            $(this).trigger('change');
            setTimeout(function() {
                localStorage.removeItem('enter_pressed_for_quantity');
            }, 2000);
        }
    });

    $(document).on('change', '.pquantity', function(){
        var row = $(this).closest('tr');
            // console.log(is_numeric($(this).val()));
        if (!is_numeric($(this).val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_pending_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        oslitems = JSON.parse(localStorage.getItem('oslitems'));

        if (new_pending_qty <= oslitems[item_id].row.qty && new_pending_qty <= (oslitems[item_id].row.qty - oslitems[item_id].row.delivered_qty) ) {
            oslitems[item_id].row.pending_qty = new_pending_qty;
            oslitems[item_id].row.bill_qty = (oslitems[item_id].row.qty - oslitems[item_id].row.delivered_qty) - new_pending_qty;
            localStorage.setItem('oslitems', JSON.stringify(oslitems));
        } else {
            $(this).val(0);
        }

        loadItems();

    });

    $(document).on('change', '.bquantity', function(){
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_bill_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        oslitems = JSON.parse(localStorage.getItem('oslitems'));

        if (new_bill_qty <= oslitems[item_id].row.qty && new_bill_qty <= (oslitems[item_id].row.qty - (oslitems[item_id].row.delivered_qty ? oslitems[item_id].row.delivered_qty : 0))) {
            oslitems[item_id].row.pending_qty = (oslitems[item_id].row.qty - oslitems[item_id].row.delivered_qty) - new_bill_qty;
            oslitems[item_id].row.bill_qty = new_bill_qty;
            oslitems[item_id].row.manually_changed = true;
            localStorage.setItem('oslitems', JSON.stringify(oslitems));
        } else {
            $(this).val(0);
        }

        loadItems($('.bquantity').index($(this)), true);

    }).on('keydown', '.bquantity', function(e){
        if (e.keyCode == 13 && site.settings.set_focus != 1) {
            setTimeout(function() {
                $('#add_item').focus();
            }, 1200);
        }
    });

    /* --------------------------
     * Edit Row Price Method
     -------------------------- */
     var old_price;
     $(document).on("focus", '.rprice', function () {
        old_price = $(this).val();
    }).on("change", '.rprice', function () {
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val())) {
            $(this).val(old_price);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_price = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        oslitems[item_id].row.price = new_price;
        localStorage.setItem('oslitems', JSON.stringify(oslitems));
        loadItems();
    });

    $(document).on("click", '#removeReadonly', function () {
        $('#oslcustomer').select2('readonly', false);
        //$('#oslwarehouse').select2('readonly', false);
        return false;
    });

    if (site.settings.management_order_sale_delivery_time == 1) {
        setTimeout(function() {
            $('#day_delivery_time').trigger('change');
        }, 2000);
    }

    /* function for manage responsive in orders*/
    resizeWindow();
    $(window).resize(resizeWindow);
}); //fin document ready
/* -----------------------
 * Misc Actions
 ----------------------- */

// hellper function for customer if no localStorage value
function nsCustomer() {
    $('#oslcustomer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "customers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
//localStorage.clear();
/*aca loaditems*/
function loadItems(tabindex = null, from_bq = false) {
    let lock_submit_by_margin = false;
    let lock_submit = false;
    if (localStorage.getItem('oslitems')) {
        subtotal = 0;
        total = 0;
        count = 1;
        count_pending = 1;
        count_delivered = 1;
        count_bill = 1;
        an = 1;
        product_tax = 0;
        product_tax_2 = 0;
        invoice_tax = 0;
        product_discount = 0;
        order_discount = 0;
        total_discount = 0;
        shipping = 0;
        var key_log = localStorage.getItem('oslkeylog');
        var oslcustomerspecialdiscount = localStorage.getItem('oslcustomerspecialdiscount');
        var tax_exempt_customer = false;
        $("#oslTable tbody").empty();
        oslitems = JSON.parse(localStorage.getItem('oslitems'));
        oslsale_status = localStorage.getItem('oslsale_status');
        sortedItems = _.sortBy(oslitems, function(o) { return [parseInt(o.order)]; });
        if (site.settings.product_order == 2) {
            sortedItems.reverse();
        }
        $('#add_sale, #edit_sale').attr('disabled', false);
        $.each(sortedItems, function () {
            var item = this;
            if (oslsale_status == 'completed') {
                rquantity_readonly = 'readonly';
            } else {
                rquantity_readonly = '';
            }
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_type = item.row.type, combo_items = item.combo_items, item_price = item.row.price, item_qty = item.row.qty, item_pending_qty = item.row.pending_qty != undefined ? item.row.pending_qty : item_qty, item_delivered_qty = item.row.delivered_qty != undefined ? item.row.delivered_qty : 0, item_bill_qty = item.row.bill_qty != undefined ? item.row.bill_qty : 0, item_aqty = item.row.quantity, item_tax_method = item.row.tax_method, item_ds = item.row.discount, item_discount = 0, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name !== undefined ? item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;") : item.label.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var product_unit = item.row.product_unit_id_selected, base_quantity = item.row.base_quantity;
            var unit_price = item.row.unit_price;
            if (site.settings.prioridad_precios_producto == 7 && item.row.product_unit_id_selected !== undefined && item.units[item.row.product_unit_id_selected] !== undefined) {
                if ($.inArray(site.settings.prioridad_precios_producto, [7, 10, 11])) {
                    unit_selected = item.units[item.row.product_unit_id_selected];
                    if (formatDecimal(item_qty) < formatDecimal(unit_selected.operation_value)) {
                        item_qty = formatDecimal(unit_selected.operation_value);
                        command: toastr.warning('El valor ingresado está por debajo del mínimo de la unidad escogida.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "50000",
                            "extendedTimeOut": "1000",
                        });
                    }
                }
            }

            pr_tax_rate = 0;
            if(item.options !== false) {
                $.each(item.options, function () {
                    if(this.id == item.row.option && this.price != 0 && this.price != '' && this.price != null) {
                        item_price = unit_price+(parseFloat(this.price));
                        unit_price = item_price;
                    }
                });
            }
            if (item.row.tax_exempt_customer && site.settings.enable_customer_tax_exemption == 1) {
                tax_exempt_customer = true;
            }
            var ds = item_ds ? item_ds : '0';
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    item_discount = formatDecimal((((item_price) * parseFloat(pds[0])) / 100));
                } else {
                    item_discount = formatDecimal(ds);
                }
            } else {
                 item_discount = formatDecimal(ds);
            }
            unit_price -= item_discount; //precio sin IVA - Descuento
            var pr_tax = item.tax_rate;
            var pr_tax_val = 0;
            if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, unit_price, item_tax_method))) {
                pr_tax_val = ptax[0];
                pr_tax_rate = ptax[1];
            }
            item_price = item_tax_method == 1 ? unit_price : unit_price - pr_tax_val;
            var sel_opt = '';
            $.each(item.options, function () {
                if(this.id == item_option) {
                    sel_opt = this.name;
                }
            });
            if (key_log == 1 && oslcustomerspecialdiscount == 1) {
                item_price = item.row.real_unit_price;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, (item_tax_method == 1 ? 0 : item_tax_method)))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                item_price -= pr_tax_val;
                var ds = item_ds ? item_ds : '0';
                if (ds.indexOf("%") !== -1) {
                    var pds = ds.split("%");
                    if (!isNaN(pds[0])) {
                        item_discount = formatDecimal((((item_price) * parseFloat(pds[0])) / 100));
                    } else {
                        item_discount = formatDecimal(ds);
                    }
                } else {
                     item_discount = formatDecimal(ds);
                }
                item_price -= item_discount;
                if (site.settings.tax1 == 1 && (ptax = calculateTax(pr_tax, item_price, item_tax_method))) {
                    pr_tax_val = ptax[0];
                    pr_tax_rate = ptax[1];
                }
                if (item_tax_method == 0) {
                    item_price -= pr_tax_val;
                }
            }

            // Sección para manejo de joyaeria con peso en productos sin variantes
            if (site.settings.handle_jewerly_products == "1") {
                if (item.row.based_on_gram_value == "1" || item.row.based_on_gram_value == "2") { // 1 valor_nacional, 2 valor_italiano
                    _jewel_weight =  parseFloat((item.row.jewel_weight !== undefined) ? item.row.jewel_weight : 0);
                    _current_price = parseFloat((item.row.current_price_gram !== undefined) ? item.row.current_price_gram : 0);
                    item_price = _jewel_weight * _current_price;
                }
            }
            // Sección para manejo de joyaeria con peso en productos sin variantes

            /* profitability_margin */
            $('.margin_alert').fadeOut();
            if (site.settings.alert_zero_cost_sale == 1 && formatDecimal(item.row.cost) == 0) {
                command: toastr.warning('El producto '+item_name+' tiene costo en 0.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "50000",
                    "extendedTimeOut": "1000",
                });
            }
            if (site.settings.profitability_margin_validation > 0) {
                var margin_cost = site.settings.which_cost_margin_validation == 1 ? item.row.cost : item.row.avg_cost; // ultmo costo o costro promedio
                var margin_price = parseFloat(item_price) + parseFloat(pr_tax_val);
                var tax_method = item_tax_method; // tax_method => 0 es impuesto incluido
                var margin_tax_rate = item.tax_rate.id; // margin_tax_rate => id del impuesto tabla tax_rates
                var margin = 0;
                let productNameE= item.row.name;
                if (site.settings.profitability_margin_validation == 3) { // cuando se valida listas minimas
                    if (item.row.based_on_gram_value == '0' || item.row.based_on_gram_value == null) { // cuando no esta basado en peso
                        if (item.row.variant_weight_selected) { // cuando no esta basado en peso pero tiene variantes, se validara el campo minimun_price
                            if (margin_price < item.row.variant_minimun_price_selected ) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }else { // cuando no esta basado en gramo y no tiene variantes se valida contra la lista marcada como minima
                            let price_minimum = validate_price_minimun(item.row.id);
                            if (!price_minimum) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor mínimo no definido para el producto ${productNameE}` );
                            }else if(price_minimum > margin_price){
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }
                    }
                    else if(item.row.based_on_gram_value == '1'){ // cuando esta basado en oro nacional
                        if (item.row.variant_weight_selected ) {
                            if (margin_price < parseFloat(item.row.variant_weight_selected * site.pos_settings.minimun_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }else{
                            if (margin_price < parseFloat(item.row.jewel_weight * site.pos_settings.minimun_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo para el producto ${productNameE}` );
                            }
                        }
                    }
                    else if(item.row.based_on_gram_value == '2'){ // cuando esta basado en oro italiano
                        if (item.row.variant_weight_selected) {
                            if (margin_price < parseFloat(item.row.variant_weight_selected * site.pos_settings.minimun_italian_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo  para el producto ${productNameE}` );
                            }
                        }else{
                            if (margin_price < parseFloat(item.row.jewel_weight * site.pos_settings.minimun_italian_gold_price)) {
                                lock_submit_by_margin = true;
                                header_alert('warning', `Valor del precio menor al valor mínimo para el producto ${productNameE}` );
                            }
                        }
                    }
                }else{
                    if (margin_cost > 0 && margin_price > 0 && margin_tax_rate > 0 && tax_method == 0) {
                        var margin_pr_tax = tax_rates[margin_tax_rate];
                        margin_pr_tax_val = 0;
                        if (ptax = calculateTax(margin_pr_tax, margin_price, tax_method)) {
                            margin_pr_tax_val = ptax[0];
                        }
                        var margin_net_price = margin_price - margin_pr_tax_val;
                        if (ptax = calculateTax(margin_pr_tax, margin_cost, tax_method)) {
                            margin_pr_tax_val = ptax[0];
                        }
                        var margin_net_cost = margin_cost - margin_pr_tax_val;
                    } else {
                        var margin_net_price = margin_price;
                        var margin_net_cost = margin_cost;
                    }
                    if (margin_net_price > 0 && margin_net_cost > 0) {
                        if (site.settings.profitability_margin_validation == 1 && item.category !== undefined) {
                            setted_margin = item.category.profitability_margin;
                        } else if(site.settings.profitability_margin_validation == 2 && item.subcategory !== undefined) {
                            setted_margin = item.subcategory.profitability_margin;
                        }
                        if (setted_margin == 0) {
                            if (item.subcategory !== undefined && item.subcategory.profitability_margin > 0) {
                                setted_margin = item.subcategory.profitability_margin;
                            } else if (item.category !== undefined && item.category.profitability_margin > 0) {
                                setted_margin = item.category.profitability_margin;
                            }
                        }
                        if (setted_margin > 0) {
                            margin = formatDecimals((((margin_net_price - margin_net_cost) / margin_net_price) * 100), 2);
                            if (parseFloat(margin) <= parseFloat(setted_margin)) {
                                lock_submit_by_margin = true;
                                msg_error = 'El margen mínimo establecido para el producto es de '+formatDecimals(setted_margin)+'%, margen actual '+margin+'%;';
                                $('.margin_alert').fadeIn();
                            }
                        }
                    }
                }
            }
            /* profitability_margin */

            pr_tax_2_val = item.row.consumption_sale_tax;
            pr_tax_2_rate = item.row.sale_tax_rate_2_percentage ? item.row.sale_tax_rate_2_percentage : pr_tax_2_val;
            pr_tax_2_rate_id = item.row.sale_tax_rate_2_id;
            var row_no = site.settings.item_addition == 1 ? item.item_id : item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                            '<input name="sale_item_id[]" type="hidden" class="rid" value="' + (item.row.sale_item_id !== undefined ? item.row.sale_item_id : null) + '">'+
                            '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                            '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                            '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                            '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                            '<input name="product_current_gold_price[]" type="hidden" class="rcurrent_gold_price" value="'+ parseFloat((item.row.current_price_gram !== undefined) ? item.row.current_price_gram : 0) +'">'+
                            '<input name="product_seller_id[]" type="hidden" value="'+ (item.row.seller_id !== undefined && item.row.seller_id ? item.row.seller_id : $('#oslseller').val()) +'">'+
                            '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(item.row.serialModal_serial === undefined && sel_opt != '' ? ' ('+sel_opt+')' : '')+'</span> '+
                            '<i class="pull-right fa fa-edit tip pointer edit" id="' + row_no + '" data-item="' + item_id + '" title="Edit" style="cursor:pointer;"></i>'+
                            (item.row.serialModal_serial ? "<input type='hidden' name='serialModal_serial[]' value='"+item.row.serialModal_serial+"'>" : "")+
                            (item.row.op_pending_quantity !== undefined && item.row.op_pending_quantity > 0 ? ' <i class="fa-solid fa-info-circle"  data-toggle="tooltip" data-placement="right" title="" style="cursor: pointer" data-original-title="'+
                                "En stock : "+formatDecimal((parseFloat(item.row.op_pending_quantity)+parseFloat(item.row.quantity)))+" | "+
                                "Pendientes orden de pedido : "+formatDecimal(item.row.op_pending_quantity)+" | "+
                                "Disponibles : "+formatDecimal(item.row.quantity)+
                            '"'+'"></i>': "")+
                        '</span>'+
                      '</td>';
            if (site.settings.product_serial == 1) {
                tr_html += '<td class="text-right no-mobile">'+
                                '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                            '</td>';
            }
            if (site.settings.product_variant_per_serial == 1) {
                tr_html += '<td class="text-right no-mobile">'+
                                '<span> '+(item.row.serialModal_serial ? item.row.serialModal_serial : '')+'</span>'+
                            '</td>';
            }
            tr_html += '<td class="text-right no-mobile">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(item_price + item_discount) + '</span>'+
                        '</td>';
            if ((site.settings.product_discount == 1 && allow_discount == 1) || item_discount) {
                tr_html += '<td class="text-right no-mobile">'+
                                '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                '<input class="form-control input-sm" name="product_discount_val[]" type="hidden" value="' + (item_discount * item_qty) + '">'+
                                // '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + formatMoney(0 - (item_discount * item_qty)) + '</span>'+
                                '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' + get_percentage_from_amount(item_ds, (item_price + item_discount)) + '</span>'+
                            '</td>';
            }
            tr_html += '<td class="text-right no-mobile">'+
                            '<input class="form-control input-sm text-right rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">'+
                            '<input class="realuprice ruprice" name="unit_price[]" type="hidden" value="' + (key_log == '1' ? unit_price : item_price) + '">'+
                            '<input class="" name="real_unit_price[]" type="hidden" value="' + (parseFloat(item.row.real_unit_price) )  + '">'+
                            '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(item_price) + '</span>'+
                        '</td>';
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right no-mobile">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax.id : 1) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_rate[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax_' + row_no + '" value="' + (key_log == '1' ? pr_tax_val : 0) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val[]" type="hidden" value="' + (key_log == '1' ? pr_tax_rate : 0) + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + pr_tax_rate + '</span>'+
                            '</td>';
            }
            if (site.settings.ipoconsumo == 1) {
                tr_html += '<td class="text-right no-mobile">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="product_tax_2[]" type="hidden" id="product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_rate_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 1) + '">'+
                                '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax_2[]" type="hidden" id="unit_product_tax_' + row_no + '_2" value="' + (key_log == '1' ? pr_tax_2_val : 0) + '">'+
                                '<input class="form-control input-sm text-right" name="product_tax_val_2[]" type="hidden" value="' + (key_log == '1' ? pr_tax_2_rate : 0) + '">'+
                                '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '_2">' + formatMoney(pr_tax_2_rate * item_qty) + '</span>'+
                            '</td>';
            }
            if (site.settings.tax1 == 1) {
                tr_html += '<td class="text-right">'+
                                '<span class="text-right">' + formatMoney(formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val))))) + '</span>'+
                            '</td>';
            }
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" name="quantity[]" type="text" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+rquantity_readonly+' tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" '+(item.row.picking_quantity > 0 ? 'min="'+item.row.picking_quantity+'"' : '')+'>'+
                            '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                            '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + base_quantity + '">'+
                            '<input name="product_aqty[]" type="hidden" class="item_aqty" value="' + item_aqty + '">'+
                        '</td>';

            tr_html += '<td class="no-mobile">'+
                            '<input class="form-control text-center dquantity"  name="quantity_delivered[]" type="text" value="' + item_delivered_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="dquantity_' + row_no + '" onClick="this.select();" readonly>'+
                        '</td>';
            tr_html += '<td class="no-mobile">'+
                            '<input class="form-control text-center pquantity"  name="quantity_pending[]" type="text" value="' + item_pending_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="pquantity_' + row_no + '" onClick="this.select();" '+rquantity_readonly+' readonly>'+
                        '</td>';
            if (item.row.picking_quantity > 0) {
                rquantity_readonly = 'readonly';
                item_bill_qty = 0;
            }
            if (converting == false && item.row.manually_changed == undefined) {
                item_bill_qty = 0;
            }
            tr_html += '<td class="no-mobile">'+
                            '<input class="form-control text-center bquantity"  name="quantity_bill[]" type="text" value="' + item_bill_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="bquantity_' + row_no + '" onClick="this.select();" '+rquantity_readonly+'>'+
                        '</td>';
            tr_html += '<td class="text-right">'+
                            '<span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty))) + '</span>'+
                        '</td>';
            tr_html += '<td class="text-center">'+
                            '<i class="fa fa-times tip pointer osldel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i>'+
                        '</td>';
            newTr.html(tr_html);
            newTr.appendTo("#oslTable");
            if (item.preferences_text !== null && item.preferences_text !== undefined) {
                var newTr2 = $('<tr class="tr_preferences" style="font-size:112%;"></tr>');
                tr2_html = '<td colspan="5">'+
                            '<input type="hidden" name="product_preferences_text[]" value="'+JSON.stringify(item.preferences_selected)+'">' +
                            item.preferences_text +
                          '</td>';
                newTr2.html(tr2_html);
                newTr2.appendTo("#oslTable");
            }

            if (item_type == 'standard' && item.options !== false) {
                item_option = this.row.option
                $.each(item.options, function () {
                    if(this.id == item_option && item_qty > this.quantity) {
                        if (site.settings.exclude_overselling_on_sale_orders == 0 && site.settings.overselling == 0) {
                            $('#row_' + row_no).addClass('danger');
                            lock_submit = true;
                        }
                    }
                });
            } else {
                if (item.row.qty > item.row.quantity) {
                    if (site.settings.exclude_overselling_on_sale_orders == 0 && site.settings.overselling == 0) {
                        $('#row_' + row_no).addClass('danger');
                        lock_submit = true;
                    }
                }
            }
            product_discount += (item_discount * item_qty);
            product_tax += parseFloat(pr_tax_val * item_qty);
            product_tax_2 += parseFloat(pr_tax_2_val * item_qty);
            subtotal += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
            total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val)) * parseFloat(item_qty)), 4);
            count += parseFloat(item_qty);
            count_pending += parseFloat(item_pending_qty);
            count_delivered += parseFloat(item_delivered_qty);
            count_bill += parseFloat(item_bill_qty);
            an++;
        });
        var col = 1;
        if (site.settings.product_variant_per_serial == 1) { col++; }
        if (site.settings.product_serial == 1) { col++; }
        var tfoot = '<tr id="tfoot" class="tfoot active">'+
                        '<th colspan="'+col+'">Total ('+(an - 1)+')</th>'+
                        '<th class="text-right no-mobile">'+formatMoney(parseFloat(subtotal) + parseFloat(product_discount))+'</th>';
        if ((site.settings.product_discount == 1 && allow_discount == 1) || product_discount) {
            tfoot += '<th class="text-righ no-mobile">'+formatMoney(product_discount)+'</th>';
        }
        tfoot +='<th class="text-right" id="subtotalS">'+formatMoney(subtotal)+'</th>';
        if (site.settings.tax1 == 1) {
            // console.log('PR TAX :'+product_tax);
            tfoot += '<th class="text-right no-mobile" id="ivaamount">'+formatMoney(product_tax)+'</th>';
            if (site.settings.ipoconsumo == 1) {
                tfoot += '<th class="text-right no-mobile" id="ivaamount">'+formatMoney(product_tax_2)+'</th>';
            }
        }
        if (site.settings.tax1 == 1) {
            tfoot += '<th class="text-right no-mobile "></th>';
        }
        tfoot +='<th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
            tfoot += '<th class="text-right no-mobile"></th>';
            tfoot += '<th class="text-right no-mobile"></th>';
            tfoot += '<th class="text-right no-mobile"></th>';
        tfoot += '<th class="text-right" id="totalS">'+formatMoney(total)+'</th>'+
                 '<th class="text-center">'+
                 '  <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>'+
                 '</th>'+
                '</tr>';
        $('#oslTable tfoot').html(tfoot);
        // Order level discount calculations
        if (osldiscount = localStorage.getItem('osldiscount')) {
            var ds = osldiscount;
            if (ds.indexOf("%") !== -1) {
                var pds = ds.split("%");
                if (!isNaN(pds[0])) {
                    order_discount = formatDecimal((((total) * parseFloat(pds[0])) / 100), 4);
                } else {
                    order_discount = formatDecimal(ds);
                }
            } else {
                order_discount = formatDecimal(ds);
            }
            //total_discount += parseFloat(order_discount);
        }
        // Order level tax calculations
        if (site.settings.tax2 != 0) {
            if (osltax2 = localStorage.getItem('osltax2')) {
                $.each(tax_rates, function () {
                    if (this.id == osltax2) {
                        if (this.type == 2) {
                            invoice_tax = formatDecimal(this.rate);
                        } else if (this.type == 1) {
                            invoice_tax = formatDecimal((((total - order_discount) * this.rate) / 100), 4);
                        }
                    }
                });
            }
        }
        total_discount = parseFloat(order_discount + product_discount);
        var gtotal = parseFloat(((total + invoice_tax) - order_discount) + shipping);
        $('#total').text(formatMoney(total));
        $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
        $('#total_items').val((parseFloat(count) - 1));
        //$('#tds').text('('+formatMoney(product_discount)+'+'+formatMoney(order_discount)+')'+formatMoney(total_discount));
        $('#tds').text(formatMoney(order_discount));
        if (site.settings.tax2 != 0) {
            $('#ttax2').text(formatMoney(invoice_tax));
        }
        $('#tship').text(formatMoney(shipping));
        lock_submit_by_min_sale_amount = false;
        amount_ordered = formatDecimal(localStorage.getItem('amount_ordered'));
        $('.error_min_sale_amount').fadeOut();
        if (amount_ordered > 0) {

        } else{
            if (gtotal > 0 && localStorage.getItem('oslbiller') && billers_data[localStorage.getItem('oslbiller')].min_sale_amount > 0) {
                $('.text_min_sale_amount').text(formatMoney(billers_data[localStorage.getItem('oslbiller')].min_sale_amount));
                if (gtotal < billers_data[localStorage.getItem('oslbiller')].min_sale_amount) {
                    $('.error_min_sale_amount').fadeIn();
                    $('#add_sale').prop('disabled', true);
                    lock_submit_by_min_sale_amount = true;
                } else if (gtotal >= billers_data[localStorage.getItem('oslbiller')].min_sale_amount) {
                    $('.error_min_sale_amount').fadeOut();
                    $('#add_sale').prop('disabled', false);
                }
            }
        }


        $('#gtotal').text(formatMoney(gtotal));
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            // $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            // $(window).scrollTop($(window).scrollTop() + 1);
        }
        if (count > 1) {
            $('#oslcustomer').select2("readonly", true);
            // $('#oslcustomerbranch').select2("readonly", true);
            // $('#oslwarehouse').select2("readonly", true);
            // $('#osldiscount').prop("readonly", true);
            // $('#oslshipping').prop("readonly", true);
        }
        if (site.settings.product_order == 2) {
            an = 2;
        }
        if (tax_exempt_customer) {
            $('#tax_exempt_customer').val(1);
        }
        if (count_bill > 1) {
            $('#redirect_order_to').prop('required', true);
        } else {
            $('#redirect_order_to').prop('required', false);
        }
        if (from_bq) {
            $('.bquantity').eq(tabindex+1).focus().select();
        } else {
            set_page_focus(an, tabindex);
        }
        
        $('[data-toggle="tooltip"]').tooltip();
    }
 
    if (lock_submit_by_margin || lock_submit) {
        $('#add_sale, #edit_sale').prop('disabled', true);
    } else {
        $('#add_sale, #edit_sale').prop('disabled', false);;
    }
}

/* -----------------------------
 * Add Sale Order Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
function add_invoice_item(item, modal = false, cont = false) {

    if (count == 1) {
        oslitems = {};
        if ($('#oslwarehouse').val() && $('#oslcustomer').val()) {
            $('#oslcustomer').select2("readonly", true);
            $('#oslwarehouse').select2("readonly", true);
        } else {
            bootbox.alert(lang.select_above);
            item = null;
            return;
        }
    }

    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;

    if (oslitems[item_id]) {

        var new_qty = parseFloat(oslitems[item_id].row.qty) + 1;
        oslitems[item_id].row.base_quantity = new_qty;
        if(oslitems[item_id].row.unit != oslitems[item_id].row.base_unit) {
            $.each(oslitems[item_id].units, function(){
                if (this.id == oslitems[item_id].row.unit) {
                    oslitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        oslitems[item_id].row.qty = new_qty;

    } else {
        oslitems[item_id] = item;
    }

    oslitems[item_id].order = new Date().getTime();

    if (site.settings.product_variant_per_serial == 1 && modal === false && item.row.attributes == 1) {
        $('#serialModal_serial').val('');
        $('#serialModal_meters').val('');
        $('.product_name_serialModal').text(oslitems[item_id].label);
        $('#serialModal_product_id').val(item_id);
        $('#serialModal_option_id').val('');
        $('#serialModal').modal('show');
        localStorage.setItem('modalSerial_item', JSON.stringify(oslitems[item_id]));
        delete oslitems[item_id];
        setTimeout(function() {
            $('#serialModal_serial').focus();
        }, 850);
    } else {
        localStorage.removeItem('modalSerial_item');
        if (site.settings.product_variant_per_serial == 1 && cont == false) {
            $('#serialModal').modal('hide');
            $('#add_item').val('').focus();
        } else {
            item = oslitems[item_id];
            $.ajax({
                url : site.base_url + "products/get_random_id"
            }).done(function(data){
                item.id = data;
                // console.log(item);
                localStorage.setItem('modalSerial_item', JSON.stringify(item));
                $('#serialModal_serial').val('');
                $('#serialModal_meters').val('');
            });
        }
        localStorage.setItem('oslitems', JSON.stringify(oslitems));
        wappsiPreciosUnidad(oslitems[item_id].row.id, item_id, oslitems[item_id].row.qty);
        if (site.pos_settings.show_variants_and_preferences == 1 && oslitems[item_id].preferences) {
            setTimeout(function() {
                $('#myModal').modal({remote: site.base_url + 'pos/product_preferences_selection/' + oslitems[item_id].row.id + "/" + item_id+"/oslitems",
                    backdrop: 'static',
                    keyboard: false});
                $('#myModal').modal('show');
            }, 850);
        }
        loadItems();
        return true;
    }

}

if (typeof (Storage) === "undefined") {
    // $(window).bind('beforeunload', function (e) {
    //     if (count > 1) {
    //         var message = "You will loss data!";
    //         return message;
    //     }
    // });
}


function validate_key_log(){
    // console.log(site.base_url+'sales/validate_key_log');
    $.ajax({
        type: "get", async: false,
        url: site.base_url+"sales/validate_key_log/" + $('#document_type_id').val(),
        dataType: "json",
        success: function (data) {
            localStorage.setItem('oslkeylog', data);
            // loadItems();
        }
    });
}


function wappsiPreciosUnidad(id,item_id,new_qty, index = null, new_item = true) {

    // if (site.settings.prioridad_precios_producto != 7 && site.settings.prioridad_precios_producto != 10) {
        var biller_price_group = $('#oslbiller option:selected').data('pricegroupdefault');
        var customer = $('#oslcustomer').val();
        var biller = $('#oslbiller').val();
        var address_id = $('#oslcustomerbranch').val();
        var unit_price_id = null;

        if (site.settings.precios_por_unidad_presentacion == 2 || site.settings.prioridad_precios_producto == 11 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 7) {
            if (item_selected = oslitems[item_id]) {
                unit_price_id = item_selected.row.product_unit_id_selected;
            }
        }

        var datos = {
                        id : id,
                        new_qty : new_qty,
                        price_group : biller_price_group,
                        customer : customer,
                        address_id : address_id,
                        biller : biller,
                        prioridad : site.settings.prioridad_precios_producto,
                        unit_price_id : unit_price_id
                    };

        $.ajax({
            type: 'get',
            url: site.base_url+"sales/preciosUnidad",
            dataType: "json",
            data: datos,
            success: function (data) {
                $(this).removeClass('ui-autocomplete-loading');
                if (data !== null && data[0] !== null && data[0] != 0 && new_item) {
                    var valor = data[0];
                    if (oslitems[item_id]) {
                        oslitems[item_id].row.real_unit_price = valor;
                        oslitems[item_id].row.unit_price = valor;
                        if (site.settings.precios_por_unidad_presentacion == 2) {
                            oslitems[item_id].row.unit_price_quantity = data[1];
                            if (data[1] !== undefined) {
                                oslitems[item_id].row.real_unit_price = valor / data[1];
                                oslitems[item_id].row.unit_price = valor / data[1];
                            }
                        }
                        if (data[2] !== undefined) {
                            oslitems[item_id].row.discount = data[2];
                        }
                        if (data[3] !== undefined) {
                            oslitems[item_id].row.tax_rate = data[3];
                            oslitems[item_id].tax_rate = data[4];
                            oslitems[item_id].row.tax_exempt_customer = true;
                        }
                    }
                } else {
                    oslitems[item_id].row.real_unit_price = oslitems[item_id].row.real_unit_price ? oslitems[item_id].row.real_unit_price : 0 ;
                    oslitems[item_id].row.unit_price = oslitems[item_id].row.unit_price ? oslitems[item_id].row.unit_price : 0;
                }
                oslitems[item_id].row.qty = new_qty;
                localStorage.setItem('oslitems', JSON.stringify(oslitems));
                $('#payment').prop('disabled', true);
                loadItems(index);
            }
        });
    // }
}
// Termina wappsiPreciosUnidad

function customer_special_discount(){

    var customer_id = $('#oslcustomer').val();
    if (!localStorage.getItem('oslcustomerspecialdiscount')) {
        $.ajax({
            url : site.base_url+"customers/get_customer_special_discount/"+customer_id,
            dataType : "JSON"
        }).done(function(data){
            loadItems();
            localStorage.setItem('oslcustomerspecialdiscount', data.special_discount);
            command: toastr.warning('Cliente con descuento especial asignado', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "4000",
                "extendedTimeOut": "1000",
            });
        });
    }

}

$(document).on('change', '#paid_by_1', function(){
    if ($(this).val() == 'Credito') {
        $('.div_payment_term').fadeIn();
    } else if ($(this).val() != 'Credito') {
        $('.div_payment_term').fadeOut();
    }
});



///SERIAL MODAL


$(document).on('click', '.send_serial_modal', function(){
    validar_modal_serial();
});

$(document).on('click', '.continue_serial_modal', function(){
    validar_modal_serial(true);
});


function validar_modal_serial(cont = false){
    if ($('#serialModal_form').valid()) {
        serial = $('#serialModal_serial').val();
        meters = $('#serialModal_meters').val();
        item_id = $('#serialModal_product_id').val();
        option_id = $('#serialModal_option_id').val();
        item = JSON.parse(localStorage.getItem('modalSerial_item'));
        item.row.serialModal_serial = serial;
        item.row.qty = meters;
        item.row.quantity = meters;
        item.row.base_quantity = meters;
        item.row.delivered_qty = 0;
        item.row.pending_qty = meters;
        item.row.bill_qty = 0;
        item.row.option = option_id;
        add_invoice_item(item, true, cont);
    }
}

$(document).on('keyup', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal, e);
});

$(document).on('change', '#serialModal_serial', function(e){
    serialModal = $(this).val();
    validate_serial(serialModal);
});

$(document).on('keyup', '#serialModal_meters', function(e){
    if (e.keyCode == 13) {
        $('.send_serial_modal').trigger('click');
    }
});

function validate_serial(serialModal, e = null){
    if ( e == null || e != null && e.keyCode == 13) {
        serial_item = JSON.parse(localStorage.getItem('modalSerial_item'));
        oslwarehouse = $('#oslwarehouse').val();
        $.ajax({
            url : site.base_url + 'products/get_product_serial_variant_quantity/'+serial_item.row.id+'/'+serialModal+'/'+oslwarehouse,
            dataType : 'JSON',
        }).done(function(data){
            if (data.id  !== undefined) {
                $('#serialModal_meters').val(data.quantity);
                $('#serialModal_meters').prop('max', data.quantity);
                $('#serialModal_option_id').val(data.option_id);
                $('#serialModal_meters').focus();
            } else {
                $('#serialModal_serial').val('');
                $('#serialModal_option_id').val('');
                 command: toastr.warning('El serial indicado no está registrado para este producto', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        });
    }
}

$(document).on('click', '#random_num', function(e){
    $('#serialModal_serial').select();
});

function set_customer_payment(){
    $('#paid_by_1').select2('val', 'cash').trigger('change');
    var customer_id = $('#oslcustomer').val(); 
    $.ajax({
        url : site.base_url+"customers/get_payment_type/"+customer_id,
        data : {
            day_delivery_time : $('#day_delivery_time').val(),
        },
        dataType : "JSON"
    }).done(function(data) {
        setTimeout(function() {
            localStorage.setItem('amount_ordered', data.amount_ordered);
            if (billers_data[localStorage.getItem('oslbiller')].min_sale_amount > 0) {
                loadItems();
            }
            if (data.payment_type == 0) {
                $('#paid_by_1').val('Credito').trigger('change');
            } else if (data.payment_type == 1) {
                $('#paid_by_1').select2('val', 'cash').trigger('change');
            }
        }, 850);
    });
}

$(document).on('click', '#edit_sale', function(){
    if ($('#form_edit_order_sale').valid()) {
        $('#form_edit_order_sale').submit();
    }
});

$(document).on('click', '#add_sale', function(){
    if ($('#form_add_order_sale').valid()) {
        $('#form_add_order_sale').submit();
        const wrapper = document.getElementById('wrapper'); 
        (function ( ) {
            wrapper.classList.add('wrapper-disabled');
            Swal.fire({
                title: 'Guardando',
                html: '<div class="dots-container"><div class="dot"></div><div class="dot"></div><div class="dot"></div></div>Por favor, espere un momento.',
                icon: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                allowOutsideClick: false,
                customClass: {
                    popup: 'swal2-popup'
                },
                backdrop: true
            });
        }) (  );
        setTimeout(function() {
            location.href = site.base_url+"sales/orders";
        }, 2000);
    }
});

$(document).on('change', '#day_delivery_time', function(){
    if (site.settings.management_order_sale_delivery_time == 1) {
        $('#delivery_time').empty();
        $('#delivery_time').append(new Option('Cargando...', '')).select2();
        $.ajax({
            url : site.base_url+'sales/get_delivery_times',
            data : {
                day : $('#day_delivery_time').val(),
                address_id : $('#oslcustomerbranch').val(),
            } ,
            type : 'get',
            dataType : 'JSON'
        }).done(function(data){
            if (data.error == 0) {
                $('#delivery_time').empty();
                $('#delivery_time').append(new Option('Seleccione...', '')).select2();
                $.each(data.days, function(index, day){
                    $('#delivery_time').append(new Option(day.time_1+" a "+day.time_2, day.id));
                    if (day.disabled == true) {
                        if (typeof(delivery_time_id) != "undefined" && day.id == delivery_time_id) {
                            $('#delivery_time option[value="'+day.id+'"]').prop('selected', true);
                        } else {
                            $('#delivery_time option[value="'+day.id+'"]').prop('disabled', true);
                        }
                    } else {
                        if (typeof(delivery_time_id) != "undefined" && day.id == delivery_time_id) {
                            $('#delivery_time option[value="'+day.id+'"]').prop('selected', true);
                        }
                    }
                });
                $('#delivery_time').select2();
            }
        });
    }
});

function set_biller_warehouses_related(){
    warehouses_related = billers_data[$('#oslbiller').val()].warehouses_related;
    if (warehouses_related) {
        $('#oslwarehouse option').each(function(index, option){
            $(option).prop('disabled', false);
            if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                $(option).prop('disabled', true);
            }
        });
        setTimeout(function() {
            $('#oslwarehouse').select2();
        }, 850);
    }
}

function updateQuantityItems(warehouseId)
{
    let items = localStorage.getItem('oslitems')
    if (items) {
        items = JSON.parse(items)
        $.each(items, function () {
            productId = this.item_id
            options = this.options
            if (options) {
                $.each(options, function() {
                    this.quantity = getQuantitiesProductInWarehouse(warehouseId, productId, this.id)
                })
            } else {
                this.row.quantity = getQuantitiesProductInWarehouse(warehouseId, productId)
            }
        })
    }
    localStorage.setItem('oslitems', JSON.stringify(items));
    loadItems()
}

function getQuantitiesProductInWarehouse(warehouseId, productId, optionId = null)
{
    let quantity = 0
    $.ajax({
        type: "get",
        async: false,
        url: site.base_url + 'products/getQuantitiesProductInWarehouseAjax',
        data: {
            'warehouseId' : warehouseId,
            'productId' : productId,
            'optionId' : optionId,
        },
        dataType: "json",
        success: function (respuesta) {
            quantity = respuesta.quantity;
        }
    });
    return quantity;
}

function resizeWindow(){
    $('#add_item').addClass('btn-pos-product_movil');
    // .div_hidden_movile -> oculta el div del input si dicho input contiene solo una opcion de seleccion
    if (user_permissions) {
        if ($(window).width() < 800) { 
            /**sucursal **/
            let count_billers = $('#oslbiller').find('option').length;
            if (count_billers == 1) {
                $('#oslbiller').closest('div').addClass('div_hidden_movile');
            }
            /**sucursal **/
  
            /** document type **/
            let count_document_type = $('#document_type_id').find('option').length;
            if (count_document_type == 1) {
                $('#document_type_id').closest('div').addClass('div_hidden_movile');
            }
            /** document type **/
   
            /** customer branch **/
            $('#oslcustomerbranch').on('change', function(){
                let count_customer_branch = $('#oslcustomerbranch').find('option').filter(function() {
                    return $(this).val() !== ''; // Filtra las opciones que no tienen valor vacío
                }).length;
                if (count_customer_branch == 1) {
                    $('#oslcustomerbranch').closest('div').addClass('div_hidden_movile');
                }else{
                    $('#oslcustomerbranch').closest('div').removeClass('div_hidden_movile');
                }
            })
            /** customer branch **/
  
            /** seller **/
            let sellerElement = $('#oslseller');
            if (sellerElement.is('input')) {
                let validateSeller = $('#oslseller').val();
                if (validateSeller != '') {
                    $('#oslseller').closest('div').addClass('div_hidden_movile');
                }
            }
            else if(sellerElement.is('select')){
                let count_seller = $('#oslseller').find('option').filter(function() {
                    return $(this).val() !== ''; // Filtra las opciones que no tienen valor vacío
                }).length;
                if (count_seller == 1) {
                    $('#oslseller').closest('div').addClass('div_hidden_movile');
                }else{
                    $('#oslseller').closest('div').removeClass('div_hidden_movile');
                }
            }
            /** seller **/

            $('#table-group').addClass('div_container_item_movile');
            $('div#table-controls.ps-container').css('min-height', '250px !important');

        } else {
            /** sucursal **/
            $('#oslbiller').closest('div').removeClass('div_hidden_movile');  // clase para ocultar si solo existe uno
            /** sucursal **/
  
            /** document type **/
            $('#document_type_id').closest('div').removeClass('div_hidden_movile');
            /** document type **/

            /** customer branch **/
            $("#oslcustomerbranch").closest('div').removeClass('input_mobile');
            /** customer branch **/
    
            /** seller **/
            $('#oslseller').closest('div').removeClass('div_hidden_movile');
            /** seller **/
    
            $('#table-group').removeClass('div_container_item_movile');
            $('#table-controls').removeClass('div_container_item_movile');
        }
    } 
}


function update_current_gram_price(current_price_gram, type){
    swal({
        title: "¡Actualización disponible!",
        text: "Se ha encontrado una actualización al valor del gramo. ¿Desea actualizar el precio de los productos seleccionados?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Aceptar",
        closeOnConfirm: true
    }, function () {
        positems = JSON.parse(localStorage.getItem('oslitems'));
        for (const key in positems) {
            if (positems.hasOwnProperty(key)) {
                const innerObject = positems[key];
                for (const keyRow in innerObject) {
                    if (keyRow == 'row') {
                        if (innerObject.hasOwnProperty(keyRow)) {
                            const innerObjectRow = innerObject[keyRow];
                            for (const keyRowData in innerObjectRow) {
                                if (innerObjectRow.hasOwnProperty(keyRowData)) {
                                    const innerObjectRowData = innerObjectRow[keyRowData];
                                    if (keyRowData == 'based_on_gram_value') {
                                        if (positems[key][keyRow][keyRowData] == type) {
                                            positems[key][keyRow]['current_price_gram'] = current_price_gram;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        localStorage.setItem('oslitems', JSON.stringify(positems));
        loadItems();
    });
    localStorage.setItem('gram_current_price', current_price_gram);
}

function validate_price_minimun(product_id){
    let price = false;
    $.ajax({
        type: "get",
        url : site.base_url + "pos/get_price_minimun/"+product_id,
        dataType: "json",
        async : false,
    })
    .done(function(data){
        price = data.price;
        price;
    });
    return price;
}