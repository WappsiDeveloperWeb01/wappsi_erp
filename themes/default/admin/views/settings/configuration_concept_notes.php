<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="concepts_notes_table" class="table table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th></th>
                                            <th><?= lang('concept_notes_type') ?></th>
                                            <th><?= lang('concept_notes_dian_code') ?></th>
                                            <th><?= lang('concept_notes_dian_description') ?></th>
                                            <th><?= lang('concept_notes_description') ?></th>
                                            <th><?= lang('concept_notes_tax') ?></th>
                                            <th><?= lang('concept_notes_tax_amount_ledger_account') ?></th>
                                            <th><?= lang('concept_notes_tax_base_ledger_account') ?></th>
                                            <th><?= lang('concept_notes_status') ?></th>
                                            <th><?= lang('concept_notes_actions') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }

    $(document).ready(function() {
        $('#concepts_notes_table').dataTable({
            aaSorting: [
                [1, "desc"],
                [2, "desc"]
            ],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page; ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/get_notes_concepts') ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                
            },
            aoColumns: [{
                    'bVisible': false
                },
                {
                    className: "text-center"
                },
                {
                    className: "text-center"
                },
                null,
                null,
                {
                    className: "text-center"
                },
                null,
                null,
                {
                    className: "text-center"
                },
                {
                    bSortable: false,
                    sWidth: '65px',
                    className: 'text-center'
                },
            ],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/add_note_concepts') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>');

                $('[data-toggle-second="tooltip"]').tooltip();
            }
        });
    });
</script>