<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('reports/user_activities', 'id="productFilterForm"'); ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">

                                    <div class="col-sm-2">
                                        <?= lang('start_date', 'start_date') ?>
                                        <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->sma->hrld(date('Y-01-01 00:00:00')) ?>" class="form-control datetime">
                                    </div>
                                    <div class="col-sm-2">
                                        <?= lang('end_date', 'end_date') ?>
                                        <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $this->sma->fld($_POST['end_date']) : $this->sma->hrld(date('Y-m-d H:i:s')) ?>" class="form-control datetime">
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('user', 'user') ?>
                                            <?php 
                                            $usarr = [];
                                            $usarr[''] = lang('select');
                                            foreach ($users as $user_key => $user) {
                                                $usarr[$user_key] = $user->first_name." ".$user->last_name;
                                            }
                                            echo form_dropdown('user', $usarr, $this->input->post('user'), 'class="form-control tip select" id="user" style="width:100%;"');
                                             ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('type', 'type') ?>
                                            <?php 
                                            // se modifica para que aparezca con el nuevo orden 
                                            $typearr = [    ''  =>  lang('select'), 
                                                            7   =>  'Ingreso',
                                                            3   =>  'Adición',
                                                            1   =>  'Edición',
                                                            2   =>  'Eliminación',
                                                            4   =>  'Consulta',
                                                            5   =>  'Cierre contable' 
                                                        ];
                                            echo form_dropdown('type', $typearr, $this->input->post('type'), 'class="form-control tip select" id="type" style="width:100%;"');
                                             ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('module', 'module') ?>
                                            <?php 
                                            $modulearr = [''=>lang('select'),
                                                        'auth' => lang('auth'),
                                                        'customers' => lang('customers'),
                                                        'group_product_prices' => lang('group_product_prices'),
                                                        'payroll' => lang('payroll'),
                                                        'pos_settings' => lang('pos_settings'),
                                                        'products' => lang('products'),
                                                        'purchases' => lang('purchases'),
                                                        'reports' => lang('reports'),
                                                        'settings' => lang('settings'),
                                                        'system_settings' => lang('system_settings')];
                                            echo form_dropdown('module', $modulearr, $this->input->post('module'), 'class="form-control tip select" id="module" style="width:100%;"');
                                             ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('table', 'table') ?>
                                            <?php 
                                            $tablearr = [''=>lang('select'),
                                            'companies' => 'companies',
                                                        'payroll_settings' => 'payroll_settings',
                                                        'pos_settings' => 'pos_settings',
                                                        'products' => 'products',
                                                        'product_prices' => 'product_prices',
                                                        'purchases' => 'purchases',
                                                        'sales' => 'sales',
                                                        'settings' => 'settings',
                                                        'unit_prices' => 'unit_prices',
                                                        'users' => 'users'];
                                            echo form_dropdown('table', $tablearr, $this->input->post('table'), 'class="form-control tip select" id="table" style="width:100%;"');
                                             ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><i class="fa fa-lg fa-chevron-down"></i></button>
                                    <button class="btn btn-primary new-button" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" style="display:none;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?= lang('description', 'description') ?> <em>Encerrar entre comillas para buscar por partes, ej: "el precio del producto" "para la unidad (PAQ 100UND)"</em>
                                            <textarea name="description" class="form-control skip"><?= $this->input->post('description') ?></textarea>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="CURData" class="table table-hover table-condensed reports-table">
                                    <thead>
                                    <tr>
                                        <th><?= lang('id') ?></th>
                                        <th><?= lang('date') ?></th>
                                        <th><?= lang('user') ?></th>
                                        <th><?= lang('type') ?></th>
                                        <th><?= lang('module') ?></th>
                                        <th><?= lang('table') ?></th>
                                        <th><?= lang('description') ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="7" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    <?php 

        $v = "";
        if ($this->input->post('start_date')) {
            $v .= "&start_date=" . $this->input->post('start_date');
        }
        if ($this->input->post('end_date')) {
            $v .= "&end_date=" . $this->input->post('end_date');
        }
        if ($this->input->post('user')) {
            $v .= "&user=" . $this->input->post('user');
        }
        if ($this->input->post('type')) {
            $v .= "&type=" . $this->input->post('type');
        }
        if ($this->input->post('module')) {
            $v .= "&module=" . $this->input->post('module');
        }
        if ($this->input->post('table')) {
            $v .= "&table=" . $this->input->post('table');
        }
        if ($this->input->post('description')) {
            $v .= "&description=" . $this->input->post('description');
        }

     ?>

    $(document).ready(function () {
        function tax_type(x) {
            return (x == 1) ? "<?=lang('percentage')?>" : "<?=lang('fixed')?>";
        }

        oTable = $('#CURData').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/getUserActivities/?v=1'.$v) ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": "<?= $this->input->post('start_date') ?>"
                }, {
                    "name": "end_date",
                    "value": "<?= $this->input->post('end_date') ?>"
                }, {
                    "name": "user",
                    "value": "<?= $this->input->post('user') ?>"
                }, {
                    "name": "type",
                    "value": "<?= $this->input->post('type') ?>"
                }, {
                    "name": "module",
                    "value": "<?= $this->input->post('module') ?>"
                }, {
                    "name": "table",
                    "value": "<?= $this->input->post('table') ?>"
                }, {
                    "name": "description",
                    "value": '<?= $this->input->post('description') ?>'
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex)
            {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "ua_link";
                return nRow;
            },
            fnDrawCallback: function() {
                $('.actionsButtonContainer').html('<div class="pull-right dropdown">'+
                    '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">'+
                        '<li>'+
                            '<a href="#" id="xls"><i class="fa fa-file-excel"></i> <?= lang('export_to_excel') ?></a>'+
                        '</li>'+
                    '</ul>'+
                '</div>');
            },
            "aoColumns": [
                            null, 
                            null, 
                            null, 
                            {"mRender": ua_type },
                            {"mRender": language },
                            {"mRender": language },
                            {"mRender": ua_max_description },
                        ],
        });


        setTimeout(function() {


            $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('system_settings/getUserActivities/xls/?v=1'.$v)?>";
            return false;
        });
        }, 850);

    <?php if ($this->input->post('description')): ?>
        setTimeout(function() {
            $('.collapse-link').click();
        }, 850);
    <?php endif ?>
    });
</script>
