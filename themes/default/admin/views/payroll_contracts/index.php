<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="employees_table" class="table table-hover" style="width: 100%">
                                    <thead>
                                        <tr class="primary">
                                            <th></th>
                                            <th><?= lang("internal_code"); ?></th>
                                            <th><?= lang("payroll_contracts_name_employee"); ?></th>
                                            <th><?= lang("type"); ?></th>
                                            <th><?= lang("start"); ?></th>
                                            <th><?= lang("end"); ?></th>
                                            <th><?= lang("payroll_contracts_biller"); ?></th>
                                            <th><?= lang("area"); ?></th>
                                            <th><?= lang("professional_position"); ?></th>
                                            <th><?= lang("payroll_contracts_status"); ?></th>
                                            <th></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        sessionStorage.clear();

        _name_months = {1:"Enero", 2:"Febrero", 3:"Marzo", 4:"Abril", 5:"Mayo", 6:"Junio", 7:"Julio", 8:"Agosto", 9:"Septiembre", 10:"Octubre", 11:"Noviembre", 12:"Diciembre"};

        load_datatable();

        $(document).on('click', '.contract_details td:not(:first-child, :last-child)', function() {
            var contract_id = $(this).parent('.contract_details').data('contract_id');
            var employee_id = $(this).parent('.contract_details').data('employee_id');

            $('#myModal').modal({remote: site.base_url + 'payroll_contracts/see/'+ contract_id +'/'+ employee_id});
            $('#myModal').modal('show');
        });
    });

    function load_datatable() {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#employees_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('payroll_contracts/get_datatables') ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function (sSource, aoData, fnCallback)
            {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (row, data, displayIndex)
            {
                var contract_id = data[0];
                var employee_id = data[10];
                row.dataset.contract_id = contract_id;
                row.dataset.employee_id = employee_id;
                row.className = "contract_details";
                return row;
            },
            drawCallback: function(settings)
            {
                $('.actionsButtonContainer').html(`<a href="<?= admin_url('payroll_contracts/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>
                <div class="dropdown pull-right">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('payroll_contracts/getPayrollContractsReport') ?>">
                                <i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>
                            </a>
                        </li>
                    </ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').not('.skip').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%'
                });
            },
            aoColumns: [
                { visible: false },
                { className: 'text-center' },
                { className: 'text-capitalize' },
                null,
                { className: 'text-center' },
                { className: 'text-center' },
                null,
                null,
                null,
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        var end_date = row[5];
                        var settlement_date = row[11];

                        if (settlement_date === null || settlement_date == '0000-00-00') {
                            if (end_date == '0000-00-00') {
                                status = '<span class="label label-primary"">Activo</span>';
                            } else if (end_date < '<?= date("Y-m-d") ?>') {
                                status = '<span class="label label-danger"">Finalizado</span>';
                            } else {
                                status = '<span class="label label-primary"">Activo</span>';
                            }
                        } else {
                            status = '<span class="label label-warning"">Terminado</span>';
                        }

                        return status;
                    }
                },
                { visible: false },
                null,
                {
                    bSortable: false,
                    render: function(data, type, row) {
                        var actions_buttons = $(data);
                        var end_date = row[5];
                        var settlement_date = row[11];

                        if (settlement_date === null || settlement_date == '0000-00-00') {
                            if (end_date == '0000-00-00') {
                                actions_buttons.find('.dropdown-menu').find('.add_contract').remove();
                            } else if (end_date < '<?= date("Y-m-d") ?>') {
                                <?php if (!$this->Owner) { ?>
                                    actions_buttons.find('.dropdown-menu').find('.edit_contract').remove();
                                <?php } ?>
                            } else {
                                actions_buttons.find('.dropdown-menu').find('.add_contract').remove();
                            }
                        } else {
                            <?php if (!$this->Owner) { ?>
                                actions_buttons.find('.dropdown-menu').find('.edit_contract').remove();
                            <?php } ?>
                        }

                        var data = actions_buttons.prop('outerHTML');
                        return data;
                    }
                }
            ]
        });
    }
</script>