<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documents_types extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

		if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if ($this->Supplier || $this->Customer) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

		$this->Payroll_settings = $this->site->get_payroll_setting();

        $this->load->admin_model('documentsTypes_model');
	}

	public function get_document_type_credit_note()
	{
		$branch_id = $this->input->post("branch_id");
		$module = $this->input->post("module");
		$is_electronic_invoice = $this->input->post("is_electronic_invoice");

		$credit_note_type_document = $this->documentsTypes_model->get_credit_note_type_document($branch_id, $module, $is_electronic_invoice);

		echo json_encode($credit_note_type_document);
	}

	public function get_document_type_debit_note()
	{
		$branch_id = $this->input->post("branch_id");
		$pos = $this->input->post("pos");

        if ($pos == 0) {
            $module = [27];
        } else if ($pos == 1) {
            $module = [26];
        } else {
            $module = [26, 27];
        }

		$credit_note_type_document = $this->documentsTypes_model->get_debit_note_type_document($branch_id, $module);

		echo json_encode($credit_note_type_document);
	}

}

/* End of file Documents_types.php */
/* Location: .//opt/lampp/htdocs/wappsi/app/controllers/admin/Documents_types.php */