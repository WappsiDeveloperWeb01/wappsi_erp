$(window).ready(function () {
    $("#loading").fadeOut("slow");
});
function cssStyle() {
    if ($.cookie('sma_style') == 'light') {
        $('link[href="'+site.assets+'styles/blue.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/blue.css"]').remove();
        $('<link>')
        .appendTo('head')
        .attr({type: 'text/css', rel: 'stylesheet'})
        .attr('href', site.assets+'styles/light.css');
    }
    else if ($.cookie('sma_style') == 'blue') {
        $('link[href="'+site.assets+'styles/light.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/light.css"]').remove();
        $('<link>')
        .appendTo('head')
        .attr({type: 'text/css', rel: 'stylesheet'})
        .attr('href', ''+site.assets+'styles/blue.css');
    }
    else {
        $('link[href="'+site.assets+'styles/light.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/blue.css"]').attr('disabled', 'disabled');
        $('link[href="'+site.assets+'styles/light.css"]').remove();
        $('link[href="'+site.assets+'styles/blue.css"]').remove();
    }

    if($('#sidebar-left').hasClass('minified')) {
        $.cookie('sma_theme_fixed', 'no', { path: '/' });
        $('#content, #sidebar-left, #header').removeAttr("style");
        $('#sidebar-left').removeClass('sidebar-fixed');
        $('#content').removeClass('content-with-fixed');
        $('#fixedText').text('Fixed');
        $('#main-menu-act').addClass('full visible-md visible-lg').show();
        $('#fixed').removeClass('fixed');
    } else {
        if(site.settings.rtl == 1) {
            $.cookie('sma_theme_fixed', 'no', { path: '/' });
        }
        if ($.cookie('sma_theme_fixed') == 'yes') {
            $('#content').addClass('content-with-fixed');
            $('#sidebar-left').addClass('sidebar-fixed').css('height', $(window).height()- 80);
            $('#header').css('position', 'fixed').css('top', '0').css('width', '100%');
            $('#fixedText').text('Static');
            $('#main-menu-act').removeAttr("class").hide();
            $('#fixed').addClass('fixed');
            $("#sidebar-left").css("overflow","hidden");
            $('#sidebar-left').perfectScrollbar({suppressScrollX: true});
        } else {
            $('#content, #sidebar-left, #header').removeAttr("style");
            $('#sidebar-left').removeClass('sidebar-fixed');
            $('#content').removeClass('content-with-fixed');
            $('#fixedText').text('Fixed');
            $('#main-menu-act').addClass('full visible-md visible-lg').show();
            $('#fixed').removeClass('fixed');
            $('#sidebar-left').perfectScrollbar('destroy');
        }
    }
    widthFunctions();
}
$('#csv_file').change(function(e) {
    v = $(this).val();
    if (v != '') {
        var validExts = new Array(".csv");
        var fileExt = v;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
        if (validExts.indexOf(fileExt) < 0) {
            e.preventDefault();
            bootbox.alert("Invalid file selected. Only .csv file is allowed.");
            $(this).val(''); $(this).fileinput('clear');
            $('form[data-toggle="validator"]').bootstrapValidator('updateStatus', 'csv_file', 'NOT_VALIDATED');
            return false;
        }
        else
            return true;
    }
});

$(document).ready(function() {
    //CÓDIGO PEGAR DE PORTAPAPELES
    setTimeout(function() {
        shortcut.add("F1",
            function() {
                    navigator.clipboard.readText()
                    .then(texto => {
                        $(':focus').val(texto);
                    })
                    .catch(error => {
                        console.log("Hubo un error: ", error);
                    });
                },
                { 'type':'keydown', 'propagate':false, 'target':document}
            );
    }, 850);
    $("#suggest_product").autocomplete({
        source: function(request, response){
            $.ajax({
                type: 'get',
                url: site.base_url+'reports/suggestions',
                dataType: "json",
                data: {
                    term: request.term,
                    type: $("#suggest_product").data('type') ? $("#suggest_product").data('type') : '',
                },
                success: function (data) {
                    $(this).removeClass('ui-autocomplete-loading');
                    response(data);
                }
            });
        },
        select: function (event, ui) {
            $('#report_product_id').val(ui.item.id);
        },
        minLength: 1,
        autoFocus: false,
        delay: 250,
        response: function (event, ui) {
            if (ui.content != null && ui.content.length == 1 && ui.content[0].id != 0) {
                ui.item = ui.content[0];
                $(this).val(ui.item.label);
                $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                $(this).autocomplete('close');
                $(this).removeClass('ui-autocomplete-loading');
            } else if(ui.content == null ){
                command: toastr.error('No se encontraron coincidencias', 'Sin resultados', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
            }
        },
    });
    $(document).on('blur', '#suggest_product', function(e) {
        if (! $(this).val()) {
            $('#report_product_id').val('');
        }
    });

    $("#suggest_variant").autocomplete({
        source: function(request, response){
            $.ajax({
                type: 'get',
                url: site.base_url+'reports/suggestions_variant',
                dataType: "json",
                data: {
                    term: request.term,
                    type: $("#suggest_variant").data('type') ? $("#suggest_variant").data('type') : '',
                },
                success: function (data) {
                    $(this).removeClass('ui-autocomplete-loading');
                    response(data);
                }
            });
        },
        select: function (event, ui) {
            $('#report_variant_id').val(ui.item.id);
        },
        minLength: 1,
        autoFocus: false,
        delay: 250,
        response: function (event, ui) {
            if (ui.content != null && ui.content.length == 1 && ui.content[0].id != 0) {
                ui.item = ui.content[0];
                $(this).val(ui.item.label);
                $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                $(this).autocomplete('close');
                $(this).removeClass('ui-autocomplete-loading');
            } else if(ui.content == null ){
                command: toastr.error('No se encontraron coincidencias', 'Sin resultados', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                });
            }
        },
    });
    $(document).on('blur', '#suggest_variant', function(e) {
        if (! $(this).val()) {
            $('#report_variant_id').val('');
        }
    });

    $(document).on('click', '#random_num, .random_num', function(){
        input = $(this).parent('.input-group').children('input');
        input.val(generateCardNo(8)).trigger('change').prop('readonly', false);
        input_consecutive_setted = input.parent('.input-group').parent('.form-group').children('.code_consecutive_setted');
        input_consecutive_setted.val(0);
    });
    $('#toogle-customer-read-attr').click(function () {
        var icus = $(this).closest('.input-group').find("input[name='customer']");
        var nst = icus.is('[readonly]') ? false : true;
        icus.select2("readonly", nst);
        return false;
    });
    $('.top-menu-scroll').perfectScrollbar();
    $('#fixed').click(function(e) {
        e.preventDefault();
        if($('#sidebar-left').hasClass('minified')) {
            bootbox.alert('Unable to fix minified sidebar');
        } else {
            if($(this).hasClass('fixed')) {
                $.cookie('sma_theme_fixed', 'no', { path: '/' });
            } else {
                $.cookie('sma_theme_fixed', 'yes', { path: '/' });
            }
            cssStyle();
        }
    });

    set_number_mask();
    $('.fileinput').fileinput();

    $(document).on('click', '.date', function(e){
        if ($(this).is(':disabled')) {
            e.preventDefault();
            return false;
        }
    });

    set_warehouse_search();

    setTimeout(function() {
        $('.dataTables_filter input').attr("placeholder", lang.search);
        $('#date_records_filter_dh').trigger('change');
        $('#date_records_filter_h').trigger('change');
    }, 600);

    set_payment_method_billers();

    $(document).on('change', 'select[name="biller"]', function(){
        set_payment_method_billers();
    });
    $('[data-toggle="tooltip"]').tooltip();
});

$(document).on('change', '#date_records_filter_dh, #filter_year_dh', function(){
    if (site.settings.big_data_limit_reports == 1) {
        if ($('#date_records_filter_dh').val() == 5) {
            fv_start_year = $('#filter_year_dh').val();
            f_start_year = moment().format(fv_start_year+"-01-01");
            f_end_year = moment().format(fv_start_year+"-12-31");
            $('.datetime').datetimepicker('remove');
            $('.datetime').datetimepicker({format: site.dateFormats.js_ldate, fontAwesome: true, language: 'sma', weekStart: 1, todayBtn: 1, autoclose: 1, todayHighlight: 1, startView: 2, forceParse: 0, startDate:f_start_year, endDate:f_end_year});
            $('.datetime').val('');
        }
    }
});

$(document).on('change', '#date_records_filter_h, #filter_year_h', function(){
    if (site.settings.big_data_limit_reports == 1) {
        if ($('#date_records_filter_h').val() == 5) {
            fv_start_year = $('#filter_year_h').val();
            f_start_year = fv_start_year+"-01-01";
            f_end_year = fv_start_year+"-12-31";
            $('input[name="start_date"]').prop('min', f_start_year).prop('max', f_end_year);
            $('input[name="end_date"]').prop('min', f_start_year).prop('max', f_end_year);
        }
    }
});

var dINPUT = new Date();
var full_year = dINPUT.getFullYear();
var min_input_date = moment().format(site.settings.system_start_date);
var max_input_date = moment().format(full_year+'-12-31');
if (site.año_actual_movimientos && site.settings.years_database_management == 2) {
    min_input_date = moment().format(site.año_actual_movimientos+'-01-01');
    max_input_date = moment().format(site.año_actual_movimientos+'-12-31');
}
if (site.settings.years_database_management == 1) {
    $(document).on('change', 'input[name="date"]:not(.skip)', function(){
        selected_date = datetimepicker_format($('input[name="date"]').val());
        actual_min_date = moment().format(dINPUT.getFullYear()+'-01-01');
        actual_max_date = moment().format(dINPUT.getFullYear()+'-12-31');
        if (selected_date < actual_min_date || selected_date > actual_max_date) {
            bootbox.confirm("La fecha escogida es diferente al año fiscal actual; Se contabilizará según el año de la fecha seleccionada.", function(result) {
                if(result == true) {
                    if ($('#myModal').hasClass('in')) {
                        setTimeout(function() {
                            $('#myModal').modal('show');
                        }, 1200);
                    }
                } else {
                    if ($('#myModal').hasClass('in')) {
                        setTimeout(function() {
                            $('#myModal').modal('show');
                        }, 1200);
                    }
                }
            });
        }
    });
}

function set_payment_method_billers(){
    $('.paid_by option').prop('disabled', true).addClass('not-display');
    setTimeout(function() {
        $('.paid_by option').each(function(index, option){
            biller_id = $('select[name="biller"]').val() ? $('select[name="biller"]').val() : $('select[name="posbiller"]').val();
            if (jQuery.inArray(biller_id, payment_methods_billers[$(option).val()]) !== -1 || payment_methods_billers[$(option).val()] == undefined) {
                $(option).prop('disabled', false).removeClass('not-display');
            }
            $('li.select2-disabled').css('display', 'none !imporant');
            // $('.paid_by').select2('val', $('.paid_by option:not([disabled]):selected').eq(1).val());
        });
        $("select.paid_by").select2({
            formatResult: payment_method_icons,
            formatSelection: payment_method_icons,
            escapeMarkup: function (m) {
              return m;
            }
        });
    }, 850);
}

function widthFunctions(e) {
    var l = $("#sidebar-left").outerHeight(true),
    c = $("#content").height(),
    co = $("#content").outerHeight(),
    h = $("header").height(),
    f = $("footer").height(),
    wh = $(window).height(),
    ww = $(window).width();
    if (ww < 992) {
        $("#main-menu-act").removeClass("minified").addClass("full").find("i").removeClass("fa-angle-double-right").addClass("fa-angle-double-left");
        $("body").removeClass("sidebar-minified");
        $("#content").removeClass("sidebar-minified");
        $("#sidebar-left").removeClass("minified")
        if ($.cookie('sma_theme_fixed') == 'yes') {
            $.cookie('sma_theme_fixed', 'no', { path: '/' });
            $('#content, #sidebar-left, #header').removeAttr("style");
            $("#sidebar-left").css("overflow-y","visible");
            $('#fixedText').text('Fixed');
            $('#main-menu-act').addClass('full visible-md visible-lg').show();
            $('#fixed').removeClass('fixed');
            $('#sidebar-left').perfectScrollbar('destroy');
        }
    }
    if (ww < 998 && ww > 750) {
        $('#main-menu-act').hide();
        $("body").addClass("sidebar-minified");
        $("#content").addClass("sidebar-minified");
        $("#sidebar-left").addClass("minified");
        $(".dropmenu > .chevron").removeClass("opened").addClass("closed");
        $(".dropmenu").parent().find("ul").hide();
        $("#sidebar-left > div > ul > li > a > .chevron").removeClass("closed").addClass("opened");
        $("#sidebar-left > div > ul > li > a").addClass("open");
        $('#fixed').hide();
    }
    if (ww > 1024 && $.cookie('sma_sidebar') != 'minified') {
        $('#main-menu-act').removeClass("minified").addClass("full").find("i").removeClass("fa-angle-double-right").addClass("fa-angle-double-left");
        $("body").removeClass("sidebar-minified");
        $("#content").removeClass("sidebar-minified");
        $("#sidebar-left").removeClass("minified");
        $("#sidebar-left > div > ul > li > a > .chevron").removeClass("opened").addClass("closed");
        $("#sidebar-left > div > ul > li > a").removeClass("open");
        $('#fixed').show();
    }
    if ($.cookie('sma_theme_fixed') == 'yes') {
        $('#content').addClass('content-with-fixed');
        $('#sidebar-left').addClass('sidebar-fixed').css('height', $(window).height()- 80);
    }
    if (ww > 767) {
        wh - 80 > l && $("#sidebar-left").css("min-height", wh - h - f - 30);
        wh - 80 > c && $("#content").css("min-height", wh - h - f - 30);
    } else {
        $("#sidebar-left").css("min-height", "0px");
        $(".content-con").css("max-width", ww);
    }
    //$(window).scrollTop($(window).scrollTop() + 1);
}

jQuery(document).ready(function(e) {
    window.location.hash ? e('#myTab a[href="' + window.location.hash + '"]').tab('show') : e("#myTab a:first").tab("show");
    e("#myTab2 a:first, #dbTab a:first").tab("show");
    e("#myTab a, #myTab2 a, #dbTab a").click(function(t) {
        t.preventDefault();
        e(this).tab("show");
    });
    e('[rel="popover"],[data-rel="popover"],[data-toggle="popover"]').popover();
    e("#toggle-fullscreen").button().click(function() {
        var t = e(this),
        n = document.documentElement;
        if (!t.hasClass("active")) {
            e("#thumbnails").addClass("modal-fullscreen");
            n.webkitRequestFullScreen ? n.webkitRequestFullScreen(window.Element.ALLOW_KEYBOARD_INPUT) : n.mozRequestFullScreen && n.mozRequestFullScreen()
        } else {
            e("#thumbnails").removeClass("modal-fullscreen");
            (document.webkitCancelFullScreen || document.mozCancelFullScreen || e.noop).apply(document)
        }
    });
    e(".btn-close").click(function(t) {
        t.preventDefault();
        e(this).parent().parent().parent().fadeOut()
    });
    e(".btn-minimize").click(function(t) {
        t.preventDefault();
        var n = e(this).parent().parent().next(".box-content");
        n.is(":visible") ? e("i", e(this)).removeClass("fa-chevron-up").addClass("fa-chevron-down") : e("i", e(this)).removeClass("fa-chevron-down").addClass("fa-chevron-up");
        n.slideToggle("slow", function() {
            widthFunctions();
        })
    });
});

jQuery(document).ready(function(e) {
    e("#main-menu-act").click(function() {
        if (e(this).hasClass("full")) {
            $.cookie('sma_sidebar', 'minified', { path: '/' });
            e(this).removeClass("full").addClass("minified").find("i").removeClass("fa-angle-double-left").addClass("fa-angle-double-right");
            e("body").addClass("sidebar-minified");
            e("#content").addClass("sidebar-minified");
            e("#sidebar-left").addClass("minified");
            e(".dropmenu > .chevron").removeClass("opened").addClass("closed");
            e(".dropmenu").parent().find("ul").hide();
            e("#sidebar-left > div > ul > li > a > .chevron").removeClass("closed").addClass("opened");
            e("#sidebar-left > div > ul > li > a").addClass("open");
            $('#fixed').hide();
        } else {
            $.cookie('sma_sidebar', 'full', { path: '/' });
            e(this).removeClass("minified").addClass("full").find("i").removeClass("fa-angle-double-right").addClass("fa-angle-double-left");
            e("body").removeClass("sidebar-minified");
            e("#content").removeClass("sidebar-minified");
            e("#sidebar-left").removeClass("minified");
            e("#sidebar-left > div > ul > li > a > .chevron").removeClass("opened").addClass("closed");
            e("#sidebar-left > div > ul > li > a").removeClass("open");
            $('#fixed').show();
        }
        return false;
    });
e(".dropmenu").click(function(t) {
    t.preventDefault();
    if (e("#sidebar-left").hasClass("minified")) {
        if (!e(this).hasClass("open")) {
            e(this).parent().find("ul").first().slideToggle();
            e(this).find(".chevron").hasClass("closed") ? e(this).find(".chevron").removeClass("closed").addClass("opened") : e(this).find(".chevron").removeClass("opened").addClass("closed")
        }
    } else {
        e(this).parent().find("ul").first().slideToggle();
        e(this).find(".chevron").hasClass("closed") ? e(this).find(".chevron").removeClass("closed").addClass("opened") : e(this).find(".chevron").removeClass("opened").addClass("closed")
    }
});
if (e("#sidebar-left").hasClass("minified")) {
    e("#sidebar-left > div > ul > li > a > .chevron").removeClass("closed").addClass("opened");
    e("#sidebar-left > div > ul > li > a").addClass("open");
    e("body").addClass("sidebar-minified")
}
});

$(document).ready(function() {
    cssStyle();
    $('select:not(.not_select), .select').select2({minimumResultsForSearch: 1});
    $('#customer, #rcustomer, .ssr-customer').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url+"customers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if(data.results != null) {
                    return { results: data.results };
                } else {
                    return { results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
    $('#supplier, #rsupplier, .rsupplier').select2({
       minimumInputLength: 1,
       ajax: {
        url: site.base_url+"suppliers/suggestions",
        dataType: 'json',
        quietMillis: 15,
        data: function (term, page) {
            return {
                term: term,
                limit: 10
            };
        },
        results: function (data, page) {
            if(data.results != null) {
                return { results: data.results };
            } else {
                return { results: [{id: '', text: lang.no_match_found}]};
            }
        }
    }
});
    $('.input-tip').tooltip({placement: 'top', html: true, trigger: 'hover focus', container: 'body',
        title: function() {
            return $(this).attr('data-tip');
        }
    });
    $('.input-pop').popover({placement: 'top', html: true, trigger: 'hover', container: 'body',
        content: function() {
            return $(this).attr('data-tip');
        },
        title: function() {
            return '<b>' + $('label[for="' + $(this).attr('id') + '"]').text() + '</b>';
        }
    });
});

$(document).on('click', '*[data-toggle="lightbox"]', function(event) {
    event.preventDefault();
    $(this).ekkoLightbox();
});
$(document).on('click', '*[data-toggle="popover"]', function(event) {
    event.preventDefault();
    $(this).popover();
});

$(document).ajaxStart(function(){
  $('#ajaxCall').show();
}).ajaxStop(function(){
  $('#ajaxCall').hide();
});

$(document).ready(function() {
    $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
    });
    $('textarea').not('.skip').redactor({
        buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', /*'image', 'video',*/ 'link', '|', 'html'],
        formattingTags: ['p', 'pre', 'h3', 'h4'],
        minHeight: 100,
        changeCallback: function(e) {
            var editor = this.$editor.next('textarea');
            if($(editor).attr('required')){
                // $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', $(editor).attr('name'));
            }
        }
    });
    $(document).on('click', '.file-caption', function(){
        $(this).next('.input-group-btn').children('.btn-file').children('input.file').trigger('click');
    });
});

function suppliers(ele) {
    $(ele).select2({
       minimumInputLength: 1,
       ajax: {
        url: site.base_url+"suppliers/suggestions",
        dataType: 'json',
        quietMillis: 15,
        data: function (term, page) {
            return {
                term: term,
                limit: 10
            };
        },
        results: function (data, page) {
            if(data.results != null) {
                return { results: data.results };
            } else {
                return { results: [{id: '', text: lang.no_match_found}]};
            }
        }
    }
});
}

$(function() {
    $('.datetime').datetimepicker({format: site.dateFormats.js_ldate, fontAwesome: true, language: 'sma', weekStart: 1, todayBtn: 1, autoclose: 1, todayHighlight: 1, startView: 2, forceParse: 0});
    $('.date').datetimepicker({format: site.dateFormats.js_sdate, fontAwesome: true, language: 'sma', todayBtn: 1, autoclose: 1, minView: 2 });
    $(document).on('focus','.date', function(t) {
        $(this).datetimepicker({format: site.dateFormats.js_sdate, fontAwesome: true, todayBtn: 1, autoclose: 1, minView: 2 });
    });
    $(document).on('focus','.datetime', function() {
        $(this).datetimepicker({format: site.dateFormats.js_ldate, fontAwesome: true, weekStart: 1, todayBtn: 1, autoclose: 1, todayHighlight: 1, startView: 2, forceParse: 0});
    });
    var startDate = moment().subtract(89, "days").format('YYYY-MM-DD');
    var endDate = moment().format('YYYY-MM-DD');
    $('#log-date').datetimepicker({startDate: startDate, endDate: endDate, format: site.dateFormats.js_sdate, fontAwesome: true, language: 'sma', todayBtn: 1, autoclose: 1, minView: 2 });
    $(document).on('focus','#log-date', function(t) {
        $(this).datetimepicker({startDate: startDate, endDate: endDate, format: site.dateFormats.js_sdate, fontAwesome: true, todayBtn: 1, autoclose: 1, minView: 2 });
    });
    $('#log-date').on('changeDate', function(ev){
        var date = moment(ev.date.valueOf()).format('YYYY-MM-DD');
        refreshPage(date);
    });
});

$(document).ready(function() {
    $('#dbTab a').on('shown.bs.tab', function(e) {
      var newt = $(e.target).attr('href');
      var oldt = $(e.relatedTarget).attr('href');
      $(oldt).hide();
      //$(newt).hide().fadeIn('slow');
      $(newt).hide().slideDown('slow');
  });
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
    });
    $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
    });
    $('.hideComment').click(function(){
        $.ajax({ url: site.base_url+'welcome/hideNotification/'+$(this).attr('id')});
    });
    $('.tip').tooltip();
    $('body').on('click', '#delete', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form').submit();
    });
    $('body').on('click', '#convert_to_assemble', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form').submit();
    });
    $('body').on('click', '#convert_to_packing', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form').submit();
    });
    $('body').on('click', '#sync_quantity', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#sync_award_points', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#excel', function(e) {
        e.preventDefault();
        $('#form_action_warehouse_id').val($('#warehouse_id').val());
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#sync_payments', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#all_excel', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#form_action_warehouse_id').val($('#warehouse_id').val());
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#fix_slug', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#form_action_warehouse_id').val($('#warehouse_id').val());
        $('#action-form-submit').trigger('click');
    });

    $('body').on('click', '#update_price_group_base', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });

    $('body').on('click', '#post_sale, #mark_printed_sale', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });

    $('body').on('click', '#mark_all_printed_sale', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#electronic').val($(this).attr('data-electronic'));
        $('#action-form-submit').trigger('click');
    });

    $('body').on('click', '#sync_returns', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#pdf', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#labelProducts', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#barcodeProducts', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#combine', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#pay_commisions', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
    $('body').on('click', '#post_transfer', function(e) {
        e.preventDefault();
        $('#form_action').val($(this).attr('data-action'));
        $('#action-form-submit').trigger('click');
    });
});

$(document).ready(function() {
    $('#product-search').click(function() {
        $('#product-search-form').submit();
    });
    //feedbackIcons:{valid: 'fa fa-check',invalid: 'fa fa-times',validating: 'fa fa-refresh'},
    $('form[data-toggle="validator"]').bootstrapValidator({ message: 'Please enter/select a value', submitButtons: 'input[type="submit"]' });
    fields = $('.form-control');
    $.each(fields, function() {
        var id = $(this).attr('id');
        var iname = $(this).attr('name');
        var iid = '#'+id;
        if (!!$(this).attr('data-bv-notempty') || !!$(this).attr('required')) {
            $("label[for='" + id + "']").append(' *');
            $(document).on('change', iid, function(){
                // $('form[data-toggle="validator"]').bootstrapValidator('revalidateField', iname);
            });
        }
    });
    $('body').on('click', 'label', function (e) {
        var field_id = $(this).attr('for');
        if (field_id) {
            if($("#"+field_id).hasClass('select')) {
                $("#"+field_id).select2("open");
                return false;
            }
        }
    });
    $('body').on('focus', 'select', function (e) {
        var field_id = $(this).attr('id');
        if (field_id) {
            if($("#"+field_id).hasClass('select')) {
                $("#"+field_id).select2("open");
                return false;
            }
        }
    });
    $('#myModal').on('hidden.bs.modal', function() {
        $(this).find('.modal-dialog').empty();
        //$(this).find('#myModalLabel').empty().html('&nbsp;');
        //$(this).find('.modal-body').empty().text('Loading...');
        //$(this).find('.modal-footer').empty().html('&nbsp;');
        $(this).removeData('bs.modal');
    });
    $('#myModal2').on('hidden.bs.modal', function () {
        $(this).find('.modal-dialog').empty();
        //$(this).find('#myModalLabel').empty().html('&nbsp;');
        //$(this).find('.modal-body').empty().text('Loading...');
        //$(this).find('.modal-footer').empty().html('&nbsp;');
        $(this).removeData('bs.modal');
        $('#myModal').css('zIndex', '1050');
        $('#myModal').css('overflow-y', 'scroll');
    });
    $('#myModal').on('show.bs.modal', function () {
        $('#myModal').css('zIndex', '1040');
        setTimeout(function() {
            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
        }, 800);
    });
    $('#myModal2').on('show.bs.modal', function () {
        $('#myModal').css('zIndex', '1040');
    });
    $('.modal').on('show.bs.modal', function () {
        $('#modal-loading').show();
        $('.blackbg').css('zIndex', '1041');
        $('.loader').css('zIndex', '1042');
    }).on('hide.bs.modal', function () {
        $('#modal-loading').hide();
        $('.blackbg').css('zIndex', '3');
        $('.loader').css('zIndex', '4');
    });
    $(document).on('click', '.po', function(e) {
        var placement = 'left';
        if ($(this).data('placement')) { placement = $(this).data('placement'); }
        e.preventDefault();
        $('.po').popover({html: true, placement: placement, trigger: 'manual'}).popover('show').not(this).popover('hide');
        return false;
    });
    $(document).on('click', '.po-close', function() {
        $('.po').popover('hide');
        return false;
    });
    $(document).on('click', '.po-delete', function(e) {
        var row = $(this).closest('tr');
        e.preventDefault();
        $('.po').popover('hide');
        var link = $(this).attr('href');
        var return_id = $(this).attr('data-return-id');
        $.ajax({type: "get", url: link, dataType: 'json',
            success: function(data) {
                if (data.error == 1) { header_alert('danger', data.msg); }
                else { header_alert('success', data.msg); if(oTable != '') { oTable.fnDraw(); } }
            },
            error: function(data) { header_alert('danger', 'Ajax call failed'); }
        });
        return false;
    });
    $(document).on('click', '.po-delete1', function(e) {
        e.preventDefault();
        $('.po').popover('hide');
        var link = $(this).attr('href');
        var s = $(this).attr('id'); var sp = s.split('__')
        $.ajax({type: "get", url: link, dataType: 'json',
            success: function(data) {
                if (data.error == 1) { addAlert(data.msg, 'danger'); }
                else { addAlert(data.msg, 'success'); if(oTable != '') { oTable.fnDraw(); } }
            },
            error: function(data) { addAlert('Ajax call failed', 'danger'); }
        });
        return false;
    });
    $(document).on('click', '.po-set_featuring', function(e) {
        e.preventDefault();
        $('.po').popover('hide');
        var link = $(this).attr('href');
        var s = $(this).attr('id'); var sp = s.split('__')
        $.ajax({type: "get", url: link, dataType: 'json',
            success: function(data) {
                if (data.error == 1) { addAlert(data.msg, 'danger'); }
                else { addAlert(data.msg, 'success'); if(oTable != '') { oTable.fnDraw(); } }
            },
            error: function(data) { addAlert('Ajax call failed', 'danger'); }
        });
        return false;
    });
    $('body').on('click', '.bpo', function(e) {
        e.preventDefault();
        $(this).popover({html: true, trigger: 'manual'}).popover('toggle');
        return false;
    });
    $('body').on('click', '.bpo-close', function(e) {
        $('.bpo').popover('hide');
        return false;
    });
    $('#inlineCalc').calculator({layout: ['_%+-CABS','_7_8_9_/','_4_5_6_*','_1_2_3_-','_0_._=_+'], showFormula:true});
    $('.calc').click(function(e) { e.stopPropagation();});
    // $(document).on('click', '.sname', function(e) {
    //     var row = $(this).closest('tr');
    //     var itemid = row.find('.rid').val();
    //     $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + itemid});
    //     $('#myModal').modal('show');
    // });
});

function addAlert(message, type) {
    $('.alerts-con').empty().append(
        '<div class="alert alert-' + type + '">' +
        '<button type="button" class="close" data-dismiss="alert">' +
        '&times;</button>' + message + '</div>');
}

$(document).ready(function() {
    if ($.cookie('sma_sidebar') == 'minified') {
        $('#main-menu-act').removeClass("full").addClass("minified").find("i").removeClass("fa-angle-double-left").addClass("fa-angle-double-right");
        $("body").addClass("sidebar-minified");
        $("#content").addClass("sidebar-minified");
        $("#sidebar-left").addClass("minified");
        $(".dropmenu > .chevron").removeClass("opened").addClass("closed");
        $(".dropmenu").parent().find("ul").hide();
        $("#sidebar-left > div > ul > li > a > .chevron").removeClass("closed").addClass("opened");
        $("#sidebar-left > div > ul > li > a").addClass("open");
        $('#fixed').hide();
    } else {

        $('#main-menu-act').removeClass("minified").addClass("full").find("i").removeClass("fa-angle-double-right").addClass("fa-angle-double-left");
        $("body").removeClass("sidebar-minified");
        $("#content").removeClass("sidebar-minified");
        $("#sidebar-left").removeClass("minified");
        $("#sidebar-left > div > ul > li > a > .chevron").removeClass("opened").addClass("closed");
        $("#sidebar-left > div > ul > li > a").removeClass("open");
        $('#fixed').show();
    }
});

$(document).ready(function() {
    $('#daterange').daterangepicker({
        timePicker: true,
        format: (site.dateFormats.js_sdate).toUpperCase()+' HH:mm',
        ranges: {
            'Today': [moment().hours(0).minutes(0).seconds(0), moment()],
            'Yesterday': [moment().subtract('days', 1).hours(0).minutes(0).seconds(0), moment().subtract('days', 1).hours(23).minutes(59).seconds(59)],
            'Last 7 Days': [moment().subtract('days', 6).hours(0).minutes(0).seconds(0), moment().hours(23).minutes(59).seconds(59)],
            'Last 30 Days': [moment().subtract('days', 29).hours(0).minutes(0).seconds(0), moment().hours(23).minutes(59).seconds(59)],
            'This Month': [moment().startOf('month').hours(0).minutes(0).seconds(0), moment().endOf('month').hours(23).minutes(59).seconds(59)],
            'Last Month': [moment().subtract('month', 1).startOf('month').hours(0).minutes(0).seconds(0), moment().subtract('month', 1).endOf('month').hours(23).minutes(59).seconds(59)]
        }
    },
    function(start, end) {
        refreshPage(start.format('YYYY-MM-DD HH:mm'), end.format('YYYY-MM-DD HH:mm'));
    });
});

function refreshPage(start, end) {
    if (end) {
        window.location.replace(CURI + '/' + encodeURIComponent(start) + '/' + encodeURIComponent(end));
    }
    window.location.replace(CURI + '/' + encodeURIComponent(start));
}

function retina() {
    retinaMode = window.devicePixelRatio > 1;
    return retinaMode
}

$(document).ready(function() {
    $('#cssLight').click(function(e) {
        e.preventDefault();
        $.cookie('sma_style', 'light', { path: '/' });
        cssStyle();
        return true;
    });
    $('#cssBlue').click(function(e) {
        e.preventDefault();
        $.cookie('sma_style', 'blue', { path: '/' });
        cssStyle();
        return true;
    });
    $('#cssBlack').click(function(e) {
        e.preventDefault();
        $.cookie('sma_style', 'black', { path: '/' });
        cssStyle();
        return true;
    });
    $("#toTop").click(function(e) {
        e.preventDefault();
        $("html, body").animate({scrollTop: 0}, 100);
    });
    $(document).on('click', '.delimg', function(e) {
        e.preventDefault();
        var ele = $(this), id = $(this).attr('data-item-id');
        bootbox.confirm(lang.r_u_sure, function(result) {
        if(result == true) {
            $.get(site.base_url+'products/delete_image/'+id, function(data) {
                if (data.error === 0) {
                    addAlert(data.msg, 'success');
                    ele.parent('.gallery-image').remove();
                }
            });
        }
        });
        return false;
    });
});
$(document).ready(function() {
    $(document).on('click', '.row_status', function(e) {
        e.preventDefault;
        var row = $(this).closest('tr');
        var id = row.attr('id');
        if (row.hasClass('invoice_link')) {
            $('#myModal').modal({remote: site.base_url + 'sales/update_status/' + id});
            $('#myModal').modal('show');
        } else if (row.hasClass('purchase_link')) {
            $('#myModal').modal({remote: site.base_url + 'purchases/update_status/' + id});
            $('#myModal').modal('show');
        } else if (row.hasClass('quote_link')) {
            $('#myModal').modal({remote: site.base_url + 'quotes/update_status/' + id});
            $('#myModal').modal('show');
        } else if (row.hasClass('transfer_link')) {
            $('#myModal').modal({remote: site.base_url + 'transfers/update_status/' + id});
            $('#myModal').modal('show');
        }
        return false;
    });

    $(document).on('click', '.row_evaluation', function(e) {
        e.preventDefault;
        var row = $(this).closest('tr');
        var id = row.attr('id');
        if (row.hasClass('purchase_link')) {
            $('#myModal').modal({remote: site.base_url + 'purchases/purchase_evaluation/' + id});
            $('#myModal').modal('show');
        }
        return false;
    });



    $(document).on('click', '.general_status', function(e) {
        e.preventDefault;
        var row = $(this).closest('tr');
        var id = row.attr('id');
        if (row.hasClass('warehouse_link')) {
            $('#myModal').modal({remote: site.base_url + 'system_settings/update_warehouse_status/' + id});
            $('#myModal').modal('show');
        } else if (row.hasClass('biller_link')) {
            $('#myModal').modal({remote: site.base_url + 'billers/update_status/' + id});
            $('#myModal').modal('show');
        }
        return false;
    });

    $(document).on('click', '.por_status', function(e) {
        e.preventDefault;
        var row = $(this).closest('tr');
        var id = row.attr('id');
        if (row.hasClass('cutting_order_link')) {
            $('#myModal').modal({remote: site.base_url + 'production_order/change_cutting_order_items_status/' + id});
            $('#myModal').modal('show');
        } else if (row.hasClass('assemble_order_link')) {
            $('#myModal').modal({remote: site.base_url + 'production_order/change_assemble_order_items_status/' + id});
            $('#myModal').modal('show');
        } else if (row.hasClass('packing_order_link')) {
            $('#myModal').modal({remote: site.base_url + 'production_order/change_packing_order_items_status/' + id});
            $('#myModal').modal('show');
        }
        return false;
    });

    $(document).on('click', '.por_add_notif', function(e) {
        e.preventDefault;
        var row = $(this).closest('tr');
        var register_type = $(this).data('registertype');
        var register_id = $(this).data('registerid');
        $('#myModal').modal({remote: site.base_url + 'production_order/add_new_notification/' + register_type+'/'+register_id});
        $('#myModal').modal('show');
        return false;
    });



});

$(document).on('ifChecked', '.checkth, .checkft', function(event) {
    $('.checkth, .checkft').iCheck('check');
    $('.multi-select').each(function() {
        $(this).iCheck('check');
    });
});
$(document).on('ifUnchecked', '.checkth, .checkft', function(event) {
    $('.checkth, .checkft').iCheck('uncheck');
    $('.multi-select').each(function() {
        $(this).iCheck('uncheck');
    });
});
$(document).on('ifUnchecked', '.multi-select', function(event) {
    $('.checkth, .checkft').attr('checked', false);
    $('.checkth, .checkft').iCheck('update');
});

function check_add_item_val() {
    $('#add_item').bind('keypress', function (e) {
        if (e.keyCode == 13 || e.keyCode == 9) {
            e.preventDefault();
            $(this).autocomplete("search");
        }
    });
}
function fld(oObj) {
    if (oObj != null) {
        var aDate = oObj.split('-');
        var bDate = aDate[2].split(' ');
        year = aDate[0], month = aDate[1], day = bDate[0], time = bDate[1];
        if (site.dateFormats.js_sdate == 'dd-mm-yyyy')
            return day + "-" + month + "-" + year + " " + time;
        else if (site.dateFormats.js_sdate === 'dd/mm/yyyy')
            return day + "/" + month + "/" + year + " " + time;
        else if (site.dateFormats.js_sdate == 'dd.mm.yyyy')
            return day + "." + month + "." + year + " " + time;
        else if (site.dateFormats.js_sdate == 'mm/dd/yyyy')
            return month + "/" + day + "/" + year + " " + time;
        else if (site.dateFormats.js_sdate == 'mm-dd-yyyy')
            return month + "-" + day + "-" + year + " " + time;
        else if (site.dateFormats.js_sdate == 'mm.dd.yyyy')
            return month + "." + day + "." + year + " " + time;
        else
            return oObj;
    } else {
        return '';
    }
}
function fls(oObj) {
    if (oObj != null) {
        var aDate = oObj.split('-');
        var bDate = aDate[2].split(' ');
        year = aDate[0], month = aDate[1], day = bDate[0], time = bDate[1];
        if (site.dateFormats.js_sdate == 'dd-mm-yyyy')
            return day + "-" + month + "-" + year + " ";
        else if (site.dateFormats.js_sdate === 'dd/mm/yyyy')
            return day + "/" + month + "/" + year + " ";
        else if (site.dateFormats.js_sdate == 'dd.mm.yyyy')
            return day + "." + month + "." + year + " ";
        else if (site.dateFormats.js_sdate == 'mm/dd/yyyy')
            return month + "/" + day + "/" + year + " ";
        else if (site.dateFormats.js_sdate == 'mm-dd-yyyy')
            return month + "-" + day + "-" + year + " ";
        else if (site.dateFormats.js_sdate == 'mm.dd.yyyy')
            return month + "." + day + "." + year + " ";
        else
            return oObj;
    } else {
        return '';
    }
}

function datetimepicker_format(oObj) {
    if (oObj != null) {
        var aDate = oObj.split('/');
        if (aDate.length > 1) {
            var bDate = aDate[2].split(' ');
            year = aDate[0], month = aDate[1], day = bDate[0], time = bDate[1];
            return day + "-" + month + "-" + year + " " + time;
        } else {
            return '';
        }
    } else {
        return '';
    }
}

function quote_type(oObj){
    if (oObj != null) {
        if (oObj == 1) {
            return lang.products;
        } else if (oObj == 2) {
            return lang.expenses;
        }
    } else {
        return '';
    }
}

function fsd(oObj) {
    if (oObj != null) {
        var aDate = oObj.split('-');
        if (site.dateFormats.js_sdate == 'dd-mm-yyyy')
            return aDate[2] + "-" + aDate[1] + "-" + aDate[0];
        else if (site.dateFormats.js_sdate === 'dd/mm/yyyy')
            return aDate[2] + "/" + aDate[1] + "/" + aDate[0];
        else if (site.dateFormats.js_sdate == 'dd.mm.yyyy')
            return aDate[2] + "." + aDate[1] + "." + aDate[0];
        else if (site.dateFormats.js_sdate == 'mm/dd/yyyy')
            return aDate[1] + "/" + aDate[2] + "/" + aDate[0];
        else if (site.dateFormats.js_sdate == 'mm-dd-yyyy')
            return aDate[1] + "-" + aDate[2] + "-" + aDate[0];
        else if (site.dateFormats.js_sdate == 'mm.dd.yyyy')
            return aDate[1] + "." + aDate[2] + "." + aDate[0];
        else
            return oObj;
    } else {
        return '';
    }
}
function generateCardNo(x) {
    if(!x) { x = 16; }
    chars = "1234567890";
    no = "";
    for (var i=0; i<x; i++) {
       var rnum = Math.floor(Math.random() * chars.length);
       no += chars.substring(rnum,rnum+1);
   }
   return no;
}
function generate_random_pin_code(x) {
    if(!x) { x = 16; }
    chars = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    no = "";
    for (var i=0; i<x; i++) {
       var rnum = Math.floor(Math.random() * chars.length);
       no += chars.substring(rnum,rnum+1);
   }
   return no;
}
function roundNumber(num, nearest) {
    if(!nearest) { nearest = 0.05; }
    return Math.round((num / nearest) * nearest);
}
function getNumber(x) {
    return accounting.unformat(x);
}
function formatQuantity(x) {
    return (x != null) ? '<div class="text-center">'+formatNumber(x, site.settings.qty_decimals)+'</div>' : '';
}
function formatQuantity2(x) {
    return (x != null) ? formatNumber(x, site.settings.qty_decimals) : '';
}
function formatQuantityNumber(x, d) {
    if (!d) { d = site.settings.qty_decimals; }
    return parseFloat(accounting.formatNumber(x, d, '', '.'));
}
function formatQty(x) {
    return (x != null) ? formatNumber(x, site.settings.qty_decimals) : '';
}
function formatNumber(x, d) {
    if(!d && d != 0) { d = site.settings.decimals; }
    if(site.settings.sac == 1) {
        return formatSA(parseFloat(x).toFixed(d));
    }
    return accounting.formatNumber(x, d, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep);
}
function formatMoney(x, symbol) {
    if(!symbol) { symbol = ''; }
    if(site.settings.sac == 1) {
        return (site.settings.display_symbol == 1 ? site.settings.symbol : '') +
            ''+formatSA(parseFloat(x).toFixed(site.settings.decimals)) +
            (site.settings.display_symbol == 2 ? site.settings.symbol : '');
    }
    var fmoney = accounting.formatMoney(x, symbol, site.settings.decimals, site.settings.thousands_sep == 0 ? ' ' : site.settings.thousands_sep, site.settings.decimals_sep, "%s%v");
    return (site.settings.display_symbol == 1 ? site.settings.symbol : '') +
        fmoney +
        (site.settings.display_symbol == 2 ? site.settings.symbol : '');
}
function is_valid_discount(mixed_var) {
    return (is_numeric(mixed_var) || (/([0-9]%)/i.test(mixed_var))) ? true : false;
}
function is_numeric(mixed_var) {
    var whitespace =
    " \n\r\t\f\x0b\xa0\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u200b\u2028\u2029\u3000";
    return (typeof mixed_var === 'number' || (typeof mixed_var === 'string' && whitespace.indexOf(mixed_var.slice(-1)) === -
        1)) && mixed_var !== '' && !isNaN(mixed_var);
}
function is_float(mixed_var) {
  return +mixed_var === mixed_var && (!isFinite(mixed_var) || !! (mixed_var % 1));
}
function decimalFormat(x) {
    return '<div class="text-center">'+formatNumber(x != null ? x : 0)+'</div>';
}
function currencyFormat(x) {
    return '<div class="text-right">'+formatMoney(x != null ? x : 0)+'</div>';
}
function formatDecimal(x, d) {
    if (!d) { d = parseFloat(site.settings.decimals); if (site.settings.rounding == 1) { d += 2; } } else { d = parseFloat(d)}

    return parseFloat(accounting.formatNumber(x, d, '', '.'));
}
function formatDecimalNoRound(num, dec) {
    if (!dec) { dec = site.settings.decimals; }
  var exp = Math.pow(10, dec || 2); // 2 decimales por defecto
  return parseInt(num * exp, 10) / exp;
}
function formatDecimals(x, d) {
    if (!d) { d = site.settings.decimals; }
    return parseFloat(accounting.formatNumber(x, d, '', '.')).toFixed(d);
}
function pqFormat(x) {
    if (x != null) {
        var d = '', pqc = x.split("___");
        for (index = 0; index < pqc.length; ++index) {
            var pq = pqc[index];
            var v = pq.split("__");
            // d += v[0]+' ('+formatQuantity2(v[1])+')<br>';
            d += v[0]+' '+formatQuantity2(v[1])+'<br>';
        }
        return d;
    } else {
        return '';
    }
}

function productMovementFormat(x) {
    // // console.log(x);
    if (x != null) {
        // var d = '';
        var pqc = x.split("___");
        // for (index = 0; index < pqc.length; ++index) {
        //     var pq = pqc[index];
        //     var v = pq.split("__");
        //     // d += v[0]+' ('+formatQuantity2(v[1])+')<br>';
        //     d += v[0]+' '+formatQuantity2(v[1])+'<br>';
        // }
        d = formatQuantity2(pqc[0])+( pqc[1] > 0 ? " <span class='text-warning'> ("+formatQuantity2(pqc[1])+") </span>" : "");
        return d;
    } else {
        return '';
    }
}

function sumListQuantitys(x){
    var sum = 0;
    if (x != null) {
        // // console.log(x);
        qtys = x.split("___");
        $.each(qtys, function(index, value){
            sum += parseFloat(value);
            // console.log(parseFloat(value));
        });
    }
    return sum;
}

function checkbox(x) {
    return '<div class="text-center"><input class="checkbox multi-select" type="checkbox" name="val[]" value="' + x + '" /></div>';
}
function radio(x) {
    return '<input class="checkbox select_auto" type="radio" name="radio" value="' + x + '" />';
}
function radio_2(x) {
    return '<input class="checkbox select_auto_2" type="radio" name="radio_2" value="' + x + '"/>';
}


function input(x) {
    return '<input class="focus_product" type="text" style="width:10%;" />';
}
function decode_html(value){
    return $('<div/>').html(value).text();
}
function img_hl(x) {
    var image_link = (x == null || x == '') ? 'no_image.png' : x;
    img_url = image_link != 'no_image.png' && (site.settings.images_from_tpro == 1 || site.settings.images_from_tpro == 2) ? 'https://'+image_link : site.url+'assets/uploads/' + image_link;
    return '<div class="text-center"><a href="'+img_url+'" data-toggle="lightbox"><img class="img-circle" src="'+img_url+'" alt="" style="width:48px; height:48px;" /></a></div>';
}
function attachment(x) {
    return x == null ? '' : '<div class="text-center"><a href="'+site.base_url+'welcome/download/' + x + '"><i class="fas fa-paperclip" style="font-size:128%;"></i></a></div>';
}
function attachment2(x) {
    return x == null ? '' : '<div class="text-center"><a href="'+site.base_url+'welcome/download/' + x + '" class="tip" title="'+lang.download+'"><i class="fa fa-file-o"></i></a></div>';
}
function user_status(x) {
    var y = x.split("__");
    return y[0] == 1 ?
    '<a href="'+site.base_url+'auth/deactivate/'+ y[1] +'" data-toggle="modal" data-target="#myModal"><span class="label label-success"><i class="fa fa-check"></i> '+lang['active']+'</span></a>' :
    '<a href="'+site.base_url+'auth/activate/'+ y[1] +'"><span class="label label-danger"><i class="fa fa-times"></i> '+lang['inactive']+'</span><a/>';
}
function login_status(x) {
    var y = x.split("__");
    if (y[0] == 1) {
        return '<a href="'+site.base_url+'auth/log_out_user/'+ y[1] +'" data-toggle="modal" data-target="#myModal"><span class="label label-success"><i class="fa fa-check"></i> '+lang['active']+'</span><a/>';
    } else if (y[0] == 0) {
        return '<p><span class="label label-danger"><i class="fa fa-times"></i> '+lang['inactive']+'</span><p/>';
    }
}
function user_group_lang(x) {
    return lang[x] !== undefined ? lang[x] : x;
}
function row_status(x) {
    if(x == null) {
        return '';
    } else if(x == 'pending') {
        return '<div class="text-center"><span class="row_status label label-warning">'+lang['not_approved']+'</span></div>';
    } else if(x == 'completed' || x == 'paid' || x == 'sent' || x == 'received') {
        return '<div class="text-center"><span class="row_status label label-success">'+lang['approved']+'</span></div>';
    } else if(x == 'partial' || x == 'transferring' || x == 'ordered') {
        return '<div class="text-center"><span class="row_status label label-info">'+lang[x]+'</span></div>';
    } else if(x == 'due' || x == 'returned') {
        return '<div class="text-center"><span class="row_status label label-danger">'+lang[x]+'</span></div>';
    } else {
        return '<div class="text-center"><span class="row_status label label-default">'+x+'</span></div>';
    }
}
function order_row_status(x) {
    x = x == 'completed' ? 'invoiced' : x ;
    if(x == null) {
        return '';
    } else if(x == 'pending') {
        return '<div class="text-center"><span class="row_status label label-warning">'+lang[x]+'</span></div>';
    } else if(x == 'invoiced') {
        return '<div class="text-center"><span class="row_status label label-success">'+lang[x]+'</span></div>';
    } else if(x == 'delivered' || x == 'sent') {
        return '<div class="text-center"><span class="row_status label label-primary">'+lang[x]+'</span></div>';
    } else if(x == 'partial' || x == 'enlistment') {
        return '<div class="text-center"><span class="row_status label label-info">'+lang[x]+'</span></div>';
    } else if(x == 'cancelled') {
        return '<div class="text-center"><span class="row_status label label-danger">'+lang[x]+'</span></div>';
    } else {
        return '<div class="text-center"><span class="row_status label label-default">'+x+'</span></div>';
    }
}
function pay_status(x) {
    if(x == null) {
        return '';
    } else if(x == 'pending') {
        return '<div class="text-center"><span class="payment_status label label-warning">'+lang[x]+'</span></div>';
    } else if(x == 'completed' || x == 'paid' || x == 'sent' || x == 'received' || x == 'sent_2') {
        return '<div class="text-center"><span class="payment_status label label-success">'+lang[x]+'</span></div>';
    } else if(x == 'partial' || x == 'transferring' || x == 'ordered') {
        return '<div class="text-center"><span class="payment_status label label-info">'+lang[x]+'</span></div>';
    } else if(x == 'due' || x == 'returned') {
        return '<div class="text-center"><span class="payment_status label label-danger">'+lang[x]+'</span></div>';
    } else {
        return '<div class="text-center"><span class="payment_status label label-default">'+x+'</span></div>';
    }
}
function gc_status(x) {
    if(x == null) {
        return '';
    } else if(x == '1') {
        return '<div class="text-center"><span class="payment_status label label-success">'+lang['active']+'</span></div>';
    } else if(x == '2') {
        return '<div class="text-center"><span class="payment_status label label-default">'+lang['consumed']+'</span></div>';
    } else if(x == '3') {
        return '<div class="text-center"><span class="payment_status label label-danger">'+lang['cancelled']+'</span></div>';
    } else if(x == '4') {
        return '<div class="text-center"><span class="payment_status label label-danger">'+lang['expired']+'</span></div>';
    } else if(x == '5') {
        return '<div class="text-center"><span class="payment_status label label-danger">'+lang['returned']+'</span></div>';
    } else {
        return "-";
    }
}
function gc_creation_type(x) {
    if(x == 1) {
        return 'Venta';
    } else if(x == 2) {
        return 'Canje de puntos';
    } else if(x == 3) {
        return 'Devolución venta';
    } else if(x == 4) {
        return 'Creación directa';
    }
}

function generalStatus(x){
    if (x == 1) {
        return '<div class="text-center"><span class="general_status label label-success" style="cursor:pointer;">'+lang['active']+'</span></div>';
    } else {
        return '<div class="text-center"><span class="general_status label label-warning" style="cursor:pointer;">'+lang['inactive']+'</span></div>';
    }
}
function por_status(x){
    if (x == "completed") {
        return '<div class="text-center"><span class="general_status label label-primary" style="cursor:pointer;">'+lang['completed']+'</span></div>';
    } else if (x == "in_process") {
        return '<div class="text-center"><span class="general_status label label-success" style="cursor:pointer;">'+lang['in_process']+'</span></div>';
    } else {
        return '<div class="text-center"><span class="general_status label label-warning" style="cursor:pointer;">'+lang['pending']+'</span></div>';
    }
}

function discontinued_status(x){
    if (x == 0) {
        return '<div class="text-center"><span class="discontinued_status label label-success">'+lang['active']+'</span></div>';
    } else {
        return '<div class="text-center"><span class="discontinued_status label label-warning">'+lang['inactive']+'</span></div>';
    }
}
function guest_register_status(x) {
    if(x == null) {
        return '';
    } else if(x == '1') {
        return '<div class="text-center"><span class="row_status label label-default">En curso</span></div>';
    } else if(x == '0') {
        return '<div class="text-center"><span class="row_status label label-danger">Cerrado</span></div>';
    }
}

function wh_type(x){
    if (x == 1) {
        return lang.warehouse_type_commercial;
    } else if (x == 2) {
        return lang.warehouse_type_picking;
    } else if (x == 3) {
        return lang.warehouse_type_packing;
    }
}
function tipo_mov_wappsi_invoicing(x){
    if (x == 1) {
        return 'Activacion Modulos';
    } else if (x == 2) {
        return 'Plan Usuarios';
    } else if (x == 3) {
        return 'Plan Sucursales';
    } else if (x == 4) {
        return 'Documentos electronicos';
    } else if (x == 5) {
        return 'Plan Nómina';
    } else if (x == 6) {
        return 'Servicios recurrentes';
    } else if (x == 7) {
        return 'Productos recurrentes';
    } else if (x == 8) {
        return 'Productos y servicios NO recurrentes';
    } else if (x == 9) {
        return 'Activación Sucursal Adicional';
    }
}
function payment_status_wappsi_invoicing(x){
    if (x == 0) {
        return '<div class="text-center"><span class="payment_status label label-warning">Pendiente</span></div>';
    } else if (x == 1) {
        return '<div class="text-center"><span class="payment_status label label-success">Pagado</span></div>';
    } else if (x == 3) {
        return '<div class="text-center"><span class="payment_status label label-danger">Mora</span></div>';
    }
}

function paid_by(x) {
    if (lang[x] !== undefined) {
        return lang[x];
    } else {
        return x;
    }
}

function formatSA (x) {
    x=x.toString();
    var afterPoint = '';
    if(x.indexOf('.') > 0)
       afterPoint = x.substring(x.indexOf('.'),x.length);
    x = Math.floor(x);
    x=x.toString();
    var lastThree = x.substring(x.length-3);
    var otherNumbers = x.substring(0,x.length-3);
    if(otherNumbers != '')
        lastThree = ',' + lastThree;
    var res = otherNumbers.replace(/\B(?=(\d{2})+(?!\d))/g, ",") + lastThree + afterPoint;

    return res;
}

function unitToBaseQty(qty, unitObj) {
    if (unitObj !== undefined) {
        switch(unitObj.operator) {
            case '*':
                return parseFloat(qty)*parseFloat(unitObj.operation_value);
                break;
            case '/':
                return parseFloat(qty)/parseFloat(unitObj.operation_value);
                break;
            case '+':
                return parseFloat(qty)+parseFloat(unitObj.operation_value);
                break;
            case '-':
                return parseFloat(qty)-parseFloat(unitObj.operation_value);
                break;
            default:
                return parseFloat(qty);
        }
    } else {
        return parseFloat(qty);
    }     
}

function baseToUnitQty(qty, unitObj) {
    switch(unitObj.operator) {
        case '*':
            return parseFloat(qty)/parseFloat(unitObj.operation_value);
            break;
        case '/':
            return parseFloat(qty)*parseFloat(unitObj.operation_value);
            break;
        case '+':
            return parseFloat(qty)-parseFloat(unitObj.operation_value);
            break;
        case '-':
            return parseFloat(qty)+parseFloat(unitObj.operation_value);
            break;
        default:
            return parseFloat(qty);
    }
}

function set_page_focus(an = 2, tabindex = null) {
    if ($('#add_item').length > 0 && (site.settings.set_focus != 1 || localStorage.getItem('enter_pressed_for_quantity'))) {
        $('#add_item').attr('tabindex', 1);
        setTimeout(function() {
            $('#add_item').focus();
        }, 850);
    } else {
        $('#add_item').attr('tabindex', an);
        f = (tabindex == null ? (an-1) : tabindex + 1);
        $('[tabindex='+(f)+']').focus().select();
    }
}

function calculateTax(tax, amt, met) {

    amt = formatDecimal(amt);
    if (tax) {
        tax_val = 0; tax_rate = '';
        if (tax.type == 1) {
            if (met == '0') {
                tax_val = formatDecimal(((amt) * parseFloat(tax.rate)) / (100 + parseFloat(tax.rate)));
                tax_rate = formatDecimal(tax.rate) + '%';
            } else {
                tax_val = formatDecimal(((amt) * parseFloat(tax.rate)) / 100);
                tax_rate = formatDecimal(tax.rate) + '%';
            }
        } else if (tax.type == 2) {
            tax_val = parseFloat(tax.rate);
            tax_rate = formatDecimal(tax.rate);
        }
        return [tax_val, tax_rate];
    }
    return false;
}

function calculateDiscount(val, amt, pr_tax = null, item_tax_method = null) {
    if (val.indexOf("%") !== -1) {
        var pds = val.split("%");
        return formatDecimal((parseFloat(((amt) * parseFloat(pds[0])) / 100)), (site.settings.rounding == 1 ? site.settings.decimals : 4));
    } else {
        if (site.settings.item_discount_apply_to == 2 && pr_tax) {
            new_ds = calculateTax(pr_tax, val, item_tax_method);
            return formatDecimal(parseFloat(val) - parseFloat(new_ds[0]));
        }
    }
    return formatDecimal(val);
}

function getDiscountAssigned(val, trmrate, action){

    if (val == undefined) { val = ''; }

    if (val.indexOf("%") !== -1) {
        return val;
    } else {
        if (action == 1) {
            return String(trmrate * val);
        } else {
            return String(val / trmrate);
        }
    }
}

function pcalculateDiscount(val, val2, amt, subtotal, item_qty) {
    var descuento_1 = 0;
    var descuento_2 = 0;
    if (val.indexOf("%") !== -1) {
        var pds = val.split("%");
        descuento_1 = formatDecimal((parseFloat(((amt) * parseFloat(pds[0])) / 100)), (site.settings.rounding == 1 ? site.settings.decimals : 4));
    } else {
        descuento_1 = formatDecimal(parseFloat(val), (site.settings.rounding == 1 ? site.settings.decimals : 4));
    }
    if (val2.indexOf("%") !== -1) {
        var pds = val2.split("%");
        descuento_2 = formatDecimal((parseFloat(((amt) * parseFloat(pds[0])) / 100)), (site.settings.rounding == 1 ? site.settings.decimals : 4));
    } else {
        amt = parseFloat(amt);
        subtotal = parseFloat(subtotal);
        if (subtotal > 0) {
            peso = ((amt * item_qty) * 100) / subtotal;
            peso = formatDecimal(parseFloat(peso / 100), 5);
            val2 = val2 * peso;
            val2 = val2 / item_qty;
        }
        descuento_2 = formatDecimal(parseFloat(val2), (site.settings.rounding == 1 ? site.settings.decimals : 4));
    }
    descuento_total = descuento_1 + descuento_2;
    return descuento_total;
}

function pcalculateRemainingCost(item_total_cost, total_cost, cost_to_match, item_qty, total_destination_qty){
    if (item_total_cost == 0) {
        peso = item_qty / total_destination_qty;
        remaining_cost = (cost_to_match - total_cost) * peso;
    } else {
        remaining_cost = cost_to_match - total_cost;
        peso = (item_total_cost * 100) / total_cost;
        peso = formatDecimal(parseFloat(peso / 100), 5);
        remaining_cost = remaining_cost * peso;
    }
    return remaining_cost;
}

function pcalculateShipping(val, amt, total, item_qty) {
    val = formatDecimal(parseFloat(val)); //a prorratear
    amt = formatDecimal(parseFloat(amt)); // costo producto
    total = formatDecimal(parseFloat(total)); // total de compra
    item_qty = formatDecimal(parseFloat(item_qty)); // cantidad de producto
    if (total > 0) {
        peso = (((amt * item_qty) * 100) / total);
        peso = (parseFloat(peso / 100));
        val = val * peso;
        val = val / item_qty;
    }
    shipping = formatDecimal(parseFloat(val));
    return shipping;
}

$(document).ready(function() {
    $(document).on('click', '#view-customer', function() {
        console.log('Customer '+$("input[name=customer]").val());
        if ($("input[name=customer]").val()) {
            $('#myModal').modal({remote: site.base_url + 'customers/view/' + $("input[name=customer]").val()});
            $('#myModal').modal('show');
        }
    });

    $(document).on('click', '#view-supplier', function() {
        if ($("input[name=supplier]").val()) {
            $('#myModal').modal({remote: site.base_url + 'suppliers/view/' + $("input[name=supplier]").val()});
            $('#myModal').modal('show');
        }
    });
    $('body').on('click', '.customer_details_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'customers/view/' + $(this).parent('.customer_details_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.supplier_details_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'suppliers/view/' + $(this).parent('.supplier_details_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.creditor_details_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'creditors/view/' + $(this).parent('.creditor_details_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.product_link td:not(:first-child, :nth-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + $(this).parent('.product_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.product_link2 td:first-child, .product_link2 td:nth-child(2)', function() {
        $('#myModal').modal({remote: site.base_url + 'products/modal_view/' + $(this).closest('tr').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.purchase_link td:not(:first-child, :nth-child(5), :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/modal_view/' + $(this).parent('.purchase_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.purchase_link2 td', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/modal_view/' + $(this).closest('tr').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.transfer_link td:not(:first-child, :nth-last-child(3), :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'transfers/view/' + $(this).parent('.transfer_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.transfer_link2', function() {
        $('#myModal').modal({remote: site.base_url + 'transfers/view/' + $(this).attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.oreturn_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'returns/view/' + $(this).parent('.oreturn_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.invoice_link td:not(:first-child, :nth-child(6), :nth-last-child(4), :nth-last-child(3), :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/modal_view/' + $(this).parent('.invoice_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.guest_register_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'hotel/guest_register_modal_view/' + $(this).parent('.guest_register_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.wappsi_invoicing_link td:not(:last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'wappsi_invoicing/view_detail/' + $(this).parent('.wappsi_invoicing_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.invoice_link_first td:nth-child(2)', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/modal_view/' + $(this).parent('.invoice_link_first').attr('id')});
        $('#myModal').modal('show');
    });
    //ordenes de pedido
    $('body').on('click', '.order_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/order_modal_view/' + $(this).parent('.order_link').attr('id')});
        $('#myModal').modal('show');
    });
    //ordenes de pedido
    $('body').on('click', '.invoice_link2 td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/modal_view/' + $(this).closest('tr').attr('id')});
        $('#myModal').modal('show');
    });
    //registro de cajas POS
    $('body').on('click', '.register_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'pos/register_details/' + $(this).closest('tr').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.register_open_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'pos/close_register/NULL/' + $(this).closest('tr').attr('id')+'/true'});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.detailed_register_link td:not(:first-child, :last-child)', function() {
        window.open(site.base_url + 'reports/close_register_detailed/' + $(this).closest('tr').attr('id'), '_blank');
    });
    $('body').on('click', '.receipt_link td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({ remote: site.base_url + 'pos/view/' + $(this).parent('.receipt_link').attr('id') + '/1' });
    });
    $('body').on('click', '.posFeRow td:not(:first-child, :nth-last-child(3), :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({ remote: site.base_url + 'pos/view/' + $(this).parent('.posFeRow').attr('id') + '/1' });
    });
    $('body').on('click', '.return_link td', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/view_return/' + $(this).parent('.return_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.return_purchase_link td', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/view_return/' + $(this).parent('.return_purchase_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.payment_link td', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/payment_note/' + $(this).parent('.payment_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.payment_link2 td', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/payment_note/' + $(this).parent('.payment_link2').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.expense_link2 td:not(:last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/expense_note/' + $(this).closest('tr').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.quote_link td:not(:first-child, :nth-last-child(3), :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'quotes/modal_view/' + $(this).parent('.quote_link').attr('id')});
        $('#myModal').modal('show');
        //window.location.href = site.base_url + 'quotes/view/' + $(this).parent('.quote_link').attr('id');
    });
    $('body').on('click', '.quote_link2', function() {
        $('#myModal').modal({remote: site.base_url + 'quotes/modal_view/' + $(this).attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.delivery_link td:not(:first-child, :nth-last-child(2), :nth-last-child(3), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/view_delivery/' + $(this).parent('.delivery_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.customer_link td:not(:first-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'customers/edit/' + $(this).parent('.customer_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.supplier_link td:not(:first-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'suppliers/edit/' + $(this).parent('.supplier_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.adjustment_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'products/view_adjustment/' + $(this).parent('.adjustment_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.production_order_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'products/view_production_order/' + $(this).parent('.production_order_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.product_transformation_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'products/view_product_transformation/' + $(this).parent('.product_transformation_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.adjustment_link2', function() {
        $('#myModal').modal({remote: site.base_url + 'products/view_adjustment/' + $(this).attr('id')});
        $('#myModal').modal('show');
    });
    // Wappsi code
    $('body').on('click', '.sequential_count_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'products/view_sequential_count/' + $(this).parent('.sequential_count_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.sequential_count_link2', function() {
        $('#myModal').modal({remote: site.base_url + 'products/view_sequential_count/' + $(this).attr('id')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.reaccount_sale_link', function() {
        $('#myModal').modal({remote: site.base_url + 'sales/contabilizarVenta/' + $(this).data('invoiceid')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.reaccount_purchase_link', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/contabilizarCompra/' + $(this).data('invoiceid')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.reaccount_adjustment_link', function() {
        $('#myModal').modal({remote: site.base_url + 'products/contabilizarAjuste/' + $(this).data('adjustmentid')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.reaccount_deposit_link', function() {
        $('#myModal').modal({remote: site.base_url + 'customers/contabilizarDeposito/' + $(this).data('invoiceid')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.reaccount_pr_deposit_link', function() {
        $('#myModal').modal({remote: site.base_url + 'suppliers/contabilizarDeposito/' + $(this).data('invoiceid')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.reaccount_expense_link', function() {
        $('#myModal').modal({remote: site.base_url + 'purchases/contabilizarGasto/' + $(this).data('invoiceid')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.multipayment_link td:not(:last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'payments/multiView/' + $(this).parent('.multipayment_link').prop('id')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.deposit_link td:not(:last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'customers/deposit_note/' + $(this).parent('.deposit_link').prop('id')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.deposit_link_supplier td:not(:last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'suppliers/deposit_note/' + $(this).parent('.deposit_link_supplier').prop('id')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.warehouse_link td:not(:last-child, :nth-child(1), :nth-child(9))', function() {
        $('#myModal').modal({remote: site.base_url + 'system_settings/warehouse_view/' + $(this).parent('.warehouse_link').attr('id')});
        $('#myModal').modal('show');
        //window.location.href = site.base_url + 'sales/view/' + $(this).parent('.invoice_link').attr('id');
    });

    $('body').on('click', '.ua_link td', function() {
        $('#myModal').modal({remote: site.base_url + 'system_settings/ua_view/' + $(this).parent('.ua_link').attr('id')});
        $('#myModal').modal('show');
        //window.location.href = site.base_url + 'sales/view/' + $(this).parent('.invoice_link').attr('id');
    });

    $('body').on('click', '.wms_picking_link td:not(:last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'wms/wms_picking_view/' + $(this).parent('.wms_picking_link').attr('id')});
        $('#myModal').modal('show');
    });

    $('body').on('click', '.biller_link td:not(:last-child, :nth-child(1), :nth-child(7))', function() {
        $('#myModal').modal({remote: site.base_url + 'billers/view/' + $(this).parent('.biller_link').attr('id')});
        $('#myModal').modal('show');
        //window.location.href = site.base_url + 'sales/view/' + $(this).parent('.invoice_link').attr('id');
    });

     $('body').on('click', '.pos_register_movement td:not(:first-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'pos/pos_register_movement_view/' + $(this).parent('.pos_register_movement').attr('id')});
        $('#myModal').modal('show');
        //window.location.href = site.base_url + 'sales/view/' + $(this).parent('.invoice_link').attr('id');
    });

     $('body').on('click', '.payment_collection td:not(:first-child, :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'payments_collections/modal_view/' + $(this).parent('.payment_collection').attr('id')});
        $('#myModal').modal('show');
        //window.location.href = site.base_url + 'sales/view/' + $(this).parent('.invoice_link').attr('id');
    });

    $('body').on('click', '.order_transfer_link td:not(:first-child,:last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'transfers/order_view/' + $(this).parent('.order_transfer_link').attr('id')});
        $('#myModal').modal('show');
    });


    // $('body').on('click', '.import_link td:not(:first-child, :nth-child(6), :last-child)', function() {
    //     $('#myModal').modal({remote: site.base_url + 'purchases/import_modal_view/' + $(this).parent('.import_link').attr('id')});
    //     $('#myModal').modal('show');
    // });

    $(document).on('click', '.por_notification_status', function(e) {
        e.preventDefault;
        var row = $(this).closest('tr');
        var id = row.attr('id');
        var notifid = $(this).data('notifid');
        if (row.hasClass('making_order_link')) {
            $('#myModal').modal({remote: site.base_url + 'production_order/edit_notification/' + id+'/'+notifid});
            $('#myModal').modal('show');
        }
        return false;
    });
    $('body').on('click', '.making_order_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'production_order/modal_view/' + $(this).parent('.making_order_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.cutting_order_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'production_order/modal_order_cutting_view/' + $(this).parent('.cutting_order_link').attr('id')});
        $('#myModal').modal('show');
    });
    $('body').on('click', '.assemble_order_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        $('#myModal').modal({remote: site.base_url + 'production_order/modal_order_assemble_view/' + $(this).parent('.assemble_order_link').attr('id')});
        $('#myModal').modal('show');
    });


    $('body').on('click', '.packing_order_link td:not(:first-child, :nth-last-child(2), :last-child)', function() {
        if (window.screen.height <= 400) {
            $('#myModal').modal({remote: site.base_url + 'production_order/change_packing_order_items_status/' + $(this).parent('.packing_order_link').attr('id')});
        } else {
            $('#myModal').modal({remote: site.base_url + 'production_order/modal_order_packing_view/' + $(this).parent('.packing_order_link').attr('id')});
        }

        $('#myModal').modal('show');
    });


    // Termina Wappsi code
    $('#clearLS').click(function(event) {
        bootbox.confirm(lang.r_u_sure, function(result) {
        if(result == true) {
            localStorage.clear();
            location.reload();
        }
        });
        return false;
    });
    $(document).on('click', '[data-toggle="ajax"]', function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        $.get(href, function( data ) {
            $("#myModal").html(data).modal();
        });
    });
    $(".sortable_rows").sortable({
        items: "> tr",
        appendTo: "parent",
        helper: "clone",
        placeholder: "ui-sort-placeholder",
        axis: "x",
        update: function(event, ui) {
            var item_id = $(ui.item).attr('data-item-id');
            // console.log(ui.item.index());
        }
    }).disableSelection();

    if (site.run_update == true) {
        swal({
            title: "¡Actualización disponible!",
            text: "Se ha encontrado una actualización disponible, el sistema se actualizará automáticamente y por razones de seguridad se bloqueará su uso hasta que haya finalizado",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Aceptar",
            closeOnConfirm: false
        }, function () {
            $('.confirm').remove();
            $('.sa-button-container').html('<button class="btn btn-primary" disabled>'+lang['updating']+'</button>');
            location.href = site.base_url + "system_settings/get_update_files";
        });
    }

});

function fixAddItemnTotals() {
    var ai = $("#sticker");
    var aiTop = (ai.position().top)+600;
    var bt = $("#bottom-total");
    $(window).scroll(function() {
        var windowpos = $(window).scrollTop();
        if (windowpos >= aiTop) {
            ai.addClass("stick").css('width', ai.parent('form').width()).css('zIndex', 2);
            if ($.cookie('sma_theme_fixed') == 'yes') { ai.css('top', '40px'); } else { ai.css('top', 0); }
            $('#add_item').removeClass('input-lg');
            $('.addIcon').removeClass('fa-2x');
        } else {
            ai.removeClass("stick").css('width', bt.parent('form').width()).css('zIndex', 2);
            if ($.cookie('sma_theme_fixed') == 'yes') { ai.css('top', 0); }
            $('#add_item').addClass('input-lg');
            $('.addIcon').addClass('fa-2x');
        }
        if (windowpos <= ($(document).height() - $(window).height() - 120)) {
            bt.css('position', 'fixed').css('bottom', 0).css('width', bt.parent('form').width()).css('zIndex', 2);
        } else {
            bt.css('position', 'static').css('width', ai.parent('form').width()).css('zIndex', 2);
        }
    });
}

function ItemnTotals() {
    fixAddItemnTotals();
    $(window).bind("resize", fixAddItemnTotals);
}

function getSlug(title, type) {
    var slug_url = site.base_url+'welcome/slug';
    $.get(slug_url, {title: title, type: type}, function (slug) {
        $('#slug').val(slug).change();
    });
}

function openImg(img) {
    var imgwindow = window.open('', 'sma_pos_img');
    imgwindow.document.write('<html><head><title>Screenshot</title>');
    imgwindow.document.write('<link rel="stylesheet" href="'+site.assets+'styles/helpers/bootstrap.min.css" type="text/css" />');
    imgwindow.document.write('</head><body style="display:flex;align-items:center;justify-content:center;">');
    imgwindow.document.write('<img src="'+img+'" class="img-thumbnail"/>');
    imgwindow.document.write('</body></html>');
    return true;
}

if(site.settings.auto_detect_barcode == 1) {
    $(document).ready(function() {
        var pressed = false;
        var chars = [];
        $(window).keypress(function(e) {
            chars.push(String.fromCharCode(e.which));
            if (pressed == false) {
                setTimeout(function(){
                    if (chars.length >= 8) {
                        var barcode = chars.join("");
                        $( "#add_item" ).focus().autocomplete( "search", barcode );
                    }
                    chars = [];
                    pressed = false;
                },200);
            }
            pressed = true;
        });
    });
}
$('.sortable_table tbody').sortable({
    containerSelector: 'tr'
});
$(window).bind("resize", widthFunctions);
$(window).load(widthFunctions);

function receivedADQ(x)
{
    if (x == '0') {
        return '<div class="text-center text-danger"><i class="fa fa-ban" data-toggle="tooltip" data-placement="top" title="'+lang["rejected"]+'"></i></div>';
    } else if(x == '1') {
        return '<div class="text-center text-info"><i class="fa fa-check" data-toggle="tooltip" data-placement="top" title="'+lang["received"]+'"></i></div>';
    } else if (x == null) {
        return '<div class="text-center" style="color: #888888;"><i class="fa fa-circle" data-toggle="tooltip" data-placement="top" title="'+lang["pending"]+'"></i></div>';
    }
}

function aceptedDIAN(document_status, message, error_message, sale_id)
{
    message = (message == null) ? '' : message;
    possible_solution = false;
    error_message = null;

    if (document_status == '0') {
        data_error_message = (error_message != '' ? 'data-error_message="'+error_message+'" data-sale_id="'+ sale_id +'"' : '');
        return '<div class="text-center"><span class="payment_status pending_errors label label-danger" '+ data_error_message +' data-toggle="tooltip" data-placement="top" data-title="'+message+'">'+lang["not_sent"]+'</span></div>';
    } else if(document_status == '1') {
        data_error_message = (error_message != '' ? 'data-error_message="'+error_message+'" data-sale_id="'+ sale_id +'"' : '');
        return '<div class="text-center"><span class="payment_status pending_errors label label-warning" '+ data_error_message +' data-toggle="tooltip" data-placement="top" data-title="'+message+'">'+lang["pending"]+'</span></div>';
    } else if(document_status == '2') {
        return '<div class="text-center"><span class="payment_status label label-primary" data-toggle="tooltip" data-placement="top" title="'+message+'">'+lang["accepted"]+'</span></div>';
    } else if (document_status == '3') {
        data_error_message = (error_message != '' ? 'data-error_message="'+error_message+'" data-sale_id="'+ sale_id +'"' : '');
        return '<div class="text-center"><span class="payment_status pending_errors label label-success" '+ data_error_message +' data-toggle="tooltip" data-placement="top" title="'+message+'">'+lang["sent"]+'</span></div>';
    }
}

function sended_email(sended_email)
{
    if (sended_email == '0') {
        return '<div class="text-center text-danger"><i class="" data-toggle="tooltip" data-placement="top" title="'+lang["pending"]+'"></i></div>';
    } else if(sended_email == '1') {
        return '<div class="text-center text-info"><i class="fa fa-check"data-toggle="tooltip" data-placement="top" title="'+lang["sent"]+'"></i></div>';
    }
}

function renderActionsButton(actions_button, accepted_dian, sale_id, document_type, sale_status, electronic_document, dateInv=null)
{
    var actions = $(actions_button);

    if (sale_status == 'returned') {
        actions.find('.dropdown-menu').find('.link_edit_sale').remove();
    }

    if (fe_technology_provider == false) {
        if (document_type == false) {
            actions.find('.dropdown-menu').find('.link_return_sale').remove();
        }
    } else {
        if (electronic_document == 1) {
            if (document_type == FACTURA_POS || document_type == FACTURA_DETAL) {
                path_show_previous_XML = 'admin/sales/showPreviousRequest/'+sale_id;
                actions.find('.dropdown-menu').prepend('<li><a href="'+ path_show_previous_XML +'" target="_blank"><i class="fa fa-file-code-o"></i> Mostrar Request Previo</a></li>');

                if (fe_technology_provider == DELCOP) {
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/document_status/'+sale_id+'"><i class="fa fa-info"></i> Estado del documento</a></li>');
                }

                if (sale_status == "completed" &&  fe_technology_provider == 3) { // <- simba tecnology provider
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusSimba/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                }

                if (accepted_dian == '0' || accepted_dian == '1') {
                    actions.find('.dropdown-menu').find('.print_document').remove();
                    actions.find('.dropdown-menu').find('.link_return_sale').remove();
                    actions.find('.dropdown-menu').find('.link_debit_note').remove();

                    if (sale_status == 'completed') {
                        const ActualDay = new Date();
                        ActualDay.setHours(0, 0, 0, 0);
                        const inputDateInv = new Date(dateInv);
                        inputDateInv.setHours(0, 0, 0, 0);
                        if (ActualDay.getTime() !== inputDateInv.getTime()) {
                            actions.find('.dropdown-menu').prepend('<li class="change_date_electronic_document" data-document_id="'+ sale_id +'"><a href="admin/sales/change_date/'+sale_id+'"><i class="fas fa-refresh"></i> Cambiar fecha y reenviar factura</a></li>');
                        }
                        actions.find('.dropdown-menu').prepend('<li class="resend_electronic_document" data-document_id="'+ sale_id +'"><a href="admin/sales/resend_electronic_document/'+sale_id+'"><i class="far fa-paper-plane"></i> Reenviar factura</a></li>');
                    }

                    if (fe_technology_provider == 5) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusBPM/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }

                    if (fe_technology_provider == CADENA) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }
                } else if (accepted_dian == '2') {
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/saleView/'+sale_id+'/0/0/0/0/1" target="_blank"><i class="fa fa-print"></i> Impresión tirilla</a></li>');
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/download_fe_xml/'+sale_id+'"><i class="far fa-file-code"></i> '+lang['xml_download']+'</a></li>');
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/download_fe_pdf/'+sale_id+'" target="_blank"><i class="far fa-file-pdf"></i> '+lang['pdf_download']+'</a></li>');
                    actions.find('.dropdown-menu').prepend('<li><a class="email_confirmation" data-sale_id="'+sale_id+'"><i class="fa fa-envelope-o"></i> '+lang['send_FE']+'</a></li>');
                } else if (accepted_dian == '3') {
                    if (fe_technology_provider == CADENA) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }
                    
                    if (fe_technology_provider == 5) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusBPM/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }

                }

                actions.find('.dropdown-menu').prepend('<li><label style="padding-left: 20px; padding-top: 10px">Facturación electrónica</label></li>');
            } else if (document_type == NOTA_CREDITO_POS || document_type == NOTA_CREDITO_DETAL) {
                if (fe_technology_provider == DELCOP) {
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/document_status/'+sale_id+'"><i class="far fa-info"></i> Estado del documento</a></li>');
                }

                if (sale_status == "returned" && fe_technology_provider == 3) { // <- simba tecnology provider
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusSimba/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                }

                path_show_previous_XML = 'admin/sales/showPreviousRequest/'+sale_id;
                actions.find('.dropdown-menu').prepend('<li><a href="'+ path_show_previous_XML +'" target="_blank"><i class="far fa-file-code"></i> Mostrar Request Previo</a></li>');

                if (accepted_dian == '0' || accepted_dian == '1') {
                    actions.find('.dropdown-menu').find('.print_document').remove();
                    actions.find('.dropdown-menu').find('.link_return_sale').remove();

                    if (sale_status == 'returned') {
                        const ActualDay = new Date();
                        ActualDay.setHours(0, 0, 0, 0);
                        const inputDateInv = new Date(dateInv);
                        inputDateInv.setHours(0, 0, 0, 0);
                        if (ActualDay.getTime() !== inputDateInv.getTime()) {
                            actions.find('.dropdown-menu').prepend('<li class="change_date_electronic_document" data-document_id="'+ sale_id +'"><a href="admin/sales/change_date/'+sale_id+'"><i class="fas fa-refresh"></i> Cambiar fecha y reenviar Nota crédito</a></li>');
                        }
                        actions.find('.dropdown-menu').prepend('<li class="resend_electronic_document" data-document_id="'+ sale_id +'"><a href="admin/sales/resend_electronic_document/'+sale_id+'"><i class="far fa-paper-plane"></i> Reenviar Nota crédito</a></li>');
                    }

                    if (fe_technology_provider == 5) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusBPM/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }
  
                } else if (accepted_dian == '2') {
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/download_fe_xml/'+sale_id+'"><i class="far fa-file-code"></i> '+lang['xml_download']+'</a></li>');
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/download_fe_pdf/'+sale_id+'" target="_blank"><i class="far fa-file-pdf"></i> '+lang['pdf_download']+'</a></li>');
                    actions.find('.dropdown-menu').prepend('<li><a class="email_confirmation" data-sale_id="'+sale_id+'"><i class="fa fa-envelope-o"></i> '+lang['send_FE']+'</a></li>');
                } else if (accepted_dian == '3') {
                    if (fe_technology_provider == CADENA) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Cambiar estado</a></li>');
                    }
                }

                actions.find('.dropdown-menu').prepend('<li><label style="padding-left: 20px; padding-top: 10px">Facturación electrónica</label></li>');
                actions.find('.dropdown-menu').find('.link_invoice_action').remove();
            } else if (document_type == NOTA_DEBITO_POS || document_type == NOTA_DEBITO_DETAL) {
                if (fe_technology_provider == DELCOP) {
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/document_status/'+sale_id+'"><i class="fa fa-info"></i> Estado del documento</a></li>');
                }
                
                if (fe_technology_provider == 3) { // <- simba tecnology provider
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusSimba/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                }

                path_show_previous_XML = 'admin/sales/showPreviousRequest/'+sale_id;
                actions.find('.dropdown-menu').prepend('<li><a href="'+ path_show_previous_XML +'" target="_blank"><i class="far fa-file-code"></i> Mostrar Request Previo</a></li>');

                if (accepted_dian == '0' || accepted_dian == '1') {
                    actions.find('.dropdown-menu').find('.print_document').remove();
                    actions.find('.dropdown-menu').find('.link_return_sale').remove();

                    actions.find('.dropdown-menu').prepend('<li class="resend_electronic_document" data-document_id="'+ sale_id +'"><a href="admin/sales/resend_electronic_document/'+sale_id+'"><i class="far fa-paper-plane"></i> Reenviar Nota débito</a></li>');

                    if (fe_technology_provider == 5) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusBPM/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }

                } else if (accepted_dian == '2') {
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/download_fe_xml/'+sale_id+'"><i class="far fa-file-code"></i> '+lang['xml_download']+'</a></li>');
                    actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/download_fe_pdf/'+sale_id+'" target="_blank"><i class="far fa-file-pdf"></i> '+lang['pdf_download']+'</a></li>');
                    actions.find('.dropdown-menu').prepend('<li><a class="email_confirmation" data-sale_id="'+sale_id+'"><i class="fa fa-envelope-o"></i> '+lang['send_FE']+'</a></li>');
                } else if (accepted_dian == '3') {
                    if (fe_technology_provider == CADENA) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Cambiar estado</a></li>');
                    }
                }

                actions.find('.dropdown-menu').prepend('<li><label style="padding-left: 20px; padding-top: 10px">Facturación electrónica</label></li>');
            }
        }
    }

    var str = actions.prop('outerHTML');
    return str;
}

$(document).on('change, keyup', '.only_number',function(){
    this.value = this.value.replace(/[^0-9.]/g,'');
    $(this).prop('title', formatNumber($(this).val()));
});
$(document).on('change, keyup', '#unit_quantity',function(){
    this.value = this.value.replace(/[^0-9.]/g,'');
    $(this).prop('title', formatNumber($(this).val()));
});
$(document).on('blur, focus, mouseover', '.only_number',function(){
    $(this).prop('title', formatNumber($(this).val()));
});
$(document).on('change, keyup', '#vat_no',function(){
    if ($('#tipo_documento').val() == 4 || $('#tipo_documento').val() == 5 || $('#tipo_documento').val() == 7  || $('#tipo_documento').val() == 8 || $('#tipo_documento').val() == 11) {

    } else {
        this.value = this.value.replace(/[^0-9]/g,'');
    }
});
$(document).on('change, keyup', 'input[type="text"]:not(.number_mask)', function(){
    this.value = this.value.replace(/[\'\"{}]/g,'');
});

$(".wizard-big").on('change, keyup', '.only_number',function(){
    this.value = this.value.replace(/[^0-9.]/g,'');
    $(this).prop('title', formatNumber($(this).val()));
});
$(".wizard-big").on('change, keyup', '#unit_quantity',function(){
    this.value = this.value.replace(/[^0-9.]/g,'');
    $(this).prop('title', formatNumber($(this).val()));
});
$(".wizard-big").on('blur, focus, mouseover', '.only_number',function(){
    $(this).prop('title', formatNumber($(this).val()));
});
$(".wizard-big").on('change, keyup', '#vat_no',function(){
    this.value = this.value.replace(/[^0-9]/g,'');
});
$(".wizard-big").on('change, keyup', 'input[type="text"]:not(.number_mask)',function(){
    this.value = this.value.replace(/[\'\"{}]/g,'');
});
function set_same_height(class_name){
    var heights = $("."+class_name).map(function() {
        return $(this).height();
    }).get(),

    maxHeight = Math.max.apply(null, heights);

    $("."+class_name).height(maxHeight);
}

function get_percentage_from_amount(partial_amount, total_amount){
    txt = partial_amount.toString();
    if (txt.indexOf("%") !== -1) {
        return partial_amount;
    } else {
        perc = formatDecimal((parseFloat(partial_amount) * 100) / parseFloat(total_amount), 2);
        return perc+" %";
    }
}

$(document).on('change', 'select[name="paid_by[]"]', function(){
    var index = $('select[name="paid_by[]"]').index($(this));
    var input_amount = $('.amount').eq(index);
    if ($(this).val() == 'Credito' || $(this).find('option:selected').data('duepayment') == 1) {
        input_amount.val(0).prop('readonly', true);
    } else {
        input_amount.prop('readonly', false);
    }
});

function set_number_mask(){
    var digits = parseFloat(site.settings.decimals);
    if (site.settings.rounding == 1) {
        digits += 2;
    }

    $(".number_mask").inputmask("decimal", {
                                            "allowMinus":true,
                                            "allowPlus":true,
                                            "autoGroup":true,
                                            "groupSeparator":".",
                                            "groupSize":3,
                                            "integerDigits":0,
                                            "digits":digits,
                                            "radixPoint":".",
                                            "autoUnmask":true,
                                            "rightAlign":true
                                        });
}

function view_payments(x){
    data = x.split('_');
    return "<a  href='"+(site.base_url)+"sales/payments/"+data[0]+"/"+data[1]+"/"+data[2]+"' data-toggle='modal' data-target='#myModal'><i class='fa fa-eye'></i></a>";
}

function comm_payment_status(x) {
    data = x.split('_');
    paid = false;
    pending = false;
    $.each(data, function(index, val){
        if (val != '') {
            if (val == 1) {
                paid = true;
            } else if (val == 0) {
                pending = true;
            }
        }
    });
    if (!pending && !paid) {
        return '<div class="text-center"><span class="payment_status label label-warning">'+lang['pending']+'</span></div>';
    } else if (!pending && paid) {
        return '<div class="text-center"><span class="payment_status label label-success">'+lang['paid']+'</span></div>';
    } else if (pending && !paid) {
        return '<div class="text-center"><span class="payment_status label label-warning">'+lang['pending']+'</span></div>';
    } else if (pending && paid) {
        return '<div class="text-center"><span class="payment_status label label-info">'+lang['partial']+'</span></div>';
    }
}


function delete_payment(id){
    $.ajax({
        url : site.base_url + "payments/delete_payment/"+id,
        dataType : "JSON"
    }).done(function(data){
        if (data.response) {
            location.reload();
        } else {
            command: toastr.warning(data.message, '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
        }
    });
}

function ask_delete_payment(id){

    bootbox.confirm(lang.r_u_sure, function (result) {
        if (result) {
            delete_payment(id);
        }
    });
}



function payment_method_icons (option) {
    if (!option.id) { return option.text; }
    var ob = '<img  src="'+option.element[0].dataset.imgSrc+'" style="max-width: 25px; margin: 0 5% 0 0;" />'+option.text;
    return ob;
};

function por_num(x) {
    if (x != null) {
        var x = x.split("_");

        return "<div class='text-right'><b>"+formatDecimalNoRound(x[0], 0)+"</b> ("+formatDecimalNoRound(x[1], 0)+")</div>";
    } else {
        return '';
    }
}

function porCurrencyFormat(x) {
    return '<div class="text-right">'+formatDecimalNoRound((x != null ? x : 0), 0)+'</div>';
}

function porPackingWh(x) {
    if (x != null) {
        var html = "";
        var existing = [];
        var x = x.split(",");
        $.each(x, function(index, arr){
            var arr_s = arr.split("_");
            if (existing.indexOf(arr_s[0]) == -1) {
                existing.push(arr_s[0]);
                html += arr_s[3]+" - "+arr_s[1]+" ("+formatDecimalNoRound(arr_s[2], 0)+") </br>";
            }
        });
        return html;
    } else {
        return '';
    }
}

function set_warehouse_search(){
    $('.search_warehouse').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "system_settings/warehouse_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function order_wms_status(x) {
    if(x == 'pending') {
        return '<div class="text-center"><span class="row_status label label-warning">'+lang[x]+'</span></div>';
    } else if(x == 'completed') {
        return '<div class="text-center"><span class="row_status label label-success">'+lang[x]+'</span></div>';
    } else if(x == 'partial') {
        return '<div class="text-center"><span class="row_status label label-info">'+lang[x]+'</span></div>';
    } else {
        return '<div class="text-center"><span class="row_status label label-warning">'+lang['pending']+'</span></div>';
    }
}

function print_por_notification(x){
    if (x != "") {
        x = x.split('_');
        return "<i class='fa "+x[0]+" por_notification_status' data-notifid='"+x[3]+"' style='cursor:pointer !important;font-size:110%;color:#00a859 !important;'></i>";
    }
    return "";
}

function rete_purchase_option_changed(option, trmrate = 1, base_change = false){
    option = option.replace('_option', '');
    rete_option_id = $('#'+option+'_option').val();
    rete_option_assumed = $('#'+option+'_assumed').is(':checked');
    prevamnt = $('#'+option+'_valor').val();
    if (!rete_option_assumed || (localStorage.getItem('assumed_click') == 1)) {
        setReteTotalAmount();
    }
    percentage = $('#'+option+'_option option:selected').data('percentage');
    apply = $('#'+option+'_option option:selected').data('apply');
    account = $('#'+option+'_option option:selected').data('account');
    assumedaccount = $('#'+option+'_option option:selected').data('assumedaccount');
    if ($('#'+option).is(':checked')) {
        $('#'+option+'_account').val(account);
        $('#'+option+'_assumed_account').val(assumedaccount);
        $('#'+option+'_id').val(rete_option_id);
    } else {
        $('#'+option+'_account').val('');
        $('#'+option+'_assumed_account').val('');
        $('#'+option+'_id').val('');
        $('#'+option+'_base').val('');
    }
    if (typeof(max_num_rows) != "undefined") {
        option2 = option.replace('rete_', '');
        setMaxBaseAllowed(apply, option2, $('#rete_applied').val());
    }
    amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    min_base = $('#'+option+'_option option:selected').data('minbase');
    // supplier_validate_min_base_retention = localStorage.getItem('supplier_validate_min_base_retention');
    cAmount = 0;
    if (typeof(return_purchase) === 'undefined') {
        return_purchase = false;
    }
    if (amount >= min_base || return_purchase) {
        $('#'+option+'_base').val(amount);
        cAmount = amount * (percentage / 100);
        cAmount = Math.round(cAmount);
        if ($('#input_rete_fuente_base').length > 0) {
            let update_base = false;
            if (!$('#input_'+option+'_base').data('realbase') || $('#input_'+option+'_base').data('realbase') != amount) {
                $('#input_'+option+'_base').data('realbase', amount);
                update_base = true;
            }
            if (base_change == false) {
                if ((option == 'rete_fuente' && $('#rete_otros_option').val() > 0) || (option == 'rete_otros' && $('#rete_fuente_option').val() > 0)) {
                    $('#input_'+(option == 'rete_fuente' ? 'rete_otros' : 'rete_fuente')+'_base').prop('readonly', false);
                } else {
                    if ($('#input_'+option+'_base').val() == 0 || update_base) {
                        $('#input_'+option+'_base').val(amount);
                    }
                    if (site.settings.withholding_validate_total_base == 1) {
                        $('#input_'+option+'_base').prop('readonly', true);
                    }
                    $('#updateOrderRete').prop('disabled', false);
                }
            }
            if (option == 'rete_fuente' || option == 'rete_otros') {
                new_amount = $('#input_'+option+'_base').val();
                if (new_amount > (amount - (site.settings.withholding_validate_total_base == 0 ? $('#input_'+(option == 'rete_fuente' ? 'rete_otros' : 'rete_fuente')+'_base').val() : 0))) {
                    new_amount = formatDecimal(amount - (site.settings.withholding_validate_total_base == 0 ? $('#input_'+(option == 'rete_fuente' ? 'rete_otros' : 'rete_fuente')+'_base').val() : 0));
                    $('#input_'+option+'_base').val(new_amount);
                }
                $('#'+option+'_base').val(formatDecimal(new_amount));
                cAmount = new_amount * (percentage / 100);
                cAmount = Math.round(cAmount);
                if (base_change) {
                    if (site.settings.withholding_validate_total_base == 1) {
                        $('#input_'+(option == 'rete_fuente' ? 'rete_otros' : 'rete_fuente')+'_base').val(formatDecimal(amount - new_amount));
                        $('#'+(option == 'rete_fuente' ? 'rete_otros' : 'rete_fuente')+'_base').val(formatDecimal(amount - new_amount));
                    }
                    setTimeout(function() {
                        $('#'+(option == 'rete_fuente' ? 'rete_otros' : 'rete_fuente')+'_option').trigger('change');
                    }, 1000);
                    $('#updateOrderRete').prop('disabled', false);
                }
            }
        }
        $('#'+option+'_tax').val(percentage);
        $('#'+option+'_valor_show').val(formatDecimal(trmrate * cAmount));
        $('#'+option+'_valor').val(cAmount);
        $('#'+option+'_apply').val(apply);
        rete_option_amount = formatMoney(cAmount);
        if (!rete_option_assumed || (localStorage.getItem('assumed_click') == 2)) {
            setReteTotalAmount();
        }
    } else {
        $('#'+option+'_valor_show').val(formatDecimal(trmrate * cAmount));
        $('#'+option+'_valor').val(cAmount);
        $('#input_'+option+'_base').val(0).prop('readonly', false);
        if ((option == 'rete_fuente' && !($('#rete_fuente_option').val() > 0) && $('#rete_otros_option').val() > 0) || option == 'rete_otros' && !($('#rete_otros_option').val() > 0) && $('#rete_fuente_option').val() > 0) {
            $('#'+(option == 'rete_fuente' ? 'rete_otros' : 'rete_fuente')+'_option').trigger('change');
        }
        setTimeout(function() {
            setReteTotalAmount();
        }, 850);
    }
    if (localStorage.getItem('assumed_click')) {
        localStorage.removeItem('assumed_click');
    }
    reref = localStorage.getItem('re_retenciones') ? true : false;
    poretenciones = JSON.parse(localStorage.getItem('poretenciones'));
    if (option == 'rete_ica' && cAmount > 0 && reref == false) {
        if (poretenciones && parseFloat(poretenciones.id_rete_bomberil) > 0) {
            $('#rete_bomberil').prop('disabled', false).prop('checked', true);
        } else {
            $('#rete_bomberil').prop('disabled', false).prop('checked', false);
        }
        $('#rete_bomberil_option').trigger('change');
        if (poretenciones && parseFloat(poretenciones.id_rete_autoaviso) > 0) {
            $('#rete_autoaviso').prop('disabled', false).prop('checked', true);
        } else {
            $('#rete_autoaviso').prop('disabled', false).prop('checked', false);
        }
        $('#rete_autoaviso_option').trigger('change');

    } else if (option == 'rete_ica' && cAmount == 0 && reref == false) {
        reset_rete_bomberil();
    }
}


function rete_sale_option_changed(option, trmrate = 1){
    option = option.replace('_option', '');
    rete_option_id = $('#'+option+'_option').val();
    $('#'+option+'_id').val(rete_option_id);
    trmraterete = trmrate;
    prevamnt = $('#'+option+'_valor').val();
    setReteTotalAmount(prevamnt, '-');
    percentage = $('#'+option+'_option option:selected').data('percentage');

    apply =  $('#'+option+'_option option:selected').data('apply');
    account = $('#'+option+'_option option:selected').data('account');
    manual_base = $('#'+option+'_option option:selected').data('manual_base');
    if (typeof(max_num_rows) != "undefined") {
        option2 = option.replace('rete_', '');
        setMaxBaseAllowed(apply, option2, $('#rete_applied').val(), manual_base);
    }

    if ($('#aiu_management').is(':checked')) {
        aiu_option_apply = $('#'+option+'_base_apply').val();
        if (aiu_option_apply == 1) {
            amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        } else if (aiu_option_apply == 2) {
            amount =  formatDecimal(parseFloat(formatDecimal($('.total_aiu_calculated').text())));
        } else if (aiu_option_apply == 3) {
            amount = formatDecimal($('#aiu_util_total').val());
        }
    } else {
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
    }
    min_base = $('#'+option+'_option option:selected').data('minbase');
    $('#'+option+'_account').val(account);
    customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
    key_log = localStorage.getItem('slkeylog');
    cAmount = 0;
    if (typeof(return_sale) === 'undefined') {
        return_sale = false;
    }
    // console.log("return_sale >>"+return_sale);
    if (key_log != 3 && (amount >= min_base || customer_validate_min_base_retention == 'false') || return_sale) {
        $('#'+option+'_base').val(amount);
        cAmount = amount * (percentage / 100);
        if (trmraterete == 1 && site.settings.rounding == 1) {
            cAmount = Math.round(cAmount);
        } else {
            cAmount = formatDecimal(cAmount);
        }
        $('#'+option+'_tax').val(percentage);
        $('#'+option+'_valor').val(cAmount);
        rete_option_amount = formatMoney(cAmount);
        setTimeout(function() {
            setReteTotalAmount(0, '+');
        }, 850);
    } else {
        $('#'+option+'_valor').val(cAmount);
        setTimeout(function() {
            setReteTotalAmount(0, '+');
        }, 850);
    }
    reref = localStorage.getItem('re_retenciones') ? true : false;
    slretenciones = JSON.parse(localStorage.getItem('slretenciones'));
    if (option == 'rete_ica' && cAmount > 0 && reref == false) {
        if (slretenciones && parseFloat(slretenciones.id_rete_bomberil) > 0) {
            $('#rete_bomberil').prop('disabled', false).prop('checked', true);
        } else {
            $('#rete_bomberil').prop('disabled', false).prop('checked', false);
        }
        $('#rete_bomberil_option').trigger('change');
        if (slretenciones && parseFloat(slretenciones.id_rete_autoaviso) > 0) {
            $('#rete_autoaviso').prop('disabled', false).prop('checked', true);
        } else {
            $('#rete_autoaviso').prop('disabled', false).prop('checked', false);
        }
        $('#rete_autoaviso_option').trigger('change');
    } else if (option == 'rete_ica' && cAmount == 0 && reref == false) {
        reset_rete_bomberil();
    }
}

function order_delivery_time(x) {
    if (x != null) {
        var sp = x.split('_');
        var arr_days = {
                        1 : 'monday',
                        2 : 'tuesday',
                        3 : 'wednesday',
                        4 : 'thursday',
                        5 : 'friday',
                        6 : 'saturday',
                        7 : 'sunday',
                        };
        return lang[arr_days[sp[0]]]+" de "+sp[1]+" a "+sp[2];
    } else {
        return ' ';
    }
}

var option_filter = false;
$(document).on('click', '.wizard_index_step', function(e) {
    $('.wizard_index_step').parent('li').removeClass('current').addClass('done');
    $(this).parent('li').removeClass('done').addClass('current');
    option_filter = $(this).data('optionfilter');
    oTable.fnUpdate();
    // oTable.fnDraw();
    e.stopPropagation();
    e.preventDefault();
});

function order_origin(x) {
    if(x == null) {
        return '';
    } else if(x == '1') {
        return 'Orden - Web ERP';
    } else if(x == '2') {
        return 'Orden - App Vendedor';
    } else if(x == '3') {
        return 'Orden - Tienda en Línea';
    } else if(x == '4') {
        return 'Orden - App Clientes';
    } else if(x == '5') {
        return 'POS';
    } else if(x == '6') {
        return 'Venta Suspendida';
    } else if(x == '7') {
        return 'Tienda Pro';
    } else if(x == '8') {
        return 'MarketPro';
    } else if(x == '9') {
        return 'Wshops';
    } else if(x == '10') {
        return 'Tienda B2B';
    }
}

function calcularDigitoVerificacion(myNit){
       var vpri,
            x,
            y,
            z;
        // Se limpia el Nit
        myNit = myNit.replace ( /\s/g, "" ) ; // Espacios
        myNit = myNit.replace ( /,/g,  "" ) ; // Comas
        myNit = myNit.replace ( /\./g, "" ) ; // Puntos
        myNit = myNit.replace ( /-/g,  "" ) ; // Guiones
        // Se valida el nit
        if  ( isNaN ( myNit ) )  {
          console.log ("El nit/cédula '" + myNit + "' no es válido(a).") ;
          return "" ;
        };
        // Procedimiento
        vpri = new Array(16) ;
        z = myNit.length ;
        vpri[1]  =  3 ;
        vpri[2]  =  7 ;
        vpri[3]  = 13 ;
        vpri[4]  = 17 ;
        vpri[5]  = 19 ;
        vpri[6]  = 23 ;
        vpri[7]  = 29 ;
        vpri[8]  = 37 ;
        vpri[9]  = 41 ;
        vpri[10] = 43 ;
        vpri[11] = 47 ;
        vpri[12] = 53 ;
        vpri[13] = 59 ;
        vpri[14] = 67 ;
        vpri[15] = 71 ;
        x = 0 ;
        y = 0 ;
        for  ( var i = 0; i < z; i++ )  {
          y = ( myNit.substr (i, 1 ) ) ;
          x += ( y * vpri [z-i] ) ;
        }
        y = x % 11 ;
        return ( y > 1 ) ? 11 - y : y ;
}


$(document).on('click', '#genNo', function () {
    var no = generateCardNo();
    $(this).parent().parent('.input-group').children('input').val(no).trigger('change');
    return false;
});

$(document).on('change', '#card_no', function(){
    var cn = $(this).val();
    $.ajax({
        type: "get", async: false,
        url: site.base_url + "sales/validate_gift_card/" + cn,
        dataType: "json",
        success: function (data) {
            if (data === false) {

            } else {
                $('#card_no').val('').select();
                command: toastr.error('El número de tarjeta ingresado ya está relacionado en otra tarjeta existente', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        }
    });
});


function hit_response_type(x) {
    if(x == null) {
        return '';
    } else if(x == '1') {
        return '<div class="text-center"><span class="row_status label label-primary">Aceptado</span></div>';
    } else if(x == '2') {
        return '<div class="text-center"><span class="row_status label label-danger">Rechazado</span></div>';
    }
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();
    if (month.length < 2)
        month = '0' + month;
    if (day.length < 2)
        day = '0' + day;
    return [year, month, day].join('-');
}

// PREFERENCES SELECTION START
var pref_arritems;
var pref_arritems_id;
var pref_arritems_name;
$(document).on('click', '.bootbox-close-button', function(e){
    $('#myModal').modal('hide');
    $('#myModal').empty();
    e.preventDefault();
});
$(document).on('keypress', '#search_reference', function(e){
    if (e.keyCode == 13) {
        $('#pv_search').click();
    }
});
$(document).on('click', '.product_preference_select',function(e){
    if (!$(this).hasClass('btn-outline')) {
        pcat_sl = formatDecimal($(this).parent('li').data('pcat_sl'));
        selected_length = $('li[data-pcat="'+$(this).parent('li').data('pcat')+'"]').find('button[class="product_preference_select btn-outline"]').length;
        if (pcat_sl == 0 || pcat_sl > 0 && pcat_sl > selected_length) {
            $(this).addClass('btn-outline');
            $(this).find('input.radio_pref').iCheck('check');
        } else {
            $(this).removeClass('btn-outline');
            $(this).find('input.radio_pref').iCheck('uncheck');
        }
    } else {
        $(this).removeClass('btn-outline');
        $(this).find('input.radio_pref').iCheck('uncheck');
    }
    $(this).blur();
});
$(document).on('ifClicked', '.radio_pref',function(e){
    e.stopPropagation();
    var checkbtn = $(this);
    setTimeout(function() {
        checkbtn.parent().parent().parent().find('.product_preference_select').trigger('click');
    }, 350);
});
$(document).on('click', '.set_product_preference', function(e){
    if (!validate_min_selected()) {
        var pfs = [];
        var pfs_text = "";
        var preferences_data = [];
        $.each(pref_arritems[pref_arritems_id].preferences, function(index, preference){
            preferences_data[preference.id] = "("+preference.category_name+") "+preference.name;
        });
        var preferences_text = '';
        var preferences_selected = [];
        var parr = [];
        var selected_prs = 0;
        $.each($('.product_preference_select'), function(index, button){
            if ($(button).hasClass('btn-outline')) {
                if (preferences_selected[$(button).data('pfcatid')] === undefined) {
                    preferences_selected[$(button).data('pfcatid')] = [];
                }
                preferences_selected[$(button).data('pfcatid')].push($(button).data('pfid'));
                psplit = preferences_data[$(button).data('pfid')].split(')');
                psplit_index = psplit[0].replace("(", "");
                if (parr[psplit_index] === undefined) {
                    parr[psplit_index] = [];
                }
                parr[psplit_index].push(psplit[1]);
                selected_prs++;
            }
        });
        $('.loading_text').fadeIn();
        setTimeout(function() {
            pref_arritems[pref_arritems_id].preferences_selected = null;
            pref_arritems[pref_arritems_id].preferences_text = null;
            if (selected_prs > 0) {
                phtml = "";
                for (let preference_category in parr) {
                    phtml += "<b>"+preference_category+" : </b>";
                    for (let index in parr[preference_category]) {
                        phtml += parr[preference_category][index]+", ";
                    }
                }
                pref_arritems[pref_arritems_id].preferences_selected = preferences_selected;
                pref_arritems[pref_arritems_id].preferences_text = phtml;
            }
            localStorage.setItem(pref_arritems_name, JSON.stringify(pref_arritems));
            loadItems();
            $('#myModal').empty();
            $('#myModal').modal('hide');
        }, 850);
    }
});
$(document).on('click', '.unset_product_preference', function(e){
    validate_close_modal();
});
// PREFERENCES SELECTION END

function encode_url_spaces(url){
    return url.replace(" ", "__");
}

var isToastShowing = false;

function apmv_type(x){
    if (x == 1) {
        return lang.addition;
    } else if (x == 2) {
        return lang.substraction;
    } else {
        return 'error';
    }
}


function ua_type(x){
    //1 = Edición, 2 = Eliminación, 3 = Agregar, 4 = consulta, 5. contabilidad cierre
    if (x == 1) {
        return '<div class="text-center"><span class="general_status label label-primary" style="cursor:pointer;">Edición</span></div>';
    } else if (x == 2) {
        return '<div class="text-center"><span class="general_status label label-danger" style="cursor:pointer;">Eliminación</span></div>';
    } else if (x == 3) {
        return '<div class="text-center"><span class="general_status label label-success" style="cursor:pointer;">Adición</span></div>';
    } else if (x == 4) {
        return '<div class="text-center"><span class="general_status label label-default" style="cursor:pointer;">Consulta</span></div>';
    } else if (x == 5) {
        return '<div class="text-center"><span class="general_status label label-default" style="cursor:pointer;">Cierre contable</span></div>';
    } else if (x == 6) {
        return '<div class="text-center"><span class="general_status label label-default" style="cursor:pointer;">Exportó</span></div>';
    } else if (x == 7) {
        return '<div class="text-center"><span class="general_status label label-success" style="cursor:pointer;">Ingreso</span></div>';
    } else {
        return 'Error';
    }
}

function ua_max_description(x){
    if (x.length > 100) {
    return x.substring(0, 100)+"...";
  } else {
    return x;
  }
}

function language(x){
    return (lang[x] !== undefined ? lang[x] : x);
}

var header_alert_texts = [];
function header_alert(type, text, seconds = 15, callback){
    if (header_alert_texts.includes(text)) {
        return false;
    } else {
        header_alert_texts.push(text);
    }
    seconds = seconds ? seconds : 9;
    $('.div_header_alerts').css('margin-top', '1%');
    var class_type = '';
    if (type == 'success') {
        class_type = 'alert-success';
    } else if (type == 'danger' || type == 'error') {
        class_type = 'alert-danger';
    } else if (type == 'warning') {
        class_type = 'alert-warning';
    } else if (type == 'info') {
        class_type = 'alert-info';
    }

    if (callback) {
        _callbackAlert = callback;
    }

    var alert = $('.header_alert.'+class_type+'[data-alertnum="0"]');
    var outerHTML = alert.prop('outerHTML');
    var num_alerts = $('.header_alert.'+class_type).length;
    var newOuterHTML = outerHTML.replace('data-alertnum="0"', 'data-alertnum="'+(num_alerts)+'"');
    $('.div_header_alerts').append(newOuterHTML);
    $('.header_alert.'+class_type+'[data-alertnum="'+num_alerts+'"]').find('.text_alert').html(text);
    $('.header_alert.'+class_type+'[data-alertnum="'+num_alerts+'"]').fadeIn();

    _intervalId = setTimeout(function() {
        remove_header_alert('.header_alert.'+class_type+'[data-alertnum="'+num_alerts+'"]', callback);
    }, (seconds * 1000));
    $('html, body').animate({scrollTop : 0}, 800);
}

function remove_header_alert(input, callback){
    $(input).fadeOut();
    $('.div_header_alerts').css('margin-top', '0');
    if (callback) {
        callback()
    }
}

$(document).on('click', '.close', function() {
    clearTimeout(_intervalId);
    if (_callbackAlert) {
        _callbackAlert()
    }
});

function nscustomer(customer = null) {
    if (customer) {
        $('#customer').val(customer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    } else {
        $('#customer').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
              $.ajax({
                type: "get", async: false,
                url: site.base_url+"customers/getcustomer/" + $(element).val(),
                dataType: "json",
                success: function (data) {
                  callback(data[0]);
                }
              });
            },
            ajax: {
              url: site.base_url+"customers/suggestions",
              dataType: 'json',
              quietMillis: 15,
              data: function (term, page) {
                return {
                  term: term,
                  limit: 10
                };
              },
              results: function (data, page) {
                if (data.results != null) {
                  return {results: data.results};
                } else {
                  return {results: [{id: '', text: lang.no_match_found}]};
                }
              }
            }
        });
    }
}

function getStates()
{
    var country = $('#country option:selected').attr('code');
    $('#countryCode').val(country);

    $.ajax({
        url: site.base_url+"customers/get_states/"+country,
    }).done(function(response) {
        $('#state').html(response);
        $('#state').trigger('change');
    }).fail(function(data) {
      console.log(data.responseText);
    });
}

function getCities()
{
    var state = $('#state option:selected').data('code');
    $('#stateCode').val(state);

    $.ajax({
      url: site.base_url+"customers/get_cities/"+state,
    }).done(function(response) {
        $('#city').html(response);
        $('#city').trigger('change');
    }).fail(function(data) {
      console.log(data.responseText);
    });
}

function getZones()
{
    var city = $('#city option:selected').data('code');
    $('#cityCode').val(city);

    $.ajax({
        url: site.base_url+'customers/get_zones/'+city
    }).done(function(data) {
        $('#zone').html(data);
        $('#zone').trigger('change');
    });
}

function getSubzones()
{
    var code = $('#zone option:selected').data('code');
    $('#zoneCode').val(code);

    $.ajax({
        url: site.base_url +'customers/get_subzones/'+ code
    }).done(function(data){
        $('#subzone').html(data);
        $('#subzone').trigger('change');
    });
}

function getSubzoneCode()
{
    var code = $('#subzone option:selected').data('code');
    $('#subzoneCode').val(code);
}
$(document).on('change', 'select[name="biller_id"], #biller_id, select[name="biller"]', function(){
    set_global_biller_warehouses_related($(this));
});
function set_global_biller_warehouses_related(biller_input){
    wh_input = $('select[name="warehouse_id"]').length > 0 ? $('select[name="warehouse_id"]') : $('select[name="warehouse"]');
    wh_input_id = wh_input.prop('id');
    biller_input_id = biller_input.prop('id');
    if (billers_data[$('#'+biller_input_id).val()]) { // se agrega esta validacion para que no entre si no existe billers_data con el id seleccionado por ejemplo all
        warehouses_related = billers_data[$('#'+biller_input_id).val()].warehouses_related;
        if (warehouses_related) {
            $('#'+wh_input_id+' option').each(function(index, option){
                $(option).prop('disabled', false);
                if (jQuery.inArray($(option).val(), warehouses_related) == -1) {
                    $(option).prop('disabled', true);
                }
            });
            setTimeout(function() {
                $('#'+wh_input_id).select2();
            }, 850);
        }
    }
}
var not_unique_code = false;
$(document).on('change', '.validate_code', function() {
    code = $(this);
    cmodule = $(this).data('module');
    id = $(this).data('id') ? $(this).data('id') : false;
    $.ajax({
        url : site.base_url + "system_settings/valid_unique_code",
        type : 'get',
        dataType : 'JSON',
        data : {
            module : cmodule,
            code : code.val(),
            id : id,
        }
    }).done(function(data){
        if (data.exists == true) {
            header_alert('error', lang.not_unique_code);
            code.val('').focus();
            not_unique_code = true;
        } else {
            if (not_unique_code == true) {
                header_alert('success', lang.unique_code_correct);
                not_unique_code = false;
            }
        }
    });
});

var recuento_mdl_code = [];
$(document).on('click', '#consecutive_code, .consecutive_code', function(){
    var input = $(this);
    var input_parent = input.parent('.input-group').children('input');
    var input_consecutive_setted = input_parent.parent('.input-group').parent('.form-group').children('.code_consecutive_setted');
    var mdl = input_parent.data('module');
    if (input_parent.prop('readonly') == true) {
        header_alert('warning', 'No puede reasignar código consecutivo');
        return false;
    }
    if (recuento_mdl_code[mdl+"_code"]) {
        recuento_mdl_code[mdl+"_code"]++;
        input_parent.val(recuento_mdl_code[mdl+"_code"]).trigger('change').prop('readonly', true);
        input_consecutive_setted.val(1);
    } else {
         $.ajax({
           url : site.base_url+"system_settings/get_code_consecutive",
           data : { module : mdl } ,
           type : "get",
           dataType : "JSON"
        }).done(function(data){
            header_alert('info', lang.consecutive_code_info);
            input_parent.val(data.code).trigger('change').prop('readonly', true);
            recuento_mdl_code[mdl+"_code"] = data.code;
            input_consecutive_setted.val(1);
        });
    }
});

function tag_option_color(option){
    if (!option.id) {
        return option.text;
    }

    return $('<div style="display:flex;align-items:center;"><i class="tag_option_color fa-solid '+($(option.element).data('type') == 2 ? "fa-circle-exclamation" : "fa-circle")+'" style="color:' + $(option.element).data('color') + ';"></i> <span>' + option.text+'</span></div>');


}

function tag_reference(x) {
    x_arr = x.split('__');
    if (x_arr.length > 1) {
        description = x_arr[0];
        color = x_arr[1];
        type = x_arr[2];
        reference = x_arr[3];
        return '<i data-toggle="tooltip" data-placement="top" title="'+(type == 2 ? "["+lang.urgent+"] " : "")+description+'" class="tag_option_color fa-solid '+(type == 2 ? "fa-circle-exclamation" : "fa-circle")+'" style="color:' + color + ';"></i>'+reference;
    } else {
        return x;
    }
}

function approved_status(x){
    if (x == 1) {
        return '<div class="text-center"><span class="discontinued_status label label-success">'+lang['approved']+'</span></div>';
    } else {
        return '<div class="text-center"><span class="discontinued_status label label-warning">'+lang['pending']+'</span></div>';
    }
}

function calculate_second_tax(second_tax, amount, tax_rate, tax_method, st_included = false, discount = 0){
    // const stackTrace = new Error().stack;
    //     if (stackTrace) {
    //         const lineaDeLlamada = stackTrace.split('\n')[2];
    //         console.log(" >>> "+lineaDeLlamada);
    //     }
    if (second_tax) {
        second_tax = second_tax.toString();
        percentage = false;
        ptr = tax_rates[tax_rate] !== undefined ? tax_rates[tax_rate] : tax_rates[site.settings.product_default_exempt_tax_rate];
        amount_base = amount;
        if (second_tax.indexOf("%") !== -1) {
            percentage = formatDecimal(second_tax.replace(/%/g, '')) / 100;
            if (st_included) {
                tax_perc = (ptr.rate / 100);
                amount_base = (amount / (parseFloat(1)+(percentage+tax_perc))) - discount;
                second_tax_val = formatDecimal(amount_base * percentage);
            } else {
                tax_val = calculateTax(ptr, amount, tax_method);
                amount_base = amount - tax_val[0] - discount;
                second_tax_val = formatDecimal(amount_base * percentage);
            }
        } else {
            second_tax_val = second_tax;
        }
        // console.log([formatDecimal(amount_base),formatDecimal(second_tax_val)]);
        return [formatDecimal(amount_base),formatDecimal(second_tax_val)];
    } else {
        return [0,0];
    }
}

function reset_purchase_rete_base(cambio){
    if ($('#rete_fuente_option').val() > 0 && $('#rete_otros_option').val() > 0 && ($('#input_rete_fuente_base').val() > 0 || $('#input_rete_fuente_base').val() > 0) || (site.settings.withholding_validate_total_base == 0 && ($('#rete_fuente_option').val() > 0 || $('#rete_otros_option').val() > 0))) {
        localStorage.removeItem('poretenciones');
        recalcular_poretenciones();
        $('#updateOrderRete').trigger('click');
        header_alert('warning', 'Porfavor reingrese las bases de las rete fuentes, se reiniciaron a 0 por cambio en los items ('+cambio+')');
    }
}

$(document).ready(function(){ // <- esto se agrega para que no se inactive el boton actualizar cuando en el quantity le dan enter
    if (site.v_action == 'print_barcodes') {
        const formulario = document.getElementById('barcode-print-form');
        formulario.addEventListener('submit', function(event){
            const boton = document.getElementsByName('print')[0];
            boton.removeAttribute('disabled');
        });
        $('#style').trigger('change');
    }
});

function show_mode () {  
    if (site.v_action == 'document_types') {
        const factura_electronica = $('#factura_electronica').val();
        const module_selected = $('#module').val();
        const allowedModules = [1, 2, 3, 4, 26, 27];
        const showModeSection = factura_electronica === '1' &&  allowedModules.includes(parseInt(module_selected));
        $('.mode_section').css('display', showModeSection ? 'block' : 'none');
        $('#mode').attr('required', showModeSection);
        if (!showModeSection) {
            $('#mode').val('');
        }
    }
}