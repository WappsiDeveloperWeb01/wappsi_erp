<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    var max_qpr = "<?= isset($max_qpr) ? $max_qpr : 0 ?>";
    var POR_SUGGESTIONS_TYPE = "<?= POR_PACKING_TYPE ?>";
    var assembles_id = JSON.parse('<?= isset($assembles_ids) ? json_encode(["ids" => $assembles_ids]) : "false" ?>');
    console.log(assembles_id);

    <?php if (isset($assemble_order_id)): ?>
        if (localStorage.getItem('pckitems')) {
            localStorage.removeItem('pckitems');
        }
        if (localStorage.getItem('pckcomproducts')) {
            localStorage.removeItem('pckcomproducts');
        }
        if (localStorage.getItem('pckdate')) {
            localStorage.removeItem('pckdate');
        }
        if (localStorage.getItem('pckest_date')) {
            localStorage.removeItem('pckest_date');
        }
        if (localStorage.getItem('pckproduction_order')) {
            localStorage.removeItem('pckproduction_order');
        }
        if (localStorage.getItem('pckcutting_order')) {
            localStorage.removeItem('pckcutting_order');
        }
        if (localStorage.getItem('pckreference_no')) {
            localStorage.removeItem('pckreference_no');
        }
        if (localStorage.getItem('pcksupplier')) {
            localStorage.removeItem('pcksupplier');
        }
        localStorage.setItem('pckassemble_order', '<?= $assemble_order_id ?>');
    <?php endif ?>

    if (localStorage.getItem('remove_assls')) {
        if (localStorage.getItem('assitems')) {
            localStorage.removeItem('assitems');
        }
        if (localStorage.getItem('asscomproducts')) {
            localStorage.removeItem('asscomproducts');
        }
        if (localStorage.getItem('assdate')) {
            localStorage.removeItem('assdate');
        }
        if (localStorage.getItem('assest_date')) {
            localStorage.removeItem('assest_date');
        }
        if (localStorage.getItem('assproduction_order')) {
            localStorage.removeItem('assproduction_order');
        }
        if (localStorage.getItem('asscutting_order')) {
            localStorage.removeItem('asscutting_order');
        }
        if (localStorage.getItem('assreference_no')) {
            localStorage.removeItem('assreference_no');
        }
        if (localStorage.getItem('asssupplier')) {
            localStorage.removeItem('asssupplier');
        }
        localStorage.removeItem('remove_assls');
    }

    <?php if ($this->session->userdata('remove_assls')) { ?>
        if (localStorage.getItem('assitems')) {
            localStorage.removeItem('assitems');
        }
        if (localStorage.getItem('asscomproducts')) {
            localStorage.removeItem('asscomproducts');
        }
        if (localStorage.getItem('assdate')) {
            localStorage.removeItem('assdate');
        }
        if (localStorage.getItem('assest_date')) {
            localStorage.removeItem('assest_date');
        }
        if (localStorage.getItem('assproduction_order')) {
            localStorage.removeItem('assproduction_order');
        }
        if (localStorage.getItem('asscutting_order')) {
            localStorage.removeItem('asscutting_order');
        }
        if (localStorage.getItem('assreference_no')) {
            localStorage.removeItem('assreference_no');
        }
        if (localStorage.getItem('asssupplier')) {
            localStorage.removeItem('asssupplier');
        }
        localStorage.removeItem('remove_assls');
    <?php $this->sma->unset_data('remove_assls');}
    ?>

    $(document).ready(function () {
        if (localStorage.getItem('remove_pckls')) {

            if (localStorage.getItem('pckitems')) {
                localStorage.removeItem('pckitems');
            }
            if (localStorage.getItem('pckcomproducts')) {
                localStorage.removeItem('pckcomproducts');
            }
            if (localStorage.getItem('pckdate')) {
                localStorage.removeItem('pckdate');
            }
            if (localStorage.getItem('pckest_date')) {
                localStorage.removeItem('pckest_date');
            }
            if (localStorage.getItem('pckproduction_order')) {
                localStorage.removeItem('pckproduction_order');
            }
            if (localStorage.getItem('pckcutting_order')) {
                localStorage.removeItem('pckcutting_order');
            }
            if (localStorage.getItem('pckreference_no')) {
                localStorage.removeItem('pckreference_no');
            }
            if (localStorage.getItem('pcksupplier')) {
                localStorage.removeItem('pcksupplier');
            }
            localStorage.removeItem('remove_pckls');
        }

        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('pckdate')) {
            $("#pckdate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#pckdate', function (e) {
            localStorage.setItem('pckdate', $(this).val());
        });
        if (pckdate = localStorage.getItem('pckdate')) {
            $('#pckdate').val(pckdate);
        }
        $(document).on('change', '#pckestimated_date', function (e) {
            localStorage.setItem('pckestimated_date', $(this).val());
        });
        if (cuestimated_date = localStorage.getItem('pckestimated_date')) {
            $('#pckestimated_date').val(pckestimated_date);
        }
        <?php } ?>
    });

    $(document).on('change', '#pckbiller', function (e) {
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/40/") ?>'+$('#pckbiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;
          $('#adjustment_document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
          if (response.status == 0) {
            $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            localStorage.setItem('locked_for_biller_resolution', 1);
          }
          $('#adjustment_document_type_id').trigger('change');
        });
        // $('#pckwarehouse').select2('val', $('#pckbiller option:selected').data('warehousedefault')).trigger('change');
        localStorage.setItem('pckbiller', $('#pckbiller').val());
    });


</script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('add_packing_order'); ?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="introtext"><?php echo lang('enter_info'); ?></p>
                            <?php
                            $attrib = array('id' => 'add_packing');
                            echo admin_form_open_multipart("production_order/add_packing_order", $attrib);
                            ?>
                            <input type="hidden" name="warehouse" id="packing_warehouse">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("date", "pckdate"); ?>
                                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="pckdate" required="required"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("making_order", "production_order"); ?>
                                                <input type="hidden" name="production_order_id" value="" id="pckproduction_order" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                                <input type="hidden" name="production_order_reference_no" value="" id="pckproduction_order_reference_no">
                                            </div>
                                        </div>
                                        <?php if (!isset($assembles_ids)): ?>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <?= lang("cutting_order", "cutting_order"); ?>
                                                    <input type="hidden" name="cutting_order_id" value="" id="pckcutting_order" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                                    <input type="hidden" name="cutting_order_reference_no" value="" id="pckcutting_order_reference_no">
                                                </div>
                                            </div>
                                        <?php endif ?>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("assemble_order", "assemble_order"); ?>
                                                <?php if (isset($assembles_ids)): ?>
                                                    <input type="text" name="assembles_ids" class="form-control" value="<?= $assembles_ids ?>" readonly>
                                                <?php else: ?>
                                                    <input type="hidden" name="assemble_order_id" value="" id="pckassemble_order" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                                    <input type="hidden" name="assemble_order_reference_no" value="" id="pckassemble_order_reference_no">
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("packing_reference_no", "pckreference_no"); ?>
                                                <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ""), 'class="form-control" id="pckreference_no" required="required"'); ?>
                                            </div>
                                        </div> -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("packer", "pcksupplier"); ?>
                                                <input type="hidden" name="supplier_id" value="" id="pcksupplier" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("estimated_date", "pckest_date"); ?>
                                                <?php echo form_input('est_date', (isset($_POST['est_date']) ? $_POST['est_date'] : ""), 'class="form-control input-tip datetime" id="pckest_date" required="required"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">

                                        <?php
                                            $bl[""] = "";
                                            $bldata = [];
                                            foreach ($billers as $biller) {
                                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                                $bldata[$biller->id] = $biller;
                                            }
                                        ?>
                                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <?= lang("biller", "pckbiller"); ?>
                                                    <select name="biller" class="form-control" id="pckbiller" required="required">
                                                        <option value=""><?= lang('select') ?></option>
                                                        <?php foreach ($billers as $biller): ?>
                                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } else { ?>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <?= lang("biller", "pckbiller"); ?>
                                                    <select name="biller" class="form-control" id="pckbiller">
                                                        <option value=""><?= lang('select') ?></option>
                                                        <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                                            $biller = $bldata[$this->session->userdata('biller_id')];
                                                            ?>
                                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                        <?php endif ?>
                                                    </select>
                                                </div>
                                            </div>
                                        <?php } ?>
                                        <!-- <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                            <div class="col-md-4 col-sm-4">
                                                <div class="form-group">
                                                    <?= lang("warehouse", "pckwarehouse"); ?>
                                                    <?php
                                                    $wh[''] = lang('select');
                                                    foreach ($warehouses as $warehouse) {
                                                        $wh[$warehouse->id] = $warehouse->name;
                                                    }
                                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ($Settings->default_warehouse)), 'id="pckwarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;"');
                                                    ?>
                                                </div>
                                            </div>
                                        <?php } else {
                                            $warehouse_input = array(
                                                'type' => 'hidden',
                                                'name' => 'warehouse',
                                                'id' => 'pckwarehouse',
                                                'value' => $this->session->userdata('warehouse_id'),
                                                );

                                            echo form_input($warehouse_input);
                                        } ?> -->
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("adjustment_reference_no", "adjustment_document_type_id"); ?>
                                                <select name="adjustment_document_type_id" class="form-control" id="adjustment_document_type_id" required="required"></select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("pfinished_products_in_packing_order"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="pckTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th class="col-md-1"><?= lang("type"); ?></th>
                                                        <th class="col-md-2"><?= lang("variant"); ?></th>
                                                        <th class="col-md-1"><?= lang("pck_order_quantity"); ?></th>
                                                        <th class="col-md-1"><?= lang("assembled_quantity"); ?></th>
                                                        <th style="max-width: 30px !important; text-align: center;">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("components"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="pckCTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th><?= lang("type"); ?></th>
                                                        <th><?= lang("order_quantity"); ?></th>
                                                        <th><?= lang("finished_quantity"); ?></th>
                                                        <th><?= lang("in_process_quantity"); ?></th>
                                                        <th><?= lang("to_packing_quantity"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <?= lang("note", "pcknote"); ?>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="pcknote" style="margin-top: 10px; height: 100px;"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="fprom-group">
                                            <button type="button" class="btn btn-primary" id="add_packing_submit"><?= lang('submit') ?></button>
                                            <button type="button" class="btn btn-primary" id="add_packing_order_submit_continue"><?= lang('submit_finish_packing') ?></button>
                                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="scontinue" name="continue" value="0">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade in" id="packingWhModal" tabindex="-1" role="dialog" aria-labelledby="packingWhModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_packingWhModal"></h2>
                <h3><?= lang('select_packing_warehouse') ?></h3>
                <p>Es necesario que escoja una bodega para realizar el ajuste por el registro de entrega del empaque</p>
            </div>
            <div class="modal-body">
                <form id="form_wh">
                    <div class="form-group">
                        <?= lang("warehouse", "pckwarehouse"); ?>
                        <?= form_input('modal_warehouse', '', 'data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" class="form-control input-tip search_warehouse" id="pckwarehouse" required="required"'); ?>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-danger cancel_warehouse" type="button"><?= lang('cancel') ?></button>
                    <button class="btn btn-success set_warehouse" type="button"><?= lang('send') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $("#add_packing").validate({
            ignore: []
        });
    });

    $(document).on('click', '#add_packing_submit', function(){
        $('#scontinue').val(0);
        if ($('#add_packing').valid()) {
            $('#add_packing').submit();
        }
    });

    $(document).on('click', '#add_packing_order_submit_continue', function(){
        $('#packingWhModal').modal('show');
        set_warehouse_search();
    });

    $(document).on('click', '.set_warehouse', function(){
        $("#form_wh").validate({
            ignore: []
        });
        if ($('#form_wh').valid()) {
            $('.set_warehouse').prop('disabled', true).text('Enviando...');
            $('.cancel_warehouse').prop('disabled', true);
            $('#packing_warehouse').val($('#pckwarehouse').val());
            // $('#packingWhModal').modal('hide');

            setTimeout(function() {
                $('#scontinue').val(1);
                if ($('#add_packing').valid()) {
                    $('#add_packing').submit();
                } else {
                    $('#packingWhModal').modal('hide');
                    $('.set_warehouse').prop('disabled', false).text('<?= lang("send") ?>');
                    $('.cancel_warehouse').prop('disabled', false);
                    command: toastr.warning('Hace falta diligenciar algunos datos del formulario', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                }
            }, 1200);
        }
    });
</script>