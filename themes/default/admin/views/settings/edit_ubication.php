<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <style type="text/css">
            .input-edit{
                color: black;
                border-color: #eaeaea;
                border-width: 1px;
            }
        </style>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <?php if ($type == 1): ?>
              <h4 class="modal-title" id="myModalLabel"><?= lang('edit_country'); ?></h4>
            <?php endif ?>
            <?php if ($type == 2): ?>
              <h4 class="modal-title" id="myModalLabel"><?= lang('edit_state'); ?></h4>
            <?php endif ?>
            <?php if ($type == 3): ?>
              <h4 class="modal-title" id="myModalLabel"><?= lang('edit_city'); ?></h4>
            <?php endif ?>
            <?php if ($type == 4): ?>
              <h4 class="modal-title" id="myModalLabel"><?= lang('edit_zone'); ?></h4>
            <?php endif ?>
            <?php if ($type == 5): ?>
              <h4 class="modal-title" id="myModalLabel"><?= lang('edit_subzone'); ?></h4>
            <?php endif ?>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/edit_ubication/".$type."/".$id, $attrib); ?>
        <div class="modal-body">

            <?php if ($type >= 2): ?>
              <div class="form-group">
                <?= lang("country", "country"); ?>
                <select class="form-control select" name="country" id="country" required>
                  <option value=""><?= lang("select"); ?></option>
                  <?php foreach ($countries as $row => $each_country): ?>
                    <option value="<?= $each_country->NOMBRE ?>" data-code="<?= $each_country->CODIGO ?>" <?= $country && $country->NOMBRE == $each_country->NOMBRE ? 'selected="selected"' : '' ?>><?= $each_country->NOMBRE ?></option>
                  <?php endforeach ?>
                </select>
                <input type="hidden" name="country_code" id="country_code" <?= (isset($_POST['country_code'])) ? "value='".$_POST['country_code']."'" : "" ?>>
              </div>
            <?php endif ?>

            <?php if ($type >= 3): ?>
              <div class="form-group">
                <?= lang("state", "state"); ?>
                <select class="form-control select" name="state" id="state" required>
                  <option value=""><?= lang("select") ?></option>
                </select>
                <input type="hidden" name="state_code" id="state_code" <?= (isset($_POST['state_code'])) ? "value='".$_POST['state_code']."'" : "" ?>>
              </div>
            <?php endif ?>

            <?php if ($type >= 4): ?>
              <div class="form-group">
                <?= lang("city", "city"); ?>
                <select class="form-control select" name="city" id="city" required>
                  <option value=""><?= lang("select") ?></option>
                </select>
                <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
              </div>
            <?php endif ?>

            <?php if ($type >= 5): ?>
              <div class="form-group">
                <?= lang("zone", "zone"); ?>
                <select class="form-control select" name="zone" id="zone">
                  <option value=""><?= lang("select") ?></option>
                </select>
                <input type="hidden" name="zone_code" id="zone_code" <?= (isset($_POST['zone_code'])) ? "value='".$_POST['zone_code']."'" : "" ?>>
              </div>
            <?php endif ?>

            <div class="form-group">
                <?= lang('name', 'name') ?>
                <input type="text" name="name" id="name" class="form-control" value="<?= $name ?>" required>
            </div>

            <div class="form-group">
                <input type="hidden" name="add_ubication" value="1">
                <?= lang('postal_code', 'postal_code') ?>
                <input type="text" name="postal_code" id="postal_code" class="form-control" value="<?= $postal_code ?>" readonly required>
            </div>

            <?php if ($type == 1): ?>
              <?php if (count($currencies) > 1): ?>
                <div class="col-md-4 row">
                        <div class="col-md-6 form-group">
                            <?= lang('currency', 'currency') ?>
                            <select name="currency" class="form-control" id="currency">
                                <?php foreach ($currencies as $currency): ?>
                                    <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $this->Settings->default_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                                <?php endforeach ?>
                            </select>
                        </div>
                    <div class="col-md-6 form-group trm-control" style="display: none;">
                        <?= lang('trm', 'trm') ?>
                        <input type="number" name="trm" id="trm" class="form-control" required="true" step="0.01">
                    </div>
                </div>
              <?php else: ?>
                  <input type="hidden" name="currency" value="<?= $this->Settings->default_currency ?>" id="currency">
              <?php endif ?>

              <div class="form-group">
                  <?= lang('codigo_iso', 'codigo_iso') ?>
                  <input type="text" name="codigo_iso" id="codigo_iso" class="form-control" maxlength="2" value="<?= $codigo_iso ?>" required>
              </div>
            <?php endif ?>

        </div>
        <div class="modal-footer">
            <?= form_submit('edit_product_preference_category', lang('submit'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

<script type="text/javascript">

  $(document).ready(function(){
    set_ubication();
  });

$('#state').on('change', function(){
  dpto = $('#state option:selected').data('code');
  $('#state_code').val(dpto);
  $.ajax({
    url:"<?= admin_url() ?>customers/get_cities/"+dpto,
  }).done(function(data){
    $('#city').html(data);
  }).fail(function(data){
    console.log(data.responseText);
  });
});

$('#country').on('change', function(){
  dpto = $('#country option:selected').data('code');
  $('#country_code').val(dpto);
  $.ajax({
    url:"<?= admin_url() ?>customers/get_states/"+dpto,
  }).done(function(data){
    $('#state').html(data);
  }).fail(function(data){
    console.log(data.responseText);
  });
});

$('#city').on('change', function(){
  code = $('#city option:selected').data('code');
  $('.postal_code').val(code);
  $('#city_code').val(code);
  $.ajax({
    url:"<?= admin_url().'customers/get_zones/' ?>"+code
  }).done(function(data){
    $('#zone').html(data);
  });

});

$('#zone').on('change', function(){
  code = $('#zone option:selected').data('code');
  $('.postal_code').val(code);
  $('#zone_code').val(code);
});

function set_ubication() {
      <?php if ($type >= 2) { ?>
        country = $('#country option:selected').data('code');
        $('#country_code').val(country)
      <?php } ?>

      <?php if ($type >= 3) { ?>
        state = "<?= $state->DEPARTAMENTO ?>";
        $('#state_code').val('<?= $state->CODDEPARTAMENTO ?>')
      <?php } ?>

      <?php if ($type >= 4) { ?>
        city = "<?= $city->DESCRIPCION ?>";
        $('#city_code').val('<?= $city->CODIGO ?>');
      <?php } ?>

      <?php if ($type == 5) { ?>
        zone = "<?= $zone->zone_name ?>";
        $('#zone_code').val('<?= $zone->zone_code ?>');
      <?php } ?>

      <?php if ($type >= 3) { ?>
        $.ajax({
          url:"<?= admin_url() ?>customers/get_states/"+country+"/"+state
        }).done(function(data) {
          $('#state').html(data);
          $('#state').select2();
          state = $('#state option:selected').data('code');
          <?php if ($type >= 4) { ?>
            $.ajax({
              url:"<?= admin_url() ?>customers/get_cities/"+state+"/"+city
            }).done(function(data) {
              $('#city').html(data);
              $('#city').select2('val', city).trigger('change');
              <?php if ($type == 5) { ?>
                city = $('#city option:selected').data('code');
                $.ajax({
                  url:"<?= admin_url() ?>customers/get_zones/"+city+"/"+zone
                }).done(function(data){
                  $('#zone').html(data).select2().trigger('change');
                });
              <?php } ?>
            }).fail(function(data) {
              console.log(data.responseText);
            });
          <?php } ?>
        }).fail(function(data) {
          console.log(data.responseText);
        });
      <?php } ?>
    }

</script>

<?= $modal_js ?>