<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<style>
    .btn-default {
        color: #333333 !important;
    }
    .table_button {
        font-size: 1.8em !important;
    }
    .table_button {
        font-size: 1.5em;
        font-weight: 600;
        height: 70px;
        margin-bottom: 10px;
        padding: 6px 6px;
        width: 70px;
    }

    .bordered {
        border: 1px solid #747476;
    }

    /* Small devices (tablets, 768px and up) */
    @media (min-width: @screen-sm-min) {
        .table_button {
            height: 90px;
            width: 90px;
        }
    }

    /* Medium devices (desktops, 992px and up) */
    @media (min-width: @screen-md-min) {  }

    /* Large devices (large desktops, 1200px and up) */
    @media (min-width: @screen-lg-min) {  }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i> </button>
            <h2 class="modal-title" id="myModalLabel"><?= sprintf(lang('select_table'), lang('mesa')) ?></h2>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <?= admin_form_open_multipart("sales/update_status/", ['data-toggle' => 'validator', 'role' => 'form']); ?>
                    <ul class="nav nav-tabs" role="tablist">
                        <?php $i = 0; ?>
                        <?php foreach ($branch_areas as $area) { ?>
                            <?php if ($area->nombre == "Domicilio" || $area->nombre == "Llevar") { ?>
                            <?php } else { ?>
                            <?php $active = ($i == 0) ? 'class="active"' : ''; $i++; ?>
                                <li role="presentation" <?= $active; ?>><a href="#area_<?= $area->id ?>" aria-controls="area_<?= $area->id ?>" role="tab" data-toggle="tab"><?= $area->nombre; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                    </ul>

                    <div class="tab-content">
                        <?php $i = 0; ?>
                        <?php foreach ($branch_areas as $area) { ?>
                            <?php if ($area->nombre == "Domicilio" || $area->nombre == "Llevar") { ?>
                            <?php } else { ?>
                                <?php $active = ($i == 0) ? 'active' : ''; $i++; ?>
                                <div role="tabpanel" class="tab-pane <?= $active; ?>" id="area_<?= $area->id ?>">
                                    <br>
                                    <div class="row">
                                        <?php foreach ($area->tables as $table) { ?>
                                            <?php
                                                if ($table->estado == 1) {
                                                    $estado = "btn-default bordered";
                                                } elseif ($table->estado == 2) {
                                                    $estado = "btn-warning";
                                                } elseif ($table->estado == 3) {
                                                    $estado = "btn-wappsi-pagar";
                                                } else {
                                                    $estado = "btn-danger";
                                                }
                                            ?>
                                            <div class="col-xs-3 col-sm-3 col-md-2">
                                                <button type="button" class="btn <?= $estado; ?> btn-block table_button center-block" data-table_id="<?= $table->id?>" data-table_number="<?= $table->numero; ?>" data-table_status="<?= $table->estado; ?>" data-table_suspended_bill_id="<?= $table->suspended_bill_id; ?>">
                                                    <?= $table->tipo . $table->numero; ?>
                                                </button>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
            <?= form_open(admin_url('pos/create_suspended_sale'), 'id="formulario_crear_cuenta"'); ?>
                <input type="hidden" name="numero_mesa" id="numero_mesa">
                <input type="hidden" name="id_mesa" id="id_mesa">
                <input type="hidden" name="id_sucursal" id="id_sucursal">
            <?= form_close(); ?>
        </div>
    </div>
</div>
