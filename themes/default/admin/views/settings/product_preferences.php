<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?= admin_form_open('system_settings/expense_category_actions', 'id="action-form"') ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="CategoryTable" class="table table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th>
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th><?= $this->lang->line("category_name"); ?></th>
                                            <th><?= $this->lang->line("preference"); ?></th>
                                            <th><?= $this->lang->line("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="3" class="dataTables_empty">
                                                <?= lang('loading_data_from_server') ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action" />
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

<script>
    $(document).ready(function() {
        $('#CategoryTable').dataTable({
            aaSorting: [
                [1, "asc"]
            ],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            'bServerSide': true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/getProductPreferencesCategories') ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [{
                bSortable: false,
                mRender: checkbox,
                sWidth: '30px',
                className: 'text-center'
            }, null, null, {
                bSortable: false,
                sWidth: '65px',
                className: 'text-center'
            }],
            fnDrawCallback: function(oSettings) {
                $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/add_product_preference_category') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="<?= lang('add_product_preference_category') ?>"><i class="fas fa-plus fa-lg"></i></a>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });

        $('#delete').click(function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function(e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

    });
</script>