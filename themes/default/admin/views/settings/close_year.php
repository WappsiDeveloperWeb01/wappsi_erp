<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php 

    $paso_1 = 'disabled="disabled"';
    $paso_2 = 'disabled="disabled"';
    $paso_3 = 'disabled="disabled"';
    $paso_4 = 'disabled="disabled"';

    if ($this->Settings->close_year_step == 0) {
        $paso_1 = '';
    }
    if ($this->Settings->close_year_step >= 1) {
        $paso_2 = '';
    } 
    if ($this->Settings->close_year_step >= 2) {
        $paso_3 = '';
    }
    if ($this->Settings->close_year_step >= 3) {
        $paso_4 = '';
    }

 ?>

<style type="text/css">
    .label {
        padding: 8px 13px;
    }
</style>


<?= admin_form_open('system_settings/category_actions', 'id="action-form"') ?>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('close_year'); ?></h2>
    </div>
</div>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <!-- Izquierda -->
                        <div class="col-xs-6">
                            <!-- <p class="introtext"><?//= lang('list_results'); ?></p> -->
                            <div class="table-responsive">
                                <table id="CategoryTable" class="table table-bordered table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th>Paso de cierre anual</th>
                                            <th>Acción</th>
                                            <th>Estado</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <span><?= lang('year_creation') ?></span>
                                            </td>
                                            <td class="text-center">
                                                <button type="button" id="close_year" class="btn btn-primary btn_actions" <?= $paso_1 ?>><?= lang('submit') ?></button>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($this->Settings->close_year_step >= 1): ?>
                                                    <label class="label label-primary label-close_year"><?= lang('successfull') ?></label>
                                                <?php else: ?>
                                                    <label class="label label-warning label-close_year"><?= lang('pending') ?></label>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><?= lang('inventory_transfer') ?></span>
                                            </td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary btn_actions" id="inventory_transfer" <?= $paso_2 ?>><?= lang('submit') ?></button>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($this->Settings->close_year_step >= 2): ?>
                                                    <label class="label label-primary label-inventory_transfer"><?= lang('successfull') ?></label>
                                                <?php else: ?>
                                                    <label class="label label-warning label-inventory_transfer"><?= lang('pending') ?></label>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><?= lang('cxc_transfer') ?></span>
                                            </td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary btn_actions" id="cxc_transfer" <?= $paso_3 ?>><?= lang('submit') ?></button>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($this->Settings->close_year_step >= 3): ?>
                                                    <label class="label label-primary label-cxc_transfer"><?= lang('successfull') ?></label>
                                                <?php else: ?>
                                                    <label class="label label-warning label-cxc_transfer"><?= lang('pending') ?></label>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><?= lang('cxp_transfer') ?></span>
                                            </td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary btn_actions" id="cxp_transfer" <?= $paso_4 ?>><?= lang('submit') ?></button>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($this->Settings->close_year_step >= 4): ?>
                                                    <label class="label label-primary label-cxp_transfer"><?= lang('successfull') ?></label>
                                                <?php else: ?>
                                                    <label class="label label-warning label-cxp_transfer"><?= lang('pending') ?></label>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span><?= lang('deposit_transfer') ?></span>
                                            </td>
                                            <td class="text-center">
                                                <button type="button" class="btn btn-primary btn_actions" id="deposit_transfer" <?= $paso_4 ?>><?= lang('submit') ?></button>
                                            </td>
                                            <td class="text-center">
                                                <?php if ($this->Settings->close_year_step >= 5): ?>
                                                    <label class="label label-primary label-deposit_transfer"><?= lang('successfull') ?></label>
                                                <?php else: ?>
                                                    <label class="label label-warning label-deposit_transfer"><?= lang('pending') ?></label>
                                                <?php endif ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table id="products_table" class="table table-bordered table-hover reports-table" style="display:none;">
                                    <tr>
                                        <th colspan="2">Productos con diferencias de cantidades</th>
                                    </tr>
                                    <tr>
                                        <th>Código</th>
                                        <th>Nombre</th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- Derecha -->
                        <div class="col-xs-6">
                            <div class="panel panel-default">
                              <div class="panel-heading"><?= lang('observations') ?></div>
                              <div class="panel-body">
                                <?php if ($products_with_negative_quantitys['warehouse_products'] || $products_with_negative_quantitys['warehouses_products_variants']): ?>
                                    <div class="row">
                                        <div class="col-sm-12">
                                           <?= lang('max_products_for_adjustment') ?>
                                           <select class="form-control" id="max_qpr" style="width: 10%;">
                                               <option value="50">50</option>
                                               <option value="100">100</option>
                                               <option value="150">150</option>
                                           </select>
                                        </div>
                                    </div>
                                    <table class="table">
                                        <tr>
                                            <th><?= lang('warehouse') ?></th>
                                            <th><?= lang('note') ?></th>
                                            <th><?= lang('actions') ?></th>
                                        </tr>
                                        <?php foreach ($warehouses as $wh): ?>
                                            <tr>
                                                <th><?= $wh->name ?></th>
                                                    <?php if (isset($products_with_negative_quantitys['warehouse_products'][$wh->id])): ?>
                                                        <td>
                                                            <?= sprintf(
                                                                        lang('products_with_negative_quantitys'), 
                                                                        count($products_with_negative_quantitys['warehouse_products'][$wh->id])
                                                                        ) 
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <a id="btn_adjustment" class="btn btn-success btn-sm" data-whid="<?= $wh->id ?>"  data-adjtype="1">Ajustar cantidades a 0</a>
                                                        </td>
                                                    <?php else: ?>
                                                        <th>0</th>
                                                        <th>-</th>
                                                    <?php endif ?>
                                            </tr>
                                        <?php endforeach ?>
                                    </table>
                                <?php endif ?>

                                <?php if ($products_with_negative_quantitys['warehouses_products_variants']): ?>
                                    <h4><?= lang('product_variants') ?></h4>
                                    <table class="table">
                                        <tr>
                                            <th><?= lang('warehouse') ?></th>
                                            <th><?= lang('note') ?></th>
                                        </tr>
                                        <?php foreach ($warehouses as $wh): ?>
                                            <tr>
                                                <th><?= $wh->name ?></th>
                                                    <?php if (isset($products_with_negative_quantitys['warehouses_products_variants'][$wh->id])): ?>

                                                        <td>
                                                            <?= sprintf(
                                                                        lang('products_with_negative_quantitys'), 
                                                                        count($products_with_negative_quantitys['warehouses_products_variants'][$wh->id])
                                                                        ) 
                                                            ?>
                                                        </td>
                                                        <th>
                                                            <a id="btn_adjustment" class="btn btn-success btn-sm" data-whid="<?= $wh->id ?>"  data-adjtype="2">Ajustar cantidades a 0</a>
                                                        </th>
                                                    <?php else: ?>
                                                        <th>0</th>
                                                    <?php endif ?>
                                            </tr>
                                        <?php endforeach ?>
                                    </table>
                                <?php endif ?>

                                <?php if ($open_pos_registers): ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <h4><?= lang('close_year_open_pos_registers') ?></h4>
                                        </div>
                                        <div class="col-md-12">
                                            <table class="table">
                                                <tr>
                                                    <th><?= lang('open_time') ?></th>
                                                    <th><?= lang('total_cash') ?></th>
                                                    <th><?= lang('user') ?></th>
                                                </tr>
                                                <?php foreach ($open_pos_registers as $pos): ?>
                                                    <tr>
                                                        <td><?= $pos->date ?></td>
                                                        <td class="text-right"><?= $this->sma->formatMoney($pos->cash_in_hand) ?></td>
                                                        <td><?= $pos->user_id ?></td>
                                                    </tr>
                                                <?php endforeach ?>
                                            </table>
                                        </div>
                                    </div>
                                <?php endif ?>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>

<script type="text/javascript">
    $(document).on('click', '.btn_actions', function(){

        var button = $(this);
        var id = button.prop('id');

        $('#'+id).prop('disabled', true);

        bootbox.confirm({
            message: "<h3><?= lang('confirm_action') ?></h3>",
            buttons: {
                confirm: {
                    label: '<?= lang('submit') ?>',
                    className: 'btn-success'
                },
                cancel: {
                    label: '<?= lang('cancel') ?>',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    label = $(".label-"+id);
                    if (label.hasClass('label-warning')) {
                        label.removeClass('label-warning');
                    }
                    label.addClass('label-default');
                    label.html('<?= lang('loading') ?>');
                    $.ajax({
                        url : "<?= admin_url('system_settings/close_year/') ?>"+id,
                        dataType : "JSON"
                    }).done(function(data){
                        if (data.error) {
                            if (label.hasClass('label-default')) {
                                label.removeClass('label-default');
                            }
                            label.addClass('label-danger');
                            label.html('<?= lang('error') ?>');
                            command: toastr.error(data.msg, '<?= lang("error"); ?>', {
                                "showDuration": "2000",
                                "hideDuration": "1000",
                                "timeOut": "4000",
                                "extendedTimeOut": "1000",
                            });
                            if (data.products) {
                                html = '';
                                $.each(data.products, function(index, prd){
                                    html += '<tr>'+
                                                '<td>'+
                                                    prd.code+
                                                '</td>'+
                                                '<td>'+
                                                    prd.name+
                                                '</td>'+
                                           '</tr>';
                                });
                                    
                                $(html).appendTo('#products_table');
                                $('#products_table').fadeIn();
                            }
                        } else {
                            if (label.hasClass('label-default')) {
                                label.removeClass('label-default');
                            }
                            label.addClass('label-primary');
                            label.html('<?= lang('successfull') ?>');
                            setTimeout(function() {
                                location.reload();
                            }, 800);
                        }
                    }).fail(function(data){
                            if (label.hasClass('label-default')) {
                                label.removeClass('label-default');
                            }
                            label.addClass('label-danger');
                            label.html('<?= lang('error') ?>');
                    });
                } else {
                    $('#'+id).prop('disabled', false);
                }
            }
        });
    });

$(document).on('click', '#btn_adjustment', function(){
    adjtype = $(this).data('adjtype');
    whid = $(this).data('whid');
    max_qpr = $('#max_qpr').val();
    url = site.base_url + "products/add_adjustment?close_year_adjustment=1&adj_type="+adjtype+"&cy_warehouse_id="+whid+"&max_qpr="+max_qpr;
    window.open(url, '_target');
});

    

</script>