<?php
    require_once APPPATH.'../vendor/fpdf/fpdf.php';

    class PDF extends FPDF
    {
        function Header()
        {
            $this->SetTitle($this->sma->utf8Decode(lang("report_z_title")));
                $this->SetFont('Arial','B',$this->fuente+3);
            if ($this->biller) {
                $this->Image((isset($this->biller->logo) ? base_url().'assets/uploads/logos/'.$this->biller->logo : ''),10,14,60);
                $this->Cell(60,5,$this->sma->utf8Decode(''),0,0,'C');
                $this->Cell(136,5,$this->sma->utf8Decode($this->biller->company),0,1,'C');
                $this->Cell(60,5,$this->sma->utf8Decode(''),0,0,'C');
                $this->Cell(136,5, lang("nit") ." : ".$this->Settings->numero_documento,0,1,'C');
                $this->Cell(60,5,$this->sma->utf8Decode(''),0,0,'C');
                $this->Cell(136,5,$this->sma->utf8Decode(ucwords(mb_strtolower($this->biller->address))),0,1,'C');
                $this->Cell(60,5,$this->sma->utf8Decode(''),0,0,'C');
                $this->Cell(136,5,$this->sma->utf8Decode(ucwords(mb_strtolower($this->biller->city." - ".$this->biller->state))),0,1,'C');
                $this->Cell(60,5,$this->sma->utf8Decode(''),0,0,'C');
                $this->Cell(136,5,$this->sma->utf8Decode(lang("report_z_title")),0,1,'C');
            } else {
                $this->Cell(196,15,$this->sma->utf8Decode(lang("report_z_title")),0,1,'C');
            }
        }

        function Footer()
        {
            $this->SetFont('Arial','',6);
            $this->SetXY(7, -14);
            $this->Cell(196, 7 , $this->sma->utf8Decode(lang("report_z_footer") ."           ". lang("user") ." : ".$this->User),'',1,'C');
            $this->SetXY(195, -14);
            $this->Cell(7, 7 , $this->sma->utf8Decode(lang("page_number").$this->PageNo()),'',1,'C');
        }
    }

    $fuente = 7;
    $adicional_fuente = 2;
    $height = 5;

    $pdf = new PDF('P', 'mm', array(216, 279));
    $pdf->sma = $this->sma;
    $pdf->Logo = 'https://static.wixstatic.com/media/a560e4_0525ed9cae7e4d42bd1ac3c8a7851bca~mv2.png';
    $pdf->AliasNbPages();
    $pdf->biller = $biller;
    $pdf->Settings = $this->Settings;
    $pdf->User = 'Usuario login';
    $pdf->fuente = $fuente;
    $pdf->AddPage();
    $pdf->SetFont('Arial','',$fuente+4);
    $pdf->Ln(8);
    if(count($billings) > 0) $canti = count($billings);
    else $canti = 0;
    $pdf->SetFont('Arial','B',$fuente+1);
    $pdf->Cell(35,$height, lang("initial_date"),0,0,'L');
    $pdf->SetFont('Arial','',$fuente+1);
    $pdf->Cell(30,$height, $input['fech_ini'],0,0,'C');
    $pdf->SetFont('Arial','B',$fuente+1);
    $pdf->Cell(30,$height, lang("end_date"),0,0,'R');
    $pdf->SetFont('Arial','',$fuente+1);
    $pdf->Cell(30,$height, $input['fech_ini'],0,0,'C');
    $pdf->SetFont('Arial','B',$fuente+1);
    $pdf->Cell(30,$height, lang("number_of_records"),0,0,'R');
    $pdf->SetFont('Arial','',$fuente+1);
    $pdf->Cell(30,$height, $canti ,0,0,'C');
    $pdf->Ln();
    $pdf->SetFont('Arial','B',$fuente+1);
    $pdf->Cell(35,$height, lang("initial_number"),0,0,'L');
    $pdf->SetFont('Arial','',$fuente+1);
    if(count($billings) > 1) $pdf->Cell(30,$height, $billings[0]['reference_no'],0,0,'C');
    $pdf->SetFont('Arial','B',$fuente+1);
    $pdf->Cell(30,$height, lang("end_number"),0,0,'R');
    $pdf->SetFont('Arial','',$fuente+1);
    if(count($billings) > 1) $pdf->Cell(30,$height, $billings[count($billings)-1]['reference_no'],0,0,'C');
    $pdf->Ln();

    $Credito = 0;
    $Credito_debo = 0;
        $ancho_columnas = 196;
    foreach ($total_payments as $payment)
    {
        ${$payment['name']} = 0;
        ${$payment['name'].'_debo'} = 0;
    }

    $pdf->SetFont('Arial','B',$fuente+1);
    $pdf->Cell(0,$height+1, $this->sma->utf8Decode(lang("sales")),'TB',0,'C');
    $pdf->Ln();
    $pdf->SetFont('Arial','',$fuente);

    $arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/' );
    $total_subtotal = $total_iva  =  $total_total  = 0;
    $count = 0;
    $prefijo_old = $referencia_old = $prefijo = '';
    $array_prefijo = array();
    $cantidad = 0;

    if (($this->pos_settings->apply_suggested_tip > 0 && $this->pos_settings->apply_suggested_home_delivery_amount == 0) || ($this->pos_settings->apply_suggested_tip == 0 && $this->pos_settings->apply_suggested_home_delivery_amount > 0)) {
        $ancho_columnas = 18.75;
    } else if ($this->pos_settings->apply_suggested_tip > 0 && $this->pos_settings->apply_suggested_home_delivery_amount > 0) {
        $ancho_columnas = 15;
    } else if ($this->pos_settings->apply_suggested_tip == 0 && $this->pos_settings->apply_suggested_home_delivery_amount == 0) {
        $ancho_columnas = 25;
    }

    foreach ($billings as $billing)
    {
        $referencia = $billing['reference_no'];
        $prefijo = str_replace($arrayReplace,'',$referencia);
        if($prefijo_old!=$prefijo)
        {
            $count++;
            if($count > 1)
            {
                $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
                $array_prefijo[$prefijo_old]['count'] = $cantidad;
            }
            $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old );
            $prefijo_old = $prefijo;
            $cantidad = 0;
        }
        $referencia_old = $referencia;
        $cantidad++;
    }

    if(count($billings) > 1) {
        $array_prefijo[$prefijo]['Fin'] = $billings[count($billings)-1]['reference_no'];
        $array_prefijo[$prefijo]['count'] = $cantidad;
    } else {
        $array_prefijo[$prefijo]['count'] = 1;
    }

    $prefijo_old = $referencia_old = '';
    $total_subtotal = $total_iva  =  $total_total  = 0;
    $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
    $total_prefijo_propina = $total_prefijo_domicilio =  0;
    $total_propina = $total_domicilio =  0;

    foreach ($billings as $billing)
    {
        if($input['Tipo']=='Detallado')
        {
            $pdf->SetXY($pdf->GetX(),$pdf->GetY());
            if($pdf->GetY()>=255)
            {
                $pdf->AddPage();
                $pdf->SetFont('Arial','B',$fuente+1);
                $pdf->Cell(0,$height+1, $this->sma->utf8Decode(lang("sales")),'TB',0,'C');
                $pdf->Ln();
                $pdf->SetFont('Arial','B',$fuente);
                $pdf->Cell(16,$height-1, lang("consecutive"),'T',0,'C');
                $pdf->Cell(65,$height-1, lang("customer"),'T',0,'C');
                $pdf->Cell(20,$height-1, lang("date"),'T',0,'C');
                $pdf->Cell($ancho_columnas,$height-1, lang("subtotal"),'T',0,'C');
                $pdf->Cell($ancho_columnas,$height-1, lang("vat"),'T',0,'C');
                if ($this->pos_settings->apply_suggested_tip > 0) {
                    $pdf->Cell($ancho_columnas,$height-1, lang("tip"),'T',0,'C');
                }
                if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                    $pdf->Cell($ancho_columnas,$height-1, lang("home_delivery"),'T',0,'C');
                }
                $pdf->Cell($ancho_columnas,$height-1, lang("total"),'T',0,'C');
                $pdf->Cell(20,$height-1, lang("payment_method"),'T',0,'C');
                $pdf->Ln();
                $pdf->SetFont('Arial','',$fuente);
            }
        }

        $subtotal = $billing['total'];
        $iva = $billing['total_tax'];
        $propina = $billing['tip_amount'];
        $domicilio = $billing['shipping'];
        $total = $billing['grand_total'];
        $fecha = substr($billing['fecha'],0,10);
        $fecha_pay = substr($billing['fecha_pay'],0,10);
        if($billing['cantidad'] > 1 ) $Pago = $billing['name'].", ". lang("others");
        else $Pago = $billing['name'];

        $referencia = $billing['reference_no'];
        $cliente = $billing['customer'];
        $saldo = $billing['saldo'];

        if($saldo > 1)
        {
            if($billing['cantidad'] > 0 ) $Pago = lang("credit"). ", ". lang("others");
            else $Pago = 'Credito';
            $Credito += $saldo;
        }
        else
        {
            if($fecha!=$fecha_pay)
            {
                if($billing['cantidad'] > 0 ) $Pago = lang("credit"). ", ". lang("others");
                else $Pago = 'Credito';
                $Credito += $saldo;
            }
            else ${$billing['name']} += $saldo;
        }

        if($Pago=='Otros, Otros') $Pago = lang("others");

        $prefijo = str_replace($arrayReplace,'',$referencia);
        if($prefijo_old!=$prefijo)
        {
            if($prefijo_old!='')
            {
                $pdf->SetFont('Arial','B',$fuente);
                $pdf->SetFont('Arial','B',$fuente);
                $pdf->Cell(101,$height+1, $this->sma->utf8Decode(lang("total")) ,'T',0,'R');
                $pdf->SetFont('Arial','',$fuente);
                $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_subtotal),'T',0,'R');
                $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_iva),'T',0,'R');
                if ($this->pos_settings->apply_suggested_tip > 0) {
                    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_propina),'T',0,'R');
                }
                if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_domicilio),'T',0,'R');
                }
                $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_total),'T',0,'R');
                $pdf->Cell(20,$height+1, ' ','T',0,'C');
                $pdf->Ln();
                $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
                $total_prefijo_propina = $total_prefijo_domicilio =  0;
            }
            $array_prefijo[$prefijo];
            $Fin = $array_prefijo[$prefijo]['Fin'];
            $Ini = $array_prefijo[$prefijo]['Ini'];
            $count = $array_prefijo[$prefijo]['count'];

            $pdf->Cell(35,$height, lang("prefix").': '.$prefijo ,'TB',0,'L');
            $pdf->Cell(30,$height, lang("records").': '.$count ,'TB',0,'L');
            $pdf->Cell(30,$height, lang("initial_number"),'TB',0,'L');
            $pdf->Cell(30,$height, $Ini,'TB',0,'C');
            $pdf->Cell(30,$height, lang("end_number"),'TB',0,'R');
            $pdf->Cell(30,$height, $Fin,'TB',0,'C');
            $pdf->Cell(0,$height, '','TB',0,'C');
            $pdf->Ln();
            if($input['Tipo']=='Detallado')
            {
                $pdf->SetFont('Arial','B',$fuente);
                $pdf->Cell(16,$height-1, lang("consecutive"),'T',0,'C');
                $pdf->Cell(65,$height-1, lang("customer"),'T',0,'C');
                $pdf->Cell(20,$height-1, lang("date"),'T',0,'C');
                $pdf->Cell($ancho_columnas,$height-1, lang("subtotal"),'T',0,'C');
                $pdf->Cell($ancho_columnas,$height-1, lang("vat"),'T',0,'C');
                if ($this->pos_settings->apply_suggested_tip > 0) {
                    $pdf->Cell($ancho_columnas,$height-1, lang("tip"),'T',0,'C');
                }
                if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                    $pdf->Cell($ancho_columnas,$height-1, lang("home_delivery"),'T',0,'C');
                }
                $pdf->Cell($ancho_columnas,$height-1, lang("total"),'T',0,'C');
                $pdf->Cell(20,$height-1, lang("payment_method"),'T',0,'C');
                $pdf->Ln();
                $pdf->SetFont('Arial','',$fuente);
            }
            $prefijo_old = $prefijo;
        }

        if($input['Tipo']=='Detallado') { //acá listado ventas
            $pdf->Cell(20,$height-1, $referencia ,0,0,'L');
            $pdf->Cell(61,$height-1, $this->sma->utf8Decode(ucwords(mb_strtolower($cliente))) ,0,0,'L');
            $pdf->Cell(20,$height-1, $fecha,0,0,'C');
            $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($subtotal),0,0,'R');
            $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($iva),0,0,'R');
            if ($this->pos_settings->apply_suggested_tip > 0) {
                $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($propina),0,0,'R');
            }
            if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($domicilio),0,0,'R');
            }
            $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($total),0,0,'R');
            $pdf->Multicell(20,$height-1, $this->sma->utf8Decode($Pago) ,0,'C');
            // $pdf->Ln();
        }

        $total_subtotal += $subtotal;
        $total_iva  += $iva;
        $total_total  += $total;
        $total_prefijo_subtotal += $subtotal;
        $total_prefijo_iva  += $iva;
        $total_prefijo_total  += $total;
        $total_prefijo_propina  += $propina;
        $total_prefijo_domicilio += $domicilio;
        $total_propina  += $propina;
        $total_domicilio += $domicilio;

    }

    $pdf->SetFont('Arial','B',$fuente);
    $pdf->Cell(101,$height+1, $this->sma->utf8Decode(lang("total")) ,'T',0,'R');
    $pdf->SetFont('Arial','',$fuente);
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_subtotal),'T',0,'R');
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_iva),'T',0,'R');
    if ($this->pos_settings->apply_suggested_tip > 0) {
        $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_propina),'T',0,'R');
    }
    if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
        $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_domicilio),'T',0,'R');
    }
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_total),'T',0,'R');
    $pdf->Cell(20,$height+1, ' ','T',0,'C');
    $pdf->Ln();

    $pdf->SetFont('Arial','B',$fuente);
    $pdf->Cell(101,$height+1, $this->sma->utf8Decode(lang("sales_total")) ,'T',0,'R');
    $pdf->SetFont('Arial','',$fuente);
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_subtotal),'T',0,'R');
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_iva),'T',0,'R');
    if ($this->pos_settings->apply_suggested_tip > 0) {
        $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_propina),'T',0,'R');
    }
    if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
        $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_domicilio),'T',0,'R');
    }
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_total),'T',0,'R');
    $pdf->Cell(20,$height+1, ' ','T',0,'C');
    $pdf->Ln();
    $pdf->Ln(4);

    $Factura_subtotal = $total_subtotal;
    $Factura_iva  = $total_iva;
    $Factura_total  = $total_total;

    /********************************* anticipos ***********************************/
    if (isset($deposits) && count($deposits)>0) {
        $pdf->SetFont('Arial', 'B', $fuente+1);
        $pdf->Cell(0, $height+1, $this->sma->utf8Decode(lang('deposits')), 'TB', 1, 'C');
        $pdf->SetXY($pdf->GetX(),$pdf->GetY());
        $pdf->SetFont('Arial','',$fuente);
        $arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/' );
        $count = $cantidad_deposit = 0;
        $prefijo_old_deposit = $referencia_old_deposit = '';
        $total_deposits = $total_prefijo_total_deposits = 0;

        foreach ($deposits as $deposit) {
            $referencia_deposit = $deposit['reference_no'];
            $prefijo_deposit = str_replace($arrayReplace,'',$referencia_deposit);
            if($prefijo_old_deposit != $prefijo_deposit) {
                $count++;
                if($count > 1) {
                    $array_prefijo_deposits[$prefijo_old_deposit]['Fin'] = $referencia_old_deposit;
                    $array_prefijo_deposits[$prefijo_old_deposit]['count'] = $cantidad_deposit;
                }
                $array_prefijo_deposits[$prefijo_deposit] = array(  "prefijo"  => $prefijo_deposit,
                                                                    "Ini"       => $referencia_deposit,
                                                                    "Fin"       => $referencia_old_deposit,
                                                                    "count"     => $cantidad_deposit);
                $prefijo_old_deposit = $prefijo_deposit;
                $cantidad_deposit = 0;
            }
            $referencia_old_deposit = $referencia_deposit;
            $cantidad_deposit++;
            if ($deposit['code'] !== 'deposit') {
                if (!isset($paids_deposits[$deposit['name']])) {
                    $paids_deposits[$deposit['name']] = $deposit['amount'];
                }
                else{
                    $paids_deposits[$deposit['name']] += $deposit['amount'];
                }
            }
        }
        if(count($deposits) > 1)
        {
            $array_prefijo_deposits[$prefijo_deposit]['Fin'] = $deposits[count($deposits)-1]['reference_no'];
            $array_prefijo_deposits[$prefijo_deposit]['count'] = $cantidad;
        }

        $array_prefijo_deposits[$prefijo_deposit];
        $Fin_deposit = $array_prefijo_deposits[$prefijo_deposit]['Fin'];
        $Ini_deposit = $array_prefijo_deposits[$prefijo_deposit]['Ini'];
        $count_deposit = $array_prefijo_deposits[$prefijo_deposit]['count'];

        $pdf->Cell(35,$height, lang("prefix").': '.$prefijo_deposit ,'TB',0,'L');
        $pdf->Cell(30,$height, lang("records").': '.$count_deposit ,'TB',0,'L');
        $pdf->Cell(30,$height, lang("initial_number"),'TB',0,'L');
        $pdf->Cell(30,$height, $Ini_deposit,'TB',0,'C');
        $pdf->Cell(30,$height, lang("end_number"),'TB',0,'R');
        $pdf->Cell(30,$height, $Fin_deposit,'TB',0,'C');
        $pdf->Cell(0,$height, '','TB',1,'C');

        if($input['Tipo']=='Detallado')
        {
            $pdf->SetFont('Arial', 'B', $fuente);
            $pdf->Cell(16, $height+1, lang("consecutive"), 'TB', 0, 'L');
            $pdf->Cell(65, $height+1, lang("customer"), 'TB', 0, 'C');
            $pdf->Cell(20, $height+1, lang("date"), 'TB', 0, 'C');
            $pdf->Cell($ancho_columnas, $height+1, lang("origin"), 'TB', 0, 'R');
            $pdf->Cell($ancho_columnas, $height+1, '', 'TB', 0, 'R');
            $pdf->Cell($ancho_columnas, $height+1, lang("total"), 'TB', 0, 'R');
            $pdf->Cell(20, $height+1, lang("payment_method"), 'TB', 0, 'C');
            $pdf->Ln();
            $pdf->SetFont('Arial', '', $fuente);
        }

        foreach ($deposits as $deposit)
        {
            if($input['Tipo'] == 'Detallado')
            {
                $pdf->SetXY($pdf->GetX(), $pdf->GetY());
                if($pdf->GetY() >= 255)
                {
                    $pdf->SetFont('Arial', 'B', $fuente);
                    $pdf->Cell(16, $height+1, lang("consecutive"), 'TB', 0, 'L');
                    $pdf->Cell(65, $height+1, lang("customer"), 'TB', 0, 'C');
                    $pdf->Cell(20, $height+1, lang("date"), 'TB', 0, 'C');
                    $pdf->Cell($ancho_columnas, $height+1, lang("origin"), 'TB', 0, 'R');
                    $pdf->Cell($ancho_columnas, $height+1, '', 'TB', 0, 'R');
                    $pdf->Cell($ancho_columnas, $height+1, lang("total"), 'TB', 0, 'R');
                    $pdf->Cell(20, $height+1, 'Forma pago', 'TB', 0, 'C');
                    $pdf->Ln();
                    $pdf->SetFont('Arial', '', $fuente);
                }
            }
            $prefijo = str_replace($arrayReplace, '', $referencia);
            if($prefijo_old != $prefijo)
            {  // aqui entrara si existe otra referencia o prefijo diferente
                if($prefijo_old != '')
                {
                    $pdf->SetFont('Arial', 'B', $fuente);
                    $pdf->Cell(101, $height+1, $this->sma->utf8Decode(lang("total")), 'T', 0, 'R');
                    $pdf->SetFont('Arial', '', $fuente);
                    $pdf->Cell($ancho_columnas, $height+1, '', 'T', 0, 'R');
                    $pdf->Cell($ancho_columnas, $height+1, '', 'T', 0, 'R');
                    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_total_deposits), 'T', 0, 'R');
                    $pdf->Cell(20,$height+1, ' ','T',0,'C');
                    $pdf->Ln();
                    $total_prefijo_total_deposits = 0;
                }
                $array_prefijo_deposits[$prefijo_deposit];
                $Fin = $array_prefijo_deposits[$prefijo_deposit]['Fin'];
                $Ini = $array_prefijo_deposits[$prefijo_deposit]['Ini'];
                $count = $array_prefijo_deposits[$prefijo_deposit]['count'];

                $pdf->Cell(35,$height, lang("prefix").': '.$prefijo_deposit ,'TB',0,'L');
                $pdf->Cell(30,$height, lang("records").': '.$count ,'TB',0,'L');
                $pdf->Cell(30,$height, lang("initial_number"),'TB',0,'L');
                $pdf->Cell(30,$height, $Ini,'TB',0,'C');
                $pdf->Cell(30,$height, lang("end_number"),'TB',0,'R');
                $pdf->Cell(30,$height, $Fin,'TB',0,'C');
                $pdf->Cell(0,$height, '','TB',0,'C');
                $pdf->Ln();

                if($input['Tipo']=='Detallado') {
                    $pdf->SetFont('Arial', 'B', $fuente);
                    $pdf->Cell(16, $height+1, lang("consecutive"), 'TB', 0, 'L');
                    $pdf->Cell(65, $height+1, lang("customer"), 'TB', 0, 'C');
                    $pdf->Cell(20, $height+1, lang("date"), 'TB', 0, 'C');
                    $pdf->Cell($ancho_columnas, $height+1, 'Origen', 'TB', 0, 'R');
                    $pdf->Cell($ancho_columnas, $height+1, '', 'TB', 0, 'R');
                    $pdf->Cell($ancho_columnas, $height+1, 'Total', 'TB', 0, 'R');
                    $pdf->Cell(20, $height+1, lang("payment_method"), 'TB', 0, 'C');
                    $pdf->Ln();
                    $pdf->SetFont('Arial','',$fuente);
                }
                $prefijo_old = $prefijo;
            }

            if($input['Tipo']=='Detallado')
            {
                $pdf->Cell(16,$height-1, $deposit['reference_no'], 0, 0, 'L');
                $pdf->Cell(65,$height-1, $this->sma->utf8Decode($deposit['customer']), 0, 0, 'L');
                $pdf->Cell(20,$height-1, substr($deposit['fecha'], 0, 10), 0, 0, 'C');
                $pdf->Cell($ancho_columnas,$height-1, ($deposit['origen_reference_no']) ? $this->sma->utf8Decode(lang("return")) : lang("manual"), 0, 0, 'R');
                $pdf->Cell($ancho_columnas,$height-1, '', 0, 0, 'R');
                $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($deposit['amount']),0,0,'R');
                $pdf->Multicell(20,$height-1, $this->sma->utf8Decode($deposit['name']), 0, 'C');
            }
            $total_deposits += $deposit['amount'];
            $total_prefijo_total_deposits += $deposit['amount'];
        }

        if(count($deposits) > 1) {
            $pdf->SetFont('Arial', 'B', $fuente);
            $pdf->Cell(101,$height+1, $this->sma->utf8Decode(lang("total")), 'T', 0, 'R');
            $pdf->SetFont('Arial', '', $fuente);
            $pdf->Cell($ancho_columnas, $height+1, '', 'T', 0, 'R');
            $pdf->Cell($ancho_columnas, $height+1, '', 'T', 0, 'R');
            $pdf->Cell($ancho_columnas, $height+1, $this->sma->formatMoney($total_prefijo_total_deposits), 'T', 0, 'R');
            $pdf->Cell(20, $height+1, ' ', 'T', 0, 'C');
            $pdf->Ln();
        }

        $pdf->SetFont('Arial', 'B', $fuente);
        $pdf->Cell(101,$height+1, lang('total_advances'), 'TB', 0, 'R');
        $pdf->SetFont('Arial', '', $fuente);
        $pdf->Cell($ancho_columnas,$height+1, '', 'TB', 0, 'R');
        $pdf->Cell($ancho_columnas,$height+1, '', 'TB', 0, 'R');
        $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_deposits),'TB',0,'R');
        $pdf->Cell(20,$height+1, ' ','TB',0,'C');
        $pdf->Ln();
        $pdf->Ln();

    }

$pdf->SetFont('Arial','B',$fuente+1);

$pdf->Cell(0,$height+1, $this->sma->utf8Decode(lang("returns")),'TB',0,'C');
$pdf->Ln();

$pdf->SetXY($pdf->GetX(),$pdf->GetY());

$pdf->SetFont('Arial','',$fuente);
$i = 0;

$total_subtotal = $total_iva  =  $total_total  = 0;
$page = 0;

$arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/' );
$total_subtotal = $total_iva  =  $total_total  = 0;
$count = 0;
$prefijo_old = $referencia_old = '';
$array_prefijo = array();
$cantidad = 0;
foreach ($returns as $return)
{
    $referencia = $return['reference_no'];
    $prefijo = str_replace($arrayReplace,'',$referencia);
    if($prefijo_old!=$prefijo)
    {
        $count++;
        if($count > 1)
        {
            $array_prefijo[$prefijo_old]['Fin'] = $referencia_old;
            $array_prefijo[$prefijo_old]['count'] = $cantidad;
        }
        $array_prefijo[$prefijo] = array("prefijo" => $prefijo, "Ini" => $referencia , "Fin" => $referencia_old , "count" => $cantidad );
        $prefijo_old = $prefijo;
        $cantidad = 0;
    }
    $referencia_old = $referencia;
    $cantidad++;
}
if(count($returns) > 1)
{
    $array_prefijo[$prefijo]['Fin'] = $returns[count($returns)-1]['reference_no'];
    $array_prefijo[$prefijo]['count'] = $cantidad;
}
$prefijo_old = $referencia_old = '';
$total_subtotal = $total_iva  =  $total_total  = 0;
$total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
$total_prefijo_propina = $total_prefijo_domicilio =  0;
$total_propina = $total_domicilio =  0;

foreach ($returns as $return)
{
    if($input['Tipo']=='Detallado')
    {
        $pdf->SetXY($pdf->GetX(),$pdf->GetY());
        if($pdf->GetY()>=255)
        {
            $pdf->AddPage();
            $pdf->SetFont('Arial','B',$fuente+1);
            $pdf->Cell(0,$height+1, $this->sma->utf8Decode(lang("returns")),'TB',0,'C');
            $pdf->Ln();
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(16,$height+1, lang("consecutive"),'TB',0,'L');
            $pdf->Cell(65,$height+1, lang("customer"),'TB',0,'L');
            $pdf->Cell(20,$height+1, lang("date"),'TB',0,'C');
            $pdf->Cell($ancho_columnas,$height+1, lang("subtotal"),'TB',0,'R');
            $pdf->Cell($ancho_columnas,$height+1, lang("vat"),'TB',0,'R');
            if ($this->pos_settings->apply_suggested_tip > 0) {
                $pdf->Cell($ancho_columnas,$height+1, lang("tip"),'TB',0,'C');
            }
            if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                $pdf->Cell($ancho_columnas,$height+1, lang("home_delivery"),'TB',0,'C');
            }
            $pdf->Cell($ancho_columnas,$height+1, lang("total"),'TB',0,'R');
            $pdf->Cell(20,$height+1, lang("payment_method"),'TB',0,'C');
            $pdf->Ln();
            $pdf->SetFont('Arial','',$fuente);
        }
    }

    $subtotal = $return['total'];
    $iva = $return['total_tax'];
    $propina = ($billing['tip_amount']*-1);
    $domicilio = ($billing['shipping']*-1);
    $total = $return['grand_total'];
    $fecha = substr($return['fecha'],0,10);
    $fecha_pay = substr($return['fecha_pay'],0,10);
    if($return['cantidad'] > 1 ) $Pago = $return['name'].', '. lang("other");
    else $Pago = $return['name'];

    $referencia = $return['reference_no'];
    $cliente = $return['customer'];
    $saldo = $return['saldo'];

    if($saldo > 1)
    {
        if($return['cantidad'] > 0 ) $Pago = lang("credit").', '.lang("others");
        else $Pago = lang("credit");
        $Credito_debo += $total;
    }
    else
    {
        if($fecha!=$fecha_pay)
        {
            if($return['cantidad'] > 0 ) $Pago = lang("credit").', '.lang("others");
            else $Pago = lang("credit");
            $Credito_debo += $total;
        } else if (isset(${$return['name'].'_debo'})){
            ${$return['name'].'_debo'} += $total;
        }
    }

    if($Pago=='Otros, Otros') $Pago = lang("others");

    $prefijo = str_replace($arrayReplace,'',$referencia);

    if($prefijo_old!=$prefijo) {
        if($prefijo_old!='') {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(101,$height+1, $this->sma->utf8Decode(lang("total")) ,'T',0,'R');
            $pdf->SetFont('Arial','',$fuente);
            $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_subtotal),'T',0,'R');
            $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_iva),'T',0,'R');
            if ($this->pos_settings->apply_suggested_tip > 0) {
                $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_propina),'T',0,'R');
            }
            if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_domicilio),'T',0,'R');
            }
            $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_total),'T',0,'R');
            $pdf->Cell(20,$height+1, ' ','T',0,'C');
            $pdf->Ln();
            $total_prefijo_subtotal = $total_prefijo_iva  =  $total_prefijo_total  = 0;
            $total_prefijo_propina = $total_prefijo_domicilio =  0;
        }
        $array_prefijo[$prefijo];
        $Fin = $array_prefijo[$prefijo]['Fin'];
        $Ini = $array_prefijo[$prefijo]['Ini'];
        $count = $array_prefijo[$prefijo]['count'];

        $pdf->Cell(35,$height, lang("prefijo").': '.$prefijo ,'TB',0,'L');
        $pdf->Cell(30,$height, lang("records").': '.$count ,'TB',0,'L');
        $pdf->Cell(30,$height, lang("initial_number"),'TB',0,'L');
        $pdf->Cell(30,$height, $Ini,'TB',0,'C');
        $pdf->Cell(30,$height, lang("final_number"),'TB',0,'R');
        $pdf->Cell(30,$height, $Fin,'TB',0,'C');
        $pdf->Cell(0,$height, '','TB',0,'C');
        $pdf->Ln();
        if($input['Tipo']=='Detallado')
        {
            $pdf->SetFont('Arial','B',$fuente);
            $pdf->Cell(16,$height+1, lang("consecutive"),'TB',0,'L');
            $pdf->Cell(65,$height+1, lang("customer"),'TB',0,'C');
            $pdf->Cell(20,$height+1, lang("date"),'TB',0,'C');
            $pdf->Cell($ancho_columnas,$height+1, lang("subtotal"),'TB',0,'R');
            $pdf->Cell($ancho_columnas,$height+1, lang("vat"),'TB',0,'R');
            if ($this->pos_settings->apply_suggested_tip > 0) {
                $pdf->Cell($ancho_columnas,$height+1, lang("tip"),'TB',0,'C');
            }
            if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
                $pdf->Cell($ancho_columnas,$height+1, lang("home_delivery"),'TB',0,'C');
            }
            $pdf->Cell($ancho_columnas,$height+1, lang("total"),'TB',0,'R');
            $pdf->Cell(20,$height+1, lang("payment_method"),'TB',0,'C');
            $pdf->Ln();
            $pdf->SetFont('Arial','',$fuente);
        }
        $prefijo_old = $prefijo;
    }

    if($input['Tipo']=='Detallado')
    {
        $pdf->Cell(16,$height-1, $referencia ,0,0,'L');
        $pdf->Cell(65,$height-1, $this->sma->utf8Decode($cliente) ,0,0,'L');
        $pdf->Cell(20,$height-1, $fecha,0,0,'C');
        $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($subtotal),0,0,'R');
        $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($iva),0,0,'R');
        if ($this->pos_settings->apply_suggested_tip > 0) {
            $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($propina),0,0,'R');
        }
        if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
            $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($domicilio),0,0,'R');
        }
        $pdf->Cell($ancho_columnas,$height-1, $this->sma->formatMoney($total),0,0,'R');
        $pdf->Cell(20,$height-1, $Pago,0,0,'C');
        $pdf->Ln();
    }

    $total_subtotal += $subtotal;
    $total_iva  += $iva;
    $total_total  += $total;
    $total_prefijo_subtotal += $subtotal;
    $total_prefijo_iva  += $iva;
    $total_prefijo_total  += $total;
    $total_prefijo_propina  += $propina;
    $total_prefijo_domicilio += $domicilio;
    $total_propina  += $propina;
    $total_domicilio += $domicilio;

}

if(count($returns) > 1)
{
    $pdf->SetFont('Arial','B',$fuente);
    $pdf->Cell(101,$height+1, $this->sma->utf8Decode(lang("total")) ,'T',0,'R');
    $pdf->SetFont('Arial','',$fuente);
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_subtotal),'T',0,'R');
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_iva),'T',0,'R');
    if ($this->pos_settings->apply_suggested_tip > 0) {
        $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_propina),'T',0,'R');
    }
    if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
        $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_domicilio),'T',0,'R');
    }
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_prefijo_total),'T',0,'R');
    $pdf->Cell(20,$height+1, ' ','T',0,'C');
    $pdf->Ln();
}

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(101,$height+1, lang("total_returns"),'TB',0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_subtotal),'TB',0,'R');
$pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_iva),'TB',0,'R');
if ($this->pos_settings->apply_suggested_tip > 0) {
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_propina),'TB',0,'R');
}
if ($this->pos_settings->apply_suggested_home_delivery_amount > 0) {
    $pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_domicilio),'TB',0,'R');
}
$pdf->Cell($ancho_columnas,$height+1, $this->sma->formatMoney($total_total),'TB',0,'R');
$pdf->Cell(20,$height+1, ' ','TB',0,'C');
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("summary_general")),'TB',0,'C');
$pdf->Ln();
$pdf->Cell(65,$height+1, '' ,0,0,'R');
$pdf->Cell(30,$height+1, lang("subtotal"),0,0,'R');
$pdf->Cell(15,$height+1, '',0,0,'R');
$pdf->Cell(30,$height+1, lang("taxes"),0,0,'R');
$pdf->Cell(15,$height+1, '',0,0,'R');
$pdf->Cell(30,$height+1, lang("total"),0,0,'R');
$pdf->Cell(0,$height+1, ' ',0,0,'C');

$pdf->Ln();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("sales_total")) ,0,0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($Factura_subtotal),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($Factura_iva),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($Factura_total),0,0,'R');
$pdf->Cell(0,$height, ' ',0,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, lang("total_returns"),0,0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($total_subtotal),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($total_iva),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($total_total),0,0,'R');
$pdf->Cell(0,$height, ' ',0,0,'C');
$pdf->Ln();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("total_sales_returns")) ,'T',0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($Factura_subtotal-($total_subtotal*-1)),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($Factura_iva-($total_iva*-1)),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($Factura_total-($total_total*-1)),'T',0,'R');
$pdf->Cell(0,$height, ' ','T',0,'C');
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("summary_payment_methods")),'TB',0,'C');
$pdf->Ln();
$total_pago = $total_pago_debo = 0;
$pdf->Cell(65,$height, lang("payment_method"),0,0,'L');
$pdf->Cell(30,$height, $this->sma->utf8Decode(lang("sales")),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, lang("returns"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->utf8Decode(lang("sales_returns")),0,0,'R');
$pdf->SetFont('Arial','',$fuente);
$pdf->Ln();

foreach ($total_payments as $payment)
{
    if ($payment['name'] != 'Retenciones aplicadas') {
        $subtotal = 0;
        if (isset($paids_deposits[$payment['name']])) {
            $tpaid = $payment['total_payment'] + $paids_deposits[$payment['name']];
        }
        else{
            $tpaid = $payment['total_payment'];
        }
        $pdf->Cell(65,$height, $this->sma->utf8Decode($payment['name']),0,0,'L');
        $pdf->Cell(30,$height, $this->sma->formatMoney($tpaid),0,0,'R');
        $pdf->Cell(15,$height, '',0,0,'R');
        $pdf->Cell(30,$height, $this->sma->formatMoney($payment['total_return_payment']),0,0,'R');
        $pdf->Cell(15,$height, '',0,0,'R');
        $pdf->Cell(30,$height, $this->sma->formatMoney($tpaid + $payment['total_return_payment']),0,0,'R');
        $pdf->Ln();
        $total_pago += $tpaid;
        $total_pago_debo += $payment['total_return_payment'];
    }
}
$subtotal = 0;
$pdf->Cell(65,$height, lang("credits"),0,0,'L');
$pdf->Cell(30,$height, $this->sma->formatMoney($Credito),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($Credito_debo),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($Credito-($Credito_debo*-1)),0,0,'R');
$pdf->Ln();
$total_pago +=  $Credito;
$total_pago_debo +=  $Credito_debo;


foreach ($total_payments as $payment) {
    if ($payment['name'] == 'Retenciones aplicadas *') {
        $subtotal = 0;
        $pdf->Cell(65,$height, $payment['name'],0,0,'L');
        $pdf->Cell(30,$height, $this->sma->formatMoney($payment['total_payment']),0,0,'R');
        $pdf->Cell(15,$height, '',0,0,'R');
        $pdf->Cell(30,$height, $this->sma->formatMoney($payment['total_return_payment']),0,0,'R');
        $pdf->Cell(15,$height, '',0,0,'R');
        $pdf->Cell(30,$height, $this->sma->formatMoney($payment['total_payment']+$payment['total_return_payment']),0,0,'R');
        $pdf->Ln();
        $total_pago += $payment['total_payment'];
        $total_pago_debo += $payment['total_return_payment'];
    }
}

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, lang("total_payment_methods"),'T',0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($total_pago),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($total_pago_debo),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($total_pago-($total_pago_debo*-1)),'T',0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();
$pdf->Ln();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("summary_tax")),'TB',0,'C');
$pdf->Ln();
$pdf->Cell(65,$height, lang("rate"),0,0,'L');
$pdf->Cell(21,$height, lang("subtotal"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(21,$height, lang("vat"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(22.5,$height, lang("general_discount"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(22.5,$height, lang("total"),0,0,'R');
$pdf->Cell(0,$height, '',0,0,'R');
$pdf->Ln();
$pdf->SetFont('Arial','',$fuente);
$unisubtotal = $item_tax = $subtotal = $order_discount_total = 0;

foreach ($total_tax_rates as $tax_rate) {
    $pdf->Cell(65,$height, $tax_rate['name'],0,0,'L');
    $pdf->Cell(21,$height, $this->sma->formatMoney($tax_rate['unisubtotal']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(21,$height, $this->sma->formatMoney($tax_rate['item_tax']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(22.5,$height, (isset($tax_rate['order_discount'])) ? $this->sma->formatMoney($tax_rate['order_discount']) : "0",0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(22.5,$height, $this->sma->formatMoney($tax_rate['subtotal'] - ((isset($tax_rate['order_discount'])) ? $tax_rate['order_discount'] : 0)),0,0,'R');
    $pdf->Ln();
    $unisubtotal += $tax_rate['unisubtotal'];
    $item_tax += $tax_rate['item_tax'];
    $subtotal += $tax_rate['subtotal'];
    $order_discount_total += (isset($tax_rate['order_discount'])) ? $tax_rate['order_discount'] : 0;
}
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, lang("total_summary_taxes"),'T',0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(21,$height, $this->sma->formatMoney($unisubtotal),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(21,$height, $this->sma->formatMoney($item_tax),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(22.5,$height, $this->sma->formatMoney($order_discount_total),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(22.5,$height, $this->sma->formatMoney($subtotal - $order_discount_total),'T',0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();
$pdf->Ln();

$unisubtotal = $item_tax = $subtotal = 0;
$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("summary_categories")),'TB',0,'C');
$pdf->Ln();
$pdf->Cell(45,$height, $this->sma->utf8Decode(lang("category")),0,0,'L');
$pdf->Cell(25,$height, $this->sma->utf8Decode(lang("product_qty")),0,0,'L');
$pdf->Cell(30,$height, lang("subtotal"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, lang("vat"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, lang("total"),0,0,'R');
$pdf->Cell(0,$height, '',0,0,'R');
$pdf->Ln();
$pdf->SetFont('Arial','',$fuente);
$unisubtotal = $item_tax = $subtotal = 0;
// $this->sma->print_arrays($categories_tax);
foreach ($categories_tax as $categori_tax)
{
    // $pdf->Cell(65,$height, $categori_tax['Nombre'],0,0,'L');
    $pdf->Cell(40,$height, $categori_tax['codigo']." ".$categori_tax['Nombre'],0,0,'L');
    $pdf->Cell(25,$height, $this->sma->formatQuantity($categori_tax['total_items']),0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($categori_tax['unisubtotal']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($categori_tax['item_tax']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($categori_tax['subtotal']),0,0,'R');
    $pdf->Ln();
    $unisubtotal += $categori_tax['unisubtotal'];
    $item_tax += $categori_tax['item_tax'];
    $subtotal += $categori_tax['subtotal'];
}
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("total_summary_categories")),'T',0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($unisubtotal),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($item_tax),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($subtotal ),'T',0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();
$pdf->Ln();

$unisubtotal = $item_tax = $subtotal = 0;
$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("vat")),'TB',0,'C');
$pdf->Ln();
$pdf->Cell(40,$height, $this->sma->utf8Decode(lang("name")),0,0,'L');
$pdf->Cell(20,$height, $this->sma->utf8Decode(lang("serial")),0,0,'L');
$pdf->Cell(15,$height, $this->sma->utf8Decode(lang("sale")),0,0,'L');
$pdf->Cell(25,$height, lang("subtotal"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(25,$height, lang("vat"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, lang("total"),0,0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();
$pdf->SetFont('Arial','',$fuente);
$unisubtotal = $item_tax = $subtotal = 0;
foreach ($total_users_taxs as $total_users_tax)
{
    $pdf->Cell(40,$height, $this->sma->utf8Decode($total_users_tax['first_name'].' '.$total_users_tax['last_name']),0,0,'L');
    $pdf->Cell(20,$height, $this->sma->utf8Decode($total_users_tax['user_pc_serial']),0,0,'L');
    $pdf->Cell(15,$height, $total_users_tax['Cantidad'],0,0,'R');
    $pdf->Cell(25,$height, $this->sma->formatMoney($total_users_tax['total']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(25,$height, $this->sma->formatMoney($total_users_tax['total_tax']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($total_users_tax['grand_total']),0,0,'R');
    $pdf->Ln();
    $unisubtotal += $total_users_tax['total'];
    $item_tax += $total_users_tax['total_tax'];
    $subtotal += $total_users_tax['grand_total'];
}
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("total_summary_users")),'T',0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($unisubtotal),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($item_tax),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($subtotal ),'T',0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();
$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("total_sales_by_seller")),'TB',0,'C');
$pdf->Ln();
$pdf->Cell(40,$height, $this->sma->utf8Decode(lang("name")),0,0,'L');
$pdf->Cell(25,$height, $this->sma->utf8Decode(lang("sales_amount")),0,0,'L');
$pdf->Cell(30,$height, lang("subtotal"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, lang("vat"),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, lang("total"),0,0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();
$pdf->SetFont('Arial','',$fuente);
$unisubtotal = $item_tax = $subtotal = 0;
foreach ($total_sellers_taxs as $total_sellers_tax)
{
    $pdf->Cell(40,$height, $this->sma->utf8Decode($total_sellers_tax['first_name']. ' ' .$total_sellers_tax['first_lastname'] ),0,0,'L');
    $pdf->Cell(25,$height, $total_sellers_tax['Cantidad'],0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($total_sellers_tax['total']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($total_sellers_tax['total_tax']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($total_sellers_tax['grand_total']),0,0,'R');
    $pdf->Ln();
    $unisubtotal += $total_sellers_tax['total'];
    $item_tax += $total_sellers_tax['total_tax'];
    $subtotal += $total_sellers_tax['grand_total'];
}
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("total_summary_sellers")),'T',0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($unisubtotal),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($item_tax),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($subtotal ),'T',0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("total_sales_by_customer")),'TB',0,'C');
$pdf->Ln();
$unisubtotal = $item_tax = $subtotal = 0;
$pdf->SetFont('Arial','',$fuente);
foreach ($total_customers_taxs as $total_customers_tax)
{
    $pdf->Cell(40,$height, $this->sma->utf8Decode($total_customers_tax['name']),0,0,'L');
    $pdf->Cell(25,$height, $total_customers_tax['Cantidad'],0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($total_customers_tax['total']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($total_customers_tax['total_tax']),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($total_customers_tax['grand_total']),0,0,'R');
    $pdf->Ln();
    $unisubtotal += $total_customers_tax['total'];
    $item_tax += $total_customers_tax['total_tax'];
    $subtotal += $total_customers_tax['grand_total'];
}
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("total_summary_customers")),'T',0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, $this->sma->formatMoney($unisubtotal),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($item_tax),'T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($subtotal ),'T',0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(0,$height+2, $this->sma->utf8Decode(lang("cash_transactions_summary")),'TB',0,'C');
$pdf->Ln();
$pdf->Cell(40,$height, $this->sma->utf8Decode(lang("date")),0,0,'L');
$pdf->Cell(25,$height, $this->sma->utf8Decode(lang("reference")),0,0,'L');
$pdf->Cell(30,$height, $this->sma->utf8Decode(lang("type_movement")),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, $this->sma->utf8Decode(lang("paymenth_method")),0,0,'R');
$pdf->Cell(15,$height, '',0,0,'R');
$pdf->Cell(30,$height, lang("total"),0,0,'R');
$pdf->Cell(0,$height, '','T',0,'R');
$pdf->Ln();
$pdf->SetFont('Arial','',$fuente+1);

foreach ($register_movements as $register_movement)
{
    $pdf->Cell(40,$height, $this->sma->utf8Decode($register_movement['date']),0,0,'L');
    $pdf->Cell(25,$height, $this->sma->utf8Decode($register_movement['reference_no']),0,0,'R');
    $pdf->Cell(30,$height, $this->sma->utf8Decode($register_movement['movement_type']  == 1 ? lang('movement_type_in') : lang('movement_type_out')),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->utf8Decode(lang($register_movement['movement_type']  == 1 ? $register_movement['destination_paid_by'] : $register_movement['origin_paid_by'])),0,0,'R');
    $pdf->Cell(15,$height, '',0,0,'R');
    $pdf->Cell(30,$height, $this->sma->formatMoney($register_movement['movement_type']  == 1 ? $register_movement['amount'] : $register_movement['amount'] * -1),0,0,'R');
    $pdf->Ln();
    $unisubtotal += ($register_movement['movement_type']  == 1 ? $register_movement['amount'] : $register_movement['amount'] * -1);
}


$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("total_summary_movement")),'T',0,'L');
$pdf->SetFont('Arial','',$fuente);
$pdf->Cell(30,$height, '','T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, '','T',0,'R');
$pdf->Cell(15,$height, '','T',0,'R');
$pdf->Cell(30,$height, $this->sma->formatMoney($unisubtotal ),'T',0,'R');
$pdf->Cell(0,$height, '','T',1,'R');
$pdf->Cell(65,$height, $this->sma->utf8Decode(lang("convention")),'T',0,'L');
$pdf->Ln();

$pdf->Output();

?>