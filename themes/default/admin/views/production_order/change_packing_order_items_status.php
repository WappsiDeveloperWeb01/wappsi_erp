<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-sm btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>
            <div class="text-center" style="margin-bottom:20px;">
                <h3><?= lang('add_receipt_order_to_packing_order') ?> N° <?= $inv->id ?></h3>
            </div>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-sm-5">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("ref"); ?>: <?= $inv->id; ?><br>
                    </p>
                    </div>
                    <div class="col-sm-7 text-right order_barcodes">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="col-sm-12 row">
                <?php
                $attrib = array('id' => 'form_receipt');
                echo admin_form_open_multipart("production_order/change_packing_order_items_status/".$inv->id, $attrib);
                ?>

                <div class="form-group col-sm-4">
                    <?= lang("warehouse", "pckwarehouse"); ?>
                    <?= form_input('warehouse', '', 'data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" class="form-control input-tip search_warehouse"'); ?>
                </div>

                <div class="table-responsive col-sm-12">
                    <label class="table-label"><?= lang("pfinished_products_in_packing_order"); ?> *</label>
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("description"); ?></th>
                                <th><?= lang("variant"); ?></th>
                                <th><?= lang("ok_quantity"); ?></th>
                                <th><?= lang("fault_quantity"); ?></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $r = 1;
                            $tTotal = 0;
                            foreach ($por_rows as $row):
                            ?>
                                <tr>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name; ?>
                                    </td>
                                    <th><?= $row->variant; ?></th>
                                    <th class="text-right">
                                        <input type="text" name="" class="form-control text-right pfinished_ok_quantity" value="<?= $this->sma->formatDecimal($row->packing_quantity, 2) ?>" data-pfinishedpid="<?= $row->id ?>" data-pfinishedoptionid="<?= $row->option_id ?>" max="<?= $this->sma->formatDecimal($row->packing_quantity) ?>" >
                                        <?php  $tTotal+= $row->packing_quantity; ?>
                                    </th>
                                    <th class="text-right">
                                        <input type="text" name="" class="form-control text-right pfinished_fault_quantity" value="<?= $this->sma->formatDecimal(0, 2) ?>" data-pfinishedpid="<?= $row->id ?>" data-pfinishedoptionid="<?= $row->option_id ?>" max="<?= $this->sma->formatDecimal($row->packing_quantity) ?>" >
                                    </th>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2" class="text-right"><?= lang('total') ?></th>
                                <th class="text-right total_pfinished_ok_quantity"><?= $this->sma->formatQuantity($tTotal); ?></th>
                                <th class="text-right total_pfinished_fault_quantity"><?= $this->sma->formatQuantity(0); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="table-responsive col-sm-12">
                    <label class="table-label"><?= lang("components"); ?> *</label>
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("description"); ?></th>
                                <th class="no-mobile"><?= lang("product"); ?></th>
                                <th class="no-mobile"><?= lang("order_quantity"); ?></th>
                                <th class="no-mobile"><?= lang("finished_quantity"); ?></th>
                                <th class="no-mobile"><?= lang("fault_quantity"); ?></th>
                                <th class="no-mobile"><?= lang("in_process_quantity"); ?></th>
                                <th><?= lang("ok_quantity"); ?></th>
                                <th><?= lang("fault_quantity"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $r = 1;
                            $fqty = 0;
                            $pqty = 0;
                            $ftqty = 0;
                            foreach ($rows as $row):
                            ?>
                                <tr>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name; ?>
                                    </td>
                                    <td class="no-mobile"><?= $row->pfinished_name.' - '.$row->pfinished_code.($row->pfinished_variant ? ' - '.$row->pfinished_variant : ''); ?></td>
                                    <th class="text-right no-mobile"><?= $this->sma->formatQuantity($row->packing_quantity); ?></th>
                                    <th class="text-right no-mobile"><?= $this->sma->formatQuantity($row->finished_quantity); $fqty+= $row->finished_quantity; ?></th>
                                    <th class="text-right no-mobile"><?= $this->sma->formatQuantity($row->fault_quantity); $ftqty+= $row->fault_quantity; ?></th>
                                    <th class="text-right no-mobile"><?= $this->sma->formatQuantity($row->packing_quantity - $row->finished_quantity - $row->fault_quantity);  $pqty+= ($row->packing_quantity - $row->finished_quantity - $row->fault_quantity);?></th>
                                    <th>
                                        <input type="hidden" name="item_id[]" value="<?= $row->id ?>">
                                        <input type="hidden" name="item_composition_qty[]" value="<?= $row->composition_quantity ?>">
                                        <input type="hidden" name="item_pfinished_id[]" value="<?= $row->production_order_pfinished_item_id ?>">
                                        <input type="hidden" name="item_product_id[]" value="<?= $row->product_id ?>">
                                        <input type="hidden" name="item_pfinished_product_id[]" value="<?= $row->pfinished_product_id ?>">
                                        <input type="hidden" name="item_pfinished_option_id[]" value="<?= $row->option_id ?>">
                                        <input type="text" name="ok_quantity[]" id="ok_quantity_<?= $row->id ?>" class="form-control text-right ok_quantity" data-pfinishedpid="<?= $row->production_order_pfinished_item_id ?>" data-pfinishedoptionid="<?= $row->option_id ?>" data-compositionqty="<?= $row->composition_quantity ?>" value="<?= ($row->packing_quantity - $row->finished_quantity - $row->fault_quantity) ?>" onclick="$(this).select();" max="<?= $row->packing_quantity - $row->finished_quantity - $row->fault_quantity ?>" min="0" readonly>
                                        <label class="error" for="ok_quantity_<?= $row->id ?>" style="display:none;"></label>
                                    </th>
                                    <th>
                                        <input type="text" name="fault_quantity[]" id="fault_quantity_<?= $row->id ?>" class="form-control text-right fault_quantity" data-pfinishedpid="<?= $row->production_order_pfinished_item_id ?>"  data-pfinishedoptionid="<?= $row->option_id ?>" data-compositionqty="<?= $row->composition_quantity ?>" value="0" onclick="$(this).select();" min="0" max="<?= $row->packing_quantity - $row->finished_quantity - $row->fault_quantity ?>" readonly>
                                        <label class="error" for="fault_quantity_<?= $row->id ?>" style="display:none;"></label>
                                    </th>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr class="no-mobile">
                                <th class="text-right" colspan="3"><?= lang('total') ?></th>
                                <th class="text-right"><?= $this->sma->formatQuantity($fqty); ?></th>
                                <th class="text-right"><?= $this->sma->formatQuantity($ftqty); ?></th>
                                <th class="text-right"><?= $this->sma->formatQuantity($pqty); ?></th>
                                <th class="text-right"><span class="total_ok_quantity">0.00</span></th>
                                <th class="text-right"><span class="total_fault_quantity">0.00</span></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-primary submit_receipt" type="button"><?= lang('submit') ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
            <div class="row">
                <div class="col-xs-12 col-md-7">
                    <?php if ($inv->note || $inv->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php } ?>
                </div>

                <div class="col-xs-12 col-md-5 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#form_receipt").validate({
            ignore: []
        });
        setTimeout(function() {
            $('#warehouse').select2();
        }, 850);
        $(document).on('click', '.submit_receipt', function(){
            if ($('#form_receipt').valid()) {
                $('#form_receipt').submit();
            }
        });
        $(document).on('keyup', '.ok_quantity', function(){
            findex = $('.ok_quantity').index($(this));
            fault_quantity = $('.fault_quantity').eq(findex);
            ok_quantity_input = $(this);
            pfinishedpid = ok_quantity_input.data('pfinishedpid');
            pfinishedoptionid = ok_quantity_input.data('pfinishedoptionid');
            modifiedcompositionqty = ok_quantity_input.data('compositionqty');
            tt = parseFloat(ok_quantity_input.val()) + parseFloat(fault_quantity.val());
            if (parseFloat(tt) > parseFloat(ok_quantity_input.prop('max'))) {
                $(this).val(0);
                $(this).select();
                command: toastr.warning('La cantidad a entregar en conformidad más en averías, no puede ser mayor a la cantidad de la orden de empaque', '¡Error!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "7000",
                            "extendedTimeOut": "1000",
                        });
            }
            $.each($('input[class="form-control text-right ok_quantity"][data-pfinishedpid="'+pfinishedpid+'"][data-pfinishedoptionid="'+pfinishedoptionid+'"]'), function(index, each_ok_quantity_input){
                f2index = $('.ok_quantity').index($(each_ok_quantity_input));
                if (findex != f2index) {
                    compositionqty = parseFloat($(each_ok_quantity_input).data('compositionqty'));
                    totalqty = compositionqty;
                    if (modifiedcompositionqty > compositionqty) {
                        totalqty = ok_quantity_input.val() / modifiedcompositionqty;
                    } else if (modifiedcompositionqty < compositionqty) {
                        totalqty = ok_quantity_input.val() * compositionqty;
                    } else if (modifiedcompositionqty == compositionqty) {
                        totalqty = ok_quantity_input.val();
                    }
                    $(each_ok_quantity_input).val(totalqty);
                }
            });
            var total_ok_quantity = 0;
            $.each($('.ok_quantity'), function(index, item){
                total_ok_quantity += parseFloat($(item).val());
            });
            $('.total_ok_quantity').text(formatQuantity2(total_ok_quantity));
        });
        $(document).on('keyup', '.fault_quantity', function(){
            findex = $('.fault_quantity').index($(this));
            fault_quantity_input = $(this);
            ok_quantity = $('.ok_quantity').eq(findex);
            pfinishedpid = fault_quantity_input.data('pfinishedpid');
            pfinishedoptionid = ok_quantity_input.data('pfinishedoptionid');
            modifiedcompositionqty = fault_quantity_input.data('compositionqty');
            tt = parseFloat($(this).val()) + parseFloat(ok_quantity.val());
            if (parseFloat(tt) > parseFloat($(this).prop('max'))) {
                $(this).val(0);
                $(this).select();
                command: toastr.warning('La cantidad a entregar en averías más en conformidad, no puede ser mayor a la cantidad de la orden de empaque', '¡Error!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "7000",
                            "extendedTimeOut": "1000",
                        });
            }
            $.each($('input[class="form-control text-right fault_quantity"][data-pfinishedpid="'+pfinishedpid+'"][data-pfinishedoptionid="'+pfinishedoptionid+'"]'), function(index, each_fault_quantity_input){
                f2index = $('.fault_quantity').index($(each_fault_quantity_input));
                if (findex != f2index) {
                    compositionqty = parseFloat($(each_fault_quantity_input).data('compositionqty'));
                    totalqty = compositionqty;
                    if (modifiedcompositionqty > compositionqty) {
                        totalqty = fault_quantity_input.val() / modifiedcompositionqty;
                    } else if (modifiedcompositionqty < compositionqty) {
                        totalqty = fault_quantity_input.val() * compositionqty;
                    } else if (modifiedcompositionqty == compositionqty) {
                        totalqty = fault_quantity_input.val();
                    }
                    $(each_fault_quantity_input).val(totalqty);
                }
            });
            var total_fault_quantity = 0;
            $.each($('.fault_quantity'), function(index, item){
                total_fault_quantity += parseFloat($(item).val());
            });
            $('.total_fault_quantity').text(formatQuantity2(total_fault_quantity));
        });
        $(document).on('keyup', '.pfinished_ok_quantity', function(){
            pfinishedpid = $(this).data('pfinishedpid');
            pfinishedoptionid = $(this).data('pfinishedoptionid');
            max = parseFloat($(this).prop('max'));
            findex = $('.pfinished_ok_quantity').index($(this));
            pfinished_fault_quantity = $('.pfinished_fault_quantity').eq(findex);
            tt = parseFloat($(this).val()) + parseFloat(pfinished_fault_quantity.val());
            if (tt > max) {
                $(this).val(0);
                $(this).select();
                command: toastr.warning('La cantidad a entregar en averías más en conformidad, no puede ser mayor a la cantidad de la orden de empaque', '¡Error!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "7000",
                            "extendedTimeOut": "1000",
                        });
            }
            comp_ok_quantity = $('input.ok_quantity[data-pfinishedpid="'+pfinishedpid+'"][data-pfinishedoptionid="'+pfinishedoptionid+'"]')[0];
            $(comp_ok_quantity).val($(this).val()).trigger('keyup');
            var total_pfinished_ok_quantity = 0;
            $('.pfinished_ok_quantity').each(function(index, pfinished_ok_quantity){
                total_pfinished_ok_quantity += $(pfinished_ok_quantity).val();
            });
            $('.total_pfinished_ok_quantity').text(formatDecimal(total_pfinished_ok_quantity));
        });
        $(document).on('keyup', '.pfinished_fault_quantity', function(){
            pfinishedpid = $(this).data('pfinishedpid');
            pfinishedoptionid = $(this).data('pfinishedoptionid');
            max = parseFloat($(this).prop('max'));
            findex = $('.pfinished_fault_quantity').index($(this));
            pfinished_ok_quantity = $('.pfinished_ok_quantity').eq(findex);
            tt = parseFloat($(this).val()) + parseFloat(pfinished_ok_quantity.val());
            if (tt > max) {
                $(this).val(0);
                $(this).select();
                command: toastr.warning('La cantidad a entregar en averías más en conformidad, no puede ser mayor a la cantidad de la orden de empaque', '¡Error!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "7000",
                            "extendedTimeOut": "1000",
                        });
            }
            comp_fault_quantity = $('input.fault_quantity[data-pfinishedpid="'+pfinishedpid+'"][data-pfinishedoptionid="'+pfinishedoptionid+'"]')[0];
            $(comp_fault_quantity).val($(this).val()).trigger('keyup');
            var total_pfinished_fault_quantity = 0;
            $('.pfinished_fault_quantity').each(function(index, pfinished_fault_quantity){
                total_pfinished_fault_quantity += $(pfinished_fault_quantity).val();
            });
            $('.total_pfinished_fault_quantity').text(formatDecimal(total_pfinished_fault_quantity));
        });
        setTimeout(function() {
            $('.ok_quantity').trigger('keyup');
        }, 850);
        set_warehouse_search();
    });
</script>