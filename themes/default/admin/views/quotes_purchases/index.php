<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }
    $(document).ready(function() {
        $('[data-toggle-second="tooltip"]').tooltip();
        
        oTable = $('#QUData').dataTable({
            aaSorting: [
                [1, "desc"],
                [2, "desc"]
            ],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('quotes/getQuotes' . ($warehouse_id ? '/' . $warehouse_id : '')) ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "index_type",
                    "value": "suppliers"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "quote_link";
                return nRow;
            },
            aoColumns: [{
                    "bSortable": false,
                    "mRender": checkbox
                },
                {
                    "mRender": fld
                },
                null,
                null,
                null,
                {
                    "bVisible": false
                },
                null,
                {
                    "mRender": currencyFormat
                },
                {
                    "mRender": row_status
                },
                {
                    "mRender": quote_type
                },
                {
                    "bSortable": false,
                    "mRender": attachment
                },
                {
                    "bSortable": false
                }
            ],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html(`<div class="pull-right dropdown">
                    <button data-toggle="dropdown" class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('quotes/addqpurchase') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_quote_purchase') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('quotes/addqexpense') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_quote_expense') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="combine" data-action="combine">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('combine_to_pdf') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <?php if (!empty($warehouses)) { ?>
                    <div class="pull-right dropdown">
                        <button data-toggle="dropdown" class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle-second="tooltip" data-placement="top" title="<?= lang("warehouses") ?>"><i class="fa fa-building"></i></button>
                        <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                            <li><a href="<?= admin_url('quotes') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                            <li class="divider"></li>
                            <?php
                            foreach ($warehouses as $warehouse) {
                                echo '<li><a href="' . admin_url('quotes/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                            }
                            ?>
                        </ul>
                    </div>
                <?php } ?>`);

                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }

        });

        <?php if ($this->session->userdata('remove_quls')) { ?>
            if (localStorage.getItem('quitems')) {
                localStorage.removeItem('quitems');
            }
            if (localStorage.getItem('qudiscount')) {
                localStorage.removeItem('qudiscount');
            }
            if (localStorage.getItem('qutax2')) {
                localStorage.removeItem('qutax2');
            }
            if (localStorage.getItem('qushipping')) {
                localStorage.removeItem('qushipping');
            }
            if (localStorage.getItem('quref')) {
                localStorage.removeItem('quref');
            }
            if (localStorage.getItem('quwarehouse')) {
                localStorage.removeItem('quwarehouse');
            }
            if (localStorage.getItem('qusupplier')) {
                localStorage.removeItem('qusupplier');
            }
            if (localStorage.getItem('qunote')) {
                localStorage.removeItem('qunote');
            }
            if (localStorage.getItem('qucustomer')) {
                localStorage.removeItem('qucustomer');
            }
            if (localStorage.getItem('qubiller')) {
                localStorage.removeItem('qubiller');
            }
            if (localStorage.getItem('qucurrency')) {
                localStorage.removeItem('qucurrency');
            }
            if (localStorage.getItem('qudate')) {
                localStorage.removeItem('qudate');
            }
            if (localStorage.getItem('qustatus')) {
                localStorage.removeItem('qustatus');
            }
            if (localStorage.getItem('othercurrency')) {
                localStorage.removeItem('othercurrency');
            }
            if (localStorage.getItem('othercurrencycode')) {
                localStorage.removeItem('othercurrencycode');
            }
            if (localStorage.getItem('othercurrencytrm')) {
                localStorage.removeItem('othercurrencytrm');
            }
        <?php $this->sma->unset_data('remove_quls');
        } ?>
    });
</script>

<?php if ($Owner || $GP['bulk_actions']) {
    echo admin_form_open('quotes/quote_actions', 'id="action-form"');
} ?>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="QUData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("destination_reference_no"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("customer_name"); ?></th>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <th><?= lang("total"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th><?= lang("type"); ?></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                            <th style="width:115px; text-align:center;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->

<?php if ($Owner || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>