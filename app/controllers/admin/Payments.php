<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->admin_model('sales_model');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model('pos_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->lang->admin_load('sales', $this->Settings->user_language);
    }

    function paypalipn()
    {

        $this->load->admin_model('sales_model');
        $paypal = $this->sales_model->getPaypalSettings();
        $this->sma->log_payment('Paypal IPN called');

        $req = 'cmd=_notify-validate';
        foreach ($_POST as $key => $value) {
            $value = urlencode(stripslashes($value));
            $req .= "&$key=$value";
        }

        $header = "POST /cgi-bin/webscr HTTP/1.1\r\n";
        $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $header .= "Host: www.paypal.com\r\n";  // www.sandbox.paypal.com for a test site
        $header .= "Content-Length: " . strlen($req) . "\r\n";
        $header .= "Connection: close\r\n\r\n";

        //$fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
        $fp = fsockopen('ssl://www.paypal.com', 443, $errno, $errstr, 30);

        if (!$fp) {

            $this->sma->log_payment('Paypal Payment Failed (IPN HTTP ERROR)', $errstr);
            $this->session->set_flashdata('error', lang('payment_failed'));

        } else {
            fputs($fp, $header . $req);
            while (!feof($fp)) {
                $res = fgets($fp, 1024);
                //log_message('error', 'Paypal IPN - fp handler -'.$res);
                if (stripos($res, "VERIFIED") !== false) {
                    $this->sma->log_payment('Paypal IPN - VERIFIED');

                    $custom = explode('__', $_POST['custom']);
                    $payer_email = $_POST['payer_email'];

                    if (($_POST['payment_status'] == 'Completed' || $_POST['payment_status'] == 'Processed' || $_POST['payment_status'] == 'Pending') &&
                        ($_POST['receiver_email'] == $paypal->account_email) &&
                        ($_POST['mc_gross'] == ($custom[1] + $custom[2]))
                    ) {

                        $invoice_no = $_POST['item_number'];
                        $reference = $_POST['item_name'];
                        if ($_POST['mc_currency'] == $this->Settings->default_currency) {
                            $amount = $_POST['mc_gross'];
                        } else {
                            $currency = $this->site->getCurrencyByCode($_POST['mc_currency']);
                            $amount = $_POST['mc_gross'] * (1 / $currency->rate);
                        }
                        if ($inv = $this->sales_model->getInvoiceByID($invoice_no)) {
                            $payment = array(
                                'date' => date('Y-m-d H:i:s'),
                                'sale_id' => $invoice_no,
                                'reference_no' => $this->site->getReference('pay'),
                                'amount' => $amount,
                                'paid_by' => 'paypal',
                                'transaction_id' => $_POST['txn_id'],
                                'type' => 'received',
                                'note' => $_POST['mc_currency'] . ' ' . $_POST['mc_gross'] . ' had been paid for the Sale Reference No ' . $reference
                            );
                            if ($this->sales_model->addPayment($payment)) {
                                $customer = $this->site->getCompanyByID($inv->customer_id);
                                $this->site->updateReference('pay');

                                $this->load->library('parser');
                                $parse_data = array(
                                    'reference_number' => $reference,
                                    'contact_person' => $customer->name,
                                    'company' => $customer->company,
                                    'site_link' => base_url(),
                                    'site_name' => $this->Settings->site_name,
                                    'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>'
                                );
                                $temp_path = is_dir('./themes/' . $this->Settings->theme . '/admin/views/email_templates/');
                                $theme = $temp_path ? $this->theme : 'default';
                                $msg = file_get_contents('./themes/' . $theme . '/admin/views/email_templates/payment.html');
                                $message = $this->parser->parse_string($msg, $parse_data);
                                $this->sma->log_payment('Payment has been made for Sale Reference #' . $_POST['item_name'] . ' via Paypal (' . $_POST['txn_id'] . ').', print_r($_POST, ture));
                                try {
                                    $this->sma->send_email($paypal->account_email, 'Payment has been made via Paypal', $message);
                                } catch (Exception $e) {
                                    $this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                                }
                                $this->session->set_flashdata('message', lang('payment_added'));
                            }
                        }
                    } else {

                        $this->sma->log_payment('Payment failed for Sale Reference #' . $reference . ' via Paypal (' . $_POST['txn_id'] . ').', print_r($_POST, ture));
                        $this->session->set_flashdata('error', lang('payment_failed'));

                    }
                } else if (stripos($res, "INVALID") !== false) {
                    $this->sma->log_payment('INVALID response from Paypal. Payment failed via Paypal.', print_r($_POST, ture));
                    $this->session->set_flashdata('error', lang('payment_failed'));
                }
            }
            fclose($fp);
        }
        redirect('/');
        exit();

    }

    function skrillipn()
    {
        $this->load->admin_model('sales_model');
        $skrill = $this->sales_model->getSkrillSettings();
        $this->sma->log_payment('Skrill IPN called');

        $concatFields = $_POST['merchant_id'] . $_POST['transaction_id'] . strtoupper(md5($skrill->secret_word)) . $_POST['mb_amount'] . $_POST['mb_currency'] . $_POST['status'];

        if (strtoupper(md5($concatFields)) == $_POST['md5sig'] && $_POST['status'] == 2 && $_POST['pay_to_email'] == $skrill->account_email) {
            $invoice_no = $_POST['item_number'];
            $reference = $_POST['item_name'];
            if ($_POST['mb_currency'] == $this->Settings->default_currency) {
                $amount = $_POST['mb_amount'];
            } else {
                $currency = $this->site->getCurrencyByCode($_POST['mb_currency']);
                $amount = $_POST['mb_amount'] * (1 / $currency->rate);
            }
            if ($inv = $this->sales_model->getInvoiceByID($invoice_no)) {
                $payment = array(
                    'date' => date('Y-m-d H:i:s'),
                    'sale_id' => $invoice_no,
                    'reference_no' => $this->site->getReference('pay'),
                    'amount' => $amount,
                    'paid_by' => 'skrill',
                    'transaction_id' => $_POST['mb_transaction_id'],
                    'type' => 'received',
                    'note' => $_POST['mb_currency'] . ' ' . $_POST['mb_amount'] . ' had been paid for the Sale Reference No ' . $reference
                );
                if ($this->sales_model->addPayment($payment)) {
                    $customer = $this->site->getCompanyByID($inv->customer_id);
                    $this->site->updateReference('pay');

                    $this->load->library('parser');
                    $parse_data = array(
                        'reference_number' => $reference,
                        'contact_person' => $customer->name,
                        'company' => $customer->company,
                        'site_link' => base_url(),
                        'site_name' => $this->Settings->site_name,
                        'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $this->Settings->logo . '" alt="' . $this->Settings->site_name . '"/>'
                    );
                    $temp_path = is_dir('./themes/' . $this->Settings->theme . '/admin/views/email_templates/');
                    $theme = $temp_path ? $this->theme : 'default';
                    $msg = file_get_contents('./themes/' . $theme . '/admin/views/email_templates/payment.html');
                    $message = $this->parser->parse_string($msg, $parse_data);
                    $this->sma->log_payment('Payment has been made for Sale Reference #' . $_POST['item_name'] . ' via Skrill (' . $_POST['mb_transaction_id'] . ').', print_r($_POST, ture));
                    try {
                        $this->sma->send_email($skrill->account_email, 'Payment has been made via Skrill', $message);
                    } catch (Exception $e) {
                        $this->sma->log_payment('Email Notification Failed: ' . $e->getMessage());
                    }
                    $this->session->set_flashdata('message', lang('payment_added'));
                }
            }
        } else {
            $this->sma->log_payment('Payment failed for via Skrill.', print_r($_POST, ture));
            $this->session->set_flashdata('error', lang('payment_failed'));
        }
        redirect('/');
        exit();

    }

    public function index(){

        $this->sma->checkPermissions();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('multi_payments')));
        $meta = array('page_title' => lang('multi_payments'), 'bc' => $bc);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['advancedFiltersContainer'] = FALSE;
        if (($this->input->post('p2payment_method') && $this->input->post('p2payment_method' !== '' )) || ($this->input->post('filter_by') && $this->input->post('filter_by') != '')) {
            $this->data['advancedFiltersContainer'] = TRUE;
        }
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([14, 33]);
        $this->data['users'] = $this->site->get_all_users();
        $this->page_construct('payments/index', $meta, $this->data);

    }

    public function pindex(){
        $this->sma->checkPermissions();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('multi_payments_purchases')));
        $meta = array('page_title' => lang('multi_payments_purchases'), 'bc' => $bc);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_multi_module_document_types([17, 34, 24]);
        $this->data['users'] = $this->site->get_all_users();
        $this->page_construct('payments/pindex', $meta, $this->data);

    }

    public function add($sale_id = NULL)
    {
        if (!$this->Owner && !$this->Admin) {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register');
            }
        }
        $this->form_validation->set_rules('date', lang('date'), 'required');
        $this->form_validation->set_rules('add_payment', lang('sales'), 'required');
        $this->form_validation->set_rules('document_type_id', lang('reference_no'), 'required');
        $this->form_validation->set_rules('customer', lang('customer'), 'required');
        // $this->form_validation->set_rules('row_selected[]', lang('sales'), 'required');
        if ($this->input->post('set_deposit')) {
            $this->form_validation->set_rules('deposit_document_type_id', lang('deposit_document_type_id'), 'required');
        }
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        $this->sma->checkPermissions();
        if ($sale_id) {
            $sale = $this->site->getSaleByID($sale_id);
            if ($sale->sale_status != 'completed') {
                $this->session->set_flashdata('error', lang('cannot_add_payment_for_sale_pending'));
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }
        }
        if ($this->form_validation->run() == true) {
            $total_payment = 0;
            $gtotal_retention = 0;
            $biller_id = $this->input->post('biller', true);
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }
            $cost_center = NULL;
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $cost_center = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $cost_center = $this->input->post('cost_center_id');
            }
            $rows_selected = $this->input->post('row_selected');
            $sales_id = $this->input->post('sale_id');
            $amounts = $this->input->post('amount');
            $customer = $this->input->post('customer');
            $consecutive_payment = $this->input->post('consecutive_payment');
            $rete_fuente_id = $this->input->post('sale_id_rete_fuente');
            $rete_iva_id = $this->input->post('sale_id_rete_iva');
            $rete_ica_id = $this->input->post('sale_id_rete_ica');
            $rete_bomberil_id = $this->input->post('sale_id_rete_bomberil');
            $rete_autoaviso_id = $this->input->post('sale_id_rete_autoaviso');
            $rete_other_id = $this->input->post('sale_id_rete_other');
            $rete_fuente_total = $this->input->post('sale_rete_fuente_total');
            $rete_iva_total = $this->input->post('sale_rete_iva_total');
            $rete_ica_total = $this->input->post('sale_rete_ica_total');
            $rete_bomberil_total = $this->input->post('sale_rete_bomberil_total');
            $rete_autoaviso_total = $this->input->post('sale_rete_autoaviso_total');
            $rete_other_total = $this->input->post('sale_rete_other_total');
            $rete_fuente_percentage = $this->input->post('sale_rete_fuente_percentage');
            $rete_iva_percentage = $this->input->post('sale_rete_iva_percentage');
            $rete_ica_percentage = $this->input->post('sale_rete_ica_percentage');
            $rete_bomberil_percentage = $this->input->post('sale_rete_bomberil_percentage');
            $rete_autoaviso_percentage = $this->input->post('sale_rete_autoaviso_percentage');
            $rete_other_percentage = $this->input->post('sale_rete_other_percentage');
            $rete_fuente_base = $this->input->post('sale_rete_fuente_base');
            $rete_iva_base = $this->input->post('sale_rete_iva_base');
            $rete_ica_base = $this->input->post('sale_rete_ica_base');
            $rete_bomberil_base = $this->input->post('sale_rete_bomberil_base');
            $rete_autoaviso_base = $this->input->post('sale_rete_autoaviso_base');
            $rete_other_base = $this->input->post('sale_rete_other_base');
            $rete_fuente_account = $this->input->post('sale_rete_fuente_account');
            $rete_iva_account = $this->input->post('sale_rete_iva_account');
            $rete_ica_account = $this->input->post('sale_rete_ica_account');
            $rete_bomberil_account = $this->input->post('sale_rete_bomberil_account');
            $rete_autoaviso_account = $this->input->post('sale_rete_autoaviso_account');
            $rete_other_account = $this->input->post('sale_rete_other_account');
            $sale_commision_base = $this->input->post('sale_commision_base');
            $sale_commision_amount = $this->input->post('sale_commision_amount');
            $sale_commision_percentage = $this->input->post('sale_commision_percentage');
            $seller_id = $this->input->post('sale_seller_id');
            $document_type_id = $this->input->post('document_type_id');
            $date = $this->sma->fld($this->input->post('date'));
            $this->site->validate_movement_date($date);
            $deposit = null;
            $txt_deposit = "";
            if ($this->input->post('set_deposit')) {
                $distribution = $this->sma->formatNumberMask($this->input->post('payment_distribution'));
                $amount_for_balance = $distribution - $this->input->post('deposit_amount');
                $txt_deposit = sprintf(lang('payment_note_for_automatic_deposit_customer'), $this->input->post('payment_distribution'), $this->sma->formatMoney($amount_for_balance), $this->sma->formatMoney($this->input->post('deposit_amount')));
                $deposit = array(
                    'date' => $date,
                    'amount' => $this->input->post('deposit_amount'),
                    'balance' => $this->input->post('deposit_amount'),
                    'paid_by' => $this->input->post('paid_by'),
                    'note' => $this->input->post('deposit_note'),
                    'company_id' => $this->input->post('customer'),
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'biller_id' => $this->input->post('biller'),
                    'document_type_id' => $this->input->post('deposit_document_type_id'),
                    'origen' => '2',
                    'origen_document_type_id' => $document_type_id,
                    'cost_center_id' => $cost_center,
                );
            }
            $referenceBiller = $this->site->getReferenceBiller($biller_id, $document_type_id);
            $manual_reference = false;
            if($referenceBiller){
                $reference = $referenceBiller;
                if ($this->input->post('manual_reference')) {
                    $ref_arr = explode("-", $reference);
                    $manual_reference = $this->input->post('manual_reference');
                    $reference = $ref_arr[0]."-".$manual_reference;
                }
            } else {
                $this->session->set_flashdata('error', 'El tipo de documento es inválido, por favor verifique.');
                admin_redirect('payments/add');
            }
            $payments = [];
            $retenciones = [];
            $payment_date = $this->input->post('payment_date') ? $this->sma->fld($this->input->post('payment_date')) : $date;
            $discount_sale_id = $this->input->post('discount_sale_id');
            $discount_sale_reference = $this->input->post('discount_sale_reference');
            $discount_apply_to = $this->input->post('discount_apply_to');
            $discount_amount = $this->input->post('discount_amount');
            $discount_amount_calculated = $this->input->post('discount_amount_calculated');
            $discount_ledger_id = $this->input->post('discount_ledger_id');
            $sale_discount = [];
            $total_discounts = 0;
            if ($discount_apply_to) {
                foreach ($discount_apply_to as $index => $apply_to) {
                    $sale_id = $discount_sale_id[$index];
                    $sale_reference = $discount_sale_reference[$index];
                    $amount = $discount_amount[$index];
                    $amount_calculated = $discount_amount_calculated[$index];
                    $total_discounts+=$amount_calculated;
                    $ledger_id = $discount_ledger_id[$index];
                    if (isset($sale_discount[$sale_id])) {
                        $sale_discount[$sale_id] += $amount_calculated;
                    } else {
                        $sale_discount[$sale_id] = $amount_calculated;
                    }
                    $payment = array(
                        'date'          => $date,
                        'sale_id'       => $sale_id,
                        'reference_no'  => $reference,
                        'amount'        => $amount_calculated,
                        'paid_by'       => 'discount',
                        'cheque_no'     => NULL,
                        'cc_no'         => NULL,
                        'cc_holder'     => NULL,
                        'cc_month'      => NULL,
                        'cc_year'       => NULL,
                        'cc_type'       => NULL,
                        'note'          => 'Descuento a Factura '.$sale_reference.', aplicado a '.lang($apply_to),
                        'cost_center_id'  => $cost_center,
                        'multi_payment' => 1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'type'          => 'discount',
                        'discount_ledger_id' => $ledger_id,
                        'document_type_id' => $document_type_id,
                    );
                    $payments[] = $payment;
                }
            }
            if ($rows_selected) {
                foreach ($rows_selected as $id => $val) {
                    // if ($amounts[$id] == 0) {
                    //     continue;
                    // }
                    $total_retention = 0;
                    if ($rete_fuente_total[$id] > 0) {
                        $retenciones[$sales_id[$id]]['rete_fuente_total'] = $rete_fuente_total[$id];
                        $retenciones[$sales_id[$id]]['rete_fuente_percentage'] = $rete_fuente_percentage[$id];
                        $retenciones[$sales_id[$id]]['rete_fuente_base'] = $rete_fuente_base[$id];
                        $retenciones[$sales_id[$id]]['rete_fuente_account'] = $rete_fuente_account[$id];
                        $total_retention += $rete_fuente_total[$id];
                    }
                    if ($rete_iva_total[$id] > 0) {
                        $retenciones[$sales_id[$id]]['rete_iva_total'] = $rete_iva_total[$id];
                        $retenciones[$sales_id[$id]]['rete_iva_percentage'] = $rete_iva_percentage[$id];
                        $retenciones[$sales_id[$id]]['rete_iva_base'] = $rete_iva_base[$id];
                        $retenciones[$sales_id[$id]]['rete_iva_account'] = $rete_iva_account[$id];
                        $total_retention += $rete_iva_total[$id];
                    }
                    if ($rete_ica_total[$id] > 0) {
                        $retenciones[$sales_id[$id]]['rete_ica_total'] = $rete_ica_total[$id];
                        $retenciones[$sales_id[$id]]['rete_ica_percentage'] = $rete_ica_percentage[$id];
                        $retenciones[$sales_id[$id]]['rete_ica_base'] = $rete_ica_base[$id];
                        $retenciones[$sales_id[$id]]['rete_ica_account'] = $rete_ica_account[$id];
                        $total_retention += $rete_ica_total[$id];
                    }
                    if ($rete_other_total[$id] > 0) {
                        $retenciones[$sales_id[$id]]['rete_other_total'] = $rete_other_total[$id];
                        $retenciones[$sales_id[$id]]['rete_other_percentage'] = $rete_other_percentage[$id];
                        $retenciones[$sales_id[$id]]['rete_other_base'] = $rete_other_base[$id];
                        $retenciones[$sales_id[$id]]['rete_other_account'] = $rete_other_account[$id];
                        $total_retention += $rete_other_total[$id];
                    }


                    if ($rete_bomberil_total[$id] > 0) {
                        $retenciones[$sales_id[$id]]['rete_bomberil_total'] = $rete_bomberil_total[$id];
                        $retenciones[$sales_id[$id]]['rete_bomberil_percentage'] = $rete_bomberil_percentage[$id];
                        $retenciones[$sales_id[$id]]['rete_bomberil_base'] = $rete_bomberil_base[$id];
                        $retenciones[$sales_id[$id]]['rete_bomberil_account'] = $rete_bomberil_account[$id];
                        $total_retention += $rete_bomberil_total[$id];
                    }
                    if ($rete_autoaviso_total[$id] > 0) {
                        $retenciones[$sales_id[$id]]['rete_autoaviso_total'] = $rete_autoaviso_total[$id];
                        $retenciones[$sales_id[$id]]['rete_autoaviso_percentage'] = $rete_autoaviso_percentage[$id];
                        $retenciones[$sales_id[$id]]['rete_autoaviso_base'] = $rete_autoaviso_base[$id];
                        $retenciones[$sales_id[$id]]['rete_autoaviso_account'] = $rete_autoaviso_account[$id];
                        $total_retention += $rete_autoaviso_total[$id];
                    }
                    $payment = [];
                    $total_payment += ($this->sma->formatNumberMask($amounts[$id]) + (isset($total_retention) ? $total_retention : 0) + $total_discounts);
                    $gtotal_retention += $total_retention;
                    $dataSale = $this->site->getSaleByID($sales_id[$id]);
                    $invoice_affected_balance = false;
                    if ($dataSale) {
                        $invoice_affected_balance = ($dataSale->grand_total - $dataSale->paid) - ($this->sma->formatNumberMask($amounts[$id]) + $total_retention);
                    }
                    $payment = array(
                        'date'          => $date,
                        'sale_id'       => $sales_id[$id],
                        'reference_no'  => $reference,
                        'amount'        => $this->sma->formatNumberMask($amounts[$id]) + $total_retention,
                        'paid_by'       => $this->input->post('paid_by'),
                        'cheque_no'     => $this->input->post('cheque_no'),
                        'cc_no'         => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                        'cc_holder'     => $this->input->post('pcc_holder'),
                        'cc_month'      => $this->input->post('pcc_month'),
                        'cc_year'       => $this->input->post('pcc_year'),
                        'cc_type'       => $this->input->post('pcc_type'),
                        'note'          => $this->input->post('payment_note')." ".$txt_deposit,
                        'payment_date'          => $payment_date,
                        'consecutive_payment'   => $consecutive_payment,
                        'cost_center_id'  => $cost_center,
                        'rete_fuente_total' => $rete_fuente_total[$id],
                        'rete_iva_total'    => $rete_iva_total[$id],
                        'rete_ica_total'    => $rete_ica_total[$id],
                        'rete_bomberil_total'    => $rete_bomberil_total[$id],
                        'rete_autoaviso_total'    => $rete_autoaviso_total[$id],
                        'rete_other_total'  => $rete_other_total[$id],
                        'rete_fuente_percentage' => $rete_fuente_percentage[$id],
                        'rete_fuente_base' => $rete_fuente_base[$id],
                        'rete_fuente_account' => $rete_fuente_account[$id],
                        'rete_iva_percentage' => $rete_iva_percentage[$id],
                        'rete_iva_base' => $rete_iva_base[$id],
                        'rete_iva_account' => $rete_iva_account[$id],
                        'rete_ica_percentage' => $rete_ica_percentage[$id],
                        'rete_ica_base' => $rete_ica_base[$id],
                        'rete_ica_account' => $rete_ica_account[$id],
                        'rete_bomberil_percentage' => $rete_bomberil_percentage[$id],
                        'rete_bomberil_base' => $rete_bomberil_base[$id],
                        'rete_bomberil_account' => $rete_bomberil_account[$id],
                        'rete_autoaviso_percentage' => $rete_autoaviso_percentage[$id],
                        'rete_autoaviso_base' => $rete_autoaviso_base[$id],
                        'rete_autoaviso_account' => $rete_autoaviso_account[$id],
                        'rete_other_percentage' => $rete_other_percentage[$id],
                        'rete_other_base' => $rete_other_base[$id],
                        'rete_other_account' => $rete_other_account[$id],
                        'rete_fuente_id' => $rete_fuente_id[$id],
                        'rete_iva_id' => $rete_iva_id[$id],
                        'rete_ica_id' => $rete_ica_id[$id],
                        'rete_bomberil_id' => $rete_bomberil_id[$id],
                        'rete_autoaviso_id' => $rete_autoaviso_id[$id],
                        'rete_other_id' => $rete_other_id[$id],

                        'comm_base' => $sale_commision_base[$id],
                        'comm_amount' => $sale_commision_amount[$id],
                        'comm_perc' => $sale_commision_percentage[$id],
                        'seller_id' => $seller_id[$id],

                        'multi_payment' => 1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'type'          => 'received',
                        'document_type_id' => $document_type_id,
                        'invoice_affected_balance' => ($invoice_affected_balance ? $invoice_affected_balance : 0)
                    );
                    $payments[] = $payment;
                }
            }
            $type_mov = $this->input->post('type_mov');
            $concepto_description = $this->input->post('concepto_description');
            $concepto_amount = $this->input->post('concepto_amount');
            $ledger = $this->input->post('ledger');
            $cost_center_id_concepto = $this->input->post('cost_center_id_concepto');
            $biller_concepto = $this->input->post('biller_concepto');
            $conceptos = [];
            if ($type_mov && count($type_mov) > 0) {
                foreach ($type_mov as $id => $tm) {
                    $concept_cost_center = $cost_center;
                    $concepto = array(
                        'date'          => $date,
                        'sale_id'       => NULL,
                        'reference_no'  => $reference,
                        'amount'        => $tm == "+" ? $concepto_amount[$id] : $concepto_amount[$id] * -1,
                        'paid_by'       => $this->input->post('paid_by'),
                        'cheque_no'     => $this->input->post('cheque_no'),
                        'cc_no'         => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                        'cc_holder'     => $this->input->post('pcc_holder'),
                        'cc_month'      => $this->input->post('pcc_month'),
                        'cc_year'       => $this->input->post('pcc_year'),
                        'cc_type'       => $this->input->post('pcc_type'),
                        'note'          => $concepto_description[$id],
                        'cost_center_id'  => $concept_cost_center,
                        'multi_payment' => 1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'type'          => 'received',
                        'company_id'    => $customer,
                        'ledger_id'     => $ledger[$id],
                        'document_type_id' => $document_type_id,
                    );
                    $conceptos[] = $concepto;
                    $total_payment += ($tm == "+" ? $concepto_amount[$id] : $concepto_amount[$id] * -1);
                }
            }
            // exit(var_dump($total_payment));
            if ((!$rows_selected && !$type_mov) || (($type_mov || $rows_selected) && $total_payment == 0)) {
                $this->session->set_flashdata('error', 'Debe diligenciar al menos una venta a pagar o un concepto para el recibo de caja');
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payments[0]['attachment'] = $photo;
            }
            if ($this->input->post('paid_by') == 'deposit') {
                if (!$this->site->check_customer_deposit($this->input->post('customer'), $total_payment - $gtotal_retention)) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            // $this->sma->print_arrays($payments, $conceptos);
        }
        if ($this->form_validation->run() == true && $this->sales_model->updateSalesPayments($this->input->post('paid_by'), $payments, $customer, $retenciones, $conceptos, $cost_center, $deposit, $manual_reference, $biller_id)) {
            $this->session->set_flashdata('message', lang('multi_payment_added'));
            $this->session->set_userdata('remove_multipayments', 1);
            admin_redirect('payments/multi_payment_view/'.$reference);
        } elseif ($this->input->post('add_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('payments');
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['payment_ref'] = $this->site->getReference('rc');
            if ($this->Settings->modulary) {
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['collection_discounts'] = $this->site->get_collection_discounts();
            if ($sale_id) {
                $sale = $this->site->getSaleByID($sale_id);
                if ($sale) {
                    $this->data['sale_id'] = $sale_id;
                    $this->data['sale'] = $sale;
                }
            }
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_multi_payment_sales')));
            $meta = array('page_title' => lang('add_multi_payment_sales'), 'bc' => $bc);
            $this->page_construct('payments/add', $meta, $this->data);
        }
    }

    public function getPayments($type = 1){

        if ($this->input->post('start_date')) {
             $start_date = $this->input->post('start_date');
             $start_date = $this->sma->fld($start_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $start_date = NULL;
            } else {
                $start_date = date('Y-m-01 00:00');
            }
        }
        if ($this->input->post('end_date')) {
             $end_date = $this->input->post('end_date');
             $end_date = $this->sma->fld($end_date);
        } else {
            if ($this->Settings->default_records_filter == 0) {
                $end_date = NULL;
            } else {
                $end_date =date('Y-m-d H:i');
            }
        }
        $document_type_id = NULL;
        if ($this->input->post('ppayment_reference_no')) {
            $document_type_id = $this->input->post('ppayment_reference_no');
        }
        if ($this->input->post('p2payment_reference_no')) {
            $document_type_id = $this->input->post('p2payment_reference_no');
        }
        $payment_method = NULL;
        if ($this->input->post('ppayment_method')) {
            $payment_method = $this->input->post('ppayment_method');
        }
        if ($this->input->post('p2payment_method')) {
            $payment_method = $this->input->post('p2payment_method');
        }
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $supplier_id = $this->input->post('supplier') ? $this->input->post('supplier') : NULL;
        $customer_id = $this->input->post('customer') ? $this->input->post('customer') : NULL;
        $filter_by = $this->input->post('filter_by') ? $this->input->post('filter_by') : NULL;
        $filter_data = [
                        'start_date' => $start_date,
                        'end_date' => $end_date,
                        'document_type_id' => $document_type_id,
                        'biller_id' => $biller_id,
                        'user_id' => $user_id,
                        'supplier_id' => $supplier_id,
                        'customer_id' => $customer_id,
                        'payment_method' => $payment_method,
                        'filter_by' => $filter_by,
                        ];

        $this->sma->checkPermissions('index');
        echo $this->site->getMultiPayments(null, $type, $filter_data);
    }

    public function multiView($reference){
        $this->sma->checkPermissions('index', true);
        $enc = $this->site->getMultiPaymentEnc($reference) ? $this->site->getMultiPaymentEnc($reference) : $this->site->getMultiPaymentEncOnlyConcepts($reference);
        $pmnts = $this->site->getDetailMultiPayment($reference);
        $this->data['enc'] = $enc[0];
        $this->data['pmnts'] = $pmnts;
        $this->data['biller'] = $this->site->getCompanyByID($enc[0]->biller_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($enc[0]->document_type_id);
        $this->data['created_user'] = $this->site->getUserById($this->data['enc']->created_by);
        $this->load_view($this->theme . 'payments/view', $this->data);
    }

    public function addConcept(){
        $ptype = $this->input->get('ptype');
        if ($this->Settings->modulary) {
            $ledgers = $this->site->getAllLedgers();
            $lopts[''] = lang('select');
            if ($ledgers) {
                foreach ($ledgers as $ledger) {
                    $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                }
            }
        }
        $type = array(
            '+' => lang('addition'),
            '-' => lang('substraction')
        );
        $html = '<tr>
                    <td>
                        <div class="form-group">
                            <select name="type_mov[]" class="form-control select2 type_mov" style="width:100%;" required>
                                <option value="">'.lang('select').'</option>';
                                foreach ($type as $id => $value) {
                                    $html .= '<option value="'.$id.'">'.$value.'</option>';
                                }
                  $html .= '</select>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="concepto_description[]" placeholder="'.lang('type_description').'" class="form-control" required>
                        </div>
                    </td>
                    <td>
                        <div class="form-group">
                            <input type="text" name="concepto_amount[]" placeholder="'.lang('type_amount').'" class="form-control only_number concepto_amount" required>
                        </div>
                    </td>';

        if ($ptype == 2) {
            $html .= '<td>
                        <div class="form-group">
                            <input type="text" name="concepto_base[]" placeholder="'.lang('base').'" class="form-control only_number concepto_base">
                        </div>
                    </td>';
        }
                    if ($this->Settings->modulary){
                       $html .= '<td>
                                    <div class="form-group">
                                        <select name="ledger[]" class="form-control select2" style="width:100%;" required>';
                                            foreach ($lopts as $id => $value) {
                                                $html .= '<option value="'.$id.'">'.$value.'</option>';
                                            }
                          $html .= '    </select>
                                    </div>
                                </td>';
                    }
        if ($ptype == 2) {
            $html .= '<td>
                        <div class="form-group">
                            <input type="text" name="concepto_supplier[]" placeholder="'.lang('supplier').'" class="form-control concepto_supplier">
                        </div>
                    </td>';
        }
            $html .= '<td>
                        <button class="btn btn-sm btn-danger delete_row" type="button"><span class="fa fa-trash"></span></button>
                      </td>';
            $html .= '</tr>';
        echo $html;
    }

    public function padd($purchase_id = null){
        if (!$this->Owner && !$this->Admin) {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register');
            }
        }
        $this->form_validation->set_rules('add_payment', lang('purchases'), 'required');
        $this->form_validation->set_rules('document_type_id', lang('reference_no'), 'required');
        $this->form_validation->set_rules('supplier', lang('supplier'), 'required');
        // $this->form_validation->set_rules('row_selected[]', lang('purchases'), 'required');
        $this->form_validation->set_rules('date', lang('date'), 'required');
        if ($this->input->post('set_deposit')) {
            $this->form_validation->set_rules('deposit_document_type_id', lang('deposit_document_type_id'), 'required');
        }
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if ($this->Settings->purchase_payment_affects_cash_register == 2 && $this->input->post('paid_by') == 'cash') {
            $this->form_validation->set_rules('payment_affects_register', lang('payment_affects_register'), 'required');
        }
        $this->sma->checkPermissions();
        if ($purchase_id) {
            $purchase = $this->site->getPurchaseByID($purchase_id);
            if ($purchase->status != 'received') {
                $this->session->set_flashdata('error', lang('cannot_add_payment_for_purchase_pending'));
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
            }
        }
        if ($this->form_validation->run() == true) {
            $total_payment = 0;
            $total_discounts = 0;
            $biller_id = $this->input->post('biller', true);
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }
            $cost_center = NULL;
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $cost_center = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $cost_center = $this->input->post('cost_center_id');
            }
            $rows_selected = $this->input->post('row_selected');
            $purchases_id = $this->input->post('purchase_id');
            $amounts = $this->input->post('amount');
            $supplier = $this->input->post('supplier');
            $consecutive_payment = $this->input->post('consecutive_payment');
            $rete_fuente_id = $this->input->post('purchase_id_rete_fuente');
            $rete_iva_id = $this->input->post('purchase_id_rete_iva');
            $rete_ica_id = $this->input->post('purchase_id_rete_ica');
            $rete_bomberil_id = $this->input->post('purchase_id_rete_bomberil');
            $rete_autoaviso_id = $this->input->post('purchase_id_rete_autoaviso');
            $rete_other_id = $this->input->post('purchase_id_rete_other');
            $rete_fuente_total = $this->input->post('purchase_rete_fuente_total');
            $rete_iva_total = $this->input->post('purchase_rete_iva_total');
            $rete_ica_total = $this->input->post('purchase_rete_ica_total');
            $rete_bomberil_total = $this->input->post('purchase_rete_bomberil_total');
            $rete_autoaviso_total = $this->input->post('purchase_rete_autoaviso_total');
            $rete_other_total = $this->input->post('purchase_rete_other_total');
            $rete_fuente_percentage = $this->input->post('purchase_rete_fuente_percentage');
            $rete_iva_percentage = $this->input->post('purchase_rete_iva_percentage');
            $rete_ica_percentage = $this->input->post('purchase_rete_ica_percentage');
            $rete_bomberil_percentage = $this->input->post('purchase_rete_bomberil_percentage');
            $rete_autoaviso_percentage = $this->input->post('purchase_rete_autoaviso_percentage');
            $rete_other_percentage = $this->input->post('purchase_rete_other_percentage');
            $rete_fuente_base = $this->input->post('purchase_rete_fuente_base');
            $rete_iva_base = $this->input->post('purchase_rete_iva_base');
            $rete_ica_base = $this->input->post('purchase_rete_ica_base');
            $rete_bomberil_base = $this->input->post('purchase_rete_bomberil_base');
            $rete_autoaviso_base = $this->input->post('purchase_rete_autoaviso_base');
            $rete_other_base = $this->input->post('purchase_rete_other_base');
            $rete_fuente_account = $this->input->post('purchase_rete_fuente_account');
            $rete_iva_account = $this->input->post('purchase_rete_iva_account');
            $rete_ica_account = $this->input->post('purchase_rete_ica_account');
            $rete_bomberil_account = $this->input->post('purchase_rete_bomberil_account');
            $rete_autoaviso_account = $this->input->post('purchase_rete_autoaviso_account');
            $rete_other_account = $this->input->post('purchase_rete_other_account');
            $rete_fuente_assumed = $this->input->post('purchase_rete_fuente_assumed');
            $rete_iva_assumed = $this->input->post('purchase_rete_iva_assumed');
            $rete_ica_assumed = $this->input->post('purchase_rete_ica_assumed');
            $rete_other_assumed = $this->input->post('purchase_rete_other_assumed');
            $rete_fuente_assumed_account = $this->input->post('purchase_rete_fuente_assumed_account');
            $rete_iva_assumed_account = $this->input->post('purchase_rete_iva_assumed_account');
            $rete_ica_assumed_account = $this->input->post('purchase_rete_ica_assumed_account');
            $rete_bomberil_assumed_account = $this->input->post('purchase_rete_bomberil_assumed_account');
            $rete_autoaviso_assumed_account = $this->input->post('purchase_rete_autoaviso_assumed_account');
            $rete_other_assumed_account = $this->input->post('purchase_rete_other_assumed_account');
            $payments = [];
            $retenciones = [];
            $document_type_id = $this->input->post('document_type_id');
            $referenceBiller = $this->site->getReferenceBiller($biller_id, $document_type_id);
            $manual_reference = false;
            if($referenceBiller){
                $reference = $referenceBiller;
                if ($this->input->post('manual_reference')) {
                    $ref_arr = explode("-", $reference);
                    $manual_reference = $this->input->post('manual_reference');
                    $reference = $ref_arr[0]."-".$manual_reference;
                }
            } else {
                $this->session->set_flashdata('error', 'El tipo de documento es inválido, por favor verifique.');
                admin_redirect('payments/padd');
            }
            $date = $this->sma->fld($this->input->post('date'));
            $this->site->validate_movement_date($date);
            $deposit = null;
            $txt_deposit = "";

            if ($this->input->post('set_deposit')) {
                $distribution = $this->sma->formatNumberMask($this->input->post('payment_distribution'));
                $amount_for_balance = $distribution - $this->input->post('deposit_amount');
                $txt_deposit = sprintf(lang('payment_note_for_automatic_deposit_supplier'), $this->input->post('payment_distribution'), $this->sma->formatMoney($amount_for_balance), $this->sma->formatMoney($this->input->post('deposit_amount')));
                $this->site->validate_movement_date($date);
                $deposit = array(
                    'date' => $date,
                    'amount' => $this->input->post('deposit_amount'),
                    'balance' => $this->input->post('deposit_amount'),
                    'paid_by' => $this->input->post('paid_by'),
                    'note' => $this->input->post('deposit_note'),
                    'company_id' => $this->input->post('supplier'),
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'biller_id' => $this->input->post('biller'),
                    'document_type_id' => $this->input->post('deposit_document_type_id'),
                    'origen' => '2',
                    'origen_document_type_id' => $document_type_id,
                    'cost_center_id' => $cost_center,
                    'affects_pos_register' => $this->input->post('payment_affects_register') == 0 ? 0 : 1
                );
                if ($this->Settings->cashier_close != 2 && $this->input->post('payment_affects_register') == 1) {
                    if (isset($deposit) && ($this->input->post('paid_by') == "cash" && $this->input->post('deposit_amount') > 0) && !$this->pos_model->getRegisterState(($this->input->post('deposit_amount')))) {
                        $this->session->set_flashdata('error', "El dinero en caja no es suficiente para registrar el anticipo.");
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
            }
            $payment_affects_register = $this->input->post('payment_affects_register');
            $type = 'sent';
            if ($payment_affects_register == 0) {
                $type = 'sent_2';
            }
            $payment_date = $this->input->post('payment_date') ? $this->sma->fld($this->input->post('payment_date')) : $date;
            $discount_purchase_id = $this->input->post('discount_purchase_id');
            $discount_purchase_reference = $this->input->post('discount_purchase_reference');
            $discount_apply_to = $this->input->post('discount_apply_to');
            $discount_amount = $this->input->post('discount_amount');
            $discount_amount_calculated = $this->input->post('discount_amount_calculated');
            $discount_ledger_id = $this->input->post('discount_ledger_id');
            $purchase_discount = [];
            if ($discount_apply_to) {
                foreach ($discount_apply_to as $index => $apply_to) {
                    $purchase_id = $discount_purchase_id[$index];
                    $purchase_reference = $discount_purchase_reference[$index];
                    $amount = $discount_amount[$index];
                    $amount_calculated = $discount_amount_calculated[$index];
                    $ledger_id = $discount_ledger_id[$index];
                    if (isset($purchase_discount[$purchase_id])) {
                        $purchase_discount[$purchase_id] += $amount_calculated;
                    } else {
                        $purchase_discount[$purchase_id] = $amount_calculated;
                    }
                    $total_discounts += $amount_calculated;
                    $payment = array(
                        'date'          => $date,
                        'purchase_id'       => $purchase_id,
                        'reference_no'  => $reference,
                        'amount'        => $amount_calculated,
                        'paid_by'       => 'discount',
                        'cheque_no'     => NULL,
                        'cc_no'         => NULL,
                        'cc_holder'     => NULL,
                        'cc_month'      => NULL,
                        'cc_year'       => NULL,
                        'cc_type'       => NULL,
                        'note'          => 'Descuento a Factura '.$purchase_reference.', aplicado a '.lang($apply_to),
                        'cost_center_id'  => $cost_center,
                        'multi_payment' => 1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'type'          => 'discount',
                        'discount_ledger_id' => $ledger_id,
                        'document_type_id' => $document_type_id,
                    );
                    $payments[] = $payment;
                }
            }

            $assumed_retentions_text = "";
            if ($rows_selected) {
                foreach ($rows_selected as $id => $val) {
                    // if ($amounts[$id] == 0) {
                    //     continue;
                    // }
                    $total_retention = 0;
                    $Ppurchase_data = $this->site->getPurchaseByID($purchases_id[$id]);
                    if ($rete_fuente_total[$id] > 0) {
                        $retenciones[$purchases_id[$id]]['rete_fuente_total'] = $rete_fuente_total[$id];
                        $retenciones[$purchases_id[$id]]['rete_fuente_percentage'] = $rete_fuente_percentage[$id];
                        $retenciones[$purchases_id[$id]]['rete_fuente_base'] = $rete_fuente_base[$id];
                        $retenciones[$purchases_id[$id]]['rete_fuente_account'] = $rete_fuente_account[$id];
                        $retenciones[$purchases_id[$id]]['rete_fuente_assumed'] = $rete_fuente_assumed[$id] == 'true' ? 1 : 0;
                        $retenciones[$purchases_id[$id]]['rete_fuente_assumed_account'] = $rete_fuente_assumed[$id] == 'true' ? $rete_fuente_assumed_account[$id] : NULL;
                        if ($rete_fuente_assumed[$id] != 'true') {
                            $total_retention += $rete_fuente_total[$id];
                        } else {
                            $assumed_retentions_text.="Rete Fuente asumida (".$Ppurchase_data->reference_no." - ".$this->sma->formatMoney($rete_fuente_total[$id])."), ";
                        }
                    }
                    if ($rete_iva_total[$id] > 0) {
                        $retenciones[$purchases_id[$id]]['rete_iva_total'] = $rete_iva_total[$id];
                        $retenciones[$purchases_id[$id]]['rete_iva_percentage'] = $rete_iva_percentage[$id];
                        $retenciones[$purchases_id[$id]]['rete_iva_base'] = $rete_iva_base[$id];
                        $retenciones[$purchases_id[$id]]['rete_iva_account'] = $rete_iva_account[$id];
                        $retenciones[$purchases_id[$id]]['rete_iva_assumed'] = $rete_iva_assumed[$id] == 'true' ? 1 : 0;
                        $retenciones[$purchases_id[$id]]['rete_iva_assumed_account'] = $rete_iva_assumed[$id] == 'true' ? $rete_iva_assumed_account[$id] : NULL;
                        if ($rete_iva_assumed[$id] != 'true') {
                            $total_retention += $rete_iva_total[$id];
                        } else {
                            $assumed_retentions_text.="Rete IVA asumida (".$Ppurchase_data->reference_no." - ".$this->sma->formatMoney($rete_iva_total[$id])."), ";
                        }
                    }
                    if ($rete_ica_total[$id] > 0) {
                        $retenciones[$purchases_id[$id]]['rete_ica_total'] = $rete_ica_total[$id];
                        $retenciones[$purchases_id[$id]]['rete_ica_percentage'] = $rete_ica_percentage[$id];
                        $retenciones[$purchases_id[$id]]['rete_ica_base'] = $rete_ica_base[$id];
                        $retenciones[$purchases_id[$id]]['rete_ica_account'] = $rete_ica_account[$id];
                        $retenciones[$purchases_id[$id]]['rete_ica_assumed'] = $rete_ica_assumed[$id] == 'true' ? 1 : 0;
                        $retenciones[$purchases_id[$id]]['rete_ica_assumed_account'] = $rete_ica_assumed[$id] == 'true' ? $rete_ica_assumed_account[$id] : NULL;
                        if ($rete_ica_assumed[$id] != 'true') {
                            $total_retention += $rete_ica_total[$id];
                        } else {
                            $assumed_retentions_text.="Rete ICA asumida (".$Ppurchase_data->reference_no." - ".$this->sma->formatMoney($rete_ica_total[$id])."), ";
                        }
                    }
                    if ($rete_other_total[$id] > 0) {
                        $retenciones[$purchases_id[$id]]['rete_other_total'] = $rete_other_total[$id];
                        $retenciones[$purchases_id[$id]]['rete_other_percentage'] = $rete_other_percentage[$id];
                        $retenciones[$purchases_id[$id]]['rete_other_base'] = $rete_other_base[$id];
                        $retenciones[$purchases_id[$id]]['rete_other_account'] = $rete_other_account[$id];
                        $retenciones[$purchases_id[$id]]['rete_other_assumed'] = $rete_other_assumed[$id] == 'true' ? 1 : 0;
                        $retenciones[$purchases_id[$id]]['rete_other_assumed_account'] = $rete_other_assumed[$id] == 'true' ? $rete_other_assumed_account[$id] : NULL;
                        if ($rete_other_assumed[$id] != 'true') {
                            $total_retention += $rete_other_total[$id];
                        } else {
                            $assumed_retentions_text.="Rete OTRAS asumida (".$Ppurchase_data->reference_no." - ".$this->sma->formatMoney($rete_other_total[$id])."), ";
                        }
                    }
                    if ($rete_bomberil_total[$id] > 0) {
                        $retenciones[$purchases_id[$id]]['rete_bomberil_total'] = $rete_bomberil_total[$id];
                        $retenciones[$purchases_id[$id]]['rete_bomberil_percentage'] = $rete_bomberil_percentage[$id];
                        $retenciones[$purchases_id[$id]]['rete_bomberil_base'] = $rete_bomberil_base[$id];
                        $retenciones[$purchases_id[$id]]['rete_bomberil_account'] = $rete_bomberil_account[$id];
                        $retenciones[$purchases_id[$id]]['rete_bomberil_assumed_account'] = $rete_ica_assumed[$id] == 'true' ? $rete_bomberil_assumed_account[$id] : NULL;
                        if ($rete_ica_assumed[$id] != 'true') {
                            $total_retention += $rete_bomberil_total[$id];
                        } else {
                            $assumed_retentions_text.="Tasa Bomberil Asumida (".$Ppurchase_data->reference_no." - ".$this->sma->formatMoney($rete_bomberil_total[$id])."), ";
                        }
                    }
                    if ($rete_autoaviso_total[$id] > 0) {
                        $retenciones[$purchases_id[$id]]['rete_autoaviso_total'] = $rete_autoaviso_total[$id];
                        $retenciones[$purchases_id[$id]]['rete_autoaviso_percentage'] = $rete_autoaviso_percentage[$id];
                        $retenciones[$purchases_id[$id]]['rete_autoaviso_base'] = $rete_autoaviso_base[$id];
                        $retenciones[$purchases_id[$id]]['rete_autoaviso_account'] = $rete_autoaviso_account[$id];
                        $retenciones[$purchases_id[$id]]['rete_autoaviso_assumed_account'] = $rete_ica_assumed[$id] == 'true' ? $rete_autoaviso_assumed_account[$id] : NULL;
                        if ($rete_ica_assumed[$id] != 'true') {
                            $total_retention += $rete_autoaviso_total[$id];
                        } else {
                            $assumed_retentions_text.="Tasa Auto Avisos y Tableros Asumida (".$Ppurchase_data->reference_no." - ".$this->sma->formatMoney($rete_autoaviso_total[$id])."), ";
                        }
                    }
                    $assumed_retentions_text = trim($assumed_retentions_text, ", ");
                    $payment = [];
                    $total_payment += ($this->sma->formatNumberMask($amounts[$id])) + (isset($total_retention) ? $total_retention : 0) + $total_discounts;
                    $payment = array(
                        'date'          => $date,
                        'purchase_id'   => $purchases_id[$id],
                        'reference_no'  => $reference,
                        'amount'        => $this->sma->formatNumberMask($amounts[$id]) + $total_retention,
                        'paid_by'       => $this->input->post('paid_by'),
                        'cheque_no'     => $this->input->post('cheque_no'),
                        'cc_no'         => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                        'cc_holder'     => $this->input->post('pcc_holder'),
                        'cc_month'      => $this->input->post('pcc_month'),
                        'cc_year'       => $this->input->post('pcc_year'),
                        'cc_type'       => $this->input->post('pcc_type'),
                        'note'          => $this->input->post('payment_note')." ".$txt_deposit." ".$assumed_retentions_text,
                        'payment_date'          => $payment_date,
                        'consecutive_payment'   => $consecutive_payment,
                        'cost_center_id'  => $cost_center,
                        'rete_fuente_total' => $rete_fuente_total[$id],
                        'rete_iva_total'    => $rete_iva_total[$id] > 0 ? $rete_iva_total[$id] : 0,
                        'rete_ica_total'    => $rete_ica_total[$id] > 0 ? $rete_ica_total[$id] : 0,
                        'rete_bomberil_total'    => $rete_bomberil_total[$id] > 0 ? $rete_bomberil_total[$id] : 0,
                        'rete_autoaviso_total'    => $rete_autoaviso_total[$id] > 0 ? $rete_autoaviso_total[$id] : 0,
                        'rete_other_total'  => $rete_other_total[$id] > 0 ? $rete_other_total[$id] : 0,
                        'rete_fuente_percentage' => $rete_fuente_percentage[$id],
                        'rete_fuente_base' => $rete_fuente_base[$id],
                        'rete_fuente_account' => $rete_fuente_account[$id],
                        'rete_iva_percentage' => $rete_iva_percentage[$id],
                        'rete_iva_base' => $rete_iva_base[$id],
                        'rete_iva_account' => $rete_iva_account[$id],
                        'rete_ica_percentage' => $rete_ica_percentage[$id],
                        'rete_ica_base' => $rete_ica_base[$id],
                        'rete_ica_account' => $rete_ica_account[$id],
                        'rete_bomberil_percentage' => $rete_bomberil_percentage[$id],
                        'rete_bomberil_base' => $rete_bomberil_base[$id],
                        'rete_bomberil_account' => $rete_bomberil_account[$id],
                        'rete_autoaviso_percentage' => $rete_autoaviso_percentage[$id],
                        'rete_autoaviso_base' => $rete_autoaviso_base[$id],
                        'rete_autoaviso_account' => $rete_autoaviso_account[$id],
                        'rete_other_percentage' => $rete_other_percentage[$id],
                        'rete_other_base' => $rete_other_base[$id],
                        'rete_other_account' => $rete_other_account[$id],
                        'rete_fuente_id' => $rete_fuente_id[$id],
                        'rete_iva_id' => $rete_iva_id[$id],
                        'rete_ica_id' => $rete_ica_id[$id],
                        'rete_bomberil_id' => $rete_bomberil_id[$id],
                        'rete_autoaviso_id' => $rete_autoaviso_id[$id],
                        'rete_other_id' => $rete_other_id[$id],
                        'rete_fuente_assumed' => $rete_fuente_assumed[$id] == 'true' ? 1 : 0,
                        'rete_iva_assumed' => $rete_iva_assumed[$id] == 'true' ? 1 : 0,
                        'rete_ica_assumed' => $rete_ica_assumed[$id] == 'true' ? 1 : 0,
                        'rete_other_assumed' => $rete_other_assumed[$id] == 'true' ? 1 : 0,
                        'rete_fuente_assumed_account' => $rete_fuente_assumed[$id] == 'true' ? $rete_fuente_assumed_account[$id] : NULL,
                        'rete_iva_assumed_account' => $rete_iva_assumed[$id] == 'true' ? $rete_iva_assumed_account[$id] : NULL,
                        'rete_ica_assumed_account' => $rete_ica_assumed[$id] == 'true' ? $rete_ica_assumed_account[$id] : NULL,
                        'rete_bomberil_assumed_account' => $rete_ica_assumed[$id] == 'true' ? $rete_bomberil_assumed_account[$id] : NULL,
                        'rete_autoaviso_assumed_account' => $rete_ica_assumed[$id] == 'true' ? $rete_autoaviso_assumed_account[$id] : NULL,
                        'rete_other_assumed_account' => $rete_other_assumed[$id] == 'true' ? $rete_other_assumed_account[$id] : NULL,
                        'multi_payment' => 1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'type'          => $type,
                        'document_type_id' => $document_type_id,
                    );
                    $payments[] = $payment;
                }
            }

            $type_mov = $this->input->post('type_mov');
            $concepto_description = $this->input->post('concepto_description');
            $concepto_amount = $this->input->post('concepto_amount');
            $ledger = $this->input->post('ledger');
            $cost_center_id_concepto = $this->input->post('cost_center_id_concepto');
            $concepto_supplier = $this->input->post('concepto_supplier');
            $concepto_base = $this->input->post('concepto_base');
            $conceptos = [];
            if ($type_mov && count($type_mov) > 0) {
                foreach ($type_mov as $id => $tm) {
                    $concept_cost_center = $cost_center;
                    $concepto = array(
                        'date'          => $date,
                        'purchase_id'   => NULL,
                        'reference_no'  => $reference,
                        'amount'        => $tm == "+" ? $concepto_amount[$id] : $concepto_amount[$id] * -1,
                        'paid_by'       => $this->input->post('paid_by'),
                        'cheque_no'     => $this->input->post('cheque_no'),
                        'cc_no'         => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                        'cc_holder'     => $this->input->post('pcc_holder'),
                        'cc_month'      => $this->input->post('pcc_month'),
                        'cc_year'       => $this->input->post('pcc_year'),
                        'cc_type'       => $this->input->post('pcc_type'),
                        'note'          => $concepto_description[$id],
                        'cost_center_id'  => $concept_cost_center,
                        'multi_payment' => 1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'type'          => $type,
                        'ledger_id'     => $ledger[$id],
                        'document_type_id' => $document_type_id,
                        'concept_company_id'    => $concepto_supplier[$id] ? $concepto_supplier[$id] : $this->input->post('supplier'),
                        'concept_base'    => $concepto_base[$id],
                    );
                    $conceptos[] = $concepto;
                    $total_payment += ($tm == "+" ? $concepto_amount[$id] : $concepto_amount[$id] * -1);
                }
            }
            // exit(var_dump($total_payment));
            if ((!$rows_selected && !$type_mov) || (($type_mov || $rows_selected) && $total_payment == 0)) {
                $this->session->set_flashdata('error', 'Debe diligenciar al menos una compra a pagar o un concepto para el comprobante de egreso');
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payments[0]['attachment'] = $photo;
            }
            if ($this->input->post('paid_by') == 'deposit') {
                if (!$this->site->check_customer_deposit($this->input->post('supplier'), ($total_payment - $total_discounts))) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            // $this->sma->print_arrays($payments, $conceptos, $retenciones);
        }
        if ($this->form_validation->run() == true && $this->purchases_model->updatePurchasesPayments($this->input->post('paid_by'), $payments, $supplier, $retenciones, $conceptos, $cost_center, $deposit, $manual_reference, $biller_id)) {
            // exit('A');
            $this->session->set_flashdata('message', lang('multi_payment_added'));
            $this->session->set_userdata('remove_multipayments', 1);
            admin_redirect('payments/multi_payment_view/'.$reference);
        } elseif ($this->input->post('add_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('payments/pindex');
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['payment_ref'] = $this->site->getReference('ppay');
            if ($this->Settings->modulary) {
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            if ($purchase_id) {
                if ($purchase) {
                    $this->data['purchase_id'] = $purchase_id;
                    $this->data['purchase'] = $purchase;
                }
            }
            $this->data['collection_discounts'] = $this->site->get_collection_discounts();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_multi_payment_purchases')));
            $meta = array('page_title' => lang('add_multi_payment_purchases'), 'bc' => $bc);
            $this->page_construct('payments/padd', $meta, $this->data);
        }
    }

    public function multi_payment_view($reference){
        $enc = $this->site->getMultiPaymentEnc($reference) ? $this->site->getMultiPaymentEnc($reference) : $this->site->getMultiPaymentEncOnlyConcepts($reference);
        $pmnts = $this->site->getDetailMultiPayment($reference);
        $contabilidad = $this->site->getPaymentAccountingNote($enc);
        $this->data['encabezado'] = $enc[0];
        $this->data['pmnts'] = $pmnts;
        $this->data['contabilidad'] = $contabilidad;
        $this->data['biller'] = $this->site->getCompanyByID($enc[0]->biller_id);
        $this->data['customer'] = $this->site->getCompanyByID($enc[0]->customer_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($enc[0]->document_type_id);
        $this->data['for_email'] = false;
        $this->data['created_user'] = $this->site->getUserById($this->data['encabezado']->created_by);
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $document_type_invoice_format = $this->site->getInvoiceFormatById($this->data['document_type']->module_invoice_format_id);
        $url = 'payments/multi_payment_view';
        $this->data['biller_logo'] = 2;
        if ($document_type_invoice_format) {
            $url = $document_type_invoice_format->format_url;
            $this->data['biller_logo'] = $document_type_invoice_format->logo;
        }
        $this->load_view($this->theme .$url, $this->data);
    }

    public function add_invoice_note($id = null)
    {
        $this->sma->checkPermissions('add', true);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['invoce_notes'] = $this->site->get_invoice_notes(14);
        $this->load_view($this->theme . 'payments/add_invoice_note', $this->data);
    }

    public function payment_actions(){

        $this->form_validation->set_rules('val[]', lang("commisions"), 'required');
        if ($this->form_validation->run() == true) {
            $form_action = $this->input->post('form_action');
            if ($form_action == 'post_multi_payments_1' || $form_action == 'post_multi_payments' || $form_action == 'post_multi_payments_2') {
                $payments = $this->input->post('val');
                // exit(var_dump($form_action));
                foreach ($payments as $key => $reference_no) {
                    if ($form_action == 'post_multi_payments_1') {
                        $this->sales_model->recontabilizarPago($reference_no);
                    } else if ($form_action == 'post_multi_payments') {
                        $this->purchases_model->recontabilizar_pagos_multiples($reference_no); //GASTOS
                    } else if ($form_action == 'post_multi_payments_2') {
                        $this->purchases_model->recontabilizar_pago($reference_no); //COMPRAS
                    }
                }
                $this->session->set_flashdata('message', lang('multi_payments_posted'));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }
            elseif ($form_action == 'export_excel') {
                $this->load->library('excel');
                $headerStyle = [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ],
                    'font' => [
                        'size' => 12,
                        'color' => ['rgb' => '000000'],
                    ],
                ];

                $secondHeaderStyle = [
                    'alignment' => [
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
                    ],
                    'borders' => [
                        'allborders' => [
                            'style' => PHPExcel_Style_Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'font' => [
                        "bold" => true,
                        'color' => ['rgb' => '000000'],
                    ],
                ];
                $currencyFormat = '$#,##0.00';
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->mergeCells('A1:J1')->setCellValue("A1", lang('multi_payments'). " - ".$this->Settings->site_name);
                $this->excel->getActiveSheet()->getStyle("A1")->applyFromArray($headerStyle);
                $this->excel->getActiveSheet()->SetCellValue('A2', lang('reference_no'))->getStyle("A2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('B2', lang('date'))->getStyle("B2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('C2', lang('payment_date'))->getStyle("C2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('D2', lang('affects_to'))->getStyle("D2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('E2', lang('consecutive_payment'))->getStyle("E2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('F2', lang('customer_name'))->getStyle("F2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('G2', lang('paid_by'))->getStyle("G2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('H2', lang('paid'))->getStyle("H2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('I2', lang('applied'))->getStyle("I2")->getFont()->setBold(true);
                $this->excel->getActiveSheet()->SetCellValue('J2', lang('payment_status'))->getStyle("J2")->getFont()->setBold(true);

                $row = 3;
                foreach ($_POST['val'] as $reference_no) {
                    $paymentData = $this->site->getMultiPayments_to_excel($reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $paymentData->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $this->sma->hrld($paymentData->date));
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $paymentData->payment_date);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $paymentData->return_reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $paymentData->consecutive_payment);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $paymentData->customer);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $paymentData->paid_by);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $paymentData->amount)->getStyle('H'.$row)->getNumberFormat()->setFormatCode($currencyFormat);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, $paymentData->amount_applied)->getStyle('I'.$row)->getNumberFormat()->setFormatCode($currencyFormat);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, lang($paymentData->type));
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(23);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(23);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = lang('multi_payments') . " - ".$this->Settings->site_name ;
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            else {
                $sales = $this->input->post('val');
                $start_date = $this->input->post('start_date_action');
                $end_date = $this->input->post('end_date_action');
                $company_id = $this->input->post('company_id_action');
                $total_pay = $this->input->post('total_pay');
                $txt_desc = 'Pago de comisiones para las ventas ';
                $comm_total_sale_paid = [];
                $to_complete_payments_ids = [];
                foreach ($sales as $key => $payments_id) {
                    $payments_ids = explode(",", $payments_id);
                    foreach ($payments_ids as $key2 => $payment_id) {
                        if (empty($payment_id)) {
                            continue;
                        }
                        $pm_data = $this->sales_model->getPaymentByID($payment_id);
                        $sale_id = $pm_data->sale_id;
                        $sale = $this->sales_model->getInvoiceByID($sale_id);
                        if (!isset($comm_total_sale_paid[$sale_id])) {
                            $comm_total_sale_paid[$sale_id]['amount'] = $pm_data->comm_amount;
                            $comm_total_sale_paid[$sale_id]['payments_id'] = $payment_id;
                            $txt_desc .= $sale->reference_no.", ";
                            if ($this->Settings->commision_payment_method == 1 && $this->sales_model->sale_has_paid_collection_commmissions($sale_id)) {
                                $this->session->set_flashdata('error', lang('commission_selection_has_already_paid'));
                                admin_redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $comm_total_sale_paid[$sale_id]['amount'] += $pm_data->comm_amount;
                            $comm_total_sale_paid[$sale_id]['payments_id'] .= ", ".$payment_id;
                        }
                        $to_complete_payments_ids[] = $payment_id;
                    }
                }
                $txt_desc .= 'desde '.$this->sma->fld($start_date).' hasta '.$this->sma->fld($end_date);
                $txt_desc .= ', con un total de '.$this->sma->formatMoney($total_pay);
                if ($total_pay > 0) {
                    if ($this->Settings->modulary) {
                        $this->data['ledgers'] = $this->site->getAllLedgers();
                    }
                    $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
                    if ($this->Settings->cost_center_selection == 1) {
                        $this->data['cost_centers'] = $this->site->getAllCostCenters();
                    }
                    $this->data['tax_rates'] = $this->site->getAllTaxRates();
                    $this->data['payment_sales'] = $sales;
                    $this->data['payment_start_date'] = $start_date;
                    $this->data['payment_end_date'] = $end_date;
                    $this->data['payment_total'] = $total_pay;
                    $this->data['payment_company_id'] = $company_id;
                    $this->data['payment_txt_desc'] = $txt_desc;
                    $row = $this->purchases_model->getExpenseCommision();
                    if ($row) {
                        $c = uniqid(mt_rand(), true);
                        $option = false;
                        $options = FALSE;
                        $row->tax_method = "1";
                        $opt = json_decode('{}');
                        $opt->cost = 0;
                        $option_id = FALSE;
                        $row->option = $option_id;
                        $row->supplier_part_no = '';
                        $row->real_unit_cost = $row->cost;
                        $row->base_quantity = 1;
                        $row->base_unit = 1;
                        $row->base_unit_cost = $row->cost;
                        $row->unit = 1;
                        $row->new_entry = 1;
                        $row->expiry = '';
                        $row->qty = 1;
                        $row->type = 'manual';
                        $row->quantity_balance = '';
                        $row->discount = '0';
                        unset($row->details, $row->product_details, $row->price, $row->file, $row->supplier1price, $row->supplier2price, $row->supplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);

                        $units = $this->site->getUnitsByBUID($row->base_unit);
                        $row->expense_tax_rate_id = $row->tax_rate;
                        $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        $tax_rate_2 = $this->site->getTaxRateByID($row->tax_rate_2);
                        $category_expense = $this->db->where('code', $row->code)->get('expense_categories')->row();
                        $total_to_pay = 0;
                        foreach ($comm_total_sale_paid as $sale_id => $comm_payment_arr) {
                            $sale = $this->sales_model->getInvoiceByID($sale_id);
                            $pr[$sale_id] = array(
                                            'id' => sha1($c),
                                            'item_id' => $row->id,
                                            'payments_ids' => $comm_payment_arr['payments_id'],
                                            'label' =>  $sale->reference_no." ".$row->name . " (" . $row->code . ")",
                                            'row' => $row,
                                            'tax_rate' => $tax_rate,
                                            'tax_rate_2' => $tax_rate_2,
                                            'units' => $units,
                                            'options' => $options,
                                            'amount_to_paid' => $comm_payment_arr['amount'],
                                            'net_cost' => $comm_payment_arr['amount'],
                                            'expense_selected' => true,
                                            'activated_manually' => true,
                                            'sale_reference_no' => $sale->reference_no,
                                        );
                            $total_to_pay += $comm_payment_arr['amount'];
                        }
                        $this->data['expense_commision'] = json_encode($pr);
                        $this->data['to_complete_payments_ids'] = $to_complete_payments_ids;
                        $this->data['paying_commission'] = true;
                    } else {
                        $this->session->set_flashdata('error', lang('expense_category_for_commission_no_setted'));
                        admin_redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_multi_payment_expenses')));
                    $meta = array('page_title' => lang('add_multi_payment_expenses'), 'bc' => $bc);
                    $this->page_construct('payments/exadd', $meta, $this->data);
                } else {
                    $this->session->set_flashdata('error', lang('total_commisions_less_than_0'));
                    admin_redirect($_SERVER["HTTP_REFERER"]);
                }
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

    }

    public function exadd($expense_id = null){
        if (!$this->Owner && !$this->Admin) {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register');
            }
        }
        $this->form_validation->set_rules('add_payment', lang('expenses'), 'required');
        $this->form_validation->set_rules('document_type_id', lang('reference_no'), 'required');
        $this->form_validation->set_rules('supplier', lang('supplier'), 'required');
        $this->form_validation->set_rules('row_selected[]', lang('expenses'), 'required');
        $this->form_validation->set_rules('date', lang('date'), 'required');
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        // $this->sma->checkPermissions();
        if ($this->form_validation->run() == true) {
            // $this->sma->print_arrays($_POST);
            $biller_id = $this->input->post('biller', true);
            $paying_commission = $this->input->post('to_complete_payments_ids') ? true : false;
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect($_SERVER["HTTP_REFERER"]);
            }
            $cost_center = NULL;
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $cost_center = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $cost_center = $this->input->post('cost_center_id');
            }
            $rows_selected = $this->input->post('row_selected');
            $expenses_id = $this->input->post('expense_id');
            $payments_ids_comm_paid = $this->input->post('payments_ids_comm_paid');
            $sale_reference_no = $this->input->post('sale_reference_no');
            $amounts = $this->input->post('amount');
            $supplier = $this->input->post('supplier');
            $consecutive_payment = $this->input->post('consecutive_payment');
            $tax_rate = $this->input->post('tax_rate_id');
            $tax_rate_2 = $this->input->post('tax_rate_2_id');
            $tax_val = $this->input->post('tax_val');
            $tax_val_2 = $this->input->post('tax_val_2 =');
            $rete_fuente_id = $this->input->post('expense_id_rete_fuente');
            $rete_iva_id = $this->input->post('expense_id_rete_iva');
            $rete_ica_id = $this->input->post('expense_id_rete_ica');
            $rete_other_id = $this->input->post('expense_id_rete_other');
            $rete_fuente_total = $this->input->post('expense_rete_fuente_total');
            $rete_iva_total = $this->input->post('expense_rete_iva_total');
            $rete_ica_total = $this->input->post('expense_rete_ica_total');
            $rete_other_total = $this->input->post('expense_rete_other_total');
            $rete_fuente_percentage = $this->input->post('expense_rete_fuente_percentage');
            $rete_iva_percentage = $this->input->post('expense_rete_iva_percentage');
            $rete_ica_percentage = $this->input->post('expense_rete_ica_percentage');
            $rete_other_percentage = $this->input->post('expense_rete_other_percentage');
            $rete_fuente_base = $this->input->post('expense_rete_fuente_base');
            $rete_iva_base = $this->input->post('expense_rete_iva_base');
            $rete_ica_base = $this->input->post('expense_rete_ica_base');
            $rete_other_base = $this->input->post('expense_rete_other_base');
            $rete_fuente_account = $this->input->post('expense_rete_fuente_account');
            $rete_iva_account = $this->input->post('expense_rete_iva_account');
            $rete_ica_account = $this->input->post('expense_rete_ica_account');
            $rete_other_account = $this->input->post('expense_rete_other_account');
            $payments = [];
            $retenciones = [];
            $expenses = [];
            $document_type_id = $this->input->post('document_type_id');
            $referenceBiller = $this->site->getReferenceBiller($biller_id, $document_type_id);
            if($referenceBiller){
                $reference = $referenceBiller;
            } else {
                $reference = $this->site->getReference('ppay');
            }
            $payment_affects_register = $this->input->post('payment_affects_register');
            $type = 'sent';
            if ($payment_affects_register == 0) {
                $type = 'sent_2';
            }
            $date = $this->input->post('date')." ".date("H:i:s");
            $payment_date = $this->input->post('payment_date') ? $this->sma->fld($this->input->post('payment_date')) : $date;
            $this->site->validate_movement_date($date);
            foreach ($rows_selected as $id => $val) {
                $payment = [];
                $payment = array(
                    'date'          => $date,
                    'expense_id'   => $expenses_id[$id],
                    'biller_id'   => $biller_id,
                    'reference_no'  => $reference,
                    'amount'        => $this->sma->formatNumberMask($amounts[$id]) - (isset($expense_discount[$expenses_id[$id]]) ? $expense_discount[$expenses_id[$id]] : 0),
                    'paid_by'       => $this->input->post('paid_by'),
                    'cheque_no'     => $this->input->post('cheque_no'),
                    'cc_no'         => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                    'cc_holder'     => $this->input->post('pcc_holder'),
                    'cc_month'      => $this->input->post('pcc_month'),
                    'cc_year'       => $this->input->post('pcc_year'),
                    'cc_type'       => $this->input->post('pcc_type'),
                    'note'          => $this->input->post('payment_note'),
                    'payment_date'          => $payment_date,
                    'consecutive_payment'   => $consecutive_payment,
                    'cost_center_id'  => $cost_center,
                    'rete_fuente_total' => $rete_fuente_total[$id],
                    'rete_iva_total'    => $rete_iva_total[$id],
                    'rete_ica_total'    => $rete_ica_total[$id],
                    'rete_other_total'  => $rete_other_total[$id],
                    'rete_fuente_percentage' => $rete_fuente_percentage[$id],
                    'rete_fuente_base' => $rete_fuente_base[$id],
                    'rete_fuente_account' => $rete_fuente_account[$id],
                    'rete_iva_percentage' => $rete_iva_percentage[$id],
                    'rete_iva_base' => $rete_iva_base[$id],
                    'rete_iva_account' => $rete_iva_account[$id],
                    'rete_ica_percentage' => $rete_ica_percentage[$id],
                    'rete_ica_base' => $rete_ica_base[$id],
                    'rete_ica_account' => $rete_ica_account[$id],
                    'rete_other_percentage' => $rete_other_percentage[$id],
                    'rete_other_base' => $rete_other_base[$id],
                    'rete_other_account' => $rete_other_account[$id],
                    'rete_fuente_id' => $rete_fuente_id[$id],
                    'rete_iva_id' => $rete_iva_id[$id],
                    'rete_ica_id' => $rete_ica_id[$id],
                    'rete_other_id' => $rete_other_id[$id],
                    'multi_payment' => 1,
                    'created_by'    => $this->session->userdata('user_id'),
                    'type'          => $type,
                    'document_type_id' => $document_type_id,
                    'seller_id' => $supplier,
                    'comm_paid_payments_ids'   => $payments_ids_comm_paid[$id],
                    'comm_paid_sale_reference'   => $sale_reference_no[$id],
                );
                $payments[] = $payment;
                if (isset($expenses[$payment['expense_id']])) {
                    $expenses[$payment['expense_id']]['amount'] += $payment['amount'];
                    $expenses[$payment['expense_id']]['tax_val'] += $tax_val[$id];
                    $expenses[$payment['expense_id']]['tax_val_2'] += $tax_val_2[$id];
                } else {
                    $expenses[$payment['expense_id']] = [
                                'date' => $payment['date'],
                                'reference' => $payment['reference_no'],
                                'amount' => $payment['amount'],
                                'note' => $payment['note'],
                                'created_by' => $payment['created_by'],
                                'category_id' => $payment['expense_id'],
                                'cost_center_id' => $payment['cost_center_id'],
                                // 'payment_id' => $payment['reference_no'],
                                'tax_rate_id' => $tax_rate[$id],
                                'tax_val' => $tax_val[$id],
                                'tax_rate_2_id' => $tax_rate_2[$id],
                                'tax_val_2' => $tax_val_2[$id],
                                'supplier_id' => $supplier
                                ];
                }

                if ($rete_fuente_total[$id] > 0) {
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_fuente_total'] = $rete_fuente_total[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_fuente_percentage'] = $rete_fuente_percentage[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_fuente_base'] = $rete_fuente_base[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_fuente_account'] = $rete_fuente_account[$id];
                }
                if ($rete_iva_total[$id] > 0) {
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_iva_total'] = $rete_iva_total[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_iva_percentage'] = $rete_iva_percentage[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_iva_base'] = $rete_iva_base[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_iva_account'] = $rete_iva_account[$id];
                }
                if ($rete_ica_total[$id] > 0) {
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_ica_total'] = $rete_ica_total[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_ica_percentage'] = $rete_ica_percentage[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_ica_base'] = $rete_ica_base[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_ica_account'] = $rete_ica_account[$id];
                }
                if ($rete_other_total[$id] > 0) {
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_other_total'] = $rete_other_total[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_other_percentage'] = $rete_other_percentage[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_other_base'] = $rete_other_base[$id];
                    $retenciones[$paying_commission ? $id : $expenses_id[$id]]['rete_other_account'] = $rete_other_account[$id];
                }
            }

            $type_mov = $this->input->post('type_mov') ? $this->input->post('type_mov') : [];
            $concepto_description = $this->input->post('concepto_description');
            $concepto_amount = $this->input->post('concepto_amount');
            $ledger = $this->input->post('ledger');
            $cost_center_id_concepto = $this->input->post('cost_center_id_concepto');
            $biller_concepto = $this->input->post('biller_concepto');
            $conceptos = [];
            if (count($type_mov) > 0) {
                foreach ($type_mov as $id => $tm) {
                    $concept_cost_center = $cost_center;
                    $concepto = array(
                        'date'          => $this->sma->fld(trim(date('d/m/Y H:i'))),
                        'expense_id'   => NULL,
                        'reference_no'  => $reference,
                        'amount'        => $tm == "+" ? $concepto_amount[$id] : $concepto_amount[$id] * -1,
                        'paid_by'       => $this->input->post('paid_by'),
                        'cheque_no'     => $this->input->post('cheque_no'),
                        'cc_no'         => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                        'cc_holder'     => $this->input->post('pcc_holder'),
                        'cc_month'      => $this->input->post('pcc_month'),
                        'cc_year'       => $this->input->post('pcc_year'),
                        'cc_type'       => $this->input->post('pcc_type'),
                        'note'          => $concepto_description[$id],
                        'cost_center_id'  => $concept_cost_center,
                        'multi_payment' => 1,
                        'created_by'    => $this->session->userdata('user_id'),
                        'type'          => $type,
                        'ledger_id'     => $ledger[$id],
                    );
                    $conceptos[] = $concepto;
                }
            }
            // $this->sma->print_arrays($this->input->post('to_complete_payments_ids'));
            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payments[0]['attachment'] = $photo;
            }
        }
        if ($this->form_validation->run() == true && $this->purchases_model->updateExpensesPayments($payments, $supplier, $retenciones, $conceptos, $cost_center, $expenses)) {
            if ($this->input->post('to_complete_payments_ids')) {
                $this->sales_model->update_payment_status_commisions($this->input->post('to_complete_payments_ids'), $this->input->post('payment_start_date'), $this->input->post('payment_end_date'), $reference);
            }
            $this->session->set_flashdata('message', lang('multi_payment_added'));
            $this->session->set_userdata('remove_multipayments', 1);
            admin_redirect('payments/multi_payment_view/'.$reference);
        } elseif ($this->input->post('add_biller')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('payments/pindex');
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            if ($this->Settings->modulary) {
                $this->data['ledgers'] = $this->site->getAllLedgers();
            }
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            if ($expense_id) {
                $expense = $this->site->getExpenseCategory($expense_id);
                if ($expense) {
                    $this->data['expense_id'] = $expense_id;
                    $this->data['expense'] = $expense;
                }
            }
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('add_multi_payment_expenses')));
            $meta = array('page_title' => lang('add_multi_payment_expenses'), 'bc' => $bc);
            $this->page_construct('payments/exadd', $meta, $this->data);
        }
    }

    public function exindex(){
        $this->sma->checkPermissions();
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('multi_payment_expenses')));
        $meta = array('page_title' => lang('multi_payment_expenses'), 'bc' => $bc);
        $this->page_construct('payments/exindex', $meta, $this->data);
    }

    public function cancel($reference){
        if (!$this->Owner && !$this->Admin) {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register');
            }
        }
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('date', lang('date'), 'required');
        $this->form_validation->set_rules('biller', lang('biller'), 'required');
        $this->form_validation->set_rules('document_type_id', lang('reference_no'), 'required');
        if ($this->form_validation->run() == true) {
            $date = $this->sma->fld($this->input->post('date'));
            $data = [
                        'date' => $date,
                        'biller' => $this->input->post('biller'),
                        'document_type_id' => $this->input->post('document_type_id'),
                        'customer_id' => $this->input->post('customer_id'),
                        'cost_center_id' => $this->input->post('cost_center_id'),
                        'reference_no' => $this->input->post('reference_no'),
                        'id' => $this->input->post('id'),
                        'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    ];
            if ($this->Settings->cashier_close != 2) {
                if (($this->input->post('paid_by') == "cash" && $this->input->post('amount-paid') > 0) && !$this->pos_model->getRegisterState(($this->input->post('amount-paid')))) {
                    $this->session->set_flashdata('error', "El dinero en caja no es suficiente para completar la devolución.");
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
        }
        if ($this->form_validation->run() == true && $response = $this->sales_model->delete_payment($reference, $data)) {
            $result = false;
            if ($result = $response['response']) {
                if ($response['reaccount'] && $response['reaccount_type'] == 2) {
                    $this->sales_model->recontabilizarVenta($response['reaccount']);
                } else {
                    $this->session->set_flashdata('message', lang('payment_cancelled'));
                }
            } else {
                $this->session->set_flashdata('error', $response['message']);
            }
            admin_redirect('payments/index');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $enc = $this->site->getMultiPaymentEnc($reference) ? $this->site->getMultiPaymentEnc($reference) : $this->site->getMultiPaymentEncOnlyConcepts($reference);
        $pmnts = $this->site->getDetailMultiPayment($reference);
        $this->data['encabezado'] = $enc[0];
        $this->data['pmnts'] = $pmnts;
        $this->data['reference'] = $reference;
        $this->data['biller'] = $this->site->getCompanyByID($enc[0]->biller_id);
        $this->data['customer'] = $this->site->getCompanyByID($enc[0]->customer_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($enc[0]->document_type_id);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        foreach ($pmnts as $pmnt) {
            if ($pmnt->sale_id) {
                $last_return = $this->site->get_last_return_date_sale($pmnt->sale_id);
                if ($last_return && $last_return->date > $enc[0]->date) {
                    $this->session->set_flashdata('error', lang('payment_with_sale_returned'));
                    admin_redirect('payments/index');
                }
            }
        }
        if ($enc[0]->return_id) {
            $this->session->set_flashdata('error', lang('payment_already_cancelled'));
            admin_redirect('payments/index');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('cancel_payment')));
        $meta = array('page_title' => lang('cancel_payment'), 'bc' => $bc);
        $this->page_construct('payments/cancel', $meta, $this->data);
    }

    public function pcancel($reference){
        if (!$this->Owner && !$this->Admin) {
            if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register');
            }
        }
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('biller', lang('biller'), 'required');
        $this->form_validation->set_rules('document_type_id', lang('reference_no'), 'required');
        if ($this->form_validation->run() == true) {
            $data = [
                        'biller' => $this->input->post('biller'),
                        'document_type_id' => $this->input->post('document_type_id'),
                        'supplier_id' => $this->input->post('supplier_id'),
                        'cost_center_id' => $this->input->post('cost_center_id'),
                        'reference_no' => $this->input->post('reference_no'),
                        'id' => $this->input->post('id'),
                        'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    ];
        }
        if ($this->form_validation->run() == true && $response = $this->purchases_model->delete_payment($reference, $data)) {
            $result = false;
            if ($result = $response['response']) {
                if ($response['reaccount'] && $response['reaccount_type'] == 2) {
                    $this->purchase_model->recontabilizarCompra($response['reaccount']);
                }
            }
            $this->session->set_flashdata('message', lang('ppayment_cancelled'));
            admin_redirect('payments/pindex');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $enc = $this->site->getMultiPaymentEnc($reference) ? $this->site->getMultiPaymentEnc($reference) : $this->site->getMultiPaymentEncOnlyConcepts($reference);
        $pmnts = $this->site->getDetailMultiPayment($reference);
        $this->data['encabezado'] = $enc[0];
        $this->data['pmnts'] = $pmnts;
        $this->data['reference'] = $reference;
        $this->data['biller'] = $this->site->getCompanyByID($enc[0]->biller_id);
        $this->data['supplier'] = $this->site->getCompanyByID($enc[0]->supplier_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($enc[0]->document_type_id);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        foreach ($pmnts as $pmnt) {
            if ($pmnt->purchase_id) {
                $last_return = $this->site->get_last_return_date_purchase($pmnt->purchase_id);
                if ($last_return && $last_return->date > $enc[0]->date) {
                    $this->session->set_flashdata('error', lang('payment_with_purchase_returned'));
                    admin_redirect('payments/pindex');
                }
            }
        }
        if ($enc[0]->return_id) {
            $this->session->set_flashdata('error', lang('payment_already_cancelled'));
            admin_redirect('payments/pindex');
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('cancel_ppayment')));
        $meta = array('page_title' => lang('cancel_ppayment'), 'bc' => $bc);
        $this->page_construct('payments/pcancel', $meta, $this->data);
    }

    public function validate_payment_reference(){
        $prefix = $this->input->get('prefix');
        $consecutive = $this->input->get('consecutive');
        $q = $this->db->get_where('payments', ['reference_no' => $prefix."-".$consecutive]);
        if ($q->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

    public function pedit($reference){
        $enc = $this->site->getMultiPaymentEnc($reference) ? $this->site->getMultiPaymentEnc($reference) : $this->site->getMultiPaymentEncOnlyConcepts($reference);
        $pmnts = $this->site->getDetailMultiPayment($reference);
        // $this->sma->print_arrays($enc[0]);
        $this->data['enc'] = $enc[0];
        $this->data['pmnts'] = $pmnts;
        $this->data['biller'] = $this->site->getCompanyByID($enc[0]->biller_id);
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('edit_multi_payment_purchases')));
        $meta = array('page_title' => lang('edit_multi_payment_purchases'), 'bc' => $bc);
        $this->page_construct('payments/pedit', $meta, $this->data);
    }

}