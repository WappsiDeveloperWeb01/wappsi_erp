<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * última edición: 22/03/2015
 * Ricardo Ramírez R.
 * Adan Rivera - 12/08/2016
 */

$lang['add_supplier']                           = "Agregar Proveedor";
$lang['edit_supplier']                          = "Editar Proveedor";
$lang['delete_supplier']                        = "Eliminar proveedor";
$lang['delete_suppliers']                       = "Eliminar Proveedores";
$lang['supplier_added']                         = "El proveedor se ha añadido correctamente";
$lang['supplier_updated']                       = "El proveedor se ha actualizado correctamente";
$lang['supplier_deleted']                       = "El proveedor se ha eliminado correctamente";
$lang['suppliers_deleted']                      = "Los proveedores se han eliminado correctamente";
$lang['import_by_csv']                          = "Agregar Proveedores por CSV";
$lang['edit_profile']                           = "Editar usuario";
$lang['delete_user']                            = "Eliminar usuario";
$lang['no_supplier_selected']                   = "Ningún proveedor seleccionado. Por favor seleccione al menos un proveedor.";
$lang['pw_not_same']                            = "La contraseña y la confirmación de la contraseña no son iguales";
$lang['user_added']                             = "Usuario-Proveedor se ha añadido correctamente";
$lang['user_deleted']                           = "Usuario-Proveedor se ha eliminado correctamente";

$lang['supplier_x_deleted_have_purchases']      = "La acción no pudo ser completada, el proveedor tiene compras";
$lang['suppliers_x_deleted_have_purchases']     = "Algunos proveedores no pueden ser eliminados ya que poseen compras";
$lang['check_supplier_email']                   = "Por favor verifique el correo de proveedor";
$lang['supplier_already_exist']                 = "Existe un proveedor con este correo";
$lang['line_no']                                = "Número de linea";
$lang['first_6_required']                       = "Las primeras 6 columnas son requeridas";
$lang['deactivate_supplier']                    = "Inactivar proveedor";
$lang['activate_supplier']                      = "Activar proveedor";
$lang['supplier_x_deleted'] = 'El proveedor no pudo ser desactivado';
$lang['supplier_deleted'] = 'Proveedor desactivado';
$lang['supplier_x_deleted_have_sales'] = 'El proveedor no puede ser desactivado, tiene compras registradas';
$lang['supplier_x_activated'] = 'El proveedor no pudo ser activado';
$lang['supplier_activated'] = 'El proveedor fue activado';
$lang['supplier_x_activated_have_sales'] = 'El proveedor no puede ser activado, tiene compras registradas';