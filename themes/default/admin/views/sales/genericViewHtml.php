<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= lang('sale_no') . " $inv->id" ?></title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" 
        href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" 
        integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" 
        crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" 
        href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap-theme.min.css" 
        integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" 
        crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js" 
        integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" 
        crossorigin="anonymous"></script>

    <style>
        .bolder {
            font-weight: bolder
        }
        .border {
            border: 1px solid red
        }
        .margin-bottom {
            margin-bottom: 10px
        }
        .vertical-align-middle {
            vertical-align: middle
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row margin-bottom">
                    <div class="col-xs-3">
                        <img class="img-responsive" style="margin-top: 20%;" 
                            src="<?= base_url("assets/uploads/logos/$biller->logo") ?>">
                    </div>

                    <div class="col-xs-5 text-center">
                        <h4  style="margin-top: 10%;">
                            <strong>
                                <?= strtoupper($biller->name != '-' ? $biller->name : $biller->company); ?>
                                <br>
                                <?= $this->Settings->numero_documento.($this->Settings->digito_verificacion > 0 ? '-'.$this->Settings->digito_verificacion : '') ?>
                            </strong>
                        </h4>
                        <?= $biller->address ?> <br> 
                        <?= "$biller->city $biller->state"?> <br>
                        <?= $biller->phone ?> <br> 
                        <?= $biller->email ?>
                    </div>

                    <div class="col-xs-4">
                        <div class="row">
                            <div class="col-xs-6">
                                <h4 style="margin-top: 20%; font-weight: bolder">
                                    <?= $document_type->nombre ?> 
                                    <br>
                                    <br>
                                    <?= $inv->reference_no; ?>
                                </h4>
                            </div>
                                
                            <div class="col-xs-6">
                                <img class="img-responsive"
                                    src="<?= base_url("themes/default/admin/assets/images/qr_code/".$inv->reference_no.".png") ?>">
                            </div>
                        </div>
                    </div>  
                </div>

                <div class="row">
                    <div class="col-xs-6">
                        <div class="well well-sm  margin-bottom">
                            <strong>
                                <?= $customer->name ? $customer->name : $customer->company; ?>
                            </strong><br>
                            <?= $customer->address ?> <br> 
                            <?= "$customer->city $customer->state $customer->country" ?> <br>
                            <?= $customer->vat_no.($customer->digito_verificacion > 0 ? '-'.$customer->digito_verificacion : '') ?><br>
                            <?= $customer->phone ?> <br>
                            <?= $customer->email ?>
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="well well-sm  margin-bottom">
                            <strong>
                                <?= lang("seller") ?>: <?= $seller->name ?>
                                <br>
                                <?= lang("date") ?>: <?= $this->sma->hrld($inv->date); ?>
                                <br>
                                <?= lang("sale_status") ?>: <?= lang($inv->sale_status) ?>
                                <br>
                                <?= lang("payment_status") ?>: <?= lang($inv->payment_status); ?>
                                <br>
                                <?= lang("Forma de Pago") ?>: <?= lang($originalPaymentMethod); ?>
                                <br>
                                <?= lang("created_by"); ?>: <?= $inv->created_by ? $created_by->first_name . ' ' . $created_by->last_name : $customer->name; ?>
                            </strong>
                        </div>
                    </div>
                </div>
                        
                <div class="row">
                    <div class="col-xs-12">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover print-table order-table">
                                <thead>
                                    <tr>
                                        <th class="text-center"><?= lang("description") ?></th>
                                        <th class="text-center" width="45px"><?= lang("IVA") ?></th>
                                        <th class="text-center" width="118px"><?= lang("unit_price") ?></th>
                                        <th class="text-center" width="77px"><?= lang("quantity") ?></th>
                                        <th class="text-center" width="65px"><?= lang("UM") ?></th>
                                        <th class="text-center" width="110px"><?= lang("subtotal"); ?></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($rows as $row): ?>
                                        <tr>
                                            <td class="vertical-align-middle">
                                                <?= $row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                            </td>
                                            <td class="text-center">
                                                <small>
                                                    <?= $row->tax_code ?>
                                                </small>
                                            </td>
                                            <td class="text-right">
                                                <?= $this->sma->formatValue($value_decimals, $row->real_unit_price); ?>
                                            </td>
                                            <td class="text-center">
                                                <?= $this->sma->formatQuantity($row->quantity); ?>
                                            </td>
                                            <td class="text-center">
                                                <small>
                                                    <?= $row->product_unit_code ?>
                                                </small>
                                            </td>
                                            <td class="text-right">
                                                <?= $this->sma->formatValue($value_decimals, $row->subtotal); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
        
                                    <?php if ($return_rows) {
                                        echo '<tr class="warning"><td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td></tr>';
                                        foreach ($return_rows as $row):
                                            ?>
                                            <tr class="warning">
                                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                                <td style="vertical-align:middle;">
                                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                                </td>
                                                <?php if ($Settings->indian_gst) { ?>
                                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                                <?php } ?>
                                                <td style="width: 100px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                                <?php
                                                if ($Settings->product_serial) {
                                                    echo '<td>' . $row->serial_no . '</td>';
                                                }
                                                ?>
                                                <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatValue($value_decimals, $row->unit_price); ?></td>
                                                <?php
                                                if ($Settings->tax1 && $inv->product_tax > 0) {
                                                    echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatValue($value_decimals, $row->item_tax) . '</td>';
                                                }
                                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                                    echo '<td style="width: 120px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatValue($value_decimals, $row->item_discount) . '</td>';
                                                }
                                                ?>
                                                <td style="text-align:right; width:120px; padding-right:10px;"><?= $this->sma->formatValue($value_decimals, $row->subtotal); ?></td>
                                            </tr>
                                            <?php
                                            $r++;
                                        endforeach;
                                    }
            
                                    ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="5" class="text-right bolder">
                                            <?= lang("subtotal"); ?>
                                            (<?= $default_currency->code; ?>)
                                        </td>
                                        <td class="text-right bolder">
                                            <?= $this->sma->formatValue($value_decimals, $inv->total); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right bolder">
                                            <?= lang("tax"); ?>
                                            (<?= $default_currency->code; ?>)
                                        </td>
                                        <td class="text-right bolder">
                                            <?= $this->sma->formatValue($value_decimals, $inv->product_tax); ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="5" class="text-right bolder">
                                            <?= lang("total_amount"); ?>
                                            (<?= $default_currency->code; ?>)
                                        </td>
                                        <td class="text-right bolder">
                                            <?= $this->sma->formatValue($value_decimals, $return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total); ?>
                                        </td>
                                    </tr>
                                <?php $total_retencion = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total; ?>
                                <?php if ($total_retencion > 0): ?>
                                    <tr>
                                        <td colspan="5"
                                            style="text-align:right; font-weight:bold;"><?= lang("retention"); ?>
                                            (<?= $default_currency->code; ?>)
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"> - <?= $this->sma->formatValue($value_decimals, $total_retencion); ?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="5"
                                            style="text-align:right; font-weight:bold;"><?= lang("balance"); ?>
                                            (<?= $default_currency->code; ?>)
                                        </td>
                                        <td style="text-align:right; font-weight:bold;"><?= $this->sma->formatValue($value_decimals, ($return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total) - $total_retencion); ?></td>
                                    </tr>
                                <?php endif ?>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>        
        
                <div class="row">
                    <?php if (!empty($document_type->invoice_footer)) { ?>
                        <div class="col-xs-6">
                            <?php if (!empty($document_type->invoice_footer)) { ?>
                                <div class="well well-sm">
                                    <p class="bold"><?= lang("note"); ?>:</p>

                                    <div><?= $this->sma->decode_html($document_type->invoice_footer); ?></div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>

                    <div class="col-xs-6 <?= (!empty($document_type->invoice_footer)) ? "" : "col-xs-offset-3" ?>">
                        <?= $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax), false, null, 0, false, $value_decimals) ?>
                    </div>
                </div>

                <div class="row">
                    <?php if ($inv->resolucion): ?>
                        <div class="col-xs-12">
                            <div class="well well-sm text-center">
                                <?= $inv->resolucion ?>
                                <br>
                                <?= "CUFE: ". (!empty($inv->cufe) ? $inv->cufe : "") ?>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
        
                <?php if ($inv->payment_status != 'paid') { ?>
                    <div id="payment_buttons" class="row text-center padding10 no-print">

                        <?php if ($paypal->active == "1" && $inv->grand_total != "0.00") {
                            if (trim(strtolower($customer->country)) == $biller->country) {
                                $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_my / 100);
                            } else {
                                $paypal_fee = $paypal->fixed_charges + ($inv->grand_total * $paypal->extra_charges_other / 100);
                            }
                            ?>
                            <div class="col-xs-6 text-center">
                                <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                                    <input type="hidden" name="cmd" value="_xclick">
                                    <input type="hidden" name="business" value="<?= $paypal->account_email; ?>">
                                    <input type="hidden" name="item_name" value="<?= $inv->reference_no; ?>">
                                    <input type="hidden" name="item_number" value="<?= $inv->id; ?>">
                                    <input type="hidden" name="image_url"
                                            value="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>">
                                    <input type="hidden" name="amount"
                                            value="<?= ($inv->grand_total - $inv->paid) + $paypal_fee; ?>">
                                    <input type="hidden" name="no_shipping" value="1">
                                    <input type="hidden" name="no_note" value="1">
                                    <input type="hidden" name="currency_code" value="<?= $default_currency->code; ?>">
                                    <input type="hidden" name="bn" value="FC-BuyNow">
                                    <input type="hidden" name="rm" value="2">
                                    <input type="hidden" name="return"
                                            value="<?= admin_url('sales/view/' . $inv->id); ?>">
                                    <input type="hidden" name="cancel_return"
                                            value="<?= admin_url('sales/view/' . $inv->id); ?>">
                                    <input type="hidden" name="notify_url"
                                            value="<?= admin_url('payments/paypalipn'); ?>"/>
                                    <input type="hidden" name="custom"
                                            value="<?= $inv->reference_no . '__' . ($inv->grand_total - $inv->paid) . '__' . $paypal_fee; ?>">
                                    <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block"><i
                                            class="fa fa-money"></i> <?= lang('pay_by_paypal') ?></button>
                                </form>
                            </div>
                        <?php } ?>


                        <?php if ($skrill->active == "1" && $inv->grand_total != "0.00") {
                            if (trim(strtolower($customer->country)) == $biller->country) {
                                $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_my / 100);
                            } else {
                                $skrill_fee = $skrill->fixed_charges + ($inv->grand_total * $skrill->extra_charges_other / 100);
                            }
                            ?>
                            <div class="col-xs-6 text-center">
                                <form action="https://www.moneybookers.com/app/payment.pl" method="post">
                                    <input type="hidden" name="pay_to_email" value="<?= $skrill->account_email; ?>">
                                    <input type="hidden" name="status_url"
                                            value="<?= admin_url('payments/skrillipn'); ?>">
                                    <input type="hidden" name="cancel_url"
                                            value="<?= admin_url('sales/view/' . $inv->id); ?>">
                                    <input type="hidden" name="return_url"
                                            value="<?= admin_url('sales/view/' . $inv->id); ?>">
                                    <input type="hidden" name="language" value="EN">
                                    <input type="hidden" name="ondemand_note" value="<?= $inv->reference_no; ?>">
                                    <input type="hidden" name="merchant_fields" value="item_name,item_number">
                                    <input type="hidden" name="item_name" value="<?= $inv->reference_no; ?>">
                                    <input type="hidden" name="item_number" value="<?= $inv->id; ?>">
                                    <input type="hidden" name="amount"
                                            value="<?= ($inv->grand_total - $inv->paid) + $skrill_fee; ?>">
                                    <input type="hidden" name="currency" value="<?= $default_currency->code; ?>">
                                    <input type="hidden" name="detail1_description" value="<?= $inv->reference_no; ?>">
                                    <input type="hidden" name="detail1_text"
                                            value="Payment for the sale invoice <?= $inv->reference_no . ': ' . $inv->grand_total . '(+ fee: ' . $skrill_fee . ') = ' . $this->sma->formatValue($value_decimals, $inv->grand_total + $skrill_fee); ?>">
                                    <input type="hidden" name="logo_url"
                                            value="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>">
                                    <button type="submit" name="submit" class="btn btn-primary btn-lg btn-block"><i
                                            class="fa fa-money"></i> <?= lang('pay_by_skrill') ?></button>
                                </form>
                            </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</body>
</html>
