<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
.table-hover>tbody>tr>td.danger, .table-hover>tbody>tr>th.danger, .table-hover>tbody>tr.danger>td, .table-hover>tbody>tr>.danger, .table-hover>tbody>tr.danger>th {
    text-decoration: line-through;
}
</style>
<div class="modal-dialog modal-lg no-printable">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('view_payments').' ('.lang('sale').' '.lang('reference').': '.$inv->reference_no.')'; ?></h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table id="CompTable" cellpadding="0" cellspacing="0" border="0"
                       class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="width:30%;"><?= $this->lang->line("date"); ?></th>
                        <th style="width:30%;"><?= $this->lang->line("reference_no"); ?></th>
                        <th style="width:15%;"><?= $this->lang->line("amount"); ?></th>
                        <th style="width:15%;"><?= $this->lang->line("paid_by"); ?></th>
                        <th style="width:10%;" class="no-printable"><?= $this->lang->line("actions"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if (!empty($payments)) {
                        foreach ($payments as $payment) { ?>
                            <?php 
                                $row_class = '';
                                if ($start_date) {
                                    if ($payment->date < $start_date || $payment->date > $end_date) {
                                        $row_class = 'danger';
                                    }
                                }

                             ?>
                            <tr class="row<?= $payment->id ?> <?= $row_class ?>">
                                <td><?= $this->sma->hrld($payment->date); ?></td>
                                <td><?= $payment->reference_no; ?></td>
                                <td><?= $this->sma->formatMoney($payment->amount) . ' ' . (($payment->attachment) ? '<a href="' . admin_url('welcome/download/' . $payment->attachment) . '"><i class="fa fa-chain"></i></a>' : ''); ?></td>
                                <td>
                                <?php
                                    if ($inv->sale_status != 'returned') {
                                        echo $payment->paid_by;
                                    } else {
                                        if ($payment->paid_by == 'due') {
                                            echo lang('return_sale');
                                        } else {
                                            echo $payment->paid_by;
                                        }
                                    }
                                 ?>
                                 </td>
                                <td class="no-printable">
                                    <div class="text-center">
                                        <?php if ($payment->multi_payment == 1): ?>
                                            <a onclick="window.open('<?= admin_url('payments/multi_payment_view/').$payment->reference_no ?>');"><i class="fa fa-file-text-o"></i></a>
                                        <?php else: ?>
                                            <a href="<?= admin_url('sales/payment_note/' . $payment->id) ?>"
                                           data-toggle="modal" data-target="#myModal2"><i class="fa fa-file-text-o"></i></a>
                                        <?php endif ?>
                                        <?php if ($payment->paid_by != 'gift_card') { ?>
                                            <a href="<?= admin_url('sales/email_payment/' . $payment->id) ?>" class="email_payment"><i class="fa fa-envelope"></i></a>
                                        <?php } ?>
                                        <?php if (($this->Owner || $this->Admin) && $inv->return_id == NULL && $payment->paid_by != 'retencion' && $payment->paid_by != 'due' && $payment->amount > 0 && $payment->multi_payment == 1){ ?>
                                            <a href="<?= admin_url('payments/cancel/'.$payment->reference_no) ?>" target="_blank"><i class="fa fa-trash"></i></a>
                                        <?php } ?>
                                    </div>
                                </td>
                            </tr>
                        <?php }
                    } else {
                        echo "<tr><td colspan='5'>" . lang('no_data_available') . "</td></tr>";
                    } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(document).on('click', '.po-delete', function () {
            var id = $(this).attr('id');
            $(this).closest('tr').remove();
        });
        $(document).on('click', '.email_payment', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            $.get(link, function(data) {
                bootbox.alert(data.msg);
            });
            return false;
        });
    });
</script>
