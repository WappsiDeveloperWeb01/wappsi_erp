<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
?>
<script type="text/javascript">
    $(document).ready(function () {
        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<?php
	$arrayReplace = array(0,1,2,3,4,5,6,7,8,9,'.','/','-','_');
	$fech_ini = date('Y-m-d');
	$fech_fin = date("Y-m-d",strtotime($fech_ini."+ 1 days"));
	$prefijo_old = $referencia_old = $prefijo = '';
	$array_prefijo = array();
	$profitabilitys_tipo = (!empty($profitabilitys_tipo)) ? $profitabilitys_tipo: $profitabilitys ;

	foreach ($profitabilitys_tipo as $profitability)
	{
		if($fech_ini>$profitability['date'])
		{
			$fech_ini = $profitability['date'];
		}
		$referencia = $profitability['reference_no'];
		$prefijo = str_replace($arrayReplace,'',$referencia);
		if($prefijo_old!=$prefijo)
		{
			$array_prefijo[$prefijo] = $prefijo;
		}
		$referencia_old = $referencia;
	}
	$fech_ini = substr($fech_ini,0,10);

	$fech_ini = (!empty($datos)) ? $datos['inicial'] : $fech_ini ;
	$fech_fin = (!empty($datos)) ? $datos['Final'] : $fech_fin ;
?>
<br>


<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
					<div class="row">
                        <div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<h3><?php echo lang('filter_date');?> <span id="textini"><?php echo $fech_ini; ?></span> a <span id="textfin"><?php echo $fech_fin; ?></span> </h3>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="panel panel-default">
								<div class="panel-body">
									<?php echo form_open('admin/reports/search_rentabilidad_doc' , 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
									<div class="row">
										<?php
	                                    $bl[""] = "";
	                                    $bldata = [];
	                                    foreach ($billers as $biller) {
	                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
	                                        $bldata[$biller->id] = $biller;
	                                    }
	                                    // exit(var_dump($this->session->userdata('biller_id')));
	                                    ?>

	                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <?= lang("biller", "slbiller"); ?>
	                                                <select name="biller" class="form-control" id="slbiller" required="required">
	                                                    <option value=""><?= lang('select') ?></option>
	                                                    <?php foreach ($billers as $biller) : ?>
	                                                        <option value="<?= $biller->id ?>" <?php echo ((!empty($datos)) AND ($datos['biller']==$biller->id)) ? 'selected' : ''; ?> ><?= $biller->company ?></option>
	                                                    <?php endforeach ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                    <?php } else { ?>
	                                        <div class="col-md-4">
	                                            <div class="form-group">
	                                                <?= lang("biller", "slbiller"); ?>
	                                                <select name="biller" class="form-control" id="slbiller">
	                                                    <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
	                                                        $biller = $bldata[$this->session->userdata('biller_id')];
	                                                    ?>
	                                                        <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
	                                                    <?php endif ?>
	                                                </select>
	                                            </div>
	                                        </div>
	                                    <?php } ?>
										<div class="col-xs-6 col-sm-6 col-md-4">
											<?= lang('document_type', 'tipodoc') ?>
											<select class="form-control " name="tipodoc" id="tipodoc" data-target="Sucursal">
												<option value="all" > <?php echo lang('select'); ?> </option>
												<?php foreach ($array_prefijo as $prefijo): ?>
													<option value="<?php echo $prefijo; ?>" <?php echo ((!empty($datos)) AND ($datos['tipodoc']==$prefijo)) ? 'selected' : ''; ?>><?php echo $prefijo; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
										<div class="clearfix"></div>
	                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
	                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
	                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
	                                            <?= $this->sma->get_filter_options(); ?>
	                                        </select>
	                                    </div>
										<div class="date_controls_dh">

	                                        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
	                                            <div class="col-sm-4 form-group">
	                                                <?= lang('filter_year', 'filter_year_dh') ?>
	                                                <select name="filter_year" id="filter_year_dh" class="form-control" required>
	                                                    <?php foreach ($this->filter_year_options as $key => $value): ?>
	                                                        <option value="<?= $key ?>"><?= $key ?></option>
	                                                    <?php endforeach ?>
	                                                </select>
	                                            </div>
	                                        <?php endif ?>
	                                        <div class="col-sm-4">
	                                            <div class="form-group">
	                                                <?= lang("start_date", "start_date_dh"); ?>
	                                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh"'); ?>
	                                            </div>
	                                        </div>
	                                        <div class="col-sm-4">
	                                            <div class="form-group">
	                                                <?= lang("end_date", "end_date_dh"); ?>
	                                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh"'); ?>
	                                            </div>
	                                        </div>
	                                    </div>
									</div>
									<br>
									<div class="row">
										<div class="col-xs-12 col-sm-6 col-md-3">
											<button type="submit" class="btn btn-success" class="Buscar" id="Buscar" ><?php echo lang('submit');?></button>
										</div>
										<div class="col-xs-12 col-sm-12 col-md-6">
										</div>
										<div class="col-xs-12 col-sm-6 col-md-3">
											<div class="pull-right dropdown">
												<button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
												<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
													<li>
														<a class="ExportPdf" id="ExportPdf"> PDF </a>
													</li>
													<li>
														<a class="ExportXls" id="ExportXls"> Excel </a>
													</li>
												</ul>
											</div>
										</div>
									</div>
									</form>
								</div>
							</div>
						</div>
						<br>
						<?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
							<div class="col-lg-12">
								<div class="table-responsive" >
									<table id="TableData" class="table table-stripped" >
										<thead>
											<tr>
												<td class="text-center"> <b><?php echo lang('date'); ?></b></td>
												<td class="text-center"> <b><?php echo lang('Reference'); ?></b></td>
												<td class="text-center" style="width: 10%;"> <b><?php echo lang('document_affected'); ?></b></td>
												<td class="text-center" style="width: 24%;"> <b><?php echo lang('customer'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('total'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('cost'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('profit'); ?></b></td>
												<td class="text-right" style="width: 11%;"> <b><?php echo lang('margin'); ?></b></td>
											</tr>
										</thead>
										<tbody>
											<?php foreach ($profitabilitys as $profitability): ?>
											<?php
												$referencia = ($profitability['return_sale_ref']!='') ? $profitability['return_sale_ref'] : $profitability['reference_no'] ;
												$afec = ($profitability['return_sale_ref']!='') ? $profitability['reference_no'] : '' ;
											?>
												<tr >
													<td class="text-center"><?php echo substr($profitability['date'],0,10); ?></td>
													<td class="text-center"><?php echo $referencia; ?></td>
													<td ><?php echo $afec; ?></td>
													<td ><?php echo $profitability['customer']; ?></td>
													<td class="text-right"> <?php echo $this->sma->formatMoney($profitability['total']); ?></td>
													<td class="text-right"> <?php echo $this->sma->formatMoney($profitability['costo']); ?></td>
													<td class="text-right"> <?php echo $this->sma->formatMoney($profitability['utilidad']); ?></td>
													<td class="text-right"> <?php echo $this->sma->formatDecimal($profitability['margen']); ?> </td>
												</tr>
											<?php endforeach; ?>
										</tbody>
										<tfoot>
											<tr>
												<th class="text-center"> <b><?php echo lang('date'); ?></b></th>
												<th class="text-center"> <b><?php echo lang('Reference'); ?></b></th>
												<th class="text-center" > <b><?php echo lang('document_affected'); ?></b></th>
												<th class="text-center" > <b><?php echo lang('Cliente'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('total'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('cost'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('profit'); ?></b></th>
												<th class="text-right" > <b><?php echo lang('margin'); ?></b></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script >

	function currencyFormat(x) {
		return x ;
	}

	function Search_Documento() {
		var tipodoc = $("#tipodoc option:selected").val();
		var inicial = $('#start_date').val();
		var Final = $('#end_date').val();
		jQuery.ajax({
			url: 'http://127.0.0.1/index.php/Rentabilidad_documento/'+tipodoc+'/'+inicial+'/'+Final ,
			async: false,
			type: 'GET',
			dataType: 'html',
			success: function(data) {
				$('#TableData').html(data);
			},
			error: function(xhr, textStatus, errorThrown) {
				$('#TableData').html('<div class="alert alert-danger" role="alert"><strong><?php echo lang('toast_error_title');?> </strong><?php echo lang('ajax_error');?></div>');
			}
		});
	}

	$(document).ready(function() {
		$('#tipodoc').select2();
		table = $('#TableData').DataTable({
			"processing": false,
			pageLength: 100,
    		responsive: true,
			"fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var gtotal = 0, paid = 0, balance = 0;
                for (var i = 0; i < aaData.length; i++) {
                    gtotal += parseFloat(aaData[aiDisplay[i]][4]);
                    paid += parseFloat(aaData[aiDisplay[i]][5]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[4].innerHTML = currencyFormat(parseFloat(gtotal));
                nCells[5].innerHTML = currencyFormat(parseFloat(paid));
            },
			oLanguage: <?php echo $dt_lang; ?>
		});

		$("#ExportPdf").click(function() {
			var tipodoc = $("#tipodoc option:selected").val() ? $("#tipodoc option:selected").val() : 'null';
			var inicial = $('#start_date').val() ? $('#start_date').val() : 'null';
			var Final = $('#end_date').val() ? $('#end_date').val() : 'null';
			var biller = $('#biller').val() ? $('#biller').val() : 'null';
			window.open(site.base_url+'reports/profitabilitys_export_pdf/'+tipodoc+'/'+inicial+'/'+Final+'/'+biller);
		});
		$("#ExportXls").click(function() {
			var tipodoc = $("#tipodoc option:selected").val() ? $("#tipodoc option:selected").val() : 'null';
			var inicial = $('#start_date').val() ? $('#start_date').val() : 'null';
			var Final = $('#end_date').val() ? $('#end_date').val() : 'null';
			var biller = $('#biller').val() ? $('#biller').val() : 'null';
			window.open(site.base_url+'reports/profitabilitys_export_xls/'+tipodoc+'/'+inicial+'/'+Final+'/'+biller);
		});
	});

    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
$('#Buscar').on('click', function(e){
    $('#filter_action').val(1);
    $('#form_filter').submit();
});
</script>


</body>
</html>