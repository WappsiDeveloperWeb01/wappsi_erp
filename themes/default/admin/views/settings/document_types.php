<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php
if ($Owner || $Admin || (isset($GP) && $GP['bulk_actions'])) {
    echo admin_form_open('system_settings/document_types_actions', 'id="action-form"');
}
?>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="QUData" class="table table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th></th>
                                            <th><?= lang('name') ?></th>
                                            <th><?= lang('document_type_num_resolucion') ?></th>
                                            <th><?= lang('document_type_sales_prefix') ?></th>
                                            <th><?= lang('document_type_sales_consecutive') ?></th>
                                            <th><?= lang('document_type_inicio_resolucion') ?></th>
                                            <th><?= lang('document_type_fin_resolucion') ?></th>
                                            <th><?= lang('document_factura_electronica') ?></th>
                                            <th><?= lang('actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || (is_array($GP) && $GP['bulk_actions'] )) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }

    $(document).ready(function() {
        oTable = $('#QUData').dataTable({
            "aaSorting": [
                [1, "desc"],
                [2, "desc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('system_settings/getDocumentTypes') ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });

                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex) {},
            aoColumns: [{
                    'bVisible': false
                },
                null,
                {
                    className: 'text-right'
                },
                null,
                {
                    className: 'text-right'
                },
                {
                    className: 'text-right'
                },
                {
                    className: 'text-right'
                },
                {
                    mRender: function(data, type, row) {
                        if (data == 1) {
                            return '<i class="fa fa-check text-success"></i>';
                        } else {
                            return '<i class="fa fa-ban text-danger"></i>';
                        }
                    },
                    className: 'text-center'
                },
                null,
            ],
            fnDrawCallback: function(oSettings) {
                $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/add_document_type') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="left" title="Agregar" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus fa-lg"></i></a>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });
    });
</script>