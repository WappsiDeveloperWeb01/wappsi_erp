<style>
    .table td:nth-child(6) {
        text-align: right;
        width: 10%;
    }

    .table td:nth-child(8) {
        text-align: center;
    }
</style>
<?php if ($Owner) {
    echo admin_form_open('auth/user_actions', 'id="action-form"');
} ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="UsrTable" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th><?php echo lang('image'); ?></th>
                                            <th><?php echo lang('first_name'); ?></th>
                                            <th><?php echo lang('last_name'); ?></th>
                                            <th><?php echo lang('username'); ?></th>
                                            <th><?php echo lang('email_address'); ?></th>
                                            <th><?php echo lang('company'); ?></th>
                                            <th><?php echo lang('award_points'); ?></th>
                                            <th><?php echo lang('group'); ?></th>
                                            <th><?php echo lang('login_status'); ?></th>
                                            <th><?php echo lang('login_hour'); ?></th>
                                            <th><?php echo lang('status'); ?></th>
                                            <th><?php echo lang('actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        'use strict';        

        loadDataTables()
    });

    function loadDataTables() {

        oTable = $('#UsrTable').dataTable({
            aaSorting: [[2, "asc"], [3, "asc"]],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            sAjaxSource: '<?= admin_url('auth/getUsers') ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "optionFilter",
                    "value": option_filter
                });

                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [
                { "bSortable": false, "mRender": checkbox },
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        if (data !== null) {
                            return '<img class="img-circle" src="assets/uploads/avatars/'+ data +'" style="width: 48px; height: 48px;"/>';
                        } else {
                            return '<img class="img-circle" src="assets/uploads/avatars/defaultUser.jpg" style="width: 48px; height: 48px;"/>';
                        }
                    }
                },
                null,
                null,
                null,
                null,
                null,
                null,
                { "mRender": user_group_lang },
                { "mRender": login_status },
                null,
                { "mRender": user_status },
                { 
                    bSortable: false,
                    mRender : function(data, type, row) {
                        console.log(row)
                        let actions = $(data);

                        let status = row[11].split("__");
                        if (status[0] == 0) {
                            actions.find('.dropdown-toggle').attr('disabled', true);
                        }

                        data = actions.prop('outerHTML');
                        return data;
                    }
                }
            ],
            drawCallback: function(settings) {
                getTabDataTables()

                $('.actionsButtonContainer').html(`<?php if ($Owner) { ?>
                    <a href="<?= admin_url('auth/create_user') ?>" class="btn btn-primary new-button pull-right" id="add" data-toggle-second="tooltip" data-placement="top" title="<?= lang("add") ?>">
                        <i class="fas fa-plus fa-lg"></i>
                    </a>
                <?php } ?>
                <div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <?php if ($Owner) { ?>
                            <li>
                                <a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#" class="bpo" 
                                    title="<b><?= $this->lang->line("delete_users") ?></b>" 
                                    data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" 
                                    data-html="true" 
                                    data-placement="left">
                                        <i class="fa fa-trash-o"></i> <?= lang('delete_users') ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                </div>`);
                
                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue',
                    increaseArea: '20%'
                });
            }
        });
    }

    function getTabDataTables() {
        if (option_filter == false) {
            let amount = 0
            let tabHtml = '';
            let first = `first current index_list`
            <?php foreach ($countByProfiles as $profile) { ?>
                tabHtml += `<li role="tab" class="done index_list" aria-disabled="false"  aria-selected="true" style="width:16.6%;">
                    <a class="wizard_index_step" data-optionfilter="<?= $profile->group_id ?>" href="#">
                        <span class="all_span"><?= $profile->count ?></span><br><?= lang($profile->name) ?>
                    </a>
                </li>`

                amount += <?= $profile->count ?>;
            <?php } ?>

            $('.additionalControlsContainer').html(`<div class="wizard">
                <div class="steps clearfix">
                    <ul role="tablist">
                        <li role="tab" class="first current index_list" aria-disabled="false"  aria-selected="true" style="width:16.6%;">
                            <a  class="wizard_index_step" data-optionfilter="" href="#">
                                <span class="all_span">${amount}</span><br><?= lang('allsf') ?>
                            </a>
                        </li>
                        ${tabHtml}
                    </ul>
                </div>
            </div>`).addClass('col-without-padding');
        }
    }
</script>

<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>

    <script language="javascript">
        $(document).ready(function() {
            $('#set_admin').click(function() {
                $('#usr-form-btn').trigger('click');
            });

        });
    </script>
<?php } ?>