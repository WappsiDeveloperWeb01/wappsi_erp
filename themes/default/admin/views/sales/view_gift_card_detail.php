<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header no-print">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('view_gift_card_detail')." ".lang('gift_card')." ".lang('number')." ".$gift_card->card_no; ?></h4>
            <p><?= lang("value")." ".$this->sma->formatMoney($gift_card->value) ?></p>
        </div>
        <div class="modal-body">
            <table class="table">
                <thead>
                    <tr>
                        <th><?= lang('movement_type') ?></th>
                        <th><?= lang('reference_no') ?></th>
                        <th><?= lang('date') ?></th>
                        <th><?= lang('amount') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $balance = $gift_card->value; ?>
                    <?php if ($movements): ?>
                        <?php foreach ($movements as $mvm): ?>
                            <tr>
                                <td class="text-center"><?= $mvm->movement_type.($mvm->movement_type == 'Recarga' ? "  <a class='fa fa-eye' href='".admin_url('sales/view_gift_card_topup/').$mvm->top_up_id."' target='_blank'></a>" : "") ?></td>
                                <td class="text-center"><?= $mvm->reference_no ?></td>
                                <td class="text-center"><?= $mvm->date ?></td>
                                <td class="text-right"><?= $this->sma->formatMoney($mvm->value) ?></td>
                            </tr>
                            <?php $balance += $mvm->value; ?>
                        <?php endforeach ?>
                    <?php endif ?>
                    <tr>
                        <th class="text-right" colspan="3"><?= lang('balance') ?></th>
                        <td class="text-right"><?= $this->sma->formatMoney($balance) ?></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
