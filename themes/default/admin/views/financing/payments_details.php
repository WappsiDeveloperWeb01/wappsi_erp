<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
.table-hover>tbody>tr>td.danger, .table-hover>tbody>tr>th.danger, .table-hover>tbody>tr.danger>td, .table-hover>tbody>tr>.danger, .table-hover>tbody>tr.danger>th {
    text-decoration: line-through;
}
</style>
<div class="modal-dialog modal-lg no-printable">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('detail') . " " . lang("voucher") . ": ". $paymentNo; ?></h4>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th><?= $this->lang->line("credit"); ?></th>
                        <th><?= $this->lang->line("fee"); ?></th>
                        <th><?= $this->lang->line("date"); ?></th>
                        <th><?= $this->lang->line("biller"); ?></th>
                        <th><?= $this->lang->line("paid_by"); ?></th>
                        <th><?= $this->lang->line("capital"); ?></th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if (!empty($payments)) { ?>
                            <?php
                                $currentInterest = $vatCurrentInterest = $defaultInteres = $vatDefaultInteres = 0;
                                $commission = $vatCommission = $interest = $vatInterest = 0;
                            ?>
                            <?php foreach ($payments as $payment) { ?>
                                <tr class="row<?= $payment->credit_no ?>">
                                    <td><?= $payment->credit_no; ?></td>
                                    <td><?= $payment->installment_no; ?></td>
                                    <td><?= $payment->paymentDate; ?></td>
                                    <td><?= $payment->name; ?></td>
                                    <td><?= $payment->paid_by; ?></td>
                                    <td class="text-right"><?= $this->sma->formatMoney($payment->capital_amount); ?></td>
                                </tr>
                                <?php
                                    $currentInterest += $payment->current_interest_amount;
                                    $vatCurrentInterest += $payment->interest_tax;
                                    $defaultInteres += $payment->default_interest;
                                    $vatDefaultInteres += $payment->default_interest_tax;

                                    $commission += $payment->procredito_commission;
                                    $vatCommission += $payment->vat_procredito_commission;
                                    $interest += $payment->procredito_interest;
                                    $vatInterest += $payment->vat_procredito_interest;
                                ?>
                            <?php } ?>
                        <?php } else { ?>
                            <tr>
                                <td colspan='5'><?= lang('no_data_available') ?></td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <hr>
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Tipo Interés</th>
                            <th class="text-center">Base</th>
                            <th class="text-center">IVA</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Mora</td>
                            <td class="text-right"><?= $this->sma->formatMoney($defaultInteres); ?></td>
                            <td class="text-right"><?= $this->sma->formatMoney($vatDefaultInteres); ?></td>
                        </tr>
                        <tr>
                            <td>Corriente</td>
                            <td class="text-right"><?= $this->sma->formatMoney($currentInterest); ?></td>
                            <td class="text-right"><?= $this->sma->formatMoney($vatCurrentInterest); ?></td>
                        </tr>
                    </tbody>
                </table>
                <?php if ($commission > 0 || $interest > 0) { ?>
                    <hr>
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Tipo Procrédtio</th>
                                <th class="text-center">Base</th>
                                <th class="text-center">IVA</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Comisión</td>
                                <td class="text-right"><?= $this->sma->formatMoney($commission); ?></td>
                                <td class="text-right"><?= $this->sma->formatMoney($vatCommission); ?></td>
                            </tr>
                            <tr>
                                <td>Interés</td>
                                <td class="text-right"><?= $this->sma->formatMoney($interest); ?></td>
                                <td class="text-right"><?= $this->sma->formatMoney($vatInterest); ?></td>
                            </tr>
                        </tbody>
                    </table>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $(document).on('click', '.po-delete', function () {
            var id = $(this).attr('id');
            $(this).closest('tr').remove();
        });
        $(document).on('click', '.email_payment', function (e) {
            e.preventDefault();
            var link = $(this).attr('href');
            $.get(link, function(data) {
                bootbox.alert(data.msg);
            });
            return false;
        });
    });
</script>
