<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .box .box-header h2 {
        display: inline-flex;
    }
    table {
        width: 100%;
    }
</style>

<style type="text/css">
     @media print {
            body {
                font-size: 12px; /* Ajusta esta escala según necesites */
                transform: scale(1.5); /* Ajusta la escala */
                transform-origin: top left;
            }
        }
</style>
<div class="wrapper wrapper-content animated fadeInRight">
     <div class="row">
       <div class="col-lg-12">
         <div class="box">
              <div class="box-header">
                <div class="row">
                  <div class="col-md-12">
                       <h2 class="" id="myModalLabel" style="font-weight: 100 !important;"><?= ucwords(mb_strtolower($product->name)); ?></h2>
                  </div>
                </div>
              </div>
              <div class="box-content">
                <div class="row">
                  <div class="col-lg-12">
                       <?php if ($Owner || $Admin || $this->GP['products-view_kardex'] || $this->GP['products-view_graph'] || $this->GP['products-view_sales'] || $this->GP['products-view_quotes'] || $this->GP['products-view_purchases'] || $this->GP['products-view_transfers'] || $this->GP['products-view_adjustments'] ) { ?>
                        <ul id="myTab" class="nav nav-tabs">
                           <li class=""><a href="#details" class="tab-grey" style="color: black;"><?= lang('product_details') ?></a></li>
                           <?php if($product->type == 'standard' || $product->type == 'raw' ||  $product->type == 'pfinished'  ||  $product->type == 'subproduct') { ?>
                              <?php if ($Owner || $Admin || $this->GP['products-view_graph']): ?>
                                 <li class=""><a href="#chart" class="tab-grey" style="color: black;"><?= lang('chart') ?></a></li>
                              <?php endif ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_sales']): ?>
                                 <li class=""><a href="#sales" class="tab-grey" style="color: black;"><?= lang('sales') ?></a></li>
                              <?php endif ?> 
                              <?php if ($Owner || $Admin || $this->GP['products-view_order_sales']): ?>
                                 <li class=""><a href="#order_sales" class="tab-grey" style="color: black;"><?= lang('order_sales') ?></a></li>
                              <?php endif ?> 
                              <?php if ($Owner || $Admin || $this->GP['products-view_quotes']): ?>
                                 <li class=""><a href="#quotes" class="tab-grey" style="color: black;"><?= lang('quotes') ?></a></li>
                              <?php endif ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_purchases']): ?>	
                                 <li class=""><a href="#purchases" class="tab-grey" style="color: black;"><?= lang('purchases') ?></a></li>
                              <?php endif ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_transfers'] ): ?>		
                                 <li class=""><a href="#transfers" class="tab-grey" style="color: black;"><?= lang('transfers') ?></a></li>
                              <?php endif ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_adjustments']): ?>
                                 <li class=""><a href="#damages" class="tab-grey" style="color: black;"><?= lang('quantity_adjustments') ?></a></li>
                              <?php endif ?>
                              <?php if ($this->Admin || $this->Owner || $this->GP['products-view_kardex']): ?>
                                 <li class=""><a href="#kardex" class="tab-grey" style="color: black;"><?= lang('kardex') ?></a></li>
                              <?php endif ?>
                           <?php } ?>
                           <?php if ($product->type == 'service'): ?>
                              <?php if ($Owner || $Admin || $this->GP['products-view_sales']): ?>
                                 <li class=""><a href="#sales" class="tab-grey" style="color: black;"><?= lang('sales') ?></a></li>
                              <?php endif ?> 
                              <?php if ($Owner || $Admin || $this->GP['products-view_quotes']): ?>
                                 <li class=""><a href="#quotes" class="tab-grey" style="color: black;"><?= lang('quotes') ?></a></li>
                              <?php endif ?> 
                           <?php endif ?>
                        </ul>
                     <?php } ?>
                     <?php if ($Owner || $Admin || $this->GP['products-view_kardex'] || $this->GP['products-view_graph'] || $this->GP['products-view_sales'] || $this->GP['products-view_quotes'] || $this->GP['products-view_purchases'] || $this->GP['products-view_transfers'] || $this->GP['products-view_adjustments']) { ?>
                        <div class="tab-content">
                           <div id="details" class="tab-pane fade in">
                     <?php } ?>
                              <div class="box">
                                 <div class="box-header">
                                    <div class="box-icon">
                                       <ul class="btn-tasks">
                                          <li class="dropdown">
                                             <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                <i class="icon fa fa-tasks tip" data-placement="left" title="<?= lang("actions") ?>"></i>
                                             </a>
                                             <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                                <li>
                                                   <a href="<?= admin_url('products/edit/' . $product->id) ?>">
                                                      <i class="fa fa-edit"></i> <?= lang('edit') ?>
                                                   </a>
                                                </li>
                                                <li>
                                                   <a href="<?= admin_url('products/print_barcodes/' . $product->id) ?>">
                                                      <i class="fa fa-print"></i> <?= lang('print_barcode_label') ?>
                                                   </a>
                                                </li>
                                                <li>
                                                   <a href="<?= admin_url('products/pdf/' . $product->id) ?>">
                                                      <i class="fa fa-download"></i> <?= lang('pdf') ?>
                                                   </a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                   <a href="#" class="bpo" title="<b><?= lang("delete_product") ?></b>"
                                                      data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('products/delete/' . $product->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                                                      data-html="true" data-placement="left">
                                                      <i class="fa fa-trash-o"></i> <?= lang('delete') ?>
                                                   </a>
                                                </li>
                                             </ul>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 <div class="box-content">
                                    <div class="row">
                                       <div class="col-lg-12">
                                          <div class="row" id="print_this">
                                             <div class="col-xs-12" class="a">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                   <i class="fa fa-2x">&times;</i>
                                                </button>
                                             </div>
                                             <div class="row">
                                                <div class="col-xs-5">
                                                   <img id="pr-image" src="<?= $this->sma->get_img_url($product->image) ?>"
                                                         alt="<?= $product->name ?>" class="img-responsive img-thumbnail"/>

                                                   <div id="multiimages" class="padding10">
                                                      <?php if (!empty($images)) {
                                                         echo '<a class="img-thumbnail change_img" href="' . $this->sma->get_img_url($product->image) . '" style="margin-right:5px;"><img class="img-responsive" src="' .$this->sma->get_img_url($product->image) . '" alt="' . $product->image . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                         foreach ($images as $ph) {
                                                                            
                                                            echo '<div class="gallery-image"><a class="img-thumbnail change_img" href="' .  $this->sma->get_img_url($ph->photo) . '" style="margin-right:5px;"><img class="img-responsive" src="' . $this->sma->get_img_url($ph->photo) . '" alt="' . $ph->photo . '" style="width:' . $Settings->twidth . 'px; height:' . $Settings->theight . 'px;" /></a>';
                                                            if ($Owner || $Admin || $GP['products-edit']) {
                                                               echo '<a href="#" class="delimg" data-item-id="'.$ph->id.'"><i class="fa fa-times"></i></a>';
                                                            }
                                                            echo '</div>';
                                                         }
                                                      }
                                                      ?>
                                                   </div>
                                                </div>
                                                <div class="col-xs-7">
                                                   <div class="">
                                                      <table class="table table-borderless  dfTable table-right-left">
                                                         <tbody>
                                                            <tr>
                                                               <td><?= lang("type"); ?></td>
                                                               <td><?= lang($product->type); ?></td>
                                                            </tr>
                                                            <tr>
                                                               <td><?= lang("name"); ?></td>
                                                               <td><?= $product->name; ?></td>
                                                            </tr>
                                                            <tr>
                                                               <td><?= lang("product_list_code"); ?></td>
                                                               <td><?= $product->code; ?></td>
                                                            </tr>
                                                            <tr>
                                                               <td><?= lang("reference"); ?></td>
                                                               <td><?= $product->reference; ?></td>
                                                            </tr>
                                                            <tr>
                                                               <td><?= lang("brand"); ?></td>
                                                               <td><?= $brand ? $brand->name : ''; ?></td>
                                                            </tr>
                                                            <tr>
                                                               <td><?= lang("category"); ?></td>
                                                               <td><?= $category->name; ?></td>
                                                            </tr>
                                                            <?php if ($product->subcategory_id) { ?>
                                                               <tr>
                                                                  <td><?= lang("subcategory"); ?></td>
                                                                  <td><?= $subcategory->name; ?></td>
                                                               </tr>
                                                            <?php } ?>
                                                            <?php if ($product->second_level_subcategory_id) { ?>
                                                               <tr>
                                                                  <td><?= lang("second_level_subcategory_id"); ?></td>
                                                                  <td><?= $second_level_subcategory->name; ?></td>
                                                               </tr>
                                                            <?php } ?>
                                                            <tr>
                                                               <td><?= lang("unit"); ?></td>
                                                               <td><?= $unit ? $unit->name.' ('.$unit->code.')' : ''; ?></td>
                                                            </tr>
                                                            <?php 
                                                               $cost_tax = $this->sma->calculate_tax($product->tax_rate, $product->cost, $product->tax_method);
                                                               $visual_cost = $product->tax_method == 0 ? ($product->cost - $cost_tax) : ($product->cost + $cost_tax);
                                                               $visual_cost = $this->sma->formatMoney($this->sma->formatDecimals($visual_cost));
                                                               
                                                               $avg_cost_tax = $this->sma->calculate_tax($product->tax_rate, $product->avg_cost, 0);
                                                               $visual_avg_cost = $product->tax_method == 0 ? ($product->avg_cost - $avg_cost_tax) : ($product->avg_cost);
                                                               $avg_net_cost = ($product->avg_cost - $avg_cost_tax);
                                                               $visual_avg_cost = $this->sma->formatMoney($this->sma->formatDecimals(($visual_avg_cost)));
                                                               
                                                               
                                                               $price_tax = $this->sma->calculate_tax($product->tax_rate, $product->price, $product->tax_method);
                                                               $visual_price = $product->tax_method == 0 ? ($product->price - $price_tax) : ($product->price + $price_tax);
                                                               $visual_price = $this->sma->formatMoney($this->sma->formatDecimals($visual_price));
                                                            ?>
                                                            <?php
                                                                     
                                                               if ($this->Admin || $this->Owner || $this->GP['products-cost']) {
                                                                  echo '<tr>
                                                                           <td>' . lang("cost") . '</td>
                                                                           <td>' . $this->sma->formatMoney($product->cost + $product->consumption_purchase_tax) .($product->tax_method == 0 ? '<i class="fa fa-exclamation-circle tip" data-toggle="tooltip" data-placement="top" title="'.lang('value_include_taxes').'"></i>' : '') . ' ('.$visual_cost.') </td>
                                                                        </tr>';
                                                                  echo '<tr>
                                                                           <td>' . lang("avg_cost") . '</td>
                                                                           <td>' . $this->sma->formatMoney(($product->tax_method == 0 ? $product->avg_cost : $avg_net_cost)) . ($product->tax_method == 0 ? '<i class="fa fa-exclamation-circle tip" data-toggle="tooltip" data-placement="top" title="'.lang('value_include_taxes').'"></i>' : '').' ('.$visual_avg_cost.') </td>
                                                                        </tr>';
                                                               }
                                                               if ($this->Admin || $this->Owner || $this->GP['products-price']) {
                                                                  echo '<tr>
                                                                           <td>' . lang("price") . '</td>
                                                                           <td>' . $this->sma->formatMoney($product->price + $product->consumption_sale_tax) . ($product->tax_method == 0 ? '<i class="fa fa-exclamation-circle tip" data-toggle="tooltip" data-placement="top" title="'.lang('value_include_taxes').'"></i>' : '') . ' ('.$visual_price.')</td>
                                                                        </tr>';
                                                                  if ($product->promotion) {
                                                                        echo '<tr><td>' . lang("promotion") . '</td><td>' . $this->sma->formatMoney($product->promo_price + ($this->Settings->ipoconsumo ? $product->consumption_sale_tax : 0)) . ' ('.$this->sma->hrsd($product->start_date).' - '.$this->sma->hrsd($product->end_date).')</td></tr>';
                                                                  }
                                                               }
                                                               
                                                            ?>

                                                            <?php if ($this->Settings->ipoconsumo): ?>
                                                               <tr>
                                                                  <td><?= lang("second_product_tax").lang('purchases'); ?></td>
                                                                  <td><?= ($tax_rate_2 ? 
                                                                              $tax_rate_2->description." ".($product->purchase_tax_rate_2_percentage ? $product->purchase_tax_rate_2_percentage : "") 
                                                                              : "")
                                                                              ; ?></td>
                                                               </tr>
                                                               <tr>
                                                                  <td><?= lang("second_product_tax").lang('sales'); ?></td>
                                                                  <td><?= ($tax_rate_2 ? 
                                                                              $tax_rate_2->description." ".($product->sale_tax_rate_2_percentage ? $product->sale_tax_rate_2_percentage : "") 
                                                                              : "").
                                                                        ($product->sale_tax_rate_2_milliliters > 0 ? "</br> Gr/Ml ".$this->sma->formatQuantity($product->sale_tax_rate_2_milliliters)." " : "").
                                                                        ($product->sale_tax_rate_2_degrees > 0 ? "</br> Grados ".$this->sma->formatQuantity($product->sale_tax_rate_2_degrees)." " : "").
                                                                        ($product->sale_tax_rate_2_nominal > 0 ? "</br> Tarifa ".$this->sma->formatMoney($product->sale_tax_rate_2_nominal)." " : "")
                                                                              ; ?></td>
                                                               </tr>
                                                               <tr>
                                                                  <td><?= lang("second_product_tax")." ".lang('sales'); ?></td>
                                                                  <td><?= $this->sma->formatMoney($product->consumption_sale_tax); ?></td>
                                                               </tr>

                                                               <tr>
                                                                  <td><?= lang("second_product_tax")." ".lang('purchases'); ?></td>
                                                                  <td><?= $this->sma->formatMoney($product->consumption_purchase_tax); ?></td>
                                                               </tr>
                                                            <?php endif ?>

                                                            <?php if ($product->tax_rate) { ?>
                                                               <tr>
                                                                  <td><?= lang("tax_rate"); ?></td>
                                                                  <td><?= $tax_rate->name; ?></td>
                                                               </tr>
                                                               <tr>
                                                                  <td><?= lang("tax_method"); ?></td>
                                                                  <td><?= $product->tax_method == 0 ? lang('inclusive') : lang('exclusive'); ?></td>
                                                               </tr>
                                                               <?php } ?>
                                                               <?php if ($product->alert_quantity != 0) { ?>
                                                               <tr>
                                                                  <td><?= lang("alert_quantity"); ?></td>
                                                                  <td><?= $this->sma->formatQuantity($product->alert_quantity); ?></td>
                                                               </tr>
                                                               <?php } ?>
                                                               <?php if ($preferences) {
                                                                  $cat_name = NULL;
                                                                  ?>
                                                               <tr>
                                                                  <td><?= lang("product_preferences"); ?></td>
                                                                  <td><?php foreach ($preferences as $preference) {
                                                                     if ($cat_name == NULL || $cat_name != $preference->prf_cat_name) {
                                                                           echo '<p>' . $preference->prf_cat_name . '</p> ';
                                                                     }
                                                                     echo '<span class="label label-success">' . $preference->name . '</span> ';
                                                                     $cat_name = $preference->prf_cat_name;
                                                                  } ?></td>
                                                               </tr>
                                                            <?php } ?>
                                                            <?php if ($this->Admin || $this->Owner || $this->GP['products-cost']) : 
                                                               $profitability_margin = $this->sma->formatDecimals((($product->price - $product->cost) / ($product->price > 0 ? $product->price : 1))*100) . '%';   
                                                            ?>
                                                               <?php if($profitability_margin >= 0): ?>
                                                                  <tr>
                                                                        <td><?= lang("profitability_margin"); ?></td>
                                                                        <td><?= $profitability_margin ?> </td>
                                                                  </tr>
                                                               <?php endif; ?>    
                                                            <?php endif ?>
                                                            <?php if ($custom_fields): ?>
                                                               <?php foreach ($custom_fields as $cf_arr): ?>
                                                                  <?php $cf = $cf_arr['data']; ?>
                                                                  <?php if ($product->{$cf->cf_code}): ?>
                                                                        <tr>
                                                                           <td><?= $cf->cf_name ?></td>
                                                                           <td><?= $product->{$cf->cf_code} ?></td>
                                                                        </tr>
                                                                  <?php endif ?>
                                                               <?php endforeach ?>
                                                            <?php endif ?>
                                                          </tbody>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                             <div class="row">
                                                <!-- warehouses quantitys -->
                                                <div class="col-xs-5">

                                                   <?php if ((!$Supplier || !$Customer) && !empty($warehouses) && ($product->type == 'standard' || $product->type == 'raw' || $product->type == 'subproduct' || $product->type == 'pfinished')) { ?>
                                                      <h3 class="bold"><?= lang('warehouse_quantity') ?></h3>
                                                      <div class="">
                                                         <table class="table table-bordered table-condensed dfTable two-columns">
                                                            <thead>
                                                                  <tr>
                                                                     <th><?= lang('warehouse_name') ?></th>
                                                                     <th><?= lang('quantity') . ' <br> (' . lang('product_location') . ')'; ?></th>
                                                                  </tr>
                                                            </thead>
                                                            <tbody>
                                                                  <?php foreach ($warehouses as $warehouse) {

                                                                     if (!$this->Owner && !$this->Admin && $this->Settings->users_can_view_warehouse_quantitys == 0 && $this->session->userdata('warehouse_id')) {
                                                                        if ($this->session->userdata('warehouse_id') != $warehouse->id) {
                                                                              continue;
                                                                        }
                                                                     }

                                                                     $decimales = 3;

                                                                     // if ($warehouse->quantity != 0) {
                                                                        echo '<tr>
                                                                                 <td>
                                                                                    ' . $warehouse->name . ' (' . $warehouse->code . ')
                                                                                 </td>
                                                                                 <td class="text-center">
                                                                                    <strong>' . $this->sma->formatQuantity($warehouse->quantity, $decimales) . '</strong> <br>' . ($warehouse->warehouse_location ? ' (' . $warehouse->warehouse_location . ')' : '') . '
                                                                                 </td>
                                                                              </tr>';
                                                                     // }
                                                                  } ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   <?php } ?>
                                                </div>
                                                <!-- combo and options -->
                                                <div class="col-xs-7">
                                                   <?php if ($product->type == 'combo' || $product->type == 'subproduct' || $product->type == 'pfinished') { ?>
                                                      <?php
                                                         $decimales = null;
                                                         if ($product->type != 'combo') {
                                                            $decimales = 3;
                                                         }
                                                      ?>
                                                      <h3 class="bold"><?= lang('combo_items') ?></h3>
                                                      <div class="">
                                                         <table
                                                            class="table table-bordered table-condensed dfTable three-columns">
                                                            <thead>
                                                               <tr>
                                                                  <th><?= lang('product_name') ?></th>
                                                                  <th><?= lang('quantity') ?></th>
                                                                  <th><?= lang('cost') ?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody>

                                                               <?php
                                                                  $total_combo_cost = 0;
                                                                  foreach ($combo_items as $combo_item) {
                                                                     echo '<tr>
                                                                              <td>' . $combo_item->name . ' (' . $combo_item->code . ') </td>
                                                                              <td class="text-right">' . $this->sma->formatQuantity($combo_item->qty, $decimales) . ' ' . $combo_item->unit_code. '</td>
                                                                              <td class="text-right">' . $this->sma->formatMoney($combo_item->price * $combo_item->qty) . '</td>
                                                                           </tr>';
                                                                     $total_combo_cost += ($combo_item->price * $combo_item->qty);
                                                                  } ?>
                                                            </tbody>
                                                            <tfoot>
                                                               <th colspan="2"><?= lang('total_cost') ?></th>
                                                               <th class="text-right"><?= $this->sma->formatMoney($total_combo_cost) ?></th>
                                                            </tfoot>
                                                         </table>
                                                      </div>
                                                   <?php } ?>
                                                   <?php if (!empty($options)) { ?>
                                                      <h3 class="bold"><?= lang('product_variants_quantity'); ?></h3>
                                                      <div class="">
                                                         <table class="table table-bordered table-condensed dfTable">
                                                            <thead>
                                                               <tr>
                                                                  <th><?= lang('warehouse_name') ?></th>
                                                                  <th><?= lang('product_variant'); ?></th>
                                                                  <th><?= lang('quantity') . ' (' . lang('rack') . ')'; ?></th>
                                                                  <?php if ($Owner || $Admin) {
                                                                     echo '<th>' . lang('price_addition') . '</th>';
                                                                  } ?>
                                                               </tr>
                                                            </thead>
                                                            <tbody>
                                                               <?php
                                                                  foreach ($options as $option) {
                                                                     if ($option->wh_qty != 0) {
                                                                        echo '<tr><td>' . $option->wh_name . '</td><td>' . $option->name . '</td><td class="text-center">' . $this->sma->formatQuantity($option->wh_qty) . '</td>';
                                                                        if ($Owner || $Admin && (!$Customer || $this->session->userdata('show_cost'))) {
                                                                           echo '<td class="text-right">' . $this->sma->formatMoney($option->price) . '</td>';
                                                                        }
                                                                        echo '</tr>';
                                                                     }

                                                                  }
                                                               ?>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   <?php } ?>
                                                </div>
                                             </div>
                                               <div class="row">
                                                <div class="col-lg-12">
                                                   <div class="ibox float-e-margins border-bottom">
                                                      <div class="ibox-title">
                                                         <h5><?= lang("product_variants"); ?></h5>
                                                         <div class="ibox-tools">
                                                            <a class="collapse-link">
                                                               <i class="fa fa-chevron-down"></i>
                                                            </a>
                                                         </div>
                                                      </div>
                                                      <div class="ibox-content" style="display: none;">
                                                         <?php if ($variants) { ?>
                                                            <div class="row">
                                                               <div class="col-xs-12">
                                                                  <?php foreach ($variants as $variant) {
                                                                     echo '<span class="btn btn-sm btn-outline btn-success" style="margin-bottom:2px !important;">' . $variant->name .' ('.$variant->code.')' . '</span> ';
                                                                  } ?>
                                                               </div>
                                                            </div>
                                                         <?php } ?>
                                                      </div>
                                                   </div>
                                                </div>
                                               </div>

                                               <div class="row">
                                                <div class="col-xs-6">
                                                   <?php if ($this->Settings->prioridad_precios_producto == 11): ?>
                                                      <table class="table">
                                                         <thead>
                                                            <tr>
                                                               <th><?= lang('unit') ?></th>
                                                               <?php foreach ($price_groups as $key => $pg): ?>
                                                                  <th><?= $pg->name ?></th>
                                                               <?php endforeach ?>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php foreach ($units as $unit): ?>
                                                               <?php
                                                                  if (!isset($hybrid_prices[$product->id][$unit->id])) {
                                                                        continue;
                                                                  }
                                                               ?>
                                                               <tr>
                                                                  <td><?= $unit->name ?></td>
                                                                  <?php foreach ($price_groups as $key => $pg): ?>
                                                                     <td>
                                                                        <?= (isset($hybrid_prices[$product->id][$unit->id][$pg->id]) ? $hybrid_prices[$product->id][$unit->id][$pg->id] : 0) + ($this->Settings->ipoconsumo && $product->tax_method == 0 ? $product->consumption_sale_tax : 0) ?>
                                                                     </td>
                                                                  <?php endforeach ?>
                                                               </tr>
                                                            <?php endforeach ?>
                                                         </tbody>
                                                      </table>
                                                   <?php else: ?>
                                                      <?php if ($product_price_groups): ?>
                                                         <h3><?= lang('price_groups') ?></h3>
                                                         <table class="table">
                                                            <thead>
                                                               <tr>
                                                                  <th><?= lang('price_group_name') ?></th>
                                                                  <th><?= lang('price_group_price') ?></th>
                                                               </tr>
                                                            </thead>
                                                            <tbody>
                                                               <?php foreach ($product_price_groups as $price_group): ?>
                                                                  <tr>
                                                                     <th><?= $price_group->name ?></th>
                                                                     <td class="text-right"><?= $this->sma->formatMoney($price_group->price + ($this->Settings->ipoconsumo && $product->tax_method == 0 ? $product->consumption_sale_tax : 0)) ?></td>
                                                                  </tr>
                                                               <?php endforeach ?>
                                                            </tbody>
                                                         </table>
                                                      <?php else: ?>
                                                         <h3><?= lang('product_without_price_group_relation') ?></h3>
                                                      <?php endif ?>
                                                   <?php endif ?>
                                                </div>
                                                <?php if (isset($product_unit_prices) && $product_unit_prices): ?>
                                                   <div class="col-xs-6">
                                                      <h3><?= lang('unit_prices') ?></h3>
                                                      <table class="table">
                                                         <thead>
                                                            <tr>
                                                               <th><?= lang('product_unit_name') ?></th>
                                                               <th><?= lang('product_unit_price') ?></th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <?php
                                                               $main_product_unit_price['valor_unitario'] = $product->price;
                                                               $main_product_unit_price['operation_value'] = 1;
                                                               $main_product_unit_price['name'] = $unit ? $unit->name : '';
                                                               array_unshift($product_unit_prices, (Object) $main_product_unit_price);
                                                               foreach ($product_unit_prices as $key => $unit_price) {
                                                                  $ordenar_units[$key] = $unit_price->operation_value;
                                                               }
                                                            ?>
                                                            <?php foreach ($product_unit_prices as $unit_price): ?>
                                                               <tr>
                                                                  <th><?= $unit_price->name ?></th>
                                                                  <td class="text-right"><?= $this->sma->formatMoney($unit_price->valor_unitario + ($this->Settings->ipoconsumo && $product->tax_method == 0 ? $product->consumption_sale_tax : 0)) ?></td>
                                                               </tr>
                                                            <?php endforeach ?>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                <?php endif ?>
                                               </div>
                                             <?php if (count($billers_selected)): ?>
                                                <div class="col-xs-12">
                                                   <h3><?= lang('product_exclusive_billers') ?></h3>
                                                   <p>
                                                      <?php foreach ($billers_selected as $bskey => $bs): ?>
                                                         <?= ($bskey > 0 ? ", " : "").$bs->company ?>
                                                      <?php endforeach ?>
                                                   </p>
                                                </div>
                                               <?php endif ?>
                                             <div class="col-xs-12">

                                                <?= $product->details ? '<div class="panel panel-success"><div class="panel-heading">' . lang('product_details_for_invoice') . '</div><div class="panel-body">' . $product->details . '</div></div>' : ''; ?>
                                                <?= $product->product_details ? '<div class="panel panel-primary"><div class="panel-heading">' . lang('product_details') . '</div><div class="panel-body">' . $product->product_details . '</div></div>' : ''; ?>
                                             </div>
                                          </div>
                                          <?php if (!$Supplier || !$Customer) { ?>
                                             <div class="row no-print">
                                                <div class="col-xs-3">
                                                   <a href="<?= admin_url('products/print_barcodes/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('print_barcode_label') ?>" style="width:100%;">
                                                      <i class="fa fa-print"></i>
                                                      <span class="hidden-sm hidden-xs"><?= lang('print_barcode_label') ?></span>
                                                   </a>
                                                </div>
                                                <div class="col-xs-3">
                                                   <a href="<?= admin_url('products/pdf/' . $product->id) ?>" class="tip btn btn-primary" title="<?= lang('pdf') ?>" style="width:100%;">
                                                      <i class="fa fa-download"></i>
                                                      <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                                                   </a>
                                                </div>
                                                <div class="col-xs-3">
                                                   <a href="<?= admin_url('products/edit/' . $product->id) ?>" class="tip btn btn-primary tip" title="<?= lang('edit_product') ?>" style="width:100%;">
                                                      <i class="fa fa-edit"></i>
                                                      <span class="hidden-sm hidden-xs"><?= lang('edit') ?></span>
                                                   </a>
                                                </div>
                                                <div class="col-xs-3">
                                                   <a class="tip btn btn-primary tip" onclick="printDiv('print_this');" style="width:100%;">
                                                      <i class="fa fa-print"></i>
                                                      <span class="hidden-sm hidden-xs"><?= lang('print') ?></span>
                                                   </a>
                                                </div>
                                                <!-- <div class="col-sm-4">
                                                   <a href="#" class="tip btn btn-danger bpo" title="<b><?= lang("delete_product") ?></b>"
                                                      data-content="<div style='width:150px;'><p><?= lang('r_u_sure') ?></p><a class='btn btn-danger' href='<?= admin_url('products/delete/' . $product->id) ?>'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button></div>"
                                                      data-html="true" data-placement="top">
                                                      <i class="fa fa-trash-o"></i>
                                                      <span class="hidden-sm hidden-xs"><?= lang('delete') ?></span>
                                                   </a>
                                                </div> -->
                                             </div>
                                             <script type="text/javascript">
                                             $(document).ready(function () {
                                                $('.tip').tooltip();
                                             });
                                             </script>
                                          <?php } ?>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <?php if ($Owner || $Admin || $this->GP['products-view_kardex'] || $this->GP['products-view_graph'] || $this->GP['products-view_sales'] || $this->GP['products-view_quotes'] || $this->GP['products-view_purchases'] || $this->GP['products-view_transfers'] || $this->GP['products-view_adjustments']) { ?>
                              <?php if ($Owner || $Admin || $this->GP['products-view_graph'] ) { ?>
                                 <div id="chart" class="tab-pane fade">
                                    <script src="<?= $assets; ?>js/hc/highcharts.js"></script>
                                    <script type="text/javascript">
                                       $(function () {
                                          Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function (color) {
                                             return {
                                                radialGradient: {cx: 0.5, cy: 0.3, r: 0.7},
                                                stops: [[0, color], [1, Highcharts.Color(color).brighten(-0.3).get('rgb')]]
                                             };
                                          });
                                          <?php if($sold) { ?>
                                             var sold_chart = new Highcharts.Chart({
                                                chart: {
                                                   renderTo: 'soldchart',
                                                   type: 'line',
                                                   width: <?= $purchased ? "($('#details').width()-160)/2" : "$('#details').width()-100"; ?>
                                                },
                                                credits: {enabled: false},
                                                title: {text: ''},
                                                xAxis: {
                                                   categories: [<?php
                                                      foreach ($sold as $r) {
                                                         $month = explode('-', $r->month);
                                                         echo "'".lang('cal_'.strtolower($month[1]))." ".$month[0]."', ";
                                                      }
                                                   ?>]
                                                },
                                                yAxis: {min: 0, title: ""},
                                                legend: {enabled: false},
                                                tooltip: {
                                                   shared: true,
                                                   followPointer: true,
                                                   formatter: function () {
                                                      var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;min-width:150px;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table"  style="margin-bottom:0;">';
                                                      $.each(this.points, function () {
                                                         if (this.series.name == '<?= lang("amount"); ?>') {
                                                            s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                                            currencyFormat(this.y) + '</b></td></tr>';
                                                         } else {
                                                            s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                                            formatQuantity(this.y) + '</b></td></tr>';
                                                         }
                                                      });
                                                      s += '</table></div>';
                                                      return s;
                                                   },
                                                   useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                                                   style: {fontSize: '14px', padding: '0', color: '#000000'}
                                                },
                                                series: [{
                                                   type: 'spline',
                                                   name: '<?= lang("sold"); ?>',
                                                   data: [<?php
                                                   foreach ($sold as $r) {
                                                      $month = explode('-', $r->month);
                                                      echo "['".lang('cal_'.strtolower($month[1]))." ".$month[0]."', ".$r->sold."],";
                                                      // echo "['".lang('cal_'.strtolower($r->month))."', ".$r->sold."],";
                                                   }
                                                   ?>]
                                                }, {
                                                   type: 'spline',
                                                   name: '<?= lang("amount"); ?>',
                                                   data: [<?php
                                                   foreach ($sold as $r) {
                                                         $month = explode('-', $r->month);
                                                         echo "['".lang('cal_'.strtolower($month[1]))." ".$month[0]."', ".$r->amount."],";
                                                         // echo "['".lang('cal_'.strtolower($r->month))."', ".$r->amount."],";
                                                   }
                                                   ?>]
                                                }]
                                             });
                                             $(window).resize(function () {
                                                sold_chart.setSize($('#soldchart').width(), 450);
                                             });
                                          <?php } if($purchased) { ?>
                                             var purchased_chart = new Highcharts.Chart({
                                                chart: {renderTo: 'purchasedchart', type: 'line', width: ($('#details').width() - 160) / 2},
                                                credits: {enabled: false},
                                                title: {text: ''},
                                                xAxis: {
                                                   categories: [<?php
                                                      foreach ($purchased as $r) {
                                                         $month = explode('-', $r->month);
                                                         echo "'".lang('cal_'.strtolower($month[1]))." ".$month[0]."', ";
                                                      }
                                                   ?>]
                                                },
                                                yAxis: {min: 0, title: ""},
                                                legend: {enabled: false},
                                                tooltip: {
                                                   shared: true,
                                                   followPointer: true,
                                                   formatter: function () {
                                                      var s = '<div class="well well-sm hc-tip" style="margin-bottom:0;min-width:150px;"><h2 style="margin-top:0;">' + this.x + '</h2><table class="table"  style="margin-bottom:0;">';
                                                      $.each(this.points, function () {
                                                         if (this.series.name == '<?= lang("amount"); ?>') {
                                                            s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                                            currencyFormat(this.y) + '</b></td></tr>';
                                                         } else {
                                                            s += '<tr><td style="color:{series.color};padding:0">' + this.series.name + ': </td><td style="color:{series.color};padding:0;text-align:right;"> <b>' +
                                                            formatQuantity(this.y) + '</b></td></tr>';
                                                         }
                                                      });
                                                      s += '</table></div>';
                                                      return s;
                                                   },
                                                   useHTML: true, borderWidth: 0, shadow: false, valueDecimals: site.settings.decimals,
                                                   style: {fontSize: '14px', padding: '0', color: '#000000'}
                                                },
                                                series: [{
                                                   type: 'spline',
                                                   name: '<?= lang("purchased"); ?>',
                                                   data: [<?php
                                                         foreach ($purchased as $r) {
                                                                        $month = explode('-', $r->month);
                                                                        echo "['".lang('cal_'.strtolower($month[1]))." ".$month[0]."', ".$r->purchased."],";
                                                                        // echo "['".lang('cal_'.strtolower($r->month))."', ".$r->purchased."],";
                                                                  }
                                                   ?>]
                                                }, {
                                                   type: 'spline',
                                                   name: '<?= lang("amount"); ?>',
                                                   data: [<?php
                                                      foreach ($purchased as $r) {
                                                         $month = explode('-', $r->month);
                                                         echo "['".lang('cal_'.strtolower($month[1]))." ".$month[0]."', ".$r->amount."],";
                                                         // echo "['".lang('cal_'.strtolower($r->month))."', ".$r->amount."],";
                                                      }
                                                   ?>]
                                                }]
                                             });
                                             $(window).resize(function () {
                                                purchased_chart.setSize($('#purchasedchart').width(), 450);
                                             });
                                          <?php } ?>
                                       });
                                    </script>
                                    <div class="box">
                                       <div class="box-content">
                                          <div class="row">
                                             <div class="col-md-12">
                                                <div class="row" style="margin-bottom: 15px;">
                                                   <div class="col-sm-<?= $purchased ? '6' : '12'; ?>">
                                                      <div class="box" style="border-top: 1px solid #dbdee0;">

                                                         <div class="box-content">
                                                            <div class="row">
                                                               <div class="col-md-12">
                                                                  <div id="soldchart" style="width:100%; height:450px;"></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <?php if ($purchased) { ?>
                                                      <div class="col-sm-6">
                                                         <div class="box" style="border-top: 1px solid #dbdee0;">
                                                               <div class="box-content">
                                                                  <div class="row">
                                                                     <div class="col-md-12">
                                                                           <div id="purchasedchart" style="width:100%; height:450px;"></div>
                                                                     </div>
                                                                  </div>
                                                               </div>
                                                         </div>
                                                      </div>
                                                   <?php } ?>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php } ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_sales']) { ?>
                                 <div id="sales" class="tab-pane fade">
                                    <?php $warehouse_id = NULL; ?>
                                    <script type="text/javascript">
                                       var sTable;
                                       $(document).ready(function () {
                                          sTable = $('#SlRData').dataTable({
                                             "aaSorting": [[0, "desc"]],
                                             "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                                             "iDisplayLength": <?= $Settings->rows_per_page ?>,
                                             'bProcessing': true, 'bServerSide': true,
                                             'sAjaxSource': '<?= admin_url('products/getSalesReport/?v=1&product='.$product->id) ?>',
                                             'fnServerData': function (sSource, aoData, fnCallback) {
                                                aoData.push({
                                                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                                                            "value": "<?= $this->security->get_csrf_hash() ?>"
                                                            },{
                                                               "name": "start_date",
                                                               "value": $('#sales_start_date').val()
                                                            },{
                                                               "name": "end_date",
                                                               "value": $('#sales_end_date').val()
                                                            });
                                                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                                             },
                                             'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                                                nRow.id = aData[12];
                                                nRow.className = (aData[5] > 0) ? "invoice_link2" : "invoice_link2 warning";
                                                return nRow;
                                             },
                                             "aoColumns": [
                                                {"mRender": fld},
                                                null,
                                                null,
                                                null,
                                                null,
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": pay_status},
                                                {"mRender": row_status}
                                             ],
                                             "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                                var gtotal = 0, paid = 0, balance = 0, qty = 0;
                                                for (var i = 0; i < aaData.length; i++) {
                                                      qty += parseFloat(aaData[aiDisplay[i]][5]);
                                                      gtotal += parseFloat(aaData[aiDisplay[i]][6]);
                                                      paid += parseFloat(aaData[aiDisplay[i]][7]);
                                                      balance += parseFloat(aaData[aiDisplay[i]][8]);
                                                }
                                                // console.log(nRow);
                                                var nCells = nRow.getElementsByTagName('th');
                                                nCells[5].innerHTML = currencyFormat(parseFloat(qty));
                                                nCells[6].innerHTML = currencyFormat(parseFloat(gtotal));
                                                nCells[7].innerHTML = currencyFormat(parseFloat(paid));
                                                nCells[8].innerHTML = currencyFormat(parseFloat(balance));
                                             }
                                          }).fnSetFilteringDelay().dtFilter([
                                             {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                             {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                                             {column_number: 2, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                                             {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                                             {column_number: 8, filter_default_label: "[<?=lang('payment_status');?>]", filter_type: "text", data: []},
                                          ], "footer");
                                       });

                                       $(document).on('click', '#sale_filter', function(){
                                          set_filter_to_all($('#sales_date_records_filter').val(), $('#sales_start_date').val(), $('#sales_end_date').val());
                                       });

                                    </script>
                                    <div class="box">
                                       <div class="box-header">
                                          <div class="box-icon">
                                             <ul class="btn-tasks">
                                                <li class="dropdown">
                                                   <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                                      <i class="icon fa fa-file-excel-o"></i>
                                                   </a>
                                                </li>
                                                <li class="dropdown">
                                                   <a href="#" id="image" class="tip image" title="<?= lang('save_image') ?>">
                                                      <i class="icon fa fa-file-picture-o"></i>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <div class="box-content">
                                          <div class="row">
                                             <div class="col-lg-12">
                                                <div class="ibox float-e-margins border-bottom">
                                                   <div class="ibox-title">
                                                      <h5><?= lang('filter') ?></h5>
                                                      <div class="ibox-tools">
                                                         <a class="collapse-link">
                                                            <i class="fa fa-chevron-down"></i>
                                                         </a>
                                                      </div>
                                                   </div>
                                                   <div class="ibox-content" style="display: none;">
                                                      <div class="row">
                                                         <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                            <?= lang('date_records_filter', 'date_records_filter') ?>
                                                            <select name="sales_date_records_filter" id="sales_date_records_filter" class="form-control">
                                                               <?= $this->sma->get_filter_options(); ?>
                                                            </select>
                                                         </div>

                                                         <div class="sales_date_controls">
                                                            <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                               <div class="col-sm-4 form-group">
                                                                  <?= lang('filter_year', 'filter_year_sales_h') ?>
                                                                  <select name="filter_year" id="filter_year_sales_h" class="form-control" required>
                                                                     <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                                              <option value="<?= $key ?>"><?= $key ?></option>
                                                                     <?php endforeach ?>
                                                                  </select>
                                                               </div>
                                                            <?php endif ?>
                                                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                               <?= lang('start_date', 'start_date') ?>
                                                               <input type="date" name="sales_start_date" id="sales_start_date" value="<?= $this->filtros_fecha_inicial ?>" class="form-control">
                                                            </div>
                                                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                               <?= lang('end_date', 'end_date') ?>
                                                               <input type="date" name="sales_end_date" id="sales_end_date" value="<?= $this->filtros_fecha_final ?>" class="form-control">
                                                            </div>
                                                         </div>
                                                         <div class="clearfix"></div>
                                                            <div class="col-sm-3" style="margin-top: 5px;">
                                                               <button class="btn btn-success" id="sale_filter"><?= lang('submit') ?></button>
                                                            </div>
                                                         </div>
                                                      </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-12">
                                                <p class="introtext"><?php echo lang('list_results'); ?></p>
                                                <div class="">
                                                      <table id="SlRData" class="table table-bordered table-hover table-condensed">
                                                         <thead>
                                                         <tr>
                                                            <th><?= lang("date"); ?></th>
                                                            <th><?= lang("reference_no"); ?></th>
                                                            <th><?= lang("biller"); ?></th>
                                                            <th><?= lang("customer_name"); ?></th>
                                                            <th><?= lang("product"); ?></th>
                                                            <th><?= lang("product_qty"); ?></th>
                                                            <th><?= lang("product_total"); ?></th>
                                                            <th><?= lang("grand_total"); ?></th>
                                                            <th><?= lang("paid"); ?></th>
                                                            <th><?= lang("balance"); ?></th>
                                                            <th><?= lang("payment_status"); ?></th>
                                                            <th><?= lang("sale_status"); ?></th>
                                                         </tr>
                                                         </thead>
                                                         <tbody>
                                                         <tr>
                                                            <td colspan="9"
                                                                  class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                                         </tr>
                                                         </tbody>
                                                         <tfoot class="dtFilter">
                                                         <tr class="active">
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th></th>
                                                            <th><?= lang("product"); ?></th>
                                                            <th><?= lang("product_qty"); ?></th>
                                                            <th><?= lang("product_total"); ?></th>
                                                            <th><?= lang("grand_total"); ?></th>
                                                            <th><?= lang("balance"); ?></th>
                                                            <th></th>
                                                            <th></th>
                                                         </tr>
                                                         </tfoot>
                                                      </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <script type="text/javascript">
                                    $('#sales_date_records_filter').on('change', function(){
                                          filter = $(this).val();
                                          fecha_inicial = "";
                                          fecha_final = "<?= date('Y-m-d') ?>";
                                          hide_date_controls = true;
                                          if (filter == 5) { //RANGO DE FECHAS
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          } else if (filter == 1) { // HOY
                                             fecha_inicial = "<?= date('Y-m-d') ?>";
                                          } else if (filter == 2) { // MES
                                             fecha_inicial = "<?= date('Y-m-01') ?>";
                                          } else if (filter == 3) { // TRIMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                                          } else if (filter == 8) { // SEMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
                                          } else if (filter == 4) { // AÑO
                                             fecha_inicial = "<?= date('Y-01-01') ?>";
                                          } else if (filter == 6) { // MES PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                                             fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                                          } else if (filter == 9) { // TRIMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 10) { // SEMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 7) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                             fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                          } else if (filter == 11) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
                                          } else if (filter == 0) {
                                             fecha_inicial = "";
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          }
                                          $('#sales_start_date').val(fecha_inicial);
                                          $('#sales_end_date').val(fecha_final);
                                          if (hide_date_controls) {
                                             $('.sales_date_controls').css('display', 'none');
                                          } else {
                                             $('.sales_date_controls').css('display', '');
                                          }
                                       });

                                       $('#sales_date_records_filter').val("<?= $this->Settings->default_records_filter != 0 ? $this->Settings->default_records_filter : 5 ?>").trigger('change');

                                       $(document).on('change', '#sales_date_records_filter, #filter_year_sales_h', function(){
                                          if (site.settings.big_data_limit_reports == 1) {
                                             if ($('#sales_date_records_filter').val() == 5) {
                                                fv_start_year = $('#filter_year_sales_h').val();
                                                f_start_year = fv_start_year+"-01-01";
                                                f_end_year = fv_start_year+"-12-30";
                                                $('input[name="sales_start_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                $('input[name="sales_end_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                             }
                                          }
                                       });
                                 </script>
                              <?php } ?>  
                              <?php if ($Owner || $Admin || $this->GP['products-view_order_sales']) { ?>
                                 <div id="order_sales" class="tab-pane fade">
                                    <?php $warehouse_id = NULL; ?>
                                    <script type="text/javascript">
                                       var osTable;
                                       $(document).ready(function () {
                                          osTable = $('#OSlRData').dataTable({
                                             "aaSorting": [[0, "desc"]],
                                             "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                                             "iDisplayLength": <?= $Settings->rows_per_page ?>,
                                             'bProcessing': true, 'bServerSide': true,
                                             'sAjaxSource': '<?= admin_url('products/getOrderSalesReport/?v=1&product='.$product->id) ?>',
                                             'fnServerData': function (sSource, aoData, fnCallback) {
                                                aoData.push({
                                                            "name": "<?= $this->security->get_csrf_token_name() ?>",
                                                            "value": "<?= $this->security->get_csrf_hash() ?>"
                                                            },{
                                                               "name": "start_date",
                                                               "value": $('#order_sales_start_date').val()
                                                            },{
                                                               "name": "end_date",
                                                               "value": $('#order_sales_end_date').val()
                                                            });
                                                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                                             },
                                             'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                                                nRow.id = aData[11];
                                                nRow.className = (aData[5] > 0) ? "order_link" : "order_link warning";
                                                return nRow;
                                             },
                                             "aoColumns": [
                                                {"mRender": fld},
                                                null,
                                                null,
                                                null,
                                                null,
                                                {"mRender": formatQuantity},
                                                {"mRender": formatQuantity},
                                                {"mRender": formatQuantity},
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": row_status}
                                             ],
                                             "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                                var gtotal = 0, paid = 0, balance = 0, qty = 0, delivered_qty = 0, pending_qty = 0;
                                                for (var i = 0; i < aaData.length; i++) {
                                                      qty += parseFloat(aaData[aiDisplay[i]][5]);
                                                      delivered_qty += parseFloat(aaData[aiDisplay[i]][6]);
                                                      pending_qty += parseFloat(aaData[aiDisplay[i]][7]);
                                                      gtotal += parseFloat(aaData[aiDisplay[i]][8]);
                                                }
                                                // console.log(nRow);
                                                var nCells = nRow.getElementsByTagName('th');
                                                nCells[5].innerHTML = formatQuantity(parseFloat(qty));
                                                nCells[6].innerHTML = formatQuantity(parseFloat(delivered_qty));
                                                nCells[7].innerHTML = formatQuantity(parseFloat(pending_qty));
                                                nCells[8].innerHTML = currencyFormat(parseFloat(gtotal));
                                             }
                                          }).fnSetFilteringDelay().dtFilter([
                                             {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                             {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                                             {column_number: 2, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                                             {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                                          ], "footer");
                                       });

                                       $(document).on('click', '#order_sale_filter', function(){
                                          set_filter_to_all($('#order_sales_date_records_filter').val(), $('#order_sales_start_date').val(), $('#order_sales_end_date').val());
                                       });

                                    </script>
                                    <div class="box">
                                       <div class="box-header">
                                          <div class="box-icon">
                                             <ul class="btn-tasks">
                                                <li class="dropdown">
                                                   <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                                      <i class="icon fa fa-file-excel-o"></i>
                                                   </a>
                                                </li>
                                                <li class="dropdown">
                                                   <a href="#" id="image" class="tip image" title="<?= lang('save_image') ?>">
                                                      <i class="icon fa fa-file-picture-o"></i>
                                                   </a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <div class="box-content">
                                          <div class="row">
                                             <div class="col-lg-12">
                                                <div class="ibox float-e-margins border-bottom">
                                                   <div class="ibox-title">
                                                      <h5><?= lang('filter') ?></h5>
                                                      <div class="ibox-tools">
                                                         <a class="collapse-link">
                                                            <i class="fa fa-chevron-down"></i>
                                                         </a>
                                                      </div>
                                                   </div>
                                                   <div class="ibox-content" style="display: none;">
                                                      <div class="row">
                                                         <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                            <?= lang('date_records_filter', 'date_records_filter') ?>
                                                            <select name="order_sales_date_records_filter" id="order_sales_date_records_filter" class="form-control">
                                                               <?= $this->sma->get_filter_options(); ?>
                                                            </select>
                                                         </div>

                                                         <div class="order_sales_date_controls">
                                                            <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                               <div class="col-sm-4 form-group">
                                                                  <?= lang('filter_year', 'filter_year_order_sales_h') ?>
                                                                  <select name="filter_year" id="filter_year_order_sales_h" class="form-control" required>
                                                                     <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                                              <option value="<?= $key ?>"><?= $key ?></option>
                                                                     <?php endforeach ?>
                                                                  </select>
                                                               </div>
                                                            <?php endif ?>
                                                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                               <?= lang('start_date', 'start_date') ?>
                                                               <input type="date" name="order_sales_start_date" id="order_sales_start_date" value="<?= $this->filtros_fecha_inicial ?>" class="form-control">
                                                            </div>
                                                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                               <?= lang('end_date', 'end_date') ?>
                                                               <input type="date" name="order_sales_end_date" id="order_sales_end_date" value="<?= $this->filtros_fecha_final ?>" class="form-control">
                                                            </div>
                                                         </div>
                                                         <div class="clearfix"></div>
                                                            <div class="col-sm-3" style="margin-top: 5px;">
                                                               <button class="btn btn-success" id="order_sale_filter"><?= lang('submit') ?></button>
                                                            </div>
                                                         </div>
                                                      </div>
                                                </div>
                                             </div>
                                             <div class="col-lg-12">
                                                <p class="introtext"><?php echo lang('list_results'); ?></p>
                                                <div class="">
                                                      <table id="OSlRData" class="table table-bordered table-hover table-condensed">
                                                         <thead>
                                                         <tr>
                                                            <th><?= lang("date"); ?></th>
                                                            <th><?= lang("reference_no"); ?></th>
                                                            <th><?= lang("biller"); ?></th>
                                                            <th><?= lang("customer_name"); ?></th>
                                                            <th><?= lang("product"); ?></th>
                                                            <th><?= lang("quantity"); ?></th>
                                                            <th><?= lang("quantity_dispatch"); ?></th>
                                                            <th><?= lang("quantity_dispatch_pending"); ?></th>
                                                            <th><?= lang("product_total"); ?></th>
                                                            <th><?= lang("grand_total"); ?></th>
                                                            <th><?= lang("sale_status"); ?></th>
                                                         </tr>
                                                         </thead>
                                                         <tbody>
                                                         <tr>
                                                            <td colspan="11"
                                                                  class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                                         </tr>
                                                         </tbody>
                                                         <tfoot class="dtFilter">
                                                         <tr class="active">
                                                            <th><?= lang("date"); ?></th>
                                                            <th><?= lang("reference_no"); ?></th>
                                                            <th><?= lang("biller"); ?></th>
                                                            <th><?= lang("customer_name"); ?></th>
                                                            <th><?= lang("product"); ?></th>
                                                            <th><?= lang("quantity"); ?></th>
                                                            <th><?= lang("quantity_dispatch"); ?></th>
                                                            <th><?= lang("quantity_dispatch_pending"); ?></th>
                                                            <th><?= lang("product_total"); ?></th>
                                                            <th><?= lang("grand_total"); ?></th>
                                                            <th><?= lang("sale_status"); ?></th>
                                                         </tr>
                                                         </tfoot>
                                                      </table>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <script type="text/javascript">
                                    $('#order_sales_date_records_filter').on('change', function(){
                                          filter = $(this).val();
                                          fecha_inicial = "";
                                          fecha_final = "<?= date('Y-m-d') ?>";
                                          hide_date_controls = true;
                                          if (filter == 5) { //RANGO DE FECHAS
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          } else if (filter == 1) { // HOY
                                             fecha_inicial = "<?= date('Y-m-d') ?>";
                                          } else if (filter == 2) { // MES
                                             fecha_inicial = "<?= date('Y-m-01') ?>";
                                          } else if (filter == 3) { // TRIMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                                          } else if (filter == 8) { // SEMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
                                          } else if (filter == 4) { // AÑO
                                             fecha_inicial = "<?= date('Y-01-01') ?>";
                                          } else if (filter == 6) { // MES PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                                             fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                                          } else if (filter == 9) { // TRIMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 10) { // SEMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 7) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                             fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                          } else if (filter == 11) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
                                          } else if (filter == 0) {
                                             fecha_inicial = "";
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          }
                                          $('#order_sales_start_date').val(fecha_inicial);
                                          $('#order_sales_end_date').val(fecha_final);
                                          if (hide_date_controls) {
                                             $('.order_sales_date_controls').css('display', 'none');
                                          } else {
                                             $('.order_sales_date_controls').css('display', '');
                                          }
                                       });

                                       $('#order_sales_date_records_filter').val("<?= $this->Settings->default_records_filter != 0 ? $this->Settings->default_records_filter : 5 ?>").trigger('change');

                                       $(document).on('change', '#order_sales_date_records_filter, #filter_year_order_sales_h', function(){
                                          if (site.settings.big_data_limit_reports == 1) {
                                             if ($('#order_sales_date_records_filter').val() == 5) {
                                                fv_start_year = $('#filter_year_order_sales_h').val();
                                                f_start_year = fv_start_year+"-01-01";
                                                f_end_year = fv_start_year+"-12-30";
                                                $('input[name="order_sales_start_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                $('input[name="order_sales_end_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                             }
                                          }
                                       });
                                 </script>
                              <?php } ?>  
                              <?php if ($Owner || $Admin || $this->GP['products-view_quotes']) { ?>	
                                 <div id="quotes" class="tab-pane fade">
                                    <script type="text/javascript">
                                       var qTable
                                       $(document).ready(function () {
                                          qTable = $('#QuRData').dataTable({
                                             "aaSorting": [[0, "desc"]],
                                             "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                                             "iDisplayLength": <?= $Settings->rows_per_page ?>,
                                             'bProcessing': true, 'bServerSide': true,
                                             'sAjaxSource': '<?= admin_url('products/getQuotesReport/?v=1&product='.$product->id) ?>',
                                             'fnServerData': function (sSource, aoData, fnCallback) {
                                                aoData.push({
                                                      "name": "<?= $this->security->get_csrf_token_name() ?>",
                                                      "value": "<?= $this->security->get_csrf_hash() ?>"
                                                },{
                                                      "name": "start_date",
                                                      "value": $('#quotes_start_date').val()
                                                },{
                                                      "name": "end_date",
                                                      "value": $('#quotes_end_date').val()
                                                });
                                                $.ajax({ 'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback });
                                             },
                                             'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                                                var oSettings = qTable.fnSettings();
                                                nRow.id = aData[7];
                                                nRow.className = "quote_link2";
                                                return nRow;
                                             },
                                             "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                                var gtotal = 0;
                                                for (var i = 0; i < aaData.length; i++) {
                                                   gtotal += parseFloat(aaData[aiDisplay[i]][5]);
                                                }
                                                var nCells = nRow.getElementsByTagName('th');
                                                nCells[5].innerHTML = currencyFormat(formatMoney(gtotal));
                                             },
                                             "aoColumns": [
                                                            {"mRender": fld},
                                                            null,
                                                            null,
                                                            null,
                                                            {"bSearchable": false, "mRender": pqFormat},
                                                            {"mRender": currencyFormat},
                                                            {"mRender": row_status},
                                                            {"bVisible": false},
                                                            ],
                                          }).fnSetFilteringDelay().dtFilter([
                                             {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                             {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                                             {column_number: 2, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
                                             {column_number: 3, filter_default_label: "[<?=lang('customer');?>]", filter_type: "text", data: []},
                                             {column_number: 5, filter_default_label: "[<?=lang('grand_total');?>]", filter_type: "text", data: []},
                                             {column_number: 6, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
                                          ], "footer");
                                       });

                                       $(document).on('click', '#quote_filter', function(){
                                          set_filter_to_all($('#quotes_date_records_filter').val(), $('#quotes_start_date').val(), $('#quotes_end_date').val());
                                       });
                                    </script>
                                    <div class="box">
                                       <div class="box-header">
                                          <div class="box-icon">
                                             <ul class="btn-tasks">
                                                <li class="dropdown">
                                                      <a href="#" id="pdf1" class="tip" title="<?= lang('download_pdf') ?>">
                                                         <i class="icon fa fa-file-pdf-o"></i>
                                                      </a>
                                                </li>
                                                <li class="dropdown">
                                                      <a href="#" id="xls1" class="tip" title="<?= lang('download_xls') ?>">
                                                         <i class="icon fa fa-file-excel-o"></i>
                                                      </a>
                                                </li>
                                                <li class="dropdown">
                                                      <a href="#" id="image1" class="tip image" title="<?= lang('save_image') ?>">
                                                         <i class="icon fa fa-file-picture-o"></i>
                                                      </a>
                                                </li>
                                             </ul>
                                          </div>
                                       </div>
                                       <div class="box-content">
                                          <div class="row">
                                             <div class="col-lg-12">
                                                <div class="ibox float-e-margins border-bottom">
                                                   <div class="ibox-title">
                                                      <h5><?= lang('filter') ?></h5>
                                                      <div class="ibox-tools">
                                                         <a class="collapse-link">
                                                            <i class="fa fa-chevron-down"></i>
                                                         </a>
                                                      </div>
                                                   </div>
                                                   <div class="ibox-content" style="display: none;">
                                                      <div class="row">
                                                         <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                            <?= lang('date_records_filter', 'date_records_filter') ?>
                                                            <select name="quotes_date_records_filter" id="quotes_date_records_filter" class="form-control">
                                                               <?= $this->sma->get_filter_options(); ?>
                                                            </select>
                                                         </div>

                                                         <div class="quotes_date_controls">
                                                            <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                               <div class="col-sm-4 form-group">
                                                                  <?= lang('filter_year', 'filter_year_quotes_h') ?>
                                                                  <select name="filter_year" id="filter_year_quotes_h" class="form-control" required>
                                                                     <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                                              <option value="<?= $key ?>"><?= $key ?></option>
                                                                           <?php endforeach ?>
                                                                  </select>
                                                               </div>
                                                            <?php endif ?>
                                                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                               <?= lang('start_date', 'start_date') ?>
                                                               <input type="date" name="quotes_start_date" id="quotes_start_date" value="<?= $this->filtros_fecha_inicial ?>" class="form-control">
                                                            </div>
                                                            <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                               <?= lang('end_date', 'end_date') ?>
                                                               <input type="date" name="quotes_end_date" id="quotes_end_date" value="<?= $this->filtros_fecha_final ?>" class="form-control">
                                                            </div>
                                                         </div>

                                                         <div class="clearfix"></div>
                                                            <div class="col-sm-3" style="margin-top: 5px;">
                                                               <button class="btn btn-success" id="quote_filter"><?= lang('submit') ?></button>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>

                                                <div class="col-lg-12">
                                                   <p class="introtext"><?php echo lang('list_results'); ?></p>

                                                   <div class="">
                                                      <table id="QuRData" class="table table-bordered table-hover table-condensed">
                                                         <thead>
                                                            <tr>
                                                               <th><?= lang("date"); ?></th>
                                                               <th><?= lang("reference_no"); ?></th>
                                                               <th><?= lang("biller"); ?></th>
                                                               <th><?= lang("customer_name"); ?></th>
                                                               <th><?= lang("product"); ?></th>
                                                               <th><?= lang("product_qty"); ?></th>
                                                               <th><?= lang("status"); ?></th>
                                                               <th></th>
                                                            </tr>
                                                         </thead>
                                                         <tbody>
                                                            <tr>
                                                               <td colspan="8"
                                                                     class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                                            </tr>
                                                         </tbody>
                                                         <tfoot class="dtFilter">
                                                            <tr class="active">
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th><?= lang("product"); ?></th>
                                                               <th><?= lang("product_qty"); ?></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                            </tr>
                                                         </tfoot>
                                                      </table>
                                                   </div>
                                                </div>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php } ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_purchases']) { ?>		
                                <div id="purchases" class="tab-pane fade">
                                    <script type="text/javascript">
                                       //  { "bSearchable": false, "mRender": pqFormat },

                                       $('#quotes_date_records_filter').on('change', function(){
                                          filter = $(this).val();
                                          fecha_inicial = "";
                                          fecha_final = "<?= date('Y-m-d') ?>";
                                          hide_date_controls = true;
                                          if (filter == 5) { //RANGO DE FECHAS
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          } else if (filter == 1) { // HOY
                                             fecha_inicial = "<?= date('Y-m-d') ?>";
                                          } else if (filter == 2) { // MES
                                             fecha_inicial = "<?= date('Y-m-01') ?>";
                                          } else if (filter == 3) { // TRIMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                                          } else if (filter == 8) { // SEMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
                                          } else if (filter == 4) { // AÑO
                                             fecha_inicial = "<?= date('Y-01-01') ?>";
                                          } else if (filter == 6) { // MES PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                                             fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                                          } else if (filter == 9) { // TRIMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 10) { // SEMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 7) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                             fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                          } else if (filter == 11) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
                                          } else if (filter == 0) {
                                             fecha_inicial = "";
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          }
                                          $('#quotes_start_date').val(fecha_inicial);
                                          $('#quotes_end_date').val(fecha_final);
                                          if (hide_date_controls) {
                                             $('.quotes_date_controls').css('display', 'none');
                                          } else {
                                             $('.quotes_date_controls').css('display', '');
                                          }
                                       });
                                       $('#quotes_date_records_filter').val("<?= $this->Settings->default_records_filter != 0 ? $this->Settings->default_records_filter : 5 ?>").trigger('change');

                                       $(document).on('change', '#quotes_date_records_filter, #filter_year_quotes_h', function(){
                                          if (site.settings.big_data_limit_reports == 1) {
                                             if ($('#quotes_date_records_filter').val() == 5) {
                                                fv_start_year = $('#filter_year_quotes_h').val();
                                                f_start_year = fv_start_year+"-01-01";
                                                f_end_year = fv_start_year+"-12-30";
                                                $('input[name="quotes_start_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                $('input[name="quotes_end_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                             }
                                          }
                                       });

                                       var pTable;
                                       $(document).ready(function () {
                                          pTable = $('#PoRData').dataTable({
                                             "aaSorting": [[0, "desc"]],
                                             "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                                             "iDisplayLength": <?= $Settings->rows_per_page ?>,
                                             'bProcessing': true, 'bServerSide': true,
                                             'sAjaxSource': '<?= admin_url('products/getPurchasesReport/?v=1&product=' . $product->id) ?>',
                                             'fnServerData': function (sSource, aoData, fnCallback) {
                                                aoData.push({
                                                      "name": "<?= $this->security->get_csrf_token_name() ?>",
                                                      "value": "<?= $this->security->get_csrf_hash() ?>"
                                                   },{
                                                      "name": "start_date",
                                                      "value": $('#purchases_start_date').val()
                                                   },{
                                                      "name": "end_date",
                                                      "value": $('#purchases_end_date').val()
                                                });
                                                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                                             },
                                             'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                                                // console.log(aData);
                                                nRow.id = aData[10];
                                                nRow.className = (aData[5] > 0) ? "purchase_link2" : "purchase_link2 warning";
                                                return nRow;
                                             },
                                             "aoColumns": [
                                                {"mRender": fld},
                                                null,
                                                null,
                                                null,
                                                null,
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": currencyFormat},
                                                {"mRender": row_status},
                                                {"bVisible": false},
                                             ],
                                             "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                                var gtotal = 0, paid = 0, balance = 0, qty = 0;
                                                for (var i = 0; i < aaData.length; i++) {
                                                      qty += parseFloat(aaData[aiDisplay[i]][5]);
                                                      gtotal += parseFloat(aaData[aiDisplay[i]][6]);
                                                      paid += parseFloat(aaData[aiDisplay[i]][7]);
                                                      balance += parseFloat(aaData[aiDisplay[i]][8]);
                                                }
                                                var nCells = nRow.getElementsByTagName('th');
                                                nCells[5].innerHTML = currencyFormat(parseFloat(qty));
                                                nCells[6].innerHTML = currencyFormat(parseFloat(gtotal));
                                                nCells[7].innerHTML = currencyFormat(parseFloat(paid));
                                                nCells[8].innerHTML = currencyFormat(parseFloat(balance));
                                             }
                                          }).fnSetFilteringDelay().dtFilter([
                                             {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                             {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                                             {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
                                             {column_number: 3, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
                                             {column_number: 8, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
                                          ], "footer");
                                       });


                                       $(document).on('click', '#purchase_filter', function(){
                                             set_filter_to_all($('#purchases_date_records_filter').val(), $('#purchases_start_date').val(), $('#purchases_end_date').val());
                                       });
                                    </script>
                                    <div class="box">
                                       <div class="box-header">
                                             <div class="box-icon">
                                                <ul class="btn-tasks">
                                                   <li class="dropdown">
                                                         <a href="#" id="pdf2" class="tip" title="<?= lang('download_pdf') ?>">
                                                            <i class="icon fa fa-file-pdf-o"></i>
                                                         </a>
                                                   </li>
                                                   <li class="dropdown">
                                                         <a href="#" id="xls2" class="tip" title="<?= lang('download_xls') ?>">
                                                            <i class="icon fa fa-file-excel-o"></i>
                                                         </a>
                                                   </li>
                                                   <li class="dropdown">
                                                         <a href="#" id="image2" class="tip image" title="<?= lang('save_image') ?>">
                                                            <i class="icon fa fa-file-picture-o"></i>
                                                         </a>
                                                   </li>
                                                </ul>
                                             </div>
                                       </div>
                                       <div class="box-content">
                                             <div class="row">
                                                <div class="col-lg-12">
                                                   <div class="ibox float-e-margins border-bottom">
                                                         <div class="ibox-title">
                                                            <h5><?= lang('filter') ?></h5>
                                                            <div class="ibox-tools">
                                                               <a class="collapse-link">
                                                                     <i class="fa fa-chevron-down"></i>
                                                               </a>
                                                            </div>
                                                         </div>
                                                         <div class="ibox-content" style="display: none;">
                                                            <div class="row">
                                                               <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                     <?= lang('date_records_filter', 'date_records_filter') ?>
                                                                     <select name="purchases_date_records_filter" id="purchases_date_records_filter" class="form-control">
                                                                        <?= $this->sma->get_filter_options(); ?>
                                                                     </select>
                                                               </div>

                                                               <div class="purchases_date_controls">
                                                                     <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                                        <div class="col-sm-4 form-group">
                                                                           <?= lang('filter_year', 'filter_year_purchases_h') ?>
                                                                           <select name="filter_year" id="filter_year_purchases_h" class="form-control" required>
                                                                                 <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                                                 <?php endforeach ?>
                                                                           </select>
                                                                        </div>
                                                                     <?php endif ?>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('start_date', 'start_date') ?>
                                                                        <input type="date" name="purchases_start_date" id="purchases_start_date" value="<?= $this->filtros_fecha_inicial ?>" class="form-control">
                                                                     </div>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('end_date', 'end_date') ?>
                                                                        <input type="date" name="purchases_end_date" id="purchases_end_date" value="<?= $this->filtros_fecha_final ?>" class="form-control">
                                                                     </div>
                                                               </div>
                                                               <div class="clearfix"></div>
                                                               <div class="col-sm-3" style="margin-top: 5px;">
                                                                     <button class="btn btn-success" id="purchase_filter"><?= lang('submit') ?></button>
                                                               </div>
                                                            </div>
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-12">
                                                   <p class="introtext"><?php echo lang('list_results'); ?></p>

                                                   <div class="">
                                                         <table id="PoRData" class="table table-bordered table-hover table-condensed">
                                                            <thead>
                                                            <tr>
                                                               <th><?= lang("date"); ?></th>
                                                               <th><?= lang("reference_no"); ?></th>
                                                               <th><?= lang("warehouse"); ?></th>
                                                               <th><?= lang("supplier_name"); ?></th>
                                                               <th><?= lang("product"); ?></th>
                                                               <th><?= lang("product_qty"); ?></th>
                                                               <th><?= lang("grand_total"); ?></th>
                                                               <th><?= lang("paid"); ?></th>
                                                               <th><?= lang("balance"); ?></th>
                                                               <th><?= lang("status"); ?></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                               <td colspan="10"
                                                                     class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                                            </tr>
                                                            </tbody>
                                                            <tfoot class="dtFilter">
                                                            <tr class="active">
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th><?= lang("product"); ?></th>
                                                               <th><?= lang("product_qty"); ?></th>
                                                               <th><?= lang("paid"); ?></th>
                                                               <th><?= lang("balance"); ?></th>
                                                               <th></th>
                                                            </tr>
                                                            </tfoot>
                                                         </table>
                                                   </div>
                                                </div>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php } ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_transfers']) { ?>		   
                                 <div id="transfers" class="tab-pane fade">
                                    <script type="text/javascript">

                                       $('#purchases_date_records_filter').on('change', function(){
                                          filter = $(this).val();
                                          fecha_inicial = "";
                                          fecha_final = "<?= date('Y-m-d') ?>";
                                          hide_date_controls = true;
                                          if (filter == 5) { //RANGO DE FECHAS
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          } else if (filter == 1) { // HOY
                                             fecha_inicial = "<?= date('Y-m-d') ?>";
                                          } else if (filter == 2) { // MES
                                             fecha_inicial = "<?= date('Y-m-01') ?>";
                                          } else if (filter == 3) { // TRIMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                                          } else if (filter == 8) { // SEMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
                                          } else if (filter == 4) { // AÑO
                                             fecha_inicial = "<?= date('Y-01-01') ?>";
                                          } else if (filter == 6) { // MES PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                                             fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                                          } else if (filter == 9) { // TRIMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 10) { // SEMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 7) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                             fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                          } else if (filter == 11) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
                                          } else if (filter == 0) {
                                             fecha_inicial = "";
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          }
                                          $('#purchases_start_date').val(fecha_inicial);
                                          $('#purchases_end_date').val(fecha_final);
                                          if (hide_date_controls) {
                                             $('.purchases_date_controls').css('display', 'none');
                                          } else {
                                             $('.purchases_date_controls').css('display', '');
                                          }
                                       });

                                       $('#purchases_date_records_filter').val("<?= $this->Settings->default_records_filter != 0 ? $this->Settings->default_records_filter : 5 ?>").trigger('change');

                                       $(document).on('change', '#purchases_date_records_filter, #filter_year_purchases_h', function(){
                                             if (site.settings.big_data_limit_reports == 1) {
                                                if ($('#purchases_date_records_filter').val() == 5) {
                                                   fv_start_year = $('#filter_year_purchases_h').val();
                                                   f_start_year = fv_start_year+"-01-01";
                                                   f_end_year = fv_start_year+"-12-30";
                                                   $('input[name="purchases_start_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                   $('input[name="purchases_end_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                }
                                             }
                                       });

                                       var tTable;
                                       $(document).ready(function () {
                                          tTable = $('#TrRData').dataTable({
                                             "aaSorting": [[0, "desc"]],
                                             "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                                             "iDisplayLength": <?= $Settings->rows_per_page ?>,
                                             'bProcessing': true, 'bServerSide': true,
                                             'sAjaxSource': '<?= admin_url('products/getTransfersReport/?v=1&product='.$product->id) ?>',
                                             'fnServerData': function (sSource, aoData, fnCallback) {
                                                aoData.push({
                                                      "name": "<?= $this->security->get_csrf_token_name() ?>",
                                                      "value": "<?= $this->security->get_csrf_hash() ?>"
                                                   },{
                                                      "name": "start_date",
                                                      "value": $('#transfers_start_date').val()
                                                   },{
                                                      "name": "end_date",
                                                      "value": $('#transfers_end_date').val()
                                                });
                                                $.ajax({ 'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback });
                                             },
                                             'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                                                var oSettings = tTable.fnSettings();
                                                nRow.id = aData[8];
                                                nRow.className = "transfer_link2";
                                                return nRow;
                                             },
                                             "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                                var gtotal = 0, qty = 0;
                                                for (var i = 0; i < aaData.length; i++) {
                                                      qty += parseFloat(aaData[aiDisplay[i]][3]);;
                                                      gtotal += parseFloat(aaData[aiDisplay[i]][6]);;
                                                }
                                                var nCells = nRow.getElementsByTagName('th');
                                                nCells[3].innerHTML = currencyFormat(formatMoney(qty));
                                                nCells[6].innerHTML = currencyFormat(formatMoney(gtotal));
                                             },
                                             "aoColumns": [{"mRender": fld}, null, {
                                                "bSearchable": false,
                                                "mRender": pqFormat
                                             },{
                                                "bSearchable": false,
                                                "mRender": pqFormat
                                             }, null, null, {"mRender": currencyFormat}, {"mRender": row_status}],
                                          }).fnSetFilteringDelay().dtFilter([
                                             {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                             {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                                             {
                                                column_number: 3,
                                                filter_default_label: "[<?=lang("warehouse").' ('.lang('from').')';?>]",
                                                filter_type: "text", data: []
                                             },
                                             {
                                                column_number: 4,
                                                filter_default_label: "[<?=lang("warehouse").' ('.lang('to').')';?>]",
                                                filter_type: "text", data: []
                                             },
                                             {column_number: 5, filter_default_label: "[<?=lang('grand_total');?>]", filter_type: "text", data: []},
                                             {column_number: 6, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
                                          ], "footer");
                                       });


                                       $(document).on('click', '#transfer_filter', function(){

                                             set_filter_to_all($('#transfers_date_records_filter').val(), $('#transfers_start_date').val(), $('#transfers_end_date').val());
                                       });
                                    </script>
                                    <div class="box">
                                       <div class="box-header">
                                             <div class="box-icon">
                                                <ul class="btn-tasks">
                                                   <li class="dropdown"><a href="#" id="pdf3" class="tip" title="<?= lang('download_pdf') ?>"><i
                                                               class="icon fa fa-file-pdf-o"></i></a></li>
                                                   <li class="dropdown"><a href="#" id="xls3" class="tip" title="<?= lang('download_xls') ?>"><i
                                                               class="icon fa fa-file-excel-o"></i></a></li>
                                                   <li class="dropdown"><a href="#" id="image3" class="tip image"
                                                                           title="<?= lang('save_image') ?>"><i
                                                               class="icon fa fa-file-picture-o"></i></a></li>
                                                </ul>
                                             </div>
                                       </div>
                                       <div class="box-content">
                                             <div class="row">
                                                <div class="col-lg-12">
                                                   <div class="ibox float-e-margins border-bottom">
                                                         <div class="ibox-title">
                                                            <h5><?= lang('filter') ?></h5>
                                                            <div class="ibox-tools">
                                                               <a class="collapse-link">
                                                                     <i class="fa fa-chevron-down"></i>
                                                               </a>
                                                            </div>
                                                         </div>
                                                         <div class="ibox-content" style="display: none;">
                                                            <div class="row">
                                                               <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                     <?= lang('date_records_filter', 'date_records_filter') ?>
                                                                     <select name="transfers_date_records_filter" id="transfers_date_records_filter" class="form-control">
                                                                        <?= $this->sma->get_filter_options(); ?>
                                                                     </select>
                                                               </div>

                                                               <div class="transfers_date_controls">
                                                                     <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                                        <div class="col-sm-4 form-group">
                                                                           <?= lang('filter_year', 'filter_year_transfers_h') ?>
                                                                           <select name="filter_year" id="filter_year_transfers_h" class="form-control" required>
                                                                                 <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                                                 <?php endforeach ?>
                                                                           </select>
                                                                        </div>
                                                                     <?php endif ?>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('start_date', 'start_date') ?>
                                                                        <input type="date" name="transfers_start_date" id="transfers_start_date" value="<?= $this->filtros_fecha_inicial ?>" class="form-control">
                                                                     </div>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('end_date', 'end_date') ?>
                                                                        <input type="date" name="transfers_end_date" id="transfers_end_date" value="<?= $this->filtros_fecha_final ?>" class="form-control">
                                                                     </div>
                                                               </div>
                                                               <div class="clearfix"></div>
                                                               <div class="col-sm-3" style="margin-top: 5px;">
                                                                     <button class="btn btn-success" id="transfer_filter"><?= lang('submit') ?></button>
                                                               </div>
                                                            </div>
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-12">
                                                   <p class="introtext"><?php echo lang('list_results'); ?></p>

                                                   <div class="">
                                                         <table id="TrRData" class="table table-bordered table-hover table-condensed">
                                                            <thead>
                                                            <tr>
                                                               <th><?= lang("date"); ?></th>
                                                               <th><?= lang("reference_no"); ?></th>
                                                               <th><?= lang("product"); ?></th>
                                                               <th><?= lang("product_qty"); ?></th>
                                                               <th><?= lang("warehouse") . ' (' . lang('from') . ')'; ?></th>
                                                               <th><?= lang("warehouse") . ' (' . lang('to') . ')'; ?></th>
                                                               <th><?= lang("grand_total"); ?></th>
                                                               <th><?= lang("status"); ?></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                               <td colspan="7"
                                                                     class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                                            </tr>
                                                            </tbody>
                                                            <tfoot class="dtFilter">
                                                            <tr class="active">
                                                               <th></th>
                                                               <th></th>
                                                               <th><?= lang("product"); ?></th>
                                                               <th><?= lang("product_qty"); ?></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                            </tr>
                                                            </tfoot>
                                                         </table>
                                                   </div>
                                                </div>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php } ?>	
                              <?php if ($Owner || $Admin || $this->GP['products-view_adjustments']) { ?>   
                                 <div id="damages" class="tab-pane fade">
                                    <script>

                                       $('#transfers_date_records_filter').on('change', function(){
                                          filter = $(this).val();
                                          fecha_inicial = "";
                                          fecha_final = "<?= date('Y-m-d') ?>";
                                          hide_date_controls = true;
                                          if (filter == 5) { //RANGO DE FECHAS
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          } else if (filter == 1) { // HOY
                                             fecha_inicial = "<?= date('Y-m-d') ?>";
                                          } else if (filter == 2) { // MES
                                             fecha_inicial = "<?= date('Y-m-01') ?>";
                                          } else if (filter == 3) { // TRIMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                                          } else if (filter == 8) { // SEMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
                                          } else if (filter == 4) { // AÑO
                                             fecha_inicial = "<?= date('Y-01-01') ?>";
                                          } else if (filter == 6) { // MES PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                                             fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                                          } else if (filter == 9) { // TRIMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 10) { // SEMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 7) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                             fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                          } else if (filter == 11) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
                                          } else if (filter == 0) {
                                             fecha_inicial = "";
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          }
                                          $('#transfers_start_date').val(fecha_inicial);
                                          $('#transfers_end_date').val(fecha_final);
                                          if (hide_date_controls) {
                                             $('.transfers_date_controls').css('display', 'none');
                                          } else {
                                             $('.transfers_date_controls').css('display', '');
                                          }
                                       });

                                       $('#transfers_date_records_filter').val("<?= $this->Settings->default_records_filter != 0 ? $this->Settings->default_records_filter : 5 ?>").trigger('change');

                                       $(document).on('change', '#transfers_date_records_filter, #filter_year_transfers_h', function(){
                                             if (site.settings.big_data_limit_reports == 1) {
                                                if ($('#transfers_date_records_filter').val() == 5) {
                                                   fv_start_year = $('#filter_year_transfers_h').val();
                                                   f_start_year = fv_start_year+"-01-01";
                                                   f_end_year = fv_start_year+"-12-30";
                                                   $('input[name="transfers_start_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                   $('input[name="transfers_end_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                }
                                             }
                                       });

                                       var adjTable;
                                       $(document).ready(function () {
                                          adjTable = $('#dmpData').dataTable({
                                             "aaSorting": [[0, "desc"]],
                                             "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                                             "iDisplayLength": <?= $Settings->rows_per_page ?>,
                                             'bProcessing': true, 'bServerSide': true,
                                             'sAjaxSource': '<?= admin_url('products/getAdjustmentReport/?v=1&product='.$product->id); ?>',
                                             'fnServerData': function (sSource, aoData, fnCallback) {
                                                aoData.push({
                                                      "name": "<?= $this->security->get_csrf_token_name() ?>",
                                                      "value": "<?= $this->security->get_csrf_hash() ?>"
                                                },{
                                                      "name": "start_date",
                                                      "value": $('#adjustments_start_date').val()
                                                },{
                                                      "name": "end_date",
                                                      "value": $('#adjustments_end_date').val()
                                                });
                                                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                                             },
                                             "aoColumns": [{"mRender": fld}, null, null, null, {"mRender": decode_html}, {"bSortable": false, "mRender": pqFormat}, null],
                                             'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                                                // console.log(aData);
                                                nRow.id = aData[7];
                                                nRow.className = "adjustment_link2";
                                                return nRow;
                                             },
                                             "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                                var qty = 0;
                                                for (var i = 0; i < aaData.length; i++) {
                                                      qty += parseFloat(aaData[aiDisplay[i]][6]);
                                                }
                                                // console.log(nRow);
                                                var nCells = nRow.getElementsByTagName('th');
                                                nCells[6].innerHTML = currencyFormat(parseFloat(qty));
                                             }
                                          }).fnSetFilteringDelay().dtFilter([
                                             {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                             {column_number: 1, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                                             {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
                                             {column_number: 3, filter_default_label: "[<?=lang('created_by');?>]", filter_type: "text", data: []},
                                             {column_number: 4, filter_default_label: "[<?=lang('note');?>]", filter_type: "text", data: []},
                                          ], "footer");
                                       });


                                       $(document).on('click', '#adjustment_filter', function(){
                                             set_filter_to_all($('#adjustments_date_records_filter').val(), $('#adjustments_start_date').val(), $('#adjustments_end_date').val());
                                       });
                                    </script>
                                    <div class="box">
                                       <div class="box-header">
                                             <div class="box-icon">
                                                <ul class="btn-tasks">
                                                   <li class="dropdown"><a href="#" id="xls4" class="tip" title="<?= lang('download_xls') ?>"><i
                                                               class="icon fa fa-file-excel-o"></i></a></li>
                                                   <li class="dropdown"><a href="#" id="image4" class="tip image"
                                                                           title="<?= lang('save_image') ?>"><i
                                                               class="icon fa fa-file-picture-o"></i></a></li>
                                                </ul>
                                             </div>
                                       </div>
                                       <div class="box-content">
                                             <div class="row">
                                                <div class="col-lg-12">
                                                   <div class="ibox float-e-margins border-bottom">
                                                         <div class="ibox-title">
                                                            <h5><?= lang('filter') ?></h5>
                                                            <div class="ibox-tools">
                                                               <a class="collapse-link">
                                                                     <i class="fa fa-chevron-down"></i>
                                                               </a>
                                                            </div>
                                                         </div>
                                                         <div class="ibox-content" style="display: none;">
                                                            <div class="row">
                                                               <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                     <?= lang('date_records_filter', 'date_records_filter') ?>
                                                                     <select name="adjustments_date_records_filter" id="adjustments_date_records_filter" class="form-control">
                                                                        <?= $this->sma->get_filter_options(); ?>
                                                                     </select>
                                                               </div>

                                                               <div class="adjustments_date_controls">
                                                                     <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                                        <div class="col-sm-4 form-group">
                                                                           <?= lang('filter_year', 'filter_year_adjustments_h') ?>
                                                                           <select name="filter_year" id="filter_year_adjustments_h" class="form-control" required>
                                                                                 <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                                                 <?php endforeach ?>
                                                                           </select>
                                                                        </div>
                                                                     <?php endif ?>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('start_date', 'start_date') ?>
                                                                        <input type="date" name="adjustments_start_date" id="adjustments_start_date" value="<?= $this->filtros_fecha_inicial ?>" class="form-control">
                                                                     </div>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('end_date', 'end_date') ?>
                                                                        <input type="date" name="adjustments_end_date" id="adjustments_end_date" value="<?= $this->filtros_fecha_final ?>" class="form-control">
                                                                     </div>
                                                               </div>
                                                               <div class="clearfix"></div>
                                                               <div class="col-sm-3" style="margin-top: 5px;">
                                                                     <button class="btn btn-success" id="adjustment_filter"><?= lang('submit') ?></button>
                                                               </div>
                                                            </div>
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-12">
                                                   <p class="introtext"><?= lang('list_results'); ?></p>

                                                   <div class="">
                                                         <table id="dmpData" class="table table-bordered table-condensed table-hover">
                                                            <thead>
                                                            <tr>
                                                               <th class="col-xs-2"><?= lang("date"); ?></th>
                                                               <th class="col-xs-2"><?= lang("reference_no"); ?></th>
                                                               <th class="col-xs-2"><?= lang("warehouse"); ?></th>
                                                               <th class="col-xs-1"><?= lang("created_by"); ?></th>
                                                               <th><?= lang("note"); ?></th>
                                                               <th class="col-xs-2"><?= lang('products'); ?></th>
                                                               <th class="col-xs-2"><?= lang('quantity'); ?></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                               <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                                            </tr>
                                                            </tbody>
                                                            <tfoot class="dtFilter">
                                                            <tr class="active">
                                                               <th></th><th></th><th></th><th></th><th></th>
                                                               <th><?= lang('products'); ?></th>
                                                               <th><?= lang('quantity'); ?></th>
                                                            </tr>
                                                            </tfoot>
                                                         </table>
                                                   </div>
                                                </div>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php } ?>
                              <?php if ($this->Admin || $this->Owner || $this->GP['products-view_kardex']): ?>
                                 <div id="kardex" class="tab-pane fade">
                                    <script>
                                       $('#adjustments_date_records_filter').on('change', function(){
                                          filter = $(this).val();
                                          fecha_inicial = "";
                                          fecha_final = "<?= date('Y-m-d') ?>";
                                          hide_date_controls = true;
                                          if (filter == 5) { //RANGO DE FECHAS
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          } else if (filter == 1) { // HOY
                                             fecha_inicial = "<?= date('Y-m-d') ?>";
                                          } else if (filter == 2) { // MES
                                             fecha_inicial = "<?= date('Y-m-01') ?>";
                                          } else if (filter == 3) { // TRIMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
                                          } else if (filter == 8) { // SEMESTRE
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
                                          } else if (filter == 4) { // AÑO
                                             fecha_inicial = "<?= date('Y-01-01') ?>";
                                          } else if (filter == 6) { // MES PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                                             fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
                                          } else if (filter == 9) { // TRIMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 10) { // SEMESTRE PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                                             fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
                                          } else if (filter == 7) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                             fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
                                          } else if (filter == 11) { // AÑO PASADO
                                             fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
                                          } else if (filter == 0) {
                                             fecha_inicial = "";
                                             fecha_final = "";
                                             hide_date_controls = false;
                                          }
                                          $('#adjustments_start_date').val(fecha_inicial);
                                          $('#adjustments_end_date').val(fecha_final);
                                          if (hide_date_controls) {
                                             $('.adjustments_date_controls').css('display', 'none');
                                          } else {
                                             $('.adjustments_date_controls').css('display', '');
                                          }
                                       });

                                       $('#adjustments_date_records_filter').val("<?= $this->Settings->default_records_filter != 0 ? $this->Settings->default_records_filter : 5 ?>").trigger('change');

                                       $(document).on('change', '#adjustments_date_records_filter, #filter_year_adjustments_h', function(){
                                          if (site.settings.big_data_limit_reports == 1) {
                                             if ($('#adjustments_date_records_filter').val() == 5) {
                                                fv_start_year = $('#filter_year_adjustments_h').val();
                                                f_start_year = fv_start_year+"-01-01";
                                                f_end_year = fv_start_year+"-12-30";
                                                $('input[name="adjustments_start_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                                $('input[name="adjustments_end_date"]').prop('min', f_start_year).prop('max', f_end_year);
                                             }
                                          }
                                       });

                                       var kardexTable;

                                       function set_kardex(){
                                          $.ajax({
                                             url: '<?= admin_url('products/getKardexReport/?v=1&product='.$product->id); ?>',
                                             type : 'POST',
                                             dataType : 'JSON',
                                             data : {
                                                         "<?= $this->security->get_csrf_token_name() ?>" :  "<?= $this->security->get_csrf_hash() ?>",
                                                         "start_date" : $('#kardex_start_date').val(),
                                                         "end_date" : $('#kardex_end_date').val(),
                                                         "warehouse_id" : $('#kardex_warehouse_id').val(),
                                                         "option_id" : $('#option_filter_kardex').val(),
                                                      }
                                          }).done(function(data){ 
                                             console.log(data);
                                             response = data;
                                             $('.kardex_balance').text('<?= lang('kardex_product_balance') ?>'+response.balance);
                                             $('#kardexData tbody').html(response.html);
                                             kardexTable = $('#kardexData').dataTable({
                                                "aaSorting": [[0, 'asc']],
                                                "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                                                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                                                'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                                                   // console.log(aData);
                                                   // nRow.id = aData[5];
                                                   // nRow.className = "kardex_link2";
                                                   // return nRow;
                                                },
                                                "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                                                   var qty_in = 0, qty_out = 0, kbalance = 0;
                                                   for (var i = 0; i < aaData.length; i++) {
                                                      <?php if($variants && count($variants) > 0){ ?>
                                                         qty_in += parseFloat(aaData[aiDisplay[i]][8]);
                                                         qty_out += parseFloat(aaData[aiDisplay[i]][9]);
                                                         kbalance += parseFloat(aaData[aiDisplay[i]][10]);
                                                      <?php } else { ?>
                                                         qty_in += parseFloat(aaData[aiDisplay[i]][7]);
                                                         qty_out += parseFloat(aaData[aiDisplay[i]][8]);
                                                         kbalance += parseFloat(aaData[aiDisplay[i]][9]);
                                                      <?php } ?>    
                                                   }
                                                   var nCells = nRow.getElementsByTagName('th');
                                                   <?php if($variants && count($variants) > 0){ ?>
                                                      nCells[8].innerHTML = formatQuantity(parseFloat(qty_in));
                                                      nCells[9].innerHTML = formatQuantity(parseFloat(qty_out));
                                                      // nCells[10].innerHTML = formatQuantity(parseFloat(kbalance));
                                                   <?php } else { ?>
                                                      nCells[7].innerHTML = formatQuantity(parseFloat(qty_in));
                                                      nCells[8].innerHTML = formatQuantity(parseFloat(qty_out));
                                                      // nCells[9].innerHTML = formatQuantity(parseFloat(kbalance));
                                                   <?php } ?>  

                                                }
                                             }).fnSetFilteringDelay().dtFilter([
                                                {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                                {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
                                                {column_number: 2, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
                                                {column_number: 3, filter_default_label: "[<?=lang('company');?>]", filter_type: "text", data: []},
                                                {column_number: 4, filter_default_label: "[<?=lang('last_cost');?>]", filter_type: "text", data: []},
                                                {column_number: 5, filter_default_label: "[<?=lang('avg_cost');?>]", filter_type: "text", data: []},
                                                {column_number: 6, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
                                                <?php if($variants && count($variants) > 0){ ?>
                                                   {column_number: 7, filter_default_label: "[<?=lang('variant');?>]", filter_type: "text", data: []},
                                                <?php } ?>    
                                             ], "footer");
                                             $('#loading').fadeOut();
                                          });

                                       }


                                       $(document).ready(function () {
                                          set_kardex();
                                          $(document).on('click', '#kardex_filter', function(){
                                             $('#loading').fadeIn();
                                             set_filter_to_all($('#kardex_date_records_filter').val(), $('#kardex_start_date').val(), $('#kardex_end_date').val());
                                             command: toastr.warning('Por favor sea paciente, los datos se están cargando.', '¡Atención!', {
                                                "showDuration": "500",
                                                "hideDuration": "1000",
                                                "timeOut": "4000",
                                                "extendedTimeOut": "1000",
                                             });
                                          });
                                       });
                                    </script>
                                    <div class="box">
                                       <div class="box-header">
                                             <div class="box-icon">
                                                <ul class="btn-tasks">
                                                   <li class="dropdown"><a href="#" id="xls5" class="tip" title="<?= lang('download_xls') ?>"><i
                                                               class="icon fa fa-file-excel-o"></i></a></li>
                                                   <li class="dropdown"><a href="#" id="image5" class="tip image"
                                                                           title="<?= lang('save_image') ?>"><i
                                                               class="icon fa fa-file-picture-o"></i></a></li>
                                                </ul>
                                             </div>
                                       </div>
                                       <div class="box-content">
                                             <div class="row">
                                                <div class="col-lg-12">
                                                   <div class="ibox float-e-margins border-bottom">
                                                         <div class="ibox-title">
                                                            <h5><?= lang('filter') ?></h5>
                                                            <div class="ibox-tools">
                                                               <a class="collapse-link">
                                                                     <i class="fa fa-chevron-down"></i>
                                                               </a>
                                                            </div>
                                                         </div>
                                                         <div class="ibox-content" style="display: none;">
                                                            <div class="row">
                                                               <?php if ($variants && count($variants) > 0): ?>
                                                                     <div class="col-sm-4">
                                                                        <?= lang('variant', 'option_filter_kardex') ?>
                                                                        <select name="option_filter_kardex" class="form-control" id="option_filter_kardex">
                                                                           <option value="">Seleccione...</option>
                                                                           <?php foreach ($variants as $variant): ?>
                                                                                 <option value="<?= $variant->id ?>"><?=  $variant->name. "(".$variant->code. ")" ?></option>
                                                                           <?php endforeach ?>
                                                                        </select>
                                                                     </div>
                                                               <?php endif ?>
                                                               <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                     <?= lang('date_records_filter', 'date_records_filter') ?>
                                                                     <select name="kardex_date_records_filter" id="kardex_date_records_filter" class="form-control">
                                                                        <?= $this->sma->get_filter_options(); ?>
                                                                     </select>
                                                               </div>

                                                               <div class="kardex_date_controls">
                                                                     <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                                                        <div class="col-sm-4 form-group">
                                                                           <?= lang('filter_year', 'filter_year_kardex_h') ?>
                                                                           <select name="filter_year" id="filter_year_kardex_h" class="form-control" required>
                                                                                 <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                                                 <?php endforeach ?>
                                                                           </select>
                                                                        </div>
                                                                     <?php endif ?>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('start_date', 'start_date') ?>
                                                                        <input type="date" name="kardex_start_date" id="kardex_start_date" value="<?= $this->filtros_fecha_inicial ?>" class="form-control">
                                                                     </div>
                                                                     <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                                                        <?= lang('end_date', 'end_date') ?>
                                                                        <input type="date" name="kardex_end_date" id="kardex_end_date" value="<?= $this->filtros_fecha_final ?>" class="form-control">
                                                                     </div>
                                                               </div>

                                                               <div class="col-sm-4">
                                                                     <?= lang('warehouse', 'kardex_warehouse_id') ?>
                                                                     <?php
                                                                        $wh_opts[''] = lang('select');
                                                                        foreach ($warehouses as $warehouse) {
                                                                           $wh_opts[$warehouse->id] = $warehouse->name;
                                                                        }
                                                                     ?>
                                                                     <?php echo form_dropdown('kardex_warehouse_id', $wh_opts, (isset($_POST['kardex_warehouse_id']) ? $_POST['kardex_warehouse_id'] : ''), 'class="form-control" id="kardex_warehouse_id" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("kardex_warehouse_id") . '"'); ?>
                                                               </div>

                                                               <div class="clearfix"></div>
                                                               <div class="col-sm-3" style="margin-top: 5px;">
                                                                     <button class="btn btn-success" id="kardex_filter"><?= lang('submit') ?></button>
                                                               </div>
                                                            </div>
                                                         </div>
                                                   </div>
                                                </div>
                                                <div class="col-lg-12">
                                                   <p class="introtext"><?= lang('list_results'); ?></p>
                                                   <h4 class="kardex_balance"></h4>

                                                   <div class="">
                                                         <table id="kardexData" class="table table-bordered table-condensed table-hover">
                                                            <thead>
                                                            <tr>
                                                               <th><?= lang("registration_date"); ?></th>
                                                               <th><?= lang("document_date"); ?></th>
                                                               <th><?= lang("reference_no"); ?></th>
                                                               <th><?= lang("company"); ?></th>
                                                               <th><?= lang("cost")." ".lang('with_tax'); ?></th>
                                                               <th><?= lang("avg_cost")." ".lang('with_tax'); ?></th>
                                                               <th><?= lang("warehouse"); ?></th>
                                                               <?php if ($variants) { ?>
                                                                     <th><?= lang("variant"); ?></th>  
                                                               <?php } ?>
                                                               <th><?= lang("quantity_in"); ?></th>
                                                               <th><?= lang("quantity_out"); ?></th>
                                                               <th><?= lang("balance"); ?></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                            <tfoot class="dtFilter">
                                                            <tr class="active">
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                               <?php if ($variants) { ?>
                                                                     <th></th>  
                                                               <?php } ?>
                                                               <th></th>
                                                               <th></th>
                                                               <th></th>
                                                            </tr>
                                                            </tfoot>
                                                         </table>
                                                   </div>
                                                </div>
                                             </div>
                                       </div>
                                    </div>
                                 </div>
                              <?php endif ?>
                           <?php } ?>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
    <script type="text/javascript">
        $('#kardex_date_records_filter').on('change', function(){
            filter = $(this).val();
            fecha_inicial = "";
            fecha_final = "<?= date('Y-m-d') ?>";
            hide_date_controls = true;
            if (filter == 5) { //RANGO DE FECHAS
                fecha_final = "";
                hide_date_controls = false;
            } else if (filter == 1) { // HOY
                fecha_inicial = "<?= date('Y-m-d') ?>";
            } else if (filter == 2) { // MES
                fecha_inicial = "<?= date('Y-m-01') ?>";
            } else if (filter == 3) { // TRIMESTRE
                fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month")) ?>";
            } else if (filter == 8) { // SEMESTRE
                fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- ".$this->meses_para_semestre." month")) ?>";
            } else if (filter == 4) { // AÑO
                fecha_inicial = "<?= date('Y-01-01') ?>";
            } else if (filter == 6) { // MES PASADO
                fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 1 month")) ?>";
                fecha_final = "<?= date("Y-m-d",strtotime($this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date("Y-m-01")."- 1 month"))))) ?>";
            } else if (filter == 9) { // TRIMESTRE PASADO
                fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['inicio_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->trimestre_pasado['fin_trimestre'].'-01').($this->trimestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
            } else if (filter == 10) { // SEMESTRE PASADO
                fecha_inicial = "<?= date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['inicio_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : ''))) ?>";
                fecha_final = "<?= $this->sma->get_last_day_of_month(date("Y-m-d",strtotime(date('Y-'.$this->semestre_pasado['fin_semestre'].'-01').($this->semestre_pasado['año_pasado'] == 1 ? '- 1 year' : '')))) ?>";
            } else if (filter == 7) { // AÑO PASADO
                fecha_inicial = "<?= date("Y-01-01",strtotime(date("Y-m-01")."- 1 year")) ?>";
                fecha_final = "<?= date("Y-12-31",strtotime(date("Y-m-01")."- 1 year")) ?>";
            } else if (filter == 11) { // AÑO PASADO
                fecha_inicial = "<?= date("Y-m-d",strtotime(date("Y-m-01")."- 2 month")) ?>";
            } else if (filter == 0) {
                fecha_inicial = "";
                fecha_final = "";
                hide_date_controls = false;
            }
            $('#kardex_start_date').val(fecha_inicial);
            $('#kardex_end_date').val(fecha_final);
            if (hide_date_controls) {
                $('.kardex_date_controls').css('display', 'none');
            } else {
                $('.kardex_date_controls').css('display', '');
            }
        });
        $('#kardex_date_records_filter').val("<?= $this->Settings->default_records_filter != 0 ? $this->Settings->default_records_filter : 5 ?>").trigger('change');
        $(document).on('change', '#kardex_date_records_filter, #filter_year_kardex_h', function(){
            if (site.settings.big_data_limit_reports == 1) {
                if ($('#kardex_date_records_filter').val() == 5) {
                    fv_start_year = $('#filter_year_kardex_h').val();
                    f_start_year = fv_start_year+"-01-01";
                    f_end_year = fv_start_year+"-12-30";
                    $('input[name="kardex_start_date"]').prop('min', f_start_year).prop('max', f_end_year);
                    $('input[name="kardex_end_date"]').prop('min', f_start_year).prop('max', f_end_year);
                }
            }
        });
        $(document).ready(function () {
            $('#pdf').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getSalesReport/pdf/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#xls').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getSalesReport/0/xls/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#pdf1').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getQuotesReport/pdf/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#xls1').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getQuotesReport/0/xls/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#pdf2').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getPurchasesReport/pdf/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#xls2').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getPurchasesReport/0/xls/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#pdf3').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getTransfersReport/pdf/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#xls3').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getTransfersReport/0/xls/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#pdf4').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getAdjustmentReport/pdf/0/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#xls4').click(function (event) {
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getAdjustmentReport/0/xls/?v=1&product='.$product->id)?>";
                return false;
            });
            $('#xls5').click(function (event) {
               kardex_start_date = $('#kardex_start_date').val();
               kardex_end_date = $('#kardex_end_date').val();
               kardex_warehouse_id = $('#kardex_warehouse_id').val();
               let v = "&start_date="+kardex_start_date+"&end_date="+kardex_end_date+"&warehouse_id="+kardex_warehouse_id;
                event.preventDefault();
                window.location.href = "<?=admin_url('products/getKardexReport/xls/?v=1&product='.$product->id)?>"+v;
                return false;
            });
            $('.image').click(function (event) {
                var box = $(this).closest('.box');
                event.preventDefault();
                html2canvas(box, {
                    onrendered: function (canvas) {
                        openImg(canvas.toDataURL());
                    }
                });
                return false;
            });
        });
    </script>

<script type="text/javascript">

    function set_filter_to_all(filter, start_date, end_date) {
        $('#sales_date_records_filter').select2('val', filter);
        $('#quotes_date_records_filter').select2('val', filter);
        $('#purchases_date_records_filter').select2('val', filter);
        $('#transfers_date_records_filter').select2('val', filter);
        $('#adjustments_date_records_filter').select2('val', filter);
        $('#kardex_date_records_filter').select2('val', filter);
        $('#sales_start_date').val(start_date);
        $('#quotes_start_date').val(start_date);
        $('#purchases_start_date').val(start_date);
        $('#transfers_start_date').val(start_date);
        $('#adjustments_start_date').val(start_date);
        $('#kardex_start_date').val(start_date);
        $('#sales_end_date').val(end_date);
        $('#quotes_end_date').val(end_date);
        $('#purchases_end_date').val(end_date);
        $('#transfers_end_date').val(end_date);
        $('#adjustments_end_date').val(end_date);
        $('#kardex_end_date').val(end_date);
        $('.ibox-content').css('display', 'block');
        if (typeof adjTable !== 'undefined') adjTable.fnDraw();
        if (typeof tTable !== 'undefined') tTable.fnDraw();
        if (typeof pTable !== 'undefined') pTable.fnDraw();
        if (typeof qTable !== 'undefined') qTable.fnDraw();
        if (typeof sTable !== 'undefined') sTable.fnDraw();
        if (typeof osTable !== 'undefined') osTable.fnDraw();
        if (typeof kardexTable !== 'undefined') kardexTable.fnDestroy();
        set_kardex();
    }


</script>