<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?= admin_form_open('system_settings/update_product_prices_per_margin/', ['id' => 'update_product_prices_per_margin']) ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-sm-3">
                            <?= lang('price_groups', 'price_groups') ?>
                            <?php if ($price_groups) : ?>
                                <select name="price_groups" id="price_groups" class="form-control" required>
                                    <option value=""><?= lang('select') ?></option>
                                    <?php foreach ($price_groups as $pg) : ?>
                                        <option value="<?= $pg->id ?>"><?= $pg->name ?></option>
                                    <?php endforeach ?>
                                </select>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-3">
                            <?= lang('rounding_options', 'rounding_options') ?>
                            <select name="rounding_options" id="rounding_options" class="form-control" required>
                                <option value=""><?= lang('select') ?></option>
                                <option value="1">No redondear (Decimales del sistema)</option>
                                <option value="2">Redondear a enteros</option>
                                <option value="3">Redondear a cientos</option>
                                <option value="4">Redondear a miles</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <?= lang('choose_how_to_calculate_prices', 'choose_how_to_calculate_prices') ?>
                            <select name="choose_how_to_calculate_prices" id="choose_how_to_calculate_prices" class="form-control" required>
                                <option value=""><?= lang('select') ?></option>
                                <option value="1"><?= lang('manual_margin') ?></option>
                                <option value="2"><?= lang('categories_margin') ?></option>
                            </select>
                        </div>
                        <div class="col-sm-3 category_section" style="display: none;">
                            <?= lang('categories', 'categories') ?>
                            <?php $cnt = 0; ?>
                            <?php if ($categories) : ?>
                                <select name="categories[]" id="categories" class="form-control" multiple>
                                    <?php foreach ($categories as $cat) : ?>
                                        <?php if ($cat->profitability_margin_price_base > 0) : ?>
                                            <?php $cnt++; ?>
                                            <option value="<?= $cat->id ?>"><?= $cat->name ?></option>
                                        <?php endif ?>
                                    <?php endforeach ?>
                                </select>
                            <?php endif ?>
                            <?php if ($cnt == 0) : ?>
                                <em class="text-danger">Ninguna categoría del sistema tiene margen definido</em>
                            <?php endif ?>
                        </div>
                        <div class="col-sm-3 margin_section" style="display: none;">
                            <?= lang('margin', 'margin_percent') ?>
                            <input type="text" name="margin_percent" id="margin_percent" class="form-control only_number">
                        </div>
                        <div class="col-sm-3">
                            <?= lang('choose_formula_for_calculat_prices', 'formula') ?><br>
                            <span>
                                <input type="radio" name="formula" value="1" required>
                                Costo / ( 1 - Margen / 100)
                            </span><br>
                            <span>
                                <input type="radio" name="formula" value="2" required>
                                Costo * ( 1 + Margen / 100)
                            </span>
                        </div>
                        <div class="col-sm-3">
                            <?= lang('cost_option', 'cost_option') ?>
                            <select name="cost_option" id="cost_option" class="form-control" required>
                                <option value=""><?= lang('select') ?></option>
                                <option value="1">Último costo</option>
                                <option value="2">Costo promedio</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12" style="margin-top: 2%">
                            <button class="btn btn-success" type="button" id="send_update"><?= lang('submit') ?></button>
                        </div>
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>">
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table_response"> </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?= form_close() ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#update_product_prices_per_margin').validate({
            ignore: []
        });

        $(document).on('change', '#choose_how_to_calculate_prices', function() {
            type = $(this).val();

            if (type == 1) {
                $('.category_section').fadeOut();
                setTimeout(function() {
                    $('.margin_section').fadeIn();
                }, 400);
            } else if (type == 2) {
                $('.margin_section').fadeOut();
                setTimeout(function() {
                    $('.category_section').fadeIn();
                }, 400);
            }

        });

        $(document).on('click', '#send_update', function() {
            if ($('#update_product_prices_per_margin').valid()) {
                datos = $('#update_product_prices_per_margin').serialize();
                $.ajax({
                    url: site.base_url + "system_settings/update_product_prices_per_margin",
                    data: datos,
                    dataType: 'JSON',
                    type: 'POST',
                }).done(function(data) {
                    if (data.error == 0) {
                        $('.table_response').html(data.msg);
                    }
                });
            }
        });
    });
</script>