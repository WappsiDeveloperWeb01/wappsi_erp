<style type="text/css">
    
    tr.row_danger > td {
        background-color: #f2dede;
        color: red;
    }

    th.cat_name {
        background-color: #e2e2e2;
        padding: 0.3%;
    }

    table > tbody > tr > td {
        border-bottom-style: solid;
        border-bottom-width: 1px;
    }

    td.text-right {
        text-align: right;
    }
</style>
<div style='border:1px solid #DDD; padding:10px; margin:10px 0;'>
    <table style="width: 100%;font-family: Arial; font-size: 11px;border-spacing: 0px;padding: 0px;margin: 0px;" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th><?= lang('product_code') ?></th>
                <th><?= lang('quantity') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $curr_cat = NULL;
            foreach ($pdata as $data): 
                if ($curr_cat == NULL || $curr_cat != $data->cat_name) { ?>
                    <tr>
                        <th class="cat_name" colspan="2"><?= lang('category')." : " .ucfirst(mb_strtolower($data->cat_name)) ?></th>
                    </tr>
                <?php 
                $curr_cat = $data->cat_name;
                }
                ?>
                <tr <?= ($data->correct_quantity == 0 ? "class='row_danger'" : "") ?>>
                    <td><?= $data->code ?></td>
                    <td class="text-right"><?= $this->sma->formatQuantity($data->quantity) ?></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>

<script type="text/javascript">
    
        window.print();

</script>