<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$tipo_mov[1] = 'Activacion Modulos ';
$tipo_mov[2] = 'Plan Usuarios ';
$tipo_mov[3] = 'Plan Sucursales ';
$tipo_mov[4] = 'Documentos electronicos ';
$tipo_mov[5] = 'Plan Nómina ';
$tipo_mov[6] = 'Servicios recurrentes ';
$tipo_mov[7] = 'Productos recurrentes ';
$tipo_mov[8] = 'Productos y servicios NO recurrentes';
$tipo_mov[9] = 'Activación Sucursal Adicional';

 ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('wappsi_invoicing_detail_product'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3">
                    <label><?= lang('date') ?> : </label><br>
                    <span><?= $invoice->fecha ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('movement_type') ?> : </label><br>
                    <span><?= $tipo_mov[$invoice->tipo_mov] ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('reference_no') ?> : </label><br>
                    <span><?= $invoice->reference_no ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('description') ?> : </label><br>
                    <span><?= $invoice->descripcion ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label><?= lang('frecuency') ?> : </label><br>
                    <span><?= $this->sma->formatQuantity($invoice->frecuencia) ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('due_date') ?> : </label><br>
                    <span><?= $invoice->vencimiento ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('unit_quantity') ?> : </label><br>
                    <span><?= $this->sma->formatQuantity($invoice->cantidad_unidad) ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('quantity') ?> : </label><br>
                    <span><?= $this->sma->formatQuantity($invoice->cantidad_facturada) ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label><?= lang('net_price') ?> : </label><br>
                    <span><?= $this->sma->formatMoney($invoice->valor_neto) ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('tax') ?> : </label><br>
                    <span><?= $this->sma->formatMoney($invoice->valor_iva) ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('unit_price') ?> : </label><br>
                    <span><?= $this->sma->formatMoney($invoice->valor_total) ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('grand_total') ?> : </label><br>
                    <span><?= $this->sma->formatMoney($invoice->valor_total * $invoice->cantidad_facturada) ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <?php 
                    $pm_st[0] = 'Pendiente';
                    $pm_st[1] = 'Pagado';
                    $pm_st[3] = 'Mora';
                     ?>
                    <label><?= lang('payment_status') ?> : </label><br>
                    <span><?= $pm_st[$invoice->estado_pago] ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('payment_reference_no') ?> : </label><br>
                    <span><?= $invoice->pago_reference_no ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('payment_date') ?> : </label><br>
                    <span><?= $invoice->fecha_pago ?></span>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>