<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
if ($this->session->userdata('purchase_processing')) {
    $this->session->unset_userdata('purchase_processing');
}
?>
<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <?= admin_form_open('purchases', ['id' => 'purchases_filter']) ?>
                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <?php if ($this->Settings->purchase_datetime_management == 1) : ?>
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <div class="form-group">
                                                <label for=""><?= $this->lang->line('date_records_filter') ?></label>
                                                <?php $dateRecordsFilter = (isset($_POST["date_records_filter"]) ? $_POST["date_records_filter"] : ''); ?>
                                                <select class="form-control" name="date_records_filter" id="date_records_filter_dh">
                                                    <?= $this->sma->get_filter_options($dateRecordsFilter); ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <div class="form-group">
                                                <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                                <?php $dateRecordsFilter = (isset($_POST["date_records_filter"]) ? $_POST["date_records_filter"] : ''); ?>
                                                <select class="form-control" name="date_records_filter" id="date_records_filter_h">
                                                    <?= $this->sma->get_filter_options($dateRecordsFilter); ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('reference_no', 'purchase_reference_no') ?>
                                            <select name="purchase_reference_no" id="purchase_reference_no" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <?php if ($documents_types) : ?>
                                                    <?php foreach ($documents_types as $dt) : ?>
                                                        <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['purchase_reference_no']) && $_POST['purchase_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre . " (" . $dt->sales_prefix . ")" ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                            <?php 
                                                $biller_readonly = false;
                                                if ($this->session->userdata('biller_id')) {
                                                    $biller_selected = $this->session->userdata('biller_id');
                                                    $biller_readonly = true;
                                                }
                                            ?>
                                            <select class="form-control" id="biller_id" name="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label class="control-label" for="supplier"><?= lang("supplier"); ?></label>
                                            <?= form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'id="filter_supplier" data-placeholder="' . lang("alls") . '" class="form-control input-tip" style="width:100%;"'); ?>
                                        </div>
                                    </div>

                                    <?php if ($this->Owner || $this->Admin) : ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label><?= lang('user') ?></label>
                                                <select name="filter_user" id="filter_user" class="form-control">
                                                    <option value=""><?= lang('alls') ?></option>
                                                    <?php if ($users) : ?>
                                                        <?php foreach ($users as $user) : ?>
                                                            <option value="<?= $user->id ?>"><?= $user->first_name . " " . $user->last_name ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('status', 'status') ?>
                                            <select name="status" id="status" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <option value="completed" <?= isset($_POST['status']) && $_POST['status'] == 'completed' ? 'selected="selected"' : '' ?>><?= lang('completed') ?></option>
                                                <option value="pending" <?= isset($_POST['status']) && $_POST['status'] == 'pending' ? 'selected="selected"' : '' ?>><?= lang('pending') ?></option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                    <button class="btn btn-danger new-button" type="button" onclick="window.location.href = `${site.base_url}purchases`" data-toggle="tooltip" data-placement="bottom" title="<?= lang('reset') ?>"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row">
                                    
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang('payment_status', 'payment_status') ?>
                                            <select name="payment_status" id="payment_status" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <option value="pending" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'pending' ? 'selected="selected"' : '' ?>><?= lang('pending') ?></option>
                                                <option value="due" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'due' ? 'selected="selected"' : '' ?>><?= lang('due') ?></option>
                                                <option value="partial" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'partial' ? 'selected="selected"' : '' ?>><?= lang('partial') ?></option>
                                                <option value="paid" <?= isset($_POST['payment_status']) && $_POST['payment_status'] == 'paid' ? 'selected="selected"' : '' ?>><?= lang('paid') ?></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= lang('payment_term') ?></label>
                                            <input type="number" name="dias_vencimiento" id="dias_vencimiento" class="form-control" value="<?= isset($dias_vencimiento) ? $dias_vencimiento : '' ?>">
                                        </div>
                                    </div>

                                    <?php if ($this->Settings->manage_purchases_evaluation == 1): ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang('purchase_evaluated', 'purchase_evaluated') ?>
                                                <?php $purchaseEvaluated = (isset($_POST["purchase_evaluated"]) ? $_POST["purchase_evaluated"] : ''); ?>
                                                <select name="purchase_evaluated" id="purchase_evaluated" class="form-control">
                                                    <option value=""><?= lang('select') ?></option>
                                                    <option value="1" <?= $purchaseEvaluated == '1' ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                                    <option value="0" <?= $purchaseEvaluated == '0' ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <?php if ($this->Settings->purchase_datetime_management == 1) : ?>
                                        <div class="date_controls_dh">
                                            <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <?= lang('filter_year', 'filter_year_dh') ?>
                                                        <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                            <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                                <option value="<?= $key ?>"><?= $key ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                            <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                                <div class="form-group">
                                                    <?= lang('start_date', 'start_date') ?>
                                                    <input class="form-control datetime" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                                <div class="form-group">
                                                    <?= lang('end_date', 'end_date') ?>
                                                    <input class="form-control datetime" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>">
                                                </div>
                                            </div>
                                        </div>
                                    <?php else : ?>
                                        <div class="date_controls_h">
                                            <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                                <div class="col-sm-2">
                                                    <div class="form-group">
                                                        <?= lang('filter_year', 'filter_year_h') ?>
                                                        <select name="filter_year" id="filter_year_h" class="form-control" required>
                                                            <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                                <option value="<?= $key ?>"><?= $key ?></option>
                                                            <?php endforeach ?>
                                                        </select>
                                                    </div>
                                                </div>
                                            <?php endif ?>
                                            <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                                <div class="form-group">
                                                    <?= lang('start_date', 'start_date') ?>
                                                    <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                                <div class="form-group">
                                                    <?= lang('end_date', 'end_date') ?>
                                                    <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif ?>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('purchases/purchase_actions', 'id="action-form"');
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="POData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("id"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_origin"); ?></th>
                                            <th><?= lang("purchase_type"); ?></th>
                                            <th><?= lang("ref_no"); ?></th>
                                            <?php if ($this->Settings->management_consecutive_suppliers == 1) : ?>
                                                <th><?= lang("consecutive_supplier"); ?></th>
                                            <?php endif ?>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <th><?= lang("purchase_status"); ?></th>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("payment_status"); ?></th>
                                            <?php if ($this->Settings->documents_reception == YES) : ?>
                                                <th><?= lang("DIAN"); ?></th>
                                            <?php endif ?>
                                            <?php if ($this->Settings->manage_purchases_evaluation == 1) : ?>
                                                <th><?= lang("purchase_evaluation"); ?></th>
                                            <?php endif ?>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                            <th style="width:100px;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script>
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
    
    var dias_vencimiento;
    var purchase_reference_no;
    var biller;
    var filter_user;
    var supplier;
    var status;
    var payment_status;
    var filtered = 0;
    var filtered_ini_date;
    var purchase_evaluated;
    <?php if (isset($_POST['start_date'])) : ?>
        start_date: "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
    <?php endif ?>
    <?php if (isset($_POST['end_date'])) : ?>
        end_date: "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>",
    <?php endif ?>
    <?php if (isset($_POST['dias_vencimiento'])) : ?>
        dias_vencimiento = '<?= $_POST['dias_vencimiento'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['purchase_reference_no'])) : ?>
        purchase_reference_no = '<?= $_POST['purchase_reference_no'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['biller_id'])) : ?>
        biller = '<?= $_POST['biller_id'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['supplier'])) : ?>
        supplier = '<?= $_POST['supplier'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['status'])) : ?>
        status = '<?= $_POST['status'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['payment_status'])) : ?>
        payment_status = '<?= $_POST['payment_status'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['filtered'])) : ?>
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php endif ?>
    <?php if (isset($_POST['filter_user'])) : ?>
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['purchase_evaluated'])) : ?>
        purchase_evaluated = '<?= $_POST['purchase_evaluated'] ?>';
    <?php endif ?>

    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }

    $(document).ready(function() {
        $(document).on('change', '#date_records_filter_dh, date_records_filter_h', function() { showHidefilterContent($(this)) });

        oTable = $('#POData').dataTable({
            "aaSorting": [
                [1, "desc"],
                [2, "desc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('purchases/getPurchases' . ($warehouse_id ? '/' . $warehouse_id : '')) ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->sma->fld($this->filtros_fecha_inicial) ?>"
                }, {
                    "name": "end_date",
                    "value": "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->sma->fld($this->filtros_fecha_final)?>"
                }, {
                    "name": "dias_vencimiento",
                    "value": dias_vencimiento
                }, {
                    "name": "purchase_reference_no",
                    "value": purchase_reference_no
                }, {
                    "name": "biller",
                    "value": biller
                }, {
                    "name": "supplier",
                    "value": supplier
                }, {
                    "name": "status",
                    "value": status
                }, {
                    "name": "payment_status",
                    "value": payment_status
                }, {
                    "name": "user",
                    "value": filter_user
                }, {
                    "name": "purchase_evaluated",
                    "value": purchase_evaluated
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [{
                    "bSortable": false,
                    "mRender": checkbox
                },
                null,
                {
                    "mRender": fld
                },
                null,
                null,
                {"mRender": tag_reference},
                <?php if ($this->Settings->management_consecutive_suppliers == 1) : ?>
                    null,
                <?php endif ?>
                null,
                {
                    "mRender": row_status
                },
                {
                    "mRender": currencyFormat
                },
                {
                    "mRender": currencyFormat
                },
                {
                    "mRender": currencyFormat
                },
                {
                    "mRender": pay_status
                },
                <?php if ($this->Settings->documents_reception == YES) : ?> {
                        "className": "text-center",
                        render: function(data) {
                            if (data == 1) {
                                return '<i class="fa fa-check fa-lg text-success"></i>';
                            } else {
                                return '<i class="fa fa-ban fa-lg text-primary"></i>';
                            }
                        }
                    },
                <?php endif ?>
                <?php if ($this->Settings->manage_purchases_evaluation == YES) : ?> {
                        "className": "text-center",
                        render: function(data) {
                            if (data == 1) {
                                return '<i class="fa fa-check fa-lg text-success"></i>';
                            } else {
                                return '<i class="fa fa-ban fa-lg text-danger"></i>';
                            }
                        }
                    },
                <?php endif ?> {
                    "bSortable": false,
                    "mRender": attachment
                },
                {
                    "bSortable": false
                }
            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "purchase_link";
                return nRow;
            },
            drawCallback: function(settings) {
                // $('.actionsButtonContainer').html('<div class="dropdown pull-right" style="margin-top: 5px;">' +
                //     '<button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>' +
                //     '<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">' +
                //     '<li>' +
                //     '<a href="<?= admin_url('purchases/add') ?>">' +
                //     '<i class="fa fa-plus-circle"></i> <?= lang('add_purchase') ?>' +
                //     '</a>' +
                //     '</li>' +
                //     '<li>' +
                //     '<a href="#" id="excel" data-action="export_excel">' +
                //     '<i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>' +
                //     '</a>' +
                //     '</li>' +
                //     '<li>' +
                //     '<a href="#" id="combine" data-action="combine">' +
                //     '<i class="fa fa-file-pdf-o"></i> <?= lang('combine_to_pdf') ?>' +
                //     '</a>' +
                //     '</li>' +
                //     '<li>' +
                //     '<a href="#" id="sync_payments" data-action="sync_payments">' +
                //     '<i class="fa fa-dollar"></i> <?= lang('sync_payments') ?>' +
                //     '</a>' +
                //     '</li>' +
                //     '<li>' +
                //     '<a href="#" id="post_sale" data-action="post_purchase">' +
                //     '<i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?>' +
                //     '</a>' +
                //     '</li>' +
                //     '<li><a href="<?= admin_url('purchases/syncDocumentReception') ?>" id="syncDocumentsReception"><i class="fa fa-refresh"></i> <?= $this->lang->line('Sincronización Recepción de documentos') ?></a></li>' +
                //     '</ul>' +
                //     '</div>' +

                //     '<div class="dropdown pull-right" style="margin-top: 5px; margin-right: 5px;">' +
                //     <?php if (!empty($warehouses)) { ?> '<button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> Almacenes <span class="caret"></span> </button>' +
                //         '<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">' +
                //         '<li><a href="<?= admin_url('purchases') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>' +
                //         '<li class="divider"></li>' +
                //         '<?php
                //             foreach ($warehouses as $warehouse) {
                //                 echo '<li ' . ($warehouse_id && $warehouse_id == $warehouse->id ? 'class="active"' : '') . '><a href="' . admin_url('purchases/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                //             }
                //             ?>' +
                //         '</ul>' +
                //     <?php } ?> '</div>').css('padding-right', '0');

                //     $('[data-toggle-second="tooltip"]').tooltip();
                //     $('[data-toggle="tooltip"]').tooltip();

                    // ,
            // fnDrawCallback: function (oSettings) {
                // if (!_tabFilterFill) {
                //     $('.additionalControlsContainer').html('<div class="wizard">' +
                //         '<div class="steps clearfix">' +
                //             '<ul role="tablist">' +
                //                 '<li role="tab" class="first current index_list" aria-disabled="false"  aria-selected="true" style="width:16.6%;">' +
                //                     '<a  class="wizard_index_step" data-optionfilter="all" href="#">'+
                //                         '<span class="all_span">0</span><br><?= lang('allsf') ?>' +
                //                     '</a>' +
                //                 '</li>' +
                //                 '<li role="tab" class="done index_list" aria-disabled="false" style="width:16.6%;">' +
                //                     '<a class="wizard_index_step" data-optionfilter="returns" href="#" aria-controls="edit_biller_form-p-0">' +
                //                         '<span class="returns_span">0</span><br><?= lang('returns') ?>' +
                //                     '</a>' +
                //                 '</li>' +
                //                 '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                //                     '<a  class="wizard_index_step" data-optionfilter="notApproved" href="#">' +
                //                         '<span class="notApproved_span">0</span><br><?= lang('notApproved') ?>' +
                //                     '</a>' +
                //                 '</li>' +
                //                 '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                //                     '<a  class="wizard_index_step" data-optionfilter="pendingPayment" href="#">'+
                //                         '<span class="pendingPayment_span">0</span><br><?= lang('pendingPayment') ?>' +
                //                     '</a>' +
                //                 '</li>' +
                //                 '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                //                     '<a  class="wizard_index_step" data-optionfilter="pendingDian" href="#">'+
                //                         '<span class="pendingDian_span">0</span><br><?= lang('pendingDian') ?>' +
                //                     '</a>' +
                //                 '</li>' +
                //                 '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                //                     '<a  class="wizard_index_step" data-optionfilter="pendingDispatch" href="#">'+
                //                         '<span class="pendingDispatch_span">0</span><br><?= lang('pendingDispatch') ?>' +
                //                     '</a>' +
                //                 '</li>' +
                //             '</ul>' +
                //         '</div>' +
                //     '</div>').addClass('col-without-padding');

                //     _tabFilterFill = true;
                // }

                $('.actionsButtonContainer').html(`<a href="<?= admin_url('purchases/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>
                <div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li>
                            <a href="<?= admin_url('purchases/add') ?>">
                                <i class="fa fa-plus-circle"></i> <?= lang('add_purchase') ?>
                            </a>
                        </li>                   
                        <li>
                            <a href="#" id="excel" data-action="export_excel">
                                <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="combine" data-action="combine">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('combine_to_pdf') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="sync_payments" data-action="sync_payments">
                                <i class="fa fa-dollar"></i> <?= lang('sync_payments') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#" id="post_sale" data-action="post_purchase">
                                <i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?= admin_url('purchases/syncDocumentReception') ?>" id="syncDocumentsReception">
                                <i class="fa fa-refresh"></i> <?= $this->lang->line('Sincronización Recepción de documentos') ?>
                            </a>
                        </li>
                    </ul>
                </div>`);

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });

                // loadDataTabFilters();
            // }
            }
        });

        <?php if ($this->session->userdata('remove_pols')) { ?>
            if (localStorage.getItem('poitems')) {
                localStorage.removeItem('poitems');
            }
            if (localStorage.getItem('podiscount')) {
                localStorage.removeItem('podiscount');
            }
            if (localStorage.getItem('pobiller')) {
                localStorage.removeItem('pobiller');
            }
            if (localStorage.getItem('potax2')) {
                localStorage.removeItem('potax2');
            }
            if (localStorage.getItem('poshipping')) {
                localStorage.removeItem('poshipping');
            }
            if (localStorage.getItem('poref')) {
                localStorage.removeItem('poref');
            }
            if (localStorage.getItem('powarehouse')) {
                localStorage.removeItem('powarehouse');
            }
            if (localStorage.getItem('ponote')) {
                localStorage.removeItem('ponote');
            }
            if (localStorage.getItem('posupplier')) {
                localStorage.removeItem('posupplier');
            }
            if (localStorage.getItem('pocurrency')) {
                localStorage.removeItem('pocurrency');
            }
            if (localStorage.getItem('poextras')) {
                localStorage.removeItem('poextras');
            }
            if (localStorage.getItem('podate')) {
                localStorage.removeItem('podate');
            }
            if (localStorage.getItem('postatus')) {
                localStorage.removeItem('postatus');
            }
            if (localStorage.getItem('popayment_term')) {
                localStorage.removeItem('popayment_term');
            }
            if (localStorage.getItem('poretenciones')) {
                localStorage.removeItem('poretenciones');
            }
            if (localStorage.getItem('poorderdiscountmethod')) {
                localStorage.removeItem('poorderdiscountmethod');
            }
            if (localStorage.getItem('poorderdiscounttoproducts')) {
                localStorage.removeItem('poorderdiscounttoproducts');
            }
            if (localStorage.getItem('othercurrency')) {
                localStorage.removeItem('othercurrency');
            }
            if (localStorage.getItem('othercurrencycode')) {
                localStorage.removeItem('othercurrencycode');
            }
            if (localStorage.getItem('othercurrencytrm')) {
                localStorage.removeItem('othercurrencytrm');
            }
            if (localStorage.getItem('popayment_status')) {
                localStorage.removeItem('popayment_status');
            }
            if (localStorage.getItem('purchase_type')) {
                localStorage.removeItem('purchase_type');
            }
            if (localStorage.getItem('po_form_import')) {
                localStorage.removeItem('po_form_import');
            }
            if (localStorage.getItem('potag')) {
                localStorage.removeItem('potag');
            }
        <?php
            $this->sma->unset_data('remove_pols');
        }
        ?>

        <?php if ($this->session->userdata('remove_rels')) { ?>
            if (localStorage.getItem('reitems')) {
                localStorage.removeItem('reitems');
            }
            if (localStorage.getItem('rediscount')) {
                localStorage.removeItem('rediscount');
            }
            if (localStorage.getItem('retax2')) {
                localStorage.removeItem('retax2');
            }
            if (localStorage.getItem('reref')) {
                localStorage.removeItem('reref');
            }
            if (localStorage.getItem('rewarehouse')) {
                localStorage.removeItem('rewarehouse');
            }
            if (localStorage.getItem('renote')) {
                localStorage.removeItem('renote');
            }
            if (localStorage.getItem('reinnote')) {
                localStorage.removeItem('reinnote');
            }
            if (localStorage.getItem('resupplier')) {
                localStorage.removeItem('resupplier');
            }
            if (localStorage.getItem('redate')) {
                localStorage.removeItem('redate');
            }
            if (localStorage.getItem('rebiller')) {
                localStorage.removeItem('rebiller');
            }
            if (localStorage.getItem('repurchase')) {
                localStorage.removeItem('repurchase');
            }
            if (localStorage.getItem('reyear_database')) {
                localStorage.removeItem('reyear_database');
            }
        <?php $this->sma->unset_data('remove_rels');
        }
        ?>

        if (supplier != '') {
            $('#filter_supplier').val(supplier).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "suppliers/getsupplier/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });

            $('#filter_supplier').trigger('change');
        } else {
            $('#filter_supplier').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "suppliers/getsupplier/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });
        }

        if (filtered !== undefined) {
            setTimeout(function() {
                $('#start_date').val(start_date);
                $('#end_date').val(end_date);
                $('#dias_vencimiento').val(dias_vencimiento);
                $('#purchase_reference_no').select2('val', purchase_reference_no);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                $('#status').select2('val', status);
                $('#payment_status').select2('val', payment_status);
                // $('.collapse-link').click();
            }, 900);
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);

        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>

        $(document).on('click', '#syncDocumentsReception', function(event) { event.preventDefault(); confirmSync($(this)); });

    });


    function calcularMinutos(start_date, end_date) {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff / (1000 * 60);
        return minutos;
    }

    function setFilterText() {

        var reference_text = $('#purchase_reference_no option:selected').data('dtprefix');
        var biller_text = $('#biller option:selected').text();
        var supplier_text = $('#filter_supplier').select2('data') !== null ? $('#filter_supplier').select2('data').text : '';
        var status_text = $('#status option:selected').text();
        var payment_status_text = $('#payment_status option:selected').text();
        var start_date_text = $('<?= $this->Settings->purchase_datetime_management == 1 ? "#start_date_dh" : "#start_date_h" ?>').val();
        var end_date_text = $('<?= $this->Settings->purchase_datetime_management == 1 ? "#end_date_dh" : "#end_date_h" ?>').val();
        var text = "Filtros configurados : ";

        coma = false;

        if (purchase_reference_no != '' && purchase_reference_no !== undefined) {
            text += " Tipo documento (" + reference_text + ")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text += coma ? "," : "";
            text += " Sucursal (" + biller_text + ")";
            coma = true;
        }
        if (supplier != '' && supplier !== undefined) {
            text += coma ? "," : "";
            text += " Proveedor (" + supplier_text + ")";
            coma = true;
        }
        if (status != '' && status !== undefined) {
            text += coma ? "," : "";
            text += " Estado de compra (" + status_text + ")";
            coma = true;
        }
        if (payment_status != '' && payment_status !== undefined) {
            text += coma ? "," : "";
            text += " Estado de pago (" + payment_status_text + ")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha de inicio (" + start_date_text + ")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha final (" + end_date_text + ")";
            coma = true;
        }

        $('.text_filter').html(text);

    }

    function confirmSync(element)
    {
        swal({
            title: 'Sincronización con Recepción de Documentos',
            text: '¿Está seguro de realizar la sincronización?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Realizando sincronización...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            syncDocumentReception(element);
        });
    }

    function syncDocumentReception(element) {
        $.ajax({
            type: "post",
            url: element.attr('href'),
            data: {
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"
            },
            dataType: "json",
            success: function (response) {
                swal.close();
                Command: toastr.success(response.message, '<?= lang("toast_success_title"); ?>');
            }
        });
    }

    <?php if ($this->Settings->purchase_datetime_management == 1) : ?>
        $(document).ready(function() {
            setTimeout(function() {
                <?php if (!isset($_POST['date_records_filter'])) : ?>
                    $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                    $('#purchases_filter').submit();
                <?php elseif ($_POST['date_records_filter'] != $this->Settings->default_records_filter) : ?>
                    $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
                <?php endif ?>
            }, 150);
        });
    <?php else : ?>
        $(document).ready(function() {
            setTimeout(function() {
                <?php if (!isset($_POST['date_records_filter'])) : ?>
                    $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                    $('#sales_filter').submit();
                <?php elseif ($_POST['date_records_filter'] != $this->Settings->default_records_filter) : ?>
                    $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
                <?php endif ?>
            }, 150);
        });
    <?php endif ?>

    function showHidefilterContent(element)
    {
        var advancedFiltersContainer = '<?= $advancedFiltersContainer ? 1 : 0 ?>';

        var dateFilter = element.val();
        if (dateFilter != '') {
            if (dateFilter == 5) {
                $('#advancedFiltersContainer').fadeIn();
                $('.new-button-container .collapse-link').html('<i class="fa fa-lg fa-chevron-up"></i>');
            }
        }
    }
</script>