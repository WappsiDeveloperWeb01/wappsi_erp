<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .dfTable th, .dfTable td {
        text-align: center;
        vertical-align: middle;
    }

    .dfTable td {
        padding: 2px;
    }

    .data tr:nth-child(odd) td {
        color: #2FA4E7;
    }

    .data tr:nth-child(even) td {
        text-align: right;
    }
</style>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                <li class="dropdown">
                                    <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                        <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                                    </a>
                                </li>
                                </ul>
                            </div>
                            <div class="pull-right dropdown">
                                <?php if (!empty($warehouses) && !$this->session->userdata('warehouse_id')) { ?>
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> Almacenes <span class="caret"></span> </button>
                                    <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                        <li><a href="<?=admin_url('reports/monthly_purchases/0/'.$year)?>"><i class="fa fa-building-o"></i> <?=lang('all_warehouses')?></a></li>
                                        <li class="divider"></li>
                                        <?php
                                        foreach ($warehouses as $warehouse) {
                                            echo '<li><a href="' . admin_url('reports/monthly_purchases/'.$warehouse->id.'/'.$year) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                        }
                                        ?>
                                    </ul>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <br>
                            <div class="table-responsive">
                                <table class="table table-bordered dfTable reports-table">
                                    <thead>
                                    <tr class="year_roller">
                                        <th><a href="<?= admin_url('reports/monthly_purchases/'.($warehouse_id ? $warehouse_id : 0).'/'.($year-1)); ?>">&lt;&lt;</a></th>
                                        <th colspan="10"> <?php echo $year; ?></th>
                                        <th><a href="<?= admin_url('reports/monthly_purchases/'.($warehouse_id ? $warehouse_id : 0).'/'.($year+1)); ?>">&gt;&gt;</a></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/01'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_january"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/02'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_february"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/03'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_march"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/04'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_april"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/05'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_may"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/06'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_june"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/07'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_july"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/08'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_august"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/09'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_september"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/10'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_october"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/11'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_november"); ?>
                                            </a>
                                        </td>
                                        <td class="bold text-center">
                                            <a href="<?= admin_url('reports/monthly_profit/'.$year.'/12'); ?>" data-toggle="modal" data-target="#myModal">
                                                <?= lang("cal_december"); ?>
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <?php
                                        if (!empty($purchases)) {
                                            foreach ($purchases as $value) {
                                                $array[$value->date] = "<table class='table table-bordered table-hover table-condensed data' style='margin:0;'><tbody><tr><td>" . $this->lang->line("discount") . "</td></tr><tr><td>" . $this->sma->formatMoney($value->discount) . "</td></tr><tr><td>" . $this->lang->line("shipping") . "</td></tr><tr><td>" . $this->sma->formatMoney($value->shipping) . "</td></tr><tr><td>" . $this->lang->line("product_tax") . "</td></tr><tr><td>" . $this->sma->formatMoney($value->tax1) . "</td></tr><tr><td>" . $this->lang->line("order_tax") . "</td></tr><tr><td>" . $this->sma->formatMoney($value->tax2) . "</td></tr><tr><td>" . $this->lang->line("total") . "</td></tr><tr><td>" . $this->sma->formatMoney($value->total) . "</td></tr></tbody></table>";
                                            }
                                            for ($i = 1; $i <= 12; $i++) {
                                                echo '<td width="8.3%">';
                                                if (isset($array[$i])) {
                                                    echo $array[$i];
                                                } else {
                                                    echo '<strong>0</strong>';
                                                }
                                                echo '</td>';
                                            }
                                        } else {
                                            for ($i = 1; $i <= 12; $i++) {
                                                echo '<td width="8.3%"><strong>0</strong></td>';
                                            }
                                        }
                                        ?>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/monthly_purchases/'.($warehouse_id ? $warehouse_id : 0).'/'.$year.'/pdf')?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
    });
</script>
