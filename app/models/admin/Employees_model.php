<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Employees_model extends CI_Model {
    public function get()
    {
        $this->db->select("companies.id, LOWER(". $this->db->dbprefix('companies') .".name) AS name");
        $this->db->join("payroll_contracts", "payroll_contracts.companies_id = companies.id", "left");
        $this->db->join('groups', 'groups.id = companies.group_id', 'inner');
        $this->db->where("groups.name", "employee");
        $this->db->or_where("groups.name='seller' AND companies.mark_as_seller='1'");
        $this->db->where("payroll_contracts.contract_status IS NULL");
        $this->db->or_where("payroll_contracts.contract_status", "0");
        $this->db->order_by("companies.name");
        $q = $this->db->get("companies");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_by_id($id, $contract_id = NULL)
    {
        $this->db->select("companies.id AS employee_id, companies.*, employee_data.*, payroll_contracts.id AS contract_id, payroll_contracts.*, branch_office.name AS branch_name, payroll_areas.name AS area_name, bank.name AS bank_name, payroll_professional_position.name AS professional_position_name, payroll_types_employees.description AS description_types_employees, afp.name AS afp_name, eps.name AS eps_name, ces.name AS cesantia_name, arl.name AS arl_name, payroll_arl_risk_classes.name AS arl_risk_classes_name, payroll_arl_risk_classes.description AS arl_risk_classes_description, caja.name AS caja_name");
        $this->db->join("employee_data", "employee_data.companies_id = companies.id", "left");
        if (!empty($contract_id)) {
            $this->db->join("payroll_contracts", "payroll_contracts.id = {$contract_id}", "left");
        } else {
            $this->db->join("payroll_contracts", "payroll_contracts.companies_id = companies.id", "left");
        }
        $this->db->join("companies branch_office", "branch_office.id = payroll_contracts.biller_id", "left");
        $this->db->join("payroll_areas", "payroll_areas.id = payroll_contracts.area", "left");
        $this->db->join("payroll_professional_position", "payroll_professional_position.id = payroll_contracts.professional_position", "left");
        $this->db->join("companies bank", "bank.id = payroll_contracts.bank", "left");
        $this->db->join("payroll_types_employees", "payroll_types_employees.code = payroll_contracts.employee_type", "left");
        $this->db->join("companies afp", "afp.id = payroll_contracts.afp_id", "left");
        $this->db->join("companies eps", "eps.id = payroll_contracts.eps_id", "left");
        $this->db->join("companies ces", "ces.id = payroll_contracts.cesantia_id", "left");
        $this->db->join("companies arl", "arl.id = payroll_contracts.arl_id", "left");
        $this->db->join("companies caja", "caja.id = payroll_contracts.caja_id", "left");
        $this->db->join("payroll_arl_risk_classes", "payroll_arl_risk_classes.id = payroll_contracts.arl_risk_classes", "left");
        $this->db->where("companies.id", $id);
        $q = $this->db->get("companies");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_without_contracts($employee_id = NULL, $contract_id = NULL)
    {
        $ids = $this->getEmployeeWithContracts();

        $this->db->select("companies.id, LOWER(". $this->db->dbprefix('companies') .".name) AS name");
        $this->db->join("payroll_contracts", "payroll_contracts.companies_id = companies.id", "left");
        $this->db->join('groups', 'groups.id = companies.group_id', 'inner');
        $this->db->where("(groups.name = 'employee' OR (groups.name = 'seller' AND ". $this->db->dbprefix("companies") .".mark_as_seller = '1'))");
        $this->db->where("(payroll_contracts.contract_status = ".INACTIVE." OR payroll_contracts.contract_status IS NULL)");

        if (!empty($ids->ids)) {
            $this->db->where($this->db->dbprefix("companies") .".id NOT IN ($ids->ids)");
        }

        $q = $this->db->get("companies");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getEmployeeWithContracts($employee_id = NULL)
    {
        $this->db->select("GROUP_CONCAT(". $this->db->dbprefix("companies") .".id) AS ids");
        $this->db->join("payroll_contracts", "payroll_contracts.companies_id = companies.id", "left");
        $this->db->join('groups', 'groups.id = companies.group_id', 'inner');
        $this->db->where("(groups.name = 'employee' OR (groups.name = 'seller' AND ". $this->db->dbprefix("companies") .".mark_as_seller = '1'))");
        $this->db->where("payroll_contracts.contract_status", ACTIVE);

        if (!empty($employee_id)) {
            $this->db->where("payroll_contracts.companies_id", $employee_id);
        }

        $q = $this->db->get("companies");

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

	public function get_documenttypes()
	{
        $q = $this->db->get('documentypes');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_document_types_by_id($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get('documentypes');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_countries()
    {
        $q = $this->db->get('countries');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_types_regime()
    {
        $q = $this->db->get("types_vat_regime");
        if ($q->num_rows() > 0 ) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_types_regime_by_id($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("types_vat_regime");
        if ($q->num_rows() > 0 ) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_document_number($document_number, $id = NULL)
    {
        if (!empty($id)) { $this->db->where('companies.id !=', $id); }
        $this->db->where('groups.id = ', 8);
        $this->db->join('groups', 'groups.id = companies.group_id', 'inner');
        $q = $this->db->where("vat_no", $document_number)->get("companies");
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_email($email) {
        $this->db->where("email", $email);
        $this->db->join("groups", "groups.id = companies.group_id AND groups.name = 'employee'", "inner");
        $q = $this->db->get("companies");
        if ($q->num_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_states_by_country($country)
    {
        $q = $this->db->where('PAIS', $country)->get('states');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_cities_by_state_id($state)
    {
        $q = $this->db->where('CODDEPARTAMENTO', $state)->get('cities');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_zones_by_city_id($city)
    {
        $q = $this->db->where('codigo_ciudad', $city)->get('zones');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_employee_contacts($id)
    {
        $q = $this->db->where("companies_id", $id)->get("employee_contacts");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_employee_data($employee_id)
    {
        $q = $this->db->where("companies_id", $employee_id)->get("employee_data");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_group_data_for_employees()
    {
        $this->db->select('id, name');
        $this->db->where('name', 'employee');
        $q = $this->db->get('groups');
        return $q->row();
    }

    public function get_group_by_name($name) {
        $this->db->where('name', $name);
        $q = $this->db->get('groups');

        return $q->row();
    }

    public function get_existing_user_transactions($employee_id)
    {
        $this->db->where('biller_id', $employee_id);
        if ($this->db->count_all_results('sales') > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getEmployeeSuggestions($term, $limit = 10)
    {
        $this->db->select("id, (CASE WHEN company = '-' THEN name ELSE CONCAT(name) END) as text, (CASE WHEN company = '-' THEN name ELSE CONCAT(name) END) as value", FALSE);
        $this->db->where(" (id LIKE '%" . $term . "%'
        OR name LIKE '%" . $term . "%'
        OR company LIKE '%" . $term . "%'
        OR email LIKE '%" . $term . "%'
        OR phone LIKE '%" . $term . "%'
        OR vat_no LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('companies', array('group_name' => 'employee', 'status' => 1), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function get_types_contracts()
    {
        $this->db->select("*");
        $q = $this->db->get("payroll_types_contracts");
        return $q->result();
    }

    public function insert_companies($data)
    {
        if ($this->db->insert("companies", $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function insert_employees_data($data)
    {
        if ($this->db->insert("employee_data", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function insert_employees_contact($data)
    {
        if ($this->db->insert_batch("employee_contacts", $data)){
            return TRUE;
        }
        return FALSE;
    }

    public function update_companies($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("companies", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_employee_data($data, $employee_id)
    {
        $this->db->where("companies_id", $employee_id);
        if ($this->db->update("employee_data", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_employee_contacts($data, $employee_id)
    {
        if ($this->delete_employee_contacts($employee_id) == TRUE) {
            if ($this->insert_employees_contact($data) == TRUE) {
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        if ($this->db->delete('companies')) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_employee_data($employee_id)
    {
        $this->db->where("companies_id", $employee_id);
        if ($this->db->delete("employee_data")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_employee_contacts($employee_id)
    {
        $this->db->where("companies_id", $employee_id);
        if ($this->db->delete("employee_contacts")) {
            return TRUE;
        }
        return FALSE;
    }
}

/* End of file Employees_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/Employees_model.php */