<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        if (file_exists('assets/uploads/logos/'.$this->logo)) {
            if ($this->biller_logo == 2) {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,27,7,28);
            } else {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,12,55);
            }
        }

        if ($this->show_document_type_header == 1) {
            $this->SetFont('Arial','',$this->fuente);
            $this->setXY(13, 36);
            $this->MultiCell(123, 3,  $this->sma->utf8Decode(strip_tags($this->sma->decode_html($this->invoice_header))), 0, 'L');
        }

        $cx = 75;
        $cy = 10;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->MultiCell(75, 3.5,  $this->sma->utf8Decode(($this->biller->company != "-" ? $this->biller->company : $this->biller->name)), '', 'L');
        $cy  = $this->getY();
        $cy+=1.3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion != "" ? '-'.$this->biller->digito_verificacion : '')."  ".$this->tipo_regimen),'',1,'L');
        if ($this->ciiu_code) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode("Códigos CIIU : ".$this->ciiu_code),'',1,'L');
        }
        if ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode(
                                                ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? 'Auto retenedor de ' : '').
                                                ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                            ),'',1,'L');
        }
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(isset($this->Settings->url_web) ? $this->Settings->url_web : ''),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+(6));
        $this->RoundedRect(138, 10, 71, 31, 3, '1234', '');
        $cx = 140;
        $cy = 15;
        $this->setXY($cx, $cy);
        $this->MultiCell(40, 5,  $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'FACTURA DE VENTA'), '', 'L');
        $cy +=13;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'L');
        if (!empty($this->factura->cufe)) {
            $this->Image(base_url().'themes/default/admin/assets/images/qr_code/'.$this->factura->reference_no.'.png', 180, 12.5, 26);
        }

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 43, 125, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 48, 125, 27, 3, '4', '');
        $cx = 13;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('Información del cliente'),'B',1,'C');
        $cx += 3;
        $cy += 6;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 100;
        $cy += 4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Multicell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio),0,'L');

        // $cy += 3;
        $this->setX($cx);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode(" "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Multicell($columns_r, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->customer->city))." - ".(ucfirst(mb_strtolower($this->customer->state)))),0,'L');
        // $cy += 3;

        // $cy += 3;
        $this->setX($cx);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',1,'L');
        // $cy += 3;
        $this->setX($cx);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->email_negocio),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $altura = 5;
        $adicional_altura = 1;
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(138, 43, 71, 5, 3, '2', 'DF');
        $this->RoundedRect(138, 48, 71, 27, 3, '3', '');
        $cx = 138;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Fecha Factura'),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Total'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, ($this->factura->grand_total - $this->factura->rete_fuente_total - $this->factura->rete_iva_total - $this->factura->rete_ica_total - $this->factura->rete_other_total - ($this->factura->rete_bomberil_id && $this->factura->rete_bomberil_total > 0 ? $this->factura->rete_bomberil_total : 0) - ($this->factura->rete_autoaviso_id && $this->factura->rete_autoaviso_total > 0 ? $this->factura->rete_autoaviso_total : 0)) * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Fecha Vencimiento'),'LBR',1,'C',1);
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Forma de Pago'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura+$adicional_altura , $this->sma->utf8Decode($this->sale_payment_method),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 35.5;
        $this->setXY($cx, $cy);
        if (!empty($this->factura->purchase_order)) {
            $this->Cell(35.5, $altura , $this->sma->utf8Decode('Vendedor'),'TLBR',1,'C',1);
            $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
            $cx += 35.5;
            $this->setXY($cx, $cy);
            $this->Cell(35.5, $altura , $this->sma->utf8Decode('Orden de compra'),'LBRT',1,'C',1);
            $cx -= 35.5;
            $cy += $altura;
            $this->setXY($cx, $cy);
            $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
            if ($this->seller) {
                $this->Cell(35.5, $altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'R',0,'C');
            } else {
                $this->Cell(35.5, $altura , $this->sma->utf8Decode('Ventas Varias'),'',0,'C');
            }
            $this->Cell(35.5, $altura , $this->sma->utf8Decode($this->factura->purchase_order),'',1,'C');
        } else {
            $this->Cell(71, $altura , $this->sma->utf8Decode('Vendedor'),'TLBR',1,'C',1);
            $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
            $cy += $altura;
            $this->setXY($cx, $cy);
            if ($this->seller) {
                $this->Cell(71, $altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
            } else {
                $this->Cell(71, $altura , $this->sma->utf8Decode('Ventas Varias'),'',1,'C');
            }
        }

        $this->ln(1.7);

        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $this->Cell(9.6 , 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(15.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(49.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(19.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(19.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(19.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(11.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10  , 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(19.6, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        $this->Cell(9.6 , 8, $this->sma->utf8Decode('Ítem'),'',0,'C',0);
        $this->Cell(15.6, 8, $this->sma->utf8Decode('Código'),'',0,'C',0);
        $this->Cell(49.6, 8, $this->sma->utf8Decode('Nombre producto'),'',0,'C',0);
        $this->Cell(19.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode('Dcto'),'',0,'C',0);
        $this->Cell(19.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(lang('iva')),'',0,'C',0);
        $this->Cell(19.6, 4, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(11.6, 8, $this->sma->utf8Decode('Cantidad'),'',0,'C',0);
        $this->Cell(10  , 8, $this->sma->utf8Decode('UM'),'',0,'C',0);
        $this->Cell(19.6, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        $this->Cell(9.6 , 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(15.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(49.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(19.6, 4, $this->sma->utf8Decode('Bruto'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(19.6, 4, $this->sma->utf8Decode('Con dcto.'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(19.6, 4, $this->sma->utf8Decode(lang('iva_included')),'',0,'C',0);
        $this->Cell(11.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(10  , 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(19.6, 4, $this->sma->utf8Decode(''),'',1,'C',0);
    }

    function Footer()
    {
        // Print centered page number
        if ($this->description2 != '') {
            // $this->SetXY(15, -70);
            $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
            $this->SetXY(13, -24);
            $this->RoundedRect(13, 254, 196, 10, 0.8, '1234', '');
            $this->Cell(196, 4 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            if (!empty($this->factura->cufe)) {
                $this->Cell(196, 4 , $this->sma->utf8Decode('CUFE: '. $this->factura->cufe),'',1,'C');
            }
        } else {
            $this->SetXY(13, -23);
            $this->RoundedRect(13, 255, 196, 10, 1, '1234', '');
            $this->Cell(196, 4 , $this->sma->utf8Decode($this->factura->resolucion),'',1,'C');
            if (!empty($this->factura->cufe)) {
                $this->Cell(196, 4 , $this->sma->utf8Decode("CUFE : ".$this->factura->cufe),'',1,'C');
            }
        }

        $this->SetXY(13, -14);
        $img_x = $this->getX()+125;
        $img_y = $this->getY()+1;
        $this->Cell(125, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS NIT 901.090.070-9     www.wappsi.com'),'',1,'L');
        if (!empty($this->factura->cufe)) {
            $this->Image(base_url($this->technologyProviderLogo), $img_x, $img_y, 45);
        }
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Página N° '.$this->PageNo()),'',1,'C');
    }

    function reduceTextToDescription1($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 715) {
            $text = substr($text, 0, 720);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        $max_size = 3500;
        if (strlen($text) > $max_size) {
            $text = substr($text, 0, ($max_size - 5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 1.5;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->value_decimals = $value_decimals;
$pdf->ciiu_code = $ciiu_code;
$pdf->invoice_header = $invoice_header ? $invoice_header : "";
$pdf->show_document_type_header = $show_document_type_header;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;
$pdf->technologyProviderLogo = $technologyProviderLogo;
$pdf->AddPage();

$maximo_footer = 240;

$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);

$taxes = [];
$total_bruto = 0;
$references = [];
$inv_total_before_promo = 0;

    foreach ($rows as $item) {

        if ($product_detail_promo == 1 && $item->price_before_promo > 0) {
            $inv_total_before_promo = ($item->price_before_promo - ($item->tax_method == 1 ? $item->net_unit_price : $item->unit_price)) * $item->quantity;
        }

        $total_bruto += (($item->net_unit_price + ($item->item_discount / $item->quantity)) * $item->quantity) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }

        if (isset($references[$item->id])) {
            $references[$item->id]['pr_subtotal'] += $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] += $item->quantity;
        } else {
            $references[$item->id]['pr_code'] = ($show_code == 1 ? $item->product_code : $item->reference);
            $pr_name = $pdf->sma->reduce_text_length(($item->under_cost_authorized == 1 ? '     ' : '').
                                                $item->product_name.
                                                (!is_null($item->variant) && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? "( ".$item->variant." )" : '').
                                                ($pdf->Settings->show_brand_in_product_search && !is_null($item->brand_name) ? " - ".$item->brand_name : '').
                                                (!is_null($item->serial_no) ? " ".$item->serial_no : '').
                                                ($product_detail_promo == 1 && $item->price_before_promo > 0 ? " (".lang('discount_by_promo')." -".$this->sma->formatDecimal((($item->price_before_promo - ($item->tax_method == 1 ? $item->net_unit_price : $item->unit_price)) / $item->price_before_promo) * 100, 2)." %)" : '' ).
                                                strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')))
                                            , 500);
            $references[$item->id]['pr_name'] = $pr_name;
            $references[$item->id]['pr_discount'] = $item->discount;
            $references[$item->id]['pr_individual_discount'] = $item->item_discount / $item->quantity;
            $references[$item->id]['pr_tax'] = $item->tax != '' ? $item->tax : 0;
            $references[$item->id]['pr_individual_tax'] = $item->item_tax / $item->quantity;
            $references[$item->id]['unit_price_1'] = ($item->net_unit_price + ($item->item_discount / $item->quantity)) * $trmrate;
            $references[$item->id]['unit_price_2'] = $item->net_unit_price * $trmrate;
            $references[$item->id]['unit_price_3'] = $item->unit_price * $trmrate;
            $references[$item->id]['pr_subtotal'] = $item->subtotal * $trmrate;
            $references[$item->id]['pr_quantity'] = $item->quantity;
            $references[$item->id]['operation_value'] = $item->operation_value;
            $references[$item->id]['under_cost_authorized'] = $item->under_cost_authorized;
            $references[$item->id]['operator'] = $item->operator;
            $references[$item->id]['product_unit_code'] = $item->product_unit_code;
        }
    }

    $cnt = 1;
// for ($i=0; $i < 15 ; $i++) {

    $total_items_qty = 0;

    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }
        $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $columns[0]['inicio_x'] = $pdf->getX();
        $pdf->Cell( 9.6, 5, $this->sma->utf8Decode($cnt),'',0,'C',0);
        $columns[0]['fin_x'] = $pdf->getX();
        $columns[1]['inicio_x'] = $pdf->getX();
        $pdf->Cell(15.6, 5, $this->sma->utf8Decode($data['pr_code']),'',0,'C',0);
        $columns[1]['fin_x'] = $pdf->getX();
        $columns[2]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.5);
        if ($data['under_cost_authorized'] == 1) {
            $unlock_icon_X = $pdf->getX();
            $unlock_icon_Y = $pdf->getY();
            $pdf->Image(base_url().'assets/images/unlock-icon.png',$unlock_icon_X+1, $unlock_icon_Y+0.6,2.6);
        }
        $pdf->MultiCell(49.6,3, $this->sma->utf8Decode($data['pr_name']),'','L');
        $columns[2]['fin_x'] = $cX+49.6;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+49.6, $cY);
        $columns[3]['inicio_x'] = $pdf->getX();
        $pdf->Cell(19.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_1'])),'',0,'C',0);
        $columns[3]['fin_x'] = $pdf->getX();
        $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_discount']),'',0,'C',0);
        $columns[4]['fin_x'] = $pdf->getX();
        $columns[5]['inicio_x'] = $pdf->getX();
        $pdf->Cell(19.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_2'])),'',0,'C',0);
        $columns[5]['fin_x'] = $pdf->getX();
        $columns[6]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_tax']),'',0,'C',0);
        $columns[6]['fin_x'] = $pdf->getX();
        $columns[7]['inicio_x'] = $pdf->getX();
        $pdf->Cell(19.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_3'])),'',0,'C',0);
        $columns[7]['fin_x'] = $pdf->getX();

        // INICIO
        $pr_quantity = $data['pr_quantity'];
        $pr_unit_code = NULL;
        if (isset($data['operator'])) {
            if ($data['operator'] == "*") {
                $pr_quantity = $data['pr_quantity'] / $data['operation_value'];
            } else if ($data['operator'] == "/") {
                $pr_quantity = $data['pr_quantity'] * $data['operation_value'];
            } else if ($data['operator'] == "+") {
                $pr_quantity = $data['pr_quantity'] - $data['operation_value'];
            } else if ($data['operator'] == "-") {
                $pr_quantity = $data['pr_quantity'] + $data['operation_value'];
            }
        }
        $pr_unit_code = $data['product_unit_code'];
        $columns[8]['inicio_x'] = $pdf->getX();
        $pdf->Cell(11.6, 5, $this->sma->utf8Decode($this->sma->formatQuantity($pr_quantity, $qty_decimals)),'',0,'C',0);
        $total_items_qty += $data['pr_quantity'];
        $columns[8]['fin_x'] = $pdf->getX();
        $columns[9]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10, 5, $this->sma->utf8Decode($pr_unit_code),'',0,'C',0);
        $total_items_qty += $data['pr_quantity'];
        $columns[9]['fin_x'] = $pdf->getX();
        // FIN

        $columns[10]['inicio_x'] = $pdf->getX();
        $pdf->Cell(19.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['pr_subtotal'])),'',0,'C',0);
        $columns[10]['fin_x'] = $pdf->getX();
            $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
            foreach ($columns as $key => $data) {
                $ancho_column = $data['fin_x'] - $data['inicio_x'];
                if ($altura_fila < 5) {
                    $altura_fila = 5;
                }
                $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
            }
            $pdf->ln($altura_fila);
        $cnt++;
    }

    if ($document_type_invoice_format && $document_type_invoice_format->totalize_quantity == 1) {
        $pdf->Cell(162.8, 5, $this->sma->utf8Decode('Total cantidades'),'LB',0,'R');
        $pdf->Cell(11.6, 5, $this->sma->utf8Decode($this->sma->formatQuantity($total_items_qty)),'LB',0,'C');
        $pdf->Cell(21.6, 5, $this->sma->utf8Decode(''),'LBR',1,'R');
    }

// }

if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

// $pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(115.6,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->SetFont('Arial','',$fuente);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente);
$pdf->MultiCell(115.6, 3, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','B',$fuente+$adicional_fuente);
$pdf->setX(13);
$pdf->setXY($current_x, $current_y+8);


$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(113.4,0, $this->sma->utf8Decode(''),'B',1,'C');
$pdf->Cell(28.89,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
$pdf->Cell(28.89,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
$pdf->Cell(28.89,5, $this->sma->utf8Decode('Valor Impuesto'),'',0,'C');
$pdf->Cell(28.89,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');

$pdf->SetFont('Arial','',$fuente);
$total_tax_base = 0;
$total_tax_amount = 0;

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $total =$arr['base'] + $arr['tax'];
    $pdf->Cell(28.89,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');
    $pdf->setXY($current_x + 28.89, $current_y);
    $pdf->setX($current_x + 28.89);
    $pdf->Cell(28.89,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['base'] * $trmrate)),'',1,'C');
    $total_tax_base += $arr['base'] * $trmrate;
    $pdf->setXY($current_x + (28.89 * 2), $current_y);
    $pdf->setX($current_x + (28.89 * 2));
    $pdf->Cell(28.89,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['tax'] * $trmrate)),'',1,'C');
    $total_tax_amount += $arr['tax'] * $trmrate;
    $pdf->setXY($current_x + (28.89 * 3), $current_y);
    $pdf->setX($current_x + 28.89 * 3);
    $pdf->Cell(28.89,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total)),'',1,'C');
}

if ($inv->order_tax > 0) {

    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $pdf->Cell(28.9,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)),'',1,'C');
    $total_tax_base +=  $inv->total * $trmrate;

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax * $trmrate)),'',1,'C');
    $total_tax_amount += $inv->order_tax * $trmrate;

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total * $trmrate)),'',1,'C');
}

if ($inv->consumption_sales > 0) {

    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $pdf->Cell(28.9,3, $this->sma->utf8Decode('Impoconsumo (L&C)'),'',1,'C');
    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode(''),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->consumption_sales * $trmrate)),'',1,'C');
    $total_tax_amount += $inv->consumption_sales * $trmrate;

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->consumption_sales * $trmrate)),'',1,'C');

}


$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(28.9,3, $this->sma->utf8Decode("Total"),'',1,'C');
$pdf->setXY($current_x + 28.9, $current_y);
$pdf->setX($current_x + 28.9);
$pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base)),'',1,'C');

$pdf->setXY($current_x + (28.9 * 2), $current_y);
$pdf->setX($current_x + (28.9 * 2));
$pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_amount)),'',1,'C');

$pdf->setXY($current_x + (28.9 * 3), $current_y);
$pdf->setX($current_x + 28.9 * 3);
$pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base + $total_tax_amount)),'',1,'C');
$pdf->SetFont('Arial','',$fuente);

$tax_summary_end = $pdf->getY() + 3;

//derecha

$pdf->setXY($cX_items_finished+124, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

// $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Total bruto'), 'BLR', 1, 'L');
// $pdf->setXY($current_x+33.2, $current_y);
// $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_bruto)), 'BR', 1, 'R');

// $current_y+=5;

if ($inv->product_discount > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Total bruto'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->total+ $inv->product_discount ) * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

if ($inv->product_discount > 0) {
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Descuento a productos'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->product_discount > 0 ? "- " : "").$this->sma->formatValue($value_decimals, ($inv->product_discount) * $trmrate)), 'TBR', 1, 'R');
    $current_y+=5;
}

$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total_tax * $trmrate)), 'TBR', 1, 'R');

if ($inv->order_discount > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Descuento general'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->order_discount > 0 ? "- " : "").$this->sma->formatValue($value_decimals, ($inv->order_discount) * $trmrate)), 'TBR', 1, 'R');
}


if ($inv->rete_fuente_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención en la fuente'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_fuente_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');
}


if ($inv->rete_iva_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención al IVA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_iva_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Retención al ICA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_ica_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode(' '.lang('rete_other')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_other_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_bomberil_id && $inv->rete_bomberil_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode(' '.lang('rete_bomberil')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_bomberil_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_bomberil_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode(' '.lang('rete_autoaviso')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode(($inv->rete_autoaviso_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_autoaviso_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(33.2, 5, $this->sma->utf8Decode('  Serv. Transporte'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell(33.2, 5, $this->sma->utf8Decode('  TOTAL A PAGAR'), 'TBLR', 1, 'L', 1);
$pdf->setXY($current_x+33.2, $current_y);
$pdf->cell(38.8, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate)), 'TBR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente+1.5);

$inv_total_summary_end = $pdf->getY() + 3;

if ($tax_summary_end > $inv_total_summary_end) {
    $pdf->setXY(13, $tax_summary_end);
} else {
    $pdf->setXY(13, $inv_total_summary_end);
}

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->ln(1);
$pdf->cell(1,3,"",0,'L');
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell(10, 3, $this->sma->utf8Decode('Observaciones'), '', 1, 'L');
$pdf->SetFont('Arial','',$fuente+1.5);
$pdf->setX($current_x+1);
$tp_text = '';
if ($inv_total_before_promo > 0) {
    // $tp = $this->sma->formatDecimal((($inv_total_before_promo - $inv->grand_total) / $inv_total_before_promo) * 100, 2)." %";
    $tp = " $ ".$this->sma->formatMoney($inv_total_before_promo + $inv->order_discount);
    $tp_text = "<p class='text-center'>".sprintf(lang('invoice_total_promo_discount'), $tp)."</p>";
}

$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1.$tp_text)),0,'L');

$footer_note_x = $pdf->getX();
$current_x = $pdf->getX();
$current_y = $pdf->getY()+3;


$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(34, 5, $this->sma->utf8Decode('Elaborado por '.$created_by->first_name . ' ' . $created_by->last_name), 'T', 1, 'L');
$pdf->setXY($current_x+34, $current_y+10);
$pdf->cell(29, 5, $this->sma->utf8Decode(''), 'T', 1, 'L');
$pdf->setXY($current_x+63, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(34, 5, $this->sma->utf8Decode('Acepto firma y sello recibido'), 'T', 1, 'L');
$pdf->setXY($current_x+34, $current_y+10);
$pdf->cell(29, 5, $this->sma->utf8Decode(''), 'T', 1, 'L');
$pdf->setXY($current_x+63, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(34, 5, $this->sma->utf8Decode('Firma representante legal'), 'T', 1, 'L');
$pdf->setXY($current_x+34, $current_y+10);
$pdf->cell(29, 5, $this->sma->utf8Decode(''), 'T', 1, 'L');
$pdf->setXY($current_x+63, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

if ($signature_root) {
    $pdf->Image($signature_root, $current_x-50,$current_y,35);
}


$pdf->setXY($footer_note_x+1, $current_y+19);
$square_initial_y = $pdf->getY()-2;
$pdf->SetFont('Arial','',$pdf->fuente+0.5);
if (isset($pdf->cost_center) && $pdf->cost_center) {
    $pdf->MultiCell(195,3,$this->sma->utf8Decode($pdf->reduceTextToDescription2( "Centro de costo : ".$pdf->cost_center->name." (".$pdf->cost_center->code.") \n".$pdf->description2)), 0, 'L');
} else {
    $pdf->MultiCell(194.5,3,$this->sma->utf8Decode($pdf->reduceTextToDescription2($pdf->description2)), 0, 'L');
}
$square_end_y = $pdf->getY();
$square_height = $square_end_y - $square_initial_y;
$pdf->RoundedRect($footer_note_x, $square_initial_y, 196, $square_height+2, 3, '1234', '');

if ($inv->sale_status == 'pending') {
    $pdf->setXY(90, 90);
    $pdf->SetFont('Arial','B',50);
    $pdf->SetTextColor(125,125,125);
    $pdf->RotatedImage(base_url().'assets/images/not_approved.png',20,220,240,18,45);
    $pdf->SetFont('Arial','',$fuente);
    $pdf->SetTextColor(0,0,0);
}
if ($for_email === true) {
    $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($download) {
        $pdf->Output($filename.".pdf", "D");
    } else if ($internal_download == TRUE) {
        $pdf->Output("files/electronic_billing/". $filename.".pdf", "F");
    } else {
        $pdf->Output($filename.".pdf", "I");
    }
}