<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$data[0]->reference_no;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3, h4, p, span, table, table > tbody > tr > td, table > tbody > tr > th, table > tfoot > tr > th, table > thead > tr > th {
                padding: 0px !important;
                margin: 0px !important;
            }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }

            table > tbody > tr > td, table > thead > tr > th {
                font-size: 130%;
            }
        </style>
    </head>

    <body>
    <div id="wrapper">
        <div id="receiptData">
            <div>
                <div class="col-sm-12 text-center">
                    <?php if (isset($data[0]->suspend_id)): ?>
                        <h3 style="font-weight:bold;">N° <?= $data[0]->reference_no ?></h3>
                    <?php else: ?>
                        <h3 style="font-weight:bold;"><?=lang('sale_invoice');?> N° <?= $data[0]->reference_no ?></h3>
                    <?php endif ?>
                </div>
                <div class="text-center">

                    <?php if (isset($data[0]->suspend_id)): ?>
                        <h4 style="text-transform:uppercase;"><?= lang('seller') ?> : <?=$data[0]->seller;?></h4>
                    <?php else: ?>
                        <h4 style="text-transform:uppercase;"><?= lang('customer') ?> : <?=$data[0]->customer;?></h4>
                    <?php endif ?>
                    <h4 style="text-transform:uppercase;"><?= lang('date') ?> : <?=$data[0]->date;?></h4>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th><?= lang('product_name') ?></th>
                            <th><?= lang('product_qty') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($data as $row): ?>
                            <tr style="<?= $row->state_readiness == 3 ? 'text-decoration: line-through;' : '' ?>">
                                <td>
                                    <?= $row->state_readiness == 3 ? "<b> (Anulado) </b>" : "" ?>
                                    <?= $row->product_name ?>
                                    <br>
                                    <span style="font-size: 80%;"><?= $this->sma->print_preference_selection($row->preferences) ?></span>
                                </td>
                                <td class="text-right"><?= $this->sma->formatQuantity($row->quantity) ?></td>
                            </tr>
                        <?php endforeach ?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </body>

        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>

    <script>
        //redirect_to_pos
        $(document).ready(function(){
            imprimir_factura_pos();
        });
        function imprimir_factura_pos()
        {
          window.print();
          setTimeout(function() {
            window.close();
          }, 2000);
        }
    </script>

</body>
</html>
