<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    p {
        word-break: break-all;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= $customer->company && $customer->company != '-' ? $customer->company : $customer->name; ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-3 text-center">
                    <div style="max-width:200px; margin: 0 auto;">
                        <?=
                        $customer->customer_profile_photo ? '<img alt="" src="' . base_url() . 'assets/uploads/avatars/' . $customer->customer_profile_photo . '" class="avatar">' :
                            '<img alt="" src="' . base_url() . 'assets/images/male.png" class="avatar">';
                        ?>
                    </div>
                </div>
                <div class="col-sm-9">
                    <div class="row">
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("name"); ?></h4>
                            <p><?= $customer->name; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("id_document_type"); ?></h4>
                            <p><?= $customer->nombre_tipo_documento; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("vat_no"); ?></h4>
                            <p><?= $customer->vat_no.($customer->digito_verificacion > 0 ? "-".$customer->digito_verificacion : ""); ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("birth_month"); ?></h4>
                            <p><?= isset(lang('months')[$customer->birth_month]) ? lang('months')[$customer->birth_month]." - ".$customer->birth_day : lang('undefined'); ?></p>
                        </div>
                    </div>
                    <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
                    <div class="row">
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("email"); ?></h4>
                            <p><?= $customer->email; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("phone"); ?></h4>
                            <p><?= $customer->phone; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("address"); ?></h4>
                            <p><?= $customer->address; ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3">
                            <h4><?= lang("customer_only_for_pos"); ?></h4>
                            <p><?= $customer->customer_only_for_pos ? lang('yes') : lang('no'); ?></p>
                        </div>
                    </div>
                    <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <h4><?= lang("label_types_obligations"); ?></h4>
                            <p>
                                <?php if ($types_customer_obligations): ?>
                                    <?php foreach ($types_customer_obligations as $tco): ?>
                                        <?= $tco->types_obligations_id." - ".$tco->description.($tco !== end($types_customer_obligations) ? ", " : "") ?>
                                    <?php endforeach ?>
                                <?php else: ?>
                                    <?= lang('undefined') ?>
                                <?php endif ?>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("country"); ?></h4>
                    <p><?= $customer->country; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("state"); ?></h4>
                    <p><?= $customer->state; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("city"); ?></h4>
                    <p><?= $customer->city; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("postal_code"); ?></h4>
                    <p><?= $customer->postal_code; ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("zone"); ?></h4>
                    <p><?= $customer->zone_name ? $customer->zone_name : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("subzone"); ?></h4>
                    <p><?= $customer->subzone_name ? $customer->subzone_name : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("label_tradename"); ?></h4>
                    <p><?= $customer->company ? $customer->company : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("label_commercial_register"); ?></h4>
                    <p><?= $customer->commercial_register ? $customer->commercial_register : lang('undefined'); ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("label_type_vat_regime"); ?></h4>
                    <p><?=lang($customer->tvat_regime); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("customer_group"); ?></h4>
                    <p><?= $customer->customer_group_name; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("price_group"); ?></h4>
                    <p><?= $customer->price_group_name; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("deposit"); ?></h4>
                    <p><?= $this->sma->formatMoney($customer->deposit_amount); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang('creation_date') ?></h4>
                    <p><?= $customer->registration_date ?></p>
                </div>
                <?php if ($created_user): ?>
                    <div class="col-sm-3 col-xs-3">
                    <h4><?= lang('created_by') ?></h4>
                        <p><?= $created_user->first_name. ' ' . $created_user->last_name ?></p>
                    </div>
                <?php endif; ?>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("customer_seller_id_assigned"); ?></h4>
                    <p><?= $seller_assigned ? $seller_assigned->name : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("customer_payment_type"); ?></h4>
                    <?php $cptypes = [
                                '' => lang('select'),
                                '0' => lang('credit'),
                                '1' => lang('payment_type_cash'),
                                ]; ?>
                    <p><?= $cptypes[$customer->customer_payment_type]; ?></p>
                </div>
                <?php if ($customer->customer_payment_type == 0): ?>
                    <div class="col-sm-3 col-xs-3">
                        <h4><?= lang("customer_credit_limit"); ?></h4>
                        <p><?= $this->sma->formatMoney($customer->customer_credit_limit) ?></p>
                    </div>
                    <div class="col-sm-3 col-xs-3">
                        <h4><?= lang("customer_payment_term"); ?></h4>
                        <p><?= $customer->customer_payment_term ?></p>
                    </div>
                <?php endif ?>
            </div>
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_fuente') ?></label>
                    <p><?= $default_rete_fuente ? $default_rete_fuente->code." - ".$default_rete_fuente->name : lang('undefined') ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_iva') ?></label>
                    <p><?= $default_rete_iva ? $default_rete_iva->code." - ".$default_rete_iva->name : lang('undefined') ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_ica') ?></label>
                    <p><?= $default_rete_ica ? $default_rete_ica->code." - ".$default_rete_ica->name : lang('undefined') ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <label><?= lang('default_rete_other') ?></label>
                    <p><?= $default_rete_other ? $default_rete_other->code." - ".$default_rete_other->name : lang('undefined') ?></p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-xs-3">
                    <label><?= lang('note') ?></label>
                    <p><?= $customer->note ?></p>
                </div>
            </div>
            
            <?php if ($custom_fields): ?>
                <hr>
                <div class="row">
                <?php foreach ($custom_fields as $cf_arr): ?>
                    <?php $cf = $cf_arr['data']; ?>
                    <?php if ($customer->{$cf->cf_code}): ?>
                        <div class="col-sm-3 col-xs-3">
                            <label><?= $cf->cf_name ?></label>
                            <p><?= $customer->{$cf->cf_code} ?></p>
                        </div>
                    <?php endif ?>
                <?php endforeach ?>
                </div>
            <?php endif ?>
            <?php if ($this->pos_settings->show_client_modal_on_select == 1): ?>
                <hr>
                <button type="button" class="btn btn-primary customer_branches" data-show="0">Ver <?=lang('customer_branches') ?></button>
                <br>
                <div class="col-md-12 div_addresses" style="display:none; margin-top: 10px;">
                    <table id="CSUData" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr class="primary">
                                <th><?= lang("customer_branches"); ?></th>
                                <th style="width:85px;"><?= lang("actions"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php if (!empty($addresses)) {
                            foreach ($addresses as $address) {
                                echo '<tr>' .
                                        '<td>
                                            <b style="font-size:110%;">' . ucwords(mb_strtolower($address->sucursal)) . '</b>
                                            <br>' . ucwords(mb_strtolower($address->direccion)) . '
                                            <br>' . ucwords(mb_strtolower($address->city.' '.$address->postal_code)) . '
                                            <br>' . ucwords(mb_strtolower($address->state.' '.$address->country)) . '
                                            <br>' . lang('phone').' '. $address->phone . '
                                            <br>' . lang('seller').' : '. ($address->seller_name ? ucwords(mb_strtolower($address->seller_name)) : lang('no_register')) . '
                                        </td>
                                        '.'<td class="text-center">
                                            <a href="' . admin_url('customers/edit_address/' . $address->id) . '" class="tip" title="' . lang("edit_customer_branch") . '" data-toggle="modal" data-target="#myModal2"><i class="fa fa-edit"></i></a> 
                                            <a href="#" class="tip po" title="' . $this->lang->line("delete_address") . '" data-content="<p>' . lang('r_u_sure') . '</p>
                                            <a class=\'btn btn-danger\' href=\'' . admin_url('customers/delete_address/' . $address->id) . '\'>' . lang('i_m_sure') . '</a> 
                                            <button class=\'btn po-close\'>' . lang('no') . '</button>"  rel="popover"><i class="fa fa-trash-o"></i></a>
                                        </td>' .
                                    '</tr>';
                            }
                        } else { ?>
                            <tr>
                                <td colspan="2" class="dataTables_empty"><?= lang('sEmptyTable') ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            <?php endif ?>

            <div class="modal-footer no-print">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('close'); ?></button>
                <?php if ($Owner || $Admin || $GP['reports-customers']) { ?>
                    <a href="<?=admin_url('reports/customer_report/'.$customer->id);?>" target="_blank" class="btn btn-primary"><i class="fa fa-chart-line"></i> <?= lang('customers_report'); ?></a>
                <?php } ?>
                <?php if ($Owner || $Admin || $GP['customers-edit']) { ?>
                    <a href="<?=admin_url('customers/edit/'.$customer->id);?>" data-toggle="modal" data-target="#myModal2" class="btn btn-primary"><i class="fa fa-edit"></i> <?= lang('edit_customer'); ?></a>
                    <a href="<?=admin_url('customers/add_address/'.$customer->id);?>" data-toggle="modal" data-target="#myModal2" class="btn btn-primary"><i class="fa fa-location-arrow"></i> <?= lang('add_customer_branch'); ?></a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

<script type="text/javascript">
    
    $(document).on('click', '.customer_branches', function(){
        show = $(this).data('show');
        if (show == 0) {
            $('.div_addresses').fadeIn();
            $(this).data('show', 1);
        } else if (show == 1) {
            $('.div_addresses').fadeOut();
            $(this).data('show', 0);
        }
    });

</script>