<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-printable">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('change_tag').' ('.lang('single_purchase').' '.lang('reference').': '.$inv->reference_no.')'; ?></h4>
            <p><?= $inv->note ?></p>
        </div>
        <?php
        $attrib = array('id' => 'change_tag');
        echo admin_form_open_multipart("purchases/change_tag/".$inv->id, $attrib);
        ?>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4 tag_div">
                        <?= lang('tag', 'tag') ?>
                        <select name="tag" id="potag" class="form-control">
                            <option value=""><?= lang('select') ?></option>
                            <?php if ($tags): ?>
                                <?php foreach ($tags as $tag_row): ?>
                                    <option value="<?= $tag_row->id ?>" data-color="<?= $tag_row->color ?>" data-type="<?= $tag_row->type ?>" <?= $tag_row->id == $inv->tag_id ? "selected" : "" ?>><?= $tag_row->description ?></option>
                                <?php endforeach ?>
                            <?php endif ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?php if ($inv->import_id): ?>
                    <em class="text-danger"><?= lang('cant_change_tag_import_assigned') ?></em>
                <?php else: ?>
                    <button class="btn btn-primary"><?= lang('submit') ?></button>
                <?php endif ?>
            </div>
            <input type="hidden" name="change_tag" value="1">
        <?php echo form_close(); ?>
    </div>
</div>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $('#potag').select2({
            formatResult : tag_option_color,
            formatSelection : tag_option_color
          });
    });
</script>