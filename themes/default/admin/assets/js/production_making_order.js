$(document).ready(function(){
    if (porbiller = localStorage.getItem('porbiller')) {
        $('#porbiller').select2('val', porbiller).trigger('change');
    } else {
        $('#porbiller').trigger('change');
    }
    if (poremployee = localStorage.getItem('poremployee')) {
        $('#poremployee').select2('val', poremployee).trigger('change');
    } else {
        $('#poremployee').trigger('change');
    }

    if (last_employee != 'false') {
        $('#poremployee').select2('val', last_employee).trigger('change');
    }
    $(document).on('change', '#poremployee', function(){
        localStorage.setItem('poremployee', $(this).val());
    });
    if (porwarehouse = localStorage.getItem('porwarehouse')) {
        $('#porwarehouse').select2('val', porwarehouse).trigger('change');
    } else {
        $('#porwarehouse').trigger('change');
    }
    $(document).on('change', '#porwarehouse', function(){
        localStorage.setItem('porwarehouse', $(this).val());
    });
    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('poritems')) {
                    localStorage.removeItem('poritems');
                }
                if (localStorage.getItem('porwarehouse')) {
                    localStorage.removeItem('porwarehouse');
                }
                if (localStorage.getItem('pornote')) {
                    localStorage.removeItem('pornote');
                }
                if (localStorage.getItem('pordate')) {
                    localStorage.removeItem('pordate');
                }
                if (localStorage.getItem('porestimated_date')) {
                    localStorage.removeItem('porestimated_date');
                }
                if (localStorage.getItem('document_type_id')) {
                    localStorage.removeItem('document_type_id');
                }
                if (localStorage.getItem('poremployee')) {
                    localStorage.removeItem('poremployee');
                }
                if (localStorage.getItem('porbiller')) {
                    localStorage.removeItem('porbiller');
                }
                $('#modal-loading').show();
                location.reload();
            }
        });
    });
    setTimeout(function() {
        loadItems();
    }, 850);
});

function add_order_item(item) {
    console.log(item);
    if (!JSON.parse(localStorage.getItem('poritems'))) {
        poritems = {};
    } else {
        poritems = JSON.parse(localStorage.getItem('poritems'));
    }
    if (item == null)
        return;
    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (poritems && poritems[item_id] !== undefined) {
        var new_qty = parseFloat(poritems[item_id].row.qty) + 1;
        poritems[item_id].row.qty = new_qty;
    } else {
        poritems[item_id] = item;
    }

    if (site.settings.production_order_one_reference_limit == 1) {
        var break_add = false;
        $.each(poritems, function(index, pitem){
            if (pitem.row.code !== item.row.code) {
                command: toastr.error('No puede ingresar más de una referencia para la orden.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
                break_add = true;
            }
        });
        if (break_add == false) {
            poritems[item_id].order = new Date().getTime();
            localStorage.setItem('poritems', JSON.stringify(poritems));
            loadItems();
            return true;
        }
    } else {
        poritems[item_id].order = new Date().getTime();
        localStorage.setItem('poritems', JSON.stringify(poritems));
        loadItems();
        return true;
    }
}

function loadItems() {
    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }
    if (localStorage.getItem('poritems')) {
        count = 1;
        an = 1;
        Ttotal = 0;
        total_com_items = 0;
        $("#porTable tbody").empty();
        poritems = JSON.parse(localStorage.getItem('poritems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(poritems, function(o){return [parseInt(o.order)];}) : poritems;
        if (comproducts !== undefined) { delete comproducts; }
        var comproducts = [];
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_qty = item.row.qty, item_price = item.row.price, item_total = item.row.qty * item.row.cost, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var type = item.row.type ? item.row.type : '';
            if (edit == true) {
                product_id = item.item_id;
            }
            var item_cost = item.row.cost;
            var item_options = item.options;

            var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" data-item-id='" + item_id + "' />");
            item_options_names = [];
            if(item.options !== false) {
                $.each(item.options, function () {
                    item_options_names[this.id] = this.name;
                    if (item.row.option == this.id) {
                        $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
                    } else {
                        $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                        if (item_option == false) {
                            item_option = this.id;
                        }
                    }
                });
            } else {
                $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
                opt = opt.hide();
            }
            Ttotal += item_total;
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><input name="product_name[]" type="hidden" value="' + item_name + '"><span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name +'</span>'+
                            (edit == true ? 
                                '<input name="por_item_id[]" type="hidden" value="' + item.id + '">' +
                                '<input name="por_original_qty[]" type="hidden" value="' + item.row.quantity + '">'
                                : "")+
                        '</td>';
            tr_html += '<td>'+lang[item.row.type]+'</td>';
            // tr_html += '<td>'+(opt.get(0).outerHTML)+'</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_qty, decimals) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                        '</td>';
            tr_html += '<td class="text-center"><i class="fa fa-times tip pordel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#porTable");
            count += parseFloat(item_qty);
            an++;

            $.each(item.composition_products, function(index, item){
                item.comp_qty = item.qty * item_qty;
                item.production_order_pfinished_item_id = product_id;
                // item.pfinished_name = item_name +' - ' + item_code + (item_options_names[item_option] !== undefined ? " - "+item_options_names[item_option] : "");
                item.pfinished_name = item_name +' - ' + item_code;
                comproducts.push(item);
            });

        });
        var tfoot = '<tr id="tfoot" class="tfoot active">'+(site.settings.set_adjustment_cost ? '<th></th>' : '')+
                        '<th colspan="1">Total</th>'+
                        '<th class="text-center">' + formatQuantity2(parseFloat(count) - 1) + '</th>'+
                        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#porTable tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        set_page_focus();
        loadCompositionItems(comproducts);
    }
}

function loadCompositionItems(comproducts){

    var decimals = 3;
    if (site.settings.rounding == 1) {
        decimals = 1;
    }
    $("#porCTable tbody").empty();
    count = 1;
    Ttotal = 0;
    $.each(comproducts, function (index, item) {
        if (item === undefined) { return; }
        var trclass = 'class="cproduct_'+item.id+'"';
        var newTr = $('<tr></tr>');
        tr_html = '<td '+trclass+'>'+
                        '<input name="cp_product_id[]" type="hidden" class="rid" value="' + item.id + '">'+
                        '<input name="cp_product_name[]" type="hidden" value="' + item.name + '">'+
                        '<span class="sname">' + item.code +' - ' + item.name +'</span>'+
                        (edit == true ? 
                            '<input name="cp_por_item_id[]" type="hidden" value="' + item.por_item_id + '">' +
                            '<input name="cp_por_original_qty[]" type="hidden" value="' + item.quantity + '">'
                            : "")+
                        '<input name="cp_production_order_pfinished_item_id[]" type="hidden" class="rid" value="' + item.production_order_pfinished_item_id + '">'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+item.pfinished_name+'</td>';
        tr_html += '<td '+trclass+'>'+lang[item.type]+'</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center rquantity" name="cp_quantity[]" type="text" value="' + formatDecimal(item.comp_qty, decimals) + '" readonly>'+
                    '</td>';
        newTr.html(tr_html);
        newTr.prependTo("#porCTable");
        Ttotal += parseFloat(item.price) * parseFloat(item.comp_qty);
        // console.log('ttotal '+Ttotal);
        count += parseFloat(item.comp_qty);
    });
    var tfoot = '<tr id="tfoot" class="tfoot active">'+
                    '<th colspan="3">Total</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(count) - 1) + '</th>'+
                '</tr>';
    $('#porCTable tfoot').html(tfoot);
}


$(document).on("change", '.rquantity', function () {
    var row = $(this).closest('tr');
    item_id = row.attr('data-item-id');
    poritems = JSON.parse(localStorage.getItem('poritems'));
    old_row_qty = poritems[item_id].row.qty;
    if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
        $(this).val(old_row_qty);
        bootbox.alert(lang.unexpected_value);
        return;
    }
    var new_qty = parseFloat($(this).val());
    if (edit == false || (edit == true && poritems[item_id].row.quantity !== undefined && new_qty > poritems[item_id].row.quantity)) {
        poritems[item_id].row.qty = new_qty;
        localStorage.setItem('poritems', JSON.stringify(poritems));
    } else {
        $(this).val(old_row_qty);
        bootbox.alert('La cantidad no puede ser menor a la cantidad original de la orden');
        return;
    }
    loadItems();
});

$(document).on('click', '.pordel', function () {
    poritems = JSON.parse(localStorage.getItem('poritems'));
    var row = $(this).closest('tr');
    var item_id = row.attr('data-item-id');
    delete poritems[item_id];
    row.remove();
    if(poritems.hasOwnProperty(item_id)) { } else {
        localStorage.setItem('poritems', JSON.stringify(poritems));
        loadItems();
        return;
    }
});

$(document).on('change', '.rvariant', function(){
    item_id = $(this).data('item-id');
    val = $(this).val();
    poritems = JSON.parse(localStorage.getItem('poritems'));
    poritems[item_id].row.option = val;
    localStorage.setItem('poritems', JSON.stringify(poritems));
    loadItems();
});