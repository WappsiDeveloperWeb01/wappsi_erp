<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    $(document).ready(function () {
        var warehouse_id;    
        var biller_id;    
        <?php if ($this->session->userdata('biller_id')): ?>
            biller_id = "<?= $this->session->userdata('biller_id') ?>";
            setTimeout(function() {
                $('#biller_id').select2('val', biller_id).select2('readonly', true);
            }, 850);
        <?php else: ?>
            <?php if ($this->input->post('biller_id')): ?>
                biller_id = "<?= $this->input->post('biller_id') ?>";
            <?php endif ?>
        <?php endif ?>
        <?php if ($this->session->userdata('warehouse_id')): ?>
            warehouse_id = "<?= $this->session->userdata('warehouse_id') ?>";
            setTimeout(function() {
                $('#warehouse_id').select2('val', warehouse_id).select2('readonly', true);
            }, 850);
        <?php else: ?>
            <?php if ($this->input->post('warehouse_id')): ?>
                warehouse_id = "<?= $this->input->post('warehouse_id') ?>";
            <?php endif ?>
        <?php endif ?>
        oTable = $('#PQData').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reports/getQuantityAlerts') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                },  {
                    "name": "biller_id",
                    "value": biller_id
                }, {
                    "name": "warehouse_id",
                    "value": warehouse_id
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": img_hl
            }, null, null, {"mRender": formatQuantity}, {"mRender": formatQuantity}],
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('product_code');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('product_name');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('alert_quantity');?>]", filter_type: "text", data: []},
        ], "footer");
    });
    
    $(document).on('change', '#biller_id', function(){
        default_wh = $('#biller_id option:selected').data('defaultwh');
        $('#warehouse_id').select2('val', (default_wh > 0 ? default_wh : null));
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('reports/quantity_alerts', 'id="qAlertForm"'); ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="biller_id"><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                            <select class="form-control" id="biller_id" name="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="warehouse_id"><?= $this->lang->line('warehouse') ?></label>
                                            <?php $warehouseId = (isset($_POST["warehouse_id"])) ? $_POST["warehouse_id"] : ''; ?>
                                            <select class="form-control" id="warehouse_id" name="warehouse_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($warehouses as $r_warehouse) : ?>
                                                    <option value="<?= $r_warehouse->id ?>" <?= ($r_warehouse->id == $warehouseId) ? 'selected' : '' ?>><?= $r_warehouse->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <button class="btn btn-primary new-button" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <dic class="col-lg-4">
                            <h3><?= lang('warehouse')." : ".(isset($warehouse) && $warehouse ?  $warehouse->name : '') ?></h3>
                        </dic>
                        <div class="col-lg-8">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="#" id="xls" title="<?= lang('download_xls') ?>"> <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?> </a>
                                    </li>
                                    <li>
                                        <a href="#" id="image" class="tip" title="" data-original-title="Save as Image"> <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?></a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pull-right dropdown">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="PQData" cellpadding="0" cellspacing="0" border="0"
                                    class="table table-bordered table-condensed table-hover dfTable reports-table">
                                    <thead>
                                    <tr class="active">
                                        <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                                        <th><?php echo $this->lang->line("product_code"); ?></th>
                                        <th><?php echo $this->lang->line("product_name"); ?></th>
                                        <th><?php echo $this->lang->line("quantity"); ?></th>
                                        <th><?php echo $this->lang->line("alert_quantity"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

    });
</script>
