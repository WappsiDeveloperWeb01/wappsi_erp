<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| FE
|--------------------------------------------------------------------------
 */
define('NUMBER_DECIMALS', 2);
define('NUMBER_DECIMALS_SIMBA', 4);


/*
|--------------------------------------------------------------------------
| Types of person
|--------------------------------------------------------------------------
 */
define('LEGAL_PERSON', 1);
define('NATURAL_PERSON', 2);

/*
|--------------------------------------------------------------------------
| Types of vat regime
|--------------------------------------------------------------------------
 */
define('COMMON', 2);
define('SIMPLIFIED', 1);

/*
|--------------------------------------------------------------------------
| Types of document
|--------------------------------------------------------------------------
 */
define('NIT', 31);

/*
|--------------------------------------------------------------------------
| Type of relationship for client and types of obligations.
|--------------------------------------------------------------------------
 */
define('TRANSMITTER', 1);
define('ACQUIRER', 2);

/*
|--------------------------------------------------------------------------
| Table shapes restobar.
|--------------------------------------------------------------------------
 */
define('SQUARE', 1);
define('RECTANGULAR', 2);
define('CIRCLE', 3);
define('ELLIPSE', 4);
define('BAR', 5);

/*
|--------------------------------------------------------------------------
| Payment method.
|--------------------------------------------------------------------------
 */
define('CASH', 1);
define('CREDIT', 2);

/*
|--------------------------------------------------------------------------
| Table states restobar.
|--------------------------------------------------------------------------
 */
define('AVAILABLE', 1);
define('NOT_AVAILABLE', 2);
define('OCCUPIED', 3);
define('RESERVED', 4);

/*
|--------------------------------------------------------------------------
| Table seats restobar.
|--------------------------------------------------------------------------
 */
const TABLE_SEATS = [
	SQUARE=>['2', '4', '8'],
	RECTANGULAR=>['6', '8', '10', '12'],
	CIRCLE=>['4', '8'],
	ELLIPSE=>['6', '8'],
	BAR=>['3', '5', '8']
];

/*
|--------------------------------------------------------------------------
| State ordered product restobar.
|--------------------------------------------------------------------------
 */
define('ORDERED', 1);
define('PREPARATION', 2);
define('CANCELLED', 3);
define('DISPATCHED', 4);

/*
|--------------------------------------------------------------------------
| FE Type of taxes.
|--------------------------------------------------------------------------
 */
define("IVA", "01");
define("IC", "02");
define("ICA", "03");
define("INC", "04");
define("BOLSAS", "22");
define("ICL", "32");
define("IBUA", "34");
define("ICUI", "35");

/*
|--------------------------------------------------------------------------
| FE Work environment.
|--------------------------------------------------------------------------
 */
define("PRODUCTION", 1);
define("TEST", 2);
define("CONTINGENCY", 3);

/*
|--------------------------------------------------------------------------
| Boolean.
|--------------------------------------------------------------------------
 */
define("YES", 1);
define("NOT", 0);


/*
|--------------------------------------------------------------------------
| IP For web service.
|--------------------------------------------------------------------------
 */
define('LOCAL_IP',(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] != '::1' ? 'www.wappsi.com.co' : '192.168.0.55'));


/*
|--------------------------------------------------------------------------
| Constants technology providers
|--------------------------------------------------------------------------
 */
define("FACTURATECH", 1);
define("DELCOP", 2);
define("SIMBA", 3);
define("CADENA", 4);
define("BPM", 5);

/*
|--------------------------------------------------------------------------
| Type electronic document
|--------------------------------------------------------------------------
 */
define("INVOICE", "01");
define("EXPORT_INVOICE", "02");
define("BILLING_CONTINGENCY_INVOICE", "03");
define("DIAN_CONTINGENCY_INVOICE", "04");
define("CREDIT_NOTE", "91");
define("DEBIT_NOTE", "92");
define('DOCUMENT_SUPPORT', '07');
define("MAX_TIME_SESSION", "36000");
define("DOCUMENT_EQUIVALENT", "20");

/*
|--------------------------------------------------------------------------
| Document type module
|--------------------------------------------------------------------------
 */
 define("FACTURA_POS", 1);
 define("FACTURA_DETAL", 2);
 define("NOTA_CREDITO_POS", 3);
 define("NOTA_CREDITO_DETAL", 4);
 define("NOTA_DEBITO_POS", 26);
 define("NOTA_DEBITO_DETAL", 27);
 define("DOCUMENTO_SOPORTE", 35);
 define("DOCUMENTO_SOPORTE_AJUSTE", 52);
 /*
|--------------------------------------------------------------------------
| Main Email FE
|--------------------------------------------------------------------------
 */
define("PRINCIPAL", 1);
define("BRANCH_OFFICE", 2);

 /*
|--------------------------------------------------------------------------
| Production order processes type
|--------------------------------------------------------------------------
 */
define("POR_CUTTING_TYPE", 1);
define("POR_ASSEMBLE_TYPE", 2);
define("POR_PACKING_TYPE", 3);


 /*
|--------------------------------------------------------------------------
| Retentions tax
|--------------------------------------------------------------------------
 */
define("RETEFUENTE", "05");
define("RETEIVA", "06");
define("RETEICA", "07");

 /*
|--------------------------------------------------------------------------
| Retentions tax
|--------------------------------------------------------------------------
 */
 define("VERSION_1_7", 1);
 define("VERSION_1_8", 2);

 /*
|--------------------------------------------------------------------------
| Cost Center
|--------------------------------------------------------------------------
 */
 define("DEFINED_IN_BRANCH", 0);
 define("DEFINED_IN_EACH_RECORD", 1);
 define("DEACTIVATE", 2);

 /*
|--------------------------------------------------------------------------
| Gender
|--------------------------------------------------------------------------
 */
define("MALE", 1);
define("FEMALE", 2);
define("UNDEFINED_SEX", 3);

 /*
|--------------------------------------------------------------------------
| Type Contract
|--------------------------------------------------------------------------
 */
define("FIXED_TERM", 1);
define("UNDEFINED_TERM", 2);
define("WORK_OR_WORK", 3);
define("LEARNING", 4);
define("PRACTICE", 5);

 /*
|--------------------------------------------------------------------------
| Payroll Workday
|--------------------------------------------------------------------------
 */
define("FULL_TIME", 1);
define("HALFTIME", 2);
define("PIECEWORK", 3);
define("FOR_HOURS", 4);

 /*
|--------------------------------------------------------------------------
| Method Withholdings
|--------------------------------------------------------------------------
 */
define("WITHHOLDING_PROCESS_1", 1);
define("WITHHOLDING_PROCESS_2", 2);
define("HAND_CALCULATION", 3);

 /*
|--------------------------------------------------------------------------
| Means payments
|--------------------------------------------------------------------------
 */
define("MCASH", 10);
define("CHECK", 20);
define("WIRE_TRANSFER", 42);
define("BANK_CREDIT_TRANSFER", 45);
define("BANK_DEBIT_TRANSFER", 47);

 /*
|--------------------------------------------------------------------------
| payroll period
|--------------------------------------------------------------------------
 */
define("WEEKLY", 1);
define("BIWEEKLY", 4);
define("MONTHLY", 5);
define("DEFINED_IN_CONTRACT", 7);

define("FIRST_FORTNIGHT", 1);
define("SECOND_FORTNIGHT", 2);

 /*
|--------------------------------------------------------------------------
| payroll concepts
|--------------------------------------------------------------------------
 */
define("SALARY", 1);
define("COMMISSIONS", 2);
define("INCENTIVES", 3);
define("BONUSES_CONSTITUTING_SALARY", 4);
define("PER_DIEM_CONSTITUTES_SALARY", 5);
define("TRANSPORTATION_ALLOWANCE", 6);
define("PAID_LICENSE", 7);
define("FAMILY_ACTIVITY_PERMIT", 8);
define("DUEL_LICENSE", 9);
define("PAID_PERMIT", 10);
define("MATERNITY_LICENSE", 11);
define("PATERNITY_LICENSE", 12);
define("DAYTIME_OVERTIME", 13);
define("NIGHT_OVERTIME", 14);
define("NIGHT_SURCHARGE", 15);
define("HOLIDAY_DAYTIME_OVERTIME", 16);
define("HOLIDAY_DAYTIME_SURCHARGE", 17);
define("HOLIDAY_NIGHT_OVERTIME", 18);
define("HOLIDAY_NIGHT_SURCHARGE", 19);
define("COMMON_DISABILITY", 23);
define("PROFESSIONAL_DISABILITY", 24);
define("WORK_DISABILITY", 25);
define("HEALTH_RATE", 26);
define("HEALTH_RATE_LESS_THAN", 28);
define("PENSION_RATE", 30);
define("HIGH_RISK_PENSION_RATE", 32);
define("SOLIDARITY_FUND_EMPLOYEES_1", 34);
define("SOLIDARITY_FUND_EMPLOYEES_1_2", 35);
define("SOLIDARITY_FUND_EMPLOYEES_1_4", 36);
define("SOLIDARITY_FUND_EMPLOYEES_1_6", 37);
define("SOLIDARITY_FUND_EMPLOYEES_1_8", 38);
define("SOLIDARITY_FUND_EMPLOYEES_2", 39);
define("WITHHOLDING", 42);
define("DISCIPLINARY_SUSPENSION", 43);
define("UNEXCUSED_ABSENTEEISM", 44);
define("NOT_PAID_LICENSE", 45);
define("NOT_PAID_PERMIT", 46);
define("TIME_VACATION", 47);
define("MONEY_VACATION", 48);
define("FINAL_CLEARANCE_VACATION", 49);
define("BEARING_ALLOWANCE", 50);
define("MAINTENANCE_ALLOWANCE", 51);
define("BONUSES_NOT_CONSTITUTING_SALARY", 52);
define("PER_DIEM_NOT_CONSTITUTES_SALARY", 53);
define("SOCIAL_SECURITY_RECOGNITION", 54);
define("OTHER_ACCRUED_CONSTITUENTS_SALARY", 55);
define("LOAN_DISBURSEMENT", 56);
define("SERVICE_BONUS", 57);
define("LAYOFF_ADJUSTMENTS", 58);
define("LAYOFFS", 59);
define("PREVIOUS_YEAR_LAYOFFS", 60);
define("LAYOFFS_INTERESTS", 61);
define("PROVISION_LAYOFFS_INTERESTS", 62);
define("FOOD_BOND", 64);
define("COMPANY_LOAN", 66);
define("DRAFT_CREDIT", 67);
define("FREE_INVESTMENT_LOAN", 68);
define("DEDUCTION_CANCELLATION_CBTE", 69);
define("OTHER_DEDUCTIONS", 70);
define("OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY", 71);
define("ENDOWMENT", 72);
define("COMPENSATION", 73);
define("SUNDAY_DISCOUNT", 74);
define("FISCAL_SEIZURES", 75);

 /*
|--------------------------------------------------------------------------
| payroll type concepts
|--------------------------------------------------------------------------
 */
define("ORDINARY", 1);
define("SALARY_PAYMENTS", 2);
define("TRANSPORTATION_ALLOWANCE_TYPE", 3);
define("PAID_LICENSE_TYPE", 4);
define("OVERTIME_OR_SURCHARGES", 5);
define("DISABILITIES", 6);
define("DEDUCTIONS_BY_LAW", 7);
define("UNEXCUSED_ABSENTEEISM_TYPE", 8);
define("NOT_PAID_LICENSE_TYPE", 9);
define("TIME_VACATION_TYPE", 10);
define("MONEY_VACATION_TYPE", 11);
define("NON_SALARY_PAYMENTS", 13);
define("OTHER_ACCRUED_TYPE", 14);
define("SERVICE_BONUS_TYPE", 15);
define("LAYOFFS_TYPE", 16);
define("COMPENSATION_TYPE", 17);
define("FOOD_BOND_TYPE", 18);
define("BUSINESS_LOANS", 20);
define("OTHER_DEDUCTIONS_TYPE", 22);
define("FISCAL_SEIZURES_TYPE", 23);

 /*
|--------------------------------------------------------------------------
| Type of providers
|--------------------------------------------------------------------------
 */
define("PRODUCTS_EXPENSES", 1);
define("PRODUCTS", 2);
define("EXPENSES", 3);
define("CREDITOR", 4);
define("FINANCIAL_ENTITY", 5);
define("EPS", 6);
define("AFP_LAYOFFS", 7);
define("ARL", 8);
define("COMPENSATION_BOX", 9);

 /*
|--------------------------------------------------------------------------
| Payroll Earned or Deduction
|--------------------------------------------------------------------------
 */
define("EARNED", 1);
define("DEDUCTION", 2);

/*
|--------------------------------------------------------------------------
| Contracts status.
|--------------------------------------------------------------------------
 */
define("ACTIVE", 1);
define("INACTIVE", 0);
define("FINALIZED", 2);

/*
|--------------------------------------------------------------------------
| Payroll status.
|--------------------------------------------------------------------------
 */
define("IN_PREPARATION", 1);
define("APPROVED", 2);
define("PAID", 3);

/*
|--------------------------------------------------------------------------
| Payroll frequency.
|--------------------------------------------------------------------------
 */
define("RECURRENT", 1);
define("PERMANENT", 2);
define("ONCE", 3);

/*
|--------------------------------------------------------------------------
| Type of activity.
|--------------------------------------------------------------------------
 */
define('EDITION', 1);
define('ELIMINATION', 2);

/*
|--------------------------------------------------------------------------
| Payrroll areas.
|--------------------------------------------------------------------------
 */
define('ADMINISTRATION_AREA', 1);
define('ACCOUNTING_AREA', 2);
define('SALES_AREA', 3);
define('HUMAN_RESOURCE_AREA', 4);
define('PRODUCTION_AREA', 5);


/*
|--------------------------------------------------------------------------
| Payroll covers_disability
|--------------------------------------------------------------------------
*/
define('EMPLOYEER', 1);
define('EPS_LESS_THAN_91_DAYS', 2);
define('EPS_MORE_THAN_91_DAYS', 3);

/*
|--------------------------------------------------------------------------
| Electronic Payroll status
|--------------------------------------------------------------------------
*/
define("NOT_SENT", 0);
define("PENDING", 1);
define("ACCEPTED", 2);
define("SEND", 3);


/*
|--------------------------------------------------------------------------
| Electronic Payroll type
|--------------------------------------------------------------------------
*/
define("SUPPORT_DOCUMENT", 0);
define("ADJUSTMENT_DOCUMENT", 1);
define("ELIMINATION_DOCUMENT", 2);

/*
|--------------------------------------------------------------------------
| Type employee
|--------------------------------------------------------------------------
*/
define("APPRENTICES_SENA_LECTIVA", 12);
define("APPRENTICES_SENA_PRODUCTIVE", 19);
define("STUDENT_CONTRIBUTIONS_ONLY_LABOR_RISK", 23);
define("PRE_PENSION_OF_ENTITY_IN_LIQUIDATION", 54);
define("PRE_PENSIONED_WITH_VOLUNTARY_CONTRIBUTION_TO_HEALTH", 56);



/*
|--------------------------------------------------------------------------
| Response type
|--------------------------------------------------------------------------
*/
define("ERROR", 2);
define("SUCCESS", 1);

/*
|--------------------------------------------------------------------------
| Movement type
|--------------------------------------------------------------------------
*/
define("ELECTRONIC_DOCUMENTS", 4);

/*
|--------------------------------------------------------------------------
| Electronic record type
|--------------------------------------------------------------------------
*/
define("ELECTRONIC_BILL", 1);
define("ELECTRONIC_PAYROLL", 2);
define("SUPPORTING_DOCUMENT", 3);
define("DOCUMENTS_RECEPTION", 4);