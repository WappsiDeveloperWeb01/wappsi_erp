<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&display=swap" rel="stylesheet">
</head>
<body style="color: #4e4e4e; font-family: Roboto, sans-serif;">
    <table style="width: 800px" align="center">
        <tbody style="width: 100%">
            <tr style="text-align: left; width: 100%;">
                <th style="width: 40%;">
                    <img src="{logo}" width="70%">
                </th>
                <th style=" text-align: right; width: 60%">
                    <h2>Comprobante de Documento Soporte Electrónico</h2>
                </th>
            </tr>
            <tr>
                <td colspan="2" style="border-bottom:1px solid #979a9b"></td>
            </tr>
            <tr style="width: 100%;">
                <td style="font-weight:lighter; text-align: justify; width: 100%;" colspan="2">
                    <p style="padding: 0 10px;">
                        <strong>
                        Señores <br>
                        <label style="text-transform: uppercase;">{supplierName}</label>
                        </strong>
                        <br>
                        <br>
                        Cordial saludo,
                    </p>
                    <br>
                    <p style="padding: 0 10px">
                        <strong><label style="text-transform: uppercase;">{receiverName}</label></strong>, hace constancia de envío de documento de soporte con número <strong>{reference}</strong> con fecha <strong>{createdDate}</strong>
                    </p>
                    <p style="padding: 0 10px">Para dar cumplimiento a la Resolución 000167 de 30 de diciembre del 2021.</p>
                </td>
            </tr>
            <tr style="width: 100%;">
                <td colspan="2" style="font-size: 20px; text-align: center;">
                    <p style="margin-bottom: 30px;">
                    </p>
                </td>
            </tr>
            <tr><td colspan="2" style="border-bottom:1px solid #979a9b"></td></tr>
            <tr>
                <td colspan="2" style="text-align: center;">
                    <p style="margin-bottom: 30px;">Documento electrónico generado desde Wappsi ERP</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center>
                        <img src="" width="45%">
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="middle">
                    <p>
                        Si desea implementar nuestra solucion de facturacion electrónica, contáctenos en: <br>
                        <a href="mailto:fe@wappsi.com" style="text-decoration: none;">fe@wappsi.com</a>
                    </p>
                    <p>
                        <a href="https://www.wappsi.com.co/" style="font-size: 18px; text-decoration: none;">wappsi.com</a>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>