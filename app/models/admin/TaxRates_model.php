<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TaxRates_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function all()
    {
        $q = $this->db->get('tax_rates');
        return $q->result();
    }

    public function find($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('tax_rates');
        return $q->row();
    }

    public function update($data, $id) {
        $this->db->where("id", $id);
        if ($this->db->update('tax_rates', $data)) {
            return true;
        }
        return false;
    }
}
