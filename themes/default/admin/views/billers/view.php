<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    p {
        word-break: break-all;
    }
</style>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= $biller->company && $biller->company != '-' ? $biller->company : $biller->name; ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("branch_type"); ?></h4>
                    <p><?= $biller_aditional->branch_type == 1 ? lang('physical') : lang('virtual'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("name"); ?></h4>
                    <p><?= $biller->name; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("email"); ?></h4>
                    <p><?= $biller->email; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("phone"); ?></h4>
                    <p><?= $biller->phone; ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("country"); ?></h4>
                    <p><?= $biller->country; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("state"); ?></h4>
                    <p><?= $biller->state; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("city"); ?></h4>
                    <p><?= $biller->city; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("postal_code"); ?></h4>
                    <p><?= $biller->postal_code; ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("address"); ?></h4>
                    <p><?= $biller->address; ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("zone"); ?></h4>
                    <p><?= $biller->location ? $biller->location : lang('undefined'); ?></p>
                </div>
                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("subzone"); ?></h4>
                    <p><?= $biller->subzone ? $biller->subzone : lang('undefined'); ?></p>
                </div>

                <?php 
                foreach ($price_groups as $price_group) {
                    $pgs[$price_group->id] = $price_group->name;
                }
                 ?>

                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("default_group_price"); ?></h4>
                    <p><?= isset($pgs[$biller_aditional->default_price_group]) && $pgs[$biller_aditional->default_price_group] ? $pgs[$biller_aditional->default_price_group] : 'N/A' ?></p>
                </div>
            </div>
            <hr class="col-sm-11 col-xs-11" style="margin: 6px !important;">
            <div class="row">
                <?php 
                foreach ($customers as $customer) {
                    $cstmrs[$customer->id] = $customer->name;
                }
                 ?>

                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("default_customer"); ?></h4>
                    <p><?= isset($cstmrs[$biller_aditional->default_customer_id]) && $cstmrs[$biller_aditional->default_customer_id] ? $cstmrs[$biller_aditional->default_customer_id] : 'N/A' ?></p>
                </div>
                <?php 
                if ($sellers) {
                    foreach ($sellers as $seller) {
                        $slls[$seller['companies_id']] = $seller['name'];
                    }
                }

                 ?>

                <div class="col-sm-3 col-xs-3">
                    <h4><?= lang("default_seller"); ?></h4>
                    <p><?= isset($slls[$biller_aditional->default_seller_id]) && $slls[$biller_aditional->default_seller_id] ? $slls[$biller_aditional->default_seller_id] : 'N/A' ?></p>
                </div>

                <?php if ($cost_centers): ?>
                    <?php 
                    $cc_name = 'N/A';
                    foreach ($cost_centers as $cc) {
                        if ($cc->company_id == $biller->id) {
                            $cc_name = "(".$cc->code.") ".$cc->name;
                        }
                    } ?>
                      <div class="col-md-3 col-xs-3">
                        <h4><?= lang("cost_center"); ?></h4>
                        <p><?= $cc_name ?></p>
                      </div>
                <?php endif ?>
            </div>

            <div class="modal-footer no-print">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><?= lang('close'); ?></button>
                <?php if ($Owner || $Admin || $GP['customers-edit']) { ?>
                    <a href="<?=admin_url('billers/edit/'.$biller->id);?>" class="btn btn-primary"><i class="fa fa-edit"></i> <?= lang('edit_biller'); ?></a>
                <?php } ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>