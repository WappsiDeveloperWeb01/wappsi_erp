<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.pa_order_link', function() {
			var ventanaNueva = window.open(site.base_url + 'pos/pa_print_server_printing/' + $(this).data('saleid'));
			ventanaNueva.addEventListener('beforeunload', function() {
		      get_html();
		    });
	    });

	    function get_html(){
			$.ajax({
				url : site.base_url+"pos/get_pa_print_server_data",
				dataType : 'JSON',
			}).done(function(data){
				if (data.html) {
					$('#POSDataBody').html(data.html);
				} 
                if (!data.wdata) {
                    setTimeout(function() {
                        get_html();
                    }, 4000);
                }
				setTimeout(function() {
					$('#POSDataBody tr:first').trigger('click');
				}, 850);
			});
	    }
	    get_html();
	});
</script>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-content">
                    <div class="box-content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="POSData" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("customer_name"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody id="POSDataBody">
                                    <tr>
                                        <td colspan="3" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>