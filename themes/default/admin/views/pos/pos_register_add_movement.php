<?php defined('BASEPATH') OR exit('No direct script access allowed');
    // var_dump($_SESSION);
?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="mv_myModalLabel"><?php echo lang('pos_register_add_movement'); ?></h4>
        </div>
        <?php
        echo admin_form_open_multipart("pos/pos_register_add_movement", "id='mv_add'"); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>
            <?php if ($Owner || $Admin) { ?>
                <div class="form-group">
                    <?= lang("date", "date"); ?>
                    <?= form_input('mv_date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control datetime" id="mv_date" required="required" disabled'); ?>
                </div>
            <?php } ?>
            <?php
            $bl[""] = "";
            $bldata = [];
            foreach ($billers as $biller) {
                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                $bldata[$biller->id] = $biller;
            }
            ?>
            <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                <div class="form-group">
                    <div class="form-group">
                        <?= lang("biller", "mv_biller"); ?>
                        <select name="mv_biller" class="form-control" id="mv_biller" required="required">
                            <?php foreach ($billers as $biller) : ?>
                                <option value="<?= $biller->id ?>"><?= $biller->company ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>
            <?php } else { ?>
                <div class="form-group">
                    <div class="form-group">
                        <?= lang("biller", "mv_biller"); ?>
                        <select name="mv_biller" class="form-control" id="mv_biller">
                            <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                $biller = $bldata[$this->session->userdata('biller_id')];
                            ?>
                                <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                            <?php endif ?>
                        </select>
                    </div>
                </div>
            <?php } ?>
            <div class="form-group">
                <?= lang("reference", "mv_document_type_id"); ?>
                <select name="mv_document_type_id" class="form-control" id="mv_document_type_id" required>

                </select>
            </div>
            <?php if (isset($cost_centers)): ?>
                <div class="form-group">
                    <?= lang('cost_center', 'mv_cost_center_id') ?>
                    <?php
                    $ccopts[''] = lang('select');
                    foreach ($cost_centers as $cost_center) {
                        $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                    }
                     ?>
                     <?= form_dropdown('mv_cost_center_id', $ccopts, '', 'id="mv_cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                </div>
            <?php endif ?>
            <div class="form-group">
                <?= lang("movement_type", "mv_movement_type"); ?>
                <select name="mv_movement_type" class="form-control" id="mv_movement_type" required>
                    <option value=""><?= lang('select') ?></option>
                    <option value="1"><?= lang('movement_type_in') ?></option>
                    <option value="2"><?= lang('movement_type_out') ?></option>
                    <option value="3"><?= lang('movement_type_transfer') ?></option>
                </select>
            </div>
            <div class="form-group">
                <?= lang("value", "mv_amount"); ?>
                <input name="mv_amount" type="text" id="mv_amount" value="" class="pa form-control kb-pad amount"
                       required="required"/>
            </div>
            <div class="form-group">
                <?= lang("origin_paying_by", "mv_origin_paid_by"); ?>
                <select name="mv_origin_paid_by" id="mv_origin_paid_by" class="form-control" readonly>
                    <?= $this->sma->paid_opts('cash', false, false, true, true); ?>
                </select>
            </div>
            <div class="transfer_mt" style="display: none;">
                <div class="form-group">
                    <?= lang("destination_biller", "destination_mv_biller"); ?>
                    <?php
                    unset($bl);
                    $bl[""] = lang('select');
                    foreach ($billers as $biller) {
                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                    }
                    echo form_dropdown('destination_mv_biller', $bl, $Settings->default_biller, 'id="destination_mv_biller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"');
                    ?>
                </div>
                <div class="form-group">
                    <?= lang("user", "mv_user"); ?>
                    <?php
                    unset($usr);
                    $usr[""] = lang('select');
                    foreach ($users as $user) {
                        $usr[$user->id] = $user->first_name." ".$user->last_name;
                    }
                    echo form_dropdown('mv_user', $usr, '', 'id="mv_user" data-placeholder="' . lang("select") . ' ' . lang("user") . '" class="form-control input-tip select" style="width:100%;"');
                    ?>
                </div>
            </div>
            <div class="form-group">
                <?= lang("destination_paying_by", "mv_destination_paid_by"); ?>
                <select name="mv_destination_paid_by" id="mv_destination_paid_by" class="form-control">
                    <?= $this->sma->paid_opts(false, false, false, true, true); ?>
                </select>
            </div>
            <div class="form-group">
                <?= lang("note", "mv_note"); ?>
                <?php echo form_textarea('mv_note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="mv_note"'); ?>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary form_submit"><?= lang('submit') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {

        $("#mv_add").validate({
            ignore: []
        });

        $(document).on('click', '.form_submit', function(){
            if ($('#mv_add').valid()) {
                $('#mv_add').submit();
            }
        });

        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $("#mv_date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());
        $(document).on('change', '#mv_biller', function(){
            $.ajax({
                url: site.base_url+'billers/getBillersDocumentTypes/37/'+$('#mv_biller').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){
                response = data;
                $('#mv_document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                                "showDuration": "500",
                                "hideDuration": "1000",
                                "timeOut": "6000",
                                "extendedTimeOut": "1000",
                            });
                }
                if (response.status == 0) {
                  $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                }
                $('#mv_document_type_id').trigger('change');
            });
        });
        $('#mv_biller').trigger('change');
        $('#mv_origin_paid_by').select2('readonly', true);
        $('#mv_movement_type').on('change', function(){
            mtype = $(this).val();
            $('.transfer_mt').fadeOut();
            $('#mv_user').prop('required', false).select2();
            $('#destination_mv_biller').prop('required', false).select2();
            if (mtype == 1) {
                $('#mv_destination_paid_by').select2('val', 'cash').select2('readonly', true);
                $('#mv_origin_paid_by').select2('readonly', false);
            } else if (mtype == 2) {
                $('#mv_origin_paid_by').select2('val', 'cash').select2('readonly', true);
                $('#mv_destination_paid_by').select2('readonly', false);
            } else if (mtype == 3) {
                $('#mv_origin_paid_by').select2('val', 'cash').select2('readonly', true);
                $('#mv_destination_paid_by').select2('val', 'cash').select2('readonly', true);
                $('.transfer_mt').fadeIn();
                $('#mv_user').prop('required', true).select2();
                $('#destination_mv_biller').prop('required', true).select2();
            }
        });

        $(document).on('change', '#mv_user', function(){
            user_id = $(this).val();
            $.ajax({
                url : site.base_url + "pos/validate_user_has_pos_register_open/"+user_id,
                dataType : "JSON",
            }).done(function(data){
                $('.form_submit').prop('disabled', false);
                if (data.open == false) {
                    command: toastr.warning('El usuario seleccionado no tiene caja abierta', '¡Atención!', {
                                "showDuration": "500",
                                "hideDuration": "1000",
                                "timeOut": "6000",
                                "extendedTimeOut": "1000",
                            });
                    $('.form_submit').prop('disabled', true);
                }
            })
        });

    });
</script>
