<?php

/***********

FORMATO DE FACTURACIÓN ELECTRÓNICA, AGRUPADO POR VARIANTES DE PRODUCTO (FASHION)

************/
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        $this->RoundedRect(13, 7, 117, 32, 3, '1234', '');

        //izquierda
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,15,8.5,29);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,17,30);
        }
        
        if ($this->show_document_type_header == 1) {
            $this->SetFont('Arial','',$this->fuente);
            if ($this->biller_logo == 2) {
                $this->setXY(45, 30);
                $this->MultiCell(85, 3,  $this->sma->utf8Decode($this->crop_text(strip_tags($this->sma->decode_html($this->invoice_header)), 145)), 0, 'L');
            } else {
                $this->setXY(13, 31);
                $this->MultiCell(117, 3,  $this->sma->utf8Decode($this->crop_text(strip_tags($this->sma->decode_html($this->invoice_header)), 135)), 0, 'L');
            }
        }

        $cx = 45;
        $cy = 10;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(($this->biller->company != "-" ? $this->biller->company : $this->biller->name)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion != "" ? '-'.$this->biller->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        if ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode(
                                                ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? 'Auto retenedor de ' : '').
                                                ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                            ),'',1,'L');
        }
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 32, 3, '1234', '');

        $cx = 134;

        //derecha
        $cy = 9;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'FACTURA DE VENTA'),'',1,'L');
        $cy +=6;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->factura->reference_no),'',1,'L');
        $cy +=10;
        $this->SetFont('Arial','',$this->fuente);
        $this->setXY($cx-2, $cy);
        $this->MultiCell(77, 3 , $this->sma->utf8Decode($this->factura->resolucion),'','C');

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 41, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 46, 118, 30, 3, '4', '');
        $cx = 13;
        $cy = 41;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode('INFORMACIÓN DEL CLIENTE'),'B',1,'C');
        $cx += 3;
        $cy += 8;
        $this->setXY($cx, $cy);
        // $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->customer->name != '-' ? $this->customer->name : $this->customer->company)),'',1,'L');
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 7;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1.5);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->email_negocio),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 41, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 46, 78, 30, 3, '3', '');
        $cx = 131;
        $cy = 41;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA FACTURA'),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatValue($this->value_decimals, ($this->factura->grand_total - $this->factura->rete_fuente_total - $this->factura->rete_iva_total - $this->factura->rete_ica_total - $this->factura->rete_other_total - ($this->factura->rete_bomberil_id && $this->factura->rete_bomberil_total > 0 ? $this->factura->rete_bomberil_total : 0) - ($this->factura->rete_autoaviso_id && $this->factura->rete_autoaviso_total > 0 ? $this->factura->rete_autoaviso_total : 0)) * $this->trmrate)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FECHA VENCE'),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('FORMA DE PAGO'),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $fecha_exp = strtotime("+".($this->factura->payment_term > 0 ? $this->factura->payment_term : 0)." day", strtotime($this->factura->date));
        $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        $payment_text = $this->factura->payment_status != 'paid' ? lang('credit').", " : '';
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sale_payment_method),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(78, $altura , $this->sma->utf8Decode('VENDEDOR'),'TLBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $cy += $altura;
        $this->setXY($cx, $cy);
        if ($this->seller) {
            $this->Cell(78, $altura , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
        } else {
            $this->Cell(78, $altura , $this->sma->utf8Decode('Ventas Varias'),'',1,'C');
        }

        $this->ln(2);


        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $this->Cell( 9.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(45.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $cnt_opt = 1;
        $this->SetFont('Arial','B',$this->fuente-1.5);
        foreach ($this->options as $option_id => $pr_arr) {
            if ($cnt_opt > 14) {
                break;
            }
            $this->Cell($this->w_cells, 8, $this->sma->utf8Decode($this->options_names[$option_id]),'TBLR',0,'C',1);
            $cnt_opt++;
        }
        $this->SetFont('Arial','B',$this->fuente-1);
        $this->Cell(11.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(15.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(8.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(18.6, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        $this->Cell( 9.6, 8, $this->sma->utf8Decode('Código'),'',0,'C',0);
        $this->Cell(45.6, 8, $this->sma->utf8Decode('Nombre producto'),'',0,'C',0);
        $this->Cell($this->max_width_cells, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(11.6, 8, $this->sma->utf8Decode('Cantidad'),'',0,'C',0);
        $this->Cell(15.6, 8, $this->sma->utf8Decode('Vr Unit'),'',0,'C',0);
        $this->Cell(8.6, 8, $this->sma->utf8Decode('Dcto'),'',0,'C',0);
        $this->Cell(18.6, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        $this->Cell( 9.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(45.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell($this->max_width_cells, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(11.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(15.6, 4, $this->sma->utf8Decode('Bruto'),'',0,'C',0);
        $this->Cell(8.6, 4, $this->sma->utf8Decode('(%)'),'',0,'C',0);
        $this->Cell(18.6, 4, $this->sma->utf8Decode('IVA Incl'),'',1,'C',0);
    }

    function Footer()
    {
        // Print centered page number
        $this->RoundedRect(13, 244, 196, 21.5, 3, '1234', '');
        $this->SetXY(14, -33);

        if (isset($this->cost_center) && $this->cost_center) {
            $this->MultiCell(194,3,$this->sma->utf8Decode($this->reduceTextToDescription2( "Centro de costo : ".$this->cost_center->name." (".$this->cost_center->code.") \n".$this->description2)), 0, 'L');
        } else {
            $this->MultiCell(194,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        }
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$this->PageNo()),'',1,'C');
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->show_code = $show_code;
$pdf->sale_payment_method = $sale_payment_method;
$pdf->biller_logo = $biller_logo;
$pdf->customer = $customer;
$pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$description1 = $inv->note;
$pdf->description2 = $invoice_footer ? $invoice_footer : "";
$pdf->view_tax = $view_tax;
$pdf->tax_inc = $tax_inc;
$pdf->Settings = $this->Settings;
$pdf->document_type = $document_type;
$pdf->trmrate = $trmrate;
$pdf->value_decimals = $value_decimals;
$pdf->show_document_type_header = $show_document_type_header;
$pdf->invoice_header = $invoice_header;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->tipo_regimen = $tipo_regimen;

$taxes = [];
$total_bruto = 0;
$products = [];
$options = [];
$options_names = [];
foreach ($rows as $item) {

    $total_bruto += (($item->net_unit_price + ($item->item_discount / $item->quantity)) * $item->quantity) * $trmrate;

    if (!isset($taxes[$item->tax_rate_id])) {
        $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
        $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
    } else {
        $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
        $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
    }

    if (!isset($products[$item->product_id])) {
        $products[$item->product_id] = $item;
    }

    if (!isset($options_names[$item->variant])) {
        $options_names[$item->variant] = $item->variant;
        // $options_names[$item->variant."-2"] = $item->variant;
    }
    if (isset($options[$item->variant][$item->product_id])) {
        $options[$item->variant][$item->product_id] += $item->quantity;
    } else {
        $options[$item->variant][$item->product_id] = $item->quantity;
        // $options[$item->variant."-2"][$item->product_id] = $item->quantity;
    }

    // if (!isset($options_names[$item->option_id])) {
    //     $options_names[$item->option_id] = $item->variant;
    // }
    // if (isset($options[$item->option_id][$item->product_id])) {
    //     $options[$item->option_id][$item->product_id] += $item->quantity;
    // } else {
    //     $options[$item->option_id][$item->product_id] = $item->quantity;
    // }

}

ksort($options);

$pdf->options_names = $options_names;
$pdf->options = $options;
$pdf->max_width_cells = 86.4;
$pdf->num_options = count($options) <= 14 ? count($options) : 14;
$pdf->w_cells = $pdf->max_width_cells / $pdf->num_options;

$pdf->AddPage();

$maximo_footer = 235;
$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);

// $this->sma->print_arrays($products);
$cnt = 1;
foreach ($products as $product_id => $product) {
    $total_qty = 0;
    if ($pdf->getY() > $maximo_footer) {
        $pdf->AddPage();
    }
    $pdf->Cell(9.6, 5, $this->sma->utf8Decode($show_code == 1 ? $product->product_code : $product->reference),'BL',0,'C',0);
    $pr_name = $pdf->sma->reduce_text_length(($item->under_cost_authorized == 1 ? '     ' : '').
                                            $product->product_name.
                                            ($pdf->Settings->show_brand_in_product_search ? " - ".$product->brand_name : '').
                                            (!is_null($product->serial_no) ? " - ".$product->serial_no : '').
                                            strip_tags($this->sma->decode_html(($show_product_preferences == 1 && $item->preferences ? ' (' . $this->sma->print_preference_selection($item->preferences) . ')' : '')))
                                        , 40);
    if ($item->under_cost_authorized == 1) {
        $unlock_icon_X = $pdf->getX();
        $unlock_icon_Y = $pdf->getY();
        $pdf->Image(base_url().'assets/images/unlock-icon.png',$unlock_icon_X+1, $unlock_icon_Y+0.6,2.6);
    }
    $pdf->Cell(45.6, 5, $this->sma->utf8Decode($pr_name),'BL',0,'L',0);
    $cnt_opt = 1;
    foreach ($options as $option_id => $pr_arr) {
        if ($cnt_opt > 14) {
            break;
        }
        if (isset($pr_arr[$product->product_id])) {
            $total_qty += $pr_arr[$product->product_id];
            $pdf->Cell($pdf->w_cells, 5, $this->sma->utf8Decode($this->sma->formatQuantity($pr_arr[$product->product_id], $qty_decimals)),'BL',0,'C',0);
        } else {
            $pdf->Cell($pdf->w_cells, 5, $this->sma->utf8Decode(''),'BL',0,'C',0);
        }
        $cnt_opt++;
    }
    $pdf->Cell(11.6, 5, $this->sma->utf8Decode($this->sma->formatQuantity($total_qty, $qty_decimals)),'BL',0,'C',0);
    $pdf->Cell(15.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $product->price_before_tax)),'BL',0,'C',0);
    $pdf->Cell(8.6, 5, $this->sma->utf8Decode($product->discount),'BL',0,'C',0);
    $pdf->Cell(18.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $product->unit_price * $total_qty)),'BLR',1,'C',0);
    $cnt++;
}

$pdf->SetFont('Arial','',$fuente);


if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 140, 12.25, 3, '1234', '');

$pdf->ln(1);

$pdf->Cell(1,5, $this->sma->utf8Decode(''),'',0,'L');
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(115.6,5, $this->sma->utf8Decode('VALOR (En letras)'),'',1,'L');
$pdf->SetFont('Arial','',$fuente);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->setX(14);
$pdf->SetFont('Arial','',$fuente);
// $pdf->Cell(115.6,6.125, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total, $currencies_names[$inv->sale_currency])),'',1,'L');
$pdf->MultiCell(140, 3, $this->sma->utf8Decode($number_convert->convertir((($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate), (isset($currencies_names[$inv->sale_currency]) ? $currencies_names[$inv->sale_currency] : $this->Settings->default_currency))), 0, 'L');
$pdf->SetFont('Arial','',$fuente+1.5);
$pdf->setX(13);

$pdf->setXY($current_x, $current_y+8);


$current_x = $pdf->getX();
$current_y = $pdf->getY();
$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(140,0, $this->sma->utf8Decode(''),'B',1,'C');
$pdf->Cell(36,5, $this->sma->utf8Decode('Tipo Impuesto'),'',0,'C');
$pdf->Cell(36,5, $this->sma->utf8Decode('Valor Base'),'',0,'C');
$pdf->Cell(36,5, $this->sma->utf8Decode('Valor Impuesto'),'',0,'C');
$pdf->Cell(36,5, $this->sma->utf8Decode('Valor Total'),'',1,'C');

$pdf->SetFont('Arial','',$fuente);
$total_tax_base = 0;
$total_tax_amount = 0;

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $total =$arr['base'] + $arr['tax'];
    $pdf->Cell(36,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');
    $pdf->setXY($current_x + 36, $current_y);
    $pdf->setX($current_x + 36);
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['base'] * $trmrate)),'',1,'C');
    $total_tax_base += $arr['base'] * $trmrate;
    $pdf->setXY($current_x + (36 * 2), $current_y);
    $pdf->setX($current_x + (36 * 2));
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $arr['tax'] * $trmrate)),'',1,'C');
    $total_tax_amount += $arr['tax'] * $trmrate;
    $pdf->setXY($current_x + (36 * 3), $current_y);
    $pdf->setX($current_x + 36 * 3);
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total)),'',1,'C');
}


if ($inv->order_tax > 0) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->Cell(36,3, $this->sma->utf8Decode("(SP) ".$this->sma->formatDecimal((isset($taxes_details[$inv->order_tax_id]) ? $taxes_details[$inv->order_tax_id]." %" : "0%"))),'',1,'C');
    $pdf->setXY($current_x + 36, $current_y);
    $pdf->setX($current_x + 36);
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)),'',1,'C');
    $total_tax_base +=  $inv->total * $trmrate;

    $pdf->setXY($current_x + (36 * 2), $current_y);
    $pdf->setX($current_x + (36 * 2));
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax * $trmrate)),'',1,'C');
    $total_tax_amount += $inv->order_tax * $trmrate;

    $pdf->setXY($current_x + (36 * 3), $current_y);
    $pdf->setX($current_x + 36 * 3);
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total * $trmrate)),'',1,'C');
}

if ($inv->consumption_sales > 0) {

    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $pdf->Cell(36,3, $this->sma->utf8Decode('Impoconsumo (L&C)'),'',1,'C');
    $pdf->setXY($current_x + 36, $current_y);
    $pdf->setX($current_x + 36);
    $pdf->Cell(36,3, $this->sma->utf8Decode(''),'',1,'C');

    $pdf->setXY($current_x + (36 * 2), $current_y);
    $pdf->setX($current_x + (36 * 2));
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->consumption_sales * $trmrate)),'',1,'C');
    $total_tax_amount += $inv->consumption_sales * $trmrate;

    $pdf->setXY($current_x + (36 * 3), $current_y);
    $pdf->setX($current_x + 36 * 3);
    $pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->consumption_sales * $trmrate)),'',1,'C');

}
$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->SetFont('Arial','B',$fuente);
$pdf->Cell(36,3, $this->sma->utf8Decode("Total"),'',1,'C');
$pdf->setXY($current_x + 36, $current_y);
$pdf->setX($current_x + 36);
$pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base)),'',1,'C');

$pdf->setXY($current_x + (36 * 2), $current_y);
$pdf->setX($current_x + (36 * 2));
$pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_amount)),'',1,'C');

$pdf->setXY($current_x + (36 * 3), $current_y);
$pdf->setX($current_x + 36 * 3);
$pdf->Cell(36,3, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_tax_base + $total_tax_amount)),'',1,'C');
$pdf->SetFont('Arial','',$fuente);

$tax_summary_end = $pdf->getY() + 3;

//derecha

$pdf->setXY($cX_items_finished+141.6, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$columns_l = 24.2;
$columns_r = 30.2;

$pdf->cell($columns_l, 5, $this->sma->utf8Decode('Total bruto'), 'BLR', 1, 'L');
$pdf->setXY($current_x+$columns_l, $current_y);
$pdf->cell($columns_r, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $total_bruto)), 'BR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($columns_l, 5, $this->sma->utf8Decode('Descuento'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$columns_l, $current_y);
$pdf->cell($columns_r, 5, $this->sma->utf8Decode(($inv->total_discount > 0 ? "- " : "").$this->sma->formatValue($value_decimals, ($inv->total_discount) * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($columns_l, 5, $this->sma->utf8Decode('Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$columns_l, $current_y);
$pdf->cell($columns_r, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($columns_l, 5, $this->sma->utf8Decode('Vr. Impuesto'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$columns_l, $current_y);
$pdf->cell($columns_r, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->total_tax * $trmrate)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell($columns_l, 5, $this->sma->utf8Decode('Retefuente'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+$columns_l, $current_y);
$pdf->cell($columns_r, 5, $this->sma->utf8Decode(($inv->rete_fuente_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_fuente_total * $trmrate)), 'TBR', 1, 'R');

if ($inv->rete_iva_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($columns_l, 5, $this->sma->utf8Decode('ReteIVA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$columns_l, $current_y);
    $pdf->cell($columns_r, 5, $this->sma->utf8Decode(($inv->rete_iva_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_iva_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_ica_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($columns_l, 5, $this->sma->utf8Decode('ReteICA'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$columns_l, $current_y);
    $pdf->cell($columns_r, 5, $this->sma->utf8Decode(($inv->rete_ica_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_ica_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_other_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($columns_l, 5, $this->sma->utf8Decode(lang('rete_other')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$columns_l, $current_y);
    $pdf->cell($columns_r, 5, $this->sma->utf8Decode(($inv->rete_other_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_other_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_bomberil_id && $inv->rete_bomberil_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($columns_l, 5, $this->sma->utf8Decode(lang('rete_bomberil')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$columns_l, $current_y);
    $pdf->cell($columns_r, 5, $this->sma->utf8Decode(($inv->rete_bomberil_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_bomberil_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($columns_l, 5, $this->sma->utf8Decode(lang('rete_autoaviso')), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$columns_l, $current_y);
    $pdf->cell($columns_r, 5, $this->sma->utf8Decode(($inv->rete_autoaviso_total > 0 ? "- " : "").$this->sma->formatValue($value_decimals, $inv->rete_autoaviso_total * $trmrate)), 'TBR', 1, 'R');
}

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($columns_l, 5, $this->sma->utf8Decode('  Serv. Transporte'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+33.2, $current_y);
    $pdf->cell($columns_r, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping * $trmrate)), 'TBR', 1, 'R');
}

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente+1.5);
$pdf->cell($columns_l, 5, $this->sma->utf8Decode('TOTAL A PAGAR'), 'TBLR', 1, 'R', 1);
$pdf->setXY($current_x+$columns_l, $current_y);
$pdf->cell($columns_r, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->grand_total - $inv->rete_fuente_total - $inv->rete_iva_total - $inv->rete_ica_total - $inv->rete_other_total - ($inv->rete_bomberil_id ? $inv->rete_bomberil_total : 0) - ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0)) * $trmrate)), 'TBLR', 1, 'R', 1);
$pdf->SetFont('Arial','',$fuente+1.5);

$inv_total_summary_end = $pdf->getY() + 3;

if ($tax_summary_end > $inv_total_summary_end) {
    $pdf->setXY(13, $tax_summary_end);
} else {
    $pdf->setXY(13, $inv_total_summary_end);
}


//FIRMAS

//izquierda
$pdf->ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;
$pdf->cell(1,3,"",0,'L');
$pdf->MultiCell(196,3, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Acepto Firma y Sello Recibido'), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('C.C o Nit'), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode('Fecha'), 'T', 1, 'L');

//derecha
$pdf->setXY($current_x+78.4, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode('Representante Legal'), '', 1, 'L');

if ($signature_root) {
    $pdf->Image($signature_root, $current_x+35,$current_y+1,35);
}

// $descargar = false;
if ($inv->sale_status == 'pending') {
    $pdf->setXY(90, 90);
    $pdf->SetFont('Arial','B',50);
    $pdf->SetTextColor(125,125,125);
    $pdf->RotatedImage(base_url().'assets/images/not_approved.png',20,220,240,18,45);
    $pdf->SetFont('Arial','',$fuente);
    $pdf->SetTextColor(0,0,0);
}

if ($for_email === true) {
    $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($download) {
      $pdf->Output($inv->reference_no.".pdf", "D");
    } else {
      $pdf->Output($inv->reference_no.".pdf", "I");
    }    
}