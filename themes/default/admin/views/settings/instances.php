<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?= admin_form_open('system_settings/brand_actions', 'id="action-form"') ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="instancesTable" class="table table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th><?= lang("name"); ?></th>
                                            <th><?= lang("security_token"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                            <th><?= lang("actions") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="5" class="dataTables_empty">
                                                <?= lang('loading_data_from_server') ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        loadDataTables();

    //     $('#delete').click(function(e) {
    //         e.preventDefault();
    //         $('#form_action').val($(this).attr('data-action'));
    //         $('#action-form-submit').trigger('click');
    //     });

    //     $('#excel').click(function(e) {
    //         e.preventDefault();
    //         $('#form_action').val($(this).attr('data-action'));
    //         $('#action-form-submit').trigger('click');
    //     });

    //     $('#pdf').click(function(e) {
    //         e.preventDefault();
    //         $('#form_action').val($(this).attr('data-action'));
    //         $('#action-form-submit').trigger('click');
    //     });

    });

    function loadDataTables()
    {
        oTable = $('#instancesTable').dataTable({
            aaSorting: [[0, "asc"]],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('system_settings/getAllInstances') ?>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [
                { visible: false },
                null,
                null,
                {
                    mRender: function (data, type, row) {
                        if (data == '<?= BPM ?>') {
                            return 'BPM';
                        }
                    }
                },
                {
                    bSortable: false,
                    sWidth: '65px',
                    className: 'text-center'
                }
            ],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html('<a href="<?= admin_url('system_settings/addIntances') ?>" class="btn btn-primary new-button pull-right" data-toggle="modal" data-target="#myModal" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>');

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            }
        });
    }
</script>