<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Settings_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function updateLogo($photo)
    {
        $logo = array('logo' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function updateLoginLogo($photo)
    {
        $logo = array('logo2' => $photo);
        if ($this->db->update('settings', $logo)) {
            return true;
        }
        return false;
    }

    public function getSettings()
    {
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDateFormats()
    {
        $q = $this->db->get('date_format');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateSetting($data, $tax_rate_traslate = array(), $dataUpdateObligations = array())
    {
        //START PROCEDURE TO ENTER CHANGES IN PARAMETERS TO USER ACTIVITIES
        foreach ($data as $key => $value) {
            if ($key == 'default_warehouse') {
                $this->db->select($this->db->dbprefix('warehouses').'.name AS "'.$key.'"');
            }if ($key == 'accounting_method') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.accounting_method = 1 THEN '" .lang('FIFO'). "'
                                        WHEN {$this->db->dbprefix('settings')}.accounting_method = 2 THEN '" .lang('AVCO'). "'
                                        ELSE {$this->db->dbprefix('settings')}.accounting_method END AS 'accounting_method' ");
            }if ($key == 'default_currency') {
                $this->db->select($this->db->dbprefix('currencies').'.name AS "'.$key.'"');
            }if ($key == 'default_tax_rate') {
                $this->db->select('(select NAME from '.$this->db->dbprefix('tax_rates'). ' where id = ' . $this->db->dbprefix('settings').'.default_tax_rate) AS "' .$key. '"' );
            }if ($key == 'default_tax_rate2') {
                $this->db->select('(select NAME from '.$this->db->dbprefix('tax_rates'). ' where id = ' . $this->db->dbprefix('settings').'.default_tax_rate2) AS "' .$key. '"' );
            }if ($key == 'dateformat') {
                $this->db->select($this->db->dbprefix('date_format').'.js AS "'.$key.'"');
            }if ($key == 'item_addition') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.item_addition = 0, '" .lang('add_new_item'). "', '" .lang('increase_quantity_if_item_exist'). "') AS '" .$key. "'  " );
            }if ($key == 'product_serial') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.product_serial = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' " );
            }if ($key == 'default_discount') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.default_discount = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' " );
            }if ($key == 'product_discount') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.product_discount = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' " );
            }if ($key == 'tax1') {
                $this->db->select('(SELECT name FROM ' .$this->db->dbprefix('tax_rates'). ' WHERE id = ' .$this->db->dbprefix('settings'). '.tax1 ) AS  "' .$key. '"');
            }if ($key == 'overselling') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.overselling = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "'  " );
            }if ($key == 'restrict_calendar') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.restrict_calendar = 0, '" .lang('shared'). "', '" .lang('private'). "') AS '" .$key. "' " );
            }if ($key == 'watermark') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.watermark = 0, '" .lang('no'). "', '" .lang('yes'). "' ) AS '" .$key. "' " );
            }if ($key == 'reg_ver') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.reg_ver = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' " );
            }if ($key == 'allow_reg') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.allow_reg = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' " );
            }if ($key == 'reg_notification') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.reg_notification = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' " );
            }if ($key == 'customer_group') {
                $this->db->select("{$this->db->dbprefix('customer_groups')}.name AS '" .$key. "'");
            }if ($key == 'mmode') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.mmode = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'auto_detect_barcode') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.auto_detect_barcode = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'captcha') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.captcha = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'reference_format') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.reference_format = 1 THEN '" .lang('prefix_year_no'). "'
                                        WHEN {$this->db->dbprefix('settings')}.reference_format = 2 THEN '" .lang('prefix_month_year_no'). "'
                                        WHEN {$this->db->dbprefix('settings')}.reference_format = 3 THEN '" .lang('sequence_number'). "'
                                        WHEN {$this->db->dbprefix('settings')}.reference_format = 4 THEN '" .lang('random_number'). "'
                                        ELSE {$this->db->dbprefix('settings')}.reference_format END AS '" .$key. "' ");
            }if ($key == 'racks') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.racks = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'attributes') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.attributes = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'product_expiry') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.product_expiry = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'decimals_sep') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.decimals_sep = '.', '" .lang('dot'). "', '" .lang('comma'). "') AS '" .$key. "' ");
            }if ($key == 'thousands_sep') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.thousands_sep = '.' THEN '" .lang('dot'). "'
                                        WHEN {$this->db->dbprefix('settings')}.thousands_sep = ',' THEN '" .lang('comma'). "'
                                        WHEN {$this->db->dbprefix('settings')}.thousands_sep = 0 THEN '" .lang('space'). "'
                                        ELSE {$this->db->dbprefix('settings')}.thousands_sep END AS '" .$key. "' ");
            }if ($key == 'invoice_view') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.invoice_view = 1 THEN '" .lang('tax_invoice'). "'
                                        WHEN {$this->db->dbprefix('settings')}.invoice_view = 0 THEN '" .lang('standard'). "'
                                        ELSE {$this->db->dbprefix('settings')}.invoice_view END AS '" .$key. "' ");
            }if ($key == 'default_biller') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('settings')}.default_biller ) AS '" .$key. "' ");
            }if ($key == 'rtl') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.rtl = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "'");
            }if ($key == 'update') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.update = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'sac') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.sac = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'display_all_products') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.display_all_products = 0, '" .lang('hide_with_0_qty'). "', '" .lang('show_with_0_qty'). "') AS '" .$key. "' ");
            }if ($key == 'display_symbol') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.display_symbol = 0 THEN '" .lang('disable'). "'
                                        WHEN {$this->db->dbprefix('settings')}.display_symbol = 1 THEN '" .lang('before'). "'
                                        WHEN {$this->db->dbprefix('settings')}.display_symbol = 2 THEN '" .lang('after'). "'
                                        ELSE {$this->db->dbprefix('settings')}.display_symbol END AS '" .$key. "' ");
            }if ($key == 'remove_expired') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.remove_expired = 0, '" .lang('no').', '.lang('i_ll_remove'). "', '" .lang('yes').', '.lang('remove_automatically'). "') AS '" .$key. "' ");
            }if ($key == 'set_focus') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.set_focus = 0, '" .lang('add_item_input'). "', '" .lang('last_order_item'). "') AS '" .$key. "' ");
            }if ($key == 'price_group') {
                $this->db->select("{$this->db->dbprefix('price_groups')}.name AS '" .$key. "' ");
            }if ($key == 'barcode_img') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.barcode_img = 0, '" .lang('image'). "', '" .lang('svg'). "') AS '" .$key. "' ");
            }if ($key == 'update_cost') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.update_cost = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'apis') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.apis = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'tipo_regimen') {
                $this->db->select("{$this->db->dbprefix('types_vat_regime')}.description AS '" .$key. "' ");
            }if ($key == 'modulary') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.modulary = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'prioridad_precios_producto') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 1 THEN '" .lang('price_group_base_with_promotions'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 2 THEN '" .lang('biller_price_group'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 3 THEN '" .lang('customer_price_group'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 4 THEN '" .lang('biller_price_group_with_discounts'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 5 THEN '" .lang('price_per_unit'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 6 THEN '" .lang('customer_price_group_with_discounts'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 7 THEN '" .lang('price_per_unit_manual'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 8 THEN '" .lang('customer_address_price_group'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 9 THEN '" .lang('customer_address_price_group_with_discounts'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 10 THEN '" .lang('price_per_unit_price_group'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 11 THEN '" .lang('price_per_unit_hybrid'). "'
                                        ELSE {$this->db->dbprefix('settings')}.prioridad_precios_producto END AS '" .$key. "' ");
            }if ($key == 'descuento_orden') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.descuento_orden = 1 THEN '" .lang('descuento_orden_afecta_iva'). "'
                                        WHEN {$this->db->dbprefix('settings')}.descuento_orden = 2 THEN '" .lang('descuento_orden_no_afecta_iva'). "'
                                        WHEN {$this->db->dbprefix('settings')}.descuento_orden = 3 THEN '" .lang('descuento_orden_decidir'). "'
                                        ELSE {$this->db->dbprefix('settings')}.descuento_orden END AS '" .$key. "' ");
            }if ($key == 'allow_change_sale_iva') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.allow_change_sale_iva = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'tax_rate_traslate') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.tax_rate_traslate = 0, '" .lang('no'). "', '" .lang('yes'). "' ) AS '" .$key. "' ");
            }if ($key == 'get_companies_check_digit') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.get_companies_check_digit = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
            }if ($key == 'tipo_persona') {
                $this->db->select("{$this->db->dbprefix('types_person')}.description AS '" .$key. "' ");
            }if ($key == 'tipo_documento') {
                $this->db->select("{$this->db->dbprefix('documentypes')}.nombre AS '" .$key. "' ");
            }if ($key == 'allow_advanced_search') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.allow_advanced_search = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'rounding') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.rounding = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'purchase_payment_affects_cash_register') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register = 0 THEN '" .lang('purchase_payments_never_affects_register'). "'
                                        WHEN {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register = 1 THEN '" .lang('purchase_payments_always_affects_register'). "'
                                        WHEN {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register = 2 THEN '" .lang('always_ask_if_purchase_payments_affects_register'). "'
                                        ELSE {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register END AS '" .$key. "' ");
            }if ($key == 'electronic_billing') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.electronic_billing = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'fe_technology_provider') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('technology_providers')} WHERE id = {$this->db->dbprefix('settings')}.fe_technology_provider ) AS '" .$key. "' ");
            }if ($key == 'fe_work_environment') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.fe_work_environment = 1, '" .lang('production'). "', '" .lang('test'). "') AS '" .$key. "' ");
            }if ($key == 'management_consecutive_suppliers') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.management_consecutive_suppliers = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'profitability_margin_validation') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.profitability_margin_validation = 0 THEN '" .lang('no_validate'). "'
                                        WHEN {$this->db->dbprefix('settings')}.profitability_margin_validation = 1 THEN '" .lang('category'). "'
                                        WHEN {$this->db->dbprefix('settings')}.profitability_margin_validation = 2 THEN '" .lang('subcategory'). "'
                                        ELSE {$this->db->dbprefix('settings')}.profitability_margin_validation END AS '" .$key. "' ");
            }if ($key == 'prorate_shipping_cost') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.prorate_shipping_cost = 0 THEN '" .lang('never_prorate'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prorate_shipping_cost = 1 THEN '" .lang('always_prorate'). "'
                                        WHEN {$this->db->dbprefix('settings')}.prorate_shipping_cost = 2 THEN '" .lang('descuento_orden_decidir'). "'
                                        ELSE {$this->db->dbprefix('settings')}.prorate_shipping_cost END AS '" .$key. "' ");
            }if ($key == 'product_order') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.product_order = 1 THEN '" .lang('product_order_descending'). "'
                                        WHEN {$this->db->dbprefix('settings')}.product_order = 2 THEN '" .lang('product_order_ascending'). "'
                                        ELSE {$this->db->dbprefix('settings')}.product_order END AS '" .$key. "' ");
            }if ($key == 'keep_seller_from_user') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.keep_seller_from_user = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'invoice_values_greater_than_zero') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.invoice_values_greater_than_zero = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'except_category_taxes') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.except_category_taxes  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'category_tax_exception_tax_rate_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.category_tax_exception_tax_rate_id ) AS '" .$key. "' ");
            }if ($key == 'customer_territorial_decree_tax_rate_id') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.customer_territorial_decree_tax_rate_id ) AS '" .$key. "' ");
            }if ($key == 'ipoconsumo') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.ipoconsumo  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'default_return_payment_method') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.default_return_payment_method = '0' THEN '" .lang('select_during_sale_registration'). "'
                                        WHEN {$this->db->dbprefix('settings')}.default_return_payment_method = 'cash' THEN '" .lang('cash'). "'
                                        WHEN {$this->db->dbprefix('settings')}.default_return_payment_method = 'deposit' THEN '" .lang('deposit'). "'
                                        ELSE {$this->db->dbprefix('settings')}.default_return_payment_method END AS '" .$key. "' ");
            }if ($key == 'manual_payment_reference') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.manual_payment_reference  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'item_discount_apply_to') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.item_discount_apply_to = 1 THEN '" .lang('base'). "'
                                        WHEN {$this->db->dbprefix('settings')}.item_discount_apply_to = 2 THEN '" .lang('total_price'). "'
                                        ELSE {$this->db->dbprefix('settings')}.item_discount_apply_to END AS '" .$key. "' ");
            }if ($key == 'automatic_update') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.automatic_update  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_due_sales') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_due_sales  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_partial_sales') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_partial_sales  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_paid_sales') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_paid_sales  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_products_expirated') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_products_expirated  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_products_promo_expirated') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_products_promo_expirated  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_sales_per_biller') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_sales_per_biller  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_pos_registers') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_pos_registers  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'detail_zeta_report') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.detail_zeta_report  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'great_contributor') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.great_contributor  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'annual_closure') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.annual_closure  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'product_variant_per_serial') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.product_variant_per_serial  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'product_search_show_quantity') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.product_search_show_quantity  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'suspended_sales_to_retail') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.suspended_sales_to_retail  = 2, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'payments_methods_retcom') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.payments_methods_retcom  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'product_default_exempt_tax_rate') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.product_default_exempt_tax_rate ) AS '" .$key. "' ");
            }if ($key == 'ciiu_code') {
                $this->db->select("(SELECT description FROM {$this->db->dbprefix('ciiu_codes')} WHERE id = {$this->db->dbprefix('settings')}.ciiu_code ) AS '" .$key. "'");
            }if ($key == 'production_order_one_reference_limit') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.production_order_one_reference_limit  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'system_start_date') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.set_product_variant_suffix  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'disable_product_price_under_cost') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.disable_product_price_under_cost  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'wms_picking_filter_biller') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.wms_picking_filter_biller  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'enable_customer_tax_exemption') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.enable_customer_tax_exemption  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'block_movements_from_another_year') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.block_movements_from_another_year  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'product_transformation_validate_quantity') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.product_transformation_validate_quantity  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'tax_method') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.tax_method = 1 THEN '" .lang('exclusive'). "'
                                        WHEN {$this->db->dbprefix('settings')}.tax_method = 0 THEN '" .lang('inclusive'). "'
                                        ELSE {$this->db->dbprefix('settings')}.tax_method END AS '" .$key. "' ");
            }if ($key == 'show_alert_sale_expired') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.show_alert_sale_expired  = 0, '" .lang('dont_show'). "', '" .lang('show'). "') AS '" .$key. "' ");
            }if ($key == 'set_adjustment_cost') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.set_adjustment_cost  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'default_records_filter') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.default_records_filter = 0 THEN 'Todos los registros'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 1 THEN 'Registros de hoy'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 2 THEN 'Registros del mes actual'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 3 THEN 'Registros del trimestre actual'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 4 THEN 'Registros del año actual'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 5 THEN 'Registros por rango de fechas'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 6 THEN 'Registros del mes pasado'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 7 THEN 'Registros del año pasado'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 8 THEN 'Registros del semestre actual'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 9 THEN 'Registros del trimestre pasado'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 10 THEN 'Registros del semestre pasado'
                                        WHEN {$this->db->dbprefix('settings')}.default_records_filter = 11 THEN 'Registros de los últimos 3 meses'
                                        ELSE {$this->db->dbprefix('settings')}.default_records_filter END AS '" .$key. "' ");
            }if ($key == 'show_brand_in_product_search') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.show_brand_in_product_search  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'precios_por_unidad_presentacion') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion = 0 THEN '". lang('select') ."'
                                        WHEN {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion = 1 THEN '". lang('price_per_unit_unit') ."'
                                        WHEN {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion = 2 THEN '". lang('price_per_unit_presentation') ."'
                                        ELSE {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion END AS '" .$key. "' ");
            }if ($key == 'lock_cost_field_in_product') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.lock_cost_field_in_product  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'fuente_retainer') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.fuente_retainer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'iva_retainer') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.iva_retainer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'ica_retainer') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.ica_retainer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'export_to_csv_each_new_customer') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.export_to_csv_each_new_customer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'commision_payment_method') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.commision_payment_method = 1 THEN '" .lang('commision_payment_method_sales_paid'). "'
                                        WHEN {$this->db->dbprefix('settings')}.commision_payment_method = 2 THEN '" .lang('commision_payment_method_sales_partial'). "'
                                        ELSE {$this->db->dbprefix('settings')}.commision_payment_method END AS '" .$key. "' ");
            }if ($key == 'default_expense_id_to_pay_commisions') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('expense_categories')} WHERE id = {$this->db->dbprefix('settings')}.default_expense_id_to_pay_commisions ) AS '" .$key. "' ");
            }if ($key == 'users_can_view_price_groups') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.users_can_view_price_groups = 0 THEN '" .lang('users_can_view_price_groups_assigned'). "'
                                        WHEN {$this->db->dbprefix('settings')}.users_can_view_price_groups = 1 THEN '" .lang('users_can_view_price_groups_all'). "'
                                        ELSE {$this->db->dbprefix('settings')}.users_can_view_price_groups END AS '" .$key. "' ");
            }if ($key == 'users_can_view_warehouse_quantitys') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.users_can_view_warehouse_quantitys = 0 THEN '" .lang('users_can_view_warehouse_quantitys_assigned'). "'
                                        WHEN {$this->db->dbprefix('settings')}.users_can_view_warehouse_quantitys = 1 THEN '" .lang('users_can_view_warehouse_quantitys_all'). "'
                                        ELSE {$this->db->dbprefix('settings')}.users_can_view_warehouse_quantitys END AS '" .$key. "' ");
            }if ($key == 'send_electronic_invoice_to') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.send_electronic_invoice_to = 0 THEN '" .lang('select'). "'
                                        WHEN {$this->db->dbprefix('settings')}.send_electronic_invoice_to = 1 THEN '" .lang('main_email'). "'
                                        WHEN {$this->db->dbprefix('settings')}.send_electronic_invoice_to = 2 THEN '" .lang('customer_branch_email'). "'
                                        ELSE {$this->db->dbprefix('settings')}.send_electronic_invoice_to END AS '" .$key. "' ");
            }if ($key == 'copy_mail_to_sender') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.copy_mail_to_sender  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'add_individual_attachments') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.add_individual_attachments  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'product_crud_validate_cost') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.product_crud_validate_cost  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'alert_zero_cost_sale') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.alert_zero_cost_sale  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'barcode_reader_exact_search') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.barcode_reader_exact_search  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'which_cost_margin_validation') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.which_cost_margin_validation = 1 THEN '" .lang('product_cost'). "'
                                        WHEN {$this->db->dbprefix('settings')}.which_cost_margin_validation = 2 THEN '" .lang('avg_cost'). "'
                                        ELSE {$this->db->dbprefix('settings')}.which_cost_margin_validation END AS '" .$key. "' ");
            }if ($key == 'cron_job_db_backup') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_db_backup  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'cron_job_images_backup') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_images_backup  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'cron_job_ebilling_backup') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_ebilling_backup  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'cron_job_send_mail') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_send_mail  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'cron_job_db_backup_partitions') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_db_backup_partitions  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'purchase_send_advertisement') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.purchase_send_advertisement  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'ignore_purchases_edit_validations') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.ignore_purchases_edit_validations  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'big_data_limit_reports') {
                $this->db->select("IF({$this->db->dbprefix('settings')}.big_data_limit_reports  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
            }if ($key == 'purchase_tax_rate') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.purchase_tax_rate ) AS '" .$key. "' ");
            }if ($key == 'cashier_close') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.cashier_close = 0 THEN '" .lang('no'). "'
                                        WHEN {$this->db->dbprefix('settings')}.cashier_close = 1 THEN '" .lang('yes'). "'
                                        WHEN {$this->db->dbprefix('settings')}.cashier_close = 2 THEN '" .lang('cash_register_not_used'). "'
                                        ELSE {$this->db->dbprefix('settings')}.cashier_close END AS '" .$key. "' ");
            }if ($key == 'tax3') {
                $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.tax3 ) AS '" .$key. "' ");
            }if ($key == 'cost_center_selection') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.cost_center_selection = 0 THEN '" .lang('default_biller_cost_center'). "'
                                        WHEN {$this->db->dbprefix('settings')}.cost_center_selection = 1 THEN '" .lang('select_cost_center'). "'
                                        WHEN {$this->db->dbprefix('settings')}.cost_center_selection = 2 THEN '" .lang('disable'). "'
                                        ELSE {$this->db->dbprefix('settings')}.cost_center_selection END AS '" .$key. "' ");
            }if ($key == 'hide_products_in_zero_price') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.hide_products_in_zero_price = 0 THEN '" .lang('show'). "'
                                        WHEN {$this->db->dbprefix('settings')}.hide_products_in_zero_price = 1 THEN '" .lang('hide'). "'
                                        ELSE {$this->db->dbprefix('settings')}.hide_products_in_zero_price END AS '" .$key. "' ");
            }if ($key == 'customer_default_payment_type') {
                $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.customer_default_payment_type = 0 THEN '" .lang('credit'). "'
                                        WHEN {$this->db->dbprefix('settings')}.customer_default_payment_type = 1 THEN '" .lang('payment_type_cash'). "'
                                        ELSE {$this->db->dbprefix('settings')}.customer_default_payment_type END AS '" .$key. "' ");
            }else if ($key !== 'default_warehouse' && $key !== 'accounting_method' && $key !== 'default_currency' && $key !== 'default_tax_rate' && $key !== 'default_tax_rate2' && $key !== 'dateformat' && $key !== 'item_addition' && $key !== 'product_serial' && $key !== 'default_discount' && $key !== 'product_discount' && $key !== 'tax1' && $key !== 'overselling' && $key !== 'restrict_calendar' && $key !== 'watermark' && $key !== 'reg_ver' && $key !== 'allow_reg' && $key !== 'reg_notification' && $key !== 'customer_group' && $key !== 'mmode' && $key !== 'auto_detect_barcode' && $key !== 'captcha' && $key !== 'reference_format' && $key !== 'racks' && $key !== 'attributes' && $key !== 'product_expiry' && $key !== 'decimals_sep' && $key !== 'thousands_sep' && $key !== 'invoice_view' && $key !== 'default_biller' && $key !== 'rtl' && $key !== 'update' && $key !== 'sac' && $key !== 'display_all_products' && $key !== 'display_symbol' && $key !== 'remove_expired' && $key !== 'set_focus' && $key !== 'price_group' && $key !== 'barcode_img' && $key !== 'update_cost' && $key !== 'apis' && $key !== 'tipo_regimen' && $key !== 'modulary' && $key !== 'prioridad_precios_producto' && $key !== 'descuento_orden' && $key !== 'allow_change_sale_iva' && $key !== 'tax_rate_traslate' && $key !== 'get_companies_check_digit' && $key !== 'tipo_persona' && $key !== 'tipo_documento' && $key !== 'allow_advanced_search' && $key !== 'rounding' && $key !== 'purchase_payment_affects_cash_register' && $key !== 'electronic_billing' && $key !== 'fe_technology_provider' && $key !== 'fe_work_environment' && $key !== 'management_consecutive_suppliers' && $key !== 'profitability_margin_validation' && $key !== 'prorate_shipping_cost' && $key !== 'product_order' && $key !== 'keep_seller_from_user' && $key !== 'invoice_values_greater_than_zero' && $key !== 'except_category_taxes' && $key !== 'category_tax_exception_tax_rate_id' && $key !== 'customer_territorial_decree_tax_rate_id' && $key !== 'ipoconsumo' && $key !== 'default_return_payment_method' && $key !== 'manual_payment_reference' && $key !== 'item_discount_apply_to' && $key !== 'automatic_update' && $key !== 'detail_due_sales' && $key !== 'detail_partial_sales' && $key !== 'detail_paid_sales' && $key !== 'detail_products_expirated' && $key !== 'detail_products_promo_expirated' && $key !== 'detail_sales_per_biller' && $key !== 'detail_pos_registers' && $key !== 'detail_zeta_report' && $key !== 'great_contributor' && $key !== 'annual_closure' && $key !== 'product_variant_per_serial' && $key !== 'product_search_show_quantity' && $key !== 'suspended_sales_to_retail' && $key !== 'payments_methods_retcom' && $key !== 'product_default_exempt_tax_rate' && $key !== 'ciiu_code' && $key !== 'production_order_one_reference_limit' && $key !== 'system_start_date' && $key !== 'disable_product_price_under_cost' && $key !== 'wms_picking_filter_biller' && $key !== 'enable_customer_tax_exemption' && $key !== 'block_movements_from_another_year' && $key !== 'product_transformation_validate_quantity' && $key !== 'tax_method' && $key !== 'show_alert_sale_expired' && $key !== 'set_adjustment_cost' && $key !== 'default_records_filter' && $key !== 'show_brand_in_product_search' && $key !== 'precios_por_unidad_presentacion' && $key !== 'lock_cost_field_in_product' && $key !== 'fuente_retainer' && $key !== 'iva_retainer' && $key !== 'ica_retainer' && $key !== 'export_to_csv_each_new_customer' && $key !== 'commision_payment_method' && $key !== 'default_expense_id_to_pay_commisions' && $key !== 'users_can_view_price_groups' && $key !== 'users_can_view_warehouse_quantitys' && $key !== 'send_electronic_invoice_to' && $key !== 'copy_mail_to_sender' && $key !== 'add_individual_attachments' && $key !== 'product_crud_validate_cost' && $key !== 'alert_zero_cost_sale' && $key !== 'barcode_reader_exact_search' && $key !== 'which_cost_margin_validation' && $key !== 'cron_job_db_backup' && $key !== 'cron_job_images_backup' && $key !== 'cron_job_ebilling_backup' && $key !== 'cron_job_send_mail' && $key !== 'cron_job_db_backup_partitions' && $key !== 'purchase_send_advertisement' && $key !== 'ignore_purchases_edit_validations' && $key !== 'big_data_limit_reports' && $key !== 'purchase_tax_rate' && $key !== 'cashier_close' && $key !== 'tax3' && $key !== 'cost_center_selection' && $key !== 'hide_products_in_zero_price' && $key !== 'customer_default_payment_type') {
                $this->db->select($this->db->dbprefix('settings').'.'.$key. ' AS "' .$key. '"' );
            }
        }
        $this->db->from($this->db->dbprefix('settings'));
        $this->db->join($this->db->dbprefix('warehouses'), $this->db->dbprefix('warehouses').'.id = '.$this->db->dbprefix('settings').'.default_warehouse', 'inner');
        $this->db->join($this->db->dbprefix('currencies'), $this->db->dbprefix('currencies').'.code = '.$this->db->dbprefix('settings').'.default_currency', 'inner');
        $this->db->join($this->db->dbprefix('date_format'), $this->db->dbprefix('date_format').'.id = '.$this->db->dbprefix('settings').'.dateformat', 'inner');
        $this->db->join($this->db->dbprefix('customer_groups'), $this->db->dbprefix('customer_groups').'.id = '.$this->db->dbprefix('settings').'.customer_group', 'inner');
        $this->db->join($this->db->dbprefix('price_groups'), $this->db->dbprefix('price_groups').'.id = '.$this->db->dbprefix('settings').'.price_group', 'inner');
        $this->db->join($this->db->dbprefix('types_vat_regime'), $this->db->dbprefix('types_vat_regime').'.id = '.$this->db->dbprefix('settings').'.tipo_regimen', 'inner');
        $this->db->join($this->db->dbprefix('types_person'), $this->db->dbprefix('types_person').'.id = '.$this->db->dbprefix('settings').'.tipo_persona', 'inner');
        $this->db->join($this->db->dbprefix('documentypes'), $this->db->dbprefix('documentypes').'.codigo_doc = '.$this->db->dbprefix('settings').'.tipo_documento', 'inner');
        $q1 = $this->db->get();
        if ($q1->num_rows() > 0) {
            $initialData = $q1->row_array();
        }

        // MANEJO  TYPE OBLIGATIONS
        $initialObligations = '';
        $updateObligations = '';
        $changes_obligations = '';
        $consulta_initialObligations = $this->db->select("{$this->db->dbprefix('types_customer_obligations')}.types_obligations_id AS types_obligations_id")
                                                ->from($this->db->dbprefix('types_customer_obligations'))
                                                ->where($this->db->dbprefix('types_customer_obligations').'.customer_id', 1);
        $q4 = $this->db->get();
        if ($q4->num_rows() > 0) {
            foreach (($q4->result_array()) as $row) {
                $dataInitialObligations[] = $row['types_obligations_id'];
            }
        }
        $differences_obligations1 = array_diff($dataInitialObligations, $dataUpdateObligations);
        $differences_obligations2 = array_diff($dataUpdateObligations, $dataInitialObligations);
        if (!empty($differences_obligations1) || !empty($differences_obligations2)){
            $consulta_type_customer_obligationsInitial = $this->db->select($this->db->dbprefix('types_obligations-responsabilities').'.description AS descripcion')
                                                            ->from($this->db->dbprefix('types_obligations-responsabilities'))
                                                            ->join($this->db->dbprefix('types_customer_obligations'), $this->db->dbprefix('types_customer_obligations').'.types_obligations_id = '.$this->db->dbprefix('types_obligations-responsabilities').'.code', 'inner')
                                                            ->where($this->db->dbprefix('types_customer_obligations').'.customer_id', 1);
            $q3 = $this->db->get();
            if ($q3->num_rows() > 0) {
                foreach (($q3->result_array()) as $row) {
                    $initialObligations .= $row['descripcion']. ", ";
                }
                $initialObligations = trim($initialObligations, ", ");
            }
            $consulta_type_customer_obligationsUpdate = $this->db->select($this->db->dbprefix('types_obligations-responsabilities').'.description AS descripcion')
                                                            ->from($this->db->dbprefix('types_obligations-responsabilities'))
                                                            ->where_in($this->db->dbprefix('types_obligations-responsabilities').'.code', $dataUpdateObligations);
            $q5 = $this->db->get();
            if ($q5->num_rows() > 0) {
                foreach (($q5->result_array()) as $row) {
                    $updateObligations .= $row['descripcion']. ", ";
                }
                $updateObligations = trim($updateObligations, ", ");
            }
            $changes_obligations = lang('label_types_obligations'). ' ' .lang('from'). ': ' .$initialObligations. '  ' .lang('to'). ': ' .$updateObligations;
        }
        // END MANEJO DE TYPE OBLIGATIOS

        // ********************************************************************************************************************************************************//

        $data['last_update'] = date('Y-m-d H:i:s');
        $this->db->where('setting_id', '1');
        if ($this->db->update('settings', $data)) {
            if (count($tax_rate_traslate) > 0) {
                foreach ($tax_rate_traslate as $ap) {
                    if (isset($ap['id'])) {
                        $this->site->updatePaymentMethodParameter($ap);
                    } else {
                        $this->site->insertPaymentMethodParameter($ap);
                    }
                }
            }
            if ($data['tax_method'] != 2) {
                $this->site->updateAllProductsTaxMethod($data['tax_method']);
            }

            // PROCEDURE TO ENTER CHANGES IN PARAMETERS TO USER ACTIVITIES
            foreach ($data as $key => $value) {
                if ($key == 'default_warehouse') {
                    $this->db->select($this->db->dbprefix('warehouses').'.name AS "'.$key.'"');
                }if ($key == 'accounting_method') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.accounting_method = 1 THEN '" .lang('FIFO'). "'
                                            WHEN {$this->db->dbprefix('settings')}.accounting_method = 2 THEN '" .lang('AVCO'). "'
                                            ELSE {$this->db->dbprefix('settings')}.accounting_method END AS 'accounting_method' ");
                }if ($key == 'default_currency') {
                    $this->db->select($this->db->dbprefix('currencies').'.name AS "'.$key.'"');
                }if ($key == 'default_tax_rate') {
                    $this->db->select('(select NAME from '.$this->db->dbprefix('tax_rates'). ' where id = ' . $this->db->dbprefix('settings').'.default_tax_rate) AS "' .$key. '"' );
                }if ($key == 'default_tax_rate2') {
                    $this->db->select('(select NAME from '.$this->db->dbprefix('tax_rates'). ' where id = ' . $this->db->dbprefix('settings').'.default_tax_rate2) AS "' .$key. '"' );
                }if ($key == 'dateformat') {
                    $this->db->select($this->db->dbprefix('date_format').'.js AS "'.$key.'"');
                }if ($key == 'item_addition') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.item_addition = 0, '" .lang('add_new_item'). "', '" .lang('increase_quantity_if_item_exist'). "') AS '" .$key. "'  " );
                }if ($key == 'product_serial') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.product_serial = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' " );
                }if ($key == 'default_discount') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.default_discount = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' " );
                }if ($key == 'product_discount') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.product_discount = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' " );
                }if ($key == 'tax1') {
                    $this->db->select('(SELECT name FROM ' .$this->db->dbprefix('tax_rates'). ' WHERE id = ' .$this->db->dbprefix('settings'). '.tax1 ) AS  "' .$key. '"');
                }if ($key == 'overselling') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.overselling = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "'  " );
                }if ($key == 'restrict_calendar') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.restrict_calendar = 0, '" .lang('shared'). "', '" .lang('private'). "') AS '" .$key. "' " );
                }if ($key == 'watermark') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.watermark = 0, '" .lang('no'). "', '" .lang('yes'). "' ) AS '" .$key. "' " );
                }if ($key == 'reg_ver') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.reg_ver = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' " );
                }if ($key == 'allow_reg') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.allow_reg = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' " );
                }if ($key == 'reg_notification') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.reg_notification = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' " );
                }if ($key == 'customer_group') {
                    $this->db->select("{$this->db->dbprefix('customer_groups')}.name AS '" .$key. "'");
                }if ($key == 'mmode') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.mmode = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'auto_detect_barcode') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.auto_detect_barcode = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'captcha') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.captcha = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'reference_format') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.reference_format = 1 THEN '" .lang('prefix_year_no'). "'
                                            WHEN {$this->db->dbprefix('settings')}.reference_format = 2 THEN '" .lang('prefix_month_year_no'). "'
                                            WHEN {$this->db->dbprefix('settings')}.reference_format = 3 THEN '" .lang('sequence_number'). "'
                                            WHEN {$this->db->dbprefix('settings')}.reference_format = 4 THEN '" .lang('random_number'). "'
                                            ELSE {$this->db->dbprefix('settings')}.reference_format END AS '" .$key. "' ");
                }if ($key == 'racks') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.racks = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'attributes') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.attributes = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'product_expiry') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.product_expiry = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'decimals_sep') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.decimals_sep = '.', '" .lang('dot'). "', '" .lang('comma'). "') AS '" .$key. "' ");
                }if ($key == 'thousands_sep') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.thousands_sep = '.' THEN '" .lang('dot'). "'
                                            WHEN {$this->db->dbprefix('settings')}.thousands_sep = ',' THEN '" .lang('comma'). "'
                                            WHEN {$this->db->dbprefix('settings')}.thousands_sep = 0 THEN '" .lang('space'). "'
                                            ELSE {$this->db->dbprefix('settings')}.thousands_sep END AS '" .$key. "' ");
                }if ($key == 'invoice_view') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.invoice_view = 1 THEN '" .lang('tax_invoice'). "'
                                            WHEN {$this->db->dbprefix('settings')}.invoice_view = 0 THEN '" .lang('standard'). "'
                                            ELSE {$this->db->dbprefix('settings')}.invoice_view END AS '" .$key. "' ");
                }if ($key == 'default_biller') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('settings')}.default_biller ) AS '" .$key. "' ");
                }if ($key == 'rtl') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.rtl = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "'");
                }if ($key == 'update') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.update = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'sac') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.sac = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'display_all_products') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.display_all_products = 0, '" .lang('hide_with_0_qty'). "', '" .lang('show_with_0_qty'). "') AS '" .$key. "' ");
                }if ($key == 'display_symbol') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.display_symbol = 0 THEN '" .lang('disable'). "'
                                            WHEN {$this->db->dbprefix('settings')}.display_symbol = 1 THEN '" .lang('before'). "'
                                            WHEN {$this->db->dbprefix('settings')}.display_symbol = 2 THEN '" .lang('after'). "'
                                            ELSE {$this->db->dbprefix('settings')}.display_symbol END AS '" .$key. "' ");
                }if ($key == 'remove_expired') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.remove_expired = 0, '" .lang('no').', '.lang('i_ll_remove'). "', '" .lang('yes').', '.lang('remove_automatically'). "') AS '" .$key. "' ");
                }if ($key == 'set_focus') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.set_focus = 0, '" .lang('add_item_input'). "', '" .lang('last_order_item'). "') AS '" .$key. "' ");
                }if ($key == 'price_group') {
                    $this->db->select("{$this->db->dbprefix('price_groups')}.name AS '" .$key. "' ");
                }if ($key == 'barcode_img') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.barcode_img = 0, '" .lang('image'). "', '" .lang('svg'). "') AS '" .$key. "' ");
                }if ($key == 'update_cost') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.update_cost = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'apis') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.apis = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'tipo_regimen') {
                    $this->db->select("{$this->db->dbprefix('types_vat_regime')}.description AS '" .$key. "' ");
                }if ($key == 'modulary') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.modulary = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'prioridad_precios_producto') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 1 THEN '" .lang('price_group_base_with_promotions'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 2 THEN '" .lang('biller_price_group'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 3 THEN '" .lang('customer_price_group'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 4 THEN '" .lang('biller_price_group_with_discounts'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 5 THEN '" .lang('price_per_unit'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 6 THEN '" .lang('customer_price_group_with_discounts'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 7 THEN '" .lang('price_per_unit_manual'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 8 THEN '" .lang('customer_address_price_group'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 9 THEN '" .lang('customer_address_price_group_with_discounts'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 10 THEN '" .lang('price_per_unit_price_group'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prioridad_precios_producto = 11 THEN '" .lang('price_per_unit_hybrid'). "'
                                            ELSE {$this->db->dbprefix('settings')}.prioridad_precios_producto END AS '" .$key. "' ");
                }if ($key == 'descuento_orden') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.descuento_orden = 1 THEN '" .lang('descuento_orden_afecta_iva'). "'
                                            WHEN {$this->db->dbprefix('settings')}.descuento_orden = 2 THEN '" .lang('descuento_orden_no_afecta_iva'). "'
                                            WHEN {$this->db->dbprefix('settings')}.descuento_orden = 3 THEN '" .lang('descuento_orden_decidir'). "'
                                            ELSE {$this->db->dbprefix('settings')}.descuento_orden END AS '" .$key. "' ");
                }if ($key == 'allow_change_sale_iva') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.allow_change_sale_iva = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'tax_rate_traslate') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.tax_rate_traslate = 0, '" .lang('no'). "', '" .lang('yes'). "' ) AS '" .$key. "' ");
                }if ($key == 'get_companies_check_digit') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.get_companies_check_digit = 0, '" .lang('disable'). "', '" .lang('enable'). "') AS '" .$key. "' ");
                }if ($key == 'tipo_persona') {
                    $this->db->select("{$this->db->dbprefix('types_person')}.description AS '" .$key. "' ");
                }if ($key == 'tipo_documento') {
                    $this->db->select("{$this->db->dbprefix('documentypes')}.nombre AS '" .$key. "' ");
                }if ($key == 'allow_advanced_search') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.allow_advanced_search = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'rounding') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.rounding = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'purchase_payment_affects_cash_register') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register = 0 THEN '" .lang('purchase_payments_never_affects_register'). "'
                                            WHEN {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register = 1 THEN '" .lang('purchase_payments_always_affects_register'). "'
                                            WHEN {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register = 2 THEN '" .lang('always_ask_if_purchase_payments_affects_register'). "'
                                            ELSE {$this->db->dbprefix('settings')}.purchase_payment_affects_cash_register END AS '" .$key. "' ");
                }if ($key == 'electronic_billing') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.electronic_billing = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'fe_technology_provider') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('technology_providers')} WHERE id = {$this->db->dbprefix('settings')}.fe_technology_provider ) AS '" .$key. "' ");
                }if ($key == 'fe_work_environment') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.fe_work_environment = 1, '" .lang('production'). "', '" .lang('test'). "') AS '" .$key. "' ");
                }if ($key == 'management_consecutive_suppliers') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.management_consecutive_suppliers = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'profitability_margin_validation') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.profitability_margin_validation = 0 THEN '" .lang('no_validate'). "'
                                            WHEN {$this->db->dbprefix('settings')}.profitability_margin_validation = 1 THEN '" .lang('category'). "'
                                            WHEN {$this->db->dbprefix('settings')}.profitability_margin_validation = 2 THEN '" .lang('subcategory'). "'
                                            ELSE {$this->db->dbprefix('settings')}.profitability_margin_validation END AS '" .$key. "' ");
                }if ($key == 'prorate_shipping_cost') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.prorate_shipping_cost = 0 THEN '" .lang('never_prorate'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prorate_shipping_cost = 1 THEN '" .lang('always_prorate'). "'
                                            WHEN {$this->db->dbprefix('settings')}.prorate_shipping_cost = 2 THEN '" .lang('descuento_orden_decidir'). "'
                                            ELSE {$this->db->dbprefix('settings')}.prorate_shipping_cost END AS '" .$key. "' ");
                }if ($key == 'product_order') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.product_order = 1 THEN '" .lang('product_order_descending'). "'
                                            WHEN {$this->db->dbprefix('settings')}.product_order = 2 THEN '" .lang('product_order_ascending'). "'
                                            ELSE {$this->db->dbprefix('settings')}.product_order END AS '" .$key. "' ");
                }if ($key == 'keep_seller_from_user') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.keep_seller_from_user = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'invoice_values_greater_than_zero') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.invoice_values_greater_than_zero = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'except_category_taxes') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.except_category_taxes  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'category_tax_exception_tax_rate_id') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.category_tax_exception_tax_rate_id ) AS '" .$key. "' ");
                }if ($key == 'customer_territorial_decree_tax_rate_id') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.customer_territorial_decree_tax_rate_id ) AS '" .$key. "' ");
                }if ($key == 'ipoconsumo') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.ipoconsumo  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'default_return_payment_method') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.default_return_payment_method = '0' THEN '" .lang('select_during_sale_registration'). "'
                                            WHEN {$this->db->dbprefix('settings')}.default_return_payment_method = 'cash' THEN '" .lang('cash'). "'
                                            WHEN {$this->db->dbprefix('settings')}.default_return_payment_method = 'deposit' THEN '" .lang('deposit'). "'
                                            ELSE {$this->db->dbprefix('settings')}.default_return_payment_method END AS '" .$key. "' ");
                }if ($key == 'manual_payment_reference') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.manual_payment_reference  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'item_discount_apply_to') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.item_discount_apply_to = 1 THEN '" .lang('base'). "'
                                            WHEN {$this->db->dbprefix('settings')}.item_discount_apply_to = 2 THEN '" .lang('total_price'). "'
                                            ELSE {$this->db->dbprefix('settings')}.item_discount_apply_to END AS '" .$key. "' ");
                }if ($key == 'automatic_update') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.automatic_update  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_due_sales') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_due_sales  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_partial_sales') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_partial_sales  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_paid_sales') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_paid_sales  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_products_expirated') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_products_expirated  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_products_promo_expirated') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_products_promo_expirated  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_sales_per_biller') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_sales_per_biller  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_pos_registers') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_pos_registers  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'detail_zeta_report') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.detail_zeta_report  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'great_contributor') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.great_contributor  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'annual_closure') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.annual_closure  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'product_variant_per_serial') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.product_variant_per_serial  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'product_search_show_quantity') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.product_search_show_quantity  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'suspended_sales_to_retail') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.suspended_sales_to_retail  = 2, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'payments_methods_retcom') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.payments_methods_retcom  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'product_default_exempt_tax_rate') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.product_default_exempt_tax_rate ) AS '" .$key. "' ");
                }if ($key == 'ciiu_code') {
                    $this->db->select("(SELECT description FROM {$this->db->dbprefix('ciiu_codes')} WHERE id = {$this->db->dbprefix('settings')}.ciiu_code ) AS '" .$key. "'");
                }if ($key == 'production_order_one_reference_limit') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.production_order_one_reference_limit  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'system_start_date') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.set_product_variant_suffix  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'disable_product_price_under_cost') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.disable_product_price_under_cost  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'wms_picking_filter_biller') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.wms_picking_filter_biller  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'enable_customer_tax_exemption') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.enable_customer_tax_exemption  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'block_movements_from_another_year') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.block_movements_from_another_year  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'product_transformation_validate_quantity') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.product_transformation_validate_quantity  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'tax_method') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.tax_method = 1 THEN '" .lang('exclusive'). "'
                                            WHEN {$this->db->dbprefix('settings')}.tax_method = 0 THEN '" .lang('inclusive'). "'
                                            ELSE {$this->db->dbprefix('settings')}.tax_method END AS '" .$key. "' ");
                }if ($key == 'show_alert_sale_expired') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.show_alert_sale_expired  = 0, '" .lang('dont_show'). "', '" .lang('show'). "') AS '" .$key. "' ");
                }if ($key == 'set_adjustment_cost') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.set_adjustment_cost  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'default_records_filter') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.default_records_filter = 0 THEN 'Todos los registros'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 1 THEN 'Registros de hoy'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 2 THEN 'Registros del mes actual'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 3 THEN 'Registros del trimestre actual'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 4 THEN 'Registros del año actual'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 5 THEN 'Registros por rango de fechas'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 6 THEN 'Registros del mes pasado'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 7 THEN 'Registros del año pasado'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 8 THEN 'Registros del semestre actual'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 9 THEN 'Registros del trimestre pasado'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 10 THEN 'Registros del semestre pasado'
                                            WHEN {$this->db->dbprefix('settings')}.default_records_filter = 11 THEN 'Registros de los últimos 3 meses'
                                            ELSE {$this->db->dbprefix('settings')}.default_records_filter END AS '" .$key. "' ");
                }if ($key == 'show_brand_in_product_search') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.show_brand_in_product_search  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'precios_por_unidad_presentacion') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion = 0 THEN '". lang('select') ."'
                                            WHEN {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion = 1 THEN '". lang('price_per_unit_unit') ."'
                                            WHEN {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion = 2 THEN '". lang('price_per_unit_presentation') ."'
                                            ELSE {$this->db->dbprefix('settings')}.precios_por_unidad_presentacion END AS '" .$key. "' ");
                }if ($key == 'lock_cost_field_in_product') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.lock_cost_field_in_product  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'fuente_retainer') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.fuente_retainer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'iva_retainer') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.iva_retainer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'ica_retainer') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.ica_retainer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'export_to_csv_each_new_customer') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.export_to_csv_each_new_customer  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'commision_payment_method') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.commision_payment_method = 1 THEN '" .lang('commision_payment_method_sales_paid'). "'
                                            WHEN {$this->db->dbprefix('settings')}.commision_payment_method = 2 THEN '" .lang('commision_payment_method_sales_partial'). "'
                                            ELSE {$this->db->dbprefix('settings')}.commision_payment_method END AS '" .$key. "' ");
                }if ($key == 'default_expense_id_to_pay_commisions') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('expense_categories')} WHERE id = {$this->db->dbprefix('settings')}.default_expense_id_to_pay_commisions ) AS '" .$key. "' ");
                }if ($key == 'users_can_view_price_groups') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.users_can_view_price_groups = 0 THEN '" .lang('users_can_view_price_groups_assigned'). "'
                                            WHEN {$this->db->dbprefix('settings')}.users_can_view_price_groups = 1 THEN '" .lang('users_can_view_price_groups_all'). "'
                                            ELSE {$this->db->dbprefix('settings')}.users_can_view_price_groups END AS '" .$key. "' ");
                }if ($key == 'users_can_view_warehouse_quantitys') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.users_can_view_warehouse_quantitys = 0 THEN '" .lang('users_can_view_warehouse_quantitys_assigned'). "'
                                            WHEN {$this->db->dbprefix('settings')}.users_can_view_warehouse_quantitys = 1 THEN '" .lang('users_can_view_warehouse_quantitys_all'). "'
                                            ELSE {$this->db->dbprefix('settings')}.users_can_view_warehouse_quantitys END AS '" .$key. "' ");
                }if ($key == 'send_electronic_invoice_to') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.send_electronic_invoice_to = 0 THEN '" .lang('select'). "'
                                            WHEN {$this->db->dbprefix('settings')}.send_electronic_invoice_to = 1 THEN '" .lang('main_email'). "'
                                            WHEN {$this->db->dbprefix('settings')}.send_electronic_invoice_to = 2 THEN '" .lang('customer_branch_email'). "'
                                            ELSE {$this->db->dbprefix('settings')}.send_electronic_invoice_to END AS '" .$key. "' ");
                }if ($key == 'copy_mail_to_sender') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.copy_mail_to_sender  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'add_individual_attachments') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.add_individual_attachments  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'product_crud_validate_cost') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.product_crud_validate_cost  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'alert_zero_cost_sale') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.alert_zero_cost_sale  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'barcode_reader_exact_search') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.barcode_reader_exact_search  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'which_cost_margin_validation') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.which_cost_margin_validation = 1 THEN '" .lang('product_cost'). "'
                                            WHEN {$this->db->dbprefix('settings')}.which_cost_margin_validation = 2 THEN '" .lang('avg_cost'). "'
                                            ELSE {$this->db->dbprefix('settings')}.which_cost_margin_validation END AS '" .$key. "' ");
                }if ($key == 'cron_job_db_backup') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_db_backup  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'cron_job_images_backup') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_images_backup  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'cron_job_ebilling_backup') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_ebilling_backup  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'cron_job_send_mail') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_send_mail  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'cron_job_db_backup_partitions') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.cron_job_db_backup_partitions  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'purchase_send_advertisement') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.purchase_send_advertisement  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'ignore_purchases_edit_validations') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.ignore_purchases_edit_validations  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'big_data_limit_reports') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.big_data_limit_reports  = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'purchase_tax_rate') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.purchase_tax_rate ) AS '" .$key. "' ");
                }if ($key == 'cashier_close') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.cashier_close = 0 THEN '" .lang('no'). "'
                                            WHEN {$this->db->dbprefix('settings')}.cashier_close = 1 THEN '" .lang('yes'). "'
                                            WHEN {$this->db->dbprefix('settings')}.cashier_close = 2 THEN '" .lang('cash_register_not_used'). "'
                                            ELSE {$this->db->dbprefix('settings')}.cashier_close END AS '" .$key. "' ");
                }if ($key == 'tax3') {
                    $this->db->select("(SELECT name FROM {$this->db->dbprefix('tax_rates')} WHERE id = {$this->db->dbprefix('settings')}.tax3 ) AS '" .$key. "' ");
                }if ($key == 'cost_center_selection') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.cost_center_selection = 0 THEN '" .lang('default_biller_cost_center'). "'
                                            WHEN {$this->db->dbprefix('settings')}.cost_center_selection = 1 THEN '" .lang('select_cost_center'). "'
                                            WHEN {$this->db->dbprefix('settings')}.cost_center_selection = 2 THEN '" .lang('disable'). "'
                                            ELSE {$this->db->dbprefix('settings')}.cost_center_selection END AS '" .$key. "' ");
                }if ($key == 'hide_products_in_zero_price') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.hide_products_in_zero_price = 0 THEN '" .lang('show'). "'
                                            WHEN {$this->db->dbprefix('settings')}.hide_products_in_zero_price = 1 THEN '" .lang('hide'). "'
                                            ELSE {$this->db->dbprefix('settings')}.hide_products_in_zero_price END AS '" .$key. "' ");
                }if ($key == 'update_prices_from_purchases') {
                    $this->db->select("IF({$this->db->dbprefix('settings')}.update_prices_from_purchases = 0, '" .lang('no'). "', '" .lang('yes'). "') AS '" .$key. "' ");
                }if ($key == 'customer_default_payment_type') {
                    $this->db->select("CASE WHEN {$this->db->dbprefix('settings')}.customer_default_payment_type = 0 THEN '" .lang('credit'). "'
                                            WHEN {$this->db->dbprefix('settings')}.customer_default_payment_type = 1 THEN '" .lang('payment_type_cash'). "'
                                            ELSE {$this->db->dbprefix('settings')}.customer_default_payment_type END AS '" .$key. "' ");
                }else if ($key !== 'default_warehouse' && $key !== 'accounting_method' && $key !== 'default_currency' && $key !== 'default_tax_rate' && $key !== 'default_tax_rate2' && $key !== 'dateformat' && $key !== 'item_addition' && $key !== 'product_serial' && $key !== 'default_discount' && $key !== 'product_discount' && $key !== 'tax1' && $key !== 'overselling' && $key !== 'restrict_calendar' && $key !== 'watermark' && $key !== 'reg_ver' && $key !== 'allow_reg' && $key !== 'reg_notification' && $key !== 'customer_group' && $key !== 'mmode' && $key !== 'auto_detect_barcode' && $key !== 'captcha' && $key !== 'reference_format' && $key !== 'racks' && $key !== 'attributes' && $key !== 'product_expiry' && $key !== 'decimals_sep' && $key !== 'thousands_sep' && $key !== 'invoice_view' && $key !== 'default_biller' && $key !== 'rtl' && $key !== 'update' && $key !== 'sac' && $key !== 'display_all_products' && $key !== 'display_symbol' && $key !== 'remove_expired' && $key !== 'set_focus' && $key !== 'price_group' && $key !== 'barcode_img' && $key !== 'update_cost' && $key !== 'apis' && $key !== 'tipo_regimen' && $key !== 'modulary' && $key !== 'prioridad_precios_producto' && $key !== 'descuento_orden' && $key !== 'allow_change_sale_iva' && $key !== 'tax_rate_traslate' && $key !== 'get_companies_check_digit' && $key !== 'tipo_persona' && $key !== 'tipo_documento' && $key !== 'allow_advanced_search' && $key !== 'rounding' && $key !== 'purchase_payment_affects_cash_register' && $key !== 'electronic_billing' && $key !== 'fe_technology_provider' && $key !== 'fe_work_environment' && $key !== 'management_consecutive_suppliers' && $key !== 'profitability_margin_validation' && $key !== 'prorate_shipping_cost' && $key !== 'product_order' && $key !== 'keep_seller_from_user' && $key !== 'invoice_values_greater_than_zero' && $key !== 'except_category_taxes' && $key !== 'category_tax_exception_tax_rate_id' && $key !== 'customer_territorial_decree_tax_rate_id' && $key !== 'ipoconsumo' && $key !== 'default_return_payment_method' && $key !== 'manual_payment_reference' && $key !== 'item_discount_apply_to' && $key !== 'automatic_update' && $key !== 'detail_due_sales' && $key !== 'detail_partial_sales' && $key !== 'detail_paid_sales' && $key !== 'detail_products_expirated' && $key !== 'detail_products_promo_expirated' && $key !== 'detail_sales_per_biller' && $key !== 'detail_pos_registers' && $key !== 'detail_zeta_report' && $key !== 'great_contributor' && $key !== 'annual_closure' && $key !== 'product_variant_per_serial' && $key !== 'product_search_show_quantity' && $key !== 'suspended_sales_to_retail' && $key !== 'payments_methods_retcom' && $key !== 'product_default_exempt_tax_rate' && $key !== 'ciiu_code' && $key !== 'production_order_one_reference_limit' && $key !== 'system_start_date' && $key !== 'disable_product_price_under_cost' && $key !== 'wms_picking_filter_biller' && $key !== 'enable_customer_tax_exemption' && $key !== 'block_movements_from_another_year' && $key !== 'product_transformation_validate_quantity' && $key !== 'tax_method' && $key !== 'show_alert_sale_expired' && $key !== 'set_adjustment_cost' && $key !== 'default_records_filter' && $key !== 'show_brand_in_product_search' && $key !== 'precios_por_unidad_presentacion' && $key !== 'lock_cost_field_in_product' && $key !== 'fuente_retainer' && $key !== 'iva_retainer' && $key !== 'ica_retainer' && $key !== 'export_to_csv_each_new_customer' && $key !== 'commision_payment_method' && $key !== 'default_expense_id_to_pay_commisions' && $key !== 'users_can_view_price_groups' && $key !== 'users_can_view_warehouse_quantitys' && $key !== 'send_electronic_invoice_to' && $key !== 'copy_mail_to_sender' && $key !== 'add_individual_attachments' && $key !== 'product_crud_validate_cost' && $key !== 'alert_zero_cost_sale' && $key !== 'barcode_reader_exact_search' && $key !== 'which_cost_margin_validation' && $key !== 'cron_job_db_backup' && $key !== 'cron_job_images_backup' && $key !== 'cron_job_ebilling_backup' && $key !== 'cron_job_send_mail' && $key !== 'cron_job_db_backup_partitions' && $key !== 'purchase_send_advertisement' && $key !== 'ignore_purchases_edit_validations' && $key !== 'big_data_limit_reports' && $key !== 'purchase_tax_rate' && $key !== 'cashier_close' && $key !== 'tax3' && $key !== 'cost_center_selection' && $key !== 'hide_products_in_zero_price' && $key !== 'customer_default_payment_type' && $key !== 'last_update') {
                    $this->db->select($this->db->dbprefix('settings').'.'.$key. ' AS "' .$key. '"' );
                }
            }
            $this->db->from($this->db->dbprefix('settings'));
            $this->db->join($this->db->dbprefix('warehouses'), $this->db->dbprefix('warehouses').'.id = '.$this->db->dbprefix('settings').'.default_warehouse', 'inner');
            $this->db->join($this->db->dbprefix('currencies'), $this->db->dbprefix('currencies').'.code = '.$this->db->dbprefix('settings').'.default_currency', 'inner');
            $this->db->join($this->db->dbprefix('date_format'), $this->db->dbprefix('date_format').'.id = '.$this->db->dbprefix('settings').'.dateformat', 'inner');
            $this->db->join($this->db->dbprefix('customer_groups'), $this->db->dbprefix('customer_groups').'.id = '.$this->db->dbprefix('settings').'.customer_group', 'inner');
            $this->db->join($this->db->dbprefix('price_groups'), $this->db->dbprefix('price_groups').'.id = '.$this->db->dbprefix('settings').'.price_group', 'inner');
            $this->db->join($this->db->dbprefix('types_vat_regime'), $this->db->dbprefix('types_vat_regime').'.id = '.$this->db->dbprefix('settings').'.tipo_regimen', 'inner');
            $this->db->join($this->db->dbprefix('types_person'), $this->db->dbprefix('types_person').'.id = '.$this->db->dbprefix('settings').'.tipo_persona', 'inner');
            $this->db->join($this->db->dbprefix('documentypes'), $this->db->dbprefix('documentypes').'.codigo_doc = '.$this->db->dbprefix('settings').'.tipo_documento', 'inner');
            $q2 = $this->db->get();
            if ($q2->num_rows() > 0) {
                $updateData = $q2->row_array();
            }
            if (isset($updateData) && isset($initialData)) {
                 $differences = array_diff_assoc($updateData, $initialData);
                if (!empty($differences)) {
                    $findText;
                    $changes = '';
                    foreach ($differences as $key => $value) {
                        $newText = '';
                        $textoCadena = '<i class="fa fa-arrow-down"></i>';
                        $findText = strpos(lang($key), $textoCadena);
                        if ($findText !== false) {
                            $oldText = lang($key);
                            $newText = str_replace('<i class="fa fa-arrow-down"></i>', '', $oldText);
                        }
                        $changes .= ( (isset($newText) && $newText != '') ? $newText : lang($key) ) . ' '. lang('from') . ': ' . (($initialData[$key] == "" || is_null($initialData[$key])) ? "Vacío" : $initialData[$key]) . '  ' .lang('to') . ': '  . (($updateData[$key] == "" || is_null($updateData[$key])) ? "Vacío" : $updateData[$key] ) .', ';
                    }
                    if($changes_obligations == '') { $changes = trim($changes,  ", "); }
                    else if($changes_obligations != '') { $changes .= $changes_obligations; }
                    $txt_fields_changed = sprintf(lang('user_fields_changed_settings'), $this->session->first_name." ".$this->session->last_name, lang($this->m) . " : " . $changes );
                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 1,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => 'settings',
                        'table_name' => 'settings',
                        'record_id' => 1,
                        'description' => $txt_fields_changed,
                    ]);
                }
            // END PROCEDURE TO ENTER CHANGES IN PARAMETERS TO USER ACTIVITIES
            }
            return true;
        }
        return false;
    }

    public function addTaxRate($data)
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->insert('tax_rates', $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function updateTaxRate($id, $data = array())
    {
        $this->db->where('id', $id);
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->update('tax_rates', $data)) {
            return true;
        }
        return false;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id)
    {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addWarehouse($data)
    {
        if ($this->db->insert('warehouses', $data)) {
            return true;
        }
        return false;
    }

    public function updateWarehouse($id, $data = array())
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        if ($this->db->update('warehouses', $data)) {
            return true;
        }
        return false;
    }

    public function getAllWarehouses()
    {
        $q = $this->db->get_where('warehouses', ['status' => 1]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseByID($id)
    {
        $q = $this->db->get_where('warehouses', array('id' => $id, 'status'=>1), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteTaxRate($id)
    {

        $q = $this->db->get_where('purchase_items', ['tax_rate_id' => $id]);
        if ($q->num_rows() > 0) {
            return false;
        }

        $q = $this->db->get_where('sale_items', ['tax_rate_id' => $id]);
        if ($q->num_rows() > 0) {
            return false;
        }

        $q = $this->db->get_where('order_sale_items', ['tax_rate_id' => $id]);
        if ($q->num_rows() > 0) {
            return false;
        }

        $q = $this->db->get_where('quote_items', ['tax_rate_id' => $id]);
        if ($q->num_rows() > 0) {
            return false;
        }

        if ($this->db->delete('tax_rates', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteInvoiceType($id)
    {
        if ($this->db->delete('invoice_types', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function deleteWarehouse($id)
    {
        if ($this->db->update('warehouses', ['status' => 0], ['id' => $id])) {
            return true;
        }
        return FALSE;
    }

    public function activeWarehouse($id)
    {
        if ($this->db->update('warehouses', ['status' => 1], ['id' => $id])) {
            return true;
        }
        return FALSE;
    }

    public function addCustomerGroup($data)
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->insert('customer_groups', $data)) {
            return true;
        }
        return false;
    }

    public function updateCustomerGroup($id, $data = array())
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        $this->db->where('id', $id);
        if ($this->db->update('customer_groups', $data)) {

            $this->db->update('companies', ['customer_group_name' => $data['name']], ['customer_group_id' => $id]);

            return true;
        }
        return false;
    }

    public function getAllCustomerGroups()
    {
        $q = $this->db->get('customer_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCustomerGroupByID($id)
    {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteCustomerGroup($id)
    {
        if ($this->db->delete('customer_groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getCustomersByGroups($id){
        $q = $this->db->where('customer_group_id', $id)->get('companies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
            return FALSE;
        }
    }

    public function getGroups()
    {
        $this->db->where('id >', 4);
        $q = $this->db->get('groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getGroupByID($id)
    {
        $q = $this->db->get_where('groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGroupPermissions($id)
    {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function GroupPermissions($id)
    {
        $q = $this->db->get_where('permissions', array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function updatePermissions($id, $data = array())
    {
        if ($this->db->update('permissions', $data, array('group_id' => $id)) && $this->db->update('users', array('show_price' => $data['products-price'], 'show_cost' => $data['products-cost']), array('group_id' => $id))) {
            return true;
        }
        return false;
    }

    public function addGroup($data)
    {
        if ($this->db->insert("groups", $data)) {
            $gid = $this->db->insert_id();
            $this->db->insert('permissions', array('group_id' => $gid));
            return $gid;
        }
        return false;
    }

    public function updateGroup($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update("groups", $data)) {
            return true;
        }
        return false;
    }


    public function getAllCurrencies()
    {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByID($id)
    {
        $q = $this->db->get_where('currencies', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCurrency($data)
    {
        if ($this->db->insert("currencies", $data)) {
            return true;
        }
        return false;
    }

    public function updateCurrency($id, $data = array())
    {
        $this->db->where('id', $id);
        if ($this->db->update("currencies", $data)) {
            return true;
        }
        return false;
    }

    public function deleteCurrency($id)
    {
        if ($this->db->delete("currencies", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getParentCategories()
    {
        $this->db->where('parent_id', NULL)->or_where('parent_id', 0);
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByID($id)
    {
        $q = $this->db->get_where("categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getCategoryByCode($code)
    {
        $q = $this->db->get_where('categories', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addCategory($data)
    {
        $update_code_consecutive = false;
        if ($data['code_consecutive'] == 1) {
            $ccode = $this->site->getCodeConsecutive('category');
            $data['code'] = $ccode;
            $update_code_consecutive = true;
        }
        if ($this->db->insert("categories", $data)) {
            $category_id = $this->db->insert_id();
            if ($update_code_consecutive) {
                $this->site->updateCodeConsecutive('category', $this->sma->formatQuantity($data['code'])+1);
            }
            return $category_id;
        }
        return false;
    }

    public function addCategories($data)
    {
        if ($this->db->insert_batch('categories', $data)) {
            return true;
        }
        return false;
    }

    public function updateCategory($id, $data = array())
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        $update_code_consecutive = false;
        if (isset($data['code_consecutive']) && $data['code_consecutive'] == 1) {
            $ccode = $this->site->getCodeConsecutive('category');
            $data['code'] = $ccode;
            $update_code_consecutive = true;
        }
        if ($this->db->update("categories", $data, array('id' => $id))) {
            if ($update_code_consecutive) {
                $this->site->updateCodeConsecutive('category', $this->sma->formatQuantity($data['code'])+1);
            }
            return true;
        }
        return false;
    }

    public function deleteCategory($id)
    {
        if ($this->db->delete("categories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getPaypalSettings()
    {
        $q = $this->db->get('paypal');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updatePaypal($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('paypal', $data)) {
            return true;
        }
        return FALSE;
    }

    public function getSkrillSettings()
    {
        $q = $this->db->get('skrill');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateSkrill($data)
    {
        $this->db->where('id', '1');
        if ($this->db->update('skrill', $data)) {
            return true;
        }
        return FALSE;
    }

    public function checkGroupUsers($id)
    {
        $q = $this->db->get_where("users", array('group_id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteGroup($id)
    {
        if ($this->db->delete('groups', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addVariant($data)
    {
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->connect_new_year_database();
            if (!$this->check_same_id_insert($DB2, 'variants')) {
                $this->session->set_flashdata('error', 'La variante no se pudo crear, inconsistencia de datos entre años');
            }
        }
        if ($this->db->insert('variants', $data)) {
            if ($DB2) {
                $DB2->insert('variants', $data);
            }
            return $this->db->insert_id();
        }
        return false;
    }

    public function updateVariant($id, $data = array())
    {
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->settings_model->connect_new_year_database();
        }
        if ($this->db->update('variants', $data, ['id'=>$id])) {
            if ($DB2) {
                $DB2->update('variants', $data, ['id'=>$id]);
            }
            return true;
        }
        return false;
    }

    public function getAllVariants()
    {
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getVariant($data)
    {
        $this->db->where($data);
        $q = $this->db->get('variants');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getVariantByID($id)
    {
        $q = $this->db->get_where('variants', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteVariant($id)
    {
        if ($this->db->delete('variants', array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function getExpenseCategoryByID($id)
    {
        $q = $this->db->get_where("expense_categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getExpenseCategoryByCode($code)
    {
        $q = $this->db->get_where("expense_categories", array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addExpenseCategory($data)
    {
        if ($this->db->insert("expense_categories", $data)) {
            return true;
        }
        return false;
    }

    public function addExpenseCategories($data)
    {
        if ($this->db->insert_batch("expense_categories", $data)) {
            return true;
        }
        return false;
    }

    public function updateExpenseCategory($id, $data = array())
    {
        if ($this->db->update("expense_categories", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function hasExpenseCategoryRecord($id)
    {
        $this->db->where('category_id', $id);
        return $this->db->count_all_results('expenses');
    }

    public function deleteExpenseCategory($id)
    {
        if ($this->db->delete("expense_categories", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function addUnit($data)
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->insert("units", $data)) {
            return true;
        }
        return false;
    }

    public function updateUnit($id, $data = array())
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->update("units", $data, array('id' => $id))) {
            return true;
        }
        return false;
    }

    public function getUnitChildren($base_unit)
    {
        $this->db->where('base_unit', $base_unit);
        $q = $this->db->get("units");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function deleteUnit($id)
    {
        if ($this->db->delete("units", array('id' => $id))) {
            $this->db->delete("units", array('base_unit' => $id));
            return true;
        }
        return FALSE;
    }

    public function addPriceGroup($data)
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->insert('price_groups', $data)) {
            $price_group_id = $this->db->insert_id();
            if ($data['price_group_base'] == 1) {
                $this->db->update('price_groups', ['price_group_base' => 0, 'last_update' => date('Y-m-d H:i:s')], ['id !=' => $price_group_id]);
            }
            $this->site->updateProductsPriceGroupBase();
            return true;
        }
        return false;
    }

    public function updatePriceGroup($id, $data = array())
    {
        $this->db->where('id', $id);
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->update('price_groups', $data)) {
            $this->db->update('companies', ['price_group_name' => $data['name']], ['price_group_id' => $id]);
            if ($data['price_group_base'] == 1) {
                $this->db->update('price_groups', ['price_group_base' => 0, 'last_update' => date('Y-m-d H:i:s')], ['id !=' => $id]);
            }
            return true;
        }
        return false;
    }

    public function getAllPriceGroups()
    {
        $q = $this->db->get('price_groups');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCustomerByPriceGroups($id){
        $q = $this->db->where('price_group_id', $id)->get('companies');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        } else {
            return FALSE;
        }

    }

    public function getPriceGroupByID($id)
    {
        $q = $this->db->get_where('price_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }


    // Wappsi Modificación
    public function getUnitByID($id)
    {
        $q = $this->db->get_where('units', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    // Termina Wappsi Modificación

    public function deletePriceGroup($id)
    {
        if ($this->db->delete('price_groups', array('id' => $id)) && $this->db->delete('product_prices', array('price_group_id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function setProductPriceForPriceGroup($product_id, $group_id, $price)
    {

        $pr_gr = $this->site->getPriceGroupByID($group_id);
        if ($this->getGroupPrice($group_id, $product_id)) {
            if ($this->db->update('product_prices', array('price' => $price, 'last_update' => date('Y-m-d H:i:s')), array('price_group_id' => $group_id, 'product_id' => $product_id))) {
                if ($pr_gr->price_group_base == 1) {
                    $this->db->update('products', ['price'=>$price], ['id' => $product_id]);
                }
                $this->db->update('products', ['last_update' => date('Y-m-d H:i:s')], ['id' => $product_id]);
                return true;
            }
        } else {
            if ($this->db->insert('product_prices', array('price' => $price, 'price_group_id' => $group_id, 'product_id' => $product_id, 'last_update' => date('Y-m-d H:i:s')))) {
                if ($pr_gr->price_group_base == 1) {
                    $this->db->update('products', ['price'=>$price], ['id' => $product_id]);
                }
                $this->db->update('products', ['last_update' => date('Y-m-d H:i:s')], ['id' => $product_id]);
                return true;
            }
        }
        return FALSE;
    }

    public function updateProductPrice($product_id, $price){
        $this->db->update('products', array('price' => $price, 'last_update' => date('Y-m-d H:i:s')), array('id' => $product_id));
    }

	     // Wappsi modificación
    public function setProductPriceForUnit($product_id, $unit_id, $price, $cantidad)
    {
        $p = $this->db->get_where('unit_prices', array('id_product' => $product_id, 'unit_id' => $unit_id));
        $unit = $this->db->get_where('units', array('id' => $unit_id))->row();

        $product = $this->site->getProductByID($product_id);

        if ($this->Settings->ipoconsumo && $product->tax_method == 0) {
            $consumption_sale_tax = $product->consumption_sale_tax;
            if ($unit->operator && $unit->operation_value > 0 && $this->Settings->precios_por_unidad_presentacion == 2) {
                if ($unit->operator == "*") {
                    $consumption_sale_tax = $consumption_sale_tax * $unit->operation_value;
                } else if ($unit->operator == "/") {
                    $consumption_sale_tax = $consumption_sale_tax / $unit->operation_value;
                }
            }
            $price = $price - $consumption_sale_tax;
            if ($price <= 0) {
                return false;
            }
        }

        if ($this->getUnitPrice($unit_id, $product_id)) {
            if ($this->db->update('unit_prices', array('valor_unitario' => $price, 'cantidad' => $cantidad, 'last_update' => date('Y-m-d H:i:s')), array('unit_id' => $unit_id, 'id_product' => $product_id))) {
                return true;
            }
        } else {
            if ($product->unit == $unit_id) {
                if ($this->db->update('products', array('price' => $price, 'last_update' => date('Y-m-d H:i:s')), array('id' => $product_id))) {
                    $this->site->update_has_multiple_units($product_id);
                    return true;
                }
            } else {
                if ($this->db->insert('unit_prices', array('valor_unitario' => $price, 'cantidad' => $cantidad, 'unit_id' => $unit_id, 'id_product' => $product_id, 'last_update' => date('Y-m-d H:i:s')))) {
                    $this->site->update_has_multiple_units($product_id);
                    return true;
                }
            }

        }
        return FALSE;
    }

    public function getUnitPrice($unit_id, $product_id)
    {
        $q = $this->db->get_where('unit_prices', array('unit_id' => $unit_id, 'id_product' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    // Termina Wappsi modificación

    public function getGroupPrice($group_id, $product_id)
    {
        $q = $this->db->get_where('product_prices', array('price_group_id' => $group_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductGroupPriceByPID($product_id, $group_id)
    {
        $pg = "(SELECT {$this->db->dbprefix('product_prices')}.price as price, {$this->db->dbprefix('product_prices')}.product_id as product_id FROM {$this->db->dbprefix('product_prices')} WHERE {$this->db->dbprefix('product_prices')}.product_id = {$product_id} AND {$this->db->dbprefix('product_prices')}.price_group_id = {$group_id}) GP";

        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.name as name, GP.price", FALSE)
        // ->join('products', 'products.id=product_prices.product_id', 'left')
        ->join($pg, 'GP.product_id=products.id', 'left');
        $q = $this->db->get_where('products', array('products.id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateGroupPrices($data = array())
    {
        foreach ($data as $row) {
            if ($this->getGroupPrice($row['price_group_id'], $row['product_id'])) {
                $this->db->update('product_prices', array('price' => $row['price'], 'last_update' => date('Y-m-d H:i:s')), array('product_id' => $row['product_id'], 'price_group_id' => $row['price_group_id']));
            } else {
                $this->db->insert('product_prices', $row);
            }
        }
        return true;
    }
	    /* Wappsi code */

    public function updateUnitPrices($data = array())
    {
        //code,unit_id,cantidad,valor_unitario
        foreach ($data as $row) {

            if ($this->getUnitPrice($row['unit_id'],$row['id_product'])) {
                $this->db->update('unit_prices',

                    array(

                        'cantidad' => $row['cantidad'],
                        'valor_unitario' => $row['valor_unitario'],
                        'code' => $row['code']





                ), array('id_product' => $row['id_product'], 'unit_id' => $row['unit_id']));
            } else {
            $this->db->insert('unit_prices', $row);
            }

        }
      return false;
    }
    /* Termina Wappsi code */

    public function deleteProductGroupPrice($product_id, $group_id)
    {
        if ($this->db->delete('product_prices', array('price_group_id' => $group_id, 'product_id' => $product_id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_brands()
    {
        $this->db->select("*");
        $this->db->from("brands");
        $response = $this->db->get();

        return $response->result();
    }

    public function getBrandByName($name)
    {
        $q = $this->db->get_where('brands', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function addBrand($data)
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        $update_code_consecutive = false;
        if ($data['code_consecutive'] == 1) {
            $ccode = $this->site->getCodeConsecutive('brand');
            $data['code'] = $ccode;
            $update_code_consecutive = true;
        }
        if ($this->db->insert("brands", $data)) {
            $brand_id = $this->db->insert_id();
            if ($update_code_consecutive) {
                $this->site->updateCodeConsecutive('brand', $this->sma->formatQuantity($data['code'])+1);
            }
            return $brand_id;

        }
        return false;
    }

    public function addBrands($data)
    {
        if ($this->db->insert_batch('brands', $data)) {
            return true;
        }
        return false;
    }

    public function updateBrand($id, $data = array())
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        $update_code_consecutive = false;
        if (isset($data['code_consecutive']) && $data['code_consecutive'] == 1) {
            $ccode = $this->site->getCodeConsecutive('brand');
            $data['code'] = $ccode;
            $update_code_consecutive = true;
        }
        if ($this->db->update("brands", $data, array('id' => $id))) {
            if ($update_code_consecutive) {
                $this->site->updateCodeConsecutive('brand', $this->sma->formatQuantity($data['code'])+1);
            }
            return true;
        }
        return false;
    }

    public function brandHasProducts($brand_id)
    {
        $q = $this->db->get_where('products', array('brand' => $brand_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deleteBrand($id)
    {
        if ($this->db->delete("brands", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function insert_type_customer_obligations($data)
    {
        if ($this->db->insert_batch('types_customer_obligations', $data) > 0)
        {
            return TRUE;
        }

        return FALSE;
    }
    public function addPaymentMethod($data, $data_conta = array())
    {
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->insert('payment_methods', $data)) {
            if ($data_conta && count($data_conta) > 0) {
                $this->site->insertPaymentMethodParameter($data_conta);
            }
            return true;
        }
        return false;
    }

    public function updatePaymentMethod($id, $data = array(), $data_conta = array())
    {
        $this->db->where('id', $id);
        $data['last_update'] = date('Y-m-d H:i:s');
        if ($this->db->update('payment_methods', $data)) {
            if ($data_conta && count($data_conta) > 0) {
                $this->site->updatePaymentMethodParameter($data_conta);
            }
            return true;
        }
        return false;
    }

    public function getPaymentMethodById($id) {
        $q = $this->db->get_where('payment_methods', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRateTraslate(){
        $trt1 = $this->site->getPaymentMethodParameter('tax_rate_traslate');
        $trt2 = $this->site->getPaymentMethodParameter('trm_difference');
        $data[0] = $trt1;
        $data[1] = $trt2;
        if (!$trt1 && !$trt2) {
            return false;
        }
        return $data;
    }

    public function paymentMethodHasPayments($payment_method)
    {
        $q = $this->db->get_where('payments', array('paid_by' => $payment_method));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function deletePaymentMethod($id)
    {
        if ($this->db->delete("payment_methods", array('id' => $id))) {
            return true;
        }
        return FALSE;
    }

    public function currencyHasMovements($id)
    {

        $cr = $this->db->get_where('currencies', array('id' => $id), 1);

        if ($cr->num_rows() > 0) {
            $cr = $cr->row();
            $currency  = $cr->code;

            $q = $this->db->get_where('purchases', array('purchase_currency' => $currency));
            if ($q->num_rows() > 0) {
                return $q->row();
            }
            $q = $this->db->get_where('quotes', array('quote_currency' => $currency));
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }

        return FALSE;
    }

    public function get_compatibility_by_id($id) {
        $this->db->select("compatibilidades.id AS id, compatibilidades.nombre AS nombre, compatibilidades.marca_id, compatibilidades.marca_nombre, brands.name AS nombre_marca");
        $this->db->from("compatibilidades");
        $this->db->join("brands", "brands.id = compatibilidades.marca_id", "INNER");
        $this->db->where("compatibilidades.id", $id);
        $response = $this->db->get();
        if ($response->num_rows() > 0) {
            return $response->row();
        }
        return FALSE;
    }

    public function get_compatibility_by_name($name, $id = NULL) {
        $this->db->select("nombre");
        $this->db->from("compatibilidades");
        $this->db->like("nombre", $name);

        if (! empty($id)) { $this->db->where("id !=", $id); }

        $response = $this->db->get();

        if ($response->num_rows() > 0) {
            return $response->row();
        }
        return FALSE;
    }

    public function add_compatibility($data)
    {
        if ($this->db->insert("compatibilidades", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_compatibility($id)
    {
        if ($this->db->delete("compatibilidades", ['id' => $id])) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_compatibility($id, $data = array())
    {
        if ($this->db->update("compatibilidades", $data, array('id' => $id))) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_technology_providers(){
        $q = $this->db->get_where('technology_providers', array('status' => 1));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_technology_provider_configuration($fe_technology_provider = NULL, $fe_work_environment = NULL)
    {
        $fe_technology_provider = ($fe_technology_provider == NULL) ? $this->Settings->fe_technology_provider : $fe_technology_provider;
        $fe_work_environment = ($fe_work_environment == NULL) ? $this->Settings->fe_work_environment : $fe_work_environment;

        $this->db->select("*");
        $this->db->from("technology_provider_configuration");
        $this->db->where("technology_provider_id", $fe_technology_provider);
        $this->db->where("work_enviroment", $fe_work_environment);

        $response = $this->db->get();

        return $response->row();
    }

    public function get_payments_mean_codes()
    {
        $this->db->select('*');
        $this->db->from('payment_mean_code_fe');
        $response = $this->db->get();

        return $response->result();
    }

    public function update_product_price_group($pr_id, $prg_id, $price){

        if ($this->Settings->prioridad_precios_producto == 10) {
            $product = $this->db->select('
                                        product_prices.*,
                                        units.operator as unit_operator,
                                        units.operation_value as unit_operation_value,
                                        products.tax_method as tax_method,
                                        products.consumption_sale_tax as consumption_sale_tax
                                    ')
                          ->join('price_groups', 'price_groups.id = product_prices.price_group_id', 'inner')
                          ->join('unit_prices', 'unit_prices.id_product = product_prices.product_id', 'left')
                          ->join('units', 'units.id = unit_prices.unit_id AND units.price_group_id = price_groups.id ', 'inner')
                          ->join('products', 'products.id = product_prices.product_id', 'inner')
                          ->where('product_prices.product_id', $pr_id)
                          ->where('product_prices.price_group_id', $prg_id)
                          ->get('product_prices')->row();
            if ($product) {
                $consumption_sale_tax = $product->consumption_sale_tax;
                if ($this->Settings->precios_por_unidad_presentacion == 2) {
                    if ($product->unit_operator) {
                        if ($product->unit_operator == "*") {
                            $consumption_sale_tax = $product->consumption_sale_tax * $product->unit_operation_value;
                        } else if ($product->unit_operator == "/") {
                            $consumption_sale_tax = $product->consumption_sale_tax / $product->unit_operation_value;
                        }
                    }
                }
            } else {
                $product = $this->site->getProductByID($pr_id);
                $consumption_sale_tax = $product->consumption_sale_tax;
            }
        } else {
            $product = $this->site->getProductByID($pr_id);
            $consumption_sale_tax = $product->consumption_sale_tax;
        }

        if ($product->tax_method == 0 && $this->Settings->ipoconsumo) {
            $price = $price - $consumption_sale_tax;
            if ($price < 0 || $this->Settings->allow_zero_price_products == 0 && $price == 0) {
                return false;
            }
        }

        $p = $this->db->get_where('product_prices', array('product_id' => $pr_id, 'price_group_id' => $prg_id));

        $pr_gr = $this->getPriceGroupByID($prg_id);

        if ($this->Settings->prioridad_precios_producto == 10) {
            $units = $this->site->get_units_by_price_group($prg_id, $pr_id);
            if ($units) {
                foreach ($units as $unit) {
                    $this->db->update('unit_prices', ['valor_unitario' => $price, 'last_update' => date('Y-m-d H:i:s')], ['id_product' => $pr_id, 'unit_id' => $unit->id]);
                }
            }

        }

        if ($p->num_rows() > 0) {
            if ($this->db->update('product_prices', array('price' => $price, 'last_update' => date('Y-m-d H:i:s')), array('product_id' => $pr_id, 'price_group_id' => $prg_id))) {

                if ($pr_gr->price_group_base == 1) {
                    $this->db->update('products', array('price' => $price), array('id' => $pr_id));
                }

                $this->db->update('products', array('last_update' => date('Y-m-d H:i:s')), array('id' => $pr_id));

                return true;
            }
        } else {
            if ($this->db->insert('product_prices', array('product_id' => $pr_id, 'price_group_id' => $prg_id, 'price' => $price, 'last_update' => date('Y-m-d H:i:s')))) {

                if ($pr_gr->price_group_base == 1) {
                    $this->db->update('products', array('price' => $price), array('id' => $pr_id));
                }

                $this->db->update('products', array('last_update' => date('Y-m-d H:i:s')), array('id' => $pr_id));
                return true;
            }
        }

        return false;
    }

    public function update_product_unit_price($pr_id, $unit_id, $price){
        $p = $this->db->get_where('unit_prices', array('id_product' => $pr_id, 'unit_id' => $unit_id));
        $unit = $this->db->get_where('units', array('id' => $unit_id))->row();

        $product = $this->site->getProductByID($pr_id);

        if ($this->Settings->ipoconsumo && $product->tax_method == 0) {
            $consumption_sale_tax = $product->consumption_sale_tax;
            if ($unit->operator && $unit->operation_value > 0 && $this->Settings->precios_por_unidad_presentacion == 2) {
                if ($unit->operator == "*") {
                    $consumption_sale_tax = $consumption_sale_tax * $unit->operation_value;
                } else if ($unit->operator == "/") {
                    $consumption_sale_tax = $consumption_sale_tax / $unit->operation_value;
                }
            }
            $price = $price - $consumption_sale_tax;
            if ($price < 0 || $this->Settings->allow_zero_price_products == 0 && $price == 0) {
                return false;
            }
        }

        if ($product->unit == $unit_id) {
            $this->db->update('products', array('price' => $price), array('id' => $pr_id));
            if ($p->num_rows() > 0) {
                $this->db->update('unit_prices', array('valor_unitario' => $price), array('id_product' => $pr_id, 'unit_id' => $unit_id));
            }
            $prgr_base = $this->site->get_price_group_base();
            $this->db->update('product_prices', array('price' => $price, 'last_update' => date('Y-m-d H:i:s')), array('product_id' => $pr_id, 'price_group_id' => $prgr_base->id));
            $this->site->update_has_multiple_units($pr_id);
            return true;
        } else {
            $unit_data = $this->site->getUnitByID($unit_id);
            if ($p->num_rows() > 0) {
                if ($this->db->update('unit_prices', array('valor_unitario' => $price, 'cantidad' => $unit_data->operation_value), array('id_product' => $pr_id, 'unit_id' => $unit_id))) {
                    $this->site->update_has_multiple_units($pr_id);
                    if ($this->Settings->prioridad_precios_producto == 10 && $unit->price_group_id) {
                        $this->db->update('product_prices', ['price' => $price, 'last_update' => date('Y-m-d H:i:s')], ['price_group_id' => $unit->price_group_id, 'product_id' => $pr_id]);
                    }
                    return true;
                }
            } else {
                if ($this->db->insert('unit_prices', array('id_product' => $pr_id, 'unit_id' => $unit_id, 'valor_unitario' => $price, 'cantidad' => $unit_data->operation_value))) {
                    $this->site->update_has_multiple_units($pr_id);
                    return true;
                }
            }
        }

        return false;
    }

    public function existing_document_type_by_resolution_number($resolution_number)
    {
        $q = $this->db->get_where("documents_types", ["num_resolucion" => $resolution_number]);

        if ($q->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function existing_document_type_by_prefix($prefix)
    {
        $q = $this->db->get_where("documents_types", ["sales_prefix" => $prefix]);

        if ($q->num_rows() > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function add_document_type($data = array())
    {
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->connect_new_year_database();
            if (!$this->check_same_id_insert($DB2, 'documents_types')) {
                $this->session->set_flashdata('error', 'El tipo de documento no se pudo crear, inconsistencia de datos entre años');
            }
        }
        if ($this->db->insert("documents_types", $data)) {
            if ($DB2) {
                $DB2->insert("documents_types", $data);
            }
            $this->site->insert_document_type_contabilidad($data);
            return true;
        }
        return false;
    }

    public function add_document_types($data = [])
    {
        if ($this->db->insert_batch("documents_types", $data) > 0) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_document_type($id, $data = array(), $prev_sales_prefix = null, $inv_format = null)
    {
        if ($data['module_invoice_format_id'] != 'NULL') {
            $this->db->update('invoice_formats', $inv_format, ['id'=>$data['module_invoice_format_id']]);
            // $this->sma->print_arrays($inv_format, $data);
        }
        $DB2 = false;
        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step >= 1) {
            $DB2 = $this->settings_model->connect_new_year_database();
        }
        if ($this->db->update("documents_types", $data, array('id' => $id))) {
            if ($DB2) {
                $DB2->update("documents_types", $data, array('id' => $id));
            }
            $this->site->update_document_type_contabilidad($data, $prev_sales_prefix);
            return true;
        }
        return false;
    }

    public function get_tax_code_fe($indicator = FALSE)
    {
        $this->db->select("*");
        $this->db->from("tax_code_fe");
        $this->db->where("indicador", $indicator);
        $response = $this->db->get();

        return $response->result();
    }

    public function get_type_document_movements($id){
        $doc_type = $this->db->get_where('documents_types', ['id' => $id]);

        if ($doc_type->num_rows() > 0) {
            $doc_type = $doc_type->row();
            $consecutive = $doc_type->sales_prefix;

            $sales = $this->db->like('reference_no', $consecutive)->get('sales');
            if ($sales->num_rows() > 0) {
                return true;
            }

            $order_sales = $this->db->like('reference_no', $consecutive)->get('order_sales');
            if ($order_sales->num_rows() > 0) {
                return true;
            }

            // $purchases = $this->db->like('reference_no', $consecutive)->get('purchases');
            // if ($purchases->num_rows() > 0) {
            //     return true;
            // }

            // $quotes = $this->db->like('reference_no', $consecutive)->get('quotes');
            // if ($quotes->num_rows() > 0) {
            //     return true;
            // }

            // $adjustments = $this->db->like('reference_no', $consecutive)->get('adjustments');
            // if ($adjustments->num_rows() > 0) {
            //     return true;
            // }

            // $transfers = $this->db->like('reference_no', $consecutive)->get('transfers');
            // if ($transfers->num_rows() > 0) {
            //     return true;
            // }

        }

        return false;

    }

    public function delete_document_type($id){
        if ($this->db->delete('documents_types', ['id' => $id])) {
            return true;
        }
        return false;
    }

    public function create_new_year_database(){
        $version = explode('.', $this->Settings->version);
        $valid = false;
        if (count($version) >= 2) {
            if ($version[0] > '2020') {
                $valid = true;
            } else if ($version[1] > '4') {
                $valid = true;
            } else if (isset($version[2]) && $version[2] > '3') {
                $valid = true;
            }
        }
        if (!$valid) {
            $response = [
                'error' => true,
                'msg' => 'La versión de la plataforma debe ser superior o igual a 2020.4.3 para poder realizar el cierre de año.',
            ];
            return $response;
        } else {
            $response = [
                'error' => false,
                'msg' => lang('successfull'),
            ];
        }
        $añoactual = "20".(substr($this->db->database, -2, 2));
        $actualanosufijo = substr($añoactual, 2, 4);
        $nuevoanosufijo = substr(($añoactual+1), 2, 4);
        $prefijo = $this->db->dbprefix;
        $dbsufijo = "_".$nuevoanosufijo;
        $nuevoano = $añoactual+1;
        if (strpos($this->db->database, "_".$actualanosufijo)) {
            $dbname = $this->db->database;
            $bdarr = explode("_", $dbname);
            $db_name_prefix = $bdarr[0];
            $dbname = str_replace("_".$actualanosufijo, "", $dbname);
            $dbname = $dbname.$dbsufijo;
        } else {
            $dbname = $this->db->database;
        }
        $new_config['hostname'] = $this->db->hostname;
        $new_config['username'] = $this->db->username;
        $new_config['password'] = $this->db->password;
        $new_config['database'] = $dbname;
        $new_config['dbdriver'] = $this->db->dbdriver;
        $new_config['dbprefix'] = strtolower($prefijo);
        $new_config['db_debug'] = $this->db->db_debug;
        $new_config['cache_on'] = $this->db->cache_on;
        $new_config['cachedir'] = $this->db->cachedir;
        $new_config['port']     = $this->db->port;
        $new_config['char_set'] = $this->db->char_set;
        $new_config['dbcollat'] = $this->db->dbcollat;
        $new_config['pconnect'] = $this->db->pconnect;
        $dbexists = $this->db->query('SHOW DATABASES LIKE "%'.$new_config['database'].'%";');
        //ACCIONES 1. Crear DB y tablas, 2. Crear sólo tablas, 3. Actualizar saldos
        if ($dbexists->num_rows() > 0) {
            $this->db->query('USE `'.$new_config['database'].'`');
            $tablesexists = $this->db->query('SHOW TABLES LIKE "'.($new_config['dbprefix']."settings").'"');
            if ($tablesexists->num_rows() > 0) {
                $accion = 3;
            } else {
                $accion = 2;
            }
            $this->db->query('USE `'.$this->db->database.'`');
        } else {
            $accion = 1;
        }
        // exit(var_dump($accion));
        if ($accion == 1) {
            $this->db->query("CREATE DATABASE IF NOT EXISTS `".$new_config['database']."`;");
        }
        if ($accion <= 2) {
            if ($this->site->check_database($new_config)) {
                $DB2 = $this->load->database($new_config, TRUE);
                // $schema = file_get_contents(APPPATH.'config/new_schema.sql');
                // /* Add prefix to the table names in the schema */
                // $final_schema = str_replace('%_PREFIX_%', $new_config['dbprefix'], $schema);
                // /* Add decimal places */
                // $sqls = explode(';', $final_schema);
                // array_pop($sqls);
                // foreach($sqls as $statement){
                //     $statment = $statement . ";";
                //     $DB2->query($statment);
                // }
                $tables = $this->db->query("SELECT TABLE_NAME
                                      FROM information_schema.TABLES
                                     WHERE TABLE_SCHEMA = SCHEMA()
                                       AND TABLE_NAME LIKE '%".$this->db->dbprefix."%'
                                       AND TABLE_NAME NOT LIKE '%_con2%'");
                $DB2->query("SET FOREIGN_KEY_CHECKS = 0;");
                $DB2->query("SET sql_mode = '';");
                if ($tables->num_rows() > 0) {
                    foreach ($tables->result() as $tbl_key => $tbl_name) {
                        $create_statement = $this->db->query("SHOW CREATE TABLE `".$tbl_name->TABLE_NAME."`");
                        if ($create_statement->num_rows() > 0) {
                            $create_statement = $create_statement->row();
                            if (isset($create_statement->{"Create Table"})) {
                                $DB2->query($create_statement->{"Create Table"});
                            }
                        }
                    }
                }
                $db_c['hostname'] = $new_config['hostname'];
                $db_c['username'] = $new_config['username'];
                $db_c['password'] = $new_config['password'];
                $db_c['database'] = $new_config['database'];
                $db_c['dbprefix'] = $new_config['dbprefix'];
                $this->db->truncate('new_year_database_connection');
                $this->db->insert('new_year_database_connection', $db_c);
            } else {
                $response = [
                                'error' => true,
                                'msg' => lang('error_loading_new_database'),
                            ];
                return $response;
            }
        }

        if ($accion <= 3) {
            error_reporting(0);
            if (!isset($DB2)) {
                $DB2 = $this->connect_new_year_database();
                $this->truncate_new_year_tables();
            }

            $tables_passing_data = [
                                    'addresses',
                                    'biller_data',
                                    'biller_documents_types',
                                    'biller_seller',
                                    'branch_areas',
                                    'brands',
                                    'categories',
                                    'combo_items',
                                    'companies',
                                    'currencies',
                                    'customer_groups',
                                    'date_format',
                                    'documents_types',
                                    'documentypes',
                                    'expense_categories',
                                    'groups',
                                    'invoice_formats',
                                    'invoice_notes',
                                    'order_ref',
                                    'pages',
                                    'permissions',
                                    'pos_settings',
                                    'price_groups',
                                    'printers',
                                    'product_photos',
                                    'product_preferences',
                                    'product_prices',
                                    'products',
                                    'product_variants',
                                    'settings',
                                    'shop_settings',
                                    'subzones',
                                    'tables',
                                    'tax_class',
                                    'tax_code_fe',
                                    'tax_rates',
                                    'technology_providers',
                                    'types_customer_obligations',
                                    'types_obligations-responsabilities',
                                    'types_person',
                                    'types_vat_regime',
                                    'unit_prices',
                                    'units',
                                    'users',
                                    'warehouses',
                                    'warehouses_products',
                                    'warehouses_products_variants',
                                    'zones',
                                    'countries',
                                    'states',
                                    'cities',
                                    'payment_methods',
                                    'debit_credit_notes_concepts',
                                    'biller_categories_concessions',
                                    'collection_discounts',
                                    'console_service_data',
                                    'cost_per_month',
                                    'dashboards',
                                    'dian_note_concept',
                                    'tax_percentages_fe',
                                    'technology_provider_configuration',
                                    'variants',
                                    'preferences',
                                    'preferences_categories',
                                    'withholdings',
                                    'payroll_areas',
                                    'payroll_arl_risk_classes',
                                    'payroll_concept_type',
                                    'payroll_concepts',
                                    'payroll_contracts',
                                    'payroll_period',
                                    'payroll_professional_position',
                                    'payroll_settings',
                                    'payroll_ss_operators',
                                    'payroll_types_contracts',
                                    'payroll_types_employees',
                                    'payroll_workday',
                                    'payment_mean_code_fe',
                                    'payroll_futures_items',
                                    'payroll_contract_concepts',
                                    'tax_secondary',
                                    'tax_secondary_percentage',
                                ];

            foreach ($tables_passing_data as $key => $table_pass) {
                $table_columns = $this->get_table_columns($table_pass, $DB2);
                if(!$DB2->query("INSERT INTO `".$DB2->dbprefix.$table_pass."` (".$table_columns.") SELECT ".$table_columns." FROM `".$this->db->database."`.`".$this->db->dbprefix.$table_pass."`")){
                    $response = [
                                'error' => true,
                                'msg' => "Error en paso de datos para la tabla sma_".$table_pass." ",
                            ];
                    $this->truncate_new_year_tables();
                    return $response;
                }
            }
            // $actualanosufijo $nuevoanosufijo
            $new_dts = $DB2->like('sales_prefix', $actualanosufijo)->get('documents_types');
            if ($new_dts->num_rows() > 0) {
                foreach (($new_dts->result()) as $dt) {
                    $DB2->update('documents_types',
                        [
                        'sales_prefix' => str_replace($actualanosufijo, $nuevoanosufijo, $dt->sales_prefix),
                        'nombre' => str_replace($actualanosufijo, $nuevoanosufijo, $dt->nombre),
                        'sales_consecutive'=>1,
                        'last_update'=>date('Y-m-d H:i:s')
                    ], ['id' => $dt->id]);
                }
            }
            error_reporting(E_ERROR);
            $DB2->update('products', ['quantity' => 0], ['id >=' => 0]);
            $DB2->update('warehouses_products', ['quantity' => 0], ['id >=' => 0]);
            $DB2->update('warehouses_products_variants', ['quantity' => 0], ['id >=' => 0]);
            $DB2->update('product_variants', ['quantity' => 0], ['id >=' => 0]);
            $gift_cards = $this->db->get_where('gift_cards', ['balance >' => 0]);
            if ($gift_cards->num_rows() > 0) {
                foreach (($gift_cards->result()) as $gc) {
                    unset($gc->id);
                    $DB2->insert('gift_cards', $gc);
                }
            }
            $deposits = $this->db->get_where('deposits', ['balance >' => 0]);
            if ($deposits->num_rows() > 0) {
                foreach (($deposits->result()) as $deposit) {
                    unset($deposit->id);
                    $DB2->insert('deposits', $deposit);
                }
            }
            $quotes = $this->db->get_where('quotes', ['status' => 'pending']);
            if ($quotes->num_rows() > 0) {
                foreach (($quotes->result()) as $q) {
                    $quote_items = $this->db->get_where('quote_items', ['quote_id' => $q->id]);
                    if ($quote_items->num_rows() > 0) {
                        unset($q->id);
                        $DB2->insert('quotes', $q);
                        $quote_id = $DB2->insert_id();
                        foreach (($quote_items->result()) as $qi) {
                            unset($qi->id);
                            $qi->quote_id = $quote_id;
                            $DB2->insert('quote_items', $qi);
                        }
                    }
                }
            }
            $suspended_bills = $this->db->get('suspended_bills');
            if ($suspended_bills->num_rows() > 0) {
                foreach (($suspended_bills->result()) as $sb) {
                    $suspended_items = $this->db->get_where('suspended_items', ['suspend_id' => $sb->id]);
                    if ($suspended_items->num_rows() > 0) {
                        unset($sb->id);
                        $DB2->insert('suspended_bills', $sb);
                        $sb_id = $DB2->insert_id();
                        foreach (($suspended_items->result()) as $sbi) {
                            unset($sbi->id);
                            $sbi->suspend_id = $sb_id;
                            $DB2->insert('suspended_items', $sbi);
                        }
                    }
                }
            }
            $tables_increase_id = [
                                    'sales',
                                    'purchases',
                                    'sale_items',
                                    'purchase_items',
                                    'deposits',
                                    'companies',
                                    'addresses',
                                    'products',
                                    'product_variants',
                                ];
            foreach ($tables_increase_id as $key => $table_incr) {
                $num_table = $this->db->select('MAX(id) as id')
                                      ->get($table_incr);
                if ($num_table->num_rows() > 0) {
                    $num_table = $num_table->row();
                    $max_table_id = (Double) $num_table->id + 1000;
                    $DB2->query("ALTER TABLE {$DB2->dbprefix($table_incr)} AUTO_INCREMENT = ".$max_table_id);
                }
            }
            $DB2->update('settings', ['closed_from_another_year' => 1], ['setting_id' => 1]);
            $DB2->close();
            // $console_bd = isset($db_name_prefix) ? $db_name_prefix."_pos_configuracion" : NULL;
            // if ($console_bd) {
            //     $console_config['hostname'] = $this->db->hostname;
            //     $console_config['username'] = $this->db->username;
            //     $console_config['password'] = $this->db->password;
            //     $console_config['database'] = $console_bd;
            //     $console_config['dbdriver'] = $this->db->dbdriver;
            //     $console_config['dbprefix'] = strtolower($prefijo);
            //     $console_config['db_debug'] = $this->db->db_debug;
            //     $console_config['cache_on'] = $this->db->cache_on;
            //     $console_config['cachedir'] = $this->db->cachedir;
            //     $console_config['port']     = $this->db->port;
            //     $console_config['char_set'] = $this->db->char_set;
            //     $console_config['dbcollat'] = $this->db->dbcollat;
            //     $console_config['pconnect'] = $this->db->pconnect;
            //     if ($this->site->check_database($new_config)) {
            //         $DB3 = $this->load->database($console_config, TRUE);
            //         $console_name = $this->Settings->site_name;
            //         $console_label = str_replace(" ", "", $console_name.$dbsufijo);
            //         $console_path = $this->site->get_project_root_folder_name();
            //         $console_query = "INSERT INTO `of_accounts`
            //                                 (
            //                                 `label`, `name`, `year`, `db_datasource`, `db_database`, `db_host`, `db_port`, `db_login`, `db_password`, `db_prefix`, `db_suffix`, `db_persistent`, `db_schema`, `db_unixsocket`, `db_settings`, `path`, `ssl_key`, `ssl_cert`, `ssl_ca`, `account_locked`, `closed_year_stock`, `closed_year_receivable`, `closed_year_purshase`, `closed_year_calculate_payments`, `closed_year_calculate_purchases`)
            //                                 VALUES
            //                                 ('".$console_label."', '".$console_name."', '".$nuevoano."', 'mysqli', '".$new_config['database']."', '".$new_config['hostname']."', 3306, '".$new_config['username']."', '".$new_config['password']."', '".$new_config['dbprefix']."', NULL, ".($new_config['pconnect'] ? 1 : 0).", NULL, NULL, NULL, '".$console_path."', NULL, NULL, NULL, 0, 0, 0, 0, 0, 0)";
            //         $DB3->query($console_query);
            //         $DB3->close();
            //     }
            // }
            $this->db->update('settings', ['close_year_step' => 1], ['setting_id' => 1]);
        }
        return $response;
    }

    public function transfer_inventory_to_new_year(){


        $negative_quantitys = $this->db->not_like('name', 'Saldo Inicial')
                                        ->where('quantity <', 0)
                                        ->get('products');
        if ($negative_quantitys->num_rows() > 0) {
            $response = [
                    'error' => true,
                    'msg' => sprintf(lang('products_with_negative_quantitys'), $negative_quantitys->num_rows()),
                    'products' => $negative_quantitys->result()
                ];
            return $response;
        }
        $w_negative_quantitys = $this->db->select('products.*')
                                        ->join('products', 'products.id = warehouses_products.product_id')
                                        ->not_like($this->db->dbprefix('products').'.name', 'Saldo Inicial')
                                        ->where('warehouses_products.quantity <', 0)
                                        ->get('warehouses_products');
        if ($w_negative_quantitys->num_rows() > 0) {
            $response = [
                    'error' => true,
                    'msg' => sprintf(lang('products_with_negative_quantitys'), $w_negative_quantitys->num_rows()),
                    'products' => $w_negative_quantitys->result()
                ];
            return $response;
        }

        if ($DB2 = $this->connect_new_year_database()) {
            $adjustment_exists = $DB2->like('reference_no', 'SIAJ-')->get('adjustments');
            if ($adjustment_exists->num_rows() > 0) {
                foreach (($adjustment_exists->result()) as $adjustment) {
                    $DB2->delete('adjustment_items', ['adjustment_id' => $adjustment->id]);
                    $DB2->delete('adjustments', ['id' => $adjustment->id]);
                }
            }
            $date = date('Y-m-d H:i:s');
            $reference = 'SIAJ-';
            $cnt_reference = 1;
            /* PROCESO SIN VARIANTES */
            $warehouses = [];
            $products = $this->db->select('
                                            warehouses_products.warehouse_id,
                                            warehouses_products.quantity,
                                            warehouses_products.product_id,
                                            warehouses_products.avg_cost,
                                            products.cost
                                          ')
                                 ->join('products', 'warehouses_products.product_id = products.id', 'inner')
                                 ->where('warehouses_products.quantity > 0')
                                 ->order_by('warehouses_products.warehouse_id ASC')
                                 ->get('warehouses_products')
                                 ;
            $items = [];
            $insert_items = [];
            $products_quantitys = [];
            $products_data = [];
            if ($products->num_rows() > 0) {
                $products_data = $products->result();
                foreach ($products_data as $product) {
                    if ($this->site->getProductVariants($product->product_id)) {
                        continue;
                    }
                    if (isset($products_quantitys[$product->product_id])) {
                        $products_quantitys[$product->product_id] += $product->quantity;
                    } else {
                        $products_quantitys[$product->product_id] = $product->quantity;
                    }
                    $DB2->update('warehouses_products', ['quantity' => $product->quantity], ['product_id' => $product->product_id, 'warehouse_id' => $product->warehouse_id]);
                    if (!isset($warehouses[$product->warehouse_id])) {
                        if (count($warehouses) > 0) {
                            $DB2->insert('adjustments', $data);
                            $adj_id = $DB2->insert_id();
                            foreach ($items as $row => $item) {
                                $items[$row]['adjustment_id'] = $adj_id;
                            }
                            $insert_items = array_merge($insert_items, $items);
                            $data = [];
                            $items = [];
                        }
                        $warehouses[$product->warehouse_id] = 1;
                        $data = array(
                                    'date' => $date,
                                    'reference_no' => $reference.$cnt_reference,
                                    'warehouse_id' => $product->warehouse_id,
                                    'note' => '',
                                    'created_by' => $this->session->userdata('user_id'),
                                    'count_id' => NULL,
                                    'companies_id' => NULL,
                                    'biller_id' => NULL,
                                );
                        $cnt_reference++;
                        $DB2->insert('adjustments', $data);
                        $adj_id = $DB2->insert_id();
                    }
                    $items[] = array(
                                'product_id' => $product->product_id,
                                'type' => 'addition',
                                'quantity' => $product->quantity,
                                'warehouse_id' => $product->warehouse_id,
                                'option_id' => NULL,
                                'serial_no' => NULL,
                                'avg_cost' => $product->avg_cost,
                                'adjustment_cost' => $product->cost,
                            );
                }
                if (count($items) > 0) {
                    foreach ($items as $row => $item) {
                        $items[$row]['adjustment_id'] = $adj_id;
                    }
                    $DB2->insert_batch('adjustment_items', $items);
                }
                $update_data = [];
                $cnt = 0;
                foreach ($products_quantitys as $product_id => $product_quantity) {
                    $update_data[$cnt]['id'] = $product_id;
                    $update_data[$cnt]['quantity'] = $product_quantity;
                    $cnt++;
                }
                $DB2->update_batch('products', $update_data, 'id');
            }

            $response = [
                            'error' => false,
                            'msg' => lang('successfull'),
                            'cnt_reference' => $cnt_reference,
                        ];
            return $response;
        }

        $response = [
                        'error' => true,
                        'msg' => lang('error_loading_new_database'),
                    ];
        return $response;

    }

    public function transfer_inventory_with_variants_to_new_year($cnt_reference){
        $response = [
                'error' => false,
                'msg' => lang('successfull'),
            ];

        // $negative_quantitys = $this->db->get_where('product_variants', ['quantity <' => 0]);

        // if ($negative_quantitys->num_rows() > 0) {
        //     $response = [
        //             'error' => true,
        //             'msg' => sprintf(lang('products_with_negative_quantitys'), $negative_quantitys->num_rows()),
        //         ];
        //     return $response;
        // }

        $w_negative_quantitys = $this->db->get_where('warehouses_products_variants', ['quantity <' => 0]);

        if ($w_negative_quantitys->num_rows() > 0) {
            $response = [
                    'error' => true,
                    'msg' => sprintf(lang('products_with_negative_quantitys'), $w_negative_quantitys->num_rows()),
                ];
            return $response;
        }

        if ($DB2 = $this->connect_new_year_database()) {

            $date = date('Y-m-d H:i:s');
            $reference = 'SIAJ-';

            /* PROCESO CON VARIANTES */
            $warehouses = [];

            $products = $this->db->select('
                                            warehouses_products_variants.warehouse_id,
                                            warehouses_products_variants.quantity,
                                            warehouses_products_variants.product_id,
                                            warehouses_products_variants.option_id,
                                            products.cost
                                          ')
                                 ->join('products', 'warehouses_products_variants.product_id = products.id', 'inner')
                                 ->where('warehouses_products_variants.quantity > 0')
                                 ->order_by('warehouses_products_variants.warehouse_id ASC')
                                 ->get('warehouses_products_variants')
                                 ;
            $items = [];
            $insert_items = [];
            $products_quantitys = [];
            $products_variants_quantitys = [];
            $warehouses_quantitys = [];
            $products_data = [];

            if ($products->num_rows() > 0) {
                foreach (($products->result()) as $pdata) {
                    $products_data[] = $pdata;
                }

                foreach ($products_data as $product) {

                    if (isset($products_quantitys[$product->product_id])) {
                        $products_quantitys[$product->product_id] += $product->quantity;
                    } else {
                        $products_quantitys[$product->product_id] = $product->quantity;
                    }

                    if (isset($products_variants_quantitys[$product->product_id][$product->option_id])) {
                        $products_variants_quantitys[$product->product_id][$product->option_id] += $product->quantity;
                    } else {
                        $products_variants_quantitys[$product->product_id][$product->option_id] = $product->quantity;
                    }

                    if (isset($warehouses_quantitys[$product->warehouse_id][$product->product_id])) {
                        $warehouses_quantitys[$product->warehouse_id][$product->product_id] += $product->quantity;
                    } else {
                        $warehouses_quantitys[$product->warehouse_id][$product->product_id] = $product->quantity;
                    }


                    $DB2->update('warehouses_products_variants', ['quantity' => $product->quantity], ['product_id' => $product->product_id, 'warehouse_id' => $product->warehouse_id, 'option_id' => $product->option_id]);

                    if (!isset($warehouses[$product->warehouse_id])) {

                        if (count($warehouses) > 0) {

                            $DB2->insert('adjustments', $data);
                            $adj_id = $DB2->insert_id();
                            foreach ($items as $row => $item) {
                                $items[$row]['adjustment_id'] = $adj_id;
                            }
                            // $DB2->insert_batch('adjustment_items', $items);
                            $insert_items = array_merge($insert_items, $items);

                            $data = [];
                            $items = [];

                        }

                        $warehouses[$product->warehouse_id] = 1;

                        $data = array(
                                    'date' => $date,
                                    'reference_no' => $reference.$cnt_reference,
                                    'warehouse_id' => $product->warehouse_id,
                                    'note' => '',
                                    'created_by' => $this->session->userdata('user_id'),
                                    'count_id' => NULL,
                                    'companies_id' => NULL,
                                    'biller_id' => NULL,
                                );
                        $cnt_reference++;
                    }

                    $items[] = array(
                                'product_id' => $product->product_id,
                                'type' => 'addition',
                                'quantity' => $product->quantity,
                                'warehouse_id' => $product->warehouse_id,
                                'option_id' => $product->option_id,
                                'serial_no' => NULL,
                                'avg_cost' => NULL,
                                'adjustment_cost' => $product->cost,
                            );


                    if ($product === end($products_data) && count($items) > 0) {

                        $DB2->insert('adjustments', $data);
                        $adj_id = $DB2->insert_id();
                        foreach ($items as $row => $item) {
                            $items[$row]['adjustment_id'] = $adj_id;
                        }
                        // $insert_items = array_merge($insert_items, $items);
                        $DB2->insert_batch('adjustment_items', $items);
                    }

                }

                if (count($insert_items) > 0) {
                    $DB2->insert_batch('adjustment_items', $insert_items);
                }

                $update_data = [];
                $cnt = 0;
                foreach ($products_quantitys as $product_id => $product_quantity) {
                    $update_data[$cnt]['id'] = $product_id;
                    $update_data[$cnt]['quantity'] = $product_quantity;
                    $cnt++;
                }
                $DB2->update_batch('products', $update_data, 'id');

                $update_warehouses_quantitys = [];

                foreach ($products_variants_quantitys as $product_id => $arr_pr) {
                    foreach ($arr_pr as $option_id => $amount) {
                        $DB2->update('product_variants', ['quantity' => $amount], ['product_id' => $product_id, 'id' => $option_id]);
                    }
                }

                foreach ($warehouses_quantitys as $warehouse_id => $arr_pr) {
                    foreach ($arr_pr as $product_id => $amount) {
                        $DB2->update('warehouses_products', ['quantity' => $amount], ['warehouse_id' => $warehouse_id, 'product_id' => $product_id]);
                    }
                }

                $this->db->update('settings', ['close_year_step' => 2], ['setting_id' => 1]);
                /* PROCESO CON VARIANTES */
            } else {
                $this->db->update('settings', ['close_year_step' => 2], ['setting_id' => 1]);
            }

            $response = [
                            'error' => false,
                            'msg' => lang('successfull'),
                        ];
            return $response;
        }

        $response = [
                        'error' => true,
                        'msg' => lang('error_loading_new_database'),
                    ];
        return $response;

    }

    public function transfer_cxc(){
        $sales = $this->db->where('(grand_total - paid) > 0 AND sale_status != "returned"')
                          ->get('sales');
        if ($DB2 = $this->connect_new_year_database()) {
            if ($sales->num_rows() > 0) {
                $DB2->delete('products', ['code' => 999999999]);
                $DB2->query("INSERT INTO {$DB2->dbprefix('products')}
                            (`code`, `name`, `unit`, `cost`, `avg_cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `product_details`, `tax_method`, `type`, `promotion`, `promo_price`, `start_date`, `end_date`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`)
                            VALUES
                            ( 999999999, 'Saldo Inicial CXC', 0, 0, 0, 0, 0, NULL, 1, /*CATEGORIA*/
                            NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 'service', 0, NULL, NULL, NULL, 1, 1, 1, 'SICXC', NULL, 0)");
                $product_service = $DB2->insert_id();
                $DB2->query("DELETE {$DB2->dbprefix('sale_items')} FROM {$DB2->dbprefix('sale_items')}
                                INNER JOIN {$DB2->dbprefix('sales')} ON {$DB2->dbprefix('sales')}.id = {$DB2->dbprefix('sale_items')}.sale_id
                            WHERE {$DB2->dbprefix('sales')}.reference_no LIKE '%SI%'
                            ");
                $DB2->like('reference_no', 'SI')
                    ->delete('sales');
                $table_sales = "`".$this->db->database."`.`".$this->db->dbprefix."sales`";
                $table_sale_items = "`".$this->db->database."`.`".$this->db->dbprefix."sale_items`";
                $sales_select = $table_sales.".`id`,
                                ".$table_sales.".`date`,
                                CONCAT('SI', ".$table_sales.".`reference_no`) as `reference_no`,
                                ".$table_sales.".`customer_id`,
                                ".$table_sales.".`customer`,
                                ".$table_sales.".`biller_id`,
                                ".$table_sales.".`biller`,
                                ".$table_sales.".`warehouse_id`,
                                ".$table_sales.".`note`,
                                ".$table_sales.".`staff_note`,
                                (".$table_sales.".`grand_total` - ".$table_sales.".`paid`) as `total`,
                                0 as `product_discount`,
                                0 as `order_discount_id`,
                                0 as `total_discount`,
                                0 as `order_discount`,
                                0 as `product_tax`,
                                0 as `order_tax_id`,
                                0 as `order_tax`,
                                0 as `total_tax`,
                                0 as `shipping`,
                                (".$table_sales.".`grand_total` - ".$table_sales.".`paid`) as `grand_total`,
                                ".$table_sales.".`sale_status`,
                                'due' as `payment_status`,
                                ".$table_sales.".`payment_term`,
                                ".$table_sales.".`due_date`,
                                ".$table_sales.".`created_by`,
                                ".$table_sales.".`updated_by`,
                                ".$table_sales.".`updated_at`,
                                1 as `total_items`,
                                ".$table_sales.".`pos`,
                                0 as `paid`,
                                NULL as `return_id`,
                                0 as `surcharge`,
                                ".$table_sales.".`attachment`,
                                NULL as `return_sale_ref`,
                                NULL as `sale_id`,
                                0 as `return_sale_total`,
                                0 as `rounding`,
                                ".$table_sales.".`suspend_note`,
                                ".$table_sales.".`api`,
                                ".$table_sales.".`shop`,
                                ".$table_sales.".`address_id`,
                                ".$table_sales.".`seller_id`,
                                ".$table_sales.".`reserve_id`,
                                ".$table_sales.".`hash`,
                                ".$table_sales.".`manual_payment`,
                                ".$table_sales.".`cgst`,
                                ".$table_sales.".`sgst`,
                                ".$table_sales.".`igst`,
                                ".$table_sales.".`payment_method`,
                                ".$table_sales.".`pay_partner`,
                                ".$table_sales.".`rete_fuente_percentage`,
                                ".$table_sales.".`rete_fuente_total`,
                                ".$table_sales.".`rete_fuente_account`,
                                ".$table_sales.".`rete_fuente_base`,
                                ".$table_sales.".`rete_iva_percentage`,
                                ".$table_sales.".`rete_iva_total`,
                                ".$table_sales.".`rete_iva_account`,
                                ".$table_sales.".`rete_iva_base`,
                                ".$table_sales.".`rete_ica_percentage`,
                                ".$table_sales.".`rete_ica_total`,
                                ".$table_sales.".`rete_ica_account`,
                                ".$table_sales.".`rete_ica_base`,
                                ".$table_sales.".`rete_other_percentage`,
                                ".$table_sales.".`rete_other_total`,
                                ".$table_sales.".`rete_other_account`,
                                ".$table_sales.".`rete_other_base`,
                                ".$table_sales.".`resolucion`,
                                ".$table_sales.".`fe_correo_enviado`,
                                ".$table_sales.".`fe_aceptado`,
                                ".$table_sales.".`fe_recibido`,
                                ".$table_sales.".`cufe`,
                                ".$table_sales.".`codigo_qr`,
                                ".$table_sales.".`fe_id_transaccion`,
                                ".$table_sales.".`fe_mensaje`,
                                ".$table_sales.".`fe_mensaje_soporte_tecnico`,
                                ".$table_sales.".`fe_xml`,
                                ".$table_sales.".`fe_debit_credit_note_concept_dian_code`,
                                ".$table_sales.".`sale_currency`,
                                ".$table_sales.".`sale_currency_trm`,
                                ".$table_sales.".`cost_center_id`,
                                ".$table_sales.".`document_type_id`,
                                ".$table_sales.".`payment_method_fe`,
                                ".$table_sales.".`payment_mean_fe`,
                                0 as `tip_amount`,
                                ".$table_sales.".`shipping_in_grand_total`,
                                ".$table_sales.".`sale_origin`,
                                ".$table_sales.".`sale_origin_reference_no`,
                                ".$table_sales.".`rete_fuente_id`,
                                ".$table_sales.".`rete_iva_id`,
                                ".$table_sales.".`rete_ica_id`,
                                ".$table_sales.".`rete_other_id`,
                                ".$table_sales.".`sale_comm_perc`,
                                ".$table_sales.".`collection_comm_perc`,
                                ".$table_sales.".`sale_comm_amount`,
                                ".$table_sales.".`sale_comm_payment_status`,
                                ".$table_sales.".`debit_note_id`,
                                ".$table_sales.".`reference_debit_note`,
                                ".$table_sales.".`reference_invoice_id`,
                                ".$table_sales.".`restobar_table_id`,
                                ".$table_sales.".`consumption_sales`,
                                ".$table_sales.".`self_withholding_amount`,
                                ".$table_sales.".`self_withholding_percentage`,
                                ".$table_sales.".`due_payment_method_id`";
                $insert = $DB2->query("INSERT INTO `".$DB2->dbprefix."sales` (id, date, reference_no, customer_id, customer, biller_id, biller, warehouse_id, note, staff_note, total, product_discount, order_discount_id, total_discount, order_discount, product_tax, order_tax_id, order_tax, total_tax, shipping, grand_total, sale_status, payment_status, payment_term, due_date, created_by, updated_by, updated_at, total_items, pos, paid, return_id, surcharge, attachment, return_sale_ref, sale_id, return_sale_total, rounding, suspend_note, api, shop, address_id, seller_id, reserve_id, hash, manual_payment, cgst, sgst, igst, payment_method, pay_partner, rete_fuente_percentage, rete_fuente_total, rete_fuente_account, rete_fuente_base, rete_iva_percentage, rete_iva_total, rete_iva_account, rete_iva_base, rete_ica_percentage, rete_ica_total, rete_ica_account, rete_ica_base, rete_other_percentage, rete_other_total, rete_other_account, rete_other_base, resolucion, fe_correo_enviado, fe_aceptado, fe_recibido, cufe, codigo_qr, fe_id_transaccion, fe_mensaje, fe_mensaje_soporte_tecnico, fe_xml, fe_debit_credit_note_concept_dian_code, sale_currency, sale_currency_trm, cost_center_id, document_type_id, payment_method_fe, payment_mean_fe, tip_amount, shipping_in_grand_total, sale_origin, sale_origin_reference_no, rete_fuente_id, rete_iva_id, rete_ica_id, rete_other_id, sale_comm_perc, collection_comm_perc, sale_comm_amount, sale_comm_payment_status, debit_note_id, reference_debit_note, reference_invoice_id, restobar_table_id, consumption_sales, self_withholding_amount, self_withholding_percentage, due_payment_method_id) (SELECT ".$sales_select." FROM ".$table_sales." WHERE (".$table_sales.".grand_total - ".$table_sales.".paid) > 0 AND ".$table_sales.".sale_status != 'returned' )");
                if ($insert) {
                    $products = [];
                    foreach (($sales->result()) as $sale) {
                        $product = array(
                            'sale_id' => $sale->id,
                            'product_id' => $product_service,
                            'product_code' => 999999999,
                            'product_name' => 'Saldo Inicial CXC',
                            'product_type' => 'service',
                            'option_id' => NULL,
                            'net_unit_price' => ($sale->grand_total - $sale->paid),
                            'unit_price' => ($sale->grand_total - $sale->paid),
                            'quantity' => 1,
                            'product_unit_id' => NULL,
                            'product_unit_code' => NULL,
                            'unit_quantity' => 1,
                            'warehouse_id' => NULL,
                            'item_tax' => 0,
                            'tax_rate_id' => 1,
                            'tax' => 0,
                            'discount' => 0,
                            'item_discount' => 0,
                            'subtotal' => ($sale->grand_total - $sale->paid),
                            'serial_no' => NULL,
                            'real_unit_price' => ($sale->grand_total - $sale->paid),
                        );
                        $products[] = $product;
                        $this->syncNewYearSalePayments($sale->id);
                    }
                    $DB2->insert_batch('sale_items', $products);
                    $this->db->update('settings', ['close_year_step' => 3], ['setting_id' => 1]);
                    return $response = [
                            'error' => false,
                            'msg' => lang('successfull'),
                        ];
                }
            } else {
                $this->db->update('settings', ['close_year_step' => 3], ['setting_id' => 1]);
                return $response = [
                    'error' => false,
                    'msg' => lang('successfull'),
                ];
            }
        }
        $response = [
                        'error' => true,
                        'msg' => lang('error_loading_new_database'),
                    ];
        return $response;
    }

    public function transfer_cxp(){
        $purchases = $this->db->where('(grand_total - paid) > 0 AND status != "returned"')
                          ->get('purchases');
        if ($DB2 = $this->connect_new_year_database()) {
            if ($purchases->num_rows() > 0) {
                $DB2->delete('products', ['code' => 888888888]);
                $DB2->query("INSERT INTO {$DB2->dbprefix('products')}
                            (`code`, `name`, `unit`, `cost`, `avg_cost`, `price`, `alert_quantity`, `image`, `category_id`, `subcategory_id`, `quantity`, `tax_rate`, `track_quantity`, `details`, `warehouse`, `product_details`, `tax_method`, `type`, `promotion`, `promo_price`, `start_date`, `end_date`, `sale_unit`, `purchase_unit`, `brand`, `slug`, `featured`, `weight`)
                            VALUES
                            ( 888888888, 'Saldo Inicial CXP', 0, 0, 0, 0, 0, NULL, 1, /*CATEGORIA*/
                            NULL, 0, 1, NULL, NULL, NULL, NULL, 0, 'service', 0, NULL, NULL, NULL, 1, 1, 1, 'SICXC', NULL, 0)");
                $product_service = $DB2->insert_id();
                $DB2->query("DELETE {$DB2->dbprefix('purchase_items')} FROM {$DB2->dbprefix('purchase_items')}
                                INNER JOIN {$DB2->dbprefix('purchases')} ON {$DB2->dbprefix('purchases')}.id = {$DB2->dbprefix('purchase_items')}.purchase_id
                            WHERE {$DB2->dbprefix('purchases')}.reference_no LIKE '%SI%'
                            ");
                $DB2->like('reference_no', 'SI')
                    ->delete('purchases');
                $table_purchases = "`".$this->db->database."`.`".$this->db->dbprefix."purchases`";
                $table_purchase_items = "`".$this->db->database."`.`".$this->db->dbprefix."purchase_items`";
                $purchases_select = $table_purchases.".`id`,
                                    CONCAT('SI', ".$table_purchases.".`reference_no`) as `reference_no`,
                                    ".$table_purchases.".`date`,
                                    ".$table_purchases.".`supplier_id`,
                                    ".$table_purchases.".`supplier`,
                                    ".$table_purchases.".`warehouse_id`,
                                    ".$table_purchases.".`note`,
                                    (".$table_purchases.".`grand_total` - ".$table_purchases.".`paid`) as `total`,
                                    0 as `product_discount`,
                                    0 as `order_discount_id`,
                                    0 as `order_discount`,
                                    0 as `total_discount`,
                                    0 as `product_tax`,
                                    0 as `order_tax_id`,
                                    0 as `order_tax`,
                                    0 as `total_tax`,
                                    0 as `shipping`,
                                    (".$table_purchases.".`grand_total` - ".$table_purchases.".`paid`) as `grand_total`,
                                    0 as `paid`,
                                    ".$table_purchases.".`status`,
                                    'due' as `payment_status`,
                                    ".$table_purchases.".`created_by`,
                                    ".$table_purchases.".`updated_by`,
                                    ".$table_purchases.".`updated_at`,
                                    ".$table_purchases.".`attachment`,
                                    ".$table_purchases.".`payment_term`,
                                    ".$table_purchases.".`due_date`,
                                    NULL as `return_id`,
                                    ".$table_purchases.".`surcharge`,
                                    NULL as `return_purchase_ref`,
                                    NULL as `purchase_id`,
                                    0 as `return_purchase_total`,
                                    ".$table_purchases.".`cgst`,
                                    ".$table_purchases.".`sgst`,
                                    ".$table_purchases.".`igst`,
                                    ".$table_purchases.".`closed_year`,
                                    ".$table_purchases.".`rete_fuente_percentage`,
                                    ".$table_purchases.".`rete_fuente_total`,
                                    ".$table_purchases.".`rete_fuente_account`,
                                    ".$table_purchases.".`rete_fuente_base`,
                                    ".$table_purchases.".`rete_iva_percentage`,
                                    ".$table_purchases.".`rete_iva_total`,
                                    ".$table_purchases.".`rete_iva_account`,
                                    ".$table_purchases.".`rete_iva_base`,
                                    ".$table_purchases.".`rete_ica_percentage`,
                                    ".$table_purchases.".`rete_ica_total`,
                                    ".$table_purchases.".`rete_ica_account`,
                                    ".$table_purchases.".`rete_ica_base`,
                                    ".$table_purchases.".`rete_other_percentage`,
                                    ".$table_purchases.".`rete_other_total`,
                                    ".$table_purchases.".`rete_other_account`,
                                    ".$table_purchases.".`rete_other_base`,
                                    0 as `order_discount_method`,
                                    ".$table_purchases.".`purchase_currency`,
                                    ".$table_purchases.".`purchase_currency_trm`,
                                    ".$table_purchases.".`purchase_type`,
                                    ".$table_purchases.".`credit_ledger_id`,
                                    ".$table_purchases.".`payment_affects_register`,
                                    ".$table_purchases.".`cost_center_id`,
                                    ".$table_purchases.".`biller_id`,
                                    ".$table_purchases.".`consecutive_supplier`,
                                    ".$table_purchases.".`document_type_id`,
                                    ".$table_purchases.".`rete_fuente_id`,
                                    ".$table_purchases.".`rete_iva_id`,
                                    ".$table_purchases.".`rete_ica_id`,
                                    ".$table_purchases.".`rete_other_id`,
                                    ".$table_purchases.".`purchase_origin`,
                                    ".$table_purchases.".`purchase_origin_reference_no`,
                                    ".$table_purchases.".`prorated_shipping_cost`,
                                    ".$table_purchases.".`consumption_purchase`,
                                    ".$table_purchases.".`resolucion`,
                                    ".$table_purchases.".`due_payment_method_id`";

                $insert = $DB2->query("INSERT INTO `".$DB2->dbprefix."purchases` (id, reference_no, date, supplier_id, supplier, warehouse_id, note, total, product_discount, order_discount_id, order_discount, total_discount, product_tax, order_tax_id, order_tax, total_tax, shipping, grand_total, paid, status, payment_status, created_by, updated_by, updated_at, attachment, payment_term, due_date, return_id, surcharge, return_purchase_ref, purchase_id, return_purchase_total, cgst, sgst, igst, closed_year, rete_fuente_percentage, rete_fuente_total, rete_fuente_account, rete_fuente_base, rete_iva_percentage, rete_iva_total, rete_iva_account, rete_iva_base, rete_ica_percentage, rete_ica_total, rete_ica_account, rete_ica_base, rete_other_percentage, rete_other_total, rete_other_account, rete_other_base, order_discount_method, purchase_currency, purchase_currency_trm, purchase_type, credit_ledger_id, payment_affects_register, cost_center_id, biller_id, consecutive_supplier, document_type_id, rete_fuente_id, rete_iva_id, rete_ica_id, rete_other_id, purchase_origin, purchase_origin_reference_no, prorated_shipping_cost, consumption_purchase, resolucion, due_payment_method_id) (SELECT ".$purchases_select." FROM ".$table_purchases." WHERE (".$table_purchases.".grand_total - ".$table_purchases.".paid) > 0 AND ".$table_purchases.".status != 'returned' )");
                $products = [];
                $fecha = date('Y-m-d H:i:s');
                if ($insert) {
                    foreach (($purchases->result()) as $purchase) {
                        if ($purchase->purchase_type == 2) {
                            $purchase_item = $this->db->where(['purchase_id' => $purchase->id])->get('purchase_items')->row();
                            $product = array(
                                'date' => $fecha,
                                'purchase_id' => $purchase->id,
                                'product_id' => $purchase_item->product_id,
                                'product_code' => $purchase_item->product_code,
                                'product_name' => $purchase_item->product_name,
                                'status' => $purchase_item->status,
                                'option_id' => $purchase_item->option_id,
                                'net_unit_cost' => ($purchase->grand_total - $purchase->paid),
                                'unit_cost' => ($purchase->grand_total - $purchase->paid),
                                'quantity' => $purchase_item->quantity,
                                'product_unit_id' => $purchase_item->product_unit_id,
                                'product_unit_code' => $purchase_item->product_unit_code,
                                'unit_quantity' => $purchase_item->quantity,
                                'warehouse_id' => $purchase_item->warehouse_id,
                                'item_tax' => $purchase_item->item_tax,
                                'tax_rate_id' => $purchase_item->tax_rate_id,
                                'tax' => $purchase_item->tax,
                                'discount' => $purchase_item->discount,
                                'item_discount' => $purchase_item->item_discount,
                                'subtotal' => ($purchase->grand_total - $purchase->paid),
                                'serial_no' => $purchase_item->serial_no,
                                'real_unit_cost' => ($purchase->grand_total - $purchase->paid),
                                'quantity_balance' => $purchase_item->quantity_balance,
                                'quantity_received' => $purchase_item->quantity_received,
                            );
                        } else {
                            $product = array(
                                'date' => $fecha,
                                'purchase_id' => $purchase->id,
                                'product_id' => $product_service,
                                'product_code' => 888888888,
                                'product_name' => 'Saldo Inicial CXP',
                                'status' => 'service_received',
                                'option_id' => NULL,
                                'net_unit_cost' => ($purchase->grand_total - $purchase->paid),
                                'unit_cost' => ($purchase->grand_total - $purchase->paid),
                                'quantity' => 1,
                                'product_unit_id' => NULL,
                                'product_unit_code' => NULL,
                                'unit_quantity' => 1,
                                'warehouse_id' => 1,
                                'item_tax' => 0,
                                'tax_rate_id' => 1,
                                'tax' => 0,
                                'discount' => 0,
                                'item_discount' => 0,
                                'subtotal' => ($purchase->grand_total - $purchase->paid),
                                'serial_no' => NULL,
                                'real_unit_cost' => ($purchase->grand_total - $purchase->paid),
                                'quantity_balance' => 1,
                                'quantity_received' => 1,
                            );
                        }

                        $products[] = $product;
                        $this->syncNewYearPurchasePayments($purchase->id);
                    }
                    $DB2->insert_batch('purchase_items', $products);
                    $this->db->update('settings', ['close_year_step' => 4], ['setting_id' => 1]);
                    return $response = [
                                'error' => false,
                                'msg' => lang('successfull'),
                            ];
                }
            } else {
                $this->db->update('settings', ['close_year_step' => 4], ['setting_id' => 1]);
                return $response = [
                    'error' => false,
                    'msg' => lang('successfull'),
                ];
            }
        }
        $response = [
                        'error' => true,
                        'msg' => lang('error_loading_new_database'),
                    ];
        return $response;
    }

    public function connect_new_year_database(){
        $new_config = $this->db->get('new_year_database_connection', 1);
        if ($new_config->num_rows() > 0) {
            $new_config = $new_config->row_array();
            $new_config['dbdriver'] = $this->db->dbdriver;
            $new_config['db_debug'] = $this->db->db_debug;
            $new_config['cache_on'] = $this->db->cache_on;
            $new_config['cachedir'] = $this->db->cachedir;
            $new_config['port']     = $this->db->port;
            $new_config['char_set'] = $this->db->char_set;
            $new_config['dbcollat'] = $this->db->dbcollat;
            $new_config['pconnect'] = $this->db->pconnect;
            if ($this->site->check_database($new_config)) {
                $DB2 = $this->load->database($new_config, TRUE);
                return $DB2;
            }
        }
        return false;
    }

    public function create_new_permissions_for_group($id){
        $data = [
            'group_id' => $id,
            'products-index' => 0,
            'products-add' => 0,
            'products-edit' => 0,
            'products-delete' => 0,
            'products-cost' => 0,
            'products-price' => 0,
            'quotes-index' => 0,
            'quotes-add' => 0,
            'quotes-edit' => 0,
            'quotes-pdf' => 0,
            'quotes-email' => 0,
            'quotes-delete' => 0,
            'sales-index' => 0,
            'sales-fe_index' => 0,
            'sales-add' => 0,
            'sales-edit' => 0,
            'sales-pdf' => 0,
            'sales-email' => 0,
            'sales-delete' => 0,
            'purchases-index' => 0,
            'purchases-add' => 0,
            'purchases-edit' => 0,
            'purchases-pdf' => 0,
            'purchases-email' => 0,
            'purchases-delete' => 0,
            'transfers-index' => 0,
            'transfers-add' => 0,
            'transfers-edit' => 0,
            'transfers-pdf' => 0,
            'transfers-email' => 0,
            'transfers-delete' => 0,
            'customers-index' => 0,
            'customers-add' => 0,
            'customers-edit' => 0,
            'customers-delete' => 0,
            'suppliers-index' => 0,
            'suppliers-add' => 0,
            'suppliers-edit' => 0,
            'suppliers-delete' => 0,
            'sales-deliveries' => 0,
            'sales-add_delivery' => 0,
            'sales-edit_delivery' => 0,
            'sales-delete_delivery' => 0,
            'sales-email_delivery' => 0,
            'sales-pdf_delivery' => 0,
            'sales-gift_cards' => 0,
            'sales-add_gift_card' => 0,
            'sales-edit_gift_card' => 0,
            'sales-delete_gift_card' => 0,
            'pos-index' => 0,
            'pos-sales' => 0,
            'sales-return_sales' => 0,
            'reports-index' => 0,
            'reports-warehouse_stock' => 0,
            'reports-quantity_alerts' => 0,
            'reports-expiry_alerts' => 0,
            'reports-products' => 0,
            'reports-daily_sales' => 0,
            'reports-monthly_sales' => 0,
            'reports-sales' => 0,
            'reports-payments' => 0,
            'reports-purchases' => 0,
            'reports-profit_loss' => 0,
            'reports-customers' => 0,
            'reports-suppliers' => 0,
            'reports-staff' => 0,
            'reports-register' => 0,
            'reports-payment_term_expired' => 0,
            'sales-payments' => 0,
            'purchases-payments' => 0,
            'purchases-expenses' => 0,
            'products-adjustments' => 0,
            'bulk_actions' => 0,
            'customers-list_deposits' => 0,
            'customers-delete_deposit' => 0,
            'products-barcode' => 0,
            'purchases-return_purchases' => 0,
            'reports-expenses' => 0,
            'reports-daily_purchases' => 0,
            'reports-monthly_purchases' => 0,
            'products-stock_count' => 0,
            'edit_price' => 0,
            'returns-index' => 0,
            'returns-add' => 0,
            'returns-edit' => 0,
            'returns-delete' => 0,
            'returns-email' => 0,
            'returns-pdf' => 0,
            'reports-tax' => 0,
            'payments-index' => 0,
            'payments-add' => 0,
            'system_settings-categories' => 0,
            'system_settings-add_category' => 0,
            'system_settings-edit_category' => 0,
            'system_settings-delete_category' => 0,
            'system_settings-units' => 0,
            'system_settings-add_unit' => 0,
            'system_settings-edit_unit' => 0,
            'system_settings-delete_unit' => 0,
            'system_settings-brands' => 0,
            'system_settings-add_brand' => 0,
            'system_settings-edit_brand' => 0,
            'system_settings-delete_brand' => 0,
            'system_settings-tax_rates' => 0,
            'system_settings-add_tax_rate' => 0,
            'system_settings-edit_tax_rate' => 0,
            'system_settings-delete_tax_rate' => 0,
            'system_settings-payment_methods' => 0,
            'system_settings-add_payment_method' => 0,
            'system_settings-edit_payment_method' => 0,
            'system_settings-delete_payment_method' => 0,
            'reports-best_sellers' => 0,
            'reports-valued_products' => 0,
            'reports-adjustments' => 0,
            'reports-categories' => 0,
            'reports-brands' => 0,
            'reports-users' => 0,
            'reports-portfolio' => 0,
            'reports-portfolio_report' => 0,
            'reports-debts_to_pay_report' => 0,
            'reports-load_zeta' => 0,
            'reports-load_bills' => 0,
            'reports-load_rentabilidad_doc' => 0,
            'reports-load_rentabilidad_customer' => 0,
            'reports-load_rentabilidad_producto' => 0,
            'shop_settings-index' => 0,
            'shop_settings-slider' => 0,
            'shop_settings-pages' => 0,
            'shop_settings-add_page' => 0,
            'shop_settings-sms_settings' => 0,
            'shop_settings-send_sms' => 0,
            'shop_settings-sms_log' => 0,
            'shop_settings-edit_page' => 0,
            'shop_settings-delete_page' => 0,
            'sales-orders' => 0,
            'sales-add_order' => 0,
            'sales-edit_order' => 0,
            'products-production_orders' => 0,
            'products-add_production_order' => 0,
            'products-edit_production_order' => 0,
            'products-delete_production_order' => 0
        ];
        $this->db->insert('permissions', $data);
    }

    public function get_unit_by_name($unit_name){
        $q = $this->db->get_where('units', ['name' => $unit_name]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_unit_movements($unit_id){
        $q = $this->db->get_where('products', ['unit' => $unit_id]);
        if ($q->num_rows() > 0) {
            return true;
        }

        $q = $this->db->get_where('sale_items', ['product_unit_id' => $unit_id]);
        if ($q->num_rows() > 0) {
            return true;
        }

        $q = $this->db->get_where('purchase_items', ['product_unit_id' => $unit_id]);
        if ($q->num_rows() > 0) {
            return true;
        }


    }

    public function get_all_expense_categories(){
        $q = $this->db->get('expense_categories');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function update_prices_margins($cost_type, $price_group_type, $price_rounding, $categories, $subcategories){

        $products = [];
        if ($categories) {
            foreach ($categories as $category_id) {
                $category = $this->site->getCategoryByID($category_id);
                $q = $this->db->get_where('products', ['category_id' => $category_id]);
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $row->profitability_margin = $category->profitability_margin;
                        $products[] = $row;
                    }
                }
            }
        }
        if ($subcategories) {
            foreach ($subcategories as $subcategory_id) {
                $subcategory = $this->site->getCategoryByID($subcategory_id);
                $q = $this->db->get_where('products', ['subcategory_id' => $subcategory_id]);
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $row->profitability_margin = $subcategory->profitability_margin;
                        $products[] = $row;
                    }
                }
            }
        }
        foreach ($products as $product) {
            $precio = $this->calculate_price_by_margin($product);
            $precio = $this->sma->number_rounding($precio, $price_rounding);
            $this->db->update('products', ['price' => $precio], ['id' => $product->id]);
        }

        return true;

    }

    public function calculate_price_by_margin($product){

        $costo = $product->cost;
        $margin = $product->profitability_margin;

        if ($product->tax_method == 0) {
            $costo = $this->sma->remove_tax_from_amount($product->tax_rate, $costo);
        }

        if ($this->Settings->margin_price_formula == 0) {
            $precio = $costo / (1 - ($margin / 100));
        } else if ($this->Settings->margin_price_formula == 1) {
            $precio = $costo * (1 + ($margin / 100));
        }

        if ($product->tax_method == 0) {
            $tax_precio = $this->sma->calculateTax($product->tax_rate, $precio, $product->tax_method);
            $precio = $precio + $tax_precio;
        }

        return $precio;
    }

    public function validate_document_type_prefix($prefix){
        $q = $this->db->get_where('documents_types', ['sales_prefix' => $prefix]);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }


    public function addPreferencesCategory($name, $preferences)
    {
        if ($this->db->insert('preferences_categories', ['name' => $name])) {
            $cat_id = $this->db->insert_id();
            $this->syncCreatedAttribute($cat_id);

            foreach ($preferences as $prf_name) {
                $this->db->insert('preferences', ['category_id' => $cat_id, 'name' => $prf_name]);
                $preferenceId = $this->db->insert_id();
                $this->syncCreatedAttributeValue($preferenceId);
            }
            return $cat_id;
        }
        return false;
    }

    private function syncCreatedAttribute($id)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->getAllIntegrations();
            $preferenceCategory = $this->getProductPreferenceCategory($id);
            if (!empty($integrations) && !empty($preferenceCategory)) {
                $this->load->integration_model('AttributeIntegration');
                foreach ($integrations as $integration) {
                    $Attribute = new AttributeIntegration();
                    $Attribute->open($integration);
                    $data = [
                        "name" => $preferenceCategory->name,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s"),
                        "id_wappsi" => $preferenceCategory->id,
                    ];
                    $Attribute->create($data);
                    $Attribute->close();
                }
            }
        }
    }

    private function syncCreatedAttributeValue($id)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->getAllIntegrations();
            $preference = $this->getPreference(["id"=>$id]);
            if (!empty($integrations) && !empty($preference)) {
                $this->load->integration_model('AttributeValue');
                $this->load->integration_model('AttributeIntegration');

                foreach ($integrations as $integration) {
                    $Attribute = new AttributeIntegration();
                    $Attribute->open($integration);
                    $attribute = $Attribute->find(["id_wappsi" => $preference->category_id]);

                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $data = [
                        "attribute_id" => $attribute->id,
                        "name" => $preference->name,
                        "created_at" => date("Y-m-d H:i:s"),
                        "updated_at" => date("Y-m-d H:i:s"),
                        "id_wappsi" => $preference->id,
                    ];
                    $AttributeValue->create($data);
                    $AttributeValue->close();
                }
            }
        }
    }

    public function getProductPreferenceCategory($id)
    {
        $q = $this->db->get_where("preferences_categories", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPreference($data)
    {
        $this->db->where($data);
        $q = $this->db->get("preferences");
        return $q->row();
    }

    public function getCategoryPreferences($id)
    {
        $q = $this->db->get_where('preferences', ['category_id' => $id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function updatePreferencesCategory($id, $data)
    {
        $name = $data['name'];
        $selection_limit = $data['selection_limit'];
        $required = $data['required'];
        $preferences = $data['preferences'];
        $preferences_id = $data['preferences_id'];
        $delete_preferences = $data['delete_preferences'];

        if ($this->db->update('preferences_categories', ['name' => $name, 'selection_limit' => $selection_limit, 'required' => $required], ['id'=>$id])) {
            $this->syncUpdatedAttribute($id);

            foreach ($preferences as $prf => $prf_name) {
                if (isset($preferences_id[$prf])) {
                    $this->db->update('preferences', ['name' => $prf_name], ['id' => $preferences_id[$prf]]);
                    $this->syncUpdatedAttributeValue($preferences_id[$prf]);
                } else {
                    $this->db->insert('preferences', ['category_id' => $id, 'name' => $prf_name]);
                    $preferenceId = $this->db->insert_id();
                    $this->syncCreatedAttributeValue($preferenceId);
                }

                if ($delete_preferences) {
                    foreach ($delete_preferences as $dprf => $dprf_id) {
                        $this->db->delete('preferences', ['id'=>$dprf_id]);
                        $this->syncDeletedAttributeValue($dprf_id);
                    }
                }

            }
            return true;
        }
        return false;
    }

    private function syncUpdatedAttribute($id)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->getAllIntegrations();
            $preferenceCategory = $this->getProductPreferenceCategory($id);
            if (!empty($integrations) && !empty($preferenceCategory)) {
                $this->load->integration_model('AttributeIntegration');
                foreach ($integrations as $integration) {
                    $Attribute = new AttributeIntegration();
                    $Attribute->open($integration);
                    $attributeExt = $Attribute->find(["id_wappsi"=>$preferenceCategory->id]);
                    $data = [
                        "name" => $preferenceCategory->name,
                        "updated_at" => date("Y-m-d H:i:s"),
                    ];
                    $Attribute->update($data, $attributeExt->id);
                    $Attribute->close();
                }
            }
        }
    }

    private function syncUpdatedAttributeValue($id)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->getAllIntegrations();
            $preference = $this->getPreference(["id"=>$id]);
            if (!empty($integrations) && !empty($preference)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $attributeValue = $AttributeValue->find(["id_wappsi"=>$preference->id]);
                    $data = [
                        "name" => $preference->name,
                        "updated_at" => date("Y-m-d H:i:s"),
                    ];
                    $AttributeValue->update($data, $attributeValue->id);
                    $AttributeValue->close();
                }
            }
        }
    }

    private function syncDeletedAttributeValue($id)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('AttributeValue');
                foreach ($integrations as $integration) {
                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);

                    $variantExt = $AttributeValue->find(["id_wappsi"=>$id]);
                    $AttributeValue->delete($variantExt->id);
                    $AttributeValue->close();
                }
            }
        }
    }

    public function getAllCountries()
    {
        $q = $this->db->get("countries");
        return $q->result();
    }

    public function getAllStates($data = [])
    {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get("states");
        return $q->result();
    }

    public function getAllCities($data = [])
    {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get("cities");
        return $q->result();
    }

    public function getAllZones($data = [])
    {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get("zones");
        return $q->result();
    }

    public function add_ubication($type, $data)
    {
        if ($type == 1) {
            $currency = $this->site->getCurrencyByCode($data['currency']);
            $response = $this->db->insert('countries', [
                'CODIGO' => $data['postal_code'],
                'NOMBRE' => $data['name'],
                'MONEDA' => $currency->id,
                'codigo_iso' => $data['codigo_iso'],
            ]);
        } else if ($type == 2) {
            $response = $this->db->insert('states', [
                'CODDEPARTAMENTO' => $data['postal_code'],
                'DEPARTAMENTO' => $data['name'],
                'DESADICIONAL' => 'DE '.$data['name'],
                'PAIS' => $data['country'],
            ]);
        } else if ($type == 3) {
            $state = $this->site->get_state_by_id($data['state']);
            $response = $this->db->insert('cities', [
                'PAIS' => $data['country'],
                'CODDEPARTAMENTO' => $data['state'],
                'DEPARTAMENTO' => $state->DEPARTAMENTO,
                'CODIGO' => $data['postal_code'],
                'DESCRIPCION' => $data['name'],
            ]);
        } else if ($type == 4) {
            $response = $this->db->insert('zones', [
                'country_code' => $data['country'],
                'state_code' => $data['state'],
                'codigo_ciudad' => $data['city'],
                'zone_name' => $data['name'],
                'zone_code' => $data['postal_code'],
            ]);
        } else if ($type == 5) {
            $response = $this->db->insert('subzones', [
                'country_code' => $data['country'],
                'state_code' => $data['state'],
                'city_code' => $data['city'],
                'zone_code' => $data['zone'],
                'subzone_name' => $data['name'],
                'subzone_code' => $data['postal_code'],
            ]);
        }
        return $response;
    }

    public function edit_ubication($type, $data)
    {
        if ($type == 1) {
            $currency = $this->site->getCurrencyByCode($data['currency']);
            $this->db->update('countries', [
                'NOMBRE' => $data['name'],
                'MONEDA' => $currency->id,
                'codigo_iso' => $data['codigo_iso']."AA"
            ],
            ['CODIGO' => $data['postal_code']]);
        } else if ($type == 2) {
            $this->db->update('states', [
                'DEPARTAMENTO' => $data['name'],
                'DESADICIONAL' => 'DE '.$data['name'],
                'PAIS' => $data['country']
            ],
            ['CODDEPARTAMENTO' => $data['postal_code']]);
        } else if ($type == 3) {
            $state = $this->site->get_state_by_id($data['state']);
            $this->db->update('cities', [
                'PAIS' => $data['country'],
                'CODDEPARTAMENTO' => $data['state'],
                'DEPARTAMENTO' => $state->DEPARTAMENTO,
                'DESCRIPCION' => $data['name']
            ],
            ['CODIGO' => $data['postal_code']]);
        } else if ($type == 4) {
            $this->db->update('zones', [
                'codigo_ciudad' => $data['city'],
                'zone_name' => $data['name']
            ],
            ['zone_code' => $data['postal_code']]);
        } else if ($type == 5) {
            $this->db->update('subzones', [
                'zone_code' => $data['zone'],
                'subzone_name' => $data['name']
            ],
            ['subzone_code' => $data['postal_code']]);
        }
        return true;
    }

    public function delete_ubication($type, $id)
    {
        $condiciones = [];
        $condiciones_delete = [];

        if ($type == 1) {
            $country = $this->site->get_country_by_id($id);
            $condiciones['country'] = $country->NOMBRE;
            $condiciones_delete['CODIGO'] = $id;
            $table = 'countries';
        } else if ($type == 2) {
            $state = $this->site->get_state_by_id($id);
            $condiciones['state'] = $state->DEPARTAMENTO;
            $condiciones_delete['CODDEPARTAMENTO'] = $id;
            $table = 'states';
        } else if ($type == 3) {
            $city = $this->site->get_city_by_id($id);
            $condiciones['city'] = $city->DESCRIPCION;
            $condiciones_delete['CODIGO'] = $id;
            $table = 'cities';
        } else if ($type == 4) {
            $zone = $this->site->get_zone_by_id($id);
            $condiciones['location'] = $zone->zone_name;
            $condiciones_delete['zone_code'] = $id;
            $table = 'zones';
        } else if ($type == 5) {
            $subzone = $this->site->get_subzone_by_id($id);
            $condiciones['subzone'] = $subzone->subzone_name;
            $condiciones_delete['subzone_code'] = $id;
            $table = 'subzones';
        }

        $q = $this->db->get_where('companies', $condiciones);
        if ($q->num_rows() > 0) {
            return (['response' => lang('ubication_cant_be_deleted_company'), 'success' => 0]);
        } else {
            if ($this->db->delete($table, $condiciones_delete)) {
                return (['response' => lang('ubication_deleted'), 'success' => 1]);
            } else {
                return (['response' => lang('ubication_cant_be_deleted_error'), 'success' => 0]);
            }
        }

    }

    public function  update_ubication_shipping_cost($biller_id, $ubicationcode, $ubicationtype, $price)
    {
        $city = null;
        $zone = null;
        $subzone = null;
        if ($ubicationtype == 3) {
            $city = $this->site->get_city_by_id($ubicationcode);
            $field_name = 'codigo_ciudad';
        } else if ($ubicationtype == 4) {
            $zone = $this->site->get_zone_by_id($ubicationcode);
            $city = $this->site->get_city_by_id($zone->codigo_ciudad);
            $field_name = 'codigo_zona';
        } else if ($ubicationtype == 5) {
            $subzone = $this->site->get_subzone_by_id($ubicationcode);
            $zone = $this->site->get_zone_by_id($subzone->zone_code);
            $city = $this->site->get_city_by_id($zone->codigo_ciudad);
            $field_name = 'codigo_subzona';
        }
        $q = $this->db->get_where('ubication_shipping_cost', [$field_name => $ubicationcode, 'biller_id' => $biller_id]);
        if ($q->num_rows() > 0) {
            $q = $q->row();
            $this->db->update('ubication_shipping_cost',
                                [
                                    'codigo_ciudad' => $city ? $city->CODIGO : null,
                                    'codigo_zona' => $zone ? $zone->zone_code : null,
                                    'codigo_subzona' => $subzone ? $subzone->subzone_code : null,
                                    'costo' => $price,
                                    'biller_id' => $biller_id,
                                ],
                                [
                                    'id' => $q->id,
                                ]
                            );
        } else {
            $this->db->insert('ubication_shipping_cost',
                                [
                                    'codigo_ciudad' => $city ? $city->CODIGO : null,
                                    'codigo_zona' => $zone ? $zone->zone_code : null,
                                    'codigo_subzona' => $subzone ? $subzone->subzone_code : null,
                                    'biller_id' => $biller_id,
                                    'costo' => $price,
                                ]
                            );
        }
        return true;
    }

    public function get_data_for_list_ubication($data)
    {
        $this->load->library('datatables');

        $this->datatables->select("
            (CASE
                WHEN {$this->db->dbprefix('cities')}.CODIGO IS NOT NULL THEN {$this->db->dbprefix('cities')}.CODIGO
                WHEN {$this->db->dbprefix('states')}.CODDEPARTAMENTO IS NOT NULL THEN {$this->db->dbprefix('states')}.CODDEPARTAMENTO
                ELSE {$this->db->dbprefix('countries')}.CODIGO
            END) AS id,
            {$this->db->dbprefix('countries')}.NOMBRE as pais,
            {$this->db->dbprefix('states')}.DEPARTAMENTO as departamento,
            {$this->db->dbprefix('cities')}.DESCRIPCION as ciudad,
            {$this->db->dbprefix('zones')}.zone_name as zona,
            {$this->db->dbprefix('subzones')}.subzone_name as subzona,

            {$this->db->dbprefix('countries')}.CODIGO as codigo_pais,
            {$this->db->dbprefix('states')}.CODDEPARTAMENTO as codigo_departamento,
            {$this->db->dbprefix('cities')}.CODIGO as codigo_ciudad,
            {$this->db->dbprefix('zones')}.zone_code as codigo_zona,
            {$this->db->dbprefix('subzones')}.subzone_code as codigo_subzona,
            (CASE
                WHEN {$this->db->dbprefix('cities')}.CODIGO IS NOT NULL THEN {$this->db->dbprefix('cities')}.status
                WHEN {$this->db->dbprefix('states')}.CODDEPARTAMENTO IS NOT NULL THEN {$this->db->dbprefix('states')}.status
                ELSE {$this->db->dbprefix('countries')}.status
            END) AS status");

        $this->datatables->join('states', "{$this->db->dbprefix('states')}.PAIS = {$this->db->dbprefix('countries')}.CODIGO", 'left');
        $this->datatables->join('cities', "{$this->db->dbprefix('cities')}.CODDEPARTAMENTO = {$this->db->dbprefix('states')}.CODDEPARTAMENTO", 'left');
        $this->datatables->join('zones', "{$this->db->dbprefix('zones')}.codigo_ciudad = {$this->db->dbprefix('cities')}.CODIGO", 'left');
        $this->datatables->join('subzones', "{$this->db->dbprefix('subzones')}.zone_code = {$this->db->dbprefix('zones')}.zone_code", 'left');
        $this->datatables->from('countries');

        if (!empty($data["countryId"])) {
            $this->datatables->where("{$this->db->dbprefix('countries')}.CODIGO", $data["countryId"]);
        }

        if (!empty($data["stateId"])) {
            $this->datatables->where("{$this->db->dbprefix('states')}.CODDEPARTAMENTO", $data["stateId"]);
        }

        if (!empty($data["cityId"])) {
            $this->datatables->where("{$this->db->dbprefix('cities')}.CODIGO", $data["cityId"]);
        }

        if ($data["status"] != null) {
            $this->datatables->where("(CASE
                WHEN {$this->db->dbprefix('cities')}.CODIGO IS NOT NULL THEN {$this->db->dbprefix('cities')}.status
                WHEN {$this->db->dbprefix('states')}.CODDEPARTAMENTO IS NOT NULL THEN {$this->db->dbprefix('states')}.status
                ELSE {$this->db->dbprefix('countries')}.status
            END) = {$data["status"]}");
        }

        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                    <div class=\"btn-group text-left\">
                                                        <button type=\"button\" class='btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle' data-toggle=\"dropdown\" data-toggle-second='tooltip' data-placement='top' title='Acciones'>
                                                            <i class='fas fa-ellipsis-v fa-lg'></i></button>
                                                        </button>
                                                        <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                            <li class=\"country\">
                                                                <a href='" . admin_url('system_settings/edit_ubication/1/$1') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>
                                                                    " . lang("edit_country") . "
                                                                </a>
                                                            </li>
                                                            <li class=\"state\">
                                                                <a href='" . admin_url('system_settings/edit_ubication/2/$2') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>
                                                                    " . lang("edit_state") . "
                                                                </a>
                                                            </li>
                                                            <li class=\"city\">
                                                                <a href='" . admin_url('system_settings/edit_ubication/3/$3') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>
                                                                    " . lang("edit_city") . "
                                                                </a>
                                                            </li>
                                                            <li class=\"zone\">
                                                                <a href='" . admin_url('system_settings/edit_ubication/4/$4') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>
                                                                    " . lang("edit_zone") . "
                                                                </a>
                                                            </li>
                                                            <li  class=\"subzone\">
                                                                <a href='" . admin_url('system_settings/edit_ubication/5/$5') . "' data-toggle='modal' data-target='#myModal'><i class=\"fa fa-edit\"></i>
                                                                    " . lang("edit_subzone") . "
                                                                </a>
                                                            </li>


                                                            <li class=\"deleteCountry\">
                                                                <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_ubication/1/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>
                                                                " . lang("delete_country") . "
                                                                </a>
                                                            </li>
                                                            <li class=\"deleteState\">
                                                                <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_ubication/2/$2') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>
                                                                " . lang("delete_state") . "
                                                                </a>
                                                            </li>
                                                            <li class=\"deleteCity\">
                                                                <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_ubication/3/$3') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>
                                                                " . lang("delete_city") . "
                                                                </a>
                                                            </li>
                                                            <li class=\"deleteZone\">
                                                                <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_ubication/4/$4') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>
                                                                " . lang("delete_zone") . "
                                                                </a>
                                                            </li>
                                                            <li class=\"deleteSubzone\">
                                                                <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('system_settings/delete_ubication/5/$5') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>
                                                                " . lang("delete_subzone") . "
                                                                </a>
                                                            </li>

                                                        </ul>
                                                    </div>
                                                </div>", "codigo_pais, codigo_departamento, codigo_ciudad, codigo_zona, codigo_subzona");
        $this->datatables->order_by('countries.NOMBRE asc, states.DEPARTAMENTO asc, cities.DESCRIPCION asc, zones.zone_name asc, subzones.subzone_name asc');

        $this->datatables->unset_column('codigo_pais');
        $this->datatables->unset_column('codigo_departamento');
        $this->datatables->unset_column('codigo_ciudad');
        $this->datatables->unset_column('codigo_zona');
        $this->datatables->unset_column('codigo_subzona');

        return $this->datatables->generate();
    }

    public function get_note_concept_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("debit_credit_notes_concepts");
        $this->db->where("id", $id);
        $response = $this->db->get();

        return $response->row();
    }

    public function get_dian_note_concept($type)
    {
        $this->db->select("*");
        $this->db->from("dian_note_concept");
        $this->db->where("type", $type);
        $response = $this->db->get();

        return $response->result();
    }

    public function get_ledgers_con()
    {
        $current_year = $this->session->userdata("accounting_module");

        $this->db->select("*");
        $this->db->from("ledgers_con".$current_year);
        $response = $this->db->get();

        return $response->result();
    }

    public function save_note_concept($data) {
        if ($this->db->insert("debit_credit_notes_concepts", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_note_concept($data, $id) {
        $this->db->where('id', $id);
        if ($this->db->update("debit_credit_notes_concepts", $data)) {
            return TRUE;
        }

        return FALSE;
    }

    public function update_warehouse_status($id, $status){
        if ($this->db->update('warehouses', ['status'=>$status], ['id'=>$id])) {
            return true;
        }
        return false;
    }

    public function add_cost_center($data){
        if($this->site->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;
            if ($this->db->insert($tabla, $data)) {
                return true;
            } else {
                $this->session->set_flashdata('error', 'Error al agregar centro de costo');
            }
            return false;
        }
    }

    public function update_cost_center($id, $data){
        if($this->site->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;
            if ($this->db->update($tabla, $data, ['id' => $id])) {
                return true;
            } else {
                $this->session->set_flashdata('error', 'Error al actualizar centro de costo');
            }
            return false;
        }
    }

    public function add_paccount($data, $update_existing){
        if($this->site->wappsiContabilidadVerificacion() > 0){
            if (!$this->validate_paccount_exists($data)) {
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $tabla = 'accounts_con'.$contabilidadSufijo;
                if ($this->db->insert($tabla, $data)) {
                    return true;
                } else {
                    $this->session->set_flashdata('error', 'Error al agregar parámetro de cuenta');
                }
            } else {
                if ($update_existing == 1) {
                    $contabilidadSufijo = $this->session->userdata('accounting_module');
                    $tabla = 'accounts_con'.$contabilidadSufijo;
                    if ($this->db->update($tabla, $data, [
                                        'type'=> $data['type'],
                                        'typemov'=> $data['typemov'],
                                        'id_tax'=> $data['id_tax'],
                                        'id_category'=> $data['id_category'],
                                    ])) {
                        return true;
                    } else {
                        $this->session->set_flashdata('error', 'Error al editar parámetro de cuenta');
                    }
                } else {
                    $this->session->set_flashdata('error', 'Ya existe una parametrización similar a la registrada');

                }
            }
            return false;
        }
    }

    public function validate_paccount_exists($data){
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $tabla = 'accounts_con'.$contabilidadSufijo;
        $conditions['type'] = $data['type'];
        $conditions['typemov'] = $data['typemov'];
        if ($data['id_tax']) {
            $conditions['id_tax'] = $data['id_tax'];
        } else {
            $conditions['id_tax'] = 0;
        }
        if ($data['id_category']) {
            $conditions['id_category'] = $data['id_category'];
        } else {
            $conditions['id_category'] = NULL;
        }
        $q = $this->db->get_where($tabla, $conditions);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function get_paccount_by_id($id){
        if($this->site->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'accounts_con'.$contabilidadSufijo;
            $q = $this->db->get_where($tabla, ['id' => $id]);
            if ($q->num_rows() > 0) {
                return $q->row();
            }
            return false;
        }
    }

    public function update_paccount($id, $data){
        if($this->site->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'accounts_con'.$contabilidadSufijo;
            if ($this->db->update($tabla, $data, ['id'=>$id])) {
                return true;
            } else {
                $this->session->set_flashdata('error', 'Error al actualizar parámetro de cuenta');
            }
            return false;
        }
    }

    public function get_product_negative_quantity($warehouse_id = NULL, $limit = NULL){

        $wv_products = [];

        $conditions['quantity <'] = 0;
        if ($warehouse_id) {
            $conditions['warehouse_id'] = $warehouse_id;
        }
        $this->db->where($conditions);
        if ($limit) {
            $this->db->limit($limit);
        }
        $wv = $this->db->get('warehouses_products_variants');
        $wv_data = [];
        if ($wv->num_rows() > 0) {
            foreach (($wv->result()) as $wv_row) {
                $wv_data[$wv_row->warehouse_id][] = $wv_row;
                $wv_products[$wv_row->product_id] = 1;
            }
        }

        $this->db->select('warehouses_products.*')
                    ->join('warehouses_products_variants', ' warehouses_products_variants.product_id = warehouses_products.product_id ', 'left')
                    ->where('warehouses_products_variants.id', NULL)
                    ->where('warehouses_products.quantity <', 0);
        if ($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $w = $this->db->get('warehouses_products');
        $w_data = [];
        if ($w->num_rows() > 0) {
            foreach (($w->result()) as $w_row) {
                $w_data[$w_row->warehouse_id][] = $w_row;
            }
        }

        return ['warehouse_products' => $w_data, 'warehouses_products_variants' => $wv_data];

    }

    public function deposit_transfer() {

        $añoactual = "20".(substr($this->db->database, -2, 2));
        $nuevoano = $añoactual+1;
        $q = $this->db->get_where('deposits', ['balance >' => 0]);
        if ($DB2 = $this->connect_new_year_database()) {
            if ($q->num_rows() > 0) {
                $DB2->query(" DELETE FROM {$DB2->dbprefix('deposits')} WHERE date < '{$nuevoano}-01-01 00:00:00'; ");
                foreach (($q->result()) as $row) {

                    $deposit_exists = $DB2->get_where('deposits', ['id' => $row->id]);
                    if ($deposit_exists->num_rows() > 0) {
                        $deposit_exists = true;
                    } else {
                        $deposit_exists = false;
                    }

                    $DB2->query("INSERT INTO {$DB2->dbprefix('deposits')}
                                (
                                    ".($deposit_exists ? "" : "`id`,")."
                                    `date`,
                                    `reference_no`,
                                    `company_id`,
                                    `amount`,
                                    `balance`,
                                    `paid_by`,
                                    `note`,
                                    `created_by`,
                                    `updated_by`,
                                    `updated_at`,
                                    `biller_id`,
                                    `cost_center_id`,
                                    `third_type`,
                                    `origen`,
                                    `origen_reference_no`,
                                    `origen_document_type_id`,
                                    `document_type_id`,
                                    `affects_pos_register`
                                )
                                VALUES
                                (
                                    ".($deposit_exists ? "" : "'".$row->id."',")."
                                    '".$row->date."',
                                    '".$row->reference_no."',
                                    '".$row->company_id."',
                                    '".$row->amount."',
                                    '".$row->balance."',
                                    '".$row->paid_by."',
                                    '".$row->note."',
                                    '".$row->created_by."',
                                    '".$row->updated_by."',
                                    ".(!empty($row->updated_at) ? $row->updated_at : 'NULL').",
                                    '".$row->biller_id."',
                                    ".(!empty($row->cost_center_id) ? $row->cost_center_id : 'NULL').",
                                    '".$row->third_type."',
                                    '".$row->origen."',
                                    '".$row->origen_reference_no."',
                                    ".(!empty($row->origen_document_type_id) ? $row->origen_document_type_id : 'NULL').",
                                    '".$row->document_type_id."',
                                    '".$row->affects_pos_register."'
                                )");
                }
                $this->db->update('settings', ['close_year_step' => 5], ['setting_id' => 1]);
                $response = [
                                'error' => false,
                                'msg' => lang('deposits_balance_transfered'),
                            ];
                return $response;
            } else {
                $this->db->update('settings', ['close_year_step' => 5], ['setting_id' => 1]);
                $response = [
                                'error' => false,
                                'msg' => lang('not_deposits_balance_found'),
                            ];
                return $response;
            }
        }
    }

    public function get_open_pos_registers(){
        $q = $this->db->get_where('pos_register', ['status' => 'open']);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function truncate_new_year_tables(){
        if ($DB2 = $this->connect_new_year_database()) {
            $DB2->query("SET FOREIGN_KEY_CHECKS = 0;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."addresses`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."biller_data`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."biller_documents_types`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."biller_seller`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."branch_areas`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."brands`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."categories`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."combo_items`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."companies`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."currencies`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."customer_groups`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."date_format`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."documents_types`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."documentypes`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."expense_categories`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."groups`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."invoice_formats`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."invoice_notes`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."order_ref`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."pages`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."permissions`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."pos_settings`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."price_groups`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."printers`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."product_photos`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."product_preferences`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."product_prices`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."products`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."product_variants`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."variants`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."settings`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."shop_settings`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."tables`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."tax_class`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."tax_code_fe`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."tax_rates`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."technology_providers`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."types_customer_obligations`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."types_obligations-responsabilities`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."types_person`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."types_vat_regime`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."unit_prices`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."units`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."users`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."warehouses`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."warehouses_products`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."warehouses_products_variants`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."subzones`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."zones`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."cities`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."states`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."countries`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."payment_methods`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."debit_credit_notes_concepts`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."biller_categories_concessions`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."collection_discounts`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."console_service_data`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."cost_per_month`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."dashboards`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."dian_note_concept`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."tax_percentages_fe`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."technology_provider_configuration`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."withholdings`;");
            //NUEVAS
            $DB2->query("TRUNCATE `".$DB2->dbprefix."preferences`;");
            $DB2->query("TRUNCATE `".$DB2->dbprefix."preferences_categories`;");
            $DB2->query("SET FOREIGN_KEY_CHECKS = 1;");
        }
    }

    public function get_table_columns($table, $DB2){
        $table_columns = '';
        $q = $DB2->query("SELECT COLUMN_NAME FROM information_schema.columns WHERE table_name = '".$DB2->dbprefix.$table."' AND table_schema = '".$DB2->database."'");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                // exit(var_dump($row));
                $table_columns .= '`'.$row->COLUMN_NAME.'`, ';
            }
            $table_columns = trim($table_columns, ', ');
            return $table_columns;
        } else {
        }
    }

    public function update_prices_per_margin($data){
        $html = "<table class='table'>".
                    "<tr>".
                        "<th>Producto</th>".
                        "<th>Precio anterior</th>".
                        "<th>Costo promedio</th>".
                        "<th>Nuevo precio</th>".
                    "</tr>";
        $txt_user_activities_prices = false;
        $html_no_updated = "";
        $pg = $this->site->getPriceGroupByID($data['price_groups']);
        if ($data['how_to_calculate_prices'] == 1) {
            $q = $this->db->select('product_prices.id as ppid, product_prices.price as prev_price, products.*')
                    ->join('products', 'products.id = product_prices.product_id', 'inner')
                    ->where('product_prices.price_group_id', $data['price_groups'])
                    ->get('product_prices');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $product) {
                    $pcost = ($data['cost_option'] == 1 ? $product->cost : $product->avg_cost);
                    if ($this->Settings->tax_method == 1 && $data['cost_option'] != 1) {
                        $pcost = $this->sma->remove_tax_from_amount($product->tax_rate, $pcost);
                    }
                    if ($pcost > 0) {
                        if ($txt_user_activities_prices == false) {
                            $txt_user_activities_prices = $this->session->first_name." ".$this->session->last_name." recalculó los precios por margen (".$data['margin_percent']."% - Manual) de la lista ".$pg->name." : ";
                        }

                        $price = $this->price_per_margin($data, $pcost);
                        $this->db->update('product_prices', ['price' => $price, 'last_update' => date('Y-m-d H:i:s')], ['id'=>$product->ppid]);
                        if ($pg->price_group_base == 1) {
                            $this->db->update('products', ['price' => $price, 'last_update' => date('Y-m-d H:i:s')], ['id'=>$product->id]);
                        }
                        $html .= "<tr>".
                                    "<td>".$product->name."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($product->prev_price)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($pcost)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($price)."</td>".
                                  "<tr>";
                        $txt_user_activities_prices .= $product->name." de ".$this->sma->formatMoney($product->prev_price)." a ".$this->sma->formatMoney($price).", ";
                    } else {
                        $html_no_updated .= "<tr class='text-danger'>".
                                    "<td>".$product->name."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney(0)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($pcost)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney(0)."</td>".
                                  "<tr>";
                    }
                }

            }
        } else {
            $q = $this->db->select('product_prices.id as ppid, product_prices.price as prev_price, categories.profitability_margin_price_base as margin_percent, categories.name as category_name, products.*')
                    ->join('products', 'products.id = product_prices.product_id', 'inner')
                    ->join('categories', 'categories.id = products.category_id', 'inner')
                    ->where('product_prices.price_group_id', $data['price_groups'])
                    ->where_in('categories.id', $data['categories'])
                    ->get('product_prices');
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $product) {
                    $pcost = ($data['cost_option'] == 1 ? $product->cost : $product->avg_cost);
                    if ($this->Settings->tax_method == 1 && $data['cost_option'] != 1) {
                        $pcost = $this->sma->remove_tax_from_amount($product->tax_rate, $pcost);
                    }
                    if ($pcost > 0 && $product->margin_percent > 0) {
                        $data['margin_percent'] = $product->margin_percent;
                        if ($txt_user_activities_prices == false) {
                            $txt_user_activities_prices = $this->session->first_name." ".$this->session->last_name." recalculó los precios por margen (".$data['margin_percent']."% - Categoría) de la lista ".$pg->name." y la categoría ".$product->category_name." : ";
                        }
                        $price = $this->price_per_margin($data, $pcost);
                        $this->db->update('product_prices', ['price' => $price, 'last_update' => date('Y-m-d H:i:s')], ['id'=>$product->ppid]);
                        if ($pg->price_group_base == 1) {
                            $this->db->update('products', ['price' => $price, 'last_update' => date('Y-m-d H:i:s')], ['id'=>$product->id]);
                        }
                        $html .= "<tr>".
                                    "<td>".$product->name."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($product->prev_price)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($pcost)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($price)."</td>".
                                  "<tr>";

                        $txt_user_activities_prices .= $product->name." de ".$this->sma->formatMoney($product->prev_price)." a ".$this->sma->formatMoney($price).", ";
                    }  else {
                        $html_no_updated .= "<tr class='text-danger'>".
                                    "<td>".$product->name."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney(0)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney($pcost)."</td>".
                                    "<td class='text-right'>".$this->sma->formatMoney(0)."</td>".
                                  "<tr>";
                    }
                }
            }
        }
        $html .= $html_no_updated."<table>";

        $this->db->insert('user_activities', [
            'date' => date('Y-m-d H:i:s'),
            'type_id' => 1,
            'table_name' => 'products',
            'record_id' => $product->id,
            'user_id' => $this->session->userdata('user_id'),
            'module_name' => lang('products'),
            'description' => $txt_user_activities_prices,
        ]);

        return $html;
    }

    public function price_per_margin($data, $cost){

        $formula_type = $data['formula'];
        $margin = $data['margin_percent'];
        $rounding = $data['rounding'];

        if ($formula_type == 1) {
            $price = $cost / ( 1 - ($margin / 100));
        } else if ($formula_type == 2) {
            $price = $cost * ( 1 + ($margin / 100));
        }

        if ($rounding == 1) { //No redondear
            $price = $this->sma->formatDecimal($price);
        } else if ($rounding == 2) { //Redondear a enteros
            $price = round($price);
        } else if ($rounding == 3) { //Redondear a cientos
            $price = round($price, -2);
            if ($price == 0) {
                $price = 100;
            }
        } else if ($rounding == 4) { //Redondear a miles
            $price = round($price, -3);
            if ($price == 0) {
                $price = 1000;
            }
        }

        return $price;
    }

    public function get_ciiu_codes($term, $limit = 10)
    {
        $this->db->select("CONCAT(".$this->db->dbprefix('ciiu_codes').".code, ' - ', ".$this->db->dbprefix('ciiu_codes').".description) as text, ".$this->db->dbprefix('ciiu_codes').".id as id", FALSE);
        $this->db->where("((code like '%".$term."%') or (description like '%".$term."%'))");
        $q = $this->db->get('ciiu_codes', $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function audit_system($data)
    {

        $modules = array(
                  '1'   =>  'sales'     ,
                  '2'   =>  'sales'     ,
                  '4'   =>  'sales'     ,
                  '3'   =>  'sales'     ,
                  '26'  =>  'sales'     ,
                  '27'  =>  'sales'     ,
                  '19'  =>  'payments'  ,
                  '13'  =>  'payments'  ,
                  '31'  =>  'payments'  ,
                  '20'  =>  'payments'  ,
                  '32'  =>  'payments'  ,
                  '14'  =>  'payments'  ,
                  '17'  =>  'payments'  ,
                  '23'  =>  'payments'  ,
                  '24'  =>  'payments'  ,
                  '28'  =>  'payments'  ,
                  '29'  =>  'payments'  ,
                  '33'  =>  'payments'  ,
                  '34'  =>  'payments'  ,
                  '5'   =>  'purchases' ,
                  '6'   =>  'purchases' ,
                  '35'  =>  'purchases' ,
                  '21'  =>  'purchases' ,
                  '22'  =>  'purchases' ,
                  '7'   =>  'quotes'    ,
                  '9'   =>  'quotes'    ,
                  '10'  =>  'quotes'    ,
                  '8'   =>  'order_sales',
                  '18'  =>  'adjustments',
                  '11'  =>  'adjustments',
                  '40'  =>  'adjustments',
                  '25'  =>  'adjustments',
                  '12'  =>  'transfers' ,
                  '15'  =>  'deposits'  ,
                  '30'  =>  'deposits'  ,
                  '36'  =>  'expenses'  ,
                  '37'  =>  'pos_register_movements',
                  '38'  =>  'sequential_count',
                  '39'  =>  'production_order',
                );
        $modules_detail = array(
                  '1'   =>  'sale_items'     ,
                  '2'   =>  'sale_items'     ,
                  '4'   =>  'sale_items'     ,
                  '3'   =>  'sale_items'     ,
                  '26'  =>  'sale_items'     ,
                  '27'  =>  'sale_items'     ,
                  '5'   =>  'purchase_items' ,
                  '6'   =>  'purchase_items' ,
                  '35'  =>  'purchase_items' ,
                  '21'  =>  'purchase_items' ,
                  '22'  =>  'purchase_items' ,
                  '7'   =>  'quote_items'    ,
                  '9'   =>  'quote_items'    ,
                  '10'  =>  'quote_items'    ,
                  '8'   =>  'order_sale_items',
                  '18'  =>  'adjustment_items',
                  '11'  =>  'adjustment_items',
                  '40'  =>  'adjustment_items',
                  '25'  =>  'adjustment_items',
                  '12'  =>  'transfer_items' ,
                  '39'  =>  'production_order_detail',
                );
        $modules_detail_key = array(
                  '1'   =>  'sale_id'     ,
                  '2'   =>  'sale_id'     ,
                  '4'   =>  'sale_id'     ,
                  '3'   =>  'sale_id'     ,
                  '26'  =>  'sale_id'     ,
                  '27'  =>  'sale_id'     ,
                  '5'   =>  'purchase_id' ,
                  '6'   =>  'purchase_id' ,
                  '35'  =>  'purchase_id' ,
                  '21'  =>  'purchase_id' ,
                  '22'  =>  'purchase_id' ,
                  '7'   =>  'quote_id'    ,
                  '9'   =>  'quote_id'    ,
                  '10'  =>  'quote_id'    ,
                  '8'   =>  'sale_id',
                  '18'  =>  'adjustment_id',
                  '11'  =>  'adjustment_id',
                  '40'  =>  'adjustment_id',
                  '25'  =>  'adjustment_id',
                  '12'  =>  'transfer_id' ,
                  '39'  =>  'production_order_id',
                );
        $table_third_type = array(
                  '1'   =>  'customer_id'     ,
                  '2'   =>  'customer_id'     ,
                  '4'   =>  'customer_id'     ,
                  '3'   =>  'customer_id'     ,
                  '26'  =>  'customer_id'     ,
                  '27'  =>  'customer_id'     ,
                  '5'   =>  'supplier_id' ,
                  '6'   =>  'supplier_id' ,
                  '35'  =>  'supplier_id' ,
                  '21'  =>  'supplier_id' ,
                  '22'  =>  'supplier_id' ,
                  '7'   =>  'customer_id'    ,
                  '9'   =>  'customer_id'    ,
                  '10'  =>  'customer_id'    ,
                  '8'   =>  'customer_id',
                  '18'  =>  'companies_id',
                  '11'  =>  'companies_id',
                  '40'  =>  'companies_id',
                  '25'  =>  'companies_id',
                  '39'  =>  'employee_id',
                );
        $module_is_return = array(
                  '3'   =>  'sales'     ,
                  '4'   =>  'sales'     ,
                  '6'   =>  'purchases' ,
                  '22'  =>  'purchases' ,
                );

        if ($data['documents_types']) {
            foreach ($data['documents_types'] as $document_type_id) {
                $doc_type_data = $this->site->getDocumentTypeById($document_type_id);
                $table = $modules[$doc_type_data->module];
                $table_detail = isset($modules_detail[$doc_type_data->module]) ? $modules_detail[$doc_type_data->module] : false;
                $detail_key = isset($modules_detail_key[$doc_type_data->module]) ? $modules_detail_key[$doc_type_data->module] : false;
                $third_type = isset($table_third_type[$doc_type_data->module]) ? $table_third_type[$doc_type_data->module] : false;
                $is_return = isset($module_is_return[$doc_type_data->module]) ? true : false;
                $start_desc_text = $table."/".$table_detail."/";
                if ($data['audit_skipped_consecutives'] && $table != 'purchases') {
                    $return_array['audit_skipped_consecutives'] = [];
                    $register_skipped_consecutives = $this->db->where('date >=', $data['start_date'])
                             ->where('date <=', $data['end_date'])
                             ->where('document_type_id', $document_type_id)
                             ->order_by('reference_no ASC')
                             ->get($table);
                    $prev_consecutive = NULL;
                    if ($register_skipped_consecutives->num_rows() > 0) {
                        foreach (($register_skipped_consecutives->result()) as $ri_index => $riskp) {
                            $ref_array = explode("-", $riskp->reference_no);
                            $analyzed_consecutives[$ref_array[1]] = $ref_array[0];
                            if ($ri_index > 0) {
                                $separated_consecutives = $prev_consecutive - $ref_array[1];
                                if ($separated_consecutives < -1) {
                                    for ($i=$ref_array[1]-1; $i > $prev_consecutive; $i--) {
                                        $return_array['audit_skipped_consecutives'][] = $start_desc_text." Tipo doc : ".$ref_array[0].", consecutivo omitido : ".$i;
                                    }
                                }
                            }
                            $prev_consecutive = $ref_array[1];
                        }
                    }
                }//audit_skipped_consecutives
                if ($data['audit_header_detail'] && $table_detail) {
                    $return_array['audit_header_detail'] = [];
                    $left_validate = [];
                    $right_validate = [];
                    /////
                    if ($table_detail == 'sale_items') {
                        $left_validate[] = "SUM({$this->db->dbprefix('sale_items')}.item_tax + {$this->db->dbprefix('sale_items')}.item_tax_2)";
                        $right_validate[] = "{$this->db->dbprefix('sales')}.product_tax";
                        $msg_validate[] = "Impuesto diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('sale_items')}.subtotal)";
                        $right_validate[] = "{$this->db->dbprefix('sales')}.grand_total";
                        $msg_validate[] = "Total diferente";

                        $left_validate[] = "SUM(TRUNCATE({$this->db->dbprefix('sale_items')}.net_unit_price * {$this->db->dbprefix('sale_items')}.quantity, 4))";
                        $right_validate[] = "{$this->db->dbprefix('sales')}.total";
                        $msg_validate[] = "Subtotal diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('sale_items')}.item_discount)";
                        $right_validate[] = "{$this->db->dbprefix('sales')}.product_discount";
                        $msg_validate[] = "Descuento diferente";
                    }

                    if ($table_detail == 'purchase_items') {
                        $left_validate[] = "SUM({$this->db->dbprefix('purchase_items')}.item_tax + {$this->db->dbprefix('purchase_items')}.item_tax_2)";
                        $right_validate[] = "{$this->db->dbprefix('purchases')}.product_tax";
                        $msg_validate[] = "Impuesto diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('purchase_items')}.subtotal)";
                        $right_validate[] = "{$this->db->dbprefix('purchases')}.grand_total";
                        $msg_validate[] = "Total diferente";

                        $left_validate[] = "SUM(TRUNCATE({$this->db->dbprefix('purchase_items')}.net_unit_cost * {$this->db->dbprefix('purchase_items')}.quantity, 4))";
                        $right_validate[] = "{$this->db->dbprefix('purchases')}.total";
                        $msg_validate[] = "Subtotal diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('purchase_items')}.item_discount)";
                        $right_validate[] = "{$this->db->dbprefix('purchases')}.product_discount";
                        $msg_validate[] = "Descuento diferente";
                    }

                    if ($table_detail == 'quote_items') {
                        $left_validate[] = "SUM({$this->db->dbprefix('quote_items')}.item_tax + {$this->db->dbprefix('quote_items')}.item_tax_2)";
                        $right_validate[] = "{$this->db->dbprefix('quotes')}.product_tax";
                        $msg_validate[] = "Impuesto diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('quote_items')}.subtotal)";
                        $right_validate[] = "{$this->db->dbprefix('quotes')}.grand_total";
                        $msg_validate[] = "Total diferente";

                        $left_validate[] = "SUM(TRUNCATE({$this->db->dbprefix('quote_items')}.net_unit_price * {$this->db->dbprefix('quote_items')}.quantity, 4))";
                        $right_validate[] = "{$this->db->dbprefix('quotes')}.total";
                        $msg_validate[] = "Subtotal diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('quote_items')}.item_discount)";
                        $right_validate[] = "{$this->db->dbprefix('quotes')}.product_discount";
                        $msg_validate[] = "Descuento diferente";
                    }

                    if ($table_detail == 'order_sale_items') {
                        $left_validate[] = "SUM({$this->db->dbprefix('order_sale_items')}.item_tax + {$this->db->dbprefix('order_sale_items')}.item_tax_2)";
                        $right_validate[] = "{$this->db->dbprefix('order_sales')}.product_tax";
                        $msg_validate[] = "Impuesto diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('order_sale_items')}.subtotal)";
                        $right_validate[] = "{$this->db->dbprefix('order_sales')}.grand_total";
                        $msg_validate[] = "Total diferente";

                        $left_validate[] = "SUM(TRUNCATE({$this->db->dbprefix('order_sale_items')}.net_unit_price * {$this->db->dbprefix('order_sale_items')}.quantity, 4))";
                        $right_validate[] = "{$this->db->dbprefix('order_sales')}.total";
                        $msg_validate[] = "Subtotal diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('order_sale_items')}.item_discount)";
                        $right_validate[] = "{$this->db->dbprefix('order_sales')}.product_discount";
                        $msg_validate[] = "Descuento diferente";
                    }

                    if ($table_detail == 'adjustment_items') {

                    }

                    if ($table_detail == 'transfer_items') {
                        $left_validate[] = "SUM({$this->db->dbprefix('transfer_items')}.item_tax)";
                        $right_validate[] = "{$this->db->dbprefix('transfers')}.product_tax";
                        $msg_validate[] = "Impuesto diferente";

                        $left_validate[] = "SUM({$this->db->dbprefix('transfer_items')}.subtotal)";
                        $right_validate[] = "{$this->db->dbprefix('transfers')}.grand_total";
                        $msg_validate[] = "Total diferente";

                        $left_validate[] = "SUM(TRUNCATE({$this->db->dbprefix('transfer_items')}.net_unit_cost * {$this->db->dbprefix('transfer_items')}.quantity, 4))";
                        $right_validate[] = "{$this->db->dbprefix('transfers')}.total";
                        $msg_validate[] = "Subtotal diferente";
                    }

                    if ($table_detail == 'production_order_detail') {

                    }
                    /////

                    if (count($left_validate) > 0) {
                        $select_sql = "";
                        foreach ($left_validate as $key => $field) {
                            if ($key > 0) {
                                $select_sql .= ", ";
                            }
                            $select_sql .= $field.", ".$right_validate[$key];
                        }
                        $this->db->select('reference_no, '.$select_sql)
                                 ->join($table_detail, $table_detail.'.'.$detail_key.' = '.$table.".id", "inner")
                                 ->where('document_type_id', $document_type_id)
                                 ->where($table.'.date >=', $data['start_date'])
                                 ->where($table.'.date <=', $data['end_date']);

                        $having_sql = "(";
                        foreach ($left_validate as $key => $field) {
                            if ($key > 0) {
                                $having_sql .= ") OR (";
                            }
                            $having_sql .= $field." != ".$right_validate[$key];
                        }
                        $having_sql .= ")";
                        $this->db->having($having_sql);

                        $register_enc_detail = $this->db->from($table)->group_by($table.".id")->get();
                        if ($register_enc_detail->num_rows() > 0) {
                            foreach (($register_enc_detail->result()) as $riencdet) {
                                $row_msg = "";
                                foreach ($left_validate as $key => $field) {
                                    if ($riencdet->{$field} != $riencdet->{str_replace($this->db->dbprefix($table).".", "", $right_validate[$key])}) {
                                        $row_msg .=($row_msg != "" ? ", ": "").$msg_validate[$key];
                                    }
                                }
                                $return_array['audit_header_detail'][] = $start_desc_text.$riencdet->reference_no." (".$row_msg.")";
                            }
                        }
                    }
                }//audit_header_detail
                if ($data['audit_tax_retention_id_relation'] && $table_detail) {
                    $return_array['audit_tax_retention_id_relation'] = [];
                    $ref_validated = [];
                    if (
                            $table_detail == 'sale_items' ||
                            $table_detail == 'purchase_items'
                        ) {
                            $this->db->select($table.'.reference_no,
                                                '.$table.'.rete_fuente_id as table_retefuente_id,
                                                '.$table.'.rete_iva_id as table_reteiva_id,
                                                '.$table.'.rete_ica_id as table_reteica_id,
                                                '.$table_detail.'.id,
                                                '.$table_detail.'.id detail_id,
                                                tax_rates.id as tax_rate_id,
                                                rete_fuente.id as rete_fuente_id,
                                                rete_iva.id as rete_iva_id,
                                                rete_ica.id as rete_ica_id
                                            ')
                            ->join('withholdings rete_fuente', 'rete_fuente.id = '.$table.".rete_fuente_id AND ".$table.".rete_fuente_id IS NOT NULL", 'left')
                            ->join('withholdings rete_iva', 'rete_iva.id = '.$table.".rete_iva_id AND ".$table.".rete_iva_id IS NOT NULL", 'left')
                            ->join('withholdings rete_ica', 'rete_ica.id = '.$table.".rete_ica_id AND ".$table.".rete_ica_id IS NOT NULL", 'left')
                            ->join($table_detail, $table_detail.'.'.$detail_key.' = '.$table.".id", "inner")
                            ->join('tax_rates', 'tax_rates.id = '.$table_detail.".tax_rate_id", 'left')
                            ->join('products', 'products.id = '.$table_detail.'.product_id', 'inner')
                            ->where($table.'.document_type_id', $document_type_id)
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date']);
                        $register_tax_retention = $this->db->from($table)->get();
                    } else {
                        $this->db->select($table.'.reference_no,
                                                '.$table.'.rete_fuente_id as table_retefuente_id,
                                                '.$table.'.rete_iva_id as table_reteiva_id,
                                                '.$table.'.rete_ica_id as table_reteica_id,
                                                '.$table_detail.'.id detail_id,
                                                '.$table_detail.'.product_id,
                                                tax_rates.id as tax_rate_id
                                            ')
                            ->join($table_detail, $table_detail.'.'.$detail_key.' = '.$table.".id", "inner")
                            ->join('tax_rates', 'tax_rates.id = '.$table_detail.".tax_rate_id", 'left')
                            ->join('products', 'products.id = '.$table_detail.'.product_id', 'inner')
                            ->where($table.'.document_type_id', $document_type_id)
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date']);
                        $register_tax_retention = $this->db->from($table)->get();
                    }
                    if ($register_tax_retention->num_rows() > 0) {
                        foreach (($register_tax_retention->result()) as $row) {
                            $error_msg = "";
                            if (property_exists($row, "tax_rate_id")) {
                                if (is_null($row->tax_rate_id)) {
                                    $error_msg = $table_detail." ID : ".$row->detail_id." (ID de tarifa de impuesto inválido ";
                                }
                            }
                            if (!isset($ref_validated[$row->reference_no])) {
                                if (property_exists($row, "rete_fuente_id")) {
                                    if (is_null($row->rete_fuente_id) && !is_null($row->table_retefuente_id)) {
                                        $error_msg .= ($error_msg == "" ? $row->reference_no." (" : "")." ID de retefuente inválido ";
                                    }
                                }
                                if (property_exists($row, "rete_iva_id")) {
                                    if (is_null($row->rete_iva_id) && !is_null($row->table_reteiva_id)) {
                                        $error_msg .= ($error_msg == "" ? $row->reference_no." (" : "")." ID de reteiva inválido ";
                                    }
                                }
                                if (property_exists($row, "rete_ica_id")) {
                                    if (is_null($row->rete_ica_id) && !is_null($row->table_reteica_id)) {
                                        $error_msg .= ($error_msg == "" ? $row->reference_no." (" : "")." ID de reteica inválido ";
                                    }
                                }
                                $ref_validated[$row->reference_no] = 1;
                            }
                            if ($error_msg != "") {
                                $error_msg .= ")";
                                $return_array['audit_tax_retention_id_relation'][] = $start_desc_text.$error_msg;
                            }
                        }
                    }
                }//audit_tax_retention_id_relation
                if ($data['audit_tax_corresponds_product'] && $table_detail) {
                    $return_array['audit_tax_corresponds_product'] = [];
                    $ref_validated = [];
                    if (
                            $table_detail == 'sale_items' ||
                            $table_detail == 'purchase_items'
                        ) {
                        $this->db->select('reference_no,
                                            products.tax_rate,
                                          '.$table_detail.'.*
                                        ')
                            ->join($table_detail, $table_detail.'.'.$detail_key.' = '.$table.".id", "inner")
                            ->join('products', 'products.id = '.$table_detail.'.product_id', 'inner')
                            ->where('products.tax_rate != '.$table_detail.'.tax_rate_id')
                            ->where($table.'.document_type_id', $document_type_id)
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date']);
                        $register_tax_corresponds_product = $this->db->from($table)->get();
                        if ($register_tax_corresponds_product->num_rows() > 0) {
                            foreach (($register_tax_corresponds_product->result()) as $row) {
                                $return_array['audit_tax_corresponds_product'][] = $start_desc_text.$row->reference_no." - ".$table_detail." ID : ".$row->id." Movimiento : ".$row->tax_rate_id.", Producto : ".$row->tax_rate;
                            }
                        }
                    }
                }//audit_tax_corresponds_product
                if ($data['audit_company_relationed_exists'] && $third_type) {
                    $return_array['audit_company_relationed_exists'] = [];
                    $register_company_relationed_exists = $this->db->select($table.'.*')
                                     ->join('companies','companies.id = '.$table.".".$third_type, "left")
                                     ->where('document_type_id', $document_type_id)
                                     ->where($table.'.date >=', $data['start_date'])
                                     ->where($table.'.date <=', $data['end_date'])
                                     ->where('companies.id IS NULL')
                                     ->from($table)->get();
                    if ($register_company_relationed_exists->num_rows() > 0) {
                        foreach (($register_company_relationed_exists->result()) as $ridate) {
                            $return_array['audit_company_relationed_exists'][] = $start_desc_text.$ridate->reference_no;
                        }
                    }
                }//audit_company_relationed_exists
                if ($data['audit_cost_center_relation']) {
                    if($this->site->wappsiContabilidadVerificacion() > 0){
                        $return_array['audit_cost_center_relation'] = [];
                        $contabilidadSufijo = $this->session->userdata('accounting_module');
                        $cctabla = 'cost_centers_con'.$contabilidadSufijo;
                        $register_cost_center_relation = $this->db->join($cctabla." cc", 'cc.id = '.$table.'.cost_center_id', 'left')
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where('document_type_id', $document_type_id)
                                ->where('cc.id IS NULL')
                                ->get($table);
                        if ($register_cost_center_relation->num_rows() > 0) {
                            foreach (($register_cost_center_relation->result()) as $ridate) {
                                $return_array['audit_cost_center_relation'][] = $start_desc_text.$ridate->reference_no;
                            }
                        }
                    }
                }//audit_cost_center_relation
                if ($data['audit_products_relation_in_movements'] && $table_detail) {
                    $return_array['audit_products_relation_in_movements'] = [];
                    $register_products_relation_in_movements = $this->db->select($table_detail.'.id as detail_id, '.$table.'.*')
                            ->join($table_detail, $table_detail.".".$detail_key.' = '.$table.'.id', 'inner')
                            ->join('products', 'products.id = '.$table_detail.'.product_id', 'left')
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where('document_type_id', $document_type_id)
                            ->where('products.id IS NULL')
                            ->get($table);
                    if ($register_products_relation_in_movements->num_rows() > 0) {
                        foreach (($register_products_relation_in_movements->result()) as $ridate) {
                            $return_array['audit_products_relation_in_movements'][] = $start_desc_text.$ridate->reference_no." - ".$table_detail." ID : ".$ridate->detail_id;
                        }
                    }
                }//audit_products_relation_in_movements
                if ($data['audit_products_option_relation_in_movements'] && $table_detail) {
                    $return_array['audit_products_option_relation_in_movements'] = [];
                    $register1 = $this->db->select($table.'.reference_no, '.$table_detail.'.id detail_id, products.*')
                            ->join($table_detail, $table_detail.".product_id = products.id", 'inner')
                            ->join($table, $table.".id = ".$table_detail.".".$detail_key)
                            ->where('products.attributes', 1)
                            ->where('('.$table_detail.'.option_id IS NULL OR '.$table_detail.'.option_id = 0)')
                            ->where($table.".date >=", $data['start_date'])
                            ->where($table.".date <=", $data['end_date'])
                            ->where($table.".document_type_id", $document_type_id)
                            ->from('products')
                            ->get();
                    if ($register1->num_rows() > 0) {
                        foreach (($register1->result()) as $r1row) {
                            $return_array['audit_products_option_relation_in_movements'][] = $start_desc_text." Sin variante asignada, ".$r1row->reference_no." - ".$table_detail." ID : ".$r1row->detail_id;
                        }
                    }

                    $register2 = $this->db->select($table.'.reference_no, '.$table_detail.'.id detail_id, products.*')
                            ->join($table_detail, $table_detail.".product_id = products.id", 'inner')
                            ->join('product_variants', 'product_variants.id = '.$table_detail.'.option_id', 'left')
                            ->join($table, $table.".id = ".$table_detail.".".$detail_key)
                            ->where('products.attributes', 1)
                            ->where($table_detail.'.option_id > 0')
                            ->where('product_variants.id IS NULL')
                            ->where($table.".date >=", $data['start_date'])
                            ->where($table.".date <=", $data['end_date'])
                            ->where($table.".document_type_id", $document_type_id)
                            ->from('products')
                            ->get();
                    if ($register2->num_rows() > 0) {
                        foreach (($register2->result()) as $r2row) {
                            $return_array['audit_products_option_relation_in_movements'][] = $start_desc_text." Variante asignada no existe, ".$r2row->reference_no." - ".$table_detail." ID : ".$r2row->detail_id;
                        }
                    }

                    $register3 = $this->db->select($table.'.reference_no, '.$table_detail.'.id detail_id, products.*')
                            ->join($table_detail, $table_detail.".product_id = products.id", 'inner')
                            ->join('product_variants', 'product_variants.id = '.$table_detail.'.option_id', 'left')
                            ->join($table, $table.".id = ".$table_detail.".".$detail_key)
                            ->where('products.attributes', 1)
                            ->where($table_detail.'.option_id > 0')
                            ->where('product_variants.id IS NOT NULL')
                            ->where('product_variants.product_id != '.$table_detail.".product_id")
                            ->where($table.".date >=", $data['start_date'])
                            ->where($table.".date <=", $data['end_date'])
                            ->where($table.".document_type_id", $document_type_id)
                            ->from('products')
                            ->get();
                    if ($register3->num_rows() > 0) {
                        foreach (($register3->result()) as $r3row) {
                            $return_array['audit_products_option_relation_in_movements'][] = $start_desc_text." Variante asignada a producto diferente del movimiento, ".$r3row->reference_no." - ".$table_detail." ID : ".$r3row->detail_id;
                        }
                    }
                }//audit_products_option_relation_in_movements
                if ($data['audit_invoice_retentions_payments'] && ($table == 'purchases' || $table == 'sales')) {
                    $return_array['audit_invoice_retentions_payments'] = [];
                    $register_invoice_retentions_payments = $this->db->select($table.'.*')
                            ->join('payments', 'payments.'.$detail_key.' = '.$table.'.id AND payments.paid_by = "retencion"', 'left')
                            ->where("(COALESCE(".$this->db->dbprefix($table).".rete_fuente_total, 0) + COALESCE(".$this->db->dbprefix($table).".rete_iva_total, 0) + COALESCE(".$this->db->dbprefix($table).".rete_ica_total, 0) + COALESCE(".$this->db->dbprefix($table).".rete_other_total, 0)) > 0")
                            ->where('payments.id IS NULL')
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where($table.'.document_type_id', $document_type_id)
                            ->from($table)->get();

                    if ($register_invoice_retentions_payments->num_rows() > 0) {
                        foreach (($register_invoice_retentions_payments->result()) as $ridate) {
                            $return_array['audit_invoice_retentions_payments'][] = $start_desc_text.$ridate->reference_no;
                        }
                    }

                    $register_invoice_retentions_payments = $this->db->select($table.'.*')
                            ->join('payments', 'payments.'.$detail_key.' = '.$table.'.id AND payments.paid_by = "retencion"', 'inner')
                            ->where('payments.date != '.$table.".date")
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where($table.'.document_type_id', $document_type_id)
                            ->from($table)->get();

                    if ($register_invoice_retentions_payments->num_rows() > 0) {
                        foreach (($register_invoice_retentions_payments->result()) as $ridate) {
                            $return_array['audit_invoice_retentions_payments'][] = $start_desc_text." Fecha de pago por retención diferente a la de la factura ".$ridate->reference_no;
                        }
                    }
                }//audit_invoice_retentions_payments
                if ($data['audit_invoice_paid_value'] && ($table == 'purchases' || $table == 'sales')) {
                    $return_array['audit_invoice_paid_value'] = [];
                    $register_invoice_paid_value = $this->db->select($table.".*, SUM(COALESCE(".$this->db->dbprefix('payments').".amount, 0)) as total_paid")
                            ->join('payments', 'payments.'.$detail_key.' = '.$table.'.id', 'left')
                            ->having("total_paid > ".$table.".grand_total")
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where($table.'.document_type_id', $document_type_id)
                            ->from($table)->group_by($table.".id")->get();

                    if ($register_invoice_paid_value->num_rows() > 0) {
                        foreach (($register_invoice_paid_value->result()) as $ridate) {
                            $return_array['audit_invoice_paid_value'][] = $start_desc_text.$ridate->reference_no;
                        }
                    }
                }//audit_invoice_paid_value
                if ($data['audit_returns_payments'] && $is_return) {
                    $return_array['audit_returns_payments'] = [];
                    $register_returns_payments = $this->db->select('
                                IF('.$this->db->dbprefix('payments').'.id IS NULL, '.$this->db->dbprefix($table).'.reference_no, NULL) as return_reference,
                                IF(rpayments.id IS NULL, returned.reference_no, NULL) as returned_reference
                            ')
                            ->join('payments', 'payments.'.$detail_key.' = '.$table.'.id', 'left')
                            ->join($table.' returned', 'returned.id = '.$table.'.'.$detail_key, 'left')
                            ->join('payments rpayments', 'rpayments.'.$detail_key.' = returned.id AND rpayments.amount = (('.$this->db->dbprefix($table).'.grand_total + (COALESCE('.$this->db->dbprefix($table).'.rete_fuente_total, 0) + COALESCE('.$this->db->dbprefix($table).'.rete_iva_total, 0) + COALESCE('.$this->db->dbprefix($table).'.rete_ica_total, 0) + COALESCE('.$this->db->dbprefix($table).'.rete_other_total, 0))) * -1)', 'left')
                            ->where('(('.$this->db->dbprefix('payments').'.id IS NULL) OR (rpayments.id IS NULL)) AND '.$table.'.grand_total < 0')
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where($table.'.document_type_id', $document_type_id)
                            ->having('((return_reference IS NOT NULL AND returned_reference IS NULL) OR (return_reference IS NULL AND returned_reference IS NOT NULL) OR (return_reference IS NOT NULL AND returned_reference IS NOT NULL))')
                            ->from($table)->get();
                    if ($register_returns_payments->num_rows() > 0) {
                        foreach (($register_returns_payments->result()) as $ridate) {
                            if (!is_null($ridate->return_reference) && is_null($ridate->returned_reference)) {
                                $return_array['audit_returns_payments'][] = $start_desc_text."La devolución no tiene el pago correspondiente (Faltante o incorrecto), ".$ridate->return_reference;
                            } else if (!is_null($ridate->returned_reference) && is_null($ridate->return_reference)) {
                                $return_array['audit_returns_payments'][] = $start_desc_text."La factura no tiene el pago correspondiente de la devolución (Faltante o incorrecto), ".$ridate->returned_reference;
                            } else if (!is_null($ridate->return_reference) && !is_null($ridate->returned_reference)) {
                                $return_array['audit_returns_payments'][] = $start_desc_text."Los pagos existentes no son correspondientes entre Devolución y Factura, ".$ridate->return_reference." - ".$ridate->returned_reference;
                            }
                        }
                    }
                }//audit_returns_payments
                if ($data['audit_pending_invoices'] && ($table == 'purchases' || $table == 'sales')) {
                    $return_array['audit_pending_invoices'] = [];
                    $register_pending_invoices = $this->db->select($table.".*")
                            ->where($table.'.'.($table == 'purchases' ? 'status' : 'sale_status'), 'pending')
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where($table.'.document_type_id', $document_type_id)
                            ->from($table)->get();
                    if ($register_pending_invoices->num_rows() > 0) {
                        foreach (($register_pending_invoices->result()) as $ridate) {
                            $return_array['audit_pending_invoices'][] = $start_desc_text.$ridate->reference_no;
                        }
                    }
                    if ($table == 'purchases') {
                        $register_pending_invoices = $this->db->select($table.".*")
                                ->join($table_detail, $table_detail.".".$detail_key." = ".$table.".id", 'inner')
                                ->where($table.'.status', 'received')
                                ->where($table_detail.'.status', 'pending')
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where($table.'.document_type_id', $document_type_id)
                                ->from($table)->get();
                        if ($register_pending_invoices->num_rows() > 0) {
                            foreach (($register_pending_invoices->result()) as $ridate) {
                                $return_array['audit_pending_invoices'][] = $start_desc_text."Estado de producto diferente al estado de encabezado, ".$ridate->reference_no;
                            }
                        }
                    }
                }//audit_pending_invoices
                if ($data['audit_invoices_commision'] && $table == 'sales') {
                    $return_array['audit_invoices_commision'] = [];
                    $register_invoices_commision = $this->db->select($table.'.*')
                                ->join('addresses ADR', 'ADR.id = '.$table.'.address_id', 'inner')
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where($table.'.document_type_id', $document_type_id)
                                ->where($table.'.collection_comm_perc', 0)
                                ->where('ADR.seller_collection_comision >', 0)
                                ->from($table)->get();
                    if ($register_invoices_commision->num_rows() > 0) {
                        foreach (($register_invoices_commision->result()) as $ridate) {
                            $return_array['audit_pending_invoices'][] = $start_desc_text."Venta sin comisión, comisión definida en sucursal de cliente, ".$ridate->reference_no;
                        }
                    }

                    $register_invoices_commision = $this->db->select($table.'.*, GROUP_CONCAT('.$this->db->dbprefix('payments').'.reference_no) as pmnt_references')
                                ->join('payments', 'payments.sale_id = '.$table.'.id', 'inner')
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where($table.'.document_type_id', $document_type_id)
                                ->where($table.'.collection_comm_perc >', 0)
                                ->where('payments.comm_amount', 0)
                                ->where('payments.paid_by !=', 'retencion')
                                ->where('payments.paid_by !=', 'discount')
                                ->group_by($table.".id")
                                ->from($table)->get()
                                ;
                    if ($register_invoices_commision->num_rows() > 0) {
                        foreach (($register_invoices_commision->result()) as $ridate) {
                            $return_array['audit_pending_invoices'][] = $start_desc_text."Venta con comisión y con pagos sin comisión calculada, ".$ridate->reference_no." Pagos : ".$ridate->pmnt_references;
                        }
                    }
                }//audit_invoices_commision
                if ($data['audit_fe_invoice_pending_dian'] && $table == 'sales') {
                    $return_array['audit_fe_invoice_pending_dian'] = [];
                    $register_fe_invoice_pending_dian = $this->db->select($table.'.*')
                             ->join('documents_types', 'documents_types.id = '.$table.'.document_type_id', 'inner')
                             ->where('documents_types.factura_electronica', 1)
                             ->where($table.'.date >=', $data['start_date'])
                             ->where($table.'.date <=', $data['end_date'])
                             ->where($table.'.document_type_id', $document_type_id)
                             ->where($table.'.fe_aceptado !=', 2)
                             ->from($table)->get();
                    if ($register_fe_invoice_pending_dian->num_rows() > 0) {
                        foreach (($register_fe_invoice_pending_dian->result()) as $row) {
                            $return_array['audit_fe_invoice_pending_dian'][] = $start_desc_text.$row->reference_no;
                        }
                    }
                }//audit_fe_invoice_pending_dian
                if ($data['audit_sale_price_avg_costing'] && $table == 'sales') {
                    $return_array['audit_sale_price_avg_costing'] = [];
                    $register_sale_price_avg_costing = $this->db->select($table.'.reference_no, '.$this->db->dbprefix('costing').'.purchase_unit_cost, ((('.$this->db->dbprefix($table_detail).'.unit_price - '.$this->db->dbprefix('costing').'.purchase_unit_cost) / '.$this->db->dbprefix($table_detail).'.unit_price) * 100) perc,'.$table_detail.".*")
                            ->join($table_detail, $table_detail.".".$detail_key." = ".$table.".id", "inner")
                            ->join('costing', 'costing.sale_item_id = '.$table_detail.'.id', "inner")
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where($table.'.document_type_id', $document_type_id)
                            ->having('(perc <= 0 OR (perc > 0 AND perc <= 20) OR perc > 30)')
                            ->from($table)->get();
                    if ($register_sale_price_avg_costing->num_rows() > 0) {
                        foreach (($register_sale_price_avg_costing->result()) as $row) {
                            $return_array['audit_sale_price_avg_costing'][] = $start_desc_text.$row->reference_no." Cod. Producto: (".$row->product_code.") Margen : ".$this->sma->formatDecimal($row->perc).", Precio : ".$this->sma->formatMoney($row->unit_price).", Costo promedio : ".$this->sma->formatMoney($row->purchase_unit_cost);
                        }
                    }
                }//audit_sale_price_avg_costing
                if ($data['audit_credit_customers_direct_payments'] && $table == 'sales') {
                    $return_array['audit_credit_customers_direct_payments'] = [];
                    $register_credit_customers_direct_payments = $this->db->select($table.'.reference_no')
                            ->join('companies', 'companies.id = '.$table.'.customer_id', 'inner')
                            ->join('payments', 'payments.'.$detail_key.' = '.$table.'.id AND '.$table.'.date = payments.date', 'inner')
                            ->where($table.'.date >=', $data['start_date'])
                            ->where($table.'.date <=', $data['end_date'])
                            ->where($table.'.document_type_id', $document_type_id)
                            ->where('companies.customer_payment_type', 0)
                            ->where('payments.multi_payment', 0)
                            ->where('payments.paid_by !=', 'retencion')
                            ->from($table)->get();
                    if ($register_credit_customers_direct_payments->num_rows() > 0) {
                        foreach (($register_credit_customers_direct_payments->result()) as $row) {
                            $return_array['audit_credit_customers_direct_payments'][] = $start_desc_text.$row->reference_no;
                        }
                    }
                }//audit_credit_customers_direct_payments
                if ($data['audit_payments_concepts_ledger_id'] && $table == 'payments') {
                    $return_array['audit_payments_concepts_ledger_id'] = [];
                    $register_payments_concepts_ledger_id = $this->db->select($table.'.*')
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where($table.'.document_type_id', $document_type_id)
                                ->where($table.'.reference_no IS NOT NULL')
                                ->where($table.'.company_id IS NOT NULL')
                                ->where($table.'.purchase_id IS NULL')
                                ->where($table.'.sale_id IS NULL')
                                ->where($table.'.concept_ledger_id IS NULL')
                                ->get($table);
                    if ($register_payments_concepts_ledger_id->num_rows() > 0) {
                        foreach (($register_payments_concepts_ledger_id->result()) as $row) {
                            $return_array['audit_payments_concepts_ledger_id'][] = $start_desc_text.$row->reference_no;
                        }
                    }
                    if($this->site->wappsiContabilidadVerificacion() > 0){
                        $contabilidadSufijo = $this->session->userdata('accounting_module');
                        $entrytabla = 'entries_con'.$contabilidadSufijo;
                        $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                        $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                        $query_start_date = $data['start_date'];
                        $query_end_date = $data['end_date'];
                        $register_payments_concepts_ledger_id = $this->db->query("SELECT
                                                concept.reference_no,
                                                concept.t_id,
                                                MAX(concept.concept) as concept
                                            FROM
                                                (SELECT
                                                {$this->db->dbprefix($table)}.reference_no,
                                                {$this->db->dbprefix($table)}.id as t_id,
                                                IF({$this->db->dbprefix($etitemstabla)}.id IS NOT NULL,
                                                    IF({$this->db->dbprefix($etitemstabla)}.amount = {$this->db->dbprefix($table)}.amount,
                                                        IF({$this->db->dbprefix($etitemstabla)}.ledger_id > 0,
                                                            IF({$this->db->dbprefix($etitemstabla)}.ledger_id = {$this->db->dbprefix($table)}.concept_ledger_id, 2, 4)
                                                        , 3 )
                                                        , 1
                                                    )
                                                ,0) as concept
                                            FROM
                                                {$this->db->dbprefix($table)}
                                                    LEFT JOIN
                                                {$this->db->dbprefix('documents_types')} ON {$this->db->dbprefix('documents_types')}.id = {$this->db->dbprefix($table)}.document_type_id
                                                    LEFT JOIN
                                                {$this->db->dbprefix($ettabla)} ON {$this->db->dbprefix($ettabla)}.label = {$this->db->dbprefix('documents_types')}.sales_prefix
                                                    LEFT JOIN
                                                {$this->db->dbprefix($entrytabla)} ON {$this->db->dbprefix($entrytabla)}.entrytype_id = {$this->db->dbprefix($ettabla)}.id
                                                    AND {$this->db->dbprefix($entrytabla)}.number = SUBSTR({$this->db->dbprefix($table)}.reference_no,
                                                    (POSITION('-' IN {$this->db->dbprefix($table)}.reference_no) + 1))
                                                    LEFT JOIN
                                                {$this->db->dbprefix($etitemstabla)} ON {$this->db->dbprefix($etitemstabla)}.entry_id = {$this->db->dbprefix($entrytabla)}.id
                                            WHERE
                                                {$this->db->dbprefix($table)}.date >= '{$query_start_date}'
                                                    AND {$this->db->dbprefix($table)}.date <= '{$query_end_date}'
                                                    AND {$this->db->dbprefix($table)}.document_type_id = {$document_type_id}
                                                    AND {$this->db->dbprefix($table)}.reference_no IS NOT NULL
                                                    AND {$this->db->dbprefix($table)}.company_id IS NOT NULL
                                                    AND {$this->db->dbprefix($table)}.purchase_id IS NULL
                                                    AND {$this->db->dbprefix($table)}.sale_id IS NULL
                                                    AND {$this->db->dbprefix($table)}.concept_ledger_id IS NOT NULL) concept
                                            GROUP BY concept.t_id
                                            HAVING (concept = 1 or concept = 3 or concept = 4)
                                            ");
                        if ($register_payments_concepts_ledger_id->num_rows() > 0) {
                            foreach (($register_payments_concepts_ledger_id->result()) as $row) {
                                $txt_msg = "";
                                if ($row->concept == 1) {
                                    $txt_msg = "Concepto no está contabilizado";
                                } else if ($row->concept == 3) {
                                    $txt_msg = "Concepto contabilizado, pero sin cuenta auxiliar válida";
                                } else if ($row->concept == 4) {
                                    $txt_msg = "Concepto contabilizado, pero la cuenta auxiliar es diferente a la escogida en el RC|CE";
                                }
                                $return_array['audit_payments_concepts_ledger_id'][] = $start_desc_text.$row->reference_no." ".$txt_msg;
                            }
                        }
                    } //0 el RC no está contabilizado, 1 está contabilizado el RC pero el concepto no, 2 El RC y el concepto están contabilizados, 3 EL RC y el Concepto están contabilizados pero el concepto está sin cuenta auxiliar, 4 El RC y el Concepto están contabilizados pero el concepto tiene una cuenta auxiliar diferente a la escogida
                }//audit_payments_concepts_ledger_id
                if ($data['audit_invoice_subtotal_tax_grand_total'] && ($table == 'purchases' || $table == 'sales')) {
                    $return_array['audit_invoice_subtotal_tax_grand_total'] = [];
                    $register_invoice_subtotal_tax_grand_total = $this->db->select($table.'.*')
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where($table.'.document_type_id', $document_type_id)
                                ->where('('.$this->db->dbprefix($table).'.total + '.$this->db->dbprefix($table).'.total_tax) != '.$this->db->dbprefix($table).'.grand_total')
                                ->from($table)->get();
                    if ($register_invoice_subtotal_tax_grand_total->num_rows() > 0) {
                        foreach (($register_invoice_subtotal_tax_grand_total->result()) as $row) {
                            $return_array['audit_invoice_subtotal_tax_grand_total'][] = $start_desc_text.$row->reference_no." Subtotal : ".$this->sma->formatDecimal($row->total).", Total impuestos : ".$this->sma->formatDecimal($row->total_tax).", Gran total : ".$this->sma->formatDecimal($row->grand_total);
                        }
                    }
                }//audit_invoice_subtotal_tax_grand_total
                if ($data['audit_invoices_negative_balance'] && ($table == 'purchases' || $table == 'sales')) {
                    $return_array['audit_invoices_negative_balance'] = [];
                    $register_invoices_negative_balance = $this->db->select($table.'.*')
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where($table.'.document_type_id', $document_type_id)
                                ->where('('.$this->db->dbprefix($table).'.grand_total - '.$this->db->dbprefix($table).'.paid) < 0')
                                ->from($table)->get();
                    if ($register_invoices_negative_balance->num_rows() > 0) {
                        foreach (($register_invoices_negative_balance->result()) as $row) {
                            $return_array['audit_invoices_negative_balance'][] = $start_desc_text.$row->reference_no." Gran total : ".$this->sma->formatDecimal($row->grand_total).", Valor pagado : ".$this->sma->formatDecimal($row->paid);
                        }
                    }
                }//audit_invoices_negative_balance
                if ($data['audit_movement_accounted']) {
                    if ($table != "quotes" &&  $table != "order_sales" &&  $table != "transfers" &&  $table != "sequential_count" &&  $table != "production_order") {
                        if($this->site->wappsiContabilidadVerificacion() > 0){
                            $return_array['audit_movement_accounted'] = [];
                            $contabilidadSufijo = $this->session->userdata('accounting_module');
                            $entrytabla = 'entries_con'.$contabilidadSufijo;
                            $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                            $register_audit_movement_accounted = $this->db->select($table.".*")
                                    ->join('documents_types', "documents_types.id = ".$table.".document_type_id", "left")
                                    ->join($ettabla, $ettabla.".label = documents_types.sales_prefix", "left")
                                    ->join($entrytabla, $entrytabla.".entrytype_id = ".$ettabla.".id AND ".$entrytabla.".number = SUBSTR(".$this->db->dbprefix($table).".reference_no, (POSITION('-' IN ".$this->db->dbprefix($table).".reference_no)+1)) ", "left")
                                    ->where($table.'.date >=', $data['start_date'])
                                    ->where($table.'.date <=', $data['end_date'])
                                    ->where($table.'.document_type_id', $document_type_id)
                                    ->where($entrytabla.".id IS NULL")
                                    ->where("documents_types.not_accounting", 0)
                                    ->from($table)->get();
                            if ($register_audit_movement_accounted->num_rows() > 0) {
                                foreach (($register_audit_movement_accounted->result()) as $row) {
                                    $return_array['audit_movement_accounted'][] = $start_desc_text.$row->reference_no;
                                }
                            }
                        }
                    }
                }//audit_movement_accounted
                if ($data['audit_total_movement_payment_method'] && ($table == "purchases" || $table == "sales")) {
                    if($this->site->wappsiContabilidadVerificacion() > 0){
                        $return_array['audit_total_movement_payment_method'] = [];
                        $contabilidadSufijo = $this->session->userdata('accounting_module');
                        $field_ledger_id = ($table == 'sales' ? 'receipt_ledger_id' : 'payment_ledger_id');
                        $payment_methods = $this->db->select('payment_methods.*, pmnt_method_con.'.$field_ledger_id)
                                 ->join('payment_methods_con'.$contabilidadSufijo.' pmnt_method_con', 'pmnt_method_con.type = payment_methods.code')
                                 ->from('payment_methods')->get();
                        $pmnt_ledger_id = "(";
                        $pmnt_comm_ledger_id = "(";
                        $pmnt_retefuente_ledger_id = "(";
                        $pmnt_reteiva_ledger_id = "(";
                        $pmnt_reteica_ledger_id = "(";

                        $setted_ledger_id = [];
                        $setted_comm_ledger_id = [];
                        $setted_retefuente_ledger_id = [];
                        $setted_reteiva_ledger_id = [];
                        $setted_reteica_ledger_id = [];

                        if ($payment_methods->num_rows() > 0) {
                            foreach (($payment_methods->result()) as $pmnt) {
                                if (!isset($setted_ledger_id[$pmnt->{$field_ledger_id}])) {
                                    $pmnt_ledger_id .= $pmnt->{$field_ledger_id}.", ";
                                    $setted_ledger_id[$pmnt->{$field_ledger_id}] = 1;
                                }
                                if (!isset($setted_comm_ledger_id[$pmnt->commision_ledger_id]) && $pmnt->commision_ledger_id != NULL && $pmnt->commision_ledger_id != 0) {
                                    $pmnt_comm_ledger_id .= $pmnt->commision_ledger_id.", ";
                                    $setted_comm_ledger_id[$pmnt->commision_ledger_id] = 1;
                                }
                                if (!isset($setted_retefuente_ledger_id[$pmnt->retefuente_ledger_id]) && $pmnt->retefuente_ledger_id != NULL && $pmnt->retefuente_ledger_id != 0) {
                                    $pmnt_retefuente_ledger_id .= $pmnt->retefuente_ledger_id.", ";
                                    $setted_retefuente_ledger_id[$pmnt->retefuente_ledger_id] = 1;
                                }
                                if (!isset($setted_reteiva_ledger_id[$pmnt->reteiva_ledger_id]) && $pmnt->reteiva_ledger_id != NULL && $pmnt->reteiva_ledger_id != 0) {
                                    $pmnt_reteiva_ledger_id .= $pmnt->reteiva_ledger_id.", ";
                                    $setted_reteiva_ledger_id[$pmnt->reteiva_ledger_id] = 1;
                                }
                                if (!isset($setted_reteica_ledger_id[$pmnt->reteica_ledger_id]) && $pmnt->reteica_ledger_id != NULL && $pmnt->reteica_ledger_id != 0) {
                                    $pmnt_reteica_ledger_id .= $pmnt->reteica_ledger_id.", ";
                                    $setted_reteica_ledger_id[$pmnt->reteica_ledger_id] = 1;
                                }
                            }
                        }
                        $pmnt_ledger_id = trim($pmnt_ledger_id, ", ");
                        $pmnt_comm_ledger_id = trim($pmnt_comm_ledger_id, ", ");
                        $pmnt_retefuente_ledger_id = trim($pmnt_retefuente_ledger_id, ", ");
                        $pmnt_reteiva_ledger_id = trim($pmnt_reteiva_ledger_id, ", ");
                        $pmnt_reteica_ledger_id = trim($pmnt_reteica_ledger_id, ", ");

                        $pmnt_ledger_id .= ")";
                        $pmnt_comm_ledger_id .= ")";
                        $pmnt_retefuente_ledger_id .= ")";
                        $pmnt_reteiva_ledger_id .= ")";
                        $pmnt_reteica_ledger_id .= ")";
                        $rs_rs = [];
                        $query_start_date = $data['start_date'];
                        $query_end_date = $data['end_date'];

                        $register_total_movement_payment_method = $this->db->query("
                            SELECT
                                {$this->db->dbprefix($table)}.reference_no,
                                SUM(".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo.".amount) as entry_amount,
                                ({$this->db->dbprefix($table)}.grand_total -
                                    (
                                        {$this->db->dbprefix($table)}.rete_fuente_total +
                                        {$this->db->dbprefix($table)}.rete_iva_total +
                                        {$this->db->dbprefix($table)}.rete_ica_total +
                                        {$this->db->dbprefix($table)}.rete_other_total
                                    )
                                ) as grand_total
                            FROM
                                {$this->db->dbprefix($table)}
                                    INNER JOIN
                                {$this->db->dbprefix('documents_types')} ON {$this->db->dbprefix('documents_types')}.id = {$this->db->dbprefix($table)}.document_type_id
                                    INNER JOIN
                                ".$this->db->dbprefix('entrytypes').'_con'.$contabilidadSufijo." ON ".$this->db->dbprefix('entrytypes').'_con'.$contabilidadSufijo.".id = {$this->db->dbprefix('documents_types')}.entrytype_id
                                    INNER JOIN
                                ".$this->db->dbprefix('entries').'_con'.$contabilidadSufijo." ON ".$this->db->dbprefix('entries').'_con'.$contabilidadSufijo.".entrytype_id = ".$this->db->dbprefix('entrytypes').'_con'.$contabilidadSufijo.".id
                                    AND (".$this->db->dbprefix('entries').'_con'.$contabilidadSufijo.".number = SUBSTR({$this->db->dbprefix($table)}.reference_no, (POSITION('-' IN {$this->db->dbprefix($table)}.reference_no) + 1)))
                                    INNER JOIN
                                ".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo." ON ".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo.".entry_id = ".$this->db->dbprefix('entries').'_con'.$contabilidadSufijo.".id
                            WHERE
                                {$this->db->dbprefix($table)}.date >= '{$query_start_date}'
                                AND {$this->db->dbprefix($table)}.date <= '{$query_end_date}'
                                AND {$this->db->dbprefix($table)}.document_type_id = {$document_type_id}
                                AND (".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo.".ledger_id IN {$pmnt_ledger_id}
                                ".($table == 'sales' ?
                                    ($pmnt_comm_ledger_id != "()" ? "OR ".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo.".ledger_id IN {$pmnt_comm_ledger_id}" : "").
                                    ($pmnt_retefuente_ledger_id != "()" ? "OR ".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo.".ledger_id IN {$pmnt_retefuente_ledger_id}" : "").
                                    ($pmnt_reteiva_ledger_id != "()" ? "OR ".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo.".ledger_id IN {$pmnt_reteiva_ledger_id}" : "").
                                    ($pmnt_reteica_ledger_id != "()" ? "OR ".$this->db->dbprefix('entryitems').'_con'.$contabilidadSufijo.".ledger_id IN {$pmnt_reteica_ledger_id}" : "")
                                    : '').")
                            GROUP BY {$this->db->dbprefix($table)}.id
                            ");
                        if ($register_total_movement_payment_method->num_rows() > 0) {
                            foreach (($register_total_movement_payment_method->result()) as $row) {
                                if ($row->entry_amount != $row->grand_total) {
                                    $return_array['audit_total_movement_payment_method'][] = $start_desc_text.$row->reference_no;
                                }
                            }
                        }
                    }
                }//audit_total_movement_payment_method
                if ($data['audit_retention_and_discounts_accounted'] && ($table == "purchases" || $table == "sales")) {
                    if($this->site->wappsiContabilidadVerificacion() > 0){
                        $return_array['audit_retention_and_discounts_accounted'] = [];
                        $contabilidadSufijo = $this->session->userdata('accounting_module');
                        $entrytabla = 'entries_con'.$contabilidadSufijo;
                        $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                        $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                        $query_start_date = $data['start_date'];
                        $query_end_date = $data['end_date'];
                        $register_retention_and_discounts_accounted = $this->db->query("SELECT
                                            rete.reference_no,
                                            MAX(rete.retefuente) retefuente,
                                            MAX(rete.reteiva) reteiva,
                                            MAX(rete.reteica) reteica,
                                            MAX(rete.reteother) reteother,
                                            MAX(rete.order_discount) order_discount
                                        FROM (
                                        SELECT
                                            {$this->db->dbprefix($table)}.id as sale_id,
                                            {$this->db->dbprefix($table)}.reference_no,
                                            IF({$this->db->dbprefix($table)}.rete_fuente_total > 0,
                                                IF({$this->db->dbprefix($etitemstabla)}.amount = {$this->db->dbprefix($table)}.rete_fuente_total,
                                                    IF({$this->db->dbprefix($etitemstabla)}.ledger_id > 0, 2, 3),
                                                    1
                                                ), 0
                                            ) AS retefuente,
                                            IF({$this->db->dbprefix($table)}.rete_iva_total > 0,
                                                IF({$this->db->dbprefix($etitemstabla)}.amount = {$this->db->dbprefix($table)}.rete_iva_total,
                                                    IF({$this->db->dbprefix($etitemstabla)}.ledger_id > 0, 2, 3),
                                                    1
                                                ), 0
                                            ) AS reteiva,
                                            IF({$this->db->dbprefix($table)}.rete_ica_total > 0,
                                                IF({$this->db->dbprefix($etitemstabla)}.amount = {$this->db->dbprefix($table)}.rete_ica_total,
                                                    IF({$this->db->dbprefix($etitemstabla)}.ledger_id > 0, 2, 3),
                                                    1
                                                ), 0
                                            ) AS reteica,
                                            IF({$this->db->dbprefix($table)}.rete_other_total > 0,
                                                IF({$this->db->dbprefix($etitemstabla)}.amount = {$this->db->dbprefix($table)}.rete_other_total,
                                                    IF({$this->db->dbprefix($etitemstabla)}.ledger_id > 0, 2, 3),
                                                    1
                                                ), 0
                                            ) AS reteother,
                                            IF({$this->db->dbprefix($table)}.order_discount > 0,
                                                IF({$this->db->dbprefix($etitemstabla)}.amount = {$this->db->dbprefix($table)}.order_discount,
                                                    IF({$this->db->dbprefix($etitemstabla)}.ledger_id > 0, 2, 3),
                                                    1
                                                ), 0
                                            ) AS order_discount,
                                            {$this->db->dbprefix($table)}.rete_fuente_total,
                                            {$this->db->dbprefix($table)}.rete_iva_total,
                                            {$this->db->dbprefix($table)}.rete_ica_total,
                                            {$this->db->dbprefix($table)}.rete_other_total,
                                            {$this->db->dbprefix($etitemstabla)}.amount
                                        FROM
                                            {$this->db->dbprefix($table)}
                                                INNER JOIN
                                            {$this->db->dbprefix('documents_types')} ON {$this->db->dbprefix('documents_types')}.id = {$this->db->dbprefix($table)}.document_type_id
                                                INNER JOIN
                                            {$this->db->dbprefix($ettabla)} ON {$this->db->dbprefix($ettabla)}.id = {$this->db->dbprefix('documents_types')}.entrytype_id
                                                INNER JOIN
                                            {$this->db->dbprefix($entrytabla)} ON {$this->db->dbprefix($entrytabla)}.entrytype_id = {$this->db->dbprefix($ettabla)}.id
                                                AND ({$this->db->dbprefix($entrytabla)}.number = SUBSTR({$this->db->dbprefix($table)}.reference_no, (POSITION('-' IN {$this->db->dbprefix($table)}.reference_no) + 1)))
                                                LEFT JOIN
                                            {$this->db->dbprefix($etitemstabla)} ON {$this->db->dbprefix($etitemstabla)}.entry_id = {$this->db->dbprefix($entrytabla)}.id
                                        WHERE
                                            {$this->db->dbprefix($table)}.date >= '{$query_start_date}'
                                            AND {$this->db->dbprefix($table)}.date <= '{$query_end_date}'
                                            AND {$this->db->dbprefix($table)}.document_type_id = {$document_type_id}
                                            AND ({$this->db->dbprefix($table)}.rete_fuente_total > 0 OR {$this->db->dbprefix($table)}.rete_iva_total > 0 OR {$this->db->dbprefix($table)}.rete_ica_total > 0 OR {$this->db->dbprefix($table)}.rete_other_total > 0 OR {$this->db->dbprefix($table)}.order_discount > 0)
                                        ORDER BY {$this->db->dbprefix($table)}.reference_no ASC
                                        ) rete
                                        INNER JOIN {$this->db->dbprefix('payments')} ON {$this->db->dbprefix('payments')}.sale_id = rete.sale_id AND {$this->db->dbprefix('payments')}.paid_by = 'retencion'
                                        GROUP BY rete.sale_id
                                        HAVING (retefuente = 1 OR retefuente = 3
                                               OR reteica = 1 OR reteica = 3
                                               OR reteiva = 1 OR reteiva = 3
                                               OR reteother = 1 OR reteother = 3
                                               OR order_discount = 1 OR order_discount = 3)");
                        if ($register_retention_and_discounts_accounted->num_rows() > 0) {
                            foreach (($register_retention_and_discounts_accounted->result()) as $row) {
                                $txt_msg = "";
                                if ($row->retefuente == 1) {
                                    $txt_msg .= "No existe contabilización de ReteFuente";
                                } else if ($row->retefuente == 3) {
                                    $txt_msg .= "La contabilización de ReteFuente no está asociada a una cuenta auxiliar válida";
                                }
                                if ($row->reteiva == 1) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "No existe contabilización de ReteIVA";
                                } else if ($row->reteiva == 3) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "La contabilización de ReteIVA no está asociada a una cuenta auxiliar válida";
                                }
                                if ($row->reteiva == 1) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "No existe contabilización de ReteICA";
                                } else if ($row->reteiva == 3) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "La contabilización de ReteICA no está asociada a una cuenta auxiliar válida";
                                }
                                if ($row->reteother == 1) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "No existe contabilización de Otras retenciones";
                                } else if ($row->reteother == 3) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "La contabilización de Otras retenciones no está asociada a una cuenta auxiliar válida";
                                }
                                if ($row->order_discount == 1) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "No existe contabilización del descuento general";
                                } else if ($row->order_discount == 3) {
                                    if ($txt_msg != "") { $txt_msg .=", "; }
                                    $txt_msg .= "La contabilización del descuento general no está asociada a una cuenta auxiliar válida";
                                }
                                $return_array['audit_total_movement_payment_method'][] = $start_desc_text.$row->reference_no." ".$txt_msg;
                            }
                        }// 0 NO HAY, 1 ESTÁ ERRÓNEO, 2 ESTÁ CORRECTO, 3 EXISTE PERO SIN CUENTA
                    }
                }//audit_retention_and_discounts_accounted
                if ($data['audit_movement_accounting_third']) {
                    if ($table != "quotes" &&  $table != "order_sales" &&  $table != "transfers" &&  $table != "sequential_count" &&  $table != "production_order") {
                        if($this->site->wappsiContabilidadVerificacion() > 0){
                            $return_array['audit_movement_accounting_third'] = [];
                            $contabilidadSufijo = $this->session->userdata('accounting_module');
                            $entrytabla = 'entries_con'.$contabilidadSufijo;
                            $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                            $etitemstabla = 'entryitems_con'.$contabilidadSufijo;

                            if ($table != 'payments') {
                                $register_movement_accounting_third = $this->db->select($table.".*")
                                        ->join('documents_types', "documents_types.id = ".$table.".document_type_id", "inner")
                                        ->join($ettabla, $ettabla.".label = documents_types.sales_prefix", "inner")
                                        ->join($entrytabla, $entrytabla.".entrytype_id = ".$ettabla.".id AND ".$entrytabla.".number = SUBSTR(".$this->db->dbprefix($table).".reference_no, (POSITION('-' IN ".$this->db->dbprefix($table).".reference_no)+1)) ", "inner")
                                        ->join($etitemstabla, $etitemstabla.".entry_id = ".$entrytabla.".id", "inner")
                                        ->where($table.'.date >=', $data['start_date'])
                                        ->where($table.'.date <=', $data['end_date'])
                                        ->where($etitemstabla.'.companies_id != '.$table.".".$third_type)
                                        ->where($table.'.document_type_id', $document_type_id)
                                        ->group_by($table.'.id')
                                        ->from($table)->get();
                                if ($register_movement_accounting_third->num_rows() > 0) {
                                    foreach (($register_movement_accounting_third->result()) as $row) {
                                        $return_array['audit_movement_accounting_third'][] = $start_desc_text.$row->reference_no;
                                    }
                                }
                            } else {
                                $register_movement_accounting_third = $this->db->select($table.".*")
                                        ->join('documents_types', "documents_types.id = ".$table.".document_type_id", "inner")
                                        ->join($ettabla, $ettabla.".label = documents_types.sales_prefix", "inner")
                                        ->join($entrytabla, $entrytabla.".entrytype_id = ".$ettabla.".id AND ".$entrytabla.".number = SUBSTR(".$this->db->dbprefix($table).".reference_no, (POSITION('-' IN ".$this->db->dbprefix($table).".reference_no)+1)) ", "inner")
                                        ->join($etitemstabla, $etitemstabla.".entry_id = ".$entrytabla.".id", "inner")
                                        ->join('sales', 'sales.id = '.$table.'.sale_id', 'left')
                                        ->join('purchases', 'purchases.id = '.$table.'.purchase_id', 'left')
                                        ->where($table.'.date >=', $data['start_date'])
                                        ->where($table.'.date <=', $data['end_date'])
                                        ->where($etitemstabla.'.base', 0)
                                        ->where($this->db->dbprefix($etitemstabla).'.companies_id !=
                                        IF(
                                            IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,
                                                IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id
                                                )
                                            )
                                            ,
                                            IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,
                                                IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id
                                                )
                                            ), '.$this->db->dbprefix("payments").'.seller_id
                                        )')
                                        ->where($table.'.document_type_id', $document_type_id)
                                        ->group_by($table.'.id')
                                        ->from($table)->get();
                                if ($register_movement_accounting_third->num_rows() > 0) {
                                    foreach (($register_movement_accounting_third->result()) as $row) {
                                        $return_array['audit_movement_accounting_third'][] = $start_desc_text.$row->reference_no;
                                    }
                                }
                            }
                        }
                    }
                }//audit_movement_accounting_third
                if ($data['audit_movement_accounting_cost_center']) {
                    if ($table != "quotes" &&  $table != "order_sales" &&  $table != "transfers" &&  $table != "sequential_count" &&  $table != "production_order") {
                        if($this->site->wappsiContabilidadVerificacion() > 0){
                            $return_array['audit_movement_accounting_cost_center'] = [];
                            $contabilidadSufijo = $this->session->userdata('accounting_module');
                            $entrytabla = 'entries_con'.$contabilidadSufijo;
                            $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                            $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                            $register_movement_accounting_cost_center = $this->db->select($table.".*")
                                    ->join('documents_types', "documents_types.id = ".$table.".document_type_id", "inner")
                                    ->join($ettabla, $ettabla.".label = documents_types.sales_prefix", "inner")
                                    ->join($entrytabla, $entrytabla.".entrytype_id = ".$ettabla.".id AND ".$entrytabla.".number = SUBSTR(".$this->db->dbprefix($table).".reference_no, (POSITION('-' IN ".$this->db->dbprefix($table).".reference_no)+1)) ", "inner")
                                    ->join($etitemstabla, $etitemstabla.".entry_id = ".$entrytabla.".id", "inner")
                                    ->where($table.'.date >=', $data['start_date'])
                                    ->where($table.'.date <=', $data['end_date'])
                                    ->where($etitemstabla.'.cost_center_id != '.$table.".cost_center_id")
                                    ->where($table.'.document_type_id', $document_type_id)
                                    ->group_by($table.'.id')
                                    ->from($table)->get();
                            if ($register_movement_accounting_cost_center->num_rows() > 0) {
                                foreach (($register_movement_accounting_cost_center->result()) as $row) {
                                    $return_array['audit_movement_accounting_cost_center'][] = $start_desc_text.$row->reference_no;
                                }
                            }
                        }
                    }
                }//audit_movement_accounting_cost_center
                if ($data['audit_movement_accounting_duplicated']) {
                    if ($table != "quotes" &&  $table != "order_sales" &&  $table != "transfers" &&  $table != "sequential_count" &&  $table != "production_order") {
                        if($this->site->wappsiContabilidadVerificacion() > 0){
                            $return_array['audit_movement_accounting_duplicated'] = [];
                            $contabilidadSufijo = $this->session->userdata('accounting_module');
                            $entrytabla = 'entries_con'.$contabilidadSufijo;
                            $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                            $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                            $register_movement_accounting_duplicated = $this->db->select($table.".*, COUNT(".$this->db->dbprefix($entrytabla).".id) as num_entries")
                                    ->join('documents_types', "documents_types.id = ".$table.".document_type_id", "inner")
                                    ->join($ettabla, $ettabla.".label = documents_types.sales_prefix", "inner")
                                    ->join($entrytabla, $entrytabla.".entrytype_id = ".$ettabla.".id AND ".$entrytabla.".number = SUBSTR(".$this->db->dbprefix($table).".reference_no, (POSITION('-' IN ".$this->db->dbprefix($table).".reference_no)+1)) ", "inner")
                                    ->where($table.'.date >=', $data['start_date'])
                                    ->where($table.'.date <=', $data['end_date'])
                                    ->where($table.'.document_type_id', $document_type_id)
                                    ->group_by($table.'.id')
                                    ->having('num_entries > 1')
                                    ->from($table)->get();
                            if ($register_movement_accounting_duplicated->num_rows() > 0) {
                                foreach (($register_movement_accounting_duplicated->result()) as $row) {
                                    $return_array['audit_movement_accounting_duplicated'][] = $start_desc_text.$row->reference_no;
                                }
                            }
                        }
                    }
                }//audit_movement_accounting_duplicated
                if ($data['audit_invoice_pending_accounted'] && ($table == "purchases" || $table == "sales")) {
                    if($this->site->wappsiContabilidadVerificacion() > 0){
                        $return_array['audit_invoice_pending_accounted'] = [];
                        $contabilidadSufijo = $this->session->userdata('accounting_module');
                        $entrytabla = 'entries_con'.$contabilidadSufijo;
                        $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                        $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                        $register_invoice_pending_accounted = $this->db->select($table.".*")
                                ->join('documents_types', "documents_types.id = ".$table.".document_type_id", "inner")
                                ->join($ettabla, $ettabla.".label = documents_types.sales_prefix", "inner")
                                ->join($entrytabla, $entrytabla.".entrytype_id = ".$ettabla.".id AND ".$entrytabla.".number = SUBSTR(".$this->db->dbprefix($table).".reference_no, (POSITION('-' IN ".$this->db->dbprefix($table).".reference_no)+1)) ", "inner")
                                ->where($table.'.date >=', $data['start_date'])
                                ->where($table.'.date <=', $data['end_date'])
                                ->where($table.'.document_type_id', $document_type_id)
                                ->where($table.'.'.($table == 'sales' ? 'sale_status' : 'status'), 'pending')
                                ->group_by($table.'.id')
                                ->from($table)->get();
                        if ($register_invoice_pending_accounted->num_rows() > 0) {
                            foreach (($register_invoice_pending_accounted->result()) as $row) {
                                $return_array['audit_invoice_pending_accounted'][] = $start_desc_text.$row->reference_no;
                            }
                        }
                    }
                }//audit_invoice_pending_accounted
            }
        }
        $start_desc_text = "products//";
        if ($data['audit_product_cost_and_avg_cost']) {
            $return_array['audit_product_cost_and_avg_cost'] = [];
            $register_product_cost_and_avg_cost = $this->db->select('products.id, products.code, products.name, products.cost, warehouses_products.avg_cost, (((('.$this->db->dbprefix('products').'.cost - '.$this->db->dbprefix('warehouses_products').'.avg_cost) / '.$this->db->dbprefix('products').'.cost)) * 100) as perc', false)
                     ->join('warehouses_products', 'warehouses_products.product_id = products.id', 'inner')
                     ->having('perc <= 0 OR perc > 30')
                     ->from('products')->get();
            if ($register_product_cost_and_avg_cost->num_rows() > 0) {
                foreach (($register_product_cost_and_avg_cost->result()) as $row) {
                    $return_array['audit_product_cost_and_avg_cost'][] = $start_desc_text.$row->name." (".$row->code.") Costo : ".$this->sma->formatMoney($row->cost).", Costo promedio : ".$this->sma->formatMoney($row->avg_cost).", Margen : ".$this->sma->formatDecimal($row->perc);
                }
            }
        }//audit_product_cost_and_avg_cost
        if ($data['audit_product_cost_and_price_base']) {
            $return_array['audit_product_cost_and_price_base'] = [];
            $register_product_cost_and_avg_cost = $this->db->select('products.id, products.code, products.name, products.cost, product_prices.price, ((('.$this->db->dbprefix('product_prices').'.price - '.$this->db->dbprefix('products').'.cost) / '.$this->db->dbprefix('product_prices').'.price) * 100) perc', false)
                     ->join('product_prices', 'product_prices.product_id = products.id', 'inner')
                     ->join('price_groups', 'price_groups.price_group_base = 1 AND price_groups.id = product_prices.price_group_id', 'inner')
                     ->having('perc <= 0 OR perc > 30')
                     ->from('products')->get();
            if ($register_product_cost_and_avg_cost->num_rows() > 0) {
                foreach (($register_product_cost_and_avg_cost->result()) as $row) {
                    $return_array['audit_product_cost_and_price_base'][] = $start_desc_text.$row->name." (".$row->code.") Costo : ".$this->sma->formatMoney($row->cost).", Precio : ".$this->sma->formatMoney($row->price).", Margen : ".$this->sma->formatDecimal($row->perc);
                }
            }
        }//audit_product_cost_and_price_base
        if ($data['audit_products_without_movements']) {
            $return_array['audit_products_without_movements'] = [];
            $table_audited = [];
            $this->db->select('products.*');
            foreach ($modules_detail as $module_id => $module_table_detail) {
                if (!isset($table_audited[$module_table_detail])) {
                    $this->db->join($module_table_detail, $module_table_detail.".product_id = products.id", "left");
                    $table_audited[$module_table_detail] = 1;
                }
            }
            foreach ($table_audited as $module_table_detail_audited => $setted) {
                $this->db->where($module_table_detail_audited.'.id IS NULL');
            }
            $register_products_without_movements = $this->db->group_by('products.id')->from('products')->get();
            if ($register_products_without_movements->num_rows() > 0) {
                foreach (($register_products_without_movements->result()) as $row) {
                    $return_array['audit_products_without_movements'][] = $start_desc_text.$row->name." (".$row->code.")";
                }
            }
        }//audit_products_without_movements
        if ($data['audit_document_types_modulary_parametrization']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $start_desc_text = "documents_types//";
                $return_array['audit_document_types_modulary_parametrization'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                $register_document_types_modulary_parametrization = $this->db->select('documents_types.*')
                        ->join($ettabla, $ettabla.".label = documents_types.sales_prefix", "left")
                        ->where($ettabla.".id IS NULL")
                        ->from('documents_types')->get();
                if ($register_document_types_modulary_parametrization->num_rows() > 0) {
                    foreach (($register_document_types_modulary_parametrization->result()) as $row) {
                        $return_array['audit_document_types_modulary_parametrization'][] = $start_desc_text.$row->sales_prefix;
                    }
                }
            }
        }// audit_document_types_modulary_parametrization
        if ($data['audit_entries_debit_credit_total']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $return_array['audit_entries_debit_credit_total'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $entrytabla = 'entries_con'.$contabilidadSufijo;
                $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                $start_desc_text = $entrytabla."/".$etitemstabla."/";
                $register_entries_debit_credit_total = $this->db
                        ->select("
                                    SUM(IF({$this->db->dbprefix($etitemstabla)}.dc = 'C', {$this->db->dbprefix($etitemstabla)}.amount, 0)) as c_amount,
                                    SUM(IF({$this->db->dbprefix($etitemstabla)}.dc = 'D', {$this->db->dbprefix($etitemstabla)}.amount, 0)) as d_amount,
                                    CONCAT({$this->db->dbprefix($ettabla)}.prefix,{$this->db->dbprefix($entrytabla)}.number) as reference_no
                                ")
                        ->join($entrytabla, $entrytabla.".id = ".$etitemstabla.".entry_id", "inner")
                        ->join($ettabla, $ettabla.".id = ".$entrytabla.".entrytype_id", "inner")
                        ->group_by($etitemstabla.'.entry_id')
                        ->having('c_amount != d_amount')
                        ->from($etitemstabla)->order_by($entrytabla.'.entrytype_id ASC, '.$entrytabla.'.number ASC')->get();
                if ($register_entries_debit_credit_total->num_rows() > 0) {
                    foreach (($register_entries_debit_credit_total->result()) as $row) {
                        $return_array['audit_entries_debit_credit_total'][] = $start_desc_text.$row->reference_no." Total Débito : ".$row->d_amount.", Total Crédito : ".$row->c_amount;
                    }
                }
            }
        }//audit_entries_debit_credit_total
        if ($data['audit_entries_pending']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $return_array['audit_entries_pending'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $entrytabla = 'entries_con'.$contabilidadSufijo;
                $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                $start_desc_text = $entrytabla."/".$etitemstabla."/";
                $register_entries_pending = $this->db
                        ->select("CONCAT({$this->db->dbprefix($ettabla)}.prefix,{$this->db->dbprefix($entrytabla)}.number) as reference_no")
                        ->join($ettabla, $ettabla.".id = ".$entrytabla.".entrytype_id", "inner")
                        ->where($entrytabla.".state", 2)
                        ->from($entrytabla)->order_by($entrytabla.'.entrytype_id ASC, '.$entrytabla.'.number ASC')->get();
                if ($register_entries_pending->num_rows() > 0) {
                    foreach (($register_entries_pending->result()) as $row) {
                        $return_array['audit_entries_pending'][] = $start_desc_text.$row->reference_no;
                    }
                }
            }
        }//audit_entries_pending
        if ($data['audit_entries_invalid_ledger']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $return_array['audit_entries_invalid_ledger'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $entrytabla = 'entries_con'.$contabilidadSufijo;
                $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                $ledger_tabla = 'ledgers_con'.$contabilidadSufijo;
                $start_desc_text = $entrytabla."/".$etitemstabla."/";
                $register_entries_invalid_ledger = $this->db
                        ->select("CONCAT({$this->db->dbprefix($ettabla)}.prefix,{$this->db->dbprefix($entrytabla)}.number) as reference_no,
                            GROUP_CONCAT({$this->db->dbprefix($etitemstabla)}.id) as entryitems_id
                            ")
                        ->join($entrytabla, $entrytabla.".id = ".$etitemstabla.".entry_id", "inner")
                        ->join($ettabla, $ettabla.".id = ".$entrytabla.".entrytype_id", "inner")
                        ->join($ledger_tabla, $ledger_tabla.".id = ".$etitemstabla.".ledger_id", "left")
                        ->where($ledger_tabla.".id IS NULL")
                        ->group_by($etitemstabla.'.entry_id')
                        ->from($etitemstabla)->order_by($entrytabla.'.entrytype_id ASC, '.$entrytabla.'.number ASC')->get();
                if ($register_entries_invalid_ledger->num_rows() > 0) {
                    foreach (($register_entries_invalid_ledger->result()) as $row) {
                        $return_array['audit_entries_invalid_ledger'][] = $start_desc_text.$row->reference_no." ID Detalles : ".$row->entryitems_id;
                    }
                }
            }
        }//audit_entries_invalid_ledger
        if ($data['audit_tax_modulary_parametrization']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $start_desc_text = "tax_rates//";
                $return_array['audit_tax_modulary_parametrization'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $acctabla = 'accounts_con'.$contabilidadSufijo;
                $register_tax_modulary_parametrization = $this->db->select('tax_rates.*')
                        ->join($acctabla, $acctabla.".id_tax = tax_rates.id", "left")
                        ->where($acctabla.".id IS NULL")
                        ->from('tax_rates')->get();
                if ($register_tax_modulary_parametrization->num_rows() > 0) {
                    foreach (($register_tax_modulary_parametrization->result()) as $row) {
                        $return_array['audit_tax_modulary_parametrization'][] = $start_desc_text.$row->name;
                    }
                }
            }
        }//audit_tax_modulary_parametrization
        if ($data['audit_payment_methods_parametrization']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $start_desc_text = "payment_methods//";
                $return_array['audit_payment_methods_parametrization'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $paymentmethodabla = 'payment_methods_con'.$contabilidadSufijo;
                $register_payment_methods_parametrization = $this->db->select('payment_methods.*')
                        ->join($paymentmethodabla, $paymentmethodabla.".type = payment_methods.code", "left")
                        ->where($paymentmethodabla.".id IS NULL")
                        ->from('payment_methods')->get();
                if ($register_payment_methods_parametrization->num_rows() > 0) {
                    foreach (($register_payment_methods_parametrization->result()) as $row) {
                        $return_array['audit_payment_methods_parametrization'][] = $start_desc_text.$row->name;
                    }
                }
            }
        }//audit_payment_methods_parametrization
        if ($data['audit_expense_categories_modulary_parametrization']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $start_desc_text = "expense_categories//";
                $return_array['audit_expense_categories_modulary_parametrization'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $ledger_tabla = 'ledgers_con'.$contabilidadSufijo;
                $register_expense_categories_modulary_parametrization = $this->db->select('
                                    expense_categories.*,
                                    IF(l1.id IS NULL, 0, 1) as ledger,
                                    IF(l2.id IS NULL, 0, 1) as creditor_ledger,
                                    IF(('.$this->db->dbprefix('tax_rates').'.rate > 0 AND l3.id IS NULL), 0, 1) as tax_ledger,
                                    IF((tax_rates2.rate > 0 AND l4.id IS NULL), 0, 1) as tax_2_ledger
                                ')
                    ->join($ledger_tabla.' l1', 'l1.id = expense_categories.ledger_id', 'left')
                    ->join($ledger_tabla.' l2', 'l2.id = expense_categories.creditor_ledger_id', 'left')
                    ->join($ledger_tabla.' l3', 'l3.id = expense_categories.tax_ledger_id', 'left')
                    ->join($ledger_tabla.' l4', 'l4.id = expense_categories.tax_2_ledger_id', 'left')
                    ->join('tax_rates', $this->db->dbprefix('tax_rates').'.id = expense_categories.tax_rate_id', 'left')
                    ->join('tax_rates tax_rates2', 'tax_rates2.id = expense_categories.tax_rate_2_id', 'left')
                    ->where('(l1.id IS NULL OR l2.id IS NULL OR ('.$this->db->dbprefix('tax_rates').'.rate > 0 AND l3.id IS NULL) OR (tax_rates2.rate > 0 AND l4.id IS NULL))')
                ->get('expense_categories');
                if ($register_expense_categories_modulary_parametrization->num_rows() > 0) {
                    foreach (($register_expense_categories_modulary_parametrization->result()) as $row) {
                        $txt_msg = "";
                        if ($row->ledger == 0) {
                            $txt_msg = "Cuenta auxiliar principal inválida";
                        }
                        if ($row->creditor_ledger == 0) {
                            $txt_msg .= ($txt_msg != "" ? ", " : "");
                            $txt_msg .= "Cuenta acreedora principal inválida";
                        }
                        if ($row->tax_ledger == 0) {
                            $txt_msg .= ($txt_msg != "" ? ", " : "");
                            $txt_msg .= "Cuenta del primer impuesto inválida";
                        }
                        if ($row->tax_2_ledger == 0) {
                            $txt_msg .= ($txt_msg != "" ? ", " : "");
                            $txt_msg .= "Cuenta del segundo impuesto inválida";
                        }
                        $return_array['audit_expense_categories_modulary_parametrization'][] = $start_desc_text.$row->name.", ".$txt_msg;
                    }
                }
            }
        }//audit_expense_categories_modulary_parametrization
        if ($data['audit_expense_categories_modulary_parametrization']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $start_desc_text = "expense_categories//";
                $return_array['audit_expense_categories_modulary_parametrization'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $ledger_tabla = 'ledgers_con'.$contabilidadSufijo;
                $register_expense_categories_modulary_parametrization = $this->db->select('
                                    expense_categories.*,
                                    IF(l1.id IS NULL, 0, 1) as ledger,
                                    IF(l2.id IS NULL, 0, 1) as creditor_ledger,
                                    IF(('.$this->db->dbprefix('tax_rates').'.rate > 0 AND l3.id IS NULL), 0, 1) as tax_ledger,
                                    IF((tax_rates2.rate > 0 AND l4.id IS NULL), 0, 1) as tax_2_ledger
                                ')
                    ->join($ledger_tabla.' l1', 'l1.id = expense_categories.ledger_id', 'left')
                    ->join($ledger_tabla.' l2', 'l2.id = expense_categories.creditor_ledger_id', 'left')
                    ->join($ledger_tabla.' l3', 'l3.id = expense_categories.tax_ledger_id', 'left')
                    ->join($ledger_tabla.' l4', 'l4.id = expense_categories.tax_2_ledger_id', 'left')
                    ->join('tax_rates', $this->db->dbprefix('tax_rates').'.id = expense_categories.tax_rate_id', 'left')
                    ->join('tax_rates tax_rates2', 'tax_rates2.id = expense_categories.tax_rate_2_id', 'left')
                    ->where('(l1.id IS NULL OR l2.id IS NULL OR ('.$this->db->dbprefix('tax_rates').'.rate > 0 AND l3.id IS NULL) OR (tax_rates2.rate > 0 AND l4.id IS NULL))')
                ->get('expense_categories');
                if ($register_expense_categories_modulary_parametrization->num_rows() > 0) {
                    foreach (($register_expense_categories_modulary_parametrization->result()) as $row) {
                        $txt_msg = "";
                        if ($row->ledger == 0) {
                            $txt_msg = "Cuenta auxiliar principal inválida";
                        }
                        if ($row->creditor_ledger == 0) {
                            $txt_msg .= ($txt_msg != "" ? ", " : "");
                            $txt_msg .= "Cuenta acreedora principal inválida";
                        }
                        if ($row->tax_ledger == 0) {
                            $txt_msg .= ($txt_msg != "" ? ", " : "");
                            $txt_msg .= "Cuenta del primer impuesto inválida";
                        }
                        if ($row->tax_2_ledger == 0) {
                            $txt_msg .= ($txt_msg != "" ? ", " : "");
                            $txt_msg .= "Cuenta del segundo impuesto inválida";
                        }
                        $return_array['audit_expense_categories_modulary_parametrization'][] = $start_desc_text.$row->name.", ".$txt_msg;
                    }
                }
            }
        }//audit_expense_categories_modulary_parametrization
        if ($data['audit_quantity_adjustments_modulary_parametrization']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $start_desc_text = "documents_types//";
                $return_array['audit_expense_categories_modulary_parametrization'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $ledger_tabla = 'ledgers_con'.$contabilidadSufijo;
                $movtype_tabla = 'movement_type_con'.$contabilidadSufijo;
                $register_quantity_adjustments_modulary_parametrization = $this->db->select('
                            IF('.$this->db->dbprefix($movtype_tabla).'.id IS NULL, 0, 1) as mov_type,
                            IF(ledger.id IS NULL, 0, 1) as ledger,
                            IF(creditor_ledger.id IS NULL, 0, 1) as creditor_ledger,
                            documents_types.*
                        ')
                        ->join($movtype_tabla, $movtype_tabla.'.document_type_id = documents_types.id', 'left')
                        ->join($ledger_tabla.' ledger', 'ledger.id = '.$movtype_tabla.'.ledger_id', 'left')
                        ->join($ledger_tabla.' creditor_ledger', 'creditor_ledger.id = '.$movtype_tabla.'.offsetting_account', 'left')
                        ->where('documents_types.module', 11)
                        ->get('documents_types');
                if ($register_quantity_adjustments_modulary_parametrization->num_rows() > 0) {
                    foreach (($register_quantity_adjustments_modulary_parametrization->result()) as $row) {
                        $txt_msg = "";
                        if ($row->mov_type == 0) {
                            $txt_msg = "El tipo de documento no está parametrizado";
                        } else {
                            if ($row->ledger == 0) {
                                $txt_msg .= ($txt_msg != "" ? ", " : "");
                                $txt_msg .= "Cuenta auxiliar principal inválida";
                            }
                            if ($row->creditor_ledger == 0) {
                                $txt_msg .= ($txt_msg != "" ? ", " : "");
                                $txt_msg .= "Cuenta acreedora principal inválida";
                            }
                        }
                        $return_array['audit_quantity_adjustments_modulary_parametrization'][] = $start_desc_text.$row->nombre." (".$row->sales_prefix.")".", ".$txt_msg;
                    }
                }
            }
        }//audit_quantity_adjustments_modulary_parametrization
        if ($data['audit_movement_type_modulary_parametrization']) {
            if($this->site->wappsiContabilidadVerificacion() > 0){
                $start_desc_text = "documents_types//";
                $return_array['audit_expense_categories_modulary_parametrization'] = [];
                $contabilidadSufijo = $this->session->userdata('accounting_module');
                $ledger_tabla = 'ledgers_con'.$contabilidadSufijo;
                $movtype_tabla = 'movement_type_con'.$contabilidadSufijo;
                $register_movement_type_modulary_parametrization = $this->db->select('
                                    '.$this->db->dbprefix($movtype_tabla).'.id,
                                    IF(ledger.id IS NULL, 0, 1) as ledger,
                                    IF(creditor_ledger.id IS NULL, 0, 1) as creditor_ledger,
                                    IF('.$this->db->dbprefix('documents_types').'.id IS NULL, 0, 1) as documents_types
                                ')
                        ->join('documents_types', 'documents_types.id = '.$movtype_tabla.'.document_type_id', 'left')
                        ->join($ledger_tabla.' ledger', 'ledger.id = '.$movtype_tabla.'.ledger_id', 'left')
                        ->join($ledger_tabla.' creditor_ledger', 'creditor_ledger.id = '.$movtype_tabla.'.offsetting_account', 'left')
                        ->where('documents_types.module !=', 11)
                        ->where('(ledger.id IS NULL OR creditor_ledger.id IS NULL OR '.$this->db->dbprefix('documents_types').'.id IS NULL)')
                        ->get($movtype_tabla);
                if ($register_movement_type_modulary_parametrization->num_rows() > 0) {
                    foreach (($register_movement_type_modulary_parametrization->result()) as $row) {
                        $txt_msg = "";
                        if ($row->documents_types == 0) {
                            $txt_msg = "El tipo de documento relacionado no existe";
                        } else {
                            if ($row->ledger == 0) {
                                $txt_msg .= ($txt_msg != "" ? ", " : "");
                                $txt_msg .= "Cuenta auxiliar principal inválida";
                            }
                            if ($row->creditor_ledger == 0) {
                                $txt_msg .= ($txt_msg != "" ? ", " : "");
                                $txt_msg .= "Cuenta acreedora inválida";
                            }
                        }
                        $return_array['audit_movement_type_modulary_parametrization'][] = $start_desc_text."ID : ".$row->id.", ".$txt_msg;
                    }
                }
            }
        }//audit_movement_type_modulary_parametrization
        $table_dont_repeat = [];
        foreach ($modules as $module => $table) {
            if (!isset($table_dont_repeat[$table])) {
                $start_desc_text = $table."//";
                if ($data['audit_header_document_type_relation']) {
                    $return_array['audit_header_document_type_relation'] = [];
                    $register_header_document_type_relation = $this->db->join('documents_types', 'documents_types.id = '.$table.'.document_type_id', 'left')
                            ->where($table.'.date >= ', $this->Settings->system_start_date.' 00:00:00')
                            ->where($table.'.date <= ', date('Y-m-d H:i:s'))
                            ->where('documents_types.id IS NULL')
                            ->get($table);
                    if ($register_header_document_type_relation->num_rows() > 0) {
                        foreach (($register_header_document_type_relation->result()) as $ridate) {
                            $return_array['audit_header_document_type_relation'][] = $start_desc_text.$ridate->reference_no;
                        }
                    }
                }//audit_header_document_type_relation
                if ($data['audit_invalid_date']) {
                    $return_array['audit_invalid_date'] = [];
                    $register_invalid_date = $this->db->where('(date <= "'.$this->Settings->system_start_date.' 00:00:00" OR date >= "'.date('Y-m-d H:i:s').'" OR date IS NULL)')
                             ->get($table);
                    if ($register_invalid_date->num_rows() > 0) {
                        foreach (($register_invalid_date->result()) as $ridate) {
                            $return_array['audit_invalid_date'][] = $start_desc_text.$ridate->reference_no;
                        }
                    }
                }//audit_invalid_date
                $table_dont_repeat[$table] = 1;
            }
        }
        return isset($return_array) ? $return_array : false;
    }

    public function getWarehouseSuggestions($term, $limit = 10)
    {
        $this->db->select("id, CONCAT(name, ' (', code, ')') as text, CONCAT(name, ' (', code, ')') as value", FALSE);
        $this->db->where(" (name LIKE '%" . $term . "%'
        OR code LIKE '%" . $term . "%') ");
        $q = $this->db->get_where('warehouses', array('status' => 1), $limit);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function check_same_id_insert($DB2, $table){
        $q = $this->db->select('(MAX(id)+1) AS new_id')->get($table);
        if ($q->num_rows() > 0) {
            $new_id = $q->row()->new_id;
        } else {
            $new_id = 1;
        }
        if ($DB2) {
            $q2 = $DB2->get_where($table, ['id'=>$new_id]);
            if ($q2->num_rows() > 0) {
                return false;
            }
        }
        return true;
    }

    public function get_new_year_new_id_for_insertion($DB2, $table){
        $new_id = 1;
        if ($DB2) {
            $q = $DB2->select('(MAX(id)+1) AS new_id')->get($table);
            if ($q->num_rows() > 0) {
                $new_id = $q->row()->new_id;
            }
        }
        return $new_id;
    }

    public function get_products_price_group_by_id($pg_id){
        $q = $this->db->get_where('product_prices', ['price_group_id'=>$pg_id]);
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function syncNewYearSalePayments($id) {
        $DB2 = $this->connect_new_year_database();
        if ($DB2) {
            $sale = $DB2->get_where('sales', array('id' => $id), 1);
            if ($sale->num_rows() > 0) {
                $sale = $sale->row();
            }
            $payments = $DB2->order_by('date asc')->get_where('payments', array('sale_id' => $id));
            if ($payments->num_rows() > 0) {
                $payments = $payments->result();
            } else {
                $payments = false;
            }
            if ($payments) {
                $paid = 0;
                $grand_total = $sale->grand_total+$sale->rounding;
                $last_date = NULL;
                foreach ($payments as $payment) {
                    $paid += $payment->amount;
                    $last_date = $payment->date;
                }
                $payment_status = $paid == 0 ? 'pending' : $sale->payment_status;
                if ($grand_total == $this->sma->formatDecimal($paid)) {
                    $payment_status = 'paid';
                } elseif ($paid == 0 && $sale->due_date != "" && $sale->due_date <= date('Y-m-d') && !$sale->sale_id) {
                    $payment_status = 'pending';
                } elseif ($paid != 0) {
                    $payment_status = 'partial';
                }

                if ($paid > 0 && ($grand_total - $paid) < 0.1) {
                    $pmnt = $DB2->get_where('payments', ['sale_id' => $id,
                                                        'paid_by != ' => 'discount',
                                                        'paid_by != ' => 'retencion',
                                                        'paid_by != ' => 'return',
                                                        ], 1);
                    if ($pmnt->num_rows() > 0) {
                        $pmnt = $pmnt->row();
                        $DB2->update('payments', ['amount' => $pmnt->amount+($grand_total - $paid)], ['id'=>$pmnt->id]);
                    }
                }
                if ($payment_status != 'paid') {
                    $last_date = NULL;
                }
                if ($DB2->update('sales', array('paid' => $paid, 'payment_status' => $payment_status, 'portfolio_end_date' => $last_date), array('id' => $id))) {
                    return true;
                }
            } else {
                $payment_status = ($sale->due_date <= date('Y-m-d')) ? 'due' : 'pending';
                if ($DB2->update('sales', array('paid' => 0, 'payment_status' => $payment_status), array('id' => $id))) {
                    return true;
                }
            }
        }
        return FALSE;
    }

    public function syncNewYearPurchasePayments($id) {
        $DB2 = $this->connect_new_year_database();
        if ($DB2) {
            $purchase = $DB2->get_where('purchases', array('id' => $id), 1);
            if ($purchase->num_rows() > 0) {
                $purchase = $purchase->row();
            }
            $payments = $DB2->get_where('payments', array('purchase_id' => $id));
            if ($payments->num_rows() > 0) {
                $payments = $payments->result();
            } else {
                $payments = false;
            }

            $paid = 0;
            if ($payments) {
                foreach ($payments as $payment) {
                    $paid += $payment->amount;
                }
            }
            $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
            if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
                $payment_status = 'partial';
            } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
                $payment_status = 'paid';
            }
            if ($DB2->update('purchases', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
                return true;
            }
        }
        return FALSE;
    }

    public function getAllIntegrations()
    {
        $this->db->where('status', ACTIVE);
        $q = $this->db->get('integrations');
        return $q->result();
    }

    public function getInstance($data, $id = null)
    {
        if (!empty($id)) {
            $this->db->where("id != $id");
        }
        $this->db->where($data);
        $q = $this->db->get("electronic_billing_instances");
        return $q->row();
    }

    public function getAllInstances()
    {
        $q = $this->db->get("electronic_billing_instances");
        return $q->result();
    }

    public function createInstance($data)
    {
        if ($this->db->insert('electronic_billing_instances', $data)) {
            return true;
        }
        return false;
    }

    public function updtateInstance($data, $id)
    {
        $this->db->where('id', $id);
        if ($this->db->update('electronic_billing_instances', $data)) {
            return true;
        }
        return false;
    }

    public function getInstanceDocumentType($data, $modules)
    {
        $this->db->where_in("module", $modules);
        $this->db->where("electronic_billing_instance_id", $data["instance"]);
        $q = $this->db->get("documents_types");
        return $q->row();
    }

    public function findColor($data, $id = null)
    {
        if (!empty($id)) {
            $this->db->where("id != $id");
        }
        $this->db->where($data);
        $q = $this->db->get("colors");
        return $q->row();
    }

    public function createColor($data) {
        if ($this->db->insert("colors", $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function updateColor($data, $id) {
        $this->db->where("id", $id);
        if ($this->db->update("colors", $data)) {
            return true;
        }
        return false;
    }

    public function findMaterial($data, $id = null)
    {
        if (!empty($id)) {
            $this->db->where("id != $id");
        }
        $this->db->where($data);
        $q = $this->db->get("materials");
        return $q->row();
    }

    public function createMaterial($data) 
    {
        if ($this->db->insert("materials", $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function updateMaterial($data, $id) 
    {
        $this->db->where("id", $id);
        if ($this->db->update("materials", $data)) {
            return true;
        }
        return false;
    }

    public function findTag($data, $id = null)
    {
        if (!empty($id)) {
            $this->db->where("id != $id");
        }
        $this->db->where($data);
        $q = $this->db->get("tags");
        return $q->row();
    }

    function createTag($data) 
    {
        if ($this->db->insert("tags", $data)) {
            return true;
        }
        return false;
    }

    public function updateTag($data, $id) 
    {
        $this->db->where("id", $id);
        if ($this->db->update("tags", $data)) {
            return true;
        }
        return false;
    }

    public function validateColorAssigned($id) {
        $q = $this->db->get_where('products', ['color_id'=>$id]);
        if ($q->num_rows() > 0) {
            return false;
        }
        return true;
    }

    public function deleteColor($id) {
        if ($this->db->delete("colors", ["id" => $id])) {
            return true;
        }
        return false;
    }

    public function findSubzone($data)
    {
        $this->db->where($data);
        $q = $this->db->get("subzones");
        return $q->row();
    }

    public function deleteSubzone($filter)
    {
        $this->db->where($filter);
        if ($this->db->delete("subzones")) {
            return true;
        }
        return false;
    }

    public function findZone($data)
    {
        $this->db->where($data);
        $q = $this->db->get("zones");
        return $q->row();
    }

    public function deleteZone($filter)
    {
        $this->db->where($filter);
        if ($this->db->delete("zones")) {
            return true;
        }
        return false;
    }
    
    public function findCity($data)
    {
        $this->db->where($data);
        $q = $this->db->get("cities");
        return $q->row();
    }

    public function updateCity($data, $id)
    {
        $this->db->where("CODIGO", $id);
        if ($this->db->update("cities", $data)) {
            return true;
        }
        return false;
    }

    public function updateCities($data, $filter)
    {
        $this->db->where($filter);
        if ($this->db->update("cities", $data)) {
            return true;
        }
        return false;
    }

    public function deleteCity($filter)
    {
        $this->db->where($filter);
        if ($this->db->delete("cities")) {
            return true;
        }
        return false;
    }


    public function updateState($data, $id)
    {
        $this->db->where("CODDEPARTAMENTO", $id);
        if ($this->db->update("states", $data)) {
            return true;
        }
        return false;
    }

    public function findState($data)
    {
        $this->db->where($data);
        $q = $this->db->get("states");
        return $q->row();
    }

    public function updateStates($data, $filter)
    {
        $this->db->where($filter);
        if ($this->db->update("states", $data)) {
            return true;
        }
        return false;
    }

    public function deleteState($filter)
    {
        $this->db->where($filter);
        if ($this->db->delete("states")) {
            return true;
        }
    }

    public function findCountry($data)
    {
        $this->db->where($data);
        $q = $this->db->get("countries");
        return $q->row();
    }

    public function updateCountry($data, $id)
    {
        $this->db->where("CODIGO", $id);
        if ($this->db->update("countries", $data)) {
            return true;
        }
        return false;
    }

    public function deleteCountry($filter)
    {
        $this->db->where($filter);
        if ($this->db->delete("countries")) {
            return true;
        }
    }
}