var purchases = {};
var expenses = {};
var subtotal_expenses = 0;
var subtotal_purchases = 0;
var import_detail_delete = [];

if (is_edit) {

	if (purchases = JSON.parse(localStorage.getItem('impurchases'))) {
		$.each(purchases, function(data_index, data_row){
			subtotal_purchases += formatDecimal(data_row.grand_sub_total);
		});
	}
	if (expenses = JSON.parse(localStorage.getItem('imexpenses'))) {
		$.each(expenses, function(data_index, data_row){
			subtotal_expenses += formatDecimal(data_row.grand_sub_total);
		});
	}

}
$(document).ready(function(){
	$('#imtag').select2({
        formatResult : tag_option_color,
        formatSelection : tag_option_color
      });
	$(document).on('change', '#imbiller', function (e) {
		localStorage.setItem('imbiller', $(this).val());
        var document_type = 54;
        $.ajax({
          url:site.base_url+"billers/getBillersDocumentTypes/"+document_type+'/'+$('#imbiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;
          $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });
        localStorage.setItem('imbiller', $('#imbiller').val());
    });
	$(document).on('change', '#imtag', function(){
		localStorage.removeItem('impurchases');
		localStorage.removeItem('imexpenses');
		purchases = {};
		expenses = {};
		localStorage.setItem('imtag', $(this).val());
		var tag = $(this).val();
		$.ajax({
			url : site.base_url+"purchases/get_tagged_purchases_pending",
			type : 'POST',
			dataType : 'JSON',
			data : {
				tag_id : tag,
				biller_id : $('#imbiller').val(),
			}
		}).done(function(data){
			if (data) {
	            if (Object.keys(data.purchases).length > 0) {
	            	$.each(data.purchases, function(index, data_row){
						purchases[index] = data_row;
						subtotal_purchases += formatDecimal(data_row.grand_sub_total);
					});
	            }
	            if (Object.keys(data.expenses).length > 0) {
	            	$.each(data.expenses, function(index, data_row){
						expenses[index] = data_row;
						subtotal_expenses += formatDecimal(data_row.grand_sub_total);
					});
	            }
				setTimeout(function() {
					localStorage.setItem('impurchases', JSON.stringify(purchases));
					localStorage.setItem('imexpenses', JSON.stringify(expenses));
					print_purchases();
				}, 850);
			} else {
				print_purchases();
			}
		});
	});
	$(document).on('change', '#impurchase, #imexpense', function(){
		add_inv($(this).val());
		nsPurchase();
		nsExpense();
	});
	$(document).on('change', '#imstatus', function(){
		localStorage.setItem('imstatus', $(this).val());
	});
    $(document).on('click', '#reset', function(){
    	reset_ls();
    });
    $(document).on('click', '#submit_import', function(){
    	if ($('#submit_import_form').valid()) {
    		$('#submit_import_form').submit();
    	}
    });
    $(document).on('click', '.delete_row', function(){
    	row = $(this);
    	id = row.data('id');
    	var ref_arr = id.split('_');
    	rowtype = row.data('rowtype');

    	bootbox.confirm({
            message: "¿Está segur@ de eliminar "+(rowtype == 1 ? "la compra":"el gasto")+" <b>"+ref_arr[1]+"</b>?",
            buttons: {
                confirm: {
                    label: 'Eliminar',
                    className: 'btn-danger send-submit-sale btn-full'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-default btn-full'
                }
            },
            callback: function (result) {
                if (result) {
                    purchases = JSON.parse(localStorage.getItem('impurchases'));
			    	expenses = JSON.parse(localStorage.getItem('imexpenses'));
			    	if (rowtype == 1) {
                		pto_delete = purchases[id];
			    		delete purchases[id];
						subtotal_purchases -= formatDecimal(pto_delete.grand_sub_total);
			    	} else {
                		pto_delete = expenses[id];
			    		delete expenses[id];
						subtotal_expenses -= formatDecimal(pto_delete.grand_sub_total);
			    	}
			    	$.each(pto_delete.rows, function(index, row){
			    		if (import_detail_delete.includes(row.import_detail_id)) {
					        return false;
					    } else {
					        import_detail_delete.push(row.import_detail_id);
					    }
			    	});
					localStorage.setItem('impurchases', JSON.stringify(purchases));
					localStorage.setItem('imexpenses', JSON.stringify(expenses));
					print_purchases();
					if (is_edit) {
						$('#deleted_detail').val(JSON.stringify(import_detail_delete));
					}
                }
            }
        });	
    });
	$(document).on('change', '#imsupplier', function(){
		localStorage.setItem('imsupplier', $(this).val());
	});
	nsPurchase();
	nsExpense();
	if (imbiller = localStorage.getItem('imbiller')) {
		$('#imbiller').select2('val', imbiller).trigger('change');
	} else {
		$('#imbiller').trigger('change');
	}
	if (imtag = localStorage.getItem('imtag')) {
		$('#imtag').select2('val', imtag);
		if (!is_edit) {
			$('#imtag').trigger('change');
		} else {
			print_purchases();
		}
	}
	if (imstatus = localStorage.getItem('imstatus')) {
		$('#imstatus').select2('val', imstatus).trigger('change');
	}
	if (imsupplier = localStorage.getItem('imsupplier')) {
        $('#imsupplier').val(imsupplier).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getSupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    } else {
        nsSupplier();
    }
});

function nsPurchase() {
    $('#impurchase').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "purchases/purchase_import_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    biller_id: $('#imbiller').val(),
                    ptype: 1,
                    limit: 10,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
function nsExpense() {
    $('#imexpense').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "purchases/purchase_import_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    biller_id: $('#imbiller').val(),
                    ptype: 3,
                    limit: 10,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}
function add_inv(id){
	purchases = JSON.parse(localStorage.getItem('impurchases'));
	expenses = JSON.parse(localStorage.getItem('imexpenses'));
	$.ajax({
		url : site.base_url+"purchases/get_tagged_purchases_pending",
		type : 'POST',
		dataType : 'JSON',
		data : {
			purchase_id : id,
			biller_id : $('#imbiller').val(),
		}
	}).done(function(data){
		$.each(data.purchases, function(index, data_row){
			purchases[index] = data_row;
			subtotal_purchases += formatDecimal(data_row.grand_sub_total);
		});
		$.each(data.expenses, function(index, data_row){
			expenses[index] = data_row;
			subtotal_expenses += formatDecimal(data_row.grand_sub_total);
		});
		localStorage.setItem('impurchases', JSON.stringify(purchases));
		localStorage.setItem('imexpenses', JSON.stringify(expenses));
		print_purchases();
	});
}

function print_purchases(){
	$('#accordion_purchases').empty();
	$('#accordion_expenses').empty();
	if (purchases = JSON.parse(localStorage.getItem('impurchases'))) {
		$.each(purchases, function(data_index, data_row){
			panel_id = data_index;
			var ref_arr = panel_id.split('_');
			panel_title = "<table class='table' data-toggle='collapse' data-parent='#accordion' href='#collapse" + panel_id + "'>"+
								"<tr>"+
									"<th style='width:14.66%;'>"+lang.date+" : <br>"+data_row.date+"</th>"+
								  	"<th style='width:14.66%;'>"+lang.reference_no+" : <br>"+ref_arr[1]+"</th>"+
								  	"<th style='width:31.66%;'>"+lang.supplier+" : <br>"+data_row.supplier+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.subtotal+" : <br>"+formatMoney(data_row.grand_sub_total)+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.shipping+" : <br>"+formatMoney(data_row.prorated_shipping_cost > 0 ? data_row.prorated_shipping_cost : data_row.shipping)+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.tax+" : <br>"+formatMoney(data_row.total_tax)+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.grand_total+" : <br>"+formatMoney(data_row.grand_total)+"</th>"+
								  	"<th style='width:4%; cursor:pointer;' class='delete_row text-danger' data-id='"+panel_id+"' data-rowtype='1'><i class='fa-solid fa-trash-can'></i></th>"+
							  	"</tr>"+
						  	"</table>"
						  ;

			var html = '<div class="panel panel-default">';
	            html += '<div class="panel-heading">';
	                html += '<span class="panel-title">';
	                
		                	html += panel_title;
		                	
	                html += '</span>';
	            html += '</div>';
	            html += '<div id="collapse' + panel_id + '" class="panel-collapse collapse">';
		            html += '<div class="panel-body">';
			            html+="<table class='table'>"+
			            		"<tr>"+
			            			"<th>"+lang.product_code+"</th>"+
			            			"<th>"+lang.product_name+"</th>"+
			            			"<th>"+lang.unit_cost+"</th>"+
			            			"<th>"+lang.purchase_shipping+"</th>"+
			            			"<th>"+lang.net_unit_cost+"</th>"+
			            			"<th>"+lang.quantity+"</th>"+
			            			"<th>"+lang.price_to_adjust+"</th>"+
			            		"</tr>";
			            
							$(data_row.rows).each(function(item_index, item){
								cost_to_adjust = pcalculateShipping(subtotal_expenses, item.net_unit_cost, subtotal_purchases, item.quantity);
								html+="<tr>"+
										"<input type='hidden' name='import_detail_id[]' value='"+(item.import_detail_id !== undefined ? item.import_detail_id : null)+"'>"+
										"<input type='hidden' name='product_id[]' value='"+item.product_id+"'>"+
										"<input type='hidden' name='purchase_item_id[]' value='"+item.id+"'>"+
										"<input type='hidden' name='purchase_id[]' value='"+item.purchase_id+"'>"+
										"<input type='hidden' name='quantity[]' value='"+item.quantity+"'>"+
										"<input type='hidden' name='net_unit_cost[]' value='"+item.net_unit_cost+"'>"+
										"<input type='hidden' name='adjustment_cost[]' value='"+cost_to_adjust+"'>"+
				            			"<td>"+item.product_code+"</td>"+
				            			"<td>"+item.product_name+"</td>"+
				            			"<td class='text-right'>"+formatMoney(item.unit_cost)+"</td>"+
				            			"<td class='text-right'>"+formatMoney(item.shipping_unit_cost)+"</td>"+
				            			"<td class='text-right'>"+formatMoney(item.net_unit_cost)+"</td>"+
				            			"<td>"+formatQuantity(item.quantity)+"</td>"+
				            			"<td>"+formatMoney(cost_to_adjust * item.quantity)+"</td>"+
				            		"</tr>";
							});
		            	html+="</table>";
	                html += '</div>';
	            html += '</div>';
	        html += '</div>';
	        $('#accordion_purchases').append(html);
		});
	}
	if (expenses = JSON.parse(localStorage.getItem('imexpenses'))) {
		if (Object.keys(expenses).length == 0) {
			$('#submit_import').prop('disabled', true);
			$('.error_expenses').fadeIn();
		} else {
			$('#submit_import').prop('disabled', false);
			$('.error_expenses').fadeOut();
		}
		$.each(expenses, function(data_index, data_row){
			panel_id = data_index;
			var ref_arr = panel_id.split('_');
			panel_title = "<table class='table' data-toggle='collapse' data-parent='#accordion' href='#collapse" + panel_id + "'>"+
								"<tr>"+
									"<th style='width:14.66%;'>"+lang.date+" : "+data_row.date+"</th>"+
								  	"<th style='width:14.66%;'>"+lang.reference_no+" : "+ref_arr[1]+"</th>"+
								  	"<th style='width:31.66%;'>"+lang.supplier+" : "+data_row.supplier+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.subtotal+" : "+formatMoney(data_row.grand_sub_total)+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.shipping+" : "+formatMoney(data_row.shipping)+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.tax+" : "+formatMoney(data_row.total_tax)+"</th>"+
								  	"<th style='width:8.75%;'>"+lang.grand_total+" : "+formatMoney(data_row.grand_total)+"</th>"+
								  	"<th style='width:4%; cursor:pointer;' class='delete_row text-danger' data-id='"+panel_id+"' data-rowtype='2'><i class='fa-solid fa-trash-can'></i></th>"+
							  	"</tr>"+
						  	"</table>"
						  ;

			var html = '<div class="panel panel-default">';
	            html += '<div class="panel-heading">';
	                html += '<span class="panel-title">';
	                
		                	html += panel_title;
		                	
	                html += '</span>';
	            html += '</div>';
	            html += '<div id="collapse' + panel_id + '" class="panel-collapse collapse">';
		            html += '<div class="panel-body">';
			            html+="<table class='table'>"+
			            		"<tr>"+
			            			"<th>"+lang.product_code+"</th>"+
			            			"<th>"+lang.product_name+"</th>"+
			            			"<th>"+lang.unit_cost+"</th>"+
			            			"<th>"+lang.net_unit_cost+"</th>"+
			            			"<th>"+lang.quantity+"</th>"+
			            			// "<th>"+lang.price_to_adjust+"</th>"+
			            		"</tr>";
			            
							$(data_row.rows).each(function(item_index, item){
								html+="<tr>"+
										"<input type='hidden' name='import_detail_id[]' value='"+(item.import_detail_id !== undefined ? item.import_detail_id : null)+"'>"+
										"<input type='hidden' name='product_id[]' value='"+item.product_id+"'>"+
										"<input type='hidden' name='purchase_item_id[]' value='"+item.id+"'>"+
										"<input type='hidden' name='purchase_id[]' value='"+item.purchase_id+"'>"+
										"<input type='hidden' name='quantity[]' value='"+item.quantity+"'>"+
										"<input type='hidden' name='net_unit_cost[]' value='"+item.net_unit_cost+"'>"+
										"<input type='hidden' name='adjustment_cost[]' value='0'>"+
				            			"<td>"+item.product_code+"</td>"+
				            			"<td>"+item.product_name+"</td>"+
				            			"<td class='text-right'>"+formatMoney(item.unit_cost)+"</td>"+
				            			"<td class='text-right'>"+formatMoney(item.net_unit_cost)+"</td>"+
				            			"<td>"+formatQuantity(item.quantity)+"</td>"+
				            			// "<td>0</td>"+
				            		"</tr>";
							});
		            	html+="</table>";
	                html += '</div>';
	            html += '</div>';
	        html += '</div>';
	        $('#accordion_expenses').append(html);
		});
	} else {
		$('#submit_import').prop('disabled', true);
		$('.error_expenses').fadeIn();
	}
}
function reset_ls(){
	localStorage.removeItem('imtag');
	localStorage.removeItem('imsupplier');
	localStorage.removeItem('imbiller');
	localStorage.removeItem('impurchases');
	localStorage.removeItem('imexpenses');
	localStorage.removeItem('imstatus');
	location.reload();
}

function nsSupplier() {
    $('#imsupplier').select2({
        minimumInputLength: 1,
        ajax: {
            url: site.base_url + "suppliers/suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}