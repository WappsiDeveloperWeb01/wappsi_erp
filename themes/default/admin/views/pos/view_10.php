<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if ($modal) { ?>

<style type="text/css">
    @media print {

      #wrapper {
        padding: 0px !important;
        margin: 0px !important;
      }
      #receiptData {
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-body{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-content{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-dialog{
        padding: 0px !important;
        margin: 0px !important;
      }

    }


</style>

<div class="modal-dialog no-modal-header" role="document"><div class="modal-content"><div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
    <?php
} else {
    ?><!doctype html>
    <html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . " " . lang("no") . " " . $inv->id;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; font-size: 110% !important}
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 10px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            /*h3 { margin: 5px 0; }*/
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            table { width: 100% ; }
            h4, p, span, table, table > tbody > tr > td, table > tfoot > tr > th, table > thead > tr > th {
                padding: 0px !important;
                margin: 0px !important;
            }
            @media print {
                #wrapper { padding-top: 0px !important; padding-bottom: 5px !important; }
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                /*.border-bottom { border-bottom: 1px solid #ddd !important; }*/
                /*table tfoot { display: table-row-group; }*/
                .rectangular_logo{
                    width: 160px !important;
                    max-height: 150px !important;
                    margin-bottom: 4px !important;
                }
            }
            .rectangular_logo{
                max-width: 130px;
                max-height: 110px;
            }
            <?php if ($product_detail_font_size != 0): ?>
                table > tbody > tr > td {
                    font-size: <?= 100 + ($product_detail_font_size * 10) ?>% !important;
                }
            <?php endif ?>
        </style>
    </head>

    <body>
        <?php
    } ?>
    <div id="wrapper">
        <div id="receiptData">
            <div class="no-print">
                <?php
                if ($message) {
                    ?>
                    <div class="alert alert-success">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?=is_array($message) ? print_r($message, true) : $message;?>
                    </div>
                    <?php
                } ?>
                <?php if ($this->session->userdata('swal_flash_message')): ?>
                    <?php $swal_msg = $this->session->userdata('swal_flash_message') ?>
                    <div class="alert alert-danger">
                        <button data-dismiss="alert" class="close" type="button">×</button>
                        <?= $swal_msg; ?>
                    </div>
                    <?php $this->session->unset_userdata('swal_flash_message') ?>
                <?php endif ?>
            </div>
            <div>
                <div style="text-align:center;">
                    <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                    <h4 style="text-transform:uppercase; font-size: 130%;"><?=$biller->name;?></h4>
                    <?php
                    echo "NIT : ".$biller->vat_no.($biller->digito_verificacion != "" ? '-'.$biller->digito_verificacion : '')." ".$tipo_regimen;
                    if ($ciiu_code) {
                        echo "</br> Códigos CIIU : ".$ciiu_code."</br>";
                    }
                    echo "<p>" . $biller->address ." - " .ucwords(mb_strtolower($biller->city)).", ".ucwords(mb_strtolower($biller->country))."<br>" .
                    lang("tel") . ": " . $biller->phone . ", Correo: ".$biller->email."<br>";
                    if ($this->pos_settings->cf_title1 != "" && $this->pos_settings->cf_value1 != "") {
                        echo $this->pos_settings->cf_title1 . ": " . $this->pos_settings->cf_value1 . "<br>";
                    }
                    if ($this->pos_settings->cf_title2 != "" && $this->pos_settings->cf_value2 != "") {
                        echo $this->pos_settings->cf_title2 . ": " . $this->pos_settings->cf_value2 . "<br>";
                    }
                    echo '</p>';
                    ?>
                    <?php
                    if ($this->Settings->url_web != "") {
                        echo $this->Settings->url_web."<br>";
                    } ?>
                </div>
                <?php
                if ($Settings->invoice_view == 1 || $Settings->indian_gst) {
                    ?>
                    <div class="col-sm-12 text-center" style="margin: 4px 0px 7px 0px !important">
                        <?php if ($inv->sale_status == 'returned'): ?>
                            <h4 style="font-weight:bold; font-size: 140%;">Devolución de venta <?= $inv->reference_no ?></h4>
                        <?php else: ?>
                            <h4 style="font-weight:bold; font-size: 140%;"><?=  ($document_type ? $document_type->nombre : lang('sale_invoice'))." ".$inv->reference_no;?></h4>
                        <?php endif ?>
                    </div>
                    <?php
                }
                echo "<p><b>" .lang("date") . ":</b> " . $this->sma->hrld($inv->date) . "<br>";
                if ($inv->payment_term > 0 && $inv->payment_status != 'paid') {
                    $fecha_exp = strtotime("+".$inv->payment_term." day", strtotime($inv->date));
                    $fecha_exp = date('d/m/Y', $fecha_exp);
                    echo "<b>".lang("expiration_date")." :</b> ".$fecha_exp." <br>";
                }
                if (!empty($inv->return_sale_ref)) {
                    echo '<p>'.lang("return_ref").': '.$inv->return_sale_ref;
                    if ($inv->return_id) {
                        echo ' <a data-target="#myModal2" data-toggle="modal" href="'.admin_url('sales/modal_view/'.$inv->return_id).'"><i class="fa fa-external-link no-print"></i></a><br>';
                    } else {
                        echo '</p>';
                    }
                }
                // echo lang("sales_person") . ": " . $created_by->first_name." ".$created_by->last_name . "</p>";
                echo "<p><b>";
                echo lang("customer") . ":</b> " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name) . "<br>";
                if ($this->pos_settings->customer_details && $customer->vat_no != "222222222222") {
                    if ($customer->vat_no != "-") {

                        if ($customer->tipo_documento == 6) {
                            echo  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion." <br>";
                        } else {
                            echo  lang("vat_no") . ": " . $customer->vat_no." <br>";
                        }

                    }
                    if ($address) {
                        echo lang("tel") . ": " . $address->phone . "<br>";
                        echo lang("address") . ": " . $address->direccion . "<br>";
                        echo $address->city ." ".$address->state." ".$address->country ."<br>";
                    } else {
                        echo lang("tel") . ": " . $customer->phone . "<br>";
                        echo lang("address") . ": " . $customer->address . "<br>";
                        echo $customer->city ." ".$customer->state." ".$customer->country ."<br>";
                    }
                        echo $customer->email ."<br>";
                }
                echo "</p>";
                ?>
                <?php if ($seller): ?>
                    <p><b><?= lang('seller') ?> :</b> <?= $seller->company != '-' ? $seller->company : $seller->name; ?></p>
                <?php endif ?>
                <?php if (!empty($inv->sale_origin_reference_no)): ?>
                    <p><b><?= lang('reference_origin') ?> :</b> <?= $inv->sale_origin_reference_no?></p>
                <?php endif ?>

                <?php
                if ($inv->restobar_table_id) {
                    echo "<p><b>".lang("table_number") . ":</b> " . $inv->restobar_table_id . "</p>";
                }
                 ?>
                <?php if (isset($cost_center) && $cost_center): ?>
                    <div>
                        <label><?= lang('cost_center') ?></label>
                        <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                    </div>
                <?php endif ?>
                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 60%">Nombre</th>
                            <th style="text-align: center;">Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $r = 1; $category = 0;
                        $brand = 0;
                        $tax_summary = array();
                        $ccategories = [];
                        $inv_total_before_promo = 0;
                        foreach ($rows as $row) {
                            if ($product_detail_promo == 1 && $row->price_before_promo > 0) {
                                $inv_total_before_promo = ($row->price_before_promo - ($row->tax_method == 1 ? $row->net_unit_price : $row->unit_price)) * $row->quantity;
                            }
                            if (!isset($ccategories[$row->category_id])) {
                                $ccategories[$row->category_id] = $row->subtotal;
                            } else {
                                $ccategories[$row->category_id] += $row->subtotal;
                            }

                            if ($this->pos_settings->item_order == 1 && $category != $row->category_id) {
                                $category = $row->category_id;
                                echo '<tr>
                                        <td colspan="100%" class="no-border">
                                            <strong>'.$row->category_name.'</strong>
                                        </td>
                                    </tr>';
                            }

                            // Condición que clasifica los productos por marcas
                            if ($this->pos_settings->item_order == "2" && $brand != $row->brand_id) {
                                $brand = $row->brand_id;
                                echo '<tr>
                                        <td colspan="100%" class="no-border">
                                            <strong>'.$row->brand_name.'</strong>
                                        </td>
                                    </tr>';
                            }

                            echo '<tr>
                                    <td class="no-border" style="padding-bottom: 0px !important;">
                                        '.($row->product_name) .
                                        ($row->variant && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? ' (' . $row->variant . ')' : '') .
                                        ($show_product_preferences == 1 && $row->preferences ? ' (' . $this->sma->print_preference_selection($row->preferences) . ')' : '') .
                                        ($this->Settings->show_brand_in_product_search ? ' - '.$row->brand_name : '') .
                                        ($row->serial_no != '' ? ' - '.$row->serial_no : '') .
                                        ($document_type_invoice_format && ($document_type_invoice_format->product_detail_discount) && $row->item_discount > 0 ? ", ".lang('product_discount_included')." (-".$row->discount.")" : "").
                                        ($product_detail_promo == 1 && $row->price_before_promo > 0 ? " (".lang('discount_by_promo')." -".$this->sma->formatDecimal((($row->price_before_promo - ($row->tax_method == 1 ? $row->net_unit_price : $row->unit_price)) / $row->price_before_promo) * 100, 2)." %)" : '' ).
                                        ($row->under_cost_authorized == 1 ? ' (<i class="fa fa-unlock-alt"></i>)' : '').
                                        '
                                    </td>
                                    <td><span class="pull-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '</span></td>
                                </tr>';
                            if (!empty($row->second_name)) {
                                echo '<tr>
                                        <td colspan="2" class="no-border">'.$row->second_name.'</td>
                                    </tr>';
                            }
                            echo '<tr>
                                    <td class="no-border border-bottom" style="padding-top: 0px !important;">' . $this->sma->formatQuantity($row->quantity, $qty_decimals) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).' '.($this->Settings->ipoconsumo && $row->tax_2 > 0 ? '+ '.sprintf(lang('ipoconsumo_additional_description_name'), $this->sma->formatMoney($row->tax_2)) : '').'
                                    </td>
                                    <td class="no-border border-bottom text-right" style="padding-top: 0px !important;">' . (!isset($document_type_invoice_format->view_item_tax) || (isset($document_type_invoice_format->view_item_tax) && $document_type_invoice_format->view_item_tax && $row->tax_code) ? '*'.$row->tax_code : '') . '
                                    </td>
                                </tr>';

                            $r++;
                        }
                        if (($document_type_invoice_format && $document_type_invoice_format->show_return_detail == 1) && $return_rows) {
                            echo '<tr class="warning">
                                    <td colspan="100%" class="no-border"><strong>'.lang('returned_items').'</strong></td>
                                  </tr>';
                            foreach ($return_rows as $row) {
                                if ($this->pos_settings->item_order == 1 && $category != $row->category_id) {
                                    $category = $row->category_id;
                                    echo '<tr>
                                            <td colspan="100%" class="no-border"><strong>'.$row->category_name.'</strong></td>
                                         </tr>';
                                }
                                echo '<tr>
                                        <td colspan="2" class="no-border">#' . $r . ': &nbsp;&nbsp;' . $row->product_name, ($printer ? $printer->char_per_line : null) . ($row->variant && ($document_type_invoice_format && $document_type_invoice_format->show_product_variants == 1) ? ' (' . $row->variant . ')' : '') . '<span class="pull-right">' . ($row->tax_code ? '*'.$row->tax_code : '') . '</span></td>
                                        </tr>';
                                echo '<tr>
                                        <td class="no-border border-bottom">' . $this->sma->formatQuantity($row->quantity, $qty_decimals) . ' x '.$this->sma->formatValue($value_decimals, $row->unit_price).($row->item_tax != 0 ? ' - '.lang('tax').' <small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small> '.$this->sma->formatValue($value_decimals, $row->item_tax).' ('.lang('hsn_code').': '.$row->hsn_code.')' : '').' '.($this->Settings->ipoconsumo && $row->tax_2 > 0 ? '+ '.sprintf(lang('ipoconsumo_additional_description_name'), $this->sma->formatMoney($row->tax_2)) : '').'
                                        </td>
                                        <td class="no-border border-bottom text-right">' . $this->sma->formatValue($value_decimals, $row->subtotal) . '</td>
                                    </tr>';
                                $r++;
                            }
                        }

                        ?>
                    </tbody>
                    <tfoot>
                        <?php
                            $rete_total = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total + ($inv->rete_bomberil_id && $inv->rete_bomberil_total > 0 ? $inv->rete_bomberil_total : 0) + ($inv->rete_autoaviso_id && $inv->rete_autoaviso_total > 0 ? $inv->rete_autoaviso_total : 0);
                        ?>
                        <?php if ($rete_total != 0 || $inv->order_discount != 0): ?>
                            <tr>
                                <th><?=lang("total_before_discount");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->total + $inv->product_tax)+($return_sale->total + $return_sale->product_tax)) : ($inv->total + $inv->product_tax));?></th>
                            </tr>
                        <?php endif ?>
                        <?php
                        if ($inv->order_tax != 0) {
                            echo '<tr><th>' . lang("tax") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $return_sale ? ($inv->order_tax+$return_sale->order_tax) : $inv->order_tax) . '</th></tr>';
                        }

                        if ($rete_total != 0) {
                            echo '<tr>
                                    <th>'.lang('retention').'</th>
                                    <th class="text-right"> -' . $this->sma->formatValue($value_decimals, $rete_total) . '</th>
                                  </tr>';
                        }

                        if ($inv->order_discount != 0) {
                            echo '<tr><th>' . lang("order_discount") . '</th><th class="text-right"> -' . $this->sma->formatValue($value_decimals, $inv->order_discount) . '</th></tr>';
                        }

                        if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 1) {
                            echo '<tr><th>' . lang("shipping") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->shipping) . '</th></tr>';
                        }

                        if ($return_sale) {
                            if ($return_sale->surcharge != 0) {
                                echo '<tr><th>' . lang("order_discount") . '</th><th class="text-right">' . $this->sma->formatValue($value_decimals, $return_sale->surcharge) . '</th></tr>';
                            }
                        }

                        if ($Settings->indian_gst) {
                            if ($inv->cgst > 0) {
                                $cgst = $return_sale ? $inv->cgst + $return_sale->cgst : $inv->cgst;
                                echo '<tr><td>' . lang('cgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $cgst) : $cgst) . '</td></tr>';
                            }
                            if ($inv->sgst > 0) {
                                $sgst = $return_sale ? $inv->sgst + $return_sale->sgst : $inv->sgst;
                                echo '<tr><td>' . lang('sgst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $sgst) : $sgst) . '</td></tr>';
                            }
                            if ($inv->igst > 0) {
                                $igst = $return_sale ? $inv->igst + $return_sale->igst : $inv->igst;
                                echo '<tr><td>' . lang('igst') .'</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatValue($value_decimals, $igst) : $igst) . '</td></tr>';
                            }
                        }


                        if ($inv->tip_amount != 0) {
                            echo '<tr>
                                    <th>' . lang("total") . '</th>
                                    <th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->grand_total - $inv->tip_amount) . '</th>
                                  </tr>';
                            echo '<tr>
                                    <th>' . lang("tip_suggested") . '</th>
                                    <th class="text-right">' . $this->sma->formatValue($value_decimals, $inv->tip_amount) . '</th>
                                  </tr>';
                        }

                        if ($this->pos_settings->rounding || $inv->rounding != 0) {
                            ?>
                            <tr>
                                <th><?=lang("rounding");?></th>
                                <th class="text-right"><?= $this->sma->formatValue($value_decimals, $inv->rounding);?></th>
                            </tr>
                            <tr>
                                <th><?=lang("total_to_pay");?></th>
                                <th class="text-right" style="font-size: 130%;"><?=$this->sma->formatValue($value_decimals, $return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)  - $rete_total );?></th>
                            </tr>
                            <?php
                        } else {
                            ?>
                            <tr>
                                <th><?=lang("total_to_pay");?></th>
                                <th class="text-right" style="font-size: 130%;"><?=$this->sma->formatValue($value_decimals, $return_sale ? ($inv->grand_total+$return_sale->grand_total) : $inv->grand_total  - $rete_total );?></th>
                            </tr>
                            <?php
                        }
                        if ($inv->paid < ($inv->grand_total + $inv->rounding)) {
                            ?>
                            <tr>
                                <th><?=lang("paid_amount");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, $return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid);?></th>
                            </tr>
                            <tr>
                                <th><?=lang("due_amount");?></th>
                                <th class="text-right"><?=$this->sma->formatValue($value_decimals, ($return_sale ? (($inv->grand_total + $inv->rounding)+$return_sale->grand_total) : ($inv->grand_total + $inv->rounding)) - ($return_sale ? ($inv->paid+$return_sale->paid) : $inv->paid));?></th>
                            </tr>
                            <?php
                        } ?>
                    </tfoot>
                </table>
                <?php
                if ($payments) {
                    echo '<table class="table table-condensed"><tbody>';
                    foreach ($payments as $payment) {
                        echo '<tr>';
                        if (($payment->paid_by == 'cash' || $payment->paid_by == 'deposit') && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatValue($value_decimals, $payment->pos_balance) : 0) . '</td>';
                        } elseif (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe')) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                            echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                        } elseif ($payment->paid_by == 'Cheque') {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                        } elseif ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("no") . ': ' . $payment->cc_no . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("balance") . ': ' . $this->sma->formatValue($value_decimals, $this->sma->getCardBalance($payment->cc_no)) . '</td>';
                        } elseif ($payment->paid_by == 'other' && $payment->amount) {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        } elseif($payment->paid_by == 'retencion') {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                        } else {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody></table>';
                }
                if ($return_payments) {
                    echo '<strong>'.lang('return_payments').'</strong><table class="table table-condensed"><tbody>';
                    foreach ($return_payments as $payment) {
                        $payment->amount = (0-$payment->amount);
                        echo '<tr>';
                        if (($payment->paid_by == 'cash' || $payment->paid_by == 'deposit') && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("change") . ': ' . ($payment->pos_balance > 0 ? $this->sma->formatValue($value_decimals, $payment->pos_balance) : 0) . '</td>';
                        } elseif (($payment->paid_by == 'CC' || $payment->paid_by == 'ppp' || $payment->paid_by == 'stripe') && $payment->cc_no) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("no") . ': ' . 'xxxx xxxx xxxx ' . substr($payment->cc_no, -4) . '</td>';
                            echo '<td>' . lang("name") . ': ' . $payment->cc_holder . '</td>';
                        } elseif ($payment->paid_by == 'Cheque' && $payment->cheque_no) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("cheque_no") . ': ' . $payment->cheque_no . '</td>';
                        } elseif ($payment->paid_by == 'gift_card' && $payment->pos_paid) {
                            echo '<td>' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td>' . lang("no") . ': ' . $payment->cc_no . '</td>';
                            echo '<td>' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo '<td>' . lang("balance") . ': ' . $this->sma->formatValue($value_decimals, $this->sma->getCardBalance($payment->cc_no)) . '</td>';
                        } elseif ($payment->paid_by == 'other' && $payment->amount) {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        } else {
                            echo '<td colspan="2">' . lang("paid_by") . ': ' . lang($payment->paid_by_name) . '</td>';
                            echo '<td colspan="2">' . lang("amount") . ': ' . $this->sma->formatValue($value_decimals, $payment->pos_paid == 0 ? $payment->amount : $payment->pos_paid) . ($payment->return_id ? ' (' . lang('returned') . ')' : '') . '</td>';
                            echo $payment->note ? '</tr><td colspan="4">' . lang("payment_note") . ': ' . $payment->note . '</td>' : '';
                        }
                        echo '</tr>';
                    }
                    echo '</tbody></table>';
                }
                ?>

                <?= $Settings->invoice_view > 0 && (!isset($document_type_invoice_format->view_item_tax) || (isset($document_type_invoice_format->view_item_tax) && $document_type_invoice_format->view_item_tax)) ? $this->gst->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax)) : ''; ?>

                <?php
                    if ($biller_data->product_order == "1" || $biller_data->product_order == "2")
                    {
                        echo $this->gscat->summary($rows, $return_rows, ($return_sale ? $inv->product_tax+$return_sale->product_tax : $inv->product_tax), FALSE, $biller_data->product_order);
                    }
                ?>
                <?php if ($show_award_points == 1): ?>
                    <?= $customer->award_points != 0 && $Settings->each_spent > 0 ? '<p style="text-align:center;">'.lang('this_sale_points').': '.floor(($inv->grand_total/$Settings->each_spent)*$Settings->ca_point)
                    .'<br>'.
                    lang('total').' '.lang('award_points').': '. $customer->award_points . '</p>' : ''; ?>
                <?php endif ?>
                <?= $inv->note ? '<p><strong>' . lang('sale_note') . ':</strong>  ' . $this->sma->decode_html($inv->note) . '</p>' : ''; ?>
                <?= $inv->staff_note ? '<p><strong>' . lang('staff_note') . ':</strong> ' . $this->sma->decode_html($inv->staff_note) . '</p>' : ''; ?>

                <?php if ($inv->resolucion): ?>
                    <p style="text-align:center;"><?= $inv->resolucion ?></p>
                <?php endif ?>

                <?php if ($inv->cufe): ?>
                    <p style="text-align:center; word-break: break-all;">CUFE : <?= $inv->cufe ?></p>
                    <div style="text-align:center;">
                        <img src="<?= base_url().'themes/default/admin/assets/images/qr_code/'.$inv->reference_no.'.png' ?>">
                    </div>
                    <br>
                <?php endif ?>
                <p style="text-align:center;"><?= $this->sma->decode_html($invoice_footer ? $invoice_footer : '') ?></p>

                <?php
                if ($inv_total_before_promo > 0) {
                    // $tp = $this->sma->formatDecimal((($inv_total_before_promo - $inv->grand_total) / $inv_total_before_promo) * 100, 2)." %";
                    $tp = " $ ".$this->sma->formatMoney($inv_total_before_promo + $inv->order_discount);
                    echo "<p class='text-center'>".sprintf(lang('invoice_total_promo_discount'), $tp)."</p>";
                }
                 ?>

                <?php if ($inv->shipping != 0 && $inv->shipping_in_grand_total == 0): ?>
                    <hr>
                    <h4><?= sprintf(lang('shipping_not_include_in_grand_total'), $this->sma->formatValue($value_decimals, $inv->shipping)) ?></h4>
                <?php endif ?>

                <?php if ($biller_data->concession_status == 1 && $biller_data->type_concession == 1): ?>
                    <?php
                        $cc_data = [];
                        foreach ($biller_categories_concession as $bcc) {
                            $cc_data[$bcc->category_id] = $bcc;
                        }
                     ?>
                    <?php foreach ($ccategories as $category_id => $category_total): ?>
                        <?php if (isset($cc_data[$category_id]) && $cc_data[$category_id]->concession_code): ?>
                            <?php
                            $total_factura_barcode = $this->sma->zero_left_consecutive(ceil($category_total), '000000000');
                            $concession_name = $cc_data[$category_id]->concession_name;
                            $concession_code = $cc_data[$category_id]->concession_code;
                             ?>
                            <div class="col-xs-12 text-center" style="padding-top: 10%">
                                <span><?= lang('concession_name') ?> : <?= $concession_name ?></span><br>
                                <span><?= lang('total') ?> : <?= $this->sma->formatMoney(ceil($category_total)) ?></span><br>
                                <img src="<?= admin_url('misc/barcode/'.($concession_code.$total_factura_barcode).'/code128/40/0/0'); ?>" alt="<?= ($concession_code.$total_factura_barcode); ?>" class="bcimg" style="width: 70%;" />
                                <br>
                                <span><?= $concession_code.$total_factura_barcode ?></span>
                            </div>
                        <?php endif ?>
                    <?php endforeach ?>
                <?php endif ?>
                <p class="text-center">Software Wappsi POS desarrollado por Web Apps Innovation SAS NIT 901.090.070-9  www.wappsi.com</p>
                <p class="text-center"><?= lang('created_by')." : ".ucwords(mb_strtolower($created_by->first_name." ".$created_by->last_name)) ?></p>
            </div>

        </div>

        <div id="buttons" style="padding-top:10px;" class="no-print">

            <div class="alert alert-info loading_print_status" <?= $this->pos_settings->remote_printing != 4 ? 'style="display:none;"' : '' ?> >
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status') ?>
            </div>

            <div class="alert alert-success loading_print_status_success" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_success') ?>
            </div>

            <div class="alert alert-warning loading_print_status_warning" style="display: none;">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <?= lang('loading_print_status_warning') ?>
            </div>

            <hr>
            <?php
            if ($message) {
                ?>
                <div class="alert alert-success">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <?=is_array($message) ? print_r($message, true) : $message;?>
                </div>
                <?php
            } ?>
            <?php
            if (!$modal)  {
                ?>
                <span class="pull-right col-xs-12">
                    <?php
                    if ($pos->remote_printing == 1) {
                        echo '<button onclick="imprimir_factura_pos();" class="btn btn-block btn-primary btn_print_html">'.lang("print").'</button>';
                    } else {
                        echo '<button onclick="return printReceipt()" class="btn btn-block btn-primary">'.lang("print").'</button>';
                        echo '<button onclick="return openCashDrawer()" class="btn btn-block btn-default">'.lang("open_cash_drawer").'</button>';
                    }
                    ?>
                </span>
                <span class="pull-left col-xs-12"><a class="btn btn-block btn-success" href="#" id="email"><?= lang("email"); ?></a></span>
                <span class="col-xs-12">
                    <a class="btn btn-block btn-warning" href="<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>"><?= lang("back_to_pos"); ?></a>
                </span>
                <?php
            }
            //if ($pos->remote_printing == 1) {
                ?>
                <!-- <div style="clear:both;"></div>
                <div class="col-xs-12" style="background:#F5F5F5; padding:10px;">
                    <p style="font-weight:bold;">
                        Please don't forget to disble the header and footer in browser print settings.
                    </p>
                    <p style="text-transform: capitalize;">
                        <strong>FF:</strong> File &gt; Print Setup &gt; Margin &amp; Header/Footer Make all --blank--
                    </p>
                    <p style="text-transform: capitalize;">
                        <strong>chrome:</strong> Menu &gt; Print &gt; Disable Header/Footer in Option &amp; Set Margins to None
                    </p>
                </div> -->
                <?php
            //} ?>
            <!--<div style="clear:both;"></div>-->


        </div>
    </div>

    <?php
    if( ! $modal) {
        ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
        <?php
    }
    ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '#email', function() { open_email_confirmation_window(); return false; });

            <?php if($this->pos_settings->print_voucher_delivery == 1 && !$modal) { ?>
                imprimir_factura_pos();
                setTimeout(function() {
                    location.href = '<?= admin_url("pos/voucher_delivery/").$inv->id."/".$redirect_to_pos ?>';
                }, 500);
            <?php } elseif ($biller_data->concession_status == 1 && $biller_data->type_concession == 2 && !$modal) { ?>
                    imprimir_factura_pos();
                    setTimeout(function() {
                        location.href = '<?= admin_url("pos/concession_print/").$inv->id."/".$redirect_to_pos ?>';
                    }, 500);
            <?php } else { ?>
                <?php if ($redirect_to_pos && $print_directly == 1) { ?>

                    window.scrollTo(0,document.body.scrollHeight);
                    setTimeout(function() {
                        $.ajax({
                            url:"<?= admin_url('pos/get_direct_printers_jobs/'.$direct_print_job_id.'/'.$printer->google_cloud_print_id) ?>",
                        }).done(function(data){
                            $('.loading_print_status').fadeOut();
                            window.scrollTo(0,document.body.scrollHeight);
                            if (data == 'DONE') {
                                $('.loading_print_status_success').fadeIn();
                            } else {
                                $('.loading_print_status_warning').fadeIn();
                            }
                        });
                    }, 8000);

                    setTimeout(function() {
                        location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                    }, 8500);
                <?php } else if ($redirect_to_pos) { ?>
                    imprimir_factura_pos();
                    if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) && site.pos_settings.mobile_print_automatically_invoice == 2) {
                        setTimeout(function() {
                            location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                        }, (site.pos_settings.mobile_print_wait_time * 1000));
                    } else {
                        setTimeout(function() {
                            location.href = '<?= $this->session->userdata('redirect_pos') ? admin_url($this->session->userdata('redirect_pos')) : admin_url("pos") ?>';
                        }, 850);
                    }
                <?php } else { ?>
                    <?php
                    if ($print_directly == 1) { ?>

                        window.scrollTo(0,document.body.scrollHeight);
                        setTimeout(function() {
                            $.ajax({
                                url:"<?= admin_url('pos/get_direct_printers_jobs/'.$direct_print_job_id.'/'.$printer->google_cloud_print_id) ?>",
                            }).done(function(data){
                                $('.loading_print_status').fadeOut();
                                window.scrollTo(0,document.body.scrollHeight);
                                if (data == 'DONE') {
                                    $('.loading_print_status_success').fadeIn();
                                } else {
                                    $('.loading_print_status_warning').fadeIn();
                                }
                            });
                        }, 8000);
                    <?php } else if ($this->pos_settings->remote_printing == 1 && !$print_directly) { ?>
                        $(window).load(function () {
                            imprimir_factura_pos();
                            return false;
                        });
                        <?php
                    }
                    ?>
                <?php } ?>
            <?php } ?>
        });

        function open_email_confirmation_window()
        {
            bootbox.prompt({
                title: "<?= lang("email_address"); ?>",
                inputType: 'email',
                value: "<?= $customer->email; ?>",
                callback: function(email) {
                    if (email != null) {
                        generate_pos_invoice_pdf(email);
                    }
                }
            });
        }

        function generate_pos_invoice_pdf (email)
        {
            $.ajax({
                type: "post",
                url: "<?= admin_url('pos/generate_pos_invoice_pdf') ?>",
                dataType: "json",
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': "<?= $this->security->get_csrf_hash(); ?>",
                    'email': email,
                    'id': <?= $inv->id; ?>
                }
            })
            .done(function(data) {
                console.log(data);

                if (data.response = "1") {
                    send_copy_inovice(email);
                } else {
                    bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                    return false;
                }
            })
            .fail(function(data) {
                console.log(data.responseText);
                bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                return false;
            });
        }

        function send_copy_inovice(email)
        {
            $.ajax({
                type: "post",
                url: "<?= admin_url('pos/email_receipt') ?>",
                dataType: "json",
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': "<?= $this->security->get_csrf_hash(); ?>",
                    'email': email,
                    'id': <?= $inv->id; ?>
                }
            })
            .done(function(data) {
                bootbox.alert({message: data.msg, size: 'small'});
            })
            .fail(function() {
                bootbox.alert({message: '<?= lang('ajax_request_failed'); ?>', size: 'small'});
                return false;
            });
        }

        function imprimir_factura_pos()
        {
            if ("<?= $this->session->userdata('print_pos_finalized_invoice') ?>" == 0 || ("<?= isset($submit_target) && $submit_target == 2 ?>")) {
                return false;
            }
            if ("<?= $this->pos_settings->mobile_print_automatically_invoice ?>" == 0) {
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    $('.btn_print_html').fadeOut();
                    return false;
                }
            }
            setTimeout(function() {
                window.print();
            }, 350);
            // var divToPrint=document.getElementById('receiptData');
            // var newWin=window.open('','Print-Window');
            // var header = $('head').html();
            // newWin.document.open();
            // newWin.document.write('<head>'+header+'</head><body onload="setTimeout(function(){window.print();},850); setTimeout(function(){window.close();},2000);" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
            // newWin.document.close();
            // setTimeout(function(){newWin.close();},2500);
        }
    </script>

    <?php include 'remote_printing.php'; ?>
    <?php if($modal) { ?>
    </div>
</div>
</div>

<div class="buttons row div_buttons_modal div_buttons_modal_sm">
    <div class="col-sm-3 col-xs-6">
        <a href='#' class='po btn btn-primary btn-outline' title='' data-placement="top" data-content="
                    <p> <?= lang('where_to_duplicate') ?> </p>
                    <a class='btn btn-primary' href='<?= admin_url('sales/add/?sale_id=').$inv->id ?>'> <?= lang('detal_sale') ?> </a>
                    <a class='btn btn-primary' href='<?= admin_url('pos/index/?duplicate=').$inv->id ?>'> <?= lang('pos_sale') ?> </a>
                    <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover' style="width: 100%;">
                    <i class='fa fa-download'></i>
            <?= lang('duplicate') ?>
        </a>
    </div>
    <div class="col-sm-3 col-xs-6" role="">
        <?php if ($modal): ?>
            <a href="<?= admin_url('pos/view/'.$inv->id) ?>" target="_blank" class="btn btn-block btn-primary btn_print_html btn-outline" style="width: 100%;"><?= lang("print") ?></a>
        <?php else: ?>
            <?php
            if ($pos->remote_printing == 1) {
                echo '<button onclick="imprimir_factura_pos();" class="btn btn-block btn-primary btn_print_html btn-outline" style="width: 100%;">'.lang("print").'</button>';
                //echo '<a href="'.admin_url('pos/view/').$sale_id.'" class="btn btn-block btn-primary" target="_blank">'.lang("print").'</a>';
            } else {
                echo '<button onclick="return printReceipt()" class="btn btn-block btn-primary btn-outline" style="width: 100%;">'.lang("print").'</button>';
            }
            ?>
        <?php endif ?>
    </div>
    <div class="col-sm-3 col-xs-6" role="group">
        <a class="btn btn-block btn-success btn-outline" href="#" id="email" style="width: 100%;"><?= lang("email"); ?></a>
    </div>
    <div class="col-sm-3 col-xs-6" role="group">
        <button type="button" class="btn btn-default btn-outline" data-dismiss="modal" style="width: 100%;"><?= lang('close'); ?></button>
    </div>
</div>
<?php } else { ?>
</body>
</html>
<?php } ?>
