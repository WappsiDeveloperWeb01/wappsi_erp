<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('credit_note_other_concepts_label_menu'); ?></h2>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="ibox">
        <div class="ibox-content">
            <div class="row">
                <div class="col-lg-12">
                    <!-- <?= admin_form_open_multipart("sales/return_sale/", array('data-toggle' => 'validator', 'role' => 'form', 'class' => 'edit-rere-form')); ?> -->
                    <?= admin_form_open_multipart("returns/add_credit_note_other_concepts", ["id"=>"add_credit_note_other_concepts_form"]); ?>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang("customer"), "customer_id"); ?>
                                <?= form_input('customer_id', (isset($_POST['customer_id']) ? $_POST['customer_id'] : ""), 'id="customer_id" data-placeholder="'. lang("select") .'" required="required" class="form-control ssr-customer input-tip" style="width:100%;"');
                                            ?>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <!-- <?= lang("affects", "affects") ?>
                                <?= form_input(["class"=>"form-control", "name"=>"affects", "id"=>"affects", "readonly"=>TRUE]); ?> -->
                                <?= form_label(lang("credit_note_other_concepts_label_invoice_reference"), 'reference_no'); ?>
                                <?= form_input("reference_no", (isset($_POST['reference_no']) ? $_POST['reference_no'] : ""), 'class="form-control" id="reference_no" placeholder="'. lang("select") .'"'); ?>
                                <input type="hidden" name="invoice_reference" id="invoice_reference">
                                <input type="hidden" name="customer_name" id="customer_name">
                                <input type="hidden" name="grand_total" id="grand_total">
                                <input type="hidden" name="dian_code" id="dian_code">
                                <input type="hidden" name="module" id="module">
                                <input type="hidden" name="addresses_id" id="addresses_id">
                                <input type="hidden" name="seller_id" id="seller_id">
                                <input type="hidden" name="withholdings_data" id="withholdings_data">
                            </div>
                        </div>

                        <?php if ($Owner || $Admin) { ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= form_label(lang("date"), "redate"); ?>
                                    <?= form_input(["class"=>"form-control datetime", "type"=>"text", "name"=>"date", "id"=>"redate", "required"=>TRUE], (isset($_POST['date']) ? $_POST['date'] : "")); ?>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="col-md-4">
                            <?= form_label(lang("credit_note_other_concepts_label_branch_office"), 'branch_office'); ?>
                            <?= form_input("branch_office", (isset($_POST['branch_office']) ? $_POST['branch_office'] : ""), 'class="form-control" id="branch_office" readonly'); ?>
                            <input type="hidden" name="branch_id" id="branch_id">
                        </div>

                        <!-- <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang("reference_no"), "document_type_id"); ?>
                                <?= form_dropdown(["class"=>"form-control", "name"=>"document_type_id", "id"=>"document_type_id", "required"=>TRUE]); ?>
                            </div>
                        </div> -->

                        <div class="col-md-4">
                            <?= form_label(lang("credit_note_other_concepts_label_document"), 'document_type'); ?>
                            <select class="form-control" name="document_type" id="document_type">
                                <option value=""><?= lang("select"); ?></option>
                            </select>
                        </div>

                        <?php if (isset($cost_centers)) { ?>
                            <div class="col-md-4 form-group">
                                <?= form_label(lang('cost_center'), "cost_center_id"); ?>
                                <?php
                                    $cost_center_option[''] = lang('select');
                                    foreach ($cost_centers as $cost_center) {
                                        $cost_center_option[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                    }
                                ?>
                                <?= form_dropdown(["class"=>"form-control input-tip select", "name"=>"cost_center_id", "id"=>"cost_center_id", "required"=>TRUE], $cost_center_option); ?>
                            </div>
                        <?php } ?>
                    </div>

                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= form_label(lang("credit_note_other_concepts_label_concept"), 'concept'); ?>
                                <div class="input-group">
                                    <select class="form-control" name="concept" id="concept">
                                        <option value=""><?= lang("select"); ?></option>
                                        <?php foreach ($credit_note_concepts as $credit_note_concept) { ?>
                                            <option value="<?= $credit_note_concept->id; ?>" data-concept_tax_rate="<?= $credit_note_concept->concept_tax_rate; ?>" data-concept_withholdings="<?= $credit_note_concept->wh_tax; ?>" data-concept_dian_code="<?= $credit_note_concept->dian_code; ?>" data-concept_tax_rate_id="<?= $credit_note_concept->concept_tax_rate_id; ?>"><?= $credit_note_concept->description; ?></option>
                                        <?php } ?>
                                    </select>
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="button" name="add_concept" id="add_concept" data-toggle="tooltip" data-placement="top" title="Agregar Concepto"><i class="fa fa-plus"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="control-group table-group">
                                <label class="table-label"><?= lang("credit_note_other_concepts_label_concepts"); ?> *</label>
                                <div class="controls table-controls">
                                    <table id="credit_note_table" class="table items  table-bordered table-condensed table-hover sortable_table">
                                        <thead>
                                            <tr>
                                                <th width="5%"><?= lang("credit_note_other_concepts_label_delete"); ?></th>
                                                <th ><?= lang('credit_note_other_concepts_label_concept'); ?></th>
                                                <th width="10%"><?= lang("credit_note_other_concepts_label_value"); ?></th>
                                                <th width="10%"><?= lang("credit_note_other_concepts_label_tax"); ?></th>
                                                <th width="10%"><?= lang("credit_note_other_concepts_label_total"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <label for="open_modal_retention_window"> Retención</label>
                            <div class="input-group">
                                <input class="form-control text-right" type="text" name="withholdings" id="withholdings" style="padding-right: 10px" readonly>
                                <span class="input-group-btn">
                                    <button class="btn btn-success" type="button" id="open_modal_retention_window"><i class="fa fa-pencil"></i> Agregar</button>
                                </span>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <?= lang("document", "document") ?>
                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false" data-show-preview="false" class="form-control file">
                            </div>
                        </div>
                    </div>

                    <!-- <div class="row">
                        <div class="col-md-12">
                            <div id="bottom-total" class="well well-sm">
                                <table class="table table-bordered table-condensed" style="margin-bottom:0;">
                                    <tr class="warning">
                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                        <td width="10%"><?= lang('total') ?> <span class="totals_val pull-right" id="total_sum_concept_value">0.00</span></td>
                                        <td width="10%"><?= lang('grand_total') ?> <span class="totals_val pull-right" id="total_sum_concept_tax">0.00</span></td>
                                        <td width="9.5%"><?= lang('grand_total') ?> <span class="totals_val pull-right" id="total_sum">0.00</span></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div> -->

                    <div class="row">


                            <!-- <div class="col-sm-4">
                                <label onclick="$('#rtModal').modal('show');" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención</label>
                                <label>Retención</label>
                                <input type="text" name="retencion" id="rete" class="form-control text-right" readonly>
                            </div> -->

                            <!-- <div class="col-md-12">
                                <div class="control-group table-group">
                                    <label class="table-label"><?= lang("order_items"); ?></label> (<?= lang('return_tip'); ?>)

                                    <div class="controls table-controls">
                                        <table id="reTable"
                                               class="table items  table-bordered table-condensed table-hover">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-4"><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                                    <?php
                                                    if ($Settings->product_serial) {
                                                        echo '<th class="col-md-2">' . lang("serial_no") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang('gross_net_unit_price') ?></th>
                                                    <?php
                                                    if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                        echo '<th class="col-md-1">' . lang("discount") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang("price_x_discount"); ?></th>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        echo '<th class="col-md-1">' . lang("product_tax") . '</th>';
                                                    }
                                                    ?>
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        echo '<th class="col-md-1">' . lang("price_x_tax") . '</th>';
                                                    }
                                                    ?>
                                                    <th class="col-md-1"><?= lang("quantity"); ?></th>

                                                    <th>
                                                        <?= lang("total"); ?>
                                                        (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th>
                                                    <th style="width: 30px !important; text-align: center;">
                                                        <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </div>
                                <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                    <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                        <tr class="warning">
                                            <td>
                                                <?= lang('items') ?>
                                                <span class="totals_val pull-right" id="titems">0</span>
                                            </td>
                                            <td>
                                                <?= lang('total') ?>
                                                <span class="totals_val pull-right" id="total">0.00</span>
                                            </td>
                                            <?php if ($Settings->tax1) { ?>
                                            <td>
                                                <?= lang('product_tax') ?>
                                                <span class="totals_val pull-right" id="ttax1">0.00</span>
                                            </td>
                                            <?php } ?>
                                            <td>
                                                <?= lang('surcharges') ?>
                                                <span class="totals_val pull-right" id="trs">0.00</span>
                                            </td>
                                            <?php if ($Settings->tax2) { ?>
                                            <td>
                                                <?= lang('order_tax') ?>
                                                <span class="totals_val pull-right" id="ttax2">0.00</span>
                                            </td>
                                            <?php } ?>
                                            <td>
                                                <?= lang('return_amount') ?>
                                                <span class="totals_val pull-right" id="gtotal">0.00</span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div> -->

                            <div style="height:15px; clear: both;"></div>
                            <div class="col-md-12">
                                <?php
                                    if (isset($invoice_data) && !empty($invoice_data)) {
                                        if ($invoice_data->payment_status == 'paid') {
                                            echo '<div class="alert alert-success">' . lang('payment_status') . ': <strong>' . $invoice_data->payment_status . '</strong> & ' . lang('paid_amount') . ' <strong>' . $this->sma->formatMoney($invoice_data->paid) . '</strong></div>';
                                        } else {
                                            echo '<div class="alert alert-warning">' . lang('payment_status_not_paid') . ' ' . lang('payment_status') . ': <strong>' . $invoice_data->payment_status . '</strong> & ' . lang('paid_amount') . ' <strong>' . $this->sma->formatMoney($invoice_data->paid) . '</strong></div>';
                                        }
                                    }
                                ?>
                            </div>
                            <?php if (($Owner || $Admin || $GP['sales-payments']) && isset($allow_payment)) { ?>
                            <div id="payments" style="display: none;">
                                <div class="col-md-12">
                                    <div class="well well-sm well_1">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="form-group">
                                                        <?= lang("payment_reference_no", "payment_reference_no"); ?>
                                                        <?= form_input('payment_reference_no', (isset($_POST['payment_reference_no']) ? $_POST['payment_reference_no'] : $payment_ref), 'class="form-control tip" id="payment_reference_no"'); ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="payment">
                                                        <div class="form-group">
                                                            <?= lang("amount", "amount_1"); ?>
                                                            <input type="text" class="form-control amount_to_paid" readonly>
                                                            <input type="hidden" name="amount-paid" id="amount_1"/>
                                                            <input type="hidden" name="amount-discounted" id="discounted"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <?= lang("paying_by", "paid_by_1"); ?>
                                                        <select name="paid_by" id="paid_by_1" class="form-control paid_by">
                                                            <?= $this->sma->paid_opts(); ?>
                                                        </select>

                                                        <input type="hidden" name="mean_peyment_code_fe" id="mean_peyment_code_fe">
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="clearfix"></div>
                                            <div class="pcc_1" style="display:none;">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input name="pcc_no" type="text" id="pcc_no_1"
                                                                   class="form-control" placeholder="<?= lang('cc_no') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">

                                                            <input name="pcc_holder" type="text" id="pcc_holder_1"
                                                                   class="form-control"
                                                                   placeholder="<?= lang('cc_holder') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <select name="pcc_type" id="pcc_type_1"
                                                                    class="form-control pcc_type"
                                                                    placeholder="<?= lang('card_type') ?>">
                                                                <option value="Visa"><?= lang("Visa"); ?></option>
                                                                <option
                                                                    value="MasterCard"><?= lang("MasterCard"); ?></option>
                                                                <option value="Amex"><?= lang("Amex"); ?></option>
                                                                <option value="Discover"><?= lang("Discover"); ?></option>
                                                            </select>
                                                            <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?= lang('card_type') ?>" />-->
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input name="pcc_month" type="text" id="pcc_month_1"
                                                                   class="form-control" placeholder="<?= lang('month') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">

                                                            <input name="pcc_year" type="text" id="pcc_year_1"
                                                                   class="form-control" placeholder="<?= lang('year') ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">

                                                            <input name="pcc_ccv" type="text" id="pcc_cvv2_1"
                                                                   class="form-control" placeholder="<?= lang('cvv2') ?>"/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="pcheque_1" style="display:none;">
                                                <div class="form-group"><?= lang("cheque_no", "cheque_no_1"); ?>
                                                    <input name="cheque_no" type="text" id="cheque_no_1"
                                                           class="form-control cheque_no"/>
                                                </div>
                                            </div>
                                            <div class="gc" style="display: none;">
                                                <div class="form-group">
                                                    <?= lang("gift_card_no", "gift_card_no"); ?>
                                                    <div class="input-group">

                                                        <input name="gift_card_no" type="text" id="gift_card_no"
                                                               class="pa form-control kb-pad"/>

                                                        <div class="input-group-addon"
                                                             style="padding-left: 10px; padding-right: 10px; height:25px;">
                                                            <a href="#" id="sellGiftCard" class="tip"
                                                               title="<?= lang('sell_gift_card') ?>"><i
                                                                    class="fa fa-credit-card"></i></a></div>
                                                    </div>
                                                </div>
                                                <div id="gc_details"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                            <input type="hidden" name="total_items" value="" id="total_items" required="required"/>
                            <input type="hidden" name="order_tax" value="" id="retax2" required="required"/>
                            <input type="hidden" name="discount" value="" id="rediscount" required="required"/>

                            <?php if (isset($discounts)) { ?>
                                <?php foreach ($discounts as $discount) { ?>
                                    <input type="hidden" name="discount_amount[]" value="<?= $discount['amount'] ?>">
                                    <input type="hidden" name="discount_ledger_id[]" value="<?= $discount['ledger_id'] ?>">
                                    <input type="hidden" name="discount_description[]" value="<?= $discount['description'] ?>">
                                <?php } ?>
                            <?php } ?>

                            <div class="row" id="bt">
                                <div class="col-md-12">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <?= lang("return_note", "renote"); ?>
                                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="renote" style="margin-top: 10px; height: 100px;"'); ?>

                                        </div>
                                    </div>

                                </div>

                            </div>
                            <div class="col-md-12">
                                <div class="fprom-group">
                                    <!-- <?php echo form_submit('add_return', $this->lang->line("submit"), 'id="add_return" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?> -->

                                    <button class="btn btn-primary" type="button" name="add_credit_note" id="add_credit_note"><?= lang("submit"); ?></button>
                                    <button class="btn btn-danger" type="button" id="reset"><?= lang('cancel') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>


            <!-- <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-lg">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                      class="fa fa-2x">&times;</i></button>
                      <h4 class="modal-title" id="rtModalLabel"><?=lang('edit_order_tax');?></h4>
                    </div>
                    <div class="modal-body">
                      <div class="row">
                        <div class="col-sm-2">
                          <label>Retención</label>
                        </div>
                        <div class="col-sm-5">
                          <label>Opción</label>
                        </div>
                        <div class="col-sm-2">
                          <label>Porcentaje</label>
                        </div>
                        <div class="col-sm-3">
                          <label>Valor</label>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                          <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                          <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                          <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-2">
                          <label>
                            <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                          </label>
                        </div>
                        <div class="col-sm-5">
                          <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                            <option>Seleccione...</option>
                          </select>
                          <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                          <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                        </div>
                        <div class="col-sm-2">
                          <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                        </div>
                        <div class="col-sm-3">
                          <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-8 text-right">
                          <label>Total : </label>
                        </div>
                        <div class="col-md-4 text-right">
                          <label id="total_rete_amount"> 0.00 </label>
                        </div>
                        <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                      </div>
                    </div>
                    <div class="modal-footer">
                      <button type="button" id="cancelOrderRete" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                      <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                    </div>
                  </div>
                </div>
              </div>



                    <?php echo form_close(); ?>

                </div>
            </div> -->
        </div>
    </div>
</div>

<div class="modal" id="gcModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="myModalLabel"><?= lang('sell_gift_card'); ?></h4>
            </div>
            <div class="modal-body">
                <p><?= lang('enter_info'); ?></p>

                <div class="alert alert-danger gcerror-con" style="display: none;">
                    <button data-dismiss="alert" class="close" type="button">×</button>
                    <span id="gcerror"></span>
                </div>
                <div class="form-group">
                    <?= lang("card_no", "gccard_no"); ?> *
                    <div class="input-group">
                        <?php echo form_input('gccard_no', '', 'class="form-control" id="gccard_no" onClick="this.select();"'); ?>
                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;"><a href="#"
                                                                                                           id="genNo"><i
                                    class="fa fa-cogs"></i></a></div>
                    </div>
                </div>
                <input type="hidden" name="gcname" value="<?= lang('gift_card') ?>" id="gcname"/>

                <div class="form-group">
                    <?= lang("value", "gcvalue"); ?> *
                    <?php echo form_input('gcvalue', '', 'class="form-control" id="gcvalue"'); ?>
                </div>
                <div class="form-group">
                    <?= lang("customer", "gccustomer"); ?>
                    <div class="input-group">
                        <?php echo form_input('gccustomer', '', 'class="form-control" id="gccustomer"'); ?>
                        <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;"><a href="#"
                                                                                                           id="noCus"
                                                                                                           class="tip"
                                                                                                           title="<?= lang('unselect_customer') ?>"><i
                                    class="fa fa-times"></i></a></div>
                    </div>
                </div>
                <div class="form-group">
                    <?= lang("expiry_date", "gcexpiry"); ?>
                    <?php echo form_input('gcexpiry', '', 'class="form-control date" id="cgexpiry"'); ?>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" id="addGiftCard" class="btn btn-primary"><?= lang('sell_gift_card') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h4 class="modal-title" id="rtModalLabel"><?= lang('credit_note_other_concepts_label_withholdings_tax');?></h4>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <table class="table table-condensed">
                            <thead>
                                <tr>
                                    <th class="col-sm-2">Retención</th>
                                    <th class="col-sm-5">Opción</th>
                                    <th class="col-sm-2">Porcentaje</th>
                                    <th class="col-sm-3">Valor</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_fuente" id="rete_fuente" data-type_retention="fuente"> Fuente
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_fuente_option" id="rete_fuente_option" data-type_retention="fuente" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                        <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                        <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_fuente_valor" id="rete_fuente_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_iva" id="rete_iva" data-type_retention="iva"> Iva
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_iva_option" id="rete_iva_option" data-type_retention="iva" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                        <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                        <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_iva_valor" id="rete_iva_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_ica" id="rete_ica" data-type_retention="ica"> Ica
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_ica_option" id="rete_ica_option" data-type_retention="ica" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                        <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                        <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_ica_valor" id="rete_ica_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <label style="padding-left: 15px">
                                            <input class="type_retention_check" type="checkbox" name="rete_otros" id="rete_otros" data-type_retention="otros"> Otros
                                        </label>
                                    </td>
                                    <td>
                                        <select class="form-control retention_type_option" name="rete_otros_option" id="rete_otros_option" data-type_retention="otros" disabled='true'>
                                            <option>Seleccione...</option>
                                        </select>
                                        <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                        <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                        <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                                    </td>
                                    <td>
                                        <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                    </td>
                                    <td>
                                        <input class="form-control text-right total_value_withholding_type" type="text" name="rete_otros_valor" id="rete_otros_valor" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2"></td>
                                    <td class="text-right">
                                        <label>Total : </label>
                                    </td>
                                    <td class="text-right">
                                        <label id="total_rete_amount"> 0.00 </label>
                                        <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" id="save_credit_note_withholdings" class="btn btn-primary"><?=lang('update')?></button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">
    var count = 1, an = 1, DT = <?= $Settings->default_tax_rate ?>,
        product_tax = 0, invoice_tax = 0, total_discount = 0, total = 0, surcharge = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>;

    $(document).ready(function () {
        _allow_withholding_calculation = 0;
        _existing_concept = false;
        _retenciones = [];

        $('[data-toggle="tooltip"]').tooltip()


        $(document).on('change', '#customer_id', function() { clean_form(); });
        $(document).on('change', '#reference_no', function() { get_sale_data(); });
        $(document).on('click', '#add_concept', function() { add_concept(); });
        $(document).on('click', '.delete_row', function() { delete_concept_row($(this)); });
        $(document).on('change', '.concept_value', function() { calculate_taxes($(this)); });
        $(document).on('click', '#open_modal_retention_window', function() { open_modal_retention_window($(this)); });
        $(document).on('click', '#save_credit_note_withholdings', function() { save_credit_note_withholdings(); });
        $(document).on('click', '#add_credit_note', function() { add_credit_note(); });
        $(document).on('click', '#reset', function() { location.href = '<?= admin_url('sales') ?>'; });

        $('#reference_no').select2({
            minimumInputLength: 1,
            ajax: {
                url: '<?= admin_url("returns/sale_reference_suggestions"); ?>',
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        customer_id: $('#customer_id').val()
                    };
                },
                results: function (data, page) {
                    if(data.results != null) {
                        return { results: data.results };
                    } else {
                        return { results: [{id: '', text: 'No se encontraron resultados'}]};
                    }
                }
            }
        });

        $(document).on('ifToggled', '.type_retention_check', function(){
            var type_retention = $(this).data('type_retention');

            if ($(this).is(':checked')) {
                 $.ajax({
                    url: site.base_url+"sales/opcionesWithHolding/"+type_retention.toUpperCase()
                }).done(function(data){
                    $('#rete_'+type_retention+'_option').html(data);
                    $('#rete_'+type_retention+'_option').attr('disabled', false).select();
                }).fail(function(data){
                    console.log(data);
                });
            } else {
                $('#rete_'+type_retention+'_option').select2('val', '').attr('disabled', true).select();
                $('#rete_'+type_retention+'_tax').val('');
                $('#rete_'+type_retention+'_valor').val('');

                total_value_withholding_type();
            }
        });

        $(document).on('change', '.retention_type_option', function() {
            var type_retention = $(this).data('type_retention');
            var percentage = $('#rete_'+ type_retention +'_option option:selected').data('percentage');
            var concept_value = $('#concept_value').val();

            $('#rete_'+type_retention+'_tax').val(percentage);

            var total_withholding = (concept_value * percentage) / 100;
            $('#rete_'+type_retention+'_valor').val(formatDecimal(parseFloat(total_withholding)));

            total_value_withholding_type();
        });

        <?php if (isset($invoice_data)) { ?>
        $('#add_return').css('display', 'none');

        command: toastr.warning('Por favor espere unos segundos...', 'Espere...');
        setTimeout(function() {
            $('#add_return').css('display', '');
            command: toastr.success('Ya se completó la carga', 'Completo');
        }, 3000);
        //localStorage.setItem('redate', '<?= $this->sma->hrld($invoice_data->date) ?>');
        localStorage.setItem('reref', '<?= $reference ?>');
        // localStorage.setItem('renote', '<?= $this->sma->decode_html($invoice_data->note); ?>');
        localStorage.setItem('reitems', JSON.stringify(<?= $invoice_data_items; ?>));
        localStorage.setItem('rediscount', '<?= $invoice_data->order_discount_id ?>');
        localStorage.setItem('retax2', '<?= $invoice_data->order_tax_id ?>');
        localStorage.setItem('reinvoice_paid', '<?= $invoice_data->paid ?>');
        localStorage.setItem('repayment_status', '<?= $invoice_data->payment_status ?>');
        localStorage.setItem('resale_balance', '<?= ($invoice_data->grand_total - ($invoice_data->paid)) ?>');
        localStorage.setItem('return_surcharge', '0');
        localStorage.removeItem('retenciones');
            <?php if ($invoice_data->rete_fuente_id || $invoice_data->rete_iva_id || $invoice_data->rete_ica_id || $invoice_data->rete_other_id) { ?>
                retenciones = {
                            'gtotal' : $('#gtotal').text(),
                            'id_rete_fuente' : '<?= $invoice_data->rete_fuente_id ?>',
                            'id_rete_iva' : '<?= $invoice_data->rete_iva_id ?>',
                            'id_rete_ica' : '<?= $invoice_data->rete_ica_id ?>',
                            'id_rete_otros' : '<?= $invoice_data->rete_other_id ?>',
                        };
                localStorage.setItem('retenciones', JSON.stringify(retenciones));
            <?php } ?>
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('redate')) {
            $("#redate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#redate', function (e) {
            localStorage.setItem('redate', $(this).val());
        });
        if (redate = localStorage.getItem('redate')) {
            $('#redate').val(redate);
        }
        <?php } ?>
        if (reref = localStorage.getItem('reref')) {
            $('#reref').val(reref);
        }
        if (rediscount = localStorage.getItem('rediscount')) {
            $('#rediscount').val(rediscount);
        }
        if (retax2 = localStorage.getItem('retax2')) {
            $('#retax2').val(retax2);
        }
        if (return_surcharge = localStorage.getItem('return_surcharge')) {
            $('#return_surcharge').val(return_surcharge);
        }
        if (localStorage.getItem('reitems')) {
            loadItems();
        }
        $(document).on('change', '.paid_by', function () {
            var p_val = $(this).val();
            $('#rpaidby').val(p_val);
            if (p_val == 'cash') {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').show();
            } else if (p_val == 'CC') {
                $('.pcheque_1').hide();
                $('.pcash_1').hide();
                $('.pcc_1').show();
                $('#pcc_no_1').focus();
            } else if (p_val == 'Cheque') {
                $('.pcc_1').hide();
                $('.pcash_1').hide();
                $('.pcheque_1').show();
                $('#cheque_no_1').focus();
            } else {
                $('.pcheque_1').hide();
                $('.pcc_1').hide();
                $('.pcash_1').hide();
            }
            if (p_val == 'gift_card') {
                $('.gc').show();
                $('.ngc').hide();
                $('#gift_card_no').focus();
            } else {
                $('.ngc').show();
                $('.gc').hide();
                $('#gc_details').html('');
            }
        });
        /* ------------------------------
         * Sell Gift Card modal
         ------------------------------- */

        $(document).on('click', '#sellGiftCard', function (e) {
            $('#gcvalue').val($('#amount_1').val());
            $('#gccard_no').val(generateCardNo());
            $('#gcModal').appendTo("body").modal('show');
            return false;
        });

        <?php if (isset($invoice_data)) {?>
            $('#gccustomer').val(<?=$invoice_data->customer_id?>).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= admin_url('customers/getCustomer') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        <?php } ?>

        $(document).on('click', '#noCus', function (e) {
            e.preventDefault();
            $('#gccustomer').select2('val', '');
            return false;
        });

        $('#genNo').click(function () {
            var no = generateCardNo();
            $(this).parent().parent('.input-group').children('input').val(no);
            return false;
        });

        $(document).on('click', '#addGiftCard', function (e) {
            var mid = (new Date).getTime(),
                gccode = $('#gccard_no').val(),
                gcname = $('#gcname').val(),
                gcvalue = $('#gcvalue').val(),
                gccustomer = $('#gccustomer').val(),
                gcexpiry = $('#gcexpiry').val() ? $('#gcexpiry').val() : '',
                gcprice = parseFloat($('#gcprice').val());
            if (gccode == '' || gcvalue == '' || gcprice == '' || gcvalue == 0 || gcprice == 0) {
                $('#gcerror').text('Please fill the required fields');
                $('.gcerror-con').show();
                return false;
            }

            var gc_data = new Array();
            gc_data[0] = gccode;
            gc_data[1] = gcvalue;
            gc_data[2] = gccustomer;
            gc_data[3] = gcexpiry;
            if (typeof reitems === "undefined") {
                var reitems = {};
            }

            $.ajax({
                type: 'get',
                url: site.base_url + 'sales/sell_gift_card',
                dataType: "json",
                data: {gcdata: gc_data},
                success: function (data) {
                    if (data.result === 'success') {
                        $('#gift_card_no').val(gccode);
                        $('#gc_details').text('<?=lang('gift_card_added')?>');
                        $('#gcModal').modal('hide');
                    } else {
                        $('#gcerror').text(data.message);
                        $('.gcerror-con').show();
                    }
                }
            });
            return false;
        });
        var old_row_qty;
        $(document).on("focus", '.rquantity', function () {
            old_row_qty = $(this).val();
        }).on("change", '.rquantity', function () {
            var row = $(this).closest('tr');
                reitems = JSON.parse(localStorage.getItem('reitems'));
            var new_qty = parseFloat($(this).val()),
                item_id = row.attr('data-item-id');
            if (!is_numeric(new_qty) || (new_qty > reitems[item_id].row.oqty)) {
                $(this).val(old_row_qty);
                bootbox.alert('<?= lang('unexpected_value'); ?>');
                return false;
            }
            if(new_qty > reitems[item_id].row.oqty) {
                bootbox.alert('<?= lang('unexpected_value'); ?>');
                $(this).val(old_row_qty);
                return false;
            }
            reitems[item_id].row.base_quantity = new_qty;
            if(reitems[item_id].row.unit != reitems[item_id].row.base_unit) {
                $.each(reitems[item_id].units, function(){
                    if (this.id == reitems[item_id].row.unit) {
                        reitems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                    }
                });
            }
            reitems[item_id].row.qty = new_qty;
            localStorage.setItem('reitems', JSON.stringify(reitems));
            recalcular_retenciones();
            loadItems($(this).prop('tabindex'));
        });
        var old_surcharge;
        $(document).on("focus", '#return_surcharge', function () {
            old_surcharge = $(this).val() ? $(this).val() : '0';
        }).on("change", '#return_surcharge', function () {
            var new_surcharge = $(this).val() ? $(this).val() : '0';
            if (!is_valid_discount(new_surcharge)) {
                $(this).val(new_surcharge);
                bootbox.alert('<?= lang('unexpected_value'); ?>');
                return;
            }
            localStorage.setItem('return_surcharge', JSON.stringify(new_surcharge));
            loadItems();
        });
        $(document).on('click', '.redel', function () {
            var row = $(this).closest('tr');
            var item_id = row.attr('data-item-id');
            delete reitems[item_id];
            row.remove();
            if(reitems.hasOwnProperty(item_id)) { } else {
                localStorage.setItem('reitems', JSON.stringify(reitems));
                loadItems();
                return;
            }
        });
        obtener_documents_types();
        validate_key_log();
        customer_special_discount();
        recalcular_retenciones();

        $(document).on('change', '#paid_by_1', cargar_codigo_forma_pago_fe);

        cargar_codigo_forma_pago_fe();
    });

    function clean_form()
    {
        $('#reference_no').select2('val', '');
        $('#reference_no').trigger('change');
    }

    function get_sale_data()
    {
        $.ajax({
            url: '<?= admin_url("returns/get_sale_data"); ?>',
            type: 'POST',
            dataType: 'JSON',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                id: $('#reference_no').val()
            },
        })
        .done(function(data) {
            if (data.status) {
                var sale_data = data.sale_data;

                $('#customer_branch').val(sale_data.customer_branch);
                $('#branch_office').val(sale_data.branch_name);
                $('#branch_id').val(sale_data.branch_id);
                $('#invoice_reference').val(sale_data.invoice_reference);
                $('#customer_name').val(sale_data.customer_name);
                $('#grand_total').val(sale_data.grand_total);
                $('#module').val(sale_data.module);
                $('#addresses_id').val(sale_data.addresses_id);
                $('#seller_id').val(sale_data.seller_id);
                $('#date').attr('min', sale_data.date);

                get_numbering_resolution_credit_note(sale_data.is_electronic_invoice);
            } else {
                $('#customer_branch').val('');
                $('#branch_office').val('');
                $('#branch_id').val('');
                $('#invoice_reference').val('');
                $('#customer_name').val('');
                $('#grand_total').val('');
                $('#module').val('');
                $('#addresses_id').val('');
                $('#seller_id').val('');
                $('#date').attr('min', '');

                get_numbering_resolution_credit_note('');
            }

            cancel_withholding_calculation();
            delete_concept_row();
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function get_numbering_resolution_credit_note(is_electronic_invoice)
    {
        if (is_electronic_invoice != '') {
            $.ajax({
                url: '<?= admin_url("documents_types/get_document_type_credit_note"); ?>',
                type: 'post',
                dataType: 'json',
                data: {
                    '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'is_electronic_invoice': is_electronic_invoice,
                    'branch_id': $('#branch_id').val()
                },
            })
            .done(function(data) {
                if (data != '') {
                    var document_types_options = '';

                    for (var i = 0; i < data.length; i++) {
                        document_types_options += '<option value="'+ data[i].document_type_id +'" '+ (i == 0 ? 'selected' : '') +'>'+ data[i].document_type_prefix +'</option>';

                        if (i == 0) { selected = data[i].document_type_id; }
                    }

                    $('#document_type').html(document_types_options);
                    $('#document_type').select2('val', selected);
                } else {
                    Command: toastr.error('No existe serie de numeración para esta sucursal', '<?= lang("error"); ?>');
                }
            })
            .fail(function(data) { console.log(data.responseText); });
        } else {
            $('#document_type').html('<option value=""><?= lang("select"); ?></option>');
            $('#document_type').select2('val', '');
        }
    }

    function cancel_withholding_calculation()
    {
        $('.type_retention_check').iCheck('uncheck');

        total_value_withholding_type();
    }

    function total_value_withholding_type()
    {
        var total_sum_withholdings = 0;

        $('.total_value_withholding_type').each(function () {
            if ($(this).val() != '') {
                total_sum_withholdings += parseFloat($(this).val());
            }
        });

        $('#total_rete_amount').text(formatDecimal(total_sum_withholdings));

        _retenciones = {
            'retention_percentage_fuente': $('#rete_fuente_option option:selected').data('percentage'),
            'retention_total_fuente': $('#rete_fuente_valor').val(),
            'retention_percentage_iva': $('#rete_iva_option option:selected').data('percentage'),
            'retention_total_iva': $('#rete_iva_valor').val(),
            'retention_percentage_ica': $('#rete_ica_option option:selected').data('percentage'),
            'retention_total_ica': $('#rete_ica_valor').val(),
            'retention_percentage_other': $('#rete_otros_option option:selected').data('percentage'),
            'retention_total_other': $('#rete_otros_valor').val()
        };

        $('#withholdings_data').val(JSON.stringify(_retenciones));
    }

    function delete_concept_row(row_control)
    {
        if (row_control === undefined) {
            $('#credit_note_table tbody tr').remove();
        } else {
            row_control.parents('tr').remove();
        }

        cancel_withholding_calculation();

        $('#total_sum_concept_value').text('0.00');
        $('#total_sum_concept_tax').text('0.00');
        $('#total_sum').text('0.00');
        $('#withholdings').val(0);

        _existing_concept = false;
    }

    function add_concept()
    {
        var concept = $('#concept').val();
        var reference = $('#reference_no').val();
        var description = $('#concept option:selected').text();
        var concept_withholdings = $('#concept option:selected').data('concept_withholdings');
        var tax_rate_id = $('#concept option:selected').data('concept_tax_rate_id');
        var tax_rate = $('#concept option:selected').data('concept_tax_rate');

        if (validate_add_concept(reference, concept)) {
            var table_row = '<tr>'+
                                '<td class="text-center">'+
                                    '<i class="fa fa-trash fa-2x text-danger delete_row" data-toggle="tooltip" data-placement="top" title="Eliminar" style="cursor: pointer;"></i>'+
                                '</td>'+
                                '<td>'+ description +'</td>'+
                                '<td>'+
                                    '<input class="form-control text-right concept_value" type="number" name="concept_value" id="concept_value">'+
                                    '<input type="hidden" name="concept_id" id="concept_id" value="'+ concept +'">'+
                                    '<input type="hidden" name="concept_name" id="concept_name" value="'+ description +'">'+
                                    '<input type="hidden" name="concept_tax_rate_id" id="concept_tax_rate_id" value="'+ tax_rate_id +'">'+
                                    '<input type="hidden" name="concept_tax_rate" id="concept_tax_rate" value="'+ tax_rate +'">'+
                                '</td>'+
                                '<td><input class="form-control text-right concept_tax" type="number" name="concept_tax" id="concept_tax" readonly></td>'+
                                '<td><input class="form-control text-right total_value_concept" type="number" name="total_value_concept" id="total_value_concept" readonly></td>'+
                            '</tr>';

            $('#credit_note_table tbody').append(table_row);
            $('[data-toggle="tooltip"]').tooltip();

            _allow_withholding_calculation = concept_withholdings;
        }
    }

    function validate_add_concept(reference, concept)
    {
        if (reference == "") {
            Command: toastr.error('Debe seleccionar la factura que se quiere afectar', '<?= lang("error"); ?>');
            return false;
        }

        if (concept == "") {
            Command: toastr.error('Debe seleccionar el concepto a agregar', '<?= lang("error"); ?>');
            return false;
        }

        if (_existing_concept == true) {
            Command: toastr.error('El concepto ya se encuentra agregado', '<?= lang("error"); ?>');
            return false;
        }

        _existing_concept = true;
        return true;
    }

    function delete_concept_row(row_control)
    {
        if (row_control === undefined) {
            $('#credit_note_table tbody tr').remove();
        } else {
            row_control.parents('tr').remove();
        }

        cancel_withholding_calculation();

        $('#total_sum_concept_value').text('0.00');
        $('#total_sum_concept_tax').text('0.00');
        $('#total_sum').text('0.00');
        $('#withholdings').val(0);

        _existing_concept = false;
    }

    function calculate_taxes(concept_value_input)
    {
        var tax_calculated_from_concept_note = calculate_tax_concept(concept_value_input);
        concept_value_input.parents('tr').find('#concept_tax').val(tax_calculated_from_concept_note);

        var total_calculated_from_concept_note = calculate_total_value_concept(concept_value_input);
        concept_value_input.parents('tr').find('#total_value_concept').val(total_calculated_from_concept_note);

        total_sums();
    }

    function total_sums()
    {
        var total_sum_concept_value = total_sum_concept_tax = total_sum_withholdings = total_sum = 0;
        $('.concept_value').each(function() {
            total_sum_concept_value += parseFloat($(this).val());
        });
        $('#total_sum_concept_value').text(total_sum_concept_value);

        $('.concept_tax').each(function() {
            total_sum_concept_tax += parseFloat($(this).val());
        });
        $('#total_sum_concept_tax').text(total_sum_concept_tax);

        $('.total_value_concept').each(function() {
            total_sum += parseFloat($(this).val());
        });
        $('#total_sum').text(total_sum);

        cancel_withholding_calculation();
        $('#withholdings').val(0);
    }

    function calculate_tax_concept(concept_value_input)
    {
        var concept_tax_rate = $('#concept option:selected').data('concept_tax_rate');
        var concept_value = concept_value_input.val();
        var calculate_tax_note_concept = 0;

        if (concept_tax_rate > 0) {
            calculate_tax_note_concept = (concept_value * concept_tax_rate) / 100;
        }

        return formatDecimal(calculate_tax_note_concept);
    }

    function calculate_total_value_concept(concept_value_input)
    {
        var concept_value = concept_value_input.val();
        var concept_tax = concept_value_input.parents('tr').find('#concept_tax').val();

        var total_value_concept = parseFloat(concept_value) + parseFloat(concept_tax);

        return total_value_concept;
    }

    function open_modal_retention_window(see_retention_button)
    {
        var concept_value = $('#concept_value').val();

        if (_allow_withholding_calculation == '1') {
            if (concept_value == 0 || concept_value == '' || concept_value === undefined) {
                Command: toastr.error('No se ha ingresado el valor base para el cálculo de las retenciones', '<?= lang("error"); ?>');
            } else {
                $('#rtModal').modal('show');
            }
        } else {
            Command: toastr.error('No está permitido el cálculo de retenciones', '<?= lang("error"); ?>');
        }
    }

    function save_credit_note_withholdings()
    {
        var total_withholding = $('#total_rete_amount').text();

        $('#withholdings').val(parseFloat(total_withholding));

        $('#rtModal').modal('hide');
    }

    function cancel_withholding_calculation()
    {
        $('.type_retention_check').iCheck('uncheck');

        total_value_withholding_type();
    }

    function add_credit_note()
    {
        if (validate_form_inputs()) {
            $('#add_credit_note_other_concepts_form').submit();
        }
    }

    function validate_form_inputs()
    {
        var total_sum = $('#total_sum').text();

        if (parseFloat(total_sum) == 0) {
            Command: toastr.error('No se puede crear La Nota Crédito. Por favor diligencie el formulario.', '<?= lang("error"); ?>');
            return false;
        }

        if (parseFloat(total_sum) >= $('#grand_total').val()) {
            Command: toastr.error('El total de la Nota crédito es mayor o igual al valor de la factura afectada.', '<?= lang("error"); ?>');
            return false;
        }

        return true;
    }







    function cargar_codigo_forma_pago_fe()
    {
        var codigo_fe_forma_pago = $('#paid_by_1 option:selected').data('code_fe');
        $('#mean_peyment_code_fe').val(codigo_fe_forma_pago);
    }

    function loadItems(tabindex = null)
    {

        if (localStorage.getItem('reitems')) {
            $('#currency').select2('readonly', true);
            subtotal = 0;
            total = 0;
            count = 1;
            an = 1;
            product_tax = 0;
            invoice_tax = 0;
            product_discount = 0;
            order_discount = 0;
            total_discount = 0;
            var lock_submit = false;
            var trmrate = 1;
            var key_log = localStorage.getItem('rekeylog');
            var recustomerspecialdiscount = localStorage.getItem('recustomerspecialdiscount');
            var return_type = "<?= isset($return_type) ? $return_type : 0; ?>";

            $("#reTable tbody").empty();
            reitems = JSON.parse(localStorage.getItem('reitems'));
            sortedItems = (site.settings.item_addition == 1) ? _.sortBy(reitems, function(o) { return [parseInt(o.order)]; }) : reitems;
            reordersale = localStorage.getItem('reordersale');
            if (reordersale || return_type == 2) {
                rquantity_readonly = 'readonly';
            } else {
                rquantity_readonly = '';
            }

            $('#edit_sale').attr('disabled', false);
            $.each(sortedItems, function () {
                var item = this;
                var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
                item.order = item.order ? item.order : new Date().getTime();
                var product_id = item.row.id,
                    item_type = item.row.type,
                    combo_items = item.combo_items,
                    item_qty = item.row.qty,
                    item_aqty = item.row.quantity,
                    item_tax_method = item.row.tax_method,
                    item_ds = item.row.discount,
                    item_discount = 0,
                    item_option = item.row.option,
                    item_code = item.row.code,
                    item_serial = item.row.serial,
                    item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;"),
                    product_unit = item.row.unit,
                    base_quantity = item.row.base_quantity,
                    unit_price = item.row.real_unit_price,
                    sale_item_id = item.row.sale_item_id,
                    item_price = item.row.net_unit_price,
                    pr_tax = item.tax_rate,
                    pr_tax_val = item.row.item_tax,
                    pr_tax_rate = pr_tax.rate,
                    pr_tax_id = pr_tax.id,
                    item_ds = item.row.discount,
                    item_discount =  item.row.item_discount;
                    if (pr_tax === false) {
                        var pr_tax_val = 0,
                            pr_tax_id = 1,
                            pr_tax_rate = 0;
                    }

                var sel_opt = '';
                $.each(item.options, function () {
                    if(this.id == item_option) {
                        sel_opt = this.name;
                    }
                });

                if (othercurrency = localStorage.getItem('othercurrency')) {
                    rate = localStorage.getItem('othercurrencyrate');
                    if (trm = localStorage.getItem('othercurrencytrm')) {
                        trmrate = rate / trm;
                    }
                }

                var row_no = item.id;
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
                tr_html = '<td>'+
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '">'+
                            '<input name="sale_item_id[]" type="hidden" class="slrid" value="' + sale_item_id + '">'+
                            '<input name="product_type[]" type="hidden" class="rtype" value="' + item_type + '">'+
                            '<input name="product_code[]" type="hidden" class="rcode" value="' + item_code + '">'+
                            '<input name="product_name[]" type="hidden" class="rname" value="' + item_name + '">'+
                            '<input name="product_option[]" type="hidden" class="roption" value="' + item_option + '">'+
                            '<span class="sname" id="name_' + row_no + '">' + item_code +' - '+ item_name +(sel_opt != '' ? ' ('+sel_opt+')' : '')+'</span> '+
                          '</td>';
                if (site.settings.product_serial == 1) {
                    tr_html += '<td class="text-right">'+
                                    '<input class="form-control input-sm rserial" name="serial[]" type="text" id="serial_' + row_no + '" value="'+item_serial+'">'+
                                '</td>';
                }
                tr_html += '<td class="text-right">'+
                                '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(parseFloat(item_price) + parseFloat(item_discount)) + '</span>'+
                            '</td>';
                if ((site.settings.product_discount == 1) || item_discount) {
                    tr_html += '<td class="text-right">'+
                                    '<input class="form-control input-sm rdiscount" name="product_discount[]" type="hidden" id="discount_' + row_no + '" value="' + item_ds + '">'+
                                    '<input class="form-control input-sm" name="product_discount_val[]" type="hidden" value="' + (item_discount * item_qty) + '">'+
                                    '<span class="text-right sdiscount text-danger" id="sdiscount_' + row_no + '">' +get_percentage_from_amount(item_ds, (item_price + item_discount))+ '</span>'+
                                '</td>';
                }
                tr_html += '<td class="text-right">'+
                                '<input class="form-control input-sm text-right rprice" name="net_price[]" type="hidden" id="price_' + row_no + '" value="' + item_price + '">'+
                                '<input class="ruprice" name="unit_price[]" type="hidden" value="' + unit_price + '">'+
                                '<input class="realuprice" name="real_unit_price[]" type="hidden" value="' + item.row.real_unit_price + '">'+
                                '<span class="text-right sprice" id="sprice_' + row_no + '">' + formatMoney(item_price) + '</span>'+
                            '</td>';

                if (site.settings.tax1 == 1) {
                    tr_html += '<td class="text-right">'+
                                    '<input class="form-control input-sm text-right rproduct_tax" name="product_tax[]" type="hidden" id="product_tax_' + row_no + '" value="' + pr_tax_id + '">'+
                                    '<input class="form-control input-sm text-right rproduct_tax" name="unit_product_tax[]" type="hidden" id="unit_product_tax_' + row_no + '" value="' + pr_tax_val + '">'+
                                    '<input class="form-control input-sm text-right" name="product_tax_rate[]" type="hidden" value="' + formatDecimal(pr_tax_rate) + '%">'+
                                    '<input class="form-control input-sm text-right" name="product_tax_val[]" type="hidden" value="' + pr_tax_rate + '">'+
                                    '<span class="text-right sproduct_tax" id="sproduct_tax_' + row_no + '">' + formatDecimal(pr_tax_rate) + '%' +
                                '</td>';
                }
                if (site.settings.tax1 == 1) {
                    tr_html += '<td class="text-right">'+
                                    '<span class="text-right">' + formatMoney(formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val))) * trmrate)) + '</span>'+
                                '</td>';
                }
                tr_html += '<td>'+
                                '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + item_qty + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();" '+rquantity_readonly+'>'+
                                '<input name="product_unit[]" type="hidden" class="runit" value="' + product_unit + '">'+
                                '<input name="product_base_quantity[]" type="hidden" class="rbase_quantity" value="' + base_quantity + '">'+
                                '<input name="product_aqty[]" type="hidden" class="item_aqty" value="' + item_aqty + '">'+
                            '</td>';
                tr_html += '<td class="text-right"><span class="text-right ssubtotal" id="subtotal_' + row_no + '">' + formatMoney(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty))) + '</span></td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip pointer redel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                newTr.html(tr_html);
                newTr.appendTo("#reTable");
                subtotal += formatDecimal(((parseFloat(item_price)) * parseFloat(item_qty)));
                total += formatDecimal(((parseFloat(item_price) + parseFloat(pr_tax_val)) * parseFloat(item_qty)));
                count += parseFloat(item_qty);
                product_tax += parseFloat(pr_tax_val * item_qty);
                product_discount += (item_discount * item_qty);
                an++;

                if (item_type == 'standard' && item.options !== false) {
                    $.each(item.options, function () {
                        if(this.id == item_option && base_quantity > this.quantity) {
                            $('#row_' + row_no).addClass('danger');
                            if(site.settings.overselling != 1) {
                                $('#edit_sale').attr('disabled', true);
                                lock_submit = true;
                            }
                        }
                    });
                } else if(item_type == 'standard' && base_quantity > item_aqty) {
                    $('#row_' + row_no).addClass('danger');
                    if(site.settings.overselling != 1) {
                        $('#edit_sale').attr('disabled', true);
                        lock_submit = true;
                    }
                } else if (item_type == 'combo') {
                    if(combo_items === false) {
                        $('#row_' + row_no).addClass('danger');
                        if(site.settings.overselling != 1) {
                            $('#edit_sale').attr('disabled', true);
                            lock_submit = true;
                        }
                    } else {
                        $.each(combo_items, function() {
                           if(parseFloat(this.quantity) < (parseFloat(this.qty)*base_quantity) && this.type == 'standard') {
                               $('#row_' + row_no).addClass('danger');
                                if(site.settings.overselling != 1) {
                                    $('#edit_sale').attr('disabled', true);
                                    lock_submit = true;
                                }
                           }
                       });
                    }
                }

            });

            var col = 2;
            if (site.settings.product_serial == 1) { col++; }
            var tfoot = '<tr id="tfoot" class="tfoot active">'+
                            '<th colspan="'+col+'">Total ('+(an - 1)+')</th>';

            if ((site.settings.product_discount == 1) || product_discount) {
                tfoot += '<th class="text-right">'+formatMoney(trmrate * product_discount)+'</th>';
            }
            tfoot +='<th class="text-right" id="subtotalS">'+formatMoney(trmrate * subtotal)+'</th>';
            if (site.settings.tax1 == 1) {
                console.log('PR TAX :'+product_tax);
                tfoot += '<th class="text-right" id="ivaamount">'+formatMoney(trmrate * product_tax)+'</th>';
            }
            if (site.settings.tax1 == 1) {
                tfoot += '<th class="text-right"></th>';
            }
            tfoot +='<th class="text-center">' + formatQty(parseFloat(count) - 1) + '</th>';
            tfoot += '<th class="text-right" id="totalS">'+formatMoney(trmrate * total)+'</th>'+
                     '<th class="text-center">'+
                     '  <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>'+
                     '</th>'+
                    '</tr>';
            $('#reTable tfoot').html(tfoot);

            // Order level discount calculations
            if (rediscount = localStorage.getItem('rediscount')) {
                var ds = rediscount;
                if (ds.indexOf("%") !== -1) {
                    var pds = ds.split("%");
                    if (!isNaN(pds[0])) {
                        // order_discount = formatDecimal((((total) * parseFloat(pds[0])) / 100));
                        order_discount = formatDecimal((((subtotal) * parseFloat(pds[0])) / 100));
                    } else {
                        order_discount = formatDecimal(ds);
                    }
                } else {
                    order_discount = formatDecimal(ds);
                }

                //total_discount += parseFloat(order_discount);
            }

            // Order level tax calculations
            if (site.settings.tax2 != 0) {
                if (retax2 = localStorage.getItem('retax2')) {
                    $.each(tax_rates, function () {
                        if (this.id == retax2) {
                            if (this.type == 2) {
                                invoice_tax = formatDecimal(this.rate);
                            } else if (this.type == 1) {
                                // invoice_tax = formatDecimal((((subtotal - order_discount) * this.rate) / 100));
                                invoice_tax = formatDecimal((((subtotal) * this.rate) / 100));
                            }
                        }
                    });
                }
            }

            // Retenciones
            if (retenciones = JSON.parse(localStorage.getItem('retenciones'))) {
                total_rete_amount = formatDecimal(retenciones.total_rete_amount);
                // console.log('Retención : '+total_rete_amount);
            } else {
                total_rete_amount = 0;
            }
            //Retenciones

            shipping = formatDecimal($('#reshipping').val());
            total_discount = parseFloat(order_discount + product_discount);
            // Totals calculations after item addition
            var gtotal = parseFloat(((parseFloat(total) + parseFloat(invoice_tax)) - parseFloat(order_discount)) + parseFloat(shipping));

            $('#amount_1').data('maxval', gtotal).prop('readonly', false);
            <?php if (isset($invoice_data)) { ?>
                <?php if ($invoice_data->payment_status == 'paid' || $invoice_data->payment_status == 'partial') { ?>
                    total_rete_amount = 0;
                    if (retenciones = JSON.parse(localStorage.getItem('retenciones'))) {
                        total_rete_amount = retenciones.total_rete_amount;
                    }
                    $('.amount_to_paid').val(formatMoney(gtotal - formatDecimal(total_rete_amount) - "<?= $amount_discounted ?>"));
                    $('#amount_1').val(formatDecimal(gtotal - formatDecimal(total_rete_amount) - "<?= $amount_discounted ?>"));
                    $('#discounted').val(formatDecimal("<?= $amount_discounted ?>"));
                <?php } ?>
            <?php } ?>


            $('#total').text(formatMoney(total));
            $('#titems').text((an - 1) + ' (' + formatQty(parseFloat(count) - 1) + ')');
            $('#total_items').val((parseFloat(count) - 1));
            $('#tds').text(formatMoney(order_discount));
            if (site.settings.tax2 != 0) {
                $('#ttax2').text(formatMoney(invoice_tax));
            }
            $('#tship').text(formatMoney(shipping));
            $('#gtotal').text(formatMoney(gtotal));
            if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
                // $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
                // $(window).scrollTop($(window).scrollTop() + 1);
            }
            if (count > 1) {
                $('#recustomer').select2("readonly", true);
                $('#recustomerbranch').select2("readonly", true);
                $('#rewarehouse').select2("readonly", true);
                $('#rediscount').prop("readonly", true);
                $('#reshipping').prop("readonly", true);
            }

            if (lock_submit) {
                localStorage.setItem('locked_for_items_quantity', 1);
            } else {
                if (localStorage.getItem('locked_for_items_quantity')) {
                    localStorage.removeItem('locked_for_items_quantity');
                }
            }
            if (retenciones = JSON.parse(localStorage.getItem('retenciones'))) {
                retenciones.gtotal = $('#gtotal').text();
                localStorage.setItem('retenciones', JSON.stringify(retenciones));
            }
            // verify_locked_submit();
            set_page_focus(tabindex);
            verify_payment_register();
        }
    }

    function obtener_documents_types()
    {
        var is_pos = "<?= isset($invoice_data) ? $invoice_data->pos : 0; ?>";

        if (is_pos == 1) {
            document_type = 3;
        } else if (is_pos == 0) {
            document_type = 4;
        }

        <?php if (isset($invoice_data)) { ?>
            $.ajax({
              url:'<?= admin_url("billers/getBillersDocumentTypes/") ?>'+document_type+'/'+"<?= $invoice_data->biller_id ?>",
              type:'get',
              dataType:'JSON'
            }).done(function(data){
              response = data;

              $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }

              if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
              }
                $('#document_type_id').trigger('change');
            });
        <?php } ?>
    }

    function validate_key_log()
    {
        var documnet_type_id = '<?= isset($invoice_data) ? $invoice_data->document_type_id : ""; ?>';

        if (documnet_type_id != '') {
            $.ajax({
                type: "get", async: false,
                url: site.base_url+"sales/validate_key_log/" + documnet_type_id,
                dataType: "json",
                success: function (data) {
                    localStorage.setItem('rekeylog', data);
                    if (data != "1") {
                        $('#slbiller').select2('readonly', true);
                        $('#document_type_id').select2('readonly', true);
                        command: toastr.warning('Tipo de documento cambiado, refresque o reinicie si quiere volver a uno normal.', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    }
                    loadItems();
                }
            });
        }
    }

    function customer_special_discount()
    {
        <?php if (isset($invoice_data)) { ?>
            var customer_id = '<?= $invoice_data->customer_id ?>';
            $.ajax({
                url : site.base_url+"customers/get_customer_special_discount/"+customer_id,
                dataType : "JSON"
            }).done(function(data){

                localStorage.setItem('recustomerspecialdiscount', data.special_discount);
                if (data.special_discount == 1) {
                        loadItems();
                        command: toastr.warning('Cliente con descuento especial asignado', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                }

            });
        <?php } ?>
    }

    $(document).on('click', '#rete_fuente', function(){
        if ($(this).is(':checked')) {
             $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
        }
    });

    $(document).on('click', '#rete_iva', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
        }
    });

    $(document).on('click', '#rete_ica', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
        }
    });

    $(document).on('click', '#rete_otros', function(){
        if ($(this).is(':checked')) {
            $.ajax({
                url: site.base_url+"sales/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
            }).fail(function(data){
                console.log(data);
            });
        } else {
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option', function(){
        rete_fuente_id = $(this).val();
        $('#rete_fuente_id').val(rete_fuente_id);
        trmraterete = 1;
        prevamnt = $('#rete_fuente_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_fuente_option option:selected').data('percentage');
        apply = $('#rete_fuente_option option:selected').data('apply');
        account = $('#rete_fuente_option option:selected').data('account');
        min_base = $('#rete_fuente_option option:selected').data('minbase');
        $('#rete_fuente_account').val(account);
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
            $('#rete_fuente_base').val(amount);
            cAmount = amount * (percentage / 100);
            if (trmraterete == 1 && site.settings.rounding == 1) {
                cAmount = Math.round(cAmount);
            } else {
                cAmount = formatDecimal(cAmount);
            }
            $('#rete_fuente_tax').val(percentage);
            $('#rete_fuente_valor').val(cAmount);
            rete_fuente_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_fuente_amount, '+');
    });

    $(document).on('change', '#rete_iva_option', function(){
        rete_iva_id = $(this).val();
        $('#rete_iva_id').val(rete_iva_id);
        trmraterete = 1;
        prevamnt = $('#rete_iva_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_iva_option option:selected').data('percentage');
        apply = $('#rete_iva_option option:selected').data('apply');
        account = $('#rete_iva_option option:selected').data('account');
        min_base = $('#rete_iva_option option:selected').data('minbase');
        $('#rete_iva_account').val(account);
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
            $('#rete_iva_base').val(amount);
            cAmount = amount * (percentage / 100);
            if (trmraterete == 1 && site.settings.rounding == 1) {
                cAmount = Math.round(cAmount);
            } else {
                cAmount = formatDecimal(cAmount);
            }
            $('#rete_iva_tax').val(percentage);
            $('#rete_iva_valor').val(cAmount);
            rete_iva_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_iva_amount, '+');
    });

    $(document).on('change', '#rete_ica_option', function(){
        rete_ica_id = $(this).val();
        $('#rete_ica_id').val(rete_ica_id);
        trmraterete = 1;
        prevamnt = $('#rete_ica_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_ica_option option:selected').data('percentage');
        apply = $('#rete_ica_option option:selected').data('apply');
        account = $('#rete_ica_option option:selected').data('account');
        min_base = $('#rete_ica_option option:selected').data('minbase');
        $('#rete_ica_account').val(account);
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
            $('#rete_ica_base').val(amount);
            cAmount = amount * (percentage / 100);
            if (trmraterete == 1 && site.settings.rounding == 1) {
                cAmount = Math.round(cAmount);
            } else {
                cAmount = formatDecimal(cAmount);
            }
            $('#rete_ica_tax').val(percentage);
            $('#rete_ica_valor').val(cAmount);
            rete_ica_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_ica_amount, '+');
    });

    $(document).on('change', '#rete_otros_option', function(){
        rete_otros_id = $(this).val();
        $('#rete_otros_id').val(rete_otros_id);
        trmraterete = 1;
        prevamnt = $('#rete_otros_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_otros_option option:selected').data('percentage');
        apply = $('#rete_otros_option option:selected').data('apply');
        account = $('#rete_otros_option option:selected').data('account');
        min_base = $('#rete_otros_option option:selected').data('minbase');
        $('#rete_otros_account').val(account);
        amount = formatDecimal(parseFloat(formatDecimal(getReteAmount(apply))));
        customer_validate_min_base_retention = localStorage.getItem('customer_validate_min_base_retention');
            $('#rete_otros_base').val(amount);
            cAmount = amount * (percentage / 100);
            if (trmraterete == 1 && site.settings.rounding == 1) {
                cAmount = Math.round(cAmount);
            } else {
                cAmount = formatDecimal(cAmount);
            }
            $('#rete_otros_tax').val(percentage);
            $('#rete_otros_valor').val(cAmount);
            rete_otros_amount = formatMoney(cAmount);
            setReteTotalAmount(rete_otros_amount, '+');
    });

    function getReteAmount(apply)
    {
        amount = 0;

        if (apply == "ST") {
            amount = $('#subtotalS').text();
        } else if (apply == "TX") {
            amount = $('#ivaamount').text();
        } else if (apply == "TO") {
            amount = $('#totalS').text();
        }

        return amount;
    }

    function setReteTotalAmount(amount, action)
    {
        if (action == "+") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) + formatDecimal(amount);
        } else if (action == "-") {
            tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) - formatDecimal(amount);
        }

        if (tra < 0) {
            tra = 0;
        }

        $('#total_rete_amount').text(formatMoney(tra));
    }

    $(document).on('click', '#updateOrderRete', function () {
        // var ts = $('#order_tax_input').val();
        // $('#sltax2').val(ts);

        $('#ajaxCall').fadeIn();

        if (rete_prev = JSON.parse(localStorage.getItem('retenciones'))) {
            var retenciones = {
            'gtotal' : rete_prev.gtotal,
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            };

            retenciones.total_discounted = formatMoney(parseFloat(formatDecimal(rete_prev.gtotal)) - parseFloat(formatDecimal($('#total_rete_amount').text())));
        } else {
            var retenciones = {
            'gtotal' : $('#gtotal').text(),
            'total_rete_amount' : $('#total_rete_amount').text(),
            'fuente_option' : $('#rete_fuente_option option:selected').data('percentage'),
            'id_rete_fuente' : $('#rete_fuente_option').val(),
            'iva_option' : $('#rete_iva_option option:selected').data('percentage'),
            'id_rete_iva' : $('#rete_iva_option').val(),
            'ica_option' : $('#rete_ica_option option:selected').data('percentage'),
            'id_rete_ica' : $('#rete_ica_option').val(),
            'otros_option' : $('#rete_otros_option option:selected').data('percentage'),
            'id_rete_otros' : $('#rete_otros_option').val(),
            };
            retenciones.total_discounted = formatMoney(parseFloat(formatDecimal($('#gtotal').text())) - parseFloat(formatDecimal($('#total_rete_amount').text())));
        }


        setTimeout(function() {
            $('#gtotal').text(retenciones.total_discounted);
            $('#ajaxCall').fadeOut();
        }, 1000);

        localStorage.setItem('retenciones', JSON.stringify(retenciones));
        // loadItems();

        if (parseFloat(formatDecimal(retenciones.total_rete_amount)) > 0) {
            $('#rete_applied').val(1);
            $('#gtotal_rete_amount').val(parseFloat(formatDecimal(retenciones.total_rete_amount)))
        } else {
            $('#rete_applied').val(0);
        }

        $('#rete').val(retenciones.total_rete_amount);
        $('#rtModal').modal('hide');
        // loadItems();
     });

    $('#rtModal').on('hidden.bs.modal', function () {
        // loadItems();
    });

     $(document).on('click', '#cancelOrderRete', function(){
        localStorage.removeItem('retenciones');
        loadItems();
        $('#rete_fuente').prop('checked', true).trigger('click');
        $('#rete_iva').prop('checked', true).trigger('click');
        $('#rete_ica').prop('checked', true).trigger('click');
        $('#rete_otros').prop('checked', true).trigger('click');
        $('#updateOrderRete').trigger('click');
     });

    function recalcular_retenciones(){

        retenciones = JSON.parse(localStorage.getItem('retenciones'));

        if (retenciones != null) {
            if (retenciones.id_rete_fuente != '' && retenciones.id_rete_fuente != null) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').trigger('click');
                }
            }

            if (retenciones.id_rete_iva != '' && retenciones.id_rete_iva != null) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').trigger('click');
                }
            }

            if (retenciones.id_rete_ica != '' && retenciones.id_rete_ica != null) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').trigger('click');
                }
            }

            if (retenciones.id_rete_otros != '' && retenciones.id_rete_otros != null) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').trigger('click');
                }
            }

            setTimeout(function() {

                $.each($('#rete_fuente_option option'), function(index, value){
                    if(retenciones.id_rete_fuente != '' && value.value == retenciones.id_rete_fuente){
                        $('#rete_fuente_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_iva_option option'), function(index, value){
                    if(retenciones.id_rete_iva != '' && value.value == retenciones.id_rete_iva){
                        $('#rete_iva_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_ica_option option'), function(index, value){
                    if(retenciones.id_rete_ica != '' && value.value == retenciones.id_rete_ica){
                        $('#rete_ica_option').select2('val', value.value).trigger('change');
                    }
                });

                $.each($('#rete_otros_option option'), function(index, value){
                    if(retenciones.id_rete_otros != '' && value.value == retenciones.id_rete_otros){
                        $('#rete_otros_option').select2('val', value.value).trigger('change');
                    }
                });

                $('#updateOrderRete').trigger('click');
                loadItems();
            }, 2000);
        }
    }

    function verify_payment_register(){
        paid = parseFloat(localStorage.getItem('reinvoice_paid'));
        payment_status = localStorage.getItem('repayment_status');
        sale_balance = parseFloat(localStorage.getItem('resale_balance'));
        total_rete_amount = 0;
        if (rete = JSON.parse(localStorage.getItem('retenciones'))) {
            total_rete_amount = parseFloat(formatDecimal(rete.total_rete_amount));
        }
        gtotal = parseFloat(formatDecimal($('#gtotal').text())) - total_rete_amount - "<?= (isset($amount_discounted)) ? $amount_discounted : 0; ?>";

        if (payment_status == 'partial') {
            if (gtotal > sale_balance ) {
                $('#payments').fadeIn();
                $('#amount_1').val(gtotal - sale_balance);
                $('.amount_to_paid').val(formatMoney(gtotal - sale_balance));
            } else {
                $('#payments').fadeOut();
                $('#amount_1').val(0);
                $('.amount_to_paid').val(formatMoney(0));
            }
        } else if (payment_status == 'pending' || payment_status == 'due') {
            $('#amount_1').val(0);
            $('.amount_to_paid').val(formatMoney(0));
        } else if (payment_status == 'paid') {
            $('#payments').fadeIn();
        }

        $('#discounted').val(formatDecimal("<?= isset($amount_discounted) ? $amount_discounted : 0; ?>"));
    }
</script>