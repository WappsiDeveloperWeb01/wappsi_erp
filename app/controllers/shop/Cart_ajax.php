<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Cart_ajax extends MY_Shop_Controller
{

    function __construct() {
        parent::__construct();
        if ($this->Settings->mmode) { redirect('notify/offline'); }
        $this->load->admin_model('companies_model');
        $this->load->admin_model('sales_model');
        $this->data['countries'] = $this->companies_model->getCountries();
        $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
        $this->data["types_vat_regime"] = $this->site->get_types_vat_regime();
        $this->data["types_obligations"] = $this->site->get_types_obligations(); 
    }

    function index() {
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }
        $this->data['page_title'] = lang('shopping_cart');
        $this->page_construct('pages/cart', $this->data);
    }

    function checkout() {
        $this->session->set_userdata('requested_page', $this->uri->uri_string());
        if ($this->cart->total_items() < 1) {
            $this->session->set_flashdata('reminder', lang('cart_is_empty'));
            shop_redirect('products');
        }
        $cant_finish_sale_documents = false;
        if ($this->shop_settings->sale_destination == 1) {
            if (empty($this->shop_settings->biller_data->detal_document_type_default) || empty($this->shop_settings->biller_data->pos_document_type_default)) {
                $cant_finish_sale_documents = true;
            }
        } else if ($this->shop_settings->sale_destination == 2) {
            if (empty($this->shop_settings->biller_data->order_sales_document_type_default)) {
                $cant_finish_sale_documents = true;
            }
        }
        $this->data['cant_finish_sale_documents'] = $cant_finish_sale_documents;
        $this->data['paypal'] = $this->shop_model->getPaypalSettings();
        $this->data['skrill'] = $this->shop_model->getSkrillSettings();
        $this->data['addresses'] = $this->loggedIn ? $this->shop_model->getAddresses() : FALSE;
        $this->data['page_title'] = lang('checkout');
        $this->page_construct('pages/checkout', $this->data);
    }

    function add($product_id) {
        if ($this->input->is_ajax_request() || $this->input->post('qty')) {
            $product = $this->shop_model->getProductForCart($product_id);
            $options = $this->shop_model->getProductVariants($product_id);
            if (
                $this->Settings->prioridad_precios_producto == 5 ||
                $this->Settings->prioridad_precios_producto == 7 ||
                $this->Settings->prioridad_precios_producto == 10 ||
                $this->Settings->prioridad_precios_producto == 11
            ) {
                $data['unit_id'] = $this->input->get_post('unit');
                $data['product_id'] = $product_id;
                $data['quantity'] = $this->input->get_post('qty');
                $price = $this->shop_model->get_item_unit_price($data);
            } else {
                $new_price = $this->site->get_item_price($product, $this->customer, $this->customer_group, $this->shop_settings->biller_data, $this->customer_main_address->id, $unit_price_id = null);
                $price = $new_price['new_price'];
            }
            $option = FALSE;
            if (!empty($options)) {
                if ($this->input->get_post('option')) {
                    foreach ($options as $op) {
                        if ($op['id'] == $this->input->get_post('option')) {
                            $option = $op;
                        }
                    }
                } else {
                    $option = array_values($options)[0];
                }
                $price = $option['price']+$price;
            }
            $selected = $option ? $option['id'] : FALSE;
            if ($this->checkProductStock($product, 1, $selected) && $this->shop_settings->overselling == 0) {
                if ($this->input->is_ajax_request()) {
                    $this->sma->send_json(['error' => 1, 'message' => lang('item_out_of_stock')]);
                } else {
                    $this->session->set_flashdata('error', lang('item_out_of_stock'));
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
            $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
            $ctax = $this->site->calculateTax($product, $tax_rate, $price);
            $tax = $this->sma->formatDecimal($ctax['amount']);
            $price = $this->sma->formatDecimal($price);
            $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);
            $id = $this->Settings->item_addition ? md5($product->id) : md5(microtime());

            $quantity = $this->input->get_post('qty');
            if ($this->input->get_post('unit')) {
                $unit_details = $this->site->getUnitById($this->input->get_post('unit'));
                if ($unit_details->operator == "*") {
                    $quantity = $quantity * $unit_details->operation_value;
                } else if ($unit_details->operator == "/") {
                    $quantity = $quantity / $unit_details->operation_value;
                }
            }

            $data = array(
                'id'            => $id,
                'product_id'    => $product->id,
                'qty'           => $quantity,
                'name'          => $product->name,
                'unit_id'       => $this->input->get_post('unit'),
                'slug'          => $product->slug,
                'code'          => $product->code,
                'price'         => $unit_price,
                'tax'           => $tax,
                'image'         => $product->image,
                'option'        => $selected,
                'options'       => !empty($options) ? $options : NULL,
                'preferences'   => $this->input->get_post('preferences'),
            );
            if (
                    $this->Settings->prioridad_precios_producto == 7 || 
                    $this->Settings->prioridad_precios_producto == 10 || 
                    $this->Settings->prioridad_precios_producto == 11
                    ) {
                    $data['units'] = $this->site->get_product_units($product->id);
                }
            if ($this->cart->insert($data)) {
                if ($this->input->post('qty')) {
                    $this->session->set_flashdata('message', lang('item_added_to_cart'));
                    redirect($_SERVER['HTTP_REFERER']);
                } else {
                    $this->cart->cart_data();
                }
            }
            $this->session->set_flashdata('error', lang('unable_to_add_item_to_cart'));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function update($data = NULL) {
        if (is_array($data)) {
            return $this->cart->update($data);
        }
        if ($this->input->is_ajax_request()) {
            if ($rowid = $this->input->post('rowid', TRUE)) {
                $item = $this->cart->get_item($rowid);
                $product = $this->shop_model->getProductForCart($item['product_id']);
                $options = $this->shop_model->getProductVariants($product->id);
                if (
                    $this->Settings->prioridad_precios_producto == 5 ||
                    $this->Settings->prioridad_precios_producto == 7 ||
                    $this->Settings->prioridad_precios_producto == 10 ||
                    $this->Settings->prioridad_precios_producto == 11
                ) {
                    $data['unit_id'] = $this->input->post('unit');
                    $data['product_id'] = $item['product_id'];
                    $data['quantity'] = $this->input->post('qty');
                    $price = $this->shop_model->get_item_unit_price($data);
                } else {
                    $new_price = $this->site->get_item_price($product, $this->customer, $this->customer_group, $this->shop_settings->biller_data, $this->customer_main_address->id, $unit_price_id = null);
                    $price = $new_price['new_price'];
                }
                if ($option = $this->input->post('option')) {
                    foreach($options as $op) {
                        if ($op['id'] == $option) {
                            $price = $price + $op['price'];
                        }
                    }
                }
                $selected = $this->input->post('option') ? $this->input->post('option', TRUE) : FALSE;
                if ($this->checkProductStock($product, $this->input->post('qty', TRUE), $selected)  && $this->shop_settings->overselling == 0) {
                    if ($this->input->is_ajax_request()) {
                        $this->sma->send_json(['error' => 1, 'message' => lang('item_stock_is_less_then_order_qty')]);
                    } else {
                        $this->session->set_flashdata('error', lang('item_stock_is_less_then_order_qty'));
                        redirect($_SERVER['HTTP_REFERER']);
                    }
                }

                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                $ctax = $this->site->calculateTax($product, $tax_rate, $price);
                $tax = $this->sma->formatDecimal($ctax['amount']);
                $price = $this->sma->formatDecimal($price);
                $unit_price = $this->sma->formatDecimal($product->tax_method ? $price+$tax : $price);

                $data = array(
                    'rowid' => $rowid,
                    'price' => $price,
                    'tax' => $tax,
                    'unit_id' => $this->input->post('unit'),
                    'qty' => $this->input->post('qty', TRUE),
                    'option' => $selected,
                );
                if ($this->cart->update($data)) {
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_updated')));
                }
            }
        }
    }

    function remove($rowid = NULL) {
        if ($rowid) {
            return $this->cart->remove($rowid);
        }
        if ($this->input->is_ajax_request()) {
            if ($rowid = $this->input->post('rowid', TRUE)) {
                if ($this->cart->remove($rowid)) {
                    $this->sma->send_json(array('cart' => $this->cart->cart_data(true), 'status' => lang('success'), 'message' => lang('cart_item_deleted')));
                }
            }
        }
    }

    function destroy() {
        if ($this->input->is_ajax_request()) {
            if ($this->cart->destroy()) {
                $this->session->set_flashdata('message', lang('cart_items_deleted'));
                $this->sma->send_json(array('redirect' => base_url()));
            } else {
                $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured')));
            }
        }
    }

    function add_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
        if ($this->shop_model->getWishlist(TRUE) >= 10) {
            $this->sma->send_json(array('status' => lang('warning'), 'message' => lang('max_wishlist'), 'level' => 'warning'));
        }
        if ($this->shop_model->addWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('added_wishlist'), 'total' => $total));
        } else {
            $this->sma->send_json(array('status' => lang('info'), 'message' => lang('product_exists_in_wishlist'), 'level' => 'info'));
        }
    }

    function remove_wishlist($product_id) {
        $this->session->set_userdata('requested_page', $_SERVER['HTTP_REFERER']);
        if (!$this->loggedIn) { $this->sma->send_json(array('redirect' => site_url('login'))); }
        if ($this->shop_model->removeWishlist($product_id)) {
            $total = $this->shop_model->getWishlist(TRUE);
            $this->sma->send_json(array('status' => lang('success'), 'message' => lang('removed_wishlist'), 'total' => $total));
        } else {
            $this->sma->send_json(array('status' => lang('error'), 'message' => lang('error_occured'), 'level' => 'error'));
        }
    }

    private function checkProductStock($product, $qty, $option_id = null) {
        if ($product->type == 'service' || $product->type == 'digital') {
            return false;
        }
        $chcek = [];
        if ($product->type == 'standard') {
            $quantity = 0;
            if ($pis = $this->site->getPurchasedItems($product->id, $this->shop_settings->warehouse, $option_id)) {
                foreach ($pis as $pi) {
                    $quantity += $pi->quantity_balance;
                }
            }
            $chcek[] =  ($qty < $quantity);
        } elseif ($product->type == 'combo') {
            $combo_items = $this->site->getProductComboItems($product->id, $this->shop_settings->warehouse);
            foreach ($combo_items as $combo_item) {
                if ($combo_item->type == 'standard') {
                    $quantity = 0;
                    if ($pis = $this->site->getPurchasedItems($combo_item->id, $this->shop_settings->warehouse, $option_id)) {
                        foreach ($pis as $pi) {
                            $quantity += $pi->quantity_balance;
                        }
                    }
                    $chcek[] = (($combo_item->qty*$qty) < $quantity);
                }
            }
        }
        return empty($chcek) || in_array(false, $chcek);
    }
}
