<?php

use Psr\Log\NullLogger;

 defined('BASEPATH') or exit('No direct script access allowed');

class Products extends MY_Controller
{
    public $data = [];
    public $typesProducts = [];

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->lang->admin_load('products', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('products_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('purchases_model');
        $this->load->admin_model("TaxRates_model");

        $this->load->integration_model('connection');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->popup_attributes = array('width' => '900', 'height' => '600', 'window_name' => 'popup', 'menubar' => 'yes', 'scrollbars' => 'yes', 'status' => 'no', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0');
        $this->typesProducts =  [
            ['id' => 'standard', 'name' => $this->lang->line('standard')],
            ['id' => 'combo', 'name' => $this->lang->line('combo')],
            ['id' => 'digital', 'name' => $this->lang->line('digital')],
            ['id' => 'service', 'name' => $this->lang->line('service')],
            ['id' => 'raw', 'name' => $this->lang->line('raw')],
            ['id' => 'subproduct', 'name' => $this->lang->line('subproduct')],
            ['id' => 'pfinished', 'name' => $this->lang->line('pfinished')]
        ];
    }

    public function index($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();
        $this->session->unset_userdata('billers_assoc_created');
        $this->data['warehouse_id'] = $warehouse_id;
        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['typesProducts'] = json_decode(json_encode($this->typesProducts));
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['taxes'] = $this->site->getAllTaxRates();
        $this->data['units'] = $this->site->getAllBaseUnits();
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        $this->data['supplier'] = $this->input->get('supplier') ? $this->site->getCompanyByID($this->input->get('supplier')) : NULL;
        $this->data['variants'] = $this->products_model->getAllVariants();

        if ($this->input->post("producto")) {
            $this->data["producto"] = $this->input->post("producto");
        }
        if ($this->input->post("descripcion")) {
            $this->data["descripcion"] = $this->input->post("descripcion");
        }
        if ($this->input->post("discontinued")) {
            $this->data["discontinued"] = $this->input->post("discontinued");
        }
        if ($this->input->post('category')) {
            $this->data['subcategories'] = $this->products_model->getSubCategories($this->input->post('category'));
        }
        if ($this->input->post('tax_rate') || $this->input->post('unit') || $this->input->post('hidden') || $this->input->post('has_multiple_units') || $this->input->post('has_variants') || $this->input->post('has_preferences') || $this->input->post('product_detail') || $this->input->post('variants') || $this->input->post('reference') ) {
            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('products/index', ['page_title' => lang('products')], $this->data);
    }

    public function getProducts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);

        $type = $this->input->post('type') ? $this->input->post('type') : NULL;
        $unit = $this->input->post('unit') ? $this->input->post('unit') : NULL;
        $brand = $this->input->post('brand') ? $this->input->post('brand') : NULL;
        $hidden = $this->input->post('hidden') ? $this->input->post('hidden') : NULL;
        $variants = $this->input->post('variants') ? $this->input->post('variants') : null;
        $category = $this->input->post('category') ? $this->input->post('category') : NULL;
        $producto = $this->input->post('producto') ? $this->input->post('producto') : NULL;
        $tax_rate = $this->input->post('tax_rate') ? $this->input->post('tax_rate') : NULL;
        $biller_id = $this->input->post('biller_id') ? $this->input->post('biller_id') : NULL;
        $get_json = $this->input->post('get_json') != '' ? $this->input->post('get_json') : NULL;
        $subcategory = $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL;
        $warehouse_id = $this->input->post('warehouse_id') ? $this->input->post('warehouse_id') : NULL;
        $has_variants = $this->input->post('has_variants') ? $this->input->post('has_variants') : NULL;
        $product_detail = $this->input->post('product_detail') ? $this->input->post('product_detail') : NULL;
        $has_preferences = $this->input->post('has_preferences') ? $this->input->post('has_preferences') : NULL;
        $has_multiple_units = $this->input->post('has_multiple_units') ? $this->input->post('has_multiple_units') : NULL;
        $synchronized_store = $this->input->post('synchronized_store')  ? $this->input->post('synchronized_store') : NULL;
        $reference = $this->input->post('reference')  ? $this->input->post('reference') : NULL;
        $option_filter = $this->input->post('option_filter') != 'false' ? $this->input->post('option_filter') : 'active';

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id && $this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        $detail_link = anchor('admin/products/view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('product_details'));
        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_product") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . admin_url('products/delete/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> "
            . lang('delete_product') . "</a>";

        $set_featuring = "<a href='#' class='tip po' title='<b>" . $this->lang->line("set_featuring") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-set_featuring' id='a__$1' href='" . admin_url('products/set_featuring/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa  fa-star-half-stroke\"></i> "
            . lang('set_featuring') . "</a>";
        $single_barcode = anchor('admin/products/print_barcodes/$1', '<i class="fa fa-print"></i> ' . lang('print_barcode_label'));
        $inactive = "<li>
                        <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='$7/$1'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"$5\"></i>
                            $6
                        </a>
                    </li";
        $action = '<div class="text-center">
                        <div class="btn-group text-left">
            <button type="button" class="btn btn-default btn-xs new-button new-button-sm dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
            <i class="fas fa-ellipsis-v fa-lg"></i></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>' . $detail_link . '</li>
                                ' . ($this->Admin || $this->Owner || $this->GP['products-add'] ? '<li><a href="' . admin_url('products/add/$1') . '"><i class="fa fa-plus-square"></i> ' . lang('duplicate_product') . '</a></li>' : '') . '
                                ' . ($this->Admin || $this->Owner || $this->GP['products-edit'] ? '<li><a href="' . admin_url('products/edit/$1') . '"><i class="fa fa-edit"></i> ' . lang('edit_product') . '</a></li>' : '');
        if ($warehouse_id) {
            $action .= '<li><a href="' . admin_url('products/set_rack/$1/' . $warehouse_id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-bars"></i> '
                . lang('set_rack') . '</a></li>';
        }
        $action .= '<li><a href="' . base_url() . 'assets/uploads/$2" data-type="image" data-toggle="lightbox"><i class="fa fa-file-photo-o"></i> '
            . lang('view_image') . '</a></li>
                    <li>' . $single_barcode . '</li>
                    <li>' . $set_featuring . '</li>
                    <li class="divider"></li>
                    ' . ($this->Admin || $this->Owner || $this->GP['products-delete'] ? '<li>' . $delete_link . '</li>' : '') . '
                    ' . ($this->Admin || $this->Owner || $this->GP['products-delete'] ? '<li>' . $inactive . '</li>' : '') . '
                    </ul>
                </div></div>';
        $data = [
            'option_filter'     => $option_filter,
            'warehouse_id'      => $warehouse_id,
            'biller_id'         => $biller_id,
            'producto'          => $producto,
            'product_detail'    => $product_detail,
            'action'            => $action,
            'get_json'          => $get_json,
            'type'              => $type,
            'brand'             => $brand,
            'category'          => $category,
            'subcategory'       => $subcategory,
            'tax_rate'          => $tax_rate,
            'unit'              => $unit,
            'hidden'            => $hidden,
            'has_multiple_units' => $has_multiple_units,
            'has_variants'      => $has_variants,
            'has_preferences'   => $has_preferences,
            'synchronized_store' => $synchronized_store,
            'variants'          => $variants,
            'reference'         => $reference,
        ];
        if ($get_json) {
            $data['option_filter'] = 'active';
            $active = $this->get_products_result($data);
            $data['option_filter'] = 'inactive';
            $inactive = $this->get_products_result($data);
            $data['option_filter'] = 'promotion';
            $promotion = $this->get_products_result($data);
            $data['option_filter'] = 'featured';
            $featured = $this->get_products_result($data);
            $data['option_filter'] = 'new';
            $new = $this->get_products_result($data);
            $data['option_filter'] = 'alert';
            $alert = $this->get_products_result($data);
            $data['option_filter'] = false;
            $all = $this->get_products_result($data);
            echo json_encode(['active' => $active, 'inactive' => $inactive, 'promotion' => $promotion, 'featured' => $featured, 'new' => $new, 'alert' => $alert, 'all' => $all]);
        } else {
            echo $this->get_products_result($data);
        }
    }

    public function get_products_result($data)
    {
        $this->load->library('datatables');

        $unit = $data['unit'];
        $type = $data['type'];
        $brand = $data['brand'];
        $action = $data['action'];
        $hidden = $data['hidden'];
        $producto = $data['producto'];
        $get_json = $data['get_json'];
        $category = $data['category'];
        $tax_rate = $data['tax_rate'];
        $biller_id = ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) ? $data['biller_id'] : $this->session->userdata('biller_id');
        $subcategory = $data['subcategory'];
        $warehouse_id = $data['warehouse_id'];
        $has_variants = $data['has_variants'];
        $option_filter = $data['option_filter'];
        $product_detail = $data['product_detail'];
        $has_preferences = $data['has_preferences'];
        $has_multiple_units = $data['has_multiple_units'];
        $synchronized_store = $data['synchronized_store'];
        $variants = $data['variants'];
        $reference = $data['reference'];
        if ($biller_id) {
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }

        $this->datatables
            ->select($this->db->dbprefix('products') . ".id as productid,
                {$this->db->dbprefix('products')}.image as image,
                {$this->db->dbprefix('products')}.code as code,
                {$this->db->dbprefix('products')}.reference as reference,
                {$this->db->dbprefix('products')}.name as name,
                {$this->db->dbprefix('products')}.type,
                {$this->db->dbprefix('brands')}.name as brand,
                {$this->db->dbprefix('categories')}.name as cname,
                {$this->db->dbprefix('tax_rates')}.name as tax_rate,
                " .
                ($this->Admin || $this->Owner || $this->GP['products-cost'] ? "({$this->db->dbprefix('products')}.cost+COALESCE(consumption_purchase_tax,0)) as cost," : "") .
                ($this->Admin || $this->Owner || $this->GP['products-price'] ?
                    "(
                    IF(
                        {$this->db->dbprefix('product_prices')}.price > 0,
                        {$this->db->dbprefix('product_prices')}.price,
                        {$this->db->dbprefix('products')}.price
                    ) " . ($this->Settings->ipoconsumo ?
                        " + IF(
                                    {$this->db->dbprefix('products')}.tax_method = 0,
                                    COALESCE({$this->db->dbprefix('products')}.consumption_sale_tax, 0),
                                    0
                                )"
                        :
                        ""
                    ) . "
                ) as price," : "") .
                ($this->Admin || $this->Owner || $this->GP['products-quantity'] ?
                    ($warehouse_id ? "COALESCE({$this->db->dbprefix('warehouses_products')}.quantity, 0) as quantity," : "COALESCE({$this->db->dbprefix('products')}.quantity, 0) as quantity,")
                    : "") .
                "
                {$this->db->dbprefix('units')}.code as unit,
                '' as rack,
                alert_quantity,
                {$this->db->dbprefix('products')}.product_details,
                {$this->db->dbprefix('products')}.discontinued,
            if(discontinued = 0, 'fas fa-user-slash', 'fas fa-user-check') as faicon,
            if(discontinued = 0, '" . lang('deactivate_product') . "', '" . lang('activate_product') . "') as detailaction,
            if(discontinued = 0, '" . admin_url('products/deactivate') . "', '" . admin_url('products/activate') . "') as linkaction", FALSE)
            ->from('products')
            ->join('categories', 'products.category_id=categories.id', 'left')
            ->join('units', 'products.unit=units.id', 'left')
            ->join('brands', 'products.brand=brands.id', 'left');
        if ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) {
        } else {
        }
        $this->datatables->group_by("products.id");
        if ($biller_id) {
            // $this->datatables->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
            $this->datatables->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
            $this->datatables->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
            $this->datatables
                ->join('price_groups', 'price_groups.id = biller_data.default_price_group', 'left')
                ->join('product_prices', 'product_prices.product_id = products.id AND product_prices.price_group_id = price_groups.id', 'left');
        } else {
            $this->datatables
                ->join('product_prices', 'products.id=product_prices.product_id', 'left')
                ->join('price_groups', 'product_prices.price_group_id=price_groups.id AND price_groups.price_group_base = 1', 'inner');
        }
        if ($warehouse_id) {
            $this->datatables->join('warehouses_products', 'warehouses_products.product_id = products.id AND warehouses_products.warehouse_id = '.$warehouse_id.($this->Settings->display_all_products == 1 ? "" : " AND warehouses_products.quantity > 0 " ), 'left');
        }

        if (!$this->Owner && !$this->Admin) {
            if (!$this->GP['products-cost']) {
                $this->datatables->unset_column("cost");
            }
            if (!$this->GP['products-price']) {
                $this->datatables->unset_column("price");
            }
        }

        if (isset($product_detail) && !empty($product_detail)) {
            $this->datatables->like("{$this->db->dbprefix('products')}.product_details", $product_detail);
        }
        if (isset($type) && !empty($type)) {
            $this->datatables->where("{$this->db->dbprefix('products')}.type", $type);
        }
        if (isset($brand) && !empty($brand)) {
            $this->datatables->where("{$this->db->dbprefix('products')}.brand", $brand);
        }
        if (isset($subcategory) && !empty($subcategory)) {
            $this->datatables->where("{$this->db->dbprefix('products')}.subcategory_id", $subcategory);
        } else {
            if (isset($category) && !empty($category)) {
                $this->datatables->where("{$this->db->dbprefix('products')}.category_id", $category);
            }
        }
        if (isset($tax_rate) && !empty($tax_rate)) {
            $this->datatables->where("{$this->db->dbprefix('tax_rates')}.id", $tax_rate);
        }
        if (isset($unit) && !empty($unit)) {
            $this->datatables->where("{$this->db->dbprefix('units')}.id", $unit);
        }
        if (isset($hidden) && !empty($hidden)) {
            $hiddens = explode(',', $hidden);
            $hiddenConditions = '';
            foreach ($hiddens as $key => $hidden) {
                if ($hidden == 1) {
                    $hiddenConditions .= "{$this->db->dbprefix('products')}.hide_detal = 1";
                }
                if ($hidden == 2) {
                    if (!empty($hiddenConditions)) {
                        $hiddenConditions .= " OR {$this->db->dbprefix('products')}.hide_pos = 1";
                    } else {
                        $hiddenConditions .= "{$this->db->dbprefix('products')}.hide_pos = 1";
                    }
                }
                if ($hidden == 3) {
                    if (!empty($hiddenConditions)) {
                        $hiddenConditions .= " OR {$this->db->dbprefix('products')}.hide_online_store = 1";
                    } else {
                        $hiddenConditions .= "{$this->db->dbprefix('products')}.hide_online_store = 1";
                    }
                }
            }

            $this->datatables->where("({$hiddenConditions})");
        }
        if (isset($has_multiple_units) && !empty($has_multiple_units)) {
            $this->datatables->where("{$this->db->dbprefix('products')}.has_multiple_units", $has_multiple_units);
        }
        if (isset($has_variants) && !empty($has_variants)) {
            $this->datatables->where("{$this->db->dbprefix('products')}.has_variants", $has_variants);

            if (isset($variants) && !empty($variants)) {
                $variants = explode(',', $variants);
                $this->datatables->join('product_variants', 'product_variants.product_id = products.id', 'inner');
                $this->datatables->where_in("{$this->db->dbprefix('product_variants')}.name", $variants);
            }
        }
        if (isset($has_preferences) && !empty($has_preferences)) {
            $this->datatables->where("{$this->db->dbprefix('products')}.has_preferences", $has_preferences);
        }
        if (isset($synchronized_store) && $synchronized_store != '') {
            $this->datatables->where("{$this->db->dbprefix('products')}.synchronized_store", $synchronized_store);
        }

        if (isset($reference) && $reference != '') {
            $this->datatables->where("{$this->db->dbprefix('products')}.reference", $reference);
        }

        if ($option_filter == 'active') {
            $this->datatables->where('products.discontinued', 0);
        } else if ($option_filter == 'inactive') {
            $this->datatables->where('products.discontinued', 1);
        } else if ($option_filter == 'promotion') {
            $this->datatables->where('products.promotion', 1);
            $this->datatables->where('(products.start_date <= "' . date('Y-m-d') . '" AND products.end_date >= "' . date('Y-m-d') . '")');
        } else if ($option_filter == 'featured') {
            $this->datatables->where('products.featured', 1);
        } else if ($option_filter == 'new') {
            $this->datatables->where('DATEDIFF("' . date('Y-m-d H:i:s') . '", ' . $this->db->dbprefix('products') . '.registration_date) <=', $this->Settings->days_to_new_product);
        } else if ($option_filter == 'alert') {
            $this->datatables->where('products.quantity <= products.alert_quantity AND products.alert_quantity > 0');
        }

        $this->datatables->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
            ->add_column("Actions", $action, "productid, image, code, name, faicon, detailaction, linkaction");
        $this->datatables->unset_column('faicon');
        $this->datatables->unset_column('detailaction');
        $this->datatables->unset_column('linkaction');
        if ($get_json) {
            $q = $this->datatables->get_result_num_rows();
            $this->site->delete_temporary_product_billers_assoc();
            return $q;
        } else {

            $q = $this->datatables->generate();
            $this->site->delete_temporary_product_billers_assoc();
            return $q;
        }
    }

    function set_rack($product_id = NULL, $warehouse_id = NULL)
    {
        $this->sma->checkPermissions('edit', true);

        $this->form_validation->set_rules('rack', lang("rack_location"), 'trim|required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'rack' => $this->input->post('rack'),
                'product_id' => $product_id,
                'warehouse_id' => $warehouse_id,
            );
        } elseif ($this->input->post('set_rack')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("products/" . $warehouse_id);
        }

        if ($this->form_validation->run() == true && $this->products_model->setRack($data)) {
            $this->session->set_flashdata('message', lang("rack_set"));
            admin_redirect("products/" . $warehouse_id);
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['product'] = $this->site->getProductByID($product_id);
            $wh_pr = $this->products_model->getProductQuantity($product_id, $warehouse_id);
            $this->data['rack'] = $wh_pr['rack'];
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'products/set_rack', $this->data);
        }
    }

    function barcode($product_code = NULL, $bcs = 'code128', $height = 40)
    {
        if ($this->Settings->barcode_img) {
            header('Content-Type: image/png');
        } else {
            header('Content-type: image/svg+xml');
        }
        echo $this->sma->barcode($product_code, $bcs, $height, true, false, true);
    }

    function print_barcodes($product_id = NULL, $mov_id = NULL, $mov_type = NULL)
    {
        $this->sma->checkPermissions('barcode', true);
        $this->form_validation->set_rules('style', lang("style"), 'required');
        $pdf_print = false;
        $this->session->unset_userdata('barcodes');
        $this->session->unset_userdata('barcodes_style');
        $custom_fields = $this->site->get_custom_fields(2);
        $this->data["custom_fields"] = $custom_fields;
        if ($this->form_validation->run() == true) {
            $style = $this->input->post('style');
            if (strpos($style, "zebra") !== false || strpos($style, "ticket") !== false) {
                $pdf_print = true;
            }
            $bci_size = ($style == 10 || $style == 12 ? 50 : ($style == 14 || $style == 18 ? 30 : 20));
            $currencies = $this->site->getAllCurrencies();

            $archivo = false;
            if (isset($_FILES["xls_file"])) {
                $archivo = $_FILES['xls_file']['tmp_name'];
            }
            if ($archivo) {
                $show_variants_quantity = $this->input->post('show_variants_quantity');
                $this->load->library('excel');
                $invalids_products = false;
                $objPHPExcel = PHPExcel_IOFactory::load($archivo);
                $hojas = $objPHPExcel->getWorksheetIterator();
                foreach ($hojas as $hoja) {
                    $data = $hoja->toArray();
                    unset($data[0]);
                    foreach ($data as $data_row) {
                        if (empty($data_row[0])) {
                            continue;
                        }
                        $product_excel['code'] = $data_row[0];
                        $product_excel['variant_code'] = $data_row[1];
                        $product_excel['quantity'] = $data_row[2];
                        $product_excel_data = $this->products_model->get_product_by_code_reference($product_excel);
                        if (!$product_excel_data) {
                            $this->session->set_flashdata('error', 'Error con el código "'.$data_row[0].'", por favor verifique que exista en el sistema');
                            admin_redirect("products/print_barcodes");
                        }
                        $pid = $product_excel_data['id'];
                        $quantity = $product_excel['quantity'];
                        $product = $this->products_model->getProductWithCategory($pid);
                        $punit_data = $this->site->getUnitByID($product->unit);
                        $product->unit = $punit_data ? $punit_data->name : 'N/A';
                        $product->price = $this->input->post('check_promo') ? ($product->promotion ? $product->promo_price : $product->price) : $product->price;
                        $biller_id = $this->input->post('biller');
                        $warehouse_id = $this->input->post('warehouse');
                        $purchase_date = $this->input->post('purchase_date');
                        $biller = $this->site->getCompanyByID($biller_id);
                        $price = $this->site->get_product_biller_price_group($pid, $biller_id);
                        $price = $price ? $price->price : $product->price;
                        $option = $this->site->get_product_variant_by_code($product_excel['variant_code']);
                        if (!$option) {
                            $this->session->set_flashdata('error', 'Error con el código "'.$product_excel['variant_code'].'", por favor verifique que exista en el sistema');
                            admin_redirect("products/print_barcodes");
                        }
                        $barcode = array(
                            'site' => $this->input->post('site_name') ? ($biller ? $biller->company : $this->Settings->site_name) : FALSE,
                            'name' => $this->input->post('product_name') ? $product->name : FALSE,
                            'color' => $this->input->post('product_name') ? $product->colors : FALSE,
                            'reference' => $this->input->post('reference') ? $product->reference : FALSE,
                            'image' => $this->input->post('product_image') ? $product->image : FALSE,
                            'barcode' => ($this->input->post('barcode') || $this->input->post('barcode_text')) ? $product->code.$option->suffix : FALSE,
                            'print_barcode' => $this->input->post('barcode') ? true : false,
                            'barcode_text' => $this->input->post('barcode_text') ? true : false,
                            'bcs' => 'code128',
                            'bcis' => $bci_size,
                            'option' => $option->name,
                            'option_code' => $option->code,
                            'option_wh_qty' => isset($option->wh_quantity) ? $option->wh_quantity : NULL,
                            // 'barcode' => $this->product_barcode($product->code . $this->Settings->barcode_separator . $option->id, 'code128', $bci_size),
                            'price' => $this->input->post('price') ? $this->sma->formatValue(0, $price+($option->price > 0 ? $option->price : 0)) : 0,
                            'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                            'category' => $this->input->post('category') ? $product->category : FALSE,
                            'currencies' => $this->input->post('currencies'),
                            'variants' => [$option],
                            'quantity' => $quantity,
                            'purchase_date' => $purchase_date,
                        );
                        $barcodes[] = $barcode;
                    }
                }
            } else {
                $s = isset($_POST['product']) ? sizeof($_POST['product']) : 0;
                if ($s < 1) {
                    $this->session->set_flashdata('error', lang('no_product_selected'));
                    admin_redirect("products/print_barcodes");
                }
                $show_variants_quantity = $this->input->post('show_variants_quantity');
                for ($m = 0; $m < $s; $m++) {
                    $pid = $_POST['product'][$m];
                    $prow_no = $_POST['product_row_no'][$m];
                    $quantity = $_POST['quantity'][$m];
                    $product = $this->products_model->getProductWithCategory($pid);
                    $punit_data = $this->site->getUnitByID($product->unit);
                    $product->unit = $punit_data ? $punit_data->name : 'N/A';
                    $product->price = $this->input->post('check_promo') ? ($product->promotion ? $product->promo_price : $product->price) : $product->price;
                    $biller_id = $this->input->post('biller');
                    $warehouse_id = $this->input->post('warehouse');
                    $purchase_date = $this->input->post('purchase_date');
                    $biller = $this->site->getCompanyByID($biller_id);
                    $price = $this->site->get_product_biller_price_group($pid, $biller_id);
                    $price = $price ? $price->price : $product->price;
                    if ($variants = $this->products_model->getProductOptionsForPrinting($pid, $warehouse_id)) {
                        foreach ($variants as $option) {
                            if ($this->input->post('vt_'.$prow_no.'_' . $product->id . '_' . $option->id)) {
                                $barcode = array(
                                    'site' => $this->input->post('site_name') ? ($biller ? $biller->company : $this->Settings->site_name) : FALSE,
                                    'name' => $this->input->post('product_name') ? $product->name : FALSE,
                                    'color' => $this->input->post('product_name') ? $product->color_id : FALSE,
                                    'reference' => $this->input->post('reference') ? $product->reference : FALSE,
                                    'image' => $this->input->post('product_image') ? $product->image : FALSE,
                                    'barcode' => ($this->input->post('barcode') || $this->input->post('barcode_text')) ? $product->code.$option->suffix : FALSE,
                                    'print_barcode' => $this->input->post('barcode') ? true : false,
                                    'barcode_text' => $this->input->post('barcode_text') ? true : false,
                                    'bcs' => 'code128',
                                    'bcis' => $bci_size,
                                    'option' => $option->name,
                                    'option_code' => $option->code,
                                    'option_wh_qty' => isset($option->wh_quantity) ? $option->wh_quantity : NULL,
                                    // 'barcode' => $this->product_barcode($product->code . $this->Settings->barcode_separator . $option->id, 'code128', $bci_size),
                                    'price' => $this->input->post('price') ?$this->sma->formatValue(0, $price+($option->price > 0 ? $option->price : 0)) : 0,
                                    'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                                    'category' => $this->input->post('category') ? $product->category : FALSE,
                                    'currencies' => $this->input->post('currencies'),
                                    'variants' => [$option],
                                    'quantity' => $quantity,
                                    'purchase_date' => $purchase_date,
                                );
                                if ($custom_fields) {
                                    foreach ($custom_fields as $cf_arr) {
                                        $cf = $cf_arr['data'];
                                        if ($this->input->post($cf->cf_code)) {
                                            $barcode[$cf->cf_code] = $product->{$cf->cf_code};
                                        }
                                    }
                                }
                                $barcodes[] = $barcode;
                            }
                        }
                    } else {
                        $barcode = array(
                            'site' => $this->input->post('site_name') ? ($biller ? $biller->company : $this->Settings->site_name) : FALSE,
                            'name' => $this->input->post('product_name') ? $product->name : FALSE,
                            'reference' => $this->input->post('reference') ? $product->reference : FALSE,
                            'image' => $this->input->post('product_image') ? $product->image : FALSE,
                            // 'barcode' => $this->product_barcode($product->code, $product->barcode_symbology, $bci_size),
                            'barcode' => ($this->input->post('barcode') || $this->input->post('barcode_text')) ? $product->code : FALSE,
                            'print_barcode' => $this->input->post('barcode') ? true : false,
                            'barcode_text' => $this->input->post('barcode_text') ? true : false,
                            'bcs' => $product->barcode_symbology,
                            'bcis' => $bci_size,
                            'price' => $this->input->post('price') ? $this->sma->formatValue(0, $price) : 0,
                            'unit' => $this->input->post('unit') ? $product->unit : FALSE,
                            'category' => $this->input->post('category') ? $product->category : FALSE,
                            'currencies' => $this->input->post('currencies'),
                            'variants' => FALSE,
                            'quantity' => $quantity,
                            'purchase_date' => $purchase_date,
                            'color' => $this->input->post('product_name') && isset($product->colors) ? $product->colors : FALSE,
                        );
                        if ($custom_fields) {
                            foreach ($custom_fields as $cf_arr) {
                                $cf = $cf_arr['data'];
                                if ($this->input->post($cf->cf_code)) {
                                    $barcode[$cf->cf_code] = $product->{$cf->cf_code};
                                }
                            }
                        }
                        $barcodes[] = $barcode;
                    }
                }
            }
            // $this->sma->print_arrays($barcodes);
            $this->data['barcodes'] = $barcodes;
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['currencies'] = $currencies;
            $this->data['style'] = $style;
            $this->data['show_variants_quantity'] = $show_variants_quantity;
            $this->data['pdf_print'] = $pdf_print;
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['items'] = false;
            if (!isset($barcodes) || !$barcodes || count($barcodes) == 0) {
                $this->session->set_flashdata('error', lang('invalid_products_to_print'));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);
        } else {
            if ($this->input->get('purchase') || $this->input->get('transfer') || $this->input->get('adjustment') || $this->input->get('pckorder')) {

                if ($this->input->get('purchase')) {
                    $purchase_id = $this->input->get('purchase', TRUE);
                    $this->data['purchase'] = $this->site->getPurchaseByID($purchase_id);
                    $items = $this->products_model->getPurchaseItems($purchase_id);
                } elseif ($this->input->get('transfer')) {
                    $transfer_id = $this->input->get('transfer', TRUE);
                    $items = $this->products_model->getTransferItems($transfer_id);
                } elseif ($this->input->get('adjustment')) {
                    $adjustment_id = $this->input->get('adjustment', TRUE);
                    $items = $this->products_model->getAdjustmentItems($adjustment_id);
                } elseif ($this->input->get('pckorder')) {
                    $pckorder_id = $this->input->get('pckorder', TRUE);
                    $items = $this->products_model->getPackingOrderItems($pckorder_id);
                }

                if ($items) {
                    foreach ($items as $item) {
                        if ($row = $this->products_model->getProductByID($item->product_id)) {
                            $selected_variants = false;
                            if ($variants = $this->products_model->getProductOptions($row->id)) {
                                foreach ($variants as $variant) {
                                    $selected_variants[$variant->id] = isset($pr[$row->id]['selected_variants'][$variant->id]) && !empty($pr[$row->id]['selected_variants'][$variant->id]) ? 1 : ($variant->id == $item->option_id ? 1 : 0);
                                }
                            }
                            $pr[$item->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $item->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                        }
                    }
                    $this->data['message'] = lang('products_added_to_list');
                }
            }
            if ($product_id) {
                if ($row = $this->site->getProductByID($product_id)) {

                    $selected_variants = false;
                    if ($variants = $this->products_model->getProductOptions($row->id)) {
                        foreach ($variants as $variant) {
                            $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                        }
                    }
                    $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);

                    $this->data['message'] = lang('product_added_to_list');
                }
            }
            if ($this->input->get('category')) {
                if ($products = $this->products_model->getCategoryProducts($this->input->get('category'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }
            if ($this->input->get('subcategory')) {
                if ($products = $this->products_model->getSubCategoryProducts($this->input->get('subcategory'))) {
                    foreach ($products as $row) {
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['message'] = lang('products_added_to_list');
                } else {
                    $pr = array();
                    $this->session->set_flashdata('error', lang('no_product_found'));
                }
            }
            $this->data['items'] = isset($pr) ? json_encode($pr) : false;
            $this->data['pdf_print'] = $pdf_print;
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
            $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
            $this->page_construct('products/print_barcodes', $meta, $this->data);
        }
    }

    function print_barcodes_pdf()
    {
        $this->data['barcodes'] = json_decode($this->session->userdata('barcodes'));
        $this->data['show_variants_quantity'] = json_decode($this->session->userdata('show_variants_quantity'));
        $style = $this->session->userdata('barcodes_style');
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
        $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
        if ($style == 'zebra_32x25') {
            $url = 'products/zebra_32-25';
        } else if ($style == 'zebra_50x25') {
            $url = 'products/zebra_50-25';
        } else if ($style == 'zebra_32x25_standard') {
            $url = 'products/zebra_32-25_standard';
        } else if ($style == 'zebra_50x25_standard') {
            $url = 'products/zebra_50-25_standard';
        } else if ($style == 'zebra_100x50_standard') {
            $url = 'products/zebra_50-100_standard';
        } else if ($style == 'zebra_7_5x1_standard') {
            $url = 'products/zebra_7_5x1_standard';
        } else if ($style == 'zebra_7_2x1_standard') {
            $url = 'products/zebra_7_2x1_standard';
        } else if ($style == 'ticket_8x4') {
            $url = 'products/ticket_8x4';
        } else if ($style == 'zebra_32x25_moda_variants') {
            $url = 'products/zebra_32-25_moda_variants';
        } else if ($style == 'zebra_32x25_inventory') {
            $url = 'products/zebra_32-25_inventory';
        } else if ($style == 'zebra_32x15_standard'){
            $url = 'products/zebra_32-15_standard';
        }
        $this->load_view($this->theme . $url, $this->data);
    }

    /* ------------------------------------------------------- */

    function add($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $warehouses = $this->site->getAllWarehouses();
        $this->form_validation->set_rules('category', lang("category"), 'required|is_natural_no_zero');
        if ($this->input->post('type') == 'standard') {
            $this->form_validation->set_rules('unit', lang("product_unit"), 'required');
        }
        $this->form_validation->set_rules('code', lang("product_code"), 'is_unique[products.code]|alpha_dash');
        if (SHOP) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[products.slug]|alpha_dash');
        }
        if ($this->input->post('type') == 'combo') {
            $this->form_validation->set_rules('combo_item_quantity[]', lang("combo_x_items"), 'required');
        }
        if ($this->input->post('preferences')) {
            $this->form_validation->set_rules('preference_id[]', lang('preferences'), 'required');
        }
        if ($this->input->post('attributes') && $this->Settings->product_variant_per_serial == 0) {
            $this->form_validation->set_rules('attr_quantity[]', lang('attributes'), 'greater_than_equal_to[0]');
        }
        $this->form_validation->set_rules('weight', lang("weight"), 'numeric');
        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');

        if ($this->Settings->product_crud_validate_cost == 1) {
            $this->form_validation->set_rules('cost', lang("cost"), 'required|is_natural_no_zero');
        }

        if ($this->form_validation->run() == true) {
            $validate_price = ($this->Owner || $this->Admin || $this->GP['system_settings-update_products_group_prices']) ? true : false;
            $txt_fields_changed = sprintf(lang('user_activity_registration'), $this->session->first_name . " " . $this->session->last_name, lang('product'));
            $attr_suffix = $this->input->post('attr_suffix');
            $hybrid_prices = $this->input->post('hybrid_price');
            $price_groups = $this->input->post('pg[]');
            $price_groups_id = $this->input->post('pg_id[]');
            $billers = $this->input->post('billers[]');
            $price_groups_data = [];
            $tax_rate = $this->input->post('tax_rate') ? $this->site->getTaxRateByID($this->input->post('tax_rate')) : NULL;
            $from_purchase = $this->input->post('from_purchase') == 1 ? true : false;
            if ($this->input->post('tax_method') == 0 && $this->Settings->ipoconsumo) {
                $price = $this->input->post('price') - ($this->input->post('consumption_sale_tax') ? $this->input->post('consumption_sale_tax') : 0);
                $cost = $this->input->post('cost') - ($this->input->post('consumption_purchase_tax') ? $this->input->post('consumption_purchase_tax') : 0);
            } else {
                $price = $this->input->post('price');
                $cost = $this->input->post('cost');
            }

            $txt_fields_changed_pg = "";
            if ($price_groups) {
                foreach ($price_groups as $n_row => $pg_price) {
                    $pg_price = $pg_price > 0 ? $pg_price : 0;
                    if (($this->Owner || $this->Admin || $this->GP['products-cost']) && ($this->Settings->disable_product_price_under_cost == 1 && $pg_price > 0 && $pg_price <= $cost)) {
                        $this->session->set_flashdata('error', lang('product_price_group_under_product_cost'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $price_groups_data[] = [
                        'id' => $price_groups_id[$n_row],
                        'price' => $pg_price,
                    ];

                    $pg_data = $this->site->getPriceGroupByID($price_groups_id[$n_row]);
                    $txt_fields_changed_pg .= lang('price_group') . " " . $pg_data->name . ($pg_data->price_group_base == 1 ? ' (Lista base)' : "") . " = " . $this->sma->formatMoney($pg_price) . " , ";
                }
            }

            $txt_fields_changed_pg = trim($txt_fields_changed_pg, ", ");

            if (($this->Owner || $this->Admin || $this->GP['products-cost']) && ($this->Settings->disable_product_price_under_cost == 1 && $price <= $cost && $validate_price)) {
                $this->session->set_flashdata('error', lang('product_price_under_product_cost'));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $data = array(
                'code' => $this->input->post('code'),
                'reference' => $this->input->post('reference'),
                'barcode_symbology' => $this->input->post('barcode_symbology'),
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type'),
                'brand' => $this->input->post('brand'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                'cost' => $this->sma->formatDecimal($cost),
                'avg_cost' => $this->sma->formatDecimal($cost),
                'price' => $this->sma->formatDecimal($price > 0 ? $price : 0),
                'unit' => $this->input->post('unit'),
                'sale_unit' => $this->input->post('default_sale_unit'),
                'purchase_unit' => $this->input->post('default_purchase_unit'),
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'based_on_gram_value' => $this->input->post('based_on_gram_value'),
                'ignore_hide_parameters' => $this->input->post('ignore_hide_parameters') ? $this->input->post('ignore_hide_parameters') : 0,
                'promo_price' => $this->sma->formatDecimal($this->input->post('promo_price')),
                'start_date' => $this->input->post('start_date'),
                'end_date' => $this->input->post('end_date'),
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
                'file' => $this->input->post('file_link'),
                'slug' => $this->input->post('slug'),
                'weight' => $this->input->post('weight'),
                'second_level_subcategory_id' => $this->input->post('second_level_subcategory_id'),
                'featured' => $this->input->post('featured'),
                'hsn_code' => $this->input->post('hsn_code'),
                'hide_pos' => $this->input->post('hide_pos') ? $this->input->post('hide_pos') : 0,
                'hide_detal' => $this->input->post('hide_detal') ? $this->input->post('hide_detal') : 0,
                'hide_online_store' => $this->input->post('hide_online_store') ? $this->input->post('hide_online_store') : 0,
                'second_name' => $this->input->post('second_name'),
                'profitability_margin' => $this->input->post('profitability_margin'),
                'consumption_sale_tax' => $this->input->post('consumption_sale_tax') ? $this->input->post('consumption_sale_tax') : 0,
                'consumption_purchase_tax' => $this->input->post('consumption_purchase_tax') ? $this->input->post('consumption_purchase_tax') : 0,
                'attributes' => $this->input->post('attributes') ? 1 : 0,
                'has_variants' => $this->input->post('attributes') ? 1 : 0,
                'has_preferences' => $this->input->post('preferences') ? 1 : 0,
                'paste_balance_value' => $this->input->post('paste_balance_value'),
                'aiu_product' => $this->input->post('aiu_product'),
                'color_id' => $this->input->post('color'),
                'material_id' => $this->input->post('material'),
                'sale_min_qty' => $this->input->post('sale_min_qty'),
                'sale_max_qty' => $this->input->post('sale_max_qty'),
                'warranty_time' => $this->input->post('warranty_time'),
                'height' => $this->input->post('height'),
                'width' => $this->input->post('width'),
                'length' => $this->input->post('length'),
                'tags' => json_encode($this->input->post('tags')),
                'product_code_consecutive' => $this->input->post('product_code_consecutive'),
                'sale_tax_rate_2_id' => $this->input->post('sale_tax_rate_2_id'),
                'purchase_tax_rate_2_id' => $this->input->post('purchase_tax_rate_2_id'),
                'purchase_tax_rate_2_percentage' => $this->input->post('purchase_tax_rate_2_percentage'),
                'sale_tax_rate_2_percentage' => $this->input->post('sale_tax_rate_2_percentage'),
                'sale_tax_rate_2_milliliters' => $this->input->post('sale_tax_rate_2_milliliters'),
                'sale_tax_rate_2_nominal' => $this->input->post('sale_tax_rate_2_nominal'),
                'sale_tax_rate_2_degrees' => $this->input->post('sale_tax_rate_2_degrees'),
                'birthday_discount' => $this->input->post('birthday_discount'),
                'jewel_weight' => $this->input->post('jewel_weight'),
            );

            $txt_fields_changed .= lang('product_code') . " = " . $data['code'] . ", " . lang('product_name') . " = " . $data['name'] . ", " . lang('cost') . " = " . $this->sma->formatMoney($data['cost']) . ", " . lang('price') . " = " . $this->sma->formatMoney($data['price']) . "; " . lang('price_groups') . " : " . $txt_fields_changed_pg;

            if ($this->input->post('custom_field_value')) {
                $last_cf_code = '';
                foreach ($_POST['custom_field_value'] as $cf_id => $cf_value) {
                    if ($cf_value) {
                        if (!isset($_POST['custom_field_code'][$cf_id])) {
                            if (isset($data[$last_cf_code])) {
                                $data[$last_cf_code] .= ", " . $cf_value;
                            } else {
                                $data[$last_cf_code] = $cf_value;
                            }
                        } else {
                            $last_cf_code = $_POST['custom_field_code'][$cf_id];
                            $cf_code = $_POST['custom_field_code'][$cf_id];
                            $data[$cf_code] = $cf_value;
                        }
                    }
                }
            }
            $warehouse_qty = NULL;
            $product_attributes = NULL;
            $product_preferences = NULL;
            $this->load->library('upload');
            if ($this->input->post('type') == 'standard' || $this->input->post('type') == 'pfinished') {
                $wh_total_quantity = 0;
                $pv_total_quantity = 0;
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s] = $this->input->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->input->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    if ($this->input->post('wh_qty_' . $warehouse->id)) {
                        $warehouse_qty[] = array(
                            'warehouse_id' => $this->input->post('wh_' . $warehouse->id),
                            'quantity' => $this->input->post('wh_qty_' . $warehouse->id),
                            'rack' => $this->input->post('rack_' . $warehouse->id) ? $this->input->post('rack_' . $warehouse->id) : NULL
                        );
                        $wh_total_quantity += $this->input->post('wh_qty_' . $warehouse->id);
                    }
                }

                if ($this->input->post('attributes') && $this->Settings->product_variant_per_serial == 0) {
                    $a = sizeof($_POST['attr_name']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['attr_name'][$r])) {
                            $product_attributes[$r] = array(
                                'name' => $_POST['attr_name'][$r],
                                'warehouse_id' => $_POST['attr_warehouse'][$r],
                                'quantity' => $_POST['attr_quantity'][$r],
                                'price' => $_POST['attr_price'][$r],
                                'minimun_price' => $_POST['attr_minimun_price'][$r],
                                'code' => $_POST['attr_code'][$r],
                                'weight' => $_POST['attr_weight'][$r],
                                'attr_code_consecutive' => $_POST['attr_code_consecutive'][$r],
                            );
                            if (isset($_POST['attr_index'][$r])) {
                                $product_attributes[$r]['attr_index'] = $_POST['attr_index'][$r];
                            }
                            if ($_FILES['attr_image']['size'][$r] > 0) {
                                $_FILES['attr_image_'.$r]['name'] = $_FILES['attr_image']['name'][$r];
                                $_FILES['attr_image_'.$r]['type'] = $_FILES['attr_image']['type'][$r];
                                $_FILES['attr_image_'.$r]['tmp_name'] = $_FILES['attr_image']['tmp_name'][$r];
                                $_FILES['attr_image_'.$r]['error'] = $_FILES['attr_image']['error'][$r];
                                $_FILES['attr_image_'.$r]['size'] = $_FILES['attr_image']['size'][$r];

                                $config['upload_path'] = $this->upload_path;
                                $config['allowed_types'] = $this->image_types;
                                $config['max_size'] = $this->allowed_file_size;
                                $config['overwrite'] = FALSE;
                                $config['encrypt_name'] = TRUE;
                                $config['max_filename'] = 25;
                                $this->upload->initialize($config);
                                if (!$this->upload->do_upload('attr_image_'.$r)) {
                                    $error = $this->upload->display_errors();
                                    $this->session->set_flashdata('error', $error);
                                    admin_redirect("products/add");
                                }
                                $file = $this->upload->file_name;
                                $product_attributes[$r]['image'] = $file;
                            }
                            $pv_total_quantity += $_POST['attr_quantity'][$r];
                        }
                    }
                }

                if ($wh_total_quantity != $pv_total_quantity && $pv_total_quantity != 0) {
                    $this->form_validation->set_rules('wh_pr_qty_issue', 'wh_pr_qty_issue', 'required');
                    $this->form_validation->set_message('required', lang('wh_pr_qty_issue'));
                }
            }

            if ($this->input->post('type') == 'standard' || $this->input->post('type') == 'combo' || $this->input->post('type') == 'pfinished') {
                if ($this->input->post('preferences')) {
                    $a = sizeof($_POST['preference_id']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['preference_id'][$r])) {
                            $product_preferences[] = $_POST['preference_id'][$r];
                        }
                    }
                }
            }

            if ($this->input->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'combo' || $this->input->post('type') == 'pfinished' || $this->input->post('type') == 'subproduct') {
                $total_price = 0;
                $alert_combo_item_without_cost = false;
                if (isset($_POST['combo_item_code'])) {
                    $c = sizeof($_POST['combo_item_code']) - 1;
                    for ($r = 0; $r <= $c; $r++) {
                        if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                            $items[] = array(
                                'item_code' => $_POST['combo_item_code'][$r],
                                'quantity' => $_POST['combo_item_quantity'][$r],
                                'unit_price' => $_POST['combo_item_price'][$r],
                                'unit' => $_POST['combo_item_unit'][$r],
                            );

                            if ($_POST['combo_item_price'][$r] == 0) {
                                $alert_combo_item_without_cost = true;
                            }
                        }
                        $total_price += $_POST['combo_item_price'][$r] * $_POST['combo_item_quantity'][$r];
                    }
                }
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'digital') {
                if ($_FILES['digital_file']['size'] > 0) {
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('digital_file')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    }
                    $file = $this->upload->file_name;
                    $data['file'] = $file;
                } else {
                    if (!$this->input->post('file_link')) {
                        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'required');
                    }
                }
                $config = NULL;
                $data['track_quantity'] = 0;
            }

            if (!isset($items)) {
                $items = NULL;
            }

            if ($_FILES['product_image']['size'] > 0) {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['max_filename'] = 25;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('product_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/add");
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }

            if ($_FILES['userfile']['name'][0] != "") {

                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {

                    $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];

                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    } else {

                        $pho = $this->upload->file_name;

                        $photos[] = $pho;

                        $this->load->library('image_lib');
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->upload_path . $pho;
                        $config['new_image'] = $this->thumbs_path . $pho;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $this->Settings->twidth;
                        $config['height'] = $this->Settings->theight;

                        $this->image_lib->initialize($config);

                        if (!$this->image_lib->resize()) {
                            echo $this->image_lib->display_errors();
                        }

                        if ($this->Settings->watermark) {
                            $this->image_lib->clear();
                            $wm['source_image'] = $this->upload_path . $pho;
                            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                            $wm['wm_type'] = 'text';
                            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                            $wm['quality'] = '100';
                            $wm['wm_font_size'] = '16';
                            $wm['wm_font_color'] = '999999';
                            $wm['wm_shadow_color'] = 'CCCCCC';
                            $wm['wm_vrt_alignment'] = 'top';
                            $wm['wm_hor_alignment'] = 'left';
                            $wm['wm_padding'] = '10';
                            $this->image_lib->initialize($wm);
                            $this->image_lib->watermark();
                        }

                        $this->image_lib->clear();
                    }
                }
                $config = NULL;
            } else {
                $photos = NULL;
            }

            $data['quantity'] = isset($wh_total_quantity) ? $wh_total_quantity : 0;

            $wh_locations = [];

            if ($this->input->post('warehouse_zone')) {
                $wh_zones = $this->input->post('warehouse_zone');
                $wh_racks = $this->input->post('warehouse_rack');
                $wh_levels = $this->input->post('warehouse_level');

                foreach ($wh_zones as $wh_id => $wh_zone) {
                    $wh_locations[$wh_id] = $wh_zone . " / " . $wh_racks[$wh_id] . " / " . $wh_levels[$wh_id];
                }
            }

            $munits_prg = [];

            $multiple_units = false;
            if ($this->input->post('product_has_multiple_units')) {
                $munits = $this->input->post('unit_id');
                $munits_cantidad = $this->input->post('unit_operation_value');
                $munitsprices = $this->input->post('unit_price');
                $marginup = $this->input->post('margin_update_price') ? $this->input->post('margin_update_price') : false;
                $munitstatus = $this->input->post('unit_status') ? $this->input->post('unit_status') : false;
                $unit_ipoconsumo = $this->input->post('unit_ipoconsumo') ? $this->input->post('unit_ipoconsumo') : false;
                $product_unit_id = $this->input->post('product_unit_id');
                $unit_price_group = $this->input->post('unit_price_group');
                $data['unit'] = $product_unit_id;
                $data['has_multiple_units'] = 1;
                $data['sale_unit'] = $this->input->post('multiple_default_sale_unit');
                $data['purchase_unit'] = $this->input->post('multiple_default_purchase_unit');

                foreach ($munits as $row => $unit_id) {
                    if (isset($unit_price_group[$row]) && $unit_price_group[$row]) {
                        $munits_prg[$unit_price_group[$row]] = $unit_id;
                    }
                    if ($unit_id != $data['unit']) {
                        $multiple_units[] = [
                            'unit_id' => $unit_id,
                            'price' => $munitsprices ? $munitsprices[$row] : 0,
                            'cantidad' => $munits_cantidad[$row],
                            'margin_update_price' => $marginup && isset($marginup[$row]) ? $marginup[$row] : 0,
                            'unit_ipoconsumo' => $unit_ipoconsumo ? $unit_ipoconsumo[$row] : 0,
                            'status' => $munitstatus && isset($munitstatus[$row]) && $munitstatus[$row] ? 1 : 0,
                        ];
                    }
                }
            }
            // $this->sma->print_arrays($product_attributes);
            $arr_data = [
                'data' => $data,
                'items' => $items,
                'warehouse_qty' => $warehouse_qty,
                'product_attributes' => $product_attributes,
                'photos' => $photos,
                'product_preferences' => $product_preferences,
                'wh_locations' => $wh_locations,
                'price_groups_data' => $price_groups_data,
                'multiple_units' => $multiple_units,
                'hybrid_prices' => $hybrid_prices,
                'munits_prg' => $munits_prg,
                'attr_suffix' => $attr_suffix,
                'billers' => $billers,
            ];
        }

        if ($this->form_validation->run() == true && $product_id = $this->products_model->addProduct($arr_data)) {

            $syncCreatedProduct = $this->syncCreatedProduct($product_id, $arr_data);
            if (isset($alert_combo_item_without_cost) && $alert_combo_item_without_cost) {
                $this->session->set_flashdata('warning', lang("alert_combo_item_without_cost"));
            }
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 3,
                'table_name' => 'products',
                'record_id' => $product_id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $txt_fields_changed,
            ]);

            if (isset($syncCreatedProduct->warning) && !empty($syncCreatedProduct->warning)) {
                $this->session->set_flashdata('warning', lang("product_added") . $syncCreatedProduct->warning);
            }
            if (isset($syncCreatedProduct->message) && !empty($syncCreatedProduct->message)) {
                $this->session->set_flashdata('message', lang("product_added") .  $syncCreatedProduct->message);
            }
            if (isset($syncCreatedProduct->error) && !empty($syncCreatedProduct->error)) {
                $this->session->set_flashdata('error', lang("product_not_added") . $syncCreatedProduct->error);
            }

            if ($from_purchase) {
                admin_redirect('purchases/add/?product_id=' . $product_id);
            } else {
                admin_redirect('products');
            }
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['colors'] = $this->products_model->get_colors();
            $this->data['materials'] = $this->products_model->get_materials();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['units'] = $this->site->get_all_units();
            $this->data['warehouses'] = $warehouses;
            $this->data['warehouses_products'] = $id ? $this->products_model->getAllWarehousesWithPQ($id) : NULL;
            $this->data['product'] = $id ? $this->products_model->getProductByID($id) : NULL;
            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['combo_items'] = ($id && ($this->data['product']->type == 'combo' || $this->data['product']->type == 'subproduct' || $this->data['product']->type == 'pfinished')) ? $this->products_model->getProductComboItems($id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $this->data['price_groups'] = $this->site->getAllPriceGroups();
            $this->data["custom_fields"] = $this->site->get_custom_fields(2);
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['preferences_categories'] = $this->products_model->get_preferences_categories();
            $this->data["tags"] = $this->products_model->getTags(["module"=>1]);
            $this->data["second_taxes"] = $this->site->getSecondTaxes();

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_product')));
            $meta = array('page_title' => lang('add_product'), 'bc' => $bc);
            $this->page_construct('products/add', $meta, $this->data);
        }
    }

    private function syncCreatedProduct($id, $data)
    {
        $ProductW = new Products_model();
        $TaxRatesModel = new TaxRates_model();
        $site = new Site();

        $_errorMessage = '';
        $_message = '';
        $response = [];

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $response = $this->validatePreSyncProducts(true);
            if (empty($response)) {
                $integrations = $this->settings_model->getAllIntegrations();
                $product = $this->products_model->getProductByID($id);
                $unit = $this->site->getUnitByID($product->sale_unit);
                $tax = $TaxRatesModel->find($product->tax_rate);

                if (!empty($integrations) && !empty($product)) {
                    $this->load->integration_model('Product');
                    $this->load->integration_model('Brand');
                    $this->load->integration_model('Category');

                    foreach ($integrations as $integration) {
                        $Product = new Product();
                        $Product->open($integration);
                        $productExt = $Product->find(["id_wappsi" => $id]);
                        if (empty($productExt) && ($product->hide_online_store == NOT)) {
                            $Brand = new Brand();
                            $Brand->open($integration);
                            $brandExt = $Brand->find(["id_wappsi" => $product->brand]);
                            if (empty($brandExt)) {
                                $response["error"] = " La marca para el producto $product->code aún no ha sido sincronizada.";
                                return (object) $response;
                            }

                            $Category = new Category();
                            $Category->open($integration);
                            $categoryExt = $Category->find(["id_wappsi" => $product->category_id]);
                            if (empty($categoryExt)) {
                                $response["error"] = " La categoría para el producto $product->code  aún no ha sido sincronizada.";
                                return (object) $response;
                            }

                            $productData = [
                                "shop_id"            => $integration->store_id,
                                "name"               => $product->name,
                                "brand_id"           => $brandExt->id,
                                "description"        => $product->product_details,
                                "slug"               => $product->slug,
                                "discount"           => $this->getDiscountValue($product),
                                "discount_type"      => "flat",
                                "discount_start_date"=> ($this->shop_settings->update_promotions_to_store == YES && !empty($product->promotion)) ? strtotime($product->start_date) : null,
                                "discount_end_date"  => ($this->shop_settings->update_promotions_to_store == YES && !empty($product->promotion)) ? strtotime($product->end_date) : null,
                                "stock"              => NOT,
                                "published"          => ($product->hide_online_store == YES) ? NOT : YES,
                                "approved"           => $product->discontinued ^= 1,
                                "unit"               => $unit->name,
                                "min_qty"            => ($product->sale_min_qty > 0) ? $product->sale_min_qty : 1,
                                "max_qty"            => $product->sale_max_qty,
                                "is_variant"         => (!empty($data["pv_old_data"]) || !empty($data["product_attributes"])) ? YES : NOT,
                                "has_warranty"       => (!empty($product->warranty_time)) ? YES : NOT,
                                "weight"             => (!empty($product->weight)) ? $product->weight : 0,
                                "height"             => (!empty($product->height)) ? $product->height : 0,
                                "length"             => (!empty($product->length)) ? $product->length : 0,
                                "width"              => (!empty($product->width)) ? $product->width : 0,
                                "meta_title"         => $product->name,
                                "meta_description"   => strip_tags($product->name),
                                "slug"               => $product->slug,
                                "rating"             => 0,
                                "digital"            => ($product->type == 'digital') ? YES : NOT,
                                "main_category"      => $categoryExt->id,
                                "created_at"         => date("Y-m-d H:i:s"),
                                "updated_at"         => date("Y-m-d H:i:s"),
                                "tags"               => $this->getTags($product->tags),
                                "lowest_price"       => ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price,
                                "highest_price"      => ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price,
                                "quantity"           => $this->getQuantityProduct($product->id),
                                "id_wappsi"          => $product->id,
                            ];

                            if ($Product->create($productData)) {
                                $this->products_model->updateStatusSyncStore(2, $product->id);
                                $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";

                                $this->syncCreateProductCategory($product, $product->category_id);

                                if (!empty($product->subcategory_id) && $product->subcategory_id != NULL) {
                                    $this->syncCreateProductCategory($product, $product->subcategory_id);
                                }

                                if (!empty($product->second_level_subcategory_id) && $product->second_level_subcategory_id != NULL) {
                                    $this->syncCreateProductCategory($product, $product->second_level_subcategory_id);
                                }

                                if (!empty($product->color_id)) {
                                    $this->syncCreateProductAttribute($product, 2, $product->color_id);
                                }

                                if (!empty($product->material_id)) {
                                    $this->syncCreateProductAttribute($product, 3, $product->material_id);
                                }

                                if (!empty($data["product_attributes"])) {
                                    foreach ($data["product_attributes"] as $productAttribute) {
                                        $productAttribute = (array) $productAttribute;
                                        $variant = $this->settings_model->getVariant(["name" => $productAttribute["name"]]);
                                        $this->syncCreateProductAttribute($product, 1, $variant->id);

                                        $productVariant = $this->products_model->getProductVariantID($id, $productAttribute["name"]);
                                        $this->syncCreateProductVariant($id, $productVariant);
                                    }
                                } else if (!empty($data["pv_old_data"])) {
                                    foreach ($data["pv_old_data"] as $productAttribute) {
                                        $productAttribute = (array) $productAttribute;
                                        $variant = $this->settings_model->getVariant(["name" => $productAttribute["name"]]);
                                        $this->syncCreateProductAttribute($product, 1, $variant->id);

                                        $productVariant = $this->products_model->getProductVariantID($id, $productAttribute["name"]);
                                        $this->syncCreateProductVariant($id, $productVariant);
                                    }
                                } else {
                                    $this->syncCreateProductVariant($id);
                                }

                                if (!empty($data["product_preferences"])) {
                                    foreach ($data["product_preferences"] as $productPreferenceId) {
                                        $preference = $this->products_model->get_preference_by_id($productPreferenceId);
                                        $this->syncCreateProductAttribute($product,  $preference->category_id, $productPreferenceId, true);
                                    }
                                }

                                $this->syncTax($product);
                            } else {
                                $_errorMessage .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                            }
                        } else {
                            $_errorMessage .= $this->site->getnameTypeIntegration($integration->type) . ", ";
                        }

                        $Product->close();
                    }

                    if (!empty($_message)) {
                        $response["message"] = " Sincronizado en: " . rtrim($_message, ", ");
                    }
                    if (!empty($_errorMessage)) {
                        $response["error"] = " No fue posible sincronizar en: " . rtrim($_errorMessage, ", ");
                    }
                }
            }
        }

        return (object) $response;
    }

    private function validateSync() 
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (empty($integrations)) {
                return false;
            }

            return true;
        } else {
            return false;
        }
    }

    private function syncTax($product)
    {
        if ($this->validateSync()) {
            $TaxRates_model = new TaxRates_model();
            $tax = $TaxRates_model->find($product->tax_rate);
    
            $integrations = $this->settings_model->getAllIntegrations();
            foreach ($integrations as $integration) {
                $Product = new Product();
                $Product->open($integration);
                $productStore = $Product->find(["id_wappsi" => $product->id]);
                if (!empty($productStore)) {
                    $this->load->integration_model('Taxes');
                    $Taxes = new Taxes();
                    $Taxes->open($integration);
                    $taxStore = $Taxes->find(["id_wappsi" => $tax->id]);
        
                    $this->load->integration_model('ProductTax');
                    $ProductTax = new ProductTax();
                    $ProductTax->open($integration);
                    
                    $ProductTax->delete(["product_id" => $productStore->id]);
    
                    $data = [
                        "product_id"=> $productStore->id,
                        "tax_id"    => $taxStore->id,
                        "tax"       => $tax->rate,
                        "tax_type"  => ($tax->type == 1) ? "percent" : "flat"
                    ];
                    $ProductTax->create($data);
                    
                    $ProductTax->close();
                    $Taxes->close();
                }

                $Product->close();
            }
        }
    }

    private function getTags($tags)
    {
        $stringTag = "";
        if (!empty($tags)) {
            $tags = json_decode($tags);
            if ($tags) {
                foreach ($tags as $tag) {
                    $tagWappsi = $this->products_model->findTags(["id" => $tag]);
                    $stringTag .= "$tagWappsi->description,";
                }
            }
        }
        return trim($stringTag, ",");
    }

    private function getDiscountValue($product)
    {
        $promoPrice = 0;
        if ($this->shop_settings->update_promotions_to_store == YES && !empty($product->promotion) && !empty($product->promo_price)) {
            $Companies = new Companies_model();
            $Product = new Products_model();

            $storeBranch = $this->shop_settings->biller;
            $billerData = $Companies->getBillerData(['biller_id' => $storeBranch]);
            $defaultPriceGroup = $billerData->default_price_group;

            $priceProduct = $Product->getProductPrice(["product_id" => $product->id, "price_group_id" => $defaultPriceGroup]);
            $promoPrice = $priceProduct->price - $product->promo_price;
        }
        return $promoPrice;
    }

    private function syncCreateProductCategory($product, $categoryId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();

            if (!empty($integrations) && !empty($product)) {
                $this->load->integration_model('ProductCategory');
                $this->load->integration_model('Category');
                $this->load->integration_model('Product');

                foreach ($integrations as $integration) {
                    $ProductCategory = new ProductCategory();
                    $ProductCategory->open($integration);

                    $Product = new Product();
                    $Product->open($integration);

                    $Category = new Category();
                    $Category->open($integration);

                    $productStore = $Product->find(["id_wappsi" => $product->id]);
                    $categoryStore = $Category->find(["id_wappsi" => $categoryId]);

                    if (!empty($productStore) && !empty($categoryStore)) {
                        $data = [
                            "product_id"    => $productStore->id,
                            "category_id"   => $categoryStore->id,
                            "created_at"    => date("Y-m-d H:i:s"),
                            "updated_at"    => date("Y-m-d H:i:s"),
                        ];
                        $ProductCategory->create($data);
                    }

                    $ProductCategory->close();
                    $Product->close();
                    $Category->close();
                }
            }
        }
    }

    private function syncCreateProductAttribute($product, $attributeId, $attributeValueId, $isPreference = false)
    {
        $settings = new Settings_model();
        $site = new Site();

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $settings->getAllIntegrations();

            if (!empty($integrations) && !empty($product)) {
                $this->load->integration_model('ProductAttribute');
                $this->load->integration_model('AttributeIntegration');
                $this->load->integration_model('Product');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);
                    $productExt = $Product->find(["id_wappsi" => $product->id]);
                    $Product->close();

                    if ($isPreference) {
                        $Attribute = new AttributeIntegration();
                        $Attribute->open($integration);
                        $attribute = $Attribute->find(["id_wappsi" => $attributeId]);
                        $attributeId = $attribute->id;
                        $Attribute->close();
                    }

                    $ProductAttribute = new ProductAttribute();
                    $ProductAttribute->open($integration);
                    $productAttribute = $ProductAttribute->find(["product_id" => $productExt->id, "attribute_id" => $attributeId]);

                    if (empty($productAttribute)) {
                        $data = [
                            "product_id"    => $productExt->id,
                            "attribute_id"   => $attributeId,
                            "created_at"    => date("Y-m-d H:i:s"),
                            "updated_at"    => date("Y-m-d H:i:s"),
                        ];
                        $ProductAttribute->create($data);
                    }

                    $this->syncCreateProductAttributeValue($productExt, $attributeId, $attributeValueId);

                    $ProductAttribute->close();
                }
            }
        }
    }

    private function syncCreateProductAttributeValue($productExt, $attributeId, $attributeValueId)
    {
        $settings = new Settings_model();
        $site = new Site();

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $settings->getAllIntegrations();

            if (!empty($integrations) && !empty($productExt)) {
                $this->load->integration_model('ProductAttributeValue');
                $this->load->integration_model('AttributeValue');

                foreach ($integrations as $integration) {
                    $ProductAttributeValue = new ProductAttributeValue();
                    $ProductAttributeValue->open($integration);

                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $attributeValue = $AttributeValue->find(["attribute_id" => $attributeId, "id_wappsi" => $attributeValueId]);
                    $AttributeValue->close();

                    if (!empty($attributeValue)) {
                        $data = [
                            "product_id"        => $productExt->id,
                            "attribute_id"      => $attributeId,
                            "attribute_value_id" => $attributeValue->id,
                            "created_at"        => date("Y-m-d H:i:s"),
                            "updated_at"        => date("Y-m-d H:i:s"),
                        ];
                        $ProductAttributeValue->create($data);
                        $ProductAttributeValue->close();
                    }
                }
            }
        }
    }

    private function syncCreateProductVariant($productId, $productVariantId = null)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $TaxRatesModel = new TaxRates_model();

            $integrations = $this->settings_model->getAllIntegrations();
            $product = $this->products_model->getProductByID($productId);
            $tax = $TaxRatesModel->find($product->tax_rate);

            if (!empty($productVariantId)) {
                $productVariant = $this->products_model->findProductVariant(['id'=>$productVariantId]);
                $variant = $this->settings_model->getVariant(['name' => $productVariant->name]);
            }

            if (!empty($integrations)) {
                $this->load->integration_model('ProductVariation');
                $this->load->integration_model('AttributeValue');
                $this->load->integration_model('Product');

                foreach ($integrations as $integration) {
                    $ProductVariation = new ProductVariation();
                    $ProductVariation->open($integration);

                    $Product = new Product();
                    $Product->open($integration);
                    $productExt = $Product->find(["id_wappsi" => $productId]);
                    if (!empty($productExt)) {
                        if (!empty($productVariantId)) {
                            $productVariation = [];
                            $AttributeValue = new AttributeValue();
                            $AttributeValue->open($integration);
                            $AttributeValueExt = $AttributeValue->find(["attribute_id" => 1, "id_wappsi" => $variant->id]);
                        } else {
                            $productVariation = $ProductVariation->find(["product_id"=>$productExt->id]);
                        }

                        if (empty($productVariation)) {
                            $quantity = $this->getQuantityProduct($productId);
                            $productPrice = ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price;
                            $data = [
                                "product_id" => $productExt->id,
                                "code"       => (!empty($productVariantId)) ? "1:$AttributeValueExt->id/" : null,
                                "sku"        => (!empty($productVariantId)) ? $productVariant->code : $product->code,
                                "price"      => (!empty($productVariantId)) ? ($productPrice + $productVariant->price) : $productPrice,
                                "stock"      => ($quantity >= $this->shop_settings->activate_available_from_quantity) ? ACTIVE : INACTIVE,
                                "created_at" => date("Y-m-d H:i:s"),
                                "updated_at" => date("Y-m-d H:i:s"),
                                "id_wappsi"  => (!empty($productVariantId)) ? $productVariant->id : null,
                                "quantity"   => $quantity,
                            ];

                            // if ($this->Settings->prioridad_precios_producto == 4) {
                                $productPrices = $this->products_model->getProductPrices(["product_id" => $productId]);
                                if (isset($productPrices) && !empty($productPrices)) {
                                    $i = 2;
                                    foreach ($productPrices as $prices) {
                                        $priceProductPrice = ($tax->type == 1) ? $prices->price / (1 + ($tax->rate / 100)) : $prices->price;
                                        if ($prices->price_group_base == 1) {
                                            $data["price"] = (!empty($productVariantId)) ? ($priceProductPrice + $productVariant->price) : $priceProductPrice;
                                        } else {
                                            $data["price_$i"] = (!empty($productVariantId)) ? ($priceProductPrice + $productVariant->price) : $priceProductPrice;
                                            $i++;
                                        }

                                    }   
                                }
                            // }

                            $produtVariationId = $ProductVariation->create($data);
                            $ProductVariation->close();

                            if (!empty($productVariantId)) {
                                $this->suncCreateProductVariationsCombination($productExt, $produtVariationId, 1, $AttributeValueExt->id);
                            }
                        }
                    }
                }
            }
        }
    }

    private function getQuantityProduct($productId)
    {
        $showProductQuantity = $this->shop_settings->show_product_quantity;
        $billerId = $this->shop_settings->biller;

        if ($showProductQuantity == 1) {
            $billerData = $this->companies_model->getBillerData(['biller_id' => $billerId]);
            $productQuantity = $this->products_model->getProductDataByWarehouse(['product_id' => $productId, 'warehouse_id' => $billerData->default_warehouse_id]);
            return (!empty($productQuantity->quantity)) ? $productQuantity->quantity : 0;
        } else if ($showProductQuantity == 2) {
            $productQuantity = $this->products_model->getProductDataByWarehouseSUM(['product_id' => $productId]);
            return (!empty($productQuantity->quantity)) ? $productQuantity->quantity : 0;
        } else {
            return 0;
        }
    }

    private function getQuantityProductVariant($productId, $productVariantId = null)
    {
        $showProductQuantity = $this->shop_settings->show_product_quantity;
        $billerId = $this->shop_settings->biller;

        if (!empty($productVariantId)) {
            if ($showProductQuantity == 1) {
                $billerData = $this->companies_model->getBillerData(['biller_id' => $billerId]);
                $productVariantQuantity = $this->products_model->getProductVariantQuantities(['option_id' => $productVariantId, 'warehouse_id' => $billerData->default_warehouse_id]);
                return (!empty($productVariantQuantity->quantity)) ? $productVariantQuantity->quantity : 0;
            } else if ($showProductQuantity == 2) {
                $productVariantQuantity = $this->products_model->getProductVariantQuantities(['option_id' => $productVariantId]);
                return (!empty($productVariantQuantity->quantity)) ? $productVariantQuantity->quantity : 0;
            } else {
                return 0;
            }
        } else {
            if ($showProductQuantity == 1) {
                $billerData = $this->companies_model->getBillerData(['biller_id' => $billerId]);
                $productVariantQuantity = $this->products_model->getProductDataByWarehouse(['product_id' => $productId, 'warehouse_id' => $billerData->default_warehouse_id]);
                return (!empty($productVariantQuantity->quantity)) ? $productVariantQuantity->quantity : 0;
            } else if ($showProductQuantity == 2) {
                $productVariantQuantity = $this->products_model->getProductDataByWarehouse(['product_id' => $productId]);
                return (!empty($productVariantQuantity->quantity)) ? $productVariantQuantity->quantity : 0;
            } else {
                return 0;
            }
        }
    }

    private function suncCreateProductVariationsCombination($productExt, $produtVariationId, $attributeId, $AttributeValueId)
    {
        $settings = new Settings_model();
        $site = new Site();

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $settings->getAllIntegrations();

            if (!empty($integrations)) {
                $this->load->integration_model('ProductVariationCombination');

                foreach ($integrations as $integration) {
                    $ProductVariationCombination = new ProductVariationCombination();
                    $ProductVariationCombination->open($integration);


                    $data = [
                        "product_id"            => $productExt->id,
                        "product_variation_id"  => $produtVariationId,
                        "attribute_id"          => $attributeId,
                        "attribute_value_id"    => $AttributeValueId,
                        "created_at"            => date("Y-m-d H:i:s"),
                        "updated_at"            => date("Y-m-d H:i:s"),
                    ];
                    $ProductVariationCombination->create($data);
                    $ProductVariationCombination->close();
                }
            }
        }
    }

    public function syncStore()
    {
        $productIds = $this->input->post('productIds');
        $warningMessage = '';
        $errorMessage = '';
        $response = [];
        $data = [];

        if (!empty($productIds)) {
            $this->validatePreSyncProducts();

            if ($this->Settings->data_synchronization_to_store == ACTIVE) {
                $integrations = $this->settings_model->getAllIntegrations();
                if (!empty($integrations)) {
                    foreach ($integrations as $integration) {
                        $this->load->integration_model('Product');

                        foreach ($productIds as $productId) {
                            $product = $this->products_model->getProductByID($productId);
                            if ($product->synchronized_store != 2) {
                                $Product = new Product();
                                $Product->open($integration);

                                $productVariants = $this->products_model->findProductVariants(["product_id" => $productId]);
                                $product_preferences = $this->products_model->getProductPreferences($productId);

                                $productExt = $Product->find(["id_wappsi" => $productId]);
                                if (empty($productExt)) {
                                    $data["product_attributes"] = $productVariants;
                                    $data["product_preferences"] = $product_preferences;
                                    $syncCreatedProduct = $this->syncCreatedProduct($productId, $data);
                                } else {
                                    $data["id"] = $productId;
                                    $data['data']['price'] = $product->price;

                                    foreach ($productVariants as $productVariant) {
                                        $productAttributeData[$productVariant->id] = [
                                            'code' => $productVariant->code,
                                            'price'=> $productVariant->price,
                                            'status'=> $productVariant->status,
                                        ];
                                    }

                                    $data['pv_old_data'] = $productAttributeData;
                                    $syncCreatedProduct = $this->syncUpdatedProduct($data);
                                }

                                if (isset($syncCreatedProduct->warning) && !empty($syncCreatedProduct->warning)) {
                                    $warningMessage .= $syncCreatedProduct->warning;
                                }
                                if (isset($syncCreatedProduct->error) && !empty($syncCreatedProduct->error)) {
                                    $errorMessage .= $syncCreatedProduct->error;
                                }
                            }
                        }
                    }
                }
            }

            if (!empty($warningMessage)) {
                $response['warning'] = $warningMessage;
            }

            if (!empty($errorMessage)) {
                $response['error'] = $errorMessage;
            }

            echo json_encode($response);
        }
    }

    function validatePreSyncProducts($ajax = false)
    {
        if ($this->validateSync()) {
            $integrations = $this->settings_model->getAllIntegrations();
            
            // $this->load->integration_model('AttributeIntegration');
            // $this->load->integration_model('AttributeValue');
            $this->load->integration_model('Category');
            $this->load->integration_model('Taxes');

            foreach ($integrations as $integration) {
                $Taxes = new Taxes();
                $Taxes->open($integration);
                $taxes = $Taxes->getAll();
                if (empty($taxes)) {
                    $response['error'] = " No se ha sincronizado los impuestos";

                    if ($ajax) {
                        return $response;
                    } else {
                        echo json_encode($response);
                        exit();
                    }

                }
                $Taxes->close();


                $Category = new Category();
                $Category->open($integration);
                $categories = $Category->getAll();
                if (empty($categories)) {
                    $response['error'] = " No se ha sincronizado las Categorías";

                    if ($ajax) {
                        return $response;
                    } else {
                        echo json_encode($response);
                        exit();
                    }

                }
                $Category->close();
                
                // $Attribute = new AttributeIntegration();
                // $Attribute->open($integration);
                // $attributes = $Attribute->getAll();
                // if (empty($attributes)) {
                //     $response['error'] = " No se ha sincronizado los Atributos";
                //     if ($ajax) {
                //         return $response;
                //     } else {
                //         echo json_encode($response);
                //         exit();
                //     }
                // }
                // $Attribute->close();

                // $AttributeValue = new AttributeValue();
                // $AttributeValue->open($integration);
                // $attributeValues1 = $AttributeValue->get(["attribute_id" => 1]);
                // if (empty($attributeValues1)) {
                //     $attributeName = "";
                //     foreach ($attributes as $attribute) {
                //         if ($attribute->id == 1) {
                //             $attributeName = $attribute->name;
                //         }
                //     }

                //     $response['error'] = " No se ha sincronizado los diferentes valores para el atributo $attributeName";
                //     if ($ajax) {
                //         return $response;
                //     } else {
                //         echo json_encode($response);
                //         exit();
                //     }
                // }
                
                // $attributeValues2 = $AttributeValue->get(["attribute_id" => 2]);
                // if (empty($attributeValues2)) {
                //     $attributeName = "";
                //     foreach ($attributes as $attribute) {
                //         if ($attribute->id == 2) {
                //             $attributeName = $attribute->name;
                //         }
                //     }

                //     $response['error'] = " No se ha sincronizado los diferentes valores para el atributo $attributeName";
                //     if ($ajax) {
                //         return $response;
                //     } else {
                //         echo json_encode($response);
                //         exit();
                //     }
                // }
                
                // $attributeValues3 = $AttributeValue->get(["attribute_id" => 3]);
                // if (empty($attributeValues3)) {
                //     $attributeName = "";
                //     foreach ($attributes as $attribute) {
                //         if ($attribute->id == 3) {
                //             $attributeName = $attribute->name;
                //         }
                //     }

                //     $response['error'] = " No se ha sincronizado los diferentes valores para el atributo $attributeName";
                //     if ($ajax) {
                //         return $response;
                //     } else {
                //         echo json_encode($response);
                //         exit();
                //     }
                // }
                // $AttributeValue->close();
            }            
        }
    }

    function suggestions()
    {
        $getCost = TRUE;
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductNames($term);
        if ($rows) {
            foreach ($rows as $row) {
                $unit_code = $this->products_model->getUnitById($row->unit);
                $unit_code = $unit_code ? $unit_code->code : NULL;
                $units_data = $this->site->getUnitsByBUID($row->unit);
                $units = [];
                foreach ($units_data as $unitdata) {
                    $units[$unitdata->id] = $unitdata;
                }
                $pr[] = array(
                    'id' => $row->id,
                    'label' => $row->name . " (" . $row->code . ")",
                    'code' => $row->code,
                    'name' => $row->name,
                    'price' => ($getCost ? $row->cost : $row->price),
                    'unit' => $row->unit,
                    'units' => $units,
                    'unit_code' => $unit_code,
                    'tax_rate' => $row->tax_rate,
                    'qty' => 1
                );
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function get_suggestions()
    {
        $term = $this->input->get('term', TRUE);
        $warehouse_id = $this->input->get('warehouse_id');
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $rows = $this->products_model->getProductsForPrinting($term, $warehouse_id);
        if ($rows) {
            foreach ($rows as $row) {
                $variants = $this->products_model->getProductOptionsForPrinting($row->id, $warehouse_id);
                $pr[] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => 1, 'variants' => $variants, 'variant_selected' => (isset($row->variant_selected) ? $row->variant_selected : NULL));
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function addByAjax()
    {
        if (!$this->mPermissions('add')) {
            exit(json_encode(array('msg' => lang('access_denied'))));
        }
        if ($this->input->get('token') && $this->input->get('token') == $this->session->userdata('user_csrf') && $this->input->is_ajax_request()) {
            $product = $this->input->get('product');
            if (!isset($product['code']) || empty($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_is_required'))));
            }
            if (!isset($product['name']) || empty($product['name'])) {
                exit(json_encode(array('msg' => lang('product_name_is_required'))));
            }
            if (!isset($product['category_id']) || empty($product['category_id'])) {
                exit(json_encode(array('msg' => lang('product_category_is_required'))));
            }
            if (!isset($product['unit']) || empty($product['unit'])) {
                exit(json_encode(array('msg' => lang('product_unit_is_required'))));
            }
            if (!isset($product['price']) || empty($product['price'])) {
                exit(json_encode(array('msg' => lang('product_price_is_required'))));
            }
            if (!isset($product['cost']) || empty($product['cost'])) {
                exit(json_encode(array('msg' => lang('product_cost_is_required'))));
            }
            if ($this->products_model->getProductByCode($product['code'])) {
                exit(json_encode(array('msg' => lang('product_code_already_exist'))));
            }
            if ($row = $this->products_model->addAjaxProduct($product)) {
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $pr = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'qty' => 1, 'cost' => $row->cost, 'name' => $row->name, 'tax_method' => $row->tax_method, 'tax_rate' => $tax_rate, 'discount' => '0');
                $this->sma->send_json(array('msg' => 'success', 'result' => $pr));
            } else {
                exit(json_encode(array('msg' => lang('failed_to_add_product'))));
            }
        } else {
            json_encode(array('msg' => 'Invalid token'));
        }
    }

    /* ------------------------------------------------------- */

    function edit($id = NULL)
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        if ($this->input->post('id')) {
            $id = $this->input->post('id');
        }
        $warehouses = $this->site->getAllWarehouses();
        $warehouses_products = $this->products_model->getAllWarehousesWithPQ($id);
        $product = $this->site->getProductByID($id);
        if ($product->has_multiple_units) {
            $multiple_units_data = [];
            $punitsp = $this->site->get_all_product_units_prices($id);
            if ($punitsp) {
                foreach ($punitsp as $row => $punt) {
                    $unit_data = $this->products_model->getUnitById($punt->unit_id);
                    if (!$unit_data) {
                        unset($punitsp[$row]);
                        continue;
                    }
                    $parent_unit_data = $this->products_model->getUnitById($unit_data->base_unit);
                    $price_group_data = $this->site->getPriceGroupByID($unit_data->price_group_id);
                    $punitsp[$row]->unit_name = $unit_data->name;
                    $punitsp[$row]->unit_operator = $unit_data->operator;
                    $punitsp[$row]->unit_operation_value = $unit_data->operation_value;
                    $punitsp[$row]->parent_unit_name = $parent_unit_data ? $parent_unit_data->name : '';
                    $punitsp[$row]->price_group_name = $price_group_data ? $price_group_data->name : '';
                    $multiple_units_data[$punt->unit_id] = $punitsp[$row];
                }
            }
            $row = $punitsp ? (count($punitsp) + 1) : 1;
            $punitsp[$row] = new stdClass;
            $main_unit_data = $this->products_model->getUnitById($product->unit);
            $main_parent_unit_data = $this->products_model->getUnitById($main_unit_data->base_unit);
            $main_price_group_data = $this->site->getPriceGroupByID($main_unit_data->price_group_id);
            $punitsp[$row]->unit_id = $product->unit;
            $punitsp[$row]->unit_name = $main_unit_data->name;
            $punitsp[$row]->unit_operator = $main_unit_data->operator;
            $punitsp[$row]->unit_operation_value = $main_unit_data->operation_value;
            $punitsp[$row]->parent_unit_name = $main_parent_unit_data ? $main_parent_unit_data->name : '';
            $punitsp[$row]->price_group_name = $main_price_group_data ? $main_price_group_data->name : '';
            $punitsp[$row]->valor_unitario = $product->price;
            $multiple_units_data[$product->unit] = $punitsp[$row];
            $this->data['product_unit_prices'] = $punitsp;
        }
        $product_price_groups = $this->products_model->getProductPriceGroups($id);
        if (!$id || !$product) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('category', lang("category"), 'required|is_natural_no_zero');
        if ($this->input->post('type') == 'standard') {
            $this->form_validation->set_rules('unit', lang("product_unit"), 'required');
        }
        $this->form_validation->set_rules('code', lang("product_code"), 'alpha_dash');
        if ($this->input->post('code') !== $product->code) {
            $this->form_validation->set_rules('code', lang("product_code"), 'is_unique[products.code]');
        }
        if (SHOP) {
            $this->form_validation->set_rules('slug', lang("slug"), 'required|alpha_dash');
            if ($this->input->post('slug') !== $product->slug) {
                $this->form_validation->set_rules('slug', lang("slug"), 'required|is_unique[products.slug]|alpha_dash');
            }
        }
        $this->form_validation->set_rules('weight', lang("weight"), 'numeric');
        $this->form_validation->set_rules('product_image', lang("product_image"), 'xss_clean');
        $this->form_validation->set_rules('digital_file', lang("digital_file"), 'xss_clean');
        $this->form_validation->set_rules('userfile', lang("product_gallery_images"), 'xss_clean');
        if ($this->Settings->product_crud_validate_cost == 1) {
            $this->form_validation->set_rules('cost', lang("cost"), 'required|is_natural_no_zero');
        }

        if ($this->input->post('type') == 'combo') {
            $this->form_validation->set_rules('combo_item_quantity[]', lang("combo_x_items"), 'required');
        }
        if ($this->input->post('preferences')) {
            $this->form_validation->set_rules('preference_id[]', lang('preferences'), 'required');
        }
        if ($this->input->post('attributes') && $this->Settings->product_variant_per_serial == 0) {
            // $this->form_validation->set_rules('attr_quantity[]', lang('attributes'), 'required');
        }
        if ($this->input->post('product_has_multiple_units')) {
            $this->form_validation->set_rules('product_unit_id', lang('product_unit'), 'required');
        }

        if ($this->form_validation->run('products/add') == true) {
            $hybrid_prices = $this->input->post('hybrid_price');
            $deleted_main_image = $this->input->post('deleted_main_image');
            $deleted_images = $this->input->post('deleted_images');
            $attr_suffix = $this->input->post('attr_suffix');
            $old_attr_suffix = $this->input->post('old_attr_suffix');
            $price_groups = $this->input->post('pg[]');
            $price_groups_ipoconsumo = $this->input->post('pg_ipoconsumo[]');
            $price_groups_id = $this->input->post('pg_id[]');
            $billers = $this->input->post('billers[]');
            $wh_max = $this->input->post('wh_max[]');
            $wh_min = $this->input->post('wh_min[]');
            $price_groups_data = [];
            $wh_qty = [];
            foreach ($wh_max as $wh_id => $wh_max_qty) {
                $wh_qty[$wh_id]['max'] = $wh_max_qty;
                $wh_qty[$wh_id]['min'] = $wh_min[$wh_id];
            }
            if ($deleted_images) {
                $deleted_images = explode(", ", trim($deleted_images, ", "));
            }

            if ($this->input->post('tax_method') == 0 && $this->Settings->ipoconsumo) {
                $price = $this->input->post('price') > $this->input->post('consumption_sale_tax') ? $this->input->post('price') - $this->input->post('consumption_sale_tax') : 0;
                $cost = $this->input->post('cost') > $this->input->post('consumption_purchase_tax') ?  $this->input->post('cost') - $this->input->post('consumption_purchase_tax') : 0;
            } else {
                $price = $this->input->post('price');
                $cost = $this->input->post('cost');
            }
            $txt_pprices_changed = "";
            if ($price_groups) {
                // $this->sma->print_arrays($price_groups);
                foreach ($price_groups as $n_row => $pg_price) {
                    if (($this->Owner || $this->Admin || $this->GP['products-cost']) && ($this->Settings->disable_product_price_under_cost == 1 && $pg_price > 0 && $pg_price <= $cost)) {
                        $this->session->set_flashdata('error', lang('product_price_group_under_product_cost'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $price_groups_data[] = [
                        'id' => $price_groups_id[$n_row],
                        'price' => $pg_price,
                        'ipoconsumo' => $price_groups_ipoconsumo[$n_row],
                    ];
                    if (isset($product_price_groups[$price_groups_id[$n_row]]) && $product_price_groups[$price_groups_id[$n_row]]->price != $pg_price) {
                        // $txt_pprices_changed .= $product_price_groups[$price_groups_id[$n_row]]->name." cambió de ".$product_price_groups[$price_groups_id[$n_row]]->price." a ".$pg_price.", ";
                        $txt_pprices_changed .= sprintf(($this->Settings->prioridad_precios_producto == 10 ? lang('product_unit_changed') : lang('price_groups_changed')), $product_price_groups[$price_groups_id[$n_row]]->name, $product_price_groups[$price_groups_id[$n_row]]->price, $pg_price);
                    }
                }
            }
            if (($this->Owner || $this->Admin || $this->GP['products-cost']) && ($this->Settings->disable_product_price_under_cost == 1 && $price <= $cost && $validate_price)) {
                $this->session->set_flashdata('error', lang('product_price_under_product_cost'));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $data = array(
                'code' => $this->input->post('code'),
                'reference' => $this->input->post('reference'),
                'barcode_symbology' => $this->input->post('barcode_symbology'),
                'name' => $this->input->post('name'),
                'type' => $this->input->post('type'),
                'brand' => $this->input->post('brand'),
                'category_id' => $this->input->post('category'),
                'subcategory_id' => $this->input->post('subcategory') ? $this->input->post('subcategory') : NULL,
                // 'cost' => $this->sma->formatDecimal($cost),
                // 'price' => $this->sma->formatDecimal($price),
                'unit' => $this->input->post('unit'),
                'sale_unit' => $this->input->post('default_sale_unit'),
                'purchase_unit' => $this->input->post('default_purchase_unit'),
                'tax_rate' => $this->input->post('tax_rate'),
                'tax_method' => $this->input->post('tax_method'),
                'alert_quantity' => $this->input->post('alert_quantity'),
                'track_quantity' => $this->input->post('track_quantity') ? $this->input->post('track_quantity') : '0',
                'details' => $this->input->post('details'),
                'product_details' => $this->input->post('product_details'),
                'supplier1' => $this->input->post('supplier'),
                'supplier1price' => $this->sma->formatDecimal($this->input->post('supplier_price')),
                'supplier2' => $this->input->post('supplier_2'),
                'supplier2price' => $this->sma->formatDecimal($this->input->post('supplier_2_price')),
                'supplier3' => $this->input->post('supplier_3'),
                'supplier3price' => $this->sma->formatDecimal($this->input->post('supplier_3_price')),
                'supplier4' => $this->input->post('supplier_4'),
                'supplier4price' => $this->sma->formatDecimal($this->input->post('supplier_4_price')),
                'supplier5' => $this->input->post('supplier_5'),
                'supplier5price' => $this->sma->formatDecimal($this->input->post('supplier_5_price')),
                'cf1' => $this->input->post('cf1'),
                'cf2' => $this->input->post('cf2'),
                'cf3' => $this->input->post('cf3'),
                'cf4' => $this->input->post('cf4'),
                'cf5' => $this->input->post('cf5'),
                'cf6' => $this->input->post('cf6'),
                'promotion' => $this->input->post('promotion'),
                'promo_price' => ($this->input->post('promotion') ? $this->sma->formatDecimal($this->input->post('promo_price')) : null),
                'start_date' => ($this->input->post('promotion') ? $this->sma->fld($this->input->post('start_date')) : null),
                'end_date' => ($this->input->post('promotion') ? $this->sma->fld($this->input->post('end_date')) : null),
                'based_on_gram_value' => $this->input->post('based_on_gram_value'),
                'ignore_hide_parameters' => $this->input->post('ignore_hide_parameters') ? $this->input->post('ignore_hide_parameters') : 0,
                'supplier1_part_no' => $this->input->post('supplier_part_no'),
                'supplier2_part_no' => $this->input->post('supplier_2_part_no'),
                'supplier3_part_no' => $this->input->post('supplier_3_part_no'),
                'supplier4_part_no' => $this->input->post('supplier_4_part_no'),
                'supplier5_part_no' => $this->input->post('supplier_5_part_no'),
                'slug' => $this->input->post('slug'),
                'weight' => $this->input->post('weight'),
                'featured' => $this->input->post('featured'),
                'hsn_code' => $this->input->post('hsn_code'),
                'hide_pos' => $this->input->post('hide_pos') ? $this->input->post('hide_pos') : 0,
                'hide_detal' => $this->input->post('hide_detal') ? $this->input->post('hide_detal') : 0,
                'hide_online_store' => $this->input->post('hide_online_store') ? $this->input->post('hide_online_store') : 0,
                'discontinued' => $this->input->post('discontinued') ? $this->input->post('discontinued') : 0,
                'second_name' => $this->input->post('second_name'),
                'profitability_margin' => $this->input->post('profitability_margin'),
                'consumption_sale_tax' => $this->input->post('sale_tax_rate_2_id') ? $this->input->post('consumption_sale_tax') : 0,
                'consumption_purchase_tax' => $this->input->post('purchase_tax_rate_2_id') ? $this->input->post('consumption_purchase_tax') : 0,
                'attributes' => $this->input->post('attributes') ? 1 : 0,
                'has_variants' => $this->input->post('attributes') ? 1 : 0,
                'has_preferences' => $this->input->post('preferences') ? 1 : 0,
                'second_level_subcategory_id' => $this->input->post('second_level_subcategory_id'),
                'main_unit_margin_update_price' => $this->input->post('main_unit_margin_update_price'),
                'paste_balance_value' => $this->input->post('paste_balance_value'),
                'aiu_product' => $this->input->post('aiu_product'),
                'color_id' => $this->input->post('color'),
                'material_id' => $this->input->post('material'),
                'sale_min_qty' => $this->input->post('sale_min_qty'),
                'sale_max_qty' => $this->input->post('sale_max_qty'),
                'warranty_time' => $this->input->post('warranty_time'),
                'height' => $this->input->post('height'),
                'width' => $this->input->post('width'),
                'length' => $this->input->post('length'),
                "tags" => json_encode($this->input->post("tags")),
                'product_code_consecutive' => $this->input->post('product_code_consecutive'),
                'sale_tax_rate_2_id' => $this->input->post('sale_tax_rate_2_id'),
                'purchase_tax_rate_2_id' => $this->input->post('purchase_tax_rate_2_id'),
                'purchase_tax_rate_2_percentage' => $this->input->post('purchase_tax_rate_2_percentage'),
                'sale_tax_rate_2_percentage' => $this->input->post('sale_tax_rate_2_percentage'),
                'sale_tax_rate_2_milliliters' => $this->input->post('sale_tax_rate_2_milliliters'),
                'sale_tax_rate_2_nominal' => ($this->input->post('sale_tax_rate_2_id') == 5) ? $this->input->post('consumption_sale_tax') : $this->input->post('sale_tax_rate_2_nominal'),
                'sale_tax_rate_2_degrees' => $this->input->post('sale_tax_rate_2_degrees'),
                'birthday_discount' => $this->input->post('birthday_discount'),
                'jewel_weight' => $this->input->post('jewel_weight'),
            );

            if ($this->Owner || $this->Admin || $this->GP['products-cost']) {
                $data['cost'] = $this->sma->formatDecimal($cost);
            }
            if ($this->Owner || $this->Admin || $this->GP['products-price'] || $this->GP['system_settings-update_products_group_prices']) {
                $data['price'] = $this->sma->formatDecimal($price);
            }

            if ($this->Owner) {
                $data['wappsi_invoicing'] = ($this->input->post('wappsi_invoicing'));
                $data['wappsi_invoicing_type'] = ($this->input->post('wappsi_invoicing_type'));
                $data['wappsi_invoicing_quantity'] = ($this->input->post('wappsi_invoicing_quantity'));
                $data['wappsi_invoicing_frecuency'] = ($this->input->post('wappsi_invoicing_frecuency'));
            }
            if ($this->input->post('custom_field_value')) {
                $last_cf_code = '';
                foreach ($_POST['custom_field_value'] as $cf_id => $cf_value) {
                    if ($cf_value) {
                        if (!isset($_POST['custom_field_code'][$cf_id])) {
                            if (isset($data[$last_cf_code])) {
                                $data[$last_cf_code] .= ", " . $cf_value;
                            } else {
                                $data[$last_cf_code] = $cf_value;
                            }
                        } else {
                            $last_cf_code = $_POST['custom_field_code'][$cf_id];
                            $cf_code = $_POST['custom_field_code'][$cf_id];
                            $data[$cf_code] = $cf_value;
                        }
                    }
                }
            }

            $this->load->library('upload');
            $pv_old_data = [];
            if ($this->input->post('p_variant_code')) {
                foreach ($this->input->post('p_variant_code') as $pv_id => $pv_code) {
                    $pv_old_data[$pv_id]['code'] = $pv_code;
                    $pv_old_data[$pv_id]['status'] = (isset($_POST['p_variant_status'][$pv_id]) && $_POST['p_variant_status'][$pv_id]) ? 1 : 0;
                    $pv_old_data[$pv_id]['price'] = $_POST['p_variant_price'][$pv_id];
                    $pv_old_data[$pv_id]['minimun_price'] = $_POST['p_variant_minimumPrice'][$pv_id];
                    $pv_old_data[$pv_id]['name'] = $_POST['p_variant_name'][$pv_id];
                    $pv_old_data[$pv_id]['weight'] = $_POST['p_weight'][$pv_id];
                    $pv_old_data[$pv_id]['p_variant_code_consecutive'] = $_POST['p_variant_code_consecutive'][$pv_id] ? 1 : 0;
                    if ($_FILES['p_variant_image_'.$pv_id]['size'] > 0) {
                        $config['upload_path'] = $this->upload_path;
                        $config['allowed_types'] = $this->image_types;
                        $config['max_size'] = $this->allowed_file_size;
                        $config['overwrite'] = FALSE;
                        $config['encrypt_name'] = TRUE;
                        $config['max_filename'] = 25;
                        $this->upload->initialize($config);
                        if (!$this->upload->do_upload('p_variant_image_'.$pv_id)) {
                            $error = $this->upload->display_errors();
                            $this->session->set_flashdata('error', $error);
                            admin_redirect("products/edit/".$id);
                        }
                        $file = $this->upload->file_name;
                        $pv_old_data[$pv_id]['image'] = $file;
                    }
                }
            }
            $warehouse_qty = NULL;
            $product_attributes = NULL;
            $product_preferences = NULL;
            if ($this->input->post('type') == 'standard' || $this->input->post('type') == 'pfinished') {
                for ($s = 2; $s > 5; $s++) {
                    $data['suppliers' . $s] = $this->input->post('supplier_' . $s);
                    $data['suppliers' . $s . 'price'] = $this->input->post('supplier_' . $s . '_price');
                }
                foreach ($warehouses as $warehouse) {
                    $warehouse_qty[] = array(
                        'warehouse_id' => $this->input->post('wh_' . $warehouse->id),
                        'rack' => $this->input->post('rack_' . $warehouse->id) ? $this->input->post('rack_' . $warehouse->id) : NULL
                    );
                }
                $txt_variants_changed = '';
                if ($this->input->post('attributes') && $this->input->post('attr_name') && $this->Settings->product_variant_per_serial == 0) {
                    $txt_variants_changed = 'Se añadieron las ' . lang('variants') . ' : ';
                    $a = sizeof($_POST['attr_name']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['attr_name'][$r])) {
                            if ($product_variatnt = $this->products_model->getPrductVariantByPIDandName($id, trim($_POST['attr_name'][$r]))) {
                                $this->form_validation->set_message('required', lang("product_already_has_variant") . ' (' . $_POST['attr_name'][$r] . ')');
                                $this->form_validation->set_rules('new_product_variant', lang("new_product_variant"), 'required');
                            } else {
                                $product_attributes[$r] = array(
                                    'name' => $_POST['attr_name'][$r],
                                    'warehouse_id' => $_POST['attr_warehouse'][$r],
                                    'quantity' => $_POST['attr_quantity'][$r],
                                    'price' => $_POST['attr_price'][$r],
                                    'minimun_price' => $_POST['attr_minimun_price'][$r],
                                    'code' => $_POST['attr_code'][$r],
                                    'weight' => $_POST['attr_weight'][$r],
                                    'suffix' => isset($_POST['attr_suffix'][$r]) ? $_POST['attr_suffix'][$r] : NULL,
                                    'attr_code_consecutive' => $_POST['attr_code_consecutive'][$r],
                                );
                                if ($_FILES['attr_image']['size'][$r] > 0) {
                                    $_FILES['attr_image_'.$r]['name'] = $_FILES['attr_image']['name'][$r];
                                    $_FILES['attr_image_'.$r]['type'] = $_FILES['attr_image']['type'][$r];
                                    $_FILES['attr_image_'.$r]['tmp_name'] = $_FILES['attr_image']['tmp_name'][$r];
                                    $_FILES['attr_image_'.$r]['error'] = $_FILES['attr_image']['error'][$r];
                                    $_FILES['attr_image_'.$r]['size'] = $_FILES['attr_image']['size'][$r];
                                    $config['upload_path'] = $this->upload_path;
                                    $config['allowed_types'] = $this->image_types;
                                    $config['max_size'] = $this->allowed_file_size;
                                    $config['overwrite'] = FALSE;
                                    $config['encrypt_name'] = TRUE;
                                    $config['max_filename'] = 25;
                                    $this->upload->initialize($config);
                                    if (!$this->upload->do_upload('attr_image_'.$r)) {
                                        $error = $this->upload->display_errors();
                                        $this->session->set_flashdata('error', $error);
                                        admin_redirect("products/edit/".$id);
                                    }
                                    $file = $this->upload->file_name;
                                    $product_attributes[$r]['image'] = $file;
                                }


                                $txt_variants_changed .= $_POST['attr_name'][$r] . (isset($_POST['attr_suffix'][$r]) ? $_POST['attr_suffix'][$r] : '') . ', ';
                            }
                        }
                    }
                } else {
                    $product_attributes = NULL;
                }
            }
            if ($this->input->post('type') == 'standard' || $this->input->post('type') == 'combo' || $this->input->post('type') == 'pfinished') {
                if ($this->input->post('preferences')) {
                    $a = sizeof($_POST['preference_id']);
                    for ($r = 0; $r <= $a; $r++) {
                        if (isset($_POST['preference_id'][$r])) {
                            $product_preferences[] = $_POST['preference_id'][$r];
                        }
                    }
                }
            }
            if ($this->input->post('type') == 'service') {
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'combo' || $this->input->post('type') == 'pfinished' || $this->input->post('type') == 'subproduct') {
                $total_price = 0;
                $c = sizeof($_POST['combo_item_code']) - 1;
                for ($r = 0; $r <= $c; $r++) {
                    if (isset($_POST['combo_item_code'][$r]) && isset($_POST['combo_item_quantity'][$r]) && isset($_POST['combo_item_price'][$r])) {
                        $items[] = array(
                            'item_code' => $_POST['combo_item_code'][$r],
                            'quantity' => $_POST['combo_item_quantity'][$r],
                            'unit_price' => $_POST['combo_item_price'][$r],
                            'unit' => $_POST['combo_item_unit'][$r],
                        );
                    }
                    $total_price += $_POST['combo_item_price'][$r] * $_POST['combo_item_quantity'][$r];
                }
                $data['track_quantity'] = 0;
            } elseif ($this->input->post('type') == 'digital') {
                if ($this->input->post('file_link')) {
                    $data['file'] = $this->input->post('file_link');
                }
                if ($_FILES['digital_file']['size'] > 0) {
                    $config['upload_path'] = $this->digital_upload_path;
                    $config['allowed_types'] = $this->digital_file_types;
                    $config['max_size'] = $this->allowed_file_size;
                    $config['overwrite'] = FALSE;
                    $config['encrypt_name'] = TRUE;
                    $config['max_filename'] = 25;
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload('digital_file')) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/add");
                    }
                    $file = $this->upload->file_name;
                    $data['file'] = $file;
                }
                $config = NULL;
                $data['track_quantity'] = 0;
            }
            if (!isset($items)) {
                $items = NULL;
            }
            if ($_FILES['product_image']['size'] > 0) {
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('product_image')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/edit/" . $id);
                }
                $photo = $this->upload->file_name;
                $data['image'] = $photo;
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = $this->upload_path . $photo;
                $config['new_image'] = $this->thumbs_path . $photo;
                $config['maintain_ratio'] = TRUE;
                $config['width'] = $this->Settings->twidth;
                $config['height'] = $this->Settings->theight;
                $this->image_lib->clear();
                $this->image_lib->initialize($config);
                if (!$this->image_lib->resize()) {
                    echo $this->image_lib->display_errors();
                }
                if ($this->Settings->watermark) {
                    $this->image_lib->clear();
                    $wm['source_image'] = $this->upload_path . $photo;
                    $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                    $wm['wm_type'] = 'text';
                    $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                    $wm['quality'] = '100';
                    $wm['wm_font_size'] = '16';
                    $wm['wm_font_color'] = '999999';
                    $wm['wm_shadow_color'] = 'CCCCCC';
                    $wm['wm_vrt_alignment'] = 'top';
                    $wm['wm_hor_alignment'] = 'left';
                    $wm['wm_padding'] = '10';
                    $this->image_lib->initialize($wm);
                    $this->image_lib->watermark();
                }
                $this->image_lib->clear();
                $config = NULL;
            }
            if ($_FILES['userfile']['name'][0] != "") {
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->image_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['max_width'] = $this->Settings->iwidth;
                $config['max_height'] = $this->Settings->iheight;
                $config['overwrite'] = FALSE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);
                for ($i = 0; $i < $cpt; $i++) {
                    $_FILES['userfile']['name'] = $files['userfile']['name'][$i];
                    $_FILES['userfile']['type'] = $files['userfile']['type'][$i];
                    $_FILES['userfile']['tmp_name'] = $files['userfile']['tmp_name'][$i];
                    $_FILES['userfile']['error'] = $files['userfile']['error'][$i];
                    $_FILES['userfile']['size'] = $files['userfile']['size'][$i];
                    $this->upload->initialize($config);
                    if (!$this->upload->do_upload()) {
                        $error = $this->upload->display_errors();
                        $this->session->set_flashdata('error', $error);
                        admin_redirect("products/edit/" . $id);
                    } else {
                        $pho = $this->upload->file_name;
                        $photos[] = $pho;
                        $this->load->library('image_lib');
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->upload_path . $pho;
                        $config['new_image'] = $this->thumbs_path . $pho;
                        $config['maintain_ratio'] = TRUE;
                        $config['width'] = $this->Settings->twidth;
                        $config['height'] = $this->Settings->theight;
                        $this->image_lib->initialize($config);
                        if (!$this->image_lib->resize()) {
                            echo $this->image_lib->display_errors();
                        }
                        if ($this->Settings->watermark) {
                            $this->image_lib->clear();
                            $wm['source_image'] = $this->upload_path . $pho;
                            $wm['wm_text'] = 'Copyright ' . date('Y') . ' - ' . $this->Settings->site_name;
                            $wm['wm_type'] = 'text';
                            $wm['wm_font_path'] = 'system/fonts/texb.ttf';
                            $wm['quality'] = '100';
                            $wm['wm_font_size'] = '16';
                            $wm['wm_font_color'] = '999999';
                            $wm['wm_shadow_color'] = 'CCCCCC';
                            $wm['wm_vrt_alignment'] = 'top';
                            $wm['wm_hor_alignment'] = 'left';
                            $wm['wm_padding'] = '10';
                            $this->image_lib->initialize($wm);
                            $this->image_lib->watermark();
                        }
                        $this->image_lib->clear();
                    }
                }
                $config = NULL;
            } else {
                $photos = NULL;
            }
            $data['quantity'] = isset($wh_total_quantity) ? $wh_total_quantity : 0;
            $wh_locations = [];
            if ($this->input->post('warehouse_zone')) {
                $wh_zones = $this->input->post('warehouse_zone');
                $wh_racks = $this->input->post('warehouse_rack');
                $wh_levels = $this->input->post('warehouse_level');
                foreach ($wh_zones as $wh_id => $wh_zone) {
                    $wh_locations[$wh_id] = $wh_zone . " / " . $wh_racks[$wh_id] . " / " . $wh_levels[$wh_id];
                }
            }
            $txt_fields_changed = '';
            $multiple_units = false;
            if ($this->input->post('product_has_multiple_units')) {
                $munits = $this->input->post('unit_id');
                $munits_cantidad = $this->input->post('unit_operation_value');
                $munitsprices = $this->input->post('unit_price') ? $this->input->post('unit_price') : false;
                $marginup = $this->input->post('margin_update_price') ? $this->input->post('margin_update_price') : false;
                $munitstatus = $this->input->post('unit_status') ? $this->input->post('unit_status') : false;
                $unit_ipoconsumo = $this->input->post('unit_ipoconsumo') ? $this->input->post('unit_ipoconsumo') : false;
                $product_unit_id = $this->input->post('product_unit_id');
                $data['unit'] = $product_unit_id;
                $data['has_multiple_units'] = 1;
                $data['sale_unit'] = $this->input->post('multiple_default_sale_unit');
                $data['purchase_unit'] = $this->input->post('multiple_default_purchase_unit');
                if ($munits) {
                    foreach ($munits as $row => $unit_id) {
                        if ($unit_id != $data['unit']) {
                            if ($munitsprices && isset($multiple_units_data[$unit_id]) && $this->sma->formatDecimals($multiple_units_data[$unit_id]->valor_unitario) != $this->sma->formatDecimals($munitsprices[$row])) {
                                if ($txt_fields_changed == '') {
                                    $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name . " " . $this->session->last_name, lang($this->m), $id);
                                }
                                $txt_fields_changed .= " Cambió precio de la unidad ".$multiple_units_data[$unit_id]->unit_name." de ".$this->sma->formatDecimals($multiple_units_data[$unit_id]->valor_unitario)." a ".$this->sma->formatDecimals($munitsprices[$row]);
                            }
                            if (isset($multiple_units_data[$unit_id]) && $this->sma->formatDecimals($multiple_units_data[$unit_id]->margin_update_price) != $this->sma->formatDecimals($marginup[$row])) {
                                if ($txt_fields_changed == '') {
                                    $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name . " " . $this->session->last_name, lang($this->m), $id);
                                }
                                $txt_fields_changed .= " Cambió margen de la unidad ".$multiple_units_data[$unit_id]->unit_name." de ".$this->sma->formatDecimals($multiple_units_data[$unit_id]->margin_update_price)." a ".$this->sma->formatDecimals($marginup[$row]);
                            }

                            if (!isset($multiple_units_data[$unit_id])) {
                                if ($txt_fields_changed == '') {
                                    $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name . " " . $this->session->last_name, lang($this->m), $id);
                                }
                                $multiple_new_unit_data = $this->site->getUnitByID($unit_id);
                                $txt_fields_changed .= " Añadió la unidad ".$multiple_new_unit_data->name." con precio ".$this->sma->formatDecimals($munitsprices[$row]);
                            }
                            $multiple_units[] = [
                                'unit_id' => $unit_id,
                                'cantidad' => $munits_cantidad[$row],
                                'price' => $munitsprices ? $munitsprices[$row] : 0,
                                'margin_update_price' => $marginup && isset($marginup[$row]) ? $marginup[$row] : 0,
                                'unit_ipoconsumo' => $unit_ipoconsumo ? $unit_ipoconsumo[$row] : 0,
                                'status' => $munitstatus && isset($munitstatus[$row]) && $munitstatus[$row] ? 1 : 0,
                            ];
                            if ($munitsprices && isset($product_price_groups[$unit_id]) && $product_price_groups[$unit_id]->price != ($munitsprices ? $munitsprices[$row] : 0)) {
                                $txt_pprices_changed .= sprintf(lang('product_unit_changed'), $product_price_groups[$unit_id]->name, $product_price_groups[$unit_id]->price, ($munitsprices ? $munitsprices[$row] : 0));
                            }
                        }
                        if (isset($multiple_units_data[$unit_id])) {
                            unset($multiple_units_data[$unit_id]);
                        }
                    }
                }
            }
            if (isset($multiple_units_data) && count($multiple_units_data) > 0) {
                foreach ($multiple_units_data as $unit_id_ch => $unit_data_ch) {
                    $txt_pprices_changed .= " Eliminó la unidad ".$unit_data_ch->unit_name;
                }
            }
            // $this->sma->print_arrays($multiple_units);
            // exit(var_dump($txt_fields_changed));
            foreach ($data as $field => $new_value) {
                if (isset($product->{$field}) && $new_value && $product->{$field} != $new_value) {
                    if ($txt_fields_changed == '') {
                        $txt_fields_changed = sprintf(lang('user_fields_changed'), $this->session->first_name . " " . $this->session->last_name, lang($this->m), $id);
                    }
                    if ($field == 'supplier1' || $field == 'supplier2' || $field == 'supplier3' || $field == 'supplier4' || $field == 'supplier5') {
                        $supplier = $this->site->getCompanyByID($new_value);
                        $new_value = '';
                        if ($supplier) {
                            $new_value = $supplier->company;
                        }
                        $supplier = $this->site->getCompanyByID($product->{$field});;
                        $product->{$field} = '';
                        if ($supplier) {
                            $product->{$field} = $supplier->company;
                        }
                    }
                    if ($field == 'tax_rate') {
                        $changed_tax_rate = $this->site->getTaxRateByID($product->{$field});
                        $product->{$field} = $changed_tax_rate->name;
                        $changed_tax_rate = $this->site->getTaxRateByID($new_value);
                        $new_value = $changed_tax_rate->name;
                    }
                    if ($field == 'category_id') {
                        $changed_category = $this->site->getCategoryByID($product->{$field});
                        $product->{$field} = $changed_category->name;
                        $changed_category = $this->site->getCategoryByID($new_value);
                        $new_value = $changed_category->name;
                    }
                    if ($field == 'brand') {
                        if (!empty($product->{$field})) {
                            $changed_brand = $this->site->getBrandByID($product->{$field});
                            $product->{$field} = $changed_brand->name;
                        } else {
                            $product->{$field} = 'Sin asignar';
                        }
                        if (!empty($new_value)) {
                            $changed_brand = $this->site->getBrandByID($new_value);
                            $new_value = $changed_brand->name;
                        } else {
                            $new_value = 'Sin asignar';
                        }
                    }
                    if ($field == 'unit' || $field == 'sale_unit' || $field == 'purchase_unit') {
                        $changed_unit = $this->site->getUnitByID($product->{$field});
                        $product->{$field} = $changed_unit->name;
                        $changed_unit = $this->site->getUnitByID($new_value);
                        $new_value = $changed_unit->name;
                    }
                    if ($field == 'type') {
                        $product->{$field} = lang($product->{$field});
                        $new_value = lang($new_value);
                    }
                    $txt_fields_changed .= sprintf(lang('fields_changed'), lang($field), $product->{$field}, $new_value);

                    if ($field == 'cost') {
                        // exit($txt_fields_changed);
                    }
                }
            }
            if (isset($txt_pprices_changed) && $txt_pprices_changed != "") {
                $txt_fields_changed .= $txt_pprices_changed;
            }
            if (isset($txt_variants_changed) && $txt_variants_changed != "") {
                $txt_fields_changed .= $txt_variants_changed;
            }
            $txt_attr_suffix_changed = "";
            if ($old_attr_suffix && count($old_attr_suffix) > 0) {
                foreach ($old_attr_suffix as $attr_suffix_option_id => $attr_suffix_value) {
                    $attr_data = $this->products_model->get_product_option_by_id($attr_suffix_option_id);
                    if ($attr_data->suffix != $attr_suffix_value) {
                        $txt_attr_suffix_changed = " El sufijo de la variante " . $attr_data->name . " cambió de " . $attr_data->suffix . " a " . $attr_suffix_value;
                    }
                }
            }
            if ($txt_attr_suffix_changed != "") {
                $txt_fields_changed .= $txt_attr_suffix_changed;
            }
            $arr_data = [
                'id' => $id,
                'data' => $data,
                'items' => $items,
                'warehouse_qty' => $warehouse_qty,
                'product_attributes' => $product_attributes,
                'photos' => $photos,
                'old_attr_suffix' => $old_attr_suffix,
                'product_preferences' => $product_preferences,
                'wh_locations' => $wh_locations,
                'price_groups_data' => $price_groups_data,
                'multiple_units' => $multiple_units,
                'hybrid_prices' => $hybrid_prices,
                'billers' => $billers,
                'deleted_images' => $deleted_images,
                'deleted_main_image' => $deleted_main_image,
                'wh_qty' => $wh_qty,
                'pv_old_data' => $pv_old_data,
                'billers_changed' => $this->input->post('billers_changed'),
            ];
            // $this->sma->print_arrays($arr_data);
        }

        if ($this->form_validation->run() == true && $this->products_model->updateProduct($arr_data)) {
            $syncUpdatedProduct = $this->syncUpdatedProduct($arr_data);

            $this->session->set_flashdata('message', lang("product_updated"));
            if (!empty($txt_fields_changed)) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 1,
                    'table_name' => 'products',
                    'record_id' => $id,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $txt_fields_changed,
                ]);
            }

            if (isset($syncUpdatedProduct->warning) && !empty($syncUpdatedProduct->warning)) {
                $this->session->set_flashdata('warning', lang("product_added") .  $syncUpdatedProduct->warning);
            }
            if (isset($syncUpdatedProduct->message) && !empty($syncUpdatedProduct->message)) {
                $this->session->set_flashdata('message', lang("product_added") .  $syncUpdatedProduct->message);
            }
            if (isset($syncUpdatedProduct->error)) {
                $this->session->set_flashdata('error', lang("product_not_added") .  $syncUpdatedProduct->error);
            }

            admin_redirect('products');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['units'] = $this->site->get_all_units();
            $this->data['warehouses'] = $warehouses;
            $this->data['warehouses_products'] = $warehouses_products;
            $this->data['product'] = $product;
            $this->data['preferences_categories'] = $this->products_model->get_preferences_categories();
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            $this->data['billers_selected'] = $this->products_model->get_products_billers($product->id);
            $supplier_prices = [];
            if ($product->supplier1price > 0 || $product->supplier1) {
                $supplier_prices[] = ['date' => $product->supplier1price_date, 'price' => $product->supplier1price, 'supplier_id' => $product->supplier1, 'part_no' => $product->supplier1_part_no];
            }
            if ($product->supplier2price > 0|| $product->supplier2) {
                $supplier_prices[] = ['date' => $product->supplier2price_date, 'price' => $product->supplier2price, 'supplier_id' => $product->supplier2, 'part_no' => $product->supplier2_part_no];
            }
            if ($product->supplier3price > 0|| $product->supplier3) {
                $supplier_prices[] = ['date' => $product->supplier3price_date, 'price' => $product->supplier3price, 'supplier_id' => $product->supplier3, 'part_no' => $product->supplier3_part_no];
            }
            if ($product->supplier4price > 0|| $product->supplier4) {
                $supplier_prices[] = ['date' => $product->supplier4price_date, 'price' => $product->supplier4price, 'supplier_id' => $product->supplier4, 'part_no' => $product->supplier4_part_no];
            }
            if ($product->supplier5price > 0|| $product->supplier5) {
                $supplier_prices[] = ['date' => $product->supplier5price_date, 'price' => $product->supplier5price, 'supplier_id' => $product->supplier5, 'part_no' => $product->supplier5_part_no];
            }
            $supplier_prices_order = [];
            foreach ($supplier_prices as $sppr) {
                $supplier_prices_order[] = $sppr['date'];
            }
            array_multisort($supplier_prices_order, SORT_DESC, $supplier_prices);
            $this->data['supplier_prices'] = $supplier_prices;
            $punitsp = [];
            $this->data['variants'] = $this->products_model->getAllVariants();
            $this->data['colors'] = $this->products_model->get_colors();
            $this->data['materials'] = $this->products_model->get_materials();
            $this->data['subunits'] = $this->site->getUnitsByBUID($product->unit);
            $this->data['product_preferences'] = $this->products_model->getProductPreferences($id);
            $this->data['has_purchases'] = $this->products_model->has_purchase($id);
            $this->data['combo_items'] = $product->type == 'combo' || $product->type == 'pfinished' || $product->type == 'subproduct' ? $this->products_model->getProductComboItems($product->id) : NULL;
            $this->data['product_options'] = $id ? $this->products_model->getProductOptionsWithWH($id) : NULL;
            $this->data['product_variants'] = $this->products_model->getProductOptions($id, NULL, NULL, true);
            $this->data['product_price_groups'] = $product_price_groups;
            $this->data['price_groups'] = $this->site->getAllPriceGroups();
            $this->data["custom_fields"] = $this->site->get_custom_fields(2);
            $this->data['images'] = $this->products_model->getProductPhotos($id);
            $this->data['customer_js'] = $this->site->customers_js();
            $this->data["tags"] = $this->products_model->getTags(["module"=>1]);
            $this->data["second_taxes"] = $this->site->getSecondTaxes();
            $this->data["movementsExistingProducts"] = $this->products_model->productHasMovements($id, false);

            if ($this->Settings->prioridad_precios_producto == 11) {
                $this->data['hybrid_prices'] = $this->site->get_all_product_hybrid_prices($id);
                // $this->sma->print_arrays($this->data['hybrid_prices']);
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_product')));
            $meta = array('page_title' => lang('edit_product'), 'bc' => $bc);
            $this->page_construct('products/edit', $meta, $this->data);
        }
    }

    private function syncUpdatedProduct($data)
    {
        $_errorMessage = '';
        $_message = '';
        $response = [];

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $response = $this->validatePreSyncProducts(true);
            if (empty($response)) {
                $TaxRatesModel = new TaxRates_model();

                $integrations = $this->settings_model->getAllIntegrations();
                $productId = $data["id"];
                $product = $this->products_model->getProductByID($productId);
                $productPreferences = $this->products_model->getProductPreferences($productId);
                $unit = $this->site->getUnitByID($product->sale_unit);
                $tax = $TaxRatesModel->find($product->tax_rate);

                if (!empty($integrations) && !empty($product)) {
                    $this->load->integration_model('Product');
                    $this->load->integration_model('Brand');
                    $this->load->integration_model('Category');

                    foreach ($integrations as $integration) {
                        $Product = new Product();
                        $Product->open($integration);
                        $productStore = $Product->find(["id_wappsi"=>$productId]);

                        $Brand = new Brand();
                        $Brand->open($integration);
                        $brandExt = $Brand->find(["id_wappsi" => $product->brand]);

                        $Category = new Category();
                        $Category->open($integration);
                        $categoryExt = $Category->find(["id_wappsi" => $product->category_id]);

                        if (!empty($productStore)) {
                            $quantity = $this->getQuantityProduct($product->id);

                            $productData = [
                                "name"               => $product->name,
                                "brand_id"           => $brandExt->id,
                                "description"        => $product->product_details,
                                "slug"               => $product->slug,
                                "discount"           => $this->getDiscountValue($product),
                                "discount_type"      => "flat",
                                "discount_start_date"=> ($this->shop_settings->update_promotions_to_store == YES && !empty($product->promotion)) ? strtotime($product->start_date) : null,
                                "discount_end_date"  => ($this->shop_settings->update_promotions_to_store == YES && !empty($product->promotion)) ? strtotime($product->end_date) : null,
                                "stock"              => ($quantity >= $this->shop_settings->activate_available_from_quantity) ? ACTIVE : INACTIVE,
                                "published"          => ($product->hide_online_store == YES) ? INACTIVE : (($quantity >= $this->shop_settings->activate_available_from_quantity) ? ACTIVE : INACTIVE),
                                "approved"           => $product->discontinued ^= 1,
                                "unit"               => $unit->name,
                                "min_qty"            => $product->sale_min_qty,
                                "max_qty"            => $product->sale_max_qty,
                                "is_variant"         => (!empty($data["pv_old_data"]) || !empty($data["product_attributes"])) ? YES : NOT,
                                "has_warranty"       => (!empty($product->warranty_time)) ? YES : NOT,
                                "weight"             => $product->weight,
                                "height"             => $product->height,
                                "length"             => $product->length,
                                "width"              => $product->width,
                                "meta_title"         => $product->name,
                                "meta_description"   => strip_tags($product->name),
                                "slug"               => $product->slug,
                                "rating"             => 0,
                                "digital"            => ($product->type == 'digital') ? YES : NOT,
                                "main_category"      => $categoryExt->id,
                                "tags"               => $this->getTags($product->tags),
                                "lowest_price"       => ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price,
                                "highest_price"      => ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price,
                                "quantity"           => $quantity,
                                "updated_at"         => date("Y-m-d H:i:s"),
                            ];

                            if ($Product->update($productData, $productStore->id)) {
                                $this->products_model->updateStatusSyncStore(2, $product->id);
                                $_message .= $this->site->getnameTypeIntegration($integration->type) . ", ";

                                $this->syncDeletedProductCategory($productId);

                                $this->syncCreateProductCategory($product, $product->category_id);

                                if (!empty($product->subcategory_id) && $product->subcategory_id != NULL) {
                                    $this->syncCreateProductCategory($product, $product->subcategory_id);
                                }

                                if (!empty($product->second_level_subcategory_id) && $product->second_level_subcategory_id != NULL) {
                                    $this->syncCreateProductCategory($product, $product->second_level_subcategory_id);
                                }

                                if (!empty($product->color_id)) {
                                    $this->syncDeletedProductAttribute($productId,  2);
                                    $this->syncCreateProductAttribute($product, 2, $product->color_id);
                                }

                                if (!empty($product->material_id)) {
                                    $this->syncDeletedProductAttribute($productId,  3);
                                    $this->syncCreateProductAttribute($product, 3, $product->material_id);
                                }

                                // $this->sycnDeleteAllProductVariation($productId);
                                $this->syncUpdateProductVariant($data, $integration);

                                if (!empty($data["product_attributes"])) {
                                    $this->borrarElNull($productId, $integration);
                                    foreach ($data["product_attributes"] as $productAttribute) {
                                        $variant = $this->settings_model->getVariant(["name"=> $productAttribute["name"]]);
                                        $this->syncCreateProductAttribute($product, 1, $variant->id);

                                        $productVariant = $this->products_model->getProductVariantID($productId, $productAttribute["name"]);
                                        $this->syncCreateProductVariant($productId, $productVariant);
                                    }
                                } else {
                                    $this->syncCreateProductVariant($productId);
                                }

                                if (!empty($productPreferences)) {
                                    $preferenceCategories = $this->getCategoryPreferences($productPreferences);
                                    if (!empty($preferenceCategories)) {
                                        foreach ($preferenceCategories as $preferenceCategory) {
                                            $this->syncDeletedProductAttribute($productId, $preferenceCategory);
                                        }
                                    }
                                }

                                if (!empty($data["product_preferences"])) {
                                    foreach ($data["product_preferences"] as $productPreferenceId) {
                                        $preference = $this->products_model->get_preference_by_id($productPreferenceId);
                                        $this->syncCreateProductAttribute($product,  $preference->category_id, $productPreferenceId, true);
                                    }
                                }

                                $this->syncTax($product);

                            }
                        } else {
                            $this->syncCreatedProduct($productId, $data);
                        }

                        $Product->close();
                    }

                    if (!empty($_message)) {
                        $response["message"] = " Sincronizado en: ". rtrim($_message, ", ");
                    }
                    if (!empty($_errorMessage)) {
                        $response["warning"] = " No fue posible sincronizar en: ". rtrim($_errorMessage, ", ");
                    }
                }
            }
        }

        return (object) $response;
    }

    private function borrarElNull($productWappsiId, $integration) 
    {        
        $Product = new Product();
        $Product->open($integration);
        $product = $Product->find(["id_wappsi"=>$productWappsiId]);


        $this->load->integration_model('ProductVariation');
        $ProductVariation = new ProductVariation();
        $ProductVariation->open($integration);

        $productVariantsStore = $ProductVariation->get(['product_id' => $product->id]);
        if (!empty($productVariantsStore)) {
            foreach ($productVariantsStore as $productVariantStore) {
                if ($productVariantStore->code == null) {
                    $ProductVariation->delete($productVariantStore->id);
                }
            }
        }

        $ProductVariation->close();
        $Product->close();
    }

    private function syncDeletedProductCategory($productId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();

            if (!empty($integrations)) {
                $this->load->integration_model('ProductCategory');
                $this->load->integration_model('Product');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);

                    $ProductCategory = new ProductCategory();
                    $ProductCategory->open($integration);

                    $productExt = $Product->find(["id_wappsi" => $productId]);

                    $ProductCategory->deleteByProductId($productExt->id);
                    $ProductCategory->close();
                    $Product->close();
                }
            }
        }
    }

    private function getCategoryPreferences($preferences)
    {
        $categoryPreference = [];
        foreach ($preferences as $preference) {
            if (!in_array($preference->preference_category_id, $categoryPreference)) {
                $categoryPreference[] = $preference->preference_category_id;
            }
        }
        return $categoryPreference;
    }

    private function syncUpdatedProductAttributeValue($productId, $attributeId, $attributeValueId)
    {
        $settings = new Settings_model();
        $site = new Site();

        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $settings->getAllIntegrations();

            if (!empty($integrations) && !empty($productId)) {
                $this->load->integration_model('ProductAttributeValue');
                $this->load->integration_model('AttributeValue');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);
                    $productExt = $Product->find(["id_wappsi" => $productId]);

                    $ProductAttributeValue = new ProductAttributeValue();
                    $ProductAttributeValue->open($integration);

                    $AttributeValue = new AttributeValue();
                    $AttributeValue->open($integration);
                    $attributeValue = $AttributeValue->find(["attribute_id"=>$attributeId ,"id_wappsi"=>$attributeValueId]);

                    $productAttributeValue = $ProductAttributeValue->find(["product_id"=>$productExt->id, "attribute_id"=>$attributeId]);

                    $data = [
                        "attribute_value_id"=> $attributeValue->id,
                        "updated_at"        => date("Y-m-d H:i:s"),
                    ];
                    $ProductAttributeValue->update($data, $productAttributeValue->id);
                    $ProductAttributeValue->close();
                }
            }
        }
    }

    private function syncDeletedProductAttribute($productWappsiId, $attributeId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Product');
                $this->load->integration_model('AttributeIntegration');
                $this->load->integration_model('ProductAttribute');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);
                    $product = $Product->find(["id_wappsi"=>$productWappsiId]);

                    $Attribute = new AttributeIntegration();
                    $Attribute->open($integration);
                    $attribute = $Attribute->find(["id"=>$attributeId]);

                    $ProductAttribute = new ProductAttribute();
                    $ProductAttribute->open($integration);
                    $productAttribute = $ProductAttribute->find(["product_id"=>$product->id, "attribute_id"=>$attribute->id]);

                    if (!empty($productAttribute)) {
                        $ProductAttribute->delete($productAttribute->id);
                        $this->syncDeletedProductAttributeValue($productWappsiId,  $attributeId);
                    }

                    $ProductAttribute->close();
                }
            }
        }
    }

    private function syncDeletedProductAttributeValue($productWappsiId,  $attributeId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Product');
                $this->load->integration_model('AttributeIntegration');
                $this->load->integration_model('ProductAttributeValue');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);
                    $product = $Product->find(["id_wappsi" => $productWappsiId]);

                    $Attribute = new AttributeIntegration();
                    $Attribute->open($integration);
                    $attribute = $Attribute->find(["id" => $attributeId]);
                    if ($attribute) {
                        $ProductAttributeValue = new ProductAttributeValue();
                        $ProductAttributeValue->open($integration);
                        $productAttributeValue = $ProductAttributeValue->find(["product_id" => $product->id, "attribute_id" => $attribute->id]);

                        $ProductAttributeValue->delete($productAttributeValue->id);
                        $ProductAttributeValue->close();
                    }
                }
            }
        }
    }

    private function syncUpdateProductVariant($data, $integration)
    {
        $TaxRatesModel = new TaxRates_model();

        $pruductVariantsData = (isset($data['pv_old_data'])) ? $data['pv_old_data'] : $data['product_attributes'];
        $productId = $data['id'];
        $tax = $TaxRatesModel->find($data["data"]["tax_rate"]);
        
        $this->load->integration_model('ProductVariation');
        $ProductVariation = new ProductVariation();
        $ProductVariation->open($integration);

        $this->load->integration_model('Product');
        $Product = new Product();
        $Product->open($integration);
        $productStore = $Product->find(['id_wappsi' => $data['id']]);

        if (!empty($pruductVariantsData)) {
            $productPrice = ($tax->type == 1) ? $data['data']['price'] / (1 + ($tax->rate / 100)) : $data['data']['price'];
            foreach ($pruductVariantsData as $productVariantId => $productVariant) {
                $productVariantPrice = $productVariant['price'];
                $productVariantStatus = $productVariant['status'];
                $productVariantStore = $ProductVariation->find(['product_id' => $productStore->id, 'id_wappsi' => $productVariantId]);
                if (!empty($productVariantStore)) {
                    $quantity = $this->getQuantityProductVariant($productId, $productVariantId);
                    $productVariantData = [
                        'sku'       => $productVariant['code'],
                        'price'     => (isset($productVariantPrice) && !empty($productVariantPrice)) ? $productPrice + $productVariantPrice : $productPrice,
                        "stock"     => ($quantity >= $this->shop_settings->activate_available_from_quantity && $productVariantStatus == YES) ? ACTIVE : INACTIVE,
                        "quantity"  => $quantity,
                    ];

                    // if ($this->Settings->prioridad_precios_producto == 4) {
                        $productPrices = $this->products_model->getProductPrices(["product_id" => $productId]);
                        
                        if (isset($productPrices) && !empty($productPrices)) {
                            $i = 2;
                            foreach ($productPrices as $prices) {
                                $priceProductPrice = ($tax->type == 1) ? $prices->price / (1 + ($tax->rate / 100)) : $prices->price;
                                if ($prices->price_group_base == 1) {                                    
                                    $productVariantData["price"] = (isset($productVariantPrice) && !empty($productVariantPrice)) ? $priceProductPrice + $productVariantPrice : $priceProductPrice;
                                } else {
                                    $productVariantData["price_$i"] = (isset($productVariantPrice) && !empty($productVariantPrice)) ? $priceProductPrice + $productVariantPrice : $priceProductPrice;
                                    $i++;
                                }
                            }   
                        }
                    // }

                    $ProductVariation->update($productVariantData, $productVariantStore->id);
                }
            }

            
            $Product->close();
        } else {
            $productVariants = $this->products_model->findProductVariants(["product_id" => $productId]);
            $productVariantionsStore = $ProductVariation->get(['product_id' => $productStore->id]);
            $productPrices = $this->products_model->getProductPrices(["product_id" => $productId]);

            foreach ($productVariantionsStore as $productVariantStore) {
                // if ($this->Settings->prioridad_precios_producto == 4) {      
                    if (isset($productPrices) && !empty($productPrices)) {
                        $i = 2;
                        foreach ($productPrices as $prices) {
                            $priceProductPrice = ($tax->type == 1) ? $prices->price / (1 + ($tax->rate / 100)) : $prices->price;
                            if ($prices->price_group_base == 1) {                                    
                                $productVariantData["price"] =  $priceProductPrice;
                            } else {
                                $productVariantData["price_$i"] = $priceProductPrice;
                                $i++;
                            }
                        }   
                    }
                // }

                    if (empty($productVariants)) {
                        $quantity = $this->getQuantityProduct($productId);

                        $productVariantData["stock"] = ($quantity >= $this->shop_settings->activate_available_from_quantity) ? ACTIVE : INACTIVE;
                        $productVariantData["quantity"] = $quantity;
                    }

                $ProductVariation->update($productVariantData, $productVariantStore->id);
            }
        }

        $ProductVariation->close();
    }

    /* ---------------------------------------------------------------- */

    function import_csv()
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('xls_file', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["xls_file"])) {
                $archivo = $_FILES['xls_file']['tmp_name'];
                $this->load->library('excel');
                $invalids_products = false;
                $objPHPExcel = PHPExcel_IOFactory::load($archivo);
                $hojas = $objPHPExcel->getWorksheetIterator();
                $txt_errors = "";
                $products = [];
                $products_consecutive_code = [];
                foreach ($hojas as $hoja) {
                    $data = $hoja->toArray();
                    if (
                        $data[0][0] != 'Tipo Producto*' ||
                        $data[0][1] != 'Nombre*' ||
                        $data[0][2] != 'Segundo Nombre' ||
                        $data[0][3] != 'Codigo*' ||
                        $data[0][4] != 'Codigo Consecutivo*' ||
                        $data[0][5] != 'Referencia' ||
                        $data[0][6] != 'Codigo Marca*' ||
                        $data[0][7] != 'Codigo Categoria*' ||
                        $data[0][8] != 'Codigo Subcategoria' ||
                        $data[0][9] != 'Codigo Subcategoria 2 Nivel' ||
                        $data[0][10] != 'Color' ||
                        $data[0][11] != 'Material' ||
                        $data[0][12] != 'Codigo Unidad*' ||
                        $data[0][13] != 'Codigo Unidad Venta*' ||
                        $data[0][14] != 'Codigo Unidad Compra*' ||
                        $data[0][15] != 'Costo*' ||
                        $data[0][16] != 'Precio*' ||
                        $data[0][17] != 'Cantidad de Alerta' ||
                        $data[0][18] != 'Codigo Impuesto*' ||
                        $data[0][19] != 'Codigo Impuesto Saludable Venta' ||
                        $data[0][20] != 'Porcentaje Impuesto Saludable Venta' ||
                        $data[0][21] != 'Valor Nominal Impuesto Saludable Venta' ||
                        $data[0][22] != 'Codigo Impuesto Saludable Compra' ||
                        $data[0][23] != 'Porcentaje Impuesto Saludable Compra' ||
                        $data[0][24] != 'Valor Nominal Impuesto Saludable Compra' ||
                        $data[0][25] != 'Descripcion Producto' ||
                        $data[0][26] != 'Destacado' ||
                        $data[0][27] != 'Ocultar en POS' ||
                        $data[0][28] != 'Ocultar en DETAL' ||
                        $data[0][29] != 'Ocultar en Tienda en línea' ||
                        $data[0][30] != 'Inactivo' ||
                        $data[0][31] != 'Ignorar parámetros de ocultar' ||
                        $data[0][32] != 'Peso' ||
                        $data[0][33] != 'Alto' ||
                        $data[0][34] != 'Ancho' ||
                        $data[0][35] != 'Largo' ||
                        $data[0][36] != 'Promocion' ||
                        $data[0][37] != 'Precio Promocion' ||
                        $data[0][38] != 'Inicio Promocion' ||
                        $data[0][39] != 'Fin Promocion' ||
                        $data[0][40] != 'Garantía (Días)' ||
                        $data[0][41] != 'NIT Proveedor 1' ||
                        $data[0][42] != 'Código de proveedor 1' ||
                        $data[0][43] != 'Costo de proveedor 1' ||
                        $data[0][44] != 'NIT Proveedor 2' ||
                        $data[0][45] != 'Código de proveedor 2' ||
                        $data[0][46] != 'Costo de proveedor 2' ||
                        $data[0][47] != 'NIT Proveedor 3' ||
                        $data[0][48] != 'Código de proveedor 3' ||
                        $data[0][49] != 'Costo de proveedor 3' ||
                        $data[0][50] != 'Descuento Cumpleaños' ||
                        $data[0][51] != 'Exclusividad Sucursales'
                    ) {
                        $this->session->set_flashdata('error', lang("invalid_import_csv_format"));
                        admin_redirect('products/import_csv');
                    }

                    unset($data[0]);
                    // $this->sma->print_arrays($data);
                    foreach ($data as $data_row) {
                        if (empty($data_row[0])) {
                            continue;
                        }
                        $column = 0;
                        $Tipo_Producto = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Nombre = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Segundo_Nombre = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Consecutivo = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Referencia = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Marca = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Categoria = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Subcategoria = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Subcategoria_2_Nivel = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Color = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Material = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Unidad = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Unidad_Venta = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Unidad_Compra = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Costo = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Precio = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Cantidad_de_Alerta = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Impuesto = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Impuesto_Saludable_Venta = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Porcentaje_Impuesto_Saludable_Venta = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Valor_Nominal_Impuesto_Saludable_Venta = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Impuesto_Saludable_Compra = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Porcentaje_Impuesto_Saludable_Compra = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Valor_Nominal_Impuesto_Saludable_Compra = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Descripcion_Producto = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Destacado = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Ocultar_en_POS = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Ocultar_en_DETAL = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Ocultar_en_Tienda_en_línea = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Inactivo = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Ignorar_parámetros_de_ocultar = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Peso = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Alto = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Ancho = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Largo = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Promocion = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Precio_Promocion = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Inicio_Promocion = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Fin_Promocion = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Garantia = $this->sma->import_field_val($data_row[$column]);$column++;
                        $NIT_Proveedor_1 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_de_proveedor_1 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Costo_de_proveedor_1 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $NIT_Proveedor_2 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_de_proveedor_2 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Costo_de_proveedor_2 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $NIT_Proveedor_3 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_de_proveedor_3 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Costo_de_proveedor_3 = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Descuento_Cumpleaños = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Exclusividad_Sucursales = $this->sma->import_field_val($data_row[$column]);$column++;
                        if (!$this->products_model->getProductByCode($Codigo)) {
                            $proveedor_1 = $proveedor_2 = $proveedor_3 = false;
                            if ($NIT_Proveedor_1) {
                                $proveedor_1 = $this->site->get_supplier_by_vatno($NIT_Proveedor_1);
                            }
                            if ($NIT_Proveedor_2) {
                                $proveedor_2 = $this->site->get_supplier_by_vatno($NIT_Proveedor_2);
                            }
                            if ($NIT_Proveedor_3) {
                                $proveedor_3 = $this->site->get_supplier_by_vatno($NIT_Proveedor_3);
                            }
                            $Tipo_Producto = mb_strtolower($Tipo_Producto) == 'servicio' ? 'service' : 'standard';
                            $category = $this->products_model->getCategoryByCode($Codigo_Categoria);
                            $subcategory = $this->products_model->getCategoryByCode($Codigo_Subcategoria);
                            $subsubcategory = $this->products_model->getCategoryByCode($Codigo_Subcategoria_2_Nivel);
                            $brand = $this->products_model->getBrandByCode($Codigo_Marca);
                            $unit = $this->products_model->getUnitByCode($Codigo_Unidad);
                            $sale_unit = $this->products_model->getUnitByCode($Codigo_Unidad_Venta);
                            $purchase_unit = $this->products_model->getUnitByCode($Codigo_Unidad_Compra);
                            $tax_details = $this->products_model->getTaxRateByCode($Codigo_Impuesto);
                            $color_detail = $this->products_model->getColorByName($Color);
                            $material_detail = $this->products_model->getMaterialByName($Material);
                            $second_tax_details_sale = $this->products_model->getSecondaryTaxRateByCode($Codigo_Impuesto_Saludable_Venta);
                            $second_tax_details_purchase = $this->products_model->getSecondaryTaxRateByCode($Codigo_Impuesto_Saludable_Compra);
                            $Precio = $Precio > 0 ? $Precio : 0;
                            $Costo = $Costo > 0 ? $Costo : 0;
                            $tax2_calculated_sale = $tax2_calculated_purchase = 0;
                            $product_error_msg = false;
                            if (!$category || ($Codigo_Subcategoria && !$subcategory) || ($Codigo_Subcategoria_2_Nivel && !$subsubcategory) || !$brand || !$unit || !$sale_unit || !$purchase_unit || !$tax_details || ($Color && !$color_detail) || ($Material && !$material_detail)) {
                                $product_error_msg = lang('code')." ".lang('product')." ".$Codigo." ".lang('with_following_errors')." : ";
                                if(!$category){
                                    $product_error_msg .= lang('category')." ".lang('invalid').", ";
                                }
                                if($Codigo_Subcategoria && !$subcategory){
                                    $product_error_msg .= lang('subcategory')." ".lang('invalid').", ";
                                }
                                if($Codigo_Subcategoria_2_Nivel && !$subsubcategory){
                                    $product_error_msg .= lang('second_level_subcategory_id')." ".lang('invalid').", ";
                                }
                                if(!$brand){
                                    $product_error_msg .= lang('brand')." ".lang('invalid').", ";
                                }
                                if(!$unit){
                                    $product_error_msg .= lang('unit')." ".lang('invalid').", ";
                                }
                                if(!$sale_unit){
                                    $product_error_msg .= lang('sale_unit')." ".lang('invalid').", ";
                                }
                                if(!$purchase_unit){
                                    $product_error_msg .= lang('purchase_unit')." ".lang('invalid').", ";
                                }
                                if(!$tax_details){
                                    $product_error_msg .= lang('tax_rate')." ".lang('invalid').", ";
                                }
                                if($Color && !$color_detail){
                                    $product_error_msg .= lang('color')." ".lang('invalid').", ";
                                }
                                if($Material && !$material_detail){
                                    $product_error_msg .= lang('material')." ".lang('invalid').", ";
                                }
                                $txt_errors .= $product_error_msg."\n";
                                continue;
                            }

                            if ($second_tax_details_sale && $second_tax_details_sale->value_type == 2 && ($Porcentaje_Impuesto_Saludable_Venta && $Porcentaje_Impuesto_Saludable_Venta)) {
                                $percentage_2tax_sale = $this->sma->get_float_from_percentage_string($Porcentaje_Impuesto_Saludable_Venta);
                                $tax = $this->sma->calculateTax($tax_details->id, $Precio, $this->Settings->tax_method);
                                $Precio_Neto = $Precio - $tax;
                                $tax2_calculated_sale = $this->sma->formatDecimal($Precio_Neto * $percentage_2tax_sale);
                            }

                            if ($second_tax_details_purchase && $second_tax_details_purchase->value_type == 2 && ($Porcentaje_Impuesto_Saludable_Compra && $Porcentaje_Impuesto_Saludable_Compra)) {
                                $percentage_2tax_purchase = $this->sma->get_float_from_percentage_string($Porcentaje_Impuesto_Saludable_Compra);
                                $tax = $this->sma->calculateTax($tax_details->id, $Costo, $this->Settings->tax_method);
                                $Costo_Neto = $Costo - $tax;
                                $tax2_calculated_purchase = $this->sma->formatDecimal($Costo_Neto * $percentage_2tax_purchase);
                            }

                            $pes_arr = false;
                            if ($Exclusividad_Sucursales) {
                                $es_arr = explode(", ", $Exclusividad_Sucursales);
                                $ex_error = false;
                                $ex_error_msg = "";
                                if (is_array($es_arr) && count($es_arr) >= 1) {
                                     foreach ($es_arr as $es_arrkey => $es_arrvalue) {
                                        $biller = $this->site->getCompanyByName($es_arrvalue);
                                        if ($biller) {
                                            $pes_arr[$biller->id] = $biller;
                                        } else {
                                            $ex_error = true;
                                            $ex_error_msg = "No se encontró la sucursal indicada con nombre {$es_arrvalue} para el producto {$Codigo} ";
                                        }
                                    }
                                }
                                if ($ex_error) {
                                    $txt_errors .= $ex_error_msg."\n";
                                    continue;
                                }
                            }

                            $product_arr = [
                                'code' => $Codigo,
                                'name' => $Nombre,
                                'unit' => $unit ? $unit->id : NULL,
                                'cost' => $Costo,
                                'avg_cost' => $Costo,
                                'price' => $Precio,
                                'alert_quantity' => $Cantidad_de_Alerta,
                                'category_id' => $category ? $category->id : NULL,
                                'subcategory_id' => $subcategory ? $subcategory->id : NULL,
                                'tax_rate' => $tax_details ? $tax_details->id : NULL,
                                'details' => $Descripcion_Producto,
                                'product_details' => $Descripcion_Producto,
                                'tax_method' => $this->Settings->tax_method,
                                'type' => $Tipo_Producto,
                                'sale_unit' => $sale_unit ? $sale_unit->id : NULL,
                                'purchase_unit' => $purchase_unit ? $purchase_unit->id : NULL,
                                'brand' => $brand ? $brand->id : NULL,
                                'slug' => $this->sma->slug($Nombre, 'products'),
                                'featured' => $Destacado,
                                'weight' => $Peso,
                                'hide_detal' => $Ocultar_en_DETAL ? $Ocultar_en_DETAL : 0,
                                'hide_pos' => $Ocultar_en_POS ? $Ocultar_en_POS : 0,
                                'hide_online_store' => $Ocultar_en_Tienda_en_línea ? $Ocultar_en_Tienda_en_línea : 0,
                                'second_name' => $Segundo_Nombre,
                                'reference' => $Referencia,
                                'profitability_margin' => $this->sma->formatDecimal((($Precio-$Costo)/$Precio)*100),
                                'ignore_hide_parameters' => $Ignorar_parámetros_de_ocultar,
                                'discontinued' => $Inactivo ? $Inactivo : 0,
                                'registration_date' => date('Y-m-d H:i:s'),
                                'last_update' => date('Y-m-d H:i:s'),
                                'second_level_subcategory_id' => $subsubcategory ? $subsubcategory->id : NULL,
                                'created_by' => $this->session->userdata('user_id'),
                                'material_id' => $material_detail ? $material_detail->id : NULL,
                                'color_id' => $color_detail ? $color_detail->id : NULL,
                                'height' => $Alto,
                                'width' => $Ancho,
                                'length' => $Largo,
                                'sale_tax_rate_2_id' => $second_tax_details_sale ? $second_tax_details_sale->id : NULL,
                                'sale_tax_rate_2_percentage' => $second_tax_details_sale && $second_tax_details_sale->value_type == 2 ? $Porcentaje_Impuesto_Saludable_Venta : NULL,
                                'consumption_sale_tax' => $second_tax_details_sale && $second_tax_details_sale->value_type == 1 ? $Valor_Nominal_Impuesto_Saludable_Venta : $tax2_calculated_sale, //pendiente calcular porcentual
                                'purchase_tax_rate_2_id' => $second_tax_details_purchase ? $second_tax_details_purchase->id : NULL,
                                'purchase_tax_rate_2_percentage' => $second_tax_details_purchase && $second_tax_details_purchase->value_type == 2 ? $Porcentaje_Impuesto_Saludable_Compra : NULL,
                                'consumption_purchase_tax' => $second_tax_details_purchase && $second_tax_details_purchase->value_type == 1 ? $Valor_Nominal_Impuesto_Saludable_Compra : $tax2_calculated_purchase, //pendiente calcular porcentul
                                'promotion' => $Promocion,
                                'promo_price' => $Precio_Promocion,
                                'start_date' => $Inicio_Promocion,
                                'end_date' => $Fin_Promocion,
                                'supplier1' => $proveedor_1 ? $proveedor_1->id : NULL,
                                'supplier2' => $proveedor_2 ? $proveedor_2->id : NULL,
                                'supplier3' => $proveedor_3 ? $proveedor_3->id : NULL,
                                'supplier1price' => $Costo_de_proveedor_1,
                                'supplier2price' => $Costo_de_proveedor_2,
                                'supplier3price' => $Costo_de_proveedor_3,
                                'supplier1_part_no' => $Codigo_de_proveedor_1,
                                'supplier2_part_no' => $Codigo_de_proveedor_2,
                                'supplier3_part_no' => $Codigo_de_proveedor_3,
                                'warranty_time' => $Garantia,
                                'birthday_discount' => $Descuento_Cumpleaños,
                                'product_code_consecutive' => $Codigo_Consecutivo,
                                'billers_exclusivity' => $pes_arr,
                            ];
                        $products[]=$product_arr;
                        if ($product_arr['product_code_consecutive'] == 1) {
                            $products_consecutive_code[]=$product_arr;
                        }
                        } else {
                            $txt_errors .= lang('code')." ".lang('product')." ".$Codigo." ".lang('with_following_errors')." : ".lang('already_exists')."\n";
                        }
                    }
                }
            }
            $pimportvariants = [];
            $pimportvariants_consecutive = [];
            if (isset($_FILES["pv_xls_file"]) && $_FILES["pv_xls_file"]['size'] > 0) {
                $archivo = $_FILES['pv_xls_file']['tmp_name'];
                $this->load->library('excel');
                $invalids_products = false;
                $objPHPExcel = PHPExcel_IOFactory::load($archivo);
                $hojas = $objPHPExcel->getWorksheetIterator();
                foreach ($hojas as $hoja) {
                    $data = $hoja->toArray();
                    unset($data[0]);
                    foreach ($data as $data_row) {
                        if (empty($data_row[0])) {
                            continue;
                        }
                        $column = 0;
                        $Codigo_Producto = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Variante = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Codigo_Consecutivo = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Nombre_Variante = $this->sma->import_field_val($data_row[$column]);$column++;
                        $Sobreprecio_Variante = $this->sma->import_field_val($data_row[$column]);$column++;
                        $pimportvariant = [
                                        'Codigo_Producto' => $Codigo_Producto,
                                        'Codigo_Variante' => $Codigo_Variante,
                                        'Codigo_Consecutivo' => $Codigo_Consecutivo,
                                        'Nombre_Variante' => $Nombre_Variante,
                                        'Sobreprecio_Variante' => $Sobreprecio_Variante,
                                    ];
                        $pimportvariants[]=$pimportvariant;
                        if ($Codigo_Consecutivo == 1) {
                            $pimportvariants_consecutive[]=$pimportvariant;
                        }
                    }
                }
            }
            usort($products, function($a, $b) {
                return strcmp($a['code'], $b['code']);
            });
            usort($products_consecutive_code, function($a, $b) {
                return strcmp($a['code'], $b['code']);
            });
            usort($pimportvariants, function($a, $b) {
                return strcmp($a['Codigo_Variante'], $b['Codigo_Variante']);
            });
            usort($pimportvariants_consecutive, function($a, $b) {
                return strcmp($a['Codigo_Variante'], $b['Codigo_Variante']);
            });
            $order_ref = $this->db->get_where('order_ref', ['ref_id'=>1])->row();
            if (count($products_consecutive_code) > 0 ) {
                if (!$this->sma->validarCodigosContinuos($products_consecutive_code, 'code')) {
                    $this->session->set_flashdata('error', lang("products_automatic_code_no_consecutive"));
                    admin_redirect('products');
                }
                if ($order_ref->product_code != $products_consecutive_code[0]['code']) {
                    $this->session->set_flashdata('error', sprintf(lang("products_automatic_code_doesnt_match"), $products_consecutive_code[0]['code'], $order_ref->product_code));
                    admin_redirect('products');
                }
            }
            if (count($pimportvariants_consecutive) > 0 ) {
                if (!$this->sma->validarCodigosContinuos($pimportvariants_consecutive, 'Codigo_Variante')) {
                    $this->session->set_flashdata('error', lang("pvariants_automatic_code_no_consecutive"));
                    admin_redirect('products');
                }
                if ($order_ref->variant_code != $pimportvariants_consecutive[0]['Codigo_Variante']) {
                    $this->session->set_flashdata('error', sprintf(lang("pvariants_automatic_code_doesnt_match"), $pimportvariants_consecutive[0]['Codigo_Variante'], $order_ref->product_code));
                    admin_redirect('products');
                }
            }
            // $this->sma->print_arrays($products, $pimportvariants, $txt_errors);
        }

        if ($this->form_validation->run() == true && $response = $this->products_model->add_products($products, $pimportvariants)) {
            if ($response['txt_errors'] && $response['txt_errors'] != "") {
                $txt_errors.=$response['txt_errors'];
            }
            if ($txt_errors != "") {
                $nombreArchivo = "importacion_productos_".date('Y_m_d_H_i_s').".txt";
                // Generar el contenido del archivo TXT
                $contenido = $txt_errors;
                // Nombre del archivo que se va a crear
                $archivo = $nombreArchivo;
                // Ruta completa del archivo en el servidor
                $ruta_archivo = 'assets/' . $archivo;
                // Crear el archivo y escribir el contenido
                file_put_contents($ruta_archivo, $contenido);
                // Configurar las cabeceras para la descarga
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($archivo) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($ruta_archivo));
                // Leer el archivo y enviar su contenido al navegador
                readfile($ruta_archivo);
                // Eliminar el archivo después de la descarga
                unlink($ruta_archivo);
            }

            if ($response['products_added'] == 0) {
                $this->session->set_flashdata('error', lang("import_file_data_with_errors"));
            } else if ($response['products_added'] > 0 && $txt_errors == "") {
                $this->session->set_flashdata('message', lang("products_added"));
                admin_redirect('products');
            } else if ($response['products_added'] > 0 && $txt_errors != "") {
                $this->session->set_flashdata('warning', lang("products_added_with_errors"));
            }
        } else {
            $this->data['order_ref'] = $this->db->get_where('order_ref', ['ref_id'=>1])->row();
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['userfile'] = array(
                'name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('import_products_by_csv')));
            $meta = array('page_title' => lang('import_products_by_csv'), 'bc' => $bc);
            $this->page_construct('products/import_csv', $meta, $this->data);
        }
    }

    /* ------------------------------------------------------------------ */

    function update_price()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (DEMO) {
                $this->session->set_flashdata('message', lang("disabled_in_demo"));
                admin_redirect('welcome');
            }

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'price');

                $final = array();

                foreach ($arrResult as $key => $value) {
                    $final[] = array_combine($keys, $value);
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if (!$this->products_model->getProductByCode(trim($csv_pr['code']))) {
                        $this->session->set_flashdata('message', lang("check_product_code") . " (" . $csv_pr['code'] . "). " . lang("code_x_exist") . " " . lang("line_no") . " " . $rw);
                        admin_redirect("products");
                    }
                    $rw++;
                }
            }
        } elseif ($this->input->post('update_price')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("system_settings/group_product_prices/" . $group_id);
        }

        if ($this->form_validation->run() == true && !empty($final)) {
            $this->products_model->updatePrice($final);
            $this->session->set_flashdata('message', lang("price_updated"));
            admin_redirect('products');
        } else {

            $this->data['userfile'] = array(
                'name' => 'userfile',
                'id' => 'userfile',
                'type' => 'text',
                'value' => $this->form_validation->set_value('userfile')
            );
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'products/update_price', $this->data);
        }
    }

    /* ------------------------------------------------------------------------------- */

    function delete($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->products_model->deleteProduct($id)) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 2,
                'table_name' => 'products',
                'record_id' => $id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' Eliminó el producto con el id '. $id,
            ]);
            $this->syncDeletedProduct($id);
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("product_deleted")));
            }
            $this->session->set_flashdata('message', lang('product_deleted'));
            admin_redirect('welcome');
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("delete_product_error")));
        }
    }

    public function syncDeletedProduct($id)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Product');
                $this->load->integration_model('ProductVariation');
                $this->load->integration_model('ProductVariationCombination');
                $this->load->integration_model('ProductAttribute');
                $this->load->integration_model('ProductAttributeValue');
                $this->load->integration_model('ProductCategory');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);

                    $Category = new ProductCategory();
                    $Category->open($integration);

                    $product = $Product->find(['id_wappsi'=>$id]);
                    if (!empty($product)) {

                        $this->syncDeletedProductCategory($id);
                        $this->syncDeletedAllProductAttribute($id);
                        $this->syncDeletedAllProductAttributeValue($id);
                        $this->sycnDeleteAllProductVariationCombination($id);
                        $this->sycnDeleteAllProductVariation($id);

                        $Product->delete($product->id);
                        $Product->close();
                    }
                }
            }
        }
    }

    private function syncDeletedAllProductAttribute($productId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Product');
                $this->load->integration_model('ProductAttribute');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);

                    $ProductAttribute = new ProductAttribute();
                    $ProductAttribute->open($integration);

                    $product = $Product->find(["id_wappsi"=>$productId]);

                    $ProductAttribute->deleteByProductId($product->id);
                    $ProductAttribute->close();

                    $Product->close();
                }
            }
        }
    }

    private function syncDeletedAllProductAttributeValue($productId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Product');
                $this->load->integration_model('ProductAttributeValue');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);

                    $ProductAttributeValue = new ProductAttributeValue();
                    $ProductAttributeValue->open($integration);

                    $product = $Product->find(["id_wappsi" => $productId]);

                    $ProductAttributeValue->deleteByProductId($product->id);
                    $ProductAttributeValue->close();

                    $Product->close();
                }
            }
        }
    }

    private function sycnDeleteAllProductVariationCombination($productId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Product');
                $this->load->integration_model('ProductVariationCombination');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);

                    $ProductVariationCombination = new ProductVariationCombination();
                    $ProductVariationCombination->open($integration);

                    $product = $Product->find(["id_wappsi" => $productId]);

                    $ProductVariationCombination->deleteByProductId($product->id);
                    $ProductVariationCombination->close();

                    $Product->close();
                }
            }
        }
    }

    private function sycnDeleteAllProductVariation($productId)
    {
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                $this->load->integration_model('Product');
                $this->load->integration_model('ProductVariation');

                foreach ($integrations as $integration) {
                    $Product = new Product();
                    $Product->open($integration);

                    $ProductVariation = new ProductVariation();
                    $ProductVariation->open($integration);

                    $product = $Product->find(["id_wappsi" => $productId]);

                    $ProductVariation->deleteByProductId($product->id);
                    $ProductVariation->close();

                    $Product->close();
                }
            }
        }
    }

    /* ----------------------------------------------------------------------------- */

    function quantity_adjustments($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('adjustments');

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('quantity_adjustments')));
        $meta = array('page_title' => lang('quantity_adjustments'), 'bc' => $bc);
        $this->page_construct('products/quantity_adjustments', $meta, $this->data);
    }

    function getadjustments()
    {
        $this->sma->checkPermissions('adjustments');

        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') . " 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') . " 23:59:00" : NULL;
        $warehouse_id = $this->input->post('warehouse_id');

        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('adjustments')}.id as id,
                        date,
                        origin_reference_no,
                        reference_no,
                        IF(adj_type.numtype > 1, '".lang('mixed')."', IF(adj_type.type = 'addition', '".lang('entry')."', '".lang('output')."')) AS type,
                        warehouses.name as wh_name,
                        CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by,
                        {$this->db->dbprefix('companies')}.name as name,
                        {$this->db->dbprefix('adjustments')}.note as note,
                        attachment
                    ")
            ->from('adjustments')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->join('companies', 'companies.id = adjustments.companies_id', 'left')
            ->join("((SELECT adjustment_id, COUNT(DISTINCT type) as numtype, type
                    FROM
                        {$this->db->dbprefix('adjustment_items')}
                    GROUP BY adjustment_id)) AS adj_type", 'adj_type.adjustment_id = adjustments.id')
            ->group_by("adjustments.id");
        // $this->datatables->where(' ('.$this->db->dbprefix('adjustments').'.type_adjustment = 2 OR '.$this->db->dbprefix('adjustments').'.type_adjustment = 0) ');
        if ($warehouse_id) {
            $this->datatables->where('adjustments.warehouse_id', $warehouse_id);
        }
        if ($start_date) {
            $this->datatables->where('adjustments.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('adjustments.date <=', $end_date);
        }
        // <li>
        //                                                         <a href='#' class='tip po' data-content=\"<p>"
        //     . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_adjustment/$1') . "'>"
        //     . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i>
        //                                                         " . $this->lang->line("delete_adjustment") . "
        //                                                         </a>
        //                                                     </li>
        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                    <div class=\"btn-group text-left\">
                                                        <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                            Acciones
                                                            <span class=\"caret\"></span>
                                                        </button>
                                                        <ul class=\"dropdown-menu pull-right\" role=\"menu\">

                                                            <li>
                                                                <a class='reaccount_adjustment_link' data-adjustmentid='$1'><i class='fas fa-sync-alt'></i> " . lang('post_adjustment') . " </a>
                                                            </li>
                                                            <li>
                                                                <a  href='admin/products/print_barcodes/?adjustment=$1'><i class='fa fa-print'></i> " . lang('print_barcodes') . " </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>", "id");
        // $this->datatables->add_column("Actions", "<div class='text-center'></div>", "id");
        // <li>
        //     <a href='" . admin_url('products/edit_adjustment/$1') . "' class='tip'><i class='fa fa-edit'></i>
        //     " . lang("edit_adjustment") . "
        //     </a>
        // </li>

        echo $this->datatables->generate();
    }

    public function view_adjustment($id)
    {
        if (!($this->Admin || $this->Owner || $this->GP['products-adjustments'] || $this->GP['reports-adjustments'])) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
        }
        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $adjustment;
        $this->data['rows'] = $this->products_model->getAdjustmentItems($id);
        // $this->sma->print_arrays($this->data['rows']);
        $this->data['created_by'] = $this->site->getUser($adjustment->created_by);
        $this->data['updated_by'] = $this->site->getUser($adjustment->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment->warehouse_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $adjustment->biller_id);
        $this->data['employee'] = $this->site->getCompanyByID($adjustment->companies_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($adjustment->document_type_id);
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($adjustment->cost_center_id);
        }
        $this->load_view($this->theme . 'products/view_adjustment', $this->data);
    }

    function add_adjustment($count_id = NULL)
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('companies_id', lang("third"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }

            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                admin_redirect('purchases/add');
            }

            $note = $this->sma->clear_tags($this->input->post('note'));

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $type = $_POST['type'][$r];
                $quantity = $_POST['quantity'][$r];
                $serial = isset($_POST['serial'][$r]) && !empty($_POST['serial'][$r]) ? $_POST['serial'][$r] : NULL;
                $adjustment_cost = isset($_POST['total'][$r]) ? $_POST['total'][$r] : 0;
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : NULL;
                $serial_subtraction_option_id = isset($_POST['serial_subtraction_option_id'][$r]) && !empty($_POST['serial_subtraction_option_id'][$r]) ? $_POST['serial_subtraction_option_id'][$r] : NULL;
                $cost = isset($_POST['cost'][$r]) && !empty($_POST['cost'][$r]) ? $_POST['cost'][$r] : 0;
                $net_cost = isset($_POST['net_cost'][$r]) && !empty($_POST['net_cost'][$r]) ? $_POST['net_cost'][$r] : 0;
                $item_tax = isset($_POST['item_tax'][$r]) && !empty($_POST['item_tax'][$r]) ? $_POST['item_tax'][$r] : 0;
                $serialModal_serial = isset($_POST['serialModal_serial'][$r]) && !empty($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : NULL;

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $serial_subtraction_option_id ? $serial_subtraction_option_id : $variant,
                    'serial_no' => $serialModal_serial ? $serialModal_serial : $serial,
                    'avg_cost' => $cost,
                    'adjustment_cost' => $cost,
                    'net_adjustment_cost' => $net_cost,
                    'item_tax' => $item_tax,
                );

                $productsNames[$product_id] = $_POST['product_name'][$r];
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            $data = array(
                'date' => $this->site->movement_setted_datetime($date),
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'count_id' => $this->input->post('count_id') ? $this->input->post('count_id') : NULL,
                'companies_id' => $this->input->post('companies_id'),
                'biller_id' => $biller_id,
                'document_type_id' => $document_type_id,
            );

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            // $this->sma->print_arrays($data, $products);
            // exit(var_dump($data)."</br>".var_dump($products));
        }
        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data, $products, $productsNames)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {
            if ($count_id) {
                $stock_count = $this->products_model->getStouckCountByID($count_id);
                $items = $this->products_model->getStockCountItems($count_id);
                foreach ($items as $item) {
                    $c = sha1(uniqid(mt_rand(), true));
                    if ($item->counted != $item->expected) {
                        $product = $this->site->getProductByID($item->product_id);
                        $row = json_decode('{}');
                        $row->id = $item->product_id;
                        $row->code = $product->code;
                        $row->name = $product->name;
                        $row->price = $product->cost;
                        $row->cost = $product->cost;
                        $row->qty = $item->counted - $item->expected;
                        $row->type = $row->qty > 0 ? 'addition' : 'subtraction';
                        $row->qty = $row->qty > 0 ? $row->qty : (0 - $row->qty);
                        $options = $this->products_model->getProductOptions($product->id);
                        $row->option = $item->product_variant_id ? $item->product_variant_id : 0;
                        $row->serial = '';
                        $ri = $this->Settings->item_addition ? $product->id : $c;

                        $pr[$ri] = array(
                            'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                            'row' => $row, 'options' => $options
                        );
                        $c++;
                    }
                }
            }

            $close_year_adjustment = $this->input->get('close_year_adjustment');
            if ($close_year_adjustment) {
                $this->data['close_year_adjustment'] = true;
                $cy_warehouse_id = $this->input->get('cy_warehouse_id');
                $max_qpr = $this->input->get('max_qpr');
                $adj_type = $this->input->get('adj_type');
                $pw = $this->settings_model->get_product_negative_quantity($cy_warehouse_id, $max_qpr);
                if ($adj_type == 1) {
                    if ($pw['warehouse_products']) {
                        foreach ($pw['warehouse_products'][$cy_warehouse_id] as $wh_products) {
                            $c = sha1(uniqid(mt_rand(), true));
                            $product = $this->site->getProductByID($wh_products->product_id);
                            $row = json_decode('{}');
                            $row->id = $wh_products->product_id;
                            $row->code = $product->code;
                            $row->name = $product->name;
                            $row->avg_cost = $product->avg_cost;
                            $row->cost = $product->cost;
                            $row->qty = $wh_products->quantity * -1;
                            $row->type = 'addition';
                            $options = $this->products_model->getProductOptions($product->id);
                            $row->option = NULL;
                            $row->serial = '';
                            $ri = $this->Settings->item_addition ? $product->id : $c;
                            $pr[$ri] = array(
                                'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                                'row' => $row, 'options' => $options
                            );
                            $c++;
                        }
                    }
                } else if ($adj_type == 2) {
                    if ($pw['warehouses_products_variants']) {
                        // $this->sma->print_arrays($pw['warehouse_products']);
                        foreach ($pw['warehouses_products_variants'][$cy_warehouse_id] as $wh_products) {
                            $c = sha1(uniqid(mt_rand(), true));
                            $product = $this->site->getProductByID($wh_products->product_id);
                            $row = json_decode('{}');
                            $row->id = $wh_products->product_id;
                            $row->code = $product->code;
                            $row->name = $product->name;
                            $row->avg_cost = $product->avg_cost;
                            $row->cost = $product->cost;
                            $row->qty = $wh_products->quantity * -1;
                            $row->type = 'addition';
                            $options = $this->products_model->getProductOptions($product->id);
                            $row->option = $wh_products->option_id;
                            if ($this->Settings->product_variant_per_serial == 1) {
                                $pv = $this->site->get_product_variant_by_id($wh_products->option_id);
                                $row->serialModal_serial = $pv->name;
                            }
                            $row->serial = '';
                            $ri = $this->Settings->item_addition ? $product->id : $c;

                            $pr[$ri] = array(
                                'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                                'row' => $row, 'options' => $options
                            );
                            $c++;
                        }
                    }
                }

                $this->data['cy_warehouse_id'] = $cy_warehouse_id;
                $this->data['max_qpr'] = $max_qpr;
            }

            $this->data['companies'] = $this->site->getAllCompanies($this->Settings->companies_for_adjustments_transfers);
            $this->data['adjustment_items'] = $count_id || $close_year_adjustment ? json_encode($pr) : FALSE;
            $this->data['warehouse_id'] = $count_id ? $stock_count->warehouse_id : FALSE;
            $this->data['count_id'] = $count_id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();

            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_adjustment')));
            $meta = array('page_title' => lang('add_adjustment'), 'bc' => $bc);
            $this->page_construct('products/add_adjustment', $meta, $this->data);
        }
    }

    function edit_adjustment($id)
    {
        $this->sma->checkPermissions('adjustments', true);
        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = $adjustment->date;
            }

            $reference_no = $this->input->post('reference_no');
            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $type = $_POST['type'][$r];
                $quantity = $_POST['quantity'][$r];
                $serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : NULL;
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : null;

                if (!$this->Settings->overselling && $type == 'subtraction') {
                    if ($variant) {
                        if ($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                            if ($op_wh_qty->quantity < $quantity) {
                                $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    if ($wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id)) {
                        if ($wh_qty['quantity'] < $quantity) {
                            $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $variant,
                    'serial_no' => $serial,
                );
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id')
            );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);

        }

        if ($this->form_validation->run() == true && $this->products_model->updateAdjustment($id, $data, $products)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {

            $inv_items = $this->products_model->getAdjustmentItems($id);
            // krsort($inv_items);
            foreach ($inv_items as $item) {
                $c = sha1(uniqid(mt_rand(), true));
                $product = $this->site->getProductByID($item->product_id);
                $row = json_decode('{}');
                $row->id = $item->product_id;
                $row->code = $product->code;
                $row->name = $product->name;
                $row->qty = $item->quantity;
                $row->type = $item->type;
                $options = $this->products_model->getProductOptions($product->id);
                $row->option = $item->option_id ? $item->option_id : 0;
                $row->serial = $item->serial_no ? $item->serial_no : '';
                $ri = $this->Settings->item_addition ? $product->id : $c;

                $pr[$ri] = array(
                    'id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'options' => $options
                );
                $c++;
            }

            $this->data['adjustment'] = $adjustment;
            $this->data['adjustment_items'] = json_encode($pr);
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_adjustment')));
            $meta = array('page_title' => lang('edit_adjustment'), 'bc' => $bc);
            $this->page_construct('products/edit_adjustment', $meta, $this->data);
        }
    }

    function add_adjustment_by_csv()
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->form_validation->set_rules('warehouse_hidden', lang("warehouse"), 'required');
        $this->form_validation->set_rules('companies_id', lang("companies"), 'required');
        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $warehouse_id = $this->input->post('warehouse_hidden');
            $companies_id = $this->input->post('companies_id');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $type_adjustment = $this->input->post('type_id');
            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'biller_id' => $this->input->post('biller'),
                'document_type_id' => $this->input->post('document_type_id'),
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'companies_id' => $companies_id,
                'count_id' => NULL,
            );
            $archivo = $_FILES['xls_file']['tmp_name'];
            $this->load->library('excel');
            $invalids_products = false;
            $objPHPExcel = PHPExcel_IOFactory::load($archivo);
            $hojas = $objPHPExcel->getWorksheetIterator();
            $txt_errors = "";
            $products = [];
            foreach ($hojas as $hoja) {
                $dataPage = $hoja->toArray();
                unset($dataPage[0]);
                $lineNo = 2;
                foreach ($dataPage as $data_row) {
                    if (empty($data_row[0])) {
                        continue;
                    }
                    if ($product = $this->products_model->getProductByCode(trim($data_row[0]))) {
                        $csv_variant = trim($data_row[2]);
                        $variant = !empty($csv_variant) && ucwords(mb_strtolower($csv_variant)) != "N/A" ? $this->products_model->getProductVariantID($product->id, $csv_variant) : FALSE;
                        $csv_quantity = trim($data_row[1]);
                        $type = $csv_quantity > 0 ? 'addition' : 'subtraction';
                        $quantity = $csv_quantity > 0 ? $csv_quantity : (0 - $csv_quantity);
                        if ($type_adjustment == '2'){
                            $dateProductSync =  new DateTime($product->last_sincronized_qty_date);
                            $dateToday = new DateTime();
                            $diffDays = $dateToday->diff($dateProductSync)->days;
                            if ($diffDays > 2) {
                                $this->session->set_flashdata('error', sprintf(lang('product_with_synchronization_greater_than_two_days'), $product->code));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }
                        if (!$this->Settings->overselling && $type == 'subtraction') {
                            if ($variant) {
                                if ($type_adjustment != '2') {
                                    if ($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                                        if ($op_wh_qty->quantity < $quantity) {
                                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage') . ' - ' . lang('line_no') . ' ' . $lineNo);
                                            redirect($_SERVER["HTTP_REFERER"]);
                                        }
                                    } else {
                                        $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage') . ' - ' . lang('line_no') . ' ' . $lineNo);
                                        redirect($_SERVER["HTTP_REFERER"]);
                                    }
                                }
                            }
                            if ($type_adjustment != '2') {
                                if ($wh_qty = $this->products_model->getProductQuantity($product->id, $warehouse_id)) {
                                    if ($this->sma->formatDecimals($wh_qty['quantity']) < $this->sma->formatDecimals($quantity)) {
                                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage') . ' -  (En bodega : ' . ($this->sma->formatDecimals($wh_qty['quantity'])." < A ajustar : ".$this->sma->formatDecimals($quantity).") ") . lang('line_no') . ' ' . $lineNo);
                                        redirect($_SERVER["HTTP_REFERER"]);
                                    }
                                } else {
                                    $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage') . lang('line_no') . ' ' . $lineNo);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            }
                        }
                        $csv_cost = $data_row[3];
                        $AVGcost = $this->products_model->getProductDataByWarehouse(['product_id' => $product->id, 'warehouse_id' => $warehouse_id]);

                        $cost_def = $csv_cost > 0 ? $csv_cost : ($AVGcost != false && $AVGcost->avg_cost != 0 ? $AVGcost->avg_cost : $product->cost);
                        $net_cost = 0;
                        if ($this->Settings->tax_method == 1) { // <- antes de impuestos
                            $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                            $ctax = $this->site->calculateTax($product, $tax_rate, $cost_def);
                            $net_cost = $cost_def - $ctax['amount'];
                        }else{ // <- impuesto incluido
                            $net_cost = $cost_def;
                        }

                        $products[] = array(
                            'product_id' => $product->id,
                            'type' => $type,
                            'quantity' => $quantity,
                            'warehouse_id' => $warehouse_id,
                            'option_id' => $variant,
                            'adjustment_cost' => $csv_cost > 0 ? $csv_cost : ($AVGcost != false && $AVGcost->avg_cost != 0 ? $AVGcost->avg_cost : $product->cost),
                            'net_adjustment_cost' => $net_cost,
                            'avg_cost' => $csv_cost > 0 ? $csv_cost : ($AVGcost != false && $AVGcost->avg_cost != 0 ? $AVGcost->avg_cost : $product->cost),
                        );
                        $productsNames[$product->id] = $product->name;
                    } else {
                        $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $data_row[0] . '). ' . lang('product_code_x_exist') . ' ' . lang('line_no') . ' ' . $lineNo);
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    $lineNo++;
                }
            }

            // $this->sma->print_arrays($data, $products);
        }
        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data, $products, $productsNames)) {
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 3,
                'table_name' => 'adjustments',
                'record_id' => null,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name.' creo ajuste de cantidades con importación de documento. ',
            ]);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/quantity_adjustments');
        } else {

            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['companies'] = $this->site->getAllCompanies('seller');
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['brands'] = $this->site->getAllBrands();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_adjustment')));
            $meta = array('page_title' => lang('add_adjustment_by_csv'), 'bc' => $bc);
            $this->page_construct('products/add_adjustment_by_csv', $meta, $this->data);
        }
    }

    function delete_adjustment($id = NULL)
    {
        $this->sma->checkPermissions('delete', TRUE);

        if ($this->products_model->deleteAdjustment($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("adjustment_deleted")));
        }
    }

    /* --------------------------------------------------------------------------------------------- */

    //Wappsi code

    // Conteo secuencial - Secuencial Count sc_
    public function sc_suggestions()
    {
        $term = $this->input->get('term', true);
        $categories = $this->input->get('categories') ? $this->input->get('categories') : NULL;
        $brands = $this->input->get('brands') ? $this->input->get('brands') : NULL;
        $nombre = $categories[0];
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $rows = $this->products_model->getSCSuggestions($sr, 1, $categories, $brands);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                $options = $this->products_model->getProductOptions($row->id);
                $row->option = $option_id;
                $row->serial = '';
                $pr[] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $nombre . " (" . $row->code . ")", 'row' => $row, 'options' => $options);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function view_sequential_count($id)
    {
        $this->sma->checkPermissions('adjustments', TRUE);

        $adjustment = $this->products_model->getSecuencialCountByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', 'Conteo secuencial no encontrado');
            //$this->session->set_flashdata('error', lang('Hola parcero'));
            //$this->session->set_flashdata('error',$id);
            $this->sma->md();
        }
        $this->data['inv'] = $adjustment;
        $this->data['rows'] = $this->products_model->getSecuencialCountItems($id);
        $this->data['nrows'] = $this->products_model->getSecuencialNoCountItems($id, $adjustment->categories, $adjustment->brands);
        //$this->sma->print_arrays($this->data['nrows']);
        $this->data['created_by'] = $this->site->getUser($adjustment->created_by);
        $this->data['updated_by'] = $this->site->getUser($adjustment->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment->warehouse_id);
        $this->load_view($this->theme . 'products/view_sequential_count', $this->data);
    }

    function sequentialCount($count_id = NULL)
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('type', lang("type"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }

            $warehouse_id = $this->input->post('warehouse');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $typeSC = $this->input->post('type');

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;

            // exit(var_dump($_POST));

            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                // $type = $_POST['type'][$r];
                $type = $_POST['type'];
                $quantity = $_POST['quantity'][$r];
                $serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : NULL;
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : NULL;

                if (!$this->Settings->overselling && $type == 'subtraction') {
                    if ($variant) {
                        if ($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                            if ($op_wh_qty->quantity < $quantity) {
                                $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    if ($wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id)) {
                        if ($wh_qty['quantity'] < $quantity) {
                            $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $variant,
                    'serial_no' => $serial,
                );
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            // Del metodo count_stock
            $categories = $this->input->post('category') ? $this->input->post('category') : NULL;
            $brands = $this->input->post('brand') ? $this->input->post('brand') : NULL;
            $category_ids = '';
            $brand_ids = '';
            $category_names = '';
            $brand_names = '';
            if ($categories) {
                $r = 1;
                $s = sizeof($categories);
                foreach ($categories as $category_id) {
                    $category = $this->site->getCategoryByID($category_id);
                    if ($r == $s) {
                        $category_names .= $category->name;
                        $category_ids .= $category->id;
                    } else {
                        $category_names .= $category->name . ', ';
                        $category_ids .= $category->id . ', ';
                    }
                    $r++;
                }
            }
            if ($brands) {
                $r = 1;
                $s = sizeof($brands);
                foreach ($brands as $brand_id) {
                    $brand = $this->site->getBrandByID($brand_id);
                    if ($r == $s) {
                        $brand_names .= $brand->name;
                        $brand_ids .= $brand->id;
                    } else {
                        $brand_names .= $brand->name . ', ';
                        $brand_ids .= $brand->id . ', ';
                    }
                    $r++;
                }
            }
            // Termina del modulo de count_stock

            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'type' => $typeSC,
                'categories' => $category_ids,
                'category_names' => $category_names,
                'brands' => $brand_ids,
                'brand_names' => $brand_names,
                'created_by' => $this->session->userdata('user_id'),
                'biller_id' => $this->input->post('biller'),
                'document_type_id' => $this->input->post('document_type_id'),
                'company_id' => $this->input->post('company_id'),
            );
        }

        if ($this->form_validation->run() == true && $this->products_model->addSequentialCount($data, $products)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', lang("secuencial_count"));
            admin_redirect('products/sequentialCount');
        } else {
            if ($count_id) {
                $stock_count = $this->products_model->getStouckCountByID($count_id);
                $items = $this->products_model->getStockCountItems($count_id);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    if ($item->counted != $item->expected) {
                        $product = $this->site->getProductByID($item->product_id);
                        $row = json_decode('{}');
                        $row->id = $item->product_id;
                        $row->code = $product->code;
                        $row->name = $product->name;
                        $row->qty = $item->counted - $item->expected;
                        $row->type = $row->qty > 0 ? 'addition' : 'subtraction';
                        $row->qty = $row->qty > 0 ? $row->qty : (0 - $row->qty);
                        $options = $this->products_model->getProductOptions($product->id);
                        $row->option = $item->product_variant_id ? $item->product_variant_id : 0;
                        $row->serial = '';
                        $ri = $this->Settings->item_addition ? $product->id : $c;

                        $pr[$ri] = array(
                            'id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                            'row' => $row, 'options' => $options
                        );
                        $c++;
                    }
                }
            }
            $this->data['companies'] = $this->companies_model->getAllCompanies();
            $this->data['adjustment_items'] = $count_id ? json_encode($pr) : FALSE;
            $this->data['warehouse_id'] = $count_id ? $stock_count->warehouse_id : FALSE;
            $this->data['count_id'] = $count_id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            // Del metodo count_stock
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            // Termina del metodo count_stock
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => 'Conteo Secuencial'));
            $meta = array('page_title' => 'Conteo Secuencial', 'bc' => $bc);
            //  $this->page_construct('products/add_adjustment', $meta, $this->data);
            //$this->page_construct('products/add_adjustment', $meta, $this->data);
            $this->page_construct('products/sequential_count', $meta, $this->data);
        }
    }

    function edit_sequentialCount($id)
    {
        //$this->sma->print_arrays($id);
        $this->sma->checkPermissions('adjustments', true);
        $adjustment = $this->products_model->getSecuencialCountByID($id);
        // Función para imprimir Arrays muy util
        //$this->sma->print_arrays($adjustment);

        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', 'No se encontro el conteo secuencial: ' + $id);
            $this->sma->md();
        }
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = $adjustment->date;
            }
            $reference_no = $this->input->post('reference_no');
            $warehouse_id = $this->input->post('warehouse');
            $typeSC = $this->input->post('type');
            $note = $this->sma->clear_tags($this->input->post('note'));

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            // exit(var_dump($_POST));
            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $type = $_POST['type'];
                $quantity = $_POST['quantity'][$r];
                $serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : null;
                $variant = isset($_POST['variant'][$r]) && !empty($_POST['variant'][$r]) ? $_POST['variant'][$r] : null;

                if (!$this->Settings->overselling && $type == 'subtraction') {
                    if ($variant) {
                        if ($op_wh_qty = $this->products_model->getProductWarehouseOptionQty($variant, $warehouse_id)) {
                            if ($op_wh_qty->quantity < $quantity) {
                                $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        } else {
                            $this->session->set_flashdata('error', lang('warehouse_option_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    }
                    if ($wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id)) {
                        if ($wh_qty['quantity'] < $quantity) {
                            $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('warehouse_qty_is_less_than_damage'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => $variant,
                    'serial_no' => $serial,
                );
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }

            // Del metodo count_stock
            $categories = $this->input->post('category') ? $this->input->post('category') : NULL;
            $brands = $this->input->post('brand') ? $this->input->post('brand') : NULL;
            $category_ids = '';
            $brand_ids = '';
            $category_names = '';
            $brand_names = '';
            if ($categories) {
                $r = 1;
                $s = sizeof($categories);
                foreach ($categories as $category_id) {
                    $category = $this->site->getCategoryByID($category_id);
                    if ($r == $s) {
                        $category_names .= $category->name;
                        $category_ids .= $category->id;
                    } else {
                        $category_names .= $category->name . ', ';
                        $category_ids .= $category->id . ', ';
                    }
                    $r++;
                }
            }
            if ($brands) {
                $r = 1;
                $s = sizeof($brands);
                foreach ($brands as $brand_id) {
                    $brand = $this->site->getBrandByID($brand_id);
                    if ($r == $s) {
                        $brand_names .= $brand->name;
                        $brand_ids .= $brand->id;
                    } else {
                        $brand_names .= $brand->name . ', ';
                        $brand_ids .= $brand->id . ', ';
                    }
                    $r++;
                }
            }
            // Termina del modulo de count_stock

            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'type' => $typeSC,
                'categories' => $category_ids,
                'category_names' => $category_names,
                'brands' => $brand_ids,
                'brand_names' => $brand_names,
                'created_by' => $this->session->userdata('user_id'),
                'company_id' => $this->input->post('company_id'),
            );

            if (isset($_FILES['document']) && $_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products);

        }
        //$this->sma->print_arrays($products);
        if ($this->form_validation->run() == true && $this->products_model->updateEditSequentialCount($id, $data, $products)) {
            $this->session->set_userdata('remove_qals', 1);
            $this->session->set_flashdata('message', 'Se ha actualizado el conteo secuencial.');
            admin_redirect('products/sequential_counts');
        } else {

            $inv_items = $this->products_model->getSecuencialCountItems($id);
            // Función para imprimir Arrays muy util
            //$this->sma->print_arrays($inv_items);
            krsort($inv_items);
            // Del metodo count_stock
            $categories = $this->input->post('category') ? $this->input->post('category') : NULL;
            $brands = $this->input->post('brand') ? $this->input->post('brand') : NULL;
            $category_ids = '';
            $brand_ids = '';
            $category_names = '';
            $brand_names = '';
            if ($categories) {
                $r = 1;
                $s = sizeof($categories);
                foreach ($categories as $category_id) {
                    $category = $this->site->getCategoryByID($category_id);
                    if ($r == $s) {
                        $category_names .= $category->name;
                        $category_ids .= $category->id;
                    } else {
                        $category_names .= $category->name . ', ';
                        $category_ids .= $category->id . ', ';
                    }
                    $r++;
                }
            }
            if ($brands) {
                $r = 1;
                $s = sizeof($brands);
                foreach ($brands as $brand_id) {
                    $brand = $this->site->getBrandByID($brand_id);
                    if ($r == $s) {
                        $brand_names .= $brand->name;
                        $brand_ids .= $brand->id;
                    } else {
                        $brand_names .= $brand->name . ', ';
                        $brand_ids .= $brand->id . ', ';
                    }
                    $r++;
                }
            }
            // Termina del modulo de count_stock
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $product = $this->site->getProductByID($item->product_id);
                // Función para imprimir Arrays muy util
                //$this->sma->print_arrays($product);
                $row = json_decode('{}');
                $row->id = $item->product_id;
                $row->code = $product->code;
                $row->name = $product->name;
                $row->qty = $item->quantity;
                $row->type = $item->type;
                $options = $this->products_model->getProductOptions($product->id);
                $row->option = $item->option_id ? $item->option_id : 0;
                $row->serial = $item->serial_no ? $item->serial_no : '';
                $ri = $this->Settings->item_addition ? $product->id : $c;

                //$pr[$ri] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'options' => $options);
                //$pr[$ri] = array('id' => str_replace(".", "", microtime(true)), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row);
                $pr[$row->id] = array('id' => $row->id, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row);




                $c++;
            }
            // Función para imprimir Arrays muy util
            //$this->sma->print_arrays($pr);

            $this->data['companies'] = $this->companies_model->getAllCompanies();
            $this->data['adjustment'] = $adjustment;
            $this->data['adjustment_items'] = json_encode($pr);
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            // Del metodo count_stock
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['brands'] = $this->site->getAllBrands();
            // Termina del metodo count_stock
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => 'Editar conteo secuencial'));
            $meta = array('page_title' => 'Editar conteo secuencial', 'bc' => $bc);
            $this->page_construct('products/edit_sequential_count', $meta, $this->data);
        }
    }

    function sequential_counts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => 'Conteos Secuenciales'));
        $meta = array('page_title' => 'Conteos Secuenciales', 'bc' => $bc);
        $this->page_construct('products/sequential_counts', $meta, $this->data);
    }

    function apply_sequential_count($id = NULL)
    {
        $mensaje = "";
        $this->sma->checkPermissions('adjustments', TRUE);
        $sequentialCount = $this->products_model->getSecuencialCountByID($id);
        if (!$id || !$sequentialCount) {
            $mensaje .= '<br>Conteo secuencial no encontrado';
        }
        $this->data['inv'] = $sequentialCount;
        $rows = $this->products_model->getSecuencialCountItems($id);
        $this->data['rows'] = $rows;
        $this->data['updated_by'] = $this->site->getUser($sequentialCount->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($sequentialCount->warehouse_id);

        // En construcción
        // 1. Crear registro en la tabla de adjustments
        // 2. Crear registros en la tabla de adjustments_items
        // LOS SIGUIENTE S 2 PUNTOS SE ACTUALIZAN AL HACER LOS REGISTROS DE LA TABLA DE AJUSTES
        // 3. Actualizar la cantidad en warehouses_products
        // 4. Actualizar la cantidad en products El campo quantity maneja la suma de las cantidades en las bodegas
        // 5. Marcar como aplicado sequential_count

        $adjustment = array(
            'date' => $sequentialCount->date,
            'reference_no' => $sequentialCount->reference_no,
            'warehouse_id' => $sequentialCount->warehouse_id,
            'note' => $sequentialCount->note,
            'created_by' => $sequentialCount->created_by,
            'biller_id' => $sequentialCount->biller_id,
            'document_type_id' => $sequentialCount->document_type_id,
            'companies_id' => $sequentialCount->company_id,
        );
        //   $this->sma->print_arrays($rows);
        foreach ($rows as $row) {
            $diference = $row->quantity - $row->warehouse_quantity;
            if ($diference < 0) {
                $typeAjustment = 'subtraction';
                $diference = $diference * -1;
            } else {
                $typeAjustment = 'addition';
            }
            $adjustmentItems[] = array(
                'product_id' => $row->product_id,
                'quantity' => $diference,
                'warehouse_id' => $row->warehouse_id,
                'type' => $typeAjustment,
                'option_id' => "",
                'serial_no' => "",
                'avg_cost' => $row->cost
            );
        }
        // Función para imprimir Arrays muy util
        if ($this->products_model->addAdjustment($adjustment, $adjustmentItems, [], false)) {
            if ($this->products_model->updateSequentialCount($id)) {
                //$mensaje .= '<br>Se realizó la actualización del conteo sequencial';
            } else {
                $mensaje .= '<br>No se pudo realizar la actualización del conteo sequencial';
            }
        } else {
            $mensaje .= '<br>No se pudo realizar la inserción del ajuste';
        }
        //Detalles de la vista
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('stock_counts')));
        $meta = array('page_title' => lang('sequential_counts'), 'bc' => $bc);
        $this->page_construct('products/sequential_counts', $meta, $this->data);
        //Mensaje en la pagina
        if ($mensaje != '') {
            $this->session->set_flashdata('danger', $mensaje);
        } else {
            $this->session->set_flashdata('message', 'Se ha aplicado con exito el conteo sequencial');
        }
        admin_redirect('products/sequential_counts');
    }

    function delete_sequential_count($id = NULL)
    {
        $mensaje = "";
        $this->sma->checkPermissions('delete', TRUE);

        if ($this->products_model->deleteSequentialCount($id)) {
        } else {
            $mensaje = "No se podido eliminar el conteo secuencial";
        }






        if ($mensaje != '') {
            $this->session->set_flashdata('danger', $mensaje);
        } else {
            //$this->session->set_flashdata('message', 'Se ha eliminado exito el conteo sequencial '.$id);
            $this->session->set_flashdata('message', 'Se ha eliminado exito el conteo sequencial');
        }
        admin_redirect('products/sequential_counts');
    }

    function getSequentialCounts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count', TRUE);

        //Boton de Borrado
        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("Eliminar Conteo Secuencial") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete1' id='a__$1' href='" . admin_url('products/update_sequential_count/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> " . lang('Eliminar Conteo Secuencial') . "</a>";

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }

        $action = '<div class="text-center"><div class="btn-group text-left">' . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle class="aplicado_$2" data-toggle="dropdown">' . lang('actions') . ' <span class="caret"></span></button> <ul class="dropdown-menu pull-right" role="menu">';

        $action .= ' <li><a class="aplicadoEditar_$2"   href="' . admin_url('products/edit_sequentialCount/$1') . '"><i class="fa fa-edit"></i> ' . lang('Editar Conteo Secuencial') . '</a></li>';


        $action .= ' <li><a class="aplicadoEliminar_$2 confirm" href="' . admin_url('products/delete_sequential_count/$1') . '"><i class="fa fa-trash"></i> ' . lang('Eliminar Conteo Secuencial') . '</a></li>';

        //$action .= '<li>' . $single_barcode . '</li> <li class="divider"></li> <li>' . $delete_link . '</li> </ul> </div></div>';

        if ($warehouse_id) {
            $action .= '<li><a href="' . admin_url('products/set_rack/$1/' . $warehouse_id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-bars"></i> ' . lang('set_rack') . '</a></li>';
        }

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('sequential_count')}.id as id, date, reference_no, {$this->db->dbprefix('warehouses')}.name as wh_name, sequential_count.type, brand_names, category_names, apply")
            ->from('sequential_count')
            ->join('warehouses', 'warehouses.id=sequential_count.warehouse_id', 'left');
        if ($warehouse_id) {
            $this->datatables->where('warehouse_id', $warehouse_id);
        }
        $this->datatables->add_column("Actions", $action, "id, apply");
        echo $this->datatables->generate();
    }

    // Wappsi termina code

    /* --------------------------------------------------------------------------------------------- */

    function modal_view($id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);

        $pr_details = $this->site->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            $this->sma->md();
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo' || $pr_details->type == 'pfinished' || $pr_details->type == 'subproduct') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['tax_rate_2'] = $this->site->getTaxRate2ByID($pr_details->purchase_tax_rate_2_id ? $pr_details->purchase_tax_rate_2_id : $pr_details->sale_tax_rate_2_id);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['second_level_subcategory'] = $pr_details->second_level_subcategory_id ? $this->site->getCategoryByID($pr_details->second_level_subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);
        $this->data['preferences'] = $this->products_model->getProductPreferences($id);
        $this->data['product_price_groups'] = $this->products_model->getProductPriceGroups($id);
        $this->data['price_groups'] = $this->site->getAllPriceGroups();
        $this->data['units'] = $this->site->get_all_units();
        $this->data['hybrid_prices'] = $this->site->get_all_product_hybrid_prices($id);
        $this->data["custom_fields"] = $this->site->get_custom_fields(2);
        $billers_selected = $this->products_model->get_products_billers($id);
        $arr_billers = [];
        if ($billers_selected) {
            foreach ($billers_selected as $bs_id => $bs) {
                $arr_billers[] = $this->site->getAllCompaniesWithState('biller', $bs_id);
            }
        }
        $this->data["billers_selected"] = $arr_billers;
        $this->data["user_creation"] = $pr_details->created_by ? $this->site->getUser($pr_details->created_by) : NULL;
        $this->data["user_edition"] = $this->products_model->get_last_user_edition($id);
        if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) {
            $this->data['product_unit_prices'] = $this->products_model->get_product_unit_prices($id);
        }
        $this->load_view($this->theme . 'products/modal_view', $this->data);
    }

    function view($id = NULL)
    {
        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo' || $pr_details->type == 'pfinished' || $pr_details->type == 'subproduct') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['tax_rate_2'] = $this->site->getTaxRate2ByID($pr_details->purchase_tax_rate_2_id ? $pr_details->purchase_tax_rate_2_id : $pr_details->sale_tax_rate_2_id);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['second_level_subcategory'] = $pr_details->second_level_subcategory_id ? $this->site->getCategoryByID($pr_details->second_level_subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);
        $this->data['preferences'] = $this->products_model->getProductPreferences($id);
        $this->data['sold'] = $this->products_model->getSoldQty($id);
        $this->data['purchased'] = $this->products_model->getPurchasedQty($id);
        $this->data['product_price_groups'] = $this->products_model->getProductPriceGroups($id);
        $this->data['price_groups'] = $this->site->getAllPriceGroups();
        $this->data['units'] = $this->site->get_all_units();
        $this->data['hybrid_prices'] = $this->site->get_all_product_hybrid_prices($id);
        $this->data["custom_fields"] = $this->site->get_custom_fields(2);
        $billers_selected = $this->products_model->get_products_billers($id);
        $arr_billers = [];
        if ($billers_selected) {
            foreach ($billers_selected as $bs_id => $bs) {
                $arr_billers[] = $this->site->getAllCompaniesWithState('biller', $bs_id);
            }
        }
        $this->data["billers_selected"] = $arr_billers;

        if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) {
            $this->data['product_unit_prices'] = $this->products_model->get_product_unit_prices($id);
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => $pr_details->name));
        $meta = array('page_title' => $pr_details->name, 'bc' => $bc);
        $this->page_construct('products/view', ['page_title' => $this->lang->line("product_report")], $this->data);
    }

    function pdf($id = NULL, $view = NULL)
    {
        $this->sma->checkPermissions('index');

        $pr_details = $this->products_model->getProductByID($id);
        if (!$id || !$pr_details) {
            $this->session->set_flashdata('error', lang('prduct_not_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $pr_details->code . '/' . $pr_details->barcode_symbology . '/40/0') . "' alt='" . $pr_details->code . "' class='pull-left' />";
        if ($pr_details->type == 'combo') {
            $this->data['combo_items'] = $this->products_model->getProductComboItems($id);
        }
        $this->data['product'] = $pr_details;
        $this->data['unit'] = $this->site->getUnitByID($pr_details->unit);
        $this->data['brand'] = $this->site->getBrandByID($pr_details->brand);
        $this->data['images'] = $this->products_model->getProductPhotos($id);
        $this->data['category'] = $this->site->getCategoryByID($pr_details->category_id);
        $this->data['subcategory'] = $pr_details->subcategory_id ? $this->site->getCategoryByID($pr_details->subcategory_id) : NULL;
        $this->data['tax_rate'] = $pr_details->tax_rate ? $this->site->getTaxRateByID($pr_details->tax_rate) : NULL;
        $this->data['popup_attributes'] = $this->popup_attributes;
        $this->data['warehouses'] = $this->products_model->getAllWarehousesWithPQ($id);
        $this->data['options'] = $this->products_model->getProductOptionsWithWH($id);
        $this->data['variants'] = $this->products_model->getProductOptions($id);

        $name = $pr_details->code . '_' . str_replace('/', '_', $pr_details->name) . ".pdf";
        if ($view) {
            $this->load_view($this->theme . 'products/pdf', $this->data);
        } else {
            $html = $this->load_view($this->theme . 'products/pdf', $this->data, TRUE);
            if (!$this->Settings->barcode_img) {
                $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
            }
            $this->sma->generate_pdf($html, $name);
        }
    }

    function getSubCategories($category_id = NULL)
    {
        if ($rows = $this->products_model->getSubCategories($category_id)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
    }

    function getSubCategoriesSecondLevel($category_id = NULL)
    {
        if ($rows = $this->products_model->getSubCategoriesSecondLevel($category_id)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
    }

    function getMultipleSubCategories($category_id = NULL)
    {
        $categories = explode(",", $category_id);
        if (count($categories) > 0 && $rows = $this->products_model->getMultipleSubCategories($categories)) {
            $data = json_encode($rows);
        } else {
            $data = false;
        }
        echo $data;
    }

    function product_actions($wh = NULL)
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val']) || $this->input->post('form_action') == 'export_all_excel' || $this->input->post('form_action') == 'export_all_excel_variants' || $this->input->post('form_action') == 'update_price_group_base') {

                if ($this->input->post('form_action') == 'sync_quantity') {
                    foreach ($_POST['val'] as $id) {
                        $this->site->syncQuantity(NULL, NULL, NULL, $id, TRUE);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("products_quantity_sync"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    $warning = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->products_model->deleteProduct($id)) {
                            $warning = true;
                        }
                    }
                    if ($warning) {
                        $this->session->set_flashdata('warning', $this->lang->line("delete_products_error"));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("products_deleted"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'labels') {
                    foreach ($_POST['val'] as $id) {
                        $row = $this->products_model->getProductByID($id);
                        $selected_variants = false;
                        if ($variants = $this->products_model->getProductOptions($row->id)) {
                            foreach ($variants as $variant) {
                                $selected_variants[$variant->id] = $variant->quantity > 0 ? 1 : 0;
                            }
                        }
                        $pr[$row->id] = array('id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'code' => $row->code, 'name' => $row->name, 'price' => $row->price, 'qty' => $row->quantity, 'variants' => $variants, 'selected_variants' => $selected_variants);
                    }
                    $this->data['items'] = isset($pr) ? json_encode($pr) : false;
                    $this->data['pdf_print'] = false;
                    $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
                    $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('print_barcodes')));
                    $meta = array('page_title' => lang('print_barcodes'), 'bc' => $bc);
                    $this->page_construct('products/print_barcodes', $meta, $this->data);
                } elseif ($this->input->post('form_action') == 'export_excel') {
                    $wh = $this->input->post('form_action_warehouse_id');

                    $this->load->library('excel');
                    $sheet = $this->excel->setActiveSheetIndex(0);
                    $sheet->setTitle('Products');
                    $sheet->SetCellValue('A1', lang('name'));
                    $sheet->SetCellValue('B1', lang('code'));
                    $sheet->SetCellValue('C1', lang('ref'));
                    $sheet->SetCellValue('D1', lang('barcode_symbology'));
                    $sheet->SetCellValue('E1', lang('brand'));
                    $sheet->SetCellValue('F1', lang('category'));
                    $sheet->SetCellValue('G1', lang('subcategory'));
                    $sheet->SetCellValue('H1', lang('unit_code'));
                    $sheet->SetCellValue('I1', lang('sale') . ' ' . lang('unit_code'));
                    $sheet->SetCellValue('J1', lang('purchase') . ' ' . lang('unit_code'));
                    $sheet->SetCellValue('K1', lang('cost'));
                    $sheet->SetCellValue('L1', lang('price'));
                    $sheet->SetCellValue('M1', lang('alert_quantity'));
                    $sheet->SetCellValue('N1', lang('tax_rate'));
                    $sheet->SetCellValue('O1', lang('tax_method'));
                    $sheet->SetCellValue('P1', lang('image'));
                    $sheet->SetCellValue('Q1', sprintf(lang('subcategory_code'), lang('subcategory')));
                    $sheet->SetCellValue('R1', lang('product_variants'));
                    $sheet->SetCellValue('S1', lang('pcf1'));
                    $sheet->SetCellValue('T1', lang('pcf2'));
                    $sheet->SetCellValue('U1', lang('pcf3'));
                    $sheet->SetCellValue('V1', lang('pcf4'));
                    $sheet->SetCellValue('W1', lang('pcf5'));
                    $sheet->SetCellValue('X1', lang('pcf6'));
                    $sheet->SetCellValue('Y1', lang('quantity'));
                    $sheet->SetCellValue('Z1', lang('status'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $product = $this->products_model->getProductDetail($id);
                        $brand = $this->site->getBrandByID($product->brand);
                        $base_unit = $sale_unit = $purchase_unit = '';
                        if ($units = $this->site->getUnitsByBUID($product->unit)) {
                            foreach ($units as $u) {
                                if ($u->id == $product->unit) {
                                    $base_unit = $u->code;
                                }
                                if ($u->id == $product->sale_unit) {
                                    $sale_unit = $u->code;
                                }
                                if ($u->id == $product->purchase_unit) {
                                    $purchase_unit = $u->code;
                                }
                            }
                        }
                        if ($wh) {
                            $variants = $this->products_model->getWarehouseProductOptions($id, $wh);
                        } else {
                            $variants = $this->products_model->getProductOptions($id);
                        }
                        $product_variants = '';
                        if ($variants) {
                            foreach ($variants as $variant) {
                                $product_variants .= trim($variant->name) . '|';
                            }
                        }
                        $quantity = $product->quantity;
                        if ($wh) {
                            if ($wh_qty = $this->products_model->getProductQuantity($id, $wh)) {
                                $quantity = $wh_qty['quantity'];
                            } else {
                                $quantity = 0;
                            }
                        }
                        $sheet->SetCellValue('A' . $row, $product->name);
                        $sheet->SetCellValue('B' . $row, $product->code);
                        $sheet->SetCellValue('C' . $row, $product->reference);
                        $sheet->SetCellValue('D' . $row, $product->barcode_symbology);
                        $sheet->SetCellValue('E' . $row, ($brand ? $brand->name : ''));
                        $sheet->SetCellValue('F' . $row, $product->category_code);
                        $sheet->SetCellValue('G' . $row, $product->subcategory_code);
                        $sheet->SetCellValue('H' . $row, $base_unit);
                        $sheet->SetCellValue('I' . $row, $sale_unit);
                        $sheet->SetCellValue('J' . $row, $purchase_unit);
                        if ($this->Owner || $this->Admin || $this->GP['products-cost']) {
                            $sheet->SetCellValue('K' . $row, $product->cost);
                        }
                        if ($this->Owner || $this->Admin || $this->GP['products-price']) {
                            $sheet->SetCellValue('L' . $row, $product->price);
                        }
                        $sheet->SetCellValue('M' . $row, $product->alert_quantity);
                        $sheet->SetCellValue('N' . $row, $product->tax_rate_name);
                        $sheet->SetCellValue('O' . $row, $product->tax_method ? lang('exclusive') : lang('inclusive'));
                        $sheet->SetCellValue('P' . $row, $product->image);
                        $sheet->SetCellValue('Q' . $row, $product->subcategory_code);
                        $sheet->SetCellValue('R' . $row, $product_variants);
                        $sheet->SetCellValue('S' . $row, $product->cf1);
                        $sheet->SetCellValue('T' . $row, $product->cf2);
                        $sheet->SetCellValue('U' . $row, $product->cf3);
                        $sheet->SetCellValue('V' . $row, $product->cf4);
                        $sheet->SetCellValue('W' . $row, $product->cf5);
                        $sheet->SetCellValue('X' . $row, $product->cf6);
                        $sheet->SetCellValue('Y' . $row, $quantity);
                        $sheet->SetCellValue('Z' . $row, $product->discontinued == 1 ? lang('inactive') : lang('active'));
                        $row++;
                    }

                    for ($i="A"; $i <= "Z"; $i++) {
                        $sheet->getColumnDimension($i)->setAutoSize(true);
                    }

                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'products_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } elseif ($this->input->post('form_action') == 'export_all_excel_variants') {
                    $wh = $this->input->post('form_action_warehouse_id');
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('Products');
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('code'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('name'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('second_name'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('unit_code'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('variant'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('variant_code'));
                    $this->excel->getActiveSheet()->SetCellValue('H1', lang('variant_quantity'));
                    $this->excel->getActiveSheet()->SetCellValue('I1', lang('color'));
                    $this->excel->getActiveSheet()->SetCellValue('J1', lang('material'));
                    $this->excel->getActiveSheet()->SetCellValue('K1', lang('brand'));
                    $this->excel->getActiveSheet()->SetCellValue('L1', lang('category'));
                    $this->excel->getActiveSheet()->SetCellValue('M1', lang('subcategory'));
                    $this->excel->getActiveSheet()->SetCellValue('N1', lang('second_level_subcategory_id'));
                    $this->excel->getActiveSheet()->SetCellValue('O1', lang('tax_rate'));
                    $letter = "P";
                    if ($this->Owner || $this->Admin || $this->GP['products-cost']) {
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('cost'));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter.'1', lang('avg_cost'));
                        $letter++;
                    }
                    $this->excel->getActiveSheet()->SetCellValue($letter . '1', lang('price'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . '1', lang('promo_price'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . '1', lang('start_date'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . '1', lang('end_date'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . '1', lang('warranty_time'));
                    $letter++;
                    $this->excel->getActiveSheet()->SetCellValue($letter . '1', lang('birthday_discount'));
                    $letter++;
                    $products = $this->site->get_catalogue_products();
                    $row = 2;
                    foreach ($products as $pdata) {
                        $product_price_groups = $this->products_model->getProductPriceGroups($pdata->id);
                        $brand = $this->site->getBrandByID($pdata->brand);
                        $base_unit = $sale_unit = $purchase_unit = '';
                        if ($units = $this->site->getUnitsByBUID($pdata->unit)) {
                            foreach ($units as $u) {
                                if ($u->id == $pdata->unit) {
                                    $base_unit = $u->code;
                                }
                            }
                        }
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $pdata->code);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $pdata->reference);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $pdata->name);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $pdata->second_name);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $base_unit);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $pdata->variant_name);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, $pdata->variant_code);
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $pdata->variant_quantity);
                        $this->excel->getActiveSheet()->SetCellValue('I' . $row, $pdata->color);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $pdata->material);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($brand ? $brand->name : ''));
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $pdata->category_code);
                        $this->excel->getActiveSheet()->SetCellValue('M' . $row, $pdata->subcategory_code);
                        $this->excel->getActiveSheet()->SetCellValue('N' . $row, $pdata->sub_subcategory_code);
                        $this->excel->getActiveSheet()->SetCellValue('O' . $row, $pdata->tax_rate_name);
                        $letter = "P";
                        if ($this->Owner || $this->Admin || $this->GP['products-cost']) {
                            $this->excel->getActiveSheet()->SetCellValue($letter . $row, $pdata->cost);
                            $letter++;
                            $this->excel->getActiveSheet()->SetCellValue($letter . $row, $pdata->avg_cost);
                            $letter++;
                        }
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, ($pdata->price + $pdata->v_price));
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $pdata->promo_price);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $pdata->start_date);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $pdata->end_date);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $pdata->warranty_time);
                        $letter++;
                        $this->excel->getActiveSheet()->SetCellValue($letter . $row, $pdata->birthday_discount);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
                    $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'products_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } elseif ($this->input->post('form_action') == 'export_all_excel') {
                    $wh = $this->input->post('form_action_warehouse_id');
                    $this->load->library('excel');
                    $sheet = $this->excel->setActiveSheetIndex(0);
                    $sheet->setTitle('Products');
                    $sheet->SetCellValue('A1', lang('name'));
                    $sheet->SetCellValue('B1', lang('code'));
                    $sheet->SetCellValue('C1', lang('ref'));
                    $sheet->SetCellValue('D1', lang('barcode_symbology'));
                    $sheet->SetCellValue('E1', lang('brand'));
                    $sheet->SetCellValue('F1', lang('category'));
                    $sheet->SetCellValue('G1', lang('subcategory'));
                    $sheet->SetCellValue('H1', lang('unit_code'));
                    $sheet->SetCellValue('I1', lang('sale') . ' ' . lang('unit_code'));
                    $sheet->SetCellValue('J1', lang('purchase') . ' ' . lang('unit_code'));
                    $sheet->SetCellValue('K1', lang('cost'));
                    $sheet->SetCellValue('L1', lang('avg_cost'));
                    $letter = "M";
                    $price_groups = $this->site->getAllPriceGroups();
                    foreach ($price_groups as $pg_id => $pg_data) {
                        $sheet->SetCellValue($letter . '1', lang('price') . " " . $pg_data->name . ($pg_data->price_group_base == 1 ? ' (Lista base)' : ""));
                        $letter++;
                    }
                    $sheet->SetCellValue($letter . '1', lang('alert_quantity'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('tax_rate'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('tax_method'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('image'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', sprintf(lang('subcategory_code'), lang('subcategory')));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('product_variants'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('pcf1'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('pcf2'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('pcf3'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('pcf4'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('pcf5'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('pcf6'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('quantity'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('product_discontinued'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('product_detail'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('warranty_time'));
                    $letter++;
                    $sheet->SetCellValue($letter . '1', lang('birthday_discount'));
                    $letter++;

                    $products = $this->products_model->getAllProducts();
                    $row = 2;
                    foreach ($products as $pdata) {

                        $product_price_groups = $this->products_model->getProductPriceGroups($pdata->id);
                        $product = $this->products_model->getProductDetail($pdata->id);
                        $brand = $this->site->getBrandByID($product->brand);
                        $base_unit = $sale_unit = $purchase_unit = '';
                        if ($units = $this->site->getUnitsByBUID($product->unit)) {
                            foreach ($units as $u) {
                                if ($u->id == $product->unit) {
                                    $base_unit = $u->code;
                                }
                                if ($u->id == $product->sale_unit) {
                                    $sale_unit = $u->code;
                                }
                                if ($u->id == $product->purchase_unit) {
                                    $purchase_unit = $u->code;
                                }
                            }
                        }
                        if ($wh) {
                            $variants = $this->products_model->getWarehouseProductOptions($product->id, $wh);
                        } else {
                            $variants = $this->products_model->getProductOptions($product->id);
                        }
                        $product_variants = '';
                        if ($variants) {
                            foreach ($variants as $variant) {
                                $product_variants .= trim($variant->name) . '|';
                            }
                        }
                        $quantity = $product->quantity;
                        if ($wh) {
                            if ($wh_qty = $this->products_model->getProductQuantity($product->id, $wh)) {
                                $quantity = $wh_qty['quantity'];
                            } else {
                                $quantity = 0;
                            }
                        }
                        $sheet->SetCellValue('A' . $row, $product->name);
                        $sheet->SetCellValue('B' . $row, $product->code);
                        $sheet->SetCellValue('C' . $row, $product->reference);
                        $sheet->SetCellValue('D' . $row, $product->barcode_symbology);
                        $sheet->SetCellValue('E' . $row, ($brand ? $brand->name : ''));
                        $sheet->SetCellValue('F' . $row, $product->category_code);
                        $sheet->SetCellValue('G' . $row, $product->subcategory_code);
                        $sheet->SetCellValue('H' . $row, $base_unit);
                        $sheet->SetCellValue('I' . $row, $sale_unit);
                        $sheet->SetCellValue('J' . $row, $purchase_unit);
                        if ($this->Owner || $this->Admin || $this->GP['products-cost']) {
                            $sheet->SetCellValue('K' . $row, $product->cost);
                            $sheet->SetCellValue('L' . $row, $product->avg_cost);
                        }
                        // if ($this->Owner || $this->Admin || $this->GP['products-price']) {
                        $letter = "M";
                        foreach ($price_groups as $pg_id => $pg_data) {
                            $sheet->SetCellValue($letter . $row, (isset($product_price_groups[$pg_data->id]) ? $product_price_groups[$pg_data->id]->price : 0));
                            $letter++;
                        }
                        // }
                        $sheet->SetCellValue($letter . $row, $product->alert_quantity);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->tax_rate_name);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->tax_method ? lang('exclusive') : lang('inclusive'));
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->image);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->subcategory_code);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product_variants);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->cf1);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->cf2);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->cf3);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->cf4);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->cf5);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->cf6);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $quantity);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, ($product->discontinued == 1 ? lang('yes') : lang('no')));
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->product_details);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->warranty_time);
                        $letter++;
                        $sheet->SetCellValue($letter . $row, $product->birthday_discount);
                        $row++;
                    }

                    for ($i="A"; $i <= "Z"; $i++) {
                        $sheet->getColumnDimension($i)->setAutoSize(true);
                    }

                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'products_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } elseif ($this->input->post('form_action') == 'update_price_group_base') {
                    $this->site->updateProductsPriceGroupBase();
                    $this->session->set_flashdata('message', $this->lang->line("updated_prices_group_base"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'set_featuring') {
                    $warning = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->products_model->set_featuring($id)) {
                            $warning = true;
                        }
                    }
                    if ($warning) {
                        $this->session->set_flashdata('warning', $this->lang->line("product_featured_error"));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("product_featured_updated"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'set_status') {
                    $warning = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$this->products_model->set_status($id)) {
                            $warning = true;
                        }
                    }
                    if ($warning) {
                        $this->session->set_flashdata('warning', $this->lang->line("product_status_error"));
                    } else {
                        $this->session->set_flashdata('message', $this->lang->line("product_status_updated"));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'sync_tienda_pro_to_erp_images') {
                    $this->load->integration_model('Uploads');
                    $this->load->integration_model('Product');
                    $Uploads = new Uploads();
                    $Product = new Product();
                    $integrations = $this->settings_model->getAllIntegrations();
                    $directorioTiendaPro = '../public/uploads/all';
                    $directorioTiendaProHost = $_SERVER['HTTP_HOST'].'/public/uploads/all/';
                    $directorioErp = 'assets/uploads';
                    $directorioErpThumb = 'assets/uploads/thumbs';
                    $delete_assigned_erp = 0;
                    foreach ($_POST['val'] as $id) {

                        $upload_secondary_photos = true; //Actualizar fotos segundarias
                        $upload_primary_photos = true; //Actualizar fotos primarias
                        $product = $this->products_model->getProductDetail($id);//datos de producto ERP

                        if ($delete_assigned_erp == 0) { //Si no queremos que sobrescriba los que en ERP ya tiene imagenes
                            $q = $this->db->get_where('product_photos', ['product_id'=>$id]);
                            if ($q->num_rows() > 0) { //Si el producto ya tiene imagenes segundarias asignadas
                                $upload_secondary_photos = false;
                            }
                            if (!empty($product->image) && $product->image != 'no_image.png') { //Si el producto ya tiene imagen asignada
                                $upload_primary_photos = false;
                            }

                            if (!$upload_secondary_photos && !$upload_primary_photos) { //Si no se actualiza ninguna imagen, saltamos el proceso
                                continue;
                            }
                        }

                        if ($integrations) {
                            foreach ($integrations as $integration) {
                                $Uploads->open($integration);
                                $Product->open($integration);
                                if ($storeProduct = $Product->find(['id_wappsi'=>$id])) { //obtener producto de tienda con id wappsi
                                    if (strpos($storeProduct->photos, ",") !== false) { //si el producto en tienda tiene varias  imagenes
                                        $num_imgs_processed = 0;
                                        $storeProductPhotos = explode(",", $storeProduct->photos);
                                        foreach ($storeProductPhotos as $keySpp => $storeProductPhoto) { //se recorren todas las imagenes con ids de tienda pro
                                            if ($num_imgs_processed > 0 && $upload_secondary_photos) { //si el id de imagen es DIFERENTE al de la principal del producto en tienda, y se actualizan imagenes SEGUNDARIAS
                                                $to_process_photo = $Uploads->find(['id' => $storeProductPhoto]); //se obtienen los datos de la imagen con el id de tienda
                                                if ($this->Settings->images_from_tpro == 0) { //Si el parámetro está en imagenes diferentes en tienda y erp
                                                    $directorioImagenTienda = '../public/'.$to_process_photo->file_name;
                                                    $newNameErpImage = str_replace("uploads/all/", "", $to_process_photo->file_name);
                                                    copy($directorioImagenTienda, $directorioErp."/".$newNameErpImage);
                                                    $this->db->insert('product_photos', ['product_id'=>$id, 'photo'=>$newNameErpImage]);
                                                } else { //Si el parámetro está en url de imagenes de tienda pro
                                                    $newNameErpImage = $_SERVER['HTTP_HOST'].'/public/'.$to_process_photo->file_name;
                                                    $this->db->insert('product_photos', ['product_id'=>$id, 'photo'=>$newNameErpImage]);
                                                }
                                            } else if ($num_imgs_processed == 0 && $upload_primary_photos) { //si el id de imagen es IGUAL al de la principal del producto en tienda, y se actualiza la imagen PRIMARIA

                                                $to_process_photo = $Uploads->find(['id' => $storeProductPhoto]); //se obtienen los datos de la imagen con el id de tienda
                                                if ($this->Settings->images_from_tpro == 0) { //Si el parámetro está en imagenes diferentes en tienda y erp
                                                    $directorioImagenTienda = '../public/'.$to_process_photo->file_name;
                                                    $newNameErpImage = str_replace("uploads/all/", "", $to_process_photo->file_name);
                                                    copy($directorioImagenTienda, $directorioErp."/".$newNameErpImage);
                                                    $this->db->update('products', ['image'=>$newNameErpImage], ['id'=>$id]);
                                                } else { //Si el parámetro está en url de imagenes de tienda pro
                                                    $newNameErpImage = $_SERVER['HTTP_HOST'].'/public/'.$to_process_photo->file_name;
                                                    $this->db->update('products', ['image'=>$newNameErpImage], ['id'=>$id]);
                                                }

                                            }
                                            $num_imgs_processed++;
                                        }
                                    } else if (strpos($storeProduct->photos, ",") === false && $upload_primary_photos) {//si el producto en tienda tiene solo una imagen
                                        
                                        $to_process_photo = $Uploads->find(['id' => $storeProduct->photos]); //se obtienen los datos de la imagen con el id de tienda
                                        if ($this->Settings->images_from_tpro == 0) { //Si el parámetro está en imagenes diferentes en tienda y erp
                                            $directorioImagenTienda = '../public/'.$to_process_photo->file_name;
                                            $newNameErpImage = str_replace("uploads/all/", "", $to_process_photo->file_name);
                                            copy($directorioImagenTienda, $directorioErp."/".$newNameErpImage);
                                            $this->db->update('products', ['image'=>$newNameErpImage], ['id'=>$id]);
                                        } else { //Si el parámetro está en url de imagenes de tienda pro
                                            $newNameErpImage = $_SERVER['HTTP_HOST'].'/public/'.$to_process_photo->file_name;
                                            $this->db->update('products', ['image'=>$newNameErpImage], ['id'=>$id]);
                                        }

                                    }
                                }
                            }
                        }
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'download_products_images') {
                    $zip_master = new ZipArchive();
                    $zip_file = 'assets/uploads/download_products.zip'; // Ruta del archivo ZIP

                    if ($zip_master->open($zip_file, ZipArchive::CREATE | ZipArchive::OVERWRITE) === TRUE) {
                        foreach ($_POST['val'] as $id) {
                            $product = $this->products_model->getProductDetail($id);
                            if (file_exists('assets/uploads/' . $product->image)) {
                                // Agregar la imagen principal del producto
                                $pimg = pathinfo($product->image);
                                $zip_master->addFile(
                                    'assets/uploads/' . $product->image,
                                    $product->code . "_01." . $pimg['extension']
                                );

                                // Agregar fotos adicionales del producto
                                $photos = $this->products_model->getProductPhotos($id);
                                if ($photos) {
                                    $img_num = 2; // Empezar desde 2 porque el principal es "01"
                                    foreach ($photos as $pphoto) {
                                        if (file_exists('assets/uploads/' . $pphoto->photo)) {
                                            $ppimg = pathinfo($pphoto->photo);
                                            $zip_master->addFile(
                                                'assets/uploads/' . $pphoto->photo,
                                                $product->code . "_" . $img_num . "." . $ppimg['extension']
                                            );
                                        }
                                        $img_num++;
                                    }
                                }
                            }
                        }
                        $zip_master->close();

                        // Configurar cabeceras para la descarga del archivo
                        header('Content-Type: application/zip');
                        header('Content-Disposition: attachment; filename="download_products.zip"');
                        header('Content-Length: ' . filesize($zip_file));

                        // Leer y enviar el archivo al navegador
                        readfile($zip_file);

                        // Eliminar el archivo ZIP del servidor después de la descarga
                        unlink($zip_file);
                    } else {
                        echo "Error al crear el archivo ZIP.";
                    }


                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_product_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'admin/products');
        }
    }

    public function delete_image($id = NULL)
    {
        $this->sma->checkPermissions('edit', true);
        if ($id && $this->input->is_ajax_request()) {
            header('Content-Type: application/json');
            $this->db->delete('product_photos', array('id' => $id));
            $this->sma->send_json(array('error' => 0, 'msg' => lang("image_deleted")));
        }
        $this->sma->send_json(array('error' => 1, 'msg' => lang("ajax_error")));
    }

    public function getSubUnits($unit_id)
    {
        $units = $this->site->getUnitsByBUID($unit_id);
        $this->sma->send_json($units);
    }

    public function getProductAVGCostByWarehouse($product_id = null, $warehouse_id = null)
    {
        $AVGcost = $this->products_model->getProductDataByWarehouse(['product_id' => $product_id, 'warehouse_id' => $warehouse_id]);
        $price = $AVGcost != false && $AVGcost->avg_cost != 0 ? $AVGcost->avg_cost : false;
        if ($price != false) {
            $product = $this->products_model->getProductByID($product_id);
            // $price = $this->sma->remove_tax_from_amount($product->tax_rate, $price);
            $price =  $price;
        }
        echo $price;
    }

    public function qa_suggestions()
    {
        $term = $this->input->get('term', true);
        $biller_id = $this->input->get('biller_id', true);
        $warehouse_id = $this->input->get('warehouse_id', true);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $rows = $this->products_model->getQASuggestions($sr, $biller_id, $warehouse_id);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                if ($this->Settings->product_variant_per_serial == 0) {
                    $options = $this->products_model->getProductOptions($row->id, $warehouse_id);
                } else {
                    $options = NULL;
                }
                if ($options) {
                    $opt = $option_id ? $this->purchases_model->getProductOptionByID($option_id) : current($options);
                    if (!$option_id) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->cost = 0;
                    $option_id = FALSE;
                }
                $row->avg_cost = $row->avg_cost;
                $row->cost = $row->cost;
                $row->option = $option_id;
                if (isset($row->variant_selected)) {
                    $row->variant_selected = $row->variant_selected;
                    $row->option = $row->variant_selected;
                }
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $row->serial = '';
                $c = sha1(uniqid(mt_rand(), true));
                $pr[] = array(
                    'id' => $c, 'item_id' => ($row->id.($row->option)), 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'options' => $options, 'tax_rate' => $tax_rate
                );
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    function adjustment_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->products_model->deleteAdjustment($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("adjustment_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'reaccount') {

                    foreach ($_POST['val'] as $id) {
                        $this->products_model->recontabilizarAjuste($id);
                    }
                    $this->session->set_flashdata('message', $this->lang->line("adjustment_reaccounted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $or_id = false;
                    foreach ($_POST['val'] as $id) {
                        if (!$or_id) {
                            $or_id = "(";
                        }
                        $or_id .= "adjustments.id = {$id} OR ";
                    }
                    $or_id = trim($or_id, " OR ").")";
                    $ai = "( SELECT adjustment_id, product_id, option_id, serial_no, {$this->db->dbprefix('products')}.name as item_nane, (CASE WHEN {$this->db->dbprefix('adjustment_items')}.type  = 'subtraction' THEN (0-{$this->db->dbprefix('adjustment_items')}.quantity) ELSE {$this->db->dbprefix('adjustment_items')}.quantity END) as pquantity from {$this->db->dbprefix('adjustment_items')} LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id={$this->db->dbprefix('adjustment_items')}.product_id) FAI";
                    $this->load->library('datatables');

                    $this->datatables
                        ->select("

                                    {$this->db->dbprefix('adjustments')}.*,
                                    DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                                    reference_no,
                                    warehouses.name as wh_name,
                                    CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by,
                                    (SELECT name FROM {$this->db->dbprefix('companies')} WHERE id = {$this->db->dbprefix('adjustments')}.companies_id) AS tercero,
                                    adjustments.note,
                                    FAI.item_nane as iname,
                                    FAI.pquantity,
                                    {$this->db->dbprefix('adjustments')}.id as id,
                                    biller.company as biller_name,
                                    categories.name as category_name,
                                    subcategory.name as subcategory_name,
                                    brands.name as brand_name,
                                    products.code as product_code,
                                    product_variants.code as variant_code,
                                    product_variants.name as variant_name
                                ", FALSE)
                        ->from('adjustments')
                        ->join($ai, 'FAI.adjustment_id=adjustments.id', 'left')
                        ->join('users', 'users.id=adjustments.created_by', 'left')
                        ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left')
                        ->join('companies as biller', 'biller.id=adjustments.biller_id', 'left')
                        ->join('products', 'products.id=FAI.product_id', 'left')
                        ->join('categories', 'products.category_id=categories.id', 'left')
                        ->join('categories as subcategory', 'products.subcategory_id=subcategory.id', 'left')
                        ->join('brands', 'products.brand=brands.id', 'left')
                        ->join('product_variants', 'product_variants.id=FAI.option_id', 'left')
                        ->where($or_id)
                        ;
                    $data = $this->datatables->get_display_result_2();
                    if (!empty($data)) {
                        $this->load->library('excel');
                        $this->excel->setActiveSheetIndex(0);
                        $this->excel->getActiveSheet()->setTitle(lang('adjustments_report'));
                        $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                        $this->excel->getActiveSheet()->SetCellValue('B1', lang('time'));
                        $this->excel->getActiveSheet()->SetCellValue('C1', lang('reference_no'));
                        $this->excel->getActiveSheet()->SetCellValue('D1', lang('origin'));
                        $this->excel->getActiveSheet()->SetCellValue('E1', lang('biller'));
                        $this->excel->getActiveSheet()->SetCellValue('F1', lang('warehouse'));
                        $this->excel->getActiveSheet()->SetCellValue('G1', lang('category'));
                        $this->excel->getActiveSheet()->SetCellValue('H1', lang('subcategory'));
                        $this->excel->getActiveSheet()->SetCellValue('I1', lang('brand'));
                        $this->excel->getActiveSheet()->SetCellValue('J1', lang('code'));
                        $this->excel->getActiveSheet()->SetCellValue('K1', lang('product'));
                        $this->excel->getActiveSheet()->SetCellValue('L1', lang('variant_code'));
                        $this->excel->getActiveSheet()->SetCellValue('M1', lang('variant'));
                        $this->excel->getActiveSheet()->SetCellValue('N1', lang('product_qty'));
                        $this->excel->getActiveSheet()->SetCellValue('O1', lang('created_by'));
                        $this->excel->getActiveSheet()->SetCellValue('P1', lang('third'));
                        $this->excel->getActiveSheet()->SetCellValue('Q1', lang('note'));
                        $row = 2;
                        foreach ($data as $data_row) {
                            $d_arr = explode(" ", $data_row->date);
                            $fecha = $d_arr[0];
                            $hora = $d_arr[1];
                            $this->excel->getActiveSheet()->SetCellValue('A'.$row, $fecha);
                            $this->excel->getActiveSheet()->SetCellValue('B'.$row, $hora);
                            $this->excel->getActiveSheet()->SetCellValue('C'.$row, $data_row->reference_no);


                            $this->excel->getActiveSheet()->SetCellValue('D'.$row, $data_row->origin_reference_no);
                            $this->excel->getActiveSheet()->SetCellValue('E'.$row, $data_row->biller_name);
                            $this->excel->getActiveSheet()->SetCellValue('F'.$row, $data_row->wh_name);

                            $this->excel->getActiveSheet()->SetCellValue('G'.$row, $data_row->category_name);
                            $this->excel->getActiveSheet()->SetCellValue('H'.$row, $data_row->subcategory_name);
                            $this->excel->getActiveSheet()->SetCellValue('I'.$row, $data_row->brand_name);
                            $this->excel->getActiveSheet()->SetCellValue('J'.$row, "'".$data_row->product_code);
                            $this->excel->getActiveSheet()->SetCellValue('K'.$row, $data_row->iname);
                            $this->excel->getActiveSheet()->SetCellValue('L'.$row, $data_row->variant_code);
                            $this->excel->getActiveSheet()->SetCellValue('M'.$row, $data_row->variant_name);
                            $this->excel->getActiveSheet()->SetCellValue('N'.$row, $data_row->pquantity);
                            $this->excel->getActiveSheet()->SetCellValue('O'.$row, $data_row->created_by);
                            $this->excel->getActiveSheet()->SetCellValue('P'.$row, $data_row->tercero);
                            $this->excel->getActiveSheet()->SetCellValue('Q'.$row, $this->sma->decode_html($data_row->note));
                            $row++;
                        }
                        for ($i='A'; $i <= 'Z'; $i++) {
                            $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                        }
                        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

                        $range = 'J1:J'.$row;
                        $this->excel->getActiveSheet()->getStyle($range)->getNumberFormat()->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
                        $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                        $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                        $this->excel->getActiveSheet()->getStyle('G2:G' . $row)->getAlignment()->setWrapText(true);
                        $filename = 'adjustments_report';
                        $this->load->helper('excel');

                        $this->db->insert('user_activities', [
                            'date' => date('Y-m-d H:i:s'),
                            'type_id' => 6,
                            'table_name' => 'adjustments',
                            'record_id' => null,
                            'user_id' => $this->session->userdata('user_id'),
                            'module_name' => $this->m,
                            'description' => $this->session->first_name." ".$this->session->last_name.' exportó '.lang('adjustments_report'),
                        ]);

                        create_excel($this->excel, $filename);
                    }


                }
            } else {
                $this->session->set_flashdata('error', $this->lang->line("no_record_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    function stock_counts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
        } else {
            $this->data['warehouses'] = NULL;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
        }

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('stock_counts')));
        $meta = array('page_title' => lang('stock_counts'), 'bc' => $bc);
        $this->page_construct('products/stock_counts', $meta, $this->data);
    }

    function getCounts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('stock_count', TRUE);

        if ((!$this->Owner || !$this->Admin) && !$warehouse_id) {
            $user = $this->site->getUser();
            $warehouse_id = $user->warehouse_id;
        }
        $detail_link = anchor('admin/products/view_count/$1', '<label class="label label-primary pointer">' . lang('details') . '</label>', 'class="tip" title="' . lang('details') . '" data-toggle="modal" data-target="#myModal"');

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('stock_counts')}.id as id, date, reference_no, {$this->db->dbprefix('warehouses')}.name as wh_name, {$this->db->dbprefix('stock_counts')}.type, brand_names, category_names, initial_file, final_file")
            ->from('stock_counts')
            ->join('warehouses', 'warehouses.id=stock_counts.warehouse_id', 'left');
        if ($warehouse_id) {
            $this->datatables->where('warehouse_id', $warehouse_id);
        }

        $this->datatables->add_column('Actions', '<div class="text-center">' . $detail_link . '</div>', "id");
        echo $this->datatables->generate();
    }

    function view_count($id)
    {
        $this->sma->checkPermissions('stock_count', TRUE);
        $stock_count = $this->products_model->getStouckCountByID($id);
        if (!$stock_count->finalized) {
            $this->sma->md('admin/products/finalize_count/' . $id);
        }

        $this->data['stock_count'] = $stock_count;
        $this->data['stock_count_items'] = $this->products_model->getStockCountItems($id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($stock_count->warehouse_id);
        $this->data['adjustment'] = $this->products_model->getAdjustmentByCountID($id);
        $this->load_view($this->theme . 'products/view_count', $this->data);
    }

    function count_stock_variants($page = NULL)
    {
        $this->load->library('excel');
        $this->sma->checkPermissions('stock_count');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('company_id', lang("third"), 'required');

        if ($this->form_validation->run() == true) {
            if ($_FILES['xls_file']['size'] > 0) {
                if ($this->Owner || $this->Admin) {
                    $date = $this->sma->fld($this->input->post('date'));
                } else {
                    $date = date('Y-m-d H:s:i');
                }
                $biller_id = $this->input->post('biller');
                $warehouse_id = $this->input->post('warehouse');
                $document_type = $this->input->post('document_type_id') ? $this->input->post('document_type_id') : NULL;
                $companies_id = $this->input->post('company_id');
                $typeAjustment = $this->input->post('type'); /*** 1=>products, 2=>variantes ***/
                $note = $this->sma->clear_tags($this->input->post('note'));
                $banderaVariante = '';
                $adjustment = array(
                    'date' => $date,
                    'warehouse_id' => $warehouse_id,
                    'biller_id' => $biller_id,
                    'document_type_id' => $document_type,
                    'note' => $note,
                    'created_by' => $this->session->userdata('user_id'),
                    'companies_id' => $companies_id,
                    'count_id' => NULL,
                );

                $archivo = $_FILES['xls_file']['tmp_name'];
                $objPHPExcel = PHPExcel_IOFactory::load($archivo);
                $hojas = $objPHPExcel->getWorksheetIterator();
                $invalids_products = false;
                $txt_errors = "";
                $products = [];
                foreach ($hojas as $hoja) {
                    $dataPage = $hoja->toArray();
                    unset($dataPage[0]);
                    $lineNo = 2;
                    foreach ($dataPage as $data_row) {
                        if (empty($data_row[0])) {
                            continue;
                        }
                        if ($typeAjustment == '1') {
                            $product = $this->products_model->getProductByCode(trim($data_row[0]));  
                        }else if($typeAjustment == '2') {
                            $variant = $this->products_model->getProductOptionsV(trim($data_row[0]));
                            if ($variant && count($variant) == '1') {
                                $product = $this->products_model->getProductByID($variant[0]->product_id);
                            } else { // <- si no existe la variante o el código esta repetido, corta la ejecución
                                $banderaVariante = $data_row[0];
                                break 2;
                            }
                        }

                        if ($product) {
                            $csv_variant = ($typeAjustment == '1') ? "N/A" : trim($data_row[0]);  // <- del archivo, si es uno es N/A, si es 2 sería la columna 0
                            $variant = !empty($csv_variant) && ucwords(mb_strtolower($csv_variant)) != "N/A" ? $this->products_model->getProductVariantByCode($product->id, $csv_variant) : FALSE;

                            /*  
                                Modificacion al proceso, al ajuste se envia las diferencias para lograr la cantidad que estan importando 
                                es decir de este producto estan importando 10 y existen en la db 9, seria un ajuste de +1, la logica seía la siguiente 
                                1. Obtener la cantidad actual de la bodega 
                                2. Calcular la diferencia (Importado - Bd)
                                3. Asignar a la variable
                            */

                            if ($typeAjustment == '1') {
                                $warehouse = $this->site->getWarehouseProducts($product->id, $warehouse_id);
                                $items[] = array(
                                    'product_code' => $product->code,
                                    'product_name' => $product->name,
                                    'variant' => '',
                                    'counted' => trim($data_row[1]),
                                );
                            }else if($typeAjustment == '2'){
                                $warehouse = $this->products_model->getWarehouseProductVariant($warehouse_id, $product->id, $variant->id);
                                $items[] = array(
                                    'product_code' => $product->code,
                                    'product_name' => $product->name,
                                    'variant' => $variant->name,
                                    'counted' => trim($data_row[1]),
                                );
                            }
                            $csv_quantity = floatval(trim($data_row[1])) - $warehouse->quantity; // <- diferencia
                            $type = $csv_quantity > 0 ? 'addition' : 'subtraction';
                            $quantity = abs($csv_quantity);
                            
                            /*
                                Modificar el proceso, 
                                Positivo => ultimo costo
                                Negativo => costo promedio 
                            */
                            $csv_cost = ($type == 'addition') ? $product->cost : $product->avg_cost;
                            $AVGcost = $this->products_model->getProductDataByWarehouse(['product_id' => $product->id, 'warehouse_id' => $warehouse_id]);
                            $cost_def = $csv_cost > 0 ? $csv_cost : ($AVGcost != false && $AVGcost->avg_cost != 0 ? $AVGcost->avg_cost : $product->cost);
                            $net_cost = 0;
                            if ($this->Settings->tax_method == 1) { // <- antes de impuestos
                                $tax_rate = $this->site->getTaxRateByID($product->tax_rate);
                                $ctax = $this->site->calculateTax($product, $tax_rate, $cost_def);
                                $net_cost = $cost_def - $ctax['amount'];
                            }else{ // <- impuesto incluido
                                $net_cost = $cost_def;
                            }
                            if ($quantity > 0) { // <- se agrega esta validacion para que no ingrese los 0 al ajuste
                                $adjustmentItems[] = array(
                                    'product_id' => $product->id,
                                    'type' => $type,
                                    'quantity' => $quantity,
                                    'warehouse_id' => $warehouse_id,
                                    'option_id' => $variant->id,
                                    'adjustment_cost' => $csv_cost,
                                    'net_adjustment_cost' => $net_cost,
                                    'avg_cost' => $csv_cost > 0 ? $csv_cost : ($AVGcost != false && $AVGcost->avg_cost != 0 ? $AVGcost->avg_cost : $product->cost),
                                );
                                $productsNames[$product->id] = $product->name;
                            }
                        } else { // <- si no existe productos corta la ejecución y no realiza el ajuste
                            $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $data_row[0] . '). ' . lang('product_code_x_exist') . ' ' . lang('line_no') . ' ' . $lineNo);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $lineNo++;
                    }
                }

                if ($banderaVariante != '') {
                    $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $banderaVariante . '). ' . lang('line_no') . ' ' . $lineNo);
                    admin_redirect('products/count_stock');
                }

                if (count($adjustmentItems) == 0) {
                    $this->session->set_flashdata('error', lang('no_quantities_to_adjust') );
                    admin_redirect('products/count_stock');
                }

                if (!empty($items)) {
                    $warehouse = $this->site->getWarehouseByID($warehouse_id);
                    $name = 'Conteo_bodega_' . $warehouse->code . '_' . round(microtime(true) * 1000) . '.CSV';
                    $csv_file = fopen('./files/' . $name, 'w');
                
                    // Preparar la cabecera
                    $header = array(
                        $this->sma->utf8Decode(lang('product_code')),
                        lang('product_name'),
                        lang('variant'),
                        lang('counted')
                    );
                    fputcsv($csv_file, $header);
                
                    // Escribir datos
                    $buffer = '';
                    foreach ($items as $item) {
                        $buffer .= implode(",", $item) . "\n"; // Agregar los datos al buffer
                    }
                
                    fwrite($csv_file, $buffer); // Escribir el buffer completo de una vez
                    fclose($csv_file);
                }

                if ($this->Owner || $this->Admin) {
                    $date = $this->sma->fld($this->input->post('date'));
                } else {
                    $date = date('Y-m-d H:s:i');
                }

                $data = array(
                    'date' => $date,
                    'warehouse_id' => $warehouse_id,
                    'initial_file' => $name,
                    'created_by' => $this->session->userdata('user_id'),
                    'biller_id' => $biller_id,
                    'type' => 'full',
                    'document_type_id' => $document_type,
                    'company_id' => $companies_id,
                    'automatic_adjustment' => $typeAjustment,
                    'finalized' => 1,
                );

                if ($this->products_model->addAdjustment($adjustment, $adjustmentItems, $productsNames, false) && $this->products_model->addStockCount($data)) {
                    $this->db->insert('user_activities', [
                        'date' => date('Y-m-d H:i:s'),
                        'type_id' => 3,
                        'table_name' => 'adjustments',
                        'record_id' => null,
                        'user_id' => $this->session->userdata('user_id'),
                        'module_name' => $this->m,
                        'description' => $this->session->first_name." ".$this->session->last_name.' creo ajuste de cantidades con importación de documento. ',
                    ]);
                    $this->session->set_flashdata('message', lang("adjustment_saved"));
                    admin_redirect('products/stock_counts');
                }  
            }
        }
         else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['companies'] = $this->site->getAllCompanies($this->Settings->companies_for_adjustments_transfers);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('count_stock')));
            $meta = array('page_title' => lang('count_stock_variants'), 'bc' => $bc);
            $this->page_construct('products/count_stock_variants', $meta, $this->data);
        }
    }

    function count_stock($page = NULL)
    {
        $this->sma->checkPermissions('stock_count');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('type', lang("type"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');

        if ($this->form_validation->run() == true) {

            $warehouse_id = $this->input->post('warehouse');
            $type = $this->input->post('type');
            $categories = $this->input->post('category') ? $this->input->post('category') : NULL;
            $brands = $this->input->post('brand') ? $this->input->post('brand') : NULL;
            $this->load->helper('string');
            $name = random_string('md5') . '.csv';
            $products = $this->products_model->getStockCountProducts($warehouse_id, $type, $categories, $brands);
            $pr = 0;
            $rw = 0;
            foreach ($products as $product) {
                if ($variants = $this->products_model->getStockCountProductVariants($warehouse_id, $product->id)) {
                    foreach ($variants as $variant) {
                        $items[] = array(
                            'product_code' => $product->code,
                            'product_name' => $product->name,
                            'variant' => $variant->name,
                            'expected' => $variant->quantity,
                            'counted' => '',
                            'activo' => '',
                        );
                        $rw++;
                    }
                } else {
                    $items[] = array(
                        'product_code' => '="' . $product->code . '"',
                        'product_name' => $product->name,
                        'variant' => '',
                        'expected' => $this->sma->formatNumber($product->quantity),
                        'counted' => '',
                        'activo' => $product->discontinued == 1 ?  'no' : 'si',
                    );
                    $rw++;
                }
                $pr++;
            }
            if (!empty($items)) {
                $csv_file = fopen('./files/' . $name, 'w');
                fputcsv($csv_file, array($this->sma->utf8Decode(lang('product_code')), lang('product_name'), lang('variant'), lang('expected'), lang('counted'), lang('active')));
                foreach ($items as $item) {
                    fputcsv($csv_file, $item);
                }
                // file_put_contents('./files/'.$name, $csv_file);
                // fwrite($csv_file, $txt);
                fclose($csv_file);
            } else {
                $this->session->set_flashdata('error', lang('no_product_found'));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $category_ids = '';
            $brand_ids = '';
            $category_names = '';
            $brand_names = '';
            if ($categories) {
                $r = 1;
                $s = sizeof($categories);
                foreach ($categories as $category_id) {
                    $category = $this->site->getCategoryByID($category_id);
                    if ($r == $s) {
                        $category_names .= $category->name;
                        $category_ids .= $category->id;
                    } else {
                        $category_names .= $category->name . ', ';
                        $category_ids .= $category->id . ', ';
                    }
                    $r++;
                }
            }
            if ($brands) {
                $r = 1;
                $s = sizeof($brands);
                foreach ($brands as $brand_id) {
                    $brand = $this->site->getBrandByID($brand_id);
                    if ($r == $s) {
                        $brand_names .= $brand->name;
                        $brand_ids .= $brand->id;
                    } else {
                        $brand_names .= $brand->name . ', ';
                        $brand_ids .= $brand->id . ', ';
                    }
                    $r++;
                }
            }
            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'type' => $type,
                'categories' => $category_ids,
                'category_names' => $category_names,
                'brands' => $brand_ids,
                'brand_names' => $brand_names,
                'initial_file' => $name,
                'products' => $pr,
                'rows' => $rw,
                'created_by' => $this->session->userdata('user_id'),
                'biller_id' => $this->input->post('biller'),
                'document_type_id' => $this->input->post('document_type_id'),
                'company_id' => $this->input->post('company_id'),
                'automatic_adjustment' => $this->input->post('automatic_adjustment') ? $this->input->post('automatic_adjustment') : 0,
            );
        }

        if ($this->form_validation->run() == true && $this->products_model->addStockCount($data)) {
            $this->session->set_flashdata('message', lang("stock_count_intiated"));
            admin_redirect('products/stock_counts');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['categories'] = $this->site->getAllCategories();
            $this->data['brands'] = $this->site->getAllBrands();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['companies'] = $this->companies_model->getAllCompanies();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('count_stock')));
            $meta = array('page_title' => lang('count_stock'), 'bc' => $bc);
            $this->page_construct('products/count_stock', $meta, $this->data);
        }
    }

    function finalize_count($id)
    {
        $this->sma->checkPermissions('stock_count');
        $stock_count = $this->products_model->getStouckCountByID($id);
        if (!$stock_count || $stock_count->finalized) {
            $this->session->set_flashdata('error', lang("stock_count_finalized"));
            admin_redirect('products/stock_counts');
        }
        $this->form_validation->set_rules('count_id', lang("count_stock"), 'required');
        if ($this->form_validation->run() == true) {
            if ($_FILES['csv_file']['size'] > 0) {
                $note = $this->sma->clear_tags($this->input->post('note'));
                $data = array(
                    'updated_by' => $this->session->userdata('user_id'),
                    'updated_at' => date('Y-m-d H:s:i'),
                    'note' => $note
                );
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('csv_file')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $csv = $this->upload->file_name;
                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                $separator = (count(fgetcsv($handle, null, ",")) > 1) ? "," : ";";
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, $separator)) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                // $titles = array_shift($arrResult);
                $keys = array('product_code', 'product_name', 'product_variant', 'expected', 'counted', 'activo');
                $final = array();
                try {
                    foreach ($arrResult as $key => $value) {
                        // Verificamos si las longitudes coinciden antes de llamar a array_combine
                        if (count($keys) !== count($value)) {
                            throw new Exception("Los tamaños de los arrays no coinciden. Keys tiene " . count($keys) . " elementos y value tiene " . count($value) . " elementos en la iteración: $key");
                        }

                        $final[] = array_combine($keys, $value);
                    }
                } catch (Exception $e) {
                    $eMsg = "";
                    $eMsg .= "Error: " . $e->getMessage() . PHP_EOL;
                    $eMsg .= "Keys: ";
                    $eMsg .= print_r($keys);
                    $eMsg .= "Value: ";
                    $eMsg .= print_r($value);
                    exit($eMsg);
                }
                $rw = 2;
                $differences = 0;
                $matches = 0;
                foreach ($final as $pr) {
                    if ($product = $this->products_model->getProductByCode(trim($pr['product_code']))) {
                        $pr['counted'] = !empty($pr['counted']) ? $pr['counted'] : 0;
                        if ($pr['expected'] == $pr['counted']) {
                            $matches++;
                        } else {
                            $pr['stock_count_id'] = $id;
                            $pr['product_id'] = $product->id;
                            $pr['cost'] = $product->cost;
                            $pr['product_variant_id'] = empty($pr['product_variant']) ? NULL : $this->products_model->getProductVariantID($pr['product_id'], $pr['product_variant']);
                            $products[] = $pr;
                            $differences++;
                        }
                    } else {
                        $this->session->set_flashdata('error', lang('check_product_code') . ' (' . $pr['product_code'] . '). ' . lang('product_code_x_exist') . ' ' . lang('line_no') . ' ' . $rw);
                        admin_redirect('products/finalize_count/' . $id);
                    }
                    $rw++;
                }
                $data['final_file'] = $csv;
                $data['differences'] = $differences;
                $data['matches'] = $matches;
                $data['missing'] = $stock_count->rows - ($rw - 2);
                $data['finalized'] = 1;
            }

            if ($stock_count->automatic_adjustment == 1) {
                $adjustment = array(
                    'date' => $stock_count->date,
                    'reference_no' => $stock_count->reference_no,
                    'warehouse_id' => $stock_count->warehouse_id,
                    'note' => $stock_count->note,
                    'created_by' => $stock_count->created_by,
                    'biller_id' => $stock_count->biller_id,
                    'document_type_id' => $stock_count->document_type_id,
                    'companies_id' => $stock_count->company_id,
                );
                foreach ($products as $row) {
                    $diference = 0;
                    $wh_data = $this->products_model->getProductDataByWarehouse(['product_id' => $row['product_id'], 'warehouse_id' => $stock_count->warehouse_id]);
                    $diference = $row['counted'] - $wh_data->quantity;
                    if ($diference < 0) {
                        $typeAjustment = 'subtraction';
                        $diference = $diference * -1;
                    } else {
                        $typeAjustment = 'addition';
                    }
                    $adjustmentItems[] = array(
                        'product_id' => $row['product_id'],
                        'quantity' => $diference,
                        'warehouse_id' => $stock_count->warehouse_id,
                        'type' => $typeAjustment,
                        'option_id' => "",
                        'serial_no' => "",
                        'avg_cost' => $row['cost']
                    );
                }
            }
        }

        if ($this->form_validation->run() == true && $this->products_model->finalizeStockCount($id, $data, $products)) {
            if ($stock_count->automatic_adjustment == 1) {
                $this->products_model->addAdjustment($adjustment, $adjustmentItems, [], false);
            }
            $this->session->set_flashdata('message', lang("stock_count_finalized"));
            admin_redirect('products/stock_counts');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['stock_count'] = $stock_count;
            $this->data['warehouse'] = $this->site->getWarehouseByID($stock_count->warehouse_id);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => admin_url('products/stock_counts'), 'page' => lang('stock_counts')), array('link' => '#', 'page' => lang('finalize_count')));
            $meta = array('page_title' => lang('finalize_count'), 'bc' => $bc);
            $this->page_construct('products/finalize_count', $meta, $this->data);
        }
    }

    function getSalesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('view_sales', TRUE, 'products');
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') . " 00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') . " 23:59" : NULL;
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;

        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, reference_no, biller, customer, GROUP_CONCAT(" . $this->db->dbprefix('sale_items') . ".product_name SEPARATOR '\n') as iname, GROUP_CONCAT(CAST(" . $this->db->dbprefix('sale_items') . ".quantity AS DECIMAL(15,4)) SEPARATOR '\n') as pquantity, grand_total, paid, payment_status, ".$this->db->dbprefix('sale_items'). ".subtotal ", FALSE)
                ->from('sales')
                ->join('sale_items', 'sale_items.sale_id=sales.id', 'left')
                ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left')
                ->group_by('sales.id')
                ->order_by('sales.date desc');

            if ($user) {
                $this->db->where('sales.created_by', $user);
            }
            if ($product) {
                $this->db->where('sale_items.product_id', $product);
            }
            if ($serial) {
                $this->db->like('sale_items.serial_no', $serial);
            }
            if ($biller) {
                $this->db->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('sales.customer_id', $customer);
            }
            if ($warehouse) {
                $this->db->where('sales.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('sales.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('sales') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {
                $letter = "A";
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('total')." ".lang('product') );
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('total_invoice'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('payment_status'));

                $row = 2;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $qty = 0;
                $total_product = 0;
                foreach ($data as $data_row) {
                    $txtQtys = "";

                    if (strpos($data_row->pquantity, "\n")) {
                        $qtys = explode("\n", $data_row->pquantity);
                        foreach ($qtys as $key => $value) {
                            $txtQtys .= $this->sma->formatQuantity($value) . "\n";
                            $qty += $value;
                        }
                    } else {
                        $txtQtys = $this->sma->formatQuantity($data_row->pquantity) . " ";
                        $qty += $data_row->pquantity;
                    }

                    $letter = 'A';
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->customer);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $txtQtys);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->subtotal);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->paid);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, ($data_row->grand_total - $data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, lang($data_row->payment_status));
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $total_product += $data_row->subtotal;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("G" . $row . ":J" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatQuantity($qty) . " ");
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $total_product);
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $balance);

                for ($i='A'; $i <= $letter; $i++) {
                    $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                }
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('G1:G' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('H1:H' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('I1:I' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('J1:J' . $row)->getNumberFormat()->setFormatCode("#,##0.00");

                $range = 'F1:F' . $row;
                $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $filename = 'sales_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {


            //GROUP_CONCAT({$this->db->dbprefix('sale_items')}.product_name) as item_nane
            $si = "(
                    SELECT
                        SUM({$this->db->dbprefix('sale_items')}.quantity) as quantity,
                        sale_id,
                        product_id,
                        serial_no,
                        GROUP_CONCAT(CONCAT({$this->db->dbprefix('sale_items')}.product_name) SEPARATOR '___') as item_nane,
                        (unit_price * quantity) as item_unit_price
                    FROM {$this->db->dbprefix('sale_items')} ";
            if ($product || $serial) {
                $si .= " WHERE ";
            }
            if ($product) {
                $si .= " {$this->db->dbprefix('sale_items')}.product_id = {$product} ";
            }
            if ($product && $serial) {
                $si .= " AND ";
            }
            if ($serial) {
                $si .= " {$this->db->dbprefix('sale_items')}.serial_no LIKe '%{$serial}%' ";
            }
            $si .= " GROUP BY {$this->db->dbprefix('sale_items')}.sale_id ) FSI";

            $this->load->library('datatables');

            $this->datatables
                ->select("
                            DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                            reference_no,
                            biller,
                            customer,
                            FSI.item_nane as iname,
                            FSI.quantity,
                            item_unit_price,
                            grand_total,
                            paid,
                            (grand_total-paid) as balance,
                            payment_status,
                            sale_status,
                            {$this->db->dbprefix('sales')}.id as id
                        ", FALSE)
                ->from('sales')
                ->join($si, 'FSI.sale_id=sales.id', 'left')
                ->join('warehouses', 'warehouses.id=sales.warehouse_id', 'left');

            if ($user) {
                $this->datatables->where('sales.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FSI.product_id', $product);
            }
            if ($serial) {
                $this->datatables->like('FSI.serial_no', $serial);
            }
            if ($biller) {
                $this->datatables->where('sales.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('sales.customer_id', $customer);
            }
            if ($warehouse) {
                $this->datatables->where('sales.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('sales.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('sales') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }

    function getQuotesReport($pdf = NULL, $xls = NULL)
    {

        if ($this->input->get('product')) {
            $product = $this->input->get('product');
        } else {
            $product = NULL;
        }
        if ($this->input->get('user')) {
            $user = $this->input->get('user');
        } else {
            $user = NULL;
        }
        if ($this->input->get('customer')) {
            $customer = $this->input->get('customer');
        } else {
            $customer = NULL;
        }
        if ($this->input->get('biller')) {
            $biller = $this->input->get('biller');
        } else {
            $biller = NULL;
        }
        if ($this->input->get('warehouse')) {
            $warehouse = $this->input->get('warehouse');
        } else {
            $warehouse = NULL;
        }
        if ($this->input->get('reference_no')) {
            $reference_no = $this->input->get('reference_no');
        } else {
            $reference_no = NULL;
        }
        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date') . " 00:00";
        } else {
            $start_date = NULL;
        }
        if ($this->input->post('end_date')) {
            $end_date = $this->input->post('end_date') . " 23:59";
        } else {
            $end_date = NULL;
        }
        if ($pdf || $xls) {

            $this->db
                ->select("date, reference_no, biller, customer, GROUP_CONCAT(CONCAT(" . $this->db->dbprefix('quote_items') . ".product_name, ' (', " . $this->db->dbprefix('quote_items') . ".quantity, ')') SEPARATOR '<br>') as iname, grand_total, " . $this->db->dbprefix('quotes') . ".status", FALSE)
                ->from('quotes')
                ->join('quote_items', 'quote_items.quote_id=quotes.id', 'left')
                ->join('warehouses', 'warehouses.id=quotes.warehouse_id', 'left')
                ->group_by('quotes.id');

            if ($user) {
                $this->db->where('quotes.created_by', $user);
            }
            if ($product) {
                $this->db->where('quote_items.product_id', $product);
            }
            if ($biller) {
                $this->db->where('quotes.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('quotes.customer_id', $customer);
            }
            if ($warehouse) {
                $this->db->where('quotes.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('quotes.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('quotes') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('quotes_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('status'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->customer);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatMoney($data_row->grand_total));
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->status);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $filename = 'quotes_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $qi = "( SELECT
                        quote_id,
                        product_id,
                        GROUP_CONCAT(CONCAT({$this->db->dbprefix('quote_items')}.product_name, '__', {$this->db->dbprefix('quote_items')}.quantity) SEPARATOR '___') as item_nane,
                        SUM({$this->db->dbprefix('quote_items')}.quantity) as item_qty
                    FROM {$this->db->dbprefix('quote_items')} ";
            if ($product) {
                $qi .= " WHERE {$this->db->dbprefix('quote_items')}.product_id = {$product} ";
            }
            $qi .= " GROUP BY {$this->db->dbprefix('quote_items')}.quote_id ) FQI";
            $this->load->library('datatables');
            $this->datatables
                ->select("date, reference_no, biller, customer, FQI.item_nane as iname, item_qty, {$this->db->dbprefix('quotes')}.status, {$this->db->dbprefix('quotes')}.id as id", FALSE)
                ->from('quotes')
                ->join($qi, 'FQI.quote_id=quotes.id', 'left')
                ->join('warehouses', 'warehouses.id=quotes.warehouse_id', 'left')
                ->group_by('quotes.id');

            if ($user) {
                $this->datatables->where('quotes.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FQI.product_id', $product, FALSE);
            }
            if ($biller) {
                $this->datatables->where('quotes.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('quotes.customer_id', $customer);
            }
            if ($warehouse) {
                $this->datatables->where('quotes.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('quotes.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('quotes') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }

    function getPurchasesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('view_purchases', TRUE, 'products');

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $supplier = $this->input->get('supplier') ? $this->input->get('supplier') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') . " 00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') . " 23:59" : NULL;

        // if ($start_date) {
        //     $start_date = $this->sma->fld($start_date);
        //     $end_date = $this->sma->fld($end_date);
        // }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        if ($pdf || $xls) {

            $this->db
                ->select("" . $this->db->dbprefix('purchases') . ".date, reference_no, " . $this->db->dbprefix('warehouses') . ".name as wname, supplier, GROUP_CONCAT(" . $this->db->dbprefix('purchase_items') . ".product_name SEPARATOR '\n') as iname, GROUP_CONCAT(CAST(" . $this->db->dbprefix('purchase_items') . ".quantity AS DECIMAL(15,4)) SEPARATOR '\n') as pquantity, grand_total, paid, " . $this->db->dbprefix('purchases') . ".status", FALSE)
                ->from('purchases')
                ->join('purchase_items', 'purchase_items.purchase_id=purchases.id AND purchase_items.status != "service_received"', 'left')
                ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left')
                ->where('purchases.purchase_type', 1)
                ->group_by('purchases.id')
                ->order_by('purchases.date desc');

            if ($user) {
                $this->db->where('purchases.created_by', $user);
            }
            if ($product) {
                $this->db->where('purchase_items.product_id', $product);
            }
            if ($supplier) {
                $this->db->where('purchases.supplier_id', $supplier);
            }
            if ($warehouse) {
                $this->db->where('purchases.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('purchases.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('purchases') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('purchase_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('supplier'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue('I1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue('J1', lang('status'));

                $row = 2;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $qty = 0;
                foreach ($data as $data_row) {

                    $txtQtys = "";

                    if (strpos($data_row->pquantity, "\n")) {
                        $qtys = explode("\n", $data_row->pquantity);
                        foreach ($qtys as $key => $value) {
                            $txtQtys .= $this->sma->formatQuantity($value) . "\n";
                            $qty += $value;
                        }
                    } else {
                        $txtQtys = $this->sma->formatQuantity($data_row->pquantity) . " ";
                        $qty += $data_row->pquantity;
                    }



                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->wname);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->supplier);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $txtQtys);
                    // $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->pquantity);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->paid);
                    $this->excel->getActiveSheet()->SetCellValue('I' . $row, ($data_row->grand_total - $data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $data_row->status);
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("G" . $row . ":I" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatQuantity($qty) . " ");
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $balance);

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(20);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('G1:G' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('H1:H' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('I1:I' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $range = 'F1:F' . $row;
                $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $filename = 'purchase_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $pi = "( SELECT
            purchase_id,
            product_id,
            status,
            SUM({$this->db->dbprefix('purchase_items')}.quantity) as quantity,
            (GROUP_CONCAT(CONCAT({$this->db->dbprefix('purchase_items')}.product_name) SEPARATOR '___')) as item_nane
            from {$this->db->dbprefix('purchase_items')} ";
            if ($product) {
                $pi .= " WHERE {$this->db->dbprefix('purchase_items')}.product_id = {$product} ";
            }
            $pi .= " GROUP BY {$this->db->dbprefix('purchase_items')}.purchase_id ) FPI";



            $this->load->library('datatables');
            $this->datatables
                ->select("DATE_FORMAT({$this->db->dbprefix('purchases')}.date, '%Y-%m-%d %T') as date, reference_no, {$this->db->dbprefix('warehouses')}.name as wname, supplier, (FPI.item_nane) as iname, FPI.quantity, grand_total, paid, (grand_total-paid) as balance, {$this->db->dbprefix('purchases')}.status, {$this->db->dbprefix('purchases')}.id as id", FALSE)
                ->from('purchases')
                ->where('purchases.purchase_type', 1)
                ->join($pi, 'FPI.purchase_id=purchases.id AND FPI.status != "service_received"', 'left')
                ->join('warehouses', 'warehouses.id=purchases.warehouse_id', 'left');

            if ($user) {
                $this->datatables->where('purchases.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FPI.product_id', $product, FALSE);
            }
            if ($supplier) {
                $this->datatables->where('purchases.supplier_id', $supplier);
            }
            if ($warehouse) {
                $this->datatables->where('purchases.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('purchases.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('purchases') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }

    function getTransfersReport($pdf = NULL, $xls = NULL)
    {
        if ($this->input->get('product')) {
            $product = $this->input->get('product');
        } else {
            $product = NULL;
        }

        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date') . " 00:00";
        } else {
            $start_date = NULL;
        }

        if ($this->input->post('end_date')) {
            $end_date = $this->input->post('end_date') . " 23:59";
        } else {
            $end_date = NULL;
        }

        if ($pdf || $xls) {

            $this->db
                ->select($this->db->dbprefix('transfers') . ".date, reference_no, (CASE WHEN " . $this->db->dbprefix('transfers') . ".status = 'completed' THEN  GROUP_CONCAT(" . $this->db->dbprefix('purchase_items') . ".product_name SEPARATOR '<br>') ELSE GROUP_CONCAT(" . $this->db->dbprefix('transfer_items') . ".product_name SEPARATOR '<br>') END) as iname, (CASE WHEN " . $this->db->dbprefix('transfers') . ".status = 'completed' THEN  GROUP_CONCAT(" . $this->db->dbprefix('purchase_items') . ".quantity SEPARATOR '<br>') ELSE GROUP_CONCAT(" . $this->db->dbprefix('transfer_items') . ".quantity SEPARATOR '<br>') END) as pquantity,  from_warehouse_name as fname, from_warehouse_code as fcode, to_warehouse_name as tname,to_warehouse_code as tcode, grand_total, " . $this->db->dbprefix('transfers') . ".status")
                ->from('transfers')
                ->join('transfer_items', 'transfer_items.transfer_id=transfers.id', 'left')
                ->join('purchase_items', 'purchase_items.transfer_id=transfers.id', 'left')
                ->group_by('transfers.id')->order_by('transfers.date desc');
            if ($product) {
                $this->db->where($this->db->dbprefix('purchase_items') . ".product_id", $product);
                $this->db->or_where($this->db->dbprefix('transfer_items') . ".product_id", $product);
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('transfers_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('warehouse') . ' (' . lang('from') . ')');
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('warehouse') . ' (' . lang('to') . ')');
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('grand_total'));
                $this->excel->getActiveSheet()->SetCellValue('H1', lang('status'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->pquantity);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->fname . ' (' . $data_row->fcode . ')');
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->tname . ' (' . $data_row->tcode . ')');
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $this->sma->formatMoney($data_row->grand_total));
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, $data_row->status);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('C2:C' . $row)->getAlignment()->setWrapText(true);
                $filename = 'transfers_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->load->library('datatables');
            $this->datatables
                ->select("{$this->db->dbprefix('transfers')}.date, reference_no, (CASE WHEN {$this->db->dbprefix('transfers')}.status = 'completed' THEN  GROUP_CONCAT({$this->db->dbprefix('purchase_items')}.product_name SEPARATOR '___') ELSE GROUP_CONCAT({$this->db->dbprefix('purchase_items')}.product_name SEPARATOR '___') END) as iname, (CASE WHEN {$this->db->dbprefix('transfers')}.status = 'completed' THEN  GROUP_CONCAT({$this->db->dbprefix('purchase_items')}.quantity SEPARATOR '___') ELSE GROUP_CONCAT({$this->db->dbprefix('purchase_items')}.quantity SEPARATOR '___') END) as pquantity,from_warehouse_name as fname, from_warehouse_code as fcode, to_warehouse_name as tname,to_warehouse_code as tcode, grand_total, {$this->db->dbprefix('transfers')}.status, {$this->db->dbprefix('transfers')}.id as id", FALSE)
                ->from('transfers')
                // ->join('transfer_items', 'transfer_items.transfer_id=transfers.id', 'left')
                ->join('purchase_items', 'purchase_items.transfer_id=transfers.id', 'left')
                ->group_by('transfers.id');
            if ($product) {
                $this->datatables->where(" (({$this->db->dbprefix('purchase_items')}.product_id = {$product}) OR ({$this->db->dbprefix('purchase_items')}.product_id = {$product})) ", NULL, FALSE);
            }

            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('transfers') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }
            $this->datatables->edit_column("fname", "$1 ($2)", "fname, fcode")
                ->edit_column("tname", "$1 ($2)", "tname, tcode")
                ->unset_column('fcode')
                ->unset_column('tcode');
            echo $this->datatables->generate();
        }
    }

    function getAdjustmentReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('view_adjustments', TRUE, 'products');

        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') . " 00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') . " 23:59" : NULL;
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;

        // if ($start_date) {
        //     $start_date = $this->sma->fld($start_date);
        //     $end_date = $this->sma->fld($end_date);
        // }
        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        if ($pdf || $xls) {

            $ai = "( SELECT adjustment_id, product_id, serial_no, GROUP_CONCAT({$this->db->dbprefix('products')}.name SEPARATOR '\n') as item_nane, GROUP_CONCAT(CASE WHEN {$this->db->dbprefix('adjustment_items')}.type  = 'subtraction' THEN (0-{$this->db->dbprefix('adjustment_items')}.quantity) ELSE {$this->db->dbprefix('adjustment_items')}.quantity END SEPARATOR '\n') as pquantity from {$this->db->dbprefix('adjustment_items')} LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id={$this->db->dbprefix('adjustment_items')}.product_id GROUP BY {$this->db->dbprefix('adjustment_items')}.adjustment_id ) FAI";

            $this->db->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference_no, warehouses.name as wh_name, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note, FAI.item_nane as iname, FAI.pquantity, {$this->db->dbprefix('adjustments')}.id as id", FALSE)
                ->from('adjustments')
                ->join($ai, 'FAI.adjustment_id=adjustments.id', 'left')
                ->join('users', 'users.id=adjustments.created_by', 'left')
                ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left');

            if ($user) {
                $this->db->where('adjustments.created_by', $user);
            }
            if ($product) {
                $this->db->where('FAI.product_id', $product);
            }
            if ($serial) {
                $this->db->like('FAI.serial_no', $serial);
            }
            if ($warehouse) {
                $this->db->where('adjustments.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('adjustments.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('adjustments') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {

                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('adjustments_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('warehouse'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('created_by'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('note'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue('G1', lang('product_qty'));

                $row = 2;
                foreach ($data as $data_row) {
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->wh_name);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, $data_row->created_by);
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $this->sma->decode_html($data_row->note));
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $data_row->pquantity);
                    $row++;
                }

                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(40);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(30);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(30);
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('G2:G' . $row)->getAlignment()->setWrapText(true);
                $filename = 'adjustments_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $ai = "( SELECT adjustment_id, product_id, serial_no, GROUP_CONCAT( {$this->db->dbprefix('products')}.name SEPARATOR '___') as item_nane, SUM(CASE WHEN {$this->db->dbprefix('adjustment_items')}.type  = 'subtraction' THEN (0-{$this->db->dbprefix('adjustment_items')}.quantity) ELSE {$this->db->dbprefix('adjustment_items')}.quantity END) as pquantity from {$this->db->dbprefix('adjustment_items')} LEFT JOIN {$this->db->dbprefix('products')} ON {$this->db->dbprefix('products')}.id={$this->db->dbprefix('adjustment_items')}.product_id ";
            if ($product || $serial) {
                $ai .= " WHERE ";
            }
            if ($product) {
                $ai .= " {$this->db->dbprefix('adjustment_items')}.product_id = {$product} ";
            }
            if ($product && $serial) {
                $ai .= " AND ";
            }
            if ($serial) {
                $ai .= " {$this->db->dbprefix('adjustment_items')}.serial_no LIKe '%{$serial}%' ";
            }
            $ai .= " GROUP BY {$this->db->dbprefix('adjustment_items')}.adjustment_id ) FAI";
            $this->load->library('datatables');
            $this->datatables
                ->select("DATE_FORMAT(date, '%Y-%m-%d %T') as date, reference_no, warehouses.name as wh_name, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note, FAI.item_nane as iname, FAI.pquantity, {$this->db->dbprefix('adjustments')}.id as id", FALSE)
                ->from('adjustments')
                ->join($ai, 'FAI.adjustment_id=adjustments.id', 'left')
                ->join('users', 'users.id=adjustments.created_by', 'left')
                ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left');

            if ($user) {
                $this->datatables->where('adjustments.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FAI.product_id', $product);
            }
            if ($serial) {
                $this->datatables->like('FAI.serial_no', $serial);
            }
            if ($warehouse) {
                $this->datatables->where('adjustments.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('adjustments.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('adjustments') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }

    function update_monthly_cost()
    {

        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');
        $this->form_validation->set_rules('month', lang("month"), 'required');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/update_monthly_cost");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('code', 'unit_cost');

                $final = array();
                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if (!$final[] = array_combine($keys, $value)) { //VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key + 1)));
                        admin_redirect("products/update_monthly_cost");
                        break;
                    }
                }

                $month = $this->input->post('month');
                $products = $this->products_model->getAllProductsWithCostingByMonth($month);
                // $this->sma->print_arrays($products);
                $pr_arr = [];
                foreach ($products as $product) {
                    $pr_arr[$product->code] = $product->purchase_unit_cost;
                }

                $items = [];

                $rw = 2;
                $items = array();

                $invalid_codes_txt = '';
                $invalid_codes = false;

                foreach ($final as $csv_pr) {
                    $product = $this->products_model->getProductByCode($csv_pr['code']);
                    if ($product) {
                        unset($pr_arr[$csv_pr['code']]);
                        $item = array(
                            'product_id' => $product->id,
                            'M' . $month => $csv_pr['unit_cost'],
                            'year' => date('Y'),
                        );

                        $items[] = $item;
                    } else {
                        $invalid_codes_txt .= $csv_pr['code'] . ', ';
                        $invalid_codes = true;
                    }
                }

                $invalid_codes_txt = trim($invalid_codes_txt, ', ');
            }

            if ($invalid_codes) { //SI HAY CÓDIGOS INVÁLIDOS

                $can_save = false;
                $this->session->set_flashdata('error', lang("invalid_codes_warning"));
            } else if (count($pr_arr) > 0) { // SI HAY PRODUCTOS SIN ACTUALIZAR

                $products_with_costing = '';
                $products_without_costing = '';

                $can_save = true;

                foreach ($pr_arr as $product => $cost) {
                    if ($cost != NULL) {
                        if ($can_save) {
                            $can_save = false;
                            $this->session->set_flashdata('error', lang("product_with_costing_is_not_updating"));
                        }
                        $products_with_costing .= $product . ', ';
                    } else {
                        $products_without_costing .= $product . ', ';
                    }
                }

                $products_with_costing = trim($products_with_costing, ', ');
                $products_without_costing = trim($products_without_costing, ', ');
            }
        }

        if ($this->form_validation->run() == true && $can_save == true && $this->products_model->updateProductsCost($items, $month)) {

            $this->session->set_flashdata('message', lang("products_costs_updated"));
            admin_redirect("products/update_monthly_cost");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            if (isset($can_save) && $can_save == false) {
                if (isset($products_with_costing)) {
                    $this->data['products_with_costing'] = $products_with_costing;
                }

                if (isset($products_without_costing)) {
                    $this->data['products_without_costing'] = $products_without_costing;
                }

                if (isset($invalid_codes_txt) && $invalid_codes_txt != '') {
                    $this->data['invalid_codes_txt'] = $invalid_codes_txt;
                }
            }

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_productupdate_monthly_cost')));
            $meta = array('page_title' => lang('update_monthly_cost'), 'bc' => $bc);
            $this->page_construct('products/update_monthly_cost', $meta, $this->data);
        }
    }

    public function syncProductCostingByMonth()
    {
        if ($this->products_model->syncProductCostingByMonth()) {
            $this->session->set_flashdata('message', lang("products_costs_synchronized_"));
            admin_redirect("products/update_monthly_cost");
        }
    }

    public function validate_add_variantes($product_id)
    {
        if ($this->products_model->productHasMovements($product_id, false)) {
            echo false;
        } else {
            echo true;
        }
    }

    public function por_suggestions()
    {
        $term = $this->input->get('term', true);
        $biller_id = $this->input->get('biller_id', true);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $rows = $this->products_model->getPORSuggestions($sr, $biller_id);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                $options = $this->products_model->getProductOptions($row->id);
                $row->option = $option_id;
                $row->serial = '';
                $unit = $this->products_model->getUnitById($row->unit);
                $composition_products = $this->products_model->getProductComboItems($row->id);
                $row->price = $row->price;
                $row->last_cost = $row->cost;
                if ($composition_products) {
                    $total_cost = 0;
                    foreach ($composition_products as $cproduct) {
                        $total_cost += ($cproduct->price * $cproduct->qty);
                    }
                    $row->cost = $total_cost;
                }
                $c = sha1(uniqid(mt_rand(), true));
                $pr[] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'options' => $options, 'composition_products' => $composition_products, 'unit' => $unit);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function getProductDataByWarehouse($product_id = null, $warehouse_id = null)
    {
        $pwdata = $this->products_model->getProductDataByWarehouse(['product_id' => $product_id, 'warehouse_id' => $warehouse_id]);
        if ($pwdata) {
            echo json_encode($pwdata);
        }
    }

    public function production_orders($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('production_orders')));
        $meta = array('page_title' => lang('production_orders'), 'bc' => $bc);
        $this->page_construct('products/production_orders', $meta, $this->data);
    }

    public function getproductionorders($warehouse_id = NULL)
    {

        $start_date = $this->input->post('start_date') ? $this->sma->fld($this->input->post('start_date'))  : NULL;
        $end_date = $this->input->post('end_date') ? $this->sma->fld($this->input->post('end_date'))  : NULL;

        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_adjustment") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_adjustment/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>";

        $this->load->library('datatables');
        $this->datatables
            ->select("{$this->db->dbprefix('adjustments')}.id as id, date, reference_no, warehouses.name as wh_name, CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by, note, {$this->db->dbprefix('adjustments')}.status, attachment")
            ->from('adjustments')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->group_by("adjustments.id");
        $this->datatables->where('adjustments.type_adjustment', 1);
        if ($warehouse_id) {
            $this->datatables->where('adjustments.warehouse_id', $warehouse_id);
        }
        if ($start_date) {
            $this->datatables->where('adjustments.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('adjustments.date <=', $end_date);
        }
        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                    <div class=\"btn-group text-left\">
                                                        <button type=\"button\" class=\"btn btn-default btn-xs dropdown-toggle\" data-toggle=\"dropdown\">
                                                            Acciones
                                                            <span class=\"caret\"></span>
                                                        </button>
                                                        <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                            <li>
                                                                <a href='" . admin_url('products/print_production_order/$1') . "' class='tip' title=''><i class='fa fa-file-pdf-o'></i> " . lang("print_production_order") . "</a>
                                                            </li>
                                                            <li>
                                                                <a href='" . admin_url('products/edit_production_order/$1') . "' class='tip' title=''><i class='fa fa-edit'></i> " . lang("edit_production_order") . "</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                  </div>", "id");

        echo $this->datatables->generate();
    }

    public function add_production_order()
    {

        $this->sma->checkPermissions();

        $this->form_validation->set_rules('document_type_id', lang("document_type_id"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('companies_id', lang("employee"), 'required');
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }

        if ($this->form_validation->run() == true) {

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }

            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $note = $this->sma->clear_tags($this->input->post('note'));

            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['product_id'][$r];
                $quantity = $_POST['quantity'][$r];
                $adjustment_cost = isset($_POST['total'][$r]) ? $_POST['total'][$r] : 0;
                $cost = isset($_POST['cost'][$r]) && !empty($_POST['cost'][$r]) ? $_POST['cost'][$r] : 0;
                $unit_cost = isset($_POST['unit_cost'][$r]) && !empty($_POST['unit_cost'][$r]) ? $_POST['unit_cost'][$r] : 0;

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => 'addition',
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => NULL,
                    'serial_no' => NULL,
                    'avg_cost' => $unit_cost,
                    'adjustment_cost' => NULL,
                    // 'unit_cost' => $unit_cost,
                );

                $productsNames[$product_id] = $_POST['product_name'][$r];
            }

            $i = isset($_POST['cp_product_id']) ? sizeof($_POST['cp_product_id']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $product_id = $_POST['cp_product_id'][$r];
                $quantity = $_POST['cp_quantity'][$r];
                $parent_id = $_POST['cp_parent_id'][$r];
                $adjustment_cost = isset($_POST['cp_total'][$r]) ? $_POST['cp_total'][$r] : 0;
                $cost = isset($_POST['cp_cost'][$r]) && !empty($_POST['cp_cost'][$r]) ? $_POST['cp_cost'][$r] : 0;
                $unit_cost = isset($_POST['cp_unit_cost'][$r]) && !empty($_POST['cp_unit_cost'][$r]) ? $_POST['cp_unit_cost'][$r] : 0;

                $products[] = array(
                    'product_id' => $product_id,
                    'type' => 'subtraction',
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => NULL,
                    'serial_no' => NULL,
                    'avg_cost' => $unit_cost,
                    'adjustment_cost' => NULL,
                    'combo_item_parent_id' => $parent_id,
                    // 'unit_cost' => $unit_cost,
                );

                $productsNames[$product_id] = $_POST['cp_product_name'][$r];
            }

            // $this->sma->print_arrays($productsNames);

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }


            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $cost_center = NULL;
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $cost_center = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $cost_center = $this->input->post('cost_center_id');
            }

            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'count_id' => $this->input->post('count_id') ? $this->input->post('count_id') : NULL,
                'companies_id' => $this->input->post('companies_id'),
                'status' => $this->input->post('por_status'),
                'biller_id' => $biller_id,
                'type_adjustment' => 1,
                'document_type_id' => $document_type_id,
                'cost_center_id'  => $cost_center,
            );
        }
        if ($this->form_validation->run() == true && $this->products_model->addAdjustment($data, $products, $productsNames)) {
            foreach ($products as $item) {
                if (isset($item['unit_cost'])) {
                    $this->purchases_model->updateAVCO(array('product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'quantity' => $item['quantity'], 'cost' => $item['unit_cost']));
                }
            }
            $this->session->set_userdata('remove_porls', 1);
            $this->session->set_flashdata('message', lang("production_order_registered"));
            admin_redirect('products/production_orders');
        } else {

            $this->data['companies'] = $this->site->getAllCompaniesWithState('employee');
            $this->data['adjustment_items'] =  FALSE;
            $this->data['warehouse_id'] = FALSE;
            $this->data['count_id'] = FALSE;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();

            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_production_order')));
            $meta = array('page_title' => lang('add_production_order'), 'bc' => $bc);
            $this->page_construct('products/add_production_order', $meta, $this->data);
        }
    }

    public function edit_production_order($id = NULL)
    {
        $inv = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$inv) {
            $this->session->set_flashdata('error', lang("invalid_production_order"));
            admin_redirect('products/production_orders');
        }
        if ($inv->status == 'completed') {
            $this->session->set_flashdata('error', lang("cant_edit_production_order_completed"));
            admin_redirect('products/production_orders');
        }
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('document_type_id', lang("document_type_id"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('companies_id', lang("employee"), 'required');
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['item_id'][$r];
                $product_id = $_POST['product_id'][$r];
                $quantity = $_POST['quantity'][$r];
                $adjustment_cost = isset($_POST['total'][$r]) ? $_POST['total'][$r] : 0;
                $cost = isset($_POST['cost'][$r]) && !empty($_POST['cost'][$r]) ? $_POST['cost'][$r] : 0;
                $unit_cost = isset($_POST['unit_cost'][$r]) && !empty($_POST['unit_cost'][$r]) ? $_POST['unit_cost'][$r] : 0;
                $products[] = array(
                    'item_id' => $item_id,
                    'product_id' => $product_id,
                    'type' => 'addition',
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => NULL,
                    'serial_no' => NULL,
                    'avg_cost' => $unit_cost,
                    'adjustment_cost' => NULL,
                    // 'unit_cost' => $unit_cost,
                );
                $productsNames[$product_id] = $_POST['product_name'][$r];
            }

            $i = isset($_POST['cp_product_id']) ? sizeof($_POST['cp_product_id']) : 0;

            for ($r = 0; $r < $i; $r++) {

                $item_id = $_POST['cp_item_id'][$r];
                $product_id = $_POST['cp_product_id'][$r];
                $quantity = $_POST['cp_quantity'][$r];
                $combo_item_parent_id = $_POST['cp_parent_id'][$r];
                $adjustment_cost = isset($_POST['cp_total'][$r]) ? $_POST['cp_total'][$r] : 0;
                $cost = isset($_POST['cp_cost'][$r]) && !empty($_POST['cp_cost'][$r]) ? $_POST['cp_cost'][$r] : 0;
                $unit_cost = isset($_POST['cp_unit_cost'][$r]) && !empty($_POST['cp_unit_cost'][$r]) ? $_POST['cp_unit_cost'][$r] : 0;

                $products[] = array(
                    'item_id' => $item_id,
                    'product_id' => $product_id,
                    'type' => 'subtraction',
                    'quantity' => $quantity,
                    'warehouse_id' => $warehouse_id,
                    'option_id' => NULL,
                    'serial_no' => NULL,
                    'avg_cost' => $unit_cost,
                    'adjustment_cost' => NULL,
                    'combo_item_parent_id' => $combo_item_parent_id,
                    // 'unit_cost' => $unit_cost,
                );

                $productsNames[$product_id] = $_POST['cp_product_name'][$r];
            }

            // $this->sma->print_arrays($productsNames);

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            } else {
                krsort($products);
            }


            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $cost_center = NULL;
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $cost_center = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $cost_center = $this->input->post('cost_center_id');
            }

            $data = array(
                'date' => $date,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'updated_by' => $this->session->userdata('user_id'),
                'count_id' => $this->input->post('count_id') ? $this->input->post('count_id') : NULL,
                'companies_id' => $this->input->post('companies_id'),
                'status' => $this->input->post('por_status'),
                'biller_id' => $biller_id,
                'type_adjustment' => 1,
                'document_type_id' => $document_type_id,
                'cost_center_id'  => $cost_center,
                'updated_at' => date('Y-m-d H:i:s'),
            );
        }
        if ($this->form_validation->run() == true && $this->products_model->updateAdjustment($id, $data, $products, $productsNames)) {
            foreach ($products as $item) {
                if (isset($item['unit_cost'])) {
                    $this->purchases_model->updateAVCO(array('product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'quantity' => $item['quantity'], 'cost' => $item['unit_cost']));
                }
            }
            $this->session->set_userdata('remove_porls', 1);
            $this->session->set_flashdata('message', lang("production_order_updated"));
            admin_redirect('products/production_orders');
        } else {
            $rows = $this->products_model->getAdjustmentItems($id);
            if ($rows) {
                $combo_items = [];
                foreach ($rows as $row) {
                    $row->id = $row->product_id;
                    if ($row->type == 'subtraction' && (!isset($row->combo_item_parent_id) || empty($row->combo_item_parent_id))) {
                        $this->session->set_flashdata('error', lang("cant_edit_production_order_invalid_field"));
                        admin_redirect('products/production_orders');
                    }
                    if ($row->combo_item_parent_id) {
                        $row->parent_id = $row->combo_item_parent_id;
                        $combo_items[$row->combo_item_parent_id][] = $row;
                        $citem_data = $this->db->get_where('combo_items', ['item_code'=>$row->product_code, 'product_id'=>$row->combo_item_parent_id])->row();
                        $row->qty = $citem_data->quantity;
                        $row->quantity = $citem_data->quantity;
                        continue;
                    }
                    $composition_products = NULL;
                    if (isset($combo_items[$row->product_id])) {
                        $composition_products = $combo_items[$row->product_id];
                    }
                    $row->qty = $row->quantity;
                    $options = $this->products_model->getProductOptions($row->id);
                    $row->option = $row->option_id;
                    $row->serial = '';
                    $unit = $this->products_model->getUnitById($row->unit);
                    $row->price = $row->price;
                    $row->last_cost = $row->cost;
                    // $this->sma->print_arrays($composition_products);
                    if ($composition_products) {
                        $total_cost = 0;
                        foreach ($composition_products as $cproduct) {
                            $total_cost += ($cproduct->price * $cproduct->quantity);
                        }
                        $row->cost = $total_cost;
                    }
                    $c = sha1(uniqid(mt_rand(), true));
                    $pr[$c] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'options' => $options, 'composition_products' => $composition_products, 'unit' => $unit);
                }
                $this->data['rows'] = $pr;
            } else if (!$rows) {
                $this->session->set_flashdata('error', lang("invalid_production_order"));
                admin_redirect('products/production_orders');
            }


            $this->data['companies'] = $this->site->getAllCompaniesWithState('employee');
            $this->data['adjustment_items'] =  FALSE;
            $this->data['warehouse_id'] = FALSE;
            $this->data['count_id'] = FALSE;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['inv'] = $inv;
            $this->data['id'] = $id;

            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('edit_production_order')));
            $meta = array('page_title' => lang('edit_production_order'), 'bc' => $bc);
            $this->page_construct('products/edit_production_order', $meta, $this->data);
        }
    }

    public function view_production_order($id)
    {
        $this->sma->checkPermissions('adjustments', TRUE);

        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }

        $this->data['inv'] = $adjustment;
        $this->data['rows'] = $this->products_model->getAdjustmentItems($id);
        $this->data['biller'] = $this->site->getCompanyByID($adjustment->biller_id);
        $this->data['created_by'] = $this->site->getUser($adjustment->created_by);
        $this->data['updated_by'] = $this->site->getUser($adjustment->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment->warehouse_id);
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($adjustment->cost_center_id);
        }
        $this->load_view($this->theme . 'products/view_production_order', $this->data);
    }

    public function print_production_order($id)
    {
        $this->sma->checkPermissions('adjustments', TRUE);
        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $adjustment;
        $this->data['rows'] = $this->products_model->getAdjustmentItems($id);
        // $this->sma->print_arrays($this->data['rows']);
        $this->data['created_by'] = $this->site->getUser($adjustment->created_by);
        $this->data['updated_by'] = $this->site->getUser($adjustment->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment->warehouse_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $adjustment->biller_id);
        $this->data['employee'] = $this->site->getCompanyByID($adjustment->companies_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($adjustment->document_type_id);
        $this->data['invoice_format'] = $this->site->getInvoiceFormatById($this->data['document_type']->module_invoice_format_id);
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($adjustment->cost_center_id);
        }
        $this->load_view($this->theme . 'products/print_production_order', $this->data);
    }

    public function add_unit_selected($id, $num = NULL, $tax_method = NULL, $sale_consumption_tax = NULL)
    {
        $unit_price = $this->products_model->getUnitById($id);
        if ($unit_price) {
            $parent_unit_price = $this->products_model->getUnitById($unit_price->base_unit);
            if ($this->Settings->prioridad_precios_producto == 10) {
                $price_group_data = $this->site->getPriceGroupByID($unit_price->price_group_id);
                $html_input = "<td>" . ($price_group_data ? $price_group_data->name : lang('undefined')) . "</td>";
            } else {
                $html_input = "<td><input type='text' class='only_number form-control unit_price' name='unit_price[" . ($num ? $num : "") . "]' required>";
                if ($tax_method == 0) {
                    $html_input .= "<input type='hidden' data-operationvalue='" . $unit_price->operation_value . "' data-operator='" . $unit_price->operator . "' class='only_number form-control unit_ipoconsumo' name='unit_ipoconsumo[" . ($num ? $num : "") . "]' value='" . $sale_consumption_tax . "' required>";
                }

                $html_input .= "</td>";
            }

            if ($this->Settings->prioridad_precios_producto == 11) {
                $price_groups = $this->site->getAllPriceGroups();
                $html = '';
                foreach ($price_groups as $pg) {
                    $html .= "<tr class='row_id_" . $unit_price->id . " row_unit'>
                        <td>
                            <input type='radio' name='product_unit_id' value='" . $unit_price->id . "' " . ($unit_price->operation_value > 1 ? "disabled" : "") . " required>
                        </td>
                        <td>
                            <input type='radio' name='multiple_default_sale_unit' value='" . $unit_price->id . "'>
                        </td>
                        <td>
                            <input type='radio' name='multiple_default_purchase_unit' value='" . $unit_price->id . "'>
                        </td>
                        <td>
                            <input type='hidden' class='m_unit_id' name='unit_id[" . ($num ? $num : "") . "]' value='" . $unit_price->id . "'>
                            <input type='hidden' name='unit_operation_value[" . ($num ? $num : "") . "]' value='" . $unit_price->operation_value . "'>
                            " . $unit_price->name . "
                        </td>
                        <td style='display:none;'>" . ($parent_unit_price ? $parent_unit_price->name : '') . "</td>
                        <td class='text-right'>" . $this->sma->formatQuantity($unit_price->operation_value > 0 ? $unit_price->operation_value : 1) . "</td>
                        <td>" . $pg->name . "</td>
                        <td><input type='text' class='only_number form-control' name='hybrid_price[" . ($id) . "][" . ($pg->id) . "]' required></td>
                        <td><span class='fa fa-times' id='delete_unit' data-rowid='" . $unit_price->id . "'></span></td>
                    </tr>";
                }
            } else {
                $html = "<tr class='row_id_" . $unit_price->id . "  row_unit'>
                            <td>
                                <input type='radio' name='product_unit_id' value='" . $unit_price->id . "' " . ($unit_price->operation_value > 1 ? "disabled" : "") . " required>
                            </td>
                            <td>
                                <input type='radio' name='multiple_default_sale_unit' value='" . $unit_price->id . "'>
                            </td>
                            <td>
                                <input type='radio' name='multiple_default_purchase_unit' value='" . $unit_price->id . "'>
                            </td>
                            <td>
                                <input type='hidden' name='unit_id[" . ($num ? $num : "") . "]' value='" . $unit_price->id . "'>
                                <input type='hidden' name='unit_operation_value[" . ($num ? $num : "") . "]' value='" . $unit_price->operation_value . "'>
                                <input type='hidden' name='unit_price_group[" . ($num ? $num : "") . "]' value='" . $unit_price->price_group_id . "'>
                                " . $unit_price->name . "
                            </td>
                            <td>" . ($parent_unit_price ? $parent_unit_price->name : '') . "</td>
                            <td>" . $this->sma->formatQuantity($unit_price->operation_value > 0 ? $unit_price->operation_value : 1) . "</td>".
                            ($this->Settings->prioridad_precios_producto == 7 ? "<td>
                                                            <input type='number' name='margin_update_price[" . ($num ? $num : "") . "]' class='form-control' value=''>
                                                        </td>" : "").
                            $html_input . "
                            <td>
                                <input type='checkbox' name='unit_status[" . ($num ? $num : "") . "]'  class='form-control' checked>
                            </td>
                            <td><span class='fa fa-times' id='delete_unit' data-rowid='" . $unit_price->id . "'></span></td>
                        </tr>";
            }

            echo $html;
        }
    }

    public function getKardexReport($xls = false)
    {

        if ($this->input->get_post('product')) {
            $product = $this->input->get_post('product');
        } else {
            $product = NULL;
        }

        if ($this->input->get_post('start_date')) {
            $start_date = $this->input->get_post('start_date') . " 00:00";
        } else {
            $start_date = NULL;
        }

        if ($this->input->get_post('end_date')) {
            $end_date = $this->input->get_post('end_date') . " 23:59";
        } else {
            $end_date = NULL;
        }

        if ($this->input->get_post('warehouse_id')) {
            $warehouse_id = $this->input->get_post('warehouse_id');
        } else {
            $warehouse_id = NULL;
        }

        if ($this->input->get_post('option_id')) {
            $option_id = $this->input->get_post('option_id');
        } else {
            $option_id = NULL;
        }

        $companies = $this->site->getCompanies();
        $si_balance = $this->products_model->get_kardex_report_SI($product, $warehouse_id, $start_date, $end_date, $option_id);
        $result = $this->products_model->get_kardex_report($product, $warehouse_id, $start_date, $end_date, $option_id);
        $variantes = $this->products_model->getProductOptions($product);

        if (!$xls) {
            $html = '';
            if ($result->num_rows() > 0) {
                $balance = $si_balance;
                foreach (($result->result()) as $row) {
                    if ($row->quantity_in > 0) {
                        $balance += $row->quantity_in;
                    } else if ($row->quantity_out > 0) {
                        $balance -= $row->quantity_out;
                    }
                    // exit(json_encode($row->nameVarian));
                    $html .= '<tr>
                                <td>' . $row->registration_date . '</td>
                                <td>' . $row->date . '</td>
                                <td>' . $row->reference_no . '</td>
                                <td>' . (isset($companies[$row->company]) ? $companies[$row->company]->company : '-') . '</td>
                                <td>' . $this->sma->formatMoney($row->unit_cost) . '</td>
                                <td>' . $this->sma->formatMoney($row->avg_cost) . '</td>
                                <td>' . (isset($row->warehouse) ? $row->warehouse : '') . '</td>';
                    if ($variantes && count($variantes) > 0) {
                        $html .= '<td class="text-center">' . (isset($row->nameVarian) ? trim($row->nameVarian) : '') . '</td>';
                    }
                    $html .= '
                                <td class="text-center" >' . $this->sma->formatQuantity($row->quantity_in) . '</td>
                                <td class="text-center" >' . $this->sma->formatQuantity($row->quantity_out) . '</td>
                                <td class="text-center" >' . $this->sma->formatQuantity($this->sma->formatDecimal($balance)) . '</td>
                            </tr>';
                }
            }
            $data = [
                'balance' => $si_balance,
                'html' => $html,
            ];
            echo json_encode($data);
        } else {
            if ($result->num_rows() > 0) {
                $balance = $si_balance;
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('kardex_report'));
                $this->excel->getActiveSheet()->SetCellValue('A1', lang('real_date'));
                $this->excel->getActiveSheet()->SetCellValue('B1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue('C1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue('D1', lang('company'));
                $this->excel->getActiveSheet()->SetCellValue('E1', lang('cost') . "/" . lang('avg_cost'));
                $this->excel->getActiveSheet()->SetCellValue('F1', lang('warehouse'));
                $letter = "F";
                if ($variantes && count($variantes) > 0) {
                    $this->excel->getActiveSheet()->SetCellValue((++$letter)."1", lang('variant'));
                }
                $this->excel->getActiveSheet()->SetCellValue((++$letter)."1", lang('quantity_in'));
                $this->excel->getActiveSheet()->SetCellValue((++$letter)."1", lang('quantity_out'));
                $this->excel->getActiveSheet()->SetCellValue((++$letter)."1", lang('balance'));
                $row = 2;
                foreach (($result->result()) as $data_row) {
                    if ($data_row->quantity_in > 0) {
                        $balance += $data_row->quantity_in;
                    } else if ($data_row->quantity_out > 0) {
                        $balance -= $data_row->quantity_out;
                    }
                    $this->excel->getActiveSheet()->SetCellValue('A' . $row, $data_row->registration_date);
                    $this->excel->getActiveSheet()->SetCellValue('B' . $row, $data_row->date);
                    $this->excel->getActiveSheet()->SetCellValue('C' . $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue('D' . $row, (isset($companies[$data_row->company]) ? $companies[$data_row->company]->company : '-'));
                    $this->excel->getActiveSheet()->SetCellValue('E' . $row, $data_row->unit_cost);
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $data_row->warehouse);
                    $letter = "F";
                    if ($variantes && count($variantes) > 0) {
                        $this->excel->getActiveSheet()->SetCellValue((++$letter). $row, ($data_row->nameVarian));
                    }
                    $this->excel->getActiveSheet()->SetCellValue((++$letter). $row, ($data_row->quantity_in));
                    $this->excel->getActiveSheet()->SetCellValue((++$letter). $row, ($data_row->quantity_out));
                    $this->excel->getActiveSheet()->SetCellValue((++$letter). $row, ($balance));
                    $row++;
                }
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(20);
                $this->excel->getActiveSheet()->getStyle('E1:E' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                if ($variantes && count($variantes) > 0) {
                    $this->excel->getActiveSheet()->getStyle('H1:H' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('I1:I' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('J1:J' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                }else{
                    $this->excel->getActiveSheet()->getStyle('G1:G' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('H1:H' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                    $this->excel->getActiveSheet()->getStyle('I1:I' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                }
                $filename = 'kardex_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
        }
    }

    function delete_product_unit($product_id, $punit_id)
    {
        $response = ['status' => 0, 'message' => lang('cant_delete_product_unit')];
        if ($this->products_model->validate_product_unit_movements($product_id, $punit_id)) {

            if ($this->products_model->delete_product_unit($product_id, $punit_id)) {
                $udata = $this->site->getUnitByID($punit_id);
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 3,
                    'table_name' => 'products',
                    'record_id' => $product_id,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => sprintf(lang('user_fields_changed'), $this->session->first_name . " " . $this->session->last_name, lang($this->m), $product_id).'Eliminó la unidad '.$udata->name,
                ]);

                $response = ['status' => 1, 'message' => lang('product_unit_deleted')];
            }
        }
        exit(json_encode($response));
    }

    public function add_product_transformation()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('document_type_id', lang("document_type_id"), 'required');
        $this->form_validation->set_rules('adjustment_document_type_id', lang("adjustment_reference_no"), 'required');
        $this->form_validation->set_rules('ptrwarehouseorigin', lang("warehouse"), 'required');
        $this->form_validation->set_rules('ptrwarehousedestination', lang("warehouse"), 'required');
        $this->form_validation->set_rules('companies_id', lang("employee"), 'required');
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
            $this->form_validation->set_rules('destination_cost_center_id', lang("destination_cost_center"), 'required');
        }
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld($this->input->post('date'));
            } else {
                $date = date('Y-m-d H:s:i');
            }
            $ptrwarehouseorigin = $this->input->post('ptrwarehouseorigin');
            $ptrwarehousedestination = $this->input->post('ptrwarehousedestination');
            $biller_id = $this->input->post('biller');
            $destination_biller_id = $this->input->post('destination_biller');
            $document_type_id = $this->input->post('document_type_id');
            $adjustment_document_type_id = $this->input->post('adjustment_document_type_id');
            $referenceBiller = $this->site->getReferenceBiller($biller_id, $document_type_id);
            if ($referenceBiller) {
                $reference_no = $referenceBiller;
            } else {
                $reference_no = $this->site->getReference('qa');
            }
            $note = $this->sma->clear_tags($this->input->post('note'));
            $productsNames = [];
            $i = isset($_POST['product_id_origin']) ? sizeof($_POST['product_id_origin']) : 0;
            $products_origin = [];
            for ($r = 0; $r < $i; $r++) {
                $product_id = $_POST['product_id_origin'][$r];
                $type = 'subtraction';
                $quantity = $_POST['quantity_origin'][$r];
                $adjustment_cost = isset($_POST['total'][$r]) ? $_POST['total'][$r] : 0;
                $cost = isset($_POST['cost_origin'][$r]) && !empty($_POST['cost_origin'][$r]) ? $_POST['cost_origin'][$r] : 0;
                $unit_cost = isset($_POST['unit_cost_origin'][$r]) && !empty($_POST['unit_cost_origin'][$r]) ? $_POST['unit_cost_origin'][$r] : 0;
                $option_id = isset($_POST['product_serial_option_id_origin'][$r]) && !empty($_POST['product_serial_option_id_origin'][$r]) ? $_POST['product_serial_option_id_origin'][$r] : NULL;
                $serial_no = isset($_POST['product_serial_no_origin'][$r]) && !empty($_POST['product_serial_no_origin'][$r]) ? $_POST['product_serial_no_origin'][$r] : NULL;
                $products_origin[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $ptrwarehouseorigin,
                    'option_id' => $option_id,
                    'serial_no' => $serial_no,
                    'avg_cost' => $unit_cost,
                    'adjustment_cost' => $unit_cost,
                );
                $productsNames[$product_id] = $_POST['product_name_origin'][$r];
            }
            $i = isset($_POST['product_id_destination']) ? sizeof($_POST['product_id_destination']) : 0;
            $products_destination = [];
            for ($r = 0; $r < $i; $r++) {
                $product_id = $_POST['product_id_destination'][$r];
                $type = 'addition';
                $quantity = $_POST['quantity_destination'][$r];
                $adjustment_cost = isset($_POST['total'][$r]) ? $_POST['total'][$r] : 0;
                $cost = isset($_POST['cost_destination'][$r]) && !empty($_POST['cost_destination'][$r]) ? $_POST['cost_destination'][$r] : 0;
                $unit_cost = isset($_POST['unit_cost_destination'][$r]) && !empty($_POST['unit_cost_destination'][$r]) ? $_POST['unit_cost_destination'][$r] : 0;
                $option_id = isset($_POST['product_serial_option_id_destination'][$r]) && !empty($_POST['product_serial_option_id_destination'][$r]) ? $_POST['product_serial_option_id_destination'][$r] : NULL;
                $serial_no = isset($_POST['product_serial_no_destination'][$r]) && !empty($_POST['product_serial_no_destination'][$r]) ? $_POST['product_serial_no_destination'][$r] : NULL;
                $products_destination[] = array(
                    'product_id' => $product_id,
                    'type' => $type,
                    'quantity' => $quantity,
                    'warehouse_id' => $ptrwarehousedestination,
                    'option_id' => $option_id,
                    'serial_no' => $serial_no,
                    'avg_cost' => $unit_cost,
                    'adjustment_cost' => $unit_cost,
                );
                $productsNames[$product_id] = $_POST['product_name_destination'][$r];
            }
            if (empty($products_origin)) {
                $this->form_validation->set_rules('product', lang("products_origin"), 'required');
            }
            if (empty($products_destination)) {
                $this->form_validation->set_rules('product', lang("products_destination"), 'required');
            }
            $data = array(
                'date' => $this->site->movement_setted_datetime($date),
                'reference_no' => $reference_no,
                'warehouse_id' => $ptrwarehouseorigin,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'count_id' => $this->input->post('count_id') ? $this->input->post('count_id') : NULL,
                'companies_id' => $this->input->post('companies_id'),
                'biller_id' => $biller_id,
                'type_adjustment' => 2,
                'origin_document_type_id' => $document_type_id,
                'document_type_id' => $adjustment_document_type_id,
                'destination_biller_id'  => $destination_biller_id,
            );
            $data_2 = false;
            if ($ptrwarehouseorigin != $ptrwarehousedestination) {
                $data_2 = array(
                    'date' => $this->site->movement_setted_datetime($date),
                    'reference_no' => $reference_no,
                    'warehouse_id' => $ptrwarehousedestination,
                    'note' => $note,
                    'created_by' => $this->session->userdata('user_id'),
                    'count_id' => $this->input->post('count_id') ? $this->input->post('count_id') : NULL,
                    'companies_id' => $this->input->post('companies_id'),
                    'biller_id' => $biller_id,
                    'type_adjustment' => 2,
                    'origin_document_type_id' => $document_type_id,
                    'document_type_id' => $adjustment_document_type_id,
                );
            }
            if ($this->Settings->cost_center_selection == 0) {
                $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
                $destination_biller_cost_center = $this->site->getBillerCostCenter($destination_biller_id);
                if ($biller_cost_center && $destination_biller_cost_center) {
                    $data['cost_center_id'] = $biller_cost_center->id;
                    $data['destination_cost_center_id'] = $destination_biller_cost_center->id;
                    if ($data_2 != false) {
                        $data_2['cost_center_id'] = $biller_cost_center->id;
                        $data_2['destination_cost_center_id'] = $destination_biller_cost_center->id;
                    }
                } else {
                    $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                    admin_redirect('products/add_product_transformation');
                }
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
                $data['destination_cost_center_id'] = $this->input->post('destination_cost_center_id');
                if ($data_2 != false) {
                    $data_2['cost_center_id'] = $this->input->post('cost_center_id');
                    $data_2['destination_cost_center_id'] = $this->input->post('destination_cost_center_id');
                }
            }
            // $this->sma->print_arrays($data, $data_2, $products_origin, $products_destination, $productsNames);
        }
        if ($this->form_validation->run() == true && $this->products_model->addProductTransformation($data, $data_2, $products_origin, $products_destination, $productsNames)) {
            $this->session->set_userdata('remove_ptrls', 1);
            $this->session->set_flashdata('message', lang("quantity_adjusted"));
            admin_redirect('products/product_transformations');
        } else {
            if ($this->input->post('date')) {
            }
            $this->data['companies'] = $this->site->getAllCompaniesWithState('employee');
            $this->data['adjustment_items'] =  FALSE;
            $this->data['warehouse_id'] = FALSE;
            $this->data['count_id'] = FALSE;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_product_transformation')));
            $meta = array('page_title' => lang('add_product_transformation'), 'bc' => $bc);
            $this->page_construct('products/add_product_transformation', $meta, $this->data);
        }
    }

    public function ptr_suggestions()
    {
        $term = $this->input->get('term', true);
        $warehouse_id = $this->input->get('warehouse_id', true);
        $biller_id = $this->input->get('biller_id', true);
        $destination_biller_id = $this->input->get('destination_biller_id', true);
        $search_type = $this->input->get('search_type', true);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $rows = $this->products_model->getPTRSuggestions($sr, $warehouse_id, $biller_id, $destination_biller_id, $search_type);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                $options = $this->products_model->getProductOptions($row->id);
                $row->option = $option_id;
                $row->serial = '';
                $unit = $this->products_model->getUnitById($row->unit);
                $composition_products = $this->products_model->getProductComboItems($row->id);
                $row->price = $row->price;
                $row->last_cost = $row->cost;
                if ($composition_products) {
                    $total_cost = 0;
                    foreach ($composition_products as $cproduct) {
                        $total_cost += ($cproduct->price * $cproduct->qty);
                    }
                    $row->cost = $total_cost;
                }
                $c = sha1(uniqid(mt_rand(), true));
                $pr[] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'options' => $options, 'composition_products' => $composition_products, 'unit' => $unit);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function getproducttransformations($warehouse_id = NULL)
    {
        // $this->sma->checkPermissions('adjustments');
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') . " 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') . " 23:59:00" : NULL;
        $delete_link = "<a href='#' class='tip po' title='<b>" . $this->lang->line("delete_adjustment") . "</b>' data-content=\"<p>"
            . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('products/delete_adjustment/$1') . "'>"
            . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i></a>";
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('adjustments')}.id as id,
                        date,
                        origin_reference_no,
                        warehouses.name as wh_name,
                        CONCAT({$this->db->dbprefix('users')}.first_name,' ',{$this->db->dbprefix('users')}.last_name) as created_by,
                        note,
                        attachment
                    ", FALSE)
            ->from('adjustments')
            ->join('warehouses', 'warehouses.id=adjustments.warehouse_id', 'left')
            ->join('users', 'users.id=adjustments.created_by', 'left')
            ->group_by("adjustments.origin_reference_no");
        $this->datatables->where('adjustments.type_adjustment', 2);
        if ($warehouse_id) {
            $this->datatables->where('adjustments.warehouse_id', $warehouse_id);
        }
        if ($start_date) {
            $this->datatables->where('adjustments.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('adjustments.date <=', $end_date);
        }
        $this->datatables->add_column("Actions", "<div class='text-center'></div>", "id");
        echo $this->datatables->generate();
    }

    public function product_transformations($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('product_transformations')));
        $meta = array('page_title' => lang('product_transformations'), 'bc' => $bc);
        $this->page_construct('products/product_transformations', $meta, $this->data);
    }

    public function view_product_transformation($ref)
    {
        // $this->sma->checkPermissions('adjustments', TRUE);
        $adjustment = $this->products_model->getProductTransformationByRef($ref);
        if (!$ref || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $adjustment;
        // $this->sma->print_arrays($adjustment);
        $this->data['rows'] = $this->products_model->getProductTransformationItemsByRef($ref);
        $warehouses = $this->site->getAllWarehouses();
        $wh_data = [];
        foreach ($warehouses as $wh) {
            $wh_data[$wh->id] = $wh;
        }
        $this->data['warehouses'] = $wh_data;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($adjustment[0]->cost_center_id);
        }
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $adjustment[0]->biller_id);
        $this->data['employee'] = $this->site->getAllCompaniesWithState('employee', $adjustment[0]->companies_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($adjustment[0]->origin_document_type_id);
        $this->data['created_by'] = $this->site->getUser($adjustment[0]->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment[0]->warehouse_id);
        $this->load_view($this->theme . 'products/view_product_transformation', $this->data);
    }

    public function view_product_transformation_format($ref)
    {
        // $this->sma->checkPermissions('adjustments', TRUE);
        $adjustment = $this->products_model->getProductTransformationByRef($ref);
        if (!$ref || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $adjustment;
        // $this->sma->print_arrays($adjustment);
        $this->data['rows'] = $this->products_model->getProductTransformationItemsByRef($ref);
        $warehouses = $this->site->getAllWarehouses();
        $wh_data = [];
        foreach ($warehouses as $wh) {
            $wh_data[$wh->id] = $wh;
        }
        $this->data['warehouses'] = $wh_data;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($adjustment[0]->cost_center_id);
        }
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $adjustment[0]->biller_id);
        $this->data['employee'] = $this->site->getAllCompaniesWithState('employee', $adjustment[0]->companies_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($adjustment[0]->origin_document_type_id);
        $this->data['created_by'] = $this->site->getUser($adjustment[0]->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment[0]->warehouse_id);
        $document_type = $this->site->getDocumentTypeById($adjustment[0]->origin_document_type_id);
        $this->data['document_type'] = $document_type;
        $invoice_format = $document_type->module_invoice_format_id ? $this->site->getInvoiceFormatById($document_type->module_invoice_format_id) : FALSE;
        $format_url = 'products/view_ptransformation_pdf';
        if ($invoice_format) {
            $format_url = $invoice_format->format_url;
        }
        // exit(var_dump($invoice_format));
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($adjustment->cost_center_id);
        }
        $this->load_view($this->theme . $format_url, $this->data);
    }
    function preferences_suggestions($product_id = NULL)
    {
        $getCost = TRUE;
        $term = $this->input->get('term', TRUE);
        $preference_category = $this->input->get('preference_category');
        $ps = $this->input->get('ps') ? trim($this->input->get('ps', TRUE), ",") : null;
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $rows = $this->products_model->getProductPreferencesSuggestions($term, $product_id, $ps, $preference_category);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array(
                    'id' => $row->id,
                    'name' => $row->prf_cat_name . " - " . $row->name,
                    'label' => $row->prf_cat_name . " - " . $row->name,
                );
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function contabilizarAjuste($adjustment_id = null, $json = null)
    {
        $this->form_validation->set_rules('adjustment_id', lang("adjustment_id"), 'required');
        if ($this->form_validation->run() == true) {
            $adjustment_id = $this->input->post('adjustment_id');
            if ($this->products_model->recontabilizarAjuste($adjustment_id)) {
                $this->session->set_flashdata('message', lang('adjustment_reaccounted'));
            } else {
                $this->session->set_flashdata('error', lang('adjustment_not_reaccounted'));
            }
            admin_redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $adjustment = $this->products_model->getAdjustmentByID($adjustment_id, true);
            $exists = $this->site->getEntryTypeNumberExisting($adjustment, false, false);
            $this->data['exists'] = $exists;
            $this->data['inv'] = $adjustment;
            $this->load_view($this->theme . 'products/recontabilizar_ajuste', $this->data);
        }
    }

    public function adjustment_zebra_products_print($adjustment_id = null, $download_pdf = false)
    {
        $this->sma->checkPermissions('index', true);

        if ($this->input->get('id')) {
            $adjustment_id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->products_model->getAdjustmentByID($adjustment_id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['rows'] = $this->products_model->getAdjustmentItems($adjustment_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['payments'] = $this->purchases_model->getPaymentsForPurchase($adjustment_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['return_purchase'] = $this->purchases_model->getReturnsInvoicesByPurchaseID($adjustment_id);
        $this->data['return_rows'] = $this->purchases_model->getAllReturnsItems($adjustment_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter(NULL, $inv->reference_no);
        $this->data['document_type'] = $this->site->getDocumentTypeById(NULL, $inv->reference_no);
        $this->data['sma'] = $this->sma;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = $tax->rate;
        }
        $this->data['taxes_details'] = $taxes_details;
        $this->data['signature_root'] = is_file("assets/uploads/signatures/" . $this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : false;
        $this->data['download'] = $download_pdf;
        $this->data['for_email'] = false;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $this->data['biller_logo'] = 2;
        $url = 'purchases/zebra_products_print';
        $this->load_view($this->theme . $url, $this->data);
    }

    public function get_product_serial_variant($product_id, $serial)
    {
        $q = $this->db->get_where('product_variants', ['product_id' => $product_id, 'name' => $serial]);
        if ($q->num_rows() > 0) {
            echo '1';
        } else {
            echo '0';
        }
    }

    public function get_product_serial_variant_quantity($product_id, $serial, $warehouse_id)
    {

        $q = $this->db->select('warehouses_products_variants.*')
            ->join('product_variants', ' product_variants.id = warehouses_products_variants.option_id AND product_variants.name = "' . $serial . '"')
            ->where('warehouses_products_variants.product_id', $product_id)
            ->where('warehouses_products_variants.warehouse_id', $warehouse_id)
            ->where('product_variants.name', $serial)
            ->get('warehouses_products_variants');

        if ($q->num_rows() > 0) {
            echo json_encode($q->row());
        } else {
            echo json_encode([0]);
        }
    }

    public function get_random_id()
    {
        echo sha1(uniqid(mt_rand(), true));
    }

    public function view_adjustment_format($id)
    {
        $this->sma->checkPermissions('adjustments', TRUE);
        $adjustment = $this->products_model->getAdjustmentByID($id);
        if (!$id || !$adjustment) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $adjustment;
        $this->data['rows'] = $this->products_model->getAdjustmentItems($id);
        // $this->sma->print_arrays($this->data['rows']);
        $this->data['created_by'] = $this->site->getUser($adjustment->created_by);
        $this->data['updated_by'] = $this->site->getUser($adjustment->updated_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($adjustment->warehouse_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $adjustment->biller_id);
        $this->data['employee'] = $this->site->getCompanyByID($adjustment->companies_id);
        $document_type = $this->site->getDocumentTypeById($adjustment->document_type_id);
        $this->data['document_type'] = $document_type;
        $invoice_format = $document_type->module_invoice_format_id ? $this->site->getInvoiceFormatById($document_type->module_invoice_format_id) : FALSE;
        $format_url = 'products/view_adjustment_pdf';
        if ($invoice_format) {
            $format_url = $invoice_format->format_url;
        }
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($adjustment->cost_center_id);
        }
        $this->load_view($this->theme . $format_url, $this->data);
    }

    function recosting_selection($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('start_date', lang("start_date"), 'required');
        $this->form_validation->set_rules('end_date', lang("end_date"), 'required');
        $this->form_validation->set_rules('val[]', lang("products"), 'required');
        if ($this->form_validation->run() == true) {
            if ($this->input->post('show_recosting_documentation')) {
                if (count($this->input->post('val')) > 1) {
                    $this->session->set_flashdata('error', 'Selección de productos inválida para los parámetros indicados');
                    admin_redirect('products/recosting_selection');
                }
            }
            $data = [
                'start_date' => $this->input->post('start_date') . " 00:00:00",
                'end_date' => $this->input->post('end_date') . " 23:59:59",
                // 'warehouse_id' => $this->input->post('warehouse'),
                'products_id' => $this->input->post('val'),
                'show_recosting_documentation' => $this->input->post('show_recosting_documentation'),
            ];
            // $this->sma->print_arrays($data);
        }
        if ($this->form_validation->run() == true && $this->products_model->process_product_recosting($data)) {
            $this->session->set_flashdata('message', lang("products_recosting_done"));
            admin_redirect('products/recosting_selection');
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
                $this->data['warehouses'] = $this->site->getAllWarehouses();
                $this->data['warehouse_id'] = $warehouse_id;
                $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : NULL;
            } else {
                $this->data['warehouses'] = NULL;
                $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
                $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : NULL;
            }
            if ($this->input->post("producto")) {
                $this->data["producto"] = $this->input->post("producto");
            }
            if ($this->input->post("descripcion")) {
                $this->data["descripcion"] = $this->input->post("descripcion");
            }
            if ($this->input->post("discontinued")) {
                $this->data["discontinued"] = $this->input->post("discontinued");
            }

            $this->data['supplier'] = $this->input->get('supplier') ? $this->site->getCompanyByID($this->input->get('supplier')) : NULL;
            $this->data['categories'] = $this->site->getAllCategories();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('recosting_selection')));
            $meta = array('page_title' => lang('recosting_selection'), 'bc' => $bc);
            $this->page_construct('products/recosting_selection', $meta, $this->data);
        }
    }

    function getRecostingProducts($warehouse_id = NULL)
    {
        $this->sma->checkPermissions('index', TRUE);
        $category = $this->input->post('category');
        $product_id = $this->input->post('product_id');
        $hide_recosted_products = json_decode($this->input->post('hide_recosted_products'));
        $detail_link = anchor('admin/products/recosting_registers_view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('recosting_registers_view'), 'target="_blank"');
        $action = '<div class="text-center">
                        <div class="btn-group text-left">'
            . '<button type="button" class="btn btn-outline btn-success btn-xs dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>' . $detail_link . '</li>
                            </ul>
                        </div>
                    </div>';
        $this->load->library('datatables');
        $this->datatables->select($this->db->dbprefix('products') . ".id as productid,
            {$this->db->dbprefix('products')}.code as code,
            {$this->db->dbprefix('products')}.reference as reference,
            {$this->db->dbprefix('products')}.name as name,
            {$this->db->dbprefix('products')}.type,
            {$this->db->dbprefix('categories')}.name as cname,
            COALESCE(quantity, 0) as quantity,
            {$this->db->dbprefix('products')}.product_details,
            {$this->db->dbprefix('products')}.discontinued", FALSE)
            ->from('products')
            ->join('categories', 'products.category_id=categories.id', 'left')
            ->join('products_recosting', 'products_recosting.product_id=products.id AND products_recosting.updated_by_csv = 0', 'left');

        if ($category) {
            $this->datatables->where('products.category_id', $category);
        }
        if ($product_id) {
            $this->datatables->where('products.id', $product_id);
        }
        if ($hide_recosted_products) {
            $this->datatables->where('products_recosting.id IS NULL');
        }
        $this->datatables->group_by("products.id");
        $this->datatables->add_column("Actions", $action, "productid, image, code, name");
        echo $this->datatables->generate();
    }

    function update_csv_recosting_registers()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('end_date', lang("end_date"), 'required');
        $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('userfile', $this->lang->line("upload_file"), 'xss_clean');
        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/update_csv_recosting_registers");
                }
                $csv = $this->upload->file_name;
                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('Codigo producto', 'Costo promedio', 'Cantidad');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if (!$final[] = array_combine($keys, $value)) { //VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key + 1)));
                        admin_redirect("products/update_csv_recosting_registers");
                        break;
                    }
                }
                $rw = 2;
                foreach ($final as $csv_pr) {
                    if ($product_details = $this->purchases_model->getProductByCode($csv_pr['Codigo producto'])) {
                        $recosting_row = [
                            'product_id' => $product_details->id,
                            'start_date' => $this->input->post('end_date') . " 00:00:00",
                            'end_date' => $this->input->post('end_date') . " 23:59:59",
                            'warehouse_id' => $this->input->post('warehouse'),
                            'calculated_avg_cost' => $csv_pr['Costo promedio'],
                            'calculated_quantity' => $csv_pr['Cantidad'],
                            'updated_by_csv' => 1,
                        ];
                        if ($this->products_model->validate_recosting_register_exist($recosting_row)) {
                            $recosting[] = $recosting_row;
                        } else {
                            $this->session->set_flashdata('error', 'El producto ' . $product_details->code . ' - ' . $product_details->name . ' ya tiene una bitácora para la fecha y bodega indicada ');
                            admin_redirect("products/update_csv_recosting_registers");
                        }
                    }
                }
            }
        }
        if ($this->form_validation->run() == true && $this->products_model->insert_recosting_register_by_csv($recosting)) {

            $this->session->set_flashdata('message', lang("recosting_registers_inserted"));
            admin_redirect('products/update_csv_recosting_registers');
        } else {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('update_csv_recosting_registers')));
            $meta = array('page_title' => lang('update_csv_recosting_registers'), 'bc' => $bc);
            $this->page_construct('products/update_csv_recosting_registers', $meta, $this->data);
        }
    }

    function recosting_registers_view($product_id)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['product_id'] = $product_id;
        $this->data['product'] = $this->site->getProductByID($product_id);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('recosting_registers_view')));
        $meta = array('page_title' => lang('recosting_registers_view'), 'bc' => $bc);
        $this->page_construct('products/recosting_registers_view', $meta, $this->data);
    }

    function getRecostingProductRegister($product_id)
    {
        $this->sma->checkPermissions('index', TRUE);
        $this->load->library('datatables');
        $this->datatables->select("products_recosting.start_date, products_recosting.end_date, warehouses.name, products_recosting.calculated_avg_cost, products_recosting.calculated_quantity", FALSE)
            ->where('products_recosting.product_id', $product_id)
            ->join('warehouses', 'warehouses.id = products_recosting.warehouse_id')
            ->from('products_recosting')
            ->order_by('products_recosting.end_date DESC');
        echo $this->datatables->generate();
    }

    function update_csv_fix_products_cost()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('start_date', lang("start_date"), 'required');
        $this->form_validation->set_rules('end_date', lang("end_date"), 'required');
        $this->form_validation->set_rules('document_types[]', "Documentos a afectar", 'required');
        $this->form_validation->set_rules('userfile', $this->lang->line("upload_file"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            if (isset($_FILES["userfile"])) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products/update_csv_fix_products_cost");
                }
                $csv = $this->upload->file_name;
                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $keys = array('Codigo producto', 'Costo');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if (!$final[] = array_combine($keys, $value)) { //VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key + 1)));
                        admin_redirect("products/update_csv_fix_products_cost");
                        break;
                    }
                }
                $rw = 2;
                // $this->sma->print_arrays($final);
                $fix_cost = [];
                if (count($final) > 500 || count($final) == 0) {
                    $this->session->set_flashdata('error', 'El número de productos relacionados supera el límite de 100 o el archivo está vacío');
                    admin_redirect("products/update_csv_fix_products_cost");
                }
                foreach ($final as $csv_pr) {
                    if ($product_details = $this->purchases_model->getProductByCode($csv_pr['Codigo producto'])) {
                        $fix_cost_row = [
                            'product' => $product_details,
                            'cost' => $csv_pr['Costo'],
                            'document_types' => $this->input->post('document_types'),
                            'prioritize_last_cost' => $this->input->post('prioritize_last_cost'),
                            'start_date' => $this->input->post('start_date') . " 00:00:00",
                            'end_date' => $this->input->post('end_date') . " 23:59:59",
                        ];
                        $fix_cost[] = $fix_cost_row;
                    } else {
                        $this->session->set_flashdata('error', 'El código ' . $csv_pr['Codigo producto'] . ' no existe');
                        admin_redirect("products/update_csv_fix_products_cost");
                    }
                }
            }
            // $this->sma->print_arrays($fix_cost);
        }
        if ($this->form_validation->run() == true && $this->products_model->fix_products_cost($fix_cost)) {
            $this->session->set_flashdata('message', lang("costs_fixed"));
            admin_redirect('products/update_csv_fix_products_cost');
        } else {
            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['document_types'] = $this->companies_model->getAllDocumentTypes();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('update_csv_fix_products_cost')));
            $meta = array('page_title' => lang('update_csv_fix_products_cost'), 'bc' => $bc);
            $this->page_construct('products/update_csv_fix_products_cost', $meta, $this->data);
        }
    }

    function deactivate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->input->get('id') == 1) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_deleted")));
        }

        if ($this->products_model->inactivateProduct($id, 1)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("customer_deleted")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_deleted_have_sales")));
        }
    }

    function activate($id = NULL)
    {
        $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->input->get('id') == 1) {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_activated")));
        }

        if ($this->products_model->inactivateProduct($id, 0)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("customer_activated")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("customer_x_activated_have_sales")));
        }
    }

    function export_format_excel($page = NULL)
    {
        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle('Products');
        $this->excel->getActiveSheet()->SetCellValue('A1', 'Filto');
        $this->excel->getActiveSheet()->SetCellValue('B1', 'Id');
        $this->excel->getActiveSheet()->SetCellValue('C1', 'Código');
        $this->excel->getActiveSheet()->SetCellValue('D1', 'Nombre Producto');
        $this->excel->getActiveSheet()->SetCellValue('E1', 'Cantidad Total');
        $this->excel->getActiveSheet()->SetCellValue('F1', 'Unidad Mayor');
        $this->excel->getActiveSheet()->SetCellValue('G1', 'Factor_1');
        $this->excel->getActiveSheet()->SetCellValue('H1', 'Total Cajas');
        $this->excel->getActiveSheet()->SetCellValue('I1', 'Conteo Cajas');
        $this->excel->getActiveSheet()->SetCellValue('J1', 'Unidad Menor');
        $this->excel->getActiveSheet()->SetCellValue('K1', 'Factor_2');
        $this->excel->getActiveSheet()->SetCellValue('L1', 'Total Unidades');
        $this->excel->getActiveSheet()->SetCellValue('M1', 'Conteo Unidades');
        $this->excel->getActiveSheet()->SetCellValue('N1', 'Nueva Cantidad Conteo');
        $this->excel->getActiveSheet()->SetCellValue('O1', 'Auditoria Ajuste');
        $this->excel->getActiveSheet()->SetCellValue('P1', 'Diferencia');
        $this->excel->getActiveSheet()->SetCellValue('Q1', 'Tipo');
        $row = 1;
        $productos = $this->products_model->getProductFormat();
        $codigoAnterior = '';
        $iteracion = 0;
        $contador = 0;
        foreach ($productos as $key => $product) {
            $totalMenor = 0;
            $valida = 0;
            $codigoActual = $product['codigo'];
            if ($codigoAnterior == '' || $codigoActual == $codigoAnterior) {
                if ($codigoAnterior == '') {
                    $row++;
                }
                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product['categoria']);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $product['id']);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $product['codigo']);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $product['nombre']);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $product['cantidad']);
                if ($iteracion == 0) {
                    if (is_null($product['unidad']) || $product['unidad'] == "") {
                        $unidad = $this->products_model->getNameUnitMinor($product['id']);
                    } else {
                        $unidad = $product['unidad'];
                    }
                    $factor = $product['factor'] > 0 ? $product['factor'] : 1;
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $unidad);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $factor);
                    $cantidadCajas = $product['cantidad'];
                    $totalMayor = $cantidadCajas / $factor;
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, intval($totalMayor));
                    $totalMenor = $totalMayor - intval($totalMayor);
                    $totalMenor = $totalMenor * 1;
                    $unidad = $this->products_model->getNameUnitMinor($product['id']);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, 1);
                    $this->excel->getActiveSheet()->SetCellValue('L' . $row, $totalMenor);
                    if ($product['unidad'] == 'UNIDADT') {
                        $unidad = $this->products_model->getNameUnitMinor($product['id']);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $factor);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $totalMenor);
                    } else if (is_null($product['unidad'])) {
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $factor);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, intval($totalMayor));
                    }
                    $iteracion = 1;
                    $valida = 1;
                    if (is_null($product['unidad'])) {
                        $iteracion = 0;
                    }
                }
                if ($iteracion == 1 && $valida == 0) {
                    $unidad = $this->products_model->getNameUnitMinor($product['id']);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($product['factor'] == 0) ? 1 : 1);
                    $totalMenor = $totalMayor - intval($totalMayor);
                    $totalMenor = $totalMenor * $factor;
                    $this->excel->getActiveSheet()->SetCellValue('L' . $row, $totalMenor);
                }
                $codigoAnterior = $product['codigo'];
                $contador++;
            } else if ($codigoActual != $codigoAnterior) {
                $iteracion = 0;
                $row++;
                $this->excel->getActiveSheet()->SetCellValue('A' . $row, $product['categoria']);
                $this->excel->getActiveSheet()->SetCellValue('B' . $row, $product['id']);
                $this->excel->getActiveSheet()->SetCellValue('C' . $row, $product['codigo']);
                $this->excel->getActiveSheet()->SetCellValue('D' . $row, $product['nombre']);
                $this->excel->getActiveSheet()->SetCellValue('E' . $row, $product['cantidad']);
                if ($iteracion == 0) {
                    if (is_null($product['unidad']) || $product['unidad'] == "") {
                        $unidad = $this->products_model->getNameUnitMinor($product['id']);
                    } else {
                        $unidad = $product['unidad'];
                    }
                    $factor = $product['factor'] > 0 ? $product['factor'] : 1;
                    $this->excel->getActiveSheet()->SetCellValue('F' . $row, $unidad);
                    $this->excel->getActiveSheet()->SetCellValue('G' . $row, $factor);
                    $cantidadCajas = $product['cantidad'];
                    $totalMayor = $cantidadCajas / $factor;
                    $this->excel->getActiveSheet()->SetCellValue('H' . $row, intval($totalMayor));
                    $totalMenor = $totalMayor - intval($totalMayor);
                    $totalMenor = $totalMenor * $factor;
                    $unidad = $this->products_model->getNameUnitMinor($product['id']);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, 1);
                    $this->excel->getActiveSheet()->SetCellValue('L' . $row, $totalMenor);
                    if ($product['unidad'] == 'UNIDADT') {
                        $unidad = $this->products_model->getNameUnitMinor($product['id']);
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $factor);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, $totalMenor);
                    } else  if (is_null($product['unidad'])) {
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, '');
                        $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                        $this->excel->getActiveSheet()->SetCellValue('K' . $row, $factor);
                        $this->excel->getActiveSheet()->SetCellValue('L' . $row, intval($totalMayor));
                    }
                    $iteracion = 1;
                    $valida = 1;
                    if (is_null($product['unidad'])) {
                        $iteracion = 0;
                    }
                }
                if ($iteracion == 1 && $valida == 0) {
                    $unidad = $this->products_model->getNameUnitMinor($product['id']);
                    $this->excel->getActiveSheet()->SetCellValue('J' . $row, $unidad);
                    $this->excel->getActiveSheet()->SetCellValue('K' . $row, ($product['factor'] == 0) ? 1 : 1);
                    $totalMenor = $totalMayor - intval($totalMayor);
                    $totalMenor = $totalMenor * $factor;
                    $this->excel->getActiveSheet()->SetCellValue('L' . $row, $totalMenor);
                }
                $codigoAnterior = $product['codigo'];
                $contador = 1;
            }
        }
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(12);
        $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(15);
        $this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(22);
        $this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(18);
        $this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(18);
        $titulos1 = [
            'font' => [
                'bold' => true,
                'size'  => 10,
                'name' => 'calibrí',
            ],
            'alignment' => [
                'wrapText' => true,
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            ],
            'borders' => [
                'diagonalDirection' => PHPExcel_Style_Borders::DIAGONAL_BOTH,
                'allBorders' => [
                    'borderStyle' => PHPExcel_Style_Border::BORDER_THIN,
                ],
            ],
        ];
        $this->excel->getActiveSheet()->getStyle("A1:Q1")->applyFromArray($titulos1);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $filename = 'Formato Conteo' . date('Y_m_d_H_i_s');
        $this->load->helper('excel');
        create_excel($this->excel, $filename);
    }

    public function resize_and_compress_images($unique_image = NULL)
    {
        $dir = 'assets/uploads/';
        $thumbDir = 'assets/uploads/thumbs/';
        $pwidth = 400;
        if ($unique_image) {
            // Obtener la extensión del archivo
            $ext = pathinfo($unique_image, PATHINFO_EXTENSION);
            // Crear una instancia de la imagen
            if (mb_strtolower($ext) == 'jpg' || mb_strtolower($ext) == 'jpeg') {
              $img = imagecreatefromjpeg($unique_image);
            } else if (mb_strtolower($ext) == 'png') {
              $img = imagecreatefrompng($unique_image);
              imagealphablending($img, false);
              imagesavealpha($img, true);
            } else if (mb_strtolower($ext) == 'gif') {
              $img = imagecreatefromgif($unique_image);
            } else {
              return false;
            }
            // Obtener las dimensiones de la imagen
            $width = imagesx($img);
            $height = imagesy($img);
            // Calcular las nuevas dimensiones
            $newWidth = $pwidth;
            $newHeight = $pwidth;
            if ($width > $height) {
              $newHeight = round($height * ($newWidth / $width));
            } else {
              $newWidth = round($width * ($newHeight / $height));
            }
            // Crear la imagen redimensionada
            $thumb = imagecreatetruecolor($newWidth, $newHeight);
            if (mb_strtolower($ext) == 'png') {
              imagealphablending($thumb, false);
              imagesavealpha($thumb, true);
            }
            imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
            // Guardar la imagen redimensionada
            $thumbPath = $thumbDir . basename($unique_image);
            if (file_exists($thumbPath)) {
              unlink($thumbPath);
            }
            if (mb_strtolower($ext) == 'jpg' || mb_strtolower($ext) == 'jpeg') {
              imagejpeg($thumb, $thumbPath, 80);
            } else if (mb_strtolower($ext) == 'png') {
              imagepng($thumb, $thumbPath, 8);
            } else if (mb_strtolower($ext) == 'gif') {
              imagegif($thumb, $thumbPath);
            }
            // Liberar memoria
            imagedestroy($img);
            imagedestroy($thumb);
        } else {
            // Abrir el directorio y obtener las imágenes
            $images = glob($dir . '*.{jpg,jpeg,png,gif}', GLOB_BRACE);
            // Iterar por cada imagen
            foreach ($images as $image) {
                // Obtener la extensión del archivo
                $ext = pathinfo($image, PATHINFO_EXTENSION);
                // Crear una instancia de la imagen
                if (mb_strtolower($ext) == 'jpg' || mb_strtolower($ext) == 'jpeg') {
                  $img = imagecreatefromjpeg($image);
                } else if (mb_strtolower($ext) == 'png') {
                  $img = imagecreatefrompng($image);
                  imagealphablending($img, false);
                  imagesavealpha($img, true);
                } else if (mb_strtolower($ext) == 'gif') {
                  $img = imagecreatefromgif($image);
                } else {
                  continue;
                }
                // Obtener las dimensiones de la imagen
                $width = imagesx($img);
                $height = imagesy($img);
                // Calcular las nuevas dimensiones
                $newWidth = $pwidth;
                $newHeight = $pwidth;
                if ($width > $height) {
                  $newHeight = round($height * ($newWidth / $width));
                } else {
                  $newWidth = round($width * ($newHeight / $height));
                }
                // Crear la imagen redimensionada
                $thumb = imagecreatetruecolor($newWidth, $newHeight);
                if (mb_strtolower($ext) == 'png') {
                  imagealphablending($thumb, false);
                  imagesavealpha($thumb, true);
                }
                imagecopyresampled($thumb, $img, 0, 0, 0, 0, $newWidth, $newHeight, $width, $height);
                // Guardar la imagen redimensionada
                $thumbPath = $thumbDir . basename($image);
                if (file_exists($thumbPath)) {
                  unlink($thumbPath);
                }
                if (mb_strtolower($ext) == 'jpg' || mb_strtolower($ext) == 'jpeg') {
                  imagejpeg($thumb, $thumbPath, 80);
                } else if (mb_strtolower($ext) == 'png') {
                  imagepng($thumb, $thumbPath, 8);
                } else if (mb_strtolower($ext) == 'gif') {
                  imagegif($thumb, $thumbPath);
                }
                // Liberar memoria
                imagedestroy($img);
                imagedestroy($thumb);
            }
        }

    }

    public function process_new_product_images()
    {
        // Directorio donde se encuentran las imágenes originales
        $directorioOriginal = 'assets/images/new_product_images';
        // Directorio de destino para las imágenes encriptadas
        $directorioDestino = 'assets/uploads';
        // Obtener la lista de archivos en el directorio original
        $archivos = scandir($directorioOriginal);
        $img_processed = 0;
        $pr_extra_img = [];
        foreach ($archivos as $archivo) {
            // Ignorar archivos ocultos y directorios
            if ($archivo != '.' && $archivo != '..') {
                // Obtener el código de producto y número de imagen del nombre del archivo
                $datosImagen = explode('-', $archivo);
                if (count($datosImagen) > 1) {
                    $codigoProducto = $datosImagen[0];
                    $numeroImagen = $datosImagen[1];
                    // Verificar si el número de imagen es "01"
                    $extension = pathinfo($archivo, PATHINFO_EXTENSION);
                    // Generar un nuevo nombre de imagen encriptado
                    $nombreEncriptado = md5(uniqid(mt_rand()));
                    // Obtener la extensión del archivo original
                    // Ruta de la imagen original
                    $rutaOriginal = $directorioOriginal . '/' . $archivo;
                    // Ruta de la imagen en el directorio de destino
                    $rutaDestino = $directorioDestino . '/' . $nombreEncriptado . '.' . $extension;
                    // Mover la imagen al directorio de destino
                    rename($rutaOriginal, $rutaDestino);
                    $this->resize_and_compress_images($rutaDestino);
                    if ($numeroImagen == '01.'.$extension || $numeroImagen == '1.'.$extension) {
                        $img_processed++;
                        // Realizar la actualización en la tabla "products"
                        $this->db->where('code', $codigoProducto);
                        $this->db->update('products', array('image' => $nombreEncriptado . '.' . $extension));
                    } else {
                        $img_processed++;
                        $pr_data = $this->products_model->getProductByCode($codigoProducto);
                        if ($pr_data) {
                            if (!isset($pr_extra_img[$pr_data->id])) {
                                $this->db->delete('product_photos', ['product_id'=>$pr_data->id]);
                                $pr_extra_img[$pr_data->id] = 1;
                            }
                            $this->db->insert('product_photos', ['product_id'=>$pr_data->id, 'photo'=>$nombreEncriptado . '.' . $extension]);
                        }

                    }
                } else {
                    continue;
                }
            }
        }
        if ($img_processed == 0) {
            $this->session->set_flashdata('error', 'No se procesó ninguna imagen');
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->session->set_flashdata('message', 'Se procesaron '.$img_processed.' imágenes ');
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function variants_suggestions($term = NULL, $limit = NULL, $a = NULL)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        if (strlen($term) < 1) {
            return FALSE;
        }
        $limit = $this->input->get('limit', TRUE);
        $result = $this->products_model->get_variants_suggestions($term, $limit);
        if ($a) {
            $this->sma->send_json($result);
        }
        $rows['results'] = $result;
        $this->sma->send_json($rows);
    }

    public function getVariant($name = NULL)
    {
        $row = $this->products_model->get_variant_by_name($name);
        $name = $row->name;
        $this->sma->send_json(array(array('id' => $row->name, 'text' => $name, 'value' => $name)));
    }

    function set_featuring($id = NULL)
    {
        // $this->sma->checkPermissions(NULL, TRUE);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($featured = $this->products_model->set_featuring($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => ($featured['featured'] == 1 ? lang("product_featured") : lang("product_not_featured"))));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("product_featured_error")));
        }
    }

    public function process_new_product_images_tienda_pro()
    {
        $this->load->integration_model('Uploads');
        $this->load->integration_model('Product');
        $Uploads = new Uploads();
        $Product = new Product();
        $directorioOriginal = 'assets/images/new_tiendapro_images';
        $directorioDestino = 'assets/uploads';
        $directorioDestinoThumb = 'assets/uploads/thumbs';
        $directorioDestino2 = '../public/uploads/all';
        $directorioDestino2Host = $_SERVER['HTTP_HOST'].'/public/uploads/all/';
        $archivos = scandir($directorioOriginal);
        $img_processed = 0;
        $pr_extra_img = [];
        foreach ($archivos as $archivo) {
            if ($archivo != '.' && $archivo != '..') {
                $datosImagen = explode('_', $archivo);
                if (count($datosImagen) > 1) {
                    $codigoProducto = $datosImagen[0];
                    $numeroImagen = $datosImagen[1];
                    $extension = pathinfo($archivo, PATHINFO_EXTENSION);
                    $nombreEncriptado = md5(uniqid(mt_rand()));
                    $rutaOriginal = $directorioOriginal . '/' . $archivo;
                    // $rutaDestino = $directorioDestino . '/' . $nombreEncriptado . '.' . $extension;
                    // copy($rutaOriginal, $rutaDestino);
                    $pr_data = $this->db->get_where('products', ['code'=>$codigoProducto]);
                    if ($pr_data->num_rows() > 0) {

                        $rutaDestino2 = $directorioDestino2 . '/' . $nombreEncriptado . '.' . $extension;
                        rename($rutaOriginal, $rutaDestino2);
                        $this->resize_and_compress_images($rutaDestino2);
                        $pr_data = $pr_data->row();
                        $erp_image = str_replace($directorioDestino2Host, $directorioDestino."/", $pr_data->image);
                        $thumb_image = str_replace($directorioDestino2Host, $directorioDestinoThumb."/", $pr_data->image);
                        $tpro_image = str_replace($directorioDestino2Host, 'uploads/all/', $pr_data->image);
                        if ($numeroImagen == '01.'.$extension || $numeroImagen == '1.'.$extension) {
                            if (file_exists($erp_image)) {
                              unlink($erp_image);
                            }//eliminar foto anterior
                            if (file_exists("../public/".$tpro_image)) {
                              unlink("../public/".$tpro_image);
                            }//elimoinar foto anterior tienda
                            if (file_exists($thumb_image)) {
                              unlink($thumb_image);
                            }//eliminar thumb
                            $img_processed++;
                            $this->db->update('products', array('image' => $directorioDestino2Host.$nombreEncriptado.'.'.$extension), ['id' => $pr_data->id]);
                            $integrations = $this->settings_model->getAllIntegrations();
                            if ($integrations) {
                                foreach ($integrations as $integration) {
                                    $Uploads->open($integration);
                                    $photo_tpro = $Uploads->find(['file_name' => $tpro_image]);
                                    if ($photo_tpro) {
                                        $Uploads->delete($photo_tpro->id);
                                    }
                                    $Uploads->close();
                                }
                            }
                        } else {
                            $img_processed++;
                            if (!isset($pr_extra_img[$pr_data->id])) {
                                $old_pr_photos = $this->db->get_where('product_photos', ['product_id'=>$pr_data->id]);
                                if ($old_pr_photos->num_rows() > 0) {
                                    foreach (($old_pr_photos->result()) as $oprph) {

                                        $erp_image = str_replace($directorioDestino2Host, $directorioDestino."/", $oprph->photo);
                                        $thumb_image = str_replace($directorioDestino2Host, $directorioDestinoThumb."/", $oprph->photo);
                                        $tpro_image = str_replace($directorioDestino2Host, 'uploads/all/', $oprph->photo);
                                        if (file_exists($erp_image)) {
                                          unlink($erp_image);
                                        }//eliminar foto anterior
                                        if (file_exists("../public/".$tpro_image)) {
                                          unlink("../public/".$tpro_image);
                                        }//eliminar foto anterior tienda
                                        if (file_exists($thumb_image)) {
                                          unlink($thumb_image);
                                        }//eliminar thumb
                                        $integrations = $this->settings_model->getAllIntegrations();
                                        if ($integrations) {
                                            foreach ($integrations as $integration) {
                                                $Uploads->open($integration);
                                                $photo_tpro = $Uploads->find(['file_name' => $tpro_image]);
                                                if ($photo_tpro) {
                                                    $Uploads->delete($photo_tpro->id);
                                                }
                                                $Uploads->close();
                                            }
                                        }
                                    }
                                }
                                $this->db->delete('product_photos', ['product_id'=>$pr_data->id]);
                                $pr_extra_img[$pr_data->id] = 1;
                            }
                            $this->db->insert('product_photos', ['product_id'=>$pr_data->id, 'photo'=>$directorioDestino2Host.$nombreEncriptado . '.' . $extension]);
                        }
                        $integrations = $this->settings_model->getAllIntegrations();
                        if ($integrations) {
                            foreach ($integrations as $integration) {
                                $Uploads->open($integration);
                                $Product->open($integration);
                                $pr_tpro = $Product->find(['id_wappsi' => $pr_data->id]);
                                if ($pr_tpro) {
                                    $file_name = "uploads/all/".$nombreEncriptado.'.'.$extension;
                                    $file_original_name = $archivo;
                                    $file_size = filesize($rutaDestino2);

                                    $id_upload = $Uploads->create([
                                        'file_original_name' => $file_original_name,
                                        'file_name' => $file_name,
                                        'user_id' => $this->session->userdata('user_id'),
                                        'file_size' => $file_size,
                                        'extension' => $extension,
                                        'type' => 'image',
                                        'created_at' => date('Y-m-d H:i:s'),
                                    ]);
                                    if ($numeroImagen == '01.'.$extension || $numeroImagen == '1.'.$extension) {
                                        $Product->update(['photos' => NULL], $pr_tpro->id, false);
                                        $pr_tpro->photos = "";
                                        $photo_tpro = $Uploads->find(['id' => $pr_tpro->thumbnail_img]);
                                        if ($photo_tpro) {
                                            $Uploads->delete($photo_tpro->id);
                                            if (file_exists("../public/".$photo_tpro->file_name)) {
                                              unlink("../public/".$photo_tpro->file_name);
                                            }//elimoinar foto anterior tienda
                                        }
                                        $file_name = "uploads/all/".$nombreEncriptado . '_1.' . $extension;
                                        $file_original_name = $archivo;
                                        $file_size = filesize($directorioDestinoThumb."/".$nombreEncriptado . '.' . $extension);

                                        $Uploads = new Uploads();
                                        $Uploads->open($integration);
                                        $id_upload_thumb = $Uploads->create([
                                            'file_original_name' => $file_original_name,
                                            'file_name' => $file_name,
                                            'user_id' => $this->session->userdata('user_id'),
                                            'file_size' => $file_size,
                                            'extension' => $extension,
                                            'type' => 'image',
                                            'created_at' => date('Y-m-d H:i:s'),
                                        ]);
                                        rename($directorioDestinoThumb."/".$nombreEncriptado . '.' . $extension, $directorioDestino2."/".$nombreEncriptado . '_1.' . $extension);
                                        $Uploads->close();
                                    }
                                    $photos = $pr_tpro->photos;
                                    if ($pr_tpro && strlen($photos) == 0) {
                                        $photos = $id_upload;
                                    } else {
                                        $photos .= " ,".$id_upload;
                                    }
                                    $Product->update(['photos' => $photos], $pr_tpro->id, false);

                                    if ($numeroImagen == '01.'.$extension) {
                                        $Product->update(['thumbnail_img' => $id_upload_thumb], $pr_tpro->id, false);
                                    }
                                }
                                $Product->close();
                            }
                        }

                    }
                } else {
                    continue;
                }
            }
        }
        if ($img_processed == 0) {
            $this->session->set_flashdata('error', 'No se procesó ninguna imagen');
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->session->set_flashdata('message', 'Se procesaron '.$img_processed.' imágenes ');
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function adjustment_product_variants_selection($product_id, $qaitems_id, $warehouse_id, $search = NULL)
    {
        $options = $this->products_model->getProductOptions($product_id, $warehouse_id, $search);
        if (!empty($options)) {
            $html = "";
            $html .= '<ul class="ob">';
            foreach ($options as $option) {
                // if (($option->quantity <= 0 && $this->Settings->overselling == 0)) {
                //     continue;
                // }
                $html .= '<li>
                            <button type="button" class="btn btn-primary btn-outline product_variant_select" data-pvid="' . $option->id . '">
                                <strong style="font-size:120%;" class="suspend_note">' . $option->name . '</strong><br>
                                <strong>' . $this->sma->formatQuantity($option->quantity) . '</strong>
                            </button>
                        </li>';
            }
            $html .= '</ul>';
        } else {
            $html = "<h3>" . lang('no_product_variants') . "</h3><p>&nbsp;</p>";
        }

        $data['html'] = $html;
        $data['product_id'] = $product_id;
        $data['qaitems_id'] = $qaitems_id;
        $data['pdata'] = $this->site->getProductByID($product_id);
        $this->load_view($this->theme . 'products/adjustment_product_variants_selection', $data);
    }
    public function getSecondaryTaxValuesAjax()
    {
        $idTaxSecundary = $this->input->post('idTaxSecundary');
        $taxesSecundary = $this->products_model->getTaxSecundaryPercentage(['id_tax_secondary' => $idTaxSecundary]);
        echo json_encode($taxesSecundary);
    }

    public function options_suggestions($term = NULL, $limit = NULL, $ptype = 1)
    {
        // $this->sma->checkPermissions('index');
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->products_model->getOptionsSuggestions($term, $limit);
        $this->sma->send_json($rows);
    }

    public function getOption($id = NULL)
    {
        $row = $this->products_model->getOptionByName($id);
        $this->sma->send_json(array(array('id' => $row->name, 'text' => $row->name)));
    }

    public function getQuantitiesProductInWarehouseAjax()
    {
        $warehouseId = $this->input->get('warehouseId');
        $productId = $this->input->get('productId');
        $optionId = $this->input->get('optionId');

        if (!empty($optionId)) {
            $productQuantity = $this->products_model->getQuantityProductVariantsByWarehouse(['product_id'=>$productId, 'warehouse_id'=>$warehouseId, 'id' => $optionId]);
        } else {
            $productQuantity = $this->products_model->getQuantityProductByWarehouse(['product_id'=>$productId, 'warehouse_id'=>$warehouseId]);
        }

        if (!empty($productQuantity)) {
            echo json_encode(['quantity'=>floatval($productQuantity->quantity)]);
        } else {
            echo json_encode(['quantity'=>0]);
        }
    }

    public function syncProductQuantitiesStore($products_ids=null)
    {
        if ($products_ids) { // here is for import price promo
            $productIds = $products_ids; // structure [[0] => "id_product", [1] => "id_product"]
            # is the same structure that productIds, so dont affected the execution flow
        }else{
            $productIds = $this->input->post('productIds');
        }
        if (!empty($productIds)) {
            if ($this->Settings->data_synchronization_to_store == ACTIVE) {
                $integrations = $this->settings_model->getAllIntegrations();
                if (!empty($integrations)) {
                    foreach ($integrations as $integration) {
                        $TaxRatesModel = new TaxRates_model();

                        $this->load->integration_model('Product');
                        $this->load->integration_model('ProductVariation');

                        foreach ($productIds as $productId) {
                            $product = $this->products_model->getProductByID($productId); //producto WAPPSI
                            $tax = $TaxRatesModel->find($product->tax_rate);

                            $Product = new Product();
                            $Product->open($integration);
                            $productExt = $Product->find(["id_wappsi" => $productId]); //producto PRO

                            $this->syncTax($product);

                            if (!empty($productExt)) {
                                $quantity = $this->getQuantityProduct($productId);

                                $data = [
                                    "lowest_price"  => ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price,
                                    "highest_price" => ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price,
                                    "quantity"      => $quantity,
                                    "discount"      => (!empty($product->promotion)) ? ($product->price - $product->promo_price) : 0,
                                    "discount_type" => "flat",
                                    "stock"         => ($quantity >= $this->shop_settings->activate_available_from_quantity) ? ACTIVE : INACTIVE,
                                    "published"     => ($product->hide_online_store == YES) ? INACTIVE : (($quantity >= $this->shop_settings->activate_available_from_quantity) ? ACTIVE : INACTIVE),
                                ];
                                if ($product->start_date) {
                                    $data['discount_start_date'] = strtotime($product->start_date);
                                }
                                if ($product->end_date) {
                                    $data['discount_end_date'] = strtotime($product->end_date);
                                }

                                if ($Product->update($data, $productExt->id)) {
                                    $productVariants = $this->products_model->findProductVariants(["product_id" => $productId]);
                                    if (!empty($productVariants)) {
                                        $ProductVariation = new ProductVariation();
                                        $ProductVariation->open($integration);

                                        foreach ($productVariants as $productVariant) {
                                            $variantExt = $ProductVariation->find(['id_wappsi' => $productVariant->id]);
                                            if (!empty($variantExt)) {
                                                $quantity = $this->getQuantityProductVariant($productId, $productVariant->id);
                                                $productPrice = ($tax->type == 1) ? $product->price / (1 + ($tax->rate / 100)) : $product->price;
                                                $data = [
                                                    "price"    => (!empty($productVariant->price)) ? ($productPrice + $productVariant->price) : $productPrice,
                                                    "stock"    => ($quantity >= $this->shop_settings->activate_available_from_quantity) && ($productVariant->status == YES) ? ACTIVE : INACTIVE,
                                                    "quantity" => $quantity,
                                                ];

                                                // if ($this->Settings->prioridad_precios_producto == 4) {
                                                    $productPrices = $this->products_model->getProductPrices(["product_id" => $productId]);
                                                    if (isset($productPrices) && !empty($productPrices)) {
                                                        $i = 2;
                                                        foreach ($productPrices as $prices) {
                                                            $priceProductPrice = ($tax->type == 1) ? $prices->price / (1 + ($tax->rate / 100)) : $prices->price;
                                                            if ($prices->price_group_base == 1) {
                                                                $data["price"] = (!empty($productVariant->price)) ? ($priceProductPrice + $productVariant->price) : $priceProductPrice;
                                                            } else {
                                                                $data["price_$i"] = (!empty($productVariant->price)) ? ($priceProductPrice + $productVariant->price) : $priceProductPrice;
                                                                $i++;
                                                            }
                                                        }   
                                                    }
                                                // }

                                                $ProductVariation->update($data, $variantExt->id);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            echo json_encode([]);
        }
    }

    public function hideProductWithoutPhotoInStore(){
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations(); //<- sma_integrations
            if (!empty($integrations)) {
                foreach ($integrations as $integration) {
                    $this->load->integration_model('Product'); // <- models/integration/Product.php
                    $Product = new Product();
                    $Product->open($integration);
                    $data = [ "published"  => '0'];
                    if ($Product->updateProductsWithOutPhoto($data)) {
                        echo json_encode([]);
                    }else{
                        echo json_encode(["type" => "error", "message" => "Error al actualizar los datos en la tienda"]);
                    }
                }
            }else{
                echo json_encode(["type" => "warning", "message" => "Por favor, configurar la integración"]);
            }
        }else{
            echo json_encode(["type" => "warning", "message" => "Por favor, activar la sincronización con la tienda"]);
        }
    }

    public function checkPricesStockAjax()
    {
        $data = [];
        $searchValue = $this->input->post('searchValue');
        $searchType = $this->input->post('searchType');


        if ($searchType == 1 || $searchType == 3) {
            if ($searchType == 1) {
                $product = $this->products_model->find(['id' => $searchValue], true);
            } else {
                $product = $this->products_model->find(['id'=>$searchValue], true);
            }
            $brand = $this->products_model->getBrand(["id"=>$product->brand]);
            $product->brand_name = $brand->name;

            if (!empty($product)) {
                $productWarehouses = $this->products_model->getAllWarehousesWithPQ($product->id);
                $productVariants = $this->products_model->getProductOptions($product->id);
                $options = $this->products_model->getProductOptionsWithWH($product->id);
            }
        } else if ($searchType == 2) {
            $product = $this->products_model->findVariant(['product_variants.code' => $searchValue]);
            $brand = $this->products_model->getBrand(["id"=>$product->brand]);
            $product->brand_name = $brand->name;

            $productWarehouses = $this->products_model->getAllWarehousesWithPVQ($product->option_id);
            $productVariants = $this->products_model->getProductOptionsV($product->option_id);
            $options = $this->products_model->getProductOptionsWithWHV($product->id, $product->option_code);
        }

        if (!empty($product)) {
            $images = $this->products_model->getProductPhotos($product->id);
            $urlImages = [];
            $maxImages = 4;
            if (!empty($images)) {
                $i = 1;
                foreach ($images as $image) {
                    if ($i <= $maxImages) {
                        $urlImages[] = $this->sma->get_img_url($image->photo);
                    }
                }
            }

            $productPriceGroups = $this->products_model->getProductPriceGroups($product->id);
            $data = [
                'productImage'      => $this->sma->get_img_url($product->image),
                'product'           => $product,
                'productWarehouses' => $productWarehouses,
                'productVariants'   => $productVariants,
                'options'           => $options,
                'productPriceGroups'=> $productPriceGroups,
                'images'            => $urlImages,
            ];
        }

        echo json_encode($data);
    }

    public function categorySuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        $parentId = $this->input->get('parentId');
        $subcategoryId = $this->input->get('subcategoryId');

        $result = $this->products_model->getCategorySuggestions($term, $limit, $parentId, $subcategoryId);

        $this->sma->send_json($result);
    }

    public function getProduct($id = NULL)
    {
        $row = $this->products_model->getProductByID($id);
        $name = "$row->name ($row->code)";
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $name, 'value' => $name)));
    }

    public function productsSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        if (empty($term)) {
            $this->sma->send_json(false);
        }
        $result = $this->products_model->getProductSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function productCodesSuggestions()
    {
        $response = [];
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        if (empty($term)) {
            $this->sma->send_json(false);
        }
        $result = $this->products_model->getProductCodesSuggestions($term, $limit);

        foreach ($result as $product) {
            $response[] = [
                "id"    => $product->id,
                "text"  => "$product->text ($product->code)"
            ];
        }

        $this->sma->send_json($response);
    }

    public function servicesSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        if (empty($term)) {
            $this->sma->send_json(false);
        }
        $result = $this->products_model->getServicesSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function referenceSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        $result = $this->products_model->getReferenceSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function reference_suggestions(){
        $term = $this->input->get('term', TRUE);
        if (strlen($term) < 1) {
            die();
        }
        $rows = $this->products_model->getReference_suggestions($term);
        if ($rows) {
            foreach ($rows as $row) {
                $pr[] = array('id' => $row->reference, 'label' => $row->reference . " (" . $row->name . ")");
            }
            $this->sma->send_json($pr);
        } else {
            echo FALSE;
        }
    }

    public function variantsSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        $result = $this->products_model->getVariantsSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function colorSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        $result = $this->products_model->getColorSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function brandSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        $result = $this->products_model->getBrandSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function materialSuggestions()
    {
        $limit = $this->input->get('limit');
        $term = $this->input->get('term');
        $result = $this->products_model->getMaterialSuggestions($term, $limit);
        $this->sma->send_json($result);
    }

    public function multipleSearchProductAjax()
    {
        $reference = $this->input->post('reference');
        $category = $this->input->post('category');
        $subcategory = $this->input->post('subcategory');
        $subsubcategory = $this->input->post('subsubcategory');
        $variants = $this->input->post('variants');
        $color = $this->input->post('color');
        $brand = $this->input->post('brand');
        $material = $this->input->post('material');
        $data = [
            'reference'     =>  $reference,
            'category'      =>  $category,
            'subcategory'   =>  $subcategory,
            'subsubcategory'=>  $subsubcategory,
            'variants'      =>  $variants,
            'color'         =>  $color,
            'brand'         =>  $brand,
            'material'      =>  $material,
        ];

        $products = $this->products_model->getMultipleSearchProducts($data);
        echo json_encode($products);
    }

    public function products_by_warehouse(){
        $warehouse_id = $this->input->get('warehouse_id');
        $biller_id = $this->input->get('biller_id');
        $destination_biller_id = $this->input->get('destination_biller_id');
        $search_type = $this->input->get('search_type');
        $sr = '';
        $all = true;
        $option_id = false;
        $term = '';

        // la idea es obtener los productos como si fueran sugerencias seleccionadas todas
        $rows = $this->products_model->getPTRSuggestions($sr, $warehouse_id, $biller_id, $destination_biller_id, $search_type, $all);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                $options = $this->products_model->getProductOptions($row->id);
                $row->option = $option_id;
                $row->serial = '';
                $unit = $this->products_model->getUnitById($row->unit);
                $composition_products = $this->products_model->getProductComboItems($row->id);
                $row->price = $row->price;
                $row->last_cost = $row->cost;
                if ($composition_products) {
                    $total_cost = 0;
                    foreach ($composition_products as $cproduct) {
                        $total_cost += ($cproduct->price * $cproduct->qty);
                    }
                    $row->cost = $total_cost;
                }
                $c = sha1(uniqid(mt_rand(), true));
                $pr[] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'options' => $options, 'composition_products' => $composition_products, 'unit' => $unit);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
        echo json_encode($dataProductos);
    }

    public function get_templade_adjustements()
    {
        $brand = isset($_GET['brand']) ? $_GET['brand'] : NULL;
        $category = isset($_GET['category']) ? $_GET['category'] : NULL;
        $products_negatives = isset($_GET['products_negatives']) ? $_GET['products_negatives'] : NULL;
        $warehouse = isset($_GET['warehouse']) ? $_GET['warehouse'] : NULL;
        $this->load->library('excel');
        $column = "A";
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->SetCellValue($column++."1", 'Codigo');
        $this->excel->getActiveSheet()->SetCellValue($column++."1", 'Cantidad');
        $this->excel->getActiveSheet()->SetCellValue($column++."1", 'Variante');
        $this->excel->getActiveSheet()->SetCellValue($column++."1", 'Costo');
        $row = 2;
        $products = $this->site->getProductsByBrandOrCategory(1, $brand, $category);
        foreach ($products as $key => $product) {
            if ($product->attributes == 1) {  // <- tiene variante
                $product_variant = $this->site->getProductVariants($product->id);
                $warehouses_products_variants = $this->site->getWarehouseProductsVariants($product_variant->id, $warehouse, $products_negatives);
                if (!empty($warehouses_products_variants)) {
                    foreach ($warehouses_products_variants as $keyWv => $valueWv) {
                        if ($valueWv->quantity != "0") {
                            /* codigo, cantidad, variante, costo*/
                            $item = new stdClass();
                            $item->codigo = $product->code;
                            $item->cantidad = $valueWv->quantity * -1;
                            $item->variante = $valueWv->code;
                            $item->costo = $product_variant->cost;
                            $items[] = $item;
                        }
                    }
                }
            }else{
                $getWarehouseProducts = $this->site->getWarehouseProducts($product->id, $warehouse, $products_negatives);
                if (!empty($getWarehouseProducts)) {
                    foreach ($getWarehouseProducts as $keyWv => $valueWv) {
                        if ($valueWv->quantity != "0.0000" && $valueWv->quantity) {
                            /* codigo, cantidad, variante, costo*/
                            $item = new stdClass();
                            $item->codigo = $product->code;
                            $item->cantidad = $valueWv->quantity * -1;
                            $item->variante = '';
                            $item->costo = $valueWv->avg_cost;
                            $items[] = $item;
                        }
                    }
                }
            }
        }

        if (!empty($items)) {
            foreach ($items as $data_row) {
                $column = "A";
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->codigo);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->cantidad);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->variante);
                $this->excel->getActiveSheet()->SetCellValue($column++. $row, $data_row->costo);
                $row++;
            }
        }

        # styles
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        for ($i='A'; $i <= 'D'; $i++) {
            $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
        }
        $filename = 'sample_adjustment';
        $this->load->helper('excel');
        create_excel($this->excel, $filename);
    }

    public function import_price_promotion()
    {
        $this->sma->checkPermissions();
        $this->load->helper('security');
        $this->form_validation->set_rules('csv_file', $this->lang->line("upload_file"), 'xss_clean');
        $changes = '';
        $products_ids = [];
        if ($this->form_validation->run() == true) {
            if (DEMO) {
                $this->session->set_flashdata('warning', $this->lang->line("disabled_in_demo"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if (isset($_FILES["csv_file"])) /* if($_FILES['userfile']['size'] > 0) */ {
                $this->load->library('upload');
                $config['upload_path'] = 'assets/uploads/csv/';
                $config['allowed_types'] = 'csv';
                $config['max_size'] = '2000';
                $config['overwrite'] = true;
                $config['encrypt_name'] = TRUE;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('csv_file')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("products");
                }
                $csv = $this->upload->file_name;
                $arrResult = array();
                $handle = fopen("assets/uploads/csv/" . $csv, "r");
                $separator = (count(fgetcsv($handle, null, ",")) > 1) ? "," : ";";
                if ($handle) {
                    while (($row = fgetcsv($handle, 5001, ';')) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $keys = array('Codigo', 'Precio_promocion', 'Fecha_inicial', 'Fecha_final');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    if (!$final[] = array_combine($keys, $value)) {
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key+1)));
                        admin_redirect("products");
                        break;
                    }
                }
                $rw = 2;
                foreach ($final as $csv) {
                    if ($pr = $this->products_model->getProductByCode($csv['Codigo'])) {
                        array_push($products_ids, $pr->id);
                    }else{
                        // <- here we validate if the product code exists
                        $this->session->set_flashdata('error', $this->lang->line("code_x_exist") . " (" . $csv['Codigo'] . ").  (" . $this->lang->line("line_no") . " " . $rw . ")");
                        admin_redirect("products");
                    }
                    $rw++;
                }
                foreach ($final as $csv) {
                    $record['code'] = $csv['Codigo'];
                    $record['promo_price'] = $csv['Precio_promocion'];

                    list($day_ini, $month_ini, $year_ini) = explode('/', $csv['Fecha_inicial']);
                    $formatted_date_ini = "$year_ini-$month_ini-$day_ini";
                    $start_date_times_format = date('Y-m-d',strtotime($formatted_date_ini)). " 00:00:00";
                    $record['start_date'] = $start_date_times_format;

                    list($day_fin, $month_fin, $year_fin) = explode('/', $csv['Fecha_final']);
                    $formatted_date_ini = "$year_fin-$month_fin-$day_fin";
                    $end_date_times_format = date('Y-m-d',strtotime($formatted_date_ini)). " 23:59:59";
                    $record['end_date'] = $end_date_times_format;

                    $record['promotion'] = "1";
                    $data[] = $record;
                    $changes .= "Código : ".$record['code']. ", Precio promo: " .$record['promo_price']. ", Fecha inicial: " .$record['start_date']. ", Fecha final: " .$record['end_date']. " - ";
                }
            }
            // $this->sma->print_arrays($products_ids);
        } elseif ($this->input->post('import')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect('products');
        }
        if ($this->form_validation->run() == true && !empty($data)) {
            if ($this->products_model->update_promotion_price($data)) {
                $this->syncProductQuantitiesStore($products_ids);
                $txt_fields_changed = sprintf(lang('user_promo_price_changed'), $this->session->first_name . " " . $this->session->last_name, lang($this->m), trim($changes, ' - '));
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 1,
                    'table_name' => 'products',
                    'record_id' => '',
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $txt_fields_changed,
                ]);
                $this->session->set_flashdata('message', $this->lang->line("price_updated"));
                admin_redirect('products');
            }else{
                $this->session->set_flashdata('warning', $this->lang->line("not_updated"));
                admin_redirect('products');
            }
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'products/import_promotion_price', $this->data);
        }
    }

    public function physical_count(){
        $type = $this->input->get('xls') ? $this->input->get('xls') : null;
        if ($type) { /*** 1=>products, 2=>variantes ***/
            $this->load->library('excel');
            $column = "A";
            $this->excel->setActiveSheetIndex(0);
            $this->excel->getActiveSheet()->SetCellValue($column++."1", ($type == '1') ? lang('product_code') : lang('variant_code'));
            $this->excel->getActiveSheet()->SetCellValue($column++."1", 'Cantidad');
            $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
            for ($i='A'; $i <= 'D'; $i++) {
                $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
            }
            $filename = 'Conteo fisico';
            $this->load->helper('excel');
            create_excel($this->excel, $filename);
        }
    }

    function getOrderSalesReport($pdf = NULL, $xls = NULL)
    {
        $this->sma->checkPermissions('view_sales', TRUE, 'products');
        $product = $this->input->get('product') ? $this->input->get('product') : NULL;
        $user = $this->input->get('user') ? $this->input->get('user') : NULL;
        $customer = $this->input->get('customer') ? $this->input->get('customer') : NULL;
        $biller = $this->input->get('biller') ? $this->input->get('biller') : NULL;
        $warehouse = $this->input->get('warehouse') ? $this->input->get('warehouse') : NULL;
        $reference_no = $this->input->get('reference_no') ? $this->input->get('reference_no') : NULL;
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date') . " 00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date') . " 23:59" : NULL;
        $serial = $this->input->get('serial') ? $this->input->get('serial') : NULL;

        if (!$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            $user = $this->session->userdata('user_id');
        }

        if ($pdf || $xls) {

            $this->db
                ->select("date, reference_no, biller, customer, GROUP_CONCAT(" . $this->db->dbprefix('order_sale_items') . ".product_name SEPARATOR '\n') as iname, GROUP_CONCAT(CAST(" . $this->db->dbprefix('order_sale_items') . ".quantity AS DECIMAL(15,4)) SEPARATOR '\n') as pquantity, grand_total, paid, payment_status, ".$this->db->dbprefix('order_sale_items'). ".subtotal ", FALSE)
                ->from('order_sales')
                ->join('order_sale_items', 'order_sale_items.sale_id=order_sales.id', 'left')
                ->join('warehouses', 'warehouses.id=order_sales.warehouse_id', 'left')
                ->group_by('order_sales.id')
                ->order_by('order_sales.date desc');

            if ($user) {
                $this->db->where('order_sales.created_by', $user);
            }
            if ($product) {
                $this->db->where('order_sale_items.product_id', $product);
            }
            if ($serial) {
                $this->db->like('order_sale_items.serial_no', $serial);
            }
            if ($biller) {
                $this->db->where('order_sales.biller_id', $biller);
            }
            if ($customer) {
                $this->db->where('order_sales.customer_id', $customer);
            }
            if ($warehouse) {
                $this->db->where('order_sales.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->db->like('order_sales.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->db->where($this->db->dbprefix('order_sales') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            $q = $this->db->get();
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
            } else {
                $data = NULL;
            }

            if (!empty($data)) {
                $letter = "A";
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('sales_report'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('date'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('reference_no'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('biller'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('customer'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('product'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('product_qty'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('total')." ".lang('product') );
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('total_invoice'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('paid'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('balance'));
                $this->excel->getActiveSheet()->SetCellValue(($letter++).'1', lang('payment_status'));

                $row = 2;
                $total = 0;
                $paid = 0;
                $balance = 0;
                $qty = 0;
                $total_product = 0;
                foreach ($data as $data_row) {
                    $txtQtys = "";

                    if (strpos($data_row->pquantity, "\n")) {
                        $qtys = explode("\n", $data_row->pquantity);
                        foreach ($qtys as $key => $value) {
                            $txtQtys .= $this->sma->formatQuantity($value) . "\n";
                            $qty += $value;
                        }
                    } else {
                        $txtQtys = $this->sma->formatQuantity($data_row->pquantity) . " ";
                        $qty += $data_row->pquantity;
                    }

                    $letter = 'A';
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $this->sma->hrld($data_row->date));
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->reference_no);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->biller);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->customer);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->iname);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $txtQtys);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->subtotal);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->grand_total);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, $data_row->paid);
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, ($data_row->grand_total - $data_row->paid));
                    $this->excel->getActiveSheet()->SetCellValue(($letter++). $row, lang($data_row->payment_status));
                    $total += $data_row->grand_total;
                    $paid += $data_row->paid;
                    $balance += ($data_row->grand_total - $data_row->paid);
                    $total_product += $data_row->subtotal;
                    $row++;
                }
                $this->excel->getActiveSheet()->getStyle("G" . $row . ":J" . $row)->getBorders()
                    ->getTop()->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);
                $this->excel->getActiveSheet()->SetCellValue('F' . $row, $this->sma->formatQuantity($qty) . " ");
                $this->excel->getActiveSheet()->SetCellValue('G' . $row, $total_product);
                $this->excel->getActiveSheet()->SetCellValue('H' . $row, $total);
                $this->excel->getActiveSheet()->SetCellValue('I' . $row, $paid);
                $this->excel->getActiveSheet()->SetCellValue('J' . $row, $balance);

                for ($i='A'; $i <= $letter; $i++) {
                    $this->excel->getActiveSheet()->getColumnDimension($i)->setAutoSize(true);
                }
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $this->excel->getActiveSheet()->getStyle('E2:E' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('F2:F' . $row)->getAlignment()->setWrapText(true);
                $this->excel->getActiveSheet()->getStyle('G1:G' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('H1:H' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('I1:I' . $row)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('J1:J' . $row)->getNumberFormat()->setFormatCode("#,##0.00");

                $range = 'F1:F' . $row;
                $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
                $filename = 'sales_report';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);
            }
            $this->session->set_flashdata('error', lang('nothing_found'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {


            //GROUP_CONCAT({$this->db->dbprefix('sale_items')}.product_name) as item_nane
            $si = "(
                    SELECT
                        SUM({$this->db->dbprefix('order_sale_items')}.quantity) as quantity,
                        SUM({$this->db->dbprefix('order_sale_items')}.quantity_delivered) as quantity_delivered,
                        sale_id,
                        product_id,
                        serial_no,
                        GROUP_CONCAT(CONCAT({$this->db->dbprefix('order_sale_items')}.product_name) SEPARATOR '___') as item_nane,
                        (unit_price * quantity) as item_unit_price
                    FROM {$this->db->dbprefix('order_sale_items')} ";
            if ($product || $serial) {
                $si .= " WHERE ";
            }
            if ($product) {
                $si .= " {$this->db->dbprefix('order_sale_items')}.product_id = {$product} ";
            }
            if ($product && $serial) {
                $si .= " AND ";
            }
            if ($serial) {
                $si .= " {$this->db->dbprefix('order_sale_items')}.serial_no LIKe '%{$serial}%' ";
            }
            $si .= " GROUP BY {$this->db->dbprefix('order_sale_items')}.sale_id ) FSI";

            $this->load->library('datatables');

            $this->datatables
                ->select("
                            DATE_FORMAT(date, '%Y-%m-%d %T') as date,
                            reference_no,
                            biller,
                            customer,
                            FSI.item_nane as iname,
                            FSI.quantity,
                            FSI.quantity_delivered,
                            (FSI.quantity - FSI.quantity_delivered) AS pending_quantity,
                            item_unit_price,
                            grand_total,
                            sale_status,
                            {$this->db->dbprefix('order_sales')}.id as id
                        ", FALSE)
                ->from('order_sales')
                ->join($si, 'FSI.sale_id=order_sales.id', 'left')
                ->join('warehouses', 'warehouses.id=order_sales.warehouse_id', 'left');

            if ($user) {
                $this->datatables->where('order_sales.created_by', $user);
            }
            if ($product) {
                $this->datatables->where('FSI.product_id', $product);
            }
            if ($serial) {
                $this->datatables->like('FSI.serial_no', $serial);
            }
            if ($biller) {
                $this->datatables->where('order_sales.biller_id', $biller);
            }
            if ($customer) {
                $this->datatables->where('order_sales.customer_id', $customer);
            }
            if ($warehouse) {
                $this->datatables->where('order_sales.warehouse_id', $warehouse);
            }
            if ($reference_no) {
                $this->datatables->like('order_sales.reference_no', $reference_no, 'both');
            }
            if ($start_date) {
                $this->datatables->where($this->db->dbprefix('order_sales') . '.date BETWEEN "' . $start_date . '" and "' . $end_date . '"');
            }

            echo $this->datatables->generate();
        }
    }

    function add_express_count($count_id = null)
    {
        $this->sma->checkPermissions('adjustments', true);
        $this->data['companies'] = $this->site->getAllCompanies($this->Settings->companies_for_adjustments_transfers);
        $this->data['warehouse_id'] = FALSE;
        $this->data['count_id'] = $count_id;
        $count = $this->products_model->getStouckCountByID($count_id);
        $this->data['stock_count']  = $count;
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->data['categories'] = $this->site->getAllCategories();
        $this->data['brands'] = $this->site->getAllBrands();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('add_express_count')));
        $meta = array('page_title' => lang('add_express_count'), 'bc' => $bc);
        $this->page_construct('products/add_express_count', $meta, $this->data);
    }

    function get_products_express_count($count_id = false){

        $biller_id = $this->input->get('biller_id');
        $warehouse_id = $this->input->get('warehouse_id');
        $company = $this->input->get('company');
        $document_type = $this->input->get('document_type');
        $category_id = $this->input->get('category_id') ? $this->input->get('category_id') : NULL;
        $brand_id = $this->input->get('brand_id') ? $this->input->get('brand_id') : NULL;
        $show_only_affected = $this->input->get('show_only_affected');
        $show_only_affected = $show_only_affected == "true" ? true : false;
        $date = $this->sma->fld($this->input->get('date'));
        $can_continue = true;
        $new_insert = false;
        $cant_continue_msg = "";
        $html = "";
        if (!$count_id) {
            if ($category_id) { //si se eligio categoria
                $q = $this->db->get_where('stock_counts', ['finalized'=> 3, 'warehouse_id' => $warehouse_id, 'category_id' => NULL]);
                if ($q->num_rows() > 0) {
                    $can_continue = false;
                    $cant_continue_msg .= "Existe un conteo general sin categoría filtrada para la bodega seleccionada; ";
                }
                $q = $this->db->get_where('stock_counts', ['finalized'=> 3, 'warehouse_id' => $warehouse_id, 'category_id' => $category_id, 'created_by !=' => $this->session->userdata('user_id')]);
                if ($q->num_rows() > 0) {
                    $can_continue = false;
                    $cant_continue_msg .= "Existe un conteo de otro usuario para la categoría filtrada y para la bodega seleccionada; ";
                }
            } else {
                $q = $this->db->get_where('stock_counts', ['finalized'=> 3, 'warehouse_id' => $warehouse_id, 'created_by !=' => $this->session->userdata('user_id')]);
                if ($q->num_rows() > 0) {
                    $can_continue = false;
                    $cant_continue_msg .= "Existe un conteo de otro usuario para la bodega seleccionada; ";
                }
            }
            $new_insert = true;
            if ($can_continue) { //si puede continuar y no se indicó id de conteo en el ajax, se busca conteo expres pendiente para el usuario, para que pueda continuar con el proceso
                $q = $this->db->get_where('stock_counts', ['finalized'=> 3, 'warehouse_id' => $warehouse_id, 'created_by' => $this->session->userdata('user_id'), 'category_id' => $category_id]);
                if ($q->num_rows() > 0) {
                    $q = $q->row();
                    $count_id = $q->id;
                    $new_insert = false;
                    $cant_continue_msg = "Se encontró un conteo pendiente en proceso y se abrió para continuar el diligenciamiento";
                }
            }
        }

        if ($can_continue) {
            if ($new_insert) {
                $data = [
                        'biller_id' => $biller_id,
                        'warehouse_id' => $warehouse_id,
                        'company_id' => $company,
                        'document_type_id' => $document_type,
                        'date' => $date,
                        'category_id' => $category_id,
                        'brand_id' => $brand_id,
                        'created_by' => $this->session->userdata('user_id'),
                        'finalized' => 3,
                        'automatic_adjustment' => 1,
                        'type' => ($category_id || $brand_id ? 'express_partial' : 'express'),
                    ];
                $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
                if($referenceBiller){
                    $reference = $referenceBiller;
                }
                $data['reference_no'] = $reference;
                $ref = explode("-", $reference);
                $consecutive = $ref[1];
                $this->db->insert('stock_counts', $data);
                $count_id = $this->db->insert_id();
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            } else {
                $this->db->update('stock_counts', ['category_id' => $category_id, 'brand_id' => $brand_id], ['id' => $count_id]);
            }
            $condiciones = ['discontinued' => 0, 'attributes' => 0];
            if ($category_id) {
                $condiciones['products.category_id'] = $category_id;
            }
            if ($brand_id) {
                $condiciones['products.brand'] = $brand_id;
            }
            $this->db->select('
                    products.id,
                    products.code,
                    products.name,
                    COALESCE('.$this->db->dbprefix('stock_count_items').'.counted, NULL) AS counted,
                    stock_count_items.id as scid
                ')
                    ->join('stock_count_items', 'stock_count_items.product_id = products.id AND stock_count_items.stock_count_id = '.$count_id, ($show_only_affected ? 'inner' : 'left'))
                    ->where($condiciones);
            $q = $this->db->order_by('name', 'asc')->get('products');
            $html = "";
            $num = 1;
            if ($q->num_rows() > 0) {
                $rows = $q->result();
                foreach ($rows as $row) {
                    $html .="<tr>";
                    $html .="<td style='width:70%;'>".$row->name." - ".$row->code."</td>";
                    $html .="<td style='width:30%;'><input class='form-control ectquantity' name='quantity[]' value='".$row->counted."' data-scid='".$row->scid."' data-productid='".$row->id."' tabindex='".$num."'></td>";
                    $html .="</tr>";
                    $num++;
                }
            } else {
                $html = "<tr><td colspan='2'>Sin resultados<td></tr>";
            }
        }

        
        echo json_encode(['status'=> $can_continue, 'status_message' => $cant_continue_msg, 'count_id' => $count_id, 'html' => $html]);
    }

    public function update_express_count($count_id){

        $product_id = $this->input->get('product_id');
        $count_item_id = $this->input->get('count_item_id');
        $quantity = $this->input->get('quantity');
        $warehouse_id = $this->input->get('warehouse_id');
        $category_id = $this->input->get('category_id') ? $this->input->get('category_id') : NULL;
        $brand_id = $this->input->get('brand_id') ? $this->input->get('brand_id') : NULL;
        $this->db->update('stock_counts', 
            [
                'category_id' => $category_id, 
                'brand_id' => $brand_id,
                'type' => ($category_id || $brand_id ? 'express_partial' : 'express'),
            ], ['id' => $count_id]);
        if ($count_item_id) {
            $this->db->update('stock_count_items', ['counted' => $quantity], ['id' => $count_item_id]);
        } else {
            $product = $this->products_model->getProductByID($product_id);
            $product_wh_qty = $this->products_model->getProductQuantity($product_id, $warehouse_id);
            $to_adjust_qty = $quantity - ($product_wh_qty && $product_wh_qty['quantity'] != NULL ? $product_wh_qty['quantity'] : 0);
            $insert_data = [
                    'stock_count_id' => $count_id,
                    'product_id' => $product_id,
                    'product_code' => $product->code,
                    'product_name' => $product->name,
                    'product_variant' => NULL,
                    'product_variant_id' => NULL,
                    'expected' => $product_wh_qty && $product_wh_qty['quantity'] != NULL ? $product_wh_qty['quantity'] : 0,
                    'counted' => $quantity,
                    'cost' => ($to_adjust_qty >= 0 ? $product->cost : ($product_wh_qty && $product_wh_qty['avg_cost'] != NULL && $product_wh_qty['avg_cost'] != 0 ? $product_wh_qty['avg_cost'] : $product->cost)),
                ];
            $this->db->insert('stock_count_items', $insert_data);
        }
        echo json_encode(['status' => true]);
    }

    public function delete_express_count($count_id){
        $this->db->delete('stock_counts', ['id' => $count_id]);
        $this->db->delete('stock_count_items', ['stock_count_id' => $count_id]);
        echo json_encode(['status' => true]);
    }
    public function finish_express_count($count_id){
        if ($this->products_model->finish_express_count($count_id)) {
            echo json_encode(['status' => true]);
        } else {
            echo json_encode(['status' => false]);
        }
    }

    public function print_express_count($count_id = NULL)
    {
        $stock_count = $this->products_model->getStouckCountByID($count_id);
        $this->data['stock_count'] = $stock_count;
        $this->data['stock_count_items'] = $this->products_model->getStockCountItems($count_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($stock_count->warehouse_id);
        $biller_id = $stock_count->biller_id;
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);
        $this->data['created_by'] = $this->site->getUser($stock_count->created_by);
        $this->data['page_title'] = $this->lang->line("express_count");
        $this->data['count_id'] = $count_id;
        $this->data['document_type'] = $this->site->getDocumentTypeById($stock_count->document_type_id);
        $url_format = "products/print_express_count";
        $this->load_view($this->theme.$url_format, $this->data);
    }
}