<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<style type="text/css" media="screen">
    <?php if ($Owner || $Admin || $this->GP['products-cost']) { ?>
        #PRData td:nth-child(9) {
            text-align: right;
        }
    <?php } ?>
    <?php if ($Owner || $Admin || $this->GP['products-price']) { ?>
        #PRData td:nth-child(8) {
            text-align: right;
        }
    <?php } ?>
</style>
<div class="wrapper wrapper-content animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="no-print notifications_container"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('products/index', 'id="productFilterForm"'); ?>
            <div class="ibox">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="biller_id"><?= $this->lang->line('biller') ?></label>
                                        <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                        <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                        <select class="form-control" id="biller_id" name="biller_id">
                                            <option value=""><?= $this->lang->line('allsf') ?></option>
                                            <?php foreach ($billers as $biller) : ?>
                                                <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="warehouse_id"><?= $this->lang->line('warehouse') ?></label>
                                        <?php $warehouseId = (isset($_POST["warehouse_id"])) ? $_POST["warehouse_id"] : ''; ?>
                                        <?php $warehouseId = (empty($warehouseId)) ? $this->session->userdata('warehouse_id') : $warehouseId; ?>
                                        <select class="form-control" id="warehouse_id" name="warehouse_id">
                                            <option value=""><?= $this->lang->line('allsf') ?></option>
                                            <?php foreach ($warehouses as $warehouse) : ?>
                                                <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouseId) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="type"><?= $this->lang->line('type') ?></label>
                                        <?php $type = (isset($_POST["type"])) ? $_POST["type"] : ''; ?>
                                        <select class="form-control" id="type" name="type">
                                            <option value=""><?= $this->lang->line('alls') ?></option>
                                            <?php foreach ($typesProducts as $typeProduct) : ?>
                                                <option value="<?= $typeProduct->id ?>" <?= ($typeProduct->id == $type) ? 'selected' : '' ?>><?= $typeProduct->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="brand"><?= $this->lang->line('brand') ?></label>
                                        <?php $brandId = (isset($_POST["brand"])) ? $_POST["brand"] : ''; ?>
                                        <select class="form-control" id="brand" name="brand">
                                            <option value=""><?= lang('allsf') ?></option>
                                            <?php foreach ($brands as $brand) : ?>
                                                <option value="<?= $brand->id ?>" <?= ($brand->id == $brandId) ? 'selected' : '' ?>><?= $brand->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="category"><?= $this->lang->line('category') ?></label>
                                        <?php $categoryId = (isset($_POST["category"])) ? $_POST["category"] : ''; ?>
                                        <select class="form-control" id="category" name="category">
                                            <option value=""><?= lang('allsf') ?></option>
                                            <?php foreach ($categories as $category) : ?>
                                                <option value="<?= $category->id ?>" <?= ($category->id == $categoryId) ? 'selected' : '' ?>><?= ucfirst(strtolower($category->name)) ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="subcategory"><?= $this->lang->line('subcategory') ?></label>
                                        <?php $subcategoryId = (isset($_POST["subcategory"])) ? $_POST["subcategory"] : ''; ?>
                                        <select class="form-control" id="subcategory" name="subcategory">
                                            <option value=""><?= lang('allsf') ?></option>
                                            <?php foreach ($subcategories as $subcategory) : ?>
                                                <option value="<?= $subcategory->id ?>" <?= ($subcategory->id == $subcategoryId) ? 'selected' : '' ?>><?= $subcategory->text ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-without-padding text-center">
                            <div class="new-button-container">
                                <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                <button class="btn btn-primary new-button" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                    <div class="row">
                        <div class="col-sm-12">
                            <h3>Filtros avanzados</h3>
                            <hr>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="tax_rate"><?= $this->lang->line('tax') ?></label>
                                        <?php $taxRate = (isset($_POST["tax_rate"])) ? $_POST["tax_rate"] : ''; ?>
                                        <select class="form-control" id="tax_rate" name="tax_rate">
                                            <option value=""><?= $this->lang->line('alls') ?></option>
                                            <?php foreach ($taxes as $tax) : ?>
                                                <option value="<?= $tax->id ?>" <?= ($tax->id == $taxRate) ? 'selected' : '' ?>><?= $tax->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="unit"><?= $this->lang->line('units') ?></label>
                                        <?php $unitId = (isset($_POST["unit"])) ? $_POST["unit"] : ''; ?>
                                        <select class="form-control" id="unit" name="unit">
                                            <option value=""><?= $this->lang->line('allsf') ?></option>
                                            <?php foreach ($units as $unit) : ?>
                                                <option value="<?= $unit->id ?>" <?= ($unit->id == $unitId) ? 'selected' : '' ?>><?= ucfirst(strtolower($unit->code)) ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="hidden"><?= $this->lang->line('hidden') ?></label>
                                        <?php
                                        $hiddenSales = $hiddenPos = $hiddenStoreInLine = '';
                                        if (isset($_POST["hidden"])) {
                                            foreach ($_POST["hidden"] as $hidden) {
                                                if ($hidden == 1) {
                                                    $hiddenSales = 'selected';
                                                }
                                                if ($hidden == 2) {
                                                    $hiddenPos = 'selected';
                                                }
                                                if ($hidden == 3) {
                                                    $hiddenStoreInLine = 'selected';
                                                }
                                            }
                                        }
                                        ?>
                                        <select class="form-control" id="hidden" name="hidden[]" multiple placeholder="<?= $this->lang->line('none') ?>">
                                            <option value="1" <?= $hiddenSales ?>><?= lang('Ventas') ?></option>
                                            <option value="2" <?= $hiddenPos ?>><?= lang('POS') ?></option>
                                            <option value="3" <?= $hiddenStoreInLine ?>><?= lang('Tienda en linea') ?></option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="has_multiple_units" style="margin-top: 25px">
                                            <?php $hasMultipleUnitsId = (isset($_POST["has_multiple_units"])) ? $_POST["has_multiple_units"] : ''; ?>
                                            <input type="checkbox" name="has_multiple_units" id="has_multiple_units" <?= (!empty($hasMultipleUnitsId) ? 'checked' : '') ?>> <?= $this->lang->line('has_multiple_units') ?>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="has_variants" style="margin-top: 25px">
                                            <?php $hasVariantsId = (isset($_POST["has_variants"])) ? $_POST["has_variants"] : ''; ?>
                                            <input type="checkbox" name="has_variants" id="has_variants" <?= (!empty($hasVariantsId) ? 'checked' : '') ?>> <?= $this->lang->line('variants') ?>
                                        </label>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="has_preferences" style="margin-top: 25px">
                                            <?php $hasPreferencesId = (isset($_POST["has_preferences"])) ? $_POST["has_preferences"] : ''; ?>
                                            <input type="checkbox" name="has_preferences" id="has_preferences" <?= (!empty($hasPreferencesId) ? 'checked' : '') ?>> <?= $this->lang->line('preferences') ?>
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <?php if ($Settings->allow_advanced_search == 1) { ?>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <?= form_label(lang("product_detail"), "product_detail"); ?>
                                            <?php $productDetail = (isset($_POST["product_detail"])) ? $_POST["product_detail"] : ''; ?>
                                            <?= form_input(["class" => "form-control", "name" => "product_detail", "id" => "product_detail", "value" => (isset($productDetail)) ? $productDetail : ""]) ?>
                                        </div>
                                    </div>
                                </div>
                            <?php } else { ?>
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <?= form_label(lang("store_synchronization_status"), "synchronized_store") ?>
                                            <?php
                                            $syncOpts = [
                                                "" => lang("alls"),
                                                1  => lang("not_synchronized"),
                                                2  => lang("synchronized"),
                                            ];
                                            $synchronizedStore = (isset($_POST["synchronized_store"])) ? $_POST["synchronized_store"] : '';
                                            ?>
                                            <?= form_dropdown(["name" => "synchronized_store", "id" => "synchronized_store", "class" => "form-control"], $syncOpts, $synchronizedStore) ?>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <?= form_label($this->lang->line('variants'), 'variants') ?>
                                            <?php $variantsIds = (isset($_POST["variants"])) ? $_POST["variants"] : ''; ?>
                                            <?php
                                            $variantsOptions = [];
                                            if (!empty($variants)) {
                                                foreach ($variants as $variant) {
                                                    $variantsOptions[$variant->name] = $variant->name;
                                                }
                                            }
                                            ?>
                                            <?= form_dropdown(["name" => "variants[]", "id" => "variants", "class" => "form-control select", "multiple" => TRUE, "tabindex" => $this->sma->set_tabindex(false)], $variantsOptions, $variantsIds) ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang("reference", "suggest_reference"); ?>
                                            <?php echo form_input('sreference', (isset($_POST['sreference']) ? $_POST['sreference'] : ""), 'class="form-control" id="suggest_reference"'); ?>
                                            <input type="hidden" name="reference" value="<?= isset($_POST['reference']) ? $_POST['reference'] : "" ?>" id="report_reference" />
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= form_close() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <?php if ($Owner || $Admin || isset($GP['bulk_actions'])) { ?>
                            <?= admin_form_open('products/product_actions' . ($warehouse_id ? '/' . $warehouse_id : ''), 'id="action-form"'); ?>
                        <?php } ?>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="PRData" class="table table-condensed table-hover">
                                    <thead>
                                        <tr class="primary">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th style="min-width:40px; width: 40px; text-align: center;"><?php echo $this->lang->line("image"); ?></th>
                                            <th><?= lang("product_list_code") ?></th>
                                            <th><?= lang("code") ?></th>
                                            <th><?= lang("name") ?></th>
                                            <th><?= lang("type") ?></th>
                                            <th><?= lang("brand") ?></th>
                                            <th><?= lang("category") ?></th>
                                            <th><?= lang("tax") ?></th>
                                            <?php
                                            if ($Owner || $Admin) {
                                                echo '<th>' . lang("product_cost") . '</th>';
                                                echo '<th>' . lang("price") . '</th>';
                                                echo '<th>' . lang("quantity") . '</th>';
                                            } else {
                                                if ($this->GP['products-cost']) {
                                                    echo '<th>' . lang("product_cost") . '</th>';
                                                }
                                                if ($this->GP['products-price']) {
                                                    echo '<th>' . lang("price") . '</th>';
                                                }
                                                if ($this->GP['products-quantity']) {
                                                    echo '<th>' . lang("quantity") . '</th>';
                                                }
                                            }
                                            ?>
                                            <th><?= lang("unit") ?></th>
                                            <th><?= lang("rack") ?></th>
                                            <th><?= lang("alert") ?></th>
                                            <th><?= lang("details"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th style="min-width:65px; text-align:center;"><?= lang("actions") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
                            <div style="display: none;">
                                <input type="hidden" name="form_action" value="" id="form_action" />
                                <input type="hidden" name="form_action_warehouse_id" value="" id="form_action_warehouse_id" />
                                <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
                            </div>
                            <?= form_close() ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        _tabFilterFill = false;

        preLoadUserData()
        loadDataTables()

        $(document).on('click', '#clean_filters_button', function() { clean_filters(); });
        $(document).on('change', '#biller_id', function() { loadWarehouses($(this)); });
        $(document).on('change', '#category', function() { loadSubcategories($(this)); });
        $(document).on('click', '#slugify', function() { confirmSlugfy() });
        $(document).on('click', '#syncProductsStore', confirmSyncProductsStore);
        $(document).on('click', '#syncProductQuantitiesStore', confirmSyncProductQuantitiesStore);
        $(document).on('click', '#hideProductWithoutPhotoInStore', confirmHideProductWithoutPhotoInStore);

        $('[data-toggle="tooltip"]').tooltip();

        $("#suggest_reference").autocomplete({
            source: function(request, response){
                $.ajax({
                    type: 'get',
                    url: site.base_url+'products/reference_suggestions',
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            select: function (event, ui) {
                $('#report_reference').val(ui.item.id);
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {
                if (ui.content != null && ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).val(ui.item.label);
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                } else if(ui.content == null ){
                    command: toastr.error('No se encontraron coincidencias', 'Sin resultados', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                }
            },
            clear : true,
        });
        $(document).on('blur', '#suggest_reference', function(e) {
            if (! $(this).val()) {
                $('#report_reference').val('');
            }
        });
    });

    function preLoadUserData() {
        <?php if ($this->session->userdata('biller_id')) { ?>
            setTimeout(function() {
                $('#biller_id').select2('readonly', true).trigger('change');
            }, 850);
        <?php } ?>

        <?php if ($this->session->userdata('warehouse_id')) { ?>
            setTimeout(function() {
                $('#warehouse_id').select2('readonly', true).trigger('change');
            }, 850);
        <?php } ?>
    }

    var oTable;
    function loadDataTables()
    {
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, 1000, -1], [10, 25, 50, 100, 500, 1000, "<?= lang('all') ?>"]];
        }

        oTable = $('#PRData').dataTable({
            aaSorting: [[4, "asc"]],
            stateSave: true,
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            bProcessing: true,
            bServerSide: true,
            sAjaxSource: '<?= admin_url('products/getProducts'); ?>',
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            fnServerData: function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "biller_id",
                    "value": <?= $this->session->userdata('biller_id') ? $this->session->userdata('biller_id') : "$('#biller_id').val()" ?>
                }, {
                    "name": "warehouse_id",
                    "value": <?= $this->session->userdata('warehouse_id') ? $this->session->userdata('warehouse_id') : "$('#warehouse_id').val()" ?>
                }, {
                    "name": "type",
                    "value": $('#type').val()
                }, {
                    "name": "brand",
                    "value": $('#brand').val()
                }, {
                    "name": "category",
                    "value": $('#category').val()
                }, {
                    "name": "subcategory",
                    "value": $('#subcategory').val()
                }, {
                    "name": "tax_rate",
                    "value": $('#tax_rate').val()
                }, {
                    "name": "unit",
                    "value": $('#unit').val()
                }, {
                    "name": "hidden",
                    "value": $('#hidden').val()
                }, {
                    "name": "has_multiple_units",
                    "value": $('#has_multiple_units').is(':checked') ? 1 : 0
                }, {
                    "name": "has_variants",
                    "value": $('#has_variants').is(':checked') ? 1 : 0
                }, {
                    "name": "has_preferences",
                    "value": $('#has_preferences').is(':checked') ? 1 : 0
                }, {
                    "name": "product_detail",
                    "value": $('#product_detail').val()
                }, {
                    "name": "synchronized_store",
                    "value": $('#synchronized_store').val()
                }, {
                    "name": "variants",
                    "value": $('#variants').val()
                }, {
                    "name": "option_filter",
                    "value": option_filter
                },
                {
                    "name": "reference",
                    "value": $('#report_reference').val()
                }
            );

                $.ajax({dataType: 'json', type: 'POST', url: sSource, data: aoData, success: fnCallback});
            },
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "product_link";

                return nRow;
            },
            fnDrawCallback: function() {
                var addButton = editOptions = deleteOptions =  storeProTitle =  warehouseOption = slugOption = syncProductsStore = syncProductsQuantitiesStore = hideProductsWithoutPhotoInStore = '' ;

                if (!_tabFilterFill) {
                    $('.additionalControlsContainer').html('<div class="wizard">' +
                        '<div class="steps clearfix">' +
                            '<ul role="tablist">' +
                                '<li role="tab" class="first current index_list" aria-disabled="false"  aria-selected="true" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="active" href="#">'+
                                        '<span class="active_span">0</span><br><?= lang('active_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" style="width:16.66%;">' +
                                    '<a class="wizard_index_step" data-optionfilter="all" aria-controls="edit_biller_form-p-0">' +
                                        '<span class="all_span">0</span><br><?= lang('all_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="inactive" href="#"><span class="inactive_span">0</span><br><?= lang('inactive_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="featured" href="#"><span class="featured_span">0</span><br><?= lang('featured_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="promotion" href="#"><span class="promotion_span">0</span><br><?= lang('promotion_products') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.66%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="new" href="#"><span class="new_span">0</span><br><?= lang('new_products') ?>' +
                                    '</a>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>').addClass('col-without-padding');

                    _tabFilterFill = true;
                }

                <?php if ($this->Owner || $this->Admin || $GP['products-add']) : ?>
                    addButton = '<a href="<?= admin_url('products/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>';
                <?php endif ?>

                <?php if ($this->Owner || $this->Admin) : ?>
                    storeProTitle = `<li class="divider"></li>
                    <li><label style="padding-left: 20px; padding-top: 10px">Tienda PRO</label></li>`
                    syncProductsStore = '<li><a id="syncProductsStore"><i class="fas fa-sync"></i> <?= lang("sync_products_store") ?></a></li>';
                    syncProductsQuantitiesStore = '<li><a id="syncProductQuantitiesStore"><i class="fas fa-sync"></i> <?= lang("sync_products_quantities_store") ?></a></li>';
                    hideProductsWithoutPhotoInStore = '<li><a id="hideProductWithoutPhotoInStore"><i class="fas fa-sync"></i> <?= lang("hide_products_without_photo_in_store") ?></a></li>';
                <?php elseif ($GP["products-sync_pro_store_products"] ||
                    $GP["products-sync_pro_store_prices_quantities"] ||
                    $GP["products-hide_products_without_photos"]) : ?>

                    storeProTitle = `<li class="divider"></li>
                    <li><label style="padding-left: 20px; padding-top: 10px">Tienda PRO</label></li>`

                    <?php if ($GP["products-sync_pro_store_products"]) : ?>
                        syncProductsStore = '<li><a id="syncProductsStore"><i class="fas fa-sync"></i> <?= lang("sync_products_store") ?></a></li>';
                    <?php endif ?>
                    <?php if ($GP["products-sync_pro_store_prices_quantities"]) : ?>
                        syncProductsQuantitiesStore = '<li><a id="syncProductQuantitiesStore"><i class="fas fa-sync"></i> <?= lang("sync_products_quantities_store") ?></a></li>';
                    <?php endif ?>
                    <?php if ($GP["products-hide_products_without_photos"]) : ?>
                        hideProductsWithoutPhotoInStore = '<li><a id="hideProductWithoutPhotoInStore"><i class="fas fa-sync"></i> <?= lang("hide_products_without_photo_in_store") ?></a></li>';
                    <?php endif ?>
                <?php endif ?>

                <?php if (!$warehouse_id) { ?>
                    warehouseOption = '<li><a href="<?= admin_url('products/update_price') ?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-pencil"></i> <?= lang('update_price') ?></a></li>';
                <?php } ?>
                <?php if ($this->Owner) : ?>
                    slugOption = '<li><a id="slugify"><i class="fa fa-magic"></i> <?= lang('auto_slugify') ?></a></li>';
                <?php endif ?>

                <?php if ($this->Owner || $this->Admin || $GP['products-edit']) : ?>
                    editOptions = warehouseOption;
                    editOptions +=  '<li>'+
                                        '<a href="<?= admin_url('products/import_price_promotion'); ?>" data-toggle="modal" data-target="#myModal">'+
                                            '<i class="fa fa-plus-circle"></i> <?= lang("import_price_promotion_by_csv"); ?>'+
                                        '</a>'+
                                    '</li>';
                    editOptions += '<li>'+
                                        '<a href="#" id="sync_quantity" data-action="set_featuring"><i class="fa fa-star-o"></i> <?= lang('set_featuring') ?></a>'+
                                    '</li>';
                    editOptions += '<li>'+
                                        '<a href="#" id="sync_quantity" data-action="set_status"><i class="fa-solid fa-eye-low-vision"></i> <?= lang('set_status') ?></a>'+
                                    '</li>';
                <?php endif ?>

                <?php if ($this->Owner || $this->Admin || $GP['products-delete']) : ?>
                     deleteOptions +=  '<li>'+
                                                '<a href="#" id="delete" data-action="delete"><i class="fa fa-trash-o"></i> <?= lang('delete_products') ?></a>'+
                                            '</li>';
                <?php endif ?>

                $('.actionsButtonContainer').html(addButton +
                '<div class="pull-right dropdown">'+
                    '<button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                    '<ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">'+
                        editOptions +
                        '<li>'+
                            '<a href="#" id="labelProducts" data-action="labels"><i class="fa fa-print"></i> <?= lang('print_barcode_label') ?></a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="sync_quantity" data-action="sync_quantity"><i class="fa fa-sync"></i> <?= lang('sync_quantity') ?></a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="<?= admin_url('products/process_new_product_images') ?>" id="" data-action=""><i class="fa fa-sync"></i> <?= lang('process_new_product_images') ?> <i class="fa fa-question-circle" data-toggle-second="tooltip" data-placement="bottom" title="Tomar imágenes de la ruta \'assets/images/new_product_images\' con nombre con estructura \' CODIGO_NUMIMAGEN (Ej: 098010_1)\' y asignar a productos ¡NO AFECTA TIENDA PRO!"></i> </a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="<?= admin_url('products/process_new_product_images_tienda_pro') ?>" id="" data-action=""><i class="fa fa-sync"></i> <?= lang('process_new_product_images_tienda_pro') ?> <i class="fa fa-question-circle" data-toggle-second="tooltip" data-placement="bottom" title="Tomar imágenes de la ruta \'assets/images/new_tiendapro_images\' con nombre con estructura \' CODIGO_NUMIMAGEN (Ej: 098010_1)\' y asignar a productos ¡SI AFECTA TIENDA PRO!"></i></a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="excel" data-action="sync_tienda_pro_to_erp_images"><i class="fa-solid fa-download"></i> <?= lang('sync_tienda_pro_to_erp_images') ?> <i class="fa fa-question-circle" data-toggle-second="tooltip" data-placement="bottom" title="A los productos seleccionados, se le revisan las imágenes en tienda Pro. Si el producto en ERP ya tiene imagen asignada, no se le realiza ningún cambio, esto aplica tanto para imagen principal cómo para imágenes segundarias"></i></a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="all_excel" data-action="export_all_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_all_products_to_excel') ?></a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="all_excel" data-action="export_all_excel_variants"><i class="fa fa-file-excel-o"></i> <?= lang('export_all_products_to_excel_variants') ?></a>'+
                        '</li>'+
                        '<li>'+
                            '<a href="#" id="excel" data-action="download_products_images"><i class="fa  fa-download"></i> <?= lang('download_selected_products_images') ?></a>'+
                        '</li>'+
                        deleteOptions+
                        slugOption +
                        storeProTitle +
                        syncProductsStore +
                        syncProductsQuantitiesStore +
                        hideProductsWithoutPhotoInStore +
                    '</ul>'+
                '</div>');

                setTimeout(function() {
                    $('[data-toggle-second="tooltip"]').tooltip();
                }, 850);

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });

                loadDataTabFilters();

            },
            aoColumns: [{
                    "bSortable": false,
                    "mRender": checkbox
                },
                {
                    "bSortable": false,
                    "mRender": img_hl
                },
                null,
                {
                    "bVisible": false
                },
                null,
                {
                    "mRender": paid_by
                },
                null,
                null,
                null,
                <?php
                if ($Owner || $Admin) {
                    echo '{"mRender": currencyFormat}, {"mRender": currencyFormat},{"mRender": formatQuantity},';
                } else {
                    if ($this->GP['products-cost']) {
                        echo '{"mRender": currencyFormat},';
                    }
                    if ($this->GP['products-price']) {
                        echo '{"mRender": currencyFormat},';
                    }
                    if ($this->GP['products-quantity']) {
                        echo '{"mRender": formatQuantity},';
                    }
                }
                ?>
                null,
                <?php if (!$warehouse_id || !$Settings->racks) {
                    echo '{"bVisible": false},';
                } else {
                    echo '{"bSortable": true},';
                } ?> {
                    "mRender": formatQuantity
                },
                <?php if ($Settings->allow_advanced_search == "1") {
                    echo '{"bVisible": true},';
                } else {
                    echo '{"bVisible": false},';
                } ?> {
                    "mRender": discontinued_status
                },
                {
                    "bSortable": false
                },
            ]
        });
    }

    function loadDataTabFilters()
    {
        let variantss = ($('#variants').val()) ? $('#variants').val().join(',') : ''
        $.ajax({
            url: '<?= admin_url('products/getProducts'); ?>',
            dataType: 'JSON',
            type: 'POST',
            data: {
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                biller_id: $('#biller_id').val(),
                warehouse_id: $('#warehouse_id').val(),
                type: $('#type').val(),
                brand: $('#brand').val(),
                category: $('#category').val(),
                subcategory: $('#subcategory').val(),
                tax_rate: $('#tax_rate').val(),
                unit: $('#unit').val(),
                hidden: $('#hidden').val(),
                synchronized_store: $('#synchronized_store').val(),
                has_multiple_units: $('#has_multiple_units').is(':checked') ? 1 : 0,
                has_variants: $('#has_variants').is(':checked') ? 1 : 0,
                has_preferences: $('#has_preferences').is(':checked') ? 1 : 0,
                product_detail: "<?= (isset($product_detail) ? $product_detail : ''); ?>",
                option_filter: option_filter,
                variants: variantss,
                reference : $('#report_reference').val(),
                get_json: true
            }
        }).done(function(data) {
            $('.all_span').text(data.all);
            $('.new_span').text(data.new);
            $('.active_span').text(data.active);
            $('.inactive_span').text(data.inactive);
            $('.featured_span').text(data.featured);
            $('.promotion_span').text(data.promotion);
        });
    }

    // function clean_filters() {
    //     $('#product_name').val("");
    //     $('#product_detail').val("");
    // }

    function loadWarehouses(element)
    {
        if (element.val()) {
            var defaultwh = element.children("option:selected").data('defaultwh');
            $('#warehouse_id').select2('val', defaultwh).trigger('change');
        } else {
            $('#warehouse_id option').each(function(index, option) {
                $(option).attr('disabled', false);
            });
            $('#warehouse_id').select2('val', '').trigger('change');
        }
    }

    function loadSubcategories(element)
    {
        var categoryId = element.val();
        optionsSelect = '<option value=""><?= $this->lang->line('allsf') ?></option>';
        if (categoryId) {
            $.ajax({
                type: "get",
                async: true,
                url: "<?= admin_url('products/getSubCategories') ?>/" + categoryId,
                dataType: "json",
                success: function(response) {
                    if (response != null) {
                        response.forEach(option => {
                            optionsSelect += '<option value="' + option.id + '">' + option.text + '</option>';
                        });
                    }
                    $("#subcategory").html(optionsSelect);
                    $('#subcategory').select2('val', '');
                },
                error: function() {
                    bootbox.alert('<?= lang('ajax_error') ?>');
                    $('#modal-loading').hide();
                }
            });
        } else {
            optionsSelect = '<option value=""><?= $this->lang->line('allsf') ?></option>';
            $("#subcategory").html(optionsSelect);
            $('#subcategory').select2('val', '');
        }
    }

    function confirmSlugfy()
    {
        bootbox.confirm({
            message: "¿Está seguro de realizar este proceso?, esto afecta todos los productos",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-success send-submit-sale btn-full'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger btn-full'
                }
            },
            callback: function(result) {
                if (result) {
                    location.href = '<?= admin_url('shop_settings/slugify') ?>';
                }
            }
        });
    }

    function confirmSyncProductsStore()
    {
        productIds = validateSelectedProducts();

        if (productIds != false) {
            swal({
                title: '¡Sincronización en curso',
                text: "Por favor espere un momento mientras se realiza la sincronización",
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            $.ajax({
                type: "post",
                url: site.base_url + 'products/syncStore',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'productIds': productIds,
                },
                dataType: "json",
                success: function(response) {
                    swal.close();
                    let alert = '';

                    if (response.length !== 0) {
                        $.each(response, function(type, message) {
                            if (type == 'error') {
                                alert += '<div class="alert alert-' + type + ' alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' + message + '</div>';
                            }

                            if (type == 'warning') {
                                alert += '<div class="alert alert-' + type + ' alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' + message + '</div>';
                            }
                        });
                    } else {
                        alert += '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>¡Sincronización completada!</div>';
                    }

                    $('.notifications_container').html(alert);
                }
            });
        }
    }

    function confirmSyncProductQuantitiesStore()
    {
        productIds = validateSelectedProducts();

        if (productIds != false) {
            swal({
                title: '¡Sincronización en curso',
                text: "Por favor espere un momento mientras se realiza la sincronización de Precios y Cantidades",
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false,
            });

            syncProductQuantitiesStore();
        }
    }

    function syncProductQuantitiesStore()
    {
        $.ajax({
            type: "post",
            url: site.base_url + 'products/syncProductQuantitiesStore',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'productIds': productIds,
            },
            dataType: "json",
            success: function(response) {
                swal.close();
                let alert = '';

                if (response.length !== 0) {
                    $.each(response, function(type, message) {
                        if (type == 'error') {
                            alert += '<div class="alert alert-' + type + ' alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' + message + '</div>';
                        }

                        if (type == 'warning') {
                            alert += '<div class="alert alert-' + type + ' alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' + message + '</div>';
                        }
                    });
                } else {
                    alert += '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>¡Sincronización completada!</div>';
                }

                $('.notifications_container').html(alert);
            }
        });
    }

    function confirmHideProductWithoutPhotoInStore()
    {
        swal({
            title: '¡Sincronización en curso',
            text: "Por favor, espere un momento mientras se ocultan los productos sin foto",
            type: 'info',
            showCancelButton: false,
            showConfirmButton: false,
            closeOnClickOutside: false,
        });
        hideProductWithoutPhotoInStore();
    }

    function hideProductWithoutPhotoInStore()
    {
        $.ajax({
            type: "post",
            url: site.base_url + 'products/hideProductWithoutPhotoInStore',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>'
            },
            dataType: "json",
            success: function(response) {
                swal.close();
                let alert = '';
                if (response.length !== 0) {
                    $.each(response, function(type, message) {
                        if (type == 'error') {
                            alert += '<div class="alert alert-' + type + ' alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' + message + '</div>';
                        }
                        if (type == 'warning') {
                            alert += '<div class="alert alert-' + type + ' alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>' + message + '</div>';
                        }
                    });
                } else {
                    alert += '<div class="alert alert-success alert-dismissable"><button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>¡Sincronización completada!</div>';
                }
                $('.notifications_container').html(alert);
            }
        });
    }

    function validateSelectedProducts() {
        let productIds = [];
        $('input[name="val[]"]').each(function() {
            if ($(this).is(":checked")) {
                productIds.push($(this).val());
            }
        });

        if (productIds.length === 0) {
            swal("Advertencia!", "No ha seleccionado productos para sincronizar.", "warning");
            return false;
        }

        return productIds;
    }
</script>