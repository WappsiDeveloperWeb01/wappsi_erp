<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <?= admin_form_open("warranty/createActivity/$warranty->id", ['id'=>'add_deposit', 'data-toggle' => 'validator', 'role' => 'form']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h3 class="modal-title" id="addDepositLabel">
                    <?= lang('add_activity'); ?>
                </h3>
                <br>
                <h2><small><?= "$warranty->reference_no - $warranty->productName" ?></small></h2>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("date"), "date") ?>
                            <input type="text" name="date" id="date" class="form-control datetime" value="<?= date("d/m/Y H:i") ?>" required>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("warranty_activity"), "warranty_activity_id"); ?>
                            <select name="warranty_activity_id" id="warranty_activity_id" class="form-control" required>
                                <option value=""><?= lang('select') ?></option>
                                <?php foreach ($activities as $activity) { ?>
                                    <option value="<?= $activity->id ?>" status="<?= $activity->triggered_status ?>" action="<?= $activity->triggered_action ?>"><?= $activity->description ?></option>
                                <?php } ?>
                            </select>
                            <?= form_hidden("warranty_activity_status") ?>
                            <?= form_hidden("warranty_activity_action") ?>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("description"), "description") ?>
                            <?= form_textarea(["name"=>"description", "id"=>"description", "class"=>"form-control", "required"=>true]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary add_deposit" type="submit"><?= lang('add') ?></button>
            </div>
        <?= form_close(); ?>
    </div>
</div>
<?= $modal_js ?>

<script>
    $(document).ready(function () {
        $(document).on('change', '#date', function () { validateDate($(this)) });
        $(document).on('change', '#warranty_activity_id', function () { getActivityStatus($(this)) });
    });

    function validateDate(element)
    {
        if (element.val()) {
            let parts = element.val().split(' ');
            let dateParts = parts[0].split('/');
            let hourParts = parts[1].split(':');
            let date1 = new Date(dateParts[2], dateParts[1] - 1, dateParts[0], hourParts[0], hourParts[1]);
            let date2 = new Date('<?= $warranty->date ?>');

            if (date2 > date1) {
                header_alert('warning', 'La fecha de la actividad debe ser mayor o igual a la fecha de creación de la garantía', 3, () => {
                    element.val('<?= $warranty->date ?>');
                });
            }
        }
    }

    function getActivityStatus(element)
    {
        if (element.val()) {
            let status = element.find('option:selected').attr('status');
            let action = element.find('option:selected').attr('action');
            $('input[name="warranty_activity_status"]').val(status);
            $('input[name="warranty_activity_action"]').val(action);
        }
    }
</script>
