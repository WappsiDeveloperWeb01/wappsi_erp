<?php
/***********
FACTURA DE VENTA CON DATOS DE LA EMPRESA, DE ACUERDO A LO PARAMETRIZADO EN EL TIPO DE DOCUMENTO SE DISCRIMINA O NO EL IVA
************/
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';
#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Factura de venta'));
        //izquierda
        if (file_exists('assets/uploads/logos/'.$this->logo)) {
            if ($this->biller_logo == 2) {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,27,7,28);
            } else {
                $this->Image(base_url().'assets/uploads/logos/'.$this->logo,14,12,55);
            }
        }
        if ($this->show_document_type_header == 1) {
            $this->SetFont('Arial','',$this->fuente);
            $this->setXY(13, 36);
            $this->MultiCell(123, 3,  $this->sma->utf8Decode($this->crop_text($this->invoice_header, 170)), 0, 'L');
        }
        $cx = 75;
        $cy = 10;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->MultiCell(75, 3.5,  $this->sma->utf8Decode(mb_strtoupper($this->biller->company != '-' ? $this->biller->company : $this->biller->name)), '', 'L');
        $cy  = $this->getY();
        $cy+=1.3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->tipo_documento == 6 ? "-".$this->biller->digito_verificacion : '')."  ".$this->tipo_regimen),'',1,'L');
        if ($this->ciiu_code) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode("Código CIIU : ".$this->ciiu_code->code),'',1,'L');
        }
        if ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode(
                                                ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? 'Auto retenedor de ' : '').
                                                ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                            ),'',1,'L');
        }
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(isset($this->Settings->url_web) ? $this->Settings->url_web : ''),'',1,'L');
        //derecha
        $this->SetFont('Arial','B',$this->fuente+(5));
        $this->RoundedRect(138, 10, 71, 31, 3, '', '');
        $cx = 140;
        $cy = 12;
        $this->setXY($cx, $cy);
        $this->MultiCell(40, 4,  $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'ORDEN DE PEDIDO'), '', 'L');
        $cy +=5;
        $this->setXY($cx, $cy);
        $this->Cell(77, 4 , $this->sma->utf8Decode("# ".$this->factura->reference_no),'',1,'L');
        $l_w = 18;
        $r_w = 59;
        $cy +=5;
        $this->SetFont('Arial','B',$this->fuente+(1));
        $this->setXY($cx, $cy);
        $this->Cell($l_w, 3 , $this->sma->utf8Decode('Sucursal : '),'',1,'L');
        $this->SetFont('Arial','',$this->fuente+(1));
        $this->setXY($cx+$l_w, $cy);
        $this->Cell($r_w, 3 , $this->sma->utf8Decode($this->biller->name),'',1,'L');
        $cy +=3;
        $this->SetFont('Arial','B',$this->fuente+(1));
        $this->setXY($cx, $cy);
        $this->Cell($l_w, 3 , $this->sma->utf8Decode('Fecha : '),'',1,'L');
        $this->SetFont('Arial','',$this->fuente+(1));
        $this->setXY($cx+$l_w, $cy);
        $this->Cell($r_w, 3 , $this->sma->utf8Decode(date('l, d', strtotime($this->factura->date))." de ".date('F Y h:i a', strtotime($this->factura->date))),'',1,'L');
        $cy +=3;
        $this->SetFont('Arial','B',$this->fuente+(1));
        $this->setXY($cx, $cy);
        $this->Cell($l_w, 3 , $this->sma->utf8Decode('Entrega : '),'',1,'L');
        $this->SetFont('Arial','',$this->fuente+(1));
        $this->setXY($cx+$l_w, $cy);
        $this->Cell($r_w, 3 , $this->sma->utf8Decode(date('l, d', strtotime($this->factura->delivery_day ? $this->factura->delivery_day : $this->factura->date))." de ".date('F Y', strtotime($this->factura->delivery_day ? $this->factura->delivery_day : $this->factura->date))),'',1,'L');
        $cy +=3;
        $this->SetFont('Arial','B',$this->fuente+(1));
        $this->setXY($cx, $cy);
        $this->Cell($l_w, 3 , $this->sma->utf8Decode('Vendedor : '),'',1,'L');
        $this->SetFont('Arial','',$this->fuente+(1));
        $this->setXY($cx+$l_w, $cy);
        $this->Cell($r_w, 3 , $this->sma->utf8Decode((($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'L');
        $cy +=3;
        $this->SetFont('Arial','B',$this->fuente+(1));
        $this->setXY($cx, $cy);
        $this->Cell($l_w, 3 , $this->sma->utf8Decode('Origen : '),'',1,'L');
        $this->SetFont('Arial','',$this->fuente+(1));
        $this->setXY($cx+$l_w, $cy);
        $origen = "Web ERP";
        if ($this->factura->order_sale_origin == 2) {
            $origen = "App Cliente";
        } else if ($this->factura->order_sale_origin == 3) {
            $origen = "App Vendedor";
        } else if ($this->factura->order_sale_origin == 4) {
            $origen = "Tienda en línea";
        }
        $this->Cell($r_w, 3 , $this->sma->utf8Decode($origen),'',1,'L');
        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 43, 89.5, 5, 3, '', '');
        $this->RoundedRect(13, 48, 89.5, 27, 3, '', '');
        $cx = 13;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(89.5, 5 , $this->sma->utf8Decode('Información del cliente'),'',1,'C');
        $cx += 3;
        $cy += 8;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $customer_name = $this->customer->name != '-' ? $this->customer->name : $this->customer->company;
        if ($this->customer->type_person == 2) {
            $customer_name = $this->customer->first_name.($this->customer->second_name != '' ? ' '.$this->customer->second_name : '').($this->customer->first_lastname != '' ? ' '.$this->customer->first_lastname : '').($this->customer->second_lastname != '' ? ' '.$this->customer->second_lastname : '');
        }
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($customer_name)), 0, 'L');
        $this->setXY($cx, $cy+3);
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->MultiCell(115,3,$this->sma->utf8Decode(mb_strtoupper($this->customer->company)), 0, 'L');
        $columns_l = 17;
        $columns_r = 98;
        $cy += 6.5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Nit/Cc : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->vat_no.($this->customer->digito_verificacion != '' ? '-'.$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Sucursal : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->negocio),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Dirección : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->direccion_negocio.", ".(ucfirst(mb_strtolower($this->factura->ciudad_negocio)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Teléfonos : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->factura->phone_negocio),'',0,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_l, 3 , $this->sma->utf8Decode("Correo : "),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell($columns_r, 3 , $this->sma->utf8Decode($this->customer->email),'',1,'L');
        //derecha

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $altura = 5;
        $adicional_altura = 1;
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(102.5, 43, 106.5,  5, 3, '', '');
        $this->RoundedRect(102.5, 48, 106.5, 11, 3, '', '');
        $this->RoundedRect(102.5, 48,  35.5, 11, 3, '', '');
        $this->RoundedRect(138,   48,  35.5, 11, 3, '', '');
        $this->RoundedRect(173.5, 48,  35.5, 11, 3, '', '');
        $cx = 102.5;
        $cy = 43;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Zona/Ruta'),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Barrio'),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Franja Horaria'),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+6);
        $cx -= 71;
        // ZONA
        $cy += $altura;
        $this->setXY($cx, $cy+1);
        $this->SetFont('Arial','B',8);
        $this->MultiCell(35.5,3,$this->sma->utf8Decode($this->factura->zone_name), 0, 'C');
        // BARRIO
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+1);
        $this->Cell(35.5, 11 , $this->sma->utf8Decode($this->factura->subzone_name),'',1,'C');
        // FRANJA HORARIA
        $this->SetFont('Arial','B',$this->fuente+4);
        $cx += 35.5;
        $this->setXY($cx, $cy);
        if ($this->factura->delivery_time_id) {
            $this->Cell(35.5, 11 , $this->sma->utf8Decode(date('h:ia', strtotime($this->factura->time_1))."-".date('h:ia', strtotime($this->factura->time_2))),'',1,'C');
        }

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 35.5;
        $cy += $altura;
        $this->setXY($cx, $cy);

        $this->RoundedRect(102.5, 59, 106.5, 5 , 3, '', '');
        $this->RoundedRect(102.5, 64, 106.5, 11, 3, '', '');
        $this->RoundedRect(102.5, 64, 35.5, 11, 3, '', 'DF');
        $this->RoundedRect(138, 64, 35.5, 11, 3, '', 'DF');
        $this->RoundedRect(173.5, 64, 35.5, 11, 3, '', 'DF');
        $cx = 102.5;
        $cy = 59;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Contenedores'),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Vehículo'),'BR',1,'C');
        $cx += 35.5;
        $this->setXY($cx, $cy);
        $this->Cell(35.5, $altura , $this->sma->utf8Decode('Conductor'),'B',1,'C');

        $this->setXY($cx, 75.5);

        $this->ln(1.7);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->Cell(54,5, $this->sma->utf8Decode('Descripción de producto o Servicio'),'TBLR',0,'C',0);
        $this->Cell(35,5, $this->sma->utf8Decode('Preferencias'),'TBLR',0,'C',0);
        $this->Cell(14.5,5, $this->sma->utf8Decode('Cantidad'),'TBLR',0,'C',0);
        $this->Cell(14,5, $this->sma->utf8Decode('Check'),'TBLR',0,'C',1);
        $unit_price_size = 27.2;
        $this->Cell(12,5, $this->sma->utf8Decode('IVA'),'TBLR',0,'C',0);
        $this->Cell($unit_price_size,5, $this->sma->utf8Decode('Valor Unitario'),'TBLR',0,'C',0);
        $this->Cell(39.2,5, $this->sma->utf8Decode('Valor total'),'TBLR',1,'C',0);
    }

    function Footer()
    {
        $this->SetXY(13, -14);
        $img_x = $this->getX()+125;
        $img_y = $this->getY()+1;
        $this->Cell(190, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS NIT 901.090.070-9     www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Página N° '.$this->PageNo()),'',1,'C');
    }
    function reduceTextToDescription1($text){
        $text=$text;
        if (strlen($text) > 980) {
            $text = substr($text, 0, 975);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
    function reduceTextToDescription2($text){
        if (strlen($text) > 870) {
            $text = substr($text, 0, 865);
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
    function crop_text($text, $length = 50){
        if (strlen($text) > $length) {
            $text = substr($text, 0, ($length-5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = ''){
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }
    function _Arc($x1, $y1, $x2, $y2, $x3, $y3){
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}
$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);
$pdf->SetAutoPageBreak(true, 4);
$fuente = 6.5;
$adicional_fuente = 1.5;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();
$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $this->sma;
$pdf->Settings = $this->Settings;

foreach ($data as $drow_id => $drow) {
    $pdf->biller = $drow['biller'];
    $pdf->logo = $drow['biller_logo'] == 2 ? $drow['biller']->logo_square : $drow['biller']->logo;
    $pdf->show_code = $drow['show_code'];
    $pdf->biller_logo = $drow['biller_logo'];
    $pdf->customer = $drow['customer'];
    $pdf->seller = $drow['seller'];
    $pdf->factura = $drow['inv'];
    $description1 = $drow['inv']->note;
    $pdf->description2 = $drow['invoice_footer'] ? $drow['invoice_footer'] : "";
    $pdf->view_tax = $drow['view_tax'];
    $pdf->tax_inc = $drow['tax_inc'];
    $pdf->document_type = $drow['document_type'];
    $pdf->trmrate = $drow['trmrate'];
    $pdf->ciiu_code = $drow['ciiu_code'];
    $pdf->invoice_header = $drow['invoice_header'] ? $drow['invoice_header'] : "";
    $pdf->show_document_type_header = $drow['show_document_type_header'];
    $pdf->value_decimals = $drow['value_decimals'];
    if (isset($drow['cost_center'])) {
        $pdf->cost_center = $drow['cost_center'];
    }
    $pdf->tipo_regimen = $drow['tipo_regimen'];
    $pdf->AddPage();
    $maximo_footer = 260;
    $pdf->SetFont('Arial','',$fuente);
    $taxes = [];
    foreach ($drow['rows'] as $item) {

        $columns = [];
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $pr_name =  $item->product_name.
                    (!is_null($item->variant) ? "( ".$item->variant." )" : '').
                    ($this->Settings->show_brand_in_product_search ? " - ".$item->brand_name : '').
                    (!is_null($item->serial_no) ? " - ".$item->serial_no : '');


            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $columns[1]['inicio_X'] = $pdf->getX();
        $pdf->setXY($cX, $cY+0.5);
        $pdf->MultiCell(54,3, $this->sma->utf8Decode($pr_name),'','L');
        $columns[1]['fin_X'] = $cX+54;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+54, $cY);


            $cX = $pdf->getX();
            $cY = $pdf->getY();
        $columns[2]['inicio_X'] = $pdf->getX();
        $pdf->setXY($cX, $cY+0.5);
        $pdf->MultiCell(35,3, $this->sma->utf8Decode(strip_tags($this->sma->decode_html(($item->preferences ? ' ' . $this->sma->print_preference_selection($item->preferences) . '' : '')))),'','L');
        $columns[2]['fin_X'] = $cX+35;
            $fin_altura_fila = $pdf->getY() > $fin_altura_fila ? $pdf->getY() : $fin_altura_fila;
            $pdf->setXY($cX+35, $cY);

        $columns[3]['inicio_X'] = $pdf->getX();
        $pdf->Cell(14.5,5, $this->sma->utf8Decode($this->sma->formatQuantity($item->quantity, $drow['qty_decimals'])),'',0,'C');
        $columns[3]['fin_X'] = $pdf->getX();
        $columns[4]['inicio_X'] = $pdf->getX();
        $pdf->Cell(14,5, $this->sma->utf8Decode(''),1,0,'C');
        $columns[4]['fin_X'] = $pdf->getX();
        $unit_price_size = 27.2;
        $columns[5]['inicio_X'] = $pdf->getX();
        $pdf->Cell(12,5, $this->sma->utf8Decode(isset($taxes_details[$item->tax_rate_id]) ? $taxes_details[$item->tax_rate_id] : "Exento"),'',0,'R');
        $columns[5]['fin_X'] = $pdf->getX();
        $columns[6]['inicio_X'] = $pdf->getX();
        $pdf->Cell($unit_price_size,5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $item->unit_price * $drow['trmrate'])),'',0,'R');
        $columns[6]['fin_X'] = $pdf->getX();
        $columns[7]['inicio_X'] = $pdf->getX();
        $pdf->Cell(39.2,5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], ($item->unit_price * $item->quantity) * $drow['trmrate'])),'',0,'R');
        $columns[7]['fin_X'] = $pdf->getX();
            $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
        foreach ($columns as $key => $data) {
            $altura_fila = $altura_fila >= 5 ? $altura_fila : 5;
            $ancho_column = $data['fin_X'] - $data['inicio_X'];
            $cx1 = $pdf->getX();
            $cy1 = $pdf->getY();
            $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R', ($key == 4 ? 1 : 0));
            $cx2 = $pdf->getX();
            $cy2 = $pdf->getY();
            if ($key == 4) {
                $pdf->setXY($cx1, $cy1);
                $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'TBLR',0,'R');
                $pdf->setXY($cx2, $cy2);
            }
        }
        $pdf->ln($altura_fila);


    }

    if ($pdf->getY() > $maximo_footer-50) {
        $pdf->AddPage();
    }
    $cX_items_finished = $pdf->getX();
    $cY_items_finished = $pdf->getY();
    $current_x = $pdf->getX();
    $current_y = $pdf->getY() + 1;
    $pdf->ln(1);
    $pdf->setX(16);
    $pdf->SetFont('Arial','B',$fuente+3);
    $pdf->Cell(115.6,5, $this->sma->utf8Decode('Notas :'),'',1,'L');
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->setX(16);
    $pdf->SetFont('Arial','',$fuente+2);
    $pdf->MultiCell(111.6, 4, $this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)), '', 'L');
    $pdf->SetFont('Arial','',$fuente);
    $pdf->setX(13);
    $pdf->setXY($current_x, $current_y+9);
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $pdf->SetFont('Arial','',$fuente);
    $total_tax_base = 0;
    $total_tax_amount = 0;
    foreach ($taxes as $tax => $arr) {
        $total_tax_base += $arr['base'] * $drow['trmrate'];
        $total_tax_amount += $arr['tax'] * $drow['trmrate'];
    }
    if ($drow['inv']->order_tax > 0) {
        $total_tax_base +=  $drow['inv']->total * $drow['trmrate'];
        $total_tax_amount += $drow['inv']->order_tax * $drow['trmrate'];
    }
    $pdf->SetFont('Arial','',$fuente);
    $tax_summary_end = $pdf->getY() + 3;
    //derecha
    $pdf->setXY($cX_items_finished+113, $cY_items_finished);
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();
    $l_column = 43.7;
    $r_column = 39.2;
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Total bruto'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], ($drow['inv']->total * $drow['trmrate']))), 'TBR', 1, 'R');
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Valor Impuesto'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $drow['inv']->order_tax + $drow['inv']->product_tax * $drow['trmrate'])), 'TBR', 1, 'R');
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención en la fuente '.($drow['inv']->rete_fuente_percentage > 0 ? $this->sma->formatQuantity($drow['inv']->rete_fuente_percentage, $drow['qty_decimals'])." %" : "")), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $drow['inv']->rete_fuente_total * $drow['trmrate'])), 'TBR', 1, 'R');
    if ($drow['inv']->rete_iva_total > 0) {
        $current_y+=5;
        $pdf->setXY($current_x, $current_y);
        $pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención al IVA '.($drow['inv']->rete_iva_percentage > 0 ? $this->sma->formatQuantity($drow['inv']->rete_iva_percentage, $drow['qty_decimals'])." %" : "")), 'TBLR', 1, 'L');
        $pdf->setXY($current_x+$l_column, $current_y);
        $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $drow['inv']->rete_iva_total * $drow['trmrate'])), 'TBR', 1, 'R');
    }
    if ($drow['inv']->rete_ica_total > 0) {
        $current_y+=5;
        $pdf->setXY($current_x, $current_y);
        $pdf->cell($l_column, 5, $this->sma->utf8Decode('Retención al ICA '.($drow['inv']->rete_ica_percentage > 0 ? $this->sma->formatQuantity($drow['inv']->rete_ica_percentage, $drow['qty_decimals'])." %" : "")), 'TBLR', 1, 'L');
        $pdf->setXY($current_x+$l_column, $current_y);
        $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $drow['inv']->rete_ica_total * $drow['trmrate'])), 'TBR', 1, 'R');
    }
    if ($drow['inv']->rete_other_total > 0) {
        $current_y+=5;
        $pdf->setXY($current_x, $current_y);
        $pdf->cell($l_column, 5, $this->sma->utf8Decode(lang('rete_other')." ".($drow['inv']->rete_other_percentage > 0 ? $this->sma->formatQuantity($drow['inv']->rete_other_percentage, $drow['qty_decimals'])." %" : "")), 'TBLR', 1, 'L');
        $pdf->setXY($current_x+$l_column, $current_y);
        $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $drow['inv']->rete_other_total * $drow['trmrate'])), 'TBR', 1, 'R');
    }
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Descuento'), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $drow['inv']->order_discount * $drow['trmrate'])), 'TBR', 1, 'R');
    if ($drow['inv']->shipping > 0) {
        $current_y+=5;
        $pdf->setXY($current_x, $current_y);
        $pdf->cell($l_column, 5, $this->sma->utf8Decode('  Domicilio'), 'TBLR', 1, 'L');
        $pdf->setXY($current_x+33.2, $current_y);
        $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], $drow['inv']->shipping * $drow['trmrate'])), 'TBR', 1, 'R');
    }
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->SetFont('Arial','B',$fuente+2);
    $pdf->cell($l_column, 5, $this->sma->utf8Decode('Total'), 'TBLR', 1, 'L', 0);
    $pdf->setXY($current_x+$l_column, $current_y);
    $pdf->cell($r_column, 5, $this->sma->utf8Decode($this->sma->formatValue($drow['value_decimals'], ($drow['inv']->grand_total - $drow['inv']->rete_fuente_total - $drow['inv']->rete_iva_total - $drow['inv']->rete_ica_total - $drow['inv']->rete_other_total) * $drow['trmrate'])), 'TBR', 1, 'R', 0);
    $pdf->SetFont('Arial','',$fuente);
    $inv_total_summary_end = $pdf->getY() + 3;


    $pdf->RoundedRect(13, 244, 64, 20 , 3, '', '');
    $pdf->setXY(13, 244);
    $pdf->cell(64, 5, $this->sma->utf8Decode('Alistado por'), '', 0, 'L');
    $pdf->setXY(15.5, 255);
    $pdf->cell(59, 5, $this->sma->utf8Decode(''), 'B', 0, 'L');

    $pdf->RoundedRect(79, 244, 64, 20 , 3, '', '');
    $pdf->setXY(79, 244);
    $pdf->cell(64, 5, $this->sma->utf8Decode('Revisado por'), '', 0, 'L');
    $pdf->setXY(81.5, 255);
    $pdf->cell(59, 5, $this->sma->utf8Decode(''), 'B', 0, 'L');

    $pdf->RoundedRect(145, 244, 64, 20 , 3, '', '');
    $pdf->setXY(145, 244);
    $pdf->cell(32, 5, $this->sma->utf8Decode('Facturado por'), '', 0, 'L');
    $pdf->setXY(177, 244);
    $pdf->cell(32, 5, $this->sma->utf8Decode('# Factura'), '', 0, 'L');
    $pdf->setXY(147.5, 255);
    $pdf->cell(28, 5, $this->sma->utf8Decode(''), 'B', 0, 'L');
    $pdf->RoundedRect(179, 249, 27, 11 , 3, '', 'DF');

    if ($tax_summary_end > $inv_total_summary_end) {
        $pdf->setXY(13, $tax_summary_end);
    } else {
        $pdf->setXY(13, $inv_total_summary_end);
    }

}
$pdf->Output("sale_view_3.pdf", "I");