<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="imprimir_factura_mayorista();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <?php if ($logo) { ?>
                <div class="text-center" style="margin-bottom:20px;">
                    <img src="<?= base_url() . 'assets/uploads/logos/' . $biller->logo; ?>"
                         alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
                </div>
            <?php } ?>
            <div class="col-xs-12">
                <?php if ($customer): ?>
                    <h2 class="text-center"><?= lang('quote') ?></h2>
                <?php else: ?>
                    <?php if ($inv->quote_type == 1): ?>
                        <h2 class="text-center"><?= lang('quote_purchase') ?></h2>
                    <?php else: ?>
                        <h2 class="text-center"><?= lang('quote_expense') ?></h2>
                    <?php endif ?>
                <?php endif ?>
            </div>

            <div class="col-sm-12 row">
                <div class="col-xs-5">
                    <p class="bold">
                        <?= lang("ref"); ?>: <?= $inv->reference_no; ?><br>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("status"); ?>: <?= lang($inv->status); ?><br>
                        <?= lang("payment_status"); ?>: <?= $inv->payment_status == 1 ? 'Contado' : 'Credito'; ?><br>
                        <?php if ($inv->payment_status == 1): ?>
                            <?= lang("paid_by"); ?>: <?= lang($inv->payment_method); ?>
                        <?php endif ?>
                        <?php if ($inv->payment_status == 0): ?>
                            <?= lang("payment_term"); ?>: <?= lang($inv->payment_term); ?>
                        <?php endif ?>
                    </p>
                </div>
                <div class="clearfix"></div>
            </div> <!-- col-sm-12 -->

            <div class="row" style="margin-bottom:15px;">
                <div class="col-xs-6">
                    <?php if ($customer): ?>
                        <?php echo $this->lang->line("from"); ?>:
                    <?php else: ?>
                        <?php echo $this->lang->line("from"); ?>:
                    <?php endif ?>
                    <h2 style="margin-top:10px;"><?= $biller->company != '-' ? $biller->company : $biller->name; ?></h2>
                    <?= $biller->company ? "" : "Attn: " . $biller->name ?>
                    <?php
                        echo $biller->address . "<br>" . $biller->city . " " . $biller->postal_code . " " . $biller->state . "<br>" . $biller->country;
                        echo "<p>";
                        if ($biller->vat_no != "-" && $biller->vat_no != "") {
                            echo "<br>" . lang("vat_no") . ": " . $biller->vat_no;
                        }
                        if ($biller->cf1 != "-" && $biller->cf1 != "") {
                            echo "<br>" . lang("bcf1") . ": " . $biller->cf1;
                        }
                        if ($biller->cf2 != "-" && $biller->cf2 != "") {
                            echo "<br>" . lang("bcf2") . ": " . $biller->cf2;
                        }
                        if ($biller->cf3 != "-" && $biller->cf3 != "") {
                            echo "<br>" . lang("bcf3") . ": " . $biller->cf3;
                        }
                        if ($biller->cf4 != "-" && $biller->cf4 != "") {
                            echo "<br>" . lang("bcf4") . ": " . $biller->cf4;
                        }
                        if ($biller->cf5 != "-" && $biller->cf5 != "") {
                            echo "<br>" . lang("bcf5") . ": " . $biller->cf5;
                        }
                        if ($biller->cf6 != "-" && $biller->cf6 != "") {
                            echo "<br>" . lang("bcf6") . ": " . $biller->cf6;
                        }
                        echo "</p>";
                        echo lang("tel") . ": " . $biller->phone . "<br>" . lang("email") . ": " . $biller->email;
                    ?>
                </div> <!-- col-xs-6 -->
                <?php if ($customer): ?>
                    <div class="col-xs-6">
                        <?php echo $this->lang->line("to"); ?>:<br/>
                        <h2 style="margin-top:10px;"><?= $customer->company ? $customer->company : $customer->name; ?></h2>
                        <?= $customer->company ? "" : "Attn: " . $customer->name ?>
                        <?php
                            echo $customer->address . "<br>" . $customer->city . " " . $customer->postal_code . " " . $customer->state . "<br>" . $customer->country;
                            echo "<p>";
                            if ($customer->vat_no != "-" && $customer->vat_no != "") {
                                echo "<br>" . lang("vat_no") . ": " . $customer->vat_no;
                            }
                            if ($customer->cf1 != "-" && $customer->cf1 != "") {
                                echo "<br>" . lang("ccf1") . ": " . $customer->cf1;
                            }
                            if ($customer->cf2 != "-" && $customer->cf2 != "") {
                                echo "<br>" . lang("ccf2") . ": " . $customer->cf2;
                            }
                            if ($customer->cf3 != "-" && $customer->cf3 != "") {
                                echo "<br>" . lang("ccf3") . ": " . $customer->cf3;
                            }
                            if ($customer->cf4 != "-" && $customer->cf4 != "") {
                                echo "<br>" . lang("ccf4") . ": " . $customer->cf4;
                            }
                            if ($customer->cf5 != "-" && $customer->cf5 != "") {
                                echo "<br>" . lang("ccf5") . ": " . $customer->cf5;
                            }
                            if ($customer->cf6 != "-" && $customer->cf6 != "") {
                                echo "<br>" . lang("ccf6") . ": " . $customer->cf6;
                            }
                            echo "</p>";
                            echo lang("tel") . ": " . $customer->phone . "<br>" . lang("email") . ": " . $customer->email;
                        ?> 
                    </div> <!-- col-xs-6 -->
                <?php else: ?>
                    <div class="col-xs-6">
                        <?php echo $this->lang->line("to"); ?>:<br/>
                        <h2 style="margin-top:10px;"><?= $supplier->company ? $supplier->company : $supplier->name; ?></h2>
                        <?= $supplier->company ? "" : "Attn: " . $supplier->name ?>
                        <?php
                            echo $supplier->address . "<br>" . $supplier->city . " " . $supplier->postal_code . " " . $supplier->state . "<br>" . $supplier->country;
                            echo "<p>";
                            if ($supplier->vat_no != "-" && $supplier->vat_no != "") {
                                echo "<br>" . lang("vat_no") . ": " . $supplier->vat_no;
                            }
                            if ($supplier->cf1 != "-" && $supplier->cf1 != "") {
                                echo "<br>" . lang("ccf1") . ": " . $supplier->cf1;
                            }
                            if ($supplier->cf2 != "-" && $supplier->cf2 != "") {
                                echo "<br>" . lang("ccf2") . ": " . $supplier->cf2;
                            }
                            if ($supplier->cf3 != "-" && $supplier->cf3 != "") {
                                echo "<br>" . lang("ccf3") . ": " . $supplier->cf3;
                            }
                            if ($supplier->cf4 != "-" && $supplier->cf4 != "") {
                                echo "<br>" . lang("ccf4") . ": " . $supplier->cf4;
                            }
                            if ($supplier->cf5 != "-" && $supplier->cf5 != "") {
                                echo "<br>" . lang("ccf5") . ": " . $supplier->cf5;
                            }
                            if ($supplier->cf6 != "-" && $supplier->cf6 != "") {
                                echo "<br>" . lang("ccf6") . ": " . $supplier->cf6;
                            }
                            echo "</p>";
                            echo lang("tel") . ": " . $supplier->phone . "<br>" . lang("email") . ": " . $supplier->email;
                        ?>
                    </div> <!-- col-xs-6 -->
                <?php endif ?>

                <div class="clearfix"></div>
                <?php if ($inv->quote_currency != $this->Settings->default_currency): ?>
                    <div class="col-xs-6 no-print">
                        <label><?= lang('choose_quote_currency_view') ?></label>
                        <div class="row">
                            <div class="col-xs-6">
                                <select name="view_currency" id="view_currency" class="form-control">
                                    <option value="<?= $inv->quote_currency ?>"><?= $inv->quote_currency ?></option>
                                    <option value="<?= $this->Settings->default_currency ?>"><?= $this->Settings->default_currency ?></option>
                                </select>
                            </div>
                            <div class="col-xs-6 inv_currency_trm">
                                <input type="text" name="inv_currency_trm" class="form-control text-right" value="TRM : <?= $this->sma->formatDecimal($inv->quote_currency_trm, 4) ?>" readonly>
                            </div>
                        </div>
                    </div>
                <?php endif ?>
            </div> <!-- row -->

            <div class="inv_currency">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("no"); ?></th>
                                <th><?= lang("description"); ?></th>
                                <th><?= lang('net_unit_price'); ?></th>
                                <?php
                                if ($Settings->tax1) {
                                    echo '<th>' . lang("tax_1") . '</th>';
                                    echo '<th>' . lang("tax_2") . '</th>';
                                }
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<th>' . lang("discount") . '</th>';
                                }
                                ?>
                                <?php if ($Settings->indian_gst) { ?>
                                    <th><?= lang("hsn_code"); ?></th>
                                <?php } ?>
                                <th><?= lang("quantity"); ?></th>                           
                                <th><?= lang("subtotal"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $r = 1;
                                $tax_summary = array();
                                $tax_1 = 0;
                                $tax_2 = 0;
                                $tot_net_unit_price = 0;
                                $tot_quantity = 0;
                                foreach ($rows as $row):
                                    $netunitpricetrm = $this->sma->formatDecimalNoRound($trmrate * $row->net_unit_price, 6);
                                    $tax_1 += $row->item_tax;
                                    $tax_2 += $row->item_tax_2;
                                    $tot_net_unit_price += $row->net_unit_price;
                                    $tot_quantity += $row->quantity;
                            ?>
                            <tr>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                    <?= $row->preferences ? '<br>' . $this->sma->print_preference_selection($row->preferences) : ''; ?>
                                </td>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatMoney($row->net_unit_price); ?></td>
                                <?php
                                    if ($Settings->tax1) {
                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney(($trmrate * $row->item_tax) / $row->quantity) . '</td>';
                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax_2 != 0 ? '<small>('.($Settings->indian_gst ? $row->tax_2 : $row->tax_2_code).')</small>' : '') . ' ' . $this->sma->formatMoney(($trmrate * $row->item_tax_2) / $row->quantity) . '</td>';
                                    }
                                    if ($Settings->product_discount && $inv->product_discount != 0) {
                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($trmrate * $row->item_discount) . '</td>';
                                    }
                                ?>
                                <?php if ($Settings->indian_gst) { ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                <?php } ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>                      
                                <td style="text-align:right; width:120px;"><?= $trmrate != 1 ? $this->sma->formatMoney($trmrate * $row->subtotal) : $this->sma->formatMoney($row->subtotal) ; ?></td>
                            </tr>
                            <?php
                                $r++;
                                endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                        <?php
                            $col = $Settings->indian_gst ? 5 : 4;
                            if ($Settings->product_discount && $inv->product_discount != 0) {
                                $col++;
                            }
                            if ($Settings->tax1) {
                                $col+=2;
                            }
                            if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1) {
                                $tcol = $col - 2;
                            } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                                $tcol = $col - 1;
                            } elseif ($Settings->tax1) {
                                $tcol = $col - 2;
                            } else {
                                $tcol = $col;
                            }
                        ?>
                        <?php if ($inv->grand_total != $inv->total) { ?>
                            <tr>
                                <td colspan="<?= $tcol+2; ?>"
                                    style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                    (<?= $inv->quote_currency; ?>)
                                </td>
                                <?php
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<td style="text-align:right;">' . $this->sma->formatMoney($trmrate * $inv->product_discount) . '</td>';
                                }
                                ?>
                                <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($trmrate * ($inv->total + $inv->product_tax)); ?></td>
                            </tr>
                        <?php } ?>
                        <?php if ($Settings->indian_gst) {
                            if ($inv->cgst > 0) {
                                echo '<tr><td colspan="' . ($col) . '" class="text-right">' . lang('cgst') . ' (' . $inv->quote_currency . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($inv->cgst) : $inv->cgst) . '</td></tr>';
                            }
                            if ($inv->sgst > 0) {
                                echo '<tr><td colspan="' . ($col) . '" class="text-right">' . lang('sgst') . ' (' . $inv->quote_currency . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($inv->sgst) : $inv->sgst) . '</td></tr>';
                            }
                            if ($inv->igst > 0) {
                                echo '<tr><td colspan="' . ($col) . '" class="text-right">' . lang('igst') . ' (' . $inv->quote_currency . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($inv->igst) : $inv->igst) . '</td></tr>';
                            }
                        } ?>

                        <?php if ($inv->order_discount != 0) {
                            echo '<tr><td colspan="' . ($col) . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $inv->quote_currency . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($trmrate * $inv->order_discount) . '</td></tr>';
                        }
                        ?>
                        <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                            echo '<tr><td colspan="' . ($col) . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $inv->quote_currency . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->order_tax) . '</td></tr>';
                        }
                        ?>
                        <?php if ($inv->shipping != 0) {
                            echo '<tr><td colspan="' . ($col) . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $inv->quote_currency . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->shipping) . '</td></tr>';
                        }
                        ?>
                        <?php
                            $total_retencion = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total;
                            if ($total_retencion != 0) {
                                echo '<tr>
                                        <td colspan="' . ($col) . '" style="text-align:right; padding-right:10px;;">' . lang("retention") . ' </td>
                                        <td style="text-align:right; padding-right:10px;"> - ' . $this->sma->formatMoney($trmrate * $total_retencion) . '</td>
                                    </tr>';
                        }
                        ?>
                        <tr>
                            <td colspan="<?= ($col) ?>"
                                style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                (<?= $inv->quote_currency; ?>)
                            </td>
                            <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $trmrate != 1 ? $this->sma->formatMoney($trmrate * ($inv->grand_total - $total_retencion)) : $this->sma->formatMoney($inv->grand_total - $total_retencion) ; ?></td>
                        </tr>
                        </tfoot>
                    </table>
                </div> <!-- table-responsive -->
                <?php
                    $inv_currency = new stdClass();
                    $inv_currency->currency = $inv->quote_currency;
                    $inv_currency->rate = $trmrate;
                ?>
                <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, null, $inv->product_tax, false, $inv_currency) : ''; ?>
            </div>
            <div class="default_currency" style="display: none;">
                <?php 
                    $trmrate = 1;
                    $inv->quote_currency = $this->Settings->default_currency; ?>
                <div class="table-responsive">
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("no"); ?></th>
                                <th><?= lang("description"); ?></th>
                                <?php if ($Settings->indian_gst) { ?>
                                    <th><?= lang("hsn_code"); ?></th>
                                <?php } ?>
                                <th><?= lang("quantity"); ?></th>
                                <th><?= lang("unit_price"); ?></th>
                                <?php
                                if ($Settings->tax1) {
                                    echo '<th>' . lang("tax_1") . '</th>';
                                    echo '<th>' . lang("tax_2") . '</th>';
                                }
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    echo '<th>' . lang("discount") . '</th>';
                                }
                                ?>
                                <th><?= lang("subtotal"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $r = 1;
                                $tax_summary = array();
                                $tax_1 = 0;
                                $tax_2 = 0;
                                foreach ($rows as $row):
                                    $tax_1 += $row->item_tax;
                                    $tax_2 += $row->item_tax_2;
                            ?>
                            <tr>
                                <td style="text-align:center; width:40px; vertical-align:middle;"><?= $r; ?></td>
                                <td style="vertical-align:middle;">
                                    <?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                    <?= $row->details ? '<br>' . $row->details : ''; ?>
                                </td>
                                <?php if ($Settings->indian_gst) { ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $row->hsn_code; ?></td>
                                <?php } ?>
                                <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                                <td style="text-align:right; width:100px;"><?= $this->sma->formatMoney($trmrate * $row->net_unit_price); ?></td>
                                <?php
                                    if ($Settings->tax1) {
                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax != 0 ? '<small>('.($Settings->indian_gst ? $row->tax : $row->tax_code).')</small>' : '') . ' ' . $this->sma->formatMoney($trmrate * $row->item_tax) . '</td>';
                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->item_tax_2 != 0 ? '<small>('.($Settings->indian_gst ? $row->tax_2 : $row->tax_2_code).')</small>' : '') . ' ' . $this->sma->formatMoney($trmrate * $row->item_tax_2) . '</td>';
                                    }
                                    if ($Settings->product_discount && $inv->product_discount != 0) {
                                        echo '<td style="width: 100px; text-align:right; vertical-align:middle;">' . ($row->discount != 0 ? '<small>(' . $row->discount . ')</small> ' : '') . $this->sma->formatMoney($trmrate * $row->item_discount) . '</td>';
                                    }
                                ?>
                                <td style="text-align:right; width:120px;"><?= $this->sma->formatMoney($trmrate * $row->subtotal); ?></td>
                            </tr>
                            <?php
                                $r++;
                                endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                            <?php
                                $col = $Settings->indian_gst ? 5 : 4;
                                if ($Settings->product_discount && $inv->product_discount != 0) {
                                    $col++;
                                }
                                if ($Settings->tax1) {
                                    $col += 2 ;
                                }
                                if ($Settings->product_discount && $inv->product_discount != 0 && $Settings->tax1) {
                                    $tcol = $col - 2;
                                } elseif ($Settings->product_discount && $inv->product_discount != 0) {
                                    $tcol = $col - 1;
                                } elseif ($Settings->tax1) {
                                    $tcol = $col - 2;
                                } else {
                                    $tcol = $col;
                                }
                            ?>
                            <?php if ($inv->grand_total != $inv->total) { ?>
                                <tr>
                                    <td colspan="<?= $tcol; ?>"
                                        style="text-align:right; padding-right:10px;"><?= lang("total"); ?>
                                        (<?= $inv->quote_currency; ?>)
                                    </td>
                                    <?php
                                    if ($Settings->tax1) {
                                        echo '<td style="text-align:right;">' . $this->sma->formatMoney($trmrate * $tax_1) . '</td>';
                                        echo '<td style="text-align:right;">' . $this->sma->formatMoney($trmrate * $tax_2) . '</td>';
                                    }
                                    if ($Settings->product_discount && $inv->product_discount != 0) {
                                        echo '<td style="text-align:right;">' . $this->sma->formatMoney($trmrate * $inv->product_discount) . '</td>';
                                    }
                                    ?>
                                    <td style="text-align:right; padding-right:10px;"><?= $this->sma->formatMoney($trmrate * ($inv->total + $inv->product_tax)); ?></td>
                                </tr>
                            <?php } ?>

                            <?php if ($Settings->indian_gst) {
                                if ($inv->cgst > 0) {
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('cgst') . ' (' . $inv->quote_currency . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($inv->cgst) : $inv->cgst) . '</td></tr>';
                                }
                                if ($inv->sgst > 0) {
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('sgst') . ' (' . $inv->quote_currency . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($inv->sgst) : $inv->sgst) . '</td></tr>';
                                }
                                if ($inv->igst > 0) {
                                    echo '<tr><td colspan="' . $col . '" class="text-right">' . lang('igst') . ' (' . $inv->quote_currency . ')</td><td class="text-right">' . ( $Settings->format_gst ? $this->sma->formatMoney($inv->igst) : $inv->igst) . '</td></tr>';
                                }
                            } ?>

                            <?php if ($inv->order_discount != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("order_discount") . ' (' . $inv->quote_currency . ')</td><td style="text-align:right; padding-right:10px;">'.($inv->order_discount_id ? '<small>('.$inv->order_discount_id.')</small> ' : '') . $this->sma->formatMoney($trmrate * $inv->order_discount) . '</td></tr>';
                            }
                            ?>
                            <?php if ($Settings->tax2 && $inv->order_tax != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;">' . lang("order_tax") . ' (' . $inv->quote_currency . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->order_tax) . '</td></tr>';
                            }
                            ?>
                            <?php if ($inv->shipping != 0) {
                                echo '<tr><td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("shipping") . ' (' . $inv->quote_currency . ')</td><td style="text-align:right; padding-right:10px;">' . $this->sma->formatMoney($trmrate * $inv->shipping) . '</td></tr>';
                            }
                            ?>
                            <?php
                                $total_retencion = $inv->rete_fuente_total + $inv->rete_iva_total + $inv->rete_ica_total + $inv->rete_other_total;
                                if ($total_retencion != 0) {
                                    echo '<tr>
                                            <td colspan="' . $col . '" style="text-align:right; padding-right:10px;;">' . lang("retention") . ' </td>
                                            <td style="text-align:right; padding-right:10px;"> - ' . $this->sma->formatMoney($trmrate * $total_retencion) . '</td>
                                        </tr>';
                                }
                            ?>
                            <tr>
                                <td colspan="<?= $col; ?>"
                                    style="text-align:right; font-weight:bold;"><?= lang("total_amount"); ?>
                                    (<?= $inv->quote_currency; ?>)
                                </td>
                                <td style="text-align:right; padding-right:10px; font-weight:bold;"><?= $this->sma->formatMoney($trmrate * ($inv->grand_total - $total_retencion)); ?></td>
                            </tr>
                        </tfoot>
                    </table> <!-- table-responsive -->
                </div>

                <?php
                    $inv_currency = new stdClass();
                    $inv_currency->currency = $this->Settings->default_currency;
                    $inv_currency->rate = 1;
                ?>
                <?= $Settings->invoice_view > 0 ? $this->gst->summary($rows, null, $inv->product_tax, false, $inv_currency) : ''; ?>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?php
                        if ($inv->note || $inv->note != "") { ?>
                            <div class="well well-sm">
                                <p class="bold"><?= lang("note"); ?>:</p>
                                <div><?= $this->sma->decode_html($inv->note); ?></div>
                            </div>
                        <?php } ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <div class="col-xs-12 pull-right text-center">
                <p>
                    <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                    <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                </p>
                <?php if ($inv->updated_by) { ?>
                <p>
                    <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                    <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv->updated_at); ?>
                </p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php if (!$Supplier || !$Customer) { ?>
    <div class="buttons row div_buttons_modal div_buttons_modal_lg">
        <?php if ($inv->attachment) { ?>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= admin_url('welcome/download/' . $inv->attachment) ?>" class="tip btn btn-primary" title="<?= lang('attachment') ?>">
                    <i class="fa fa-chain"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('attachment') ?></span>
                </a>
            </div>
        <?php } ?>
        <?php if ($customer): ?>
            <div class="col-sm-3 col-xs-6">
                <a href='#' class='po btn btn-primary btn-outline' title='' data-placement="top" data-content="
                            <p> <?= lang('where_to_convert') ?> </p>
                            <a class='btn btn-primary' href='<?= admin_url('sales/add/').$inv->id ?>'> <?= lang('detal_sale') ?> </a>
                            <a class='btn btn-primary' href='<?= admin_url('pos/index/').$inv->id.'/1/1' ?>'> <?= lang('pos_sale') ?> </a>
                            <a class='btn btn-primary' href='<?= admin_url('pos/add_wholesale/').$inv->id.'/1' ?>'> <?= lang('pos_wholesale') ?> </a>
                            <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover' style="width: 100%;">
                            <i class='fa fa-download'></i>
                    <?= lang('create_sale') ?>
                </a>
            </div>
            <?php else: ?>
                <div class="col-sm-3 col-xs-6">
                    <a href="<?= admin_url('purchases/add/' . $inv->id) ?>" class="tip btn btn-primary btn-outline" title="<?= lang('create_purchase') ?>" style="width: 100%;">
                        <i class="fa fa-star"></i>
                        <span class="hidden-sm hidden-xs"><?= lang('create_purchase') ?></span>
                    </a>
                </div>
            <?php endif ?>
                <div class="col-sm-3 col-xs-6">

                <a href="<?= admin_url('quotes/view/' . $inv->id) ?>" class="tip btn btn-primary btn-outline" title="<?= lang('download_pdf') ?>" target="_blank" style="width: 100%;">
                    <i class="fa fa-print"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('print') ?></span>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= admin_url('quotes/email/' . $inv->id) ?>" data-toggle="modal" data-target="#myModal2" class="tip btn btn-primary btn-outline" title="<?= lang('email') ?>" style="width: 100%;">
                    <i class="fa fa-envelope-o"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('email') ?></span>
                </a>
            </div>
            <div class="col-sm-3 col-xs-6">
                <a href="<?= admin_url('quotes/view/' . $inv->id.'/0/1') ?>" class="tip btn btn-primary btn-outline" title="<?= lang('download_pdf') ?>" style="width: 100%;">
                    <i class="fa fa-download"></i>
                    <span class="hidden-sm hidden-xs"><?= lang('pdf') ?></span>
                </a>
            </div>
        </div>
    </div>
<?php } ?>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });

    $('#view_currency').on('change', function(){
        actual_currency = $(this).val();
        default_currency = site.settings.default_currency;

        if (actual_currency != default_currency) {
            $('.inv_currency').css('display', '');
            $('.inv_currency_trm').css('display', '');
            $('.default_currency').css('display', 'none');
        } else {
            $('.inv_currency').css('display', 'none');
            $('.inv_currency_trm').css('display', 'none');
            $('.default_currency').css('display', '');
        }

    });

    function imprimir_factura_mayorista()
    {
        window.open('<?= admin_url('quotes/view/' . $inv->id) ?>');
    }

</script>
