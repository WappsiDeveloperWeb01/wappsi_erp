<?php
defined('BASEPATH') or exit('No direct script access allowed');

class CommissionPercentage_model extends CI_Model
{
    private $tableName = "commission_percentage";

    public function __construct()
    {
        parent::__construct();
    }

    public function find($data)
    {
        $this->db->where($data);
        $q = $this->db->get($this->tableName);
        return $q->row();
    }

    public function get($data = null)
    {
        if (!empty($data)) {
            $this->db->where($data);
        }

        $q = $this->db->get($this->tableName);
        return $q->result();
    }
}