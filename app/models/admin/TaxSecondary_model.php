<?php
defined('BASEPATH') or exit('No direct script access allowed');

class TaxSecondary_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function find($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('tax_secondary');
        return $q->row();
    }
}