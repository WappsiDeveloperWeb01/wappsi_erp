<?php defined('BASEPATH') or exit('No direct script access allowed');
class Wms extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->lang->admin_load('wms', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('transfers_model');
        $this->load->admin_model('wms_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '3072';
        $this->data['logo'] = true;
    }

    public function wms_pickings(){
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['picking_notices'] = $this->wms_model->get_default_picking_notices(1);
        $warehouses = $this->site->getAllWarehouses(2);
        $wh_arr = [];
        if ($warehouses) {
            foreach ($warehouses as $wh) {
                $wh_arr[$wh->id] = $wh;
            }
        }
        $this->data['warehouses'] = $wh_arr;
        $this->data['packing_warehouses'] = $this->site->getAllWarehouses(3);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('wms_pickings'), 'page' => lang('wms_pickings')), array('link' => '#', 'page' => lang('wms_pickings')));
        $meta = array('page_title' => lang('wms_pickings'), 'bc' => $bc);
        $this->page_construct('wms/wms_pickings', $meta, $this->data);
    }

    public function getPickings(){
        $this->load->library('datatables');
        $this->datatables->select('
                                    wms_picking.id, 
                                    wms_picking.date, 
                                    wms_picking.reference_no, 
                                    companies.name as biller,
                                    customer.name as customer,
                                    warehouses.name as warehouse_name,
                                    GROUP_CONCAT(DISTINCT('.$this->db->dbprefix('order_sales').'.reference_no)) as order_ref_no,
                                    wms_picking.packed_status,
                                    wms_picking.invoiced_status
                                    ');
        $this->datatables->join('companies', 'companies.id = wms_picking.biller_id', 'inner');
        $this->datatables->join('companies customer', 'customer.id = wms_picking.customer_id', 'inner');
        $this->datatables->join('warehouses', 'warehouses.id = wms_picking.packing_warehouse_id', 'inner');
        $this->datatables->join('wms_picking_detail', 'wms_picking_detail.picking_id = wms_picking.id', 'inner');
        $this->datatables->join('order_sales', 'order_sales.id = wms_picking_detail.order_sale_id', 'inner');
        $this->datatables->from('wms_picking');
        $this->datatables->group_by('wms_picking.id');
        $this->datatables->add_column("Actions", "", "id");
        echo $this->datatables->generate();
    }

	public function add_picking()
	{
        $this->form_validation->set_rules('product_id[]', lang("products"), 'required');
        if ($this->form_validation->run() == true) {
            $date = $this->sma->fld(trim($this->input->post('date')));
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $packing_warehouse = $this->input->post('packing_warehouse');

            $product_id = $this->input->post('product_id');
            $product_code = $this->input->post('product_code');
            $product_name = $this->input->post('product_name');
            $option_id = $this->input->post('option_id');
            $warehouse_id = $this->input->post('warehouse_id');
            $order_sale_id = $this->input->post('order_sale_id');
            $order_sale_item_id = $this->input->post('order_sale_item_id');
            $quantity = $this->input->post('required_quantity');

            $data = [
                        'customer_id' => $customer_id,
                        'biller_id' => $biller_id,
                        'document_type_id' => $document_type_id,
                        'packing_warehouse_id' => $packing_warehouse,
                        'date' => $date,
                        'created_by' =>  $this->session->userdata('user_id'),
                    ];

            $r = count($product_id);
            $products = [];
            for ($i=0; $i < $r; $i++) { 
                $product = [
                    'product_id' => $product_id[$i],
                    'product_code' => $product_code[$i],
                    'product_name' => $product_name[$i],
                    'option_id' => $option_id[$i],
                    'warehouse_id' => $warehouse_id[$i],
                    'order_sale_id' => $order_sale_id[$i],
                    'order_sale_item_id' => $order_sale_item_id[$i],
                    'quantity' => $quantity[$i],
                ];
                $products[] = $product;
                if (isset($tf_data[$warehouse_id[$i]])) {
                    $from_warehouse = $this->site->getWarehouseByID($warehouse_id[$i]);
                    $to_warehouse = $this->site->getWarehouseByID($packing_warehouse);
                    $tf_data[$warehouse_id[$i]]['data'] = array(
                        'date' => $data['date'],
                        'from_warehouse_id' => $warehouse_id[$i],
                        'from_warehouse_code' => $from_warehouse->code,
                        'from_warehouse_name' => $from_warehouse->name,
                        'to_warehouse_id' => $packing_warehouse,
                        'to_warehouse_code' => $to_warehouse->code,
                        'to_warehouse_name' => $to_warehouse->name,
                        'note' => NULL,
                        'total_tax' => NULL,
                        'total' => NULL,
                        'grand_total' => NULL,
                        'created_by' => $this->session->userdata('user_id'),
                        'status' => 'completed',
                        'shipping' => 0,
                    );
                }
                $pr_data = $this->site->getProductByID($product_id[$i]);
                $tf_data[$warehouse_id[$i]]['rows'][] = [
                        'product_id' => $product_id[$i],
                        'product_code' => $product_code[$i],
                        'product_name' => $product_name[$i],
                        'option_id' => $option_id[$i],
                        'net_unit_cost' => 0,
                        'unit_cost' => 0,
                        'quantity' => $quantity[$i],
                        'product_unit_id' => $pr_data->unit,
                        'product_unit_code' => NULL,
                        'unit_quantity' => $quantity[$i],
                        'quantity_balance' => $quantity[$i],
                        'warehouse_id' => $packing_warehouse,
                        'item_tax' => NULL,
                        'tax_rate_id' => NULL,
                        'tax' => NULL,
                        'subtotal' => 0,
                        'expiry' => NULL,
                        'real_unit_cost' => NULL,
                        'date' => $data['date'],
                        'serial_no' => NULL
                    ];
            }
            $notice_product_id = $this->input->post('notice_product_id');
            $notice_option_id = $this->input->post('notice_option_id');
            $notice_quantity = $this->input->post('notice_quantity');
            $notice_warehouse_id = $this->input->post('notice_warehouse_id');
            $picking_notice = $this->input->post('picking_notice');
            $r = $notice_product_id ? count($notice_product_id) : 0;
            $notices = [];
            for ($i=0; $i < $r; $i++) { 
                $notice = [
                    'date' => $date,
                    'product_id' => $notice_product_id[$i],
                    'option_id' => $notice_option_id[$i],
                    'quantity' => $notice_quantity[$i],
                    'warehouse_id' => $notice_warehouse_id[$i],
                    'default_notice_id' => $picking_notice[$i],
                    'type' => 1,
                ];
                $notices[] = $notice;
            }
        }
        if ($this->form_validation->run() == true && $this->wms_model->add_picking($data, $products, $notices, $tf_data)) {
            $this->session->set_userdata('remove_wmspicking', 1);
            $this->session->set_flashdata('message', lang("wms_picking_added"));
            admin_redirect('wms/wms_pickings');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['picking_notices'] = $this->wms_model->get_default_picking_notices(1);
            $warehouses = $this->site->getAllWarehouses(2);
            $wh_arr = [];
            if ($warehouses) {
                foreach ($warehouses as $wh) {
                    $wh_arr[$wh->id] = $wh;
                }
            }
            $this->data['warehouses'] = $wh_arr;
            $this->data['packing_warehouses'] = $this->site->getAllWarehouses(3);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('wms/wms_pickings'), 'page' => lang('wms_pickings')), array('link' => '#', 'page' => lang('add_picking')));
            $meta = array('page_title' => lang('add_picking'), 'bc' => $bc);
            $this->page_construct('wms/add_picking', $meta, $this->data);
        }
    }

    public function customer_order_sales(){
        $customer_id = $this->input->get('customer_id');
        $biller_id = $this->input->get('biller_id');
        $order_sales = $this->wms_model->get_customer_order_sales($customer_id, $biller_id);
        if ($order_sales) {
            $this->sma->send_json($order_sales);
        } else {
            $this->sma->send_json(array('response' => 0));
        }
    }

    public function order_sale_suggestions(){
        $term = $this->input->get('term', TRUE);
        $biller_id = $this->input->get('biller_id', TRUE);
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->wms_model->get_order_sale_suggestion($term, $biller_id, $limit);
        $this->sma->send_json($rows);
    }

    public function order_sale_by_reference(){
        $order_sale_reference = $this->input->get('order_sale_reference');
        $order_sales = $this->wms_model->get_customer_order_sales(NULL, NULL, $order_sale_reference);
        if ($order_sales) {
            $this->sma->send_json($order_sales);
        } else {
            $this->sma->send_json(array('response' => 0));
        }
    }

    public function getOrderSale($id = NULL, $withvatno = NULL)
    {
        $row = $this->wms_model->getOrderSaleByID($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->reference_no, 'value' => $row->reference_no, 'customer' => $row->customer_id)));
    }

    public function wms_picking_view($picking_id){
        $inv = $this->wms_model->get_picking($picking_id);
        $rows = $this->wms_model->get_picking_detail($picking_id);
        $notices = $this->wms_model->get_picking_notices($picking_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $rows;
        $this->data['notices'] = $notices;
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $warehouses = $this->site->getAllWarehouses();
        $wh_arr = [];
        if ($warehouses) {
            foreach ($warehouses as $wh) {
                $wh_arr[$wh->id] = $wh;
            }
        }
        $this->data['warehouses'] = $wh_arr;
        $this->load_view($this->theme . 'wms/wms_picking_view', $this->data);
    }

    public function add_packing()
    {
        $this->form_validation->set_rules('product_id[]', lang("products"), 'required');
        if ($this->form_validation->run() == true) {
            $date = $this->sma->fld(trim($this->input->post('date')));
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $packing_warehouse = $this->input->post('packing_warehouse');

            $product_id = $this->input->post('product_id');
            $product_code = $this->input->post('product_code');
            $product_name = $this->input->post('product_name');
            $option_id = $this->input->post('option_id');
            $warehouse_id = $this->input->post('warehouse_id');
            $order_sale_id = $this->input->post('order_sale_id');
            $order_sale_item_id = $this->input->post('order_sale_item_id');
            $quantity = $this->input->post('required_quantity');

            $data = [
                        'customer_id' => $customer_id,
                        'biller_id' => $biller_id,
                        'document_type_id' => $document_type_id,
                        'packing_warehouse_id' => $packing_warehouse,
                        'date' => $date,
                        'created_by' =>  $this->session->userdata('user_id'),
                        'status' => 'pending',
                    ];

            $r = count($product_id);
            $products = [];
            for ($i=0; $i < $r; $i++) { 
                $product = [
                    'product_id' => $product_id[$i],
                    'product_code' => $product_code[$i],
                    'product_name' => $product_name[$i],
                    'option_id' => $option_id[$i],
                    'warehouse_id' => $warehouse_id[$i],
                    'order_sale_id' => $order_sale_id[$i],
                    'order_sale_item_id' => $order_sale_item_id[$i],
                    'quantity' => $quantity[$i],
                ];
                $products[] = $product;
                if (isset($tf_data[$warehouse_id[$i]])) {
                    $from_warehouse = $this->site->getWarehouseByID($warehouse_id[$i]);
                    $to_warehouse = $this->site->getWarehouseByID($packing_warehouse);
                    $tf_data[$warehouse_id[$i]]['data'] = array(
                        'date' => $data['date'],
                        'from_warehouse_id' => $warehouse_id[$i],
                        'from_warehouse_code' => $from_warehouse->code,
                        'from_warehouse_name' => $from_warehouse->name,
                        'to_warehouse_id' => $packing_warehouse,
                        'to_warehouse_code' => $to_warehouse->code,
                        'to_warehouse_name' => $to_warehouse->name,
                        'note' => NULL,
                        'total_tax' => NULL,
                        'total' => NULL,
                        'grand_total' => NULL,
                        'created_by' => $this->session->userdata('user_id'),
                        'status' => 'completed',
                        'shipping' => 0,
                    );
                }
                $pr_data = $this->site->getProductByID($product_id[$i]);
                $tf_data[$warehouse_id[$i]]['rows'][] = [
                        'product_id' => $product_id[$i],
                        'product_code' => $product_code[$i],
                        'product_name' => $product_name[$i],
                        'option_id' => $option_id[$i],
                        'net_unit_cost' => 0,
                        'unit_cost' => 0,
                        'quantity' => $quantity[$i],
                        'product_unit_id' => $pr_data->unit,
                        'product_unit_code' => NULL,
                        'unit_quantity' => $quantity[$i],
                        'quantity_balance' => $quantity[$i],
                        'warehouse_id' => $packing_warehouse,
                        'item_tax' => NULL,
                        'tax_rate_id' => NULL,
                        'tax' => NULL,
                        'subtotal' => 0,
                        'expiry' => NULL,
                        'real_unit_cost' => NULL,
                        'date' => $data['date'],
                        'serial_no' => NULL
                    ];
            }
            $notice_product_id = $this->input->post('notice_product_id');
            $notice_option_id = $this->input->post('notice_option_id');
            $notice_quantity = $this->input->post('notice_quantity');
            $notice_warehouse_id = $this->input->post('notice_warehouse_id');
            $picking_notice = $this->input->post('picking_notice');
            $r = $notice_product_id ? count($notice_product_id) : 0;
            $notices = [];
            for ($i=0; $i < $r; $i++) { 
                $notice = [
                    'date' => $date,
                    'product_id' => $notice_product_id[$i],
                    'option_id' => $notice_option_id[$i],
                    'quantity' => $notice_quantity[$i],
                    'warehouse_id' => $notice_warehouse_id[$i],
                    'default_notice_id' => $picking_notice[$i],
                    'type' => 1,
                ];
                $notices[] = $notice;
            }
        }
        if ($this->form_validation->run() == true && $this->wms_model->add_packing($data, $products, $notices, $tf_data)) {
            $this->session->set_userdata('remove_wmspicking', 1);
            $this->session->set_flashdata('message', lang("wms_picking_added"));
            admin_redirect('wms/wms_pickings');
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['picking_notices'] = $this->wms_model->get_default_picking_notices(1);
            $warehouses = $this->site->getAllWarehouses(2);
            $wh_arr = [];
            if ($warehouses) {
                foreach ($warehouses as $wh) {
                    $wh_arr[$wh->id] = $wh;
                }
            }
            $this->data['warehouses'] = $wh_arr;
            $this->data['packing_warehouses'] = $this->site->getAllWarehouses(3);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('wms/wms_pickings'), 'page' => lang('wms_pickings')), array('link' => '#', 'page' => lang('add_packing')));
            $meta = array('page_title' => lang('add_packing'), 'bc' => $bc);
            $this->page_construct('wms/add_packing', $meta, $this->data);
        }
    }

    public function picking_suggestions(){
        $term = $this->input->get('term', TRUE);
        $biller_id = $this->input->get('biller_id', TRUE);
        $limit = $this->input->get('limit', TRUE);
        $rows['results'] = $this->wms_model->get_picking_suggestion($term, $biller_id, $limit);
        $this->sma->send_json($rows);
    }

    public function customer_pickings(){
        $customer_id = $this->input->get('customer_id');
        $biller_id = $this->input->get('biller_id');
        $pickings = $this->wms_model->get_customer_pickings($customer_id, $biller_id);
        if ($pickings) {
            $this->sma->send_json($pickings);
        } else {
            $this->sma->send_json(array('response' => 0));
        }
    }

    public function picking_by_reference(){
        $picking_reference = $this->input->get('picking_reference');
        $pickings = $this->wms_model->get_customer_pickings(NULL, NULL, $picking_reference);
        if ($pickings) {
            $this->sma->send_json($pickings);
        } else {
            $this->sma->send_json(array('response' => 0));
        }
    }

    public function getPicking($id = NULL, $withvatno = NULL)
    {
        $row = $this->wms_model->getPicking($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->reference_no, 'value' => $row->reference_no, 'customer' => $row->customer_id)));
    }
}