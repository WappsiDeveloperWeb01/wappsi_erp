<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="employees_table" class="table table-hover" style="width: 100%">
                                    <thead>
                                        <tr class="primary">
                                            <th></th>
                                            <th><?= lang("name"); ?></th>
                                            <th><?= lang("vat_no"); ?></th>
                                            <th><?= lang("employees_email"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th><?= lang("phone"); ?></th>
                                            <th><?= lang("month"); ?></th>
                                            <th><?= lang("day"); ?></th>
                                            <th><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="11" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        sessionStorage.clear();

        _name_months = {1:"Enero", 2:"Febrero", 3:"Marzo", 4:"Abril", 5:"Mayo", 6:"Junio", 7:"Julio", 8:"Agosto", 9:"Septiembre", 10:"Octubre", 11:"Noviembre", 12:"Diciembre"};
        load_datatable();

        $(document).on('click', '.employee_details td:not(:first-child, :last-child)', function() {
            var employee_id = $(this).parent('.employee_details').attr('id');

            $('#myModal').modal({remote: site.base_url + 'employees/see/'+ employee_id});
            $('#myModal').modal('show');
        });

        $(document).on('click', '.remove_employee', function() { confirm_deletion_employee($(this).data('employee_id'), $(this).data('contract_id')); });
    });

    function load_datatable()
    {
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#employees_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('employees/get_employees_datatables') ?>',
            dom: '<"col-sm-7 containerBtn"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>',
            fnServerData: function (sSource, aoData, fnCallback)
            {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (row, data, displayIndex)
            {
                var employee_id = data[0];
                row.id = employee_id;
                row.className = "employee_details";
                return row;
            },
            drawCallback: function(settings)
            {
                $('.actionsButtonContainer').html(`<a href="<?= admin_url('employees/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="left" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>`);

                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    radioClass: 'iradio_flat-blue',
                    increaseArea: '20%'
                });
            },
            "aoColumns": [
                { visible: false },
                { className: 'text-capitalize' },
                null,
                null,
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        return (data == 1) ? '<span class="row_status label label-primary"">Activo</span>': '<span class="row_status label label-danger"">Inactivo</span>';
                    }
                },
                null,
                {
                    className: 'text-center',
                    render: function(data, type, row) {
                        if (data > 0) {
                            return _name_months[data];
                        }

                        return '';
                    }
                },
                { className: 'text-center' },
                {
                    'bSortable': false,
                    render: function(data, type, row) {
                        var employee_id = row[0];
                        var actions_buttons = $(data);

                        actions_buttons.find('.dropdown-menu').append('<li><a class="remove_employee" data-employee_id="'+ employee_id +'"><i class="fa fa-trash-o"></i> <?= lang("remove_employee"); ?> </a></li>');

                        var str = actions_buttons.prop('outerHTML');
                        return str;
                    }
                }
            ]
        });
    }

    function confirm_deletion_employee(employee_id, contract_id)
    {
        swal({
            title: "¿Seguro desea eliminar al empleado?",
            text: "Esto proceso eliminará todos los datos del empleado",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, eliminarlo!",
            closeOnConfirm: false
        }, function (dismiss) {
            if (dismiss == true) {
                remove_employee(employee_id, contract_id);
            }
        });
    }

    function remove_employee(employee_id, contract_id)
    {
        $.ajax({
            url: '<?= admin_url('employees/remove') ?>',
            type: 'GET',
            dataType: 'JSON',
            data: {
                'employee_id': employee_id,
                'contract_id': contract_id
            }
        })
        .done(function(data) {
            if (data.status == 1) {
                swal({
                    title: "¡Eliminado!",
                    text: data.message,
                    type: "success"},
                    function () {
                        location.reload();
                    });
            } else if (data.status == 0) {
                swal("¡Error!", data.message, "error");
            }
        })
        .fail(function(data) {
            console.log("error");
        });
    }
</script>