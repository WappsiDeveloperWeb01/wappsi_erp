<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="invoice_notes_table" class="table table-hover">
                                    <thead>
                                        <tr class="active">
                                        	<th></th>
                                            <th><?= lang('invoice_notes_description') ?></th>
                                            <th><?= lang('invoice_notes_status') ?></th>
                                            <th><?= lang('invoice_notes_module') ?></th>
                                            <th><?= lang('actions'); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="10" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function() {
		load_invoice_notes_table();
	});

	function load_invoice_notes_table()
	{
	    if ($(window).width() < 1000) {
	        var nums = [[10, 25], [10, 25]];
	    } else {
	        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
	    }

        oTable = $('#invoice_notes_table').dataTable({
            "aaSorting": [[1, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength":  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?=admin_url('system_settings/get_invoice_notes_datatables')?>',
            dom: 'lr<"containerBtn"><"inputFiltro"f>tip<"clear">',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                { 'bVisible':false },
                null,
                {
                    mRender:  function ( data, type, row ) {
                        if (data == 1) {
                            return '<span class="label label-primary">Activo</span>';
                        } else {
                            return '<span class="label label-warning">Inactivo</span>';
                        }
                    },
                    className: 'text-center'
                },
                {
                    mRender: function (data, type, row){
                        if (data == 2) {
                            return "Ventas";
                        } else {
                            return "Recibo de caja";
                        }
                    }
                },
                null
            ],
            drawCallback: function(settings) {
                $('.containerBtn').html('<div class="dropdown pull-right" style="margin-top: 5px; margin-right: 1px;">'+
                                            '<button class="btn btn-outline btn-success" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true">'+
                                                '<?= lang('actions') ?> <span class="caret"></span>'+
                                            '</button>'+
                                        '<ul class="dropdown-menu pull-right" aria-labelledby="dropdownMenu2">'+
                                            '<li>'+
                                                '<a href="<?= admin_url('system_settings/add_invoice_notes') ?>" data-toggle="modal" data-target="#myModal" class="tip" title="" data-original-title="<?= lang('invoice_notes_add') ?>">'+
                                                    '<i class="fa fa-plus-circle"></i> <?= lang('invoice_notes_add') ?>'+
                                                '</a>'+
                                            '</li>'+
                                        '</ul>'+
                                      '</div>');
            }
        });
	}
</script>