<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Hotel_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('settings_model');
    }
    public function get_guests_register($id){
        $q = $this->db
            ->select("
                        guests_register.id,
                        customer.company,
                        customer_id,
                        biller_id,
                        guests_register.created_by,
                        reference_no,
                        date_in,
                        estimated_date_out,
                        date_out,
                        CONCAT(".$this->db->dbprefix('rooms').".room_no, ' - ', ".$this->db->dbprefix('rooms').".room_name) as room,
                        guests_no,
                        nights_no,
                        guests_register.status,
                        guests_register.registration_date,
                        document_type_id,
                        documents_types.module_invoice_format_id,
                        total,
                        room_id
                    ")
            ->join('rooms', 'rooms.id = guests_register.room_id', 'left')
            ->join('companies as customer', 'customer.id = guests_register.customer_id', 'left')
            ->join('documents_types', 'documents_types.id = guests_register.document_type_id', 'left')
            ->from('guests_register')
            ->where('guests_register.id', $id)
            ->get();
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }
    public function get_guests_register_detail($id){
        $q = $this->db
            ->select("
                        guests.*,
                        documentypes.nombre tipo_doc
                    ")
            ->join('guests', 'guests.id = guests_register_detail.guest_id', 'left')
            ->join('documentypes', 'documentypes.id = guests.document_type_id', 'left')
            ->from('guests_register_detail')
            ->where('guests_register_detail.register_id', $id)
            ->order_by('guests_register_detail.id ASC')
            ->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;

    }
    public function get_available_rooms($room_id = NULL){
        $this->db->select('rooms.*, products.*, rooms.id as id')
                 ->join('products', 'products.id = rooms.product_id', 'inner')
                 ->where('rooms.status', 0);
        if ($room_id) {
            $this->db->or_where('rooms.id', $room_id);
        }
        $q = $this->db->from('rooms')->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function add_guest_register($data, $rows){
        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        $ref = explode("-", $referenceBiller);
        $data['reference_no'] = $referenceBiller;
        $consecutive = $ref[1];
        if ($this->db->insert('guests_register', $data)) {
            $register_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($rows as $row) {
                if ($row['action'] == 1) { //crear huésped y registrar detalle
                    unset($row['action']);
                    unset($row['guest_id']);
                    if ($this->db->insert('guests', $row)) {
                        $guest_id = $this->db->insert_id();
                    } else {
                        return false;
                    }
                } else if ($row['action'] == 2) { //actualizar huesped y registrar detalle
                    $guest_id = $row['guest_id'];
                    unset($row['action']);
                    unset($row['guest_id']);
                    $this->db->update('guests', $row, ['id'=>$guest_id]);
                }
                if (!$this->db->insert('guests_register_detail', ['register_id'=> $register_id, 'guest_id'=> $guest_id])) {
                    return false;
                }
            }
            $this->db->update('rooms', ['status'=>1], ['id'=>$data['room_id']]);
            $room = $this->db->get_where('rooms', ['id'=>$data['room_id']])->row();
            $customer = $this->db->get_where('companies', ['id'=>$data['customer_id']])->row();
            $biller = $this->db->get_where('companies', ['id'=>$data['biller_id']])->row();
            $biller_data = $this->db->get_where('biller_data', ['biller_id'=>$data['biller_id']])->row();
            $product = $this->db->get_where('products', ['id'=>$room->product_id])->row();
            $tax = $this->sma->calculateTax($product->tax_rate, $data['total'], $product->tax_method);
            $net = $data['total'] - $tax;
            $product_det = array(
                'product_id' => $product->id,
                'product_code' => $product->code,
                'product_name' => $product->name,
                'product_type' => $product->type,
                'option_id' => NULL,
                'net_unit_price' => $net,
                'unit_price' => $data['total'],
                'real_unit_price' => $data['total'],
                'item_tax' => $tax,
                'tax_rate_id' => $product->tax_rate,
                'tax' => $product->tax_rate,
                'subtotal' => $data['total'],
                'quantity' => 1,
                'product_unit_id' => NULL,
                'product_unit_code' => NULL,
                'unit_quantity' => 1,
                'warehouse_id' => NULL,
                'item_tax_2' => 0,
                'tax_rate_2_id' => 0,
                'tax_2' => 0,
                'discount' => 0,
                'item_discount' => 0,
                'serial_no' => NULL,
                'quantity_to_bill' => 0,
                'price_before_tax' => 0,
                'preferences' => NULL,
            );
            $oslreferenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['order_sale_document_type_id']);
            $oslref = explode("-", $oslreferenceBiller);
            $oslreference_no = $oslreferenceBiller;
            $oslconsecutive = $oslref[1];
            $order = array(
                'date' => $data['date_in'],
                'reference_no' => $oslreference_no,
                'customer_id' => $data['customer_id'],
                'customer' => $customer->company,
                'biller_id' => $data['biller_id'],
                'biller' => $biller->company,
                'warehouse_id' => NULL,
                'note' => "Orden de pedido creada por Registro de huéspedes ".$data['reference_no'],
                'staff_note' => NULL,
                'total' => $data['total'],
                'product_discount' => NULL,
                'order_discount_id' => NULL,
                'order_discount' => NULL,
                'total_discount' => NULL,
                'product_tax' => $tax,
                'order_tax_id' => NULL,
                'order_tax' => NULL,
                'total_tax' => $tax,
                'shipping' => NULL,
                'grand_total' => $data['total'],
                'total_items' => 1,
                'sale_status' => 'pending',
                'due_date' => NULL,
                'paid' => 0,
                'seller_id' => $biller_data->default_seller_id,
                'address_id' => NULL,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'resolucion' => NULL,
                'payment_method' =>  NULL,
                'payment_term' => NULL,
                'document_type_id' => NULL,
                'delivery_time_id' => NULL,
                'delivery_day' => NULL,
                'document_type_id' => $data['order_sale_document_type_id'],
            );
            $this->db->insert('order_sales', $order);
            $order_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['order_sale_document_type_id'], $oslconsecutive+1);
            $product_det['sale_id'] = $order_id;
            $this->db->update('guests_register', ['order_sale_id'=>$order_id], ['id'=>$register_id]);
            $this->db->insert('order_sale_items', $product_det);
            return true;
        } else {
            return false;
        }
    }
    public function update_guest_register($id, $data, $rows){
        $txt_fields_changed = '';
        $register_id = $id;
        $old_register = $this->db->get_where('guests_register', ['id'=>$register_id])->row();
        if (!$old_register->order_sale_id) {
            $old_osl = $this->db->like('note', $old_register->reference_no, 'both')->get('order_sales')->row();
            $old_register->order_sale_id = $old_osl->id;
            $data['order_sale_id'] = $old_osl->id;
        }
        $update_consecutive = false;
        $data['reference_no'] = $old_register->reference_no;
        if ($old_register->biller_id != $data['biller_id'] || $old_register->document_type_id != $data['document_type_id']) {
            $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
            $ref = explode("-", $referenceBiller);
            $data['reference_no'] = $referenceBiller;
            $consecutive = $ref[1];
            $update_consecutive = true;
            $txt_fields_changed .= ' referencia cambió de '.$old_register->reference_no.' a '.$data['reference_no'].', ';
        }
        if ($old_register->room_id != $data['room_id']) {
            $old_room = $this->get_room_by_id($old_register->room_id);
            $room = $this->get_room_by_id($data['room_id']);
            $txt_fields_changed .= ' la habitación cambió de '.$old_room->room_name.' a '.$room->room_name.', ';

        }
        if ($old_register->customer_id != $data['customer_id']) {
            $old_customer = $this->site->getCompanyByID($old_register->customer_id);
            $customer = $this->site->getCompanyByID($data['customer_id']);
            $txt_fields_changed .= ' el cliente cambió de '.$old_customer->company.' a '.$customer->company.', ';

        }
        if ($old_register->nights_no != $data['nights_no']) {
            $txt_fields_changed .= ' las noches cambiaron de '.$old_register->nights_no.' a '.$data['nights_no'].', ';

        }
        if ($old_register->total != $data['total']) {
            $txt_fields_changed .= ' el total cambió de '.$old_register->total.' a '.$data['total'].', ';

        }
        if ($old_register->guests_no != $data['guests_no']) {
            $txt_fields_changed .= ' el N° de huéspedes cambió de '.$old_register->guests_no.' a '.$data['guests_no'].', ';

        }
        if ($this->db->update('guests_register', $data, ["id"=>$register_id])) {
            if ($update_consecutive) {
                $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            }
            $this->db->delete('guests_register_detail', ['register_id'=>$register_id]);
            foreach ($rows as $row) {
                if ($row['action'] == 1) { //crear huésped y registrar detalle
                    unset($row['action']);
                    unset($row['guest_id']);
                    if ($this->db->insert('guests', $row)) {
                        $txt_fields_changed .= ' Creó el siguiente huésped : '.$row['names'].' '.$row['surnames'].' con n° doc '.$row['vat_no'].', ';
                        $guest_id = $this->db->insert_id();
                    } else {
                        return false;
                    }

                } else if ($row['action'] == 2) { //actualizar huesped y registrar detalle
                    $guest_id = $row['guest_id'];
                    unset($row['action']);
                    unset($row['guest_id']);
                    $old_guest = $this->get_guest_by_id($guest_id);
                    $this->db->update('guests', $row, ['id'=>$guest_id]);
                    $guest_txt_fields_changed = '';
                    if ($old_guest->vat_no != $row['vat_no']) {
                        $guest_txt_fields_changed .= ' n° documento de '.$old_guest->vat_no.' a '.$row['vat_no'].', ';
                    }
                    if ($old_guest->names != $row['names']) {
                        $guest_txt_fields_changed .= ' nombres de '.$old_guest->names.' a '.$row['names'].', ';
                    }
                    if ($old_guest->surnames != $row['surnames']) {
                        $guest_txt_fields_changed .= ' apellidos de '.$old_guest->surnames.' a '.$row['surnames'].', ';
                    }
                    if ($old_guest->nationality != $row['nationality']) {
                        $guest_txt_fields_changed .= ' nacionalidad de '.$old_guest->nationality.' a '.$row['nationality'].', ';
                    }
                    if ($old_guest->phone != $row['phone']) {
                        $guest_txt_fields_changed .= ' teléfono de '.$old_guest->phone.' a '.$row['phone'].', ';
                    }
                    if ($old_guest->email != $row['email']) {
                        $guest_txt_fields_changed .= ' correo de '.$old_guest->email.' a '.$row['email'].', ';
                    }
                    if (date('Y-m-d', strtotime($old_guest->birth_date)) != $row['birth_date']) {
                        $guest_txt_fields_changed .= ' fecha nacimiento de '.date('Y-m-d', strtotime($old_guest->birth_date)).' a '.$row['birth_date'].', ';
                    }
                    if ($guest_txt_fields_changed != '') {
                        $txt_fields_changed .= 'Cambió los siguientes datos del huésped con id :'.$guest_id.'('.$guest_txt_fields_changed.')';
                    }
                }
                if (!$this->db->insert('guests_register_detail', ['register_id'=> $register_id, 'guest_id'=> $guest_id])) {
                    return false;
                }
            }
            $this->db->update('rooms', ['status'=>1], ['id'=>$data['room_id']]);
            $room = $this->db->get_where('rooms', ['id'=>$data['room_id']])->row();
            $customer = $this->db->get_where('companies', ['id'=>$data['customer_id']])->row();
            $biller = $this->db->get_where('companies', ['id'=>$data['biller_id']])->row();
            $biller_data = $this->db->get_where('biller_data', ['biller_id'=>$data['biller_id']])->row();
            $product = $this->db->get_where('products', ['id'=>$room->product_id])->row();
            $tax = $this->sma->calculateTax($product->tax_rate, $data['total'], $product->tax_method);
            $net = $data['total'] - $tax;
            $product_det = array(
                'product_id' => $product->id,
                'product_code' => $product->code,
                'product_name' => $product->name,
                'product_type' => $product->type,
                'option_id' => NULL,
                'net_unit_price' => $net,
                'unit_price' => $data['total'],
                'real_unit_price' => $data['total'],
                'item_tax' => $tax,
                'tax_rate_id' => $product->tax_rate,
                'tax' => $product->tax_rate,
                'subtotal' => $data['total'],
                'quantity' => 1,
                'product_unit_id' => NULL,
                'product_unit_code' => NULL,
                'unit_quantity' => 1,
                'warehouse_id' => NULL,
                'item_tax_2' => 0,
                'tax_rate_2_id' => 0,
                'tax_2' => 0,
                'discount' => 0,
                'item_discount' => 0,
                'serial_no' => NULL,
                'quantity_to_bill' => 0,
                'price_before_tax' => 0,
                'preferences' => NULL,
            );
            $update_osl_consecutive = false;
            $old_osl_data = $this->db->get_where('order_sales', ['id'=>$old_register->order_sale_id])->row();
            if ($old_register->order_sale_document_type_id != $data['order_sale_document_type_id']) {
                $oslreferenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['order_sale_document_type_id']);
                $oslref = explode("-", $oslreferenceBiller);
                $oslreference_no = $oslreferenceBiller;
                $oslconsecutive = $oslref[1];
                $update_osl_consecutive = true;
                $txt_fields_changed .= ' referencia de orden de pedido cambió de '.$old_osl_data->reference_no.' a '.$oslreference_no.', ';
            } else {
                $oslreference_no = $old_osl_data->reference_no;
            }
            $order = array(
                'date' => $data['date_in'],
                'reference_no' => $oslreference_no,
                'customer_id' => $data['customer_id'],
                'customer' => $customer->company,
                'biller_id' => $data['biller_id'],
                'biller' => $biller->company,
                'warehouse_id' => NULL,
                'note' => "Orden de pedido creada por Registro de huéspedes ".$data['reference_no'],
                'staff_note' => NULL,
                'total' => $data['total'],
                'product_discount' => NULL,
                'order_discount_id' => NULL,
                'order_discount' => NULL,
                'total_discount' => NULL,
                'product_tax' => $tax,
                'order_tax_id' => NULL,
                'order_tax' => NULL,
                'total_tax' => $tax,
                'shipping' => NULL,
                'grand_total' => $data['total'],
                'total_items' => 1,
                'sale_status' => 'pending',
                'due_date' => NULL,
                'paid' => 0,
                'seller_id' => $biller_data->default_seller_id,
                'address_id' => NULL,
                'created_by' => $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'resolucion' => NULL,
                'payment_method' =>  NULL,
                'payment_term' => NULL,
                'document_type_id' => NULL,
                'delivery_time_id' => NULL,
                'delivery_day' => NULL,
                'document_type_id' => $data['order_sale_document_type_id'],
            );
            $order_id = $old_register->order_sale_id;
            $this->db->update('order_sales', $order, ['id'=>$order_id]);
            if ($update_osl_consecutive) {
                $this->site->updateBillerConsecutive($data['order_sale_document_type_id'], $oslconsecutive+1);
            }
            $product_det['sale_id'] = $order_id;
            $this->db->update('order_sale_items', $product_det, ['sale_id'=>$order_id]);
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 1,
                'table_name' => 'guests_register',
                'record_id' => $register_id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $this->session->first_name." ".$this->session->last_name." editó registro de huésped : ".$txt_fields_changed,
            ]);
            return true;
        } else {
            return false;
        }
    }
    public function add_guest($data){
        if ($this->db->insert('guests', $data)) {
            return true;
        }
        return false;
    }

    public function get_guest_by_id($id){
        $q = $this->db->get_where('guests', ['id'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function edit_guest($id, $data){
        if ($this->db->update('guests', $data, ['id'=>$id])) {
            return true;
        }
        return false;
    }
    public function get_guest_register_by_id($id){
        $q = $this->db->get_where('guests_register', ['id'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
    public function close_guest_register($id, $data){
        $guest_register = $this->get_guest_register_by_id($id);
        $data['status'] = 0;
        if ($this->db->update('guests_register', $data, ['id'=>$id])) {
            if ($this->db->update('rooms', ['status'=>0], ['id'=>$guest_register->room_id])) {
                return true;
            }
        }
        return false;
    }
    public function validate_no_in_another_open_register($document_type, $vat_no){
        $q = $this->db->select('*')
                 ->join('guests_register_detail', 'guests_register_detail.guest_id = guests.id', 'join')
                 ->join('guests_register', 'guests_register.id = guests_register_detail.register_id', 'join')
                 ->where('guests.vat_no', $vat_no)
                 ->where('guests.document_type_id', $document_type)
                 ->get('guests');
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }
    public function get_room_by_id($room_id){
        $q = $this->db->get_where('rooms', ['id'=>$room_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }
}