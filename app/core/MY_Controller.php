<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $enableElectronicPayroll  = false;
    public $enableDocumentsReception = false;
    public $enableSupportingDocument = false;
    public $data = [];

    public function __construct()
    {
        parent::__construct();
        setlocale(LC_TIME, "es_CO.utf8");
        $this->Settings = $this->site->get_setting();
        $this->pos_settings = $this->site->get_pos_setting();
        $this->shop_settings = $this->site->getShopSettings();

        $this->load->admin_model('sales_model');
        $this->load->admin_model('pos_model');
        $this->loggedIn = $this->sma->logged_in();
        $rutas_limpiar  = [
                                'app/logs',
                                'assets/uploads/csv',
                            ];
        $this->sma->eliminar_archivos_antiguos($rutas_limpiar, ['csv', 'php', 'txt'], 30);
        if($sma_language = $this->input->cookie('sma_language', TRUE)) {
            $this->config->set_item('language', $sma_language);
            $this->lang->admin_load('sma', $sma_language);
            $this->Settings->user_language = $sma_language;
        } else {
            if ($this->loggedIn && $this->session->userdata('language')) {
                $this->config->set_item('language', $this->session->userdata('language'));
                $this->lang->admin_load('sma', $this->session->userdata('language'));
                $this->Settings->user_language = $this->session->userdata('language');
            } else {
                $this->config->set_item('language', $this->Settings->language);
                $this->lang->admin_load('sma', $this->Settings->language);
                $this->Settings->user_language = $this->Settings->language;
            }
        }
        if($rtl_support = $this->input->cookie('sma_rtl_support', TRUE)) {
            $this->Settings->user_rtl = $rtl_support;
        } else {
            $this->Settings->user_rtl = $this->Settings->rtl;
        }
        $this->theme = $this->Settings->theme.'/admin/views/';
        $this->theme_path = base_url('themes/default/admin/views/');
        if(is_dir(VIEWPATH.$this->Settings->theme.DIRECTORY_SEPARATOR.'assets'.DIRECTORY_SEPARATOR)) {
            $this->data['assets'] = base_url() . 'themes/' . $this->Settings->theme . '/assets/';
        } else {
            $this->data['assets'] = base_url() . 'themes/default/admin/assets/';
        }
        $this->data['Settings'] = $this->Settings;
        $this->sma->update_users_session_expirated();
        if($this->loggedIn) {
            $this->default_currency = $this->site->getCurrencyByCode($this->Settings->default_currency);
            $this->data['default_currency'] = $this->default_currency;
            $this->Owner = $this->sma->in_group('owner') ? TRUE : NULL;
            $this->data['Owner'] = $this->Owner;
            $this->Customer = $this->sma->in_group('customer') ? TRUE : NULL;
            $this->data['Customer'] = $this->Customer;
            $this->Supplier = $this->sma->in_group('supplier') ? TRUE : NULL;
            $this->data['Supplier'] = $this->Supplier;
            $this->Admin = $this->sma->in_group('admin') ? TRUE : NULL;
            $this->data['Admin'] = $this->Admin;
            if($sd = $this->site->getDateFormat($this->Settings->dateformat)) {
                $dateFormats = array(
                    'js_sdate' => $sd->js,
                    'php_sdate' => $sd->php,
                    'mysq_sdate' => $sd->sql,
                    'js_ldate' => $sd->js . ' hh:ii',
                    'php_ldate' => $sd->php . ' H:i',
                    'mysql_ldate' => $sd->sql . ' %H:%i'
                    );
            } else {
                $dateFormats = array(
                    'js_sdate' => 'mm-dd-yyyy',
                    'php_sdate' => 'm-d-Y',
                    'mysq_sdate' => '%m-%d-%Y',
                    'js_ldate' => 'mm-dd-yyyy hh:ii:ss',
                    'php_ldate' => 'm-d-Y H:i:s',
                    'mysql_ldate' => '%m-%d-%Y %T'
                    );
            }
            if(file_exists(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'admin'.DIRECTORY_SEPARATOR.'Pos.php')) {
                define("POS", 1);
            } else {
                define("POS", 0);
            }
            if(file_exists(APPPATH.'controllers'.DIRECTORY_SEPARATOR.'shop'.DIRECTORY_SEPARATOR.'Shop.php')) {
                define("SHOP", 1);
            } else {
                define("SHOP", 0);
            }
            if(!$this->Owner && !$this->Admin) {
                $gp = $this->site->checkPermissions();
                $this->GP = $gp[0];
                $this->data['GP'] = $gp[0];
            } else {
                $this->GP = NULL;
                $this->data['GP'] = NULL;
            }
            $this->dateFormats = $dateFormats;
            $this->data['dateFormats'] = $dateFormats;
            $this->load->language('calendar');
            $this->m = strtolower($this->router->fetch_class());
            $this->v = strtolower($this->router->fetch_method());
            $this->data['m']= $this->m;
            $this->data['v'] = $this->v;
            $this->data['dt_lang'] = json_encode(lang('datatables_lang'));
            $this->data['dp_lang'] = json_encode(array('days' => array(lang('cal_sunday'), lang('cal_monday'), lang('cal_tuesday'), lang('cal_wednesday'), lang('cal_thursday'), lang('cal_friday'), lang('cal_saturday'), lang('cal_sunday')), 'daysShort' => array(lang('cal_sun'), lang('cal_mon'), lang('cal_tue'), lang('cal_wed'), lang('cal_thu'), lang('cal_fri'), lang('cal_sat'), lang('cal_sun')), 'daysMin' => array(lang('cal_su'), lang('cal_mo'), lang('cal_tu'), lang('cal_we'), lang('cal_th'), lang('cal_fr'), lang('cal_sa'), lang('cal_su')), 'months' => array(lang('cal_january'), lang('cal_february'), lang('cal_march'), lang('cal_april'), lang('cal_may'), lang('cal_june'), lang('cal_july'), lang('cal_august'), lang('cal_september'), lang('cal_october'), lang('cal_november'), lang('cal_december')), 'monthsShort' => array(lang('cal_jan'), lang('cal_feb'), lang('cal_mar'), lang('cal_apr'), lang('cal_may'), lang('cal_jun'), lang('cal_jul'), lang('cal_aug'), lang('cal_sep'), lang('cal_oct'), lang('cal_nov'), lang('cal_dec')), 'today' => lang('today'), 'suffix' => array(), 'meridiem' => array()));
            $this->Settings->indian_gst = FALSE;
            if ($this->Settings->invoice_view > 0) {
                $this->Settings->indian_gst = $this->Settings->invoice_view == 2 ? TRUE : FALSE;
                $this->Settings->format_gst = TRUE;
                $this->load->library('gst');
            }
            $this->load->library('gscat');
            if (!$this->sma->update_user_login_hour()) {
                redirect('logout');
            }
            if ($this->Owner) {
                $this->enableElectronicPayroll  = true;
                $this->enableDocumentsReception = true;
                $this->enableSupportingDocument = true;
            } else {
                if ($this->Settings->electronic_payroll == YES) { $this->enableElectronicPayroll = true; }
                if ($this->Settings->documents_reception == YES) { $this->enableDocumentsReception = true; }
                if ($this->Settings->supportingDocument == YES) { $this->enableSupportingDocument = true; }
            }
        }
        //VERSION !!!!
        $version_actual = '2025.1.3.4';
        //VERSION !!!!
        if ($this->Settings->version != $version_actual) {
            $this->db->update('settings', array('version' => $version_actual), array('setting_id' => 1));
        }
        $filtros_fecha_inicial = NULL;
        $filtros_fecha_final = NULL;
        $this->hide_date_range = false;
        $this->meses_para_trimestre = $this->obtener_trimestre();
        $this->meses_para_semestre = $this->obtener_semestre();
        $this->trimestre_pasado = $this->obtener_trimestre(true);
        $this->semestre_pasado = $this->obtener_semestre(true);
        $fecha_actual = date('d/m/Y');
        if ($this->Settings->default_records_filter == 1) { // HOY
            $filtros_fecha_inicial = $fecha_actual;
            $filtros_fecha_final = $fecha_actual;
        } else if ($this->Settings->default_records_filter == 2) { // MES
            $filtros_fecha_inicial = date("01/m/Y");
            $filtros_fecha_final = $fecha_actual;
        } else if ($this->Settings->default_records_filter == 3) { // TRIMESTRE
            $filtros_fecha_inicial = date("d/m/Y", strtotime(date("Y-m-01")."- ".$this->meses_para_trimestre." month"));
            $filtros_fecha_final = $fecha_actual;
        } else if ($this->Settings->default_records_filter == 4) { // AÑO
            $filtros_fecha_inicial =  date('01/01/Y');
            $filtros_fecha_final = $fecha_actual;
        } else if ($this->Settings->default_records_filter == 5 || $this->Settings->default_records_filter == 0) { // RANGO DE FECHAS
            $this->hide_date_range = false;
        }
        $this->filtros_fecha_inicial = $filtros_fecha_inicial;
        $this->filtros_fecha_final = $filtros_fecha_final;
        $this->deposit_payment_method = $this->site->validate_payment_method_active('deposit');
        $this->due_payment_method = $this->site->validate_payment_method_active('Credito');
        // $this->site->set_users_erp_data();
        $this->run_update = false;
        if ($this->Settings->automatic_update == 1 && isset($this->v) &&  $this->v != 'get_update_files' && $this->v != 'maintenance') {
            $console_service_data = $this->site->get_console_service_data();

            if ($this->Settings->version > '2021.1.4' && $console_service_data && $console_service_data->current_version && $console_service_data->current_version > $this->Settings->version) {
                $this->run_update = true;
            }
        }
        $payment_methods_billers = [];
        $q = $this->db->get('payment_methods');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $pm) {
                $billers = json_decode($pm->billers ? $pm->billers : "");
                if ($billers && count($billers) > 0) {
                    foreach ($billers as $bll_key => $biller) {
                        $payment_methods_billers[$pm->code][] = $biller;
                    }
                }
            }
        }
        $this->payment_methods_billers = json_encode($payment_methods_billers);
        if ($this->session->userdata('account_locked') && $this->session->userdata('account_locked') == 1 && $this->v != 'account_locked' && $this->v != 'logout') {
            admin_redirect('calendar/account_locked');
        }
        if ($this->Settings->annual_closure == 1 && $this->Settings->years_database_management == 1) {
            $db_arr = explode("_", $this->db->database);
            $anno_db = isset($db_arr[2]) ? $db_arr[2] : NULL;
            $year_actual = date('Y');
            $ano = substr($year_actual, -2, 2);
            if ($anno_db && $anno_db != $ano && isset($this->m)) {
                if (
                        $this->v == 'add' || $this->v == 'edit' || $this->v == 'add_wholesale' || $this->v == 'add_expense' ||
                        $this->v == 'padd' || $this->v == 'cancel' || $this->v == 'pcancel' || $this->v == 'exadd' ||
                        $this->v == 'add_adjustment' || $this->v == 'add_product_transformation' || $this->v == 'return_sale' ||
                        $this->v == 'return_purchase' ||
                        ($this->m == 'pos' && $this->v == 'index')
                    ) {
                    $this->session->set_flashdata('error', lang('annual_closure_restriction'));
                    admin_redirect('calendar');
                }
            }
        }
        if ($this->Settings->close_year_step >= 2 && isset($this->m)) {
            if (
                    $this->v == 'add' || $this->v == 'edit' || $this->v == 'add_wholesale' || $this->v == 'add_expense' ||
                    $this->v == 'padd' || $this->v == 'cancel' || $this->v == 'pcancel' || $this->v == 'exadd' ||
                    $this->v == 'add_adjustment' || $this->v == 'add_product_transformation' || $this->v == 'return_sale' ||
                    $this->v == 'return_purchase' ||
                    ($this->m == 'pos' && $this->v == 'index')
                ) {
                $this->session->set_flashdata('error', lang('closed_year_restriction'));
                admin_redirect('calendar');
            }
        }

        $start_year = date('Y', strtotime($this->Settings->system_start_date));
        $end_year = date('Y');
        $this->filter_year_options = [];
        for ($i=$start_year; $i <= $end_year ; $i++) {
            $this->filter_year_options[$i] = $i;
        }
        $this->año_actual_movimientos = $this->Settings->years_database_management == 1 ? false : "20".(substr($this->db->database, -2, 2));
        $this->erp_db = false;
        if ($this->Settings->interconect_login == 1) {
            $this->erp_db = $this->site->get_erp_db();
        }
        if (strpos($this->db->database, "_prueba") === false && $_SERVER['SERVER_NAME'] != 'localhost' && ($this->Settings->db_global_hostname_saved == NULL || $this->Settings->db_global_hostname_saved != $_SERVER['HTTP_HOST'])) {
            $DBGLOBAL = $this->site->get_erp_db();
            if ($DBGLOBAL) {
                $hosts_ips = [];
                $host_ips_q = $DBGLOBAL->get('wappsi_ip_hosts');
                foreach (($host_ips_q->result()) as $hiq_row) {
                    $hosts_ips[$hiq_row->host_url] = $hiq_row->host_ip;
                }
                $db_exists = $DBGLOBAL->not_like('wappsi_customers_databases.database', 'prueba')
                                       ->not_like('wappsi_customers_databases.database', 'demo')
                                       ->where('vat_no', $this->Settings->numero_documento)
                                       ->get('wappsi_customers_databases');
                if ($db_exists->num_rows() == 0) {
                    $insert = $DBGLOBAL->insert('wappsi_customers_databases',
                                [
                                    'vat_no' => $this->Settings->numero_documento,
                                    'wappsi_customer_address_id' => $this->Settings->wappsi_customer_address_id,
                                    'hostname' => $this->sma->wappsi_encrypt($hosts_ips[$_SERVER['HTTP_HOST']], 'wappsi'),
                                    'username' => $this->sma->wappsi_encrypt($this->db->username, 'wappsi'),
                                    'password' => $this->sma->wappsi_encrypt($this->db->password, 'wappsi'),
                                    'database' => $this->db->database,
                                    'update_at' => date('Y-m-d H:i:s'),
                                ]
                                );
                    if ($insert) {
                        $this->db->update('settings', ['db_global_hostname_saved'=>$_SERVER['HTTP_HOST']], ['setting_id' => 1]);
                        $this->session->set_flashdata('message', 'Base datos guardada');
                    }
                } else {
                    $db_exists = $db_exists->row();
                    $update = $DBGLOBAL->update('wappsi_customers_databases', [
                        'vat_no' => $this->Settings->numero_documento,
                        'wappsi_customer_address_id' => $this->Settings->wappsi_customer_address_id,
                        'hostname' => $this->sma->wappsi_encrypt($hosts_ips[$_SERVER['HTTP_HOST']], 'wappsi'),
                        'username' => $this->sma->wappsi_encrypt($this->db->username, 'wappsi'),
                        'password' => $this->sma->wappsi_encrypt($this->db->password, 'wappsi'),
                        'update_at' => date('Y-m-d H:i:s'),
                    ], ['id'=>$db_exists->id]);
                    if ($update) {
                        $this->db->update('settings', ['db_global_hostname_saved'=>$_SERVER['HTTP_HOST']], ['setting_id' => 1]);
                        $this->session->set_flashdata('message', 'Base datos actualizada');
                    }
                }
            } else {
                $this->session->set_flashdata('error', 'Error conexion global');
            }
        }
        $this->meses = array(
            1 => lang('January'),
            2 => lang('February'),
            3 => lang('March'),
            4 => lang('April'),
            5 => lang('May'),
            6 => lang('June'),
            7 => lang('July'),
            8 => lang('August'),
            9 => lang('September'),
            10 => lang('October'),
            11 => lang('November'),
            12 => lang('December')
        );
        $billers = $this->site->getAllCompaniesWithState('biller');
        $billers_data = [];
        foreach ($billers as $biller) {
            $biller->pin_code = $biller->pin_code ? md5($biller->pin_code) : NULL;
            unset($biller->invoice_footer);
            if ($biller->warehouses_related) {
                $biller->warehouses_related = json_decode($biller->warehouses_related);
            }
            $billers_data[$biller->id] = $biller;
        }
        $this->billers_data = ($billers_data);
        // exit(var_dump($this->billers_data));
        $this->set_settings_lang();
        $this->modules_index = [
                                'index' => 1,
                                'pindex' => 1,
                                'fe_index' => 1,
                                'orders' => 1,
                                'sales' => 1,
                                'supporting_document_index' => 1,
                                'imports' => 1,
                            ];
        $this->modules_NOT_index = [
                                'pos' => 1,
                            ];

        // $this->log_queries();
        $this->deleteQrImages();
    }

    public function page_construct($page, $meta = array(), $data = array())
    {

        $meta['message'] = isset($data['message']) ? $data['message'] : $this->session->flashdata('message');
        $meta['error'] = isset($data['error']) ? $data['error'] : $this->session->flashdata('error');
        $meta['warning'] = isset($data['warning']) ? $data['warning'] : $this->session->flashdata('warning');
        if (!$this->session->userdata('getNotifications')) { // Si no se ha consultado
            $this->session->set_userdata('getNotifications', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('notifications', $this->site->getNotifications()); // Consulta y guarda en la session
        }
        $meta['info'] = $this->session->userdata('notifications'); // si ya existe lo asigna de la session sin consultar la base de datos

        if (!$this->session->userdata('get_pending_sales_dian')) { // Si no se ha consultado
            $this->session->set_userdata('get_pending_sales_dian', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('pending_sales_dian', $this->site->get_pending_sales_dian()); // Consulta y guarda en la session
        }
        $meta['pending_electronic_invoice'] = $this->session->userdata('pending_sales_dian');

        if (!$this->session->userdata('getUpcomingEvents')) { // Si no se ha consultado
            $this->session->set_userdata('getUpcomingEvents', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('UpcomingEvents', $this->site->getUpcomingEvents()); // Consulta y guarda en la session
        }
        $meta['events'] = $this->session->userdata('UpcomingEvents');

        if (!$this->session->userdata('get_not_accepted_transfers')) { // Si no se ha consultado
            $this->session->set_userdata('get_not_accepted_transfers', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('not_accepted_transfers', $this->site->get_not_accepted_transfers($this->session->userdata('biller_id'), $this->session->userdata('warehouse_id'))); // Consulta y guarda en la session
        }
        $meta['new_transfers'] = $this->session->userdata('not_accepted_transfers');

        if (!$this->session->userdata('get_customer_birthdays')) { // Si no se ha consultado
            $this->session->set_userdata('get_customer_birthdays', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('customer_birthdays', $this->site->get_customer_birthdays()); // Consulta y guarda en la session
        }
        $meta['customer_birthday_alerts'] = $this->session->userdata('customer_birthdays');

        if (!$this->session->userdata('get_customer_birthdays_today')) { // Si no se ha consultado
            $this->session->set_userdata('get_customer_birthdays_today', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('customer_birthdays_today', $this->site->get_customer_birthdays(true)); // Consulta y guarda en la session
        }
        $meta['today_customer_birthday_alerts'] = $this->session->userdata('customer_birthdays_today');

        if (!$this->session->userdata('get_tax_exempt')) { // Si no se ha consultado
            $this->session->set_userdata('get_tax_exempt', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('tax_exempt', $this->site->get_tax_exempt()); // Consulta y guarda en la session
        }
        $meta['except_tax_rate_id'] = $this->session->userdata('tax_exempt');

        $meta['ip_address'] = $this->input->ip_address();
        $meta['Owner'] = $data['Owner'];
        $meta['Admin'] = $data['Admin'];
        $meta['Supplier'] = $data['Supplier'];
        $meta['Customer'] = $data['Customer'];
        $meta['Settings'] = $data['Settings'];
        $meta['pos_settings'] = $this->pos_settings;
        // $this->sma->print_arrays($meta['pos_settings']);
        $meta['dateFormats'] = $data['dateFormats'];
        $meta['assets'] = $data['assets'];
        $files_version = "?n=".$this->Settings->version;
        $meta['files_version'] = $files_version;
        // exit(var_dump($data['GP']));
        $meta['GP'] = $data['GP'];
        $meta['pos_register_status'] = $this->pos_model->registerData($this->session->userdata('user_id'));
        // exit(var_dump($data['GP']['system_settings-update_products_group_prices']));
        if ($this->session->userdata('account_locked') && $this->session->userdata('account_locked') == 2) {
            $meta['page_title'] = lang('account_for_reading_only')." ".$meta['page_title'];
        }
        if ($this->Owner || $this->Admin || $data['GP']['reports-quantity_alerts'] == 1) {
            if (!$this->session->userdata('get_total_qty_alerts')) { // Si no se ha consultado
                $this->session->set_userdata('get_total_qty_alerts', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
                $this->session->set_userdata('total_qty_alerts', $this->site->get_total_qty_alerts()); // Consulta y guarda en la session
            }
            $meta['qty_alert_num'] = $this->session->userdata('total_qty_alerts');
        } else {
            $meta['qty_alert_num'] = NULL;
        }

        if (!$this->session->userdata('get_new_order_sales')) { // Si no se ha consultado
            $this->session->set_userdata('get_new_order_sales', 1); // Cambia el valor para que en la siguiente vista no vuelva a entrar
            $this->session->set_userdata('new_order_sales', $this->site->get_new_order_sales()); // Consulta y guarda en la session
        }
        $meta['new_order_sales'] = $this->session->userdata('new_order_sales');
        // $meta['exp_alert_num'] = $this->site->get_expiring_qty_alerts();
        // $meta['shop_sale_alerts'] = SHOP ? $this->site->get_shop_sale_alerts() : 0;
        // $meta['shop_payment_alerts'] = SHOP ? $this->site->get_shop_payment_alerts() : 0;

        $meta['exp_alert_num'] = NULL;
        $meta['shop_sale_alerts'] = NULL;
        $meta['shop_payment_alerts'] = NULL;
        // $meta['customer_birthday_alerts'] = NULL;
        $meta['sid'] = (!empty($data['sid'])) ? $data['sid'] : NULL;
        $this->session->unset_userdata('post_sending');
        $this->session->unset_userdata('post_sending_saved');
        $this->set_settings_lang();
        $this->load->view($this->theme . 'header', $meta);
        $this->load->view($this->theme . $page, $data);
        $this->load->view($this->theme . 'footer');
    }

    public function load_view($url, $data)
    {
        $this->set_settings_lang();
        $this->load->view($url, $data);
    }

    public function set_settings_lang()
    {
        /* PERSONALIZACIÓN DE LENGUAJE */
        if (isset($this->Settings) && $this->Settings->product_variant_language) {
            set_lang('product_variant', $this->Settings->product_variant_language);
            set_lang('variant', $this->Settings->product_variant_language);
        }
        if ( isset($this->Settings) && $this->Settings->product_variants_language) {
            set_lang('product_variants', $this->Settings->product_variants_language);
            set_lang('variants', $this->Settings->product_variants_language);
        }
        if ( isset($this->Settings) && $this->Settings->gift_card_language) {
            set_lang('gift_card', $this->Settings->gift_card_language);
        }
        if ( isset($this->Settings) && $this->Settings->gift_cards_language) {
            set_lang('gift_cards', $this->Settings->gift_cards_language);
        }
        if ( isset($this->Settings) && $this->Settings->rete_other_language) {
            set_lang('rete_other', $this->Settings->rete_other_language);
        }
        if ( isset($this->Settings) && $this->Settings->customer_branch_language) {
            set_lang('customer_branch', $this->Settings->customer_branch_language);
            set_lang('add_customer_branch', lang('add')." ".$this->Settings->customer_branch_language);
            set_lang('edit_customer_branch', lang('edit')." ".$this->Settings->customer_branch_language);
        }
        if ( isset($this->Settings) && $this->Settings->customer_branches_language) {
            set_lang('customer_branches', $this->Settings->customer_branches_language);
            set_lang('addresses', $this->Settings->customer_branches_language);
        }
        if (isset($this->pos_settings) && $this->pos_settings->restobar_mode == 1) {
            set_lang('seller', 'Mesero');
        }
        if (isset($this->Settings) && $this->Settings->brand_language) {
            set_lang('brand', $this->Settings->brand_language);
        }
        if (isset($this->Settings) && $this->Settings->brands_language) {
            set_lang('brands', $this->Settings->brands_language);
        }
        if (isset($this->Settings) && $this->Settings->category_language) {
            set_lang('category', $this->Settings->category_language);
        }
        if (isset($this->Settings) && $this->Settings->categories_language) {
            set_lang('categories', $this->Settings->categories_language);
        }
        if (isset($this->Settings) && $this->Settings->subcategory_language) {
            // exit(var_dump($this->Settings->subcategory_language));
            set_lang('subcategory', $this->Settings->subcategory_language);
        }
        if (isset($this->Settings) && $this->Settings->subcategories_language) {
            set_lang('subcategories', $this->Settings->subcategories_language);
        }
        if (isset($this->Settings) && $this->Settings->subsubcategory_language) {
            set_lang('second_level_subcategory_id', $this->Settings->subsubcategory_language);
            set_lang('subsubcategory', $this->Settings->subsubcategory_language);
        }
        if (isset($this->Settings) && $this->Settings->subsubcategories_language) {
            set_lang('subsubcategories', $this->Settings->subsubcategories_language);
        }
        if (isset($this->Settings) && $this->Settings->payments_collections_language) {
            set_lang('payment_collection', $this->Settings->payment_collection_language);
            set_lang('payments_collections', $this->Settings->payments_collections_language);
        }
        if (isset($this->Settings) && $this->Settings->credit_financing_language) {
            set_lang('credit_financing_language', $this->Settings->credit_financing_language);
        }
        if (isset($this->Settings) && $this->Settings->tip_language) {
            set_lang('tip', $this->Settings->tip_language);
            set_lang('tip_amount', $this->Settings->tip_language);
            set_lang('tip_suggested', $this->Settings->tip_language);
            // set_lang('credit_financing_language', $this->Settings->tip_language);
        }

        if (isset($this->Settings) && $this->Settings->shipping_language) {
            set_lang('shipping', $this->Settings->shipping_language);
        }
        if (isset($this->Settings) && $this->Settings->restobar_table_language) {
            set_lang('mesa', $this->Settings->restobar_table_language);
            set_lang('restobar', $this->Settings->restobar_table_language);
        }
        /* PERSONALIZACIÓN DE LENGUAJE */
    }

    private function obtener_trimestre($past = false)
    {
        $trimestres = [1 => [1,2,3], 2 => [4,5,6], 3 => [7,8,9], 4 => [10,11,12]];
        if ($past) {
            $trimestre_actual = null;
            foreach($trimestres as $trimestre_n => $trimestre){
                foreach($trimestre as $mes){
                    if($mes == date('m')){
                        $trimestre_actual = $trimestre_n;
                        break;
                    }
                }
            }
            $año_pasado = ($trimestre_actual - 1 == 0 ? 1 : 0);
            $trimestre_pasado = ($trimestre_actual - 1 == 0 ? 4 : $trimestre_actual - 1);
            $mes_inicio_trimestre = $trimestres[$trimestre_pasado][0];
            $mes_fin_trimestre = $trimestres[$trimestre_pasado][2];
            return ['inicio_trimestre' => ($mes_inicio_trimestre < 10 ? "0".$mes_inicio_trimestre : $mes_inicio_trimestre), 'fin_trimestre' => ($mes_fin_trimestre < 10 ? "0".$mes_fin_trimestre : $mes_fin_trimestre), 'año_pasado' => $año_pasado];
        } else {
            foreach($trimestres as $trimestre){
                $meses = 0;
                foreach($trimestre as $mes){
                    if($mes == date('m')){
                        return $meses;
                    }
                    $meses++;
                }
            }
        }
    }

    private function obtener_semestre($past = false)
    {
        $semestres = [1 => [1,2,3,4,5,6], 2 => [7,8,9,10,11,12]];
        if ($past) {
            $semestre_actual = null;
            foreach($semestres as $semestre_n => $semestre){
                foreach($semestre as $mes){
                    if($mes == date('m')){
                        $semestre_actual = $semestre_n;
                        break;
                    }
                }
            }
            $año_pasado = $semestre_actual == 1 ? 1 : 0;
            $mes_inicio_semestre = $semestres[$semestre_actual == 1 ? 2 : 1][0];
            $mes_fin_semestre = $semestres[$semestre_actual == 1 ? 2 : 1][5];
            return ['inicio_semestre' => ($mes_inicio_semestre < 10 ? "0".$mes_inicio_semestre : $mes_inicio_semestre), 'fin_semestre' => ($mes_fin_semestre < 10 ? "0".$mes_fin_semestre : $mes_fin_semestre), 'año_pasado' => $año_pasado];
        } else {
            foreach($semestres as $semestre){
                $meses = 0;
                foreach($semestre as $mes){
                    if($mes == date('m')){
                        return $meses;
                    }
                    $meses++;
                }
            }
        }
    }

    public function saleView($id = null, $internal_download = FALSE, $download = FALSE, $for_email = FALSE,  $quickPrintFromPos = FALSE, $quickPrint = FALSE)
    {
        $this->sma->checkPermissions('index');

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->load->library('qr_code');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getSaleByID($id);
        $document_type = $this->site->getDocumentTypeById($inv->document_type_id);
        if (!empty($inv->sale_id)) {
            if (!empty($inv->year_database)) {
                $affected_bill = $this->site->get_past_year_sale($inv->return_sale_ref, $inv->year_database);
                // $resolution_data_referenced_invoice = $this->site->get_past_year_document_type_by_id($reference_invoice->document_type_id, $inv->year_database);
            } else {
                $affected_bill = $this->sales_model->getSaleByID($inv->sale_id);
            }

            $this->data['affected_bill'] = $affected_bill;
        }
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        //Declaramos una carpeta temporal para guardar la imagenes generadas
        $dir = 'themes/default/admin/assets/images/qr_code/';
        //Declaramos la ruta y nombre del archivo a generar
        $filename = $dir . $inv->reference_no.'.png';
        //Si no existe la carpeta la creamos
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        if ($inv->codigo_qr && $inv->codigo_qr != '') {
            QRcode::png($inv->codigo_qr, $filename, 0, 3, 0);
        }

        $quickPrintFormatId = $document_type->quick_print_format_id;
        if ($quickPrint == FALSE) {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        } else {
            if (!empty($quickPrintFormatId)) {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($document_type->quick_print_format_id);
            } else {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
            }
        }

        if (!empty($inv->fe_xml)) {
            $xml_file = base64_decode($inv->fe_xml);

            $xml = new DOMDocument("1.0", "ISO-8859-15");
            $xml->loadXML($xml_file);

            $path = 'files/electronic_billing';
            if (!file_exists($path)) {
                mkdir($path, 0777);
            }
            $xml->save('files/electronic_billing/' . $inv->reference_no . '.xml');

            $positionDate = strpos($xml_file, '<cbc:ValidationDate>');
            $substringDate = substr($xml_file, $positionDate);
            $substringDate2 = substr($substringDate, 20, 10);

            $positionHour = strpos($xml_file, '<cbc:ValidationTime>');
            $substringHour = substr($xml_file, $positionHour);
            $substringHour2 = substr($substringHour, 20, 8);

            $validationDateTime = $substringDate2 .' '. $substringHour2;

            $this->data['validationDateTime'] = $validationDateTime;

            if (file_exists(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml')) {
                unlink(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml');
            }
        } else {
            $this->data['validationDateTime'] = '';
        }
        if (isset($this->Settings) && $this->Settings->tip_language) {
            set_lang('tip', $this->Settings->tip_language);
            set_lang('tip_amount', $this->Settings->tip_language);
            set_lang('tip_suggested', $this->Settings->tip_language);
        }

        $this->data['modal'] = FALSE;
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['quickPrintFormatId'] = $quickPrintFormatId;
        $this->data['quickPrintFromPos'] = $quickPrintFromPos;
        $this->data['internal_download'] = $internal_download;
        $this->data['qr_code'] = $filename;
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['page_title'] = $this->lang->line("invoice");
        // $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['payments'] = $this->pos_model->getInvoicePayments($id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id, false);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        // $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id, NULL, false, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $this->data['rows'] = $this->pos_model->getAllInvoiceItems($id, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
		$this->data['paypal'] = $this->sales_model->getPaypalSettings();
		$this->data['skrill'] = $this->sales_model->getSkrillSettings();
		$this->data['settings'] = $this->Settings;
		$this->data['seller'] = $this->site->getSellerById(!empty($inv->seller_id) ? $inv->seller_id : $this->data['biller']->default_seller_id);
        $this->data['filename'] = $this->site->getFilename($id);
        $ciius_rows = $this->site->get_ciiu_code_by_ids(explode(",", $this->Settings->ciiu_code));
        $ciius = "";
        if ($ciius_rows) {
            foreach ($ciius_rows as $ciiu) {
                $ciius.= $ciiu->code.", ";
            }
        }
        $this->data['ciiu_code'] = trim($ciius, ", ");
		$this->data['sma'] = $this->sma;
		$this->data['document_type'] = $document_type;

        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        if ($this->Settings->great_contributor == 1) {
            $this->data['tipo_regimen'] = lang('great_contributor');
        } else {
            $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        }
        $payments = $this->sales_model->getPaymentsForSale($id);
        $original_payment_method = '';
        $original_payment_method_with_amount = '';
        $total_original_payments_amount = 0;
        $direct_payments_num = 0;
        if ($payments) {
            foreach ($payments as $payment) {
                if ($payment->paid_by == "retencion") {
                    $total_original_payments_amount += $payment->amount;
                }
                if (date('Y-m-d H', strtotime($payment->date)) == date('Y-m-d H', strtotime($inv->date)) && $payment->paid_by != "retencion") {
                    if ($original_payment_method == '') {
                        $original_payment_method = lang($payment->paid_by) . ", ";
                    } else {
                        $original_payment_method .= lang($payment->paid_by) . ", ";
                    }
                    if ($original_payment_method_with_amount == '') {
                        $original_payment_method_with_amount = lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    } else {
                        $original_payment_method_with_amount .= lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    }
                    $direct_payments_num++;
                    $total_original_payments_amount += $payment->amount;
                }
            }
            if ($direct_payments_num > 1) {
                $inv->note .= $original_payment_method_with_amount;
                $original_payment_method = 'Varios, ver nota';
            } else {
            }
        }
        if ($original_payment_method != '') {
            $original_payment_method = trim($original_payment_method, ", ");
        }
        if ($original_payment_method == '' || ($total_original_payments_amount < $inv->grand_total)) {
            $original_payment_method .= ($original_payment_method != '' ? ", " : "") . lang('due') . " " . $inv->payment_term . ($inv->payment_term > 1 ? " Días" : " Día");
        }

        $currencies = $this->site->getAllCurrencies();
        $currencies_names = [];
        if ($currencies) {
            foreach ($currencies as $currency) {
                $currencies_names[$currency->code] = $currency->name;
            }
        }
        $this->data['currencies_names'] = $currencies_names;
        $this->data['sale_payment_method'] = $original_payment_method;
        $prueba = false;
        $url_format = "sales/sale_view";
        $view_tax = true;
        $tax_inc = true;
        $this->data['document_type_invoice_format'] = false;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
        $this->data['biller_logo'] = 2;
        $this->data['show_code'] = 1;
        $this->data['product_detail_promo'] = 1;
        $this->data['show_document_type_header'] = 1;
        $this->data['show_product_preferences'] = 1;
        $this->data['tax_indicator'] = 0;
        $this->data['product_detail_font_size'] = 0;
        $this->data['show_award_points'] = 1;
        $this->data['pos'] = $this->pos_model->getSetting();
        if (!$prueba) {
            $url_format = "sales/sale_view";
            if ($inv->sale_status == 'returned') {
                $url_format = "sales/return_sale_view";
            }
            if ($document_type_invoice_format) {
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
                $this->data['biller_logo'] = $document_type_invoice_format->logo;
                $this->data['show_code'] = $document_type_invoice_format->product_show_code;
                $this->data['product_detail_promo'] = $document_type_invoice_format->product_detail_promo;
                $this->data['show_document_type_header'] = $document_type_invoice_format->show_document_type_header;
                $this->data['document_type_invoice_format'] = $document_type_invoice_format;
                $this->data['show_product_preferences'] = $document_type_invoice_format->show_product_preferences;
                $this->data['tax_indicator'] = $document_type_invoice_format->tax_indicator;
                $this->data['product_detail_font_size'] = $document_type_invoice_format->product_detail_font_size > 0 ? $document_type_invoice_format->product_detail_font_size : 0;
				$url_format = $document_type_invoice_format->format_url;
				$view_tax = $document_type_invoice_format->view_item_tax ? true : false;
				$tax_inc = $document_type_invoice_format->tax_inc ? true : false;
    		}
        }

        $this->data['view_tax'] = $view_tax;
        $this->data['tax_inc'] = $tax_inc;
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = (!$document_type_invoice_format || ($document_type_invoice_format && $document_type_invoice_format->tax_indicator == 1) ? "(" . $tax->tax_indicator . ") " : "") . $tax->name;
        }
        $this->data['taxes_details'] = $taxes_details;
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->customer_id);
        }
        $this->data['signature_root'] = is_file("assets/uploads/signatures/" . $this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : false;
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
        $currency = $this->site->getCurrencyByCode($inv->sale_currency);
        $trmrate = 1;
        if (!empty($inv->sale_currency) && $inv->sale_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->sale_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;
        $this->data['download'] = $download;
        $this->data['for_email'] = $for_email === true ? $for_email : false;

        if ($this->Settings->fe_technology_provider == CADENA) {
            $this->data["technologyProviderLogo"] = "assets/images/cadena_logo.jpeg";
        } else if ($this->Settings->fe_technology_provider == BPM) {
            $this->data["technologyProviderLogo"] = "assets/images/bpm_logo.jpeg";
        } else if ($this->Settings->fe_technology_provider == SIMBA) {
            $this->data["technologyProviderLogo"] = "assets/images/simba_logo.png";
        } else {
            $this->data["technologyProviderLogo"] = "assets/images/delcop_logo.png";
        }

        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;
        $this->data['print_directly'] = $print_directly;

        $this->load->view($this->theme . $url_format, $this->data);
    }

    private $last_log_time_file ="";
    public function log_queries()
    {
        $this->last_log_time_file = APPPATH.'logs/'.$this->session->userdata('user_id').'_last_log_time.txt';
        // Verifica si han pasado más de 5 minutos desde la última vez que se registraron las consultas
        $current_time = time();
        $last_log_time = $this->get_last_log_time();

        if (($current_time - $last_log_time) > 300) { // 300 segundos = 5 minutos
            // Accede a las consultas y tiempos de ejecución
            $queries = $this->db->queries;
            $query_times = $this->db->query_times;
            // Limpia las consultas guardadas
            $this->db->queries = [];
            $this->db->query_times = [];
            // Prepara el contenido del log
            $log_content = '';
            $long_query_content  = '';
            foreach ($queries as $index => $query) {
                $time = isset($query_times[$index]) ? $query_times[$index] : 'N/A';
                $log_content .= "Query: " . $query . "\n";
                $log_content .= "Execution Time: " . $time . " seconds\n\n";
                if ($time > 5) {
                    $long_query_content .= "Query: " . $query . "\n";
                    $long_query_content .= "Execution Time: " . $formatted_time . " seconds\n\n";
                }
            }
            // Guarda las consultas y tiempos en un archivo de log
            $log_file = APPPATH . 'logs/'.$this->session->userdata('user_id').'_query_log_' . date('Y-m-d_H-i-s') . '.log';
            $this->load->helper('file');
            if (!write_file($log_file, $log_content, 'a+')) {
                log_message('error', 'Unable to write query log.');
            } else {
                log_message('info', 'Query log written successfully.');
            }
            // Guarda las consultas largas en un archivo de log aparte
            if ($long_query_content) {
                $long_log_file = APPPATH . 'logs/'.$this->session->userdata('user_id').'_long_query_log_' . date('Y-m-d_H-i-s') . '.log';
                if (!write_file($long_log_file, $long_query_content, 'a+')) {
                    log_message('error', 'Unable to write long query log.');
                } else {
                    log_message('info', 'Long query log written successfully.');
                }
            }
            // Actualiza el tiempo de la última ejecución
            $this->update_last_log_time($current_time);

            $this->delete_old_logs();
        }
    }

    private function get_last_log_time()
    {
        if (file_exists($this->last_log_time_file)) {
            return (int) file_get_contents($this->last_log_time_file);
        }
        return 0;
    }

    private function update_last_log_time($time)
    {
        file_put_contents($this->last_log_time_file, $time);
    }

    private function delete_old_logs()
    {
        $log_dir = APPPATH . 'logs/';
        $files = glob($log_dir . 'query_log_*.log');
        $current_time = time();

        foreach ($files as $file) {
            if (is_file($file)) {
                $file_time = filemtime($file);
                if (($current_time - $file_time) > 3600) { // 3600 segundos = 1 hora
                    unlink($file);
                }
            }
        }
    }

    private function deleteQrImages()
    {
        $folderPath = FCPATH ."themes/default/admin/assets/images/qr_code";
        $files = glob($folderPath . '/*.png');
        $currentTime = time();

        foreach ($files as $file) {
            if (is_file($file)) {
                $file_time = filemtime($file);
                if (($currentTime - $file_time) > 3600) { // 3600 segundos = 1 hora
                    unlink($file);
                }
            }
        }
    }
}
