$(document).ready(function(){
    localStorage.removeItem('total_conceptos');

    if (biller_id) {
        $('#rcbiller').select2('val', biller_id).trigger('change').select2('readonly', true);
    }

    if (localStorage.getItem('supplier') || supplier_id) {
        supplier = localStorage.getItem('supplier') ? localStorage.getItem('supplier') : supplier_id;
        $('#supplier').val(supplier).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getsupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        if (localStorage.getItem('expenses')) {
            loadItems();
        }

        if (supplier_id) {
            $('#supplier').select2('readonly', true);
        }

    } else {
        nssupplier();
    }

    if (payment_date = localStorage.getItem('payment_date')) {
        $('#payment_date').val(payment_date);
    }

    if (consecutive_payment = localStorage.getItem('consecutive_payment')) {
        $('#consecutive_payment').val(consecutive_payment);
    }
});

$(document).on('keyup', function(e){
    $('#ajaxCall').fadeIn();
    if (e.keyCode == 13) {
        setTimeout(function() {
            $('#add_payment').prop('disabled', false);
            $('#ajaxCall').fadeOut();
        }, 800);
    }
});

// clear localStorage and reload
$('#reset').click(function (e) {
    bootbox.confirm(lang.r_u_sure, function (result) {

        remove_localstorage_data();
        
        $('#modal-loading').show();
        location.reload();
    });
});

$('#add_payment').click(function(){
        
});

function remove_localstorage_data(){
    if (localStorage.getItem('expenses')) {
        localStorage.removeItem('expenses');
    }
    if (localStorage.getItem('supplier')) {
        localStorage.removeItem('supplier');
    }
    if (localStorage.getItem('payment_date')) {
        localStorage.removeItem('payment_date');
    }
    if (localStorage.getItem('consecutive_payment')) {
        localStorage.removeItem('consecutive_payment');
    }
}

function nssupplier() {
    $('#supplier').select2({
        minimumInputLength: 1,
        data: [],
        initSelection: function (element, callback) {
          $.ajax({
            type: "get", async: false,
            url: site.base_url+"suppliers/getsupplier/" + $(element).val(),
            dataType: "json",
            success: function (data) {
              callback(data[0]);
            }
          });
        },
        ajax: {
          url: site.base_url+"suppliers/suggestions",
          dataType: 'json',
          quietMillis: 15,
          data: function (term, page) {
            return {
              term: term,
              limit: 10
            };
          },
          results: function (data, page) {
            if (data.results != null) {
              return {results: data.results};
            } else {
              return {results: [{id: '', text: lang.no_match_found}]};
            }
          }
        }
    });
}


$(document).on('change', '#supplier', function(){
    localStorage.setItem('supplier', $(this).val());
    $('#amount_1').val(formatMoney(0));
    $('#total_rete_amount_2').val(formatMoney(0));
    $('#total_other_concepts').val(formatMoney(0));
    $('#total_received').val(formatMoney(0));
    if (expenses = localStorage.getItem('expenses')) {
        localStorage.removeItem('expenses');
    }
});

$(document).on('click', '#editItem', function(){
    var check = $(this);
    var expenses = JSON.parse(localStorage.getItem('expenses'));
    row_id = $('#row_id').val();
    expenses[row_id].expense_selected = true;
    expenses[row_id].activated_manually = true;
    expenses[row_id].amount_to_paid = formatDecimal($('#pro_total').text());
    expenses[row_id].net_cost = $('#net_cost').val();
    expenses[row_id].row.tax_val = $('#ptax_value').val();
    expenses[row_id].row.tax_val_2 = $('#ptax_value_2').val();
    localStorage.removeItem('expenses');
    localStorage.setItem('expenses', JSON.stringify(expenses));
    loadItems();
    $('#pamount_to_paid').val('');
    $('#prModal').modal('hide');
});

$(document).on('click, change', '.enable_retencion', function(){
    var check = $(this);
    var row = check.closest('tr');
    row_id = row.attr('data-item-id');
    if (check.prop('checked')) {

    } else {
        reiniciar_retencion(row_id);
    }
});

$(document).on('change', '.paid_by', function () {
    var p_val = $(this).val();
    localStorage.setItem('paid_by', p_val);
    $('#rpaidby').val(p_val);
    if (p_val == 'cash' ||  p_val == 'other') {
        $('.pcheque_1').hide();
        $('.pcc_1').hide();
        $('.pcash_1').show();
        $('#payment_note_1').focus();
    } else if (p_val == 'CC') {
        $('.pcheque_1').hide();
        $('.pcash_1').hide();
        $('.pcc_1').show();
        $('#pcc_no_1').focus();
    } else if (p_val == 'Cheque') {
        $('.pcc_1').hide();
        $('.pcash_1').hide();
        $('.pcheque_1').show();
        $('#cheque_no_1').focus();
    } else {
        $('.pcheque_1').hide();
        $('.pcc_1').hide();
        $('.pcash_1').hide();
    }
    if (p_val == 'gift_card') {
        $('.gc').show();
        $('.ngc').hide();
        $('#gift_card_no').focus();
    } else {
        $('.ngc').show();
        $('.gc').hide();
        $('#gc_details').html('');
    }
});

$(document).on('click', '.edit', function(){
    reset_retention_window();
    // console.log('id '+$('#rete_applied').val());
    itemid = $(this).data('item-id');
    expenses = JSON.parse(localStorage.getItem('expenses'));
    expense = expenses[itemid];
    if (expense.rete_fuente_base_paid > 0 || expense.rete_iva_base_paid > 0 || expense.rete_ica_base_paid > 0 || expense.rete_other_base_paid > 0) {
        $('#rete_otros').prop('disabled', true);
    } else {
        $('#rete_otros').prop('disabled', false);
    }

    $('#suggested_total').html(0);
    $('#suggested_tax').html(0);
    $('#suggested_subtotal').html(0);

    if (expense.expense_selected) {

        $('#rete_base_subtotal').val(0).prop('readonly', true);
        $('#rete_base_tax').val(0).prop('readonly', true);
        $('#rete_base_total').val(0).prop('readonly', true);

        $('#rete_applied').val(itemid);

        if (rete_total = expense.total_rete_amount) {
            retenciones = {
                    'id_rete_fuente' : expense.id_rete_fuente,
                    'id_rete_iva' : expense.id_rete_iva,
                    'id_rete_ica' : expense.id_rete_ica,
                    'id_rete_otros' : expense.id_rete_otros,
                };

            recalcular_retenciones(retenciones);
        }

        if (discount = expense.discount) {
            recalcular_descuento(discount);
        }

        $('#rtModal').appendTo("body").modal('show');
    }

});
//aca load
function loadItems(){
    $('#table_expenses tbody').empty();
    if (localStorage.getItem('expenses')) {
        var expenses = JSON.parse(localStorage.getItem('expenses'));

        var total_received = 0;
        var total_receipted = 0;
        var total_rete_amount = 0;
        var disable_submit = false;
        var count = 0;
        var max = 100;

        var total_discounted = 0;

        $.each(expenses, function(index){
            count++;
            if (count == (max + 1)) {
                return false;
            }
            expenses[index].expense_in_view = true;
            expense = this;
            if (!expense.activated_manually) {
                expense.expense_selected = false;
                expense.amount_to_paid = 0;    
            }
            var expense_selected = expense.expense_selected;
            var expense_total_rete_amount = 0;
            var invalid_retention = false;
            total_received += formatDecimal(expense_selected ? expense.amount_to_paid : 0);
            total_rete_amount += formatDecimal(expense.total_rete_amount);
            expense_total_rete_amount = formatDecimal(expense.total_rete_amount);
            if ((formatDecimal(expense.amount_to_paid) > 0 && formatDecimal(expense.amount_to_paid) < formatDecimal(expense.total_rete_amount))||formatDecimal(expense.amount_to_paid) < 0 && formatDecimal(expense.amount_to_paid) > formatDecimal(expense.total_rete_amount)) {
                disable_submit = true;
                invalid_retention = true;
            }

            var tr_html = $('<tr data-item-id="'+index+'"></tr>');
            var discounted = 0;
            // console.log(expense);
            tbody = "<td>"+
                            expense.label+
                            ( is_commision ? '' : 
                            '<i class="pull-right fa fa-edit tip exedit exedit-' + index + '" id="' + expense.id + '" data-item="' + expense.id + '" title="Edit" style="cursor:pointer;"></i>'
                            )+
                    "</td>"+
                    "<td>"+
                        "<div>"+
                          "<input type='hidden' name='expense_id["+index+"]' value='"+(expense.item_id !== undefined ? expense.item_id : index)+"'>"+
                          "<input type='text' name='amount["+index+"]' value='"+(expense_selected ? expense.amount_to_paid : 0)+"' class='amount number_mask form-control expense_id_"+expense.id+" text-right' readonly='true' >"+
                          "<input type='hidden' name='row_selected["+index+"]' value='"+index+"'>"+
                          "<input type='hidden' name='tax_rate_id["+index+"]' value='"+expense.row.tax_rate+"'>"+
                          "<input type='hidden' name='tax_rate_2_id["+index+"]' value='"+expense.row.tax_rate_2+"'>"+
                          "<input type='hidden' name='tax_val["+index+"]' value='"+(expense.row.tax_val !== undefined ? expense.row.tax_val : 0)+"'>"+
                          "<input type='hidden' name='tax_val_2["+index+"]' value='"+(expense.row.tax_val_2 !== undefined ? expense.row.tax_val_2 : 0)+"'>"+
                          "<input type='hidden' name='payments_ids_comm_paid["+index+"]' value='"+(expense.payments_ids !== undefined ? expense.payments_ids : null)+"'>"+
                          "<input type='hidden' name='sale_reference_no["+index+"]' value='"+(expense.sale_reference_no !== undefined ? expense.sale_reference_no : null)+"'>"+
                        "</div>"+
                    "</td>"+
                    "<td>"+
                        "<div class='input-group'>"+
                            "<input type='hidden' name='expense_id_rete_fuente["+index+"]' value='"+formatDecimal(expense.id_rete_fuente)+"'>"+
                            "<input type='hidden' name='expense_id_rete_iva["+index+"]' value='"+formatDecimal(expense.id_rete_iva)+"'>"+
                            "<input type='hidden' name='expense_id_rete_ica["+index+"]' value='"+formatDecimal(expense.id_rete_ica)+"'>"+
                            "<input type='hidden' name='expense_id_rete_other["+index+"]' value='"+formatDecimal(expense.id_rete_otros)+"'>"+

                            "<input type='hidden' name='expense_rete_fuente_total["+index+"]' value='"+expense.rete_fuente_total+"'>"+
                            "<input type='hidden' name='expense_rete_iva_total["+index+"]' value='"+expense.rete_iva_total+"'>"+
                            "<input type='hidden' name='expense_rete_ica_total["+index+"]' value='"+expense.rete_ica_total+"'>"+
                            "<input type='hidden' name='expense_rete_other_total["+index+"]' value='"+expense.rete_other_total+"'>"+

                            "<input type='hidden' name='expense_rete_fuente_percentage["+index+"]' value='"+expense.rete_fuente_percentage+"'>"+
                            "<input type='hidden' name='expense_rete_iva_percentage["+index+"]' value='"+expense.rete_iva_percentage+"'>"+
                            "<input type='hidden' name='expense_rete_ica_percentage["+index+"]' value='"+expense.rete_ica_percentage+"'>"+
                            "<input type='hidden' name='expense_rete_other_percentage["+index+"]' value='"+expense.rete_other_percentage+"'>"+

                            "<input type='hidden' name='expense_rete_fuente_base["+index+"]' value='"+expense.rete_fuente_base+"'>"+
                            "<input type='hidden' name='expense_rete_iva_base["+index+"]' value='"+expense.rete_iva_base+"'>"+
                            "<input type='hidden' name='expense_rete_ica_base["+index+"]' value='"+expense.rete_ica_base+"'>"+
                            "<input type='hidden' name='expense_rete_other_base["+index+"]' value='"+expense.rete_other_base+"'>"+

                            "<input type='hidden' name='expense_rete_fuente_account["+index+"]' value='"+expense.rete_fuente_account+"'>"+
                            "<input type='hidden' name='expense_rete_iva_account["+index+"]' value='"+expense.rete_iva_account+"'>"+
                            "<input type='hidden' name='expense_rete_ica_account["+index+"]' value='"+expense.rete_ica_account+"'>"+
                            "<input type='hidden' name='expense_rete_other_account["+index+"]' value='"+expense.rete_other_account+"'>"+
                            (paying_commission ? "" : (invalid_retention ? "<span class='input-group-addon edit text-danger' data-item-id='"+index+"' style='cursor:pointer;' ata-toggle='tooltip' data-placement='bottom' title='El valor de la retención es mayor al monto a pagar.'><i class='fa fa-exclamation-triangle'></i></span>" : "<span class='input-group-addon edit' data-item-id='"+index+"' style='cursor:pointer;'><i class='fa fa-pencil'></i></span>"))+
                            (expense.discount ? discount_html : '')+
                            "<input type='text' class='form-control text-right' value='"+(tra = expense.total_rete_amount ? expense.total_rete_amount : 0)+"' readonly>"+
                            "<span class='input-group-addon'>"+
                                "<input type='checkbox' class='retencion_"+expense.id+" enable_retencion skip' data-item-id='"+expense.id+"' "+(tra = expense.total_rete_amount ? "checked" : "")+">"+
                            "</span>"+
                        "</div>"+
                    "</td>"+
                    (is_commision ? '<td></td>' :
                    '<td class="delete_expense text-center" data-item="' + index + '" style="cursor:pointer;">'+
                        '<i class="fa fa-times"></i>'+
                    "</td>"
                    );
            tr_html.html(tbody);
            tr_html.appendTo('#table_expenses tbody');

            total_receipted = total_received - total_rete_amount;

            $('#amount_1').val(formatMoney(total_received));
            $('#total_rete_amount_2').val(formatMoney(total_rete_amount));
            $('#total_received').val(formatMoney(total_receipted));
            $('#base').val(total_received);

            if (total_conceptos = localStorage.getItem('total_conceptos')) {
                total_receipted += formatDecimal(total_conceptos);
                $('#total_other_concepts').val(formatMoney(total_conceptos));
                $('#total_received').val(formatMoney(total_receipted));
            }

        });

        localStorage.removeItem('expenses');
        localStorage.setItem('expenses', JSON.stringify(expenses));

        if (formatDecimal(total_receipted) < 0) {
            disable_submit = true;
            $('.negative_alert').css('display', '');
            Command: toastr.error('El monto total recibido está en negativo.', 'Error', {onHidden : function(){}})
        } else {
            $('.negative_alert').css('display', 'none');
        }

        if (disable_submit) {
            $('#add_payment').prop('disabled', true);
        } else {
            $('#add_payment').prop('disabled', false);
        }
        set_number_mask();
    }
}

// JS Retenciones


    $(document).on('click', '#rete_fuente', function(){

        var check = $(this);

        $('#updateOrderRete').prop('disabled', true);

        if (check.is(':checked')) {
             $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_fuente_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_fuente_option option:selected').data('apply');
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select();
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor').val('');
            setReteTotalAmount(rete_fuente_amount, '-');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_iva', function(){

        var check = $(this);

        $('#updateOrderRete').prop('disabled', true);

        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_iva_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_iva_option option:selected').data('apply');
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select();
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor').val('');
            setReteTotalAmount(rete_iva_amount, '-');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_ica', function(){

        var check = $(this);

        $('#updateOrderRete').prop('disabled', true);

        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_ica_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_ica_option option:selected').data('apply');
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select();
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor').val('');
            setReteTotalAmount(rete_ica_amount, '-');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_otros', function(){

        var check = $(this);

        $('#updateOrderRete').prop('disabled', true);

        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_otros_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_otros_option option:selected').data('apply');
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select();
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor').val('');
            setReteTotalAmount(rete_otros_amount, '-');
            resetBase(apply);
        }
    });

    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option', function(){
        prevamnt = $('#rete_fuente_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_fuente_option option:selected').data('percentage');
        apply = $('#rete_fuente_option option:selected').data('apply');
        account = $('#rete_fuente_option option:selected').data('account');
        setMaxBaseAllowed(apply, 'fuente', $('#rete_applied').val());
        $('#rete_fuente_account').val(account);
        amount = formatDecimal(getReteAmount(apply));
        $('#rete_fuente_base').val(amount);
        cAmount = amount * (percentage / 100);
        cAmount = Math.round(cAmount);
        $('#rete_fuente_tax').val(percentage);
        $('#rete_fuente_valor').val(cAmount);
        rete_fuente_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_fuente_amount, '+');
    });

    $(document).on('change', '#rete_iva_option', function(){
        prevamnt = $('#rete_iva_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_iva_option option:selected').data('percentage');
        apply = $('#rete_iva_option option:selected').data('apply');
        account = $('#rete_iva_option option:selected').data('account');
        setMaxBaseAllowed(apply, 'iva', $('#rete_applied').val());
        $('#rete_iva_account').val(account);
        amount = formatDecimal(getReteAmount(apply));
        $('#rete_iva_base').val(amount);
        cAmount = amount * (percentage / 100);
        cAmount = Math.round(cAmount);
        $('#rete_iva_tax').val(percentage);
        $('#rete_iva_valor').val(cAmount);
        rete_iva_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_iva_amount, '+');
    });

    $(document).on('change', '#rete_ica_option', function(){
        prevamnt = $('#rete_ica_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_ica_option option:selected').data('percentage');
        apply = $('#rete_ica_option option:selected').data('apply');
        account = $('#rete_ica_option option:selected').data('account');
        setMaxBaseAllowed(apply, 'ica', $('#rete_applied').val());
        $('#rete_ica_account').val(account);
        amount = formatDecimal(getReteAmount(apply));
        $('#rete_ica_base').val(amount);
        cAmount = amount * (percentage / 100);
        cAmount = Math.round(cAmount);
        $('#rete_ica_tax').val(percentage);
        $('#rete_ica_valor').val(cAmount);
        rete_ica_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_ica_amount, '+');
    });

    $(document).on('change', '#rete_otros_option', function(){
        prevamnt = $('#rete_otros_valor').val();
        setReteTotalAmount(prevamnt, '-');
        percentage = $('#rete_otros_option option:selected').data('percentage');
        apply = $('#rete_otros_option option:selected').data('apply');
        account = $('#rete_otros_option option:selected').data('account');
        setMaxBaseAllowed(apply, 'other', $('#rete_applied').val());
        $('#rete_otros_account').val(account);
        amount = formatDecimal(getReteAmount(apply));
        $('#rete_otros_base').val(amount);
        cAmount = amount * (percentage / 100);
        cAmount = Math.round(cAmount);
        $('#rete_otros_tax').val(percentage);
        $('#rete_otros_valor').val(cAmount);
        rete_otros_amount = formatMoney(cAmount);
        setReteTotalAmount(rete_otros_amount, '+');
    });


    function getReteAmount(apply){

        amount = 0;

        if (apply == "ST") {
            amount = $('#rete_base_subtotal').val();
        } else if (apply == "TX") {
            amount = $('#rete_base_tax').val();
        } else if (apply == "TO") {
            amount = $('#rete_base_total').val();
        }

        return amount;
    }

    function setReteTotalAmount(amount, action){

        var expenses = JSON.parse(localStorage.getItem('expenses'));
        expense = expenses[$('#rete_applied').val()];
        if (paying_commission || expense) {
            if (action == "+") {
                tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) + formatDecimal(amount);
            } else if (action == "-") {
                tra =  formatDecimal(parseFloat(formatDecimal($('#total_rete_amount').text()))) - formatDecimal(amount);
            }
            if (tra < 0) {
                tra = 0;
            }
            $('#total_rete_amount').text(formatMoney(tra));
            if (paying_commission == false && (formatDecimal(tra) > formatDecimal(expense.amount_to_paid))) {
                $('.advise_invalid_retention').css('display', '');
                $('#updateOrderRete').prop('disabled', true);
            } else {
                $('.advise_invalid_retention').css('display', 'none');
                $('#updateOrderRete').prop('disabled', false);
            }
        }
    }

    function resetBase(apply){
        if (apply == "ST") {
            $('#rete_base_subtotal').val(0).prop('readonly', true);
        } else if (apply == "TX") {
            $('#rete_base_tax').val(0).prop('readonly', true);
        } else if (apply == "TO") {

        }
        total = formatDecimal($('#rete_base_subtotal').val()) + formatDecimal($('#rete_base_tax').val());
        $('#rete_base_total').val(total);
    }

    function setMaxBaseAllowed(apply, rete, item_id){
        var expenses = JSON.parse(localStorage.getItem('expenses'));
        var expense = expenses[item_id];
        var total = 0;

        if (apply == "ST") {
            max_val = paying_commission ? parseFloat(formatDecimal($('#total_received').val())) : expense.net_cost;
            $('#rete_base_subtotal').prop('max', max_val).prop('readonly', true);
            if ($('#rete_base_subtotal').val() == 0) {
                $('#rete_base_subtotal').val(max_val);
            }
            base_subtotal = max_val;
            $('#suggested_subtotal').html(formatMoney(base_subtotal));
        } else if (apply == "TX") {
            max_val = paying_commission ? 0 : (expense.row.tax_val + expense.row.tax_val_2);
            $('#rete_base_tax').prop('max', max_val).prop('readonly', true);
            if ($('#rete_base_tax').val() == 0) {
                $('#rete_base_tax').val(max_val);
            }
            base_iva = max_val;
            $('#suggested_tax').html(formatMoney(base_iva));
        } else if (apply == "TO") {
            base_total = formatDecimal($('#suggested_subtotal').html()) + formatDecimal($('#suggested_tax').html());
            $('#suggested_total').html(formatMoney(base_total));
        }
        total = formatDecimal($('#rete_base_subtotal').val()) + formatDecimal($('#rete_base_tax').val());
        $('#rete_base_total').val(total);
    }

    $(document).on('keyup', '#rete_base_subtotal, #rete_base_tax, #rete_base_total', function(){

        var expenses = JSON.parse(localStorage.getItem('expenses'));
        var expense = expenses[$('#rete_applied').val()];
        var rete_base = $(this);
        var val = formatDecimal(rete_base.val());
        var max = formatDecimal(rete_base.prop('max'));
        var min = formatDecimal(rete_base.prop('min') ? rete_base.prop('min') : 0);

        if (expense.amount_to_paid == expense.grand_total) {

            rete_base.val(max);

            Command: toastr.error('No es posible modificar la base por que se está pagando el total de la factura', 'Error', {onHidden : function(){}})

        } else {

            if (val < min || val > max) {
                rete_base.val(max);
            }

            debbug_retenciones();
        }


    });

    $(document).on('focus', '#rete_base_subtotal, #rete_base_tax, #rete_base_total', function(){

        $(this).select();

    });

    $(document).on('click', '#updateOrderRete', function () {
        $('#ajaxCall').fadeIn();
        var expenses = JSON.parse(localStorage.getItem('expenses'));
        if (paying_commission) {
            var n = 0;
            $.each(expenses, function(index, expense){
                if (n > 0) {
                    expenses = JSON.parse(expenses);
                }
                prorrated_base_percentage = (formatDecimal(expense.amount_to_paid) * 100) / formatDecimal($('#rete_base_subtotal').val());
                prorrated_base = formatDecimal($('#rete_base_subtotal').val()) * (prorrated_base_percentage / 100);

                prorrated_rete_fuente = formatDecimal($('#rete_fuente_valor').val() * (prorrated_base_percentage / 100));
                prorrated_rete_iva = formatDecimal($('#rete_iva_valor').val() * (prorrated_base_percentage / 100));
                prorrated_rete_ica = formatDecimal($('#rete_ica_valor').val() * (prorrated_base_percentage / 100));
                prorrated_rete_otros = formatDecimal($('#rete_otros_valor').val() * (prorrated_base_percentage / 100));
                expenses[index].rete_fuente_percentage = $('#rete_fuente_tax').val();
                expenses[index].rete_ica_percentage = $('#rete_ica_tax').val();
                expenses[index].rete_iva_percentage = $('#rete_iva_tax').val();
                expenses[index].rete_other_percentage = $('#rete_otros_tax').val();
                expenses[index].rete_fuente_base = prorrated_base;
                expenses[index].rete_ica_base = prorrated_base;
                expenses[index].rete_iva_base = prorrated_base;
                expenses[index].rete_other_base = prorrated_base;
                expenses[index].rete_fuente_total = prorrated_rete_fuente;
                expenses[index].rete_ica_total = prorrated_rete_ica;
                expenses[index].rete_iva_total = prorrated_rete_iva;
                expenses[index].rete_other_total = prorrated_rete_otros;
                expenses[index].rete_fuente_account = $('#rete_fuente_account').val();
                expenses[index].rete_ica_account = $('#rete_ica_account').val();
                expenses[index].rete_iva_account = $('#rete_iva_account').val();
                expenses[index].rete_other_account = $('#rete_otros_account').val();
                expenses[index].fuente_option = $('#rete_fuente_option option:selected').data('percentage');
                expenses[index].iva_option = $('#rete_iva_option option:selected').data('percentage');
                expenses[index].ica_option = $('#rete_ica_option option:selected').data('percentage');
                expenses[index].otros_option = $('#rete_otros_option option:selected').data('percentage');
                expenses[index].id_rete_fuente = $('#rete_fuente_option').val();
                expenses[index].id_rete_iva = $('#rete_iva_option').val();
                expenses[index].id_rete_ica = $('#rete_ica_option').val();
                expenses[index].id_rete_otros = $('#rete_otros_option').val();
                expenses[index].total_rete_amount = parseFloat(prorrated_rete_fuente) + parseFloat(prorrated_rete_iva) + parseFloat(prorrated_rete_ica) + parseFloat(prorrated_rete_otros);
                expenses = JSON.stringify(expenses);
                n++;
            });

            expenses = JSON.parse(expenses);
            localStorage.removeItem('expenses');
            localStorage.setItem('expenses', JSON.stringify(expenses));
            setTimeout(function() {
                loadItems();
                reset_retention_window();
                $('#rtModal').modal('hide');
                $('#ajaxCall').fadeOut();
            }, 1800);
        } else {
            var itemid = $('#rete_applied').val();
            if (itemid != 0) {
                expenses[itemid].rete_fuente_percentage = $('#rete_fuente_tax').val();
                expenses[itemid].rete_ica_percentage = $('#rete_ica_tax').val();
                expenses[itemid].rete_iva_percentage = $('#rete_iva_tax').val();
                expenses[itemid].rete_other_percentage = $('#rete_otros_tax').val();
                expenses[itemid].rete_fuente_base = $('#rete_fuente_base').val();
                expenses[itemid].rete_ica_base = $('#rete_ica_base').val();
                expenses[itemid].rete_iva_base = $('#rete_iva_base').val();
                expenses[itemid].rete_other_base = $('#rete_otros_base').val();
                expenses[itemid].rete_fuente_total = $('#rete_fuente_valor').val();
                expenses[itemid].rete_ica_total = $('#rete_ica_valor').val();
                expenses[itemid].rete_iva_total = $('#rete_iva_valor').val();
                expenses[itemid].rete_other_total = $('#rete_otros_valor').val();
                expenses[itemid].rete_fuente_account = $('#rete_fuente_account').val();
                expenses[itemid].rete_ica_account = $('#rete_ica_account').val();
                expenses[itemid].rete_iva_account = $('#rete_iva_account').val();
                expenses[itemid].rete_other_account = $('#rete_otros_account').val();
                expenses[itemid].fuente_option = $('#rete_fuente_option option:selected').data('percentage');
                expenses[itemid].iva_option = $('#rete_iva_option option:selected').data('percentage');
                expenses[itemid].ica_option = $('#rete_ica_option option:selected').data('percentage');
                expenses[itemid].otros_option = $('#rete_otros_option option:selected').data('percentage');
                expenses[itemid].id_rete_fuente = $('#rete_fuente_option').val();
                expenses[itemid].id_rete_iva = $('#rete_iva_option').val();
                expenses[itemid].id_rete_ica = $('#rete_ica_option').val();
                expenses[itemid].id_rete_otros = $('#rete_otros_option').val();
                expenses[itemid].total_rete_amount = $('#total_rete_amount').text();
                var discount_apply_to = [];
                var discount_amount = [];
                var discount_amount_calculated = [];
                var discount_ledger_id = [];
                $('select.discount_apply_to').each(function(index, select){
                    index_d = $(select).data('index');
                    d_discount_apply_to = $(select).val();
                    d_discount_amount = $('.discount_amount[data-index="'+index_d+'"]').val();
                    d_discount_amount_calculated = $('.discount_amount_calculated[data-index="'+index_d+'"]').val();
                    d_discount_ledger_id = $(select).find('option:selected').data('ledgerid');
                    if (d_discount_amount_calculated > 0) {
                        discount_apply_to.push(d_discount_apply_to);
                        discount_amount.push(d_discount_amount);
                        discount_amount_calculated.push(d_discount_amount_calculated);
                        discount_ledger_id.push(d_discount_ledger_id);
                    }
                });
                if (discount_apply_to.length > 0 || discount_amount.length > 0 || discount_amount_calculated.length > 0 || discount_ledger_id.length > 0 ) {
                    expenses[itemid].discount = {discount_apply_to, discount_amount, discount_amount_calculated, discount_ledger_id};
                } else {
                    expenses[itemid].discount = false;
                }
                localStorage.removeItem('expenses');
                localStorage.setItem('expenses', JSON.stringify(expenses));
                loadItems();
                reset_retention_window();
                $('#rtModal').modal('hide');
                $('#ajaxCall').fadeOut();
            }
        }
            
    });

     $(document).on('click', '#cancelOrderRete', function(){
        // localStorage.removeItem('retenciones');
        // loadItems();
        reset_retention_window();
        $('#updateOrderRete').trigger('click');
     });

    function recalcular_retenciones(retenciones){

        if (retenciones != null) {

            if (retenciones.id_rete_fuente != '' && retenciones.id_rete_fuente != null) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').attr('data-selected', retenciones.id_rete_fuente).trigger('click');
                }
            }

            if (retenciones.id_rete_iva != '' && retenciones.id_rete_iva != null) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').attr('data-selected', retenciones.id_rete_iva).trigger('click');
                }
            }

            if (retenciones.id_rete_ica != '' && retenciones.id_rete_ica != null) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').attr('data-selected', retenciones.id_rete_ica).trigger('click');
                }
            }

            if (retenciones.id_rete_otros != '' && retenciones.id_rete_otros != null) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').attr('data-selected', retenciones.id_rete_otros).trigger('click');
                }
            }

        }

    }


    function debbug_retenciones(){

        if ($('#rete_fuente').is(':checked')) {
            $('#rete_fuente_option').trigger('change');
        }
        
        if ($('#rete_iva').is(':checked')) {
            $('#rete_iva_option').trigger('change');
        }

        if ($('#rete_ica').is(':checked')) {
            $('#rete_ica_option').trigger('change');
        }

        if ($('#rete_otros').is(':checked')) {
            $('#rete_otros_option').trigger('change');
        }

    }

    function reiniciar_retencion(row_id){

        var expenses = JSON.parse(localStorage.getItem('expenses'));

        delete expenses[row_id].total_rete_amount;

        delete expenses[row_id].fuente_option;
        delete expenses[row_id].iva_option;
        delete expenses[row_id].ica_option;
        delete expenses[row_id].otros_option;
        delete expenses[row_id].id_rete_fuente;
        delete expenses[row_id].id_rete_iva;
        delete expenses[row_id].id_rete_ica;
        delete expenses[row_id].id_rete_otros;

        expenses[row_id].rete_fuente_percentage = 0;
        expenses[row_id].rete_ica_percentage = 0;
        expenses[row_id].rete_iva_percentage = 0;
        expenses[row_id].rete_other_percentage = 0;

        expenses[row_id].rete_fuente_base = 0;
        expenses[row_id].rete_ica_base = 0;
        expenses[row_id].rete_iva_base = 0;
        expenses[row_id].rete_other_base = 0;

        expenses[row_id].rete_fuente_total = 0;
        expenses[row_id].rete_ica_total = 0;
        expenses[row_id].rete_iva_total = 0;
        expenses[row_id].rete_other_total = 0;

        expenses[row_id].rete_fuente_account = 0;
        expenses[row_id].rete_ica_account = 0;
        expenses[row_id].rete_iva_account = 0;
        expenses[row_id].rete_other_account = 0;

        $('#cancelOrderRete').trigger('click');

        localStorage.removeItem('expenses');
        localStorage.setItem('expenses', JSON.stringify(expenses));
        loadItems();
    }

$(document).on('click', '.add_concept', function(){
    $.ajax({
        url : site.base_url+"payments/addConcept",
    }).done(function(data){
        $(data).appendTo('#table_concepts tbody');
        $('.select2').select2('destroy');
        $('.select2').select2();
        actualizarConceptos();
    })
});

$(document).on('click', '.delete_row', function(){
    tr = $(this).closest('tr');
    tr.remove();
    actualizarConceptos();
});

$(document).on('change', '.type_mov, .concepto_amount', function(){
    var val = $(this).val();
    if (val < 0) {
        $(this).val(0);
    }
    actualizarConceptos();
});

$(document).on('keyup', '.concepto_amount', function(){
    var val = $(this).val();

    this.value = this.value.replace(/[^0-9],[^0-9]/g,'');

    if (val < 0) {
        $(this).val(0);
    }

});

function actualizarConceptos(){

    localStorage.removeItem('total_conceptos');

    var total = 0;
    $('select[name="type_mov[]"]').each(function(index){
        var type = $(this);
        var amount = $('input[name="concepto_amount[]"]').eq(index);

        // console.log('Type : '+type.val()+', amount : '+amount.val());

        if (type.val() == "+") {
            total += formatDecimal(amount.val());
        } else {
            total -= formatDecimal(amount.val());
        }

    });
    localStorage.setItem('total_conceptos', total);
    loadItems();
}

$(document).on('click', '#add_payment', function(){

    var submit = true;

    if ($('select[name="type_mov[]"]').length > 0) {
        
        $('select[name="type_mov[]"]').each(function(index){

            type_mov = $(this);

            if (type_mov.val() == '') {
                type_mov.select2('open');
                submit = false;
                return false;
            }

            description = $('input[name="concepto_description[]"]').eq(index);

            if (description.val() == '') {
                description.focus();
                submit = false;
                return false;
            }

            amount = $('input[name="concepto_amount[]"]').eq(index);

            if (amount.val() == '') {
                amount.focus();
                submit = false;
                return false;
            }

            if (site.settings.modulary == 1) {
                ledger = $('select[name="ledger[]"]').eq(index);
                if (ledger.val() == '') {
                    ledger.select2('open');
                    submit = false;
                    return false;
                }
            }

            if (site.settings.cost_center_selection == 0) {
                biller_concepto = $('select[name="biller_concepto[]"]').eq(index);
                if (biller_concepto.val() == '') {
                    biller_concepto.select2('open');
                    submit = false;
                    return false;
                }
            } else if (site.settings.cost_center_selection == 1) {
                cost_center_id_concepto = $('select[name="cost_center_id_concepto[]"]').eq(index);
                if (cost_center_id_concepto.val() == '') {
                    cost_center_id_concepto.select2('open');
                    submit = false;
                    return false;
                }
            }

        });

    }

    if (submit && site.settings.cost_center_selection == 1) {
        if ($('#cost_center_id').val() == '') {
            $('#cost_center_id').select2('open');
            submit = false;
        }
    }

    if (submit) {
        $('#add_multi_payment_expenses').submit();
        $('#add_payment').prop('disabled', true);
        $('#reset').prop('disabled', true);remove_localstorage_data();
        setTimeout(function() {
            location.href = site.base_url+'payments/exindex';
        }, 1300);
    }

});


$(document).on('change', '.discount_apply_to', function(){
    var form_select = $(this);
    var apply_to = form_select.val();
    $('.discount_apply_to').each(function(index, select){
        if (form_select.index('.discount_apply_to') != index) {
            $.each($(select).find('option'), function(index, option){
                if ($(option).val() == apply_to && apply_to != '') {
                    $(option).prop('disabled', true);
                }
            });
        }
    });
});

$(document).on('blur, change', '.discount_amount', function(){
    var input = $(this);
    var expenses = JSON.parse(localStorage.getItem('expenses'));
    if (input.val() != '') {
        var index = input.data('index');
        var expense_id = $('#rete_applied').val();
        var expense = expenses[expense_id];
        var apply_to = $('.discount_apply_to[data-index="'+index+'"]').val();
        var base_discount = getAmountToDiscount(apply_to, expense_id);
        var item_discount = 0;
        var ds = input.val();
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = formatDecimal(parseFloat(((base_discount) * parseFloat(pds[0])) / 100));
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        $('.discount_amount_calculated[data-index="'+index+'"]').val(formatDecimals(item_discount));
        set_total_calculated_discount(expense);
    }
});

function getAmountToDiscount(apply, item_id){
    var expenses = JSON.parse(localStorage.getItem('expenses'));
    var expense = expenses[item_id];
    var base_discount = 0;
    if (apply == "ST") {
        // base_discount = ((expense.amount_to_paid / expense.grand_total) * expense.total);
        base_discount = expense.total;
    } else if (apply == "TX") {
        // base_discount = ((expense.amount_to_paid / expense.grand_total) * expense.total_tax);
        base_discount = expense.total_tax;
    } else if (apply == "TO") {
        // base_discount = ((expense.amount_to_paid / expense.grand_total) * expense.grand_total);
        base_discount = expense.grand_total;
    }
    return base_discount;
}

function recalcular_descuento(discount){
    arr_apply = discount.discount_apply_to;
    arr_amount = discount.discount_amount;
    arr_calculated = discount.discount_amount_calculated;
    $.each(arr_apply, function(index, val){
        index_d = index+1;
        apply_to = val;
        amount = arr_amount[index];
        $('.discount_apply_to[data-index="'+index_d+'"]').select2('val', apply_to).trigger('change');
        $('.discount_amount[data-index="'+index_d+'"]').val(amount).trigger('change');
    });
}

function reset_retention_window(){
    $('#rete_fuente').prop('checked', true).trigger('click');
    $('#rete_iva').prop('checked', true).trigger('click');
    $('#rete_ica').prop('checked', true).trigger('click');
    $('#rete_otros').prop('checked', true).trigger('click');

    $('.discount_apply_to').each(function(index, select){
        $(select).find('option').prop('disabled', false);
    });
    $('.discount_apply_to').select2('val', '').trigger('change');
    $('.discount_amount').val(0).trigger('change');
    $('#total_discount').val(0);
    $('#updateOrderRete').prop('disabled', false);
}

function set_total_calculated_discount(expense){
    var total = 0;
    $('#updateOrderRete').prop('disabled', true);
    $('.discount_amount_calculated').each(function(index, input){
        if ($(input).val() > 0) {
            total += parseFloat($(input).val());
        }
    });
        if (total > 0) {
            if (total <= expense.amount_to_paid) {
                $('#total_discount').val(formatDecimals(total));
                $('#updateOrderRete').prop('disabled', false);
            } else {
                command: toastr.warning('El total de los descuentos es mayor al valor pagado', 'Inconsistencia en descuentos', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
        }
}

$('#payment_date').on('change', function(){
    if ($(this).val() != '') {
        localStorage.setItem('payment_date', $(this).val());
    }
});

$('#consecutive_payment').on('change', function(){
    if ($(this).val() != '') {
        localStorage.setItem('consecutive_payment', $(this).val());
    }
});

 function add_expense_item(item) {
    if ($('#supplier').val()) {

    } else {
        bootbox.alert(lang.select_above);
        item = null;
        return;
    }
    if (localStorage.getItem('expenses')) {
        expenses = JSON.parse(localStorage.getItem('expenses'));
    } else {
        expenses = {};
    }
    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    expenses[item_id] = item;
    localStorage.setItem('expenses', JSON.stringify(expenses));
    loadItems();
    console.log('id  '+item_id);
    $('.exedit-'+item_id).trigger('click');
    $('#pamount_to_paid').focus();
    return true;
}

$(document).on('click', '.exedit', function () {
    expenses = JSON.parse(localStorage.getItem('expenses'));
    var row = $(this).closest('tr');
    item_id = row.attr('data-item-id');
    item = expenses[item_id];
    unit_cost = item.net_cost;
    net_cost = item.net_cost;
    real_net_cost = item.net_cost;
    $('#pamount_to_paid').val(item.net_cost);
    $('#prModalLabel').text(item.row.name + ' (' + item.row.code + ')');
    if (site.settings.tax1) {
        $('#ptax').select2('val', item.row.tax_rate);
        $('#ptax_2').select2('val', item.row.tax_rate_2);
        $('#ptax_value').prop('readonly', 'readonly');
        $('#ptax_2_value').prop('readonly', 'readonly');
        $('#ptax').select2('readonly', 'readonly');
        $('#ptax_2').select2('readonly', 'readonly');
        if (item.tax_rate == false) {
            $('#ptax').select2('val', '').select2('readonly', 'readonly');
        }
        if (item.tax_rate_2 == false) {
            $('#ptax_2').select2('val', '').select2('readonly', 'readonly');
        }
        var pr_tax = item.row.tax_rate, pr_tax_val = 0;
        if (pr_tax !== null && pr_tax != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax){
                    if (this.type == 1) {
                        if (expenses[item_id].row.tax_method == 0) {
                            pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                            net_cost -= pr_tax_val;
                        } else {
                            pr_tax_val = formatDecimal((((net_cost) * parseFloat(this.rate)) / 100));
                            pr_tax_rate = formatDecimal(this.rate) + '%';
                        }
                        $('#ptax_value').prop('readonly', 'readonly');
                    } else if (this.type == 2) {
                        pr_tax_val = parseFloat(this.rate);
                        pr_tax_rate = this.rate;
                        $('#ptax_value').removeAttr('readonly');
                    }
                    $('#ptax_value').val(formatDecimal(pr_tax_val));
                }
            });
        }
        var pr_tax_2 = item.row.tax_rate_2, pr_tax_2_val = 0;
        if (pr_tax_2 !== null && pr_tax_2 != 0) {
            $.each(tax_rates, function () {
                if(this.id == pr_tax_2){
                    if (this.type == 1) {
                        if (expenses[item_id].row.tax_method == 0) {
                            pr_tax_2_val = formatDecimal((((real_net_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate))));
                            pr_tax_2_rate = formatDecimal(this.rate) + '%';
                            net_cost -= pr_tax_2_val;
                        } else {
                            pr_tax_2_val = formatDecimal((((real_net_cost) * parseFloat(this.rate)) / 100));
                            pr_tax_2_rate = formatDecimal(this.rate) + '%';
                        }
                        $('#ptax_2_value').prop('readonly', 'readonly');
                    } else if (this.type == 2) {
                        pr_tax_2_val = parseFloat(this.rate);
                        pr_tax_2_rate = this.rate;
                        $('#ptax_2_value').removeAttr('readonly');
                    }
                    $('#ptax_2_value').val(formatDecimal(pr_tax_2_val));
                }
            });
        }
        var opt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        uopt = '<p style="margin: 12px 0 0 0;">n/a</p>';
        $('#poptions-div').html(opt);
        $('#punits-div').html(uopt);
        $('select.select').select2({minimumResultsForSearch: 7});
        $('#net_cost_label').text(formatMoney((parseFloat(net_cost))));
        $('#net_cost').val(formatDecimal(net_cost));
        $('#pro_tax').text(formatMoney((parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val))));
        $('#pro_total').text(formatMoney((parseFloat(net_cost) + parseFloat(pr_tax_val) + parseFloat(pr_tax_2_val))));
        $('#prModal').appendTo("body").modal('show');
        $('#pname').val(item.row.name);
        $('#row_id').val(item_id);
        $('#pamount_to_paid').select();
    }
});

$(document).on('keyup', '#pamount_to_paid', function () {
    var expenses = JSON.parse(localStorage.getItem('expenses'));
    var item_id = $('#row_id').val();
    var unit_cost = parseFloat($('#pamount_to_paid').val());
    var item = expenses[item_id];
    real_unit_cost = unit_cost;
    var pr_tax = $('#ptax').val();
    var pr_tax_2 = $('#ptax_2').val();
    var item_tax_method = item.row.tax_method;
    var pr_tax_val = 0, pr_tax_rate = 0;
    var pr_tax_2_val = 0, pr_tax_2_rate = 0;
    if ((pr_tax !== null && pr_tax != 0) || (pr_tax_2 !== null && pr_tax_2 != 0)) {
        $.each(tax_rates, function () {
            type_tax = this.type;
            if(this.id == pr_tax){
                if (this.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_val = formatDecimal(((unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                        unit_cost -= pr_tax_val;
                    } else {
                        pr_tax_val = formatDecimal((((unit_cost) * parseFloat(this.rate)) / 100));
                        pr_tax_rate = formatDecimal(this.rate) + '%';
                    }
                    $('#ptax_value').val(pr_tax_val).prop('readonly', 'readonly');
                } else if (this.type == 2) {

                    $('#ptax_value').removeAttr('readonly');
                    rate_tax_c = $('#ptax_value').val();
                    pr_tax_val = parseFloat(rate_tax_c);
                    pr_tax_rate = rate_tax_c;

                }
            }
            if(this.id == pr_tax_2){
                if (this.type == 1) {
                    if (item_tax_method == 0) {
                        pr_tax_2_val = formatDecimal(((real_unit_cost) * parseFloat(this.rate)) / (100 + parseFloat(this.rate)));
                        pr_tax_2_rate = formatDecimal(this.rate) + '%';
                        unit_cost -= pr_tax_2_val;
                    } else {
                        pr_tax_2_val = formatDecimal((((real_unit_cost) * parseFloat(this.rate)) / 100));
                        pr_tax_2_rate = formatDecimal(this.rate) + '%';
                    }
                    $('#ptax_2_value').val(pr_tax_2_val).prop('readonly', 'readonly');
                } else if (this.type == 2) {

                    $('#ptax_2_value').removeAttr('readonly');
                    rate_tax_c = $('#ptax_2_value').val();
                    pr_tax_2_val = parseFloat(rate_tax_c);
                    pr_tax_2_rate = rate_tax_c;
                }
            }
        });

        $('#net_cost_label').text(formatMoney((parseFloat(unit_cost))));
        $('#net_cost').val(formatDecimal(unit_cost));
        $('#pro_tax').text(formatMoney(pr_tax_val + pr_tax_2_val));
        $('#pro_total').text(formatMoney(unit_cost + pr_tax_val + pr_tax_2_val));
    }
});

$(document).on('keypress', '#pamount_to_paid', function(e){
    if (e.keyCode == 13) {
        $('#editItem').trigger('click');
    }
});

$(document).on('click', '.delete_expense', function(){
    item = $(this).data('item');
    expenses = JSON.parse(localStorage.getItem('expenses'));
    delete expenses[item];
    localStorage.removeItem('expenses');
    localStorage.setItem('expenses', JSON.stringify(expenses));
    loadItems();
});

$(document).on('click', '#rt_paying_commission', function(){
    $('#rtModal').modal('show');
});