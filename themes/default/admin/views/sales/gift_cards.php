<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
    var customer;
    var filter_user;
    var status;
    var creation_type;
    var expiration_date;
    <?php if (isset($_POST['start_date'])): ?>
        start_date = '<?= $_POST['start_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['end_date'])): ?>
        end_date = '<?= $_POST['end_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['customer'])): ?>
        customer = '<?= $_POST['customer'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['filter_user'])): ?>
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['status'])): ?>
        status = '<?= $_POST['status'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['creation_type'])): ?>
        creation_type = '<?= $_POST['creation_type'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['expiration_date'])): ?>
        expiration_date = '<?= $_POST['expiration_date'] ?>';
    <?php endif ?>
    $(document).ready(function () {
        $('#GCData').dataTable({
            "aaSorting": [[1, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('sales/getGiftCards') ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": start_date
                }, {
                    "name": "end_date",
                    "value": end_date
                }, {
                    "name": "customer",
                    "value": customer
                }, {
                    "name": "user",
                    "value": filter_user
                }, {
                    "name": "status",
                    "value": status
                }, {
                    "name": "creation_type",
                    "value": creation_type
                }, {
                    "name": "expiration_date",
                    "value": expiration_date
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false,"mRender": checkbox},
                {"mRender": fld},
                {"mRender": gc_creation_type},
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                null,
                null,
                {"mRender": fsd},
                {"mRender": gc_status},
                {"bSortable": false}
            ]
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <?= admin_form_open('sales/gift_cards', ['id'=>'sales_filter']) ?>
                                <div class="col-sm-4">
                                    <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                    <?php
                                    echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="filter_customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');
                                    ?>
                                </div>
                                <?php if ($this->Owner || $this->Admin): ?>
                                    <div class="col-sm-4">
                                        <label><?= lang('user') ?></label>
                                        <select name="filter_user" id="filter_user" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($users): ?>
                                                <?php foreach ($users as $user): ?>
                                                    <option value="<?= $user->id ?>"><?= $user->first_name." ".$user->last_name ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                <?php endif ?>

                                <div class="col-sm-4">
                                    <label><?= lang('status') ?></label>
                                    <select name="status" id="status" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="1"><?= lang('active') ?></option>
                                        <option value="2"><?= lang('consumed') ?></option>
                                        <option value="3"><?= lang('cancelled') ?></option>
                                        <option value="4"><?= lang('expired') ?></option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label><?= lang('creation_type') ?></label>
                                    <select name="creation_type" id="creation_type" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="1"><?= lang('sale') ?></option>
                                        <option value="2"><?= lang('points_redeemed') ?></option>
                                    </select>
                                </div>

                                <hr class="col-sm-11">

                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>

                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                        <div class="col-sm-4 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('start_date', 'start_date') ?>
                                        <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                    </div>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('end_date', 'end_date') ?>
                                        <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $this->sma->fld($_POST['end_date']) : $this->filtros_fecha_final ?>" class="form-control datetime">
                                    </div>
                                </div>

                                <hr class="col-sm-11">

                                <div class="col-sm-4">
                                    <?= lang('expiration_date', 'expiration_date') ?>
                                    <input type="text" name="expiration_date" id="expiration_date" value="<?= isset($_POST['expiration_date']) ? $_POST['expiration_date'] : '' ?>" class="form-control date">
                                </div>

                                <div class="col-sm-12" style="margin-top: 2%;">
                                    <input type="hidden" name="filtered" value="1">
                                    <button type="submit" class="btn btn-primary btn-outline" id="submit-sales-filter_dh"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    <button type="button" class="btn btn-danger btn-outline" onclick="window.location.href = site.base_url+'sales/gift_cards';"><span class="fa fa-times"></span> <?= lang('reset') ?></button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?= admin_form_open('sales/gift_card_actions', 'id="action-form"') ?>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?php echo admin_url('sales/add_gift_card'); ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus"></i> <?= sprintf(lang('add_gift_card'), lang('gift_card')) ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><?php //echo $this->lang->line("list_results"); ?></p> -->
                            <div class="table-responsive">
                                <table id="GCData" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkth" type="checkbox" name="check"/>
                                        </th>
                                        <th><?php echo $this->lang->line("date"); ?></th>
                                        <th><?php echo $this->lang->line("creation_type"); ?></th>
                                        <th><?php echo $this->lang->line("sale_reference_no"); ?></th>
                                        <th><?php echo $this->lang->line("gift_card")." No"; ?></th>
                                        <th><?php echo $this->lang->line("value"); ?></th>
                                        <th><?php echo $this->lang->line("balance"); ?></th>
                                        <th><?php echo $this->lang->line("created_by"); ?></th>
                                        <th><?php echo $this->lang->line("customer"); ?></th>
                                        <th><?php echo $this->lang->line("expiry"); ?></th>
                                        <th><?php echo $this->lang->line("status"); ?></th>
                                        <th style="width:65px;"><?php echo $this->lang->line("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="12" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->

<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
    <?= form_submit('submit', 'submit', 'id="action-form-submit"') ?>
</div>
<?= form_close() ?>
<script language="javascript">
    $(document).ready(function () {

        $('#delete').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#excel').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        $('#pdf').click(function (e) {
            e.preventDefault();
            $('#form_action').val($(this).attr('data-action'));
            $('#action-form-submit').trigger('click');
        });

        if (customer != '') {
                $('#filter_customer').val(customer).select2({
                    minimumInputLength: 1,
                    data: [],
                    initSelection: function (element, callback) {
                        $.ajax({
                            type: "get", async: false,
                            url: site.base_url+"customers/getCustomer/" + $(element).val(),
                            dataType: "json",
                            success: function (data) {
                                callback(data[0]);
                            }
                        });
                    },
                    ajax: {
                        url: site.base_url + "customers/suggestions",
                        dataType: 'json',
                        quietMillis: 15,
                        data: function (term, page) {
                            return {
                                term: term,
                                limit: 10
                            };
                        },
                        results: function (data, page) {
                            if (data.results != null) {
                                return {results: data.results};
                            } else {
                                return {results: [{id: '', text: lang.no_match_found}]};
                            }
                        }
                    }
                });

            $('#filter_customer').trigger('change');
        } else {
            $('#filter_customer').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: site.base_url+"customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        }

        if (filter_user) {
            setTimeout(function() {
                $('#filter_user').select2('val', filter_user);
            }, 850);
        }

        if (status) {
            setTimeout(function() {
                $('#status').select2('val', status);
            }, 850);
        }

        if (creation_type) {
            setTimeout(function() {
                $('#creation_type').select2('val', creation_type);
            }, 850);
        }


    });

    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
</script>
