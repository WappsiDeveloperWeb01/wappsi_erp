<div class="row">
    <div class="col-xs-12">
        <?php $arr_sn = json_decode($shop_settings->manage_social_networks); ?>
        <p style="font-size:1.1em;font-weight:bold;" class="share_title"><?= lang('share_message'); ?></p>
        <ul class="rrssb-buttons clearfix">
            <?php foreach ($arr_sn as $arr_sn_key => $arr_sn_data): ?>
                <?php if ($arr_sn_data->status == 1): ?>
                    <li class="rrssb-<?= $arr_sn_data->name ?> rrsb_item">
                        <a class="popup" <?= !empty($arr_sn_data->color) ? 'style="background-color: '.$arr_sn_data->color.' !important;"' : '' ?>>
                            <span class="rrssb-icon"><img src="<?= base_url('assets/images/social_networks_logos/').$arr_sn_data->icon ?>"></span>
                            <span class="rrssb-text"><?= $arr_sn_data->name ?></span>
                        </a>
                    </li>
                <?php endif ?>
            <?php endforeach ?>
        </ul>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        if ($('.rrsb_item').length == 0) {
            $('.share_title').fadeOut();
        }
    });
</script>