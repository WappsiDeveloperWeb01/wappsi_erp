<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
        </div>
        <?= admin_form_open("system_settings/update_invoice_notes", ["id"=>"update_invoice_notes_form"]); ?>
        <?= form_hidden('id', $invoice_note_data->id); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <?= form_label(lang("invoice_notes_description") ." *"); ?>
                <?= form_input(["name"=>"description", "id"=>"description", "class"=>"form-control", "max-length"=>500, "required"=>TRUE], $invoice_note_data->description); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("invoice_notes_status") ." *"); ?>
                <?php $sopts = [""=>lang('select'), "1"=>"Activo", "0"=>"Inactivo"]; ?>
                <?= form_dropdown(["name"=>"status", "id"=>"status", "class"=>"form-control select2", "required"=>TRUE], $sopts, $invoice_note_data->state); ?>
            </div>

            <div class="form-group">
                <?= form_label(lang("invoice_notes_module") ." *"); ?>
                <?php $mopts = [""=>lang('select'), "2"=>"Ventas", "14"=>"Recibo de caja"]; ?>
                <?= form_dropdown(["name"=>"module", "id"=>"module", "class"=>"form-control select2", "required"=>TRUE], $mopts, $invoice_note_data->module); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_button("invoice_note_edit", lang("save"), 'class="btn btn-primary" id="invoice_note_edit"'); ?>
        </div>
        <?= form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script>
    $(document).ready(function() {
        $("#update_invoice_notes_form").validate({ ignore: [] });

        $(document).on('click', '#invoice_note_edit', function() {
            if ($('#update_invoice_notes_form').valid()) {
                $('#update_invoice_notes_form').submit();
            }
        });
    });
</script>
