<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Budget_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBudgets($data = NULL)
    {
        if (!empty($data)) {
            $this->db->where($data);
        }

        $q = $this->db->get('sales_budget');
        return $q->result();
    }

    public function getBudget($data = NULL)
    {
        if (!empty($data)) {
            $this->db->where($data);
        }

        $q = $this->db->get('sales_budget');
        return $q->row();

    }

    public function getBudgetByAllBranch($data = NULL, $isDetailed = FALSE)
    {
        if ($isDetailed) {
            $this->db->where('biller_id <>', 0);
        } else {
            $this->db->where('biller_id', 0);
        }

        if (!empty($data)) {
            $this->db->where($data);
        }

        $q = $this->db->get('sales_budget');
        return $q->row();
    }

    public function getBudgetByAllSellers($data = NULL, $isDetailed = FALSE)
    {
        if ($isDetailed) {
            $this->db->where('seller_id <>', 0);
        } else {
            $this->db->where('seller_id', 0);
        }

        if (!empty($data)) {
            $this->db->where($data);
        }

        $q = $this->db->get('sales_budget');
        return $q->row();

    }

    public function getBudgetByAllCategories($data = NULL, $isDetailed = FALSE)
    {
        if ($isDetailed) {
            $this->db->where('category_id <>', 0);
        } else {
            $this->db->where('category_id', 0);
        }

        if (!empty($data)) {
            $this->db->where($data);
        }

        $q = $this->db->get('sales_budget');
        return $q->row();

    }

    public function getSumByBranches($data = NULL)
    {
        $this->db->select('SUM(amount) AS amount, biller_id, biller.name');
        $this->db->join('companies biller', 'biller.id = sales_budget.biller_id', 'left');
        if (!empty($data)) {
            $this->db->where($data);
        }
        $this->db->group_by('biller_id');
        $q = $this->db->get('sales_budget');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getSumBySellers($data = NULL)
    {
        $this->db->select('SUM(amount) AS amount, seller_id, seller.name');
        $this->db->join('companies seller', 'seller.id = sales_budget.seller_id', 'left');
        if (!empty($data)) {
            $this->db->where($data);
        }
        $this->db->group_by('seller_id');
        $q = $this->db->get('sales_budget');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getSumByCategories($data = NULL)
    {
        $this->db->select('SUM(amount) AS amount, category_id, categories.name');
        $this->db->join('categories', 'categories.id = sales_budget.category_id', 'left');
        if (!empty($data)) {
            $this->db->where($data);
        }
        $this->db->group_by('category_id');
        $q = $this->db->get('sales_budget');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getBranchesFromBudget($data = NULL)
    {
        $this->db->select('companies.id, companies.name');
        $this->db->join('companies', 'companies.id = sales_budget.biller_id', 'inner');
        $this->db->group_by('companies.id');
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get('sales_budget');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getSellerByBillerFromBudget($data = NULL)
    {
        $this->db->select('companies.id, companies.name');
        $this->db->join('companies', 'companies.id = sales_budget.seller_id', 'inner');
        $this->db->group_by('companies.id');
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get('sales_budget');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getCategoriesBySellerFromBudget($data = NULL)
    {
        $this->db->select("categories.id, CONCAT(UCASE(SUBSTRING({$this->db->dbprefix('categories')}.name, 1, 1)), LCASE(SUBSTRING({$this->db->dbprefix('categories')}.name, 2))) AS name");
        $this->db->join('categories', 'categories.id = sales_budget.category_id', 'inner');
        $this->db->group_by('categories.id');
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get('sales_budget');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function insert($data)
    {
        if ($this->db->insert('sales_budget', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update($data)
    {
        $this->db->where('id', $data['id']);
        unset($data['id']);
        if ($this->db->update('sales_budget', $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);
        if ($this->db->delete('sales_budget')) {
            return TRUE;
        }
        return FALSE;
    }
}