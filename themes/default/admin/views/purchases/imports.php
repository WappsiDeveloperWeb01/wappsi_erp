<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
if ($this->session->userdata('purchase_processing')) {
    $this->session->unset_userdata('purchase_processing');
}
?>
<div class="wrapper wrapper-content  animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <?= admin_form_open('purchases/imports', ['id' => 'purchases_filter']) ?>

                                <div class="col-sm-4">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                    $biller_selected = '';
                                    $biller_readonly = false;
                                    if ($this->session->userdata('biller_id')) {
                                        $biller_selected = $this->session->userdata('biller_id');
                                        $biller_readonly = true;
                                    }

                                    $bl[""] = lang('select');
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <?= lang('status', 'status') ?>
                                    <select name="status" id="status" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="completed" <?= isset($_POST['status']) && $_POST['status'] == 'completed' ? 'selected="selected"' : '' ?>><?= lang('completed') ?></option>
                                        <option value="pending" <?= isset($_POST['status']) && $_POST['status'] == 'pending' ? 'selected="selected"' : '' ?>><?= lang('pending') ?></option>
                                    </select>
                                </div>
                                <?php if ($this->Owner || $this->Admin) : ?>
                                    <div class="col-sm-4">
                                        <label><?= lang('user') ?></label>
                                        <select name="filter_user" id="filter_user" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($users) : ?>
                                                <?php foreach ($users as $user) : ?>
                                                    <option value="<?= $user->id ?>"><?= $user->first_name . " " . $user->last_name ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                <?php endif ?>
                                <hr class="col-sm-11">
                                <?php if ($this->Settings->purchase_datetime_management == 1) : ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                    <div class="date_controls_dh">
                                        <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_dh') ?>
                                                <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('start_date', 'start_date') ?>
                                            <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                        </div>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('end_date', 'end_date') ?>
                                            <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control datetime">
                                        </div>
                                    </div>
                                <?php else : ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <?= lang('date_records_filter', 'date_records_filter') ?>
                                        <select name="date_records_filter" id="date_records_filter_h" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>

                                    <div class="date_controls_h">
                                        <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_h') ?>
                                                <select name="filter_year" id="filter_year_h" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('start_date', 'start_date') ?>
                                            <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                        </div>
                                        <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                            <?= lang('end_date', 'end_date') ?>
                                            <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                        </div>
                                    </div>
                                <?php endif ?>


                                <div class="col-sm-12" style="margin-top: 2%;">
                                    <input type="hidden" name="filtered" value="1">
                                    <button type="submit" id="submit-purchases-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    <button type="button" onclick="window.location.href = site.base_url+'purchases';" class="btn btn-danger"><span class="fa fa-times"></span> <?= lang('reset') ?></button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('purchases/purchase_actions', 'id="action-form"');
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <h4 class="text_filter"></h4>
                                <table id="POData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("biller"); ?></th>
                                            <th><?= lang("tag"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th style="width:100px;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script>
    var start_date = "<?= $this->sma->fsd($this->filtros_fecha_inicial) ?>";
    var end_date = "<?= $this->sma->fsd($this->filtros_fecha_final) ?>";
    var dias_vencimiento;
    var purchase_reference_no;
    var biller;
    var filter_user;
    var supplier;
    var status;
    var payment_status;
    var filtered = 0;
    var filtered_ini_date;
    <?php if (isset($_POST['start_date'])) : ?>
        start_date = '<?= $_POST['start_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['end_date'])) : ?>
        end_date = '<?= $_POST['end_date'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['dias_vencimiento'])) : ?>
        dias_vencimiento = '<?= $_POST['dias_vencimiento'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['purchase_reference_no'])) : ?>
        purchase_reference_no = '<?= $_POST['purchase_reference_no'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['biller'])) : ?>
        biller = '<?= $_POST['biller'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['supplier'])) : ?>
        supplier = '<?= $_POST['supplier'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['status'])) : ?>
        status = '<?= $_POST['status'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['payment_status'])) : ?>
        payment_status = '<?= $_POST['payment_status'] ?>';
    <?php endif ?>
    <?php if (isset($_POST['filtered'])) : ?>
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php endif ?>
    <?php if (isset($_POST['filter_user'])) : ?>
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php endif ?>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }
    $(document).ready(function() {
        oTable = $('#POData').dataTable({
            "aaSorting": [
                [1, "desc"],
                [2, "desc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('purchases/getImports') ?>',
            dom: '<"row" <"col-sm-1"l><"col-sm-2 additionalControlsContainer"><"col-sm-6"f><"col-sm-3 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": start_date
                }, {
                    "name": "end_date",
                    "value": end_date
                }, {
                    "name": "purchase_reference_no",
                    "value": purchase_reference_no
                }, {
                    "name": "biller",
                    "value": biller
                }, {
                    "name": "status",
                    "value": status
                }, {
                    "name": "user",
                    "value": filter_user
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            aoColumns: [{
                    "bSortable": false,
                    "mRender": checkbox
                },
                null,
                null,
                null,
                null,
                {
                    "mRender": approved_status
                },
                {
                    "bSortable": false
                }
            ],
            fnRowCallback: function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "import_link";
                return nRow;
            },
            drawCallback: function(settings) {
                $('.actionsButtonContainer').html('<div class="dropdown pull-right" style="margin-top: 5px;">' +
                    '<button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>' +
                    '<ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">' +
                    '<li>' +
                    '<a href="<?= admin_url('purchases/add_import') ?>">' +
                    '<i class="fa fa-plus-circle"></i> <?= lang('add_import') ?>' +
                    '</a>' +
                    '</li>' +
                    '</ul>' +
                    '</div></div>').css('padding-right', '0');

                    $('[data-toggle-second="tooltip"]').tooltip();
                    $('[data-toggle="tooltip"]').tooltip();
            }
        });

        <?php if ($this->session->userdata('remove_imls')) { ?>
            if (localStorage.getItem('imtag')) {
                localStorage.removeItem('imtag');
            }
            if (localStorage.getItem('imbiller')) {
                localStorage.removeItem('imbiller');
            }
            if (localStorage.getItem('impurchases')) {
                localStorage.removeItem('impurchases');
            }
            if (localStorage.getItem('imexpenses')) {
                localStorage.removeItem('imexpenses');
            }
            if (localStorage.getItem('imstatus')) {
                localStorage.removeItem('imstatus');
            }
            if (localStorage.getItem('imsupplier')) {
                localStorage.removeItem('imsupplier');
            }
        <?php
            $this->sma->unset_data('remove_imls');
        }
        ?>

        if (filtered !== undefined) {
            setTimeout(function() {
                console.log('biller ' + biller);
                $('#start_date').val(start_date);
                $('#end_date').val(end_date);
                $('#dias_vencimiento').val(dias_vencimiento);
                $('#purchase_reference_no').select2('val', purchase_reference_no);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                $('#status').select2('val', status);
                $('#payment_status').select2('val', payment_status);
                // $('.collapse-link').click();
            }, 900);
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);

        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>

    });


    function calcularMinutos(start_date, end_date) {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff / (1000 * 60);
        return minutos;
    }

    function setFilterText() {

        var reference_text = $('#purchase_reference_no option:selected').data('dtprefix');
        var biller_text = $('#biller option:selected').text();
        var supplier_text = $('#filter_supplier').select2('data') !== null ? $('#filter_supplier').select2('data').text : '';
        var status_text = $('#status option:selected').text();
        var payment_status_text = $('#payment_status option:selected').text();
        var start_date_text = $('<?= $this->Settings->purchase_datetime_management == 1 ? "#start_date_dh" : "#start_date_h" ?>').val();
        var end_date_text = $('<?= $this->Settings->purchase_datetime_management == 1 ? "#end_date_dh" : "#end_date_h" ?>').val();
        var text = "Filtros configurados : ";

        coma = false;

        if (purchase_reference_no != '' && purchase_reference_no !== undefined) {
            text += " Tipo documento (" + reference_text + ")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text += coma ? "," : "";
            text += " Sucursal (" + biller_text + ")";
            coma = true;
        }
        if (supplier != '' && supplier !== undefined) {
            text += coma ? "," : "";
            text += " Proveedor (" + supplier_text + ")";
            coma = true;
        }
        if (status != '' && status !== undefined) {
            text += coma ? "," : "";
            text += " Estado de compra (" + status_text + ")";
            coma = true;
        }
        if (payment_status != '' && payment_status !== undefined) {
            text += coma ? "," : "";
            text += " Estado de pago (" + payment_status_text + ")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha de inicio (" + start_date_text + ")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha final (" + end_date_text + ")";
            coma = true;
        }

        $('.text_filter').html(text);

    }

    function confirmSync(element)
    {
        swal({
            title: 'Sincronización con Recepción de Documentos',
            text: '¿Está seguro de realizar la sincronización?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function() {
            swal({
                title: 'Realizando sincronización...',
                text: 'Por favor, espere un momento',
                type: 'info',
                showCancelButton: false,
                showConfirmButton: false,
                closeOnClickOutside: false
            });

            syncDocumentReception(element);
        });
    }

    function syncDocumentReception(element) {
        $.ajax({
            type: "post",
            url: element.attr('href'),
            data: {
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>"
            },
            dataType: "json",
            success: function (response) {
                swal.close();
                Command: toastr.success(response.message, '<?= lang("toast_success_title"); ?>');
            }
        });
    }

    <?php if ($this->Settings->purchase_datetime_management == 1) : ?>
        $(document).ready(function() {
            setTimeout(function() {
                <?php if (!isset($_POST['date_records_filter'])) : ?>
                    $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                    $('#purchases_filter').submit();
                <?php elseif ($_POST['date_records_filter'] != $this->Settings->default_records_filter) : ?>
                    $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
                <?php endif ?>
            }, 150);
        });
    <?php else : ?>
        $(document).ready(function() {
            setTimeout(function() {
                <?php if (!isset($_POST['date_records_filter'])) : ?>
                    $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                    $('#sales_filter').submit();
                <?php elseif ($_POST['date_records_filter'] != $this->Settings->default_records_filter) : ?>
                    $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
                <?php endif ?>
            }, 150);
        });
    <?php endif ?>
</script>