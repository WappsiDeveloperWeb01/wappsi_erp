<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-md">
    <div class="modal-content">
        <?= admin_form_open_multipart("system_settings/add_cost_center/" . '', ['id' => 'add_cost_center']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_cost_center'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-12">
                        <?= lang('name', 'name') ?>
                        <?= form_input('name', '', 'class="form-control" id="name" required="required"'); ?>
                    </div>
                    <div class="form-group col-md-12">
                        <?= lang('code', 'code') ?>
                        <?= form_input('code', '', 'class="form-control" id="code" required="required"'); ?>
                    </div>
                </div>
                <?= form_hidden('add', '1'); ?>
            </div>
            <div class="modal-footer">
                <button class="btn btn-success submit" type="button"><?= lang('submit') ?></button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function(){

    });

    $(document).on('click', '.submit', function(){
        if ($('#add_cost_center').valid()) {
            $('#add_cost_center').submit();
        }
    });
</script>