<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight no-print">
    <div class="ibox">
        <div class="ibox-content">
            <div class="row ">
                <div class="col-md-12">
                    <div class="title-action">
                        <a href="#" class="btn btn-primary" onclick="window.print();return false;" id="print-icon">
                            <?= lang('print') ?>
                        </a>
                    </div>
                </div>
            </div>
            <?= admin_form_open_multipart("products/print_barcodes", ['id' => 'barcode-print-form', 'data-toggle'=> 'validator']); ?>
                <div class="">
                    <div class="col-sm-12 form-group">
                        <?= lang('style', 'style'); ?>
                        <?php $opts = array(
                                        '' => lang('select').' '.lang('style'),
                                        40 => lang('40_per_sheet'),
                                        30 => lang('30_per_sheet'),
                                        24 => lang('24_per_sheet'),
                                        20 => lang('20_per_sheet'),
                                        18 => lang('18_per_sheet'),
                                        14 => lang('14_per_sheet'),
                                        12 => lang('12_per_sheet'),
                                        10 => lang('10_per_sheet'),
                                        50 => lang('continuous_feed'),
                                        'zebra_32x25' => lang('zebra_32x25'),
                                        'zebra_32x25_standard' => lang('zebra_32x25_standard'),
                                        'zebra_50x25' => lang('zebra_50x25'),
                                        'zebra_50x25_standard' => lang('zebra_50x25_standard'),
                                        'zebra_100x50_standard' => lang('zebra_100x50_standard'),
                                        'zebra_7_5x1_standard' => lang('zebra_7_5x1_standard'),
                                        'zebra_7_2x1_standard' => lang('zebra_7_2x1_standard'),
                                        'ticket_8x4' => lang('ticket_8x4'),
                                        'zebra_32x25_moda_variants' => lang('zebra_32x25_moda_variants'),
                                        'zebra_32x25_inventory' => lang('inventory'),
                                        'zebra_32x15_standard'   => lang('zebra_32x15_standard'),
                                    ); ?>
                        <?= form_dropdown('style', $opts, set_value('style', $this->Settings->default_zebra_print), 'class="form-control tip" id="style" required="required"'); ?>
                        <div class="row cf-con" style="margin-top: 10px; display: none;">
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <?= form_input('cf_width', '', 'class="form-control" id="cf_width" placeholder="' . lang("width") . '"'); ?>
                                        <span class="input-group-addon" style="padding-left:10px;padding-right:10px;"><?= lang('inches'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                    <div class="input-group">
                                        <?= form_input('cf_height', '', 'class="form-control" id="cf_height" placeholder="' . lang("height") . '"'); ?>
                                        <span class="input-group-addon" style="padding-left:10px;padding-right:10px;"><?= lang('inches'); ?></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="form-group">
                                <?php $oopts = array(0 => lang('portrait'), 1 => lang('landscape')); ?>
                                    <?= form_dropdown('cf_orientation', $oopts , '', 'class="form-control" id="cf_orientation" placeholder="' . lang("orientation") . '"'); ?>
                                </div>
                            </div>
                        </div>
                        <span class="help-block"><?= lang('barcode_tip'); ?></span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-sm-4 form-group div_100x50" style="display:none;">
                        <?= lang('warehouse', 'warehouse') ?>
                        <select name="warehouse" id="warehouse" class="form-control">
                            <option value=""><?= lang('select') ?></option>
                            <?php foreach ($warehouses as $wh): ?>
                                <option value="<?= $wh->id ?>"><?= $wh->name ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                    <div class="col-sm-4 form-group div_100x50" style="display:none;">
                        <?= lang('show_zero_quantity', 'show_zero_quantity') ?>
                        <select name="show_zero_quantity" id="show_zero_quantity" class="form-control">
                            <option value="1"><?= lang('yes') ?></option>
                            <option value="0"><?= lang('no') ?></option>
                        </select>
                    </div>
                    <div class="col-sm-4 form-group div_100x50" style="display:none;">
                        <?= lang('show_variants_quantity', 'show_variants_quantity') ?>
                        <select name="show_variants_quantity" id="show_variants_quantity" class="form-control">
                            <option value="1"><?= lang('yes') ?></option>
                            <option value="0"><?= lang('no') ?></option>
                        </select>
                    </div>
                    <div class="col-sm-12 form-group">
                        <span style="font-weight: bold; margin-right: 15px;"><?= lang('print'); ?>:</span> <br>
                        <input name="site_name" type="checkbox" id="site_name" value="1" checked="checked" style="display:inline-block;" />
                        <label for="site_name" class="padding05"><?= lang('site_name'); ?></label>
                        <?php if ($this->session->userdata('biller_id') && !isset($purchase)): ?>
                            <select name="biller" readonly>
                                <?php foreach ($billers as $biller): ?>
                                    <option value="<?= $biller->id ?>" <?= $this->session->userdata('biller_id') == $biller->id ? "selected='selected'" : "" ?>><?= $biller->name ?></option>
                                <?php endforeach ?>
                            </select>
                        <?php else: ?>
                            <select name="biller">
                                <?php foreach ($billers as $biller): ?>
                                    <option value="<?= $biller->id ?>" <?= isset($purchase) && $purchase->biller_id == $biller->id ? "selected='selected'" : "" ?>><?= $biller->name ?></option>
                                <?php endforeach ?>
                            </select>
                        <?php endif ?>
                        <input name="product_name" type="checkbox" id="product_name" value="1" checked="checked" style="display:inline-block;" />
                        <label for="product_name" class="padding05"><?= lang('product_name'); ?></label>
                        <input name="price" type="checkbox" id="price" value="1" checked="checked" style="display:inline-block;" />
                        <label for="price" class="padding05"><?= lang('price'); ?></label>
                        <input name="currencies" type="checkbox" id="currencies" value="1" style="display:inline-block;" />
                        <label for="currencies" class="padding05"><?= lang('currencies'); ?></label>
                        <input name="unit" type="checkbox" id="unit" value="1" style="display:inline-block;" />
                        <label for="unit" class="padding05"><?= lang('unit'); ?></label>
                        <input name="category" type="checkbox" id="category" value="1" style="display:inline-block;" />
                        <label for="category" class="padding05"><?= lang('category'); ?></label>
                        <input name="variants" type="checkbox" id="variants" value="1" style="display:inline-block;" />
                        <label for="variants" class="padding05"><?= lang('variants'); ?></label>
                        <input name="product_image" type="checkbox" id="product_image" value="1" style="display:inline-block;" />
                        <label for="product_image" class="padding05"><?= lang('product_image'); ?></label>
                        <input name="check_promo" type="checkbox" id="check_promo" value="1" checked="checked" style="display:inline-block;" />
                        <label for="check_promo" class="padding05"><?= lang('check_promo'); ?></label>
                        <input name="reference" type="checkbox" id="reference" value="1" checked="checked" style="display:inline-block;" />
                        <label for="reference" class="padding05"><?= lang('reference'); ?></label>


                        <input name="barcode" type="checkbox" id="barcode" value="1" checked="checked" style="display:inline-block;" />
                        <label for="barcode" class="padding05"><?= lang('barcode'); ?></label>

                        <input name="barcode_text" type="checkbox" id="barcode_text" value="1" checked="checked" style="display:inline-block;" />
                        <label for="barcode_text" class="padding05"><?= lang('barcode_text'); ?></label>

                        <?php if (isset($custom_fields) && $custom_fields): ?>
                            <?php foreach ($custom_fields as $cf_arr): ?>
                                <?php $cf = $cf_arr['data']; ?>
                                <input name="<?= $cf->cf_code ?>" type="checkbox" id="<?= $cf->cf_code ?>" value="1" checked="checked" style="display:inline-block;" />
                                <label for="<?= $cf->cf_code ?>"><?= $cf->cf_name ?></label>
                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                    <div class="form-group">
                        <?php echo form_input('add_item', '', 'class="form-control" id="add_item" placeholder="' . $this->lang->line("add_item") . '"'); ?>
                    </div>
                    <div class="form-group div_moda_variants" style="display:none;">
                       <div class="form-group col-sm-4">
                            <label>Subir detalle vía excel</label>
                            <br>
                            <label class="status_switch">
                              <input name="upload_xls" id="upload_xls" type="checkbox" class="skip">
                              <span class="status_slider"></span>
                            </label>
                        </div>
                        <div class="form-group col-sm-4 xls_file_div" style="display:none;">
                            <?= lang("xls_file", "xls_file") ?>
                            <input id="xls_file" type="file" data-browse-label="<?= lang('browse'); ?>" name="xls_file" required="required" data-show-upload="false" data-show-preview="false" class="form-control file">
                        </div>
                        <div class="col-md-4 xls_file_div" style="display:none;">
                            <label><?= lang('sample_file') ?></label>
                            <br>
                            <a href="<?php echo $this->config->base_url(); ?>assets/csv/sample_print_products.xlsx" class="btn btn-primary"><i class="fa fa-download"></i> <?= lang('download') ?></a>
                        </div>
                    </div>
                    <?php if (isset($purchase)): ?>
                        <input type="hidden" name="purchase_date" value="<?= $purchase->date ?>">
                    <?php endif ?>
                    <div class="controls table-controls">
                        <table id="bcTable"
                            class="table items  table-bordered table-condensed table-hover">
                            <thead>
                            <tr>
                                <th class="col-xs-4"><?= lang("product_name") . " (" . $this->lang->line("product_code") . ")"; ?></th>
                                <th class="col-xs-1"><?= lang("quantity"); ?></th>
                                <th class="col-xs-7"><?= lang("variants"); ?></th>
                                <th class="text-center" style="width:30px;">
                                    <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                </th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                    <div class="form-group">
                        <?php echo form_submit('print', lang("update"), 'class="btn btn-primary"'); ?>
                        <button type="button" id="reset" class="btn btn-danger"><?= lang('reset'); ?></button>
                    </div>
                    <div class="clearfix"></div>
                </div><!-- /.well well-sm no-print -->
            <?= form_close(); ?>
        </div>
    </div>
    
</div>

<div id="barcode-con">
    <?php
        if ($this->input->post('print') && !$pdf_print) {
            if (!empty($barcodes)) {
                echo '<button type="button" onclick="window.print();return false;" class="btn btn-primary btn-block tip no-print" title="'.lang('print').'"><i class="icon fa fa-print"></i> '.lang('print').'</button>';
                $c = 1;
                if ($style == 12 || $style == 18 || $style == 24 || $style == 40) {
                    echo '<div class="barcodea4">';
                } elseif ($style != 50) {
                    echo '<div class="barcode">';
                }
                foreach ($barcodes as $item) {
                    for ($r = 1; $r <= $item['quantity']; $r++) {
                        echo '<div class="item style'.$style.'" '.
                        ($style == 50 && $this->input->post('cf_width') && $this->input->post('cf_height') ?
                            'style="width:'.$this->input->post('cf_width').'in;height:'.$this->input->post('cf_height').'in;border:0;"' : '')
                        .'>';
                        if ($style == 50) {
                            if ($this->input->post('cf_orientation')) {
                                $ty = (($this->input->post('cf_height')/$this->input->post('cf_width'))*100).'%';
                                $landscape = '
                                -webkit-transform-origin: 0 0;
                                -moz-transform-origin:    0 0;
                                -ms-transform-origin:     0 0;
                                transform-origin:         0 0;
                                -webkit-transform: translateY('.$ty.') rotate(-90deg);
                                -moz-transform:    translateY('.$ty.') rotate(-90deg);
                                -ms-transform:     translateY('.$ty.') rotate(-90deg);
                                transform:         translateY('.$ty.') rotate(-90deg);
                                ';
                                echo '<div class="div50" style="width:'.$this->input->post('cf_height').'in;height:'.$this->input->post('cf_width').'in;border: 1px dotted #CCC;'.$landscape.'">';
                            } else {
                                echo '<div class="div50" style="width:'.$this->input->post('cf_width').'in;height:'.$this->input->post('cf_height').'in;border: 1px dotted #CCC;padding-top:0.025in;">';
                            }
                        }
                        if($item['image']) {
                            echo '<span class="product_image"><img src="'.base_url('assets/uploads/thumbs/'.$item['image']).'" alt="" /></span>';
                        }
                        if($item['site']) {
                            echo '<span class="barcode_site">'.$item['site'].'</span>';
                        }
                        if($item['name']) {
                            echo '<span class="barcode_name">'.$item['name'].'</span>';
                        }
                        if($item['price']) {
                            echo '<span class="barcode_price"> ';
                            if($item['currencies']) {
                                foreach ($currencies as $currency) {
                                    echo $currency->code . ': ' . $this->sma->formatMoney($item['price'] * $currency->rate, 'none').', ';
                                }
                            } else {
                                echo $item['price'];
                            }
                            echo '</span> ';
                        }
                        if($item['unit']) {
                            echo '<span class="barcode_unit">'.lang('unit').': '.$item['unit'].'</span>, ';
                        }
                        if($item['category']) {
                            echo '<span class="barcode_category">'.lang('category').': '.$item['category'].'</span> ';
                        }
                        if($item['variants']) {
                            echo '<span class="variants">';
                            foreach ($item['variants'] as $variant) {
                                echo $variant->name.' ';
                            }
                            echo '</span> ';
                        }
                        echo '<span class="barcode_image"><img src="'.admin_url('products/barcode/'.$item['barcode'].'/'.$item['bcs'].'/'.$item['bcis']).'" alt="'.$item['barcode'].'" class="bcimg" /></span>';
                        if ($style == 50) {
                            echo '</div>';
                        }
                        echo '</div>';
                        if ($style == 40) {
                            if ($c % 40 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcodea4">';
                            }
                        } elseif ($style == 30) {
                            if ($c % 30 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcode">';
                            }
                        } elseif ($style == 24) {
                            if ($c % 24 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcodea4">';
                            }
                        } elseif ($style == 20) {
                            if ($c % 20 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcode">';
                            }
                        } elseif ($style == 18) {
                            if ($c % 18 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcodea4">';
                            }
                        } elseif ($style == 14) {
                            if ($c % 14 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcode">';
                            }
                        } elseif ($style == 12) {
                            if ($c % 12 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcodea4">';
                            }
                        } elseif ($style == 10) {
                            if ($c % 10 == 0) {
                                echo '</div><div class="clearfix"></div><div class="barcode">';
                            }
                        }
                        $c++;
                    }
                }
                if ($style != 50) {
                    echo '</div>';
                }
                echo '<button type="button" onclick="window.print();return false;" class="btn btn-primary btn-block tip no-print" title="'.lang('print').'"><i class="icon fa fa-print"></i> '.lang('print').'</button>';
            } else {
                echo '<h3>'.lang('no_product_selected').'</h3>';
            }
        }
    ?>
</div><!-- /#barcode-con -->
<script type="text/javascript">
    var ac = false; bcitems = {};
    if (localStorage.getItem('bcitems')) {
        bcitems = JSON.parse(localStorage.getItem('bcitems'));
    }
    <?php if($items) { ?>
    localStorage.setItem('bcitems', JSON.stringify(<?= $items; ?>));
    <?php } ?>
    $(document).ready(function() {
        <?php if ($this->input->post('print')) { ?>
            $( window ).load(function() {
                $('html, body').animate({
                    scrollTop: ($("#barcode-con").offset().top)-15
                }, 1000);
            });
        <?php } ?>
        if (localStorage.getItem('bcitems')) {
            loadItems();
        }
        $("#add_item").autocomplete({
            source: function (request, response) {
                 $.ajax({
                    type: 'get',
                    url: '<?= admin_url('products/get_suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#warehouse").val(),
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                    abc: 4,
                };
            },
            delay: 250,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_product_item(ui.item);
                    if (row) {
                        $(this).val('');
                    }
                } else {
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        check_add_item_val();

        $('#style').change(function (e) {
            localStorage.setItem('bcstyle', $(this).val());
            if ($(this).val() == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        });
        if (style = localStorage.getItem('bcstyle')) {
            $('#style').val(style);
            $('#style').select2("val", style).trigger('change');
            if (style == 50) {
                $('.cf-con').slideDown();
            } else {
                $('.cf-con').slideUp();
            }
        }


        $('#warehouse').change(function (e) {
            localStorage.setItem('bcwarehouse', $(this).val());
            $('#warehouse').select2("readonly", true);
        });
        if (warehouse = localStorage.getItem('bcwarehouse')) {
            $('#warehouse').val(warehouse);
            $('#warehouse').select2("val", warehouse).trigger('change');
            setTimeout(function() {
                $('#warehouse').select2("readonly", true);
            }, 850);
            console.log('BLOQUEAR');
        }
        $('#cf_width').change(function (e) {
            localStorage.setItem('cf_width', $(this).val());
        });
        if (cf_width = localStorage.getItem('cf_width')) {
            $('#cf_width').val(cf_width);
        }

        $('#cf_height').change(function (e) {
            localStorage.setItem('cf_height', $(this).val());
        });
        if (cf_height = localStorage.getItem('cf_height')) {
            $('#cf_height').val(cf_height);
        }

        $('#cf_orientation').change(function (e) {
            localStorage.setItem('cf_orientation', $(this).val());
        });
        if (cf_orientation = localStorage.getItem('cf_orientation')) {
            $('#cf_orientation').val(cf_orientation);
        }

        $(document).on('ifChecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 1);
        });
        $(document).on('ifUnchecked', '#site_name', function(event) {
            localStorage.setItem('bcsite_name', 0);
        });
        if (site_name = localStorage.getItem('bcsite_name')) {
            if (site_name == 1)
                $('#site_name').iCheck('check');
            else
                $('#site_name').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 1);
        });
        $(document).on('ifUnchecked', '#product_name', function(event) {
            localStorage.setItem('bcproduct_name', 0);
        });
        if (product_name = localStorage.getItem('bcproduct_name')) {
            if (product_name == 1)
                $('#product_name').iCheck('check');
            else
                $('#product_name').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#price', function(event) {
            localStorage.setItem('bcprice', 1);
        });
        $(document).on('ifUnchecked', '#price', function(event) {
            localStorage.setItem('bcprice', 0);
            $('#currencies').iCheck('uncheck');
        });
        if (price = localStorage.getItem('bcprice')) {
            if (price == 1)
                $('#price').iCheck('check');
            else
                $('#price').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 1);
        });
        $(document).on('ifUnchecked', '#currencies', function(event) {
            localStorage.setItem('bccurrencies', 0);
        });
        if (currencies = localStorage.getItem('bccurrencies')) {
            if (currencies == 1)
                $('#currencies').iCheck('check');
            else
                $('#currencies').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 1);
        });
        $(document).on('ifUnchecked', '#unit', function(event) {
            localStorage.setItem('bcunit', 0);
        });
        if (unit = localStorage.getItem('bcunit')) {
            if (unit == 1)
                $('#unit').iCheck('check');
            else
                $('#unit').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#category', function(event) {
            localStorage.setItem('bccategory', 1);
        });
        $(document).on('ifUnchecked', '#category', function(event) {
            localStorage.setItem('bccategory', 0);
        });
        if (category = localStorage.getItem('bccategory')) {
            if (category == 1)
                $('#category').iCheck('check');
            else
                $('#category').iCheck('uncheck');
        }
        $(document).on('ifChecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 1);
        });
        $(document).on('ifUnchecked', '#check_promo', function(event) {
            localStorage.setItem('bccheck_promo', 0);
        });
        if (check_promo = localStorage.getItem('bccheck_promo')) {
            if (check_promo == 1)
                $('#check_promo').iCheck('check');
            else
                $('#check_promo').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 1);
        });
        $(document).on('ifUnchecked', '#product_image', function(event) {
            localStorage.setItem('bcproduct_image', 0);
        });
        if (product_image = localStorage.getItem('bcproduct_image')) {
            if (product_image == 1)
                $('#product_image').iCheck('check');
            else
                $('#product_image').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 1);
        });
        $(document).on('ifUnchecked', '#variants', function(event) {
            localStorage.setItem('bcvariants', 0);
        });
        if (variants = localStorage.getItem('bcvariants')) {
            if (variants == 1)
                $('#variants').iCheck('check');
            else
                $('#variants').iCheck('uncheck');
        }

        $(document).on('ifChecked', '.checkbox', function(event) {
            var item_id = $(this).attr('data-item-id');
            var vt_id = $(this).attr('id');
            bcitems[item_id]['selected_variants'][vt_id] = 1;
            localStorage.setItem('bcitems', JSON.stringify(bcitems));
        });
        $(document).on('ifUnchecked', '.checkbox', function(event) {
            var item_id = $(this).attr('data-item-id');
            var vt_id = $(this).attr('id');
            bcitems[item_id]['selected_variants'][vt_id] = 0;
            localStorage.setItem('bcitems', JSON.stringify(bcitems));
        });
        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete bcitems[id];
            localStorage.setItem('bcitems', JSON.stringify(bcitems));
            $(this).closest('#row_' + id).remove();
        });

        $('#reset').click(function (e) {
            bootbox.confirm(lang.r_u_sure, function (result) {
                if (result) {
                    if (localStorage.getItem('bcitems')) {
                        localStorage.removeItem('bcitems');
                    }
                    if (localStorage.getItem('bcstyle')) {
                        localStorage.removeItem('bcstyle');
                    }
                    if (localStorage.getItem('bcsite_name')) {
                        localStorage.removeItem('bcsite_name');
                    }
                    if (localStorage.getItem('bcproduct_name')) {
                        localStorage.removeItem('bcproduct_name');
                    }
                    if (localStorage.getItem('bcprice')) {
                        localStorage.removeItem('bcprice');
                    }
                    if (localStorage.getItem('bccurrencies')) {
                        localStorage.removeItem('bccurrencies');
                    }
                    if (localStorage.getItem('bcunit')) {
                        localStorage.removeItem('bcunit');
                    }
                    if (localStorage.getItem('bccategory')) {
                        localStorage.removeItem('bccategory');
                    }
                    // if (localStorage.getItem('cf_width')) {
                    //     localStorage.removeItem('cf_width');
                    // }
                    // if (localStorage.getItem('cf_height')) {
                    //     localStorage.removeItem('cf_height');
                    // }
                    // if (localStorage.getItem('cf_orientation')) {
                    //     localStorage.removeItem('cf_orientation');
                    // }
                    $('#modal-loading').show();
                    window.location.replace("<?= admin_url('products/print_barcodes'); ?>");
                }
            });
        });
        var old_row_qty;
        $(document).on("focus", '.quantity', function () {
            old_row_qty = $(this).val();
        }).on("change", '.quantity', function () {
            var row = $(this).closest('tr');
            if (!is_numeric($(this).val())) {
                $(this).val(old_row_qty);
                bootbox.alert(lang.unexpected_value);
                return;
            }
            var new_qty = parseFloat($(this).val()),
            item_id = row.attr('data-item-id');
            bcitems[item_id].qty = new_qty;
            localStorage.setItem('bcitems', JSON.stringify(bcitems));
        });



        $(document).on('ifChecked', '#barcode', function(event) {
            localStorage.setItem('bcbarcode', 1);
        });
        $(document).on('ifUnchecked', '#barcode', function(event) {
            localStorage.setItem('bcbarcode', 0);
        });
        if (barcode = localStorage.getItem('bcbarcode')) {
            if (barcode == 1)
                $('#barcode').iCheck('check');
            else
                $('#barcode').iCheck('uncheck');
        }

        $(document).on('ifChecked', '#barcode_text', function(event) {
            localStorage.setItem('bcbarcode_text', 1);
        });
        $(document).on('ifUnchecked', '#barcode_text', function(event) {
            localStorage.setItem('bcbarcode_text', 0);
        });
        if (barcode_text = localStorage.getItem('bcbarcode_text')) {
            if (barcode_text == 1)
                $('#barcode_text').iCheck('check');
            else
                $('#barcode_text').iCheck('uncheck');
        }

    <?php if ($pdf_print): ?>
        <?php
            $this->session->set_userdata('barcodes', json_encode($barcodes));
            $this->session->set_userdata('show_variants_quantity', $show_variants_quantity);
            $this->session->set_userdata('barcodes_style', $style);
        ?>
        window.open(site.base_url+'products/print_barcodes_pdf');
    <?php else: ?>
        <?php
            $this->session->unset_userdata('barcodes');
            $this->session->unset_userdata('show_variants_quantity');
            $this->session->unset_userdata('barcodes_style');
        ?>
    <?php endif ?>

    });

    function add_product_item(item) {
        ac = true;
        if (item == null) {
            return false;
        }
        // item_id = item.id;
        item_id = item.id + (item.variant_selected !== undefined && item.variant_selected ? item.variant_selected : Math.floor(Math.random() * 900000) + 100000);
        if (bcitems[item_id]) {
            bcitems[item_id].qty = parseFloat(bcitems[item_id].qty) + 1;
        } else {
            bcitems[item_id] = item;
            bcitems[item_id]['selected_variants'] = {};
            $.each(item.variants, function () {
                if (item.variant_selected) {
                    if (item.variant_selected == this.id) {
                        bcitems[item_id]['selected_variants'][this.id] = 1;
                    } else {
                        bcitems[item_id]['selected_variants'][this.id] = 0;
                    }
                } else {
                    $.each(item.variants, function () {
                        bcitems[item_id]['selected_variants'][this.id] = 0;
                    });
                }
            });
                
        }
        localStorage.setItem('bcitems', JSON.stringify(bcitems));
        loadItems();
        return true;
    }

    function loadItems () {
        if (localStorage.getItem('bcitems')) {
            $("#bcTable tbody").empty();
            bcitems = JSON.parse(localStorage.getItem('bcitems'));
            $.each(bcitems, function (index) {
                var item = this;
                var row_no = item.id;
                var vd = '';
                var newTr = $('<tr id="row_' + row_no + '" class="row_' + item.id + '" data-item-id="' + index + '"></tr>');
                tr_html = '<td>'+
                                '<input name="product[]" type="hidden" value="' + item.id + '">'+
                                '<input name="product_row_no[]" type="hidden" value="' + index + '">'+
                                '<span id="name_' + row_no + '">' + item.name + ' (' + item.code + ')</span></td>';
                tr_html += '<td><input class="form-control quantity text-center" name="quantity[]" type="text" value="' + formatDecimal(item.qty) + '" data-id="' + row_no + '" data-item="' + item.id + '" id="quantity_' + row_no + '" onClick="this.select();"></td>';
                if(item.variants) {
                    $.each(item.variants, function () {
                        if (($('#show_zero_quantity').val() == 0 && this.wh_quantity > 0) || $('#show_zero_quantity').val() == 1) {
                            vd += '<input name="vt_'+index+'_'+ item.id +'_'+ this.id +'" type="checkbox" class="checkbox" id="'+this.id+'" data-item-id="'+index+'" value="'+this.id+'" '+( item.selected_variants[this.id] == 1 ? 'checked="checked"' : '')+' style="display:inline-block;" /><label for="'+this.id+'" class="padding05">'+this.name+" ("+this.code+")"+($('#show_variants_quantity').val() == 1 && this.wh_quantity ? "("+formatQuantity2(this.wh_quantity)+")" : '')+'</label>';
                        }
                    });
                }
                tr_html += '<td>'+vd+'</td>';
                tr_html += '<td class="text-center"><i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
                newTr.html(tr_html);
                newTr.appendTo("#bcTable");
            });
            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
            return true;
        }
    }
    $(document).on('change', '#show_variants_quantity, #show_zero_quantity', function(){
        loadItems();
    });


    $(document).on('change', '#style', function(){
        if ($(this).val() == 'zebra_32x25') {
            $('#site_name').iCheck('uncheck');
            $('#product_name').iCheck('check');
            $('#price').iCheck('uncheck');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#variants').iCheck('uncheck');
            $('#product_image').iCheck('uncheck');
            $('#check_promo').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check');
        } else if ($(this).val() == 'zebra_50x25') {
            $('#site_name').iCheck('uncheck');
            $('#product_name').iCheck('check');
            $('#price').iCheck('uncheck');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#variants').iCheck('uncheck');
            $('#product_image').iCheck('check');
            $('#check_promo').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check');
        } else if ($(this).val() == 'zebra_32x25_standard') {
            $('#site_name').iCheck('check');
            $('#product_name').iCheck('check');
            $('#price').iCheck('check');
            $('#reference').iCheck('check');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#variants').iCheck('uncheck');
            $('#product_image').iCheck('uncheck');
            $('#check_promo').iCheck('uncheck');
            $('#color').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check');
        } else if ($(this).val() == 'zebra_32x25_inventory') {
            $('#site_name').iCheck('check');
            $('#product_name').iCheck('check');
            $('#price').iCheck('uncheck');
            $('#reference').iCheck('check');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#variants').iCheck('uncheck');
            $('#product_image').iCheck('uncheck');
            $('#check_promo').iCheck('uncheck');
            $('#color').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check'); 
        } else if ($(this).val() == 'zebra_50x25_standard') {
            $('#site_name').iCheck('check');
            $('#product_name').iCheck('check');
            $('#price').iCheck('check');
            $('#reference').iCheck('check');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#variants').iCheck('uncheck');
            $('#product_image').iCheck('uncheck');
            $('#check_promo').iCheck('uncheck');
            $('#color').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check');
        } else if ($(this).val() == 'ticket_8x4') {
            $('#site_name').iCheck('uncheck');
            $('#product_name').iCheck('check');
            $('#price').iCheck('check');
            $('#reference').iCheck('uncheck');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#variants').iCheck('uncheck');
            $('#product_image').iCheck('uncheck');
            $('#check_promo').iCheck('uncheck');
            $('#color').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check');
        } else if ($(this).val() == 'zebra_32x15_standard') {
            $('#site_name').iCheck('uncheck');
            $('#product_name').iCheck('check');
            $('#price').iCheck('check');
            $('#reference').iCheck('uncheck');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#variants').iCheck('uncheck');
            $('#product_image').iCheck('uncheck');
            $('#check_promo').iCheck('uncheck');
            $('#color').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check');
        }  else {
            $('#site_name').iCheck('check');
            $('#product_name').iCheck('check');
            $('#price').iCheck('check');
            $('#variants').iCheck('check');
            $('#product_image').iCheck('check');
            $('#check_promo').iCheck('check');
            $('#currencies').iCheck('uncheck');
            $('#unit').iCheck('uncheck');
            $('#category').iCheck('uncheck');
            $('#barcode').iCheck('check');
            $('#barcode_text').iCheck('check');
        }

        if ($(this).val() == 'zebra_100x50_standard') {
            $('.div_100x50').fadeIn();
            $('.div_normal').fadeOut();
        } else {
            if ($(this).val() == 'zebra_32x25_moda_variants') {
                $('.div_normal').fadeOut();
                $('.div_moda_variants').fadeIn();
            } else {
                $('.div_normal').fadeIn();
                $('.div_moda_variants').fadeOut();
            }
            $('.div_100x50').fadeOut();
        }
    });
$(document).on('change', '#upload_xls', function(){
    input = $(this);
    if (input.is(':checked')) {
        $('.xls_file_div').fadeIn();
    } else {
        $('.xls_file_div').fadeOut();
    }
});
    
</script>