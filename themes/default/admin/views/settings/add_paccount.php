<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
$tipos = array(1 =>
                array('Ingreso' => 'Ingreso', 'Iva' => 'Iva (Venta)', 'Inventario' => 'Inventario (Venta)', 'Costo' => 'Costo', 'Dev vta' => 'Dev vta', 'Iva Dev vta' => 'Iva Dev vta', 'Impto consumo vta' => 'Impto consumo vta', 'Descuento' => 'Descuento (Venta)', 'Impto verde' => 'Impto verde', 'Envio vta' => 'Envio vta', 'Propina vta' => 'Propina vta', 'Autorretencion Debito' => 'Autorretención Cuenta Débito', 'Autorretencion Credito' => 'Autorretención Cuenta Crédito'),
               2 =>
                array('Inventario' => 'Inventario (Compra)', 'Iva' => 'Iva (Compra)', 'Dev compra' => 'Dev compra', 'Iva Dev compra' => 'Iva Dev compra', 'Impto consumo compra' => 'Impto consumo compra', 'Descuento' => 'Descuento (Compra)', 'Envio compra' => 'Envio compra'));

 ?>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <?= admin_form_open_multipart("system_settings/add_paccount/" . '', ['id' => 'add_paccount']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
                </button>
                <h4 class="modal-title" id="myModalLabel"><?php echo lang('add_paccount'); ?></h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12 form-group">
                        <?= lang('parameter_type', 'typemov') ?>
                        <select name="typemov" id="typemov" class="form-control" required>
                            <option value=""><?= lang('select') ?></option>
                            <?php foreach ($tipos as $tipoP => $arr): ?>
                                <?php if ($tipoP==1): ?>
                                    <optgroup label="VENTA">
                                <?php elseif ($tipoP == 2): ?>
                                    <optgroup label="COMPRA">
                                <?php endif ?>
                                <?php foreach ($arr as $tipoC => $tipoC2): ?>
                                    <option value="<?= $tipoC ?>" data-typeparent="<?= $tipoP ?>"><?= $tipoC2 ?></option>
                                <?php endforeach ?>
                                </optgroup>
                            <?php endforeach ?>
                        </select>
                        <input type="hidden" name="type" id="type">
                    </div>
                    <?php if (isset($tax_rates)): ?>
                        <div class="col-sm-12 form-group">
                            <?= lang("tax", "tax_id"); ?>
                            <?php $tr[''] = lang("select");
                            foreach ($tax_rates as $rate) {
                                $tr[$rate->id] = $rate->name;
                            }
                            echo form_dropdown('tax_id', $tr, '', 'id="tax_id" class="form-control tip" style="width:100%;"'); ?>
                        </div>
                    <?php endif ?>
                    <?php if (isset($categories)): ?>
                        <div class="col-sm-12 form-group">
                            <?= lang('category', 'category_id') ?>
                            <select name="category_id" id="category_id" class="form-control ledger-dropdown2">
                                <option value=""><?= lang('select') ?></option>
                                <?php foreach ($categories as $category): ?>
                                    <option value="<?= $category->id; ?>"><?= $category->name; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    <?php endif ?>
                    <div class="col-sm-12 form-group">
                        <?= lang('ledger', 'ledger_id') ?>
                        <?php
                            $lopts[''] = lang('select');
                            foreach ($ledgers as $ledger) {
                                $lopts[$ledger->id] = $ledger->code." - ".$ledger->name;
                            }
                         ?>
                        <?= form_dropdown('ledger_id', $lopts, '', 'class="form-control" id="ledger_id" style="width:100%;" required="required"'); ?>
                    </div>
                </div>
                <?= form_hidden('add', '1'); ?>
                <input type="hidden" name="update_existing" id="update_existing">
            </div>
            <div class="modal-footer">
                <button class="btn btn-success submit" type="button"><?= lang('submit') ?></button>
            </div>
        <?php echo form_close(); ?>
    </div>
</div>

<?= $modal_js ?>

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change', '#typemov', function(){
            type = $('option:selected', this).data('typeparent');
            $('#type').val(type);
        });
    });

    $(document).on('click', '.submit', function(){

        $.ajax({
            url : site.base_url+'system_settings/validate_paccount_exists',
            dataType : 'JSON',
            data : {
                'type' : $('#type').val(),
                'typemov' : $('#typemov').val(),
                'id_tax' : $('#tax_id').val(),
                'id_category' : $('#category_id').val(),
            }
        }).done(function(data){
            if (data.response == true) {
                bootbox.confirm({
                    message: "Ya existe una parametrización contable con las características ingresadas, ¿Desea actualizar?",
                    buttons: {
                        confirm: {
                            label: 'Si, Actualizar',
                            className: 'btn-success btn-full send-submit-sale'
                        },
                        cancel: {
                            label: 'Cancelar',
                            className: 'btn-danger btn-full btn-outline'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            $('#update_existing').val(1);
                            setTimeout(function() {
                                if ($('#add_paccount').valid()) {
                                    $('#add_paccount').submit();
                                } else {
                                    setTimeout(function() {
                                        $('#myModal').modal('show')
                                    }, 850);
                                }
                            }, 1000);
                        } else {
                            $('#update_existing').val(0);
                            setTimeout(function() {
                                $('#myModal').modal('show')
                            }, 850);
                        }
                    }
                });
            } else {
                if ($('#add_paccount').valid()) {
                    $('#add_paccount').submit();
                }
            }
        });

        // 
    });
</script>