<?php
    $apppath = str_replace("\app", "", APPPATH);
    $apppath = str_replace("/app", "", $apppath);
    require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';

    class PDF extends HPDF
    {
        public $initialX = 0;
        public $initialY = 0;

        function Header()
        {
            $firstColumnWidth = 100;
            $secondColumnWith = 80;

            $this->SetTitle($this->pageTitle, true);

            $this->Image(base_url($this->imagePath), 25, 28, 47);

            $this->SetTextColor(98);

            $this->initialX = $this->GetX();
            $this->initialY = $this->GetY();

            $this->SetXY($this->initialX, $this->initialY + 20);
            $this->SetFont('Arial','', 7);
            $this->Cell($firstColumnWidth, 5, $this->sma->utf8Decode($this->billerName), 0, 1, 'L');
            $this->Cell($firstColumnWidth, 3, $this->sma->utf8Decode($this->greatContributor .' gran contribuyentes'), 0, 1, 'L');
            $this->Cell($firstColumnWidth, 3, $this->sma->utf8Decode($this->ivaRetainer .' Retenedores de IVA'), 0, 1, 'L');
            $this->Cell($firstColumnWidth, 3, $this->sma->utf8Decode($this->fuenteRetainer .' Autoretenedor del Impuesto sobre las Renta'), 0, 1, 'L');
            $this->Cell($firstColumnWidth, 3, $this->sma->utf8Decode($this->icaRetainer .' Autoretenedor de ICA'), 0, 0, 'L');

            $x = $this->GetX();

            $this->SetXY($x, $this->initialY + 5);
            $this->SetFont('Arial','B', 14);
            $this->MultiCell(0, 4, $this->sma->utf8Decode('Colilla de Nómina'), 0, 'L');

            $this->SetXY($x, $this->initialY + 20);
            $this->SetFont('Arial','', 7);
            $this->Cell($secondColumnWith, 5, $this->sma->utf8Decode('Representación Gráfica'), 0, 0);

            $this->SetXY($x, $this->initialY + 25);
            $this->Cell($secondColumnWith / 2, 3, $this->sma->utf8Decode('Fecha de generación:'), 0, 0);
            $this->Cell(0, 3, $this->sma->utf8Decode($this->generationDate), 0, 0, 'R');

            $this->SetXY($x, $this->initialY + 28);
            $this->Cell($secondColumnWith / 2, 3, $this->sma->utf8Decode('Medio de pago:'), 0, 0);
            $this->Cell(0, 3, $this->sma->utf8Decode($this->paymentMethod), 0, 0, 'R');

            $this->SetXY($x, $this->initialY + 31);
            $this->Cell($secondColumnWith / 2, 3, $this->sma->utf8Decode('Moneda:'), 0, 0);
            $this->Cell(0, 3, $this->sma->utf8Decode($this->defaultCurrency), 0, 0, 'R');

            $this->SetXY($x, $this->initialY + 34);
            $this->Cell($secondColumnWith / 2, 3, $this->sma->utf8Decode('Periodo:'), 0, 0);
            $this->Cell(0, 3, $this->sma->utf8Decode($this->fortnight. " " .$this->period), 0, 0, 'R');

            $x = $this->GetX();

            $this->SetXY($x, $this->initialY);
            $this->SetFont('Arial','B', 11);
        }

        function Footer()
        {
            $this->SetY(-25);

            $this->RoundedRect(20, 252, 176, 8, 0.8, '1234', '');
            $this->SetFont('Arial','B',8);

            $this->SetFont('Arial','',8);
            $this->Cell(176, 5 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS NIT 901.090.070-9  www.wappsi.com'), 0, 1, "C");
            // $this->Image(base_url().'assets/images/cadena_logo.jpeg', 150, 253.5, 45);
            $this->Ln(1);

            $this->Cell(0, 5, $this->sma->utf8Decode('Página '.$this->PageNo().' / {nb}'), 0, 0, 'C');
        }

        function Table($header, $data, $showTotal = true)
        {
            $this->SetFont('Arial','B', 11);

            $w = array(96, 10, 10, 30, 29);
            for($i = 0; $i < count($header); $i++) {
                if ($i == 0) {
                    $align = 'L';
                } else if ($i == 3 || $i == 4 || $i == 5) {
                    $align = 'R';
                } else {
                    $align = 'C';
                }

                $this->Cell($w[$i], 7, $this->sma->utf8Decode($header[$i]), 'TB', 0, $align);
            }
            $this->Ln();

            $this->SetFont('Arial','', 9);

            $earneds = $deductions = 0;
            foreach ($data as $row) {
                $earned = $deduction = $percentage = 0;
                $conceptName = '';
                foreach($this->concepts as $concept) {
                    if ($row->payroll_concept_id == $concept->id) {
                        $conceptName = $concept->name;

                        if ($concept->percentage > 0) {
                            $percentage = $concept->percentage;
                        }

                        break;
                    }
                }

                if ($row->type == EARNED) {
                    $earned = $row->amount;
                    $earneds += $row->amount;
                }
                if ($row->type == DEDUCTION) {
                    $deduction = $row->amount;
                    $deductions += $row->amount;
                }

                $this->Cell($w[0], 6, $this->sma->utf8Decode($conceptName), 'B');
                $this->Cell($w[1], 6, $percentage, 'B', 0, 'C');
                $this->Cell($w[2], 6, $row->days_quantity, 'B', 0, 'C');
                $this->Cell($w[3], 6, number_format($earned, 2), 'B', 0, 'R');
                $this->Cell($w[4], 6, number_format($deduction, 2), 'B', 0, 'R');

                $this->Ln();
            }

            $this->Cell($w[0] + $w[1] + $w[2], 6, '', 'T');
            $this->Cell($w[3], 6, number_format($earneds, 2), 0, 0, 'R');
            $this->Cell($w[4], 6, number_format($deductions, 2), 0, 0, 'R');
            $this->Ln(10);

            if ($showTotal) {
                $this->SetFont('Arial','B', 11);
                $this->Cell(array_sum($w), 6, $this->sma->utf8Decode('Total a pagar: '. number_format($earneds - $deductions, 2)), 0, 0, 'R');
            }

            $this->Ln(10);
        }
    }

    $pdf = new PDF('P', 'mm', array(216, 279));

    $pdf->period = $period;
    $pdf->concepts = $concepts;
    $pdf->fortnight = $fortnight;
    $pdf->imagePath = $imagePath;
    $pdf->pageTitle = $pageTitle;
    $pdf->billerName = $billerName;
    $pdf->ivaRetainer = $ivaRetainer;
    $pdf->icaRetainer = $icaRetainer;
    $pdf->paymentMethod = $paymentMethod;
    $pdf->fuenteRetainer = $fuenteRetainer;
    $pdf->generationDate = $generationDate;
    $pdf->defaultCurrency = $defaultCurrency;
    $pdf->greatContributor = $greatContributor;
    $pdf->widthPageWithoutMargins = $pdf->GetPageWidth() - 40;
    $pdf->widthColumnHeader = $pdf->widthPageWithoutMargins / 2;

    $pdf->AliasNbPages();
    $pdf->sma = $this->sma;
$pdf->SetMargins(20, 30);

    $pdf->AddPage();
    $pdf->SetTextColor(98);
    $pdf->SetDrawColor(180);
    $pdf->SetLineWidth(0.4);

    $initialX = $pdf->GetX();
    $initialY = $pdf->GetY();

    $pdf->SetXY($pdf->initialX, $initialY + 50);

    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell($pdf->widthColumnHeader, 6, $this->sma->utf8Decode(strtoupper('Datos del empleador')), 'RB', 0, 'C');

    $X = $pdf->GetX();

    $pdf->SetXY($pdf->initialX, $initialY + 57);

    $widthFirstColumnInternal = $pdf->widthColumnHeader / 3;
    $widthSecondColumnInternal = $pdf->widthColumnHeader - $widthFirstColumnInternal;

    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Razón Social'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($company), 'R', 1);

    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('NIT/Cédula'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($numeroDocumento), 'R', 1);

    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('E-mail'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($billerEmail), 'R', 1);

    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Teléfono'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($billerPhone), 'R', 1);

    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Dirección'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($billerAddress), 'R', 1);

    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Ciudad, Depart.'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode(ucwords(strtolower($billerLocation))), 'R');

    $pdf->SetXY($X, $initialY + 50);

    $pdf->SetFont('Arial', 'B', 12);
    $pdf->Cell($pdf->widthColumnHeader, 6, $this->sma->utf8Decode(strtoupper('Datos del empleado')), 'B', 1, 'C');

    $pdf->SetXY($X, $initialY + 57);

    $pdf->SetFont('Arial', '', 9);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Razón Social'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($employeeName));

    $pdf->SetXY($X, $initialY + 61);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('NIT/Cédula'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($employeeVatNo));

    $pdf->SetXY($X, $initialY + 65);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('E-mail'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($employeeEmail), 0, 1);

    $pdf->SetXY($X, $initialY + 69);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Teléfono'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($employeePhone), 0, 1);

    $pdf->SetXY($X, $initialY + 73);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Dirección'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode($employeeAddress), 0, 1);

    $pdf->SetXY($X, $initialY + 77);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Ciudad, Depart.'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode(ucwords(strtolower($employeeLocation))));

    $pdf->SetXY($X, $initialY + 81);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Cargo'));
    $pdf->Cell($widthSecondColumnInternal, 4, $this->sma->utf8Decode(ucwords(strtolower($employeeProfessionalPositionName))));

    $pdf->SetXY($X, $initialY + 85);
    $pdf->Cell($widthFirstColumnInternal, 4, $this->sma->utf8Decode('Salario'));
    $pdf->Cell($widthSecondColumnInternal, 4, number_format($employeeBaseAmount, 2));

    $pdf->Ln(15);

    $header = array('Descripción', '%', 'Días', 'Devengados', 'Deducidos');
    $pdf->Table($header, $electronicPayrollItems);

    $pdf->Ln(30);
    $pdf->Cell(100, 5, "Empleado", "T", 0, "C");

    // if (!empty($conceptsPaidEmployee)) {
    //     $pdf->SetFont('Arial','B', 13);
    //     $pdf->Cell(0, 4, $this->sma->utf8Decode("Provisiones"), 0, 1);
    //     $pdf->Ln(3);
    //     $header = array('Descripción', '%', 'Días', 'Devengados', 'Deducidos');
    //     $pdf->Table($header, $conceptsPaidEmployee, false);
    // }

    $pdf->Output('I','Desprendible de Pago Nómina'.'.pdf');