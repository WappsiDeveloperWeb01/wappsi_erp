<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if (DEMO && ($m == 'main' && $v == 'index')) { ?>
<div class="page-contents padding-top-no">
    <div class="container">
        <div class="alert alert-info margin-bottom-no">
            <p>
                <strong>Shop module is not complete item but add-on to Stock Manager Advance and is available separately.</strong><br>
                This is joint demo for main item (Stock Manager Advance) and add-ons (POS & Shop Module). Please check the item page on codecanyon.net for more info about what's not included in the item and you must read the page there before purchase. Thank you
            </p>
        </div>
    </div>
</div>
<?php } ?>
<div class="modal fade in" id="preferencesModal" tabindex="-1" role="dialog" aria-labelledby="preferencesModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
          class="fa fa-2x">&times;</i></button>
          <h4 class="modal-title" id="preferencesModalLabel"><?=lang('product_preference_selection');?></h4>
        </div>
        <div class="modal-body pmmodal-body" style="height: 70%; overflow-y: auto; overflow-x: hidden;">
 
        </div>
        <div class="modal-footer">
            <input type="hidden" id="pref_pid">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
          <button type="button" id="resetPreferences" class="btn btn-danger"><?= lang('reset') ?></button>
          <button type="button" id="updatePreferences" class="btn btn-primary"><?=lang('update')?></button>
        </div>
      </div>
    </div>
</div>
<section class="footer <?= $this->v == 'reset_password' ? 'reset_password_footer' : '' ?>">
    <div class="container padding-bottom-md">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <div class="title-footer"><span><?= lang('about_us'); ?></span></div>
                <p>
                    <?= $shop_settings->description; ?> <a href="<?= site_url('page/'.$shop_settings->about_link); ?>"><?= lang('read_more'); ?></a>
                </p>
                <p>
                    <i class="fa fa-phone"></i> <span class="margin-left-md"><?= $shop_settings->phone; ?></span>
                    <i class="fa fa-envelope margin-left-xl"></i> <span class="margin-left-md"><?= $shop_settings->email; ?></span>
                </p>
                <ul class="list-inline">
                    <li><a href="<?= site_url('page/'.$shop_settings->privacy_link); ?>"><?= lang('privacy_policy'); ?></a></li>
                    <li><a href="<?= site_url('page/'.$shop_settings->terms_link); ?>"><?= lang('terms_conditions'); ?></a></li>
                    <li><a href="<?= site_url('page/'.$shop_settings->contact_link); ?>"><?= lang('contact_us'); ?></a></li>
                </ul>
            </div>

            <div class="clearfix visible-sm-block"></div>
            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span><?= lang('payment_methods'); ?></span></div>
                <p><?= $shop_settings->payment_text; ?></p>
                <img class="img-responsive payment_methods_img" src="<?= $assets; ?>/images/payment-methods.png" alt="Payment Methods">
            </div>

            <div class="col-md-3 col-sm-6">
                <div class="title-footer"><span><?= lang('follow_us'); ?></span></div>
                <p><?= $shop_settings->follow_text; ?></p>
                <ul class="follow-us">
                    <?php if (!empty($shop_settings->facebook)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->facebook; ?>"><i class="fa fa-facebook"></i></a></li>
                    <?php } if (!empty($shop_settings->twitter)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->twitter; ?>"><i class="fa fa-twitter"></i></a></li>
                    <?php } if (!empty($shop_settings->google_plus)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->google_plus; ?>"><i class="fa fa-google-plus"></i></a></li>
                    <?php } if (!empty($shop_settings->instagram)) { ?>
                    <li><a target="_blank" href="<?= $shop_settings->instagram; ?>"><i class="fa fa-instagram"></i></a></li>
                    <?php } ?>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="copyright line-height-lg">
                &copy; <?= date('Y'); ?> <?= $shop_settings->shop_name; ?>. <?= lang('all_rights_reserved'); ?>
            </div>
            <!-- <ul class="list-inline pull-right line-height-md">
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-blue" data-color="blue"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-blue-grey" data-color="blue-grey"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-brown" data-color="brown"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-cyan" data-color="cyan"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-green" data-color="green"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-grey" data-color="grey"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-purple" data-color="purple"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-orange" data-color="orange"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-pink" data-color="pink"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-red" data-color="red"><i class="fa fa-square"></i></a>
                </li>
                <li class="padding-x-no text-size-lg">
                    <a href="#" class="theme-color text-teal" data-color="teal"><i class="fa fa-square"></i></a>
                </li>
            </ul> -->
            <div class="clearfix"></div>
        </div>
    </div>
</section>

<a href="#" class="back-to-top text-center" onclick="$('body,html').animate({scrollTop:0},500); return false">
    <i class="fa fa-angle-double-up"></i>
</a>
</section>
<?php if (!get_cookie('shop_use_cookie') && get_cookie('shop_use_cookie') != 'accepted' && !empty($shop_settings->cookie_message)) { ?>
<div class="cookie-warning">
    <div class="bounceInLeft alert alert-info">
        <a href="<?= site_url('main/cookie/accepted'); ?>" class="close">&times;</a>
        <p>
            <?= $shop_settings->cookie_message; ?>
            <?php if (!empty($shop_settings->cookie_link)) { ?>
            <a href="<?= site_url('page/'.$shop_settings->cookie_link); ?>"><?= lang('read_more'); ?></a>
            <?php } ?>
        </p>
    </div>
</div>
<?php } ?>


<div class="modal fade in" id="registerModal" tabindex="-1" role="dialog" aria-labelledby="registerModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-lg no-modal-header modal_login">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <h2><?= lang('register_form') ?></h2>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 20px;font-size: 110%;top:1%;">
                        <i class="fa fa-2x">&times;</i>
                    </button>
                </div>
            </div>
            <div class="modal-body row">
                <div class="col-md-12">
                    <?php echo admin_form_open("auth/register", 'id="register_user" autocomplete="off"'); ?>
                    <div class="row">
                        <div class="col-md-12">
                            <label><?= lang('type_register') ?></label>
                            <div style="padding: 2% 0 2% 0; text-align: center;">
                                <label style="padding: 0 1% 0 1%">
                                    <input type="radio" name="type_register" class="type_register_personal" value="1">
                                    <?= lang('type_register_personal') ?>
                                </label>
                                <label style="padding: 0 1% 0 1%">
                                    <input type="radio" name="type_register" class="type_register_business" value="2">
                                    <?= lang('type_register_business') ?>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="div_data" style="display: none;">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><?= lang('legal_data') ?></h4>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= form_label(lang('label_type_person'), 'type_person'); ?>
                              <select name="type_person" id="type_person" class="form-control" required="required">
                                <option value=""><?= lang('select') ?></option>
                                <option value="<?= NATURAL_PERSON ?>">Persona Natural</option>
                                <option value="<?= LEGAL_PERSON ?>">Persona Jurídica</option>
                              </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 legal_person">
                              <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
                              <?php
                                $types_vat_regime_options[""] = lang('select');
                                foreach ($types_vat_regime as $type_vat_regime)
                                {
                                  $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                                }
                              ?>
                              <?= form_dropdown(["name"=>"type_vat_regime", "id"=>"type_vat_regime", "class"=>"form-control select"], $types_vat_regime_options); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 legal_person">
                                <input type="hidden" name="document_code" id="document_code">
                              <?= lang("company_type_document", "id_document_type"); ?>
                              <select class="form-control select" name="tipo_documento" id="tipo_documento" style="width: 100%;" required="required">
                                <option value=""><?= lang("select"); ?></option>
                                <?php foreach ($id_document_types as $idt) : ?>
                                  <option value="<?= $idt->id; ?>" data-code="<?= $idt->codigo_doc; ?>"><?= $idt->nombre; ?></option>
                                <?php endforeach ?>
                              </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <?= lang("vat_no", "vat_no"); ?>
                                <?php echo form_input('vat_no', '', 'class="form-control" id="vat_no" required="required"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 digito-verificacion legal_person">
                                <?= lang("check_digit", "check_digit"); ?>
                                <?php echo form_input('digito_verificacion', '', 'class="form-control" id="digito_verificacion" readonly'); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 person juridical_person legal_person">
                                <?= lang("label_business_name", "name"); ?>
                                <?php echo form_input('name', '', 'class="form-control tip" id="name" required="required"'); ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 natural_person">
                                <label>Primer Nombre</label><span class='input_required'> *</span>
                                <input type="text" name="first_name" class="form-control">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 natural_person">
                                <label>Segundo Nombre</label>
                                <input type="text" name="second_name" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 natural_person">
                                <label>Primer Apellido</label><span class='input_required'> *</span>
                                <input type="text" name="first_lastname" class="form-control">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 natural_person">
                                <label>Segundo Apellido</label>
                                <input type="text" name="second_lastname" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 company hide_pos_customer legal_person">
                                <?= lang("label_tradename", "company"); ?>
                                <?php echo form_input('company', '', 'class="form-control tip" id="company"'); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-12 col-xs-12 row">
                              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <?= lang('birth_month', 'birth_month') ?>
                                <?php
                                $bmopts[''] = lang('select');
                                for ($i=1; $i <=12 ; $i++) {
                                  $bmopts[$i] = lang('months')[$i];
                                }

                                 ?>
                                 <?= form_dropdown('birth_month', $bmopts, '', 'class="form-control" id="birth_month"') ?>
                              </div>
                              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <?= lang('birth_day', 'birth_day') ?>
                                <?php

                                $bdopts[''] = lang('select');

                                 ?>
                                <?= form_dropdown('birth_day', $bdopts, '', 'class="form-control" id="birth_day"') ?>
                              </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 form-group hide_pos_customer legal_person">
                              <?= form_label(lang('label_commercial_register'), 'commercial_register'); ?>
                              <?= form_input(['name'=>'commercial_register', 'id'=>'commercial_register', 'class'=>'form-control']); ?>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 legal_person">
                                <?= lang("company_email_address", "company_email_address"); ?>
                                <input type="email" name="company_email" class="form-control" id="company_email_address" required="required">
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6 legal_person">
                                <?= lang("company_phone", "company_phone"); ?>
                                <input type="tel" name="company_phone" class="form-control" required="required" id="phone"/>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group legal_person">
                              <?= form_label(lang("label_types_obligations"), "types_obligations"); ?>
                                <?php
                                    $types_obligations_options = [''=>lang('select')];
                                    foreach ($types_obligations as $types_obligation)
                                    {
                                        $types_obligations_options[$types_obligation->code] = $types_obligation->code ." - ". $types_obligation->description;
                                    }
                                ?>
                                <?= form_dropdown(['name'=>'types_obligations[]', 'id'=>'types_obligations', 'class'=>'form-control select', 'multiple'=>TRUE, 'required'=>TRUE, 'style'=>'height: auto;'], $types_obligations_options); ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><?= lang('ubication_data') ?></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= lang("country", "country"); ?>
                              <select class="form-control select" name="country" id="country" required>
                                <option value=""><?= lang("select") ?></option>
                                <?php foreach ($countries as $row => $country): ?>
                                  <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= $country->NOMBRE == $this->Settings->customer_default_country ? 'selected="selected"' : '' ?>><?= $country->NOMBRE ?></option>
                                <?php endforeach ?>
                              </select>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= lang("state", "state"); ?>
                              <select class="form-control select" name="state" id="state" required>
                                <option value=""><?= lang("select") ?></option>
                              </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= lang("city", "city"); ?>
                              <select class="form-control select" name="city" id="city" required>
                                <option value=""><?= lang("select") ?></option>
                              </select>
                              <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= lang("address", "address"); ?>
                              <input class="form-control" type="text" name="address" id="address" required="required">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= lang("zone", "zone"); ?>
                              <select class="form-control select" name="zone" id="zone">
                                <option value=""><?= lang("select") ?></option>
                              </select>
                              <input type="hidden" name="zone_code" id="zone_code" <?= (isset($_POST['zone_code'])) ? "value='".$_POST['zone_code']."'" : "" ?>>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= lang("subzone", "subzone"); ?>
                              <select class="form-control select" name="subzone" id="subzone">
                                <option value=""><?= lang("select") ?></option>
                              </select>
                              <input type="hidden" name="subzone_code" id="subzone_code" <?= (isset($_POST['subzone_code'])) ? "value='".$_POST['subzone_code']."'" : "" ?>>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                              <?= lang("postal_code", "postal_code"); ?>
                              <?php echo form_input('postal_code', '', 'class="form-control postal_code" id="postal_code"'); ?>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h4><?= lang('user_data') ?></h4>
                            </div>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 same_data" style="display: none;">
                                <label>
                                    <input type="checkbox" id="same_data_for_user">
                                    <?= lang('same_data_for_user') ?>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 legal_person" style="display: none;">
                                <label>Primer Nombre</label><span class='input_required'> *</span>
                                <input type="text" name="user_first_name" class="form-control">
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 legal_person" style="display: none;">
                                <label>Primer Apellido</label><span class='input_required'> *</span>
                                <input type="text" name="user_first_lastname" class="form-control">
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <?= lang("email_address", "email_address"); ?>
                                <input type="email" name="email" class="form-control" id="email_address" required="required">
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <?= lang("phone", "phone"); ?>
                                <input type="tel" name="phone" class="form-control" required="required" id="phone"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <?= lang("gender", "gender"); ?>
                                <select name="gender" id="gender" class="form-control">
                                    <option value=""><?= lang('select') ?></option>
                                    <option value="1"><?= lang('male') ?></option>
                                    <option value="2"><?= lang('female') ?></option>
                                </select>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?= lang('username', 'username'); ?>
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-user"></i></span>
                                        <input type="text" name="username" id="username" class="form-control " placeholder="<?= lang('username') ?>" required="required"  autocomplete="off"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php echo lang('password', 'password1'); ?>
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-key"></i></span>
                                        <?php echo form_password('password', '', 'class="form-control tip" id="password1" required="required" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" data-bv-regexp-message="'.lang('pasword_hint').'"'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <?php echo lang('confirm_password', 'confirm_password'); ?>
                                    <div class="input-group">
                                        <span class="input-group-addon "><i class="fa fa-key"></i></span>
                                        <?php echo form_password('confirm_password', '', 'class="form-control" id="confirm_password" required="required" data-bv-identical="true" data-bv-identical-field="password" data-bv-identical-message="' . lang('pw_not_same') . '"'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>      

                        <div class="row">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-primary pull-right submit_register"><span class="fa fa-user"></span> <?= lang('register_now') ?></button>
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>  
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false" z-index="1">
    <div class="modal-dialog modal-sm no-modal-header modal_login">
        <div class="modal-content">
            <div class="modal-header">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <img src="<?= base_url('assets/images/user-default.png') ?>" style="width: 35%;">
                    </div>
                    <div class="col-md-12 text-center">
                        <h3><?= lang('login') ?></h3>
                    </div>
                    <!-- <div class="col-md-12"> -->
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="position: absolute;right: 20px;font-size: 110%;top:1%;">
                            <i class="fa fa-2x">&times;</i>
                        </button>
                    <!-- </div> -->
                </div>
            </div>
            <div class="modal-body row">
                <?= form_open('login', 'class="validate"'); ?>
                    <input type="hidden" name="from_shop" value="1">
                    <div class="col-md-12 div_login">
                        <div class="form-group">
                            <label for="username" class="control-label"><?= lang('identity'); ?></label>
                            <input type="text" name="identity" id="identity" class="form-control" value="" required placeholder="<?= lang('email'); ?>">
                        </div>
                        <div class="form-group">
                            <label for="password" class="control-label"><?= lang('password'); ?></label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="<?= lang('password'); ?>" value="" required>
                        </div>
                        <?php
                        if ($Settings->captcha) {
                            ?>
                            <div class="form-group">
                            <div class="form-group text-center">
                                    <span class="captcha-image"><?= $image; ?></span>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <a href="<?= admin_url('auth/reload_captcha'); ?>" class="reload-captcha text-blue">
                                                <i class="fa fa-refresh"></i>
                                            </a>
                                        </span>
                                        <?= form_input($captcha); ?>
                                    </div>
                                </div>
                            </div>
                            <?php
                        } /* echo $recaptcha_html; */
                        ?>
                        <div class="form-group">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="1" name="remember_me"><span> <?= lang('remember_me'); ?></span>
                                </label>
                            </div>
                        </div>
                        <button type="submit" value="login" name="login" class="btn btn-block btn-success"><?= lang('login'); ?></button>
                        <button type="button" class="btn btn-block btn-success btn-outline btn_register"><?= lang('register'); ?></button>
                    </div>
                <?= form_close(); ?>
                <?php
                $providers = config_item('providers');
                foreach($providers as $key => $provider) {
                    if($provider['enabled']) {
                        echo '<div style="margin-top:10px;"><a href="'.site_url('social_auth/login/'.$key).'" class="btn btn-sm mt btn-default btn-block" title="'.lang('login_with').' '.$key.'">'.lang('login_with').' '.$key.'</a></div>';
                    }
                }
                ?>

            </div>
            <div class="modal-footer forgot-password" style="background-color: #d5d5d5;cursor: pointer;padding: 3% 0% 0% 0%;">
                <p style="text-align: center;font-size: 115%;color: #6e6f6f;"><?= lang('recover_password'); ?></p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).on('click', '.btn_register', function(){
        $('#loginModal').modal('hide');
        setTimeout(function() {
            $('#registerModal').modal('show');
        }, 850);
    });
    <?php if (isset($_GET['login']) && $_GET['login'] == 1): ?>
        $('#loginModal').modal('show');
    <?php endif ?>
    var lang = {};
    lang.page_info = '<?= lang('page_info'); ?>';
    lang.cart_empty = '<?= lang('empty_cart'); ?>';
    lang.item = '<?= lang('item'); ?>';
    lang.items = '<?= lang('items'); ?>';
    lang.unique = '<?= lang('unique'); ?>';
    lang.total_items = '<?= lang('total_items'); ?>';
    lang.total_unique_items = '<?= lang('total_unique_items'); ?>';
    lang.tax = '<?= lang('tax'); ?>';
    lang.shipping = '<?= lang('shipping'); ?>';
    lang.total_w_o_tax = '<?= lang('total_w_o_tax'); ?>';
    lang.product_tax = '<?= lang('product_tax'); ?>';
    lang.order_tax = '<?= lang('order_tax'); ?>';
    lang.total = '<?= lang('total'); ?>';
    lang.grand_total = '<?= lang('grand_total'); ?>';
    lang.reset_pw = '<?= lang('forgot_password?'); ?>';
    lang.type_email = '<?= lang('type_email_to_reset'); ?>';
    lang.submit = '<?= lang('submit'); ?>';
    lang.error = '<?= lang('error'); ?>';
    lang.add_address = '<?= lang('add_address'); ?>';
    lang.update_address = '<?= lang('update_address'); ?>';
    lang.fill_form = '<?= lang('fill_form'); ?>';
    lang.already_have_max_addresses = '<?= lang('already_have_max_addresses'); ?>';
    lang.send_email_title = '<?= lang('send_email_title'); ?>';
    lang.message_sent = '<?= lang('message_sent'); ?>';
    lang.add_to_cart = '<?= lang('add_to_cart'); ?>';
    lang.out_of_stock = '<?= lang('out_of_stock'); ?>';
    lang.x_product = '<?= lang('x_product'); ?>';
    lang.ok = '<?= lang('ok'); ?>';
    lang.cancel = '<?= lang('cancel'); ?>';
</script>
<?php if ($m == 'shop' && $v == 'product') { ?>
<script type="text/javascript">


$(document).ready(function ($) {
    
  $('.rrssb-buttons').rrssb({
    title: '<?= $product->name; ?>',
    url: '<?= site_url('product/'.$product->slug); ?>',
    image: '<?= base_url('assets/uploads/'.$product->image); ?>',
    description: $("meta[name='description']").attr('content'),
  });
  $("meta[name='shareUrl']").attr('content', '<?= site_url('product/'.$product->slug); ?>');
  $("meta[name='shareImage']").attr('content', '<?= base_url('assets/uploads/'.$product->image); ?>');
});
</script>
<?php } ?>
<script type="text/javascript">
<?php if ($message || $warning || $error || $reminder) { ?>
$(document).ready(function() {
    <?php if ($message) { ?>
        sa_alert('<?=lang('success');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($message))); ?>');
    <?php } if ($warning) { ?>
        sa_alert('<?=lang('warning');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($warning))); ?>', 'warning');
    <?php } if ($error) { ?>
        sa_alert('<?=lang('error');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($error))); ?>', 'error', 1);
    <?php } if ($reminder) { ?>
        sa_alert('<?=lang('reminder');?>', '<?= trim(str_replace(array("\r","\n","\r\n"), '', addslashes($reminder))); ?>', 'info');
    <?php } ?>
});
<?php } ?>
</script>

    <script type="text/javascript">


        jQuery.extend(jQuery.validator.messages, {
            required: "Este campo es obligatorio.",
            remote: "Por favor, rellena este campo.",
            email: "Por favor, escribe una dirección de correo válida",
            url: "Por favor, escribe una URL válida.",
            date: "Por favor, escribe una fecha válida.",
            dateISO: "Por favor, escribe una fecha (ISO) válida.",
            number: "Por favor, escribe un número entero válido.",
            digits: "Por favor, escribe sólo dígitos.",
            creditcard: "Por favor, escribe un número de tarjeta válido.",
            equalTo: "Por favor, escribe el mismo valor de nuevo.",
            accept: "Por favor, escribe un valor con una extensión aceptada.",
            maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
            minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
            rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
            range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
            max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
            min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "progressBar": true,
            "preventDuplicates": false,
            "positionClass": "toast-bottom-right",
            "onclick": null,
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "2000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        $(document).ready(function () {
            $("#register_user").validate({
                  ignore: []
            });
            $('select').not('.skip').select2();
            $('input[type="checkbox"],[type="radio"]').not('.skip').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
            $('#type_person').val('<?= NATURAL_PERSON; ?>').trigger('change');
            $('#country').trigger('change');

            setTimeout(function() {
                $('#carousel-example-generic').carousel({
                  interval: 4200
                });
            }, 850);


        });

        $('#state').on('change', function(){
          dpto = $('#state option:selected').data('code');
          $.ajax({
            url:"<?= admin_url() ?>auth/get_cities/"+dpto,
          }).done(function(data){
            $('#city').html(data);
            <?php if (!empty($this->Settings->customer_default_city)) { ?>
              $('#city').select2('val', '<?= $this->Settings->customer_default_city ?>');
              $('#city').trigger('change');
            <?php } ?>
          }).fail(function(data){
            console.log(data.responseText);
          });
        });

        $('#country').on('change', function(){
          dpto = $('#country option:selected').data('code');
          $.ajax({
            url:"<?= admin_url() ?>auth/get_states/"+dpto,
          }).done(function(data){
            $('#state').html(data);
            <?php if (!empty($this->Settings->customer_default_state)) { ?>
              $('#state').select2('val', '<?= $this->Settings->customer_default_state ?>');
              $('#state').trigger('change');
            <?php } ?>
          }).fail(function(data){
            console.log(data.responseText);
          });
        });

        $('#city').on('change', function(){
          code = $('#city option:selected').data('code');
          $('.postal_code').val(code);
          $('#city_code').val(code);
          $.ajax({
            url:"<?= admin_url().'auth/get_zones/' ?>"+code
          }).done(function(data){
            $('#zone').html(data);
          });

        });

        $('#zone').on('change', function(){
          code = $('#zone option:selected').data('code');
          $('.postal_code').val(code);

          $.ajax({
            url:"<?= admin_url().'auth/get_subzones/' ?>"+code
          }).done(function(data){
            $('#subzone').html(data);
          });

        });

        $('#subzone').on('change', function(){
          code = $('#subzone option:selected').data('code');
          $('.postal_code').val(code);
        });

        $('#vat_no').on('change', function(){
            nit = $(this).val();
              dvf = calcularDigitoVerificacion(nit);
              $('#digito_verificacion').val(dvf);
        });

        $(document).on('click', '.submit_register', function(){
            if ($('#register_user').valid()) {
                $('#register_user').submit();
            }
        });

        $(document).on('change', '#tipo_documento', function(){
            $('input[name="document_code"]').val($('#tipo_documento option:selected').data('code'));
        });

        $(document).on('keyup', '#email_address', function(){
            validate_user_exists();
        });

        $(document).on('keyup', '#username', function(){
            validate_user_exists();
        });

        $(document).on('change', '#password1, #confirm_password', function(){
            $('.submit_register').prop('disabled', true);
            password1 = $('#password1').val();
            confirm_password = $('#confirm_password').val();
            if (password1.length >= 6) {
                if (password1 != '' && confirm_password != '') {
                    if (password1 != confirm_password) {
                        command: toastr.error('<?= lang('passwords_dont_match') ?>', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                    } else {
                        command: toastr.success('<?= lang('passwords_match') ?>', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                        $('.submit_register').prop('disabled', false);
                    }
                }
            } else {
                command: toastr.error('<?= lang('passwords_min_length') ?>', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        });

        $(document).on('ifChecked', '.type_register_personal', function(){
            $('#type_person').select2('val', 2).select2('readonly', true).trigger('change');
            $('.legal_person').fadeOut();
            $('.legal_person').find('.form-control').prop('required', false);
            $('.div_data').fadeIn();
            $('#tipo_documento').select2('val', 3);
            $('#type_vat_regime').prop('required', false);
        });

        $(document).on('ifChecked', '.type_register_business', function(){
            $('#type_person').select2('val', '').select2('readonly', false);
            $('.legal_person').fadeIn();
            $('.legal_person').find('.form-control').prop('required', true);
            $('.div_data').fadeIn();
            $('#tipo_documento').select2('val', 0);
            $('#type_person').select2('open');
            $('#type_vat_regime').prop('required', true);
        });

        $(document).on('ifChecked', '#same_data_for_user', function(){
            first_name = $('input[name="first_name"]').val();
            first_lastname = $('input[name="first_lastname"]').val();
            company_email = $('input[name="company_email"]').val();
            company_phone = $('input[name="company_phone"]').val();

            $('input[name="user_first_name"]').val(first_name);
            $('input[name="user_first_lastname"]').val(first_lastname);
            $('input[name="email"]').val(company_email);
            $('input[name="phone"]').val(company_phone);
        });



        $(document).on('ifUnchecked', '#same_data_for_user', function(){
            $('input[name="user_first_name"]').val('');
            $('input[name="user_first_lastname"]').val('');
            $('input[name="email"]').val('');
            $('input[name="phone"]').val('');
        });

        // $(document).on('change', '#type_person', function() { show_hide_inputs_type_person($(this).val()); });

        function validate_user_exists(){
            $('.submit_register').prop('disabled', false);
            username = $('#username').val();
            email_address = $('#email_address').val();
            $.ajax({
                url : '<?= admin_url() ?>auth/verify_user_exists',
                type :'get',
                data : {
                    'username' : username,
                    'email' : email_address,
                }
            }).done(function(data){
                if (data) {
                    command: toastr.warning('Ya existe un usuario con el nombre de usuario o correo indicado.', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('.submit_register').prop('disabled', true);
                }
            });
        }

        

    $(document).on('change', '#type_person', function(){
        show_hide_inputs_type_person($(this).val());
    });

    function show_hide_inputs_type_person($type_person_id)
    {
      if ($type_person_id == '<?= NATURAL_PERSON; ?>')
      {
        $('#type_vat_regime').select2('val', '');
        $('#tipo_documento').select2('val', '');
        $('input[name="first_name"]').attr('required', true);
        $('input[name="first_lastname"]').attr('required', true);
        $('input[name="second_lastname"]').attr('required', true);
        $('input[name="name"]').attr('required', false);
        $('.natural_person').fadeIn();
        $('.juridical_person').fadeOut();
        if ($('input[name="type_register"]:checked').val() == 2) {
            $('.same_data').fadeIn();
        } else {
            $('.same_data').fadeOut();
        }
      }
      else if ($type_person_id == '<?= LEGAL_PERSON; ?>')
      {
        $('#type_vat_regime').select2('val', 2);
        $('#tipo_documento').select2('val', 6);
        $('input[name="first_name"]').attr('required', false);
        $('input[name="first_lastname"]').attr('required', false);
        $('input[name="second_lastname"]').attr('required', false);
        $('input[name="name"]').attr('required', true);
        $('.natural_person').fadeOut();
        $('.juridical_person').fadeIn();
        $('.same_data').fadeOut();
      }
    }

    $('#birth_month').on('change', function(){
      var month = $(this).val();
      var days_of_the_month = new Date("<?= date('Y') ?>", month, 0).getDate();
      var options_html = "";
      for (var i = 1; i <= days_of_the_month; i++) {
          options_html += "<option value='"+i+"'>"+i+"</option>";
      }
      $('#birth_day').html(options_html);
    });

    $('.set_session_biller').on('click', function(){
        biller_id = $(this).data('billerid');
        $.ajax({
            url:"<?= base_url('shop/set_session_biller/') ?>"+biller_id
        }).done(function(data){
            if (data == 1) {
                location.reload();
            }
        });
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "Este campo es obligatorio.",
        remote: "Por favor, rellena este campo.",
        email: "Por favor, escribe una dirección de correo válida",
        url: "Por favor, escribe una URL válida.",
        date: "Por favor, escribe una fecha válida.",
        dateISO: "Por favor, escribe una fecha (ISO) válida.",
        number: "Por favor, escribe un número entero válido.",
        digits: "Por favor, escribe sólo dígitos.",
        creditcard: "Por favor, escribe un número de tarjeta válido.",
        equalTo: "Por favor, escribe el mismo valor de nuevo.",
        accept: "Por favor, escribe un valor con una extensión aceptada.",
        maxlength: jQuery.validator.format("Por favor, no escribas más de {0} caracteres."),
        minlength: jQuery.validator.format("Por favor, no escribas menos de {0} caracteres."),
        rangelength: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
        range: jQuery.validator.format("Por favor, escribe un valor entre {0} y {1}."),
        max: jQuery.validator.format("Por favor, escribe un valor menor o igual a {0}."),
        min: jQuery.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
      });

    </script>

        <?php if ($message) {
            $message = str_replace("\n", "", $message);
        ?>
            <script type="text/javascript">
                Command: toastr.success('<?= strip_tags($message); ?>', '<?= lang("toast_success_title"); ?>', {onHidden : function(){}})
            </script>
        <?php } ?>

        <?php if ($error) {
            $error = str_replace("\n", "", $error);
        ?>
            <script type="text/javascript">
                Command: toastr.error('<?= strip_tags($error); ?>', '<?= lang("toast_error_title"); ?>', {onHidden : function(){}})
            </script>
        <?php } ?>

        <?php if ($warning) {
            $warning = str_replace("\n", "", $warning);
        ?>
            <script type="text/javascript">
                Command: toastr.warning('<?= strip_tags($warning); ?>', '<?= lang("toast_warning_title"); ?>', {onHidden : function(){}})
            </script>
        <?php } ?>


        <?php if (isset($_GET['search'])): ?>
            <script type="text/javascript">
                searchProducts();
            </script>
        <?php endif ?>
</body>
</html>
