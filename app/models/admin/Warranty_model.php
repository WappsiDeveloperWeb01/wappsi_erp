<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Warranty_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function findWarranty($data = [])
    {
        $this->db->select("
            product_warranty.*,
            p.name productName,
            p.code productCode,
            b.name billerName,
            s.reference_no referenceNoSale,
            c.name customerName,
            wr.description reason");
        if (!empty($data)) {
            $this->db->where($data);
        }
        $this->db->join("products p", "p.id = product_warranty.product_id");
        $this->db->join("companies b", "b.id = product_warranty.biller_id");
        $this->db->join("companies c", "c.id = product_warranty.customer_id");
        $this->db->join("sales s", "s.id = product_warranty.sale_id");
        $this->db->join("warranty_reason wr", "wr.id = product_warranty.warranty_reason");
        $q = $this->db->get("product_warranty");
        return $q->row();
    }

    public function findProducts($data)
    {
        $this->db->select("si.id, CONCAT(p.code, ' - ', p.name, ' - ', sales.reference_no, ' - ', {$this->db->dbprefix('sales')}.date) AS text, p.id AS value");
        $this->db->join("sale_items si", "si.sale_id = sales.id", "inner");
        $this->db->join("products p", "p.id = si.product_id", "inner");

        if (!empty($data["reference_no"])) {
            $this->db->where("sales.reference_no", $data["reference_no"]);
        }

        if (!empty($data["customer_id"])) {
            $this->db->where("sales.customer_id", $data["customer_id"]);
        }

        if (!empty($data["term"])) {
            $this->db->where(" (p.id LIKE '%{$data['term']}%'
            OR p.name LIKE '%{$data['term']}%'
            OR p.code LIKE '%{$data['term']}%') ");
        }

        $this->db->where("si.warranty_applied", NOT);

        $q = $this->db->get("sales");

        return $q->result();
    }

    public function findInvoices($data)
    {
        $this->db->select("reference_no AS id, reference_no AS text, reference_no AS value");

        if (!empty($data["customer_id"])) {
            $this->db->where("customer_id", $data["customer_id"]);
        }

        if (!empty($data["term"])) {
            $this->db->where(" (reference_no LIKE '%{$data['term']}%') ");
        }

        $q = $this->db->get("sales");

        return $q->result();
    }

    public function findActivities($data)
    {
        $this->db->select("product_warranty_activities.*, a.description AS descriptionActivity");
        $this->db->where($data);
        $this->db->join("warranty_activities a", "a.id = product_warranty_activities.warranty_activity_id");
        $q = $this->db->get("product_warranty_activities");
        return $q->result();
    }

    public function getDateSale($productId)
    {
        $this->db->select("CAST({$this->db->dbprefix("sales")}.date AS date) AS date, p.warranty_time");
        $this->db->join("sale_items si", "si.sale_id = sales.id");
        $this->db->join("products p", "p.id = si.product_id");
        $this->db->where("si.id", $productId);
        $q = $this->db->get("sales");
        return $q->row();
    }

    public function getWarrantyActivities($data = [])
    {
        if (!empty($data)) {
            $this->db->like($data);
        }
        $this->db->order_by('description');
        $q = $this->db->get("warranty_activities");
        return $q->result();
    }

    public function findWarrantyReason($data = [])
    {
        $this->db->order_by('description');
        $q = $this->db->get("warranty_reason");
        return $q->result();
    }

    public function getAllCustomerQualifications()
    {
        $q = $this->db->get("customer_qualification");
        return $q->result();
    }

    public function getAllCustomerAttitude()
    {
        $q = $this->db->get("customer_attitude");
        return $q->result();
    }

    public function create($data)
    {
        if ($this->db->insert("product_warranty", $data)) {
            $warrantyId = $this->db->insert_id();
            return $warrantyId;
        }
        return false;
    }

    public function createActivity($data)
    {
        if ($this->db->insert("product_warranty_activities", $data)) {
            return $this->db->insert_id();
        }
        return false;
    }

    public function update($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("product_warranty", $data)) {
            return true;
        }
        return false;
    }
}