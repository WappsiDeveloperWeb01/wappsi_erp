<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg no-printable">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: -2px">
                <i class="fa fa-lg">&times;</i>
            </button>
            <h2 class="modal-title" id="myModalLabel"><?= lang("installment_edit") ?></h2>
        </div>
        <div class="modal-body">
                <div class="row">
                    <div class="col-sm-3 text-center"><h4><?= lang("credit_branch") ?></h4><?= $installment->billerName ?></div>
                    <div class="col-sm-3 text-center"><h4><?= lang("credit") ?></h4><?= $installment->creditNo ?></div>
                    <div class="col-sm-3 text-center"><h4><?= lang("quota_number") ?></h4><?= $installment->installmentNo ?></div>
                    <div class="col-sm-3 text-center"><h4><?= lang("customer") ?></h4><?= ucwords( strtolower($installment->customerName)) ?></div>
                </div>
                <hr>
                <?= admin_form_open("financing/installmentUpdate", ["id" => "installmentEditForm"]); ?>
                    <?= form_hidden('installmentId', $installment->installmentId) ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="form-group">
                                <label><?= lang("capital") ?></label>
                                <input class="form-control number_mask" type="text" name="capital_amount" id="capital_amount" value="<?= $installment->capitalAmount ?>" />
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label><?= lang("current_interest") ?></label>
                                <input class="form-control number_mask" type="text" name="current_interest" id="current_interest" value="<?= $installment->currentInterestAmount ?>" />
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label><?= lang("vat_current_interest") ?></label>
                                <input class="form-control number_mask" type="text" name="vat_current_interest" id="vat_current_interest" value="<?= $installment->interestTax ?>" readonly/>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="form-group">
                                <label><?= lang("value_to_apply") ?></label>
                                <input class="form-control number_mask" type="text" name="value_to_apply" id="value_to_apply" value="<?= $installment->installmentAmount ?>" readonly/>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2 pull-right">
                            <div class="new-button-container pull-right">
                                <button class="btn btn-primary new-button" type="button" id="updateButton" data-toggle="tooltip" data-placement="bottom" title="Actualizar"><i class="fas fa-check fa-lg"></i></button>
                            </div>
                        </div>
                    </div>
                <?= form_close() ?>
                <!-- <br>
                <pre>
                    <?php var_dump($installment) ?>
                </pre> -->
        </div>

    </div>
</div>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        set_number_mask()

        $(document).on('change', '#capital_amount', function() { calculateValueToApply() });
        $(document).on('change', '#current_interest', function() { calculateVATInterestAJAX($(this), 'current') });
        $(document).on('click', '#updateButton', function() { confirmUpdate() });
    });

    function calculateValueToApply()
    {
        let capital_amount = $('#capital_amount').val()
        let current_interest = $('#current_interest').val()
        let vat_current_interest = $('#vat_current_interest').val()

        valueToApply = parseFloat(capital_amount) + parseFloat(current_interest) + parseFloat(vat_current_interest)

        $('#value_to_apply').val(valueToApply)
    }

    function calculateVATInterestAJAX(element, type) {
        $.ajax({
            type: "post",
            url: site.base_url + 'financing/calculateVATInterestAJAX',
            data: {
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                'baseValue' : element.val(),
                'type' : type
            },
            dataType: 'json',
            success: function (response) {
                response
                if (type == 'default') {
                    $('#vat_current_interest').val(response.interestTax)
                } else {
                    $('#vat_current_interest').val(response.interestTax)
                }

                calculateValueToApply()
            }
        })
    }

    function confirmUpdate() {
        swal({
            title: "Actualización de Datos",
            text: "¿Está seguro de actualizar los datos?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function(result) {
            if (result) {
                $('#installmentEditForm').submit()
            }
        });
    }
</script>
