<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_total_qty_alerts() {
        $warehouse_id = $biller_id = NULL;
        if ($this->session->userdata('biller_id')) {
            $biller_id = $this->session->userdata('biller_id');
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }
        if ($this->session->userdata('warehouse_id')) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
            $this->db
                ->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
            $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
        }

        if ($warehouse_id) {
            $q = $this->db
                ->select('products.image as image, products.code, products.name, warehouses_products.quantity, alert_quantity')
                ->from('products')->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
                ->where('warehouse_id', $warehouse_id)
                ->where('track_quantity', 1)
                ->where('alert_quantity > 0')
                ->where('alert_quantity > warehouses_products.quantity')
                ->order_by('products.code desc')->get();
        } else {
            $q = $this->db
                ->select('image, code, name, quantity, alert_quantity')
                ->from('products')
                ->where('alert_quantity > quantity')
                ->where('track_quantity', 1)
                ->order_by('code desc')->get();
        }

        return $q->num_rows();
    }

    public function get_expiring_qty_alerts() {
        $date = date('Y-m-d', strtotime('+3 months'));
        $this->db->select('SUM(quantity_balance) as alert_num')
        ->where('expiry !=', NULL)->where('expiry !=', '0000-00-00')
        ->where('expiry <', $date);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $res = $q->row();
            return (INT) $res->alert_num;
        }
        return FALSE;
    }

    public function get_shop_sale_alerts() {
        $this->db->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
        ->where('sales.shop', 1)->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
        ->group_start()->where('deliveries.status !=', 'delivered')->or_where('deliveries.status IS NULL', NULL)->group_end();
        return $this->db->count_all_results('sales');
    }

    public function get_shop_payment_alerts() {
        $this->db->where('shop', 1)->where('attachment !=', NULL)->where('payment_status !=', 'paid');
        return $this->db->count_all_results('sales');
    }

    public function get_setting() {
        $this->db->join("countries", "countries.NOMBRE = settings.pais", "INNER");
        $q = $this->db->get('settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_pos_setting() {
        $q = $this->db->get('pos_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getShopSettings() {
        $q = $this->db->get('shop_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getDateFormat($id) {
        $q = $this->db->get_where('date_format', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCompanies($group_name = NULL) {
        if ($group_name) {
            $q = $this->db->get_where('companies', array('group_name' => $group_name));
        } else {
            $q = $this->db->get('companies');
        }
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllCompaniesWithState($group_name, $company_id = NULL, $status = true) {
        if ($group_name == 'biller') {
            $condition['companies.group_name'] = $group_name;
            if ($status) {
                $condition['companies.status'] = 1;
            }
            if ($company_id) {
                $condition['companies.id'] = $company_id;
            }
            $q = $this->db->select('companies.id,
                               companies.group_id,
                               companies.group_name,
                               companies.customer_group_id,
                               companies.customer_group_name,
                               companies.type_person,
                               companies.name,
                               companies.first_name,
                               companies.second_name,
                               companies.first_lastname,
                               companies.second_lastname,
                               IF('.$this->db->dbprefix('companies').'.company != "", '.$this->db->dbprefix('companies').'.company, '.$this->db->dbprefix('companies').'.name) company,
                               companies.tipo_documento,
                               companies.vat_no,
                               companies.digito_verificacion,
                               companies.address,
                               companies.city,
                               companies.state,
                               companies.postal_code,
                               companies.country,
                               companies.phone,
                               companies.email,
                               companies.cf1,
                               companies.cf2,
                               companies.cf3,
                               companies.cf4,
                               companies.cf5,
                               companies.cf6,
                               companies.invoice_footer,
                               companies.payment_term,
                               companies.logo,
                               companies.logo_square,
                               companies.award_points,
                               companies.deposit_amount,
                               companies.price_group_id,
                               companies.price_group_name,
                               companies.id_partner,
                               companies.tipo_regimen,
                               companies.city_code,
                               biller_data.default_price_group,
                               biller_data.default_warehouse_id,
                               biller_data.default_customer_id,
                               biller_data.default_seller_id,
                               biller_data.default_affiliate_id,
                               biller_data.pos_document_type_default,
                               biller_data.pos_document_type_default_fe,
                               biller_data.detal_document_type_default,
                               biller_data.key_log,
                               biller_data.pin_code,
                               biller_data.default_pos_section,
                               biller_data.rete_autoica_percentage,
                               biller_data.rete_autoica_account,
                               biller_data.rete_autoica_account_counterpart,
                               biller_data.rete_bomberil_percentage,
                               biller_data.rete_bomberil_account,
                               biller_data.rete_bomberil_account_counterpart,
                               biller_data.rete_autoaviso_percentage,
                               biller_data.rete_autoaviso_account,
                               biller_data.rete_autoaviso_account_counterpart,
                               biller_data.cash_payment_method_account,
                               biller_data.pin_code_request,
                               biller_data.pin_code_method,
                               biller_data.random_pin_code,
                               biller_data.random_pin_code_date,
                               biller_data.min_sale_amount,
                               biller_data.warehouses_related,
                               biller_data.default_credit_payment_method,
                               companies.status')
                     ->join('biller_data', 'biller_data.biller_id = companies.id', 'left')
                     ->where($condition)
                     ->get('companies');
        } else {
            $condition['group_name'] = $group_name;
            if ($status) {
                $condition['companies.status'] = 1;
            }

            if ($company_id) {
                $condition['id'] = $company_id;
            }
            $q = $this->db->get_where('companies', $condition);
        }
        if ($company_id) {
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        } else {
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
        }
        return FALSE;
    }

    public function getSellerByBiller($biller_id)
    {
        $q = $this->db->select('
                                biller_seller.id as seller_id,
                                companies.id as companies_id,
                                companies.name as name,
                                companies.vat_no as vat_no
                                ')
                      ->join('companies', 'companies.id = biller_seller.companies_id')
                      ->where('companies.status', 1)
                      ->where('companies.group_name', 'seller')
                      ->where('biller_seller.biller_id', $biller_id)
                      ->get('biller_seller');

        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getAffiliateByBiller($biller_id)
    {
        $q = $this->db->select('
                                biller_seller.id as seller_id,
                                companies.id as companies_id,
                                companies.name as name,
                                companies.vat_no as vat_no
                                ')
                      ->join('companies', 'companies.id = biller_seller.companies_id')
                      ->where('companies.status', 1)
                      ->where('companies.group_name', 'affiliate')
                      ->where('biller_seller.biller_id', $biller_id)
                      ->get('biller_seller');

        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getSellerById($seller_id)
    {

        $q = $this->db->where('id', $seller_id)->get('companies');

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getCompanyByID($id)
    {
        $this->db->select("companies.*, countries.*, states.*, cities.CODIGO AS codigo_municipio, types_vat_regime.codigo AS regime_code_fe, (CASE WHEN company = '' THEN name ELSE CONCAT(company, ' (', name, ')') END) as company_text");
        $this->db->from('companies');
        $this->db->join('cities', 'cities.CODIGO = companies.city_code', 'left');
        $this->db->join('states', 'states.CODDEPARTAMENTO = cities.CODDEPARTAMENTO', "left");
        $this->db->join("countries", "sma_countries.CODIGO = sma_states.PAIS", "left");
        $this->db->join("types_vat_regime", "types_vat_regime.id = companies.tipo_regimen", "left");
        $this->db->where('companies.id', $id);
        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return FALSE;
    }

    public function getCustomerGroupByID($id)
    {
        $q = $this->db->get_where('customer_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllGroups()
    {
        $q = $this->db->get_where('groups');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getUser($id = NULL)
    {
        if (!$id) {
            $id = $this->session->userdata('user_id');
        }

        $q = $this->db->select('users.*, groups.name as group_name')
                ->join('groups', 'groups.id = users.group_id', 'left')
                ->where('users.id', $id)
                ->get('users');

        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductByID($id)
    {
        $q = $this->db->get_where('products', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllProducts($view_discontinued = false, $biller_id = NULL)
    {
        $biller_id = $this->session->userdata('biller_id') ? $this->session->userdata('biller_id') : $biller_id;
        if ($biller_id) {
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }
        if (!$view_discontinued) {
            $this->db->where('discontinued', 0);
        }
        if ($biller_id) {
            $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
        }
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return FALSE;
    }

    public function getAllCurrencies()
    {
        $q = $this->db->get('currencies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getCurrencyByCode($code)
    {
        $q = $this->db->get_where('currencies', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllTaxRates()
    {
        $q = $this->db->get('tax_rates');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->id] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getTaxRateByID($id)
    {
        $q = $this->db->get_where('tax_rates', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getTaxRate2ByID($id)
    {
        $q = $this->db->get_where('tax_secondary', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllWarehouses($type = null, $only_active = true)
    {
        $conditions = [];
        if($only_active){
            $conditions['status'] = 1;
        }
        if ($type) {
            $conditions['type'] = $type;
        }
        if (count($conditions) > 0) {
            $this->db->where($conditions);
        }
        $q = $this->db->get('warehouses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getWarehouseByID($id)
    {
        $q = $this->db->get_where('warehouses', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllCategories()
    {
        $this->db->where('parent_id', NULL)->or_where('parent_id', 0)->order_by('name');
        $q = $this->db->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubCategories($parent_id = NULL, $not_this_parent = NULL, $only_sub = false)
    {
        if ($only_sub) {
            $this->db->where('(parent_id IS NOT NULL AND parent_id != 0)');
        }
        if ($parent_id) {
            $this->db->where('parent_id', $parent_id)->where('(subcategory_id IS NULL OR subcategory_id = 0)');
        }
        if ($not_this_parent) {
            $this->db->where('id !=', $not_this_parent);
        }
        $q = $this->db->order_by('name')->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getSubCategoriesSecondLevel($parent_id = NULL, $not_this_parent = NULL, $only_sub = false)
    {
        if ($parent_id) {
            $this->db->where('subcategory_id', $parent_id);
        }
        if ($not_this_parent) {
            $this->db->where('id !=', $not_this_parent);
        }
        $q = $this->db->order_by('name')->get("categories");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPromotions($biller_id = NULL, $warehouse_id = NULL)
    {
        if ((!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) || $biller_id) {
            if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
                $biller_id = $this->session->userdata('biller_id');
            }
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }

        $this->db->select('products.*
                            '.($warehouse_id ? ', WP.quantity AS quantity' : '').'')
                 ->where('promotion >', 0)
                 ->where('end_date >=', date('Y-m-d'));
        if ($warehouse_id) {
            $this->db->join('warehouses_products WP', 'WP.product_id = products.id AND WP.warehouse_id = '.$warehouse_id, 'inner');
            if ($this->Settings->overselling == 0) {
                $this->db->where('WP.quantity >', 0);
            }
        }
        if ($biller_id) {
            $this->db->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
            $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
        }
        $q = $this->db->limit($this->pos_settings->pro_limit)->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return FALSE;
    }

    public function getFavorites($biller_id = NULL, $warehouse_id = NULL)
    {
        if ((!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) || $biller_id) {
            if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
                $biller_id = $this->session->userdata('biller_id');
            }
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }

        $this->db->select('P.*
                            '.($warehouse_id ? ', WP.quantity AS quantity' : '').'');
        $this->db->from("(SELECT
                            COUNT(SI.product_id), SI.product_id
                        FROM
                            {$this->db->dbprefix('sale_items')} SI
                        INNER JOIN {$this->db->dbprefix('sales')} S ON S.id = SI.sale_id
                        WHERE
                            DATEDIFF('".date('Y-m-d')."', S.date) <= 30
                        GROUP BY SI.product_id
                        ORDER BY COUNT(SI.product_id) DESC
                        LIMIT ".$this->pos_settings->pro_limit.") AS best_seller");
        $this->db->join('products P', 'P.id = best_seller.product_id', 'inner');
        if ($warehouse_id) {
            $this->db->join('warehouses_products WP', 'WP.product_id = P.id AND WP.warehouse_id = '.$warehouse_id, 'inner');
            if ($this->Settings->overselling == 0) {
                $this->db->where('WP.quantity >', 0);
            }
        }
        $this->db->where('P.discontinued', 0);
        if ($biller_id) {
            $this->db->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
            $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = P.id', 'inner');
        }
        $q = $this->db->limit($this->pos_settings->pro_limit)->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return FALSE;
    }

    public function getFeatured($biller_id = NULL, $warehouse_id = NULL)
    {
        if ((!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) || $biller_id) {
            if (!$this->Owner && !$this->Admin && $this->session->userdata('biller_id')) {
                $biller_id = $this->session->userdata('biller_id');
            }
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }

        $this->db->select('products.*
                            '.($warehouse_id ? ', WP.quantity AS quantity' : '').'');
        $this->db->where($this->db->dbprefix('products').'.featured', 1);
        $this->db->where($this->db->dbprefix('products').'.discontinued', 0);
        if ($warehouse_id) {
            $this->db->join('warehouses_products WP', 'WP.product_id = products.id AND WP.warehouse_id = '.$warehouse_id, 'inner');
            if ($this->Settings->overselling == 0) {
                $this->db->where('WP.quantity >', 0);
            }
        }
        if ($biller_id) {
            $this->db->join('biller_data', 'biller_data.biller_id IN ('.$biller_id.')', 'left');
            $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = '.$this->db->dbprefix('products').'.id', 'inner');
        }
        $q = $this->db->order_by('name', 'asc')->limit($this->pos_settings->pro_limit)->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return FALSE;
    }

    public function getCategoryByID($id)
    {
        $q = $this->db->get_where('categories', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGiftCardByID($id)
    {
        $q = $this->db->get_where('gift_cards', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getGiftCardByNO($no)
    {
        $q = $this->db->get_where('gift_cards', array('card_no' => $no), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateInvoiceStatus()
    {
        $date = date('Y-m-d');
        $q = $this->db->get_where('invoices', array('status' => 'unpaid'));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->due_date < $date) {
                    $this->db->update('invoices', array('status' => 'due'), array('id' => $row->id));
                }
            }
            $this->db->update('settings', array('update' => $date), array('setting_id' => '1'));
            return true;
        }
    }

    public function modal_js()
    {
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'js/modal.js') . '</script>';
    }

    public function documents_types_js()
    {
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'js/documents_types.js') . '</script>';
    }

    public function jasny_js()
    {
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'inspinia/js/plugins/jasny/jasny-bootstrap.min.js') . '</script>';
    }

    public function customers_js()
    {
        return '<script type="text/javascript">' . file_get_contents($this->data['assets'] . 'inspinia/js/plugins/jasny/jasny-bootstrap.min.js') . '</script>';
    }

    public function getReference($field, $without_number = false)
    {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            switch ($field) {
                // case 'so':
                //     $prefix = $this->Settings->sales_prefix;
                //     break;
                // case 'pos':
                //     $prefix = isset($this->Settings->sales_prefix) ? $this->Settings->sales_prefix . '/POS' : '';
                //     break;
                case 'qu':
                    $prefix = $this->Settings->quote_prefix;
                    break;
                case 'qup':
                    $prefix = $this->Settings->quote_purchase_prefix;
                    break;
                case 'os':
                    $prefix = $this->Settings->order_sale_prefix;
                    break;
                case 'po':
                    $prefix = $this->Settings->purchase_prefix;
                    break;
                case 'to':
                    $prefix = $this->Settings->transfer_prefix;
                    break;
                case 'do':
                    $prefix = $this->Settings->delivery_prefix;
                    break;
                case 'pay':
                    $prefix = $this->Settings->payment_prefix;
                    break;
                // case 'rc':
                //     $prefix = $this->Settings->sma_payment_prefix;
                //     break;
                case 'ppay':
                    $prefix = $this->Settings->ppayment_prefix;
                    break;
                case 'ex':
                    $prefix = $this->Settings->expense_prefix;
                    break;
                case 're':
                    $prefix = $this->Settings->return_prefix;
                    break;
                case 'rep':
                    $prefix = $this->Settings->returnp_prefix;
                    break;
                case 'qa':
                    $prefix = $this->Settings->qa_prefix;
                    break;
                case 'dp':
                    $prefix = $this->Settings->deposit_prefix;
                    break;
                default:
                    $prefix = '';
            }

            $ref_no = $prefix;

            if ($without_number) {
                return $ref_no;
            }

            if ($prefix != '') {
                if ($this->Settings->reference_format == 1) {
                    $ref_no .= date('Y') . "/" . sprintf("%04s", $ref->{$field});
                } elseif ($this->Settings->reference_format == 2) {
                    $ref_no .= date('Y') . "/" . date('m') . "/" . sprintf("%04s", $ref->{$field});
                } elseif ($this->Settings->reference_format == 3) {
                    // $ref_no .= sprintf("%04s", $ref->{$field});
                    // var_dump($ref);
                    $ref_no .= "-".$ref->{$field};
                } else {
                    $ref_no .= $this->getRandomReference();
                }
            } else {
                return FALSE;
            }

            return $ref_no;
        }
        return FALSE;
    }

    //Wappsi función para obtener el consecutivo de la sucursal que hace la venta
    public function getReferenceBiller($biller_id, $document_type_id)
    {
        $aux = '';
        $ref_no = '';
        $text = '';
        $consecutivo = 0;
        if($biller_id != ''){
            $q = $this->db->where('id', $document_type_id)->get('documents_types');
            if ($q->num_rows() > 0) {
                $q = $q->row();
                $sales_prefix = $q->sales_prefix;
                $consecutive = $q->sales_consecutive;
                $fin_resolucion = $q->fin_resolucion;
                if ($q->add_consecutive_left_zeros == 1) {
                    $consecutive = $this->sma->zero_left_consecutive($consecutive, $fin_resolucion);
                }
                $hyphen = !empty($sales_prefix) ? "-" : "";
                $ref_no = $sales_prefix.$hyphen.$consecutive;
                return $ref_no;
            }
            else{
                return FALSE;
            }
        }else{
            return FALSE;
        }
    }// Termina la función Wappsi para optener el consecutivo de la sucursal que hace la venta

    //Se pone aparte la actualización de consecutivo, para no actualizarlo sólo por que se consultó.
    //Sólo se actualiza cuando se registra pago para la venta, pero cuando la misma se suspende no se debe actualizar.
    public function updateBillerConsecutive($document_type_id, $consecutive)
    {
        // $update =$this->db->update('companies', array('cf1' => $consecutive), array('id' => $biller_id));
        $update =$this->db->update('documents_types', array('sales_consecutive' => $consecutive), array('id' => $document_type_id));
        if ($update) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getRandomReference($len = 12)
    {
        $result = '';
        for ($i = 0; $i < $len; $i++) {
            $result .= mt_rand(0, 9);
        }

        if ($this->getSaleByReference($result)) {
            $this->getRandomReference();
        }

        return $result;
    }

    public function getSaleByReference($ref) {
        $this->db->like('reference_no', $ref, 'both');
        $q = $this->db->get('sales', 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function updateReference($field) {
        $q = $this->db->get_where('order_ref', array('ref_id' => '1'), 1);
        if ($q->num_rows() > 0) {
            $ref = $q->row();
            $this->db->update('order_ref', array($field => $ref->{$field} + 1), array('ref_id' => '1'));
            return $ref->{$field} + 1;
        }
        return FALSE;
    }

    public function checkPermissions() {
        $q = $this->db->get_where('permissions', array('group_id' => $this->session->userdata('group_id')), 1);
        if ($q->num_rows() > 0) {
            return $q->result_array();
        }
        return FALSE;
    }

    public function getNotifications() {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where("from_date <=", $date);
        $this->db->where("till_date >=", $date);
        if (!$this->Owner) {
            if ($this->Supplier) {
                $this->db->where('scope', 4);
            } elseif ($this->Customer) {
                $this->db->where('scope', 1)->or_where('scope', 3);
            } elseif (!$this->Customer && !$this->Supplier) {
                $this->db->where('scope', 2)->or_where('scope', 3);
            }
        }
        $q = $this->db->get("notifications");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getUpcomingEvents() {
        $dt = date('Y-m-d');
        $this->db->where('start >=', $dt)->order_by('start')->limit(5);
        if ($this->Settings->restrict_calendar) {
            $this->db->where('user_id', $this->session->userdata('user_id'));
        }

        $q = $this->db->get('calendar');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUserGroup($user_id = false) {
        if (!$user_id) {
            $user_id = $this->session->userdata('user_id');
        }
        $group_id = $this->getUserGroupID($user_id);
        $q = $this->db->get_where('groups', array('id' => $group_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getUserGroupID($user_id = false) {
        $user = $this->getUser($user_id);
        // exit(var_dump($user_id));
        // $this->sma->print_arrays($user);
        return $user->group_id;
    }

    public function getWarehouseProductsVariants($option_id, $warehouse_id = NULL, $negative = NULL) {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        if ($negative) {
            $this->db->where('quantity <', '0');
        }
        $q = $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getPurchasedItem($clause) {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->order_by('date', $orderby);
        $this->db->order_by('purchase_id', $orderby);
        if (!isset($clause['option_id']) || empty($clause['option_id'])) {
            $this->db->group_start()->where('option_id', NULL)->or_where('option_id', 0)->group_end();
        }
        $q = $this->db->get_where('purchase_items', $clause);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function setPurchaseItem($clause, $qty) {
        if ($product = $this->getProductByID($clause['product_id'])) {
            if ($pi = $this->getPurchasedItem($clause)) {
                $quantity_balance = $pi->quantity_balance+$qty;
                return $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
            } else {
                $unit = $this->getUnitByID($product->unit);
                $clause['product_unit_id'] = $product->unit;
                $clause['product_unit_code'] = (empty($unit->code) ? 1 : $unit->code);
                $clause['product_code'] = $product->code;
                $clause['product_name'] = $product->name;
                $clause['purchase_id'] = $clause['transfer_id'] = $clause['item_tax'] = NULL;
                $clause['net_unit_cost'] = $clause['real_unit_cost'] = $clause['unit_cost'] = $product->cost;
                $clause['quantity_balance'] = $clause['quantity'] = $clause['unit_quantity'] = $clause['quantity_received'] = $qty;
                $clause['subtotal'] = ($product->cost * $qty);
                if (isset($product->tax_rate) && $product->tax_rate != 0) {
                    $tax_details = $this->site->getTaxRateByID($product->tax_rate);
                    $ctax = $this->calculateTax($product, $tax_details, $product->cost);
                    $item_tax = $clause['item_tax'] = $ctax['amount'];
                    $tax = $clause['tax'] = $ctax['tax'];
                    $clause['tax_rate_id'] = $tax_details->id;
                    if ($product->tax_method != 1) {
                        $clause['net_unit_cost'] = $product->cost - $item_tax;
                        $clause['unit_cost'] = $product->cost;
                    } else {
                        $clause['net_unit_cost'] = $product->cost;
                        $clause['unit_cost'] = $product->cost + $item_tax;
                    }
                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $clause['unit_quantity']);
                    if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($this->Settings->state == $supplier_details->state), $tax_details)) {
                        $clause['gst'] = $gst_data['gst'];
                        $clause['cgst'] = $gst_data['cgst'];
                        $clause['sgst'] = $gst_data['sgst'];
                        $clause['igst'] = $gst_data['igst'];
                    }
                    $clause['subtotal'] = (($clause['net_unit_cost'] * $clause['unit_quantity']) + $pr_item_tax);
                }
                $clause['status'] = 'received';
                $clause['date'] = date('Y-m-d');
                $clause['option_id'] = !empty($clause['option_id']) && is_numeric($clause['option_id']) ? $clause['option_id'] : NULL;
                return $this->db->insert('purchase_items', $clause);
            }
        }
        return FALSE;
    }

    public function syncVariantQty($variant_id, $warehouse_id, $product_id = NULL) {
        $balance_qty = $this->getBalanceVariantQuantity($variant_id);
        $wh_balance_qty = $this->getBalanceVariantQuantity($variant_id, $warehouse_id);
        if ($this->db->update('product_variants', array('quantity' => $balance_qty), array('id' => $variant_id))) {
            if ($this->getWarehouseProductsVariants($variant_id, $warehouse_id)) {
                $this->db->update('warehouses_products_variants', array('quantity' => $wh_balance_qty, 'last_update' => date('Y-m-d H:i:s')), array('option_id' => $variant_id, 'warehouse_id' => $warehouse_id));
            } else {
                if($wh_balance_qty) {
                    $this->db->insert('warehouses_products_variants', array('quantity' => $wh_balance_qty, 'option_id' => $variant_id, 'warehouse_id' => $warehouse_id, 'product_id' => $product_id, 'last_update' => date('Y-m-d H:i:s')));
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getWarehouseProducts($product_id, $warehouse_id = NULL, $negative = NULL) {
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        if ($negative) {
            $this->db->where('quantity <', '0');
        }
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncProductQty($product_id, $warehouse_id, $variants = FALSE) {
        $balance_qty = $this->getBalanceQuantity($product_id, NULL, $variants);
        $wh_balance_qty = $this->getBalanceQuantity($product_id, $warehouse_id, $variants);
        if ($this->db->update('products', array('quantity' => $balance_qty), array('id' => $product_id))) {
            if ($this->getWarehouseProducts($product_id, $warehouse_id)) {
                $this->db->update('warehouses_products', array('quantity' => $wh_balance_qty, 'last_update' => date('Y-m-d H:i:s')), array('product_id' => $product_id, 'warehouse_id' => $warehouse_id));
            } else {
                if( ! $wh_balance_qty) { $wh_balance_qty = 0; }
                $product = $this->site->getProductByID($product_id);
                if ($product != FALSE) {
                    $this->db->insert('warehouses_products', array('quantity' => $wh_balance_qty, 'product_id' => $product_id, 'warehouse_id' => $warehouse_id, 'avg_cost' => ($product->avg_cost ? $product->avg_cost : 0), 'last_update' => date('Y-m-d H:i:s')));
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getSaleByID($id) {
        $q = $this->db->get_where('sales', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getSalePayments($sale_id) {
        $q = $this->db->order_by('date asc')->get_where('payments', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncSalePayments($id) {
        $sale = $this->getSaleByID($id);
        if ($payments = $this->getSalePayments($id)) {
            $paid = 0;
            $grand_total = $sale->grand_total+$sale->rounding;
            $last_date = NULL;
            foreach ($payments as $payment) {
                $paid += $payment->amount;
                $last_date = $payment->date;
            }
            $payment_status = $paid == 0 ? 'pending' : $sale->payment_status;
            if ($grand_total == $this->sma->formatDecimal($paid)) {
                $payment_status = 'paid';
            } elseif ($paid == 0 && $sale->due_date != "" && $sale->due_date <= date('Y-m-d') && !$sale->sale_id) {
                $payment_status = 'pending';
            } elseif ($paid != 0) {
                $payment_status = 'partial';
            }

            if ($paid > 0 && ($grand_total - $paid) < 0.1) {
                $pmnt = $this->db->get_where('payments', ['sale_id' => $id,
                                                    'paid_by != ' => 'discount',
                                                    'paid_by != ' => 'retencion',
                                                    'paid_by != ' => 'return',
                                                    ], 1);
                if ($pmnt->num_rows() > 0) {
                    $pmnt = $pmnt->row();
                    $this->db->update('payments', ['amount' => $pmnt->amount+($grand_total - $paid)], ['id'=>$pmnt->id]);
                }
            }
            if ($payment_status != 'paid') {
                $last_date = NULL;
            }
            if ($this->db->update('sales', array('paid' => $paid, 'payment_status' => $payment_status, 'portfolio_end_date' => $last_date), array('id' => $id))) {
                return true;
            }
        } else {
            $payment_status = ($sale->due_date <= date('Y-m-d')) ? 'due' : 'pending';
            if ($this->db->update('sales', array('paid' => 0, 'payment_status' => $payment_status), array('id' => $id))) {
                return true;
            }
        }

        return FALSE;
    }

    public function getPurchaseByID($id) {
        $q = $this->db->get_where('purchases', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPurchasePayments($purchase_id) {
        $q = $this->db->get_where('payments', array('purchase_id' => $purchase_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchasePayments($id) {
        $purchase = $this->getPurchaseByID($id);
        $paid = 0;
        if ($payments = $this->getPurchasePayments($id)) {
            foreach ($payments as $payment) {
                $paid += $payment->amount;
            }
        }

        $payment_status = $paid <= 0 ? 'pending' : $purchase->payment_status;
        if ($this->sma->formatDecimal($purchase->grand_total) > $this->sma->formatDecimal($paid) && $paid > 0) {
            $payment_status = 'partial';
        } elseif ($this->sma->formatDecimal($purchase->grand_total) <= $this->sma->formatDecimal($paid)) {
            $payment_status = 'paid';
        }

        if ($this->db->update('purchases', array('paid' => $paid, 'payment_status' => $payment_status), array('id' => $id))) {
            return true;
        }

        return FALSE;
    }

    private function getBalanceQuantity($product_id, $warehouse_id = NULL, $variants = FALSE) {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('product_id', $product_id)
                 ->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        if ($variants) {
            $this->db->where('option_id IS NOT NULL');
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    private function getBalanceVariantQuantity($variant_id, $warehouse_id = NULL) {
        $this->db->select('SUM(COALESCE(quantity_balance, 0)) as stock', False);
        $this->db->where('option_id', $variant_id)->where('quantity_balance !=', 0);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $this->db->group_start()->where('status', 'received')->or_where('status', 'partial')->group_end();
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            $data = $q->row();
            return $data->stock;
        }
        return 0;
    }

    public function calculateAVCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity, $sale_id, $recontabilizacion, $preparation_id_origin = false, $sale_status = 'completed') {
        $product = $this->getProductByID($product_id);
        $real_item_qty = $quantity;
        $wp_details = $this->getWarehouseProduct($warehouse_id, $product_id);
        if ($recontabilizacion) {
            $purchase_unit_cost = $this->getSaleItemCosting($product_id, $sale_id);
            $tax_rate = $this->getTaxRateByID($product->tax_rate);
            $ctax = $this->calculateTax($product, $tax_rate, $purchase_unit_cost);
            $avg_net_unit_cost = $purchase_unit_cost - $ctax['amount'];
            $avg_unit_cost = $purchase_unit_cost;
        } else {
            if ($wp_details && $wp_details->avg_cost > 0) {
                $con = $wp_details->avg_cost;
            } else {
                $con = $product->cost;
            }
            $tax_rate = $this->getTaxRateByID($product->tax_rate);
            $ctax = $this->calculateTax($product, $tax_rate, $con);
            $avg_unit_cost = $con;
            $avg_net_unit_cost  = ($con - $ctax['amount']);
        }
        if ($pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id)) {
            $quantity = $item_quantity;
            $balance_qty = $quantity;
            $txt = "";
            foreach ($pis as $pi) {
                $cost_row = [];
                $txt.=",  ".$balance_qty;
                if (!empty($pi) && $pi->quantity_balance > 0 && $balance_qty <= $quantity && $quantity != 0) {
                    if ($pi->quantity_balance >= $quantity && $quantity != 0) {
                        $balance_qty = $pi->quantity_balance - $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $quantity, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id, 'preparation_id_origin' => $preparation_id_origin);
                        $quantity = 0;
                    } elseif ($quantity != 0) {
                        $quantity = $quantity - $pi->quantity_balance;
                        $balance_qty = $quantity;
                        $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id, 'preparation_id_origin' => $preparation_id_origin);
                    }
                }
                if (!empty($cost_row)) {
                    $cost[] = $cost_row;
                }
                if ($quantity == 0) {
                    break;
                }
            }
        }
        $ignore_qty_valid = false;
        if (isset($_SERVER['HTTP_REFERER'])) {
            $hr = $_SERVER['HTTP_REFERER'];
            if (strpos($hr, 'fe_pos_sale_id')) {
                $ignore_qty_valid = true;
            }
        }

        $quantity = (Double) $this->sma->formatDecimal($quantity);
        if ($quantity > 0 && !$this->Settings->overselling && !$recontabilizacion && $ignore_qty_valid == false && $sale_status != 'pending') {
            if ($this->session->userdata('pos_post_processing')) {
                $this->session->unset_userdata('pos_post_processing');
            }
            if ($this->session->userdata('detal_post_processing')) {
                $this->session->unset_userdata('detal_post_processing');
            }
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), (isset($pi->product_name) ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        } elseif ($quantity != 0) {
            $cost[] = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $quantity, 'purchase_net_unit_cost' => $avg_net_unit_cost, 'purchase_unit_cost' => $avg_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => NULL, 'overselling' => 1, 'inventory' => 1);
            $cost[] = array('pi_overselling' => 1, 'product_id' => $product_id, 'quantity_balance' => (0 - $quantity), 'warehouse_id' => $warehouse_id, 'option_id' => $option_id);
        }
        // $this->sma->print_arrays($cost);
        return $cost;
    }

    public function calculateCost($product_id, $warehouse_id, $net_unit_price, $unit_price, $quantity, $product_name, $option_id, $item_quantity, $recontabilizacion) {
        $pis = $this->getPurchasedItems($product_id, $warehouse_id, $option_id);
        $real_item_qty = $quantity;
        $quantity = $item_quantity;
        $balance_qty = $quantity;
        foreach ($pis as $pi) {
            $cost_row = NULL;
            if (!empty($pi) && $balance_qty <= $quantity && $quantity != 0) {
                $purchase_unit_cost = $pi->unit_cost ? $pi->unit_cost : ($pi->net_unit_cost + ($pi->item_tax / ($pi->quantity == 0 ? 1 : $pi->quantity)));
                if ($pi->quantity_balance >= $quantity && $quantity != 0) {
                    $balance_qty = $pi->quantity_balance - $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $quantity, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => $balance_qty, 'inventory' => 1, 'option_id' => $option_id);
                    $quantity = 0;
                } elseif ($quantity != 0) {
                    $quantity = $quantity - $pi->quantity_balance;
                    $balance_qty = $quantity;
                    $cost_row = array('date' => date('Y-m-d'), 'product_id' => $product_id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => $pi->id, 'quantity' => $pi->quantity_balance, 'purchase_net_unit_cost' => $pi->net_unit_cost, 'purchase_unit_cost' => $purchase_unit_cost, 'sale_net_unit_price' => $net_unit_price, 'sale_unit_price' => $unit_price, 'quantity_balance' => 0, 'inventory' => 1, 'option_id' => $option_id);
                }
            }
            $cost[] = $cost_row;
            if ($quantity == 0) {
                break;
            }
        }
        if ($quantity > 0 && !$recontabilizacion) {
            if ($this->session->userdata('pos_post_processing')) {
                $this->session->unset_userdata('pos_post_processing');
            }
            if ($this->session->userdata('detal_post_processing')) {
                $this->session->unset_userdata('detal_post_processing');
            }
            $this->session->set_flashdata('error', sprintf(lang("quantity_out_of_stock_for_%s"), (isset($pi->product_name) ? $pi->product_name : $product_name)));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        return $cost;
    }

    public function getPurchasedItems($product_id, $warehouse_id, $option_id = NULL) {
        $orderby = ($this->Settings->accounting_method == 1) ? 'asc' : 'desc';
        $this->db->select('id, quantity, quantity_balance, net_unit_cost, unit_cost, item_tax');
        $this->db->where('product_id', $product_id)->where('warehouse_id', $warehouse_id)->where('quantity_balance !=', 0)->where('status !=', 'service_received');
        if (!isset($option_id) || empty($option_id)) {
            $this->db->group_start()->where('option_id', NULL)->or_where('option_id', 0)->group_end();
        } else {
            $this->db->where('option_id', $option_id);
        }
        $this->db->group_start()->where('(status = "received" or status = "partial")')->group_end();
        $this->db->group_by('id');
        $this->db->order_by('date', $orderby);
        // $this->db->where('quantity_balance >', 0);
        $this->db->order_by('purchase_id', $orderby);
        $q = $this->db->get('purchase_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getProductComboItems($pid, $warehouse_id = NULL) {
        $this->db->select('products.id as id,
                           combo_items.item_code as code,
                           combo_items.quantity as qty,
                           products.name as name,
                           products.type as type,
                           products.cost as unit_price,
                           combo_items.unit as unit,
                           products.type as type,
                           warehouses_products.quantity as quantity')
            ->join('products', 'products.code=combo_items.item_code', 'left')
            ->join('warehouses_products', 'warehouses_products.product_id=products.id', 'left')
            ->group_by('combo_items.id');
        if($warehouse_id) {
            $this->db->where('warehouses_products.warehouse_id', $warehouse_id);
        }
        $this->db->where('combo_items.product_id', $pid);
        $q = $this->db->get('combo_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $units_data = $this->getUnitsByBUID($row->unit);
                $units = [];
                foreach ($units_data as $unitdata) {
                    $units[$unitdata->id] = $unitdata;
                }
                $row->units = $units;
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function item_costing($item, $pi = NULL, $recontabilizacion = false, $sale_status = NULL) {
        $item_quantity = $pi ? $item['aquantity'] : $item['quantity'];
        if (!isset($item['option_id']) || empty($item['option_id']) || $item['option_id'] == 'null') {
            $item['option_id'] = NULL;
        }
        if ($this->Settings->accounting_method != 2 && !$this->Settings->overselling) {
            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard' || $item['product_type'] == 'raw' || $item['product_type'] == 'subproduct' || $item['product_type'] == 'pfinished') {
                    $unit = $this->getUnitByID($item['product_unit_id']);
                    $item['net_unit_price'] = $this->convertToBase($unit, $this->sma->formatDecimal($item['net_unit_price']));
                    $item['unit_price'] = $this->convertToBase($unit, $item['unit_price']);
                    $cost = $this->calculateCost($item['product_id'], $item['warehouse_id'], $this->sma->formatDecimal($item['net_unit_price']), $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity, $recontabilizacion);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    foreach ($combo_items as $combo_item) {
                        $pr = $this->getProductByCode($combo_item->code);
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        if ($pr->type == 'standard' || $pr->type == 'raw' || $pr->type == 'subproduct' || $pr->type == 'pfinished') {
                            $cost[] = $this->calculateCost($pr->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $pr->name, NULL, $item_quantity, $recontabilizacion, $item['product_id']);
                        } else {
                            $cost[] = array(array('date' => date('Y-m-d'), 'product_id' => $pr->id, 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => ($combo_item->qty * $item['quantity']), 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $combo_item->unit_price, 'sale_unit_price' => $combo_item->unit_price, 'quantity_balance' => NULL, 'inventory' => NULL));
                        }
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $this->sma->formatDecimal($item['net_unit_price']), 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $this->sma->formatDecimal($item['net_unit_price']), 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        } else {


            if ($this->getProductByID($item['product_id'])) {
                if ($item['product_type'] == 'standard' || $item['product_type'] == 'raw' || $item['product_type'] == 'subproduct' || $item['product_type'] == 'pfinished') {
                    $cost = $this->calculateAVCost($item['product_id'], $item['warehouse_id'], $this->sma->formatDecimal($item['net_unit_price']), $item['unit_price'], $item['quantity'], $item['product_name'], $item['option_id'], $item_quantity, (isset($item['sale_id']) ? $item['sale_id'] : NULL), $recontabilizacion, (isset($item['preparation_id']) ? $item['preparation_id'] : NULL), $sale_status);
                } elseif ($item['product_type'] == 'combo') {
                    $combo_items = $this->getProductComboItems($item['product_id'], $item['warehouse_id']);
                    //ACÁ REVISIÓN DE CANTIDADES EN COMBO
                    foreach ($combo_items as $combo_item) {
                        $ci_unit = $this->getUnitByID($combo_item->unit);
                        if ($ci_unit->operation_value != 1) {
                            if ($ci_unit->operator == "*") {
                                $combo_item->qty = ($combo_item->qty * $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "/") {
                                $combo_item->qty = ($combo_item->qty / $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "+") {
                                $combo_item->qty = ($combo_item->qty + $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "-") {
                                $combo_item->qty = ($combo_item->qty - $ci_unit->operation_value);
                            }
                        }
                        $pr = $this->getProductByCode($combo_item->code);
                        $item_quantity = $combo_item->qty;
                        if ($pr->tax_rate) {
                            $pr_tax = $this->getTaxRateByID($pr->tax_rate);
                            if ($pr->tax_method) {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / (100 + $pr_tax->rate));
                                $net_unit_price = $combo_item->unit_price - $item_tax;
                                $unit_price = $combo_item->unit_price;
                            } else {
                                $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $pr_tax->rate) / 100);
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price + $item_tax;
                            }
                        } else {
                            $net_unit_price = $combo_item->unit_price;
                            $unit_price = $combo_item->unit_price;
                        }
                        $cost[] = $this->calculateAVCost($combo_item->id, $item['warehouse_id'], $net_unit_price, $unit_price, ($combo_item->qty * $item['quantity']), $item['product_name'], $item['option_id'], $item_quantity, $item['sale_id'], $recontabilizacion, $item['product_id']);
                    }
                } else {
                    $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $this->sma->formatDecimal($item['net_unit_price']), 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
                }
            } elseif ($item['product_type'] == 'manual') {
                $cost = array(array('date' => date('Y-m-d'), 'product_id' => $item['product_id'], 'sale_item_id' => 'sale_items.id', 'purchase_item_id' => NULL, 'quantity' => $item['quantity'], 'purchase_net_unit_cost' => 0, 'purchase_unit_cost' => 0, 'sale_net_unit_price' => $this->sma->formatDecimal($item['net_unit_price']), 'sale_unit_price' => $item['unit_price'], 'quantity_balance' => NULL, 'inventory' => NULL));
            }

        }
        return $cost;
    }

    public function costing($items, $recontabilizacion = false, $sale_status = NULL) {
        $citems = array();
        foreach ($items as $item) {
            $option = (isset($item['option_id']) && !empty($item['option_id']) && $item['option_id'] != 'null' && $item['option_id'] != 'false') ? $item['option_id'] : '';
            $pr = $this->getProductByID($item['product_id']);
            $item['option_id'] = $option;
            if ($pr && ($pr->type == 'standard' || $pr->type == 'raw' || $pr->type == 'subproduct' || $pr->type == 'pfinished')) {
                if (isset($citems['p' . $item['product_id'] . 'o' . $item['option_id']])) {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] += $item['quantity'];
                } else {
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']] = $item;
                    $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'] = $item['quantity'];
                }
            } elseif ($pr && $pr->type == 'combo') {
                $wh = $this->Settings->overselling ? NULL : $item['warehouse_id'];
                $combo_items = $this->getProductComboItems($item['product_id'], $wh);
                foreach ($combo_items as $combo_item) {
                    $ci_unit = $this->getUnitByID($combo_item->unit);
                    if ($ci_unit->operation_value != 1) {
                        if ($ci_unit->operator == "*") {
                            $combo_item->qty = ($combo_item->qty * $ci_unit->operation_value);
                        } else if ($ci_unit->operator == "/") {
                            $combo_item->qty = ($combo_item->qty / $ci_unit->operation_value);
                        } else if ($ci_unit->operator == "+") {
                            $combo_item->qty = ($combo_item->qty + $ci_unit->operation_value);
                        } else if ($ci_unit->operator == "-") {
                            $combo_item->qty = ($combo_item->qty - $ci_unit->operation_value);
                        }
                    }
                    if ($combo_item->type == 'standard'  || $combo_item->type == 'raw' || $combo_item->type == 'subproduct'  || $combo_item->type == 'pfinished') {
                        if (isset($citems['p' . $combo_item->id . 'o' . $item['option_id']])) {
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] += ($combo_item->qty*$item['quantity']);
                        } else {
                            $cpr = $this->getProductByID($combo_item->id);
                            if ($cpr->tax_rate) {
                                $cpr_tax = $this->getTaxRateByID($cpr->tax_rate);
                                if ($cpr->tax_method) {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / (100 + $cpr_tax->rate));
                                    $net_unit_price = $combo_item->unit_price - $item_tax;
                                    $unit_price = $combo_item->unit_price;
                                } else {
                                    $item_tax = $this->sma->formatDecimal((($combo_item->unit_price) * $cpr_tax->rate) / 100);
                                    $net_unit_price = $combo_item->unit_price;
                                    $unit_price = $combo_item->unit_price + $item_tax;
                                }
                            } else {
                                $net_unit_price = $combo_item->unit_price;
                                $unit_price = $combo_item->unit_price;
                            }
                            $cproduct = array('preparation_id' => $item['product_id'], 'product_id' => $combo_item->id, 'product_name' => $cpr->name, 'product_type' => $combo_item->type, 'quantity' => ($combo_item->qty*$item['quantity']), 'net_unit_price' => $net_unit_price, 'unit_price' => $unit_price, 'warehouse_id' => $item['warehouse_id'], 'item_tax' => $item_tax, 'tax_rate_id' => $cpr->tax_rate, 'tax' => ($cpr_tax->type == 1 ? $cpr_tax->rate.'%' : $cpr_tax->rate), 'option_id' => NULL, 'product_unit_id' => $cpr->unit);
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']] = $cproduct;
                            $citems['p' . $combo_item->id . 'o' . $item['option_id']]['aquantity'] = ($combo_item->qty*$item['quantity']);
                        }
                    }
                }
            }
        }

        // $this->sma->print_arrays($combo_items, $citems);
        $cost = array();
        foreach ($citems as $item) {
            $item['aquantity'] = $citems['p' . $item['product_id'] . 'o' . $item['option_id']]['aquantity'];
            $cost[] = $this->item_costing($item, TRUE, $recontabilizacion, $sale_status);
        }
        return $cost;
    }

    public function syncQuantity($sale_id = NULL, $purchase_id = NULL, $oitems = NULL, $product_id = NULL, $with_movements = FALSE) {
        if ($sale_id) {
            $sale_items = $this->getAllSaleItems($sale_id);
            foreach ($sale_items as $item) {
                if ($item->product_type == 'standard' || $item->product_type == 'raw' || $item->product_type == 'subproduct' || $item->product_type == 'pfinished') {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                } elseif ($item->product_type == 'combo') {
                    $wh = $this->Settings->overselling ? NULL : $item->warehouse_id;
                    $combo_items = $this->getProductComboItems($item->product_id, $wh);
                    foreach ($combo_items as $combo_item) {
                        if($combo_item->type == 'standard' || $combo_item->type == 'raw' || $combo_item->type == 'subproduct' || $combo_item->type == 'pfinished') {
                            $this->syncProductQty($combo_item->id, $item->warehouse_id);
                            $this->syncComboQty($combo_item);
                        }
                    }
                }
            }

        } elseif ($purchase_id) {

            $purchase_items = $this->getAllPurchaseItems($purchase_id);
            foreach ($purchase_items as $item) {
                $this->syncProductQty($item->product_id, $item->warehouse_id);
                if (isset($item->option_id) && !empty($item->option_id)) {
                    $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                }
                $pr = $this->getProductByID($item->product_id);
                if ($pr->type == 'raw') {
                    $this->syncComboQty($pr);
                }
            }

        } elseif ($oitems) {

            foreach ($oitems as $item) {
                if (isset($item->product_type)) {
                    if ($item->product_type == 'standard' || $item->product_type == 'raw' || $item->product_type == 'subproduct' || $item->product_type == 'pfinished') {
                        $this->syncProductQty($item->product_id, $item->warehouse_id);
                        if (isset($item->option_id) && !empty($item->option_id)) {
                            $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                        }
                    } elseif ($item->product_type == 'combo') {
                        $combo_items = $this->getProductComboItems($item->product_id, $item->warehouse_id);
                        foreach ($combo_items as $combo_item) {
                            if($combo_item->type == 'standard' || $combo_item->type == 'raw' || $combo_item->type == 'subproduct' || $combo_item->type == 'pfinished') {
                                $this->syncProductQty($combo_item->id, $item->warehouse_id);
                            }
                        }
                    }
                } else {
                    $this->syncProductQty($item->product_id, $item->warehouse_id);
                    if (isset($item->option_id) && !empty($item->option_id)) {
                        $this->syncVariantQty($item->option_id, $item->warehouse_id, $item->product_id);
                    }
                }
            }

        } elseif ($product_id) {
            $product = $this->getProductByID($product_id);
            $this->db
                ->where('(purchase_id IS NULL AND transfer_id IS NULL AND quantity = 0 AND quantity_balance = 0)')
                ->delete('purchase_items');
            $this->db->update('warehouses_products', ['quantity'=>0], ['product_id'=>$product_id]);
            $this->db->update('warehouses_products_variants', ['quantity'=>0], ['product_id'=>$product_id]);
            if ($product->type != 'service') {
                if ($with_movements == TRUE){
                    $product_variants = $this->getProductVariants($product_id);
                    $purchases              = []; //OK
                    $return_purchases       = []; //OK
                    $sales                  = []; //OK
                    $return_sales           = []; //OK
                    $return_sales_2         = []; //OK
                    $negative_adjustment    = []; //OK
                    $positive_adjustment    = []; //OK
                    $transfer_in            = []; //OK
                    $transfer_out           = []; //OK
                    $overselling            = []; //OK
                    $all_warehouses = $this->getAllWarehouses();
                    $warehouses = [];
                    foreach ($all_warehouses as $ew) {
                        $warehouses[$ew->id] = 1;
                    }
                    $pis = []; //PURCHASES ITEMS EN LAS QUE SE MANIPULARAN LOS QUANTITY BALANCE CON LAS SALIDAS
                    $oss = []; //PURCHASES ITEMS EN LAS QUE SE MANIPULARAN LOS QUANTITY BALANCE CON LOS SALDOS RESTANTES DE LOS MOVIMIENTOS
                    $saldos = [];
                    $saldos_2 = [];
                    $saldos_procesar = [];
                    $entradas_wh = [];
                    $salidas_wh = [];
                    $msg = '';
                    if (!$product_variants) { //EL PRODUCTO NO TIENE VARIANTES
                        $purchase_items =
                        $this->db->select('purchase_items.*')
                                ->where('purchase_items.product_id', $product_id)
                                ->where('purchase_items.status', 'received')
                                ->where('(IF('.$this->db->dbprefix('purchases').'.id IS NOT NULL, '.$this->db->dbprefix('purchases').'.purchase_type = 1, '.$this->db->dbprefix('purchases').'.id IS NULL))')
                                ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'left')
                                ->get('purchase_items');
                        if ($purchase_items->num_rows() > 0) {
                            foreach (($purchase_items->result()) as $pi) {

                                if (!isset($warehouses[$pi->warehouse_id])) {
                                    $warehouses[$pi->warehouse_id] = 1;
                                }
                                if ($pi->purchase_id != NULL) { //SI SON REGISTROS POR COMPRAS
                                    if ($pi->quantity > 0) { //COMPRAS POR BODEGA
                                        if (isset($purchases[$pi->warehouse_id])) {
                                            $purchases[$pi->warehouse_id] += $pi->quantity;
                                        } else {
                                            $purchases[$pi->warehouse_id] = $pi->quantity;
                                        }
                                        $pis[] = $pi;
                                    } else { //DEVOLUCIONES DE COMPRAS POR BODEGA
                                        if (isset($return_purchases[$pi->warehouse_id])) {
                                            $return_purchases[$pi->warehouse_id] += $pi->quantity;
                                        } else {
                                            $return_purchases[$pi->warehouse_id] = $pi->quantity;
                                        }
                                    }
                                } else if ($pi->transfer_id != NULL) { //SI SON REGISTROS POR TRANSFERENCIAS
                                    $transfer = $this->db->where('id', $pi->transfer_id)->get('transfers');
                                    if ($transfer->num_rows() > 0) {
                                        $transfer = $transfer->row();
                                        if (isset($transfer_in[$transfer->to_warehouse_id])) { //ENTRADAS POR TRANSFERENCIA POR BODEGAS
                                            $transfer_in[$transfer->to_warehouse_id] += $pi->quantity;
                                        } else {
                                            $transfer_in[$transfer->to_warehouse_id] = $pi->quantity;
                                        }
                                        if (isset($transfer_out[$transfer->from_warehouse_id])) { //SALIDAS POR TRANSFERENCIA POR BODEGAS
                                            $transfer_out[$transfer->from_warehouse_id] += $pi->quantity;
                                        } else {
                                            $transfer_out[$transfer->from_warehouse_id] = $pi->quantity;
                                        }
                                        $pis[] = $pi;
                                    }
                                } else if ($pi->purchase_id == NULL && $pi->transfer_id == NULL) { //SI ES UN REGISTRO DE CONTROL SOBRE VENTA O DEVOLUCIONES DE VENTAS
                                    $oss[$pi->warehouse_id] = $pi;
                                    if ($pi->quantity_balance > 0) { //SI SON DEVOLUCIONES DE VENTAS
                                        if (isset($return_sales_2[$pi->warehouse_id])) { //ENTRADAS POR TRANSFERENCIA POR BODEGAS
                                            $return_sales_2[$pi->warehouse_id] += $pi->quantity_balance;
                                        } else {
                                            $return_sales_2[$pi->warehouse_id] = $pi->quantity_balance;
                                        }
                                    } else { // SI SON SOBREVENTAS
                                        if (isset($overselling[$pi->warehouse_id])) { //ENTRADAS POR TRANSFERENCIA POR BODEGAS
                                            $overselling[$pi->warehouse_id] += $pi->quantity_balance;
                                        } else {
                                            $overselling[$pi->warehouse_id] = $pi->quantity_balance;
                                        }
                                    }
                                }
                            }
                        }
                        $adjustment_items = $this->db->where('product_id', $product_id)->get('adjustment_items');
                        if ($adjustment_items->num_rows() > 0) {
                            foreach (($adjustment_items->result()) as $ai) {
                                if (!isset($warehouses[$ai->warehouse_id])) {
                                    $warehouses[$ai->warehouse_id] = 1;
                                }
                                if ($ai->type == 'addition') {
                                    if (isset($positive_adjustment[$ai->warehouse_id])) { //AJUSTES POSITIVOS POR BODEGAS
                                        $positive_adjustment[$ai->warehouse_id] += $ai->quantity;
                                    } else {
                                        $positive_adjustment[$ai->warehouse_id] = $ai->quantity;
                                    }
                                } else if ($ai->type == 'subtraction') {
                                    if (isset($negative_adjustment[$ai->warehouse_id])) { //AJUSTES NEGATIVOS POR BODEGAS
                                        $negative_adjustment[$ai->warehouse_id] += $ai->quantity;
                                    } else {
                                        $negative_adjustment[$ai->warehouse_id] = $ai->quantity;
                                    }
                                }
                            }
                        }
                        $sale_items = $this->db->select('sale_items.*')
                                                ->join('sales', 'sales.id = sale_items.sale_id')
                                                ->where('sale_items.product_id', $product_id)
                                                ->where('sales.sale_status', 'completed')
                                                // ->group_by('sale_items.product_id')
                                                ->get('sale_items');
                        if ($sale_items->num_rows() > 0) {
                            foreach (($sale_items->result()) as $si) {

                                if (isset($sales[$si->warehouse_id])) { //VENTAS POR BODEGAS
                                    $sales[$si->warehouse_id] += $si->quantity;
                                } else {
                                    $sales[$si->warehouse_id] = $si->quantity;
                                }

                            }
                        }
                        $sale_items = $this->db->select('sale_items.*')
                                                ->join('sales', 'sales.id = sale_items.sale_id')
                                                ->where('sale_items.product_id', $product_id)
                                                ->where('sales.sale_status', 'returned')
                                                // ->group_by('sale_items.product_id')
                                                ->get('sale_items');
                        if ($sale_items->num_rows() > 0) {
                            foreach (($sale_items->result()) as $si) {

                                if (isset($return_sales[$si->warehouse_id])) { //VENTAS POR BODEGAS
                                    $return_sales[$si->warehouse_id] += $si->quantity;
                                } else {
                                    $return_sales[$si->warehouse_id] = $si->quantity;
                                }

                            }
                        }
                        foreach ($warehouses as $wh_id => $setted) {
                            $salidas =
                                    (isset($return_purchases[$wh_id]) ? ($return_purchases[$wh_id] * -1) : 0)
                                    +
                                    (isset($sales[$wh_id]) ? $sales[$wh_id] : 0)
                                    +
                                    (isset($negative_adjustment[$wh_id]) ? $negative_adjustment[$wh_id] : 0)
                                    +
                                    (isset($transfer_out[$wh_id]) ? $transfer_out[$wh_id] : 0)
                                    ;
                            $entradas =
                                    (isset($purchases[$wh_id]) ? $purchases[$wh_id] : 0)
                                    +
                                    (isset($return_sales[$wh_id]) ? ($return_sales[$wh_id] * -1) : 0)
                                    +
                                    (isset($positive_adjustment[$wh_id]) ? $positive_adjustment[$wh_id] : 0)
                                    +
                                    (isset($transfer_in[$wh_id]) ? $transfer_in[$wh_id] : 0)
                                    ;
                            $saldo = $entradas - $salidas;
                            $saldos[$wh_id] = $saldo;
                            $salidas_wh[$wh_id] = $salidas;
                            $entradas_wh[$wh_id] = $entradas;
                            $p_items = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'warehouse_id' => $wh_id, 'status' => 'received'));
                            $saldo_procesar = $saldo;
                            $saldos_procesar[$wh_id] = $saldo_procesar;
                            if ($p_items->num_rows() > 0) {
                                $p_items = $p_items->result();
                                $pis = $p_items;
                                $q_balance = 0;
                                foreach ($p_items as $pi) {
                                    $this->db->update('purchase_items', array('quantity_balance' => 0), array('id' => $pi->id));
                                    $q_balance += $pi->quantity_balance;
                                }
                                arsort($pis);
                                if ($saldo > 0) {
                                    foreach ($pis as $pi2) {
                                        if ($pi2->purchase_id != null || $pi2->transfer_id != null) {
                                            if ($pi2->quantity <= $saldo_procesar && $saldo_procesar > 0) {
                                                $saldo_procesar -= $pi2->quantity;
                                                $this->db->update('purchase_items', array('quantity_balance' => $pi2->quantity), array('id' => $pi2->id));
                                            } else if ($pi2->quantity >= $saldo_procesar && $saldo_procesar > 0) {
                                                $this->db->update('purchase_items', array('quantity_balance' => $saldo_procesar), array('id' => $pi2->id));
                                                $saldo_procesar = 0;
                                                break;
                                            }
                                        } else {
                                            continue;
                                        }
                                    }
                                    if ($saldo_procesar > 0 || $saldo_procesar < 0) {
                                        if (isset($oss[$wh_id]) && count((array) $oss[$wh_id]) > 0) {
                                            $this->db->update('purchase_items', array('quantity_balance' => $saldo_procesar), array('id' => $oss[$wh_id]->id));
                                        } else {
                                            $os_insert = array(
                                                            'product_id' => $pis[0]->product_id,
                                                            'product_code' => $pis[0]->product_code,
                                                            'product_name' => $pis[0]->product_name,
                                                            'warehouse_id' => $wh_id,
                                                            'quantity_balance' => $saldo_procesar,
                                                            'status' => 'received'
                                                            );
                                            $this->db->insert('purchase_items', $os_insert);
                                        }
                                    }
                                } else {
                                    if (isset($oss[$wh_id]) && count((array) $oss[$wh_id]) > 0) {
                                        $this->db->update('purchase_items', array('quantity_balance' => $saldo_procesar), array('id' => $oss[$wh_id]->id));
                                    } else {
                                        if ($saldo_procesar > 0 || $saldo_procesar < 0) {
                                            $os_insert = array(
                                                            'product_id' => $pis[0]->product_id,
                                                            'product_code' => $pis[0]->product_code,
                                                            'product_name' => $pis[0]->product_name,
                                                            'warehouse_id' => $wh_id,
                                                            'quantity_balance' => $saldo_procesar,
                                                            'status' => 'received'
                                                            );
                                            $this->db->insert('purchase_items', $os_insert);
                                        }
                                    }
                                }
                            } else {
                                if ($saldo_procesar > 0 || $saldo_procesar < 0) {
                                    $os_insert = array(
                                                'product_id' => $product_id,
                                                'product_code' => ' ',
                                                'product_name' => ' ',
                                                'warehouse_id' => $wh_id,
                                                'quantity_balance' => $saldo_procesar,
                                                'status' => 'received'
                                                );
                                    $this->db->insert('purchase_items', $os_insert);
                                }
                            }
                            $saldos_2[$wh_id] = $saldo_procesar;
                            $msg .= "Product id : ".$product_id.", BODEGA ".$wh_id." </br>".
                            '</br> Compras : '.(isset($purchases[$wh_id]) ? $purchases[$wh_id] : 0).
                            '</br> Devolución Compras : '.(isset($return_purchases[$wh_id]) ? $return_purchases[$wh_id] : 0).
                            '</br> Ventas : '.(isset($sales[$wh_id]) ? $sales[$wh_id] : 0).
                            '</br> Devolución Ventas : '.(isset($return_sales[$wh_id]) ? $return_sales[$wh_id] : 0).
                            '</br> Devolución Ventas 2 : '.(isset($return_sales_2[$wh_id]) ? $return_sales_2[$wh_id] : 0).
                            '</br> Ajustes Negativos : '.(isset($negative_adjustment[$wh_id]) ? $negative_adjustment[$wh_id] : 0).
                            '</br> Ajustes Positivos : '.(isset($positive_adjustment[$wh_id]) ? $positive_adjustment[$wh_id] : 0).
                            '</br> Transferencias IN : '.(isset($transfer_in[$wh_id]) ? $transfer_in[$wh_id] : 0).
                            '</br> Transferencias OUT : '.(isset($transfer_out[$wh_id]) ? $transfer_out[$wh_id] : 0).
                            '</br> Sobreventas : '.(isset($overselling[$wh_id]) ? $overselling[$wh_id] : 0).
                            '</br> Salidas : '.(isset($salidas_wh[$wh_id]) ? $salidas_wh[$wh_id] : 0).
                            '</br> Entradas : '.(isset($entradas_wh[$wh_id]) ? $entradas_wh[$wh_id] : 0).
                            '</br> Saldos : '.(isset($saldos[$wh_id]) ? $saldos[$wh_id] : 0).
                            '</br> Saldo Procesar : '.(isset($saldos_procesar[$wh_id]) ? $saldos_procesar[$wh_id] : 0).
                            '</br> Saldos : '.(isset($saldos_2[$wh_id]) ? $saldos_2[$wh_id] : 0).
                            "</br></br>"
                            ;
                        }
                    } else { //EL PRODUCTO SI TIENE VARIANTES
                        foreach ($product_variants as $pvariant) {
                            // $pvariant->id;
                            $purchase_items = $this->db->select('purchase_items.*')
                                ->where('purchase_items.product_id', $product_id)
                                ->where('purchase_items.status', 'received')
                                ->where('purchase_items.option_id', $pvariant->id)
                                ->where('(IF('.$this->db->dbprefix('purchases').'.id IS NOT NULL, '.$this->db->dbprefix('purchases').'.purchase_type = 1, '.$this->db->dbprefix('purchases').'.id IS NULL))')
                                ->join('purchases', 'purchases.id = purchase_items.purchase_id', 'left')
                                ->get('purchase_items');
                            if ($purchase_items->num_rows() > 0) {
                                foreach (($purchase_items->result()) as $pi) {
                                    if (!isset($warehouses[$pi->warehouse_id])) {
                                        $warehouses[$pi->warehouse_id] = 1;
                                    }
                                    if ($pi->purchase_id != NULL) { //SI SON REGISTROS POR COMPRAS
                                        if ($pi->quantity > 0) { //COMPRAS POR BODEGA
                                            if (isset($purchases[$pi->warehouse_id][$pvariant->id])) {
                                                $purchases[$pi->warehouse_id][$pvariant->id] += $pi->quantity;
                                            } else {
                                                $purchases[$pi->warehouse_id][$pvariant->id] = $pi->quantity;
                                            }
                                            $pis[$pvariant->id][] = $pi;
                                        } else { //DEVOLUCIONES DE COMPRAS POR BODEGA
                                            if (isset($return_purchases[$pi->warehouse_id][$pvariant->id])) {
                                                $return_purchases[$pi->warehouse_id][$pvariant->id] += $pi->quantity;
                                            } else {
                                                $return_purchases[$pi->warehouse_id][$pvariant->id] = $pi->quantity;
                                            }
                                        }
                                    } else if ($pi->transfer_id != NULL) { //SI SON REGISTROS POR TRANSFERENCIAS
                                        $transfer = $this->db->where('id', $pi->transfer_id)->get('transfers');
                                        if ($transfer->num_rows() > 0) {
                                            $transfer = $transfer->row();
                                            if (isset($transfer_in[$transfer->to_warehouse_id][$pvariant->id])) { //ENTRADAS POR TRANSFERENCIA POR BODEGAS
                                                $transfer_in[$transfer->to_warehouse_id][$pvariant->id] += $pi->quantity;
                                            } else {
                                                $transfer_in[$transfer->to_warehouse_id][$pvariant->id] = $pi->quantity;
                                            }
                                            if (isset($transfer_out[$transfer->from_warehouse_id][$pvariant->id])) { //SALIDAS POR TRANSFERENCIA POR BODEGAS
                                                $transfer_out[$transfer->from_warehouse_id][$pvariant->id] += $pi->quantity;
                                            } else {
                                                $transfer_out[$transfer->from_warehouse_id][$pvariant->id] = $pi->quantity;
                                            }
                                            $pis[$pvariant->id][] = $pi;
                                        }
                                    } else if ($pi->purchase_id == NULL && $pi->transfer_id == NULL) { //SI ES UN REGISTRO DE CONTROL SOBRE VENTA O DEVOLUCIONES DE VENTAS
                                        $oss[$pi->warehouse_id][$pvariant->id] = $pi;
                                        if ($pi->quantity_balance > 0) { //SI SON DEVOLUCIONES DE VENTAS
                                            if (isset($return_sales_2[$pi->warehouse_id][$pvariant->id])) { //ENTRADAS POR TRANSFERENCIA POR BODEGAS
                                                $return_sales_2[$pi->warehouse_id][$pvariant->id] += $pi->quantity_balance;
                                            } else {
                                                $return_sales_2[$pi->warehouse_id][$pvariant->id] = $pi->quantity_balance;
                                            }
                                        } else { // SI SON SOBREVENTAS
                                            if (isset($overselling[$pi->warehouse_id][$pvariant->id])) { //ENTRADAS POR TRANSFERENCIA POR BODEGAS
                                                $overselling[$pi->warehouse_id][$pvariant->id] += $pi->quantity_balance;
                                            } else {
                                                $overselling[$pi->warehouse_id][$pvariant->id] = $pi->quantity_balance;
                                            }
                                        }
                                    }
                                }
                            }
                            $adjustment_items = $this->db->where('product_id', $product_id)->where('option_id', $pvariant->id)->get('adjustment_items');
                            if ($adjustment_items->num_rows() > 0) {
                                foreach (($adjustment_items->result()) as $ai) {
                                    if (!isset($warehouses[$ai->warehouse_id])) {
                                        $warehouses[$ai->warehouse_id] = 1;
                                    }
                                    if ($ai->type == 'addition') {
                                        if (isset($positive_adjustment[$ai->warehouse_id][$pvariant->id])) { //AJUSTES POSITIVOS POR BODEGAS
                                            $positive_adjustment[$ai->warehouse_id][$pvariant->id] += $ai->quantity;
                                        } else {
                                            $positive_adjustment[$ai->warehouse_id][$pvariant->id] = $ai->quantity;
                                        }
                                    } else if ($ai->type == 'subtraction') {
                                        if (isset($negative_adjustment[$ai->warehouse_id][$pvariant->id])) { //AJUSTES NEGATIVOS POR BODEGAS
                                            $negative_adjustment[$ai->warehouse_id][$pvariant->id] += $ai->quantity;
                                        } else {
                                            $negative_adjustment[$ai->warehouse_id][$pvariant->id] = $ai->quantity;
                                        }
                                    }
                                }
                            }
                            $sale_items = $this->db->select('sale_items.*')
                                                    ->join('sales', 'sales.id = sale_items.sale_id')
                                                    ->where('sale_items.product_id', $product_id)
                                                    ->where('sale_items.option_id', $pvariant->id)
                                                    ->where('sales.sale_status', 'completed')
                                                    // ->group_by('sale_items.product_id')
                                                    ->get('sale_items');
                            if ($sale_items->num_rows() > 0) {
                                foreach (($sale_items->result()) as $si) {
                                    if (!isset($warehouses[$si->warehouse_id])) {
                                        $warehouses[$si->warehouse_id] = 1;
                                    }
                                    if (isset($sales[$si->warehouse_id][$pvariant->id])) { //VENTAS POR BODEGAS
                                        $sales[$si->warehouse_id][$pvariant->id] += $si->quantity;
                                    } else {
                                        $sales[$si->warehouse_id][$pvariant->id] = $si->quantity;
                                    }
                                }
                            }
                            $sale_items = $this->db->select('sale_items.*')
                                                    ->join('sales', 'sales.id = sale_items.sale_id')
                                                    ->where('sale_items.product_id', $product_id)
                                                    ->where('sale_items.option_id', $pvariant->id)
                                                    ->where('sales.sale_status', 'returned')
                                                    // ->group_by('sale_items.product_id')
                                                    ->get('sale_items');
                            if ($sale_items->num_rows() > 0) {
                                foreach (($sale_items->result()) as $si) {
                                    if (!isset($warehouses[$si->warehouse_id])) {
                                        $warehouses[$si->warehouse_id] = 1;
                                    }
                                    if (isset($return_sales[$si->warehouse_id][$pvariant->id])) { //VENTAS POR BODEGAS
                                        $return_sales[$si->warehouse_id][$pvariant->id] += $si->quantity;
                                    } else {
                                        $return_sales[$si->warehouse_id][$pvariant->id] = $si->quantity;
                                    }
                                }
                            }
                        }
                        foreach ($warehouses as $wh_id => $setted) {
                            foreach ($product_variants as $pvariant) {
                                $salidas =
                                        (isset($return_purchases[$wh_id][$pvariant->id]) ? ($return_purchases[$wh_id][$pvariant->id] * -1) : 0)
                                        +
                                        (isset($sales[$wh_id][$pvariant->id]) ? $sales[$wh_id][$pvariant->id] : 0)
                                        +
                                        (isset($negative_adjustment[$wh_id][$pvariant->id]) ? $negative_adjustment[$wh_id][$pvariant->id] : 0)
                                        +
                                        (isset($transfer_out[$wh_id][$pvariant->id]) ? $transfer_out[$wh_id][$pvariant->id] : 0)
                                        ;
                                $entradas =
                                        (isset($purchases[$wh_id][$pvariant->id]) ? $purchases[$wh_id][$pvariant->id] : 0)
                                        +
                                        (isset($return_sales[$wh_id][$pvariant->id]) ? ($return_sales[$wh_id][$pvariant->id] * -1) : 0)
                                        +
                                        (isset($positive_adjustment[$wh_id][$pvariant->id]) ? $positive_adjustment[$wh_id][$pvariant->id] : 0)
                                        +
                                        (isset($transfer_in[$wh_id][$pvariant->id]) ? $transfer_in[$wh_id][$pvariant->id] : 0)
                                        ;
                                $saldo = $entradas - $salidas;
                                $saldos[$wh_id][$pvariant->id] = $saldo;
                                $salidas_wh[$wh_id][$pvariant->id] = $salidas;
                                $entradas_wh[$wh_id][$pvariant->id] = $entradas;
                                $p_items = $this->db->get_where('purchase_items', array('product_id' => $product_id, 'option_id' => $pvariant->id, 'warehouse_id' => $wh_id, 'status' => 'received'));
                                $saldo_procesar = $saldo;
                                $saldos_procesar[$wh_id][$pvariant->id] = $saldo_procesar;
                                if ($p_items->num_rows() > 0) {
                                    $p_items = $p_items->result();
                                    $pis = $p_items;
                                    $q_balance = 0;
                                    foreach ($p_items as $pi) {
                                        $this->db->update('purchase_items', array('quantity_balance' => 0), array('id' => $pi->id));
                                        $q_balance += $pi->quantity_balance;
                                    }
                                    arsort($pis);
                                    if ($saldo > 0) {
                                        foreach ($pis as $pi2) {
                                            if ($pi2->purchase_id != null || $pi2->transfer_id != null) {
                                                if ($pi2->quantity <= $saldo_procesar && $saldo_procesar > 0) {
                                                    $saldo_procesar -= $pi2->quantity;
                                                    $this->db->update('purchase_items', array('quantity_balance' => $pi2->quantity), array('id' => $pi2->id));
                                                } else if ($pi2->quantity >= $saldo_procesar && $saldo_procesar > 0) {
                                                    $this->db->update('purchase_items', array('quantity_balance' => $saldo_procesar), array('id' => $pi2->id));
                                                    $saldo_procesar = 0;
                                                    break;
                                                }
                                            } else {
                                                continue;
                                            }
                                        }
                                        if ($saldo_procesar > 0 || $saldo_procesar < 0) {
                                            if (isset($oss[$wh_id][$pvariant->id]) && count((array) $oss[$wh_id][$pvariant->id]) > 0) {
                                                $this->db->update('purchase_items', array('quantity_balance' => $saldo_procesar), array('id' => $oss[$wh_id][$pvariant->id]->id));
                                            } else {
                                                $os_insert = array(
                                                                'product_id' => $pis[0]->product_id,
                                                                'option_id' => $pvariant->id,
                                                                'product_code' => $pis[0]->product_code,
                                                                'product_name' => $pis[0]->product_name,
                                                                'warehouse_id' => $wh_id,
                                                                'quantity_balance' => $saldo_procesar,
                                                                'status' => 'received'
                                                                );
                                                $this->db->insert('purchase_items', $os_insert);
                                            }
                                        }
                                    } else {
                                        if (isset($oss[$wh_id][$pvariant->id]) && count((array) $oss[$wh_id][$pvariant->id]) > 0) {
                                            $this->db->update('purchase_items', array('quantity_balance' => $saldo_procesar), array('id' => $oss[$wh_id][$pvariant->id]->id));
                                        } else {
                                            if ($saldo_procesar > 0 || $saldo_procesar < 0) {
                                                $os_insert = array(
                                                                'product_id' => $pis[0]->product_id,
                                                                'option_id' => $pvariant->id,
                                                                'product_code' => $pis[0]->product_code,
                                                                'product_name' => $pis[0]->product_name,
                                                                'warehouse_id' => $wh_id,
                                                                'quantity_balance' => $saldo_procesar,
                                                                'status' => 'received'
                                                                );
                                                $this->db->insert('purchase_items', $os_insert);
                                            }
                                        }
                                    }
                                } else {
                                    if ($saldo_procesar > 0 || $saldo_procesar < 0) {
                                        $os_insert = array(
                                                        'product_id' => $product_id,
                                                        'option_id' => $pvariant->id,
                                                        'product_code' => ' ',
                                                        'product_name' => ' ',
                                                        'warehouse_id' => $wh_id,
                                                        'quantity_balance' => $saldo_procesar,
                                                        'status' => 'received'
                                                        );
                                        $this->db->insert('purchase_items', $os_insert);
                                    }
                                }
                                $saldos_2[$wh_id][$pvariant->id] = $saldo_procesar;
                                $msg .= "Product id : ".$product_id.", Option id : ".$pvariant->id.", BODEGA ".$wh_id." </br>".
                                '</br> Compras : '.(isset($purchases[$wh_id][$pvariant->id]) ? $purchases[$wh_id][$pvariant->id] : 0).
                                '</br> Devolución Compras : '.(isset($return_purchases[$wh_id][$pvariant->id]) ? $return_purchases[$wh_id][$pvariant->id] : 0).
                                '</br> Ventas : '.(isset($sales[$wh_id][$pvariant->id]) ? $sales[$wh_id][$pvariant->id] : 0).
                                '</br> Devolución Ventas : '.(isset($return_sales[$wh_id][$pvariant->id]) ? $return_sales[$wh_id][$pvariant->id] : 0).
                                '</br> Devolución Ventas 2 : '.(isset($return_sales_2[$wh_id][$pvariant->id]) ? $return_sales_2[$wh_id][$pvariant->id] : 0).
                                '</br> Ajustes Negativos : '.(isset($negative_adjustment[$wh_id][$pvariant->id]) ? $negative_adjustment[$wh_id][$pvariant->id] : 0).
                                '</br> Ajustes Positivos : '.(isset($positive_adjustment[$wh_id][$pvariant->id]) ? $positive_adjustment[$wh_id][$pvariant->id] : 0).
                                '</br> Transferencias IN : '.(isset($transfer_in[$wh_id][$pvariant->id]) ? $transfer_in[$wh_id][$pvariant->id] : 0).
                                '</br> Transferencias OUT : '.(isset($transfer_out[$wh_id][$pvariant->id]) ? $transfer_out[$wh_id][$pvariant->id] : 0).
                                '</br> Sobreventas : '.(isset($overselling[$wh_id][$pvariant->id]) ? $overselling[$wh_id][$pvariant->id] : 0).
                                '</br> Salidas : '.(isset($salidas_wh[$wh_id][$pvariant->id]) ? $salidas_wh[$wh_id][$pvariant->id] : 0).
                                '</br> Entradas : '.(isset($entradas_wh[$wh_id][$pvariant->id]) ? $entradas_wh[$wh_id][$pvariant->id] : 0).
                                '</br> Saldos : '.(isset($saldos[$wh_id][$pvariant->id]) ? $saldos[$wh_id][$pvariant->id] : 0).
                                '</br> Saldo Procesar : '.(isset($saldos_procesar[$wh_id][$pvariant->id]) ? $saldos_procesar[$wh_id][$pvariant->id] : 0).
                                '</br> Saldos : '.(isset($saldos_2[$wh_id][$pvariant->id]) ? $saldos_2[$wh_id][$pvariant->id] : 0).
                                "</br></br>"
                                ;
                            }
                        }
                    }
                } //REPROCESAR CON MOVIMIENTOS

                $this->db->update('warehouses_products', ['quantity'=>0], ['product_id'=>$product_id]);
                $this->db->update('warehouses_products_variants', ['quantity'=>0], ['product_id'=>$product_id]);
                if ($product_variants = $this->getProductVariants($product_id)) {
                    $warehouses = $this->getAllWarehouses(null, false);
                    foreach ($warehouses as $warehouse) {
                        $this->syncProductQty($product_id, $warehouse->id, true);
                        if ($product_variants = $this->getProductVariants($product_id)) {
                            foreach ($product_variants as $pv) {
                                $this->syncVariantQty($pv->id, $warehouse->id, $product_id);
                            }
                        }
                    }
                } else {
                    $warehouses = $this->getAllWarehouses(null, false);
                    foreach ($warehouses as $warehouse) {
                        $this->syncProductQty($product_id, $warehouse->id);
                    }
                }
                $this->db->update('products', ['last_sincronized_qty_date' => date('Y-m-d H:i:s')], ['id' => $product_id]); // actualizar la fecha de sincronizacion
                // exit($msg);
            }
        }
    }

    public function getProductVariants($product_id) {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    /*public function getAllSaleItems($sale_id) {
        $this->db->join('tax_rates', 'tax_rates.id = sale_items.tax_rate_id', 'left');
        $q = $this->db->get_where('sale_items', array('sale_id' => $sale_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }*/

    public function getAllSaleItems($sale_id) {
        $this->db->select("sale_items.*, tax_rates.*, tax_code_fe.nombre AS code_fe_name, product_variants.name AS nombre_variante,
        tax_secondary.code AS codeTax2,
        tax_secondary.tax_indicator AS nameTax2,
        products.sale_tax_rate_2_milliliters AS mililiters,
        sale_tax_rate_2_nominal AS nominal,
        sale_tax_rate_2_degrees AS degrees,
        products.aiu_product");
        $this->db->from("sale_items");
        $this->db->join('tax_rates', 'tax_rates.id = sale_items.tax_rate_id', 'left');
        $this->db->join('tax_code_fe', 'tax_code_fe.codigo = tax_rates.code_fe', 'left');
        $this->db->join('product_variants', 'product_variants.id = sma_sale_items.option_id', 'left');
        $this->db->join('tax_secondary', 'tax_secondary.id = sale_items.tax_rate_2_id', 'left');
        $this->db->join('products', 'products.id = sale_items.product_id', 'left');
        $this->db->where("sale_items.sale_id", $sale_id);
        $this->db->order_by('sale_items.product_name');
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllPurchaseItems($purchase_id) {
        $this->db->select("purchase_items.*, tax_rates.*, tax_code_fe.nombre AS code_fe_name, product_variants.name AS nombre_variante, purchase_items.id as id");
        $this->db->from("purchase_items");
        $this->db->join('tax_rates', 'tax_rates.id = purchase_items.tax_rate_id', 'left');
        $this->db->join('tax_code_fe', 'tax_code_fe.codigo = tax_rates.code_fe', 'left');
        $this->db->join('product_variants', 'product_variants.id = purchase_items.option_id', 'left');

        $this->db->where("purchase_items.purchase_id", $purchase_id);
        $q = $this->db->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncPurchaseItems($data = array()) {
        // $this->sma->print_arrays($data);
        if (!empty($data)) {
            foreach ($data as $items) {
                foreach ($items as $item) {
                    if (isset($item['pi_overselling'])) {
                        unset($item['pi_overselling']);
                        $option_id = (isset($item['option_id']) && !empty($item['option_id'])) ? $item['option_id'] : NULL;
                        $clause = array('purchase_id' => NULL, 'transfer_id' => NULL, 'product_id' => $item['product_id'], 'warehouse_id' => $item['warehouse_id'], 'option_id' => $option_id);
                        if ($pi = $this->getPurchasedItem($clause)) {
                            $quantity_balance = $pi->quantity_balance + $item['quantity_balance'];
                            $this->db->update('purchase_items', array('quantity_balance' => $quantity_balance), array('id' => $pi->id));
                        } else {
                            $clause['quantity'] = 0;
                            $clause['item_tax'] = 0;
                            $clause['quantity_balance'] = $item['quantity_balance'];
                            $clause['status'] = 'received';
                            $clause['option_id'] = !empty($clause['option_id']) && is_numeric($clause['option_id']) ? $clause['option_id'] : NULL;
                            $this->db->insert('purchase_items', $clause);
                        }
                    } else {
                        if (isset($item['inventory']) && $item['inventory']) {
                            $this->db->update('purchase_items', array('quantity_balance' => $item['quantity_balance']), array('id' => $item['purchase_item_id']));
                        }
                    }
                }
            }
            return TRUE;
        }
        return FALSE;
    }

    public function getProductByCode($code) {
        $q = $this->db->get_where('products', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function check_customer_deposit($customer_id, $amount) {
        $customer = $this->getCompanyByID($customer_id);
        // exit(var_dump($customer->deposit_amount));
        // exit(var_dump($amount));
        return (Double) $customer->deposit_amount >= (Double) $amount;
    }

    public function getWarehouseProduct($warehouse_id, $product_id) {
        $q = $this->db->get_where('warehouses_products', array('product_id' => $product_id, 'warehouse_id' => $warehouse_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllBaseUnits() {
        $q = $this->db->get_where("units", array('base_unit' => NULL));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUnitsByBUID($base_unit) {
        $this->db->where('id', $base_unit)->or_where('base_unit', $base_unit)
        ->group_by('id')->order_by('operation_value asc');
        $q = $this->db->get("units");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getUnitByID($id) {
        $q = $this->db->get_where("units", array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPriceGroupByID($id) {
        $q = $this->db->get_where('price_groups', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllPriceGroups($page = 0, $paginate = false, $limit = null, $price_groups = null) {
        $number_pg = 10;
        if ($price_groups) {
            $this->db->where_in('id', $price_groups);
        }
        $biller_id = $this->session->userdata('biller_id');
        if ($biller_id && $this->Settings->users_can_view_price_groups == 0) {
            $this->db->select('price_groups.*')
                    ->join('biller_data', 'biller_data.default_price_group = price_groups.id AND biller_data.biller_id IN ('.$biller_id.')')
                    ->order_by('price_groups.price_group_base desc, price_groups.id asc');
            if ($paginate != false) {
                if ($page > 0) {
                    $pg_inicio = $number_pg*($page);
                } else {
                    $pg_inicio = $page;
                }
                $q = $this->db->get('price_groups', $number_pg, $pg_inicio);
            } else {
                $q = $this->db->get('price_groups', $limit);
            }
        } else {
            if ($paginate != false) {
                if ($page > 0) {
                    $pg_inicio = $number_pg*($page);
                } else {
                    $pg_inicio = $page;
                }
                $q = $this->db->order_by('price_group_base desc, id asc')->get("price_groups", $number_pg, $pg_inicio);
            } else {
                $q = $this->db->order_by('price_group_base desc, id asc')->get("price_groups", $limit);
            }
        }


        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProductsPriceGroups() {

        $biller_id = $this->session->userdata('biller_id');
        if ($biller_id) {
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }
        if ($this->Settings->prioridad_precios_producto == 10) {
            $this->db->select('
                                        product_prices.*,
                                        units.operator as unit_operator,
                                        units.operation_value as unit_operation_value,
                                        products.tax_method as product_tax_method,
                                        products.consumption_sale_tax as consumption_sale_tax
                                    ')
                          ->join('price_groups', 'price_groups.id = product_prices.price_group_id', 'inner')
                          ->join('unit_prices', 'unit_prices.id_product = product_prices.product_id', 'left')
                          ->join('units', 'units.id = unit_prices.unit_id AND units.price_group_id = price_groups.id ', 'inner')
                          ->join('products', 'products.id = product_prices.product_id', 'inner');
            if ($biller_id) {
                $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
            }
            $q = $this->db->get('product_prices');
        } else {
            $this->db->select('
                                    product_prices.*,
                                    products.tax_method as product_tax_method,
                                    products.consumption_sale_tax as consumption_sale_tax
                                   ')
                        ->join('products', 'products.id = product_prices.product_id');
            $biller_id = $this->session->userdata('biller_id');
            if ($biller_id) {
                $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');

            }
            $q = $this->db->get('product_prices');
        }
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $consumption_sale_tax = 0;
                if ($row->product_tax_method == 0 && $this->Settings->ipoconsumo == 1) {
                    $consumption_sale_tax = $row->consumption_sale_tax;

                    if (isset($row->unit_operator) && $row->unit_operator && $this->Settings->precios_por_unidad_presentacion == 2) {
                        if ($row->unit_operator == "*") {
                            $consumption_sale_tax = $consumption_sale_tax * $row->unit_operation_value;
                        } else if ($row->unit_operator == "/") {
                            $consumption_sale_tax = $consumption_sale_tax / $row->unit_operation_value;
                        }
                    }
                }
                $data[$row->product_id][$row->price_group_id] = $row->price + $consumption_sale_tax;
            }

            if ($this->Settings->prioridad_precios_producto == 10) {
                $pgb = $this->getPriceGroupBase();
                foreach ($pgb as $pgb_data) {
                    $data[$pgb_data->product_id][$pgb_data->price_group_id] = $pgb_data->price + $pgb_data->consumption_sale_tax;
                }
            }

            $this->site->delete_temporary_product_billers_assoc();

            return $data;
        }
        return FALSE;
    }

    public function getProductGroupPrice($product_id, $group_id) {
        $q = $this->db->get_where('product_prices', array('price_group_id' => $group_id, 'product_id' => $product_id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getAllBrands() {
        $q = $this->db->get("brands");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getBrandByID($id) {
        $q = $this->db->get_where('brands', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function convertToBase($unit, $value) {
        switch($unit->operator) {
            case '*':
                return $value / $unit->operation_value;
                break;
            case '/':
                return $value * $unit->operation_value;
                break;
            case '+':
                return $value - $unit->operation_value;
                break;
            case '-':
                return $value + $unit->operation_value;
                break;
            default:
                return $value;
        }
    }

    function calculateTax($product_details = NULL, $tax_details = NULL, $custom_value = NULL, $c_on = NULL, $tax_method = NULL) {
        $value = $custom_value ? $custom_value : (($c_on == 'cost') ? $product_details->cost : $product_details->price);
        $tax_amount = 0; $tax = 0;
        if ($tax_details && $tax_details->type == 1 && $tax_details->rate != 0) {
            if (($tax_method === NULL && ($product_details && $product_details->tax_method == 1)) || $tax_method == 1) {
                $tax_amount = $this->sma->formatDecimal((($value) * $tax_details->rate) / 100);
                $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
            } else {
                $tax_amount = $this->sma->formatDecimal((($value) * $tax_details->rate) / (100 + $tax_details->rate));
                $tax = $this->sma->formatDecimal($tax_details->rate, 0) . "%";
            }
        } elseif ($tax_details && $tax_details->type == 2) {
            $tax_amount = $this->sma->formatDecimal($tax_details->rate);
            $tax = $this->sma->formatDecimal($tax_details->rate, 0);
        }
        return array('id' => ($tax_details ? $tax_details->id : null), 'tax' => $tax, 'amount' => $tax_amount);
    }

    public function getAddressByID($id) {
        return $this->db->get_where('addresses', ['id' => $id], 1)->row();
    }

    public function getAddressByCustomer($customer_id){
        $q = $this->db->get_where('addresses', array('company_id' => $customer_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function checkSlug($slug, $type = NULL) {
        if (!$type) {
            return $this->db->get_where('products', ['slug' => $slug], 1)->row();
        } elseif ($type == 'category') {
            return $this->db->get_where('categories', ['slug' => $slug], 1)->row();
        } elseif ($type == 'brand') {
            return $this->db->get_where('brands', ['slug' => $slug], 1)->row();
        }
        return FALSE;
    }

    public function calculateDiscount($discount = NULL, $amount = NULL) {
        if ($discount && $this->Settings->product_discount) {
            $dpos = strpos($discount, '%');
            if ($dpos !== false) {
                $pds = explode("%", $discount);
                return $this->sma->formatDecimal(((($this->sma->formatDecimal($amount)) * (Float) ($pds[0])) / 100));
            } else {
                return $this->sma->formatDecimal($discount);
            }
        }
        return 0;
    }

    public function calculateOrderTax($order_tax_id = NULL, $amount = NULL) {
        if ($this->Settings->tax2 != 0 && $order_tax_id) {
            if ($order_tax_details = $this->site->getTaxRateByID($order_tax_id)) {
                if ($order_tax_details->type == 1) {
                    return $this->sma->formatDecimal((($amount * $order_tax_details->rate) / 100));
                } else {
                    return $this->sma->formatDecimal($order_tax_details->rate);
                }
            }
        }
        return 0;
    }

    public function getSmsSettings() {
        $q = $this->db->get('sms_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function wappsiContabilidadVerificacion($date = null)
    {
        $this->load->library('session');

        if ($this->Settings->modulary != 1) {
            return 0;
        }
        if($this->session->has_userdata('accounting_module') && $date == null){
            $accountingModule = $this->session->userdata('accounting_module');
            if($accountingModule > 0){
                return $accountingModule;
            } else{
                return 0;
            }
        } else {
            $accountant_year = false;
            if ($this->Settings->years_database_management == 1 && $date) {
                $accountant_year = substr(date('Y', strtotime($date)), 2, 4);
            }
            $tablas = $this->db->list_tables();
            $tablasConfigCount = array();
            foreach ($tablas as $tabla) {
                $pos = strpos($tabla, "settings_con".($accountant_year ? $accountant_year : ""));
                if ($pos !== false) {
                    $tablasConfigCount[] = $tabla;
                }
            }
            if (empty($tablasConfigCount)) {
                foreach ($tablas as $tabla) {
                    $pos = strpos($tabla, "settings_con");
                    if ($pos !== false) {
                        $tablasConfigCount[] = $tabla;
                    }
                }
            }
            $indices = count($tablasConfigCount);
            if(count($tablasConfigCount) > 0){
                $indice = $indices - 1;
                $tablaConfigCount = $tablasConfigCount[$indice];
                $q = $this->db->get_where($tablaConfigCount, array('modulary' => 1), 1);
                if ($q->num_rows() > 0) {
                    $contabilidadSufijo = substr($tablaConfigCount,-2);
                    $this->session->set_userdata('accounting_module', $contabilidadSufijo);
                    return $contabilidadSufijo;
                } else{
                    $this->session->set_userdata('accounting_module', 0);
                    return 0;
                }
            } else{
                $this->session->set_userdata('accounting_module', 0);
                return 0;
            }
        }
    }

    public function insertWappsiEntryItem($entryItem = array(), $contabilidadSufijo = NULL)
    {
      //$this->sma->print_arrays($entryItem);
      $tabla = 'entryitems_con'.$contabilidadSufijo;

        $insert_data = array(
                            'entry_id' => $entryItem['entry_id'],
                            'ledger_id' => $entryItem['ledger_id'],
                            'amount' => $entryItem['amount'],
                            'dc' => $entryItem['dc'],
                            'narration' => $entryItem['narration'],
                            'base' => $entryItem['base'],
                            'companies_id' => $entryItem['companies_id'],
                            );

        if (isset($entryItem['cost_center_id'])) {
            $insert_data['cost_center_id'] = $entryItem['cost_center_id'];
        }

      if ($this->db->insert($tabla, $insert_data)) {
        $id = $this->db->insert_id();
        return $id;
      }
      return FALSE;
    }

    public function getWappsiIdEntryType($label, $contabilidadSufijo)
    {
      $tabla = 'entrytypes_con'.$contabilidadSufijo;
      $q = $this->db->get_where($tabla, array('label' => $label), 1);
      if ($q->num_rows() > 0) {
        $row = $q->row();
        return $row->id;
      }
      return FALSE;
    }

    public function setWappsiNumberingEntryType($id, $number, $contabilidadSufijo)
    {
      $tabla = 'entrytypes_con'.$contabilidadSufijo;
      if ($this->db->update($tabla, array('numbering' => $number), array('id' => $id))) {
        return true;
      }
      return FALSE;
    }

    public function insertWappsiEntri($wappsiEntri = array(), $contabilidadSufijo = null)
    {
      $tabla = 'entries_con'.$contabilidadSufijo;
      if ($this->db->insert($tabla, $wappsiEntri)) {
        $id = $this->db->insert_id();
        return $id;
      }
      return FALSE;
    }

    public function updateWappsiEntri($entryId, $wappsiEntri = array(), $contabilidadSufijo = null)
    {
      $tabla = 'entries_con'.$contabilidadSufijo;
      $desc = str_replace("(R)", "", $wappsiEntri['notes']);
      if ($this->db->update($tabla, array( 'entrytype_id' => $wappsiEntri['entrytype_id'],
                                           'number' => $wappsiEntri['number'],
                                           'date' => $wappsiEntri['date'],
                                           'dr_total' => $wappsiEntri['dr_total'],
                                           'cr_total' => $wappsiEntri['cr_total'],
                                           'notes' => $desc." (R)",
                                           'state' => $wappsiEntri['state'],
                                           'companies_id' => $wappsiEntri['companies_id'],
                                           'consecutive_supplier' => isset($wappsiEntri['consecutive_supplier']) ? $wappsiEntri['consecutive_supplier'] : NULL,
                                           'user_id' => $this->session->userdata('user_id') ),
                            array('id' => $entryId))) {
        return TRUE;
      }
      return FALSE;
    }

    public function getWappsiLedgerId($type, $typeMov, $idTax = 0, $contabilidadSufijo = null, $account_parameter_method = 1)
    {
        $tabla = 'accounts_con'.$contabilidadSufijo;
        $conditions['type'] = $type;
        $conditions['typemov'] = $typeMov;
        if ($account_parameter_method == 1) {
            $conditions['id_tax'] = $idTax ? $idTax : 0;
        } else if ($account_parameter_method == 2) {
            $conditions['id_category'] = $idTax ? $idTax : 0;
        }
        $q = $this->db->get_where($tabla, $conditions, 1);
        if ($q->num_rows() > 0) {
            $row = $q->row();
            return $row->ledger_id;
        }
        return FALSE;
    }

    public function getWappsiReceipLedgerId($type, $contabilidadSufijo, $biller_id)
    {
      $tabla = 'payment_methods_con'.$contabilidadSufijo;
      if ($biller_id && $type == 'cash' && $biller_id != "all") {
        $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
        if ($biller_data->cash_payment_method_account) {
            return $biller_data->cash_payment_method_account;
        }
      }
      $q = $this->db->get_where($tabla, array('type' => $type), 1);
      if ($q->num_rows() > 0) {
        $row = $q->row();
        return $row->receipt_ledger_id;
      }
      return FALSE;
    }

    public function getWappsiPaymentLedgerId($type, $contabilidadSufijo, $biller_id)
    {
      $tabla = 'payment_methods_con'.$contabilidadSufijo;
      if ($biller_id && $type == 'cash' && $biller_id != "all") {
        $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
        if ($biller_data->cash_payment_method_account) {
            return $biller_data->cash_payment_method_account;
        }
      }
      $q = $this->db->get_where($tabla, array('type' => $type), 1);
      if ($q->num_rows() > 0) {
        $row = $q->row();
        return $row->payment_ledger_id;
      }
      return FALSE;
    }

    public function getExpenseCategory($category_id){
        $q = $this->db->get_where('expense_categories', array('id' => $category_id), 1);
        if ($q->num_rows() > 0) {
            $row = $q->row();
            return $row;
        }
        return FALSE;
    }

    public function getWappsiSaleById($id)
    {
      $tabla = 'sales';
      $q = $this->db->get_where($tabla, array('id' => $id), 1);
      if ($q->num_rows() > 0) {
        $row = $q->row();
        return $row;
      }
      return FALSE;
    }

    public function getWappsiPurchaseById($id)
    {
      $tabla = 'purchases';
      $q = $this->db->get_where($tabla, array('id' => $id), 1);
      if ($q->num_rows() > 0) {
        $row = $q->row();
        return $row;
      }
      return FALSE;
    }

    public function getWappsiContabilidadOperationPrefix($field)
    {
      $tabla = 'settings';
      $q = $this->db->get($tabla, 1);
      if ($q->num_rows() > 0) {
        $row = $q->row();
        return $row->{$field};
      }
      return FALSE;
    }

    public function updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo)
    {
      $tabla = 'entries_con'.$contabilidadSufijo;
      $this->db->update($tabla, array('dr_total' => $drTotal, 'cr_total' => $crTotal), array('id' => $entryId));
      return FALSE;
    }

    public function cleanContaReference($reference){

        if (strpos($reference, "-")) {
            $rarr = explode("-", $reference);
            $reference = $rarr[1];
        }

        // for ($i="a"; $i <= "z"; $i++) {
        //     $reference = str_replace($i, "", $reference);
        //     $reference = str_replace(mb_strtoupper($i), "", $reference);
        // }

        $reference = str_replace("-", "", $reference);
        $reference = str_replace("_", "", $reference);
        $reference = str_replace("/", "", $reference);

        return $reference;

    }

    public function wappsiContabilidadCompras($purchase_id, $data = array(), $items = array(), $payments = array(), $data_taxrate_traslate = array(), $prev_reference = null, $has_payment_retention = false, $from_edit = false)
    {
      if($this->wappsiContabilidadVerificacion($data['date']) > 0)
      {
        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }
        $settings_con = $this->getSettingsCon();
        $account_parameter_method = $settings_con->account_parameter_method;
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = NULL;
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = $data['grand_total'];
        $wappsiEntri['cr_total'] = $data['grand_total'];
        $wappsiEntri['notes'] = "Factura de Compra ".$data['reference_no']." ".($this->Settings->management_consecutive_suppliers == 1 ? lang('consecutive_supplier')." : ".$data['consecutive_supplier'] : "");
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['supplier_id'];
        $wappsiEntri['user_id'] = $data['created_by'];
        //Actualizar wap_entrytypes
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        $data['wappsiEntri'] = $wappsiEntri;
        if ($entryId = $this->site->getEntryTypeNumberExisting($data, false, false, $prev_reference, $from_edit)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            // insert in the table entries_con
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        // En el caso de las compras
        // Se van a crear 2 array
        // * IVA
        // * Inventario
        $entryItemIva = array();
        $entryItemIpoconsumo = array();
        $entryItemInventario = array();
        $entryItemEnvio = array();
        $amountForPay = 0;
        foreach ($items as $item){
            if ($account_parameter_method == 2) {
                $item_d = $this->getProductByID($item['product_id']);
                $item['category_id'] = $item_d->category_id;
            }
            if ($item['item_tax_2'] > 0) {
                if (isset($entryItemIpoconsumo[$item['tax_rate_2_id']])) {
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['amount'] += $item['item_tax_2'];
                } else {
                    $ipoconsumo_ledger = $this->getWappsiLedgerId(2, 'Impto consumo compra', $item['tax_rate_2_id'], $contabilidadSufijo, $account_parameter_method);
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['amount'] = $item['item_tax_2'];
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['entry_id'] =  $entryId;
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['dc'] = 'D';
                    $tax_2_details = $this->site->getTaxRate2ByID($item['tax_rate_2_id']);
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['narration'] = 'Segundo impuesto '.($tax_2_details ? $tax_2_details->code : "");
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['base'] = "";
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['companies_id'] = $wappsiEntri['companies_id'];
                    $entryItemIpoconsumo[$item['tax_rate_2_id']]['ledger_id'] = $ipoconsumo_ledger;
                    if (!$this->validate_account_parametrization($ipoconsumo_ledger, "Segundo impuesto", $entryId, $contabilidadSufijo)) return false;
                }
            }
          /* Tratamiento Array de Iva */
          $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
          $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
          if(isset($entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){
            $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + $this->sma->formatDecimal($item['item_tax']);
          }
          else{
            $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $this->sma->formatDecimal($item['item_tax']);
          }
          $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "D";
          $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Iva ";
          if(isset($entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'])){
            $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] + ($this->sma->formatDecimal($item['net_unit_cost']) * $item['unit_quantity']);
          }
          else{
            $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = $this->sma->formatDecimal($item['net_unit_cost']) * $item['unit_quantity'];
          }
          $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
          /* Termina tratamiento Array de Iva */

          /* Tratamiento Array de Inventario */
          $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
          $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
          if(isset($entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){
            $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + ($this->sma->formatDecimal($item['net_unit_cost']) * $item['unit_quantity']);
          }
          else{
            $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $this->sma->formatDecimal($item['net_unit_cost']) * $item['unit_quantity'];
          }
          $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "D";
          $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Inventario ";
          $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = "";
          $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
          /* Termina tratamiento Array de Costo */
          $amountForPay = $amountForPay + (($this->sma->formatDecimal($item['net_unit_cost']) * $item['unit_quantity'])+$this->sma->formatDecimal($item['item_tax'])+$this->sma->formatDecimal($item['item_tax_2']));
        }
        /* Los ID's de los tipos de IVA encontrados en la transacción. */
        $taxRatesIds = array_keys($entryItemInventario);

        /* Buscar del ledger ID */
        foreach ($taxRatesIds as $taxRatesId) {
          $ledgerIdIVA = $this->getWappsiLedgerId(2, 'IVA', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
          $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;
          $ledgerIdInventario = $this->getWappsiLedgerId(2, 'Inventario', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
          $entryItemInventario[$taxRatesId]['ledger_id'] = $ledgerIdInventario;
          // Buscar el nombre de la tax_id in the table tax_rates
          // $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);
          if ($account_parameter_method == 1) {
            // Buscar el nombre de la tax_id in the table tax_rates
            // Lo aplica en los campos narration correspondientes de los 4 arrays
            $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);
          } else if ($account_parameter_method == 2) {
            $tax_details = $this->site->getCategoryByID($taxRatesId, $contabilidadSufijo);
          }
          // Lo aplica en los campos narration correspondientes de los 4 arrays
          $entryItemIva[$taxRatesId]['narration'] .= isset($tax_details) && $tax_details ? $tax_details->name : NULL;
          $entryItemInventario[$taxRatesId]['narration'] .= isset($tax_details) && $tax_details ? $tax_details->name : NULL;
          if ($entryItemIva[$taxRatesId]['amount'] > 0) {
            if (!$this->validate_account_parametrization($ledgerIdIVA, "Iva", $entryId, $contabilidadSufijo)) return false;
          }
          if (!$this->validate_account_parametrization($ledgerIdInventario, "Inventario", $entryId, $contabilidadSufijo)) return false;
        }

        /* TRABAJANDO CON LOS DESCUENTOS */
        $entryDiscounts = array();
        if($data['order_discount'] > 0){
          // $this->sma->print_arrays($data);
          $ledgerPago = $this->getWappsiLedgerId(2, 'Descuento', 0, $contabilidadSufijo, $account_parameter_method);
          $entryDiscount['entry_id'] =  $entryId;
          $entryDiscount['amount'] = $data['order_discount'];
          $entryDiscount['dc'] = 'C';
          $entryDiscount['narration'] = 'Descuento de Factura de compra';
          $entryDiscount['base'] = "";
          $entryDiscount['companies_id'] = $wappsiEntri['companies_id'];
          $entryDiscount['ledger_id'] = $ledgerPago;
          if (!$this->validate_account_parametrization($ledgerPago, "Descuento", $entryId, $contabilidadSufijo)) return false;
          $entryDiscounts[] = $entryDiscount;
          $amountForPay = $amountForPay - $data['order_discount'];
        }
        /* TRABAJANDO CON LOS PAGOS */
        // Si la longitud del array de payments es 0 significa que la venta se hizo a credito y no se ha recibido ningun pago
        // $entryPayments = array();
        $entryPayments = array();
        $entryPayment = array();
        $totalPayments = 0;
        $saldoPorPagar = 0;
        $paymentMethodCon = "";
        /* Tratamiento Array de Envíos */
        if ($data['shipping'] > 0) {
            $entryItemEnvio[0]['entry_id'] = $entryId;
            $entryItemEnvio[0]['amount'] = $data['shipping'];
            $entryItemEnvio[0]['dc'] = 'D';
            $entryItemEnvio[0]['narration'] = 'Envío';
            $entryItemEnvio[0]['base'] = '';
            $entryItemEnvio[0]['companies_id'] = $wappsiEntri['companies_id'];
            $ledgerIdEnvio = $this->getWappsiLedgerId(2, 'Envio compra', 0, $contabilidadSufijo);
            $entryItemEnvio[0]['ledger_id'] = $ledgerIdEnvio;
            if (!$this->validate_account_parametrization($ledgerIdEnvio, "Envio compra", $entryId, $contabilidadSufijo)) return false;
            $amountForPay += $data['shipping'];
        }
        if ($data['prorated_shipping_cost'] > 0) {
            $entryItemEnvio[0]['entry_id'] = $entryId;
            $entryItemEnvio[0]['amount'] = $data['prorated_shipping_cost'];
            $entryItemEnvio[0]['dc'] = 'D';
            $entryItemEnvio[0]['narration'] = 'Envío';
            $entryItemEnvio[0]['base'] = '';
            $entryItemEnvio[0]['companies_id'] = $wappsiEntri['companies_id'];
            $ledgerIdEnvio = $this->getWappsiLedgerId(2, 'Envio compra', 0, $contabilidadSufijo);
            $entryItemEnvio[0]['ledger_id'] = $ledgerIdEnvio;
            if (!$this->validate_account_parametrization($ledgerIdEnvio, "Envio compra", $entryId, $contabilidadSufijo)) return false;
            $amountForPay += $data['prorated_shipping_cost'];
        }
        /* Termina tratamiento Arrao de Envíos */
        //TRATAMIENTO RETENCIONES SOBRE COMPRAS
        if (!$has_payment_retention && (isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']))) {
            if ($data['rete_fuente_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_fuente_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Fuente '.($data['rete_fuente_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_fuente_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                if (!$this->validate_account_parametrization($data['rete_fuente_account'], 'Rete Fuente '.($data['rete_fuente_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_fuente_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                if ($data['rete_fuente_assumed'] == 1) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_fuente_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente Asumida '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_assumed_account'];
                    if (!$this->validate_account_parametrization($data['rete_fuente_assumed_account'], 'Rete Fuente Asumida '.$data['rete_fuente_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                } else {
                    $amountForPay-=$data['rete_fuente_total'];
                }
            }
            if ($data['rete_iva_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_iva_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete IVA '.($data['rete_iva_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_iva_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_iva_account'];
                if (!$this->validate_account_parametrization($data['rete_iva_account'], 'Rete iva Asumida '.$data['rete_iva_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                if ($data['rete_iva_assumed'] == 1) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_iva_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete IVA Asumida '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_assumed_account'];
                    if (!$this->validate_account_parametrization($data['rete_iva_assumed_account'], 'Rete iva Asumida '.$data['rete_iva_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                } else {
                    $amountForPay-=$data['rete_iva_total'];
                }
            }
            if ($data['rete_ica_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_ica_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete ICA '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_ica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_ica_account'];
                if (!$this->validate_account_parametrization($data['rete_ica_account'], 'Rete ica Asumida '.$data['rete_ica_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                if ($data['rete_ica_assumed'] == 1) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_ica_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ICA Asumida '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_assumed_account'];
                    if (!$this->validate_account_parametrization($data['rete_ica_assumed_account'], 'Rete ica Asumida '.$data['rete_ica_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                } else {
                    $amountForPay-=$data['rete_ica_total'];
                }
            }
            if ($data['rete_other_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_other_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Otros '.($data['rete_other_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_other_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_other_account'];
                if (!$this->validate_account_parametrization($data['rete_other_account'], 'Rete other Asumida '.$data['rete_other_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                if ($data['rete_other_assumed'] == 1) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_other_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Otros Asumida '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_assumed_account'];
                    if (!$this->validate_account_parametrization($data['rete_other_assumed_account'], 'Rete other Asumida '.$data['rete_other_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                } else {
                    $amountForPay-=$data['rete_other_total'];
                }
            }
            if ($data['rete_bomberil_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_bomberil_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Tasa bomberil '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_bomberil_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_bomberil_account'];
                if (!$this->validate_account_parametrization($data['rete_bomberil_account'], 'Rete bomberil Asumida '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                if ($data['rete_ica_assumed'] == 1) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_bomberil_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa bomberil Asumida '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_assumed_account'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_assumed_account'], 'Rete bomberil Asumida '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                } else {
                    $amountForPay-=$data['rete_bomberil_total'];
                }
            }
            if ($data['rete_autoaviso_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_autoaviso_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Tasa auto avisos y tableros '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_autoaviso_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_autoaviso_account'];
                if (!$this->validate_account_parametrization($data['rete_autoaviso_account'], 'Rete autoaviso Asumida '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                if ($data['rete_ica_assumed'] == 1) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa auto avisos y tableros '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_assumed_account'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_assumed_account'], 'Rete autoaviso Asumida '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                } else {
                    $amountForPay-=$data['rete_autoaviso_total'];
                }
            }
        }
        if(count($payments) > 0){
            if(!isset($payments[0])){
                $aux = $payments;
                unset($payments);
                $payments[0] = $aux;
            }
            if ($data_taxrate_traslate && count($data_taxrate_traslate) > 0 && $account_parameter_method == 1) { //TRASLADO DE IVA
                $ledgerPago = $data_taxrate_traslate['tax_rate_traslate_ledger_id'];
                foreach ($data_taxrate_traslate['purchase_taxes'] as $tax => $valor) {
                    $total_iva = $valor;
                    // $paid_amount -= $total_iva;
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $total_iva;
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$data['reference_no'];
                    $entryPayment['base'] = "";
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $ledgerPago;
                    $entryPayments[] = $entryPayment;
                    $ledgerIdIVA = $this->getWappsiLedgerId(2, 'IVA', $tax, $contabilidadSufijo);
                    // $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $total_iva;
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$data['reference_no'];
                    $entryPayment['base'] = "";
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $ledgerIdIVA;
                    $entryPayments[] = $entryPayment;
                }
            }
            foreach ($payments as $payment) {
                $paymentMethodCon = $payment['paid_by'];
                $ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $payment['amount'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Factura de compra '.$data['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                if (!$this->validate_account_parametrization($ledgerPago, "Método de pago", $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                $totalPayments = $totalPayments + $payment['amount'];
                $amountForPay -= $totalPayments;
            }
            if ($amountForPay > 0) {
               /* TRABAJO CON EL PAGO DE LA FACTURA */
                $paymentMethodCon = 'Credito';
                $ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $amountForPay;
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Factura de compra '.$data['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                if (!$this->validate_account_parametrization($ledgerPago, "Método de pago Contrapartida Credito", $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                $creditor_ledger_id = $ledgerPago;
            }  
        } else {
            /* TRABAJO CON EL PAGO DE LA FACTURA */
            if (isset($data['due_payment_method_id']) && $data['due_payment_method_id']) {
                $pmCredito = $this->getPaymentMethodById($data['due_payment_method_id']);
                $paymentMethodCon = $pmCredito->code;
            } else {
                $billerDataAdd = $this->getAllCompaniesWithState('biller', $data['biller_id']);
                if ($billerDataAdd->default_credit_payment_method) {
                    $paymentMethodCon = $billerDataAdd->default_credit_payment_method;
                } else {
                    $paymentMethodCon = 'Credito';
                }
            }
            $ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
            $entryPayment['entry_id'] =  $entryId;
            $entryPayment['amount'] = $amountForPay;
            $entryPayment['dc'] = 'C';
            $entryPayment['narration'] = 'Factura de compra '.$data['reference_no'];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $ledgerPago;
            if (!$this->validate_account_parametrization($ledgerPago, "Método de pago Contrapartida Credito", $entryId, $contabilidadSufijo)) return false;
            $entryPayments[] = $entryPayment;
            $creditor_ledger_id = $ledgerPago;
        }
        if (isset($creditor_ledger_id)) {
            $this->db->update('purchases', array('credit_ledger_id' => $creditor_ledger_id), array('id' => $purchase_id));
        }
        /* TRABAJANDO CON LOS DESCUENTOS */
        if($data['order_discount'] < 0){
          // $this->sma->print_arrays($data);
          $ledgerPago = $this->getWappsiLedgerId(2, 'Descuento', 0, $contabilidadSufijo, $account_parameter_method);
          $entryPayment['entry_id'] =  $entryId;
          $entryPayment['amount'] = $data['order_discount'];
          $entryPayment['dc'] = 'C';
          $entryPayment['narration'] = 'Descuento';
          $entryPayment['base'] = "";
          $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
          $entryPayment['ledger_id'] = $ledgerPago;
          if (!$this->validate_account_parametrization($ledgerPago, "Descuento", $entryId, $contabilidadSufijo)) return false;
          $entryPayments[] = $entryPayment;
          $totalPayments = $totalPayments + $data['order_discount'];
        }
        // Uniendo todos los array en uno solo para la inserción
        $entryItems = array_merge($entryItemIva, $entryItemInventario, $entryItemIpoconsumo, $entryDiscounts, $entryPayments, $entryItemEnvio);
        /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items
        para actualizar en la tabla entries_con los campos dr_total y cr_total */
        $drTotal = 0;
        $crTotal = 0;
        // Insertar los entry items
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            if (isset($data['cost_center_id'])) {
                $entryItem['cost_center_id'] = $data['cost_center_id'];
            }
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function wappsiContabilidadComprasDevolucion($purchase_id, $data = array(), $items = array(), $payments = array(), $from_edit = false, $si_return = true)
    {
        if($this->wappsiContabilidadVerificacion($data['date']) > 0){

            if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
                return false;
            }

            $contabilidadSufijo = $this->session->userdata('accounting_module');

            $settings_con = $this->getSettingsCon();

            $account_parameter_method = $settings_con->account_parameter_method;

            $wappsiEntri = array();
            $wappsiEntri['tag_id'] = null;
            $wappsiEntri['entrytype_id'] = 0;

            $label = $this->getWappsiContabilidadOperationPrefix("returnp_prefix");
            $position2 = strpos($data['reference_no'], '-');
            if($position2 !== false){
                $label = substr($data['reference_no'], 0,$position2);
            }
            $number = $this->cleanContaReference($data['reference_no']);
            // exit(var_dump($label));
            //Se va a buscar el label en la tabla wap_entrytypes
            $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
            $wappsiEntri['number'] = $number;
            $wappsiEntri['date'] = substr($data['date'], 0, 10);
            $wappsiEntri['dr_total'] = $data['grand_total'] * -1;
            $wappsiEntri['cr_total'] = $data['grand_total'] * -1;
            $wappsiEntri['consecutive_supplier'] = $data['consecutive_supplier'];
            $wappsiEntri['notes'] = "Devolucion afecta Factura ".$data['return_purchase_ref'];
            $wappsiEntri['state'] = 1;
            $wappsiEntri['companies_id'] = $data['supplier_id'];
            $wappsiEntri['user_id'] = $data['created_by'];
            //Actualizar wap_entrytypes
            // $this->sma->print_arrays($wappsiEntri);
            $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
            // insert in the table entries_con
            // $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            if ($entryId = $this->site->getEntryTypeNumberExisting($data, false, true, null, $from_edit)) {
                $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
            } else {
                // insert in the table entries_con
                $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            }

            $entryItemIva = array();
            $entryItemInventario = array();
            $entryItemIpoconsumo = array();
            $entryItemDescuento = array();
            $amountForPay = 0;

            // $this->sma->print_arrays($items);
            foreach ($items as $item){

                if ($account_parameter_method == 2) {
                    $item_d = $this->getProductByID($item['product_id']);
                    $item['category_id'] = $item_d->category_id;
                }

                if ($item['item_tax_2'] < 0) {
                    if (isset($entryItemIpoconsumo['ipoconsumo'])) {
                        $entryItemIpoconsumo['ipoconsumo']['amount'] += ($item['item_tax_2'] * -1);
                    } else {
                        $ipoconsumo_ledger = $this->getWappsiLedgerId(2, 'Impto consumo compra', $item['tax_rate_2_id'], $contabilidadSufijo, $account_parameter_method);
                        $entryItemIpoconsumo['ipoconsumo']['amount'] = ($item['item_tax_2'] * -1);
                        $entryItemIpoconsumo['ipoconsumo']['entry_id'] =  $entryId;
                        $entryItemIpoconsumo['ipoconsumo']['dc'] = 'C';
                        $entryItemIpoconsumo['ipoconsumo']['narration'] = 'Impoconsumo';
                        $entryItemIpoconsumo['ipoconsumo']['base'] = "";
                        $entryItemIpoconsumo['ipoconsumo']['companies_id'] = $wappsiEntri['companies_id'];
                        $entryItemIpoconsumo['ipoconsumo']['ledger_id'] = $ipoconsumo_ledger;
                    }
                }

                /* Tratamiento Array de Iva */
                $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
                $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
                if(isset($entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + ($this->sma->formatDecimal($item['item_tax']) * -1);
                }
                else{
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = ($this->sma->formatDecimal($item['item_tax']) * -1);
                }
                $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "C";
                $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Iva ";
                if(isset($entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'])){
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] + ($this->sma->formatDecimal($item['net_unit_cost']) * ($item['quantity'] * -1) );
                }
                else{
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = $this->sma->formatDecimal($item['net_unit_cost']) * ($item['quantity'] * -1);
                }
                $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
                /* Termina tratamiento Array de Iva */

                /* Tratamiento Array de Inventario */
                $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
                $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
                if(isset($entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + ($this->sma->formatDecimal($item['net_unit_cost']) * ($item['quantity'] * -1));
                }
                else{
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $this->sma->formatDecimal($item['net_unit_cost']) * ($item['quantity'] * -1);
                }
                $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "C";
                $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Inventario ";
                $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = "";
                $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
                /* Termina tratamiento Array de Inventario */
                $amountForPay = $amountForPay + (($this->sma->formatDecimal($item['net_unit_cost']) * $item['quantity'])+$this->sma->formatDecimal($item['item_tax']));
            }
            // $this->sma->print_arrays(array_merge($entryItemInventario));
            // $this->sma->print_arrays(array_merge($entryItemCosto));
            // $this->sma->print_arrays(array_merge($entryItemIva));
            // $this->sma->print_arrays(array_merge($entryItemDevolucion));

            /* Los ID's de los tipos de IVA encontrados en la transacción. */
            $taxRatesIds = array_keys($entryItemIva);

            /* Buscar del ledger ID */
            foreach ($taxRatesIds as $taxRatesId){
                $ledgerIdIVA = $this->getWappsiLedgerId(2, 'Iva Dev compra', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                if (!$ledgerIdIVA) {
                    $ledgerIdIVA = $this->getWappsiLedgerId(2, 'Iva', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                }
                $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;

                $ledgerIdInventario = $this->getWappsiLedgerId(2, 'Dev compra', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                $entryItemInventario[$taxRatesId]['ledger_id'] = $ledgerIdInventario;

                // Buscar el nombre de la tax_id in the table tax_rates
                // $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);

                  if ($account_parameter_method == 1) {
                    // Buscar el nombre de la tax_id in the table tax_rates
                    // Lo aplica en los campos narration correspondientes de los 4 arrays
                    $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);
                  } else if ($account_parameter_method == 2) {
                    $tax_details = $this->site->getCategoryByID($taxRatesId, $contabilidadSufijo);
                  }

                // Lo aplica en los campos narration correspondientes de los 4 arrays
                // $entryItemDevolucion[$taxRatesId]['narration'] .= $tax_details->name;
                $entryItemIva[$taxRatesId]['narration'] .= $tax_details->name;
                $entryItemInventario[$taxRatesId]['narration'] .= $tax_details->name;

                if ($entryItemIva[$taxRatesId]['amount'] != 0) {
                    if (!$this->validate_account_parametrization($ledgerIdIVA, "Iva Dev compra", $entryId, $contabilidadSufijo)) return false;
                }
                if (!$this->validate_account_parametrization($ledgerIdInventario, "Dev compra Inventario", $entryId, $contabilidadSufijo)) return false;
            }

            /* TRABAJANDO CON LOS PAGOS */
            // Si la longitud del array de payments es 0 significa que la venta se hizo a credito y no se ha recibido ningun pago
            $entryPayments = array();
            $entryPayment = array();
            $totalPayments = 0;
            $saldoPorPagar = 0;
            $amountForPay = 0;

            if(count($payments) > 0){
                if(!isset($payments[0])){
                    $aux = $payments;
                    unset($payments);
                    $entryPayments = array();
                    $payments[0] = $aux;
                }
                foreach ($payments as $payment){
                    if ($payment['paid_by'] == 'retencion' || (isset($payment['note']) && strpos($payment['note'], 'Retención posterior')) !== false) {
                        continue;
                    } else if ($payment['paid_by'] == 'discount') {
                        $ledgerPago = $payment['discount_ledger_id'];
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = $payment['note'];
                    } else {
                        if (isset($data['due_payment_method_id']) && $data['due_payment_method_id'] && $payment['paid_by'] == 'due') {
                            $pmCredito = $this->getPaymentMethodById($data['due_payment_method_id']);
                            $paymentMethodCon = $pmCredito->code;
                        } else if ($payment['paid_by'] == 'due') {
                            $billerDataAdd = $this->getAllCompaniesWithState('biller', $data['biller_id']);
                            if ($billerDataAdd->default_credit_payment_method) {
                                $paymentMethodCon = $billerDataAdd->default_credit_payment_method;
                            } else {
                                $paymentMethodCon = 'Credito';
                            }
                        } else {
                            $paymentMethodCon = $payment['paid_by'];
                        }
                        $ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = 'Factura de compra '.$data['reference_no'];
                    }
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['dc'] = 'D';
                    $entryPayment['base'] = "";
                    $entryPayment['ledger_id'] = $ledgerPago;
                    if (!$this->validate_account_parametrization($ledgerPago, "Pago directo", $entryId, $contabilidadSufijo)) return false;
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayments[] = $entryPayment;
                    $amountForPay = $amountForPay + ($payment['amount'] * -1);
                }
            }

            if ((isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']))) {

                if ($data['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_fuente_total']);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente '.($data['rete_fuente_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                    if (!$this->validate_account_parametrization($data['rete_fuente_account'], 'Rete Fuente '.($data['rete_fuente_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_fuente_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    if ($data['rete_fuente_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = ($data['rete_fuente_total']);
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete Fuente Asumida '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                        $entryPayment['base'] = $data['rete_fuente_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $data['rete_fuente_assumed_account'];
                        if (!$this->validate_account_parametrization($data['rete_fuente_assumed_account'], 'Rete Fuente Asumida '.$data['rete_fuente_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$data['rete_fuente_total'];
                    }
                }

                if ($data['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_iva_total']);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete IVA '.($data['rete_iva_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_account'];
                    if (!$this->validate_account_parametrization($data['rete_iva_account'], 'Rete iva '.($data['rete_iva_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_iva_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    if ($data['rete_iva_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = ($data['rete_iva_total']);
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete IVA Asumida '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                        $entryPayment['base'] = $data['rete_iva_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $data['rete_iva_assumed_account'];
                        if (!$this->validate_account_parametrization($data['rete_iva_assumed_account'], 'Rete IVA Asumida '.$data['rete_iva_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$data['rete_iva_total'];
                    }
                }

                if ($data['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_ica_total']);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ICA '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_account'];
                    if (!$this->validate_account_parametrization($data['rete_ica_account'], 'Rete ica '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_ica_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    if ($data['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = ($data['rete_ica_total']);
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete ICA Asumida '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                        $entryPayment['base'] = $data['rete_ica_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $data['rete_ica_assumed_account'];
                        if (!$this->validate_account_parametrization($data['rete_ica_assumed_account'], 'Rete ICA Asumida '.$data['rete_ica_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$data['rete_ica_total'];
                    }
                }

                if ($data['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_other_total']);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Otras '.($data['rete_other_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_account'];
                    if (!$this->validate_account_parametrization($data['rete_other_account'], 'Rete other '.($data['rete_other_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_other_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    if ($data['rete_other_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = ($data['rete_other_total']);
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete Otras Asumida '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                        $entryPayment['base'] = $data['rete_other_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $data['rete_other_assumed_account'];
                        if (!$this->validate_account_parametrization($data['rete_other_assumed_account'], 'Rete Otras Asumida '.$data['rete_other_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$data['rete_other_total'];
                    }
                }

                if ($data['rete_bomberil_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_bomberil_total']);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa Bomberil '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_account'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_account'], 'Tasa Bomberil '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    if ($data['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = ($data['rete_bomberil_total']);
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Tasa Bomberil Asumida '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'];
                        $entryPayment['base'] = $data['rete_bomberil_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $data['rete_bomberil_assumed_account'];
                        if (!$this->validate_account_parametrization($data['rete_bomberil_assumed_account'], 'Tasa Bomberil Asumida '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$data['rete_bomberil_total'];
                    }
                }

                if ($data['rete_autoaviso_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_autoaviso_total']);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_account'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_account'], 'Tasa Auto Avisos y Tableros '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    if ($data['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = ($data['rete_autoaviso_total']);
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Tasa Auto Avisos y tableros Asumida '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'];
                        $entryPayment['base'] = $data['rete_autoaviso_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $data['rete_autoaviso_assumed_account'];
                        if (!$this->validate_account_parametrization($data['rete_autoaviso_assumed_account'], 'Tasa Auto Avisos y tableros Asumida '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$data['rete_autoaviso_total'];
                    }
                }

            }

            //ACÁ CE RETENCIONES

            if (isset($si_return['ce_retentions_data'])) {
                foreach ($si_return['ce_retentions_data'] as $ce_reference => $ce_data) {
                    if (isset($ce_data['rete_fuente_total']) && $ce_data['rete_fuente_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $ce_data['rete_fuente_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Retención posterior, rete fuente '.$ce_reference;
                        $entryPayment['base'] = $ce_data['rete_fuente_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ce_data['rete_fuente_account'];
                        $entryPayments[] = $entryPayment;
                        if ($ce_data['rete_fuente_assumed'] == 1) {
                            $entryPayment['entry_id'] =  $entryId;
                            $entryPayment['amount'] = ($ce_data['rete_fuente_total']);
                            $entryPayment['dc'] = 'C';
                            $entryPayment['narration'] = 'Rete Fuente Asumida '.$ce_data['rete_fuente_percentage'].'% '.$ce_reference;
                            $entryPayment['base'] = $ce_data['rete_fuente_base'];
                            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                            $entryPayment['ledger_id'] = $ce_data['rete_fuente_assumed_account'];
                            if (!$this->validate_account_parametrization($ce_data['rete_fuente_assumed_account'], 'Rete Fuente Asumida '.$ce_data['rete_fuente_percentage'].'% '.$ce_reference, $entryId, $contabilidadSufijo)) return false;
                            $entryPayments[] = $entryPayment;
                        } else {
                            $amountForPay+=$ce_data['rete_fuente_total'];
                        }
                    }
                    if (isset($ce_data['rete_iva_total']) && $ce_data['rete_iva_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $ce_data['rete_iva_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Retención posterior, rete iva '.$ce_reference;
                        $entryPayment['base'] = $ce_data['rete_iva_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ce_data['rete_iva_account'];
                        $entryPayments[] = $entryPayment;
                        if ($ce_data['rete_iva_assumed'] == 1) {
                            $entryPayment['entry_id'] =  $entryId;
                            $entryPayment['amount'] = ($ce_data['rete_iva_total']);
                            $entryPayment['dc'] = 'C';
                            $entryPayment['narration'] = 'Rete iva Asumida '.$ce_data['rete_iva_percentage'].'% '.$ce_reference;
                            $entryPayment['base'] = $ce_data['rete_iva_base'];
                            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                            $entryPayment['ledger_id'] = $ce_data['rete_iva_assumed_account'];
                            if (!$this->validate_account_parametrization($ce_data['rete_iva_assumed_account'], 'Rete iva Asumida '.$ce_data['rete_iva_percentage'].'% '.$ce_reference, $entryId, $contabilidadSufijo)) return false;
                            $entryPayments[] = $entryPayment;
                        } else {
                            $amountForPay+=$ce_data['rete_iva_total'];
                        }
                    }
                    if (isset($ce_data['rete_ica_total']) && $ce_data['rete_ica_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $ce_data['rete_ica_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Retención posterior, rete ica '.$ce_reference;
                        $entryPayment['base'] = $ce_data['rete_ica_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ce_data['rete_ica_account'];
                        $entryPayments[] = $entryPayment;
                        if ($ce_data['rete_ica_assumed'] == 1) {
                            $entryPayment['entry_id'] =  $entryId;
                            $entryPayment['amount'] = ($ce_data['rete_ica_total']);
                            $entryPayment['dc'] = 'C';
                            $entryPayment['narration'] = 'Rete ica Asumida '.$ce_data['rete_ica_percentage'].'% '.$ce_reference;
                            $entryPayment['base'] = $ce_data['rete_ica_base'];
                            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                            $entryPayment['ledger_id'] = $ce_data['rete_ica_assumed_account'];
                            if (!$this->validate_account_parametrization($ce_data['rete_ica_assumed_account'], 'Rete ica Asumida '.$ce_data['rete_ica_percentage'].'% '.$ce_reference, $entryId, $contabilidadSufijo)) return false;
                            $entryPayments[] = $entryPayment;
                        } else {
                            $amountForPay+=$ce_data['rete_ica_total'];
                        }
                    }
                    if (isset($ce_data['rete_other_total']) && $ce_data['rete_other_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $ce_data['rete_other_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Retención posterior, rete other '.$ce_reference;
                        $entryPayment['base'] = $ce_data['rete_other_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ce_data['rete_other_account'];
                        $entryPayments[] = $entryPayment;
                        if ($ce_data['rete_other_assumed'] == 1) {
                            $entryPayment['entry_id'] =  $entryId;
                            $entryPayment['amount'] = ($ce_data['rete_other_total']);
                            $entryPayment['dc'] = 'C';
                            $entryPayment['narration'] = 'Rete other Asumida '.$ce_data['rete_other_percentage'].'% '.$ce_reference;
                            $entryPayment['base'] = $ce_data['rete_other_base'];
                            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                            $entryPayment['ledger_id'] = $ce_data['rete_other_assumed_account'];
                            if (!$this->validate_account_parametrization($ce_data['rete_other_assumed_account'], 'Rete other Asumida '.$ce_data['rete_other_percentage'].'% '.$ce_reference, $entryId, $contabilidadSufijo)) return false;
                            $entryPayments[] = $entryPayment;
                        } else {
                            $amountForPay+=$ce_data['rete_other_total'];
                        }
                    }
                    if (isset($ce_data['rete_autoaviso_total']) && $ce_data['rete_autoaviso_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $ce_data['rete_autoaviso_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Retención posterior, rete autoaviso '.$ce_reference;
                        $entryPayment['base'] = $ce_data['rete_autoaviso_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ce_data['rete_autoaviso_account'];
                        $entryPayments[] = $entryPayment;
                        if ($ce_data['rete_ica_assumed'] == 1) {
                            $entryPayment['entry_id'] =  $entryId;
                            $entryPayment['amount'] = ($ce_data['rete_autoaviso_total']);
                            $entryPayment['dc'] = 'C';
                            $entryPayment['narration'] = 'Rete autoaviso Asumida '.$ce_data['rete_autoaviso_percentage'].'% '.$ce_reference;
                            $entryPayment['base'] = $ce_data['rete_autoaviso_base'];
                            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                            $entryPayment['ledger_id'] = $ce_data['rete_autoaviso_assumed_account'];
                            if (!$this->validate_account_parametrization($ce_data['rete_autoaviso_assumed_account'], 'Rete autoaviso Asumida '.$ce_data['rete_autoaviso_percentage'].'% '.$ce_reference, $entryId, $contabilidadSufijo)) return false;
                            $entryPayments[] = $entryPayment;
                        } else {
                            $amountForPay+=$ce_data['rete_autoaviso_total'];
                        }
                    }
                    if (isset($ce_data['rete_bomberil_total']) && $ce_data['rete_bomberil_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $ce_data['rete_bomberil_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Retención posterior, rete bomberil '.$ce_reference;
                        $entryPayment['base'] = $ce_data['rete_bomberil_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ce_data['rete_bomberil_account'];
                        $entryPayments[] = $entryPayment;
                        if ($ce_data['rete_ica_assumed'] == 1) {
                            $entryPayment['entry_id'] =  $entryId;
                            $entryPayment['amount'] = ($ce_data['rete_bomberil_total']);
                            $entryPayment['dc'] = 'C';
                            $entryPayment['narration'] = 'Rete bomberil Asumida '.$ce_data['rete_bomberil_percentage'].'% '.$ce_reference;
                            $entryPayment['base'] = $ce_data['rete_bomberil_base'];
                            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                            $entryPayment['ledger_id'] = $ce_data['rete_bomberil_assumed_account'];
                            if (!$this->validate_account_parametrization($ce_data['rete_bomberil_assumed_account'], 'Rete bomberil Asumida '.$ce_data['rete_bomberil_percentage'].'% '.$ce_reference, $entryId, $contabilidadSufijo)) return false;
                            $entryPayments[] = $entryPayment;
                        } else {
                            $amountForPay+=$ce_data['rete_bomberil_total'];
                        }
                    }
                }
            }

            // No hay registro de pagos ylas compras siempre son credito
            // // $this->sma->print_arrays($data);
            $paymentMethodCon = 'Credito';
            $ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
            $entryPayment['entry_id'] =  $entryId;
            $entryPayment['amount'] = ($data['grand_total'] * -1) - $amountForPay;
            $entryPayment['dc'] = 'D';
            $entryPayment['narration'] = 'Afecta factura '.$data['reference_no'];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $ledgerPago;
            if (!$this->validate_account_parametrization($ledgerPago, "Contra partida de pago", $entryId, $contabilidadSufijo)) return false;
            $entryPayments[] = $entryPayment;

            /* TRABAJANDO CON LOS DESCUENTOS */
            $entryDiscounts = array();

            // $this->sma->print_arrays($data);
            if($data['order_discount'] < 0){
              $ledgerPago = $this->getWappsiLedgerId(2, 'Descuento', 0, $contabilidadSufijo, $account_parameter_method);
              $entryDiscount['entry_id'] =  $entryId;
              $entryDiscount['amount'] = ($data['order_discount'] * -1);
              $entryDiscount['dc'] = 'D';
              $entryDiscount['narration'] = 'Descuento de Factura de compra';
              $entryDiscount['base'] = "";
              $entryDiscount['companies_id'] = $wappsiEntri['companies_id'];
              $entryDiscount['ledger_id'] = $ledgerPago;
              if (!$this->validate_account_parametrization($ledgerPago, 'Descuento de Factura de compra', $entryId, $contabilidadSufijo)) return false;
              $entryDiscounts[] = $entryDiscount;
              $amountForPay = $amountForPay - $data['order_discount'];
            }

             /* Tratamiento Array de Envíos */
            $entryEnvios = array();
            if ($data['shipping'] < 0) {
                $entryItemEnvio['entry_id'] = $entryId;
                $entryItemEnvio['amount'] = ($data['shipping'] * -1);
                $entryItemEnvio['dc'] = 'C';
                $entryItemEnvio['narration'] = 'Envío';
                $entryItemEnvio['base'] = '';
                $entryItemEnvio['companies_id'] = $wappsiEntri['companies_id'];
                $ledgerIdEnvio = $this->getWappsiLedgerId(2, 'Envio compra', 0, $contabilidadSufijo);
                $entryItemEnvio['ledger_id'] = $ledgerIdEnvio;
                if (!$this->validate_account_parametrization($ledgerIdEnvio, 'Envio compra', $entryId, $contabilidadSufijo)) return false;
                $amountForPay += $data['shipping'];
                // exit($amountForPay);
                $entryEnvios[] = $entryItemEnvio;
            }
            if ($data['prorated_shipping_cost'] < 0) {
                $entryItemEnvio['entry_id'] = $entryId;
                $entryItemEnvio['amount'] = ($data['prorated_shipping_cost'] * -1);
                $entryItemEnvio['dc'] = 'C';
                $entryItemEnvio['narration'] = 'Envío';
                $entryItemEnvio['base'] = '';
                $entryItemEnvio['companies_id'] = $wappsiEntri['companies_id'];
                $ledgerIdEnvio = $this->getWappsiLedgerId(2, 'Envio compra', 0, $contabilidadSufijo);
                $entryItemEnvio['ledger_id'] = $ledgerIdEnvio;
                if (!$this->validate_account_parametrization($ledgerIdEnvio, 'Envio compra', $entryId, $contabilidadSufijo)) return false;
                $amountForPay += $data['prorated_shipping_cost'];
                // exit($amountForPay);
                $entryEnvios[] = $entryItemEnvio;
            }

            // /* Uniendo todos los array en uno solo para la inserción */
            $entryItems = array_merge($entryItemIva, $entryItemInventario, $entryItemIpoconsumo, $entryPayments, $entryDiscounts, $entryEnvios);
            // $this->sma->print_arrays($entryItems);

            /* Se va a realizar la suma de los debitos y los creditos mientras se recorre
            el array para la inserción de los items para actualizar en la tabla entries_con
            los campos dr_total y cr_total */

            $drTotal = 0;
            $crTotal = 0;

            // Insertar los entry items
            foreach ($entryItems as $entryItem){
                if($entryItem['amount'] >= 0.01){
                    if (isset($data['cost_center_id'])) {
                        $entryItem['cost_center_id'] = $data['cost_center_id'];
                    }
                    $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                    if( $entryItem['dc'] == 'D' ){
                    $drTotal = $drTotal + $entryItem['amount'];
                    }else if( $entryItem['dc'] == 'C' ){
                    $crTotal = $crTotal + $entryItem['amount'];
                    }
                }
            }
            $totals["drTotal"]=$drTotal;
            $totals["crTotal"]=$crTotal;
            //$this->sma->print_arrays($totals);

            // Actualizar los totales
            $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);

            if( $data['payment_status'] == 'pending' ){
                //Traer datos de la compra
                $purchase = $this->getWappsiPurchaseById($data['purchase_id']);
                //$this->sma->print_arrays($data);

                // Actualizar el campo paid en la compra
                $paid = $purchase->paid + ($data['grand_total'] * -1);
                // $this->sma->print_arrays($paid);
                $tabla = 'purchases';
                $this->db->update($tabla, array('paid' => $paid), array('id' => $data['purchase_id']));
                $this->db->update($tabla, array('payment_status' => 'paid'), array('id' => $purchase_id));


                // $this->sma->print_arrays($data);
                // Insertando registro en la tabla de payments
                // $prefix = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
                $tabla = 'payments';
                $payment = array();
                $payment['date'] = $data['date'];
                $payment['purchase_id'] = $data['purchase_id'];
                $payment['amount'] = ($data['grand_total'] * -1);
                $payment['reference_no'] = "Devolución ".$data['return_purchase_ref'];
                $payment['paid_by'] = "due";
                $payment['type'] = "devolución";
                $payment['note'] = "Devolución factura Credito ".$data['reference_no'];
                //$this->sma->print_arrays($payment);
                $this->db->insert($tabla,$payment);
            }
            return TRUE;
        } /* Termina el if que valida la existencia del modulo de contabilidad. */
        return FALSE;
    }

    public function wappsiContabilidadVentas($sale_id, $data = array(), $items = array(), $payments = array(), $cost = array(), $data_taxrate_traslate = array(), $item_costing_each = array(), $has_payment_retention = false, $from_edit = false)

        {
        if($this->wappsiContabilidadVerificacion($data['date']) > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $settings_con = $this->getSettingsCon();
            $account_parameter_method = $settings_con->account_parameter_method;
            if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
                return false;
            }
            // Apartir de aqui ejecuta todas las consultas de contabilidad
            // $this->sma->print_arrays($data);
            $wappsiEntri = array();
            $wappsiEntri['tag_id'] = null;
            $wappsiEntri['entrytype_id'] = 0;
            //Buscando si el inicio de la cadena es FV/POS para saber si es el prefijo por defecto de las ventas POS
            $label = '';
            $number = 0;
            $position = strpos($data['reference_no'], 'FV/POS');
            if($position !== false){
              $label = "FV";
              $number = substr($data['reference_no'], 6);
            }
            else{
              $position2 = strpos($data['reference_no'], '-');
              if($position2 !== false){
                $label = substr($data['reference_no'], 0,$position2);
                $position2 = $position2 + 1;
                $number = substr($data['reference_no'], $position2);
              }
            }
            //Se va a buscar el label en la tabla wap_entrytypes
            $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
            $wappsiEntri['number'] = $number;
            $wappsiEntri['date'] = substr($data['date'], 0, 10);
            $wappsiEntri['dr_total'] = $data['grand_total'];
            $wappsiEntri['cr_total'] = $data['grand_total'];
            $wappsiEntri['notes'] = "Factura de venta ".$data['reference_no'];
            $wappsiEntri['state'] = 1;
            $wappsiEntri['companies_id'] = $data['customer_id'];
            $wappsiEntri['user_id'] = $data['created_by'];
            //Actualizar wap_entrytypes
            $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
            if ($entryId = $this->site->getEntryTypeNumberExisting($data, false, false, null, $from_edit)) {
                $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
            } else {
                // insert in the table entries_con
                $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            }
            // Se van a leer cada uno de los items para asociar por tax_rate
            // Se van a crear 4 array
            // * Ingreso
            // * IVA
            // * Costo
            // * Inventario
            $entryItemIngresos = array();
            $entryItemIva = array();
            $entryItemIpoconsumo = array();
            $entryItemCosto = array();
            $entryItemInventario = array();
            $entryItemEnvio = array();
            $entryItemPropina = array();
            $amountForPay = 0;
            $taxes_d = [];
            foreach ($items as $item){
                $item_d = $this->getProductByID($item['product_id']);
                if ($account_parameter_method == 2) {
                    $item['category_id'] = $item_d->category_id;
                }
                if (isset($item['item_tax_2']) && $item['item_tax_2'] > 0) {
                    // $amount = $item['item_tax_2'] > 0 ? $item['item_tax_2'] : ($item['consumption_sales_costing'] * $item['quantity']);
                    $amount = $item['item_tax_2'] > 0 ? $item['item_tax_2'] : 0;
                    if (isset($entryItemIpoconsumo['ipoconsumo'])) {
                        $entryItemIpoconsumo['ipoconsumo']['amount'] += $amount;
                    } else {
                        $ipoconsumo_ledger = $this->getWappsiLedgerId(1, 'Impto consumo vta', NULL, $contabilidadSufijo, $account_parameter_method);
                        $entryItemIpoconsumo['ipoconsumo']['amount'] = $amount;
                        $entryItemIpoconsumo['ipoconsumo']['entry_id'] =  $entryId;
                        $entryItemIpoconsumo['ipoconsumo']['dc'] = 'C';
                        $entryItemIpoconsumo['ipoconsumo']['narration'] = 'Impoconsumo';
                        $entryItemIpoconsumo['ipoconsumo']['base'] = "";
                        $entryItemIpoconsumo['ipoconsumo']['companies_id'] = $wappsiEntri['companies_id'];
                        $entryItemIpoconsumo['ipoconsumo']['ledger_id'] = $ipoconsumo_ledger;
                        if (!$this->validate_account_parametrization($ipoconsumo_ledger, "Impoconsumo", $entryId, $contabilidadSufijo)) return false;
                    }
                } else if (isset($item['consumption_sales_costing']) && $item['consumption_sales_costing'] > 0) {
                    $amount = $item['consumption_sales_costing'];
                    if (isset($entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id])) {
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['amount'] += ($item['consumption_sales_costing'] * $item['quantity']);
                    } else {
                        $ipoconsumo_ledger = $this->getWappsiLedgerId(2, 'Impto consumo compra', $item_d->purchase_tax_rate_2_id, $contabilidadSufijo, $account_parameter_method);
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['amount'] = ($item['consumption_sales_costing'] * $item['quantity']);
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['entry_id'] =  $entryId;
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['dc'] = 'C';
                        $tax_2_details = $this->site->getTaxRate2ByID($item_d->purchase_tax_rate_2_id);
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['narration'] = 'Segundo impuesto registrado para compra '.($tax_2_details ? $tax_2_details->code : "");
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['base'] = "";
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['companies_id'] = $wappsiEntri['companies_id'];
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['ledger_id'] = $ipoconsumo_ledger;
                        if (!$this->validate_account_parametrization($ipoconsumo_ledger, "Segundo impuesto", $entryId, $contabilidadSufijo)) return false;
                    }
                }

                $trid_item = $account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id'];

                /* Tratamiento Array de Ingresos */
                $entryItemIngresos[$trid_item] ['entry_id'] = $entryId;
                $entryItemIngresos[$trid_item] ['tax_rate_id'] = $trid_item;
                if(isset($entryItemIngresos[$trid_item] ['amount'])){
                    $entryItemIngresos[$trid_item] ['amount'] = $entryItemIngresos[$trid_item] ['amount'] + ($this->sma->formatDecimal($item['net_unit_price']) * $item['quantity']);
                } else {
                    $entryItemIngresos[$trid_item] ['amount'] = $this->sma->formatDecimal($item['net_unit_price']) * $item['quantity'];
                }
                $entryItemIngresos[$trid_item] ['dc'] = "C";
                // $entryItemIngresos[$trid_item] ['narration'] = "Ingresos ";
                $entryItemIngresos[$trid_item] ['base'] = "";
                $entryItemIngresos[$trid_item] ['companies_id'] = $wappsiEntri['companies_id'];
                /* Termina tratamiento Array de Ingresos */

                /* Tratamiento Array de Iva */
                if ($item['item_tax'] > 0) {
                    $entryItemIva[$trid_item] ['entry_id'] = $entryId;
                    $entryItemIva[$trid_item] ['tax_rate_id'] = $trid_item;
                    if(isset($entryItemIva[$trid_item] ['amount'])){
                        $entryItemIva[$trid_item] ['amount'] = $entryItemIva[$trid_item] ['amount'] + $this->sma->formatDecimal($item['item_tax']);
                    } else {
                        $entryItemIva[$trid_item] ['amount'] = $this->sma->formatDecimal($item['item_tax']);
                    }
                    $entryItemIva[$trid_item]['dc'] = "C";
                    // $entryItemIva[$trid_item]['narration'] = "Iva ";
                    if(isset($entryItemIva[$trid_item] ['base'])){
                        $entryItemIva[$trid_item] ['base'] = $entryItemIva[$trid_item] ['base'] + ($this->sma->formatDecimal($item['net_unit_price']) * $item['quantity']);
                    } else {
                        $entryItemIva[$trid_item] ['base'] = $this->sma->formatDecimal($item['net_unit_price']) * $item['quantity'];
                    }
                    $entryItemIva[$trid_item] ['companies_id'] = $wappsiEntri['companies_id'];
                }
                /* Termina tratamiento Array de Iva */
                if ($this->Settings->accounting_method == 1) { //FIFO
                    if (!isset($taxes_d[$trid_item] )) {
                        $taxes_d[$trid_item]  = 1;
                    }
                    if (isset($item_costing_each[$item['product_id']])) {
                        $cost_item_costing_each = 0;
                        $cnt = 0;
                        foreach ($item_costing_each[$item['product_id']] as $ice) {
                            if (!isset($ice['date'])) {
                                $ice = $ice[0];
                            }
                            $cost_item_costing_each = $ice['purchase_net_unit_cost'] * $ice['quantity'];
                            $entryItemCosto[$cnt][$trid_item] ['entry_id'] = $entryId;
                            $entryItemCosto[$cnt][$trid_item] ['tax_rate_id'] = $trid_item;
                            if(isset($entryItemCosto[$cnt][$trid_item] ['amount'])){
                                $entryItemCosto[$cnt][$trid_item] ['amount'] = $entryItemCosto[$cnt][$trid_item] ['amount'] + $cost_item_costing_each;
                            }
                            else{
                                $entryItemCosto[$cnt][$trid_item] ['amount'] = $cost_item_costing_each;
                            }
                            if ($this->Settings->ipoconsumo && isset($item['consumption_sales_costing']) && $item['consumption_sales_costing'] > 0) {
                                $entryItemCosto[$cnt][$trid_item] ['amount'] += ($item['consumption_sales_costing'] * $item['quantity']);
                            }
                            $entryItemCosto[$cnt][$trid_item] ['dc'] = "D";
                            $entryItemCosto[$cnt][$trid_item] ['narration'] = "Costo  (".$this->sma->formatMoney($ice['purchase_net_unit_cost']).") ";
                            $entryItemCosto[$cnt][$trid_item] ['base'] = "";
                            $entryItemCosto[$cnt][$trid_item] ['companies_id'] = $wappsiEntri['companies_id'];
                            /* Termina tratamiento Array de Costo */
                            /* Tratamiento Array de Inventario */
                            $entryItemInventario[$cnt][$trid_item] ['entry_id'] = $entryId;
                            $entryItemInventario[$cnt][$trid_item] ['tax_rate_id'] = $trid_item;
                            if(isset($entryItemInventario[$cnt][$trid_item] ['amount'])){
                            $entryItemInventario[$cnt][$trid_item] ['amount'] = $entryItemInventario[$cnt][$trid_item] ['amount'] + $cost_item_costing_each;
                            }
                            else{
                            $entryItemInventario[$cnt][$trid_item] ['amount'] = $cost_item_costing_each;
                            }
                            $entryItemInventario[$cnt][$trid_item] ['dc'] = "C";
                            $entryItemInventario[$cnt][$trid_item] ['narration'] = "Inventario (".$this->sma->formatMoney($ice['purchase_net_unit_cost']).") ";
                            $entryItemInventario[$cnt][$trid_item] ['base'] = "";
                            $entryItemInventario[$cnt][$trid_item] ['companies_id'] = $wappsiEntri['companies_id'];
                            /* Termina tratamiento Array de Costo */

                            $cnt++;

                        }

                    }

                    $amountForPay = $amountForPay + (($this->sma->formatDecimal($item['net_unit_price']) * $item['quantity'])+$this->sma->formatDecimal($item['item_tax']));

                    /* Los ID's de los tipos de IVA encontrados en la transacción. */
                    $taxRatesIds = array_keys($taxes_d);

                    // $this->sma->print_arrays($taxRatesIds);
                    /* Buscar del ledger ID */
                    foreach ($taxRatesIds as $taxRatesId) {

                        $ledgerIdIngreso = $this->getWappsiLedgerId(1, 'Ingreso', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                        $entryItemIngresos[$taxRatesId]['ledger_id'] = $ledgerIdIngreso;

                        $ledgerIdIVA = $this->getWappsiLedgerId(1, 'IVA', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                        $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;
                        $ledgerIdCosto = $this->getWappsiLedgerId(1, 'Costo', $taxRatesId, $contabilidadSufijo, $account_parameter_method);

                        if ($account_parameter_method == 1) {
                            // Buscar el nombre de la tax_id in the table tax_rates
                            // Lo aplica en los campos narration correspondientes de los 4 arrays
                            $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);
                        } else if ($account_parameter_method == 2) {
                            $tax_details = $this->site->getCategoryByID($taxRatesId, $contabilidadSufijo);
                        }

                        foreach ($entryItemCosto as $nrow => $eic) {
                            $entryItemCosto[$nrow][$taxRatesId]['ledger_id'] = $ledgerIdCosto;
                            if (!isset($entryItemCosto[$nrow][$taxRatesId]['narration'])) {
                                $entryItemCosto[$nrow][$taxRatesId]['narration'] .= $tax_details->name;
                            }
                        }

                        $ledgerIdInventario = $this->getWappsiLedgerId(1, 'Inventario', $taxRatesId, $contabilidadSufijo, $account_parameter_method);

                        foreach ($entryItemInventario as $nrow => $eii) {
                            $entryItemInventario[$nrow][$taxRatesId]['ledger_id'] = $ledgerIdInventario;
                            if (!isset($entryItemInventario[$nrow][$taxRatesId]['narration'])) {
                                $entryItemInventario[$nrow][$taxRatesId]['narration'] .= $tax_details->name;
                            }
                        }

                        if ($tax_details) {
                            $entryItemIngresos[$taxRatesId]['narration'] .= $tax_details->name;
                            $entryItemIva[$taxRatesId]['narration'] .= $tax_details->name;
                        }

                        if (!$this->validate_account_parametrization($ledgerIdIngreso, "Ingreso ".($tax_details ? $tax_details->name : $taxRatesId), $entryId, $contabilidadSufijo)) return false;
                        if (!$this->validate_account_parametrization($ledgerIdIVA, "Iva ".($tax_details ? $tax_details->name : $taxRatesId), $entryId, $contabilidadSufijo)) return false;
                        if (!$this->validate_account_parametrization($ledgerIdInventario, "Inventario ".($tax_details ? $tax_details->name : $taxRatesId), $entryId, $contabilidadSufijo)) return false;

                    }
                } else if ($this->Settings->accounting_method == 2) { //AVCO



                    /* Buscando en el array multidimensional de cost el costo neto para el item que estamos revisando */
                    $purchaseNetUnitCost = 0;

                    foreach ($cost as $costSub) {
                        foreach ($costSub as $costSubSub) {
                            if($costSubSub['product_id'] == $item['product_id']){
                                if(isset($costSubSub['purchase_net_unit_cost']) && $costSubSub['purchase_net_unit_cost'] > 0){
                                    $purchaseNetUnitCost = $costSubSub['purchase_net_unit_cost'];
                                }
                            } else if (isset($costSubSub['preparation_id_origin']) && $costSubSub['preparation_id_origin'] == $item['product_id']) {
                                /* ACÁ COSTO PREPARACION */
                                $purchaseNetUnitCost += ($costSubSub['purchase_net_unit_cost'] *  $costSubSub['quantity']);
                            }
                        }
                    }


                    /* Termina busqueda en el array multidimensional de cost el costo neto para el item que estamos revisando */

                    /* Tratamiento Array de Costo */
                    if ($item_d->type != 'service') {
                        $entryItemCosto[$trid_item] ['entry_id'] = $entryId;
                        $entryItemCosto[$trid_item] ['tax_rate_id'] = $trid_item;

                        if(isset($entryItemCosto[$trid_item] ['amount'])){
                            $entryItemCosto[$trid_item] ['amount'] = $entryItemCosto[$trid_item] ['amount'] + ($purchaseNetUnitCost * $item['quantity']);
                        } else {
                            $entryItemCosto[$trid_item] ['amount'] = $purchaseNetUnitCost * $item['quantity'];
                        }

                        if ($this->Settings->ipoconsumo && isset($item['consumption_sales_costing']) && $item['consumption_sales_costing'] > 0) {
                            $entryItemCosto[$trid_item] ['amount'] += ($item['consumption_sales_costing'] * $item['quantity']);
                        }

                        $entryItemCosto[$trid_item] ['dc'] = "D";
                        // $entryItemCosto[$trid_item] ['narration'] = "Costo ";
                        $entryItemCosto[$trid_item] ['base'] = "";
                        $entryItemCosto[$trid_item] ['companies_id'] = $wappsiEntri['companies_id'];
                    }
                    /* Termina tratamiento Array de Costo */

                    /* Tratamiento Array de Inventario */
                    if ($item_d->type != 'service') {
                        $entryItemInventario[$trid_item] ['entry_id'] = $entryId;
                        $entryItemInventario[$trid_item] ['tax_rate_id'] = $trid_item;
                        if(isset($entryItemInventario[$trid_item] ['amount'])){
                            $entryItemInventario[$trid_item] ['amount'] = $entryItemInventario[$trid_item] ['amount'] + ($purchaseNetUnitCost * $item['quantity']);
                        } else {
                            $entryItemInventario[$trid_item] ['amount'] = $purchaseNetUnitCost * $item['quantity'];
                        }
                        $entryItemInventario[$trid_item] ['dc'] = "C";
                        // $entryItemInventario[$trid_item] ['narration'] = "Inventario ";
                        $entryItemInventario[$trid_item] ['base'] = "";
                        $entryItemInventario[$trid_item] ['companies_id'] = $wappsiEntri['companies_id'];
                    }

                    /* Termina tratamiento Array de Costo */

                    $amountForPay = $amountForPay + (($this->sma->formatDecimal($item['net_unit_price']) * $item['quantity'])+$this->sma->formatDecimal($item['item_tax'])+$this->sma->formatDecimal(isset($item['item_tax_2']) ? $item['item_tax_2'] : 0));

                    /* Los ID's de los tipos de IVA encontrados en la transacción. */
                    $taxRatesIds = array_keys($entryItemIngresos);


                    /* Buscar del ledger ID */
                    foreach ($taxRatesIds as $taxRatesId) {
                        if ($account_parameter_method == 1) {
                            // Buscar el nombre de la tax_id in the table tax_rates
                            // Lo aplica en los campos narration correspondientes de los 4 arrays
                            $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);
                        } else if ($account_parameter_method == 2) {
                            $tax_details = $this->site->getCategoryByID($taxRatesId, $contabilidadSufijo);
                        }
                        if (isset($entryItemIngresos[$taxRatesId])) {
                            $ledgerIdIngreso = $this->getWappsiLedgerId(1, 'Ingreso', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                            $entryItemIngresos[$taxRatesId]['ledger_id'] = $ledgerIdIngreso;
                            if (!$this->validate_account_parametrization($ledgerIdIngreso, "Ingreso ".($tax_details ? $tax_details->name : $taxRatesId), $entryId, $contabilidadSufijo)) return false;
                        }

                        if (isset($entryItemIva[$taxRatesId])) {
                            $ledgerIdIVA = $this->getWappsiLedgerId(1, 'IVA', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                            $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;
                            if (isset($entryItemIva[$taxRatesId]['amount']) && $entryItemIva[$taxRatesId]['amount'] > 0) {
                                if (!$this->validate_account_parametrization($ledgerIdIVA, "Iva".($tax_details ? $tax_details->name : $taxRatesId), $entryId, $contabilidadSufijo)) return false;
                            }
                        }

                        if (isset($entryItemCosto[$taxRatesId])) {
                            $ledgerIdCosto = $this->getWappsiLedgerId(1, 'Costo', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                            $entryItemCosto[$taxRatesId]['ledger_id'] = $ledgerIdCosto;
                        }

                        if (isset($entryItemInventario[$taxRatesId])) {
                            $ledgerIdInventario = $this->getWappsiLedgerId(1, 'Inventario', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                            $entryItemInventario[$taxRatesId]['ledger_id'] = $ledgerIdInventario;
                            if (!$this->validate_account_parametrization($ledgerIdInventario, "Inventario ".($tax_details ? $tax_details->name : $taxRatesId), $entryId, $contabilidadSufijo)) return false;
                        }

                        if ($account_parameter_method == 1) {
                            // Buscar el nombre de la tax_id in the table tax_rates
                            // Lo aplica en los campos narration correspondientes de los 4 arrays
                            $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);
                        } else if ($account_parameter_method == 2) {
                            $tax_details = $this->site->getCategoryByID($taxRatesId, $contabilidadSufijo);
                        }

                        if ($tax_details) {
                            if (!isset($entryItemIngresos[$taxRatesId]['narration'])) {
                                $entryItemIngresos[$taxRatesId]['narration'] = "Ingresos ".$tax_details->name;
                            }
                            if (!isset($entryItemIva[$taxRatesId]['narration'])) {
                                $entryItemIva[$taxRatesId]['narration'] = "Impuesto ".$tax_details->name;
                            }
                            if (!isset($entryItemCosto[$taxRatesId]['narration'])) {
                                $entryItemCosto[$taxRatesId]['narration'] = "Costo ".$tax_details->name;
                            }
                            if (!isset($entryItemInventario[$taxRatesId]['narration'])) {
                                $entryItemInventario[$taxRatesId]['narration'] = "Inventario ".$tax_details->name;
                            }
                        }
                    }
                }
            }//foreach

            /* TRABAJANDO CON LOS PAGOS */
            // Si la longitud del array de payments es 0 significa que la venta se hizo a credito y no se ha recibido ningun pago
            $entryPayments = array();
            $entryPayment = array();
            $totalPayments = 0;
            $saldoPorPagar = 0;
            $paymentMethodCon = "";

            /* Tratamiento Array de Envíos */
            $shipping = isset($data['shipping']) ? $data['shipping'] : 0;
            if ($shipping > 0 && (isset($data['pos']) && $data['pos'] != 1 || (isset($data['pos']) && $data['pos'] == 1 && isset($data['shipping_in_grand_total']) && $data['shipping_in_grand_total'] == 1))) {
                $entryItemEnvio[0]['entry_id'] = $entryId;
                $entryItemEnvio[0]['amount'] = $shipping;
                $entryItemEnvio[0]['dc'] = 'C';
                $entryItemEnvio[0]['narration'] = 'Envío';
                $entryItemEnvio[0]['base'] = '';
                $entryItemEnvio[0]['companies_id'] = $wappsiEntri['companies_id'];
                $ledgerIdEnvio = $this->getWappsiLedgerId(1, 'Envio vta', 0, $contabilidadSufijo, $account_parameter_method);
                $entryItemEnvio[0]['ledger_id'] = $ledgerIdEnvio;
                $amountForPay += $shipping;
                if (!$this->validate_account_parametrization($ledgerIdEnvio, "Envío", $entryId, $contabilidadSufijo)) return false;
                // exit($amountForPay);
            }
            /* Termina tratamiento Arrao de Envíos */

            /* Tratamiento Array de Propina */
            if (isset($data['tip_amount']) && $data['tip_amount'] > 0) {
                $entryItemPropina[0]['entry_id'] = $entryId;
                $entryItemPropina[0]['amount'] = $data['tip_amount'];
                $entryItemPropina[0]['dc'] = 'C';
                $entryItemPropina[0]['narration'] = 'Propina de venta';
                $entryItemPropina[0]['base'] = '';
                $entryItemPropina[0]['companies_id'] = $wappsiEntri['companies_id'];
                $ledgerIdPropina = $this->getWappsiLedgerId(1, 'Propina vta', 0, $contabilidadSufijo, $account_parameter_method);
                $entryItemPropina[0]['ledger_id'] = $ledgerIdPropina;
                $amountForPay += $data['tip_amount'];
                if (!$this->validate_account_parametrization($ledgerIdPropina, "Propina", $entryId, $contabilidadSufijo)) return false;
                // exit($amountForPay);
            }
            /* Termina tratamiento Arrao de Propina */


            if(count($payments) > 0){

                if ($data_taxrate_traslate && count($data_taxrate_traslate) > 0 && $account_parameter_method == 1) { //TRASLADO DE IVA

                    $ledgerPago = $data_taxrate_traslate['tax_rate_traslate_ledger_id'];

                    foreach ($data_taxrate_traslate['sale_taxes'] as $tax => $valor) {
                        $total_iva = $valor;
                        // $paid_amount -= $total_iva;
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $total_iva;
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$data['reference_no'];
                        $entryPayment['base'] = "";
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ledgerPago;
                        $entryPayments[] = $entryPayment;

                        $ledgerIdIVA = $this->getWappsiLedgerId(1, 'IVA', $tax, $contabilidadSufijo);
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $total_iva;
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$data['reference_no'];
                        $entryPayment['base'] = "";
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ledgerIdIVA;
                        $entryPayments[] = $entryPayment;
                    }

                }
                // $this->sma->print_arrays($payments);
                //aca pagos
                foreach ($payments as $payment) {
                    if ($payment['paid_by'] == 'retencion') {
                        continue;
                    }

                    $pm_retcom_total = 0;

                    if ($this->Settings->payments_methods_retcom == 1 && isset($payment['pmnt_data'])) {
                        $pm_d = $payment['pmnt_data'];
                        if ((isset($payment['pm_commision_value']) && $payment['pm_commision_value'] != NULL) || $pm_d->commision_value != NULL) {
                            if ((isset($payment['pm_commision_value']) && $payment['pm_commision_value'] != NULL)) {
                                $pm_d->commision_value = $payment['pm_commision_value'];
                            }
                            if (strpos($pm_d->commision_value, '%') !== false) {
                                $p_comm_perc = (Double)(str_replace("%", "", $pm_d->commision_value));
                                $p_comm_base = $data['total'] * ($payment['amount'] / $data['grand_total']);
                                $p_comm_amount = $p_comm_base * ($p_comm_perc / 100);
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_comm_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'Comisión forma de pago '.$pm_d->name.' ('.$pm_d->commision_value.')';
                                $entryPayment['base'] = $p_comm_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->commision_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_comm_amount;
                                if (!$this->validate_account_parametrization($pm_d->commision_ledger_id, 'Comisión forma de pago '.$pm_d->name.' ('.$pm_d->commision_value.')', $entryId, $contabilidadSufijo)) return false;
                            } else {
                                $p_comm_base = $data['total'] * ($payment['amount'] / $data['grand_total']);
                                $p_comm_amount = $pm_d->commision_value;
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_comm_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'Comisión forma de pago '.$pm_d->name;
                                $entryPayment['base'] = $p_comm_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->commision_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_comm_amount;
                                if (!$this->validate_account_parametrization($pm_d->commision_ledger_id, 'Comisión forma de pago '.$pm_d->name, $entryId, $contabilidadSufijo)) return false;
                            }
                        }

                        if ((isset($payment['pm_retefuente_value']) && $payment['pm_retefuente_value'] != NULL) || $pm_d->retefuente_value != NULL) {
                            if ((isset($payment['pm_retefuente_value']) && $payment['pm_retefuente_value'] != NULL)) {
                                $pm_d->retefuente_value = $payment['pm_retefuente_value'];
                            }
                            if (strpos($pm_d->retefuente_value, '%') !== false) {
                                $p_retefuente_perc = (Double)(str_replace("%", "", $pm_d->retefuente_value));
                                $p_retefuente_base = $data['total'] * ($payment['amount'] / $data['grand_total']);
                                $p_retefuente_amount = $p_retefuente_base * ($p_retefuente_perc / 100);
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_retefuente_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'Retefuente forma de pago '.$pm_d->name.' ('.$pm_d->retefuente_value.')';
                                $entryPayment['base'] = $p_retefuente_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->retefuente_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_retefuente_amount;
                                if (!$this->validate_account_parametrization($pm_d->retefuente_ledger_id, 'Retefuente forma de pago '.$pm_d->name.' ('.$pm_d->retefuente_value.')', $entryId, $contabilidadSufijo)) return false;
                            } else {
                                $p_retefuente_base = $data['total'] * ($payment['amount'] / $data['grand_total']);
                                $p_retefuente_amount = $pm_d->retefuente_value;
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_retefuente_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'Retefuente forma de pago '.$pm_d->name;
                                $entryPayment['base'] = $p_retefuente_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->retefuente_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_retefuente_amount;
                                if (!$this->validate_account_parametrization($pm_d->retefuente_ledger_id, 'Retefuente forma de pago '.$pm_d->name, $entryId, $contabilidadSufijo)) return false;
                            }
                        }

                        if ((isset($payment['pm_reteiva_value']) && $payment['pm_reteiva_value'] != NULL) || $pm_d->reteiva_value != NULL) {
                            if ((isset($payment['pm_reteiva_value']) && $payment['pm_reteiva_value'] != NULL)) {
                                $pm_d->reteiva_value = $payment['pm_reteiva_value'];
                            }
                            if (strpos($pm_d->reteiva_value, '%') !== false) {
                                $p_reteiva_perc = (Double)(str_replace("%", "", $pm_d->reteiva_value));
                                $p_reteiva_base = $data['total_tax'] * ($payment['amount'] / $data['grand_total']);
                                $p_reteiva_amount = $p_reteiva_base * ($p_reteiva_perc / 100);
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_reteiva_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'ReteIVA forma de pago '.$pm_d->name.' ('.$pm_d->reteiva_value.')';
                                $entryPayment['base'] = $p_reteiva_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->reteiva_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_reteiva_amount;
                                if (!$this->validate_account_parametrization($pm_d->reteiva_ledger_id, 'ReteIVA forma de pago '.$pm_d->name.' ('.$pm_d->reteiva_value.')', $entryId, $contabilidadSufijo)) return false;
                            } else {
                                $p_reteiva_base = $data['total_tax'] * ($payment['amount'] / $data['grand_total']);
                                $p_reteiva_amount = $pm_d->reteiva_value;
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_reteiva_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'ReteIVA forma de pago '.$pm_d->name;
                                $entryPayment['base'] = $p_reteiva_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->reteiva_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_reteiva_amount;
                                if (!$this->validate_account_parametrization($pm_d->reteiva_ledger_id, 'ReteIVA forma de pago '.$pm_d->name, $entryId, $contabilidadSufijo)) return false;
                            }
                        }

                        if ((isset($payment['pm_reteica_value']) && $payment['pm_reteica_value'] != NULL) || $pm_d->reteica_value != NULL) {
                            if ((isset($payment['pm_reteica_value']) && $payment['pm_reteica_value'] != NULL)) {
                                $pm_d->reteica_value = $payment['pm_reteica_value'];
                            }
                            if (strpos($pm_d->reteica_value, '%') !== false) {
                                $p_reteica_perc = (Double)(str_replace("%", "", $pm_d->reteica_value));
                                $p_reteica_base = $data['total'] * ($payment['amount'] / $data['grand_total']);
                                $p_reteica_amount = $p_reteica_base * ($p_reteica_perc / 100);
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_reteica_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'ReteICA forma de pago '.$pm_d->name.' ('.$pm_d->reteica_value.')';
                                $entryPayment['base'] = $p_reteica_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->reteica_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_reteica_amount;
                                if (!$this->validate_account_parametrization($pm_d->reteica_ledger_id, 'ReteIVA forma de pago '.$pm_d->name.' ('.$pm_d->reteiva_value.')', $entryId, $contabilidadSufijo)) return false;
                            } else {
                                $p_reteica_base = $data['total'] * ($payment['amount'] / $data['grand_total']);
                                $p_reteica_amount = $pm_d->reteica_value;
                                $entryPayment['entry_id'] =  $entryId;
                                $entryPayment['amount'] = $p_reteica_amount;
                                $entryPayment['dc'] = 'D';
                                $entryPayment['narration'] = 'ReteICA forma de pago '.$pm_d->name;
                                $entryPayment['base'] = $p_reteica_base;
                                $entryPayment['companies_id'] = $pm_d->supplier_id;
                                $entryPayment['ledger_id'] = $pm_d->reteica_ledger_id;
                                $entryPayments[] = $entryPayment;
                                $pm_retcom_total += $p_reteica_amount;
                                if (!$this->validate_account_parametrization($pm_d->reteica_ledger_id, 'ReteICA forma de pago '.$pm_d->name, $entryId, $contabilidadSufijo)) return false;
                            }
                        }

                    }

                    $paymentMethodCon = $payment['paid_by'];
                    $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $payment['amount'] - $pm_retcom_total;
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Factura de venta '.$data['reference_no'];
                    $entryPayment['base'] = "";
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $ledgerPago;
                    $entryPayments[] = $entryPayment;
                    $totalPayments = $totalPayments + $payment['amount'];
                    if (!$this->validate_account_parametrization($ledgerPago, 'Forma de pago', $entryId, $contabilidadSufijo)) return false;
                }
            }



            /* TRABAJANDO CON LOS DESCUENTOS */
            if(isset($data['order_discount']) && $data['order_discount'] > 0){
              // $this->sma->print_arrays($data);
              $ledgerPago = $this->getWappsiLedgerId(1, 'Descuento', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = $data['order_discount'];
              $entryPayment['dc'] = 'D';
              $entryPayment['narration'] = 'Descuento';
              $entryPayment['base'] = "";
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              if (!$this->validate_account_parametrization($ledgerPago, 'Descuento general', $entryId, $contabilidadSufijo)) return false;
              $entryPayments[] = $entryPayment;
              $totalPayments = $totalPayments + $data['order_discount'];
            }

            if(isset($data['self_withholding_amount']) && $data['self_withholding_amount'] > 0){
              $ledgerPago = $this->getWappsiLedgerId(1, 'Autorretención Debito', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = $data['self_withholding_amount'];
              $entryPayment['dc'] = 'D';
              $entryPayment['narration'] = 'Autorretención';
              $entryPayment['base'] = $data['total'];
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              $entryPayments[] = $entryPayment;
              if (!$this->validate_account_parametrization($ledgerPago, 'Autorretención', $entryId, $contabilidadSufijo)) return false;

              $ledgerPago = $this->getWappsiLedgerId(1, 'Autorretención Credito', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = $data['self_withholding_amount'];
              $entryPayment['dc'] = 'C';
              $entryPayment['narration'] = 'Autorretención';
              $entryPayment['base'] = $data['total'];
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              $entryPayments[] = $entryPayment;
              if (!$this->validate_account_parametrization($ledgerPago, 'Autorretención Contrapartida', $entryId, $contabilidadSufijo)) return false;
            }

            //TRATAMIENTO RETENCIONES SOBRE VENTAS

            if (!$has_payment_retention && (isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']) || (isset($data['rete_bomberil_total']) && isset($data['rete_bomberil_id'])) || (isset($data['rete_autoaviso_total']) && isset($data['rete_autoaviso_id'])))) {

                if ($data['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_fuente_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                    $entryPayments[] = $entryPayment;
                    $amountForPay -= $data['rete_fuente_total'];
                    if (!$this->validate_account_parametrization($data['rete_fuente_account'], 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                }

                if ($data['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_iva_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_account'];
                    $entryPayments[] = $entryPayment;
                    $amountForPay -= $data['rete_iva_total'];
                    if (!$this->validate_account_parametrization($data['rete_iva_account'], 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                }

                if ($data['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_ica_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_account'];
                    $entryPayments[] = $entryPayment;
                    $amountForPay -= $data['rete_ica_total'];

                    if (!$this->validate_account_parametrization($data['rete_ica_account'], 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                }

                if ($data['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_other_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_account'];
                    $entryPayments[] = $entryPayment;
                    $amountForPay -= $data['rete_other_total'];
                    if (!$this->validate_account_parametrization($data['rete_other_account'], 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                }

                if ($data['rete_bomberil_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_bomberil_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa bomberil '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_account'];
                    $entryPayments[] = $entryPayment;
                    $amountForPay -= $data['rete_bomberil_total'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_account'], 'Tasa bomberil '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                }

                if ($data['rete_autoaviso_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_account'];
                    $entryPayments[] = $entryPayment;
                    $amountForPay -= $data['rete_autoaviso_total'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_account'], 'Tasa Auto Avisos y Tableros '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                }

            }
            /* Si la suma de los pagos es menor que el monto a pagar se debe hacer un registro como credito para el tema de la deuda. */

            $saldoPorPagar =  $this->sma->formatDecimal($amountForPay - $totalPayments);
            // exit(var_dump($this->sma->formatDecimal($amountForPay)));
            if($saldoPorPagar > 0.01 ){
                if (isset($data['due_payment_method_id']) && $data['due_payment_method_id']) {
                    $pmCredito = $this->getPaymentMethodById($data['due_payment_method_id']);
                    $paymentMethodCon = $pmCredito->code;
                } else {
                    $billerDataAdd = $this->getAllCompaniesWithState('biller', $data['biller_id']);
                    if ($billerDataAdd->default_credit_payment_method) {
                        $paymentMethodCon = $billerDataAdd->default_credit_payment_method;
                    } else {
                        $paymentMethodCon = 'Credito';
                    }
                }
                $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $saldoPorPagar;
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Factura de venta '.$data['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayments[] = $entryPayment;
                if (!$this->validate_account_parametrization($ledgerPago, 'Factura de venta Saldo Credito '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
            }
            //$this->sma->print_arrays($entryPayments);

            // Uniendo todos los array en uno solo para la inserción
            // $entryItems = array_merge($entryItemIngresos, $entryItemIva, $entryItemCosto, $entryItemInventario, $entryPayments);

            if (isset($data['rete_autoica_total']) && $data['rete_autoica_total'] > 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_autoica_total'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)';
                $entryPayment['base'] = $data['rete_autoica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_autoica_account'];
                if (!$this->validate_account_parametrization($data['rete_autoica_account'], 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;

                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_autoica_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)';
                $entryPayment['base'] = $data['rete_autoica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_autoica_account_counterpart'];
                if (!$this->validate_account_parametrization($data['rete_autoica_account_counterpart'], 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;

                if (isset($data['rete_bomberil_total']) && $data['rete_bomberil_total'] > 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_bomberil_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_account'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_account'], 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;

                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_bomberil_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_account_counterpart'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_account_counterpart'], 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                }
                if (isset($data['rete_autoaviso_total']) && $data['rete_autoaviso_total'] > 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_account'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_account'], 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;

                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_account_counterpart'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_account_counterpart'], 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                }
            }

            if ($this->Settings->accounting_method == 1) {//FIFO
                $entryItems = array_merge($entryItemIngresos, $entryItemIva, $entryItemIpoconsumo, $entryPayments, $entryItemEnvio, $entryItemPropina);
                foreach ($entryItemCosto as $nrow => $eic) {
                    foreach ($eic as $arreic) {
                        $entryItems[] = $arreic;
                    }
                }
                foreach ($entryItemInventario as $nrow => $eii) {
                    foreach ($eii as $arreii) {
                        $entryItems[] = $arreii;
                    }
                }
            } else if ($this->Settings->accounting_method == 2) { //AVCO
                $entryItems = array_merge($entryItemIngresos, $entryItemIva, $entryItemIpoconsumo, $entryItemCosto, $entryItemInventario, $entryItemEnvio, $entryItemPropina, $entryPayments);
            }

            /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items
            para actualizar en la tabla entries_con los campos dr_total y cr_total */

            $drTotal = 0;
            $crTotal = 0;

            // $this->sma->print_arrays($entryItems);
            // Insertar los entry items
            foreach ($entryItems as $entryItem){
              if(isset($entryItem['amount']) && $entryItem['amount'] > 0){
                if (isset($data['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $data['cost_center_id'];
                }
                // exit(var_dump($entryItem));
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                  $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                  $crTotal = $crTotal + $entryItem['amount'];
                }
              } else {
                if (!isset($entryItem['amount'])) {
                    // $this->sma->print_arrays($entryItem);
                }
              }
            }
            $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
            // $this->sma->print_arrays($entryItems);
            return TRUE;
        } /* Termina el if que valida la existencia del modulo de contabilidad. */
        return FALSE;
    }

    public function wappsiContabilidadVentasDevolucion($sale_id, $data = array(), $items = array(), $payments = array(), $si_return = array(), $cost = array(), $from_edit = false)
    {
        if($this->wappsiContabilidadVerificacion($data['date']) > 0){

            if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
                return false;
            }

            $data['order_discount'] = (isset($data['return_order_discount_not_applied']) ? $data['return_order_discount_not_applied'] : 0) * -1;
            $data['grand_total'] = $data['grand_total'] - (isset($data['return_order_discount_not_applied']) ? $data['return_order_discount_not_applied'] : 0);

            $settings_con = $this->getSettingsCon();
            $account_parameter_method = $settings_con->account_parameter_method;
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            // exit(var_dump($this->session->userdata('accounting_module')));
            $wappsiEntri = array();
            $wappsiEntri['tag_id'] = null;
            $wappsiEntri['entrytype_id'] = 0;
            $label = '';
            $number = 0;
            $position2 = strpos($data['reference_no'], '-');
            if($position2 !== false){
                $label = substr($data['reference_no'], 0,$position2);
            } else {
                $label = $this->getWappsiContabilidadOperationPrefix("return_prefix");
            }
            $number = $this->cleanContaReference($data['reference_no']);
            $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
            $wappsiEntri['number'] = $number;
            $wappsiEntri['date'] = substr($data['date'], 0, 10);
            $wappsiEntri['dr_total'] = $data['grand_total'];
            $wappsiEntri['cr_total'] = $data['grand_total'];
            $wappsiEntri['notes'] = "Devolucion afecta Factura ".$data['return_sale_ref'];
            $wappsiEntri['state'] = 1;
            $wappsiEntri['companies_id'] = $data['customer_id'];
            $wappsiEntri['user_id'] = $data['created_by'];
            $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
            if ($entryId = $this->site->getEntryTypeNumberExisting($data, true, true, null, $from_edit)) {
                $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
            } else {
                $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            }
            $entryItemDevolucion = array();
            $entryItemIva = array();
            $entryItemCosto = array();
            $entryItemIpoconsumo = array();
            $entryItemInventario = array();
            $entryItemDescuento = array();
            $entryItemEnvio = array();
            $amountForPay = 0;
            foreach ($items as $item){
                $item_d = $this->getProductByID($item['product_id']);
                if ($account_parameter_method == 2) {
                    $item['category_id'] = $item_d->category_id;
                }
                if (isset($item['item_tax_2']) && $item['item_tax_2'] != 0) {
                    $amount = $item['item_tax_2'] < 0 ? $item['item_tax_2'] * -1 : $item['item_tax_2'];
                    if (isset($entryItemIpoconsumo['ipoconsumo'])) {
                        $entryItemIpoconsumo['ipoconsumo']['amount'] += $amount;
                    } else {
                        $ipoconsumo_ledger = $this->getWappsiLedgerId(1, 'Impto consumo vta', NULL, $contabilidadSufijo, $account_parameter_method);
                        $entryItemIpoconsumo['ipoconsumo']['amount'] = $amount;
                        $entryItemIpoconsumo['ipoconsumo']['entry_id'] =  $entryId;
                        $entryItemIpoconsumo['ipoconsumo']['dc'] = 'C';
                        $entryItemIpoconsumo['ipoconsumo']['narration'] = 'Impoconsumo';
                        $entryItemIpoconsumo['ipoconsumo']['base'] = "";
                        $entryItemIpoconsumo['ipoconsumo']['companies_id'] = $wappsiEntri['companies_id'];
                        $entryItemIpoconsumo['ipoconsumo']['ledger_id'] = $ipoconsumo_ledger;
                        if (!$this->validate_account_parametrization($ipoconsumo_ledger, "Impoconsumo", $entryId, $contabilidadSufijo)) return false;
                    }
                } else if (isset($item['consumption_sales_costing']) && $item['consumption_sales_costing'] != 0) {
                    $amount = $item['consumption_sales_costing'];
                    if (isset($entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id])) {
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['amount'] += ($amount * $item['quantity']);
                    } else {
                        $ipoconsumo_ledger = $this->getWappsiLedgerId(2, 'Impto consumo compra', $item_d->purchase_tax_rate_2_id, $contabilidadSufijo, $account_parameter_method);
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['amount'] = ($amount * $item['quantity']);
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['entry_id'] =  $entryId;
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['dc'] = 'D';
                        $tax_2_details = $this->site->getTaxRate2ByID($item_d->purchase_tax_rate_2_id);
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['narration'] = 'Segundo impuesto registrado para compra '.($tax_2_details ? $tax_2_details->code : "");
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['base'] = "";
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['companies_id'] = $wappsiEntri['companies_id'];
                        $entryItemIpoconsumo[$item_d->purchase_tax_rate_2_id]['ledger_id'] = $ipoconsumo_ledger;
                        if (!$this->validate_account_parametrization($ipoconsumo_ledger, "Segundo impuesto", $entryId, $contabilidadSufijo)) return false;
                    }
                }
                /* Tratamiento Array de Devolucion */
                $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
                $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
                if(isset($entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){
                    $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + ($this->sma->formatDecimal($item['net_unit_price']) * ($item['quantity'] * -1));
                }
                else{
                    $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $this->sma->formatDecimal($item['net_unit_price']) * ($item['quantity'] * -1);
                }
                $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "D";
                $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Devolucion ";
                $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = "";
                $entryItemDevolucion[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
                /* Termina tratamiento Array de Devolucion */
                /* Tratamiento Array de Iva */
                if ($item['item_tax'] != 0) {
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
                    if(isset($entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){
                        $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + ($this->sma->formatDecimal($item['item_tax']) * -1);
                    }
                    else{
                        $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = ($this->sma->formatDecimal($item['item_tax']) * -1);
                    }
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "D";
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Iva ";
                    if(isset($entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'])){
                        $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] + ($this->sma->formatDecimal($item['net_unit_price']) * ($item['quantity'] * -1) );
                    }
                    else{
                        $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = $this->sma->formatDecimal($item['net_unit_price']) * ($item['quantity'] * -1);
                    }
                    $entryItemIva[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
                }
                /* Termina tratamiento Array de Iva */
                /* Tratamiento Array de Costo */
                if ($item_d->type != 'service') {
                    $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
                    $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
                    /* Buscando en el array multidimensional de cost el costo neto para el item que estamos revisando */
                    $purchaseNetUnitCost = 0;
                    foreach ($cost as $costSub) {
                        foreach ($costSub as $costSubSub) {
                            if($costSubSub['product_id'] == $item['product_id']){
                                if(isset($costSubSub['purchase_net_unit_cost']) && $costSubSub['purchase_net_unit_cost'] > 0){
                                    $purchaseNetUnitCost = $costSubSub['purchase_net_unit_cost'];
                                }
                            }
                        }
                    }
                    /* Termina busqueda en el array multidimensional de cost el costo neto para el item que estamos revisando */
                    if(isset($entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){

                        $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + ($purchaseNetUnitCost * ($item['quantity'] * - 1) );

                    }
                    else{
                        $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $purchaseNetUnitCost * ($item['quantity'] * - 1);

                    }
                    if ($this->Settings->ipoconsumo && isset($item['consumption_sales_costing']) && $item['consumption_sales_costing'] < 0) {
                        $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] += ($item['consumption_sales_costing'] * $item['quantity']);
                    }
                    $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "C";
                    $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Costo ";
                    $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = "";
                    $entryItemCosto[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
                }
                /* Termina tratamiento Array de Costo */
                /* Tratamiento Array de Inventario */
                if ($item_d->type != 'service') {
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['entry_id'] = $entryId;
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['tax_rate_id'] = $item['tax_rate_id'];
                    if(isset($entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'])){
                        $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] + ($purchaseNetUnitCost * ($item['quantity'] * -1));
                    }
                    else{
                        $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['amount'] = $purchaseNetUnitCost * ($item['quantity'] * -1);
                    }
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['dc'] = "D";
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['narration'] = "Inventario ";
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['base'] = "";
                    $entryItemInventario[$account_parameter_method == 1 ? $item['tax_rate_id'] : $item['category_id']] ['companies_id'] = $wappsiEntri['companies_id'];
                    /* Termina tratamiento Array de Inventario */
                    $amountForPay = $amountForPay + (($this->sma->formatDecimal($item['net_unit_price']) * $item['quantity'])+$this->sma->formatDecimal($item['item_tax']));
                }
            }
            /* Los ID's de los tipos de IVA encontrados en la transacción. */
            $taxRatesIds = array_keys($entryItemDevolucion);
            /* Buscar del ledger ID */
            foreach ($taxRatesIds as $taxRatesId){
                $ledgerIdDevolucion = $this->getWappsiLedgerId(1, 'Dev vta', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                if (isset($entryItemDevolucion[$taxRatesId])) {
                    $entryItemDevolucion[$taxRatesId]['ledger_id'] = $ledgerIdDevolucion;
                }
                $ledgerIdIVA = $this->getWappsiLedgerId(1, 'Iva Dev vta', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                if (!$ledgerIdIVA) {
                    $ledgerIdIVA = $this->getWappsiLedgerId(1, 'Iva', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                }
                if (isset($entryItemIva[$taxRatesId])) {
                    $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;
                }
                $ledgerIdCosto = $this->getWappsiLedgerId(1, 'Costo', $taxRatesId, $contabilidadSufijo, $account_parameter_method);
                if (isset($entryItemCosto[$taxRatesId])) {
                    $entryItemCosto[$taxRatesId]['ledger_id'] = $ledgerIdCosto;
                }
                $ledgerIdInventario = $this->getWappsiLedgerId(1, 'Inventario', $taxRatesId, $contabilidadSufijo, $account_parameter_method);

                if (isset($entryItemInventario[$taxRatesId])) {
                    $entryItemInventario[$taxRatesId]['ledger_id'] = $ledgerIdInventario;
                }
                if ($account_parameter_method == 1) {
                    $tax_details = $this->site->getTaxRateByID($taxRatesId, $contabilidadSufijo);
                } else if ($account_parameter_method == 2) {
                    $tax_details = $this->site->getCategoryByID($taxRatesId, $contabilidadSufijo);
                }
                // Lo aplica en los campos narration correspondientes de los 4 arrays
                if (isset($entryItemDevolucion[$taxRatesId]['narration'])) {
                    $entryItemDevolucion[$taxRatesId]['narration'] .= $tax_details ? $tax_details->name : 'Sin iva';
                }
                if (isset($entryItemIva[$taxRatesId]['narration'])) {
                    $entryItemIva[$taxRatesId]['narration'] .= $tax_details ? $tax_details->name : 'Sin iva';
                }
                if (isset($entryItemCosto[$taxRatesId]['narration'])) {
                    $entryItemCosto[$taxRatesId]['narration'] .= $tax_details ? $tax_details->name : 'Sin iva';
                }
                if (isset($entryItemInventario[$taxRatesId]['narration'])) {
                    $entryItemInventario[$taxRatesId]['narration'] .= $tax_details ? $tax_details->name : 'Sin iva';
                }
                if (isset($entryItemIva[$taxRatesId]) && $entryItemIva[$taxRatesId]['amount'] != 0) {
                    if (!$this->validate_account_parametrization($ledgerIdIVA, "Iva", $entryId, $contabilidadSufijo)) return false;
                }
                if (isset($entryItemCosto[$taxRatesId]) && $entryItemCosto[$taxRatesId]['amount'] != 0) {
                    if (!$this->validate_account_parametrization($ledgerIdCosto, "Costo", $entryId, $contabilidadSufijo)) return false;
                }
                if (isset($entryItemInventario[$taxRatesId]) && $entryItemInventario[$taxRatesId]['amount'] != 0) {
                    if (!$this->validate_account_parametrization($ledgerIdInventario, "Inventario", $entryId, $contabilidadSufijo)) return false;
                }
            }
            /* TRABAJANDO CON LOS PAGOS */
            // Si la longitud del array de payments es 0 significa que la venta se hizo a credito y no se ha recibido ningun pago
            $entryPayments = array();
            $entryPayment = array();
            $totalPayments = 0;
            $saldoPorPagar = 0;
            $entryItemEnvio = array();
            $entryItemPropina = array();
            /* Tratamiento Array de Envíos */
            if ($data['shipping'] != 0 && (isset($data['pos']) && $data['pos'] != 1 || (isset($data['pos']) && $data['pos'] == 1 && isset($data['shipping_in_grand_total']) && $data['shipping_in_grand_total'] == 1))) {
                $entryItemEnvio[0]['entry_id'] = $entryId;
                $entryItemEnvio[0]['amount'] = ($data['shipping'] * -1);
                $entryItemEnvio[0]['dc'] = 'D';
                $entryItemEnvio[0]['narration'] = 'Envío';
                $entryItemEnvio[0]['base'] = '';
                $entryItemEnvio[0]['companies_id'] = $wappsiEntri['companies_id'];
                $ledgerIdEnvio = $this->getWappsiLedgerId(1, 'Envio vta', 0, $contabilidadSufijo, $account_parameter_method);
                $entryItemEnvio[0]['ledger_id'] = $ledgerIdEnvio;
                $amountForPay += ($data['shipping'] * -1);
                if (!$this->validate_account_parametrization($ledgerIdEnvio, "Envío", $entryId, $contabilidadSufijo)) return false;
                // exit($amountForPay);
            }
            // exit(var_dump($data['shipping']));
            /* Termina tratamiento Arrao de Envíos */
            /* Tratamiento Array de Propina */
            if (isset($data['tip_amount']) && $data['tip_amount'] != 0) {
                $entryItemPropina[0]['entry_id'] = $entryId;
                $entryItemPropina[0]['amount'] = ($data['tip_amount'] * -1);
                $entryItemPropina[0]['dc'] = 'D';
                $entryItemPropina[0]['narration'] = 'Propina de venta';
                $entryItemPropina[0]['base'] = '';
                $entryItemPropina[0]['companies_id'] = $wappsiEntri['companies_id'];
                $ledgerIdPropina = $this->getWappsiLedgerId(1, 'Propina vta', 0, $contabilidadSufijo, $account_parameter_method);
                $entryItemPropina[0]['ledger_id'] = $ledgerIdPropina;
                $amountForPay += ($data['tip_amount'] * -1);
                if (!$this->validate_account_parametrization($ledgerIdPropina, "Propina", $entryId, $contabilidadSufijo)) return false;
                // exit($amountForPay);
            }
            /* Termina tratamiento Arrao de Propina */
            $entryPayments = array();
            if(count($payments) > 0){
                if(!isset($payments[0])){
                    $aux = $payments;
                    unset($payments);
                    $payments[0] = $aux;
                }
                foreach ($payments as $payment){
                    if ($payment['paid_by'] == 'retencion' || strpos($payment['note'], 'Retención posterior') !== false) {
                        continue;
                    } else if ($payment['paid_by'] == 'discount') {
                        $ledgerPago = $payment['discount_ledger_id'];
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = $payment['note'];
                    } else {
                        if (isset($data['due_payment_method_id']) && $data['due_payment_method_id'] && $payment['paid_by'] == 'due') {
                            $pmCredito = $this->getPaymentMethodById($data['due_payment_method_id']);
                            $paymentMethodCon = $pmCredito->code;
                        } else if ($payment['paid_by'] == 'due') {
                            $billerDataAdd = $this->getAllCompaniesWithState('biller', $data['biller_id']);
                            if ($billerDataAdd->default_credit_payment_method) {
                                $paymentMethodCon = $billerDataAdd->default_credit_payment_method;
                            } else {
                                $paymentMethodCon = 'Credito';
                            }
                        } else {
                            $paymentMethodCon = $payment['paid_by'];
                        }
                        $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = 'Factura de venta '.$data['reference_no'];
                    }
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['dc'] = 'C';
                    $entryPayment['base'] = "";
                    $entryPayment['ledger_id'] = $ledgerPago;
                    if (!$this->validate_account_parametrization($ledgerPago, "Pago", $entryId, $contabilidadSufijo)) return false;
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayments[] = $entryPayment;
                    $totalPayments = $totalPayments + ($payment['amount'] * -1);
                }
            }
            //TRATAMIENTO RETENCIONES SOBRE VENTAS
            $totalRete = 0;
            if ((isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']) || (isset($data['rete_bomberil_total']) && isset($data['rete_bomberil_id'])) || (isset($data['rete_autoaviso_total']) && isset($data['rete_autoaviso_id'])))) {
                if ($data['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_fuente_total']);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                    if (!$this->validate_account_parametrization($data['rete_fuente_account'], 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_fuente_total'];
                }
                if ($data['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_iva_total']);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_account'];
                    if (!$this->validate_account_parametrization($data['rete_iva_account'], 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_iva_total'];
                }
                if ($data['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_ica_total']);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_account'];
                    if (!$this->validate_account_parametrization($data['rete_ica_account'], 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_ica_total'];
                }
                if ($data['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_other_total']);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_account'];
                    if (!$this->validate_account_parametrization($data['rete_other_account'], 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_other_total'];
                }
                if ((isset($data['rete_bomberil_total']) && isset($data['rete_bomberil_id']))) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_bomberil_total']);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Tasa bomberil '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_account'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_account'], 'Tasa bomberil '.$data['rete_bomberil_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_bomberil_total'];
                }
                if ((isset($data['rete_autoaviso_total']) && isset($data['rete_autoaviso_id']))) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_autoaviso_total']);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Tasa Auto Aviso y Tableros '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_account'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_account'], 'Tasa Auto Aviso y Tableros '.$data['rete_autoaviso_percentage'].'% '.$data['reference_no'], $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_autoaviso_total'];
                }
            }
            //tratamiento retenciones posteriores ACÁ QUEDÉ
            // $this->sma->print_arrays($si_return);
            if (isset($si_return[0]['rc_retentions_data'])) {
                foreach ($si_return[0]['rc_retentions_data'] as $rc_reference => $rc_data) {
                    if (isset($rc_data['rete_fuente_total']) && $rc_data['rete_fuente_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $rc_data['rete_fuente_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Retención posterior, rete fuente '.$rc_reference;
                        $entryPayment['base'] = $rc_data['rete_fuente_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $rc_data['rete_fuente_account'];
                        $entryPayments[] = $entryPayment;
                    }
                    if (isset($rc_data['rete_iva_total']) && $rc_data['rete_iva_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $rc_data['rete_iva_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Retención posterior, rete iva '.$rc_reference;
                        $entryPayment['base'] = $rc_data['rete_iva_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $rc_data['rete_iva_account'];
                        $entryPayments[] = $entryPayment;
                    }
                    if (isset($rc_data['rete_ica_total']) && $rc_data['rete_ica_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $rc_data['rete_ica_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Retención posterior, rete ica '.$rc_reference;
                        $entryPayment['base'] = $rc_data['rete_ica_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $rc_data['rete_ica_account'];
                        $entryPayments[] = $entryPayment;
                    }
                    if (isset($rc_data['rete_other_total']) && $rc_data['rete_other_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $rc_data['rete_other_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Retención posterior, rete other '.$rc_reference;
                        $entryPayment['base'] = $rc_data['rete_other_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $rc_data['rete_other_account'];
                        $entryPayments[] = $entryPayment;
                    }
                    if (isset($rc_data['rete_autoaviso_total']) && $rc_data['rete_autoaviso_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $rc_data['rete_autoaviso_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Retención posterior, rete autoaviso '.$rc_reference;
                        $entryPayment['base'] = $rc_data['rete_autoaviso_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $rc_data['rete_autoaviso_account'];
                        $entryPayments[] = $entryPayment;
                    }
                    if (isset($rc_data['rete_bomberil_total']) && $rc_data['rete_bomberil_total'] > 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $rc_data['rete_bomberil_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Retención posterior, rete bomberil '.$rc_reference;
                        $entryPayment['base'] = $rc_data['rete_bomberil_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $rc_data['rete_bomberil_account'];
                        $entryPayments[] = $entryPayment;
                    }
                }
            }


            /* Si la suma de los pagos es menor que el monto a pagar se debe hacer un registro como credito para el tema de la deuda. */
            if( $data['payment_status'] == 'pending' || $data['payment_status'] == 'due' || (isset($payments[0]) && $data['payment_status'] == 'paid' && $payments[0] == false)){
                $paymentMethodCon = 'Credito';
                $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = (($data['grand_total'] + $totalRete) * -1);
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Afecta factura '.$data['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                if (!$this->validate_account_parametrization($ledgerPago, 'Contrapartida Método pago Crédito', $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
            }
            /* TRABAJANDO CON LOS DESCUENTOS */
            if($data['order_discount'] < 0){
              $ledgerPago = $this->getWappsiLedgerId(1, 'Descuento', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = ($data['order_discount'] * -1);
              $entryPayment['dc'] = 'C';
              $entryPayment['narration'] = 'Descuento';
              $entryPayment['base'] = "";
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              if (!$this->validate_account_parametrization($ledgerPago, 'Descuento', $entryId, $contabilidadSufijo)) return false;
              $entryPayments[] = $entryPayment;
            }
            if(isset($data['self_withholding_amount']) && $data['self_withholding_amount'] != 0){
              $ledgerPago = $this->getWappsiLedgerId(1, 'Autorretención Debito', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = ($data['self_withholding_amount'] * -1);
              $entryPayment['dc'] = 'C';
              $entryPayment['narration'] = 'Autorretención';
              $entryPayment['base'] = ($data['total'] * -1);
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              if (!$this->validate_account_parametrization($ledgerPago, 'Autorretención', $entryId, $contabilidadSufijo)) return false;
              $entryPayments[] = $entryPayment;

              $ledgerPago = $this->getWappsiLedgerId(1, 'Autorretención Credito', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = ($data['self_withholding_amount'] * -1);
              $entryPayment['dc'] = 'D';
              $entryPayment['narration'] = 'Autorretención';
              $entryPayment['base'] = ($data['total'] * -1);
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              if (!$this->validate_account_parametrization($ledgerPago, 'Autorretención Contrapartida', $entryId, $contabilidadSufijo)) return false;
              $entryPayments[] = $entryPayment;
            }
            if (isset($data['rete_autoica_total']) && $data['rete_autoica_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = ($data['rete_autoica_total'] * -1);
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)';
                $entryPayment['base'] = $data['rete_autoica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_autoica_account'];
                if (!$this->validate_account_parametrization($data['rete_autoica_account'], 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;

                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = ($data['rete_autoica_total'] * -1);
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)';
                $entryPayment['base'] = $data['rete_autoica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_autoica_account_counterpart'];
                if (!$this->validate_account_parametrization($data['rete_autoica_account_counterpart'], 'Autorretención al ICA ('.$this->sma->formatDecimals($data['rete_autoica_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                $entryPayments[] = $entryPayment;
                if (isset($data['rete_bomberil_total']) && $data['rete_bomberil_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_bomberil_total'] * -1);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_account'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_account'], 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;

                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_bomberil_total'] * -1);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_bomberil_account_counterpart'];
                    if (!$this->validate_account_parametrization($data['rete_bomberil_account_counterpart'], 'Auto Tasa Bomberil ('.$this->sma->formatDecimals($data['rete_bomberil_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                }
                if (isset($data['rete_autoaviso_total']) && $data['rete_autoaviso_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_autoaviso_total'] * -1);
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_account'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_account'], 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;

                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = ($data['rete_autoaviso_total'] * -1);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)';
                    $entryPayment['base'] = $data['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_autoaviso_account_counterpart'];
                    if (!$this->validate_account_parametrization($data['rete_autoaviso_account_counterpart'], 'Auto Aviso y Tableros ('.$this->sma->formatDecimals($data['rete_autoaviso_percentage']).'%)', $entryId, $contabilidadSufijo)) return false;
                    $entryPayments[] = $entryPayment;
                }
            }
            /* Uniendo todos los array en uno solo para la inserción */
            // exit(var_dump($entryItemDevolucion));
            $entryItems = array_merge($entryItemDevolucion, $entryItemIpoconsumo, $entryItemIva, $entryItemCosto, $entryItemInventario, $entryPayments, $entryItemEnvio, $entryItemPropina);
            /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items para actualizar en la tabla entries_con los campos dr_total y cr_total */
            $drTotal = 0;
            $crTotal = 0;
            // Insertar los entry items
            // $this->sma->print_arrays($entryItems);
            foreach ($entryItems as $entryItem){
                if($entryItem['amount'] >= 0){
                    if( $entryItem['dc'] == 'D' ){
                        $drTotal = $drTotal + $entryItem['amount'];
                    }else if( $entryItem['dc'] == 'C' ){
                        $crTotal = $crTotal + $entryItem['amount'];
                    }
                }
            }
            $totals["drTotal"]=$drTotal;
            $totals["crTotal"]=$crTotal;
            $totalDiff = $drTotal - $crTotal;
            foreach ($entryItems as $entryItem){
                if($entryItem['amount'] >= 0){
                    if ($totalDiff > 0 && $totalDiff <= 1 && $entryItem['dc'] == 'C') {
                        $entryItem['amount'] += $totalDiff;
                        $totalDiff = 0;
                    } else if ($totalDiff < 0 && $totalDiff >= -1 && $entryItem['dc'] == 'D') {
                        $entryItem['amount'] += ($totalDiff * -1);
                        $totalDiff = 0;
                    }
                    if (isset($data['cost_center_id'])) {
                        $entryItem['cost_center_id'] = $data['cost_center_id'];
                    }
                    $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                }
            }
            // Actualizar los totales
            $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
            // $this->sma->print_arrays($entryItems);
            return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function wappsiContabilidadPagoVentas($payment = array(), $customer_id = null, $retencion = array(), $data_taxrate_traslate = array())
    {
      if($this->wappsiContabilidadVerificacion($payment['date']) > 0){
        $contabilidadSufijo = $this->session->userdata('accounting_module');

        $settings_con = $this->getSettingsCon();

        $account_parameter_method = $settings_con->account_parameter_method;
        // Apartir de aqui ejecuta todas las consultas de contabilidad

        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;

        //Buscando si el inicio de la cadena es FV/POS para saber si es el prefijo por defecto de las ventas POS

        $label = $this->getWappsiContabilidadOperationPrefix("sma_payment_prefix");
        $position2 = strpos($payment['reference_no'], '-');
        if($position2 !== false){
            $label = substr($payment['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($payment['reference_no']);

        //Traer datos de la venta
        $sale = $this->getWappsiSaleById($payment['sale_id']);
        // $this->sma->print_arrays($sale);

        //Se va a buscar el label en la tabla wap_entrytypes
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($payment['date'], 0, 10);
        $wappsiEntri['dr_total'] = $payment['amount'];
        $wappsiEntri['cr_total'] = $payment['amount'];
        $wappsiEntri['notes'] = "Afecta a Factura de venta ".$sale->reference_no."\n ".(isset($payment['note']) ? $payment['note'] : '');
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $sale->customer_id;
        $wappsiEntri['user_id'] = $payment['created_by'];

        //Actualizar wap_entrytypes
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);

        if ($entryId = $this->site->getEntryTypeNumberExisting($payment)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            // insert in the table entries_con
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }


        //$this->sma->print_arrays($payment);

        $amountForPay = 0;
        /* TRABAJANDO CON LOS PAGOS */
        $paymentMethodCon = $payment['paid_by'];

                 //TRATAMIENTO RETENCIONES SOBRE PAGOS INDIVIDUALES A COMPRAS

                if ($retencion != null) {

                    if ($retencion['rete_fuente_total'] != 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_fuente_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete Fuente '.$retencion['rete_fuente_percentage'].'% '.$payment['reference_no'];
                        $entryPayment['base'] = $retencion['rete_fuente_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_fuente_account'];
                        $entryPayments[] = $entryPayment;
                        $amountForPay+=$retencion['rete_fuente_total'];
                    }

                    if ($retencion['rete_iva_total'] != 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_iva_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete IVA '.$retencion['rete_iva_percentage'].'% '.$payment['reference_no'];
                        $entryPayment['base'] = $retencion['rete_iva_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_iva_account'];
                        $entryPayments[] = $entryPayment;
                        $amountForPay+=$retencion['rete_iva_total'];
                    }

                    if ($retencion['rete_ica_total'] != 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_ica_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete ICA '.$retencion['rete_ica_percentage'].'% '.$payment['reference_no'];
                        $entryPayment['base'] = $retencion['rete_ica_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_ica_account'];
                        $entryPayments[] = $entryPayment;
                        $amountForPay+=$retencion['rete_ica_total'];
                    }

                    if ($retencion['rete_other_total'] != 0) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_other_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete Other '.$retencion['rete_other_percentage'].'% '.$payment['reference_no'];
                        $entryPayment['base'] = $retencion['rete_other_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_other_account'];
                        $entryPayments[] = $entryPayment;
                        $amountForPay+=$retencion['rete_other_total'];
                    }

                }

                if ($data_taxrate_traslate && count($data_taxrate_traslate) > 0 && $account_parameter_method == 1) { //TRASLADO DE IVA

                    $ledgerPago = $data_taxrate_traslate['tax_rate_traslate_ledger_id'];

                    foreach ($data_taxrate_traslate['sale_taxes'] as $tax => $valor) {
                        $total_iva = $valor;
                        // $paid_amount -= $total_iva;
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $total_iva;
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$sale->reference_no;
                        $entryPayment['base'] = "";
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ledgerPago;
                        $entryPayments[] = $entryPayment;

                        $ledgerIdIVA = $this->getWappsiLedgerId(1, 'IVA', $tax, $contabilidadSufijo);
                        // $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $total_iva;
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$sale->reference_no;
                        $entryPayment['base'] = "";
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $ledgerIdIVA;
                        $entryPayments[] = $entryPayment;
                    }

                }

				$ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
				$entryPayment['entry_id'] =  $entryId;
				$entryPayment['amount'] = $payment['amount'];
				$entryPayment['dc'] = 'D';
				$entryPayment['narration'] = 'Afecta a Factura de venta '.$sale->reference_no;
				$entryPayment['base'] = "";
				$entryPayment['companies_id'] = $wappsiEntri['companies_id'];
				$entryPayment['ledger_id'] = $ledgerPago;
				$entryPayments[] = $entryPayment;

				$ledgerPago = $this->getWappsiReceipLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
				$entryPayment['entry_id'] =  $entryId;
				$entryPayment['amount'] = $payment['amount'] + $amountForPay;
				$entryPayment['dc'] = 'C';
				$entryPayment['narration'] = 'Afecta a Factura de venta '.$sale->reference_no;
				$entryPayment['base'] = "";
				$entryPayment['companies_id'] = $wappsiEntri['companies_id'];
				$entryPayment['ledger_id'] = $ledgerPago;
				$entryPayments[] = $entryPayment;

        // Uniendo todos los array en uno solo para la inserción
        // $entryItems = array_merge($entryPayments);
        $entryItems = $entryPayments;
        // $this->sma->print_arrays($entryItems);

        /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items
        para actualizar en la tabla entries_con los campos dr_total y cr_total */

        $drTotal = 0;
        $crTotal = 0;

        // Insertar los entry items
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){

            if (isset($payment['cost_center_id'])) {
                $entryItem['cost_center_id'] = $payment['cost_center_id'];
            }

            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        //$this->sma->print_arrays($drTotal);
        //$this->sma->print_arrays($crTotal);

        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function wappsiContabilidadPagoCompras($payment = array(), $retencion = array(), $data_taxrate_traslate = array(), $expense_payment = false)
    {
      if($this->wappsiContabilidadVerificacion($payment['date']) > 0){
        $contabilidadSufijo = $this->session->userdata('accounting_module');

        $settings_con = $this->getSettingsCon();

        $account_parameter_method = $settings_con->account_parameter_method;
        // Apartir de aqui ejecuta todas las consultas de contabilidad

        $prefix = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");

        //$this->sma->print_arrays($payment)
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;

        $label = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
        $position2 = strpos($payment['reference_no'], '-');
        if($position2 !== false){
            $label = substr($payment['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($payment['reference_no']);

        //Traer datos de la venta
        $purchase = $this->getWappsiPurchaseById($payment['purchase_id']);
        // $this->sma->print_arrays($purchase);

        //Se va a buscar el label en la tabla wap_entrytypes
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($payment['date'], 0, 10);
        $wappsiEntri['dr_total'] = $payment['amount'];
        $wappsiEntri['cr_total'] = $payment['amount'];
        $wappsiEntri['notes'] = "Afecta a Factura de compra ".$purchase->reference_no."\n ".$this->sma->decode_html($payment['note']);
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $purchase->supplier_id;
        $wappsiEntri['user_id'] = $payment['created_by'];

        //Actualizar wap_entrytypes
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);

        if ($entryId = $this->site->getEntryTypeNumberExisting($payment)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            // insert in the table entries_con
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }


        //$this->sma->print_arrays($payment);

        $amountForPay = 0;
        /* TRABAJANDO CON LOS PAGOS */

        $paymentMethodCon = $payment['paid_by'];

        if ($retencion != null) {

            if ($retencion['rete_fuente_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $retencion['rete_fuente_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Fuente '.$retencion['rete_fuente_percentage'].'% '.$payment['reference_no'];
                $entryPayment['base'] = $retencion['rete_fuente_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $retencion['rete_fuente_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay+=$retencion['rete_fuente_total'];
            }

            if ($retencion['rete_iva_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $retencion['rete_iva_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete IVA '.$retencion['rete_iva_percentage'].'% '.$payment['reference_no'];
                $entryPayment['base'] = $retencion['rete_iva_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $retencion['rete_iva_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay+=$retencion['rete_iva_total'];
            }

            if ($retencion['rete_ica_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $retencion['rete_ica_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete ICA '.$retencion['rete_ica_percentage'].'% '.$payment['reference_no'];
                $entryPayment['base'] = $retencion['rete_ica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $retencion['rete_ica_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay+=$retencion['rete_ica_total'];
            }

            if ($retencion['rete_other_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $retencion['rete_other_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Other '.$retencion['rete_other_percentage'].'% '.$payment['reference_no'];
                $entryPayment['base'] = $retencion['rete_other_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $retencion['rete_other_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay+=$retencion['rete_other_total'];
            }

        }

		$ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
		$entryPayment['entry_id'] =  $entryId;
		$entryPayment['amount'] = $payment['amount'];
		$entryPayment['dc'] = 'C';
		$entryPayment['narration'] = 'Afecta a Factura de compra '.$purchase->reference_no;
		$entryPayment['base'] = "";
		$entryPayment['companies_id'] = $wappsiEntri['companies_id'];
		$entryPayment['ledger_id'] = $ledgerPago;
		$entryPayments[] = $entryPayment;
        $paid_amount = $payment['amount'];

        if (isset($payment['trm_difference'])) { //DIFERENCIA DE TRM

            if ($payment['trm_difference'] >= 0) {
                $trm_diff = $payment['trm_difference'];
                $paid_amount += $trm_diff;
                $dc_trm_diff = 'D';
            } else {
                $trm_diff = $payment['trm_difference'] * -1;
                $paid_amount -= $trm_diff;
                $dc_trm_diff = 'C';
            }

            $ledgerPago = $payment['trm_difference_ledger_id'];
            $entryPayment['entry_id'] =  $entryId;
            $entryPayment['amount'] = $trm_diff;
            $entryPayment['dc'] = $dc_trm_diff;
            $entryPayment['narration'] = 'Diferencia TRM '.$purchase->reference_no;
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $ledgerPago;
            $entryPayments[] = $entryPayment;
        }
        if ($data_taxrate_traslate && count($data_taxrate_traslate) > 0 && $account_parameter_method == 1) { //TRASLADO DE IVA
            $ledgerPago = $data_taxrate_traslate['tax_rate_traslate_ledger_id'];
            foreach ($data_taxrate_traslate['purchase_taxes'] as $tax => $valor) {
                $total_iva = $valor;
                // $paid_amount -= $total_iva;
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $total_iva;
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$purchase->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayments[] = $entryPayment;
                $ledgerIdIVA = $this->getWappsiLedgerId(2, 'IVA', $tax, $contabilidadSufijo);
                // $entryItemIva[$taxRatesId]['ledger_id'] = $ledgerIdIVA;
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $total_iva;
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Traslado IMPUESTOS ('.$tax.') '.$purchase->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerIdIVA;
                $entryPayments[] = $entryPayment;
            }
        }
        if (!$expense_payment) {
            $ledgerPago = $this->getWappsiPaymentLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
        } else {
            $ledgerPago = $expense_payment;
        }
		$entryPayment['entry_id'] =  $entryId;
		$entryPayment['amount'] = $payment['amount'] + $amountForPay;
		$entryPayment['dc'] = 'D';
		$entryPayment['narration'] = 'Afecta a Factura de compra '.$purchase->reference_no;
		$entryPayment['base'] = "";
		$entryPayment['companies_id'] = $wappsiEntri['companies_id'];
		$entryPayment['ledger_id'] = $ledgerPago;
		$entryPayments[] = $entryPayment;
        // Uniendo todos los array en uno solo para la inserción
        // $entryItems = array_merge($entryPayments);
        $entryItems = $entryPayments;
        //$this->sma->print_arrays($entryItems);
        /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items
        para actualizar en la tabla entries_con los campos dr_total y cr_total */
        $drTotal = 0;
        $crTotal = 0;
        // Insertar los entry items
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            if (isset($payment['cost_center_id'])) {
                $entryItem['cost_center_id'] = $payment['cost_center_id'];
            }
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function textoResolucion($documentType)
    {
        if ($documentType && $this->config->item('language') != $documentType->language) {
            $this->lang->admin_load('sma', $documentType->language);
        }

        $resolutionWord = $documentType->palabra_resolucion == "1" ? lang('resolution_enable') : lang('resolution_authorize');
        $resolutionDueDate = $documentType->vencimiento_resolucion ? " ".lang('resolution_expiration')." ".$documentType->vencimiento_resolucion : "";

        $text = lang('invoice_resolution')." {$documentType->word_type_sale} No.{$documentType->num_resolucion} ".lang('of_the')." {$documentType->emision_resolucion} {$resolutionWord} ".lang('from')." {$documentType->sales_prefix}-{$documentType->inicio_resolucion} ".lang('to')." {$documentType->sales_prefix}-{$documentType->fin_resolucion} {$resolutionDueDate}";
        return $text;
    }

    public function getWappsiMovementTypes($movement, $contabilidadSufijo, $document_type_id){

        $tabla = 'movement_type_con'.$contabilidadSufijo;
        $q = $this->db->get_where($tabla, array('movement' => $movement, 'document_type_id' => $document_type_id), 1);
        if ($q->num_rows() > 0) {
            $row = $q->row();
            return $row;
        }
        return FALSE;

    }

    public function wappsiContabilidadAjustes($data = array(), $items = array(), $productNames = array())
    {
      if($this->wappsiContabilidadVerificacion($data['date']) > 0)
      {
        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = $this->getWappsiContabilidadOperationPrefix("qa_prefix");
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = $data['grand_total'];
        $wappsiEntri['cr_total'] = $data['grand_total'];
        $wappsiEntri['notes'] = "Ajuste de inventario ".$data['reference_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['companies_id'];
        $wappsiEntri['user_id'] = $data['created_by'];
        //Actualizar wap_entrytypes
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        // insert in the table entries_con
        if ($entryId = $this->site->getEntryTypeNumberExisting($data, true, false)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            // insert in the table entries_con
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items
        para actualizar en la tabla entries_con los campos dr_total y cr_total */
        $drTotal = 0;
        $crTotal = 0;
        //consulta auxiliar y auxiliar de contrapartida para el ajuste
        if (isset($data['type_adjustment']) && $data['type_adjustment'] == 1) {
            $movement = $this->getWappsiMovementTypes('order_production', $contabilidadSufijo, $data['document_type_id']);
        } else {
            $movement = $this->getWappsiMovementTypes('adjustment', $contabilidadSufijo, $data['document_type_id']);
        }
        //tratamiento entryitems
        $entryItems = [];
        $cnt = 0;
        foreach ($items as $row => $item) {
            $pdata = $this->site->getProductByID($item['product_id']);
            $entryItems[$cnt]['entry_id'] = $entryId;
            $entryItems[$cnt]['ledger_id'] = isset($movement->ledger_id) ? $movement->ledger_id : NULL;
            if (isset($item['avg_cost']) && $item['avg_cost'] >= 0) {
                $item['avg_cost'] = $this->sma->remove_tax_from_amount($pdata->tax_rate, $item['avg_cost']);
                $entryItems[$cnt]['amount'] = (Double) $item['avg_cost'] * (Double) $item['quantity'];
            } else if (isset($item['adjustment_cost']) && $item['adjustment_cost'] > 0) {
                $item['adjustment_cost'] = $this->sma->remove_tax_from_amount($pdata->tax_rate, $item['adjustment_cost']);
                $entryItems[$cnt]['amount'] = (Double) $item['adjustment_cost'] * (Double) $item['quantity'];
            }
            $entryItems[$cnt]['dc'] = $item['type'] == "subtraction" ? "C" : "D";
            $entryItems[$cnt]['narration'] = 'Ajuste '.(isset($productNames[$item['product_id']]) ? $productNames[$item['product_id']] : "")." (".($item['type'] == "subtraction" ? "-" : "+").$item['quantity'].")";
            $entryItems[$cnt]['companies_id'] = $data['companies_id'];
            $entryItems[$cnt]['base'] = 0;
            $entryItems[($cnt+1)]['entry_id'] = $entryId;
            $entryItems[($cnt+1)]['ledger_id'] = isset($movement->offsetting_account) ? $movement->offsetting_account : NULL;
            if (isset($item['avg_cost']) && $item['avg_cost'] >= 0) {
                $entryItems[($cnt+1)]['amount'] = (Double) $item['avg_cost'] * (Double) $item['quantity'];
            } else if (isset($item['adjustment_cost']) && $item['adjustment_cost'] > 0) {
                $entryItems[($cnt+1)]['amount'] = (Double) $item['adjustment_cost'] * (Double) $item['quantity'];
            }
            $entryItems[($cnt+1)]['dc'] = $item['type'] == "subtraction" ? "D" : "C";
            $entryItems[($cnt+1)]['narration'] = 'Ajuste contrapartida';
            $entryItems[($cnt+1)]['companies_id'] = $data['companies_id'];
            $entryItems[($cnt+1)]['base'] = 0;
            $cnt += 2;
        }
        // Insertar los entry items
        foreach ($entryItems as $entryItem){
            $entryItem['entry_id'] = $entryId;
            if(isset($entryItem['amount'])){
                if (isset($data['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $data['cost_center_id'];
                }
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                  $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                  $crTotal = $crTotal + $entryItem['amount'];
                }
            }
        }
        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        // $this->sma->print_arrays($items);
        return TRUE;
      }
      return FALSE;
    }

    public function cleanRowCsvImport($row){
        if (is_array($row)) {
            foreach ($row as $key => $value) {
                $row[$key] = $this->sma->utf8Encode($row[$key]);
                $row[$key] = str_replace("á", "a", $row[$key]);
                $row[$key] = str_replace("é", "e", $row[$key]);
                $row[$key] = str_replace("í", "i", $row[$key]);
                $row[$key] = str_replace("ó", "o", $row[$key]);
                $row[$key] = str_replace("ú", "u", $row[$key]);
                $row[$key] = str_replace("ñ", "n", $row[$key]);
                $row[$key] = str_replace("/", "", $row[$key]);
                $row[$key] = str_replace("'", "", $row[$key]);
                $row[$key] = str_replace("\"", "", $row[$key]);
            }
        } else {
            $row = str_replace("á", "a", $row);
            $row = str_replace("é", "e", $row);
            $row = str_replace("í", "i", $row);
            $row = str_replace("ó", "o", $row);
            $row = str_replace("ú", "u", $row);
            $row = str_replace("ñ", "n", $row);
            $row = str_replace("/", "", $row);
            $row = str_replace("'", "", $row);
            $row = str_replace("\"", "", $row);
        }
        return $row;
    }

    public function wappsiContabilidadDeposito($data = array(), $dtype = 1)
    {
      if($this->wappsiContabilidadVerificacion($data['date']) > 0)
      {
        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;

        $label = $this->getWappsiContabilidadOperationPrefix("deposit_prefix");
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = $data['amount'];
        $wappsiEntri['cr_total'] = $data['amount'];
        $wappsiEntri['notes'] = lang('deposit')." ".$data['reference_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['company_id'];
        $wappsiEntri['user_id'] = $data['created_by'];
        //Actualizar wap_entrytypes
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        // insert in the table entries_con
        // $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($data, false, false, null)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            // insert in the table entries_con
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryItems = [];
        $drTotal = 0;
        $crTotal = 0;
        $entryItems = [];
        $paymentMethodCon = $data['paid_by'];
        $pm_retcom_total = 0;
        if ($this->Settings->payments_methods_retcom == 1 && $dtype == 1) {
            $pm_d = $this->getPaymentMethodByCode($paymentMethodCon);
            if ($pm_d->commision_value != NULL) {
                if (strpos($pm_d->commision_value, '%') !== false) {
                    $p_comm_perc = (Double)(str_replace("%", "", $pm_d->commision_value));
                    $p_comm_base = $data['amount'];
                    $p_comm_amount = $p_comm_base * ($p_comm_perc / 100);
                    $entryItem['entry_id'] =  $entryId;
                    $entryItem['amount'] = $p_comm_amount;
                    $entryItem['dc'] = 'D';
                    $entryItem['narration'] = 'Comisión forma de pago '.$pm_d->name.' ('.$pm_d->commision_value.')';
                    $entryItem['base'] = $p_comm_base;
                    $entryItem['companies_id'] = $pm_d->supplier_id;
                    $entryItem['ledger_id'] = $pm_d->commision_ledger_id;
                    $entryItems[] = $entryItem;
                    $pm_retcom_total += $p_comm_amount;
                } else {
                    $p_comm_base = $data['amount'];
                    $p_comm_amount = $pm_d->commision_value;
                    $entryItem['entry_id'] =  $entryId;
                    $entryItem['amount'] = $p_comm_amount;
                    $entryItem['dc'] = 'D';
                    $entryItem['narration'] = 'Comisión forma de pago '.$pm_d->name;
                    $entryItem['base'] = $p_comm_base;
                    $entryItem['companies_id'] = $pm_d->supplier_id;
                    $entryItem['ledger_id'] = $pm_d->commision_ledger_id;
                    $entryItems[] = $entryItem;
                    $pm_retcom_total += $p_comm_amount;
                }
            }

            if ($pm_d->retefuente_value != NULL) {
                if (strpos($pm_d->retefuente_value, '%') !== false) {
                    $p_retefuente_perc = (Double)(str_replace("%", "", $pm_d->retefuente_value));
                    $p_retefuente_base = $data['amount'];
                    $p_retefuente_amount = $p_retefuente_base * ($p_retefuente_perc / 100);
                    $entryItem['entry_id'] =  $entryId;
                    $entryItem['amount'] = $p_retefuente_amount;
                    $entryItem['dc'] = 'D';
                    $entryItem['narration'] = 'Retefuente forma de pago '.$pm_d->name.' ('.$pm_d->retefuente_value.')';
                    $entryItem['base'] = $p_retefuente_base;
                    $entryItem['companies_id'] = $pm_d->supplier_id;
                    $entryItem['ledger_id'] = $pm_d->retefuente_ledger_id;
                    $entryItems[] = $entryItem;
                    $pm_retcom_total += $p_retefuente_amount;
                } else {
                    $p_retefuente_base = $data['amount'];
                    $p_retefuente_amount = $pm_d->retefuente_value;
                    $entryItem['entry_id'] =  $entryId;
                    $entryItem['amount'] = $p_retefuente_amount;
                    $entryItem['dc'] = 'D';
                    $entryItem['narration'] = 'Retefuente forma de pago '.$pm_d->name;
                    $entryItem['base'] = $p_retefuente_base;
                    $entryItem['companies_id'] = $pm_d->supplier_id;
                    $entryItem['ledger_id'] = $pm_d->retefuente_ledger_id;
                    $entryItems[] = $entryItem;
                    $pm_retcom_total += $p_retefuente_amount;
                }
            }

            if ($pm_d->reteica_value != NULL) {
                if (strpos($pm_d->reteica_value, '%') !== false) {
                    $p_reteica_perc = (Double)(str_replace("%", "", $pm_d->reteica_value));
                    $p_reteica_base = $data['amount'];
                    $p_reteica_amount = $p_reteica_base * ($p_reteica_perc / 100);
                    $entryItem['entry_id'] =  $entryId;
                    $entryItem['amount'] = $p_reteica_amount;
                    $entryItem['dc'] = 'D';
                    $entryItem['narration'] = 'ReteICA forma de pago '.$pm_d->name.' ('.$pm_d->reteica_value.')';
                    $entryItem['base'] = $p_reteica_base;
                    $entryItem['companies_id'] = $pm_d->supplier_id;
                    $entryItem['ledger_id'] = $pm_d->reteica_ledger_id;
                    $entryItems[] = $entryItem;
                    $pm_retcom_total += $p_reteica_amount;
                } else {
                    $p_reteica_base = $data['amount'];
                    $p_reteica_amount = $pm_d->reteica_value;
                    $entryItem['entry_id'] =  $entryId;
                    $entryItem['amount'] = $p_reteica_amount;
                    $entryItem['dc'] = 'D';
                    $entryItem['narration'] = 'ReteICA forma de pago '.$pm_d->name;
                    $entryItem['base'] = $p_reteica_base;
                    $entryItem['companies_id'] = $pm_d->supplier_id;
                    $entryItem['ledger_id'] = $pm_d->reteica_ledger_id;
                    $entryItems[] = $entryItem;
                    $pm_retcom_total += $p_reteica_amount;
                }
            }

        }

        $entryItem = [];
        $ledgerPago = $dtype == 1 ? $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']) : $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
        $ledgerContrapartida = $dtype == 1 ? $this->getWappsiReceipLedgerId("deposit", $contabilidadSufijo, $data['biller_id']) : $this->getWappsiPaymentLedgerId("deposit", $contabilidadSufijo, $data['biller_id']);
        $entryItem['entry_id'] = $entryId;
        $entryItem['ledger_id'] = $ledgerPago;
        $entryItem['amount'] = $data['amount'] - $pm_retcom_total;
        $entryItem['dc'] = $dtype == 1 ? "D" : "C";
        $entryItem['narration'] = lang('deposit')." ".$data['reference_no'];
        $entryItem['companies_id'] = $data['company_id'];
        $entryItem['base'] = 0;
        $entryItems[] = $entryItem;

        $entryItem = [];
        $entryItem['entry_id'] = $entryId;
        $entryItem['ledger_id'] = $ledgerContrapartida;
        $entryItem['amount'] = $data['amount'];
        $entryItem['dc'] = $dtype == 1 ? "C" : "D";
        $entryItem['narration'] = "Contrapartida ".lang('deposit')." ".$data['reference_no'];
        $entryItem['companies_id'] =  $data['company_id'];
        $entryItem['base'] = 0;
        $entryItems[] = $entryItem;
        // Insertar los entry items
        foreach ($entryItems as $entryItem){
            $entryItem['entry_id'] = $entryId;
            if($entryItem['amount'] >= 0){
                if (isset($data['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $data['cost_center_id'];
                }
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                  $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                  $crTotal = $crTotal + $entryItem['amount'];
                }
            }
        }
        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function set_payments_affected_by_deposits($company_id, $payments){

        foreach ($payments as $key => $pmnt) {

            $updates = [];
            $affected_deposits = false;
            if ($pmnt['paid_by'] == 'discount' ) {
                continue;
            }
            if ($pmnt['amount'] < 0) {
                $disc_amount = ($pmnt['amount'] * -1);
                $p = $this->db->where('company_id', $company_id)->where('balance != amount')->order_by('date', 'desc')->get('deposits');
                if ($p->num_rows() > 0) {
                    foreach (($p->result()) as $p_row) {
                        if ($disc_amount == 0) {
                            break;
                        }
                        $update_balance_amount = 0;
                        $prow_dev_balance = $p_row->amount - $p_row->balance;
                        if ($disc_amount > $prow_dev_balance) {
                            $update_balance_amount = $prow_dev_balance;
                            $disc_amount -= $prow_dev_balance;
                        } else if ($disc_amount <= $prow_dev_balance) {
                            $update_balance_amount = $disc_amount;
                            $disc_amount = 0;
                        }
                        $this->db->update('deposits',
                                            [
                                                'balance' => $p_row->balance+$update_balance_amount
                                            ],
                                            [
                                                'id' => $p_row->id
                                            ]
                                            );
                        // $this->sma->print_arrays($p_row);
                        $pmnt['amount'] = $update_balance_amount * -1;
                        $pmnt['affected_deposit_id'] = $p_row->id;
                        $payments[] = $pmnt;
                    }
                }
                unset($payments[$key]);
                continue;
            } else {
                $amount_topay = $pmnt['amount'] - (isset($pmnt['rete_fuente_total']) ? ($pmnt['rete_fuente_total'] + $pmnt['rete_iva_total'] + $pmnt['rete_ica_total'] + $pmnt['rete_other_total'] + $pmnt['rete_bomberil_total'] + $pmnt['rete_autoaviso_total']) : 0);
                $amount_prev_retentions = (isset($pmnt['rete_fuente_total']) ? ($pmnt['rete_fuente_total'] + $pmnt['rete_iva_total'] + $pmnt['rete_ica_total'] + $pmnt['rete_other_total'] + $pmnt['rete_bomberil_total'] + $pmnt['rete_autoaviso_total']) : 0);
                $q = $this->db->order_by('date asc')->get_where('deposits', array('company_id' => $company_id, 'balance >' => 0));
                if ($q->num_rows() > 0) {
                    $q = $q->result();
                } else {
                    continue;
                }
                foreach ($q as $deposit) {
                    if ($amount_topay == 0) {
                        break;
                    }
                    if ($deposit->balance < $amount_topay) {
                        $amount_topay -= $deposit->balance;
                        $updates[$deposit->id] = 0;
                        $affected_deposits[$deposit->id] = $deposit->balance;
                    } else if ($deposit->balance >= $amount_topay) {
                        $updates[$deposit->id] = $deposit->balance - $amount_topay;
                        $affected_deposits[$deposit->id] = $amount_topay;
                        $amount_topay -= $amount_topay;
                    }
                }
                if ($affected_deposits) {
                    foreach ($affected_deposits as $deposit_id => $amount_affected) {
                        $customer = $this->site->getCompanyByID($company_id);
                        $amount_prev_retentions_prorrated = 0;

                        $affected_amount_percentage = ((($amount_affected)* 100) / ($pmnt['amount'] - (isset($pmnt['rete_fuente_total']) ? ($pmnt['rete_fuente_total'] + $pmnt['rete_iva_total'] + $pmnt['rete_ica_total'] + $pmnt['rete_other_total'] + $pmnt['rete_bomberil_total'] + $pmnt['rete_autoaviso_total']) : 0))) / 100;

                        if ($amount_prev_retentions > 0) {
                            $amount_prev_retentions_prorrated = $amount_prev_retentions * $affected_amount_percentage;
                        }

                        if (isset($pmnt['rete_fuente_total']) && $pmnt['rete_fuente_total'] > 0) {
                            $pmnt['rete_fuente_total'] = $pmnt['rete_fuente_total'] * $affected_amount_percentage;
                            $pmnt['rete_fuente_base'] = $pmnt['rete_fuente_base'] * $affected_amount_percentage;
                        }
                        if (isset($pmnt['rete_iva_total']) && $pmnt['rete_iva_total'] > 0) {
                            $pmnt['rete_iva_total'] = $pmnt['rete_iva_total'] * $affected_amount_percentage;
                            $pmnt['rete_iva_base'] = $pmnt['rete_iva_base'] * $affected_amount_percentage;
                        }
                        if (isset($pmnt['rete_ica_total']) && $pmnt['rete_ica_total'] > 0) {
                            $pmnt['rete_ica_total'] = $pmnt['rete_ica_total'] * $affected_amount_percentage;
                            $pmnt['rete_ica_base'] = $pmnt['rete_ica_base'] * $affected_amount_percentage;
                        }
                        if (isset($pmnt['rete_bomberil_total']) && $pmnt['rete_bomberil_total'] > 0) {
                            $pmnt['rete_bomberil_total'] = $pmnt['rete_bomberil_total'] * $affected_amount_percentage;
                            $pmnt['rete_bomberil_base'] = $pmnt['rete_bomberil_base'] * $affected_amount_percentage;
                        }
                        if (isset($pmnt['rete_autoaviso_total']) && $pmnt['rete_autoaviso_total'] > 0) {
                            $pmnt['rete_autoaviso_total'] = $pmnt['rete_autoaviso_total'] * $affected_amount_percentage;
                            $pmnt['rete_autoaviso_base'] = $pmnt['rete_autoaviso_base'] * $affected_amount_percentage;
                        }
                        if (isset($pmnt['rete_other_total']) && $pmnt['rete_other_total'] > 0) {
                            $pmnt['rete_other_total'] = $pmnt['rete_other_total'] * $affected_amount_percentage;
                            $pmnt['rete_other_base'] = $pmnt['rete_other_base'] * $affected_amount_percentage;
                        }
                        $pmnt['amount'] = $amount_affected + (isset($pmnt['rete_fuente_total']) ? ($pmnt['rete_fuente_total'] + $pmnt['rete_iva_total'] + $pmnt['rete_ica_total'] + $pmnt['rete_other_total'] + $pmnt['rete_bomberil_total'] + $pmnt['rete_autoaviso_total']) : 0);
                        $pmnt['affected_deposit_id'] = $deposit_id;
                        $new_balance = $updates[$deposit_id];
                        $this->db->update('deposits', array('balance' => $new_balance), array('id' => $deposit_id));
                        $payments[] = $pmnt;
                    }
                    unset($payments[$key]);
                }
            }
        }
        $this->sync_company_deposit_amount($company_id);
        return $payments;
    }

    public function get_invoice_notes($module = 2){
        $q = $this->db->get_where('invoice_notes', array('state' => 1, 'module' => $module));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getCountries(){
        $q = $this->db->get('countries');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getCountryByName($name){
        $this->db->where('NOMBRE', $name);
        $q = $this->db->get('countries');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getAllLedgers(){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $q = $this->db->order_by('code asc')->get('ledgers_con'.$contabilidadSufijo);
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }

                return $data;
            }
        }
        return FALSE;
    }

    public function wappsiContabilidadGastosPOS($purchase_id, $expense, $data = array(), $item = array(), $payment = array(), $prev_reference = NULL)
    {
      if($this->wappsiContabilidadVerificacion($expense['date']) > 0){

        if (!$this->validate_doc_type_accounting($expense['document_type_id'])) {
            return false;
        }

        $expense_edit = isset($expense['expense_edit']) ? true : false;
        $settings_con = $this->getSettingsCon();
        $account_parameter_method = $settings_con->account_parameter_method;
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $prefixExpense = $this->getWappsiContabilidadOperationPrefix("expense_prefix");
        $position2 = strpos($expense['reference'], '-');
        if($position2 !== false){
            $prefixExpense = substr($expense['reference'], 0,$position2);
        }
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = $this->getWappsiContabilidadOperationPrefix("expense_prefix");
        $position2 = strpos($expense['reference'], '-');
        if($position2 !== false){
            $label = substr($expense['reference'], 0,$position2);
        }
        $number = $this->cleanContaReference($expense['reference']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($expense['date'], 0, 10);
        $wappsiEntri['dr_total'] = $expense['amount'];
        $wappsiEntri['cr_total'] = $expense['amount'];
        $wappsiEntri['notes'] = "Gasto añadido ".$expense['reference'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $expense['supplier_id']; //PENDIENTE CONFIRMAR JUAN CARLOS
        $wappsiEntri['user_id'] = $expense['created_by'];
        $expense_category = $this->getExpenseCategory($expense['category_id']);
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting((is_null($data) ? $expense : $data), ($expense_edit ? true : false), false, $prev_reference)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $amountForPay = $expense['amount'];
        $amountForPayD = $expense['amount'];
        $entryItemsIva = [];
        if (count($item) > 0) {
            if ($item['tax_rate_id'] > 0 && $expense_category->tax_ledger_id > 0 && $item['item_tax'] > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal($item['item_tax']);
                $entryItemIva['dc'] = 'D';
                $entryItemIva['narration'] = 'Iva 1 sobre gasto';
                $entryItemIva['base'] = $this->sma->formatDecimal($item['net_unit_cost']);
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_ledger_id;
                $entryItemsIva[] = $entryItemIva;
                $amountForPayD -= $this->sma->formatDecimal($item['item_tax']);
            }
            if ($item['tax_rate_2_id'] > 0 && $expense_category->tax_2_ledger_id > 0 && $item['item_tax_2'] > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $item['tax_rate_2_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal($item['item_tax_2']);
                $entryItemIva['dc'] = 'D';
                $entryItemIva['narration'] = 'Iva 2 sobre gasto';
                $entryItemIva['base'] = $this->sma->formatDecimal($item['net_unit_cost']);
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_2_ledger_id;
                $entryItemsIva[] = $entryItemIva;
                $amountForPayD -= $this->sma->formatDecimal($item['item_tax_2']);
            }
        }
        $entryPayments = array();
        if ((isset($expense['rete_fuente_total']) || isset($expense['rete_iva_total']) || isset($expense['rete_ica_total']) || isset($expense['rete_other_total']))) {
            if ($expense['rete_fuente_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $expense['rete_fuente_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Fuente '.$expense['rete_fuente_percentage'].'% '.$expense['reference'];
                $entryPayment['base'] = $expense['rete_fuente_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $expense['rete_fuente_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay-=$expense['rete_fuente_total'];
            }
            if ($expense['rete_iva_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $expense['rete_iva_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete IVA '.$expense['rete_iva_percentage'].'% '.$expense['reference'];
                $entryPayment['base'] = $expense['rete_iva_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $expense['rete_iva_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay-=$expense['rete_iva_total'];
            }
            if ($expense['rete_ica_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $expense['rete_ica_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete ICA '.$expense['rete_ica_percentage'].'% '.$expense['reference'];
                $entryPayment['base'] = $expense['rete_ica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $expense['rete_ica_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay-=$expense['rete_ica_total'];
            }
            if ($expense['rete_other_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $expense['rete_other_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Other '.$expense['rete_other_percentage'].'% '.$expense['reference'];
                $entryPayment['base'] = $expense['rete_other_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $expense['rete_other_account'];
                $entryPayments[] = $entryPayment;
                $amountForPay-=$expense['rete_other_total'];
            }
        }

        $metodoPago = "cash";
        $ledgerGasto = $this->getWappsiPaymentLedgerId($metodoPago, $contabilidadSufijo, $expense['biller_id']);

        $entryExpense['entry_id'] =  $entryId;
        $entryExpense['amount'] = $amountForPay;
        $entryExpense['dc'] = 'C';
        $entryExpense['narration'] = 'Gasto añadido '.$expense['reference'];
        $entryExpense['base'] = "";
        $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
        $entryExpense['ledger_id'] = $ledgerGasto;
        $entryExpenses[] = $entryExpense;

        $ledgerGasto = $expense_category->ledger_id;
        $entryExpense['entry_id'] =  $entryId;
        $entryExpense['amount'] = $amountForPayD;
        $entryExpense['dc'] = 'D';
        $entryExpense['narration'] = 'Gasto añadido '.$expense['reference'];
        $entryExpense['base'] = "";
        $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
        $entryExpense['ledger_id'] = $ledgerGasto;
        $entryExpenses[] = $entryExpense;
        $entryItems = array_merge($entryExpenses, $entryPayments, $entryItemsIva);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            if (isset($expense['cost_center_id'])) {
                $entryItem['cost_center_id'] = $expense['cost_center_id'];
            }
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }

        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function wappsiContabilidadGastos($purchase_id, $data = array(), $items = array(), $payment = array(), $prev_reference = NULL, $has_payment_retention = false)
    {
      if($this->wappsiContabilidadVerificacion($data['date']) > 0){

        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }

        foreach ($payment as $key => $pmnt) {
            if ($pmnt['paid_by'] == 'retencion') {
                unset($payment[$key]);
            }
        }

        $expense_edit = isset($data['expense_edit']) ? true : false;
        $settings_con = $this->getSettingsCon();
        $account_parameter_method = $settings_con->account_parameter_method;
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        // exit(var_dump($contabilidadSufijo));
        $prefixExpense = $this->getWappsiContabilidadOperationPrefix("expense_prefix");
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $prefixExpense = substr($data['reference_no'], 0,$position2);
        }
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = $this->getWappsiContabilidadOperationPrefix("expense_prefix");
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = $data['grand_total'];
        $wappsiEntri['cr_total'] = $data['grand_total'];
        $wappsiEntri['notes'] = "Gasto añadido ".$data['reference_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['supplier_id']; //PENDIENTE CONFIRMAR JUAN CARLOS
        $wappsiEntri['user_id'] = $data['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($data, ($expense_edit ? true : false), false, $prev_reference)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $amountForPay = $data['grand_total'];
        $amountForPayD = $data['grand_total'];

        $entryPayments = array();

        $total_rete = 0;

        if ($has_payment_retention == false && (isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']))) {
            if ($data['rete_fuente_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_fuente_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Fuente '.($data['rete_fuente_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_fuente_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                $entryPayments[] = $entryPayment;
                if ($data['rete_fuente_assumed']) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_fuente_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente Asumida '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_assumed_account'];
                    $entryPayments[] = $entryPayment;
                } else {
                    $total_rete +=$data['rete_fuente_total'];
                }
            }
            if ($data['rete_iva_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_iva_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete IVA '.($data['rete_iva_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_iva_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_iva_account'];
                $entryPayments[] = $entryPayment;
                if ($data['rete_iva_assumed']) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_iva_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete IVA Asumida '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_assumed_account'];
                    $entryPayments[] = $entryPayment;
                } else {
                    $total_rete +=$data['rete_iva_total'];
                }
            }
            if ($data['rete_ica_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_ica_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete ICA '.($data['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_ica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_ica_account'];
                $entryPayments[] = $entryPayment;
                if ($data['rete_ica_assumed']) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_ica_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ICA Asumida '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_assumed_account'];
                    $entryPayments[] = $entryPayment;
                } else {
                    $total_rete +=$data['rete_ica_total'];
                }
            }
            if ($data['rete_other_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_other_total'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Rete Otros '.($data['rete_other_assumed'] == 1 ? 'Asumida' : '').' '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_other_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_other_account'];
                $entryPayments[] = $entryPayment;
                if ($data['rete_other_assumed']) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_other_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Otros Asumida '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_assumed_account'];
                    $entryPayments[] = $entryPayment;
                } else {
                    $total_rete +=$data['rete_other_total'];
                }
            }
        }

        //HACE FALTA DIFERENCIAR : SI VIENE A CRÉDITO, TOME LA CUENTA ACREEDORA DE LA FORMA DE PAGO A CRÉDITO, SI VIENE POR CAUSACIÓN, LA DE LA CATEGORÍA DE GASTO
        // NOTA : PARA DIFERENCIAR, USAR EL CAMPO "due_payment_method_id" PARA VER SI CON LA FORMA DE PAGO "Credito" QUEDA DATO Y CON ESO DIFERENCIAR
        $entryItemsIva = [];
        $entryExpenses = [];
        foreach ($items as $item) {
            $expense_category = $this->getExpenseCategory($item['product_id']);

            if ($item['tax_rate_id'] > 0 && $expense_category->tax_ledger_id > 0 && $item['item_tax'] > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal($item['item_tax']);
                $entryItemIva['dc'] = 'D';
                $entryItemIva['narration'] = 'Iva 1 sobre gasto '.$item['product_name'].' '.$data['reference_no'];
                $entryItemIva['base'] = $this->sma->formatDecimal($item['net_unit_cost'] * $item['quantity']);
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_ledger_id;
                $entryItemsIva[] = $entryItemIva;
                $amountForPayD -= $this->sma->formatDecimal($item['item_tax']);
            }
            if ($item['tax_rate_2_id'] > 0 && $expense_category->tax_2_ledger_id > 0 && $item['item_tax_2'] > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $item['tax_rate_2_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal($item['item_tax_2']);
                $entryItemIva['dc'] = 'D';
                $entryItemIva['narration'] = 'Iva 2 sobre gasto'.$item['product_name'].' '.$data['reference_no'];
                $entryItemIva['base'] = $this->sma->formatDecimal($item['net_unit_cost'] * $item['quantity']);
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_2_ledger_id;
                $entryItemsIva[] = $entryItemIva;
                $amountForPayD -= $this->sma->formatDecimal($item['item_tax_2']);
            }

            if (isset($data['expense_causation'])) {

                $prorrated_retention = $total_rete > 0 ? ($total_rete * (($item['unit_cost'] * $item['quantity']) / $data['grand_total'])) : 0;

                if($expense_category->creditor_ledger_id > 0) {
                    $ledgerGasto = $expense_category->creditor_ledger_id;
                    $this->db->update('purchases', array('credit_ledger_id' => $ledgerGasto), array('id' => $purchase_id));
                    if (isset($data['purchase_item_id'])) {
                        $this->db->update('purchase_items', ['expense_category_creditor_ledger_id' => $ledgerGasto], ['id' => $data['purchase_item_id']]);
                    }
                }
                $entryExpense['entry_id'] =  $entryId;
                $entryExpense['amount'] = ($item['unit_cost'] * $item['quantity']) - $prorrated_retention;
                $entryExpense['dc'] = 'C';
                $entryExpense['narration'] = 'Gasto '.$item['product_name'].' '.$data['reference_no'];
                $entryExpense['base'] = "";
                $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
                $entryExpense['ledger_id'] = $ledgerGasto;
                $entryExpenses[] = $entryExpense;
            }

            $ledgerGasto = $expense_category->ledger_id;
            $entryExpense['entry_id'] =  $entryId;
            $entryExpense['amount'] = ($item['net_unit_cost']  * $item['quantity']);
            $entryExpense['dc'] = 'D';
            $entryExpense['narration'] = 'Gasto añadido '.$item['product_name'].' '.$data['reference_no'];
            $entryExpense['base'] = "";
            $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
            $entryExpense['ledger_id'] = $ledgerGasto;
            $entryExpenses[] = $entryExpense;
        }

        if (count($payment) > 0 && isset($payment[0]['paid_by'])) {
            $metodoPago = $payment[0]['paid_by'];
            $ledgerGasto = $this->getWappsiPaymentLedgerId($metodoPago, $contabilidadSufijo, $data['biller_id']);
            $entryExpense['entry_id'] =  $entryId;
            $entryExpense['amount'] = $data['grand_total'] - $total_rete;
            $entryExpense['dc'] = 'C';
            $entryExpense['narration'] = 'Gasto añadido '.$item['product_name'].' '.$data['reference_no'];
            $entryExpense['base'] = "";
            $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
            $entryExpense['ledger_id'] = $ledgerGasto;
            $entryExpenses[] = $entryExpense;
        }

        if (count($payment) == 0 && !isset($data['expense_causation'])) {
            if (isset($data['due_payment_method_id']) && $data['due_payment_method_id']) {
                $pmCredito = $this->getPaymentMethodById($data['due_payment_method_id']);
                $metodoPago = $pmCredito->code;
            } else {
                $billerDataAdd = $this->getAllCompaniesWithState('biller', $data['biller_id']);
                if ($billerDataAdd->default_credit_payment_method) {
                    $metodoPago = $billerDataAdd->default_credit_payment_method;
                } else {
                    $metodoPago = 'Credito';
                }
            }
            $ledgerGasto = $this->getWappsiPaymentLedgerId($metodoPago, $contabilidadSufijo, $data['biller_id']);
            $entryExpense['entry_id'] =  $entryId;
            $entryExpense['amount'] = $data['grand_total'] - $total_rete;
            $entryExpense['dc'] = 'C';
            $entryExpense['narration'] = 'Gasto añadido '.$item['product_name'].' '.$data['reference_no'];
            $entryExpense['base'] = "";
            $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
            $entryExpense['ledger_id'] = $ledgerGasto;
            $entryExpenses[] = $entryExpense;
        }

        $entryItems = array_merge($entryExpenses, $entryPayments, $entryItemsIva);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            if (isset($data['cost_center_id'])) {
                $entryItem['cost_center_id'] = $data['cost_center_id'];
            }
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }

        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        // exit(var_dump($entryItems));
        return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function get_document_type()
    {
        $this->db->select("*");
        $this->db->from("documentypes");
        $response = $this->db->get();

        return $response->result();
    }

    public function get_document_type_by_id($id)
    {
        $this->db->select("*");
        $this->db->where('id', $id);
        $this->db->from("documentypes");
        $response = $this->db->get();
        return $response->row();
    }

    public function getStates($country = NULL)
    {
        if (!empty($country)) {
            $this->db->where("pais", $country);
        }

        $response = $this->db->get('states');
        if ($response->num_rows() > 0) {
            return $response->result();
        }

        return FALSE;
    }

    public function get_cities_by_state_id($state_id = NULL)
    {
        $this->db->select("*");
        $this->db->from("cities");
        $this->db->where("CODDEPARTAMENTO", $state_id);
        $response = $this->db->get();

        return $response->result();
    }

    public function get_types_person()
    {
        $this->db->select("*");
        $this->db->from("types_person");
        $response = $this->db->get();

        return $response->result();
    }

    public function get_types_vat_regime($id = NULL)
    {
        $this->db->select("*");
        $this->db->from("types_vat_regime");

        if (!empty($id)) {
            $this->db->where('id', $id);
        }

        $response = $this->db->get();

        if ($id) {
            return $response->row();
        }

        return $response->result();
    }

    public function get_types_obligations()
    {
        $this->db->select("*");
        $this->db->from("types_obligations-responsabilities");
        $response = $this->db->get();

        return $response->result();
    }

    public function getTypesCustomerObligations($customer_id, $relation = ACQUIRER)
    {
        $this->db->where("customer_id", $customer_id);
        $this->db->where("relation", $relation);
        $response = $this->db->get("types_customer_obligations");

        return $response->result();
    }

    public function get_customer_obligations($customer_id, $relation = ACQUIRER){
        $q = $this->db->select('types_customer_obligations.*, types_obligations-responsabilities.description')
                 ->join('types_obligations-responsabilities', 'types_obligations-responsabilities.code = types_customer_obligations.types_obligations_id')
                 ->where('types_customer_obligations.customer_id', $customer_id)
                 ->where('types_customer_obligations.relation', $relation)
                 ->get('types_customer_obligations');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function delete_types_customer_obligations($id, $relation = ACQUIRER)
    {
        if ($this->db->delete('types_customer_obligations', ['customer_id' => $id, "relation" => $relation])) {
            return true;
        }
        return FALSE;
    }

    public function getPaidOpts($state_sale = 1, $state_purchase = 1, $counted_active = false){
        if ($state_sale && $state_purchase) {
            $this->db->where('state_sale', $state_sale);
            $this->db->or_where('state_purchase', $state_purchase);
        } else if ($state_sale) {
            $this->db->where('state_sale', $state_sale);
        } else if ($state_purchase) {
            $this->db->where('state_purchase', $state_purchase);
        }
        if ($counted_active) {
            $this->db->where('(cash_payment = 1 OR code = "cash")');
        }
        $q = $this->db->get('payment_methods');
        if ($q->num_rows() > 0 ) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getPaymentMethodById($id) {
        $q = $this->db->get_where('payment_methods', array('id' => $id), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentMethodByCode($code) {
        $q = $this->db->get_where('payment_methods', array('code' => $code), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentMethodByName($name) {
        $q = $this->db->get_where('payment_methods', array('name' => $name), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getPaymentMethodParameter($name){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'payment_methods_con'.$contabilidadSufijo;
            $q = $this->db->get_where($tabla, array('type' => $name), 1);
            if ($q->num_rows() > 0) {
                $row = $q->row();
                return $row;
            }
        }
        return FALSE;
    }

    public function updatePaymentMethodParameter($data){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $id = $data['id'];
            unset($data['id']);
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'payment_methods_con'.$contabilidadSufijo;

            $pm_exist = $this->db->get_where($tabla, array('id' => $id));

            if ($pm_exist->num_rows() > 0) {
                unset($data['type']);
                $this->db->update($tabla, $data, array('id' => $id));
            } else {
                $this->db->insert($tabla, $data);
            }
        }
        return FALSE;
    }

     public function insertPaymentMethodParameter($data){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'payment_methods_con'.$contabilidadSufijo;
            $this->db->insert($tabla, $data);
        }
        return FALSE;
    }

    public function getEntryTypeNumberExisting($data, $reset = false, $return = false, $prev_reference = null, $from_edit = false){
        if ($this->wappsiContabilidadVerificacion($data['date']) > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            if ($prev_reference != null) {
                $ref = explode('-', $prev_reference);
            } else {
                $ref = explode('-', isset($data['reference_no']) ? $data['reference_no'] : $data['reference']);
            }
            $entryTypeId = $this->getWappsiIdEntryType($ref[0], $contabilidadSufijo);
            $this->db->where('entrytype_id', $entryTypeId)
                     ->where('number', $ref[1]);
            if ((isset($data['customer_id']) || isset($data['supplier_id']) || isset($data['company_id'])) && $from_edit == false) {
                if (isset($data['customer_id']) && $data['customer_id']) {
                    $company_id = $data['customer_id'];
                }
                if (isset($data['supplier_id']) && $data['supplier_id']) {
                    $company_id = $data['supplier_id'];
                }
                if (isset($data['company_id']) && $data['company_id']) {
                    $company_id = $data['company_id'];
                }
                $this->db->where('companies_id', $company_id);
            }
            $exists = $this->db->get('entries_con'.$contabilidadSufijo, 1);
            if ($exists->num_rows() > 0) {
                $entry = $exists->row();
                if ($reset == true || $from_edit == true) {
                    $this->db->update('entries_con'.$contabilidadSufijo, array(
                                                                                'state' => 1,
                                                                                'dr_total' => "0",
                                                                                'cr_total' => "0"
                                                                            ), array('id' => $entry->id));
                    $this->db->delete('entryitems_con'.$contabilidadSufijo, array('entry_id' => $entry->id));
                }
                return $entry->id;
            }
        }
        return FALSE;
    }

    public function get_sale_item_costing_accounted($sale_id, $product_id, $recontabilizacion){
        $q = $this->db->where('sale_id', $sale_id)
                      ->where('product_id', $product_id)
                      ->get('costing');

        $item_costing = [];

        if ($q->num_rows() > 0) {
            $cnt = 0;
            foreach (($q->result()) as $costing) {
                $item_costing[$cnt]['date'] = $costing->date;
                $item_costing[$cnt]['product_id'] = $product_id;
                $item_costing[$cnt]['sale_item_id'] = $costing->sale_item_id;
                $item_costing[$cnt]['purchase_item_id'] = $costing->purchase_item_id;
                $item_costing[$cnt]['quantity'] = $costing->quantity;
                $item_costing[$cnt]['purchase_net_unit_cost'] = $costing->purchase_net_unit_cost;
                $item_costing[$cnt]['purchase_unit_cost' ] = $costing->purchase_unit_cost;
                $item_costing[$cnt]['sale_net_unit_price' ] = $costing->sale_net_unit_price;
                $item_costing[$cnt]['sale_unit_price' ] = $costing->sale_unit_price;
                $item_costing[$cnt]['quantity_balance' ] = $costing->quantity_balance;
                $item_costing[$cnt]['inventory' ] = $costing->inventory;
                $item_costing[$cnt]['option_id' ] = $costing->option_id;
                $cnt++;
            }

        }

        return $item_costing;

    }

    public function getInvoiceFormats($module = null){

        if ($module != null) {
            $this->db->where('module', $module);
        }

        $q = $this->db->where('status', '1')->get('invoice_formats');

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }

        return false;

    }

    public function getInvoiceFormatById($id){
        $q = $this->db->get_where('invoice_formats', array('id' => $id));

        if ($q->num_rows() > 0) {
            return $q->row();
        }

        return false;
    }

    public function getDocumentTypeById($document_type_id = NULL, $ref_no = NULL, $arr = false){

        $this->db->select('documents_types.*, invoice_formats.logo as invoice_format_logo')
                 ->join('invoice_formats', 'invoice_formats.id = documents_types.module_invoice_format_id', 'left');
        if ($document_type_id) {
            $this->db->where('documents_types.id', $document_type_id);
        } else if ($ref_no) {
            $sales_prefix = explode("-", $ref_no);
            $sales_prefix = $sales_prefix[0];
            $this->db->where('documents_types.sales_prefix', $sales_prefix);

        }
        $document_type = $this->db->get('documents_types');
        if ($document_type->num_rows() > 0) {
            if ($arr) {
                return $document_type->row_array();
            } else {
                return $document_type->row();
            }
        }
        return FALSE;
    }

    public function getCustomerBranch($customer_id, $take_first = false){
        // $q = $this->db->get_where('addresses', array('company_id' => $customer_id));
        $q = $this->db->where('company_id', $customer_id)
             ->order_by('id asc')
             ->limit(1)->get('addresses');

        if ($q->num_rows() > 0) {

            if ($take_first == true) {
                $first = $q->row();
                return $first->id;
            }

            return false;
        } else {
            $c = $this->db->get_where('companies', array('id' => $customer_id));
            if ($c->num_rows() > 0) {

                $customer_data = $c->row();

                $datos = array(
                    'company_id' => $customer_id,
                    'direccion' => $customer_data->address,
                    'sucursal' => 'Principal',
                    'city' => $customer_data->city,
                    'postal_code' => $customer_data->postal_code,
                    'state' => $customer_data->state,
                    'country' => $customer_data->country,
                    'phone' => $customer_data->phone,
                    'city_code' => $customer_data->postal_code,
                );

                if ($this->db->insert('addresses', $datos)) {
                    return $this->db->insert_id();
                }
            }
        }

        return false;
    }

    public function getCostCenters($company_id = null){
        if ($this->wappsiContabilidadVerificacion() > 0) {

            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;
            $this->db->where('company_id', NULL);
            if ($company_id != null) {
                $this->db->or_where('company_id', $company_id);
            }
            $cc = $this->db->get($tabla);
            if ($cc->num_rows() > 0) {
                foreach (($cc->result()) as $cost_center) {
                    $data[] = $cost_center;
                }
                return $data;
            }
        }

        return false;
    }

    public function updateBillerCostCenter($cost_center_id, $company_id){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;
            // $this->db->update($tabla, array('company_id' => NULL), array('company_id' => $company_id));
            $this->db->update($tabla, array('company_id' => $company_id), array('id' => $cost_center_id));
        }
    }

    public function getSaleItemCosting($product_id, $sale_id){
       $q = $this->db->get_where('costing', array('product_id' => $product_id, 'sale_id' => $sale_id));

       if ($q->num_rows() > 0) {
           $q = $q->row();
           return $q->purchase_unit_cost;
       }

       return false;

    }

    public function getAllCostCenters(){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;
            $q = $this->db->get($tabla);
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
        }
        return false;
    }

    public function getCostCenterByid($cost_center_id){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;
            $q = $this->db->get_where($tabla, array('id' => $cost_center_id));
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }
        return false;
    }

    public function getBillerCostCenter($biller_id){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'cost_centers_con'.$contabilidadSufijo;
            $q = $this->db->select('cost_center.*')
                    ->join($tabla.' as  cost_center', 'cost_center.id = biller_data.default_cost_center_id', 'inner')
                    ->where('biller_data.biller_id', $biller_id)
                    ->get('biller_data');
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }
        return false;
    }

    ///PAGOS MULTIPLES VENTAS
    public function wappsiContabilidadPagosMultiplesVentas($payment = array(), $customer_id = null, $retenciones = array(), $data_taxrate_traslate = array(), $conceptos = array(), $cost_center = NULL)
    {
      if($this->wappsiContabilidadVerificacion($payment['date']) > 0){
        if (!$this->validate_doc_type_accounting($payment['document_type_id'])) {
            return false;
        }
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $prefix = $this->getWappsiContabilidadOperationPrefix("sma_payment_prefix");
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = $this->getWappsiContabilidadOperationPrefix("sma_payment_prefix");
        $position2 = strpos($payment['reference_no'], '-');
        if($position2 !== false){
            $label = substr($payment['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($payment['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($payment['date'], 0, 10);
        $wappsiEntri['dr_total'] = $payment['amount'];
        $wappsiEntri['cr_total'] = $payment['amount'];
        $wappsiEntri['notes'] = (isset($payment['note']) ? "Pago múltiple ".$payment['reference_no']." Nota : ".$payment['note'] : "");
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $payment['companies_id'];
        $wappsiEntri['user_id'] = $payment['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($payment, true)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryPayments = [];
        $entryDiscounts = [];
        $pmntMethodTotal = [];
        $pmntMethodDesc = [];
        $pmntMethodLedgerId = [];
        $pm_d = null;
        $pm_retcom_total = 0;
        $pmntRetCom = [];
        $sales_data = [];


        foreach ($payment['payments'] as $pmnt) {
            $paymentMethodCon = $pmnt['paid_by'];
            $sale = $this->getWappsiSaleById($pmnt['sale_id']);
            $sales_data[$pmnt['sale_id']] = $sale;
            if ($paymentMethodCon == 'discount') {
                if (!isset($entryDiscounts[$sale->reference_no])) {
                    $entryDiscounts[$sale->reference_no] = $pmnt['amount'];
                } else {
                    $entryDiscounts[$sale->reference_no] += $pmnt['amount'];
                }
            }
        }
        // $this->sma->print_arrays($entryDiscounts);
        $counter_part_discounts = [];
        $main_part_discounts = [];
        foreach ($payment['payments'] as $pmnt) {
            $paymentMethodCon = $pmnt['paid_by'];
            if (!$pm_d) {
                $pm_d = $this->getPaymentMethodByCode($paymentMethodCon);
            }
            $sale = $sales_data[$pmnt['sale_id']];
            if ($paymentMethodCon != 'discount') {
                if (!isset($pmntMethodTotal[$paymentMethodCon])) {
                    $pmntMethodTotal[$paymentMethodCon] = $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] = 'Afecta a Facturas de Venta '.$sale->reference_no;
                    $pmntMethodLedgerId[$paymentMethodCon] = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
                } else {
                    $pmntMethodTotal[$paymentMethodCon] += $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] .= ', '.$sale->reference_no;

                }
                if ($this->Settings->payments_methods_retcom == 1) {
                    if (isset($pmnt['pmnt_insert_id'])) {

                        $this->db->update('payments', [
                                                    'pm_commision_value' => $pm_d->commision_value,
                                                    'pm_retefuente_value' => $pm_d->retefuente_value,
                                                    'pm_reteiva_value' => $pm_d->reteiva_value,
                                                    'pm_reteica_value' => $pm_d->reteica_value,
                                                ], ['id'=>$pmnt['pmnt_insert_id']]);

                    }
                    if ((isset($pmnt['pm_commision_value']) && $pmnt['pm_commision_value'] != NULL) || $pm_d->commision_value != NULL) {
                        if ((isset($pmnt['pm_commision_value']) && $pmnt['pm_commision_value'] != NULL)) {
                            $pm_d->commision_value = $pmnt['pm_commision_value'];
                        }
                        if (strpos($pm_d->commision_value, '%') !== false) {
                            $p_comm_perc = (Double)(str_replace("%", "", $pm_d->commision_value));
                            $p_comm_base = $sale->total * ($pmnt['amount'] / $sale->grand_total);
                            $p_comm_amount = $p_comm_base * ($p_comm_perc / 100);
                            if (!isset($pmntRetCom['commision'])) {
                                $pmntRetCom['commision']['entry_id'] =  $entryId;
                                $pmntRetCom['commision']['dc'] = 'D';
                                $pmntRetCom['commision']['narration'] = 'Comisión forma de pago '.$pm_d->name.' ('.$pm_d->commision_value.')';
                                $pmntRetCom['commision']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['commision']['ledger_id'] = $pm_d->commision_ledger_id;
                                $pmntRetCom['commision']['base'] = $p_comm_base;
                                $pmntRetCom['commision']['amount'] = $p_comm_amount;
                            } else {
                                $pmntRetCom['commision']['base'] += $p_comm_base;
                                $pmntRetCom['commision']['amount'] += $p_comm_amount;
                            }
                            $pm_retcom_total += $p_comm_amount;
                        } else {
                            $p_comm_base = $sale->total * ($pmnt['amount'] / $sale->grand_total);
                            $p_comm_amount = $pm_d->commision_value;
                            if (!isset($pmntRetCom['commision'])) {
                                $pmntRetCom['commision']['entry_id'] =  $entryId;
                                $pmntRetCom['commision']['dc'] = 'D';
                                $pmntRetCom['commision']['narration'] = 'Comisión forma de pago '.$pm_d->name;
                                $pmntRetCom['commision']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['commision']['ledger_id'] = $pm_d->commision_ledger_id;
                                $pmntRetCom['commision']['amount'] = $p_comm_amount;
                                $pmntRetCom['commision']['base'] = $p_comm_base;
                            } else {
                                $pmntRetCom['commision']['amount'] = $p_comm_amount;
                                $pmntRetCom['commision']['base'] = $p_comm_base;
                            }
                            $pm_retcom_total += $p_comm_amount;
                        }
                    }
                    if ((isset($pmnt['pm_retefuente_value']) && $pmnt['pm_retefuente_value'] != NULL) || $pm_d->retefuente_value != NULL) {
                        if ((isset($pmnt['pm_retefuente_value']) && $pmnt['pm_retefuente_value'] != NULL)) {
                            $pm_d->retefuente_value = $pmnt['pm_retefuente_value'];
                        }
                        if (strpos($pm_d->retefuente_value, '%') !== false) {
                            $p_retefuente_perc = (Double)(str_replace("%", "", $pm_d->retefuente_value));
                            $p_retefuente_base = $sale->total * ($pmnt['amount'] / $sale->grand_total);
                            $p_retefuente_amount = $p_retefuente_base * ($p_retefuente_perc / 100);
                            if (!isset($pmntRetCom['retefuente'])) {
                                $pmntRetCom['retefuente']['entry_id'] =  $entryId;
                                $pmntRetCom['retefuente']['dc'] = 'D';
                                $pmntRetCom['retefuente']['narration'] = 'ReteFuente forma de pago '.$pm_d->name.' ('.$pm_d->retefuente_value.')';
                                $pmntRetCom['retefuente']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['retefuente']['ledger_id'] = $pm_d->retefuente_ledger_id;
                                $pmntRetCom['retefuente']['base'] = $p_retefuente_base;
                                $pmntRetCom['retefuente']['amount'] = $p_retefuente_amount;
                            } else {
                                $pmntRetCom['retefuente']['base'] += $p_retefuente_base;
                                $pmntRetCom['retefuente']['amount'] += $p_retefuente_amount;
                            }
                            $pm_retcom_total += $p_retefuente_amount;
                        } else {
                            $p_retefuente_base = $sale->total * ($pmnt['amount'] / $sale->grand_total);
                            $p_retefuente_amount = $pm_d->retefuente_value;
                            if (!isset($pmntRetCom['retefuente'])) {
                                $pmntRetCom['retefuente']['entry_id'] =  $entryId;
                                $pmntRetCom['retefuente']['dc'] = 'D';
                                $pmntRetCom['retefuente']['narration'] = 'ReteFuente forma de pago '.$pm_d->name;
                                $pmntRetCom['retefuente']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['retefuente']['ledger_id'] = $pm_d->retefuente_ledger_id;
                                $pmntRetCom['retefuente']['amount'] = $p_retefuente_amount;
                                $pmntRetCom['retefuente']['base'] = $p_retefuente_base;
                            } else {
                                $pmntRetCom['retefuente']['amount'] = $p_retefuente_amount;
                                $pmntRetCom['retefuente']['base'] = $p_retefuente_base;
                            }
                            $pm_retcom_total += $p_retefuente_amount;
                        }
                    }
                    if ((isset($pmnt['pm_reteiva_value']) && $pmnt['pm_reteiva_value'] != NULL) || $pm_d->reteiva_value != NULL) {
                        if ((isset($pmnt['pm_reteiva_value']) && $pmnt['pm_reteiva_value'] != NULL)) {
                            $pm_d->reteiva_value = $pmnt['pm_reteiva_value'];
                        }
                        if (strpos($pm_d->reteiva_value, '%') !== false) {
                            $p_reteiva_perc = (Double)(str_replace("%", "", $pm_d->reteiva_value));
                            $p_reteiva_base = $sale->total_tax * ($pmnt['amount'] / $sale->grand_total);
                            $p_reteiva_amount = $p_reteiva_base * ($p_reteiva_perc / 100);
                            if (!isset($pmntRetCom['reteiva'])) {
                                $pmntRetCom['reteiva']['entry_id'] =  $entryId;
                                $pmntRetCom['reteiva']['dc'] = 'D';
                                $pmntRetCom['reteiva']['narration'] = 'ReteIVA forma de pago '.$pm_d->name.' ('.$pm_d->reteiva_value.')';
                                $pmntRetCom['reteiva']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['reteiva']['ledger_id'] = $pm_d->reteiva_ledger_id;
                                $pmntRetCom['reteiva']['base'] = $p_reteiva_base;
                                $pmntRetCom['reteiva']['amount'] = $p_reteiva_amount;
                            } else {
                                $pmntRetCom['reteiva']['base'] += $p_reteiva_base;
                                $pmntRetCom['reteiva']['amount'] += $p_reteiva_amount;
                            }
                            $pm_retcom_total += $p_reteiva_amount;
                        } else {
                            $p_reteiva_base = $sale->total_tax * ($pmnt['amount'] / $sale->grand_total);
                            $p_reteiva_amount = $pm_d->reteiva_value;
                            if (!isset($pmntRetCom['reteiva'])) {
                                $pmntRetCom['reteiva']['entry_id'] =  $entryId;
                                $pmntRetCom['reteiva']['dc'] = 'D';
                                $pmntRetCom['reteiva']['narration'] = 'ReteIVA forma de pago '.$pm_d->name;
                                $pmntRetCom['reteiva']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['reteiva']['ledger_id'] = $pm_d->reteiva_ledger_id;
                                $pmntRetCom['reteiva']['amount'] = $p_reteiva_amount;
                                $pmntRetCom['reteiva']['base'] = $p_reteiva_base;
                            } else {
                                $pmntRetCom['reteiva']['amount'] = $p_reteiva_amount;
                                $pmntRetCom['reteiva']['base'] = $p_reteiva_base;
                            }
                            $pm_retcom_total += $p_reteiva_amount;
                        }
                    }
                    if ((isset($pmnt['pm_reteica_value']) && $pmnt['pm_reteica_value'] != NULL) || $pm_d->reteica_value != NULL) {
                        if ((isset($pmnt['pm_reteica_value']) && $pmnt['pm_reteica_value'] != NULL)) {
                            $pm_d->reteica_value = $pmnt['pm_reteica_value'];
                        }
                        if (strpos($pm_d->reteica_value, '%') !== false) {
                            $p_reteica_perc = (Double)(str_replace("%", "", $pm_d->reteica_value));
                            $p_reteica_base = $sale->total * ($pmnt['amount'] / $sale->grand_total);
                            $p_reteica_amount = $p_reteica_base * ($p_reteica_perc / 100);
                            if (!isset($pmntRetCom['reteica'])) {
                                $pmntRetCom['reteica']['entry_id'] =  $entryId;
                                $pmntRetCom['reteica']['dc'] = 'D';
                                $pmntRetCom['reteica']['narration'] = 'ReteICA forma de pago '.$pm_d->name.' ('.$pm_d->reteica_value.')';
                                $pmntRetCom['reteica']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['reteica']['ledger_id'] = $pm_d->reteica_ledger_id;
                                $pmntRetCom['reteica']['base'] = $p_reteica_base;
                                $pmntRetCom['reteica']['amount'] = $p_reteica_amount;
                            } else {
                                $pmntRetCom['reteica']['base'] += $p_reteica_base;
                                $pmntRetCom['reteica']['amount'] += $p_reteica_amount;
                            }
                            $pm_retcom_total += $p_reteica_amount;
                        } else {
                            $p_reteica_base = $sale->total * ($pmnt['amount'] / $sale->grand_total);
                            $p_reteica_amount = $pm_d->reteica_value;
                            if (!isset($pmntRetCom['reteica'])) {
                                $pmntRetCom['reteica']['entry_id'] =  $entryId;
                                $pmntRetCom['reteica']['dc'] = 'D';
                                $pmntRetCom['reteica']['narration'] = 'ReteICA forma de pago '.$pm_d->name;
                                $pmntRetCom['reteica']['companies_id'] = $pm_d->supplier_id;
                                $pmntRetCom['reteica']['ledger_id'] = $pm_d->reteica_ledger_id;
                                $pmntRetCom['reteica']['amount'] = $p_reteica_amount;
                                $pmntRetCom['reteica']['base'] = $p_reteica_base;
                            } else {
                                $pmntRetCom['reteica']['amount'] = $p_reteica_amount;
                                $pmntRetCom['reteica']['base'] = $p_reteica_base;
                            }
                            $pm_retcom_total += $p_reteiva_amount;
                        }
                    }
                }

                if (isset($sale->due_payment_method_id) && $sale->due_payment_method_id) {
                    $pmCredito = $this->getPaymentMethodById($sale->due_payment_method_id);
                    $metodoPago = $pmCredito->code;
                } else {
                    $billerDataAdd = $this->getAllCompaniesWithState('biller', $sale->biller_id);
                    if ($billerDataAdd->default_credit_payment_method) {
                        $metodoPago = $billerDataAdd->default_credit_payment_method;
                    } else {
                        $metodoPago = 'Credito';
                    }
                }
                $ledgerPago = $this->getWappsiReceipLedgerId($metodoPago, $contabilidadSufijo, $payment['biller_id']);
                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'] + (isset($entryDiscounts[$sale->reference_no]) ? $entryDiscounts[$sale->reference_no] : 0);
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Afecta a Facturas de Venta '.$sale->reference_no.(isset($entryDiscounts[$sale->reference_no]) ? ', Descuento ('.$this->sma->formatMoney($entryDiscounts[$sale->reference_no]).')' : '');
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
                if (isset($entryDiscounts[$sale->reference_no])) {
                    $main_part_discounts[$sale->reference_no] = 1;
                }
            } else {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = '(Descuento) Afecta a Facturas de Venta '.$sale->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $pmnt['discount_ledger_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
                $counter_part_discounts[$sale->reference_no] = 1;
            }
        }

        if ($entryDiscounts && count($entryDiscounts) > 0) {
            foreach ($main_part_discounts as $mpdis_reference => $setted) {
                unset($counter_part_discounts[$sale->reference_no]);
            }
            // if (count($counter_part_discounts) > 0) {
            //     foreach ($counter_part_discounts as $cp_dis_reference_no => $setted) {
            //         $ledgerPago = $this->getWappsiReceipLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
            //         $entryPayment['ledger_id'] = $ledgerPago;
            //         $entryPayment['entry_id'] =  $entryId;
            //         $entryPayment['amount'] = $entryDiscounts[$cp_dis_reference_no];
            //         $entryPayment['dc'] = 'C';
            //         $entryPayment['narration'] = '(Descuento) Afecta a Facturas de Venta '.$cp_dis_reference_no;
            //         $entryPayment['base'] = "";
            //         $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            //         $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
            //         $entryPayments[] = $entryPayment;
            //     }
            // }
        }

        $amountForPay = 0;
        if (count($retenciones) > 0) {
            foreach ($retenciones as $sale_id => $retencion) {
                $sale = $this->getWappsiSaleById($sale_id);
                if (isset($retencion['rete_fuente_total']) && $retencion['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_fuente_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente '.$retencion['rete_fuente_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_fuente_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_fuente_total'];
                }
                if (isset($retencion['rete_iva_total']) && $retencion['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_iva_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete IVA '.$retencion['rete_iva_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_iva_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_iva_total'];
                }
                if (isset($retencion['rete_ica_total']) && $retencion['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_ica_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ICA '.$retencion['rete_ica_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_ica_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_ica_total'];
                }
                if (isset($retencion['rete_bomberil_total']) && $retencion['rete_bomberil_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_bomberil_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa Bomberil '.$retencion['rete_bomberil_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_bomberil_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_bomberil_total'];
                }
                if (isset($retencion['rete_autoaviso_total']) && $retencion['rete_autoaviso_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros '.$retencion['rete_autoaviso_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_autoaviso_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_autoaviso_total'];
                }
                if (isset($retencion['rete_other_total']) && $retencion['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_other_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Other '.$retencion['rete_other_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_other_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_other_total'];
                }
            }
        }

        $concepto_positivo = 0;
        $concepto_negativo = 0;

        foreach ($conceptos as $concepto) {
            $entryPayment['entry_id'] =  $entryId;
            if ($concepto['amount'] < 0) {
                //$entryPayment['dc'] = 'C';
                $entryPayment['dc'] = 'D';
                $entryPayment['amount'] = $concepto['amount'] * -1;
                $concepto_negativo += $concepto['amount'];
            } else {
                //$entryPayment['dc'] = 'D';
                $entryPayment['dc'] = 'C';
                $entryPayment['amount'] = $concepto['amount'];
                $concepto_positivo += $concepto['amount'];
            }
            $entryPayment['narration'] = $concepto['note'];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $concepto['concept_ledger_id'];
            $entryPayment['cost_center_id'] = $concepto['cost_center_id'];
            $entryPayments[] = $entryPayment;
        }

        foreach ($pmntRetCom as $retcom_type => $retcom_row) {
            $entryPayments[] = $retcom_row;
        }

        if (count($pmntMethodTotal) > 0) {
            foreach ($pmntMethodTotal as $method => $amount) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $amount - $amountForPay + $concepto_positivo + $concepto_negativo - $pm_retcom_total;
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = $pmntMethodDesc[$method];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $pmntMethodLedgerId[$method];
                if ($cost_center != NULL) {
                    $entryPayment['cost_center_id'] = $cost_center;
                }
                $entryPayments[] = $entryPayment;
            }
        } else {
            $entryPayment['entry_id'] =  $entryId;
            $entryPayment['amount'] = $concepto_positivo + $concepto_negativo;
            $entryPayment['dc'] = 'D';
            $entryPayment['narration'] = '';
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $this->getWappsiReceipLedgerId($conceptos[0]['paid_by'], $contabilidadSufijo, $payment['biller_id']);;
            if ($cost_center != NULL) {
                $entryPayment['cost_center_id'] = $cost_center;
            }
            $entryPayments[] = $entryPayment;
        }

        $entryItems = array_merge($entryPayments);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){

            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function getMultiPayments($reference = null, $type = null, $filter_data = []){

        $start_date = $filter_data['start_date'];
        $end_date = $filter_data['end_date'];
        $document_type_id = $filter_data['document_type_id'];
        $biller_id = $filter_data['biller_id'];
        $user_id = $filter_data['user_id'];
        $supplier_id = $filter_data['supplier_id'];
        $customer_id = $filter_data['customer_id'];
        $payment_method = $filter_data['payment_method'];
        $filter_by = $filter_data['filter_by'];
        $this->load->library('datatables');
        $delete_payment = '';
        if ($type == 1) {
            $delete_payment = '<a href="'.admin_url('payments/cancel/').'$1"><i class="fas fa-trash"></i> '.lang('cancel_payment').' </a>';
        } else if ($type == 2) {
            $delete_payment = '<a href="'.admin_url('payments/pcancel/').'$1"><i class="fas fa-trash"></i> '.lang('cancel_ppayment').' </a>';
        }
        $edit_payment = '<a href="'.admin_url('payments/pedit/').'$1"><i class="fas fa-trash"></i> '.lang('edit_ppayment').' </a>';
        $action = '<div class="text-center"><div class="btn-group text-left">
                        <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                        <i class="fas fa-ellipsis-v fa-lg"></i></button>
                            <ul class="dropdown-menu pull-right" role="menu">

                               '.($this->Owner || $this->Admin || ($this->GP['payments-cancel'] && $type == 1) || ($this->GP['payments-pcancel'] && $type == 2) ? '<li>' . $delete_payment . '</li>' : '')
                                .(1 == 2 && ($this->Owner || $this->Admin || ($this->GP['payments-cancel'] && $type == 1) || ($this->GP['payments-pcancel'] && $type == 2)) ? '<li>' . $edit_payment . '</li>' : '');
                   $action .='</ul>
                        </div></div>';
        $this->datatables->select("
                          {$this->db->dbprefix('payments')}.reference_no as reference_no,
                          {$this->db->dbprefix('payments')}.reference_no as ref_2,
                          {$this->db->dbprefix('payments')}.date,
                          {$this->db->dbprefix('payments')}.payment_date,
                          {$this->db->dbprefix('payments')}.return_reference_no,
                          {$this->db->dbprefix('payments')}.consecutive_payment,
                          companies.name as ".($type == 1 ? "customer" : "supplier").",
                          payment_methods.name as paid_by,
                          SUM(
                                {$this->db->dbprefix('payments')}.amount
                                -
                                (
                                    ".($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_fuente_assumed = 0, {$this->db->dbprefix('payments')}.rete_fuente_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_fuente_total + ").
                                    ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_iva_assumed = 0, {$this->db->dbprefix('payments')}.rete_iva_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_iva_total + ").
                                    ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_ica_assumed = 0, {$this->db->dbprefix('payments')}.rete_ica_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_ica_total + ").
                                    ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_ica_assumed = 0, {$this->db->dbprefix('payments')}.rete_bomberil_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_bomberil_total + ").
                                    ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_ica_assumed = 0, {$this->db->dbprefix('payments')}.rete_autoaviso_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_autoaviso_total + ").
                                    ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_other_assumed = 0, {$this->db->dbprefix('payments')}.rete_other_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_other_total + ").
                                    "IF({$this->db->dbprefix('payments')}.paid_by = 'discount', {$this->db->dbprefix('payments')}.amount, 0)
                                )
                                ) as amount_applied,
                        ".($type != 3 ?
                                ($type == 1 ?
                                    "SUM(IF({$this->db->dbprefix('payments')}.sale_id IS NOT NULL, {$this->db->dbprefix('payments')}.amount, 0)) as amount"
                                    :
                                    "SUM(IF({$this->db->dbprefix('payments')}.purchase_id IS NOT NULL, {$this->db->dbprefix('payments')}.amount, 0)) as amount"
                                  )
                                :
                                "SUM({$this->db->dbprefix('payments')}.amount)"
                            ).",
                          {$this->db->dbprefix('payments')}.type,
                          {$this->db->dbprefix('payments')}.attachment

                          ")
                 ->join('payment_methods', 'payment_methods.code = payments.paid_by', 'left')
                 ->join('purchases', 'purchases.id = payments.purchase_id', 'left')
                 ->join('sales', 'sales.id = payments.sale_id', 'left')
                 ->join('companies', 'companies.id = IF(IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), '.$this->db->dbprefix("payments").'.seller_id)', 'left')
                 ->join('companies as biller', 'biller.id = IF('.$this->db->dbprefix("sales").'.biller_id IS NOT NULL, '.$this->db->dbprefix("sales").'.biller_id, IF('.$this->db->dbprefix("purchases").'.biller_id IS NOT NULL, '.$this->db->dbprefix("purchases").'.biller_id, '.$this->db->dbprefix("payments").'.seller_id))', 'left')
                 ->where('payments.paid_by != "due"')
                 ->where('payments.paid_by != "retencion"')
                 ->where('payments.multi_payment = "1"')
                 ->group_by('payments.reference_no');

        if ($reference != null) {
            $this->datatables->where('reference_no', $reference);
        }

        if ($type == 1) {
            $this->datatables->where('(payments.type = "received" OR payments.sale_id IS NOT NULL)');
        } else if ($type == 2) {
            $this->datatables->where('type !=', 'received')
                             ->where('(payments.purchase_id IS NOT NULL OR (payments.purchase_id IS NULL AND expense_id IS NULL AND (type = "sent" OR type = "sent_2")))');
        } else if ($type == 3) {
            $this->datatables->where('type !=', 'received')
                             ->where('expense_id !=', 'NULL');
        }

        /* FILTROS */

        if ($filter_by == 2) {
            $date_field_name = "payment_date";
        } else {
            $date_field_name = "date";
        }

        if ($start_date) {
            $this->db->where('payments.'.$date_field_name.' >= ', $start_date);
        }
        if ($end_date) {
            $this->db->where('payments.'.$date_field_name.' <= ', $end_date);
        }
        if ($document_type_id) {
            $this->db->where('payments.document_type_id', $document_type_id);
        }
        if ($user_id) {
            $this->db->where('payments.created_by', $user_id);
        }
        if ($payment_method) {
            $this->db->where('payments.paid_by', $payment_method);
        }
        if (!$this->Owner && !$this->Admin) {
            $biller_id = $this->session->userdata('biller_id');
        }
        if ($biller_id) {
            $this->db->where('biller.id', $biller_id);
        }
        if ($supplier_id) {
            $this->db->where('companies.id', $supplier_id);
        }
        if ($customer_id) {
            $this->db->where('companies.id', $customer_id);
        }

        /* FILTROS */

        if (!$this->Owner && !$this->Admin) {
            if ($biller_id) {
                if ($type == 1) {
                    $this->datatables->where('sales.biller_id IN ('.$biller_id.')');
                } else if ($type == 2) {
                    $this->datatables->where('purchases.biller_id IN ('.$biller_id.')');
                }
            }
        }

        if (!$this->Customer  && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($type == 1) {
                if ($this->session->userdata('company_id')) {
                    $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR sales.seller_id = '.$this->session->userdata('company_id').')');
                } else {
                    $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
                }
            }
                
        }

        $this->datatables->order_by('date desc');
        $this->datatables->from('payments');
        $this->datatables->add_column("Actions", $action, "reference_no");
        return $this->datatables->generate();
    }

    public function getMultiPaymentEnc($reference = null){
        $this->db->select(
                          'payments.reference_no,
                           payment_methods.name as paid_by,
                           SUM('.$this->db->dbprefix("payments").'.amount -
                               '.$this->db->dbprefix("payments").'.rete_fuente_total -
                               '.$this->db->dbprefix("payments").'.rete_iva_total -
                               '.$this->db->dbprefix("payments").'.rete_ica_total -
                               '.$this->db->dbprefix("payments").'.rete_other_total) as amount,
                           MAX('.$this->db->dbprefix("payments").'.type) AS type,
                           MAX('.$this->db->dbprefix("payments").'.date) AS date,
                           MAX('.$this->db->dbprefix("payments").'.payment_date) AS payment_date,
                           MAX('.$this->db->dbprefix("payments").'.consecutive_payment) AS consecutive_payment,
                           MAX('.$this->db->dbprefix("payments").'.note) AS note,
                           MAX('.$this->db->dbprefix("payments").'.document_type_id) AS document_type_id,
                           companies.name as customer,
                           companies.city as city,
                           companies.address as address,
                           MAX('.$this->db->dbprefix("sales").'.customer_id) AS customer_id,
                           MAX('.$this->db->dbprefix("purchases").'.supplier_id) AS supplier_id,
                           MAX('.$this->db->dbprefix("purchases").'.purchase_type) AS purchase_type,
                           payments.company_id,
                           companies.vat_no,
                           companies.digito_verificacion,
                           IF('.$this->db->dbprefix("purchases").'.cost_center_id IS NULL, sales.cost_center_id, '.$this->db->dbprefix("purchases").'.cost_center_id) as cost_center_id,
                           IF('.$this->db->dbprefix("purchases").'.biller_id IS NULL, sales.biller_id, '.$this->db->dbprefix("purchases").'.biller_id) as biller_id,
                           payments.return_id,
                           expense_categories.name as expense_name,
                           payments.created_by
                           '
                        )
                 ->join('sales', 'sales.id = payments.sale_id', 'left')
                 ->join('purchases', 'purchases.id = payments.purchase_id', 'left')
                 ->join('expense_categories', 'expense_categories.id = payments.expense_id', 'left')
                 ->join('companies', 'companies.id = IF(IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), '.$this->db->dbprefix("payments").'.seller_id)', 'left')
                 ->join('payment_methods', 'payment_methods.code = payments.paid_by', 'left')
                 ->where('payments.paid_by != "due"')
                 ->where('payments.paid_by != "retencion"')
                 ->where('payments.paid_by != "discounted"')
                 ->where('payments.concept_company_id IS NULL')
                 ->where('payments.multi_payment = "1"')
                 ->group_by('payments.reference_no');
        if ($reference != null) {
            $this->db->where('payments.reference_no', $reference);
        }
        $pmnt = $this->db->get('payments');
        if ($pmnt->num_rows() > 0) {
            foreach (($pmnt->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getDetailMultiPayment($reference, $type = 1){
        $pmnt = $this->db->select("
                                    IF(
                                        ".$this->db->dbprefix."sales.reference_no IS NOT NULL,
                                        ".$this->db->dbprefix."sales.reference_no,
                                        ".$this->db->dbprefix."purchases.reference_no
                                    ) as reference_no,
                                    payments.amount,
                                    payments.note,
                                    payments.rete_fuente_total,
                                    payments.rete_iva_total,
                                    payments.rete_ica_total,
                                    payments.rete_bomberil_total,
                                    payments.rete_autoaviso_total,
                                    payments.rete_other_total,
                                    payments.rete_fuente_assumed,
                                    payments.rete_iva_assumed,
                                    payments.rete_ica_assumed,
                                    payments.rete_other_assumed,
                                    payments.type,
                                    payments.sale_id,
                                    payments.purchase_id,
                                    payments.expense_id,
                                    payments.comm_paid_sale_reference,
                                    (SELECT email FROM {$this->db->dbprefix('users')} WHERE id = {$this->db->dbprefix('payments')}.created_by) AS user_cr,
                                    (SELECT CONCAT(first_name, ' ', last_name) FROM {$this->db->dbprefix('users')} WHERE id = {$this->db->dbprefix('payments')}.created_by) AS user_cr_name,
                                    payments.invoice_affected_balance
                                    ")
                 ->where('payments.paid_by != "due"')
                 ->where('payments.paid_by != "retencion"')
                 ->where('payments.multi_payment = "1"')
                 ->where('payments.reference_no', $reference)
                 ->join('sales', 'sales.id = payments.sale_id', 'left')
                 ->join('purchases', 'purchases.id = payments.purchase_id', 'left')
                 // ->group_by('sales.reference_no')
                 ->get('payments');
        if ($pmnt->num_rows() > 0) {
            foreach (($pmnt->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_advance_payments_sale($sales_id)
    {
        $this->db->select("payments.amount, payments.date");
        $this->db->from("sales");
        $this->db->join("payments", "payments.sale_id = sales.id AND payments.date = sales.date", "INNER");
        $this->db->where("sales.id", $sales_id);
        $this->db->where("payments.paid_by", "deposit");
        $response = $this->db->get();

        return $response->result();
    }

    // public function getAllBillers() {
    //     $this->db->select("name AS nombre");
    //     $this->db->from("companies");
    //     $this->db->like("group_name", "biller");
    //     $response = $this->db->get();

    //     return $response->result();
    // }

    public function getPriceGroupBase($product_id = NULL){
        $this->db->select('products.consumption_sale_tax as consumption_sale_tax, product_prices.*')
              ->join('price_groups', 'price_groups.id = product_prices.price_group_id AND price_groups.price_group_base = 1')
              ->join('products', 'products.id = product_prices.product_id');
        if ($product_id) {
            $this->db->where('product_prices.product_id', $product_id);
        }
        $q = $this->db->get('product_prices');

        if ($q->num_rows() > 0) {

            if ($product_id) {
                return $q->row();
            } else {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }

        }

        return false;
    }

    public function getProductsByTaxMethod($tax_method, $different = false){
        if ($different) {
            $q = $this->db->get_where('products', array('tax_method != ' => $tax_method));
        } else {
            $q = $this->db->get_where('products', array('tax_method' => $tax_method));
        }
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function updateAllProductsTaxMethod($tax_method){
        $this->db->update('products', array('tax_method' => $tax_method), array('id >' => 0));
    }

    public function getAllPrinters(){
        $q = $this->db->get('printers');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getUsersByGroupId($group_id){
        $q = $this->db->get_where('users', array('group_id' => $group_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getUserGroupByName($group_name){
        $q = $this->db->get_where('groups', array('name' => $group_name));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_customer_birthdays($today = false){

        $actual_month = date('m');
        $actual_day = date('d');

        $conditions['group_name'] = 'customer';
        $conditions['birth_month'] = $actual_month;
        if ($today) {
            $conditions['birth_day'] = $actual_day;
        }

        $cbd = $this->db->get_where('companies', $conditions);

        if ($cbd->num_rows() > 0) {
            return $cbd->num_rows();
        }

        return false;
    }

    public function getInvoiceFooter($document_type_id = NULL, $ref_no = NULL){
        if ($document_type_id) {
            $document_type = $this->db->get_where('documents_types', array('id' => $document_type_id));
            if ($document_type->num_rows() > 0) {
                $dt = $document_type->row();
                return $dt->invoice_footer;
            }
        } else if ($ref_no) {
            $sales_prefix = explode("-", $ref_no);
            $sales_prefix = $sales_prefix[0];
            $document_type = $this->db->get_where('documents_types', array('sales_prefix' => $sales_prefix));
            if ($document_type->num_rows() > 0) {
                $dt = $document_type->row();
                return $dt->invoice_footer;
            }
        }
        return false;
    }

    public function getInvoiceHeader($document_type_id = NULL, $ref_no = NULL){
        if ($document_type_id) {
            $document_type = $this->db->get_where('documents_types', array('id' => $document_type_id));
            if ($document_type->num_rows() > 0) {
                $dt = $document_type->row();
                return $dt->invoice_header;
            }
        } else if ($ref_no) {
            $sales_prefix = explode("-", $ref_no);
            $sales_prefix = $sales_prefix[0];
            $document_type = $this->db->get_where('documents_types', array('sales_prefix' => $sales_prefix));
            if ($document_type->num_rows() > 0) {
                $dt = $document_type->row();
                return $dt->invoice_header;
            }
        }
        return false;
    }

    public function getSettingsCon($date = null){
        if($this->wappsiContabilidadVerificacion($date) > 0) {
            // $this->sma->print_arrays($items);
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $settings_con = $this->db->get('settings_con'.$contabilidadSufijo);
            if ($settings_con->num_rows() > 0) {
                return $settings_con->row();
            }
        }
        return false;
    }

    public function getProductsByCategory($category_id){
        $q = $this->db->get_where('products', array('category_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function getProductsBySubCategory($category_id){
        $q = $this->db->get_where('products', array('subcategory_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    public function getProductsBySubSubCategory($category_id){
        $q = $this->db->get_where('products', array('second_level_subcategory_id' => $category_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }
    // Verify mysql db (DB1) connection
    public function check_database($config)
    {
        //  Check if using mysqli driver
        if( $config['dbdriver'] === 'mysqli' )
        {
            // initilize mysqli connection
            @$mysqli = new mysqli( $config['hostname'] , $config['username'] , $config['password'] , $config['database'] );
            // Check database connection
            if( !$mysqli->connect_error )
            {
                // if no connection errors are found close connection and return true
                @$mysqli->close();
                return true;
            } else {
                exit(var_dump($mysqli->connect_error));
            }
        }
        // else return false
        return false;
    }

    public function url_origin($use_forwarded_host=false) {

      $s = $_SERVER;

      $ssl = ( ! empty($s['HTTPS']) && $s['HTTPS'] == 'on' ) ? true:false;
      $sp = strtolower( $s['SERVER_PROTOCOL'] );
      $protocol = substr( $sp, 0, strpos( $sp, '/'  )) . ( ( $ssl ) ? 's' : '' );

      $port = $s['SERVER_PORT'];
      $port = ( ( ! $ssl && $port == '80' ) || ( $ssl && $port=='443' ) ) ? '' : ':' . $port;

      $host = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
      $host = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;

      return $protocol . '://' . $host;

    }

    public function get_project_root_folder_name(){
        $root = str_replace($this->url_origin(), "", base_url());
        $root = str_replace("/", "", $root);
        return $root;
    }

    public function get_all_units($page = 0, $paginate = false) {
        if ($paginate != false) {
            if ($page > 0) {
                $pg_inicio = 7*($page);
            } else {
                $pg_inicio = $page;
            }
            $q = $this->db->order_by('id', 'asc')->get("units", 7, $pg_inicio);
        } else {
            $q = $this->db->order_by('id', 'asc')->get("units");
        }
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function getAllProductsUnitPrices($page) {

        $data = [];
        $s = $this->db->get('products');
        if ($s->num_rows() > 0) {
            foreach (($s->result()) as $row) {
                if ($row->price > 0) {
                    $data[$row->id][$row->unit] = $row->price + ($row->tax_method == 0 && $this->Settings->ipoconsumo ? $row->consumption_sale_tax : 0);
                }
            }
        }

        $q = $this->db->select('
                                    unit_prices.*,
                                    units.operation_value as unit_operation_value,
                                    units.operator as unit_operator,
                                    products.tax_method as product_tax_method,
                                    products.consumption_sale_tax as consumption_sale_tax
                                ')
                        ->join('products', 'products.id = unit_prices.id_product')
                        ->join('units', 'units.id = unit_prices.unit_id')
                        ->order_by('unit_prices.id', 'asc')
                        ->get('unit_prices');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->valor_unitario >= 0) {
                    $consumption_sale_tax = $row->consumption_sale_tax;
                    if ($row->unit_operator && $row->unit_operation_value > 0 && $this->Settings->precios_por_unidad_presentacion == 2) {
                        if ($row->unit_operator == "*") {
                            $consumption_sale_tax = $row->consumption_sale_tax * $row->unit_operation_value;
                        } else if ($row->unit_operator == "/") {
                            $consumption_sale_tax = $row->consumption_sale_tax / $row->unit_operation_value;
                        }
                    }
                    $data[$row->id_product][$row->unit_id] = $row->valor_unitario + ($row->product_tax_method == 0 && $this->Settings->ipoconsumo ? $consumption_sale_tax : 0);
                }
            }
        }

        if (count($data) > 0) {
            return $data;
        }
        return FALSE;
    }

    public function get_price_group_base(){
        $q = $this->db->get_where('price_groups', array('price_group_base'=>1));
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_all_product_units_prices($pr_id){
        $q = $this->db->get_where('unit_prices', ['id_product' => $pr_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_all_customer_addresses($seller_id = NULL){
        $condicion = "";
        if ($seller_id) {
            $condicion = ', IF(sma_addresses.customer_address_seller_id_assigned = '.$seller_id.', "1", "0") as selected';
        }
        if ($seller_id) {
            $this->db->where('addresses.customer_address_seller_id_assigned', $seller_id);
        }
        $q = $this->db->select('addresses.*, companies.vat_no as vat_no_company , companies.name as company_name'.$condicion)
                      ->join('companies', 'companies.id = addresses.company_id')
                      ->where('companies.group_name', 'customer')
                      ->having('selected', 1)
                      ->get('addresses');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_item_price($row, $customer, $customer_group, $biller, $address_id, $unit_price_id = null, $ignore_promo = false)
    {

        if ($address_id) {
            $address = $this->getAddressByID($address_id);
            if ($address) {
                $customer_address_price_group_id = $address->price_group_id;
                $customer_address_customer_group_id = $address->customer_group_id;
                $customer_address_customer_group = $this->getCustomerGroupByID($customer_address_customer_group_id);
            }
        }

        if (!$customer_group) {
            $customer_group = new stdClass;
            $customer_group->percent = 0;
        }

        $new_discount = 0;

        if ($this->Settings->prioridad_precios_producto == 1 || $this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) {

            if ($this->sma->isPromo($row) && !isset($row->no_validate_promo) && !$ignore_promo) {
                $row->price = $row->promo_price;
            } else if ($product_price_base = $this->site->getPriceGroupBase($row->id)) {
                $row->price = $product_price_base->price;
                if ($this->Settings->prioridad_precios_producto == 5) {
                    $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                    $new_discount = $customer_group->percent;
                }
            } else {
                $row->price = 0;
            }

        } else if ($this->Settings->prioridad_precios_producto == 2) {

            if ($biller && $biller->default_price_group) {
                if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $biller->default_price_group)) {
                    $row->price = $pr_group_price->price;
                } else {
                    $row->price = 0;
                }
                $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                $new_discount = $customer_group->percent;
            } else {
                $row->price = 0;
            }

        } else if ($this->Settings->prioridad_precios_producto == 3) {

            if ($customer && $customer->price_group_id) {
                if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer->price_group_id)) {
                    $row->price = $pr_group_price->price;
                } else {
                    $row->price = 0;
                }
                $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                $new_discount = $customer_group->percent;
            } else {
                $row->price = 0;
            }

        } else if ($this->Settings->prioridad_precios_producto == 8) {//sucursal del cliente

            if (isset($customer_address_price_group_id) && $customer_address_price_group_id) {
                $pr_group_price = $this->site->getProductGroupPrice($row->id, $customer_address_price_group_id);
                $row->price = $pr_group_price->price;
            } else {
                $row->price = 0;
            }

        } else if ($this->Settings->prioridad_precios_producto == 4 || $this->Settings->prioridad_precios_producto == 6 || $this->Settings->prioridad_precios_producto == 9) {

            if ($this->sma->isPromo($row) && !isset($row->no_validate_promo) && !$ignore_promo) {

                $row->price = $row->promo_price;

            } else {

                if ($this->Settings->prioridad_precios_producto == 4) { //sucursal | cliente

                    if ($biller && $biller->default_price_group) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $biller->default_price_group)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    } elseif ($customer && $customer->price_group_id) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer->price_group_id)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    }

                } else if ($this->Settings->prioridad_precios_producto == 6) { // cliente | sucursal

                    if ($customer && $customer->price_group_id) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer->price_group_id)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    } elseif ($biller && $biller->default_price_group) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $biller->default_price_group)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    }

                } else if ($this->Settings->prioridad_precios_producto == 9) { //sucursal del cliente



                    if (isset($customer_address_price_group_id) && $customer_address_price_group_id) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $customer_address_price_group_id)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    } elseif ($biller && $biller->default_price_group) {

                        if ($pr_group_price = $this->site->getProductGroupPrice($row->id, $biller->default_price_group)) {
                            $row->price = $pr_group_price->price;
                        } else {
                            $row->price = 0;
                        }

                    }

                }

            }
            if ($this->Settings->prioridad_precios_producto == 9 && isset($customer_address_customer_group) && $customer_address_customer_group) {//sucursal del cliente
                // $row->price = $row->price + (($row->price * $customer_address_customer_group->percent) / 100);
                $row->price = $row->price;
                $new_discount = $customer_address_customer_group->percent;
            } else {
                // $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                $row->price = $row->price;
                $new_discount = $customer_group->percent;
            }
        } else if ($this->Settings->prioridad_precios_producto == 11) { //híbrido

            if ($customer && $customer->price_group_id) {
                if (!$unit_price_id) {
                    $unit_price_id = $row->unit;
                }
                if ($pr_group_price = $this->site->get_all_product_hybrid_prices($row->id)) {
                    $row->price = isset($pr_group_price[$row->id][$unit_price_id][$customer->price_group_id]) ? $pr_group_price[$row->id][$unit_price_id][$customer->price_group_id] : 0;
                } else {
                    $row->price = 0;
                }
                $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                $new_discount = $customer_group->percent;
            } else {
                $row->price = 0;
            }

        }
        $data['new_price'] = $row->price;
        $data['new_discount'] = $new_discount * -1;
        return $data;
    }

    public function get_company_by_field_name($field_name, $field_value, $type)
    {
        $q = $this->db->get_where('companies', [$field_name => $field_value, 'group_name' => $type], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_warehouse_by_code($wh_code)
    {
        $q = $this->db->get_where('warehouses', ['code' => $wh_code], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_address_by_code($adr_code)
    {
        $q = $this->db->get_where('addresses', ['code' => $adr_code], 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function wappsiContabilidadPagosMultiplesCompras($payment = array(), $customer_id = null, $retenciones = array(), $data_taxrate_traslate = array(), $conceptos = array(), $cost_center = NULL)
    {
      if($this->wappsiContabilidadVerificacion($payment['date']) > 0){

        if (!$this->validate_doc_type_accounting($payment['document_type_id'])) {
            return false;
        }

        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $prefix = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
        $position2 = strpos($payment['reference_no'], '-');
        if($position2 !== false){
            $label = substr($payment['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($payment['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($payment['date'], 0, 10);
        $wappsiEntri['dr_total'] = $payment['amount'];
        $wappsiEntri['cr_total'] = $payment['amount'];
        $wappsiEntri['notes'] = "Pago múltiple ".$payment['reference_no'].(isset($payment['note']) ? " Nota : ".$payment['note'] : "");
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $payment['companies_id'];
        $wappsiEntri['user_id'] = $payment['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($payment, true)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryPayments = [];
        $entryDiscounts = [];
        $pmntMethodTotal = [];
        $pmntMethodDesc = [];
        $pmntMethodLedgerId = [];
        $purchases_data = [];
        foreach ($payment['payments'] as $pmnt) {
            $paymentMethodCon = $pmnt['paid_by'];
            $purchase = $this->getWappsipurchaseById($pmnt['purchase_id']);
            $purchases_data[$pmnt['purchase_id']] = $purchase;
            if ($paymentMethodCon == 'discount') {
                if (!isset($entryDiscounts[$purchase->reference_no])) {
                    $ledgerPago = $this->getWappsiPaymentLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
                    $entryDiscounts[$purchase->reference_no] = $pmnt['amount'];
                } else {
                    $entryDiscounts[$purchase->reference_no] += $pmnt['amount'];
                }
            }
        }

        foreach ($payment['payments'] as $pmnt) {
            $paymentMethodCon = $pmnt['paid_by'];
            $purchase = isset($purchases_data[$pmnt['purchase_id']]) ? $purchases_data[$pmnt['purchase_id']] : NULL;
            if ($paymentMethodCon != 'discount') {
                if (!isset($pmntMethodTotal[$paymentMethodCon])) {
                    $pmntMethodTotal[$paymentMethodCon] = $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] = 'Afecta a Facturas de Compra '.($purchase ? $purchase->reference_no : '');
                    $pmntMethodLedgerId[$paymentMethodCon] = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
                } else {
                    $pmntMethodTotal[$paymentMethodCon] += $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] .= ', '.($purchase ? $purchase->reference_no : '');
                }
                if (isset($pmnt['purchase']) && $pmnt['purchase']->expense_causation == 1) {
                    $payment_discount_expense = (isset($entryDiscounts[$purchase->reference_no]) ? $entryDiscounts[$purchase->reference_no] : 0);
                    foreach ($pmnt['purchase_items'] as $pur_item) {
                        $expense_category = $this->getExpenseCategory($pur_item->product_id);
                        $prorrated_payment_amount = $pmnt['amount'] * (($pur_item->unit_cost * $pur_item->quantity) / $pmnt['purchase']->grand_total);
                        $prorrated_discount_amount = $payment_discount_expense * (($pur_item->unit_cost * $pur_item->quantity) / $pmnt['purchase']->grand_total);
                        $entryPayment['ledger_id'] = $expense_category->creditor_ledger_id;
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $prorrated_payment_amount + $prorrated_discount_amount;
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Afecta a Facturas de Compra '.$purchase->reference_no.' Gasto '.$pur_item->product_name.($prorrated_discount_amount > 0 ? ', Descuento ('.$this->sma->formatMoney($prorrated_discount_amount).')' : '');
                        $entryPayment['base'] = "";
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                        $entryPayments[] = $entryPayment;
                    }
                } else {
                    $ledgerPago = $this->getWappsiPaymentLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
                    $entryPayment['ledger_id'] = $ledgerPago;
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $pmnt['amount'] + (isset($entryDiscounts[$purchase->reference_no]) ? $entryDiscounts[$purchase->reference_no] : 0);
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Afecta a Facturas de Compra '.$purchase->reference_no.(isset($entryDiscounts[$purchase->reference_no]) ? ', Descuento ('.$this->sma->formatMoney($entryDiscounts[$purchase->reference_no]).')' : '');
                    $entryPayment['base'] = "";
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                    $entryPayments[] = $entryPayment;
                }
            } else {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = '(Descuento) Afecta a Facturas de Compra '.$purchase->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $pmnt['discount_ledger_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
            }
        }


        // $this->sma->print_arrays($entryPayments);

        $amountForPay = 0;
        if (count($retenciones) > 0) {
            foreach ($retenciones as $purchase_id => $retencion) {
                $purchase = $this->getWappsipurchaseById($purchase_id);

                if (isset($retencion['rete_fuente_total']) && $retencion['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_fuente_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Fuente '.($retencion['rete_fuente_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_fuente_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_fuente_account'];
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_fuente_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_fuente_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete Fuente Asumida '.$retencion['rete_fuente_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_fuente_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_fuente_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_fuente_total'];
                    }
                }

                if (isset($retencion['rete_iva_total']) && $retencion['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_iva_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete IVA '.($retencion['rete_iva_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_iva_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_iva_account'];
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_iva_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_iva_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete IVA Asumida '.$retencion['rete_iva_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_iva_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_iva_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_iva_total'];
                    }
                }

                if (isset($retencion['rete_ica_total']) && $retencion['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_ica_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete ICA '.($retencion['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_ica_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_ica_account'];
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_ica_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete ICA Asumida '.$retencion['rete_ica_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_ica_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_ica_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_ica_total'];
                    }
                }

                if (isset($retencion['rete_other_total']) && $retencion['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_other_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Otros '.($retencion['rete_other_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_other_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_other_account'];
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_other_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_other_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Rete Otros Asumida '.$retencion['rete_other_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_other_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_other_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_other_total'];
                    }
                }

                if (isset($retencion['rete_bomberil_total']) && $retencion['rete_bomberil_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_bomberil_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Tasa Bomberil '.($retencion['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_bomberil_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_bomberil_account'];
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_bomberil_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Tasa Bomberil Asumida '.$retencion['rete_bomberil_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_bomberil_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_bomberil_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_bomberil_total'];
                    }
                }

                if (isset($retencion['rete_autoaviso_total']) && $retencion['rete_autoaviso_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros '.($retencion['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_autoaviso_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_autoaviso_account'];
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_autoaviso_total'];
                        $entryPayment['dc'] = 'D';
                        $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros Asumida '.$retencion['rete_autoaviso_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_autoaviso_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_autoaviso_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_autoaviso_total'];
                    }
                }
            }
        }

        $concepto_positivo = 0;
        $concepto_negativo = 0;
        $concept_third_id = NULL;
        foreach ($conceptos as $concepto) {
            $entryPayment['entry_id'] =  $entryId;
            if ($concepto['amount'] < 0) {
                $entryPayment['dc'] = 'C';
                $entryPayment['amount'] = $concepto['amount'] * -1;
                $concepto_negativo += $concepto['amount'];
            } else {
                $entryPayment['dc'] = 'D';
                $entryPayment['amount'] = $concepto['amount'];
                $concepto_positivo += $concepto['amount'];
            }
            $entryPayment['narration'] = $concepto['note'];
            $entryPayment['base'] = $concepto['concept_base'] > 0 ? $concepto['concept_base'] : 0;
            $entryPayment['companies_id'] = $concepto['concept_company_id'];
            $entryPayment['ledger_id'] = $concepto['ledger_id'];
            $entryPayment['cost_center_id'] = $concepto['cost_center_id'];
            $entryPayments[] = $entryPayment;

            $paymentMethodCon = $concepto['paid_by'];
            if (!isset($pmntMethodTotal[$paymentMethodCon])) {
                $pmntMethodTotal[$paymentMethodCon] = $concepto['amount'];
                $pmntMethodDesc[$paymentMethodCon] = 'Afecta a Facturas de Compra ';
                $pmntMethodLedgerId[$paymentMethodCon] = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
            } else {
                $pmntMethodTotal[$paymentMethodCon] += $concepto['amount'];
                $pmntMethodDesc[$paymentMethodCon] .= ', ';
            }
            $concept_third_id = $concepto['concept_company_id'];
        }
        // $this->sma->print_arrays($pmntMethodTotal);
        if (count($pmntMethodTotal) > 0) {
            foreach ($pmntMethodTotal as $method => $amount) {
                $ledgerPago = $this->getWappsiPaymentLedgerId($method, $contabilidadSufijo, $payment['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $amount - $amountForPay;
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = $pmntMethodDesc[$method];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = !empty($wappsiEntri['companies_id']) ? $wappsiEntri['companies_id'] : $concept_third_id;
                $entryPayment['ledger_id'] = $ledgerPago;
                if ($cost_center != NULL) {
                    $entryPayment['cost_center_id'] = $cost_center;
                }
                $entryPayments[] = $entryPayment;
            }
        }

        $entryItems = array_merge($entryPayments);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if($entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function get_ledger_by_id($ledger_id)
    {
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $q = $this->db->get_where('ledgers_con'.$contabilidadSufijo, ['id' => $ledger_id]);
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }
        return FALSE;
    }

    public function get_document_type_by_prefix($prefix)
    {
        $q = $this->db->get_where('documents_types', ['sales_prefix' => $prefix]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_units_by_price_group($pg_id, $pr_id)
    {
        // $q = $this->db->get_where("units", array('price_group_id' => $pg_id));

        $q = $this->db->select('units.*')
                ->join('unit_prices', 'unit_prices.unit_id = units.id')
                ->where('unit_prices.id_product', $pr_id)
                ->where('units.price_group_id', $pg_id)
                ->get('units');


        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function get_product_units($product_id)
    {
        $q = $this->db->query("
            SELECT
                2 as num,
                U.id as product_unit_id,
                U.name,
                U.code,
                U.operator,
                IF(U.operation_value > 0, U.operation_value, 1) as operation_value,
                UP.valor_unitario,
                UP.valor_unitario AS valor_unitario_old,
                UP.margin_update_price,
                UP.id

            FROM {$this->db->dbprefix('unit_prices')} AS UP
                INNER JOIN {$this->db->dbprefix('units')} AS U ON U.id = UP.unit_id
            WHERE UP.id_product = {$product_id} AND UP.status = 1

                UNION

            SELECT
                1 as num,
                U2.id as product_unit_id,
                U2.name,
                U2.code,
                '*' as operator,
                1 as operation_value,
                P.price as valor_unitario,
                P.price as valor_unitario_old,
                P.main_unit_margin_update_price as margin_update_price,
                0 as id

            FROM {$this->db->dbprefix('products')} AS P
                INNER JOIN {$this->db->dbprefix('units')} AS U2 ON U2.id = P.unit
            WHERE P.id = {$product_id}

            ORDER BY num ASC
            ");
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->product_unit_id] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_document_types_by_module($module){
        $q = $this->db->get_where('documents_types', ['module' => $module]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_collection_discounts(){
        $q = $this->db->get('collection_discounts');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getCustomerBranchByCustomerAndAddresId($customer_id, $address_id){
        $q = $this->db->get_where('addresses', ['company_id' => $customer_id, 'id' => $address_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function wappsiContabilidadPagosMultiplesGastos($payment = array(), $customer_id = null, $retenciones = array(), $data_taxrate_traslate = array(), $conceptos = array(), $cost_center = NULL)
    {
      if($this->wappsiContabilidadVerificacion($payment['date']) > 0){
        if (!$this->validate_doc_type_accounting($payment['document_type_id'])) {
            return false;
        }
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $prefix = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;

        $label = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
        $position2 = strpos($payment['reference_no'], '-');
        if($position2 !== false){
            $label = substr($payment['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($payment['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($payment['date'], 0, 10);
        $wappsiEntri['dr_total'] = $payment['amount'];
        $wappsiEntri['cr_total'] = $payment['amount'];
        $wappsiEntri['notes'] = "Pago múltiple ".$payment['reference_no']." Nota : ".$payment['note'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $payment['companies_id'];
        $wappsiEntri['user_id'] = $payment['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($payment, true)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryPayments = [];
        $entryDiscounts = [];
        $pmntMethodTotal = [];
        $pmntMethodDesc = [];
        $pmntMethodLedgerId = [];
        foreach ($payment['payments'] as $pmnt) {
            $expense_data = $pmnt['expense_data'];
            $expense_category = $this->getExpenseCategory($pmnt['expense_id']);
            if ($expense_data['tax_rate_id'] > 0 && $expense_category->tax_ledger_id > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $expense_data['tax_rate_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal($expense_data['tax_val']);
                $entryItemIva['dc'] = 'D';
                $entryItemIva['narration'] = 'Iva 1 sobre gasto '.$expense_category->name.' '.$payment['reference_no'];
                $entryItemIva['base'] = 0;
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_ledger_id;
                $entryPayments[] = $entryItemIva;
            }
            if ($expense_data['tax_rate_2_id'] > 0 && $expense_category->tax_2_ledger_id > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $expense_data['tax_rate_2_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal($expense_data['tax_val_2']);
                $entryItemIva['dc'] = 'D';
                $entryItemIva['narration'] = 'Iva 2 sobre gasto'.$expense_category->name.' '.$payment['reference_no'];
                $entryItemIva['base'] = 0;
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_2_ledger_id;
                $entryPayments[] = $entryItemIva;
            }
            $paymentMethodCon = $pmnt['paid_by'];
            if ($paymentMethodCon != 'discount') {
                if (!isset($pmntMethodTotal[$paymentMethodCon])) {
                    $pmntMethodTotal[$paymentMethodCon] = $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] = 'Afecta a Gasto '.$expense_category->name.' '.$payment['reference_no'].' ('.$pmnt['comm_paid_sale_reference'].')';
                    $pmntMethodLedgerId[$paymentMethodCon] = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
                } else {
                    $pmntMethodTotal[$paymentMethodCon] += $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] .= ', '.$expense_category->name."(".$pmnt['comm_paid_sale_reference'].")";
                }
                $entryPayment['ledger_id'] = $expense_category->ledger_id;
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = (($pmnt['amount'] - $expense_data['tax_val'] - $expense_data['tax_val_2']) * ($pmnt['amount'] < 0 ? -1 : 1));
                $entryPayment['dc'] = $pmnt['amount']< 0 ? 'C' : 'D';
                $entryPayment['narration'] = 'Afecta a Gasto '.$expense_category->name.' '.$payment['reference_no'].' ('.$pmnt['comm_paid_sale_reference'].')';
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
            } else {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = '(Descuento) Afecta a Gasto '.$expense_category->name.' '.$payment['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $pmnt['discount_ledger_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
                if (!isset($entryDiscounts[$expense_category->name])) {
                    $ledgerPago = $this->getWappsiPaymentLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
                    $entryDiscount['ledger_id'] = $ledgerPago;
                    $entryDiscount['entry_id'] =  $entryId;
                    $entryDiscount['amount'] = $pmnt['amount'];
                    $entryDiscount['dc'] = 'C';
                    $entryDiscount['narration'] = '(Descuento) Afecta a Gasto '.$expense_category->name.' '.$payment['reference_no'];
                    $entryDiscount['base'] = "";
                    $entryDiscount['companies_id'] = $wappsiEntri['companies_id'];
                    $entryDiscount['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                    $entryDiscounts[$expense_category->name] = $entryDiscount;
                } else {
                    $entryDiscounts[$expense_category->name]['amount'] += $pmnt['amount'];
                }
            }
        }
        $amountForPay = 0;
        // exit(json_encode($retenciones));
        if (count($retenciones) > 0) {
            foreach ($retenciones as $expense_id => $retencion) {
                $expense = $this->getExpenseCategory($expense_id);
                if (isset($retencion['rete_fuente_total']) && $retencion['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_fuente_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Fuente '.$retencion['rete_fuente_percentage'].'% '.$expense->name.' '.$payment['reference_no'];
                    $entryPayment['base'] = $retencion['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_fuente_account'];
                    // $entryPayment['cost_center_id'] = ($this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_fuente_total'];
                }
                if (isset($retencion['rete_iva_total']) && $retencion['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_iva_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete IVA '.$retencion['rete_iva_percentage'].'% '.$expense->name.' '.$payment['reference_no'];
                    $entryPayment['base'] = $retencion['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_iva_account'];
                    // $entryPayment['cost_center_id'] = ($this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_iva_total'];
                }
                if (isset($retencion['rete_ica_total']) && $retencion['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_ica_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete ICA '.$retencion['rete_ica_percentage'].'% '.$expense->name.' '.$payment['reference_no'];
                    $entryPayment['base'] = $retencion['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_ica_account'];
                    // $entryPayment['cost_center_id'] = ($this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_ica_total'];
                }
                if (isset($retencion['rete_other_total']) && $retencion['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_other_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Other '.$retencion['rete_other_percentage'].'% '.$expense->name.' '.$payment['reference_no'];
                    $entryPayment['base'] = $retencion['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_other_account'];
                    // $entryPayment['cost_center_id'] = ($this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_other_total'];
                }
            }
        }
        $concepto_positivo = 0;
        $concepto_negativo = 0;
        foreach ($conceptos as $concepto) {
            $entryPayment['entry_id'] =  $entryId;
            if ($concepto['amount'] < 0) {
                //$entryPayment['dc'] = 'C';
                $entryPayment['dc'] = 'C';
                $entryPayment['amount'] = $concepto['amount'] * -1;
                $concepto_negativo += $concepto['amount'];
            } else {
                //$entryPayment['dc'] = 'D';
                $entryPayment['dc'] = 'D';
                $entryPayment['amount'] = $concepto['amount'];
                $concepto_positivo += $concepto['amount'];
            }
            $entryPayment['narration'] = $concepto['note'];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $concepto['ledger_id'];
            $entryPayment['cost_center_id'] = $concepto['cost_center_id'];
            $entryPayments[] = $entryPayment;
        }
        foreach ($pmntMethodTotal as $method => $amount) {
            //exit(var_dump($cost_center));
            $ledgerPago = $this->getWappsiPaymentLedgerId($method, $contabilidadSufijo, $payment['biller_id']);
            $entryPayment['entry_id'] =  $entryId;
            $entryPayment['amount'] = $amount - $amountForPay + $concepto_positivo + $concepto_negativo;
            $entryPayment['dc'] = 'C';
            $entryPayment['narration'] = $pmntMethodDesc[$method];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $ledgerPago;
            if ($cost_center != NULL) {
                $entryPayment['cost_center_id'] = $cost_center;
            }
            $entryPayments[] = $entryPayment;
        }
        /*
            CONTRAPARTIDA DE CONCEPTOS
            CONCEPTO NEGATIVO A CREDITO Y POSITIVO A DEBITO?
        */
        $entryItems = array_merge($entryPayments, $entryDiscounts);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] > 0){

            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function get_expense_by_payment_id($payment_id, $expense_id){
        $q = $this->db->get_where('expenses', ['payment_id' => $payment_id, 'category_id' => $expense_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function wappsiContabilidadGastosDevolucion($purchase_id, $data = array(), $items = array(), $payment = array(), $prev_reference = NULL)
    {

        //HACE FALTA LA REVISIÓN DE LA RECONTABILIZACIÓN DE LAS DEVOLUCIONES DE GASTO
        //HACE FALTA REVISIÓN DE VALORES EN NEGATIVOS, PARA VOLVERLOS POSITIVOS

      if($this->wappsiContabilidadVerificacion($data['date']) > 0){

        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }

        foreach ($payment as $key => $pmnt) {
            if ($pmnt['paid_by'] == 'retencion' || $pmnt['paid_by'] == 'due') {
                unset($payment[$key]);
            }
        }

        $expense_edit = isset($data['expense_edit']) ? true : false;
        $settings_con = $this->getSettingsCon();
        $account_parameter_method = $settings_con->account_parameter_method;
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $prefixExpense = $this->getWappsiContabilidadOperationPrefix("expense_prefix");
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $prefixExpense = substr($data['reference_no'], 0,$position2);
        }
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = $this->getWappsiContabilidadOperationPrefix("expense_prefix");
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = ($data['grand_total'] * -1);
        $wappsiEntri['cr_total'] = ($data['grand_total'] * -1);
        $wappsiEntri['notes'] = "Gasto añadido ".$data['reference_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['supplier_id']; //PENDIENTE CONFIRMAR JUAN CARLOS
        $wappsiEntri['consecutive_supplier'] = $data['consecutive_supplier'];
        $wappsiEntri['user_id'] = $data['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($data, ($expense_edit ? true : false), false, $prev_reference)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }

        $entryPayments = array();

        $total_rete = 0;

        if ((isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']))) {
            if ($data['rete_fuente_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_fuente_total'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_fuente_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                $entryPayments[] = $entryPayment;
                $total_rete +=$data['rete_fuente_total'];
            }
            if ($data['rete_iva_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_iva_total'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_iva_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_iva_account'];
                $entryPayments[] = $entryPayment;
                $total_rete +=$data['rete_iva_total'];
            }
            if ($data['rete_ica_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_ica_total'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_ica_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_ica_account'];
                $entryPayments[] = $entryPayment;
                $total_rete +=$data['rete_ica_total'];
            }
            if ($data['rete_other_total'] != 0) {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $data['rete_other_total'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                $entryPayment['base'] = $data['rete_other_base'];
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $data['rete_other_account'];
                $entryPayments[] = $entryPayment;
                $total_rete +=$data['rete_other_total'];
            }
        }

        //HACE FALTA DIFERENCIAR : SI VIENE A CRÉDITO, TOME LA CUENTA ACREEDORA DE LA FORMA DE PAGO A CRÉDITO, SI VIENE POR CAUSACIÓN, LA DE LA CATEGORÍA DE GASTO
        // NOTA : PARA DIFERENCIAR, USAR EL CAMPO "due_payment_method_id" PARA VER SI CON LA FORMA DE PAGO "Credito" QUEDA DATO Y CON ESO DIFERENCIAR
        $entryItemsIva = [];
        $entryExpenses = [];
        foreach ($items as $item) {
            $expense_category = $this->getExpenseCategory($item['product_id']);

            if ($item['tax_rate_id'] > 0 && $expense_category->tax_ledger_id > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal(($item['item_tax'] * -1));
                $entryItemIva['dc'] = 'C';
                $entryItemIva['narration'] = 'Iva 1 sobre gasto '.$item['product_name'];
                $entryItemIva['base'] = $this->sma->formatDecimal(($item['net_unit_cost'] * $item['quantity']) * -1);
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_ledger_id;
                $entryItemsIva[] = $entryItemIva;
            }
            if ($item['tax_rate_2_id'] > 0 && $expense_category->tax_2_ledger_id > 0) {
                $entryItemIva = [];
                $entryItemIva['entry_id'] = $entryId;
                $entryItemIva['tax_rate_id'] = $item['tax_rate_2_id'];
                $entryItemIva['amount'] = $this->sma->formatDecimal(($item['item_tax_2'] * -1));
                $entryItemIva['dc'] = 'C';
                $entryItemIva['narration'] = 'Iva 2 sobre gasto'.$item['product_name'];
                $entryItemIva['base'] = $this->sma->formatDecimal(($item['net_unit_cost'] * $item['quantity']) * -1);
                $entryItemIva['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemIva['ledger_id'] = $expense_category->tax_2_ledger_id;
                $entryItemsIva[] = $entryItemIva;
            }

            if (isset($data['expense_causation'])) {

                $prorrated_retention = $total_rete > 0 ? ($total_rete * (($item['unit_cost'] * $item['quantity']) / $data['grand_total'])) : 0;

                if($expense_category->creditor_ledger_id > 0) {
                    $ledgerGasto = $expense_category->creditor_ledger_id;
                    $this->db->update('purchases', array('credit_ledger_id' => $ledgerGasto), array('id' => $purchase_id));
                    if (isset($data['purchase_item_id'])) {
                        $this->db->update('purchase_items', ['expense_category_creditor_ledger_id' => $ledgerGasto], ['id' => $data['purchase_item_id']]);
                    }
                }
                $entryExpense['entry_id'] =  $entryId;
                $entryExpense['amount'] = (($item['unit_cost'] * $item['quantity']) * -1) - $prorrated_retention;
                $entryExpense['dc'] = 'D';
                $entryExpense['narration'] = 'Gasto '.$item['product_name'].' '.$data['reference_no'];
                $entryExpense['base'] = "";
                $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
                $entryExpense['ledger_id'] = $ledgerGasto;
                $entryExpenses[] = $entryExpense;
            }

            $ledgerGasto = $expense_category->ledger_id;
            $entryExpense['entry_id'] =  $entryId;
            $entryExpense['amount'] = (($item['net_unit_cost'] * $item['quantity']) * -1);
            $entryExpense['dc'] = 'C';
            $entryExpense['narration'] = 'Gasto '.$item['product_name'].' '.$data['reference_no'];
            $entryExpense['base'] = "";
            $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
            $entryExpense['ledger_id'] = $ledgerGasto;
            $entryExpenses[] = $entryExpense;
        }
        if (count($payment) > 0 && isset($payment[0]['paid_by'])) {
            $metodoPago = $payment[0]['paid_by'];
            $ledgerGasto = $this->getWappsiPaymentLedgerId($metodoPago, $contabilidadSufijo, $data['biller_id']);
            $entryExpense['entry_id'] =  $entryId;
            $entryExpense['amount'] = (($data['grand_total']) * -1) - $total_rete;
            $entryExpense['dc'] = 'D';
            $entryExpense['narration'] = 'Gasto '.$data['reference_no'];
            $entryExpense['base'] = "";
            $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
            $entryExpense['ledger_id'] = $ledgerGasto;
            $entryExpenses[] = $entryExpense;
        }

        if (count($payment) == 0 && !isset($data['expense_causation'])) {
            if (isset($data['due_payment_method_id']) && $data['due_payment_method_id']) {
                $pmCredito = $this->getPaymentMethodById($data['due_payment_method_id']);
                $metodoPago = $pmCredito->code;
            } else {
                $billerDataAdd = $this->getAllCompaniesWithState('biller', $data['biller_id']);
                if ($billerDataAdd->default_credit_payment_method) {
                    $metodoPago = $billerDataAdd->default_credit_payment_method;
                } else {
                    $metodoPago = 'Credito';
                }
            }
            $ledgerGasto = $this->getWappsiPaymentLedgerId($metodoPago, $contabilidadSufijo, $data['biller_id']);
            $entryExpense['entry_id'] =  $entryId;
            $entryExpense['amount'] = (($data['grand_total']) * -1) - $total_rete;
            $entryExpense['dc'] = 'D';
            $entryExpense['narration'] = 'Gasto '.$item['product_name'].' '.$data['reference_no'];
            $entryExpense['base'] = "";
            $entryExpense['companies_id'] = $wappsiEntri['companies_id'];
            $entryExpense['ledger_id'] = $ledgerGasto;
            $entryExpenses[] = $entryExpense;
        }

        $entryItems = array_merge($entryExpenses, $entryPayments, $entryItemsIva);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            if (isset($data['cost_center_id'])) {
                $entryItem['cost_center_id'] = $data['cost_center_id'];
            }
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }

        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        // $this->sma->print_arrays($payment);
        return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function biller_company_logo($biller_id){
        $q = $this->db->join('documents_types', 'documents_types.id = biller_documents_types.document_type_id')
                      ->where('biller_documents_types.biller_id', $biller_id)
                      ->where('documents_types.key_log !=', 1)
                      ->get('biller_documents_types');
        if ($q->num_rows() > 0) {
            return false;
        }
        return true;
    }

    public function getCompanies() {
        $q = $this->db->get('companies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->id] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function validate_all_product_unit_movements($punit_id){
        $sl = $this->db->get_where('sale_items', ['product_unit_id' => $punit_id]);
        if ($sl->num_rows() > 0) {
            return false;
        }

        $po = $this->db->get_where('purchase_items', ['product_unit_id' => $punit_id]);
        if ($po->num_rows() > 0) {
            return false;
        }

        $osl = $this->db->get_where('order_sale_items', ['product_unit_id' => $punit_id]);
        if ($osl->num_rows() > 0) {
            return false;
        }

        $qu = $this->db->get_where('quote_items', ['product_unit_id' => $punit_id]);
        if ($qu->num_rows() > 0) {
            return false;
        }

        return true;

    }

    public function get_all_product_hybrid_prices($product_id = null){

        $q = $this->db->query("
                SELECT
                    U.operator as unit_operator,
                    U.operation_value as unit_operation_value,
                    P.id AS id_product,
                    U.id AS unit_id,
                    PG.id AS price_group_id,
                    IF(UP.id IS NULL, PPR.price, UP.valor_unitario) AS valor_unitario,
                    P.tax_method AS product_tax_method,
                    P.consumption_sale_tax AS consumption_sale_tax
                FROM {$this->db->dbprefix('products')} P
                INNER JOIN {$this->db->dbprefix('product_prices')} PPR ON PPR.product_id = P.id
                INNER JOIN {$this->db->dbprefix('price_groups')} PG ON PG.id = PPR.price_group_id
                LEFT JOIN {$this->db->dbprefix('unit_prices')} UP ON UP.price_group_id = PPR.price_group_id AND UP.id_product = PPR.product_id
                LEFT JOIN {$this->db->dbprefix('units')} U ON (IF(PG.price_group_base = 1 AND UP.id IS NULL,  U.id = P.unit, U.id = UP.unit_id))
                ".($product_id ? 'WHERE P.id = '.$product_id : '' ));
        $hybrid_prices = [];
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $consumption_sale_tax = $row->consumption_sale_tax;
                if ($row->unit_operator && $this->Settings->precios_por_unidad_presentacion == 2) {
                    if ($row->unit_operator == "*") {
                        $consumption_sale_tax = $consumption_sale_tax * $row->unit_operation_value;
                    } else if ($row->unit_operator == "/") {
                        $consumption_sale_tax = $consumption_sale_tax / $row->unit_operation_value;
                    }
                }
                $hybrid_prices[$row->id_product][$row->unit_id][$row->price_group_id] = $row->valor_unitario + ($row->product_tax_method == 0 && $this->Settings->ipoconsumo ? $consumption_sale_tax : 0);
            }
            // $this->sma->print_arrays($hybrid_prices);
            return $hybrid_prices;
        }
        return false;
    }

    public function update_product_hybrid_price($pr_id, $prg_id, $unit_id, $price){

        $product = $this->site->getProductByID($pr_id);

        $p = $this->db->get_where('unit_prices', array('id_product' => $pr_id, 'price_group_id' => $prg_id, 'unit_id' => $unit_id));
        $unit = $this->db->get_where('units', array('id' => $unit_id))->row();

        if ($product->tax_method == 0 && $this->Settings->ipoconsumo) {
            $consumption_sale_tax = $product->consumption_sale_tax;
            if ($unit->operator && $this->Settings->precios_por_unidad_presentacion == 2) {
                if ($unit->operator == "*") {
                    $consumption_sale_tax = $consumption_sale_tax * $unit->operation_value;
                } else if ($unit->operator == "/") {
                    $consumption_sale_tax = $consumption_sale_tax / $unit->operation_value;
                }
            }
            $price = $price - $consumption_sale_tax;

            if ($price <= 0) {
                return false;
            }
        }

        $pr_gr = $this->getPriceGroupByID($prg_id);

        if ($p->num_rows() > 0) {
            if ($this->db->update('unit_prices', array('valor_unitario' => $price, 'last_update'=>date('Y-m-d H:i:s')), array('id_product' => $pr_id, 'price_group_id' => $prg_id, 'unit_id' => $unit_id))) {
                if ($pr_gr->price_group_base == 1) {
                    $this->db->update('products', array('price' => $price), array('id' => $pr_id));
                }
            }
        } else {
            $unit_data = $this->site->getUnitByID($unit_id);
            $cantidad = $unit_data->operation_value ? $unit_data->operation_value : 1;

            if ($this->db->insert('unit_prices', array('id_product' => $pr_id, 'price_group_id' => $prg_id, 'unit_id' => $unit_id, 'valor_unitario' => $price, 'cantidad' => $cantidad))) {

                if ($pr_gr->price_group_base == 1) {
                    $this->db->update('products', array('price' => $price, 'last_update'=>date('Y-m-d H:i:s')), array('id' => $pr_id));
                }

            }
        }

        $q = $this->db->get_where('product_prices', ['product_id' => $pr_id, 'price_group_id'=>$prg_id]);

        if ($q->num_rows() > 0) {
            $this->db->update('product_prices', ['price'=>$price], ['product_id' => $pr_id, 'price_group_id'=>$prg_id]);
        } else {
            $this->db->insert('product_prices', ['price'=>$price, 'product_id' => $pr_id, 'price_group_id'=>$prg_id]);
        }
        return true;
    }

    public function text_delete_payment($payment){
        $txt = '';
        if ($payment->purchase_id) {
            $purchase = $this->getPurchaseByID($payment->purchase_id);
            $txt .= 'Pago eliminado que pertenecía a la compra '.$purchase->reference_no.' y estaba por un valor de '.$this->sma->formatMoney($payment->amount);
        } else if ($payment->sale_id) {
            $sale = $this->getSaleByID($payment->sale_id);
            $txt .= 'Pago '.($payment->paid_by == 'discount' ? '(Descuento)' : '').' eliminado que pertenecía a la venta '.$sale->reference_no.' y estaba por un valor de '.$this->sma->formatMoney($payment->amount);
        }


        if ($payment->rete_fuente_total > 0) {
            $txt.=", éste pago tenia retención a la fuente con un total de : ".$this->sma->formatMoney($payment->rete_fuente_total)." con porcentaje del ".$this->sma->formatMoney($payment->rete_fuente_percentage)."% y con base ".$this->sma->formatMoney($payment->rete_fuente_base);
        }

        if ($payment->rete_iva_total > 0) {
            $txt.=", éste pago tenia retención al iva con un total de : ".$this->sma->formatMoney($payment->rete_iva_total)." con porcentaje del ".$this->sma->formatMoney($payment->rete_iva_percentage)."% y con base ".$this->sma->formatMoney($payment->rete_iva_base);
        }

        if ($payment->rete_ica_total > 0) {
            $txt.=", éste pago tenia retención al ica con un total de : ".$this->sma->formatMoney($payment->rete_ica_total)." con porcentaje del ".$this->sma->formatMoney($payment->rete_ica_percentage)."% y con base ".$this->sma->formatMoney($payment->rete_ica_base);
        }

        if ($payment->rete_other_total > 0) {
            $txt.=", éste pago tenia otras retenciones con un total de : ".$this->sma->formatMoney($payment->rete_other_total)." con porcentaje del ".$this->sma->formatMoney($payment->rete_other_percentage)."% y con base ".$this->sma->formatMoney($payment->rete_other_base);
        }

        //HACE FALTA LOS DESCUENTOS Y ELIMINAR VALORES

        return $txt;

    }

    public function cancel_accounting_entry($reference){
        if ($this->wappsiContabilidadVerificacion() > 0) {
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $ref = explode('-', $reference);
            $entryTypeId = $this->getWappsiIdEntryType($ref[0], $contabilidadSufijo);
            $this->db->where('entrytype_id', $entryTypeId)
                     ->where('number', $ref[1]);
            $exists = $this->db->get('entries_con'.$contabilidadSufijo, 1);
            if ($exists->num_rows() > 0) {
                $entry = $exists->row();
                $this->db->update('entries_con'.$contabilidadSufijo, array(
                                                                            'state' => 0
                                                                        ), array('id' => $entry->id));
            }
        }
        return FALSE;
    }

    public function get_country_by_id($id){
        $q = $this->db->get_where('countries', ['CODIGO'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_state_by_id($id){
        $q = $this->db->get_where('states', ['CODDEPARTAMENTO'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_city_by_id($id){
        $q = $this->db->get_where('cities', ['CODIGO'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_zone_by_id($id){
        $q = $this->db->get_where('zones', ['zone_code'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_subzone_by_id($id){
        $q = $this->db->get_where('subzones', ['subzone_code'=>$id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_ubication_for_cost_shipping() {

        $q = $this->db->order_by('subzone_name', 'asc')->get("subzones");

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;

        } else { //NO HAY SUBZONAS

            $q = $this->db->order_by('zone_name', 'asc')->get("zones");

            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;

            } else { // NO HAY ZONAS

                $q = $this->db->order_by('DESCRIPCION', 'asc')->get("cities");

                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $data[] = $row;
                    }
                    return $data;
                }

            }

        }
        return FALSE;
    }

    public function get_billers_for_shipping_cost($page = 0, $paginate = false, $view_biller = false) {
        if ($paginate != false) {
            if ($page > 0) {
                $pg_inicio = 7*($page);
            } else {
                $pg_inicio = $page;
            }

            if ($view_biller) {
                $this->db->where('companies.id', $view_biller);
            }

            $q = $this->db->join('biller_data', 'biller_data.biller_id = companies.id')
                            ->where('group_name', 'biller')
                            ->where('biller_data.charge_shipping_cost', '1')
                            ->order_by('companies.id', 'asc')
                            ->get("companies", 7, $pg_inicio);
        } else {

            if ($view_biller) {
                $this->db->where('id', $view_biller);
            }

            $q = $this->db->join('biller_data', 'biller_data.biller_id = companies.id')
                            ->where('group_name', 'biller')
                            ->where('biller_data.charge_shipping_cost', '1')
                            ->order_by('companies.id', 'asc')
                            ->get("companies");
        }

        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function get_all_ubication_shipping_costs() {
        $q = $this->db->get('ubication_shipping_cost');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                if ($row->codigo_zona) {
                    if ($row->codigo_subzona) {
                        $data[$row->biller_id][$row->codigo_ciudad][$row->codigo_zona][$row->codigo_subzona] = $row->costo;
                    } else {
                        $data[$row->biller_id][$row->codigo_ciudad][$row->codigo_zona] = $row->costo;
                    }
                } else {
                    $data[$row->biller_id][$row->codigo_ciudad] = $row->costo;
                }
            }
            return $data;
        }
        return FALSE;
    }

    public function wappsiContabilidadVentasDevolucionOtherConcepts($sale_id, $data = array(), $item = array(), $payments = array(), $from_edit = false)
    {
        // exit(var_dump($data));
        if($this->wappsiContabilidadVerificacion($data['date']) > 0){

            if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
                return false;
            }

            $settings_con = $this->getSettingsCon();
            $account_parameter_method = $settings_con->account_parameter_method;
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $wappsiEntri = array();
            $wappsiEntri['tag_id'] = null;
            $wappsiEntri['entrytype_id'] = 0;
            $label = '';
            $number = 0;
            $position2 = strpos($data['reference_no'], '-');
            if($position2 !== false){
                $label = substr($data['reference_no'], 0,$position2);
            } else {
                $label = $this->getWappsiContabilidadOperationPrefix("return_prefix");
            }
            $number = $this->cleanContaReference($data['reference_no']);
            $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
            $wappsiEntri['number'] = $number;
            $wappsiEntri['date'] = substr($data['date'], 0, 10);
            $wappsiEntri['dr_total'] = $data['grand_total'];
            $wappsiEntri['cr_total'] = $data['grand_total'];
            $wappsiEntri['notes'] = "Devolucion afecta Factura ".$data['return_sale_ref'];
            $wappsiEntri['state'] = 1;
            $wappsiEntri['companies_id'] = $data['customer_id'];
            $wappsiEntri['user_id'] = $data['created_by'];
            $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
            if ($entryId = $this->site->getEntryTypeNumberExisting($data, true, true, null, $from_edit)) {
                $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
            } else {
                $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            }
            $entryItemDevolucion = array();
            $entryItemIva = array();
            $entryItemDescuento = array();
            $entryItemEnvio = array();
            $amountForPay = 0;

            $tax_details = $this->site->getTaxRateByID($item['tax_rate_id'], $contabilidadSufijo);
            $credit_concept = $this->get_debit_credit_note_concept_by_id($item['product_code']);
            if ($tax_details->rate >  0) {
                /* Tratamiento Array de Devolucion */
                $entryItemDevolucion[$item['tax_rate_id']]['entry_id'] = $entryId;
                $entryItemDevolucion[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['amount'] = $this->sma->formatDecimal($item['net_unit_price']) * -$item['quantity'];
                $entryItemDevolucion[$item['tax_rate_id']]['dc'] = "D";
                $entryItemDevolucion[$item['tax_rate_id']]['narration'] = "Dev. Otros conceptos Base ".$tax_details->name." ";
                $entryItemDevolucion[$item['tax_rate_id']]['base'] = "";
                $entryItemDevolucion[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_base_ledger_account;
                /* Termina tratamiento Array de Devolucion */

                /* Tratamiento Array de Iva */
                if ($item['item_tax'] != 0) {
                    $entryItemIva[$item['tax_rate_id']]['entry_id'] = $entryId;
                    $entryItemIva[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                    $entryItemIva[$item['tax_rate_id']]['amount'] = ($this->sma->formatDecimal($item['item_tax']) * -1);
                    $entryItemIva[$item['tax_rate_id']]['dc'] = "D";
                    $entryItemIva[$item['tax_rate_id']]['narration'] = "Dev. Otros conceptos Monto ".$tax_details->name." ";
                    $entryItemIva[$item['tax_rate_id']]['base'] = $this->sma->formatDecimal($item['net_unit_price']) * ($item['quantity'] * -1);
                    $entryItemIva[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                    $entryItemIva[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_amount_ledger_account;
                }
                /* Termina tratamiento Array de Iva */
            } else {
                /* Tratamiento Array de Devolucion */
                $entryItemDevolucion[$item['tax_rate_id']]['entry_id'] = $entryId;
                $entryItemDevolucion[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['amount'] = $this->sma->formatDecimal($item['net_unit_price']) * -$item['quantity'];
                $entryItemDevolucion[$item['tax_rate_id']]['dc'] = "D";
                $entryItemDevolucion[$item['tax_rate_id']]['narration'] = "Dev. Otros conceptos Base ".$tax_details->name." ";
                $entryItemDevolucion[$item['tax_rate_id']]['base'] = "";
                $entryItemDevolucion[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_base_ledger_account;
                /* Termina tratamiento Array de Devolucion */
            }
            /* TRABAJANDO CON LOS PAGOS */
            // Si la longitud del array de payments es 0 significa que la venta se hizo a credito y no se ha recibido ningun pago

            $entryPayments = array();
            $entryPayment = array();
            $totalPayments = 0;
            $saldoPorPagar = 0;
            // $this->sma->print_arrays($payments);
            if(count($payments) > 0){
                if(!isset($payments[0])){
                    $aux = $payments;
                    unset($payments);
                    $entryPayments = array();
                    $payments[0] = $aux;
                }
                foreach ($payments as $payment){
                    if ($payment['paid_by'] == 'retencion') {
                        continue;
                    } else if ($payment['paid_by'] == 'discount') {
                        $ledgerPago = $payment['discount_ledger_id'];
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = $payment['note'];
                    } else {
                        $paymentMethodCon = $payment['paid_by'] == 'due' ? 'Credito' : $payment['paid_by'];
                        $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = 'Factura de venta '.$data['reference_no'];
                    }
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['dc'] = 'C';
                    $entryPayment['base'] = "";
                    $entryPayment['ledger_id'] = $ledgerPago;
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayments[] = $entryPayment;
                    $totalPayments = $totalPayments + ($payment['amount'] * -1);
                }
            }

            //TRATAMIENTO RETENCIONES SOBRE VENTAS

            $totalRete = 0;
            if ((isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']))) {
                if ($data['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_fuente_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_fuente_total'];
                }

                if ($data['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_iva_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_iva_total'];
                }

                if ($data['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_ica_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_ica_total'];
                }

                if ($data['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_other_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_other_total'];
                }

            }
            /* Si la suma de los pagos es menor que el monto a pagar se debe hacer un registro como credito para el tema de la deuda. */
            if( $data['payment_status'] == 'pending' || $data['payment_status'] == 'due' || (isset($payments[0]) && $data['payment_status'] == 'paid' && $payments[0] == false)){
                $paymentMethodCon = 'Credito';
                $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = (($data['grand_total'] + $totalRete) * -1);
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Afecta factura '.$data['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayments[] = $entryPayment;
            }
            /* TRABAJANDO CON LOS DESCUENTOS */
            if(isset($data['order_discount']) && $data['order_discount'] < 0){
              // $this->sma->print_arrays($data);
              $ledgerPago = $this->getWappsiLedgerId(1, 'Descuento', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = ($data['order_discount'] * -1);
              $entryPayment['dc'] = 'C';
              $entryPayment['narration'] = 'Descuento';
              $entryPayment['base'] = "";
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              $entryPayments[] = $entryPayment;
            }
            /* Uniendo todos los array en uno solo para la inserción */
            $entryItems = array_merge($entryItemDevolucion, $entryItemIva, $entryPayments);
            /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items para actualizar en la tabla entries_con los campos dr_total y cr_total */
            $drTotal = 0;
            $crTotal = 0;

            // Insertar los entry items
            foreach ($entryItems as $entryItem){
            if($entryItem['amount'] >= 0){
                if (isset($data['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $data['cost_center_id'];
                }
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                $crTotal = $crTotal + $entryItem['amount'];
                }
            }
            }
            $totals["drTotal"]=$drTotal;
            $totals["crTotal"]=$crTotal;
            //$this->sma->print_arrays($totals);

            // Actualizar los totales
            $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
            return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function get_debit_credit_note_concept_by_id($id){
        $q = $this->db->get_where('debit_credit_notes_concepts', ['id' => $id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function electronic_billing_environment(){
        if ($this->Settings->electronic_billing == 1) {
            $q = $this->db->get_where('documents_types', ['module' => 2, 'factura_electronica' => 1]);
            if ($q->num_rows() > 0) {
                return true;
            }
        }
        return false;
    }

    public function get_multi_module_document_types($modules = []){
        $where_sql = "";
        if (count($modules) > 0) {
            foreach ($modules as $row => $module) {
                $where_sql .="module = ".$module." OR ";
            }
            $where_sql = trim($where_sql, ' OR');
            $this->db->where($where_sql);
        }
        $q = $this->db->get('documents_types');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_payments_affects_deposit_sales($deposit_id){
        $q = $this->db
                    ->select('
                              payments.*,
                              sales.reference_no as sale_reference_no
                              ')
                    ->join('sales', 'sales.id = payments.sale_id', 'left')
                    ->where('payments.affected_deposit_id', $deposit_id)
                    ->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_payments_affects_deposit_purchases($deposit_id){
        $q = $this->db
                    ->select('
                              payments.*,
                              purchases.reference_no as purchase_reference_no
                              ')
                    ->join('purchases', 'purchases.id = payments.purchase_id', 'left')
                    ->where('payments.affected_deposit_id', $deposit_id)
                    ->get('payments');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            // exit(var_dump($data));
            return $data;
        }
        return false;
    }

    ///PAGOS MULTIPLES VENTAS

    public function wappsiContabilidadPagosMultiplesVentasAnulacion($payment = array(), $customer_id = null, $retenciones = array(), $data_taxrate_traslate = array(), $conceptos = array(), $cost_center = NULL)
    {
        // exit(var_dump($payment));
      if($this->wappsiContabilidadVerificacion($payment['date']) > 0){

        if (!$this->validate_doc_type_accounting($payment['document_type_id'])) {
            return false;
        }

        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $prefix = $this->getWappsiContabilidadOperationPrefix("sma_payment_prefix");
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = $this->getWappsiContabilidadOperationPrefix("sma_payment_prefix");
        $position2 = strpos($payment['reference_no'], '-');
        if($position2 !== false){
            $label = substr($payment['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($payment['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($payment['date'], 0, 10);
        $wappsiEntri['dr_total'] = $payment['amount'];
        $wappsiEntri['cr_total'] = $payment['amount'];
        $wappsiEntri['notes'] = (isset($payment['note']) ? "Anulación Pago múltiple ".$payment['reference_no']." Nota : ".$payment['note'] : "");
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $payment['companies_id'];
        $wappsiEntri['user_id'] = $payment['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        // $this->sma->print_arrays($payment);
        $payment['wappsiEntri'] = $wappsiEntri;
        if ($entryId = $this->site->getEntryTypeNumberExisting($payment, true)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryPayments = [];
        $entryDiscounts = [];
        $pmntMethodTotal = [];
        $pmntMethodDesc = [];
        $pmntMethodLedgerId = [];
        foreach ($payment['payments'] as $pmnt) {
            $paymentMethodCon = $pmnt['paid_by'];
            $sale = $this->getWappsiSaleById($pmnt['sale_id']);
            if ($paymentMethodCon != 'discount') {
                if (!isset($pmntMethodTotal[$paymentMethodCon])) {
                    $pmntMethodTotal[$paymentMethodCon] = $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] = 'Afecta a Facturas de Venta '.$sale->reference_no;
                    $pmntMethodLedgerId[$paymentMethodCon] = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
                } else {
                    $pmntMethodTotal[$paymentMethodCon] += $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] .= ', '.$sale->reference_no;

                }
                if (isset($sale->due_payment_method_id) && $sale->due_payment_method_id) {
                    $pmCredito = $this->getPaymentMethodById($sale->due_payment_method_id);
                    $metodoPago = $pmCredito->code;
                } else {
                    $billerDataAdd = $this->getAllCompaniesWithState('biller', $sale->biller_id);
                    if ($billerDataAdd->default_credit_payment_method) {
                        $metodoPago = $billerDataAdd->default_credit_payment_method;
                    } else {
                        $metodoPago = 'Credito';
                    }
                }
                $ledgerPago = $this->getWappsiReceipLedgerId($metodoPago, $contabilidadSufijo, $payment['biller_id']);
                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'];
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Afecta a Facturas de Venta '.$sale->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
            } else {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = '(Descuento) Afecta a Facturas de Venta '.$sale->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $pmnt['discount_ledger_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;

                if (!isset($entryDiscounts[$sale->reference_no])) {

                    if (isset($sale->due_payment_method_id) && $sale->due_payment_method_id) {
                        $pmCredito = $this->getPaymentMethodById($sale->due_payment_method_id);
                        $metodoPago = $pmCredito->code;
                    } else {
                        $billerDataAdd = $this->getAllCompaniesWithState('biller', $data['biller_id']);
                        if ($billerDataAdd->default_credit_payment_method) {
                            $metodoPago = $billerDataAdd->default_credit_payment_method;
                        } else {
                            $metodoPago = 'Credito';
                        }
                    }
                    $ledgerPago = $this->getWappsiReceipLedgerId($metodoPago, $contabilidadSufijo, $payment['biller_id']);
                    $entryDiscount['ledger_id'] = $ledgerPago;
                    $entryDiscount['entry_id'] =  $entryId;
                    $entryDiscount['amount'] = $pmnt['amount'];
                    $entryDiscount['dc'] = 'D';
                    $entryDiscount['narration'] = '(Descuento) Afecta a Facturas de Venta '.$sale->reference_no;
                    $entryDiscount['base'] = "";
                    $entryDiscount['companies_id'] = $wappsiEntri['companies_id'];
                    $entryDiscount['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                    $entryDiscounts[$sale->reference_no] = $entryDiscount;
                } else {
                    $entryDiscounts[$sale->reference_no]['amount'] += $pmnt['amount'];
                }
            }
        }
        $amountForPay = 0;
        if (count($retenciones) > 0) {
            foreach ($retenciones as $sale_id => $retencion) {
                $sale = $this->getWappsiSaleById($sale_id);
                if (isset($retencion['rete_fuente_total']) && $retencion['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_fuente_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Fuente '.$retencion['rete_fuente_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_fuente_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_fuente_total'];
                }
                if (isset($retencion['rete_iva_total']) && $retencion['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_iva_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete IVA '.$retencion['rete_iva_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_iva_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_iva_total'];
                }
                if (isset($retencion['rete_ica_total']) && $retencion['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_ica_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete ICA '.$retencion['rete_ica_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_ica_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_ica_total'];
                }
                if (isset($retencion['rete_bomberil_total']) && $retencion['rete_bomberil_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_bomberil_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Tasa Bomberil '.$retencion['rete_bomberil_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_bomberil_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_bomberil_total'];
                }
                if (isset($retencion['rete_autoaviso_total']) && $retencion['rete_autoaviso_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros '.$retencion['rete_autoaviso_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_autoaviso_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_autoaviso_total'];
                }
                if (isset($retencion['rete_other_total']) && $retencion['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_other_total'];
                    $entryPayment['dc'] = 'C';
                    $entryPayment['narration'] = 'Rete Other '.$retencion['rete_other_percentage'].'% '.$sale->reference_no;
                    $entryPayment['base'] = $retencion['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_other_account'];
                    // $entryPayment['cost_center_id'] = ($sale->cost_center_id != NULL ? $sale->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    $amountForPay+=$retencion['rete_other_total'];
                }
            }
        }

        $concepto_positivo = 0;
        $concepto_negativo = 0;

        foreach ($conceptos as $concepto) {
            $entryPayment['entry_id'] =  $entryId;
            if ($concepto['amount'] < 0) {
                $entryPayment['dc'] = 'C';
                $entryPayment['amount'] = $concepto['amount'] * -1;
                $concepto_negativo += $concepto['amount'];
            } else {
                $entryPayment['dc'] = 'D';
                $entryPayment['amount'] = $concepto['amount'];
                $concepto_positivo += $concepto['amount'];
            }
            $entryPayment['narration'] = $concepto['note'];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $concepto['concept_ledger_id'];
            $entryPayment['cost_center_id'] = $concepto['cost_center_id'];
            $entryPayments[] = $entryPayment;
        }

        foreach ($pmntMethodTotal as $method => $amount) {
            $entryPayment['entry_id'] =  $entryId;
            $entryPayment['amount'] = $amount - $amountForPay + $concepto_positivo + $concepto_negativo;
            $entryPayment['dc'] = 'C';
            $entryPayment['narration'] = $pmntMethodDesc[$method];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $pmntMethodLedgerId[$method];
            if ($cost_center != NULL) {
                $entryPayment['cost_center_id'] = $cost_center;
            }
            $entryPayments[] = $entryPayment;
        }

        $entryItems = array_merge($entryPayments, $entryDiscounts);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] != 0){
            // if ($entryItem['amount'] < 0) {
            //     $entryItem['amount'] = $entryItem['amount'] * -1;
            // }
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function wappsiContabilidadPagosMultiplesComprasAnulacion($payment = array(), $customer_id = null, $retenciones = array(), $data_taxrate_traslate = array(), $conceptos = array(), $cost_center = NULL)
    {
      if($this->wappsiContabilidadVerificacion($payment['date']) > 0){
        if (!$this->validate_doc_type_accounting($payment['document_type_id'])) {
            return false;
        }
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $prefix = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;

        $label = $this->getWappsiContabilidadOperationPrefix("ppayment_prefix");
        $position2 = strpos($payment['reference_no'], '-');
        if($position2 !== false){
            $label = substr($payment['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($payment['reference_no']);

        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($payment['date'], 0, 10);
        $wappsiEntri['dr_total'] = $payment['amount'];
        $wappsiEntri['cr_total'] = $payment['amount'];
        $wappsiEntri['notes'] = "Pago múltiple ".$payment['reference_no'].(isset($payment['note']) ? " Nota : ".$payment['note'] : "");
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $payment['companies_id'];
        $wappsiEntri['user_id'] = $payment['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        $payment['wappsiEntri'] = $wappsiEntri;
        if ($entryId = $this->site->getEntryTypeNumberExisting($payment, true)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }

        $entryPayments = [];
        $entryDiscounts = [];
        $pmntMethodTotal = [];
        $pmntMethodDesc = [];
        $pmntMethodLedgerId = [];
        foreach ($payment['payments'] as $pmnt) {
            $paymentMethodCon = $pmnt['paid_by'];
            $purchase = $this->getWappsipurchaseById($pmnt['purchase_id']);
            if ($paymentMethodCon != 'discount') {
                if (!isset($pmntMethodTotal[$paymentMethodCon])) {
                    $pmntMethodTotal[$paymentMethodCon] = $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] = 'Afecta a Facturas de Compra '.$purchase->reference_no;
                    $pmntMethodLedgerId[$paymentMethodCon] = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $payment['biller_id']);
                } else {
                    $pmntMethodTotal[$paymentMethodCon] += $pmnt['amount'];
                    $pmntMethodDesc[$paymentMethodCon] .= ', '.$purchase->reference_no;
                }
                if (isset($pmnt['expense_payment']) && $pmnt['expense_payment']) {
                    $ledgerPago = $pmnt['expense_payment'];
                } else {
                    $ledgerPago = $this->getWappsiPaymentLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
                }

                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = 'Afecta a Facturas de Venta '.$purchase->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
            } else {
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = $pmnt['amount'];
                $entryPayment['dc'] = 'C';
                $entryPayment['narration'] = '(Descuento) Afecta a Facturas de Compra '.$purchase->reference_no;
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $pmnt['discount_ledger_id'];
                $entryPayment['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                $entryPayments[] = $entryPayment;
                if (!isset($entryDiscounts[$purchase->reference_no])) {
                    $ledgerPago = $this->getWappsiPaymentLedgerId("Credito", $contabilidadSufijo, $payment['biller_id']);
                    $entryDiscount['ledger_id'] = $ledgerPago;
                    $entryDiscount['entry_id'] =  $entryId;
                    $entryDiscount['amount'] = $pmnt['amount'];
                    $entryDiscount['dc'] = 'D';
                    $entryDiscount['narration'] = '(Descuento) Afecta a Facturas de Compra '.$purchase->reference_no;
                    $entryDiscount['base'] = "";
                    $entryDiscount['companies_id'] = $wappsiEntri['companies_id'];
                    $entryDiscount['cost_center_id'] = isset($pmnt['cost_center_id']) ? $pmnt['cost_center_id'] : NULL;
                    $entryDiscounts[$purchase->reference_no] = $entryDiscount;
                } else {
                    $entryDiscounts[$purchase->reference_no]['amount'] += $pmnt['amount'];
                }
            }
        }

        $amountForPay = 0;

        // exit(json_encode($retenciones));

        if (count($retenciones) > 0) {
            foreach ($retenciones as $purchase_id => $retencion) {
                $purchase = $this->getWappsipurchaseById($purchase_id);

                if (isset($retencion['rete_fuente_total']) && $retencion['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_fuente_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente '.($retencion['rete_fuente_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_fuente_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_fuente_account'];
                    // $entryPayment['cost_center_id'] = ($purchase->cost_center_id != NULL ? $purchase->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_fuente_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_fuente_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete Fuente Asumida '.$retencion['rete_fuente_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_fuente_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_fuente_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_fuente_total'];
                    }
                }

                if (isset($retencion['rete_iva_total']) && $retencion['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_iva_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete iva '.($retencion['rete_iva_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_iva_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_iva_account'];
                    // $entryPayment['cost_center_id'] = ($purchase->cost_center_id != NULL ? $purchase->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_iva_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_iva_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete iva Asumida '.$retencion['rete_iva_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_iva_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_iva_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_iva_total'];
                    }
                }

                if (isset($retencion['rete_ica_total']) && $retencion['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_ica_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ica '.($retencion['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_ica_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_ica_account'];
                    // $entryPayment['cost_center_id'] = ($purchase->cost_center_id != NULL ? $purchase->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_ica_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete ica Asumida '.$retencion['rete_ica_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_ica_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_ica_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_ica_total'];
                    }
                }


                if (isset($retencion['rete_bomberil_total']) && $retencion['rete_bomberil_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_bomberil_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa Bomberil '.($retencion['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_bomberil_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_bomberil_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_bomberil_account'];
                    // $entryPayment['cost_center_id'] = ($purchase->cost_center_id != NULL ? $purchase->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_bomberil_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Tasa Bomberil Asumida '.$retencion['rete_bomberil_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_bomberil_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_bomberil_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_bomberil_total'];
                    }
                }

                if (isset($retencion['rete_autoaviso_total']) && $retencion['rete_autoaviso_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_autoaviso_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros '.($retencion['rete_ica_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_autoaviso_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_autoaviso_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_autoaviso_account'];
                    // $entryPayment['cost_center_id'] = ($purchase->cost_center_id != NULL ? $purchase->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_ica_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_autoaviso_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Tasa Auto Avisos y Tableros Asumida '.$retencion['rete_autoaviso_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_autoaviso_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_autoaviso_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_autoaviso_total'];
                    }
                }

                if (isset($retencion['rete_other_total']) && $retencion['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $retencion['rete_other_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete other '.($retencion['rete_other_assumed'] == 1 ? 'Asumida' : '').' '.$retencion['rete_other_percentage'].'% '.$purchase->reference_no;
                    $entryPayment['base'] = $retencion['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $retencion['rete_other_account'];
                    // $entryPayment['cost_center_id'] = ($purchase->cost_center_id != NULL ? $purchase->cost_center_id : $this->Settings->default_cost_center);
                    $entryPayment['cost_center_id'] = $cost_center;
                    $entryPayments[] = $entryPayment;
                    if ($retencion['rete_other_assumed'] == 1) {
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['amount'] = $retencion['rete_other_total'];
                        $entryPayment['dc'] = 'C';
                        $entryPayment['narration'] = 'Rete other Asumida '.$retencion['rete_other_percentage'].'% '.$purchase->reference_no;
                        $entryPayment['base'] = $retencion['rete_other_base'];
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayment['ledger_id'] = $retencion['rete_other_assumed_account'];
                        $entryPayment['cost_center_id'] = $cost_center;
                        $entryPayments[] = $entryPayment;
                    } else {
                        $amountForPay+=$retencion['rete_other_total'];
                    }
                }
            }
        }

        $concepto_positivo = 0;
        $concepto_negativo = 0;

        foreach ($conceptos as $concepto) {
            $entryPayment['entry_id'] =  $entryId;
            if ($concepto['amount'] < 0) {
                $entryPayment['dc'] = 'D';
                $entryPayment['amount'] = $concepto['amount'] * -1;
                $concepto_negativo += $concepto['amount'];
            } else {
                $entryPayment['dc'] = 'C';
                $entryPayment['amount'] = $concepto['amount'];
                $concepto_positivo += $concepto['amount'];
            }
            $entryPayment['narration'] = $concepto['note'];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $concepto['ledger_id'];
            $entryPayment['cost_center_id'] = $concepto['cost_center_id'];
            $entryPayments[] = $entryPayment;
        }

        foreach ($pmntMethodTotal as $method => $amount) {

            //exit(var_dump($cost_center));
            $ledgerPago = $this->getWappsiPaymentLedgerId($method, $contabilidadSufijo, $payment['biller_id']);
            $entryPayment['entry_id'] =  $entryId;
            $entryPayment['amount'] = $amount - $amountForPay + $concepto_positivo + $concepto_negativo;
            $entryPayment['dc'] = 'D';
            $entryPayment['narration'] = $pmntMethodDesc[$method];
            $entryPayment['base'] = "";
            $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
            $entryPayment['ledger_id'] = $ledgerPago;
            if ($cost_center != NULL) {
                $entryPayment['cost_center_id'] = $cost_center;
            }
            $entryPayments[] = $entryPayment;

        }

        /*

            CONTRAPARTIDA DE CONCEPTOS
            CONCEPTO NEGATIVO A CREDITO Y POSITIVO A DEBITO?

        */

        $entryItems = array_merge($entryPayments, $entryDiscounts);
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){

            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function sync_company_deposit_amount($company_id){
        $q = $this->db->get_where('deposits', ['company_id'=>$company_id]);
        if ($q->num_rows() > 0) {
            $total_deposit_amount = 0;
            foreach (($q->result()) as $deposit) {
                $total_deposit_amount += $deposit->balance;
            }
            $this->db->update('companies', ['deposit_amount'=>$total_deposit_amount], ['id'=>$company_id]);
        }
    }

    public function get_all_users(){
        $q = $this->db->get('users');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_all_dashboards(){
        $q = $this->db->get_where('dashboards', ['status' => 1]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_dashboard_by_id($id){
        $q = $this->db->get_where('dashboards', ['status' => 1, 'id' => $id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function update_has_multiple_units($product_id){
        $q = $this->db->get_where('unit_prices', ['id_product' => $product_id]);
        if ($q->num_rows() > 0) {
            $this->db->update('products', ['has_multiple_units' => 1, 'last_update' => date('Y-m-d H:i:s')], ['id' => $product_id]);
        }
    }

    public function validate_supplier_reference($reference_no, $supplier_id){
        $q = $this->db->get_where('purchases', ['reference_no' => $reference_no, 'supplier_id' => $supplier_id]);
        if ($q->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function validate_payment_method_active($code){
        $q = $this->db->get_where('payment_methods', ['code' => $code]);
        $data = ['purchase' => false, 'sale' => false];
        if ($q->num_rows() > 0) {
            $q = $q->row();
            if ($q->state_sale == 1) {
                $data['sale'] = true;
            }
            if ($q->state_purchase == 1) {
                $data['purchase'] = true;
            }
        }
        return $data;
    }

    public function get_custom_fields($type = 1){
        $q = $this->db->get_where('custom_fields', ['module' => $type]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->id]['data'] = $row;
                if ($row->cf_type == 'select' || $row->cf_type == 'multiple') {
                    $r = $this->db->get_where('custom_field_values', ['cf_id' => $row->id]);
                    if ($r->num_rows() > 0) {
                        foreach (($r->result()) as $row2) {
                            $data[$row->id]['values'][] = $row2;
                        }
                    }
                }
            }
            return $data;
        }
        return false;
    }

    public function get_tax_exempt(){
        $q = $this->db->get_where('tax_rates', ['rate' => 0, 'type' => 1]);
        if ($q->num_rows() > 0) {
            $tax = $q->row();
            return $tax->id;
        }
    }

    public function get_preparation_areas(){
        $q = $this->db->get('preparation_areas');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function insert_document_type_contabilidad($data) {
        if($this->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'entrytypes_con'.$contabilidadSufijo;
            $et_data = [
                    'label' => $data['sales_prefix'],
                    'name' => $data['nombre'],
                    'description' => $data['nombre'],
                    'numbering' => 1,
                    'prefix' => $data['sales_prefix']."-",
                    'restriction_bankcash' => 1,
                    'origin' => 0,
                    'consecutive' => $data['sales_consecutive']
                ];
            if ($this->db->insert($tabla, $et_data)) {
                return true;
            }
            return false;
        }
        return true;
    }

    public function update_document_type_contabilidad($data, $prev_sales_prefix) {
        if($this->wappsiContabilidadVerificacion() > 0){
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $tabla = 'entrytypes_con'.$contabilidadSufijo;
            $q = $this->db->get_where($tabla, ['label' => $prev_sales_prefix]);
            $et_data = [
                    'label' => $data['sales_prefix'],
                    'name' => $data['nombre'],
                    'description' => $data['nombre'],
                    'numbering' => 1,
                    'prefix' => $data['sales_prefix']."-",
                    'restriction_bankcash' => 1,
                    'origin' => 0,
                ];
            if ($q->num_rows() > 0) {
                $q = $q->row();
                $et_id = $q->id;
                if ($this->db->update($tabla, $et_data, ['id' => $et_id])) {
                    return true;
                }
            } else {
                if ($this->db->insert($tabla, $et_data)) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }

    public function get_restful_service_data(){
        if (!empty($this->Settings->version)) {
            $ch = curl_init();
            $curl_fields = ['current_version' => $this->Settings->version];
            curl_setopt($ch, CURLOPT_URL, $this->Settings->wappsi_rest_url);
            curl_setopt($ch, CURLOPT_HTTPGET, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type"));
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $curl_fields);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $version_data = json_decode(curl_exec($ch));
            // $this->sma->print_arrays($version_data);
            if (isset($version_data->current_version)) {
                curl_close($ch);
                if (isset($version_data->current_version) && $version_data->current_version) {
                    $this->db->update('console_service_data', ['current_version'=> $version_data->current_version, 'update_date'=>date('Y-m-d')], ['id' => 1]);
                } else {
                    $this->db->update('console_service_data', ['current_version'=> $this->Settings->version, 'update_date'=>date('Y-m-d')], ['id' => 1]);
                }
                return $version_data;
            }
        }
        return false;
    }

    public function get_console_service_data(){
        $q = $this->db->get_where('console_service_data', ['update_date' => date('Y-m-d')]);
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return $this->get_restful_service_data();
        }
    }

    public function updateProductsPriceGroupBase(){
        $products = $this->getAllProducts();
        $items = [];
        foreach ($products as $product) {
            $items[] = array(
                            'product_id' => $product->id,
                            'price' => $product->price,
                        );
        }
        $q = $this->db->get_where('price_groups', array('price_group_base' => 1));
        if ($q->num_rows() > 0) {
            $q = $q->row();
            $price_group_base_id = $q->id;
        } else {
            $this->db->insert('price_groups', array(
                                                    'name' => 'PVP Base',
                                                    'price_group_base' => '1',
                                                    )
                            );
            $price_group_base_id = $this->db->insert_id();
        }
        foreach ($items as $item) {
            $datos = array(
                            'product_id' => $item['product_id'],
                            'price_group_id' => $price_group_base_id,
                            'price' => $item['price'],
                        );
            $q = $this->db->get_where('product_prices', array('product_id' => $item['product_id'], 'price_group_id' => $price_group_base_id));
            if ($q->num_rows() == 0) {
                $this->db->insert('product_prices', $datos);
            } else {
                $q = $q->row();
                $datos['last_update'] = date('Y-m-d H:i:s');
                $this->db->update('product_prices', $datos, array('id' => $q->id));
            }

        }
        return true;
    }

    public function syncComboQty($cproduct){
        // $q = $this->db->get_where('warehouses_products', ['product_id' => $cproduct->id]);
        // if ($q->num_rows() > 0) {
        //     $cqty = 0;
        //     foreach (($q->result()) as $row) {
        //         $cqty += $row->quantity;
        //     }
        //     $this->db->update('combo_items', ['quantity' => $cqty],['item_code' => $cproduct->code]);
        // }
    }

    public function wappsiCOntabilidadMovimientosCaja($data){
        if($this->wappsiContabilidadVerificacion($data['date']) > 0){
            if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
                return false;
            }
            $settings_con = $this->getSettingsCon();
            $account_parameter_method = $settings_con->account_parameter_method;
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $wappsiEntri = array();
            $wappsiEntri['tag_id'] = null;
            $wappsiEntri['entrytype_id'] = 0;
            $label = $this->getWappsiContabilidadOperationPrefix("expense_prefix");
            $position2 = strpos($data['reference_no'], '-');
            if($position2 !== false){
                $label = substr($data['reference_no'], 0,$position2);
            }
            $number = $this->cleanContaReference($data['reference_no']);
            $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
            $wappsiEntri['number'] = $number;
            $wappsiEntri['date'] = substr($data['date'], 0, 10);
            $wappsiEntri['dr_total'] = $data['amount'];
            $wappsiEntri['cr_total'] = $data['amount'];
            $wappsiEntri['notes'] = "Movimiento de caja de ".($data['movement_type'] == 1 ? "Entrada" : "Salida").", ".$data['reference_no']." ";
            $wappsiEntri['state'] = 1;
            $wappsiEntri['companies_id'] = $data['biller_id'];
            $wappsiEntri['user_id'] = $data['created_by'];
            $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
            $data['wappsiEntri'] = $wappsiEntri;
            if ($entryId = $this->site->getEntryTypeNumberExisting($data, true)) {
                $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
            } else {
                $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            }
            $ledgerMV = $this->getWappsiPaymentLedgerId($data['origin_paid_by'], $contabilidadSufijo, $data['biller_id']);
            $entryItem['entry_id'] =  $entryId;
            $entryItem['amount'] = $data['amount'];
            $entryItem['dc'] = 'C';
            $entryItem['narration'] = 'Movimiento de caja '.$data['reference_no'];
            $entryItem['base'] = "";
            $entryItem['companies_id'] = $wappsiEntri['companies_id'];
            $entryItem['ledger_id'] = $ledgerMV;
            $entryItems[] = $entryItem;

            $ledgerMV = $this->getWappsiPaymentLedgerId($data['destination_paid_by'], $contabilidadSufijo, $data['biller_id']);
            $entryItem['entry_id'] =  $entryId;
            $entryItem['amount'] = $data['amount'];
            $entryItem['dc'] = 'D';
            $entryItem['narration'] = 'Movimiento de caja '.$data['reference_no'];
            $entryItem['base'] = "";
            $entryItem['companies_id'] = $wappsiEntri['companies_id'];
            $entryItem['ledger_id'] = $ledgerMV;
            $entryItems[] = $entryItem;

            $drTotal = 0;
            $crTotal = 0;

            foreach ($entryItems as $entryItem){
              if($entryItem['amount'] >= 0){
                if (isset($data['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $data['cost_center_id'];
                }
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                  $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                  $crTotal = $crTotal + $entryItem['amount'];
                }
              }
            }
            // Actualizar los totales
            $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
            return true;
        }
        return false;
    }

    public function wappsiContabilidadVentasDebitNotes($sale_id, $data = array(), $item = array(), $payments = array(), $from_edit = false)
    {
        if($this->wappsiContabilidadVerificacion($data['date']) > 0){
            if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
                return false;
            }

            $settings_con = $this->getSettingsCon();
            $account_parameter_method = $settings_con->account_parameter_method;
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $wappsiEntri = array();
            $wappsiEntri['tag_id'] = null;
            $wappsiEntri['entrytype_id'] = 0;
            $label = '';
            $number = 0;
            $position2 = strpos($data['reference_no'], '-');
            if($position2 !== false){
                $label = substr($data['reference_no'], 0,$position2);
            } else {
                $label = $this->getWappsiContabilidadOperationPrefix("return_prefix");
            }
            $number = $this->cleanContaReference($data['reference_no']);
            $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
            $wappsiEntri['number'] = $number;
            $wappsiEntri['date'] = substr($data['date'], 0, 10);
            $wappsiEntri['dr_total'] = $data['grand_total'];
            $wappsiEntri['cr_total'] = $data['grand_total'];
            $wappsiEntri['notes'] = "Nota débito afecta Factura ".$data['reference_debit_note'];
            $wappsiEntri['state'] = 1;
            $wappsiEntri['companies_id'] = $data['customer_id'];
            $wappsiEntri['user_id'] = $data['created_by'];
            $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
            $data['wappsiEntri'] = $wappsiEntri;
            if ($entryId = $this->site->getEntryTypeNumberExisting($data, true, true, null, $from_edit)) {
                $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
            } else {
                $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            }
            $entryItemDevolucion = array();
            $entryItemIva = array();
            $entryItemDescuento = array();
            $entryItemEnvio = array();
            $amountForPay = 0;

            $tax_details = $this->site->getTaxRateByID($item['tax_rate_id'], $contabilidadSufijo);
            $credit_concept = $this->get_debit_credit_note_concept_by_id($item['product_code']);
            if ($tax_details && $tax_details->rate >  0) {
                /* Tratamiento Array de Devolucion */
                $entryItemDevolucion[$item['tax_rate_id']]['entry_id'] = $entryId;
                $entryItemDevolucion[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['amount'] = $this->sma->formatDecimal($item['net_unit_price'] * $item['quantity']);
                $entryItemDevolucion[$item['tax_rate_id']]['dc'] = "C";
                $entryItemDevolucion[$item['tax_rate_id']]['narration'] = "Nota Débito Base ".$tax_details->name." ";
                $entryItemDevolucion[$item['tax_rate_id']]['base'] = "";
                $entryItemDevolucion[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_base_ledger_account;
                /* Termina tratamiento Array de Devolucion */

                /* Tratamiento Array de Iva */
                if ($item['item_tax'] > 0) {
                    $entryItemIva[$item['tax_rate_id']]['entry_id'] = $entryId;
                    $entryItemIva[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                    $entryItemIva[$item['tax_rate_id']]['amount'] = $this->sma->formatDecimal($item['item_tax']);
                    $entryItemIva[$item['tax_rate_id']]['dc'] = "C";
                    $entryItemIva[$item['tax_rate_id']]['narration'] = "Nota Débito Monto ".$tax_details->name." ";
                    $entryItemIva[$item['tax_rate_id']]['base'] = $this->sma->formatDecimal($item['net_unit_price'] * $item['quantity']);
                    $entryItemIva[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                    $entryItemIva[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_amount_ledger_account;
                }
                /* Termina tratamiento Array de Iva */
            } else {
                /* Tratamiento Array de Devolucion */
                $entryItemDevolucion[$item['tax_rate_id']]['entry_id'] = $entryId;
                $entryItemDevolucion[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['amount'] = $this->sma->formatDecimal($item['net_unit_price']* $item['quantity']);
                $entryItemDevolucion[$item['tax_rate_id']]['dc'] = "C";
                $entryItemDevolucion[$item['tax_rate_id']]['narration'] = "Nota Débito Base ".($tax_details ? $tax_details->name : "")." ";
                $entryItemDevolucion[$item['tax_rate_id']]['base'] = "";
                $entryItemDevolucion[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_base_ledger_account;
                /* Termina tratamiento Array de Devolucion */
            }
            /* TRABAJANDO CON LOS PAGOS */
            // Si la longitud del array de payments es 0 significa que la venta se hizo a credito y no se ha recibido ningun pago

            $entryPayments = array();
            $entryPayment = array();
            $totalPayments = 0;
            $saldoPorPagar = 0;
            if(count($payments) > 0){
                if(!isset($payments[0])){
                    $aux = $payments;
                    unset($payments);
                    $entryPayments = array();
                    $payments[0] = $aux;
                }
                foreach ($payments as $payment){
                    if (isset($payment['paid_by'])) {
                        if ($payment['paid_by'] == 'retencion') {
                            continue;
                        } else if ($payment['paid_by'] == 'discount') {
                            $ledgerPago = $payment['discount_ledger_id'];
                            $entryPayment['amount'] =  ($payment['amount']);
                            $entryPayment['narration'] = $payment['note'];
                        } else {
                            $paymentMethodCon = $payment['paid_by'] == 'due' ? 'Credito' : $payment['paid_by'];
                            $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                            $entryPayment['amount'] =  ($payment['amount']);
                            $entryPayment['narration'] = 'Factura de venta '.$data['reference_no'];
                        }
                        $entryPayment['entry_id'] =  $entryId;
                        $entryPayment['dc'] = 'D';
                        $entryPayment['base'] = "";
                        $entryPayment['ledger_id'] = $ledgerPago;
                        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                        $entryPayments[] = $entryPayment;
                        $totalPayments = $totalPayments + ($payment['amount']);
                    }
                }
            }

            //TRATAMIENTO RETENCIONES SOBRE VENTAS

            $totalRete = 0;
            if ((isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']))) {
                if (isset($data['rete_fuente_total']) && $data['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_fuente_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_fuente_total'];
                }

                if (isset($data['rete_iva_total']) && $data['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_iva_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_iva_total'];
                }

                if (isset($data['rete_ica_total']) && $data['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_ica_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_ica_total'];
                }

                if (isset($data['rete_other_total']) && $data['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_other_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_other_total'];
                }

            }
            /* Si la suma de los pagos es menor que el monto a pagar se debe hacer un registro como credito para el tema de la deuda. */
            if( $data['payment_status'] == 'pending' || $data['payment_status'] == 'due' || (isset($payments[0]) && $data['payment_status'] == 'paid' && $payments[0] == false)){
                $paymentMethodCon = 'Credito';
                $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = (($data['grand_total'] + $totalRete));
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Afecta factura '.$data['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayments[] = $entryPayment;
            }
            /* TRABAJANDO CON LOS DESCUENTOS */
            /* Uniendo todos los array en uno solo para la inserción */
            $entryItems = array_merge($entryItemDevolucion, $entryItemIva, $entryPayments);
            /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items para actualizar en la tabla entries_con los campos dr_total y cr_total */
            $drTotal = 0;
            $crTotal = 0;

            // $this->sma->print_arrays($entryItems);

            // Insertar los entry items
            foreach ($entryItems as $entryItem){
                if($entryItem['amount'] >= 0){
                    if (isset($data['cost_center_id'])) {
                        $entryItem['cost_center_id'] = $data['cost_center_id'];
                    }
                    $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                    if( $entryItem['dc'] == 'D' ){
                        $drTotal = $drTotal + $entryItem['amount'];
                    }else if( $entryItem['dc'] == 'C' ){
                        $crTotal = $crTotal + $entryItem['amount'];
                    }
                }
            }
            $totals["drTotal"]=$drTotal;
            $totals["crTotal"]=$crTotal;
            //$this->sma->print_arrays($totals);

            // Actualizar los totales
            $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
            return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function getPaymentAccountingNote($payment){
        if($this->wappsiContabilidadVerificacion() > 0)
        {
            $et_number = $this->cleanContaReference($payment[0]->reference_no);
            $contabilidadSufijo = "_con".$this->session->userdata('accounting_module');
            $q = $this->db->select('entryitems'.$contabilidadSufijo.'.*, ledgers'.$contabilidadSufijo.'.name as ledger_name, ledgers'.$contabilidadSufijo.'.code as ledger_code')
                        ->join('entries'.$contabilidadSufijo, 'entries'.$contabilidadSufijo.'.id = entryitems'.$contabilidadSufijo.'.entry_id')
                        ->join('documents_types', 'documents_types.id = '.$payment[0]->document_type_id)
                        ->join('entrytypes'.$contabilidadSufijo , 'entrytypes'.$contabilidadSufijo.'.label = documents_types.sales_prefix AND entries'.$contabilidadSufijo.'.entrytype_id = entrytypes'.$contabilidadSufijo.'.id')
                        ->join('ledgers'.$contabilidadSufijo, 'ledgers'.$contabilidadSufijo.'.id = entryitems'.$contabilidadSufijo.'.ledger_id', 'left')
                        ->where('entries'.$contabilidadSufijo.'.number', $et_number)
                        ->get('entryitems'.$contabilidadSufijo);
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $data[] = $row;
                }
                return $data;
            }
        }
        return false;
    }

    public function get_ciiu_code_by_id($id)
    {
        $q = $this->db->get_where('ciiu_codes', ['id' => $id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_ciiu_code_by_ids($ids)
    {
        $q = $this->db->where_in('id',$ids)->get('ciiu_codes');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function validate_doc_type_accounting($document_type_id){
        $doc_type = $this->getDocumentTypeById($document_type_id);
        if ($doc_type->not_accounting == 0) {
            return true;
        } else if ($doc_type->not_accounting == 1) {
            return false;
        }
    }

    public function validate_doc_type_accounting_parametrization($document_type_id){
        if($this->wappsiContabilidadVerificacion() > 0)
        {
            $contabilidadSufijo = "_con".$this->session->userdata('accounting_module');
            $q = $this->db->select('*')
                    ->join('entrytypes'.$contabilidadSufijo.' et', 'et.label = documents_types.sales_prefix')
                    ->where('documents_types.id', $document_type_id)
                    ->get('documents_types');
            if ($q->num_rows() > 0) {
                return true;
            }
            return false;
        }
        return true;
    }

    public function invoice_date_existing_in_resolution_date_range($invoice_date, $resolution_data)
    {
        $invoice_date = date("Y-m-d", strtotime($invoice_date));
        if (strtotime($invoice_date) >= strtotime($resolution_data->emision_resolucion) && strtotime($invoice_date) <= strtotime($resolution_data->vencimiento_resolucion)) {
            return TRUE;
        }
        return FALSE;
    }

    public function invoice_date_outside_allowed_period($invoice_date, $document_type_id)
    {
        $current_date = date("y-m-d");
        $invoice_date = date("Y-m-d", strtotime($invoice_date));
        $date_from = date("Y-m-d", strtotime($current_date." - ". $this->Settings->days_before_current_date ." days"));
        $date_to = date("Y-m-d", strtotime($current_date." + ". $this->Settings->days_after_current_date ." days"));
        $document_type = $this->getDocumentTypeById($document_type_id);
        if ($document_type->factura_electronica == 1) {
            if (strtotime($invoice_date) < strtotime($date_from) || strtotime($invoice_date) > strtotime($date_to)) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function get_product_variant_by_id($option_id){
        $q = $this->db->get_where('product_variants', ['id' => $option_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_product_variant_by_code($option_code){
        $q = $this->db->get_where('product_variants', ['code' => $option_code]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_pending_sales_dian($pos = 0)
    {
        $this->db->select("count(*) AS quantity");
        $this->db->join('documents_types', 'documents_types.id = sales.document_type_id', 'left');
        $this->db->where("fe_aceptado !=", 2);
        $this->db->where('documents_types.factura_electronica', YES);
        $this->db->where('pos', $pos);
        $q = $this->db->get("sales");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function validate_movement_date($date){
        $min_date = $this->Settings->system_start_date." 00:00:00";
        $max_date = date('Y')."-12-31 23:59:59";
        $date_arr = explode(" ", $date);
        if (count($date_arr) == 1) {
            $date.=date(' H:i:s');
        }
        if ($this->año_actual_movimientos && $this->Settings->years_database_management == 2) {
            $min_date = $this->año_actual_movimientos."-01-01 00:00:00";
            $max_date = $this->año_actual_movimientos."-12-31 23:59:59";
        }
        if ($date < $min_date || $date > $max_date) {
            $this->session->set_flashdata("error", lang("movement_out_of_date_allowed"));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    public function wappsiContabilidadComprasDevolucionOtherConcepts($purchase_id, $data = array(), $item = array(), $payments = array(), $from_edit = false)
    {
        if($this->wappsiContabilidadVerificacion($data['date']) > 0){
            if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
                return false;
            }
            $settings_con = $this->getSettingsCon();
            $account_parameter_method = $settings_con->account_parameter_method;
            $contabilidadSufijo = $this->session->userdata('accounting_module');
            $wappsiEntri = array();
            $wappsiEntri['tag_id'] = null;
            $wappsiEntri['entrytype_id'] = 0;
            $label = '';
            $number = 0;
            $position2 = strpos($data['reference_no'], '-');
            if($position2 !== false){
                $label = substr($data['reference_no'], 0,$position2);
            } else {
                $label = $this->getWappsiContabilidadOperationPrefix("return_prefix");
            }
            $number = $this->cleanContaReference($data['reference_no']);
            $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
            $wappsiEntri['number'] = $number;
            $wappsiEntri['date'] = substr($data['date'], 0, 10);
            $wappsiEntri['dr_total'] = $data['grand_total'];
            $wappsiEntri['cr_total'] = $data['grand_total'];
            $wappsiEntri['notes'] = "Devolucion afecta Factura ".$data['return_purchase_ref'];
            $wappsiEntri['state'] = 1;
            $wappsiEntri['companies_id'] = $data['supplier_id'];
            $wappsiEntri['user_id'] = $data['created_by'];
            $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
            if ($entryId = $this->site->getEntryTypeNumberExisting($data, true, true, null, $from_edit)) {
                $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
            } else {
                $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
            }
            $entryItemDevolucion = array();
            $entryItemIva = array();
            $entryItemDescuento = array();
            $entryItemEnvio = array();
            $amountForPay = 0;

            $tax_details = $this->site->getTaxRateByID($item['tax_rate_id'], $contabilidadSufijo);
            $credit_concept = $this->get_debit_credit_note_concept_by_id($item['product_code']);
            if ($tax_details && $tax_details->rate >  0) {
                /* Tratamiento Array de Devolucion */
                $entryItemDevolucion[$item['tax_rate_id']]['entry_id'] = $entryId;
                $entryItemDevolucion[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['amount'] = $this->sma->formatDecimal($item['net_unit_cost']) * -$item['quantity'];
                $entryItemDevolucion[$item['tax_rate_id']]['dc'] = "C";
                $entryItemDevolucion[$item['tax_rate_id']]['narration'] = "Dev. Otros conceptos Base ".$tax_details->name." ";
                $entryItemDevolucion[$item['tax_rate_id']]['base'] = "";
                $entryItemDevolucion[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_base_ledger_account;
                /* Termina tratamiento Array de Devolucion */

                /* Tratamiento Array de Iva */
                if ($item['item_tax'] != 0) {
                    $entryItemIva[$item['tax_rate_id']]['entry_id'] = $entryId;
                    $entryItemIva[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                    $entryItemIva[$item['tax_rate_id']]['amount'] = ($this->sma->formatDecimal($item['item_tax']) * -1);
                    $entryItemIva[$item['tax_rate_id']]['dc'] = "C";
                    $entryItemIva[$item['tax_rate_id']]['narration'] = "Dev. Otros conceptos Monto ".$tax_details->name." ";
                    $entryItemIva[$item['tax_rate_id']]['base'] = $this->sma->formatDecimal($item['net_unit_cost']) * ($item['quantity'] * -1);
                    $entryItemIva[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                    $entryItemIva[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_amount_ledger_account;
                }
                /* Termina tratamiento Array de Iva */
            } else {
                /* Tratamiento Array de Devolucion */
                $entryItemDevolucion[$item['tax_rate_id']]['entry_id'] = $entryId;
                $entryItemDevolucion[$item['tax_rate_id']]['tax_rate_id'] = $item['tax_rate_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['amount'] = $this->sma->formatDecimal($item['net_unit_cost']) * -$item['quantity'];
                $entryItemDevolucion[$item['tax_rate_id']]['dc'] = "C";
                $entryItemDevolucion[$item['tax_rate_id']]['narration'] = "Dev. Otros conceptos Base ".$tax_details->name." ";
                $entryItemDevolucion[$item['tax_rate_id']]['base'] = "";
                $entryItemDevolucion[$item['tax_rate_id']]['companies_id'] = $wappsiEntri['companies_id'];
                $entryItemDevolucion[$item['tax_rate_id']]['ledger_id'] = $credit_concept->tax_base_ledger_account;
                /* Termina tratamiento Array de Devolucion */
            }
            /* TRABAJANDO CON LOS PAGOS */
            // Si la longitud del array de payments es 0 significa que la venta se hizo a credito y no se ha recibido ningun pago

            $entryPayments = array();
            $entryPayment = array();
            $totalPayments = 0;
            $saldoPorPagar = 0;
            // $this->sma->print_arrays($payments);
            if(count($payments) > 0){
                if(!isset($payments[0])){
                    $aux = $payments;
                    unset($payments);
                    $entryPayments = array();
                    $payments[0] = $aux;
                }
                foreach ($payments as $payment){
                    if ($payment['paid_by'] == 'retencion') {
                        continue;
                    } else if ($payment['paid_by'] == 'discount') {
                        $ledgerPago = $payment['discount_ledger_id'];
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = $payment['note'];
                    } else {
                        $paymentMethodCon = $payment['paid_by'] == 'due' ? 'Credito' : $payment['paid_by'];
                        $ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                        $entryPayment['amount'] =  ($payment['amount'] * -1);
                        $entryPayment['narration'] = 'Factura de compra '.$data['reference_no'];
                    }
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['dc'] = 'D';
                    $entryPayment['base'] = "";
                    $entryPayment['ledger_id'] = $ledgerPago;
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayments[] = $entryPayment;
                    $totalPayments = $totalPayments + ($payment['amount'] * -1);
                }
            }

            //TRATAMIENTO RETENCIONES SOBRE VENTAS

            $totalRete = 0;
            if ((isset($data['rete_fuente_total']) || isset($data['rete_iva_total']) || isset($data['rete_ica_total']) || isset($data['rete_other_total']))) {
                if ($data['rete_fuente_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_fuente_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Fuente '.$data['rete_fuente_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_fuente_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_fuente_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_fuente_total'];
                }

                if ($data['rete_iva_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_iva_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete IVA '.$data['rete_iva_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_iva_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_iva_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_iva_total'];
                }

                if ($data['rete_ica_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_ica_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete ICA '.$data['rete_ica_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_ica_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_ica_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_ica_total'];
                }

                if ($data['rete_other_total'] != 0) {
                    $entryPayment['entry_id'] =  $entryId;
                    $entryPayment['amount'] = $data['rete_other_total'];
                    $entryPayment['dc'] = 'D';
                    $entryPayment['narration'] = 'Rete Other '.$data['rete_other_percentage'].'% '.$data['reference_no'];
                    $entryPayment['base'] = $data['rete_other_base'];
                    $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                    $entryPayment['ledger_id'] = $data['rete_other_account'];
                    $entryPayments[] = $entryPayment;
                    $totalRete += $data['rete_other_total'];
                }

            }
            /* Si la suma de los pagos es menor que el monto a pagar se debe hacer un registro como credito para el tema de la deuda. */
            if( $data['payment_status'] == 'pending' || $data['payment_status'] == 'due' || (!isset($payments[0]) && $data['payment_status'] == 'paid')){
                $paymentMethodCon = 'Credito';
                $ledgerPago = $this->getWappsiPaymentLedgerId($paymentMethodCon, $contabilidadSufijo, $data['biller_id']);
                $entryPayment['entry_id'] =  $entryId;
                $entryPayment['amount'] = (($data['grand_total'] + $totalRete) * -1);
                $entryPayment['dc'] = 'D';
                $entryPayment['narration'] = 'Afecta factura '.$data['reference_no'];
                $entryPayment['base'] = "";
                $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
                $entryPayment['ledger_id'] = $ledgerPago;
                $entryPayments[] = $entryPayment;
            }
            /* TRABAJANDO CON LOS DESCUENTOS */
            if(isset($data['order_discount']) && $data['order_discount'] < 0){
              // $this->sma->print_arrays($data);
              $ledgerPago = $this->getWappsiLedgerId(1, 'Descuento', 0, $contabilidadSufijo, $account_parameter_method);
              $entryPayment['entry_id'] =  $entryId;
              $entryPayment['amount'] = ($data['order_discount'] * -1);
              $entryPayment['dc'] = 'D';
              $entryPayment['narration'] = 'Descuento';
              $entryPayment['base'] = "";
              $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
              $entryPayment['ledger_id'] = $ledgerPago;
              $entryPayments[] = $entryPayment;
            }
            /* Uniendo todos los array en uno solo para la inserción */
            $entryItems = array_merge($entryItemDevolucion, $entryItemIva, $entryPayments);
            /* Se va a realizar la suma de los debitos y los creditos mientras se recorre el array para la inserción de los items para actualizar en la tabla entries_con los campos dr_total y cr_total */
            $drTotal = 0;
            $crTotal = 0;

            // Insertar los entry items
            foreach ($entryItems as $entryItem){
            if($entryItem['amount'] >= 0){
                if (isset($data['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $data['cost_center_id'];
                }
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                $crTotal = $crTotal + $entryItem['amount'];
                }
            }
            }
            $totals["drTotal"]=$drTotal;
            $totals["crTotal"]=$crTotal;
            //$this->sma->print_arrays($totals);

            // Actualizar los totales
            $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
            return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }

    public function wappsiContabilidadTraslados($data = array(), $items = array())
    {
      if($this->wappsiContabilidadVerificacion($data['date']) > 0)
      {
        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = $data['grand_total'];
        $wappsiEntri['cr_total'] = $data['grand_total'];
        $wappsiEntri['notes'] = "Traslado de bodega ".$data['reference_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['companies_id'];
        $wappsiEntri['user_id'] = $data['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($data, true, false)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $drTotal = 0;
        $crTotal = 0;
        $movement = $this->getWappsiMovementTypes('transfer', $contabilidadSufijo, $data['document_type_id']);
        $entryItems = [];
        $cnt = 0;
        foreach ($items as $row => $item) {
            $entryItems[$cnt]['entry_id'] = $entryId;
            $entryItems[$cnt]['ledger_id'] = isset($movement->ledger_id) ? $movement->ledger_id : NULL;
            $entryItems[$cnt]['amount'] = (Double) $item['net_unit_cost'] * (Double) $item['quantity'];
            $entryItems[$cnt]['dc'] = "C";
            $entryItems[$cnt]['narration'] = 'Traslado '.($item['product_name'])." (".$item['quantity'].")";
            $entryItems[$cnt]['companies_id'] = $data['companies_id'];
            $entryItems[$cnt]['base'] = 0;
            $entryItems[$cnt]['cost_center_id'] = $data['cost_center_id'];


            $entryItems[($cnt+1)]['entry_id'] = $entryId;
            $entryItems[($cnt+1)]['ledger_id'] = isset($movement->ledger_id) ? $movement->ledger_id : NULL;
            $entryItems[($cnt+1)]['amount'] = (Double) $item['net_unit_cost'] * (Double) $item['quantity'];
            $entryItems[($cnt+1)]['dc'] = "D";
            $entryItems[($cnt+1)]['narration'] = 'Traslado '.($item['product_name'])." (".$item['quantity'].")";
            $entryItems[($cnt+1)]['companies_id'] = $data['companies_id'];
            $entryItems[($cnt+1)]['base'] = 0;
            $entryItems[($cnt+1)]['cost_center_id'] = $data['destination_cost_center_id'];
            $cnt += 2;

        }
        foreach ($entryItems as $entryItem){
            $entryItem['entry_id'] = $entryId;
            if(isset($entryItem['amount'])){
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                  $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                  $crTotal = $crTotal + $entryItem['amount'];
                }
            }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function get_product_biller_price_group($product_id, $biller_id){
        $biller = $this->getAllCompaniesWithState('biller', $biller_id);
        if ($biller && $biller->default_price_group) {
            $q = $this->db->select('price')
                    ->where('price_group_id', $biller->default_price_group)
                    ->where('product_id', $product_id)
                    ->get('product_prices');
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }
        return false;
    }

    public function updateAVCO($data, $edit = false)
    {
        $data['unit_cost'] = !isset($data['unit_cost']) ? $data['cost'] : $data['unit_cost'];
        $product = $this->getProductByID($data['product_id']);
        $tax = $this->site->getTaxRateByID($product->tax_rate);
        $shipping_total = 0;
        $shipping_unit = 0;
        if (isset($data['shipping_unit_cost']) && $data['shipping_unit_cost'] != 0) {
            $data['unit_cost'] = $data['unit_cost'] - $data['shipping_unit_cost'];
            $shipping_total = $data['quantity'] * ($data['shipping_unit_cost'] > 0 ? $data['shipping_unit_cost'] : ($data['shipping_unit_cost'] * -1));
            $shipping_total_tax = $this->site->calculateTax(NULL, $tax, $shipping_total, NULL, 1);
            $shipping_total += $shipping_total_tax['amount'];
            $shipping_unit = $shipping_total / $data['quantity'];
        }
        if (isset($data['original_tax_rate_id']) && $data['original_tax_rate_id'] > 0) {
            $base_unit_cost_tax = $this->site->calculateTax(NULL, $tax, $data['unit_cost'], NULL, 1);
            $data['unit_cost'] += $base_unit_cost_tax['amount'];
        }
        $import_adjustment_cost = 0;
        $import_adjustment_unit_cost = 0;
        if (isset($data['import_adjustment_cost']) && $data['import_adjustment_cost'] != 0) {
            $import_adjustment_cost = $data['quantity'] * ($data['import_adjustment_cost'] > 0 ? $data['import_adjustment_cost'] : ($data['import_adjustment_cost'] * -1));
            $import_adjustment_cost_tax = $this->site->calculateTax(NULL, $tax, $import_adjustment_cost, NULL, 1);
            $import_adjustment_cost += $import_adjustment_cost_tax['amount'];
            $import_adjustment_unit_cost = $import_adjustment_cost / $data['quantity'];
        }

        $item_tax_2 = 0;
        $item_tax_2_unit = 0;
        if (isset($data['item_tax_2']) && $data['item_tax_2'] != 0) {
            // $item_tax_2 = ($data['item_tax_2'] > 0 ? $data['item_tax_2'] : ($data['item_tax_2'] * -1));
            $item_tax_2 = $data['item_tax_2'];
        }

        if (!isset($data['sale_return']) && (!isset($data['item_tax_2']) || $data['item_tax_2'] == 0)) {
            if (strpos(($product->purchase_tax_rate_2_percentage ? $product->purchase_tax_rate_2_percentage : ""), "%") !== false) {
                $item_tax_2 = $this->sma->calculate_second_tax($product->purchase_tax_rate_2_percentage, $data['net_unit_cost'], false, 0);
                $item_tax_2 = ($item_tax_2[1]) * $data['quantity'];
            } else if ($product->consumption_purchase_tax > 0) {
                $item_tax_2 = $product->consumption_purchase_tax * $data['quantity'];
            }
        }

        if ($item_tax_2 != 0) {
            $item_tax_2_tax = $this->site->calculateTax(NULL, $tax, $item_tax_2, NULL, 1);
            $item_tax_2 += $item_tax_2_tax['amount'];
            $item_tax_2_unit = $item_tax_2 / $data['quantity'];
        }
        $wp_details = $this->getWarehousesProductQuantity($data['product_id'], $data['warehouse_id']);
        if (!$wp_details) {
            $wp_details = $this->getWarehousesProductQuantity($data['product_id']);
        }
        if ($wp_details) {
            if ($edit) {
                $pitems = $this->db->where('product_id', $data['product_id'])
                                   ->where('purchase_id IS NOT NULL')
                                   ->get('purchase_items');
                if ($pitems->num_rows() > 0) {
                    $total_cost = 0;
                    $total_quantity = 0;
                    foreach (($pitems->result()) as $pitem) {
                        $total_cost += ($pitem->quantity_balance * $pitem->net_unit_cost);
                        $total_quantity += $pitem->quantity_balance;
                    }
                } else {
                    $total_cost = $data['quantity'] * $data['unit_cost'];
                    $total_quantity = $data['quantity'];
                }
            } else {
                if ($product->tax_method == 1) { //aca
                    // $wp_details_cost = $this->site->calculateTax($product, $tax, $wp_details->avg_cost, null, 0);
                    // $wp_details_cost['monto'] = $wp_details->avg_cost;
                    // $wp_details->avg_cost -= $wp_details_cost['amount'];
                }
                $total_in_warehouse = ($wp_details->quantity - (isset($data['prev_quantity']) ? $data['prev_quantity'] : 0)) * $wp_details->avg_cost;
                $total_in_addition = $data['quantity'] * $data['unit_cost'];
                $total_cost = (
                                ($total_in_warehouse > 0 ? $total_in_warehouse : 0) +
                                ($total_in_addition) +
                                $shipping_total +
                                $import_adjustment_cost +
                                $item_tax_2
                            );
                $total_quantity = ($wp_details->quantity > 0 ? $wp_details->quantity - (isset($data['prev_quantity']) ? $data['prev_quantity'] : 0) : 0) + $data['quantity'];

                // exit((
                // ">> wp_details->quantity ".$wp_details->quantity."\n".
                // ">> wp_details->avg_cost ".$wp_details->avg_cost."\n".
                // ">> total_in_warehouse ".$total_in_warehouse."\n".
                // ">> total_in_addition ".$total_in_addition."\n".
                // ">> total_cost ".$total_cost."\n".
                // ">> prev_quantity ".(isset($data['prev_quantity']) ? $data['prev_quantity'] : 0)."\n".
                // ">> total_quantity ".$total_quantity."\n".
                // ">> shipping_total ".$shipping_total."\n".
                // ">> import_adjustment_cost ".$import_adjustment_cost."\n".
                // ">> item_tax_2 ".$item_tax_2."\n".
                // ">> avg_cost ".($total_cost / $total_quantity)."\n"
                // ));
            }
            if ($total_quantity > 0) {
                $avg_cost = ($total_cost / $total_quantity);
                if ($product->tax_method == 1) {
                    // $tax_avg_cost = $this->site->calculateTax($product, $tax, $avg_cost);
                    // $avg_cost += $tax_avg_cost['amount'];
                }
                $this->db->update('warehouses_products', array('avg_cost' => $avg_cost, 'last_update' => date('Y-m-d H:i:s')), array('product_id' => $data['product_id']));
            }
        } else {
            $avg_cost = $data['unit_cost']+(
                    $shipping_unit+
                    $import_adjustment_unit_cost+
                    $item_tax_2_unit
                );
            if ($product->tax_method == 1) {
                // $tax_avg_cost = $this->site->calculateTax($product, $tax, $avg_cost);
                // $avg_cost += $tax_avg_cost['amount'];
                // exit(var_dump($avg_cost));
            }
            $this->db->insert('warehouses_products', array('product_id' => $data['product_id'], 'warehouse_id' => $data['warehouse_id'],
                'avg_cost' => $avg_cost,
                'quantity' => 0, 'last_update' => date('Y-m-d H:i:s')));
        }
        if ($this->Settings->accounting_method == 2) { //AVG
            if (isset($avg_cost)) {
                $this->db->update('products', array('avg_cost' => $avg_cost), array('id' => $data['product_id']));
                if (isset($data['purchase_item_id'])) {
                    $this->db->update('purchase_items', array('calculated_avg_cost' => $avg_cost), array('id' => $data['purchase_item_id']));
                }
                if (isset($data['adjustment_item_id'])) {
                    $this->db->update('adjustment_items', array('calculated_avg_cost' => $avg_cost), array('id' => $data['adjustment_item_id']));
                }
            }
        } else if ($this->Settings->accounting_method == 1) { //FIFO
            $q = $this->db->select('purchase_items.id, purchase_items.unit_cost, purchase_items.quantity')
                    ->join('purchases P', 'P.id = purchase_items.purchase_id', 'inner')
                    ->where('purchase_items.product_id', $data['product_id'])
                    ->order_by('P.id', 'ASC')
                    ->limit(1)
                    ->get('purchase_items');

            if ($q->num_rows() > 0) {
                $q = $q->row();
                $this->db->update('products', array('avg_cost' => $q->unit_cost), array('id' => $data['product_id']));
                if (isset($data['purchase_item_id'])) {
                    $this->db->update('purchase_items', array('calculated_avg_cost' => $q->unit_cost), array('id' => $data['purchase_item_id']));
                }

                if (isset($data['adjustment_item_id'])) {
                    $this->db->update('adjustment_items', array('calculated_avg_cost' => $q->unit_cost), array('id' => $data['adjustment_item_id']));
                }
            }
        }
    }

    public function getWarehousesProductQuantity($product_id, $warehouse_id = NULL)
    {
        $this->db->select('
                            SUM('.$this->db->dbprefix('warehouses_products').'.quantity) AS quantity,
                            SUM('.$this->db->dbprefix('warehouses_products').'.id) AS id,
                            avg_cost
                         ')
                      ->where('product_id', $product_id);
        if ($warehouse_id) {
            $this->db->where('warehouse_id', $warehouse_id);
        }
        $this->db->having('id >', 0);
        $q = $this->db->get('warehouses_products');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function validate_random_pin_code_date(){
        $c = $this->db->get_where('biller_data', ['random_pin_code !=' => NULL, 'random_pin_code_date !=' => NULL]);
        if ($c->num_rows() > 0) {
            foreach (($c->result()) as $data) {
                $fecha1 = new DateTime($data->random_pin_code_date);//fecha inicial
                $fecha2 = new DateTime(date('Y-m-d H:i:s'));//fecha de cierre
                $intervalo = $fecha1->diff($fecha2);
                $minutes = $intervalo->format('%i') + ($intervalo->format('%h') * 60) + ($intervalo->format('%d') * 1440);
                if ($minutes > $this->Settings->max_minutes_random_pin_code) {
                    $this->db->update('biller_data', ['random_pin_code'=>98987878747471424454745, 'random_pin_code_date'=>NULL], ['id'=>$data->id]);
                }
            }
        }
    }

    public function get_payroll_setting() {
        $q = $this->db->get('payroll_settings');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getProductUnitPrice($product_id, $unit_id, $prg_id = NULL) {
        if ($prg_id != NULL) {
            $this->db->where('price_group_id', $prg_id);
        }
        $q = $this->db->get_where('unit_prices', ['id_product'=>$product_id, 'unit_id'=>$unit_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function fix_item_tax($document_type_id, $item, $new_calculation = false, $purchase = false){
        $document_type = $this->site->getDocumentTypeById($document_type_id);
        $pdata = $this->site->getProductByID($item['product_id']);
        $category = $this->site->getCategoryByID($pdata->category_id);
        $subcategory = $this->site->getCategoryByID($pdata->subcategory_id);
        $except_category = $this->sma->validate_except_category_taxes($category, $subcategory);
        if ($except_category == false && $document_type->key_log == 1) {
            $pr = $this->site->getProductByID($item['product_id']);
            if ($pr->tax_rate != $item['tax_rate_id'] || $new_calculation)  {
                $tax_details = $this->site->getTaxRateByID($pr->tax_rate);
                $tx = $this->site->calculateTax($pr, $tax_details, $item[$purchase ? 'real_unit_cost' : 'real_unit_price']);
                if ($pr->tax_method == 0) {
                    $item[$purchase ? 'net_unit_cost' : 'net_unit_price'] = $item[$purchase ? 'real_unit_cost' : 'real_unit_price'] - $tx['amount'];
                    $item[$purchase ? 'unit_cost' : 'unit_price'] = $item[$purchase ? 'real_unit_cost' : 'real_unit_price'];
                } else if ($pr->tax_method == 1) {
                    $item[$purchase ? 'net_unit_cost' : 'net_unit_price'] = $item[$purchase ? 'real_unit_cost' : 'real_unit_price'];
                    $item[$purchase ? 'unit_cost' : 'unit_price'] = $item[$purchase ? 'real_unit_cost' : 'real_unit_price'] + $tx['amount'];
                }
                $item['item_tax'] = ($tx['amount'] * $item['quantity']);
                $item['tax_rate_id'] = $pr->tax_rate;
                $item['tax'] = $tx['tax'];
                $item['subtotal'] = $item[$purchase ? 'unit_cost' : 'unit_price'] * $item['quantity'];
                return $item;
            }
        }
        return false;
    }

    public function getAllOrderItems($order_id)
    {
        $q = $this->db->where(array('sale_id' => $order_id))
                      ->order_by('order_sale_items.id', 'desc')
                      ->get('order_sale_items');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function updateOrderSaleStatus($order_id){
        if ($order_items = $this->getAllOrderItems($order_id)) {
            $total_pending = 0;
            $total_delivered = 0;
           foreach ($order_items as $row => $item) {
               $total_pending += $item->quantity;
               $total_delivered += $item->quantity_delivered;
           }

           $total_pending -= $total_delivered;

           if ($total_pending > 0 && $total_delivered > 0) {
               if ($this->db->update('order_sales', array('sale_status' => 'partial'), array('id' => $order_id))) {
                   return TRUE;
               }
           }  else if ($total_pending > 0 && $total_delivered == 0) {
               if ($this->db->update('order_sales', array('sale_status' => 'pending'), array('id' => $order_id))) {
                   return TRUE;
               }
           } else if ($total_pending == 0) {
               if ($this->db->update('order_sales', array('sale_status' => 'completed'), array('id' => $order_id))) {
                   return TRUE;
               }
           }
        }
       return FALSE;
    }

    public function get_price_groups_units_relationed(){
        $q = $this->db->select('
                            product_prices.*,
                            units.name as unit_name,
                            units.operator as unit_operator,
                            units.operation_value as unit_operation_value,
                            products.tax_method as product_tax_method,
                            products.consumption_sale_tax as consumption_sale_tax,
                            price_groups.id as price_group_id
                        ')
              ->join('price_groups', 'price_groups.id = product_prices.price_group_id', 'inner')
              ->join('unit_prices', 'unit_prices.id_product = product_prices.product_id', 'left')
              ->join('units', 'units.id = unit_prices.unit_id AND units.price_group_id = price_groups.id ', 'inner')
              ->join('products', 'products.id = product_prices.product_id', 'inner')
              ->get('product_prices');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[$row->price_group_id][$row->product_id] = $row->unit_name;
            }
            return $data;
        }
        return false;
    }

    public function movement_setted_datetime($date){
        if (!empty($date)) {
            $strdate = date('Y-m-d', strtotime($date));
            if ($strdate == date('Y-m-d')) {
                $strdate = $strdate." ".date('H:i:s');
                return $strdate;
            }
        }
        return $date;
    }

    public function getExistingImage($imageName)
    {
        if (!file_exists($imageName)) {
            $this->session->set_flashdata('error', 'La imágen aún no ha sido configurada');
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function export_accountant_notes($data){
        $date_start = $data['date']." 00:00:00";
        $date_end   = $data['date']." 23:59:59";
        $dt_checked = [];
        if ($data['module'] == 1 || $data['module'] == 2) {
            $this->db->select('sales.*')
                     ->join('documents_types', 'documents_types.id = sales.document_type_id');
            $this->db->where('date >=', $date_start);
            $this->db->where('date <=', $date_end);
            if ($data['module'] == 1) {
                $this->db->where('pos', 1);
            } else {
                $this->db->where('pos', 0);
            }
            $this->db->where('documents_types.not_accounting', 0);
            $this->db->where('sale_status !=', 'pending');
            $q = $this->db->get('sales');
            if ($q->num_rows() > 0) {
                $this->load->library('excel');
                $this->excel->setActiveSheetIndex(0);
                $this->excel->getActiveSheet()->setTitle(lang('product_quantity_alerts'));
                $this->excel->getActiveSheet()->SetCellValue('A1', 'Tipo Doc');
                $this->excel->getActiveSheet()->SetCellValue('B1', 'Numero Doc');
                $this->excel->getActiveSheet()->SetCellValue('C1', 'Fecha');
                $this->excel->getActiveSheet()->SetCellValue('D1', 'Cuenta');
                $this->excel->getActiveSheet()->SetCellValue('E1', 'Concepto');
                $this->excel->getActiveSheet()->SetCellValue('F1', 'Centro de Costo ');
                $this->excel->getActiveSheet()->SetCellValue('G1', 'Valor');
                $this->excel->getActiveSheet()->SetCellValue('H1', 'Naturaleza');
                $this->excel->getActiveSheet()->SetCellValue('I1', 'Identidad del tercero');
                $Nrow = 2;
                foreach (($q->result_array()) as $row) {
                    $entry_id = $this->site->getEntryTypeNumberExisting($row);
                    // NO EXPORTAR LOS QUE EL TIPO DE DOCUMENTO TIENEN PARA NO CONTABILIZAR
                    $contabilidadSufijo = $this->session->userdata('accounting_module');
                    $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                    $ledgerstabla = 'ledgers_con'.$contabilidadSufijo;
                    $costcenterstabla = 'cost_centers_con'.$contabilidadSufijo;
                    $etitems = $this->db->select('
                                    '.$this->db->dbprefix($ledgerstabla).'.code as ledger_code,
                                    '.$this->db->dbprefix($costcenterstabla).'.code as cost_center_code,
                                    '.$this->db->dbprefix('companies').'.vat_no as third_vat_no,
                                    '.$this->db->dbprefix($etitemstabla).'.*
                                        ')
                             ->join($ledgerstabla, $ledgerstabla.".id = ".$etitemstabla.".ledger_id", 'left')
                             ->join($costcenterstabla, $costcenterstabla.".id = ".$etitemstabla.".cost_center_id", 'left')
                             ->join('companies', "companies.id = ".$etitemstabla.".companies_id", 'left')
                             ->where($etitemstabla.".entry_id", $entry_id)
                             ->get($etitemstabla);
                    $ref_arr = explode("-", $row['reference_no']);
                    if ($etitems->num_rows() > 0) {
                        foreach (($etitems->result()) as $etitem) {
                            $this->excel->getActiveSheet()->SetCellValue('A' . $Nrow, $ref_arr[0]);
                            $this->excel->getActiveSheet()->SetCellValue('B' . $Nrow, $ref_arr[1]);
                            $this->excel->getActiveSheet()->SetCellValue('C' . $Nrow, date('d/m/Y', strtotime($row['date'])));
                            $this->excel->getActiveSheet()->SetCellValue('D' . $Nrow, $etitem->ledger_code);
                            $this->excel->getActiveSheet()->SetCellValue('E' . $Nrow, $etitem->narration);
                            $this->excel->getActiveSheet()->SetCellValue('F' . $Nrow, $etitem->cost_center_code);
                            $this->excel->getActiveSheet()->SetCellValue('G' . $Nrow, $etitem->amount);
                            $this->excel->getActiveSheet()->SetCellValue('H' . $Nrow, $etitem->dc);
                            $this->excel->getActiveSheet()->SetCellValue('I' . $Nrow, $etitem->third_vat_no);
                            $Nrow++;
                        }
                    }

                }
                $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);
                $this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                $this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(15);
                $this->excel->getActiveSheet()->getStyle('G1:G'.$Nrow)->getNumberFormat()->setFormatCode("#,##0.00");
                $this->excel->getActiveSheet()->getStyle('I1:I'.$Nrow)->getNumberFormat()->setFormatCode("###0");
                $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                $filename = 'export_accountant_notes';
                $this->load->helper('excel');
                create_excel($this->excel, $filename);

            }
        }
    }

    public function export_accountant_notes_validations($data){
        $date_start = $data['date']." 00:00:00";
        $date_end   = $data['date']." 23:59:59";
        $dt_checked = [];
        if ($data['module'] == 1 || $data['module'] == 2) {
            $this->db->select('sales.*')
                     ->join('documents_types', 'documents_types.id = sales.document_type_id');
            $this->db->where('date >=', $date_start);
            $this->db->where('date <=', $date_end);
            if ($data['module'] == 1) {
                $this->db->where('pos', 1);
            } else {
                $this->db->where('pos', 0);
            }
            $this->db->where('documents_types.not_accounting', 0);
            $this->db->where('sale_status !=', 'pending');
            $q = $this->db->get('sales');
            if ($q->num_rows() > 0) {
                foreach (($q->result_array()) as $row) {
                    if (!isset($dt_checked[$row['document_type_id']])) {
                        $dt_checked[$row['document_type_id']] = 1;
                        $dt_data = $this->getDocumentTypeById($row['document_type_id'], NULL, true);
                        $this->update_document_type_contabilidad($dt_data, $dt_data['sales_prefix']);
                    }
                    if ($entry_id = $this->site->getEntryTypeNumberExisting($row)) {
                        $contabilidadSufijo = $this->session->userdata('accounting_module');
                        $entrytabla = 'entries_con'.$contabilidadSufijo;
                        $ettabla = 'entrytypes_con'.$contabilidadSufijo;
                        $etitemstabla = 'entryitems_con'.$contabilidadSufijo;
                        $start_desc_text = $entrytabla."/".$etitemstabla."/";
                        $register_entries_debit_credit_total = $this->db
                                ->select("
                                            {$this->db->dbprefix($ettabla)}.id as entry_id,
                                            SUM(IF({$this->db->dbprefix($etitemstabla)}.dc = 'C', {$this->db->dbprefix($etitemstabla)}.amount, 0)) as c_amount,
                                            SUM(IF({$this->db->dbprefix($etitemstabla)}.dc = 'D', {$this->db->dbprefix($etitemstabla)}.amount, 0)) as d_amount,
                                            CONCAT({$this->db->dbprefix($ettabla)}.prefix,{$this->db->dbprefix($entrytabla)}.number) as reference_no
                                        ")
                                ->join($entrytabla, $entrytabla.".id = ".$etitemstabla.".entry_id", "inner")
                                ->join($ettabla, $ettabla.".id = ".$entrytabla.".entrytype_id", "inner")
                                ->group_by($etitemstabla.'.entry_id')
                                ->having('c_amount != d_amount')
                                ->where($entrytabla.'.id', $entry_id)
                                ->from($etitemstabla)
                                ->order_by($entrytabla.'.entrytype_id ASC, '.$entrytabla.'.number ASC')
                                ->get();
                        if ($register_entries_debit_credit_total->num_rows() > 0) {
                            foreach (($register_entries_debit_credit_total->result()) as $et) {
                                $diff = $et->d_amount - $et->c_amount;
                                if ($inv = $this->db->get_where('sales', ['reference_no'=>$et->reference_no])->row()) {
                                    if ($diff > 1 || $diff < 1) {
                                        $this->sales_model->recontabilizarVenta($inv->id);
                                    } else if (($diff > 0 && $diff <= 1) || ($diff >= -1 && $diff < 0)) {
                                        if ($row_to_update = $this->db->limit(1)->get_where($etitemstabla, ['entry_id'=>$et->entry_id, 'dc'=>($diff < 0 ? 'D' : 'C')])->row()) {
                                            $this->db->update($etitemstabla, ['amount'=>$row_to_update->amount + abs($diff)], ['id'=>$row_to_update->id]);
                                        }
                                    }
                                } else {
                                    return ['error'=>1, 'msg'=>'Existe una nota contable asociada a una factura inexistente ('.$et->reference_no.')'];
                                }
                            }
                        }
                    } else {
                        $this->sales_model->recontabilizarVenta($row['id']);
                        if (!$entry_id = $this->site->getEntryTypeNumberExisting($row)){
                            return ['error'=>1, 'msg'=>'Existe un problema al intentar recontabilizar el registro ('.$row['reference_no'].')'];
                        }
                    }
                }
            } else {
                return ['error'=>1, 'msg'=>'Sin registros'];
            }

        }

        return false;
    }

    public function getMultiPaymentEncOnlyConcepts($reference = null){
        $this->db->select(
                          'payments.reference_no,
                           payment_methods.name as paid_by,
                           SUM('.$this->db->dbprefix("payments").'.amount -
                               '.$this->db->dbprefix("payments").'.rete_fuente_total -
                               '.$this->db->dbprefix("payments").'.rete_iva_total -
                               '.$this->db->dbprefix("payments").'.rete_ica_total -
                               '.$this->db->dbprefix("payments").'.rete_other_total) as amount,
                           MAX('.$this->db->dbprefix("payments").'.type) AS type,
                           MAX('.$this->db->dbprefix("payments").'.date) AS date,
                           MAX('.$this->db->dbprefix("payments").'.payment_date) AS payment_date,
                           MAX('.$this->db->dbprefix("payments").'.consecutive_payment) AS consecutive_payment,
                           MAX('.$this->db->dbprefix("payments").'.note) AS note,
                           MAX('.$this->db->dbprefix("payments").'.document_type_id) AS document_type_id,
                           companies.name as customer,
                           companies.city as city,
                           companies.address as address,
                           MAX('.$this->db->dbprefix("sales").'.customer_id) AS customer_id,
                           MAX('.$this->db->dbprefix("purchases").'.supplier_id) AS supplier_id,
                           MAX('.$this->db->dbprefix("purchases").'.purchase_type) AS purchase_type,
                           payments.company_id,
                           companies.vat_no,
                           companies.digito_verificacion,
                           IF('.$this->db->dbprefix("purchases").'.cost_center_id IS NULL, sales.cost_center_id, '.$this->db->dbprefix("purchases").'.cost_center_id) as cost_center_id,
                           IF('.$this->db->dbprefix("purchases").'.biller_id IS NULL, sales.biller_id, '.$this->db->dbprefix("purchases").'.biller_id) as biller_id,
                           payments.return_id,
                           expense_categories.name as expense_name
                           '
                        )
                 ->join('sales', 'sales.id = payments.sale_id', 'left')
                 ->join('purchases', 'purchases.id = payments.purchase_id', 'left')
                 ->join('expense_categories', 'expense_categories.id = payments.expense_id', 'left')
                 ->join('companies', 'companies.id = IF(IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), '.$this->db->dbprefix("payments").'.seller_id)', 'left')
                 ->join('payment_methods', 'payment_methods.code = payments.paid_by', 'left')
                 ->where('payments.paid_by != "due"')
                 ->where('payments.paid_by != "retencion"')
                 ->where('payments.paid_by != "discounted"')
                 ->where('payments.multi_payment = "1"')
                 ->group_by('payments.reference_no');
        if ($reference != null) {
            $this->db->where('payments.reference_no', $reference);
        }
        $pmnt = $this->db->get('payments');
        if ($pmnt->num_rows() > 0) {
            foreach (($pmnt->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function set_users_erp_data(){
        $host_name = $_SERVER['SERVER_NAME'];
        $ru = explode("/", $_SERVER['REQUEST_URI']);
        $host_folder = $ru[1];
        $host_database = $this->db->database;
        $this->db->update('users', ['host_url'=>$host_name,'company_folder'=>$host_folder,'db_name'=>$host_database], ['host_url'=>NULL]);
    }

    public function get_erp_db(){
        $new_config['hostname'] = '162.240.232.29';
        $new_config['username'] = 'wappsi_wp';
        $new_config['password'] = 'WpBd2030$';
        $new_config['database'] = 'wappsi_wappsiglobal';
        $new_config['dbdriver'] = $this->db->dbdriver;
        $new_config['dbprefix'] = '';
        $new_config['db_debug'] = $this->db->db_debug;
        $new_config['cache_on'] = $this->db->cache_on;
        $new_config['cachedir'] = $this->db->cachedir;
        $new_config['port']     = $this->db->port;
        $new_config['char_set'] = $this->db->char_set;
        $new_config['dbcollat'] = $this->db->dbcollat;
        $new_config['pconnect'] = $this->db->pconnect;
        if ($this->check_database($new_config)) {
            $DB2 = $this->load->database($new_config, TRUE);
            return $DB2;
        }
        return false;
    }

    public function deleteEntry($entry_id = array(), $contabilidadSufijo = null)
    {
      $tabla = 'entries_con'.$contabilidadSufijo;
      $tabla_ei = 'entryitems_con'.$contabilidadSufijo;
      if ($this->db->delete($tabla, ['id'=>$entry_id])) {
        if ($this->db->delete($tabla_ei, ['entry_id'=>$entry_id])) {
            return true;
        }
      }
      return FALSE;
    }

    public function validate_account_parametrization($ledger_id, $message, $entryId, $contabilidadSufijo){
        if (empty($ledger_id) || $ledger_id == false) {
            $this->session->set_userdata('swal_flash_message', sprintf(lang('invalid_account_parametrization'), $message));
            $this->deleteEntry($entryId, $contabilidadSufijo);
            return false;
        } else {
            return true;
        }
    }

    public function get_last_return_date_sale($sale_id){
        $q = $this->db->select('sales.*')
                  ->join('(SELECT MAX(return_id) as last_return_id, sale_id FROM '.$this->db->dbprefix('sales_returns').' WHERE sale_id = '.$sale_id.' GROUP BY sale_id) tbl_returns', 'tbl_returns.last_return_id = sales.id')
                  ->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
    }

    public function get_last_return_date_purchase($purchase_id){
        $q = $this->db->select('purchases.*')
                  ->join('(SELECT MAX(return_id) as last_return_id, purchase_id FROM '.$this->db->dbprefix('purchases_return').' WHERE purchase_id = '.$purchase_id.' GROUP BY purchase_id) tbl_returns', 'tbl_returns.last_return_id = purchases.id')
                  ->get('purchases');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
    }


    public function getTechnologyProviderConfiguration($technologyProviderId, $workEnvironment)
    {
        $this->db->where("technology_provider_id", $technologyProviderId);
        $this->db->where("work_enviroment", $workEnvironment);
        $response = $this->db->get("technology_provider_configuration");
        return $response->row();
    }

    public function get_zones(){
        $q = $this->db->get('zones');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_cities(){
        $q = $this->db->get('cities');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_states(){
        if ($this->Settings->pais) {
            $country = $this->getCountryByName($this->Settings->pais);
            $this->db->where('PAIS', $country->CODIGO);
        }
        $q = $this->db->get('states');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_product_preference_category_by_id($cat_id){
        $q = $this->db->get_where('preferences_categories', ['id'=>$cat_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_product_preference_by_id($prf_id){
        $q = $this->db->get_where('product_preferences', ['id'=>$prf_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getInvoicing($movementType)
    {
        $this->db->where('tipo_mov', $movementType);
        $this->db->select('COALESCE(SUM(cantidad_facturada * cantidad_unidad), 0) AS amount');
        $response = $this->db->get('wappsi_invoicing');

        return $response->row();
    }

    public function getHits()
    {
        $this->db->where('tipo_respuesta', SUCCESS);
        $this->db->where('tipo_registro <>', ELECTRONIC_PAYROLL);
        $this->db->select('COALESCE(SUM(cantidad), 0) AS amount');
        $response = $this->db->get('electronic_hits');

        return $response->row();
    }

    public function saveHits($documentId, $typeRegister, $message, $responseType, $date)
    {
        $data = [
            'fecha'         => $date,
            'tipo_registro' => $typeRegister,
            'registro_id'   => $documentId,
            'usuario'       => $this->session->userdata('user_id'),
            'tipo_respuesta'=> $responseType,
            'cantidad'      => 1,
            'mensaje'       => $message,
            'fecha_registro'=> date('Y-m-d H:m:i')
        ];

        if ($this->db->insert('electronic_hits', $data)) {
            return true;
        }

        return false;
    }

    public function validationElectronicInvoice()
    {
        $hits = $this->getHits();
        $invoicing = $this->getInvoicing(ELECTRONIC_DOCUMENTS);

        if ($this->Settings->electronic_hits_validation == YES) {
            if ($invoicing->amount <= 0) {
                return (object) [
                    "response"  => false,
                    "message"   => $this->lang->line('empty_electronic_packages')
                ];
            }

            if ($hits->amount > $invoicing->amount) {
                return (object) [
                    "response"  => false,
                    "message"   => $this->lang->line("past_max_attempts")
                ];
            }

            return (object) [
                'response' => true
            ];
        }

        return (object) [
            'response' => true
        ];
    }

    public function update_gift_card_balance_status($gc_id = NULL, $gc_no = NULL){
        if ($gc_id || $gc_no) {
            if ($gc_id) {
                $this->db->where('id', $gc_id);
            }
            if ($gc_no) {
                $this->db->where('card_no', $gc_no);
            }
            $q = $this->db->get('gift_cards')->row();
            if ($q->balance <= 0) {
                $this->db->update('gift_cards', ['status'=>2], ['id'=>$q->id]);
            } else {
                if ($q->expiry < date('Y-m-d')) {
                    $this->db->update('gift_cards', ['status'=>4], ['id'=>$q->id]);
                } else {
                    $this->db->update('gift_cards', ['status'=>1], ['id'=>$q->id]);
                }
            }
        }
    }

    public function wappsi_contabilidad_giftcard_topup($gift_card_topup = array())
    {
      if($this->wappsiContabilidadVerificacion($gift_card_topup['date']) > 0){
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $settings_con = $this->getSettingsCon();
        $account_parameter_method = $settings_con->account_parameter_method;
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $position2 = strpos($gift_card_topup['reference_no'], '-');
        if($position2 !== false){
            $label = substr($gift_card_topup['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($gift_card_topup['reference_no']);
        $gift_card = $this->getGiftCardByID($gift_card_topup['card_id']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($gift_card_topup['date'], 0, 10);
        $wappsiEntri['dr_total'] = $gift_card_topup['amount'];
        $wappsiEntri['cr_total'] = $gift_card_topup['amount'];
        $wappsiEntri['notes'] = "Recarga a tarjeta ".$gift_card->card_no."\n ";
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $gift_card->customer_id;
        $wappsiEntri['user_id'] = $gift_card_topup['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($gift_card_topup)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $amountForPay = 0;
        /* TRABAJANDO CON LOS PAGOS */
        $paymentMethodCon = $gift_card_topup['paid_by'];
        $ledgerPago = $this->getWappsiReceipLedgerId($paymentMethodCon, $contabilidadSufijo, $gift_card_topup['biller_id']);
        $entryPayment['entry_id'] =  $entryId;
        $entryPayment['amount'] = $gift_card_topup['amount'];
        $entryPayment['dc'] = 'D';
        $entryPayment['narration'] = 'Recarga a tarjeta '.$gift_card->card_no.', '.$gift_card_topup['reference_no'];
        $entryPayment['base'] = "";
        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
        $entryPayment['ledger_id'] = $ledgerPago;
        $entryPayments[] = $entryPayment;
        $ledgerPago = $this->getWappsiReceipLedgerId("gift_card", $contabilidadSufijo, $gift_card_topup['biller_id']);
        $entryPayment['entry_id'] =  $entryId;
        $entryPayment['amount'] = $gift_card_topup['amount'];
        $entryPayment['dc'] = 'C';
        $entryPayment['narration'] = 'Recarga a tarjeta '.$gift_card->card_no.', '.$gift_card_topup['reference_no'];
        $entryPayment['base'] = "";
        $entryPayment['companies_id'] = $wappsiEntri['companies_id'];
        $entryPayment['ledger_id'] = $ledgerPago;
        $entryPayments[] = $entryPayment;
        $entryItems = $entryPayments;
        $drTotal = 0;
        $crTotal = 0;
        // Insertar los entry items
        foreach ($entryItems as $entryItem){
            if($entryItem['amount'] >= 0){
                if (isset($gift_card_topup['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $gift_card_topup['cost_center_id'];
                }
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                    $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                    $crTotal = $crTotal + $entryItem['amount'];
                }
            }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      } /* Termina el if que valida la existencia del modulo de contabilidad. */
      return FALSE;
    }
    public function get_giftcard_by_saleitem($si_id){
        $q = $this->db->get_where('gift_cards', ['sale_item_id'=>$si_id]);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_new_order_sales(){
        $qno = $this->db->get_where('order_sales', ['sale_status'=>'pending']);
        if ($qno->num_rows() > 0) {
            return $qno->num_rows();
        }
        return false;
    }

    public function get_last_years_databases(){
        $actual_database = $this->db->database;
        $arrdb = explode("_", $actual_database);
        $txt_search = $arrdb[1];
        $dbexists = $this->db->query('SELECT SCHEMA_NAME AS "Database" FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME LIKE "%'.$txt_search.'%";');
        if ($dbexists->num_rows() > 0) {
            $data = [];
            foreach ($dbexists->result() as $dbrow) {
                if ($dbrow->Database != $this->db->database) {
                    $data[] = $dbrow;
                }
            }
            return $data;
        }
        return false;
    }

    public function get_past_year_connection($database){
        $new_config['hostname'] = $this->db->hostname;
        $new_config['username'] = $this->db->username;
        $new_config['password'] = $this->db->password;
        $new_config['database'] = $database;
        $new_config['dbdriver'] = $this->db->dbdriver;
        $new_config['dbprefix'] = $this->db->dbprefix;
        $new_config['db_debug'] = $this->db->db_debug;
        $new_config['cache_on'] = $this->db->cache_on;
        $new_config['cachedir'] = $this->db->cachedir;
        $new_config['port']     = $this->db->port;
        $new_config['char_set'] = $this->db->char_set;
        $new_config['dbcollat'] = $this->db->dbcollat;
        $new_config['pconnect'] = $this->db->pconnect;
        if ($this->check_database($new_config)) {
            $DB3 = $this->load->database($new_config, TRUE);
            return $DB3;
        }
        return false;
    }

    public function get_past_year_sale($term, $database){
        $DB3 = $this->get_past_year_connection($database);
        if ($DB3) {
            $sale = $DB3->select('sales.*')
                ->join('documents_types', 'documents_types.id = sales.document_type_id')
                ->where('sales.reference_no', $term)
                // ->where('documents_types.factura_electronica !=', 1)
                ->get('sales');
            if ($sale->num_rows() > 0) {
                return $sale->row();
            }
        }
        return false;
    }

    public function get_past_year_invoice_items($sale_id, $database, $return_id = NULL, $arr = false, $product_order = NULL)
    {
        $DB3 = $this->get_past_year_connection($database);
        if ($DB3) {
            $DB3->select('sale_items.*,
                sale_items.id as sale_item_id,
                tax_rates.code as tax_code,
                tax_rates.name as tax_name,
                tax_rates.rate as tax_rate,
                tax_rates.id as tax_rate_id,
                tax_rates.tax_indicator as tax_indicator,
                products.image,
                products.details as details,
                CONCAT('.$DB3->dbprefix('product_variants').'.name, '.$DB3->dbprefix('product_variants').'.suffix) as variant,
                products.hsn_code as hsn_code,
                products.second_name as second_name,
                products.tax_method as tax_method,
                brands.name as brand_name,
                products.reference as reference,
                units.code as product_unit_code,
                units.operation_value,
                units.operator')
                ->join('products', 'products.id = sale_items.product_id', 'left')
                ->join('brands', 'brands.id = products.brand', 'left')
                ->join('units', $DB3->dbprefix('units').'.id = IF('.$DB3->dbprefix('sale_items').'.product_unit_id_selected  > 0, '.$DB3->dbprefix('sale_items').'.product_unit_id_selected, IF('.$DB3->dbprefix('sale_items').'.product_unit_id > 0, '.$DB3->dbprefix('sale_items').'.product_unit_id, '.$DB3->dbprefix('products').'.sale_unit))', 'left')
                ->join('product_variants', 'product_variants.id = sale_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id = sale_items.tax_rate_id', 'left')
                ->group_by('sale_items.id');
            if ($product_order == NULL || $product_order == 4) {
                // exit('A');
                $DB3->order_by('sale_items.id', 'asc');
            } else if ($product_order == 1) {
                $DB3->order_by('products.code', 'asc');
            } else if ($product_order == 2) {
                $DB3->order_by('products.name', 'asc');
            } else if ($product_order == 3) {
                $DB3->order_by('products.name', 'desc');
            } else if ($product_order == 5) {
                if ($this->Settings->product_order == 2) {
                    $DB3->order_by('sale_items.id', 'asc');
                } else {
                    $DB3->order_by('sale_items.id', 'desc');
                }
            }
            if ($sale_id && !$return_id) {
                $DB3->where('sale_id', $sale_id);
            } elseif ($return_id) {
                $DB3->where('sale_id', $return_id);
            }
            $q = $DB3->get('sale_items');
            if ($q->num_rows() > 0) {
                    if ($arr == false) {
                        foreach (($q->result()) as $row) {
                            $data[] = $row;
                        }
                    } else {
                        foreach (($q->result_array()) as $row) {
                            $row['sale_id'] = $sale_id;
                            $data[] = $row;
                        }
                    }
                return $data;
            }
        }
        return FALSE;
    }

    public function get_past_year_invoice_payments($sale_id, $database){
        $DB3 = $this->get_past_year_connection($database);
        if ($DB3) {
            $payments = $DB3->get_where('payments', ['sale_id'=>$sale_id, 'paid_by !='=> 'retencion']);
            if ($payments->num_rows() > 0) {
                return $payments->result();
            }
        }
        return false;
    }

    public function get_past_year_document_type_by_id($document_type_id, $database) {
        $DB3 = $this->get_past_year_connection($database);

        $DB3->select('documents_types.*, invoice_formats.logo as invoice_format_logo')
                 ->join('invoice_formats', 'invoice_formats.id = documents_types.module_invoice_format_id', 'left');
        $DB3->where('documents_types.id', $document_type_id);
        $query = $DB3->get('documents_types');

        if ($query->num_rows() > 0) {
            return $query->row();
        }

        return FALSE;
    }

    public function getTypeElectronicDocument($documentId, $module = NULL)
    {
        $document = ($module == DOCUMENTO_SOPORTE) ? $this->site->getPurchaseByID($documentId) : $this->site->getSaleByID($documentId);
        $resolution = $this->site->getDocumentTypeById($document->document_type_id);

        if ($resolution->module == FACTURA_POS || $resolution->module == FACTURA_DETAL) {
            return INVOICE;
        } elseif ($resolution->module == NOTA_CREDITO_POS || $resolution->module == NOTA_CREDITO_DETAL) {
            return CREDIT_NOTE;
        } elseif ($resolution->module == NOTA_DEBITO_POS || $resolution->module == NOTA_DEBITO_DETAL) {
            return DEBIT_NOTE;
        } elseif ($resolution->module == DOCUMENTO_SOPORTE) {
            return DOCUMENT_SUPPORT;
        }
    }

    public function updateSale($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("sales", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_past_year_purchase($term, $database){
        $DB3 = $this->get_past_year_connection($database);
        if ($DB3) {
            $purchase = $DB3->select('purchases.*')
                ->join('documents_types', 'documents_types.id = purchases.document_type_id')
                ->where('purchases.reference_no', $term)
                // ->where('documents_types.factura_electronica !=', 1)
                ->get('purchases');
            if ($purchase->num_rows() > 0) {
                return $purchase->row();
            }
        }
        return false;
    }

    public function get_past_year_purchase_items($purchase_id, $database, $return_id = NULL, $arr = false, $product_order = NULL)
    {
        $DB3 = $this->get_past_year_connection($database);
        if ($DB3) {
            $DB3->select('purchase_items.*,
                purchase_items.id as purchase_item_id,
                tax_rates.code as tax_code,
                tax_rates.name as tax_name,
                tax_rates.rate as tax_rate,
                tax_rates.id as tax_rate_id,
                tax_rates.tax_indicator as tax_indicator,
                products.image,
                products.details as details,
                CONCAT('.$DB3->dbprefix('product_variants').'.name, '.$DB3->dbprefix('product_variants').'.suffix) as variant,
                products.hsn_code as hsn_code,
                products.second_name as second_name,
                products.tax_method as tax_method,
                brands.name as brand_name,
                products.reference as reference,
                units.code as product_unit_code,
                units.operation_value,
                units.operator')
                ->join('products', 'products.id = purchase_items.product_id', 'left')
                ->join('brands', 'brands.id = products.brand', 'left')
                ->join('units', $DB3->dbprefix('units').'.id = IF('.$DB3->dbprefix('purchase_items').'.product_unit_id_selected  > 0, '.$DB3->dbprefix('purchase_items').'.product_unit_id_selected, IF('.$DB3->dbprefix('purchase_items').'.product_unit_id > 0, '.$DB3->dbprefix('purchase_items').'.product_unit_id, '.$DB3->dbprefix('products').'.purchase_unit))', 'left')
                ->join('product_variants', 'product_variants.id = purchase_items.option_id', 'left')
                ->join('tax_rates', 'tax_rates.id = purchase_items.tax_rate_id', 'left')
                ->group_by('purchase_items.id');
            if ($product_order == NULL || $product_order == 4) {
                // exit('A');
                $DB3->order_by('purchase_items.id', 'asc');
            } else if ($product_order == 1) {
                $DB3->order_by('products.code', 'asc');
            } else if ($product_order == 2) {
                $DB3->order_by('products.name', 'asc');
            } else if ($product_order == 3) {
                $DB3->order_by('products.name', 'desc');
            } else if ($product_order == 5) {
                if ($this->Settings->product_order == 2) {
                    $DB3->order_by('purchase_items.id', 'asc');
                } else {
                    $DB3->order_by('purchase_items.id', 'desc');
                }
            }
            if ($purchase_id && !$return_id) {
                $DB3->where('purchase_id', $purchase_id);
            } elseif ($return_id) {
                $DB3->where('purchase_id', $return_id);
            }
            $q = $DB3->get('purchase_items');
            if ($q->num_rows() > 0) {
                    if ($arr == false) {
                        foreach (($q->result()) as $row) {
                            $data[] = $row;
                        }
                    } else {
                        foreach (($q->result_array()) as $row) {
                            $row['purchase_id'] = $purchase_id;
                            $data[] = $row;
                        }
                    }
                return $data;
            }
        }
        return FALSE;
    }
    public function get_past_year_purchase_payments($purchase_id, $database){
        $DB3 = $this->get_past_year_connection($database);
        if ($DB3) {
            $payments = $DB3->get_where('payments', ['purchase_id'=>$purchase_id,'paid_by !='=>'retencion']);
            if ($payments->num_rows() > 0) {
                return $payments->result();
            }
        }
        return false;
    }

    public function getFilename($documentId)
    {
        $document = $this->site->getSaleByID($documentId);
        $nit = $this->getZeroLeft($this->Settings->numero_documento);
        $documentType = $this->site->getTypeElectronicDocument($document->id);
        $year = date('y', strtotime($document->date));
        $reference = $document->reference_no;
        $referenceArray = explode('-', $reference);
        $consecutive = $referenceArray[1];
        $alterConsecutive = $this->getZeroLeft(dechex($consecutive), 8);
        $providerTechnologyCode = '021';
        if ($this->Settings->fe_technology_provider == '3') {
            $providerTechnologyCode = '018';
        }

        if ($documentType == INVOICE) { $prefix = 'fv'; }
        else if ($documentType == CREDIT_NOTE) { $prefix = 'nc'; }
        else { $prefix = 'nd'; }

        return $prefix.$nit.$providerTechnologyCode.$year.$alterConsecutive;
    }

    public function getZeroLeft($value, $numberZeros = 10)
    {
        $length = strlen($value);
        $diff = $numberZeros - $length;
        if ($diff > 0) {
            for ($i=1; $i <= $diff  ; $i++) {
                $value = "0".$value;
            }
        }

        return $value;
    }

    public function get_past_year_option_by_id($option_id, $database){
        $DB3 = $this->get_past_year_connection($database);
        if ($DB3) {
            $option = $DB3->get_where('product_variants', ['id'=>$option_id]);
            if ($option->num_rows() > 0) {
                return $option->row();
            }
        }
        return false;
    }

    public function getDocumentsTypeByBillerId($billerId = NULL, $modules = [], $electronicDocument = 0)
    {
        $this->db->select("documents_types.id AS id, documents_types.sales_prefix, documents_types.nombre");
        if (!empty($modules)) {
            $this->db->where_in('documents_types.module', $modules);
        }
        if (!empty($billerId)) {
            $this->db->where('biller_documents_types.biller_id', $billerId);
            $this->db->join('biller_documents_types', 'biller_documents_types.document_type_id = documents_types.id');
        }

        $this->db->where('factura_electronica', $electronicDocument);
        $q = $this->db->get('documents_types');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getSalesOrigins()
    {
        $q = $this->db->get('sale_origins');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getSalesCampaigns()
    {
        $q = $this->db->get('sale_campaigns');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getPaymentsMethods()
    {
        $q = $this->db->get('payment_methods');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function create_temporary_product_billers_assoc($biller_id = NULL){

        if ($biller_id) {
            $tbl_pb_name = 'products_billers_assoc_'.$biller_id;
            $q = $this->db->query('SHOW TABLES LIKE "%'.$this->db->dbprefix($tbl_pb_name).'%"');
            if ($q->num_rows() >= 1) {
                if ($this->session->userdata('last_products_exclusivity_update') && $this->session->userdata('last_products_exclusivity_update') > $this->Settings->last_products_exclusivity_update) {
                    return false;
                }
            }
            $this->session->set_userdata('last_products_exclusivity_update', date('Y-m-d H:i:s'));
            $this->db->query('DROP TABLE IF EXISTS '.$this->db->dbprefix($tbl_pb_name));
            $this->db->query('DROP TABLE IF EXISTS sma_products_billers_assoc_'.$this->session->userdata('user_id'));
            $this->db->query('CREATE TABLE '.$this->db->dbprefix($tbl_pb_name).'(product_id INT(11), product_name VARCHAR(200));');
            $this->db->query('
                INSERT INTO '.$this->db->dbprefix($tbl_pb_name).'
                (
                    SELECT P.id, P.name
                    FROM
                    '.$this->db->dbprefix('products').' P
                    LEFT JOIN
                    '.$this->db->dbprefix('products_billers').' PB ON PB.product_id = P.id
                    WHERE PB.id IS NULL
                    ) ');

            $this->db->query('
                INSERT INTO '.$this->db->dbprefix($tbl_pb_name).'
                (
                    SELECT P.id, P.name
                    FROM
                    '.$this->db->dbprefix('products').' P
                    LEFT JOIN
                    '.$this->db->dbprefix('products_billers').' PB ON PB.product_id = P.id
                    WHERE PB.id IS NOT NULL AND PB.biller_id = '.$biller_id.'
                    ) ');
            $this->db->query('ALTER TABLE `'.$this->db->dbprefix($tbl_pb_name).'`
                                ADD INDEX `pbassoc` (`product_id` ASC);');
        }
    }

    public function delete_temporary_product_billers_assoc(){
        if ($this->session->userdata('biller_id')) {
            return false;
        }
        $this->db->query('DROP TABLE IF EXISTS '.$this->db->dbprefix('products_billers_assoc'));
    }

    public function getCustomerProduct()
    {
        $this->db->join('products', 'products.id = customer_product.product_id', 'inner');
        $q = $this->db->get('customer_product');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getCitiesFromSuppliers()
    {
        $q = $this->db->query("SELECT * FROM {$this->db->dbprefix('cities')} C
                            INNER JOIN (SELECT city_code FROM {$this->db->dbprefix('companies')} C WHERE C.city_code <> '' GROUP BY C.city_code) AS CC ON CC.city_code = C.CODIGO;");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getCitiesByCodes($cityCodes) {
        $this->db->where("CODIGO IN ({$cityCodes})");
        $q = $this->db->get('cities');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_invoice_format($id = null){
        if ($id != null) {
            $this->db->where('id', $id);
        }
        $q = $this->db->get('invoice_formats');
        if ($q->num_rows() > 0) {
            return $id ? $q->row() : $q->result();
        }
        return false;
    }

    public function create_temporary_sales_payments($end_date){
        $this->db->query('DROP TABLE IF EXISTS '.$this->db->dbprefix('temp_sales_payments'));
        $this->db->query('CREATE TEMPORARY TABLE '.$this->db->dbprefix('temp_sales_payments').'(sale_id INT(11), total_paid DECIMAL(25,4), PRIMARY KEY(sale_id));');
        $this->db->query("
            INSERT INTO {$this->db->dbprefix('temp_sales_payments')}
            (
                SELECT P.sale_id, sum(P.amount) AS totAal_paid FROM {$this->db->dbprefix('payments')} P
                    INNER JOIN {$this->db->dbprefix('sales')} S ON S.id = P.sale_id
                WHERE P.date <= '{end_date}' AND S.date <= '{end_date}' AND S.sale_status != 'returned' AND (S.portfolio_end_date >= '{end_date}' OR S.portfolio_end_date IS NULL) AND P.sale_id IS NOT NULL
                GROUP BY P.sale_id
            ) ");
    }
    public function delete_temporary_sales_payments(){
        $this->db->query('DROP TABLE IF EXISTS '.$this->db->dbprefix('temp_sales_payments'));
    }
    public function get_all_products_units($filters = []){
        $this->db->select('
                            p.id as product_id,
                            p.unit as main_unit_id,
                            p.price as main_price,
                            p.cost as main_cost,
                            p.name as product_name,
                            p.code as product_code,
                            u.id as unit_id,
                            u.name as unit_name,
                            u.price_group_id as unit_pgid,
                            up.valor_unitario as unit_price,
                            main_u.name as main_u_name,
                            main_u.operation_value as main_operation_value,
                            u.operation_value as unit_operation_value,
                            main_u.price_group_id as main_u_pgid
                        ');
        // $this->Settings->prioridad_precios_producto == 10
        $this->db->from($this->db->dbprefix('products').' p');
        $this->db->join($this->db->dbprefix('unit_prices').' up', 'up.id_product = p.id');
        $this->db->join($this->db->dbprefix('units').' u', 'u.id = up.unit_id');
        $this->db->join($this->db->dbprefix('units').' main_u', 'main_u.id = p.unit');
        // $this->db->order_by('p.name asc, u.operation_value asc');
        $this->db->order_by('u.operation_value, up.valor_unitario DESC, p.id, u.id');
        if ($filters) {
            if ($filters['with_stock']) {
                $this->db->where('p.quantity >', 0);
            }
            if ($filters['with_discontinued']) {
            } else {
                $this->db->where('p.discontinued', 0);
            }
            if ($filters['only_featured']) {
                $this->db->where('p.featured', 1);
            }
        } else {
            $this->db->where('p.discontinued', 0);
        }
        $this->db->where('up.status', 1);
        $this->db->group_by('p.id, u.id');
        $query = $this->db->get();
        $results = $query->result();
        return $results;
    }


    public function get_user_activity_by_id($id)
    {
        $q = $this->db->select("
                        user_activities.date,
                        CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as user,
                        user_activities.type_id,
                        user_activities.module_name,
                        user_activities.table_name,
                        user_activities.record_id,
                        user_activities.description")->join('users', 'users.id = user_activities.user_id')->where(array('user_activities.id' => $id))->get('user_activities');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_catalogue_products($view_discontinued = false, $biller_id = NULL)
    {
        $biller_id = $this->session->userdata('biller_id') ? $this->session->userdata('biller_id') : $biller_id;
        if ($biller_id) {
            $this->site->create_temporary_product_billers_assoc($biller_id);
        }
        $this->db->select('products.*, product_variants.name as variant_name, product_variants.code as variant_code, product_variants.price AS v_price,  COALESCE('.$this->db->dbprefix('product_variants') . '.quantity, 0) as variant_quantity, materials.name as material, colors.name as color, ' . $this->db->dbprefix('tax_rates') . '.name as tax_rate_name, '.$this->db->dbprefix('tax_rates') . '.code as tax_rate_code, c.name as category_code, sc.name as subcategory_code, ssc.name as sub_subcategory_code');
        if (!$view_discontinued) {
            $this->db->where('discontinued', 0);
        }
        if ($biller_id) {
            $this->db->join('products_billers_assoc_'.$biller_id.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner');
        }
        $q = $this->db->join('product_variants', 'product_variants.product_id = products.id', 'left')
        ->join('materials', 'materials.id = products.material_id', 'left')
        ->join('colors', 'colors.id = products.color_id', 'left')
        ->join('tax_rates', 'tax_rates.id=products.tax_rate', 'left')
        ->join('categories c', 'c.id=products.category_id', 'left')
        ->join('categories sc', 'sc.id=products.subcategory_id', 'left')
        ->join('categories ssc', 'ssc.id=products.second_level_subcategory_id', 'left')
                      ->from('products')->get();
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            $this->site->delete_temporary_product_billers_assoc();
            return $data;
        }
        return FALSE;
    }

    public function getSalesByUUID($uuid)
    {
        $this->db->where('cufe', trim($uuid));
        $q = $this->db->get('sales');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getInstanceById($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get("electronic_billing_instances");
        if ($q->result() > 0) {
            return $q->row();
        }
        return false;
    }


    public function getnameTypeIntegration($typeId)
    {
        if ($typeId == 1) {
            return 'Tienda PRO';
        } else if ($typeId == 2) {
            return "Marketplace";
        } else if ($typeId == 3) {
            return "Wshops";
        } else {
            return "CRM";
        }
    }

    public function getCodeConsecutive($module){
        $q = $this->db->get('order_ref')->row();
        $code = false;
        if ($module == 'product') {
            $code = ($q->product_code);
        } else if ($module == 'variant') {
            $code = ($q->variant_code);
        } else if ($module == 'category') {
            $code = ($q->category_code);
        } else if ($module == 'brand') {
            $code = ($q->brand_code);
        }
        return $code;
    }

    public function updateCodeConsecutive($module, $consecutive){
        if ($module == 'product') {
            $this->db->update('order_ref', ['product_code'=>$consecutive], ['ref_id'=>1]);
        } else if ($module == 'variant') {
            $this->db->update('order_ref', ['variant_code'=>$consecutive], ['ref_id'=>1]);
        } else if ($module == 'category') {
            $this->db->update('order_ref', ['category_code'=>$consecutive], ['ref_id'=>1]);
        } else if ($module == 'brand') {
            $this->db->update('order_ref', ['brand_code'=>$consecutive], ['ref_id'=>1]);
        }
    }


    public function get_tags_by_module($module){
        $q = $this->db->get_where('tags', ['module'=>$module, 'status'=>1]);
        if ($q->num_rows() > 0) {
            return $q->result();
        } else {
            return false;
        }
    }

    public function getBillers()
    {
        if (!empty($this->session->userdata('biller_id'))) {
            $this->db->where_in('id', $this->session->userdata('biller_id'));
        }
        $this->db->where('status', ACTIVE);
        $this->db->like("group_name", "biller");
        $q = $this->db->get('companies');
        return $q->result();
    }

    public function findWarehouse($data)
    {
        if (!empty($data)) {
            $this->db->where($data);
        }
        $q = $this->db->get("warehouses");
        return $q->row();
    }

    public function wappsiContabilidadImportaciones($data=[], $items=[])
    {
        //GUARDAR CUENTAS DE GASTOS NO APROBADO
      if($this->wappsiContabilidadVerificacion($data['date']) > 0)
      {
        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }
        $settings_con = $this->getSettingsCon();
        $account_parameter_method = $settings_con->account_parameter_method;
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = NULL;
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = 0;
        $wappsiEntri['cr_total'] = 0;
        $wappsiEntri['notes'] = "Importación ".$data['reference_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['supplier_id'];
        $wappsiEntri['user_id'] = $data['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        $data['wappsiEntri'] = $wappsiEntri;
        if ($entryId = $this->site->getEntryTypeNumberExisting($data)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryItems = array();
        $amountForPay = 0;
        foreach ($items as $row){
            $p = $this->db->get_where('purchases', ['id'=>$row['purchase_id']])->row();
            $pi = $this->db->get_where('purchase_items', ['id'=>$row['purchase_item_id']])->row();
            $pr = $this->db->get_where('products', ['id'=>$row['product_id']])->row();
            if ($row['adjustment_cost'] > 0) {
                $ledgerPago = $this->getWappsiLedgerId(2, 'Ajuste Importacion', $pr->tax_rate, $contabilidadSufijo, $account_parameter_method);
                $entryItem['entry_id'] =  $entryId;
                $entryItem['amount'] = $row['total_adjustment_cost'];
                $entryItem['dc'] = 'D';
                $entryItem['narration'] = 'Ajuste costo '.$row['adjustment_cost']." producto ".$pr->code." - ".$pr->name." de compra ".$p->reference_no;
                $entryItem['base'] = "";
                $entryItem['companies_id'] = $p->supplier_id;
                $entryItem['ledger_id'] = $ledgerPago;
                if (!$this->validate_account_parametrization($ledgerPago, "Ajuste Importacion", $entryId, $contabilidadSufijo)) return false;
            } else {
                $expense_category = $this->getExpenseCategory($row['expense_category_id']);
                $entryItem['entry_id'] =  $entryId;
                $entryItem['amount'] = $row['net_unit_cost'];
                $entryItem['dc'] = 'C';
                $entryItem['narration'] = "Gasto ".$p->reference_no." a prorratear por valor de ".$p->total;
                $entryItem['base'] = "";
                $entryItem['companies_id'] = $p->supplier_id;
                $entryItem['ledger_id'] = $expense_category->ledger_id;
            }
            $entryItems[] = $entryItem;
        }
        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function getSecondTaxes(){
        $q = $this->db->get('tax_secondary');
        if ($q->num_rows() > 0) {
            return $q->result();
        }
    }

    public function variantHasProducts($id){
        $v = $this->db->get_where('variants', ['id'=>$id])->row();
        $pv = $this->db->get_where('product_variants', ['name'=>$v->name]);
        if ($pv->num_rows() > 0) {
            return true;
        }
        return false;
    }

    public function wappsiContabilidadRecaudo($data=[])
    {
        //GUARDAR CUENTAS DE GASTOS NO APROBADO
      if($this->wappsiContabilidadVerificacion($data[0]['date']) > 0)
      {
        if (!$this->validate_doc_type_accounting($data[0]['document_type_id'])) {
            return false;
        }
        $settings_con = $this->getSettingsCon();
        $account_parameter_method = $settings_con->account_parameter_method;
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;
        $wappsiEntri['entrytype_id'] = 0;
        $label = NULL;
        $position2 = strpos($data[0]['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data[0]['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data[0]['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data[0]['date'], 0, 10);
        $wappsiEntri['dr_total'] = 0;
        $wappsiEntri['cr_total'] = 0;
        $wappsiEntri['notes'] = lang('payment_collection')." ".$data[0]['reference_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data[0]['customer_id'];
        $wappsiEntri['user_id'] = $data[0]['created_by'];
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        $data[0]['wappsiEntri'] = $wappsiEntri;
        if ($entryId = $this->site->getEntryTypeNumberExisting($data[0])) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryItems = array();
        $amountForPay = 0;
        foreach ($data as $dataArr) {
            $entryItem['entry_id'] =  $entryId;
            $entryItem['amount'] = $dataArr['amount'];
            $entryItem['dc'] = 'D';
            $entryItem['narration'] = lang('payment_collection')." ".$dataArr['reference_no'];
            $entryItem['base'] = "";
            $entryItem['companies_id'] = $dataArr['customer_id'];
            $ledgerPago = $this->getWappsiReceipLedgerId($dataArr['paid_by'], $contabilidadSufijo, $dataArr['biller_id']);
            $entryItem['ledger_id'] = $ledgerPago;
            if (isset($dataArr['cost_center_id'])) {
                $entryItem['cost_center_id'] = $dataArr['cost_center_id'];
            }
            $entryItems[] = $entryItem;

            $entryItem['entry_id'] =  $entryId;
            $entryItem['amount'] = $dataArr['amount'];
            $entryItem['dc'] = 'C';
            $entryItem['narration'] = lang('payment_collection')." ".$dataArr['reference_no'];
            $entryItem['base'] = "";
            $entryItem['companies_id'] = $dataArr['customer_id'];
            $ledgerPago = $this->getWappsiLedgerId(3, 'Pago', NULL, $contabilidadSufijo, $account_parameter_method);
            $entryItem['ledger_id'] = $ledgerPago;
            if (isset($dataArr['cost_center_id'])) {
                $entryItem['cost_center_id'] = $dataArr['cost_center_id'];
            }
            $entryItems[] = $entryItem;
        }


        $drTotal = 0;
        $crTotal = 0;
        foreach ($entryItems as $entryItem){
          if($entryItem['amount'] >= 0){
            $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
            if( $entryItem['dc'] == 'D' ){
              $drTotal = $drTotal + $entryItem['amount'];
            }else if( $entryItem['dc'] == 'C' ){
              $crTotal = $crTotal + $entryItem['amount'];
            }
          }
        }
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        // $this->sma->print_arrays($entryItems);
        return TRUE;
      }
      return FALSE;
    }
    function get_supplier_by_vatno($vatno){
        $q = $this->db->where([
                    'group_name' => 'supplier',
                    'vat_no' => $vatno,
                        ])->get('companies');
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function getMultiPayments_to_excel($reference = null){
        $type = 1;
        $q = $this->db->select("
                            {$this->db->dbprefix('payments')}.reference_no as reference_no,
                            {$this->db->dbprefix('payments')}.reference_no as ref_2,
                            {$this->db->dbprefix('payments')}.date,
                            {$this->db->dbprefix('payments')}.payment_date,
                            {$this->db->dbprefix('payments')}.return_reference_no,
                            {$this->db->dbprefix('payments')}.consecutive_payment,
                            companies.name as ".($type == 1 ? "customer" : "supplier").",
                            payment_methods.name as paid_by,
                            SUM(
                                  {$this->db->dbprefix('payments')}.amount
                                  -
                                  (
                                      ".($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_fuente_assumed = 0, {$this->db->dbprefix('payments')}.rete_fuente_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_fuente_total + ").
                                      ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_iva_assumed = 0, {$this->db->dbprefix('payments')}.rete_iva_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_iva_total + ").
                                      ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_ica_assumed = 0, {$this->db->dbprefix('payments')}.rete_ica_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_ica_total + ").
                                      ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_ica_assumed = 0, {$this->db->dbprefix('payments')}.rete_bomberil_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_bomberil_total + ").
                                      ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_ica_assumed = 0, {$this->db->dbprefix('payments')}.rete_autoaviso_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_autoaviso_total + ").
                                      ($type == 2 ? "IF({$this->db->dbprefix('payments')}.rete_other_assumed = 0, {$this->db->dbprefix('payments')}.rete_other_total, 0) + " : "{$this->db->dbprefix('payments')}.rete_other_total + ").
                                      "IF({$this->db->dbprefix('payments')}.paid_by = 'discount', {$this->db->dbprefix('payments')}.amount, 0)
                                  )
                                  ) as amount_applied,
                          ".($type != 3 ?
                                  ($type == 1 ?
                                      "SUM(IF({$this->db->dbprefix('payments')}.sale_id IS NOT NULL, {$this->db->dbprefix('payments')}.amount, 0)) as amount"
                                      :
                                      "SUM(IF({$this->db->dbprefix('payments')}.purchase_id IS NOT NULL, {$this->db->dbprefix('payments')}.amount, 0)) as amount"
                                    )
                                  :
                                  "SUM({$this->db->dbprefix('payments')}.amount)"
                              ).",
                            {$this->db->dbprefix('payments')}.type,
                            {$this->db->dbprefix('payments')}.attachment

                            ")
                        ->join('payment_methods', 'payment_methods.code = payments.paid_by', 'left')
                        ->join('purchases', 'purchases.id = payments.purchase_id', 'left')
                        ->join('sales', 'sales.id = payments.sale_id', 'left')
                        ->join('companies', 'companies.id = IF(IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), IF('.$this->db->dbprefix("payments").'.company_id IS NOT NULL, '.$this->db->dbprefix("payments").'.company_id,  IF('.$this->db->dbprefix("sales").'.customer_id IS NOT NULL, '.$this->db->dbprefix("sales").'.customer_id, '.$this->db->dbprefix("purchases").'.supplier_id)), '.$this->db->dbprefix("payments").'.seller_id)', 'left')
                        ->join('companies as biller', 'biller.id = IF('.$this->db->dbprefix("sales").'.biller_id IS NOT NULL, '.$this->db->dbprefix("sales").'.biller_id, IF('.$this->db->dbprefix("purchases").'.biller_id IS NOT NULL, '.$this->db->dbprefix("purchases").'.biller_id, '.$this->db->dbprefix("payments").'.seller_id))', 'left')
                        ->where('payments.paid_by != "due"')
                        ->where('payments.paid_by != "retencion"')
                        ->where('payments.multi_payment = "1"')
                        ->group_by('payments.reference_no')
                        ->where("{$this->db->dbprefix('payments')}.reference_no", $reference)
                        ->get("{$this->db->dbprefix('payments')}");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return NULL;
    }
    function get_users_serials(){
        $q = $this->db->select('users.user_pc_serial as serial')
                    ->from('users')
                    ->where('users.user_pc_serial IS NOT NULL')
                    ->where('users.user_pc_serial != ""')
                    ->group_by('users.user_pc_serial')->get();
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    function get_not_accepted_transfers($biller_id = NULL, $warehouse_id = NULL){
        $this->db->select('*');
        if ($biller_id) {
            $this->db->where('to_warehouse_id', $warehouse_id);
        }
        if ($warehouse_id) {
            $this->db->where('destination_biller_id', $biller_id);
        }
        $q = $this->db->where('received', 0)->get('transfers');
        if ($q->num_rows() > 0) {
            return $q->num_rows();
        }
        return false;
    }

    function check_if_register_exists($table, $data){
        $unset_fields = ['date', 'payment_status', 'registration_date', 'last_update', 'portfolio_end_date', 'hash', 'codigo_qr', 'cufe', 'printed', 'paid'];
        foreach ($unset_fields as $key => $value) {
            if (isset($data[$value])) {
                unset($data[$value]);
            }
        }
        if ($data && count($data) > 0 && $table && $table != "") {
            $q = $this->db->select('id')->where($data)->order_by('id', 'desc')->limit(1)->get($table);
            if ($q->num_rows() == 0) {
                return false;
            } else {
                $q = $q->row();
                return $q->id;
            }
        }
        return true;
    }

    function check_reference($data){
        $table = $data['table'];
        $record_id = $data['record_id'];
        $reference = $data['reference'];
        $biller_id = $data['biller_id'];
        $document_type_id = $data['document_type_id'];
        $ref = explode("-", $reference);
        $actual_consecutive = ($ref[1]);
        $new_consecutive = ($ref[1]+1);
        $record = $this->db->get_where($table, ['reference_no'=>$reference, 'document_type_id'=>$document_type_id, 'id !=' => $record_id]); //obtener registros con mismo consecutivo
        if ($record->num_rows() > 0) {
            $record_row = $record->row();
            if ($record_row->id < $record_id) { //si el otro registro, tiene menor id al que se revisa
                //entramos a actualizar la referencia
                $new_reference = $ref[0]."-".$new_consecutive;
                $data['reference'] = $new_reference;
                if ($this->check_reference($data)) { //revisamos si nueva referencia existe, en caso de no existir actualizamos
                    $this->db->update($table, ['reference_no'=>$new_reference], ['id'=>$record_id]);
                    return $new_reference;
                    //Si existe, pero se actualiza, se retorna true
                }
                //Si existe, pero no se hace nada, se retorna true sin actualizar
                return $reference;
            } else { // el otro registro tiene mayor id, por lo que no se hace nada
                return false;
            }
        }
        $this->updateBillerConsecutive($document_type_id, $actual_consecutive+1);
        return $reference;
    }

    public function getCompanyByName($name){
        $q = $this->db->get_where('companies', "( name = '{$name}' OR company = '{$name}' )");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function wappsiContabilidadTarjetasRegalo($data = array(), $return = false)
    {
      if($this->wappsiContabilidadVerificacion($data['date']) > 0)
      {
        if (!$this->validate_doc_type_accounting($data['document_type_id'])) {
            return false;
        }
        $contabilidadSufijo = $this->session->userdata('accounting_module');
        $wappsiEntri = array();
        $wappsiEntri['tag_id'] = null;

        $label = $this->getWappsiContabilidadOperationPrefix("deposit_prefix");
        $position2 = strpos($data['reference_no'], '-');
        if($position2 !== false){
            $label = substr($data['reference_no'], 0,$position2);
        }
        $number = $this->cleanContaReference($data['reference_no']);
        $wappsiEntri['entrytype_id'] = $this->getWappsiIdEntryType($label, $contabilidadSufijo);
        $wappsiEntri['number'] = $number;
        $wappsiEntri['date'] = substr($data['date'], 0, 10);
        $wappsiEntri['dr_total'] = $data['value'];
        $wappsiEntri['cr_total'] = $data['value'];
        $wappsiEntri['notes'] = (!$return ? 'Creación ' : 'Anulación ').lang('gift_card')." ".$data['reference_no']." N° ".$data['card_no'];
        $wappsiEntri['state'] = 1;
        $wappsiEntri['companies_id'] = $data['customer_id'];
        $wappsiEntri['user_id'] = $data['created_by'];
        //Actualizar wap_entrytypes
        $this->setWappsiNumberingEntryType($wappsiEntri['entrytype_id'], $wappsiEntri['number'], $contabilidadSufijo);
        // insert in the table entries_con
        // $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        if ($entryId = $this->site->getEntryTypeNumberExisting($data, false, false, null)) {
            $this->updateWappsiEntri($entryId, $wappsiEntri, $contabilidadSufijo);
        } else {
            // insert in the table entries_con
            $entryId = $this->insertWappsiEntri($wappsiEntri, $contabilidadSufijo);
        }
        $entryItems = [];
        $drTotal = 0;
        $crTotal = 0;
        $entryItems = [];
        $paymentMethodCon = $data['paid_by'];

        $entryItem = [];
        $ledgerPago = $this->getWappsiReceipLedgerId((!$return ? $paymentMethodCon : 'gift_card'), $contabilidadSufijo, $data['biller_id']);
        $ledgerContrapartida = $this->getWappsiReceipLedgerId((!$return ? "gift_card" : $paymentMethodCon), $contabilidadSufijo, $data['biller_id']);
        $entryItem['entry_id'] = $entryId;
        $entryItem['ledger_id'] = $ledgerPago;
        $entryItem['amount'] = $data['value'];
        $entryItem['dc'] = "D";
        $entryItem['narration'] = (!$return ? 'Creación ' : 'Anulación ').lang('gift_card')." ".$data['reference_no']." N° ".$data['card_no'];
        $entryItem['companies_id'] = $data['customer_id'];
        $entryItem['base'] = 0;
        $entryItems[] = $entryItem;

        $entryItem = [];
        $entryItem['entry_id'] = $entryId;
        $entryItem['ledger_id'] = $ledgerContrapartida;
        $entryItem['amount'] = $data['value'];
        $entryItem['dc'] = "C";
        $entryItem['narration'] = (!$return ? 'Creación ' : 'Anulación ')."Contrapartida ".lang('gift_card')." ".$data['reference_no']." N° ".$data['card_no'];
        $entryItem['companies_id'] =  $data['customer_id'];
        $entryItem['base'] = 0;
        $entryItems[] = $entryItem;
        // Insertar los entry items
        foreach ($entryItems as $entryItem){
            $entryItem['entry_id'] = $entryId;
            if($entryItem['amount'] >= 0){
                if (isset($data['cost_center_id'])) {
                    $entryItem['cost_center_id'] = $data['cost_center_id'];
                }
                $this->insertWappsiEntryItem($entryItem, $contabilidadSufijo);
                if( $entryItem['dc'] == 'D' ){
                  $drTotal = $drTotal + $entryItem['amount'];
                }else if( $entryItem['dc'] == 'C' ){
                  $crTotal = $crTotal + $entryItem['amount'];
                }
            }
        }
        // Actualizar los totales
        $this->updateWappsiEntryTotals($entryId, $drTotal, $crTotal, $contabilidadSufijo);
        return TRUE;
      }
      return FALSE;
    }

    public function getUserById($id = NULL)
    {
        if ($id) {
            $q = $this->db->select('users.*')
            ->where('users.id', $id)
            ->get('users');
            if ($q->num_rows() > 0) {
                return $q->row();
            }
        }
        return FALSE;
    }

    public function getProductsByBrandOrCategory($discontinued=null, $brand=null, $category=null) {
        $this->db->where('type !=', 'service');
        if ($discontinued) {
            $this->db->where('discontinued', 0);
        }
        if ($brand) {
            $this->db->where('brand', $brand);
        }
        if ($category) {
            $this->db->where('category_id', $category);
        }
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }

    public function syncStoreProductsQuantityMovement($items, $movement_type)
    {
        $_errorMessage = '';
        $_message = '';
        $response = [];
        $this->load->integration_model('Product');
        $this->load->integration_model('ProductVariation');
        $shop_biller_default = $this->shop_settings->biller ? $this->getAllCompaniesWithState('biller', $this->shop_settings->biller) : false; //si se definió sucursal predeterminada, se obtiene la misma, caso contrario falso
        $shop_biller_default_warehouse_id = ($shop_biller_default && $this->shop_settings->show_product_quantity == 1) ? $shop_biller_default->default_warehouse_id : false; //si existe sucursal predeterminada y los ajustes de ecommerce está en cantidades solo de la bodega de la sucursal predeterminada, se obtiene el id de la bodega, caso contrario falso
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                foreach ($integrations as $integration) { 
                    $Product = new Product();
                    $Product->open($integration);
                    $ProductVariation = new ProductVariation();
                    $ProductVariation->open($integration); 
                    foreach ($items as $item_data) {
                        $product_id = $item_data['product_id'];
                        $option_id = isset($item_data['option_id']) ? $item_data['option_id'] : NULL;
                        if (isset($item_data['type'])) {
                            $movement_type = $item_data['type'] == 'addition' ? 1 : 2;
                        }
                        $quantity = $item_data['quantity'] * ($movement_type == 1 ? 1 : -1);
                        $sync_to_store = (($shop_biller_default_warehouse_id && $shop_biller_default_warehouse_id == $item_data['warehouse_id']) || !$shop_biller_default_warehouse_id) ? true : false; //si existe id de bodega por defecto y el id de bodega del movimiento es igual a la por defecto OR si la variable de bodega por defecto es falso (parametro ecomerce no está en solo de bodega predeterminada), se sincroniza, caso contrario no se sincroniza

                        // exit(var_dump($this->shop_settings->show_product_quantity)."  ".var_dump($shop_biller_default)."  ".var_dump($shop_biller_default_warehouse_id)."  ".var_dump($item_data['warehouse_id'])."  ".var_dump($sync_to_store)."  ");
                        if ($sync_to_store) {
                            $ProductStore = $Product->find(["id_wappsi" => $product_id]);
                            if ($ProductStore) {
                                $productStoreQty = $ProductStore->quantity;
                                $newProductStoreQty = $productStoreQty + $quantity;
                                $productStoreStock = ($newProductStoreQty >= $this->shop_settings->activate_available_from_quantity) ? 1 : 0;
                                $Product->update(['quantity'=>$newProductStoreQty, 'stock' => $productStoreStock, 'published' => $productStoreStock], $ProductStore->id);
                                if ($option_id) {
                                    $ProductVariationStore = $ProductVariation->find(["id_wappsi" => $option_id]);
                                    if ($ProductVariationStore) {
                                        $variationProductStoreQty = $ProductVariationStore->quantity;
                                        $newVariationProductStoreQty = $variationProductStoreQty + $quantity;
                                        $productVariationStoreStock = ($newVariationProductStoreQty >= $this->shop_settings->activate_available_from_quantity) ? 1 : 0;
                                        $ProductVariation->update(['quantity'=>$newVariationProductStoreQty, 'stock' => $productVariationStoreStock], $ProductVariationStore->id);
                                    } 
                                }
                            }    
                        }
                    }
                    $ProductVariation->close();
                    $Product->close();
                }
            }
        }
        return true;
    }
    public function check_customer_credit_limit($customer_id, $credit_amount){
        $customer = $this->getCompanyByID($customer_id);
        if ($this->Settings->control_customer_credit == 1 && $customer->customer_payment_type == 0) {
            $due_q = $this->db->select(' SUM(COALESCE(grand_total, 0) - COALESCE(paid, 0)) AS due_amount')
                ->where('customer_id', $customer_id)->get('sales');
            $due_balance = 0;
            if ($due_q->num_rows() > 0) {
                $due_q = $due_q->row();
                $due_balance = $due_q->due_amount;
            }
            $due_balance = $customer->customer_credit_limit - $due_balance;
            if ($credit_amount > $due_balance) {
                return false;
            }

            $due_term_expired = 0;
            $due_q = $this->db
                    ->where('(DATEDIFF(NOW(), date)) > '.$customer->customer_payment_term)
                    ->where('((COALESCE(grand_total, 0) - COALESCE(paid, 0))) > 0')
                    ->where('customer_id', $customer_id)
                    ->get('sales');
            if ($due_q->num_rows() > 0) {
                return false;
            }
        }
        return true;
    }

    public function get_Product_pending_order_quantity($product_id, $warehouse_id, $option_id = NULL){
        if ($option_id) {
            $sql = "SELECT 
                OSI.product_id,
                OSI.option_id,
                SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
            FROM sma_order_sale_items OSI
                INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
            WHERE 
                OSI.warehouse_id = {$warehouse_id} AND
                OSI.product_id = {$product_id} AND
                OSI.option_id = {$option_id}
            GROUP BY OSI.product_id, OSI.option_id";
        } else {$sql = "SELECT 
                OSI.product_id,
                SUM(COALESCE(quantity, 0) - COALESCE(quantity_delivered, 0)) as pending_quantity
            FROM sma_order_sale_items OSI
                INNER JOIN sma_order_sales OS ON OS.id = OSI.sale_id
            WHERE 
                OSI.warehouse_id = {$warehouse_id} AND
                OSI.product_id = {$product_id}
            GROUP BY OSI.product_id";
        }

        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            $q = $q->row();
            return $q->pending_quantity;
        }
        return 0;
    }
}