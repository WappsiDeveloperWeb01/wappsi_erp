<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('add_instance'); ?></h4>
        </div>
        <?= admin_form_open_multipart("system_settings/updateInstance/". $instance->id, ['data-toggle' => 'validator', 'role' => 'form']); ?>
        <div class="modal-body">
            <?php var_dump($instance); ?>
            <div class="form-group">
                <?= form_label($this->lang->line('name'), 'name'); ?>
                <?php $name = $instance->name; ?>
                <?= form_input(['name'=>'name', 'id'=>'name', 'class'=>'form-control', 'required'=>true], $name); ?>
            </div>

            <div class="form-group">
                <?= form_label($this->lang->line('security_token_bpm'), 'security_token'); ?>
                <?php $securityToken = $instance->security_token; ?>
                <?= form_input(['name'=>'security_token', 'id'=>'security_token', 'class'=>'form-control', 'type'=>'password', 'required'=>true], $securityToken); ?>
            </div>
        </div>
        <div class="modal-footer">
            <?= form_button(['name'=>'saveInstance', 'id'=>'saveInstance', 'type'=>'submit', 'class'=>'btn btn-primary new-button', 'data-toogle-second'=>'tooltip', 'title'=>$this->lang->line('save'), 'content'=>'<i class="fas fa-check"></i>']); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>
<script>
    $(document).ready(function () {
        $('[data-toogle-second="tooltip"]').tooltip();
    });
</script>