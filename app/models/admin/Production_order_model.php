<?php defined('BASEPATH') OR exit('No direct script access allowed');

#[\AllowDynamicProperties]
class Production_order_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->admin_model('purchases_model');
    }

    public function getSuggestions($term)
    { $limit = $this->Settings->max_num_results_display;
        $this->db->select($this->db->dbprefix('products') . '.id, 
                          code, 
                          ' . $this->db->dbprefix('products') . '.name as name, 
                          ' . $this->db->dbprefix('products') . '.avg_cost as price, 
                          ' . $this->db->dbprefix('products') . '.cost as cost, 
                          tax_rate, 
                          unit, 
                          type')
            ->where("(type = 'pfinished') AND "
                . "(" . $this->db->dbprefix('products') . ".name ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR code ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'")." OR
                concat(" . $this->db->dbprefix('products') . ".name, ' (', code, ')') ".($this->Settings->barcode_reader_exact_search == 0 ? "LIKE '%" . $term . "%'" : "= '" . $term . "'").")")
            ->limit($limit);
        $q = $this->db->get('products');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function addProductionOrder($data, $products)
    {
      $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
      if($referenceBiller){
          $reference = $referenceBiller;
      }
      $data['reference_no'] = $reference;
      $ref = explode("-", $reference);
      $consecutive = $ref[1];
      $por_detail_insert_id = [];
      if ($this->db->insert('production_order', $data)) {
          $production_order_id = $this->db->insert_id();
          $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
          $totalAdjustmenCost = 0;
          foreach ($products as $product) {
              $product['production_order_id'] = $production_order_id;
              if (!isset($product['production_order_pfinished_item_id'])) {
                $this->db->insert('production_order_detail', $product);
                $por_detail_insert_id[$product['product_id']] = $this->db->insert_id();
              } else {
                $cpfinished_pid = $product['production_order_pfinished_item_id'];
                if (isset($por_detail_insert_id[$cpfinished_pid])) {
                  $product['production_order_pfinished_item_id'] = $por_detail_insert_id[$cpfinished_pid];
                }
                $this->db->insert('production_order_detail', $product);
              }
          }
          return $production_order_id;
      }
      return false;
    }

    public function updateProductionOrder($data, $products)
    {
      $por_detail_insert_id = [];
      if ($this->db->update('production_order', $data, ['id'=>$data['id']])) {
          foreach ($products as $product) {
              if (!isset($product['production_order_pfinished_item_id'])) {
                $this->db->update('production_order_detail', $product, ['id' => $product['id']]);
                $por_detail_insert_id[$product['product_id']] = $product['id'];
              } else {
                $cpfinished_pid = $product['production_order_pfinished_item_id'];
                if (isset($por_detail_insert_id[$cpfinished_pid])) {
                  $product['production_order_pfinished_item_id'] = $por_detail_insert_id[$cpfinished_pid];
                } else {
                  // exit(var_dump($cpfinished_poption_id));
                }
                $this->db->update('production_order_detail', $product, ['id'=>$product['id']]);
              }
          }
          // $this->sma->print_arrays($products);
          return true;
      }
      return false;
    }

    public function get_production_order_by_id($id){
      $q = $this->db->select('
                              production_order.*,
                              companies.name as responsable, 
                            ')
                  ->join('companies', 'companies.id = production_order.employee_id')
                  ->get_where('production_order', ['production_order.id' => $id]);
      if ($q->num_rows() > 0) {
        return $q->row();
      }
      return false;
    }

    public function get_poritems($porid = NULL, $por_product_id = NULL){
      $this->db->select('
                          production_order_detail.*,
                          CONCAT('.$this->db->dbprefix('product_variants').'.name, '.$this->db->dbprefix('product_variants').'.suffix) as variant,
                          products.code as product_code,
                          products.name as product_name,
                          por_pfinished_data.name as pfinished_name,
                          por_pfinished_data.code as pfinished_code,
                          por_pfinished_data.id as pfinished_id,
                          por_pfinished.quantity as por_pfinished_qty,
                          CONCAT(por_pfinished_variant.name, por_pfinished_variant.suffix) as pfinished_variant,
                          products.type,
                          SUM(COALESCE('.$this->db->dbprefix("cutting_detail").'.cutting_quantity - '.$this->db->dbprefix("cutting_detail").'.fault_quantity, 0)) as in_process_quantity,
                          SUM(COALESCE('.$this->db->dbprefix("cutting_detail").'.finished_quantity, 0)) as finished_quantity,
                          (COALESCE(assemble_detail.assembling_quantity - assemble_detail.fault_quantity, 0)) as assembling_quantity,
                          (COALESCE(assemble_detail.finished_quantity, 0)) as assembling_finished_quantity,
                          (COALESCE(packing_detail.packing_quantity - packing_detail.fault_quantity, 0)) as packing_quantity,
                          (COALESCE(packing_detail.finished_quantity, 0)) as packing_finished_quantity,
                          (ciq_pfinished_complete_cutting.complete_cutting_quantity) AS pfinished_cutting_quantity,
                          (ciq_pfinished_complete_assemble.complete_assembling_quantity) AS pfinished_assembling_quantity,
                          (ciq_pfinished_complete_assemble.complete_assembling_quantity_completed) AS pfinished_assembling_finished_quantity,
                          (ciq_pfinished_complete_packing.complete_packing_quantity) AS pfinished_packing_quantity,
                          (ciq_pfinished_complete_packing.complete_packing_quantity_completed) AS pfinished_packing_finished_quantity,
                          ciq_combo_item.quantity as combo_item_qty
                        ', FALSE)
              ->join('products', 'products.id = production_order_detail.product_id')
              ->join('production_order_detail as por_pfinished', 'por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
              ->join('products as por_pfinished_data', 'por_pfinished_data.id = por_pfinished.product_id', 'left')
              ->join('product_variants', 'product_variants.id = production_order_detail.option_id', 'left')
              ->join('product_variants as por_pfinished_variant', 'por_pfinished_variant.id = por_pfinished.option_id', 'left')
              ->join('combo_items as ciq_combo_item', 'ciq_combo_item.item_code = products.code AND ciq_combo_item.product_id = por_pfinished.product_id', 'left')
              ->join('cutting_detail', 'cutting_detail.production_order_pid = production_order_detail.id', 'left')
              ->join('(
                        SELECT  CD.production_order_pid, SUM(CD.assembling_quantity) as assembling_quantity, SUM(CD.fault_quantity) as fault_quantity, SUM(CD.finished_quantity) as finished_quantity FROM '.$this->db->dbprefix('assemble_detail').' CD
                        GROUP BY CD.production_order_pid
                      ) assemble_detail', 'assemble_detail.production_order_pid = production_order_detail.id', 'left')
              ->join('(
                        SELECT  CD.production_order_pid, SUM(CD.packing_quantity) as packing_quantity, SUM(CD.fault_quantity) as fault_quantity, SUM(CD.finished_quantity) as finished_quantity FROM '.$this->db->dbprefix('packing_detail').' CD
                        GROUP BY CD.production_order_pid
                      ) packing_detail', 'packing_detail.production_order_pid = production_order_detail.id', 'left')

              ->join("(SELECT 
                            production_order_pfinished_item_id,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(cutting_composition_complete_quantity)) AS complete_cutting_quantity,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.cutting_quantity) AS pieces_cutting_quantity,
                                (SUM(CD.cutting_quantity) / CI.quantity) AS cutting_composition_complete_quantity
                        FROM
                            {$this->db->dbprefix('cutting_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS cutting_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_cutting", "ciq_pfinished_complete_cutting.production_order_pfinished_item_id = production_order_detail.id", "left")
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity)) AS complete_assembling_quantity,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity_completed)) AS complete_assembling_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_assembling_quantity,
                                (SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS assemble_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_assembling_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS assemble_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS assemble_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_assemble", "ciq_pfinished_complete_assemble.production_order_pfinished_item_id = production_order_detail.id", "left")
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            MIN(packing_composition_complete_quantity) AS complete_packing_quantity,
                            MIN(packing_composition_complete_quantity_completed) AS complete_packing_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_packing_quantity,
                                (SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS packing_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_packing_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS packing_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('packing_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS packing_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_packing", "ciq_pfinished_complete_packing.production_order_pfinished_item_id = production_order_detail.id", "left")
            ->join("(SELECT 
                            pfinished_POD.id as pfinished_POD_id,
                            SUM(CD.cutting_quantity) as cutting_quantity,
                            COUNT( DISTINCT CD.product_id) AS cutting_comp_qty
                        FROM
                            {$this->db->dbprefix('cutting_detail')} CD
                                INNER JOIN
                            {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                                INNER JOIN
                            {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD_id
                        ) cutting", "cutting.pfinished_POD_id = production_order_detail.id", "left")
            ->join("(SELECT 
                            pfinished_POD.id as pfinished_POD_id,
                            SUM(CD.assembling_quantity) AS assembling_quantity,
                            SUM(CD.finished_quantity) AS finished_quantity,
                            SUM(CD.fault_quantity) AS fault_quantity,
                            COUNT( DISTINCT CD.product_id) AS assembling_comp_qty
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD.id) assemble", "assemble.pfinished_POD_id = production_order_detail.id", "left")
            ->join("(SELECT 
                            pfinished_POD.id as pfinished_POD_id,
                            SUM(CD.packing_quantity) AS packing_quantity,
                            SUM(CD.finished_quantity) AS finished_quantity,
                            SUM(CD.fault_quantity) AS fault_quantity,
                            COUNT( DISTINCT CD.product_id) AS packing_comp_qty
                        FROM
                            {$this->db->dbprefix('packing_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD.id) packing", "packing.pfinished_POD_id = production_order_detail.id", "left")
            ->group_by('production_order_detail.id');
      if ($porid) {
        $this->db->where('production_order_detail.production_order_id', $porid);
      }
      if ($por_product_id) {
        $this->db->where('production_order_detail.id', $por_product_id);
      }
      $q = $this->db->get('production_order_detail');

      if ($q->num_rows() > 0) {
        if ($por_product_id) {
          return $q->row();
        } else {
          $data = [];
          foreach (($q->result()) as $row) {
            if (empty($row->production_order_pfinished_item_id)) {
                $row->options = $this->get_cutting_variants($row->product_id);
                // $this->sma->print_arrays($row->options);
            } else {
                $row->options = $this->get_cutting_variants($row->pfinished_id, $row->id);
            }
            $data[$row->id] = $row;
          }
          return $data;
        }
      }
      return false;
    }
    /* DESDE AQUI */
    public function get_production_order_suggestions($term, $limit = 10, $POR_SUGGESTIONS_TYPE = POR_CUTTING_TYPE){

      $q = $this->db->select('id, reference_no as text')->where('status !=', 'completed')->where('(reference_no LIKE "%'.$term.'%")')->get('production_order');
      if ($q->num_rows() > 0) {
        $data = [];
        foreach (($q->result()) as $row) {
          if ($POR_SUGGESTIONS_TYPE == POR_CUTTING_TYPE) {
            $od_valid = $this->db->query('SELECT SUM(quantity), SUM(cutting_quantity) FROM (SELECT OD.quantity, SUM(CD.cutting_quantity) as cutting_quantity FROM '.$this->db->dbprefix('production_order_detail').' OD
                LEFT JOIN '.$this->db->dbprefix('cutting_detail').' CD ON CD.production_order_pid = OD.id
              WHERE OD.production_order_pfinished_item_id IS NOT NULL AND OD.production_order_id = '.$row->id.'
              GROUP BY OD.id) AS por_total
              HAVING SUM(quantity) = SUM(cutting_quantity)');
            if ($od_valid->num_rows() > 0) {
              continue;
            }
          } else if ($POR_SUGGESTIONS_TYPE == POR_ASSEMBLE_TYPE) {
            $od_valid = $this->db->query('SELECT SUM(quantity), SUM(assembling_quantity) FROM (SELECT OD.quantity, SUM(CD.assembling_quantity) as assembling_quantity FROM '.$this->db->dbprefix('production_order_detail').' OD
                LEFT JOIN '.$this->db->dbprefix('assemble_detail').' CD ON CD.production_order_pid = OD.id
              WHERE OD.production_order_pfinished_item_id IS NOT NULL AND OD.production_order_id = '.$row->id.'
              GROUP BY OD.id) AS por_total
              HAVING SUM(quantity) = SUM(assembling_quantity)');
            if ($od_valid->num_rows() > 0) {
              continue;
            }
          } else if ($POR_SUGGESTIONS_TYPE == POR_PACKING_TYPE) {
            $od_valid = $this->db->query('SELECT SUM(quantity), SUM(packing_quantity) FROM (SELECT OD.quantity, SUM(CD.packing_quantity) as packing_quantity FROM '.$this->db->dbprefix('production_order_detail').' OD
                LEFT JOIN '.$this->db->dbprefix('packing_detail').' CD ON CD.production_order_pid = OD.id
              WHERE OD.production_order_pfinished_item_id IS NOT NULL AND OD.production_order_id = '.$row->id.'
              GROUP BY OD.id) AS por_total
              HAVING SUM(quantity) = SUM(packing_quantity)');
            if ($od_valid->num_rows() > 0) {
              continue;
            }
          }
          $data[] = $row;
        }
        return $data;
      }
      return false;
    }

    public function get_cutting_order_suggestions($term, $limit = 10, $POR_SUGGESTIONS_TYPE = POR_ASSEMBLE_TYPE){

      $q = $this->db->select('cutting.id, cutting.id as text')
                    ->where('cutting.status', 'completed')
                    ->where('(cutting.id LIKE "%'.$term.'%")')
                    ->get('cutting');
      if ($q->num_rows() > 0) {
        $data = [];
        foreach (($q->result()) as $row) {
          if ($POR_SUGGESTIONS_TYPE == POR_ASSEMBLE_TYPE) {
            $cd_valid = $this->db->query('SELECT SUM(cutting_quantity), SUM(assembling_quantity) FROM '.$this->db->dbprefix('cutting_detail').' CD
                                INNER JOIN '.$this->db->dbprefix('assemble_detail').' AD ON AD.cutting_id_pid = CD.id
                              WHERE CD.cutting_id = '.$row->id.'
                              HAVING SUM(cutting_quantity) = SUM(assembling_quantity)');
            if ($cd_valid->num_rows() > 0) {
              continue;
            }
          } else if ($POR_SUGGESTIONS_TYPE == POR_PACKING_TYPE) {
            $cd_valid = $this->db->query('SELECT SUM(packing_quantity), SUM(assembling_quantity) FROM '.$this->db->dbprefix('cutting_detail').' CD
                                INNER JOIN '.$this->db->dbprefix('assemble_detail').' AD ON AD.cutting_id_pid = CD.id
                                INNER JOIN '.$this->db->dbprefix('packing_detail').' PD ON PD.assemble_id_pid = AD.id
                              WHERE CD.cutting_id = '.$row->id.'
                              HAVING SUM(packing_quantity) = SUM(assembling_quantity)');
            if ($cd_valid->num_rows() > 0) {
              continue;
            }
          }
          $data[] = $row;
        }
        return $data;
      }
      return false;
    }

    public function get_assemble_order_suggestions($term, $limit = 10, $POR_SUGGESTIONS_TYPE = POR_PACKING_TYPE){
      $q = $this->db->select('assemble.id, assemble.id as text')
                    ->where('assemble.status', 'completed')
                    ->where('(assemble.id LIKE "%'.$term.'%")')
                    ->get('assemble');
      if ($q->num_rows() > 0) {
        $data = [];
        foreach (($q->result()) as $row) {
          if ($POR_SUGGESTIONS_TYPE == POR_PACKING_TYPE) {
            $ad_valid = $this->db->query('SELECT SUM(assembling_quantity), SUM(packing_quantity) FROM '.$this->db->dbprefix('assemble_detail').' AD
                                INNER JOIN '.$this->db->dbprefix('packing_detail').' PD ON PD.assemble_id_pid = AD.id
                              WHERE AD.assemble_id = '.$row->id.'
                              HAVING SUM(assembling_quantity) = SUM(packing_quantity);');
            if ($ad_valid->num_rows() > 0) {
              continue;
            }
          }
          $data[] = $row;
        }
        return $data;
      }
      return false;
    }
    /* HASTA AQUI */

    public function addCuttingOrder($data, $products, $continue)
    {
      if ($this->db->insert('cutting', $data)) {
          $cutting_id = $this->db->insert_id();
          $this->db->update('production_order', ['status'=>'in_process'], ['id' => $data['production_order_id']]);
          foreach ($products as $product) {
              $product['cutting_id'] = $cutting_id;
              $this->db->insert('cutting_detail', $product);
              // if ($continue == 1) {
                $cutting_detail_id = $this->db->insert_id();
                unset($receipt_data);
                $receipt_data[] = [
                            'detail_id' => $cutting_detail_id,
                            'ok_quantity' => $product['cutting_quantity'],
                            'fault_quantity' => 0,
                            'created_by' => $this->session->userdata('user_id'),
                            'type' => POR_CUTTING_TYPE,
                          ];
                $this->production_order_model->add_receipt($receipt_data);
              // }
          }
          return $cutting_id;
      }
      return false;
    }

    public function get_cutting_order_by_id($id){
      $q = $this->db->select('
                              cutting.*,
                              companies.name as cutter_name, 
                              companies2.name as cutter_2_name,
                              companies3.name as tender_name,
                              companies4.name as tender_2_name,
                              companies5.name as packer_name,
                              companies6.name as packer_2_name,
                              companies7.name as embroiderer_name,
                              companies8.name as stamper_name
                            ')
                  ->join('companies', 'companies.id = cutting.employee_id')
                  ->join('companies companies2', 'companies2.id = cutting.employee_2_id', 'left')
                  ->join('companies companies3', 'companies3.id = cutting.tender_id', 'left')
                  ->join('companies companies4', 'companies4.id = cutting.tender_2_id', 'left')
                  ->join('companies companies5', 'companies5.id = cutting.packer_id', 'left')
                  ->join('companies companies6', 'companies6.id = cutting.packer_2_id', 'left')
                  ->join('companies companies7', 'companies7.id = cutting.embroiderer_id', 'left')
                  ->join('companies companies8', 'companies8.id = cutting.stamper_id', 'left')
                  ->get_where('cutting', ['cutting.id' => $id]);
      if ($q->num_rows() > 0) {
        return $q->row();
      }
      return false;
    }

    public function get_cuttingitems($id = NULL, $cutting_detail_id = NULL){
      $this->db->select('
                          cutting_detail.*,
                          products.code as product_code,
                          products.name as product_name,
                          products.type,
                          production_order_detail.product_id as por_pid,
                          production_order_detail.production_order_pfinished_item_id,
                          production_order_detail.production_order_id,
                          production_order_detail.quantity as order_quantity,
                          production_order_detail.option_id,
                          por_pfinished_data.name as pfinished_name,
                          por_pfinished_data.code as pfinished_code,
                          CONCAT(por_pfinished_variant.name, por_pfinished_variant.suffix) as pfinished_variant
                        ')
              ->join('production_order_detail', $this->db->dbprefix('production_order_detail').'.id = cutting_detail.production_order_pid')
              ->join('products', 'products.id = '.$this->db->dbprefix('production_order_detail').'.product_id')

              ->join('production_order_detail as por_pfinished', 'por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
              ->join('products as por_pfinished_data', 'por_pfinished_data.id = por_pfinished.product_id', 'left')
              ->join('product_variants as por_pfinished_variant', 'por_pfinished_variant.id = cutting_detail.option_id', 'left')


              ->group_by('cutting_detail.id');
      if ($id) {
        $this->db->where('cutting_detail.cutting_id', $id);
      }
      if ($cutting_detail_id) {
        $this->db->where('cutting_detail.id', $cutting_detail_id);
      }
      $q = $this->db->get('cutting_detail');
      if ($q->num_rows() > 0) {
        if ($cutting_detail_id) {
          return $q->row();
        } else {
          $data = [];
          foreach (($q->result()) as $row) {
            $data[] = $row;
          }
          return $data;
        }
      }
      return false;
    }


    public function add_receipt($data){
      $date = date('Y-m-d H:i:s');
      foreach ($data as $row) {
        $row['date'] = $date;
        if ($this->db->insert('production_order_receipts', $row)) {
          if ($row['type'] == POR_CUTTING_TYPE) {
            $cutting_detail = $this->get_cuttingitems(NULL, $row['detail_id']);
            if ($cutting_detail) {
              $this->db->update('cutting_detail', [
                                            'finished_quantity' => ($row['ok_quantity'] + $cutting_detail->finished_quantity),
                                            'fault_quantity' => ($row['fault_quantity'] + $cutting_detail->fault_quantity),
                                            'status' => (($row['ok_quantity'] + $row['fault_quantity'] + $cutting_detail->finished_quantity + $cutting_detail->fault_quantity) == $cutting_detail->cutting_quantity ? 'completed' : 'in_process')
                                                  ], ['id'=>$row['detail_id']]);
              $this->sync_cutting_order_status($cutting_detail->cutting_id);
            } else {
              exit('error');
            }
          } else if ($row['type'] == POR_ASSEMBLE_TYPE) {
            $assemble_detail = $this->get_assembleitems(NULL, $row['detail_id']);
            if ($assemble_detail) {
              $this->db->update('assemble_detail', [
                                            'finished_quantity' => ($row['ok_quantity'] + $assemble_detail->finished_quantity),
                                            'fault_quantity' => ($row['fault_quantity'] + $assemble_detail->fault_quantity),
                                            'status' => (($row['ok_quantity'] + $row['fault_quantity'] + $assemble_detail->finished_quantity + $assemble_detail->fault_quantity) == $assemble_detail->assembling_quantity ? 'completed' : 'in_process')
                                                  ], ['id'=>$row['detail_id']]);
              $this->sync_assemble_order_status($assemble_detail->assemble_id);
            } else {
              exit('error');
            }
          } else if ($row['type'] == POR_PACKING_TYPE) {
            $packing_detail = $this->get_packingitems(NULL, $row['detail_id']);
            if ($packing_detail) {
              $this->db->update('packing_detail', [
                                            'finished_quantity' => ($row['ok_quantity'] + $packing_detail->finished_quantity),
                                            'fault_quantity' => ($row['fault_quantity'] + $packing_detail->fault_quantity),
                                            'status' => (($row['ok_quantity'] + $row['fault_quantity'] + $packing_detail->finished_quantity + $packing_detail->fault_quantity) == $packing_detail->packing_quantity ? 'completed' : 'in_process')
                                                  ], ['id'=>$row['detail_id']]);
              $this->sync_packing_order_status($packing_detail->packing_id);
            } else {
              exit('error');
            }
          }
        }
      }
      return true;
    }

    public function sync_cutting_order_status($cutting_id){
      $q = $this->db->get_where('cutting_detail', ['cutting_id' => $cutting_id]);
      if ($q->num_rows() > 0) {
        $balance = 0;
        foreach (($q->result()) as $row) {
          $balance += ($row->cutting_quantity - $row->finished_quantity - $row->fault_quantity);
        }
        $this->db->update('cutting', ['status' => $balance == 0 ? 'completed' : 'in_process'], ['id'=>$cutting_id]);
      }
    }

    public function sync_assemble_order_status($assemble_id){
      $q = $this->db->get_where('assemble_detail', ['assemble_id' => $assemble_id]);
      if ($q->num_rows() > 0) {
        $balance = 0;
        foreach (($q->result()) as $row) {
          $balance += ($row->assembling_quantity - $row->finished_quantity - $row->fault_quantity);
        }
        $this->db->update('assemble', ['status' => $balance == 0 ? 'completed' : 'in_process'], ['id'=>$assemble_id]);
      }
    }

    public function sync_packing_order_status($packing_id){
      $q = $this->db->get_where('packing_detail', ['packing_id' => $packing_id]);
      if ($q->num_rows() > 0) {
        $balance = 0;
        foreach (($q->result()) as $row) {
          $balance += ($row->packing_quantity - $row->finished_quantity - $row->fault_quantity);
        }
        $this->db->update('packing', ['status' => $balance == 0 ? 'completed' : 'in_process'], ['id'=>$packing_id]);
      }
    }

    public function get_receipts($id = NULL, $type = POR_CUTTING_TYPE, $receipt_id = NULL){
      if ($type == POR_CUTTING_TYPE) {
        $this->db->select("production_order_receipts.*,
                                products.name as product_name,
                                products.code as product_code,
                                CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by,
                                por_pfinished_data.name as pfinished_name,
                                por_pfinished_data.code as pfinished_code,
                                CONCAT(por_pfinished_variant.name, por_pfinished_variant.suffix) as pfinished_variant
                              ")
              ->join('cutting_detail', 'cutting_detail.id = production_order_receipts.detail_id')
              ->join('products', 'cutting_detail.product_id = products.id')
              ->join('users', 'users.id=production_order_receipts.created_by', 'left')

              ->join('production_order_detail', 'production_order_detail.id = cutting_detail.production_order_pid', 'left')
              ->join('production_order_detail as por_pfinished', 'por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
              ->join('products as por_pfinished_data', 'por_pfinished_data.id  =  por_pfinished.product_id', 'left')
              ->join('product_variants as por_pfinished_variant', 'por_pfinished_variant.id  =  cutting_detail.option_id', 'left');
        if ($id) {
          $this->db->where('cutting_detail.cutting_id', $id);
        } else if ($receipt_id) {
          $this->db->where('production_order_receipts.id', $receipt_id);
        }
        $q = $this->db->where('production_order_receipts.type', POR_CUTTING_TYPE)
                      ->get('production_order_receipts');
        if ($q->num_rows() > 0) {
          foreach (($q->result()) as $row) {
            $data[] = $row;
          }
          return $data;
        }
      } else if ($type == POR_ASSEMBLE_TYPE) {  //separar cuando viene id de recibo
        $this->db->select("production_order_receipts.*,
                          products.name as product_name,
                          products.code as product_code,
                          por_pfinished_data.name as pfinished_name,
                          por_pfinished_data.code as pfinished_code,
                          CONCAT(por_pfinished_variant.name, por_pfinished_variant.suffix) as pfinished_variant,
                          CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by")
              ->join('assemble_detail', 'assemble_detail.id = production_order_receipts.detail_id')
              ->join('products', 'assemble_detail.product_id = products.id')
              ->join('users', 'users.id=production_order_receipts.created_by', 'left')
              ->join('production_order_detail', 'production_order_detail.id = assemble_detail.production_order_pid', 'left')
              ->join('production_order_detail as por_pfinished', 'por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
              ->join('products as por_pfinished_data', 'por_pfinished_data.id  =  por_pfinished.product_id', 'left')
              ->join('product_variants as por_pfinished_variant', 'por_pfinished_variant.id  =  assemble_detail.option_id', 'left');
        if ($id) {
          $this->db->where('assemble_detail.assemble_id', $id);
        } else if ($receipt_id) {
          $this->db->where('production_order_receipts.id', $receipt_id);
        }
        $q = $this->db->where('production_order_receipts.type', POR_ASSEMBLE_TYPE)
                      ->get('production_order_receipts');
        if ($q->num_rows() > 0) {
          foreach (($q->result()) as $row) {
            $data[] = $row;
          }
          return $data;
        }
      } else if ($type == POR_PACKING_TYPE) { 
        $this->db->select("production_order_receipts.*,
                          products.name as product_name,
                          products.code as product_code,
                          por_pfinished_data.name as pfinished_name,
                          por_pfinished_data.code as pfinished_code,
                          CONCAT(por_pfinished_variant.name, por_pfinished_variant.suffix) as pfinished_variant,
                          CONCAT({$this->db->dbprefix('users')}.first_name, ' ', {$this->db->dbprefix('users')}.last_name) as created_by")
              ->join('packing_detail', 'packing_detail.id = production_order_receipts.detail_id')
              ->join('products', 'packing_detail.product_id = products.id')
              ->join('users', 'users.id=production_order_receipts.created_by', 'left')
              ->join('production_order_detail', 'production_order_detail.id = packing_detail.production_order_pid', 'left')
              ->join('production_order_detail as por_pfinished', 'por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
              ->join('products as por_pfinished_data', 'por_pfinished_data.id  =  por_pfinished.product_id', 'left')
              ->join('product_variants as por_pfinished_variant', 'por_pfinished_variant.id  =  packing_detail.option_id', 'left');
        if ($id) {
          $this->db->where('packing_detail.packing_id', $id);
        } else if ($receipt_id) {
          $this->db->where('production_order_receipts.id', $receipt_id);
        }
        $q = $this->db->where('production_order_receipts.type', POR_PACKING_TYPE)
                      ->get('production_order_receipts');
        if ($q->num_rows() > 0) {
          foreach (($q->result()) as $row) {
            $data[] = $row;
          }
          return $data;
        }
      }
      return false;
    }

    public function get_cutting_items_for_assemble($cutting_id, $por_id, $cuttings_ids){
      $this->db->select('
                          cutting_detail.*,
                          '.
                          ($por_id || $cuttings_ids ?
                            'SUM('.$this->db->dbprefix("cutting_detail").'.cutting_quantity) AS cutting_quantity,
                          SUM('.$this->db->dbprefix("cutting_detail").'.fault_quantity) AS fault_quantity,
                          SUM('.$this->db->dbprefix("cutting_detail").'.finished_quantity) AS finished_quantity,
                          "NULL" as cutting_detail_id,
                          '
                            : 'cutting_detail.id as cutting_detail_id,')
                          .'
                          pfinished_data.id as por_product_id,
                          pfinished_data.name as por_product_name,
                          pfinished_data.code as por_product_code,
                          pfinished_data.type as por_product_type,
                          por_pfinished.quantity as por_product_quantity,
                          cutting_detail.option_id as por_option_id,
                          CONCAT('.$this->db->dbprefix('product_variants').'.name, '.$this->db->dbprefix('product_variants').'.suffix) as por_variant,
                          products.name as product_name,
                          products.code as product_code,
                          products.type,
                          SUM(assemble_detail.assemble_in_process_quantity) as assemble_in_process_quantity,
                          SUM(assemble_detail.assemble_finished_quantity) as assemble_finished_quantity,
                          production_order_detail.production_order_id,
                          production_order_detail.id as production_order_pid
                        ')
                ->join('products', 'products.id = cutting_detail.product_id', 'left')

                // ->join('assemble_detail', ($por_id || $cuttings_ids ? 'assemble_detail.production_order_pid = cutting_detail.production_order_pid' :'assemble_detail.cutting_id_pid = cutting_detail.id'), 'left')

                ->join('(
                        SELECT
                            production_order_pid,
                            cutting_id_pid,
                            product_id,
                            option_id,
                            SUM(assembling_quantity - fault_quantity) AS assemble_in_process_quantity,
                            SUM(finished_quantity) AS assemble_finished_quantity
                        FROM sma_assemble_detail
                            GROUP BY '.($por_id ? 'production_order_pid' : 'cutting_id_pid').', product_id, option_id
                        ) assemble_detail', ($por_id ? 'assemble_detail.production_order_pid = cutting_detail.production_order_pid' :'assemble_detail.cutting_id_pid = cutting_detail.id'), 'left')


                ->join('production_order_detail', 'production_order_detail.id = cutting_detail.production_order_pid', 'left')
                ->join('production_order_detail as por_pfinished', ' por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
                ->join('products as pfinished_data', ' pfinished_data.id = por_pfinished.product_id', 'left')
                ->join('product_variants', ' product_variants.id = cutting_detail.option_id', 'left');
      if ($por_id) {
        $this->db->where('production_order_detail.production_order_id', $por_id);
        $this->db->group_by('cutting_detail.product_id, cutting_detail.option_id');
      } else if ($cutting_id) {
        $this->db->where('cutting_detail.cutting_id', $cutting_id);
        $this->db->group_by('cutting_detail.id, cutting_detail.option_id');
      } else if ($cuttings_ids) {
        $cuttings_ids = explode(", ", $cuttings_ids);
        $this->db->where_in('cutting_detail.cutting_id', $cuttings_ids);
        $this->db->group_by('cutting_detail.product_id, cutting_detail.option_id');
      }
      $q = $this->db->get('cutting_detail');
      if ($q->num_rows() > 0) {
        foreach (($q->result()) as $row) {
          $data[] = $row;
        }
        return $data;
      }
      return false;
    }


    public function get_cutted_items_for_assemble($pfinished_id, $production_order_id, $option_id){
      $q = $this->db->select('
                          cutting_detail.product_id,
                          ( 
                            SUM('.$this->db->dbprefix("cutting_detail").'.finished_quantity) / 
                            (
                              por_composition.quantity / 
                              '.$this->db->dbprefix("production_order_detail").'.quantity
                              ) 
                          ) as individual_quantity_composition
                        ')
               ->join('production_order_detail as por_composition', 'por_composition.production_order_pfinished_item_id = production_order_detail.id')
               ->join('cutting_detail', 'cutting_detail.production_order_pid = por_composition.id')
               ->where('production_order_detail.product_id', $pfinished_id)
               ->where('cutting_detail.option_id', $option_id)
               ->where('production_order_detail.production_order_id', $production_order_id)
               ->group_by('cutting_detail.product_id')
               ->get('production_order_detail');
      if ($q->num_rows() > 0) {
        $menor = 0;
        foreach (($q->result()) as $row) {
          if ($menor == 0 || $row->individual_quantity_composition < $menor) {
            $menor = $row->individual_quantity_composition;
          }
        }
        return $menor;
      }
      return false;
    }

    public function get_assembled_items_for_packing($pfinished_id, $production_order_id, $option_id){
      $q = $this->db->select('
                          assemble_detail.product_id,
                          ( 
                            SUM('.$this->db->dbprefix("assemble_detail").'.finished_quantity) / 
                            (
                              por_composition.quantity / 
                              '.$this->db->dbprefix("production_order_detail").'.quantity
                              ) 
                          ) as individual_quantity_composition
                        ')
               ->join('production_order_detail as por_composition', 'por_composition.production_order_pfinished_item_id = production_order_detail.id', 'inner')
               ->join('cutting_detail', 'cutting_detail.production_order_pid = por_composition.id', 'left')
               ->join('assemble_detail', '('.$this->db->dbprefix("assemble_detail").'.cutting_id_pid = '.$this->db->dbprefix("cutting_detail").'.id) or ('.$this->db->dbprefix("assemble_detail").'.production_order_pid = por_composition.id AND '.$this->db->dbprefix("assemble_detail").'.cutting_id_pid IS NULL)', 'inner')
               ->where('production_order_detail.product_id', $pfinished_id)
               ->where('assemble_detail.option_id', $option_id)
               ->where('production_order_detail.production_order_id', $production_order_id)
               ->group_by('assemble_detail.product_id')
               ->get('production_order_detail');

      $citems = $this->site->getProductComboItems($pfinished_id);
      if ($q->num_rows() > 0 && count($citems) == $q->num_rows()) {
        $menor = 0;
        foreach (($q->result()) as $row) {
          if ($menor == 0 || $row->individual_quantity_composition < $menor) {
            $menor = $row->individual_quantity_composition;
          }
        }
        return $menor;
      }
      return false;
    }

    public function get_packed_items($pfinished_id, $production_order_id, $option_id){
      $q = $this->db->select('
                          packing_detail.product_id,
                          ( 
                            SUM('.$this->db->dbprefix("packing_detail").'.packing_quantity) / 
                            (
                              por_composition.quantity / 
                              '.$this->db->dbprefix("production_order_detail").'.quantity
                              ) 
                          ) as individual_quantity_composition
                        ')
               ->join('production_order_detail as por_composition', 'por_composition.production_order_pfinished_item_id = production_order_detail.id', 'inner')
               ->join('assemble_detail', 'assemble_detail.production_order_pid = por_composition.id', 'left')
               ->join('packing_detail', '('.$this->db->dbprefix("packing_detail").'.assemble_id_pid = '.$this->db->dbprefix("assemble_detail").'.id) or ('.$this->db->dbprefix("packing_detail").'.production_order_pid = por_composition.id AND '.$this->db->dbprefix("packing_detail").'.assemble_id_pid IS NULL)', 'inner')
               ->where('production_order_detail.product_id', $pfinished_id)
               ->where('packing_detail.option_id', $option_id)
               ->where('production_order_detail.production_order_id', $production_order_id)
               ->group_by('packing_detail.product_id')
               ->get('production_order_detail');

      $citems = $this->site->getProductComboItems($pfinished_id);
      if ($q->num_rows() > 0 && count($citems) == $q->num_rows()) {
        $menor = 0;
        foreach (($q->result()) as $row) {
          if ($menor == 0 || $row->individual_quantity_composition < $menor) {
            $menor = $row->individual_quantity_composition;
          }
        }
        return $menor;
      }
      return false;
    }

  public function addAssembleOrder($data, $products, $continue, $cuttings_ids)
  {    
    $new_products = [];
    if (!$data['cutting_id']) {
      foreach ($products as $product) {
            $this->db->select(
                        '
                        cutting_detail.id,
                        ('.$this->db->dbprefix("cutting_detail").'.cutting_quantity - SUM(COALESCE('.$this->db->dbprefix("assemble_detail").'.assembling_quantity, 0))) AS cutting_quantity
                        '
                    )
                   ->join('assemble_detail', 'assemble_detail.cutting_id_pid = cutting_detail.id', 'left')
                   ->where('cutting_detail.production_order_pid', $product['production_order_pid']);
            if ($cuttings_ids) {
                $this->db->where_in('cutting_detail.cutting_id', explode(", ", $cuttings_ids));
            }
            $cd = $this->db->where('cutting_detail.option_id', $product['option_id'])
                   ->group_by('cutting_detail.id')
                   ->get('cutting_detail');

          if ($cd->num_rows() > 0) {
            $old_product_assembling_qty = $product['assembling_quantity'];
            foreach (($cd->result()) as $cd_row) {
              if ($old_product_assembling_qty > 0 && $old_product_assembling_qty >= $cd_row->cutting_quantity) {
                $old_product_assembling_qty -= $cd_row->cutting_quantity;
                $product['assembling_quantity'] = $cd_row->cutting_quantity;
                $product['cutting_id_pid'] = $cd_row->id;
                $new_products[] = $product;
              } else if ($old_product_assembling_qty > 0 && $old_product_assembling_qty < $cd_row->cutting_quantity) {
                $product['assembling_quantity'] = $old_product_assembling_qty;
                $product['cutting_id_pid'] = $cd_row->id;
                $new_products[] = $product;
                $old_product_assembling_qty = 0;
                break;
              }
            }
          }
        }
      if (count($new_products) > 0) {
        $products = $new_products;
      } else {
        return false;
      }
    }
    // $this->sma->print_arrays($products);
    if ($this->db->insert('assemble', $data)) {
      $assemble_id = $this->db->insert_id();
      foreach ($products as $product) {
        $product['assemble_id'] = $assemble_id;
        $this->db->insert('assemble_detail', $product);
        if ($continue == 1) {
          $assemble_detail_id = $this->db->insert_id();
          unset($receipt_data);
          $receipt_data[] = [
                      'detail_id' => $assemble_detail_id,
                      'ok_quantity' => $product['assembling_quantity'],
                      'fault_quantity' => 0,
                      'created_by' => $this->session->userdata('user_id'),
                      'type' => POR_ASSEMBLE_TYPE,
                    ];
          $this->production_order_model->add_receipt($receipt_data);
        }
      }
      return $assemble_id;
    }
    return false;
  }

  public function get_assemble_order_by_id($id){
    $q = $this->db->select('assemble.*, companies.name as assembler_name')
                  ->where('assemble.id', $id)
                  ->join('companies', 'companies.id = assemble.supplier_id', 'inner')
                  ->get('assemble')
                  ;
    if ($q->num_rows() > 0) {
      return $q->row();
    }
    return false;
  }

  public function get_assembleitems($id = NULL, $assemble_detail_id = NULL){
    $this->db->select('
                        assemble_detail.*,
                        products.code as product_code,
                        products.name as product_name,
                        products.type,
                        por_pfinished_data.name as pfinished_name,
                        por_pfinished_data.code as pfinished_code,
                        CONCAT(por_pfinished_variant.name, por_pfinished_variant.suffix) as pfinished_variant,
                        production_order_detail.product_id as por_pid,
                        production_order_detail.production_order_pfinished_item_id,
                        production_order_detail.production_order_id,
                        production_order_detail.quantity as order_quantity,
                        assemble_detail.option_id
                      ')
            ->join('cutting_detail', 'cutting_detail.id = assemble_detail.cutting_id_pid', 'left')
            ->join('production_order_detail', 'production_order_detail.id = (IF('.$this->db->dbprefix('cutting_detail').'.production_order_pid IS NOT NULL, '.$this->db->dbprefix('cutting_detail').'.production_order_pid , '.$this->db->dbprefix('assemble_detail').'.production_order_pid)) ', 'left')
            ->join('products', 'products.id = '.$this->db->dbprefix('production_order_detail').'.product_id', 'left')

            ->join('production_order_detail as por_pfinished', 'por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
            ->join('products as por_pfinished_data', 'por_pfinished_data.id = por_pfinished.product_id', 'left')
            ->join('product_variants as por_pfinished_variant', 'por_pfinished_variant.id = assemble_detail.option_id', 'left')
            ->group_by('assemble_detail.id');
    if ($id) {
      $this->db->where('assemble_detail.assemble_id', $id);
    }
    if ($assemble_detail_id) {
      $this->db->where('assemble_detail.id', $assemble_detail_id);
    }
    $q = $this->db->get('assemble_detail');
    if ($q->num_rows() > 0) {
      if ($assemble_detail_id) {
        return $q->row();
      } else {
        $data = [];
        foreach (($q->result()) as $row) {
          $data[] = $row;
        }
        return $data;
      }
    }
    return false;
  }

  public function get_assemble_items_for_packing($assemble_id, $por_id, $cutting_id, $assembles_id){
      $this->db->select('
                          assemble_detail.*,
                          '.
                          ($por_id || $cutting_id || $assembles_id ?
                            'SUM('.$this->db->dbprefix("assemble_detail").'.assembling_quantity) AS assembling_quantity,
                          SUM('.$this->db->dbprefix("assemble_detail").'.fault_quantity) AS fault_quantity,
                          SUM('.$this->db->dbprefix("assemble_detail").'.finished_quantity) AS finished_quantity,
                          "NULL" as assemble_detail_id,
                          '
                            : 'assemble_detail.id as assemble_detail_id,')
                          .'
                          pfinished_data.id as por_product_id,
                          pfinished_data.name as por_product_name,
                          pfinished_data.code as por_product_code,
                          pfinished_data.type as por_product_type,
                          por_pfinished.id as production_order_pfinished_item_id,
                          por_pfinished.quantity as por_product_quantity,
                          assemble_detail.option_id as por_option_id,
                          CONCAT('.$this->db->dbprefix('product_variants').'.name, '.$this->db->dbprefix('product_variants').'.suffix) as por_variant,
                          products.name as product_name,
                          products.code as product_code,
                          products.type,
                          SUM(COALESCE('.$this->db->dbprefix("packing_detail").'.packing_quantity - '.$this->db->dbprefix("packing_detail").'.fault_quantity, 0)) as packing_in_process_quantity,
                          SUM(COALESCE('.$this->db->dbprefix("packing_detail").'.finished_quantity, 0)) as packing_finished_quantity,
                          production_order_detail.production_order_id,
                          assemble_detail.assemble_id,
                          cutting_detail.cutting_id,
                          production_order.biller_id,
                          production_order.warehouse_id,
                          production_order_detail.id as production_order_pid,
                          por_pfinished.product_id as pfinished_product_id,
                          por_pfinished.option_id as pfinished_option_id,
                          combo_items.quantity as composition_quantity,
                          ciq_pfinished.ciq_num
                        ')
                ->join('products', 'products.id = assemble_detail.product_id', 'left')
                ->join('packing_detail', 'packing_detail.assemble_id_pid = assemble_detail.id', 'left')
                ->join('cutting_detail', $this->db->dbprefix('cutting_detail').'.id = '.$this->db->dbprefix('assemble_detail').'.cutting_id_pid', 'left')
                ->join('production_order_detail', 'production_order_detail.id = (IF('.$this->db->dbprefix('cutting_detail').'.production_order_pid IS NOT NULL, '.$this->db->dbprefix('cutting_detail').'.production_order_pid , '.$this->db->dbprefix('assemble_detail').'.production_order_pid))', 'left')
                ->join('production_order', 'production_order.id = production_order_detail.production_order_id', 'left')
                ->join('production_order_detail as por_pfinished', ' por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
                ->join('products as pfinished_data', ' pfinished_data.id = por_pfinished.product_id', 'left')
                ->join('combo_items', 'combo_items.item_code = products.code AND combo_items.product_id = por_pfinished.product_id')
                ->join('(
                        SELECT product_id, COUNT(id) as ciq_num FROM '.$this->db->dbprefix('combo_items').' GROUP BY product_id
                        ) as ciq_pfinished', 'ciq_pfinished.product_id = por_pfinished.product_id')
                ->join('product_variants', ' product_variants.id = assemble_detail.option_id', 'left');
      if ($por_id) {
        $this->db->where('production_order_detail.production_order_id', $por_id);
        $this->db->group_by('assemble_detail.product_id, , assemble_detail.option_id');
      } else if ($cutting_id) {
        $this->db->where('cutting_detail.cutting_id', $cutting_id);
        $this->db->group_by('cutting_detail.product_id, cutting_detail.option_id');
      } else if ($assemble_id) {
        $this->db->where('assemble_detail.assemble_id', $assemble_id);
        $this->db->group_by('assemble_detail.id, assemble_detail.option_id');
      } else if ($assembles_id) {
        $this->db->where_in('assemble_detail.assemble_id', explode(', ', $assembles_id));
        $this->db->group_by('assemble_detail.product_id, assemble_detail.option_id');
      }
      $q = $this->db->get('assemble_detail');
      if ($q->num_rows() > 0) {
        foreach (($q->result()) as $row) {
          $data[] = $row;
        }
        return $data;
      }
      return false;
    }

  public function addPackingOrder($data, $products, $continue, $assembles_ids)
  {
    $new_products = [];
    if (!$data['assemble_id']) {
      foreach ($products as $product) {
          $this->db->select(
                        '
                        assemble_detail.id,
                        ('.$this->db->dbprefix("assemble_detail").'.assembling_quantity - SUM(COALESCE('.$this->db->dbprefix("packing_detail").'.packing_quantity, 0))) AS assembling_quantity
                        '
                    )
                   ->join('packing_detail', 'packing_detail.assemble_id_pid = assemble_detail.id', 'left')
                   ->where('assemble_detail.production_order_pid', $product['production_order_pid']);
            if ($assembles_ids) {
                $this->db->where_in('assemble_detail.assemble_id', explode(", ", $assembles_ids));
            }
            $cd = $this->db->where('assemble_detail.option_id', $product['option_id'])
                   ->group_by('assemble_detail.id')
                   ->get('assemble_detail');

          if ($cd->num_rows() > 0) {
            $old_product_packing_qty = $product['packing_quantity'];
            foreach (($cd->result()) as $cd_row) {
              if ($old_product_packing_qty > 0 && $old_product_packing_qty >= $cd_row->assembling_quantity) {
                $old_product_packing_qty -= $cd_row->assembling_quantity;
                $product['packing_quantity'] = $cd_row->assembling_quantity;
                $product['assemble_id_pid'] = $cd_row->id;
                $new_products[] = $product;
              } else if ($old_product_packing_qty > 0 && $old_product_packing_qty < $cd_row->assembling_quantity) {
                $product['packing_quantity'] = $old_product_packing_qty;
                $product['assemble_id_pid'] = $cd_row->id;
                $new_products[] = $product;
                $old_product_packing_qty = 0;
                break;
              }
            }
          }
        }
      if (count($new_products) > 0) {
        $products = $new_products;
      } else {
        return false;
      }
    }
    // $this->sma->print_arrays($products);
    if ($this->db->insert('packing', $data)) {
        $packing_id = $this->db->insert_id();
        foreach ($products as $product) {
          $product['packing_id'] = $packing_id;
          $this->db->insert('packing_detail', $product);
          if ($continue == 1) {
            $packing_detail_id = $this->db->insert_id();
            unset($receipt_data);
            $receipt_data[] = [
                        'detail_id' => $packing_detail_id,
                        'ok_quantity' => $product['packing_quantity'],
                        'packing_warehouse_id' => $data['warehouse_id'],
                        'fault_quantity' => 0,
                        'created_by' => $this->session->userdata('user_id'),
                        'type' => POR_PACKING_TYPE,
                      ];
            $this->production_order_model->add_receipt($receipt_data);
          }
        }
        return $packing_id;
    }
    return false;
  }

  public function get_packingitems($id = NULL, $packing_detail_id = NULL){
    $this->db->select('
                        packing_detail.*,
                        products.code as product_code,
                        products.name as product_name,
                        products.type,
                        por_pfinished_data.name as pfinished_name,
                        por_pfinished_data.code as pfinished_code,
                        CONCAT(por_pfinished_variant.name, por_pfinished_variant.suffix) as pfinished_variant,
                        production_order_detail.product_id as por_pid,
                        production_order_detail.production_order_pfinished_item_id,
                        production_order_detail.production_order_id,
                        production_order_detail.quantity as order_quantity,
                        por_pfinished.product_id as pfinished_product_id,
                        por_pfinished.option_id as pfinished_option_id,
                        combo_items.quantity as composition_quantity
                      ')
            ->join('assemble_detail', 'assemble_detail.id = packing_detail.assemble_id_pid', 'left')
            ->join('cutting_detail', 'cutting_detail.id = assemble_detail.cutting_id_pid', 'left')
            ->join('production_order_detail', '('.$this->db->dbprefix('production_order_detail').'.id = '.$this->db->dbprefix('cutting_detail').'.production_order_pid) or ('.$this->db->dbprefix('production_order_detail').'.id = '.$this->db->dbprefix('packing_detail').'.production_order_pid)')
            ->join('products', 'products.id = '.$this->db->dbprefix('production_order_detail').'.product_id')

            ->join('production_order_detail as por_pfinished', 'por_pfinished.id = production_order_detail.production_order_pfinished_item_id', 'left')
            ->join('products as por_pfinished_data', 'por_pfinished_data.id = por_pfinished.product_id', 'left')
            ->join('product_variants as por_pfinished_variant', 'por_pfinished_variant.id = packing_detail.option_id', 'left')
            ->join('combo_items', 'combo_items.item_code = products.code AND combo_items.product_id = por_pfinished.product_id')
            ->group_by('packing_detail.id');
    if ($id) {
      $this->db->where('packing_detail.packing_id', $id);
    }
    if ($packing_detail_id) {
      $this->db->where('packing_detail.id', $packing_detail_id);
    }
    $q = $this->db->get('packing_detail');
    if ($q->num_rows() > 0) {
      if ($packing_detail_id) {
        return $q->row();
      } else {
        $data = [];
        foreach (($q->result()) as $row) {
          $data[] = $row;
        }
        return $data;
      }
    }
    return false;
  }

  public function get_packing_order_by_id($id){
    $q = $this->db->select('packing.*, assemble.cutting_id, companies.name as packer_name')
                  ->where('packing.id', $id)
                  ->join('companies', 'companies.id = packing.supplier_id', 'inner')
                  ->join('assemble', 'assemble.id = packing.assemble_id', 'left')
                  ->get('packing')
                  ;
    if ($q->num_rows() > 0) {
      return $q->row();
    }
    return false;
  }


  public function syncProductionOrderStatus($reference_no){
    $q = $this->db->select('
                            IF('.$this->db->dbprefix("production_order_detail").'.quantity = SUM('.$this->db->dbprefix("packing_detail").'.finished_quantity), "completed", "in_process") as item_status
                          ')
            ->join('packing_detail', 'packing_detail.production_order_pid = production_order_detail.id')
            ->join('production_order', 'production_order.id = production_order_detail.production_order_id')
            ->where('production_order.reference_no', $reference_no)
            ->where('production_order_detail.production_order_pfinished_item_id IS NOT NULL')
            ->group_by('production_order_detail.id')
            ->get('production_order_detail');
    $por_status = 'in_process';
    if ($q->num_rows() > 0) {
      $completed_items = 0;
      foreach (($q->result()) as $row) {
        if ($row->item_status == 'completed') {
          $completed_items++;
        }
      }
      if ($completed_items == $q->num_rows()) {
        $por_status = 'completed';
      }
    }
    $this->db->update('production_order', ['status'=>$por_status], ['reference_no' => $reference_no]);
  }

  public function get_last_employee_order(){
    $q = $this->db->order_by('id', 'desc')->get('production_order', 1);
    if ($q->num_rows() > 0) {
        return $q->row();
    }
    return false;
  }


  public function get_cutting_variants($product_id, $production_order_pid = NULL){
    $this->db->select('
                        product_variants.*,
                        SUM('.$this->db->dbprefix('cutting_detail').'.cutting_quantity) as cutting_quantity,
                        SUM('.$this->db->dbprefix('cutting_detail').'.finished_quantity) as finished_quantity,
                        SUM('.$this->db->dbprefix('cutting_detail').'.fault_quantity) as fault_quantity
                    ')
            ->join('cutting_detail', 'cutting_detail.option_id = product_variants.id '.($production_order_pid ? ' AND cutting_detail.production_order_pid = '.$production_order_pid : ''), 'left');
    $q = $this->db->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id')
            ->get('product_variants');
    if ($q->num_rows() > 0) {
        foreach (($q->result()) as $row) {
            $data[] = $row;
        }
        return $data;
    }
    return false;
  }

  public function get_notification_by_id($notification_id){
    $q = $this->db->get_where('production_order_notices', ['id' => $notification_id]);
    if ($q->num_rows() > 0) {
        return $q->row();
    }
    return false;
  }

  public function add_notification($data) {
    if ($this->db->insert('production_order_notices', $data)) {
        return true;
    }
    return false;
  }

  public function update_notification($notification_id, $data) {
    if ($this->db->update('production_order_notices', $data, ['id' => $notification_id])) {
        return true;
    }
    return false;
  }

}