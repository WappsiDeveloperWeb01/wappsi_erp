<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Budget extends MY_Controller
{
    public $months = [];

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('budget', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('Budget_model');

        $this->months =  json_decode(json_encode([
            ['id' => 1, 'name' => "Enero"],
            ['id' => 2, 'name' => "Febrero"],
            ['id' => 3, 'name' => "Marzo"],
            ['id' => 4, 'name' => "Abril"],
            ['id' => 5, 'name' => "Mayo"],
            ['id' => 6, 'name' => "Junio"],
            ['id' => 7, 'name' => "Julio"],
            ['id' => 8, 'name' => "Agosto"],
            ['id' => 9, 'name' => "Septiembre"],
            ['id' => 10, 'name' => "Octubre"],
            ['id' => 11, 'name' => "Noviembre"],
            ['id' => 12, 'name' => "Diciembre"]
        ]));
    }

    public function index()
    {
        $this->sma->checkPermissions();

        $data = [
            'year' => ($this->input->post('year')) ? $this->input->post('year') : (($this->session->userdata('year')) ? $this->session->userdata('year') : date('Y')),
            'month' => ($this->input->post('month')) ? $this->input->post('month') : (($this->session->userdata('month')) ? $this->session->userdata('month') : date('n'))
        ];

        $this->data['budgets'] = $this->Budget_model->getBudgets($data);
        $this->data['sumByCategories'] = $this->Budget_model->getSumByCategories($data);
        $this->data['sumBySellers'] = $this->Budget_model->getSumBySellers($data);
        $this->data['sumByBranches'] = $this->Budget_model->getSumByBranches($data);
        $this->data['months'] = $this->months;

        $this->page_construct('sales/budget/index', ["page_title" => $this->lang->line("budget")], $this->data);
    }

    public function getDataTables()
    {
        $this->load->library('datatables');

        $this->datatables->select("sales_budget.id AS id, biller.name AS billerName, seller.name AS sellerName, categories.name AS categoryName, sales_budget.amount, sales_budget.units");
        $this->datatables->join('companies AS biller', 'biller.id = sales_budget.biller_id', 'left');
        $this->datatables->join('companies AS seller', 'seller.id = sales_budget.seller_id', 'left');
        $this->datatables->join('categories', 'categories.id=sales_budget.category_id', 'left');
        $this->datatables->where('sales_budget.year', $this->input->post('year'));
        $this->datatables->where('sales_budget.month', $this->input->post('month'));
        $this->datatables->from("sales_budget");

        $action = '<a href="'.admin_url("budget/edit/$1/{$this->input->post('year')}/{$this->input->post('month')}").'" class="btn btn-primary new-button new-button-sm editBudgetButton" data-toggle-second="tooltip" data-placement="top" title="Editar" data-toggle="modal" data-target="#myModal" budget_id="$1"><i class="fas fa-pencil"></i></a>
        <button class="btn btn-danger new-button new-button-sm deleteBudgetButton" data-toggle-second="tooltip" data-placement="top" title="Eliminar" budgetId="$1"><i class="fas fa-trash"></i></button>';

        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function add($year, $month)
    {
        $this->sma->checkPermissions();

        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['categories'] = $this->site->getAllCategories();

        $this->load_view($this->theme . 'sales/budget/add', $this->data);
    }

    public function save()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('amount', $this->lang->line('amount'), 'required');

        if ($this->form_validation->run()) {
            $this->validations();

            $amount = $this->sma->formatNumberMask($this->input->post("amount"));
            $_POST['amount'] = $amount;

            if ($this->input->post("units")) {
                $units = $this->sma->formatNumberMask($this->input->post("units"));
                $_POST['units'] = $units;
            }

            if ($this->Budget_model->insert($this->input->post())) {
                $this->session->set_flashdata('message', lang("Presupuesto agregado"));
            } else {
                $this->session->set_flashdata('message', lang("El Presupuesto no pudo ser agregado"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        $this->session->set_userdata('year', $this->input->post('year'));
        $this->session->set_userdata('month', $this->input->post('month'));

        admin_redirect("budget");
    }

    public function edit($id, $year, $month)
    {
        $this->sma->checkPermissions();

        $budget =  $this->Budget_model->getBudget(['id'=>$id]);

        $this->data['year'] = $year;
        $this->data['month'] = $month;
        $this->data['budget'] = $budget;
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['sellers'] = json_decode(json_encode($this->site->getSellerByBiller($budget->biller_id)));
        $this->data['categories'] = $this->site->getAllCategories();

        $this->load_view($this->theme . 'sales/budget/edit', $this->data);
    }

    public function update()
    {
        $this->sma->checkPermissions();

        $this->form_validation->set_rules('amount', $this->lang->line('amount'), 'required');

        if ($this->form_validation->run()) {
            $this->validations($this->input->post('id'));

            if ($this->Budget_model->update($this->input->post())) {
                $this->session->set_flashdata('message', lang("Presupuesto actualizado"));
            } else {
                $this->session->set_flashdata('message', lang("El Presupuesto no pudo ser actualizado"));
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
        }

        admin_redirect("budget");
    }

    private function validations($id = NULL)
    {
        $this->validateBudgetIfExists($id);
        $this->validateBudgetByBranchIfExists($id);

        if ($this->Settings->detailed_budget_by_seller == YES) {
            $this->validateBudgetBySellersIfExists($id);
        }
        if ($this->Settings->detailed_budget_by_seller == YES && $this->Settings->detailed_budget_by_category == YES) {
            $this->validateBudgetByCategoriesIfExists($id);
        }
    }

    private function validateBudgetIfExists($id = NULL)
    {
        $data = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'biller_id' => $this->input->post('biller_id')
        ];

        if ($this->Settings->detailed_budget_by_seller == YES) {
            $data['seller_id'] = $this->input->post('seller_id');
        }

        if ($this->Settings->detailed_budget_by_seller == YES && $this->Settings->detailed_budget_by_category == YES) {
            $data['category_id'] = $this->input->post('category_id');
        }

        $budget = $this->Budget_model->getBudget($data);
        if (!empty($budget)) {
            if (!empty($id)) {
                if ($budget->id != $id) {
                    $this->session->set_flashdata('error', lang("Ya existe un presupuesto con los datos ingresados"));
                    admin_redirect("budget");
                }
            } else {
                $this->session->set_flashdata('error', lang("Ya existe un presupuesto con los datos ingresados"));
                admin_redirect("budget");
            }
        }
    }

    private function validateBudgetByBranchIfExists($id = NULL)
    {
        $validate = FALSE;
        $isDetailed = ($this->input->post('biller_id')) ? FALSE : TRUE;
        $data = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month')
        ];

        $budget = $this->Budget_model->getBudgetByAllBranch($data, $isDetailed);
        if (!empty($budget)) {
            if (!empty($id)) {
                if ($budget->id != $id) {
                    $validate = TRUE;
                }
            } else {
                $validate = TRUE;
            }
        }

        if ($validate) {
            if ($isDetailed) {
                $message = "No es posible agregar presupuesto general por sucursal debido a que ya existe un presupuesto detallado por sucursal";
            } else {
                $message = "No es posible agregar presupuesto detallado por sucursal debido a que ya existe un presupuesto general por sucursal";
            }

            $this->session->set_flashdata('error', lang($message));
            admin_redirect("budget");
        }
    }

    private function validateBudgetBySellersIfExists($id = NULL)
    {
        $validate = FALSE;
        $isDetailed = $this->input->post('seller_id') ? FALSE : TRUE;
        $data = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'biller_id' => $this->input->post('biller_id')
        ];

        $budget = $this->Budget_model->getBudgetByAllSellers($data, $isDetailed);
        if (!empty($budget)) {
            if (!empty($id)) {
                if ($budget->id != $id) {
                    $validate = TRUE;
                }
            } else {
                $validate = TRUE;
            }
        }

        if ($validate) {
            if ($isDetailed) {
                $message = "No es posible agregar presupuesto general para vendedores de la sucursal debido a que ya existe un presupuesto detallado por vendedores de la sucursal seleccionada";
            } else {
                $message = "No es posible agregar presupuesto detallado por vendedores de la sucursal debido a que ya existe un presupuesto general para vendedores de la sucursal seleccionada";
            }

            $this->session->set_flashdata('error', lang($message));
            admin_redirect("budget");
        }
    }

    private function validateBudgetByCategoriesIfExists($id = NULL)
    {
        $validate = FALSE;
        $isDetailed = $this->input->post('category_id') ? FALSE : TRUE;
        $data = [
            'year' => $this->input->post('year'),
            'month' => $this->input->post('month'),
            'biller_id' => $this->input->post('biller_id'),
            'seller_id' => $this->input->post('seller_id')
        ];

        $budget = $this->Budget_model->getBudgetByAllCategories($data, $isDetailed);
        if (!empty($budget)) {
            if (!empty($id)) {
                if ($budget->id != $id) {
                    $validate = TRUE;
                }
            } else {
                $validate = TRUE;
            }
        }

        if ($validate) {
            if ($isDetailed) {
                $message = "No es posible agregar presupuesto general para categorias de productos del vendedor debido a que ya existe un presupuesto detallado por categorias de productos del vendedor seleccionado";
            } else {
                $message = "No es posible agregar presupuesto detallado por categorias de productos del vendedor debido a que ya existe un presupuesto general por categorías de productos del vendedor seleccionado";
            }

            $this->session->set_flashdata('error', lang($message));
            admin_redirect("budget");
        }
    }

    public function getButget()
    {
        $budget = $this->Budget_model->getBudget($this->input->post('budgetId'));
        echo json_encode($budget);
    }

    public function getSeller()
    {
        $sellers = $this->site->getSellerByBiller($this->input->post('billerId'));
        echo json_encode($sellers);
    }

    public function delete($id, $year, $month)
    {
        $budget = $this->Budget_model->getBudget(['id'=>$id]);

        if (!empty($budget)) {
            if ($this->Budget_model->delete($id)) {
                $this->session->set_flashdata('message', lang("Presupuesto eliminado"));
            } else {
                $this->session->set_flashdata('error', lang("El Presupuesto no pudo ser eliminado"));
            }
        }

        $this->session->set_userdata('year', $year);
        $this->session->set_userdata('month', $month);
        admin_redirect("budget");
    }

    public function doubleBudget($year, $month)
    {
        $this->data['months'] = $this->months;
        $this->data['month'] = $month;
        $this->data['year'] = $year;

        $this->load_view($this->theme . 'sales/budget/doubleBudget', $this->data);
    }

    public function saveDoubleBudget()
    {
        $errorMessage = 0;
        $this->getBudgetIfExists();

        $data = ['year'=>$this->input->post('year'), 'month'=>$this->input->post('month')];
        $budgets = $this->Budget_model->getBudgets($data);
        if (!empty($budgets)) {
            foreach ($budgets as $budget) {
                unset($budget->id, $budget->year, $budget->month);
                $budget->year = $this->input->post('yearToDouble');
                $budget->month = $this->input->post('monthToDouble');
                if ($this->Budget_model->insert($budget) == FALSE) {
                    $errorMessage ++;
                }
            }
        }

        if ($errorMessage > 0) {
            $this->session->set_flashdata('error', lang("[$errorMessage] No fueron duplicados"));
        } else {
            $this->session->set_flashdata('message', lang("Presupuestos duplicado correctamente"));
        }

        admin_redirect("budget");
    }

    public function getBudgetIfExists()
    {
        $data = [
            'year'=>$this->input->post('yearToDouble'),
            'month'=>$this->input->post('monthToDouble'),
        ];

        $budgets = $this->Budget_model->getBudgets($data);
        if (!empty($budgets)) {
            $this->session->set_flashdata('error', lang("Ya existe presupuestos asignados para el periodo seleccionado"));
            admin_redirect("budget");
        }
    }
}