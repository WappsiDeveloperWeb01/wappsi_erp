$(document).ready(function (e) {

    $('.panel-heading').on('click', function() {
        const target = $(this).find('a').attr('href'); 
        $(target).collapse('toggle'); 
    });

    const btnConsultar = document.getElementById('consultar');
    btnConsultar.addEventListener('click', function(e){
        e.preventDefault()
        const validacion = validarYear();
        if (validacion) {
            document.getElementById('FilterForm').submit();
        }
    });

    $('#year').on('change', function(){
        if ($(this).val() == 'all') {
            $('#month').select2('val', 'all');
            $('.divMonth').css('display', 'none');
        }else{
            $('.divMonth').css('display', 'block');
        }
    }).trigger('change');

    const totalFe = $('#totalFe').text();
    if (totalFe)  $('h5.panel-title #titleFe').text(totalFe);
    
    const totalPoFe = $('#totalPoFe').text();
    if (totalPoFe)  $('h5.panel-title #titlePoFe').text(totalPoFe);
    
    const totalDocSup = $('#totalDocSup').text();
    if (totalDocSup)  $('h5.panel-title #titleDocSup').text(totalDocSup);
    
    const totalRep = $('#totalRep').text();
    if (totalRep)  $('h5.panel-title #titleRep').text(totalRep);
    
    const totalNe = $('#totalNe').text();
    if (totalNe)  $('h5.panel-title #titleNe').text(totalNe);
    
});


const validarYear = () => {
    const month = $('#month').val();  
    if (month != 'all') {
        const year = $('#year').val();
        if (year == 'all') {
            header_alert('warning', 'Debe seleecionar un año');
            return false;
        }
    }
    return true;
}


