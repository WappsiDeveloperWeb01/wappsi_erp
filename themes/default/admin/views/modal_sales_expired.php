<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->session->set_userdata('payment_term_first_view', 1);
 ?>

  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Ventas con plazo de pago a vencerse</h4>
      </div>
      <div class="modal-body">
        <?php if ($sales_expired !== false): ?>
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                  <th><?= lang("date"); ?></th>
                  <th><?= lang("payment_term"); ?></th>
                  <th><?= lang("due_date"); ?></th>
                  <th><?= lang("reference_no"); ?></th>
                  <th><?= lang("customer_name"); ?></th>
                  <th><?= lang("grand_total"); ?></th>
                  <th><?= lang("paid"); ?></th>
                  <th><?= lang("balance"); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($sales_expired as $id => $row): ?>
                <tr>
                  <td><?= $row->date ?></td>
                  <td><?= $row->payment_term ?></td>
                  <td><?= $row->due_date ?></td>
                  <td><?= $row->reference_no ?></td>
                  <td><?= $row->customer ?></td>
                  <td><?= $this->sma->formatMoney($row->grand_total) ?></td>
                  <td><?= $this->sma->formatMoney($row->paid) ?></td>
                  <td><?= $this->sma->formatMoney($row->grand_total - $row->paid) ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
            <tfoot>
              <tr>
                  <th><?= lang("date"); ?></th>
                  <th><?= lang("payment_term"); ?></th>
                  <th><?= lang("due_date"); ?></th>
                  <th><?= lang("reference_no"); ?></th>
                  <th><?= lang("customer_name"); ?></th>
                  <th><?= lang("grand_total"); ?></th>
                  <th><?= lang("paid"); ?></th>
                  <th><?= lang("balance"); ?></th>
              </tr>
            </tfoot>
          </table>
        <?php else: ?>
          No se encontraron ventas con plazo de pago a vencerse según el parámetro.
        <?php endif ?>
          
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Entendido</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->