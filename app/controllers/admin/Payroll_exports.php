<?php

use Mpdf\Css\Border;

defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_exports extends MY_Controller
{
    public $frequencies;
    public $status;
    public $reportTypes;
    public $months;

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableElectronicPayroll) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->Payroll_settings = $this->site->get_payroll_setting();

        $this->lang->admin_load('payroll', $this->Settings->user_language);

        $this->load->admin_model('Payroll_contracts_model');
        $this->load->admin_model('Payroll_management_model');
        $this->load->admin_model("Employees_model");

        $this->load->library('form_validation');

        $this->frequencies = json_decode(json_encode([
            ['id' => BIWEEKLY, 'name' => $this->lang->line('biweekly')],
            ['id' => MONTHLY, 'name' => $this->lang->line('monthly')]
        ]));

        $this->status = json_decode(json_encode([
            ['id' => IN_PREPARATION, 'name' => $this->lang->line('payroll_management_in_preparation')],
            ['id' => APPROVED, 'name' => $this->lang->line('payroll_management_approved')],
            ['id' => PAID, 'name' => $this->lang->line('payroll_management_paid')]
        ]));

        $this->reportTypes = json_decode(json_encode([
            ['id' => 1, 'name' => $this->lang->line('summarized')],
            ['id' => 2, 'name' => $this->lang->line('detailed')]
        ]));
    }

    public function index()
    {
        $this->sma->checkPermissions("index", NULL, "payroll_management");

        $this->data["frequencies"] = $this->frequencies;
        $this->data["status"] = $this->status;
        $this->data['reportTypes'] = $this->reportTypes;
        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data["areas"] = $this->Payroll_contracts_model->get_areas();
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');

        if ($this->input->post('areas')) {
            $this->data["professionalPosition"] = $this->Payroll_contracts_model->get_professional_position_by_area_id($this->input->post('areas'));
        }

        $this->page_construct("payroll_exports/index", ["page_title" => $this->lang->line("payroll_exports")], $this->data);
    }

    public function getProfessionalPosition()
    {
        $areaId = $this->input->post('area');
        $professionalPosition = $this->Payroll_contracts_model->get_professional_position_by_area_id($areaId);

        $optionsHtml = '<option value="">'.lang("alls").'</option>';
        if ($professionalPosition !== FALSE) {
            foreach ($professionalPosition as $position) {
                $optionsHtml .= '<option value="'.$position->id.'">'. ucfirst(mb_strtolower($position->name)) .'</option>';
            }
        }

        echo $optionsHtml;
    }

    public function getPayrollReport()
    {
        $this->load->library('excel');
        $this->load->helper('excel');

        $sheet = $this->excel->getActiveSheet();
        $reportType = $this->input->post("report_type");
        // $includeProvisionsSocialBenefits = $this->input->post("include_provisions_social_benefits");

        if ($reportType == 2) {
            $fileName = "Nómina Detallada";
            $sheet->setTitle("Nómina Detallada");

            $this->getPayrollReportHeader($sheet);
            $this->getPayrollReportBody($sheet);
        } else {
            $fileName = "Nómina Resumida";
            $sheet->setTitle("Nómina Resumida");

            $this->getPayrollReportHeaderSummarized($sheet);
            $this->getPayrollReportBodySummarized($sheet);
        }

        // if ($includeProvisionsSocialBenefits) {
        //     $sheet2 = $this->excel->createSheet();
        //     $sheet2->setTitle("Provisiones");
        //     $this->getPayrollReportProvitions($sheet2);
        // }

        create_excel($this->excel, $fileName);
    }

    private function getPayrollReportHeader($sheet)
    {
        $period = (!empty($this->input->post('date_records_filter'))) ? "{$this->input->post('start_date')} - {$this->input->post('end_date')}" : 'Todos los registros';
        if (!empty($this->input->post("employee_id"))) {
            $employeeData = $this->Employees_model->get_by_id($this->input->post("employee_id"));
            $employee = $employeeData->name;
        } else {
            $employee =  'Todos los empleados';
        }

        $includeProvisionsSocialBenefits = $this->input->post("include_provisions_social_benefits");
        $headerStyle = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font' => [
                'bold' => true,
                'size' => 14,
                'color' => ['rgb' => '000000'],
            ],
        ];

        $secondHeaderStyle = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font' => [
                "bold" => true,
                'color' => ['rgb' => '000000'],
            ],
        ];

        $sheet->mergeCells('A2:E2')->setCellValue("A2", "Reporte de Nomína Detallada")->getStyle('A2')->getFont()->setBold(true);
        $sheet->mergeCells('A3:E3')->setCellValue("A3", $period)->getStyle('A3')->getFont()->setBold(true);
        $sheet->mergeCells('A4:E4')->setCellValue("A4", $employee)->getStyle('A4')->getFont()->setBold(true);

        $sheet->mergeCells('A6:N6');
        $sheet->setCellValue("A6", "Contrato");
        $sheet->getStyle('A6:N6')->applyFromArray($headerStyle);

        $sheet->mergeCells('O6:BI6');
        $sheet->setCellValue("O6", "Devengados");
        $sheet->getStyle('O6:BI6')->applyFromArray($headerStyle);

        $sheet->mergeCells('BJ6:CA6');
        $sheet->setCellValue("BJ6", "Deducidos");
        $sheet->getStyle('BJ6:CA6')->applyFromArray($headerStyle);

        $sheet->getRowDimension(7)->setRowHeight(48);
        $sheet->setCellValue("A7", "Año")->getColumnDimension("A")->setAutoSize(true);
        $sheet->setCellValue("B7", "Mes")->getColumnDimension("B")->setAutoSize(true);
        $sheet->setCellValue("C7", "Quincena")->getColumnDimension("C")->setAutoSize(true);
        $sheet->setCellValue("D7", "N° documento")->getColumnDimension("D")->setAutoSize(true);
        $sheet->setCellValue("E7", "Nombre empleado")->getColumnDimension("E")->setAutoSize(true);
        $sheet->setCellValue("F7", "Dirección")->getColumnDimension("F")->setAutoSize(true);
        $sheet->setCellValue("G7", "Teléfono")->getColumnDimension("G")->setAutoSize(true);
        $sheet->setCellValue("H7", "Correo")->getColumnDimension("H")->setAutoSize(true);
        $sheet->setCellValue("I7", "Tipo de contrato")->getColumnDimension("I")->setAutoSize(true);
        $sheet->setCellValue("J7", "Sucursal")->getColumnDimension("J")->setAutoSize(true);
        $sheet->setCellValue("K7", "Area")->getColumnDimension("K")->setAutoSize(true);
        $sheet->setCellValue("L7", "Cargo")->getColumnDimension("L")->setAutoSize(true);
        $sheet->setCellValue("M7", "Salario Base")->getColumnDimension("M")->setAutoSize(true);
        $sheet->setCellValue("N7", "Auxilio de\ntransporte")->getColumnDimension("N")->setAutoSize(true);

        $sheet->setCellValue("O7", "Cantidad\nDías")->getColumnDimension("O")->setAutoSize(true);
        $sheet->setCellValue("P7", "Salario\nDevengado")->getColumnDimension("P")->setAutoSize(true);
        $sheet->setCellValue("Q7", "Auxilio De\nTransporte\nDevengado")->getColumnDimension("Q")->setAutoSize(true);

        $sheet->setCellValue("BI7", "Total\nDevengado")->getColumnDimension("BI")->setAutoSize(true);
        $sheet->setCellValue("CA7", "Total\nDeducibles")->getColumnDimension("CA")->setAutoSize(true);
        $sheet->setCellValue("CB7", "Neto\nPagado")->getColumnDimension("CB")->setAutoSize(true);

        if ($includeProvisionsSocialBenefits) {
            $sheet->mergeCells('CC6:CG6');
            $sheet->setCellValue("CC6", "Provisiones");
            $sheet->getStyle('CC6:CG6')->applyFromArray($headerStyle);
            $sheet->setCellValue("CC7", "Cesantías")->getColumnDimension("CC")->setAutoSize(true);
            $sheet->setCellValue("CD7", "Intereses a\nCesantías")->getColumnDimension("CD")->setAutoSize(true);
            $sheet->setCellValue("CE7", "Prima de\nServicios")->getColumnDimension("CE")->setAutoSize(true);
            $sheet->setCellValue("CF7", "Vacaciones")->getColumnDimension("CF")->setAutoSize(true);
            $sheet->setCellValue("CG7", "Total\nProvisiones")->getColumnDimension("CG")->setAutoSize(true);
            $sheet->getStyle('A7:CG7')->applyFromArray($secondHeaderStyle);
            $sheet->getStyle('A7:CG7')->getAlignment()->setWrapText(true);
        } else {
            $sheet->getStyle('A7:CB7')->applyFromArray($secondHeaderStyle);
            $sheet->getStyle('A7:CB7')->getAlignment()->setWrapText(true);
        }

            
    }

    private function getPayrollReportBody($sheet)
    {
        $payrolls = $this->Payroll_management_model->getPayrollToExcel($this->input->post());
        $includeProvisionsSocialBenefits = $this->input->post("include_provisions_social_benefits");
        if (!empty($payrolls)) {
            $row = 8;
            $columnsToDelete = [
                "dayTimeOverTime"                   => true,
                "nightOverTime"                     => true,
                "nightSurcharge"                    => true,
                "holidayDaytimeOvertime"            => true,
                "holidayDaytimeSurcharge"           => true,
                "holidayNightOvertime"              => true,
                "holidayNightSurcharge"             => true,
                "commissions"                       => true,
                "incentives"                        => true,
                "bonusesConstitutingSalary"         => true,
                "perDiemConstitutesSalary"          => true,
                "paidLicense"                       => true,
                "familyActivityPermit"              => true,
                "duelLicense"                       => true,
                "paidPermit"                        => true,
                "maternityLicense"                  => true,
                "paternityLicense"                  => true,
                "commonDisability"                  => true,
                "professionalDisability"            => true,
                "workDisability"                    => true,
                "disciplinarySuspension"            => true,
                "notPaidLicense"                    => true,
                "notPaidPermit"                     => true,
                "timeVacation"                      => true,
                "moneyVacation"                     => true,
                "bearingAllowance"                  => true,
                "bonusesNotConstitutingSalary"      => true,
                "perDiemNotConstitutesSalary"       => true,
                "socialSecurityRecognition"         => true,
                "otherAccruedConstituentsSalary"    => true,
                "loanDisbursement"                  => true,
                "foodBond"                          => true,
                "otherAccruedNotConstituentsSalary" => true,
                "endowment"                         => true,
                "compensation"                      => true,
                "healthRate"                        => true,
                "healthRateLessThan"                => true,
                "pensionRate"                       => true,
                "highRiskPensionRate"               => true,
                "solidarityFundEmployees1"          => true,
                "solidarityFundEmployees1_2"        => true,
                "solidarityFundEmployees1_4"        => true,
                "solidarityFundEmployees1_6"        => true,
                "solidarityFundEmployees1_8"        => true,
                "solidarityFundEmployees2"          => true,
                "withholding"                       => true,
                "unexcusedAbsenteeism"              => true,
                "companyLoan"                       => true,
                "draftCredit"                       => true,
                "freeInvestmentLoan"                => true,
                "deductionCancellationCbte"         => true,
                "otherDeductions"                   => true,
            ];

            foreach ($payrolls as $payroll) {
                $totalProvisions = 0;
                $sheet->setCellValue("A{$row}", $payroll->year)->getColumnDimension("A")->setAutoSize(true);
                $sheet->setCellValue("B{$row}", $payroll->month)->getColumnDimension("B")->setAutoSize(true);
                $sheet->setCellValue("C{$row}", $payroll->number)->getColumnDimension("C")->setAutoSize(true);
                $sheet->setCellValue("D{$row}", $payroll->vat_no)->getColumnDimension("D")->setAutoSize(true);
                $sheet->setCellValue("E{$row}", $payroll->name)->getColumnDimension("E")->setAutoSize(true);
                $sheet->setCellValue("F{$row}", ucwords(mb_strtolower($payroll->address)))->getColumnDimension("F")->setAutoSize(true);
                $sheet->setCellValue("G{$row}", $payroll->phone)->getColumnDimension("G")->setAutoSize(true);
                $sheet->setCellValue("H{$row}", $payroll->email)->getColumnDimension("H")->setAutoSize(true);
                $sheet->setCellValue("I{$row}", $payroll->contractTypeDescription)->getColumnDimension("I")->setAutoSize(true);
                $sheet->setCellValue("J{$row}", ucwords(mb_strtolower($payroll->sucursal)))->getColumnDimension("J")->setAutoSize(true);
                $sheet->setCellValue("K{$row}", $payroll->area)->getColumnDimension("K")->setAutoSize(true);
                $sheet->setCellValue("L{$row}", $payroll->professionalPositions)->getColumnDimension("L")->setAutoSize(true);
                $sheet->setCellValue("M{$row}", $payroll->base_amount)->getColumnDimension("M")->setAutoSize(true);
                    $sheet->getStyle("M{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                $sheet->setCellValue("N{$row}", $payroll->transAllowance)->getColumnDimension("N")->setAutoSize(true);

                $payrollItems = $this->Payroll_management_model->getPayrollItems(["payroll_id" => $payroll->id, "contract_id" => $payroll->contractId, "employee_id" => $payroll->companies_id]);
                if (!empty($payrollItems)) {
                    $earnedTotal = 0;
                    $deductionsTotal = 0;

                    foreach ($payrollItems as $item) {
                        if ($item->earned_deduction == EARNED) {
                            if ($item->payroll_concept_id == SALARY) {
                                $sheet->setCellValue("O{$row}", $item->days)->getColumnDimension("O")->setAutoSize(true);
                                $sheet->setCellValue("P{$row}", $item->amount)->getColumnDimension("P")->setAutoSize(true);
                                    $sheet->getStyle("P{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == TRANSPORTATION_ALLOWANCE) {
                                $sheet->setCellValue("Q{$row}", $item->amount)->getColumnDimension("Q")->setAutoSize(true);
                                    $sheet->getStyle("Q{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == DAYTIME_OVERTIME) {
                                if ($columnsToDelete["dayTimeOverTime"]) {
                                    $sheet->setCellValue("R7", " Hora Extra\nDiurna 25%")->getColumnDimension("R")->setAutoSize(true);
                                    $sheet->setCellValue("S7", "Cantidad\nHoras")->getColumnDimension("S")->setAutoSize(true);
                                }
                                $sheet->setCellValue("R{$row}", $item->amount)->getColumnDimension("R")->setAutoSize(true);
                                    $sheet->getStyle("R{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $sheet->setCellValue("S{$row}", $item->hours)->getColumnDimension("S")->setAutoSize(true);
                                $columnsToDelete["dayTimeOverTime"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == NIGHT_OVERTIME) {
                                if ($columnsToDelete["nightOverTime"]) {
                                    $sheet->setCellValue("T7", "Hora Extra\nNocturna\n75%")->getColumnDimension("T")->setAutoSize(true);
                                    $sheet->setCellValue("U7", "Cantidad\nHoras")->getColumnDimension("U")->setAutoSize(true);
                                }
                                $sheet->setCellValue("T{$row}", $item->amount)->getColumnDimension("T")->setAutoSize(true);
                                    $sheet->getStyle("T{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $sheet->setCellValue("U{$row}", $item->hours)->getColumnDimension("U")->setAutoSize(true);
                                $columnsToDelete["nightOverTime"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == NIGHT_SURCHARGE) {
                                if ($columnsToDelete["nightSurcharge"]) {
                                    $sheet->setCellValue("V7", "Recargo\nNocturno\n35%")->getColumnDimension("V")->setAutoSize(true);
                                    $sheet->setCellValue("W7", "Cantidad\nHoras")->getColumnDimension("W")->setAutoSize(true);
                                }
                                $sheet->setCellValue("V{$row}", $item->amount)->getColumnDimension("V")->setAutoSize(true);
                                    $sheet->getStyle("V{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $sheet->setCellValue("W{$row}", $item->hours)->getColumnDimension("W")->setAutoSize(true);
                                $columnsToDelete["nightSurcharge"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HOLIDAY_DAYTIME_OVERTIME) {
                                if ($columnsToDelete["holidayDaytimeOvertime"]) {
                                    $sheet->setCellValue("X7", "Hora Extra\nDiurna Dominical Y\nFestivos 100%")->getColumnDimension("X")->setAutoSize(true);
                                    $sheet->setCellValue("Y7", "Cantidad\nHoras")->getColumnDimension("Y")->setAutoSize(true);
                                }
                                $sheet->setCellValue("X{$row}", $item->amount)->getColumnDimension("X")->setAutoSize(true);
                                    $sheet->getStyle("X{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $sheet->setCellValue("Y{$row}", $item->hours)->getColumnDimension("Y")->setAutoSize(true);
                                $columnsToDelete["holidayDaytimeOvertime"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HOLIDAY_DAYTIME_SURCHARGE) {
                                if ($columnsToDelete["holidayDaytimeSurcharge"]) {
                                    $sheet->setCellValue("Z7", "Recargo\nDiurno Dominical Y\nFestivos 75%")->getColumnDimension("Z")->setAutoSize(true);
                                    $sheet->setCellValue("AA7", "Cantidad\nHoras")->getColumnDimension("AA")->setAutoSize(true);
                                }
                                $sheet->setCellValue("Z{$row}", $item->amount)->getColumnDimension("Z")->setAutoSize(true);
                                    $sheet->getStyle("Z{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $sheet->setCellValue("AA{$row}", $item->hours)->getColumnDimension("AA")->setAutoSize(true);
                                $columnsToDelete["holidayDaytimeSurcharge"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HOLIDAY_NIGHT_OVERTIME) {
                                if ($columnsToDelete["holidayNightOvertime"]) {
                                    $sheet->setCellValue("AB7", "Hora Extra\nNocturna Dominical Y\nFestivos 150%")->getColumnDimension("AB")->setAutoSize(true);
                                    $sheet->setCellValue("AC7", "Cantidad\nHoras")->getColumnDimension("AC")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AB{$row}", $item->amount)->getColumnDimension("AB")->setAutoSize(true);
                                    $sheet->getStyle("AB{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $sheet->setCellValue("AC{$row}", $item->hours)->getColumnDimension("AC")->setAutoSize(true);
                                $columnsToDelete["holidayNightOvertime"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HOLIDAY_NIGHT_SURCHARGE) {
                                if ($columnsToDelete["holidayNightSurcharge"]) {
                                    $sheet->setCellValue("AD7", "Recargo\nNocturno Dominical Y\nFestivos 110%")->getColumnDimension("AD")->setAutoSize(true);
                                    $sheet->setCellValue("AE7", "Cantidad\nHoras")->getColumnDimension("AE")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AD{$row}", $item->amount)->getColumnDimension("AD")->setAutoSize(true);
                                    $sheet->getStyle("AD{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $sheet->setCellValue("AE{$row}", $item->hours)->getColumnDimension("AE")->setAutoSize(true);
                                $columnsToDelete["holidayNightSurcharge"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == COMMISSIONS) {
                                if ($columnsToDelete["commissions"]) {
                                    $sheet->setCellValue("AF7", "Comisiones")->getColumnDimension("AF")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AF{$row}", $item->amount)->getColumnDimension("AF")->setAutoSize(true);
                                    $sheet->getStyle("AF{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                    $columnsToDelete["commissions"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == INCENTIVES) {
                                if ($columnsToDelete["incentives"]) {
                                    $sheet->setCellValue("AG7", "Incentivos")->getColumnDimension("AG")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AG{$row}", $item->amount)->getColumnDimension("AG")->setAutoSize(true);
                                    $sheet->getStyle("AG{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["incentives"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == BONUSES_CONSTITUTING_SALARY) {
                                if ($columnsToDelete["bonusesConstitutingSalary"]) {
                                    $sheet->setCellValue("AH7", "Bonificación Constitutiva de Salario")->getColumnDimension("AH")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AH{$row}", $item->amount)->getColumnDimension("AH")->setAutoSize(true);
                                    $sheet->getStyle("AH{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                    $columnsToDelete["bonusesConstitutingSalary"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PER_DIEM_CONSTITUTES_SALARY) {
                                if ($columnsToDelete["perDiemConstitutesSalary"]) {
                                    $sheet->setCellValue("AI7", "Viaticos constitutivos de salario")->getColumnDimension("AI")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AI{$row}", $item->amount)->getColumnDimension("AI")->setAutoSize(true);
                                    $sheet->getStyle("AI{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["perDiemConstitutesSalary"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PAID_LICENSE) {
                                if ($columnsToDelete["paidLicense"]) {
                                    $sheet->setCellValue("AJ7", "Licencia Remunerada")->getColumnDimension("AJ")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AJ{$row}", $item->amount)->getColumnDimension("AJ")->setAutoSize(true);
                                    $sheet->getStyle("AJ{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["paidLicense"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == FAMILY_ACTIVITY_PERMIT) {
                                if ($columnsToDelete["familyActivityPermit"]) {
                                    $sheet->setCellValue("AK7", "Permiso por Actividad Familiar")->getColumnDimension("AK")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AK{$row}", $item->amount)->getColumnDimension("AK")->setAutoSize(true);
                                    $sheet->getStyle("AK{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["familyActivityPermit"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == DUEL_LICENSE) {
                                if ($columnsToDelete["duelLicense"]) {
                                    $sheet->setCellValue("AL7", "Licencia por Luto")->getColumnDimension("AL")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AL{$row}", $item->amount)->getColumnDimension("AL")->setAutoSize(true);
                                    $sheet->getStyle("AL{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["duelLicense"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PAID_PERMIT) {
                                if ($columnsToDelete["paidPermit"]) {
                                    $sheet->setCellValue("AM7", "Permiso Remunerado")->getColumnDimension("AM")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AM{$row}", $item->amount)->getColumnDimension("AM")->setAutoSize(true);
                                    $sheet->getStyle("AM{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["paidPermit"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == MATERNITY_LICENSE) {
                                if ($columnsToDelete["maternityLicense"]) {
                                    $sheet->setCellValue("AN7", "Licencia de Maternidad")->getColumnDimension("AN")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AN{$row}", $item->amount)->getColumnDimension("AN")->setAutoSize(true);
                                    $sheet->getStyle("AN{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["maternityLicense"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PATERNITY_LICENSE) {
                                if ($columnsToDelete["paternityLicense"]) {
                                    $sheet->setCellValue("AO7", "Licencia de Paternidad")->getColumnDimension("AO")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AO{$row}", $item->amount)->getColumnDimension("AO")->setAutoSize(true);
                                    $sheet->getStyle("AO{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["paternityLicense"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == COMMON_DISABILITY) {
                                if ($columnsToDelete["commonDisability"]) {
                                    $sheet->setCellValue("AP7", "Incapacidad Común")->getColumnDimension("AP")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AP{$row}", $item->amount)->getColumnDimension("AP")->setAutoSize(true);
                                    $sheet->getStyle("AP{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["commonDisability"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PROFESSIONAL_DISABILITY) {
                                if ($columnsToDelete["professionalDisability"]) {
                                    $sheet->setCellValue("AQ7", "Incapacidad Profesional")->getColumnDimension("AQ")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AQ{$row}", $item->amount)->getColumnDimension("AQ")->setAutoSize(true);
                                    $sheet->getStyle("AQ{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["professionalDisability"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == WORK_DISABILITY) {
                                if ($columnsToDelete["workDisability"]) {
                                    $sheet->setCellValue("AR7", "Incapacidad Laboral")->getColumnDimension("AR")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AR{$row}", $item->amount)->getColumnDimension("AR")->setAutoSize(true);
                                    $sheet->getStyle("AR{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["workDisability"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == DISCIPLINARY_SUSPENSION) {
                                if ($columnsToDelete["disciplinarySuspension"]) {
                                    $sheet->setCellValue("AS7", "Suspensión Disciplinaria")->getColumnDimension("AS")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AS{$row}", $item->amount)->getColumnDimension("AS")->setAutoSize(true);
                                    $sheet->getStyle("AS{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["disciplinarySuspension"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == NOT_PAID_LICENSE) {
                                if ($columnsToDelete["notPaidLicense"]) {
                                    $sheet->setCellValue("AT7", "Licencia no Remunerada")->getColumnDimension("AT")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AT{$row}", $item->amount)->getColumnDimension("AT")->setAutoSize(true);
                                    $sheet->getStyle("AT{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["notPaidLicense"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == NOT_PAID_PERMIT) {
                                if ($columnsToDelete["notPaidPermit"]) {
                                    $sheet->setCellValue("AU7", "Permiso no Remunerado")->getColumnDimension("AU")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AU{$row}", $item->amount)->getColumnDimension("AU")->setAutoSize(true);
                                    $sheet->getStyle("AU{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["notPaidPermit"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == TIME_VACATION) {
                                if ($columnsToDelete["timeVacation"]) {
                                    $sheet->setCellValue("AV7", "Vacaciones en Tiempo")->getColumnDimension("AV")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AV{$row}", $item->amount)->getColumnDimension("AV")->setAutoSize(true);
                                    $sheet->getStyle("AV{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["timeVacation"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == MONEY_VACATION && $item->paid_to_employee == TRUE) {
                                if ($columnsToDelete["moneyVacation"]) {
                                    $sheet->setCellValue("AW7", "Vacaciones en Dinero")->getColumnDimension("AW")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AW{$row}", $item->amount)->getColumnDimension("AW")->setAutoSize(true);
                                    $sheet->getStyle("AW{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["moneyVacation"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == BEARING_ALLOWANCE) {
                                if ($columnsToDelete["bearingAllowance"]) {
                                    $sheet->setCellValue("AX7", "Auxilio de Rodamiento")->getColumnDimension("AX")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AX{$row}", $item->amount)->getColumnDimension("AX")->setAutoSize(true);
                                    $sheet->getStyle("AX{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["bearingAllowance"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == BONUSES_NOT_CONSTITUTING_SALARY) {
                                if ($columnsToDelete["bonusesNotConstitutingSalary"]) {
                                    $sheet->setCellValue("AY7", "Bonificación no Constitutiva de Salario")->getColumnDimension("AY")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AY{$row}", $item->amount)->getColumnDimension("AY")->setAutoSize(true);
                                    $sheet->getStyle("AY{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["bonusesNotConstitutingSalary"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PER_DIEM_NOT_CONSTITUTES_SALARY) {
                                if ($columnsToDelete["perDiemNotConstitutesSalary"]) {
                                    $sheet->setCellValue("AZ7", "Viaticos No constitutivos de salario")->getColumnDimension("AZ")->setAutoSize(true);
                                }
                                $sheet->setCellValue("AZ{$row}", $item->amount)->getColumnDimension("AZ")->setAutoSize(true);
                                    $sheet->getStyle("AZ{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["perDiemNotConstitutesSalary"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == SOCIAL_SECURITY_RECOGNITION) {
                                if ($columnsToDelete["socialSecurityRecognition"]) {
                                    $sheet->setCellValue("BA7", "Reconocimiento seguridad social")->getColumnDimension("BA")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BA{$row}", $item->amount)->getColumnDimension("BA")->setAutoSize(true);
                                    $sheet->getStyle("BA{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["socialSecurityRecognition"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == OTHER_ACCRUED_CONSTITUENTS_SALARY) {
                                if ($columnsToDelete["otherAccruedConstituentsSalary"]) {
                                    $sheet->setCellValue("BB7", "Otros Devengados constitutivos de salario")->getColumnDimension("BB")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BB{$row}", $item->amount)->getColumnDimension("BB")->setAutoSize(true);
                                    $sheet->getStyle("BB{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["otherAccruedConstituentsSalary"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == LOAN_DISBURSEMENT) {
                                if ($columnsToDelete["loanDisbursement"]) {
                                    $sheet->setCellValue("BC7", "Desembolso Préstamo")->getColumnDimension("BC")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BC{$row}", $item->amount)->getColumnDimension("BC")->setAutoSize(true);
                                    $sheet->getStyle("BC{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["loanDisbursement"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == FOOD_BOND) {
                                if ($columnsToDelete["foodBond"]) {
                                    $sheet->setCellValue("BE7", "Bonos de Alimentación")->getColumnDimension("BE")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BE{$row}", $item->amount)->getColumnDimension("BE")->setAutoSize(true);
                                    $sheet->getStyle("BE{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["foodBond"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY) {
                                if ($columnsToDelete["otherAccruedNotConstituentsSalary"]) {
                                    $sheet->setCellValue("BF7", "Otros Devengados no constitutivos de salario")->getColumnDimension("BF")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BF{$row}", $item->amount)->getColumnDimension("BF")->setAutoSize(true);
                                    $sheet->getStyle("BF{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["otherAccruedNotConstituentsSalary"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == ENDOWMENT) {
                                if ($columnsToDelete["endowment"]) {
                                    $sheet->setCellValue("BG7", "Dotación")->getColumnDimension("BG")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BG{$row}", $item->amount)->getColumnDimension("BG")->setAutoSize(true);
                                    $sheet->getStyle("BG{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["endowment"] = false;

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == COMPENSATION) {
                                if ($columnsToDelete["compensation"]) {
                                    $sheet->setCellValue("BH7", "Indemnización")->getColumnDimension("BH")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BH{$row}", $item->amount)->getColumnDimension("BH")->setAutoSize(true);
                                    $sheet->getStyle("BH{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["compensation"] = false;

                                $earnedTotal += $item->amount;
                            }
                        }

                        if ($item->earned_deduction == DEDUCTION) {
                            if ($item->payroll_concept_id == HEALTH_RATE) {
                                if ($columnsToDelete["healthRate"]) {
                                    $sheet->setCellValue("BJ7", "Salud Tarifa (12.5%) Trabajador")->getColumnDimension("BJ")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BJ{$row}", $item->amount)->getColumnDimension("BJ")->setAutoSize(true);
                                    $sheet->getStyle("BJ{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["healthRate"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HEALTH_RATE_LESS_THAN) {
                                if ($columnsToDelete["healthRateLessThan"]) {
                                    $sheet->setCellValue("BK7", "Salud Sal<10SMLV Tarifa (4%) Trabajador")->getColumnDimension("BK")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BK{$row}", $item->amount)->getColumnDimension("BK")->setAutoSize(true);
                                    $sheet->getStyle("BK{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["healthRateLessThan"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PENSION_RATE) {
                                if ($columnsToDelete["pensionRate"]) {
                                    $sheet->setCellValue("BL7", "Pension Tarifa (4%) Trabajador")->getColumnDimension("BL")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BL{$row}", $item->amount)->getColumnDimension("BL")->setAutoSize(true);
                                    $sheet->getStyle("BL{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["pensionRate"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HIGH_RISK_PENSION_RATE) {
                                if ($columnsToDelete["highRiskPensionRate"]) {
                                    $sheet->setCellValue("BM7", "Pension Alto Riesgo Tarifa (26%) Trabajador")->getColumnDimension("BM")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BM{$row}", $item->amount)->getColumnDimension("BM")->setAutoSize(true);
                                    $sheet->getStyle("BM{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["highRiskPensionRate"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == SOLIDARITY_FUND_EMPLOYEES_1) {
                                if ($columnsToDelete["solidarityFundEmployees1"]) {
                                    $sheet->setCellValue("BN7", "Fondo de Solidaridad Pensional 1%")->getColumnDimension("BN")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BN{$row}", $item->amount)->getColumnDimension("BN")->setAutoSize(true);
                                    $sheet->getStyle("BN{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["solidarityFundEmployees1"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == SOLIDARITY_FUND_EMPLOYEES_1_2) {
                                if ($columnsToDelete["solidarityFundEmployees1_2"]) {
                                    $sheet->setCellValue("BO7", "Fondo de Solidaridad Pensional 1,2%")->getColumnDimension("BO")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BO{$row}", $item->amount)->getColumnDimension("BO")->setAutoSize(true);
                                    $sheet->getStyle("BO{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["solidarityFundEmployees1_2"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == SOLIDARITY_FUND_EMPLOYEES_1_4) {
                                if ($columnsToDelete["solidarityFundEmployees1_4"]) {
                                    $sheet->setCellValue("BP7", "Fondo de Solidaridad Pensional 1,2%")->getColumnDimension("BP")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BP{$row}", $item->amount)->getColumnDimension("BP")->setAutoSize(true);
                                    $sheet->getStyle("BP{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["solidarityFundEmployees1_4"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == SOLIDARITY_FUND_EMPLOYEES_1_6) {
                                if ($columnsToDelete["solidarityFundEmployees1_6"]) {
                                    $sheet->setCellValue("BQ7", "Fondo de Solidaridad Pensional 1,2%")->getColumnDimension("BQ")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BQ{$row}", $item->amount)->getColumnDimension("BQ")->setAutoSize(true);
                                    $sheet->getStyle("BQ{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["solidarityFundEmployees1_6"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == SOLIDARITY_FUND_EMPLOYEES_1_8) {
                                if ($columnsToDelete["solidarityFundEmployees1_8"]) {
                                    $sheet->setCellValue("BR7", "Fondo de Solidaridad Pensional 1,2%")->getColumnDimension("BR")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BR{$row}", $item->amount)->getColumnDimension("BR")->setAutoSize(true);
                                    $sheet->getStyle("BR{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["solidarityFundEmployees1_8"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == SOLIDARITY_FUND_EMPLOYEES_2) {
                                if ($columnsToDelete["solidarityFundEmployees2"]) {
                                    $sheet->setCellValue("BS7", "Fondo de Solidaridad Pensional 1,2%")->getColumnDimension("BS")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BS{$row}", $item->amount)->getColumnDimension("BS")->setAutoSize(true);
                                    $sheet->getStyle("BS{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["solidarityFundEmployees2"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == WITHHOLDING) {
                                if ($columnsToDelete["withholding"]) {
                                    $sheet->setCellValue("BT7", "Retención en la Fuente")->getColumnDimension("BT")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BT{$row}", $item->amount)->getColumnDimension("BT")->setAutoSize(true);
                                    $sheet->getStyle("BT{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["withholding"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == UNEXCUSED_ABSENTEEISM) {
                                if ($columnsToDelete["unexcusedAbsenteeism"]) {
                                    $sheet->setCellValue("BU7", "Ausentismo no justificado")->getColumnDimension("BU")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BU{$row}", $item->amount)->getColumnDimension("BU")->setAutoSize(true);
                                    $sheet->getStyle("BU{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["unexcusedAbsenteeism"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == COMPANY_LOAN) {
                                if ($columnsToDelete["companyLoan"]) {
                                    $sheet->setCellValue("BV7", "Préstamo Empresa")->getColumnDimension("BV")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BV{$row}", $item->amount)->getColumnDimension("BV")->setAutoSize(true);
                                    $sheet->getStyle("BV{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["companyLoan"] = false;

                                $deductionsTotal += $item->amount;
                            }  else if ($item->payroll_concept_id == DRAFT_CREDIT) {
                                if ($columnsToDelete["draftCredit"]) {
                                    $sheet->setCellValue("BW7", "Libranza")->getColumnDimension("BW")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BW{$row}", $item->amount)->getColumnDimension("BW")->setAutoSize(true);
                                    $sheet->getStyle("BW{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["draftCredit"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == FREE_INVESTMENT_LOAN) {
                                if ($columnsToDelete["freeInvestmentLoan"]) {
                                    $sheet->setCellValue("BX7", "Préstamos libre inversión")->getColumnDimension("BX")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BX{$row}", $item->amount)->getColumnDimension("BX")->setAutoSize(true);
                                    $sheet->getStyle("BX{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["freeInvestmentLoan"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == DEDUCTION_CANCELLATION_CBTE) {
                                if ($columnsToDelete["deductionCancellationCbte"]) {
                                    $sheet->setCellValue("BY7", "Deducción por Anulación de Cbte.")->getColumnDimension("BY")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BY{$row}", $item->amount)->getColumnDimension("BY")->setAutoSize(true);
                                    $sheet->getStyle("BY{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["deductionCancellationCbte"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == OTHER_DEDUCTIONS) {
                                if ($columnsToDelete["otherDeductions"]) {
                                    $sheet->setCellValue("BZ7", "Otras Deducciones")->getColumnDimension("BZ")->setAutoSize(true);
                                }
                                $sheet->setCellValue("BZ{$row}", $item->amount)->getColumnDimension("BZ")->setAutoSize(true);
                                    $sheet->getStyle("BZ{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["otherDeductions"] = false;

                                $deductionsTotal += $item->amount;
                            }
                        }
                        if ($includeProvisionsSocialBenefits) {
                            if ($item->payroll_concept_id == LAYOFFS && $item->paid_to_employee == NOT) {
                                $sheet->setCellValue("CC{$row}", $item->amount)->getColumnDimension("CC")->setAutoSize(true);
                                    $sheet->getStyle("CC{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            } else if ($item->payroll_concept_id == LAYOFFS_INTERESTS && $item->paid_to_employee == NOT) {
                                $sheet->setCellValue("CD{$row}", $item->amount)->getColumnDimension("CD")->setAutoSize(true);
                                    $sheet->getStyle("CD{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            } else if ($item->payroll_concept_id == SERVICE_BONUS) {
                                $sheet->setCellValue("CE{$row}", $item->amount)->getColumnDimension("CE")->setAutoSize(true);
                                    $sheet->getStyle("CE{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            } else if ($item->payroll_concept_id == MONEY_VACATION && $item->paid_to_employee == NOT) {
                                $sheet->setCellValue("CF{$row}", $item->amount)->getColumnDimension("CF")->setAutoSize(true);
                                    $sheet->getStyle("CF{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            }
                        }
                    }

                    $sheet->setCellValue("BI{$row}", $earnedTotal)->getColumnDimension("BI")->setAutoSize(true);
                        $sheet->getStyle("BI{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                    $sheet->setCellValue("CA{$row}", $deductionsTotal)->getColumnDimension("CA")->setAutoSize(true);
                        $sheet->getStyle("CA{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                    $sheet->setCellValue("CB{$row}", $earnedTotal - $deductionsTotal)->getColumnDimension("CB")->setAutoSize(true);
                            $sheet->getStyle("CB{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                    if ($includeProvisionsSocialBenefits) {
                        $sheet->setCellValue("CG{$row}", $totalProvisions)->getColumnDimension("CG")->setAutoSize(true);
                        $sheet->getStyle("CG{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                    }
                }

                $row++;
            }

            $this->deleteColumns($sheet, $columnsToDelete);
        } else {
            $this->session->set_flashdata('error', 'No hay datos disponibles con los filtros de búsqueda adminitrados');
            admin_redirect('payroll_exports');
        }
    }

    private function deleteColumns($sheet, $columns)
    {
        if ($columns["otherDeductions"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BZ") - 1);
        }
        if ($columns["deductionCancellationCbte"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BY") - 1);
        }
        if ($columns["freeInvestmentLoan"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BX") - 1);
        }
        if ($columns["draftCredit"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BW") - 1);
        }
        if ($columns["companyLoan"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BV") - 1);
        }
        if ($columns["unexcusedAbsenteeism"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BU") - 1);
        }
        if ($columns["withholding"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BT") - 1);
        }
        if ($columns["solidarityFundEmployees2"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BS") - 1);
        }
        if ($columns["solidarityFundEmployees1_8"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BR") - 1);
        }
        if ($columns["solidarityFundEmployees1_6"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BQ") - 1);
        }
        if ($columns["solidarityFundEmployees1_4"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BP") - 1);
        }
        if ($columns["solidarityFundEmployees1_2"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BO") - 1);
        }
        if ($columns["solidarityFundEmployees1"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BN") - 1);
        }
        if ($columns["highRiskPensionRate"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BM") - 1);
        }
        if ($columns["pensionRate"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BL") - 1);
        }
        if ($columns["healthRateLessThan"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BK") - 1);
        }
        if ($columns["healthRate"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BJ") - 1);
        }
        if ($columns["compensation"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BH") - 1);
        }
        if ($columns["endowment"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BG") - 1);
        }
        if ($columns["otherAccruedNotConstituentsSalary"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BF") - 1);
        }
        if ($columns["foodBond"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BE") - 1);
        }
        if ($columns["loanDisbursement"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BC") - 1);
        }
        if ($columns["otherAccruedConstituentsSalary"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BB") - 1);
        }
        if ($columns["socialSecurityRecognition"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("BA") - 1);
        }
        if ($columns["perDiemNotConstitutesSalary"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AZ") - 1);
        }
        if ($columns["bonusesNotConstitutingSalary"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AY") - 1);
        }
        if ($columns["bearingAllowance"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AX") - 1);
        }
        if ($columns["moneyVacation"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AW") - 1);
        }
        if ($columns["timeVacation"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AV") - 1);
        }
        if ($columns["notPaidPermit"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AU") - 1);
        }
        if ($columns["notPaidLicense"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AT") - 1);
        }
        if ($columns["disciplinarySuspension"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AS") - 1);
        }
        if ($columns["workDisability"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AR") - 1);
        }
        if ($columns["professionalDisability"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AQ") - 1);
        }
        if ($columns["commonDisability"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AP") - 1);
        }
        if ($columns["paternityLicense"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AO") - 1);
        }
        if ($columns["maternityLicense"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AN") - 1);
        }
        if ($columns["paidPermit"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AM") - 1);
        }
        if ($columns["duelLicense"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AL") - 1);
        }
        if ($columns["familyActivityPermit"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AK") - 1);
        }
        if ($columns["paidLicense"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AJ") - 1);
        }
        if ($columns["perDiemConstitutesSalary"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AI") - 1);
        }
        if ($columns["bonusesConstitutingSalary"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AH") - 1);
        }
        if ($columns["incentives"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AG") - 1);
        }
        if ($columns["commissions"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AF") - 1);
        }
        if ($columns["holidayNightSurcharge"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AE") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AD") - 1);
        }
        if ($columns["holidayNightOvertime"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AC") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AB") - 1);
        }
        if ($columns["holidayDaytimeSurcharge"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("AA") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("Z") - 1);
        }
        if ($columns["holidayDaytimeOvertime"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("Y") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("X") - 1);
        }
        if ($columns["nightSurcharge"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("W") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("V") - 1);
        }
        if ($columns["nightOverTime"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("U") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("T") - 1);
        }
        if ($columns["dayTimeOverTime"]) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("S") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("R") - 1);
        }

        if (!$this->input->post('include_personal_data_employee')) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("H") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("G") - 1);
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("F") - 1);
        }

        if ($this->input->post('frequency') == MONTHLY) {
            $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("C") - 1);
        }
    }

    private function getPayrollReportHeaderSummarized($sheet)
    {
        $period = (!empty($this->input->post('date_records_filter'))) ? "{$this->input->post('start_date')} - {$this->input->post('end_date')}" : 'Todos los registros';
        if (!empty($this->input->post("employee_id"))) {
            $employeeData = $this->Employees_model->get_by_id($this->input->post("employee_id"));
            $employee = $employeeData->name;
        } else {
            $employee =  'Todos los empleados';
        }
        $includeProvisionsSocialBenefits = $this->input->post("include_provisions_social_benefits");

        $headerStyle = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font' => [
                'bold' => true,
                'size' => 14,
                'color' => ['rgb' => '000000'],
            ],
        ];

        $secondHeaderStyle = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font' => [
                "bold" => true,
                'color' => ['rgb' => '000000'],
            ],
        ];

        $sheet->mergeCells('A2:E2')->setCellValue("A2", "Reporte de Nomína Resumida")->getStyle('A2')->getFont()->setBold(true);
        $sheet->mergeCells('A3:E3')->setCellValue("A3", $period)->getStyle('A3')->getFont()->setBold(true);
        $sheet->mergeCells('A4:E4')->setCellValue("A4", $employee)->getStyle('A4')->getFont()->setBold(true);

        $sheet->mergeCells('A6:H6');
        $sheet->setCellValue("A6", "Contrato");
        $sheet->getStyle('A6:H6')->applyFromArray($headerStyle);

        $sheet->mergeCells('I6:M6');
        $sheet->setCellValue("I6", "Devengados");
        $sheet->getStyle('I6:M6')->applyFromArray($headerStyle);

        $sheet->mergeCells("N6:S6");
        $sheet->setCellValue("N6", "Deducidos");
        $sheet->getStyle("N6:S6")->applyFromArray($headerStyle);

        $sheet->getRowDimension(7)->setRowHeight(48);

        $sheet->setCellValue("A7", "Año")->getColumnDimension("A")->setAutoSize(true);
        $sheet->setCellValue("B7", "Mes")->getColumnDimension("B")->setAutoSize(true);
        $sheet->setCellValue("C7", "Quincena")->getColumnDimension("C")->setAutoSize(true);
        $sheet->setCellValue("D7", "N° documento")->getColumnDimension("D")->setAutoSize(true);
        $sheet->setCellValue("E7", "Nombre empleado")->getColumnDimension("E")->setAutoSize(true);
        $sheet->setCellValue("F7", "Sucursal")->getColumnDimension("F")->setAutoSize(true);
        $sheet->setCellValue("G7", "Area")->getColumnDimension("G")->setAutoSize(true);
        $sheet->setCellValue("H7", "Cargo")->getColumnDimension("H")->setAutoSize(true);


        $sheet->setCellValue("M7", "Total\nDevengado")->getColumnDimension("N")->setAutoSize(true);
        $sheet->setCellValue("S7", "Total\nDeducibles")->getColumnDimension("R")->setAutoSize(true);
        $sheet->setCellValue("T7", "Neto\nPagado")->getColumnDimension("S")->setAutoSize(true);
        if ($includeProvisionsSocialBenefits) {
            $sheet->mergeCells("U6:Y6");
            $sheet->setCellValue("U6", "Provisiones");
            $sheet->getStyle("U6:Y6")->applyFromArray($headerStyle);
            $sheet->setCellValue("U7", "Cesantías")->getColumnDimension("L")->setAutoSize(true);
            $sheet->setCellValue("V7", "Intereses a\nCesantías")->getColumnDimension("M")->setAutoSize(true);
            $sheet->setCellValue("W7", "Prima de\nServicios")->getColumnDimension("N")->setAutoSize(true);
            $sheet->setCellValue("X7", "Vacaciones")->getColumnDimension("O")->setAutoSize(true);
            $sheet->setCellValue("Y7", "Total\nProvisiones")->getColumnDimension("P")->setAutoSize(true);
            $sheet->getStyle('A7:Y7')->applyFromArray($secondHeaderStyle);
            $sheet->getStyle('A7:Y7')->getAlignment()->setWrapText(true);
        } else {
            $sheet->getStyle('A7:T7')->applyFromArray($secondHeaderStyle);
            $sheet->getStyle('A7:T7')->getAlignment()->setWrapText(true);
        }
            
    }

    private function getPayrollReportBodySummarized($sheet)
    {
        $payrolls = $this->Payroll_management_model->getPayrollToExcel($this->input->post());
        $includeProvisionsSocialBenefits = $this->input->post("include_provisions_social_benefits");
        if (!empty($payrolls)) {
            $row = 8;
            $columnsToDelete = [
                "healthRate"                        => true,
                "healthRateLessThan"                => true,
                "pensionRate"                       => true,
                "highRiskPensionRate"               => true,
            ];

            foreach ($payrolls as $payroll) {
                $totalProvisions = 0;
                $sheet->setCellValue("A{$row}", $payroll->year)->getColumnDimension("A")->setAutoSize(true);
                $sheet->setCellValue("B{$row}", $payroll->month)->getColumnDimension("B")->setAutoSize(true);
                $sheet->setCellValue("C{$row}", $payroll->number)->getColumnDimension("C")->setAutoSize(true);
                $sheet->setCellValue("D{$row}", $payroll->vat_no)->getColumnDimension("D")->setAutoSize(true);
                $sheet->setCellValue("E{$row}", $payroll->name)->getColumnDimension("E")->setAutoSize(true);
                $sheet->setCellValue("F{$row}", ucwords(mb_strtolower($payroll->sucursal)))->getColumnDimension("F")->setAutoSize(true);
                $sheet->setCellValue("G{$row}", $payroll->area)->getColumnDimension("G")->setAutoSize(true);
                $sheet->setCellValue("H{$row}", $payroll->professionalPositions)->getColumnDimension("H")->setAutoSize(true);

                $payrollItems = $this->Payroll_management_model->getPayrollItems(["payroll_id" => $payroll->id, "contract_id" => $payroll->contractId, "employee_id" => $payroll->companies_id]);
                if (!empty($payrollItems)) {
                    $earnedTotal = 0;
                    $otherEarneds = 0;
                    $deductionsTotal = 0;
                    $otherDectuctions = 0;

                    foreach ($payrollItems as $item) {
                        if ($item->earned_deduction == EARNED) {
                            if ($item->payroll_concept_id == SALARY) {
                                $sheet->setCellValue("I7", "Cantidad\nde Días")->getColumnDimension("I")->setAutoSize(true);
                                $sheet->setCellValue("J7", "Salario")->getColumnDimension("J")->setAutoSize(true);
                                $sheet->setCellValue("I{$row}", $item->days)->getColumnDimension("I")->setAutoSize(true);
                                $sheet->setCellValue("J{$row}", $item->amount)->getColumnDimension("J")->setAutoSize(true);
                                    $sheet->getStyle("J{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $earnedTotal += $item->amount;
                            } else if ($item->payroll_concept_id == TRANSPORTATION_ALLOWANCE) {
                                $sheet->setCellValue("K7", "Auxilio de\nTransporte")->getColumnDimension("K")->setAutoSize(true);
                                $sheet->setCellValue("K{$row}", $item->amount)->getColumnDimension("K")->setAutoSize(true);
                                    $sheet->getStyle("K{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $earnedTotal += $item->amount;
                            } else {
                                $otherEarneds += $item->amount;
                            }
                        }

                        if ($item->earned_deduction == DEDUCTION) {
                            if ($item->payroll_concept_id == HEALTH_RATE_LESS_THAN) {
                                $sheet->setCellValue("N7", "Salud Sal<10SMLV Tarifa (4%) Trabajador")->getColumnDimension("N")->setAutoSize(true);
                                $sheet->setCellValue("N{$row}", $item->amount)->getColumnDimension("N")->setAutoSize(true);
                                    $sheet->getStyle("N{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["healthRateLessThan"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HEALTH_RATE) {
                                $sheet->setCellValue("O7", "Salud Tarifa (12.5%) Trabajador")->getColumnDimension("O")->setAutoSize(true);
                                $sheet->setCellValue("O{$row}", $item->amount)->getColumnDimension("O")->setAutoSize(true);
                                    $sheet->getStyle("O{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["healthRate"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == PENSION_RATE) {
                                $sheet->setCellValue("P7", "Pension Tarifa (4%) Trabajador")->getColumnDimension("P")->setAutoSize(true);
                                $sheet->setCellValue("P{$row}", $item->amount)->getColumnDimension("P")->setAutoSize(true);
                                    $sheet->getStyle("P{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["pensionRate"] = false;

                                $deductionsTotal += $item->amount;
                            } else if ($item->payroll_concept_id == HIGH_RISK_PENSION_RATE) {
                                $sheet->setCellValue("Q7", "Pension Alto Riesgo Tarifa (26%) Trabajador")->getColumnDimension("Q")->setAutoSize(true);
                                $sheet->setCellValue("Q{$row}", $item->amount)->getColumnDimension("Q")->setAutoSize(true);
                                    $sheet->getStyle("Q{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                                $columnsToDelete["highRiskPensionRate"] = false;

                                $deductionsTotal += $item->amount;
                            } else {
                                $otherDectuctions += $item->amount;
                            }
                        }


                        if ($includeProvisionsSocialBenefits) {
                            if ($item->payroll_concept_id == LAYOFFS && $item->paid_to_employee == NOT) {
                                $sheet->setCellValue("U{$row}", $item->amount)->getColumnDimension("U")->setAutoSize(true);
                                    $sheet->getStyle("U{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            } else if ($item->payroll_concept_id == LAYOFFS_INTERESTS && $item->paid_to_employee == NOT) {
                                $sheet->setCellValue("V{$row}", $item->amount)->getColumnDimension("V")->setAutoSize(true);
                                    $sheet->getStyle("V{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            } else if ($item->payroll_concept_id == SERVICE_BONUS) {
                                $sheet->setCellValue("W{$row}", $item->amount)->getColumnDimension("W")->setAutoSize(true);
                                    $sheet->getStyle("W{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            } else if ($item->payroll_concept_id == MONEY_VACATION && $item->paid_to_employee == NOT) {
                                $sheet->setCellValue("X{$row}", $item->amount)->getColumnDimension("X")->setAutoSize(true);
                                    $sheet->getStyle("X{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                                $totalProvisions += $item->amount;
                            }
                        }

                    }

                    $sheet->setCellValue("L7", "Otros\nDevengados")->getColumnDimension("L")->setAutoSize(true);
                    $sheet->setCellValue("L{$row}", $otherEarneds)->getColumnDimension("L")->setAutoSize(true);
                        $sheet->getStyle("L{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                    $sheet->setCellValue("M{$row}", $earnedTotal)->getColumnDimension("M")->setAutoSize(true);
                    $sheet->getStyle("M{$row}")->getNumberFormat()->setFormatCode("#,##0.00");



                    $sheet->setCellValue("R7", "Otros\nDeducidos")->getColumnDimension("R")->setAutoSize(true);
                    $sheet->setCellValue("R{$row}", $otherDectuctions)->getColumnDimension("R")->setAutoSize(true);
                        $sheet->getStyle("R{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                    $sheet->setCellValue("S{$row}", $deductionsTotal)->getColumnDimension("S")->setAutoSize(true);
                        $sheet->getStyle("S{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                    $sheet->setCellValue("T{$row}", $earnedTotal - $deductionsTotal)->getColumnDimension("T")->setAutoSize(true);
                            $sheet->getStyle("T{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                    if ($includeProvisionsSocialBenefits) {
                        $sheet->setCellValue("Y{$row}", $totalProvisions)->getColumnDimension("Y")->setAutoSize(true);
                        $sheet->getStyle("Y{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                    }
                }

                $row++;
            }

            if ($columnsToDelete["highRiskPensionRate"]) {
                $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("Q") - 1);
            }
            if ($columnsToDelete["pensionRate"]) {
                $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("P") - 1);
            }
            if ($columnsToDelete["healthRate"]) {
                $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("O") - 1);
            }
            if ($columnsToDelete["healthRateLessThan"]) {
                $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("N") - 1);
            }
            if ($this->input->post('frequency') == MONTHLY) {
                $sheet->removeColumnByIndex(PHPExcel_Cell::columnIndexFromString("C") - 1);
            }
        } else {
            $this->session->set_flashdata('error', 'No hay datos disponibles con los filtros de búsqueda adminitrados');
            admin_redirect('payroll_exports');
        }
    }

    private function getPayrollReportProvitions($sheet)
    {
        $period = (!empty($this->input->post('date_records_filter'))) ? "{$this->input->post('start_date')} - {$this->input->post('end_date')}" : 'Todos los registros';
        if (!empty($this->input->post("employee_id"))) {
            $employeeData = $this->Employees_model->get_by_id($this->input->post("employee_id"));
            $employee = $employeeData->name;
        } else {
            $employee =  'Todos los empleados';
        }

        $secondHeaderStyle = [
            'alignment' => [
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
            ],
            'borders' => [
                'allborders' => [
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => ['rgb' => '000000'],
                ],
            ],
            'font' => [
                "bold" => true,
                'color' => ['rgb' => '000000'],
            ],
        ];

        $sheet->mergeCells('A2:E2')->setCellValue("A2", "Reporte de Nomína Provisiones")->getStyle('A2')->getFont()->setBold(true);
        $sheet->mergeCells('A3:E3')->setCellValue("A3", $period)->getStyle('A3')->getFont()->setBold(true);
        $sheet->mergeCells('A4:E4')->setCellValue("A4", $employee)->getStyle('A4')->getFont()->setBold(true);

        $sheet->getRowDimension(7)->setRowHeight(48);
        $sheet->setCellValue("A7", "Año")->getColumnDimension("A")->setAutoSize(true);
        $sheet->setCellValue("B7", "Mes")->getColumnDimension("B")->setAutoSize(true);
        $sheet->setCellValue("C7", "Quincena")->getColumnDimension("C")->setAutoSize(true);
        $sheet->setCellValue("D7", "N° documento")->getColumnDimension("D")->setAutoSize(true);
        $sheet->setCellValue("E7", "Nombre empleado")->getColumnDimension("E")->setAutoSize(true);
        $sheet->setCellValue("F7", "Sucursal")->getColumnDimension("F")->setAutoSize(true);
        $sheet->setCellValue("G7", "Area")->getColumnDimension("G")->setAutoSize(true);
        $sheet->setCellValue("H7", "Cargo")->getColumnDimension("H")->setAutoSize(true);
        $sheet->setCellValue("I7", "Cantidad\nde Días")->getColumnDimension("I")->setAutoSize(true);
        $sheet->setCellValue("J7", "Salario")->getColumnDimension("J")->setAutoSize(true);
        $sheet->setCellValue("K7", "Auxilio de\nTransporte")->getColumnDimension("K")->setAutoSize(true);
        $sheet->setCellValue("L7", "Cesantías")->getColumnDimension("L")->setAutoSize(true);
        $sheet->setCellValue("M7", "Intereses a\nCesantías")->getColumnDimension("M")->setAutoSize(true);
        $sheet->setCellValue("N7", "Prima de\nServicios")->getColumnDimension("N")->setAutoSize(true);
        $sheet->setCellValue("O7", "Vacaciones")->getColumnDimension("O")->setAutoSize(true);
        $sheet->setCellValue("P7", "Total\nProvisiones")->getColumnDimension("P")->setAutoSize(true);

        $sheet->getStyle('A7:P7')->applyFromArray($secondHeaderStyle);
        $sheet->getStyle('A7:P7')->getAlignment()->setWrapText(true);


        $payrolls = $this->Payroll_management_model->getPayrollToExcel($this->input->post());
        if (!empty($payrolls)) {
            $row = 8;
            foreach ($payrolls as $payroll) {
                $totalProvisions = 0;
                $sheet->setCellValue("A{$row}", $payroll->year)->getColumnDimension("A")->setAutoSize(true);
                $sheet->setCellValue("B{$row}", $payroll->month)->getColumnDimension("B")->setAutoSize(true);
                $sheet->setCellValue("C{$row}", $payroll->number)->getColumnDimension("C")->setAutoSize(true);
                $sheet->setCellValue("D{$row}", $payroll->vat_no)->getColumnDimension("D")->setAutoSize(true);
                $sheet->setCellValue("E{$row}", $payroll->name)->getColumnDimension("E")->setAutoSize(true);
                $sheet->setCellValue("F{$row}", ucwords(mb_strtolower($payroll->sucursal)))->getColumnDimension("F")->setAutoSize(true);
                $sheet->setCellValue("G{$row}", $payroll->area)->getColumnDimension("G")->setAutoSize(true);
                $sheet->setCellValue("H{$row}", $payroll->professionalPositions)->getColumnDimension("H")->setAutoSize(true);

                $payrollItems = $this->Payroll_management_model->getPayrollItems(["payroll_id" => $payroll->id, "contract_id" => $payroll->contractId, "employee_id" => $payroll->companies_id]);
                if (!empty($payrollItems)) {
                    foreach ($payrollItems as $item) {
                        if ($item->payroll_concept_id == SALARY) {
                            $sheet->setCellValue("I{$row}", $item->days)->getColumnDimension("I")->setAutoSize(true);
                            $sheet->setCellValue("J{$row}", $item->amount)->getColumnDimension("J")->setAutoSize(true);
                                $sheet->getStyle("J{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                        } else if ($item->payroll_concept_id == TRANSPORTATION_ALLOWANCE) {
                            $sheet->setCellValue("K{$row}", $item->amount)->getColumnDimension("K")->setAutoSize(true);
                                $sheet->getStyle("K{$row}")->getNumberFormat()->setFormatCode("#,##0.00");
                        } else if ($item->payroll_concept_id == LAYOFFS && $item->paid_to_employee == NOT) {
                            $sheet->setCellValue("L{$row}", $item->amount)->getColumnDimension("L")->setAutoSize(true);
                                $sheet->getStyle("L{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                            $totalProvisions += $item->amount;
                        } else if ($item->payroll_concept_id == LAYOFFS_INTERESTS && $item->paid_to_employee == NOT) {
                            $sheet->setCellValue("M{$row}", $item->amount)->getColumnDimension("M")->setAutoSize(true);
                                $sheet->getStyle("M{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                            $totalProvisions += $item->amount;
                        } else if ($item->payroll_concept_id == SERVICE_BONUS) {
                            $sheet->setCellValue("N{$row}", $item->amount)->getColumnDimension("N")->setAutoSize(true);
                                $sheet->getStyle("N{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                            $totalProvisions += $item->amount;
                        } else if ($item->payroll_concept_id == MONEY_VACATION && $item->paid_to_employee == NOT) {
                            $sheet->setCellValue("O{$row}", $item->amount)->getColumnDimension("O")->setAutoSize(true);
                                $sheet->getStyle("O{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                            $totalProvisions += $item->amount;
                        }
                    }
                }

                $sheet->setCellValue("P{$row}", $totalProvisions)->getColumnDimension("P")->setAutoSize(true);
                    $sheet->getStyle("P{$row}")->getNumberFormat()->setFormatCode("#,##0.00");

                $row++;
            }
        }
    }
}