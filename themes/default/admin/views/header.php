<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <base href="<?= site_url() ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?= $page_title ?> - <?= $Settings->site_name ?></title>
    <link rel="shortcut icon" href="<?= $assets ?>images/icon.png"/>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css<?= $files_version ?>" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/font-awesome/css/font-awesome.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/font-awesome-5.8.1/css/fontawesome.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/css/bootstrap.min.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/css/plugins/toastr/toastr.min.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/css/plugins/steps/jquery.steps.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>styles/theme.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/css/animate.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= base_url() ?>vendor/icheck-1.x/skins/all.css<?= $files_version ?>?v=1.0.2">
    <link rel="stylesheet" href="<?= base_url() ?>vendor/select2-4.0.11/dist/css/select2.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/css/plugins/sweetalert/sweetalert.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/css/plugins/jasny/jasny-bootstrap.min.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>styles/style.css<?= $files_version ?>">
    <link rel="stylesheet" href="<?= $assets ?>inspinia/css/style.css<?= $files_version ?>">
    <link href="<?= $assets ?>fontawesome-free-6.3.0-web/css/fontawesome.css" rel="stylesheet">
    <link href="<?= $assets ?>fontawesome-free-6.3.0-web/css/brands.css" rel="stylesheet">
    <link href="<?= $assets ?>fontawesome-free-6.3.0-web/css/solid.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <style type="text/css">
        @media print {
            .modal-dialog {
                padding: 0 !important;
                margin: 0 !important
            }
            .modal-content {
                padding: 0 !important;
                margin: 0 !important
            }
            .modal-header {
                padding: 0 !important;
                margin: 0 !important
            }
            .modal-body {
                padding: 0 !important;
                margin: 0 !important
            }
            #page-wrapper {
                padding: 0 !important;
                margin: 0 !important;
            }

            .tablePrint {
                font-size: 78% !important;
            }
        }

        .html5buttons{ display: none; }
        .buttons-html5{ display: none; }
        .not-display { display: none !important; }
    </style>

    <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-migrate-1.2.1.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery-ui.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $assets ?>js/select2.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $assets ?>js/jquery.validate.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js"></script>
    <noscript><style type="text/css">#loading { display: none; }</style></noscript>
    <?php if ($Settings->user_rtl) { ?>
        <link rel="stylesheet" href="<?= $assets ?>styles/helpers/bootstrap-rtl.min.css<?= $files_version ?>"/>
        <link rel="stylesheet" href="<?= $assets ?>styles/style-rtl.css<?= $files_version ?>"/>
        <script type="text/javascript">
            $(document).ready(function () { $('.pull-right, .pull-left').addClass('flip'); });
        </script>
    <?php } ?>
    <script type="text/javascript">
        $(window).load(function () {
            $("#loading").fadeOut("slow");
        });
        var order_sale_notification;
        var pos_order_preparation_notification;
        $(document).ready(function(){
            if (site.settings.order_sale_notification != 0) {
                order_sale_notification = new Audio('<?= $assets.'/sounds/'.$this->Settings->order_sale_notification ?>');
                <?php if ($new_order_sales): ?>
                    if (site.settings.repeat_order_sale_notification == 1) {
                       order_sale_notification.loop = true;
                    }
                    order_sale_notification.play();
                <?php endif ?>
            }
            if (site.settings.pos_order_preparation_notification != 0) {
                pos_order_preparation_notification = new Audio('<?= $assets.'/sounds/'.$this->Settings->pos_order_preparation_notification ?>');
            }
        });
    </script>

    <script type="text/javascript" src="<?= $assets ?>inspinia/js/plugins/toastr/toastr.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $assets ?>inspinia/js/plugins/steps/jquery.steps.min.js<?= $files_version ?>"></script>
    <script type="text/javascript" src="<?= $assets ?>inspinia/js/plugins/sweetalert/sweetalert.min.js<?= $files_version ?>"></script>
</head>

<body class="pace-done skin-1">
    <div id="ajaxCall">
        <i class="fa fa-spinner fa-pulse fa-5x"></i>
    </div>
    <noscript>
        <div class="global-site-notice noscript">
            <div class="notice-inner">
                <p>
                    <strong>JavaScript seems to be disabled in your browser.</strong>
                    <br>You must have JavaScript enabled in your browser to utilize the functionality of this website.
                </p>
            </div>
        </div>
    </noscript>
    <div id="loading"></div>

    
    <?php include 'estructura_inspinia.php'; ?>
