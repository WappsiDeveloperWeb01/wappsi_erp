<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: System Settings
 *
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */


$lang['site_name']                      = "Site Name";
$lang['dateformat']                     = "Date Format";
$lang['timezone']                       = "Timezone";
$lang['maintenance_mode']               = "Maintenance Mode";
$lang['image_width']                    = "Image Width";
$lang['image_height']                   = "Image Height";
$lang['thumbnail_width']                = "Thumbnail Width";
$lang['thumbnail_height']               = "Thumbnail Height";
$lang['watermark']                      = "Watermark";
$lang['reg_ver']                        = "Registration Verification";
$lang['allow_reg']                      = "Allow Registration";
$lang['reg_notification']               = "Registration Notification";
$lang['default_email']                  = "Default Email";
$lang['default_warehouse']              = "Default Warehouse";
$lang['invoice_tax']                    = "Order Tax";
$lang['sales_prefix']                   = "Sales Reference Prefix";
$lang['quote_prefix']                   = "Quotation Reference Prefix";
$lang['purchase_prefix']                = "Purchase Reference Prefix";
$lang['transfer_prefix']                = "Transfer Reference Prefix";
$lang['delivery_prefix']                = "Delivery Reference Prefix";
$lang['payment_prefix']                 = "Payment Reference Prefix";
$lang['detect_barcode']                 = "Detect Barcode";
$lang['theme']                          = "Theme";
$lang['rows_per_page']                  = "Rows per page";
$lang['accounting_method']              = "Accounting Method";
$lang['product_serial']                 = "Product Serial";
$lang['product_discount']               = "Product Discount";
$lang['bc_fix']                         = "Products count to fix barcode input";
$lang['decimals']                       = "Decimals";
$lang['decimals_sep']                   = "Decimals Separator";
$lang['thousands_sep']                  = "Thousands Separator";
$lang['overselling_will_only_work_with_AVCO_accounting_method_only'] = "Overselling will only work with AVCO accounting method and the value has been changed for you.";
$lang['accounting_method_change_alert'] = "You are going to change the accounting method and it will mess up your old account. We suggest you to do this only on 1st day of any month.";
$lang['setting_updated']                = "Settings successfully updated";
$lang['logo_uploaded']                  = "Logo successfully uploaded";
$lang['site_config']                    = "Site Configuration";
$lang['dot']                            = "Dot";
$lang['comma']                          = "Comma";
$lang['space']                          = "Space";
$lang['email_protocol']                 = "Email Protocol";
$lang['smtp_host']                      = "SMTP Host";
$lang['smtp_user']                      = "SMTP User";
$lang['smtp_pass']                      = "SMTP Password";
$lang['smtp_port']                      = "SMTP Port";
$lang['update_settings']                = "Update Settings";
$lang['prefix_year_no']                 = "YEAR/Sequence Number (SL/2014/001)";
$lang['prefix_month_year_no']           = "YEAR/MONTH/Sequence Number (SL/2014/08/001)";
$lang['login_captcha']                  = "Login Captcha";
$lang['default_currency']               = "Default Currency";
$lang['calendar']                       = "Calender";
$lang['private']                        = "Private";
$lang['shared']                         = "Shared";
$lang['racks']                          = "Racks";
$lang['product_expiry']                 = "Product Expiry";
$lang['image_size']                     = "Image Size";
$lang['thumbnail_size']                 = "Thumbnail Size";
$lang['over_selling']                   = "Over Selling";
$lang['reference_format']               = "Reference Format";
$lang['product_level_discount']         = "Product Level Discount";
$lang['auto_detect_barcode']            = "Auto Detect Barcode";
$lang['item_addition']                  = "Cart Item Addition Method";
$lang['add_new_item']                   = "Add New Item to cart";
$lang['increase_quantity_if_item_exist'] = "Increase item quantity, if it already exists in cart";
$lang['prefix']                         = "Prefix";
$lang['money_number_format']            = "Money and Number Format";
$lang['reg_notification']               = "Registration Notification";
$lang['site_logo']                      = "Site Logo";
$lang['biller_logo']                    = "Biller Logo";
$lang['upload_logo']                    = "Update Logo";
$lang['add_currency']                   = "Add Currency";
$lang['edit_currency']                  = "Edit Currency";
$lang['delete_currency']                = "Delete Currency";
$lang['delete_currencies']              = "Delete Currencies";
$lang['currency_code']                  = "Currency Code";
$lang['currency_name']                  = "Currency Name";
$lang['exchange_rate']                  = "Exchange Rate";
$lang['new_currency']                   = "Add Currency";
$lang['currency_added']                 = "Currency successfully added";
$lang['currency_updated']               = "Currency successfully updated";
$lang['currency_deleted']               = "Currency successfully deleted";
$lang['currencies_deleted']             = "Currencies successfully deleted";
$lang['add_customer_group']             = "Add Customer Group";
$lang['edit_customer_group']            = "Edit Customer Group";
$lang['delete_customer_group']          = "Delete Customer Group";
$lang['delete_customer_groups']         = "Delete Customer Groups";
$lang['percentage']                     = "Percentage";
$lang['group_name']                     = "Group Name";
$lang['group_percentage']               = "Group Percentage (without % sign)";
$lang['customer_group_added']           = "Product group successfully added";
$lang['customer_group_updated']         = "Product group successfully updated";
$lang['customer_group_deleted']         = "Product group successfully deleted";
$lang['customer_groups_deleted']        = "Product groups successfully deleted";
$lang['add_category']                   = "Add Category";
$lang['edit_category']                  = "Edit Category";
$lang['delete_category']                = "Delete Category";
$lang['delete_categories']              = "Delete Category";
$lang['category_code']                  = "Category Code";
$lang['category_name']                  = "Category Name";
$lang['category_added']                 = "Category successfully added";
$lang['category_updated']               = "Category successfully updated";
$lang['category_deleted']               = "Category successfully deleted";
$lang['categories_deleted']             = "Categories successfully deleted";
$lang['category_image']                 = "Category Image";
$lang['list_subcategories']             = "List Sub Categories";
$lang['add_subcategory']                = "Add Sub Category";
$lang['edit_subcategory']               = "Edit Sub Category";
$lang['delete_subcategory']             = "Delete Sub Category";
$lang['delete_subcategories']           = "Delete Sub Category";
$lang['parent_category']                = "Parent Category";
$lang['subcategory_code']               = "Sub Category Code";
$lang['subcategory_name']               = "Sub Category Name";
$lang['subcategory_added']              = "Sub Category successfully added";
$lang['subcategory_updated']            = "Sub Category successfully updated";
$lang['subcategory_deleted']            = "Sub Category successfully deleted";
$lang['subcategories_deleted']          = "Sub Categories successfully deleted";
$lang['tax_rate']                       = "Tax Rate";
$lang['add_tax_rate']                   = "Add Tax Rate";
$lang['edit_tax_rate']                  = "Edit Tax Rate";
$lang['delete_tax_rate']                = "Delete Tax Rate";
$lang['delete_tax_rates']               = "Delete Tax Rates";
$lang['fixed']                          = "Fixed";
$lang['type']                           = "Type";
$lang['rate']                           = "Rate";
$lang['tax_rate_added']                 = "Tax rate successfully added";
$lang['tax_rate_updated']               = "Tax rate successfully updated";
$lang['tax_rate_deleted']               = "Tax rate successfully deleted";
$lang['tax_rates_deleted']              = "Tax rates successfully deleted";
$lang['tax_rate_code_fe']               = "Código facturación electrónica";
$lang['warehouse']                      = "Warehouse";
$lang['add_warehouse']                  = "Add Warehouse";
$lang['edit_warehouse']                 = "Edit Warehouse";
$lang['deactivate_warehouse']               = "Delete Warehouse";
$lang['deactivate_warehouses']              = "Delete Warehouses";
$lang['code']                           = "Code";
$lang['name']                           = "Name";
$lang['map']                            = "map";
$lang['map_image']                      = "Map Image";
$lang['warehouse_map']                  = "Warehouse Map";
$lang['warehouse_added']                = "Warehouse successfully added";
$lang['warehouse_updated']              = "Warehouse successfully updated";
$lang['warehouse_deleted']              = "Warehouse successfully deleted";
$lang['warehouses_deleted']             = "Warehouse successfully deleted";
$lang['mail_message']                   = "Mail Message";
$lang['activate_email']                 = "Activation Email";
$lang['forgot_password']                = "Forgot Password";
$lang['short_tags']                     = "Short tags";
$lang['message_successfully_saved']     = "Message successfully saved";
$lang['group']                          = "User Group";
$lang['groups']                         = "User Groups";
$lang['add_group']                      = "Add User Group";
$lang['create_group']                   = "Add Group";
$lang['edit_group']                     = "Edit User Group";
$lang['delete_group']                   = "Delete User Group";
$lang['delete_groups']                  = "Delete User Groups";
$lang['group_id']                       = "User Group ID";
$lang['description']                    = "Description";
$lang['group_description']              = "Group Description";
$lang['change_permissions']             = "Change Permissions";
$lang['group_added']                    = "User group successfully added";
$lang['group_updated']                  = "User group successfully updated";
$lang['group_deleted']                  = "User group successfully deleted";
$lang['groups_deleted']                 = "User group successfully deleted";
$lang['permissions']                    = "Permissions";
$lang['set_permissions']                = "Please set group permissions below";
$lang['module_name']                    = "Module Name";
$lang['misc']                           = "Miscellaneous";
$lang['default_customer_group']         = "Default Customer Group";
$lang['paypal']                         = "Paypal";
$lang['paypal_settings']                = "Paypal Settings";
$lang['activate']                       = "Activate";
$lang['paypal_account_email']           = "Paypal Account Email";
$lang['fixed_charges']                  = "Fixed Charges";
$lang['account_email_tip']              = "Please type your paypal account email address";
$lang['fixed_charges_tip']              = "Any Fixed Extra charges for all payments through this gateway";
$lang['extra_charges_my']               = "Extra charges percentage for your country.";
$lang['extra_charges_my_tip']           = "Extra charges percentage for all payments from your country.";
$lang['extra_charges_others']           = "Extra charges percentage for other countries.";
$lang['extra_charges_others_tip']       = "Extra charges percentage for all payments from other countries.";
$lang['ipn_link']                       = "IPN Link";
$lang['ipn_link_tip']                   = "Add this link to your paypal account in order to activate IPN.";
$lang['paypal_setting_updated']         = "Paypal settings successfully updated";
$lang['skrill']                         = "Skrill";
$lang['skrill_account_email']           = "Skrill Account Email";
$lang['skrill_email_tip']               = "Please type your skrill account email address";
$lang['secret_word']                    = "Secret Word";
$lang['paypal_setting_updated']         = "Paypal settings successfully updated";
$lang['skrill_settings']                = "Skrill Settings";
$lang['secret_word_tip']                = "Please type your skrill secret word";
$lang['default_currency']               = "Default Currency";
$lang['auto_update_rate']               = "Auto Update Rate";
$lang['return_prefix']                  = "Return Sale Prefix";
$lang['product_variants_feature_x']     = "Product variants feature will not work with this option";
$lang['group_permissions_updated']      = "Group permissions successfully updated";
$lang['tax_invoice']                    = "Tax Invoice";
$lang['standard']                       = "Standard";
$lang['invoice_view']                   = "Invoice View";
$lang['restrict_user']                  = "Restrict User";
$lang['logo_image_tip']                 = "Max. file size 1024KB and (width=300px) x (height=80px).";
$lang['biller_logo_tip']                = "Please edit the biller after uploading the new logo and select newly updated logo.";
$lang['default_biller']                 = "Default Biller";
$lang['group_x_b_deleted']              = "Action failed, there are some users assigned to this group";
$lang['profit_loss']                    = "Profit and/or Loss";
$lang['staff']                          = "Staff";
$lang['stock_chartr']                   = "Stock Chart";
$lang['rtl_support']                    = "RTL Support";
$lang['backup_on']                      = "Backup taken on ";
$lang['restore']                        = "Restore";
$lang['download']                       = "Download";
$lang['file_backups']                   = "File Backups";
$lang['backup_files']                   = "Backup Files";
$lang['database_backups']               = "Database Backups";
$lang['db_saved']                       = "Database successfully saved.";
$lang['db_deleted']                     = "Database successfully deleted.";
$lang['backup_deleted']                 = "Backup successfully deleted.";
$lang['backup_saved']                   = "Backup successfully saved.";
$lang['backup_modal_heading']           = "Backing up your files";
$lang['backup_modal_msg']               = "Please wait, this could take few minutes.";
$lang['restore_modal_heading']          = "Restoring the backup files";
$lang['restore_confirm']                = "This action cannot be undone. Are you sure about this restore?";
$lang['delete_confirm']                 = "This action cannot be undone. Are you sure about this delete?";
$lang['restore_heading']                = "Please backup before restoring to any older version.";
$lang['full_backup']                    = 'Full Backup';
$lang['database']                       = 'Database';
$lang['files_restored']                 = 'Files successfully restored';
$lang['variants']                       = "Variants";
$lang['add_variant']                    = "Add Variant";
$lang['edit_variant']                   = "Edit Variant";
$lang['delete_variant']                 = "Delete Variant";
$lang['delete_variants']                = "Delete Variants";
$lang['variant_added']                  = "Variant successfully added";
$lang['variant_updated']                = "Variant successfully updated";
$lang['variant_deleted']                = "Variant successfully deleted";
$lang['variants_deleted']               = "Variants successfully deleted";
$lang['customer_award_points']          = 'Customer Award Points';
$lang['staff_award_points']             = 'Staff Award Points';
$lang['each_spent']                     = 'Each <i class="fa fa-arrow-down"></i> spent is equal to';
$lang['each_in_sale']                   = 'Each <i class="fa fa-arrow-down"></i> in sale is equal to';
$lang['mailpath']                       = "Mail Path";
$lang['smtp_crypto']                    = "SMTP Crypto";
$lang['random_number']                  = "Random Number";
$lang['sequence_number']                = "Sequence Number";
$lang['expense_prefix']                 = "Expense Prefix";
$lang['sac']                            = "South Asian Countries Currency Format";
$lang['qty_decimals']                   = "Quantity Decimals";
$lang['display_all_products']           = "Display warehouse products";
$lang['hide_with_0_qty']                = "Hide with 0 quantity";
$lang['show_with_0_qty']                = "Show all even with 0 quantity";
$lang['display_currency_symbol']        = "Display Currency Symbol";
$lang['currency_symbol']                = "Currency Symbol";
$lang['after']                          = "After";
$lang['before']                         = "Before";
$lang['remove_expired']                 = "Remove expired products from stock";
$lang['remove_automatically']           = "remove from stock automatically";
$lang['i_ll_remove']                    = "I will remove manually";
$lang['db_restored']                    = "Database successfully restored";
$lang['bulk_actions']                   = "Bulk actions";
$lang['barcode_separator']              = "Barcode separator";
$lang['dash']                           = "Dash ( - )";
$lang['dot']                            = "Dot ( . )";
$lang['tilde']                          = "Tilde ( ~ )";
$lang['underscore']                     = "Underscore ( _ )";
$lang['deposits']                       = "Deposits (view, add and edit)";
$lang['delete_deposit']                 = "Delete Deposit";
$lang['add_expense_category']           = "Add Expense Category";
$lang['edit_expense_category']          = "Edit Expense Category";
$lang['delete_expense_category']        = "Delete Expense Category";
$lang['delete_expense_categories']      = "Delete Expense Categories";
$lang['expense_category_added']         = "Expense category successfully added";
$lang['expense_category_updated']       = "Expense category successfully updated";
$lang['expense_category_deleted']       = "Expense category successfully deleted";
$lang['category_has_expenses']          = "Category with expenses cannot be deleted";
$lang['returnp_prefix']                 = "Return Purchase Prefix";
$lang['set_focus']                      = "Default Order Page Focus";
$lang['add_item_input']                 = "Add item input";
$lang['last_order_item']                = "Quantity input for last order item";
$lang['import_categories']              = "Import Categories";
$lang['categories_added']               = "Categories successfully imported";
$lang['parent_category_code']           = "Parent Category Code";
$lang['import_subcategories']           = "Import Subcategories";
$lang['subcategories_added']            = "Subcategories successfully imported";
$lang['import_expense_categories']      = "Import Expense Categories";
$lang['unit']                           = "Unit";
$lang['list_subunits']                  = "List Sub Units";
$lang['unit_code']                      = "Unit Code";
$lang['unit_name']                      = "Unit Name";
$lang['base_unit']                      = "Base Unit";
$lang['operator']                       = "Operator";
$lang['+']                              = "Plus (+)";
$lang['-']                              = "Minus (-)";
$lang['*']                              = "Multiply (*)";
$lang['/']                              = "Divide (/)";
$lang['operation_value']                = "Operation Value";
$lang['add_unit']                       = "Add Unit";
$lang['edit_unit']                      = "Edit Unit";
$lang['delete_unit']                    = "Delete Unit";
$lang['delete_units']                   = "Delete Units";
$lang['unit_added']                     = "Unit successfully added";
$lang['unit_updated']                   = "Unit successfully updated";
$lang['unit_deleted']                   = "Unit successfully deleted";
$lang['language_x_found']               = "System unable to find language files, please check if your language folder exists.";
$lang['default_price_group']            = "Default Price Group";
$lang['barcode_renderer']               = "Barcode Renderer";
$lang['svg']                            = "SVG";
$lang['disable_editing']                = "Number of days to disable editing";
$lang['add_price_group']                = "Add Price Group";
$lang['edit_price_group']               = "Edit Price Group";
$lang['delete_price_group']             = "Delete Price Group";
$lang['delete_price_groups']            = "Delete Price Groups";
$lang['price_group_added']              = "Product price group successfully added";
$lang['price_group_updated']            = "Product price group successfully updated";
$lang['price_group_deleted']            = "Product price group successfully deleted";
$lang['price_groups_deleted']           = "Product price groups successfully deleted";
$lang['no_price_group_selected']        = "No price group selected, please select at least one.";
$lang['no_customer_group_selected']     = "No customer group selected, please select at least one.";
$lang['group_product_prices']           = "Group Product Prices";
$lang['update_prices_csv']              = "Update Prices with CSV";
$lang['delete_product_group_prices']    = "Delete Group Price for Products";
$lang['products_group_price_updated']   = "Products group price successfully updated";
$lang['products_group_price_deleted']   = "Products group price successfully deleted";
$lang['ppayment_prefix']                = "Purchase Payment Prefix";
$lang['add_brand']                       = "Add Brand";
$lang['edit_brand']                      = "Edit Brand";
$lang['delete_brand']                    = "Delete Brand";
$lang['delete_brands']                   = "Delete Brands";
$lang['import_brands']                   = "Import Brands";
$lang['brand_added']                     = "Brand successfully added";
$lang['brands_added']                    = "Brands successfully added";
$lang['brand_updated']                   = "Brand successfully updated";
$lang['brand_deleted']                   = "Brand successfully deleted";
$lang['brands_deleted']                  = "Brands successfully deleted";
$lang['code_x_exist']                    = "Product code does not exist.";
$lang['price_updated']                   = "Products price successfully updated";
$lang['qa_prefix']                       = "Quantity Adjustment Prefix";
$lang['categories_import_tip']           = "If your csv file has subcategories (child categories - categories with parent category code) then parent categories will be imported first. You will need to import the same file again for subcategories (child categories).<br>Image and parent category code are optionals.";
$lang['update_cost_with_purchase']       = "Update cost with purchase";
$lang['edit_price_on_sale']              = "Edit Price on Sale";
$lang['login_logo']                      = "Login Logo";
$lang['unit_has_subunit']                = "Unit can not be deleted as it has sub-units";
$lang['brand_has_products']              = "Brand can not be deleted as it has products attachment, please assign products to different brand then try again.";
$lang['apis_feature']                    = "APIs Feature";
$lang['pdf_lib']                         = "PDF Library";
$lang['indian_gst']                      = "Indian GST";
$lang['biz_state']                       = "Business State";
$lang['not_recommended']                 = "Not Recommended";

//NO TRADUCIDO

$lang['product_tax'] = 'Product tax';
$lang['product_variants'] = 'Product variants';
$lang['quote_purchase_prefix'] = 'Purchase order prefix';
$lang['order_sale_prefix'] = 'Sales order prefix';
$lang['main_category'] = 'Main category';
$lang['update_heading'] = 'Update';
$lang['update_successful'] = 'Update successful';
$lang['using_latest_update'] = 'You are using the latest version.';
$lang['version'] = 'Version';
$lang['install'] = 'Install';
$lang['changelog'] = 'Change Log';
$lang['purchase_code'] = 'Purchase Code';
$lang['envato_username'] = 'Username';
$lang['adjust_quantity'] = 'Adjust quantity';
$lang['deposit_prefix'] = 'Deposits prefix';
$lang['prioridad_precios_producto'] = 'Prices priority';
$lang['descuento_orden'] = '¿Discounts affect taxes?';
$lang['descuento_orden_decidir'] = 'Choose at purchase';
$lang['descuento_orden_afecta_iva'] = 'Discounts always affect taxes';
$lang['descuento_orden_no_afecta_iva'] = 'Discounts never affect taxes';


/* Electronic billing*/
$lang['seccion_title_electronic_billing'] = 'Electronic billing';

  // Label
  $lang['label_city'] = 'City';
  $lang['label_state'] = 'State';
  $lang['label_address'] = 'Address';
  $lang['label_locality'] = 'Locality';
  $lang['label_first_name'] = 'First name';
  $lang['label_trade_name'] = 'Trade name';
  $lang['label_type_person'] = 'Type person';
  $lang['label_second_name'] = 'Second name';
  $lang['label_business_name'] = 'Business name';
  $lang['label_document_type'] = 'Document type';
  $lang['label_type_vat_regime'] = "Regime type";
  $lang['label_first_lastname'] = 'First lastname';
  $lang['label_second_lastname'] = 'Second lastname';
  $lang['label_document_number'] = 'Document number';
  $lang['label_commercial_register'] = 'Commercial register';

  // Text
  $lang['legal_person'] = 'Legal';
  $lang['natural person'] = 'Natural';
  $lang['great_contributor'] = 'Great contributor';
/*********************/

$lang['edit_payment_method']="Edit payment method";
$lang['delete_payment_method']="Delete payment method";
$lang['delete_payment_methods']="Delete payment methods";
$lang['add_payment_method']="Add payment method";

$lang['state_sale']="Sale status";
$lang['state_purchase']="Purchase status";

$lang['payment_method_added'] = "The payment method was added succesfully";
$lang['payment_method_updated'] = "The payment method was updated succesfully";

$lang['cashier_close_register'] = "Cashier can close register";
$lang['allow_change_sale_iva']="Allow to change the tax of the products in the sale";
$lang['alert_sale_expired']="Days for payment term expiry alert";
$lang['resolucion_porc_aviso']="Remaining percentage of notice for resolution due by numbering";
$lang['resolucion_dias_aviso']="Remaining notice days for expiration of resolution by expiration date";

$lang['payment_method_has_payments'] = 'You can not delete the payment method because payments have already been registered with this method';
$lang['payment_method_deleted'] = 'The payment method was successfully deleted';
$lang['delete_payment_method'] = 'Delete payment method';

$lang['currency_has_movements'] = 'The Currency selected can not be deleted because has movements';

$lang['get_companies_check_digit'] = 'Calculate the check digit for third-party NIT';

$lang['FIFO'] = 'FIFO (First In First Out)';
$lang['AVCO'] = 'AVCO (Average Cost Method)';

$lang['rounding'] = 'Round figures';


$lang['purchase_payment_affects_cash_register'] = "Do payments to purchases affect the cash register?";
$lang['purchase_payments_never_affects_register'] = "Never affects the cash register";
$lang['purchase_payments_always_affects_register'] = "Always affects the cash register";
$lang['always_ask_if_purchase_payments_affects_register'] = "Decide when registering the payment";

$lang['use_cost_center'] = 'Use the cost centers';

$lang['cost_center_selection'] = 'Method to define the cost center';
$lang['default_biller_cost_center'] = 'Defined cost center in biller';
$lang['select_cost_center'] = 'Define cost center in each record';
$lang["advanced_search"] = "Advanced search";
$lang["brands"] = "Brands";
$lang['default_cost_center'] = 'Default cost center';
$lang["enlist_customer"] = "Enlist customer";
$lang["electronic_billing"] = "Electronic billing";
$lang["check_digit"] = "Check digit";

$lang['technology_provider'] = 'Technology provider';
$lang['electronic_billing_user'] = 'User';
$lang['electronic_billing_password'] = 'Password';
$lang['mean_payment_code_fe'] = 'Mean of payment code (DIAN)';

$lang['hide_products_in_zero_price'] = 'Hide products priced at 0 in the list';

$lang['hide'] = 'Hide';
$lang['show'] = 'Show';

$lang['price_group_base_with_promotions'] = 'Base price list plus promotions';
$lang['biller_price_group'] = 'Price list assigned to branch office';
$lang['customer_price_group'] = 'Price list assigned to customer';
$lang['biller_price_group_with_discounts'] = 'List assigned to branch / customer, plus promotions and customer discounts.';
$lang['customer_price_group_with_discounts'] = 'List assigned to customer / branch, plus promotions and customer discounts.';

$lang['select_each_product'] = 'Choose in each product';

$lang['dont_show'] = "Dont show";

$lang['customer_default_country'] = 'Default country for Customers';
$lang['customer_default_state'] = 'Default Department for Customers';
$lang['customer_default_city'] = 'Default city for Customers';

$lang['set_adjustment_cost'] = 'Record cost in adjustments';
$lang["work_environment"] = "Work environment";
$lang["contingency_invoice"] = "Contingency invoice";
$lang["test_resolution"] = "Test environment";
$lang["test"] = "Test";
$lang["production"] = "Production";
$lang["contingency"] = "Contingency";
$lang["setTestId"] = "TestId";


/**
 * Lang Restobar
 */

$lang["restobar"] = "Tables";

$lang["restobar_title_add"] = "Add table";

$lang["restobar_label_new"] = "New";
$lang["restobar_label_table_number"] = "Number";
$lang["restobar_label_table_shape"] = "Shape";
$lang["restobar_label_number_table_seats"] = "Numbre of seats";
$lang["restobar_label_table_state"] = "State";
$lang["restobar_label_table_width"] = "Width";
$lang["restobar_label_table_height"] = "Height";
$lang["restobar_label_table_size"] = "Size";
$lang["restobar_label_table_radius"] = "Radius";
$lang["restobar_label_x_position_table"] = "X position";
$lang["restobar_label_y_position_table"] = "Y position";
$lang["restobar_label_available_status"] = "Available";
$lang["restobar_label_not_available_status"] = "Not available";
$lang["restobar_label_occuped_status"] = "Occuped";
$lang["restobar_label_reserved_status"] = "Reserved";
$lang["restobar_label_square_shape"] = "Square";
$lang["restobar_label_circle_shape"] = "Circle";
$lang["restobar_label_rectangular_shape"] = "Rectangular";
$lang["restobar_label_ellipse_shape"] = "Ellipse";
$lang["restobar_label_bar_shape"] = "Bar";

/*
|
|---------------------------------
| Document Types
|---------------------------------
|
*/

$lang["document_type_add_automatic"] = "add automatic document type";
$lang["document_types_added_successfully"] = "types of documents added successfully";
$lang["document_types_already_updated"] = "The document types are already updated";
$lang["period_days_allowed_before_current_date"] = "period days allowed before current date";
$lang["period_days_allowed_after_current_date"] = "period days allowed after current date";



/*
|-------------------------------
| SETTINGS UPDATE 
|-------------------------------
*/
$lang['user_fields_changed_settings']                    = '%s made the following changes to %s ';
$lang['from']                                            = 'FROM';
$lang['to']                                              = 'TO';
$lang['las_update']                                      = 'Last update';
$lang['customer_branches_language']                      = 'Change customer branch text';
$lang['city_code']                                       = 'Code city';
$lang['fuente_retainer']                                 = 'self-retainer font';
$lang['iva_retainer']                                    = 'self-retainer vat';
$lang['ica_retainer']                                    = 'self-retainer ica';
$lang['mmode']                                           = 'Maintenance mode';
$lang['rtl']                                             = 'rtl support';
$lang['restrict_calendar']                               = 'Calendar';
$lang['default_tax_rate']                                = 'Product tax';
$lang['iheight']                                         = 'Image height';
$lang['iwidth']                                          = 'Image width';
$lang['twidth']                                          = 'Small image width';
$lang['theight']                                         = 'Small image height';
$lang['barcode_img']                                     = 'Barcode Renderer';
$lang['update_cost']                                     = 'Update cost with purchase';
$lang['purchase_tax_rate']                               = 'Order purchases TAX';
$lang['tax3']                                            = 'Discounts affect VAT';
$lang['default_tax_rate2']                               = 'Order sales TAX';
$lang['cashier_close']                                   = "Cashier can close register";
$lang["allow_advanced_search"]                           = "Advanced search";
$lang["days_before_current_date"]                        = "period days allowed before current date";
$lang["days_after_current_date"]                         = "period days allowed after current date";
$lang['fe_technology_provider']                          = 'Technology provider';
$lang["fe_work_environment"]                             = "Work environment";  
$lang["fe_technical_annex_version"]                      = "Technical version";
$lang['display_symbol']                         		 = "Display Currency Symbol";
$lang['protocol']                 				         = "Email Protocol";
$lang['ca_point']                                        = "Customer award points";
$lang['each_sale']                                       = 'Each sale made is equal to';
$lang['sa_point']                                        = 'Employee award points';
