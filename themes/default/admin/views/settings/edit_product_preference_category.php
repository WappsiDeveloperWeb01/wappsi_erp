<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <style type="text/css">
            .input-edit{
                color: black;
                border-color: #eaeaea;
                border-width: 1px;
            }
        </style>
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('edit_product_preference_category'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/edit_product_preference_category/".$category->id, $attrib); ?>
        <div class="modal-body">

            <div class="form-group">
                <input type="hidden" name="edit_product_preference_category" value="1">
                <?= lang('category_name', 'name'); ?>
                <?= form_input('name', $category->name, 'class="form-control" id="name" required="required"'); ?>
            </div>

            <div class="form-group">
                <?= lang('selection_limit', 'selection_limit'); ?> <em>Dejar en 0 para no establecer límite</em>
                <?= form_input('selection_limit', $category->selection_limit, 'class="form-control" id="selection_limit"'); ?>
            </div>

            <div class="form-group">
                <?= lang('required', 'required'); ?>
                <br>
                <label>
                    <input type="radio" name="required" value="1" <?= $category->required == 1 ? 'checked' : '' ?>>
                    <?= lang('yes') ?>
                </label>
                <label>
                    <input type="radio" name="required" value="0" <?= $category->required != 1 ? 'checked' : '' ?>>
                    <?= lang('no') ?>
                </label>
            </div>

            <div class="form-group">
                <?= lang('preferences', 'preferencesInput'); ?>
                <div class="form-group" id="ui" style="margin-bottom: 0;">
                    <div class="input-group">
                        <?php echo form_input('preferencesInput', '', 'class="form-control select-tags-preferences" id="preferencesInput" placeholder="' . $this->lang->line("enter_preferences") . '"'); ?>
                        <div class="input-group-addon" style="padding: 2px 5px;">
                            <a href="#" id="addPreferences">
                                <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                            </a>
                        </div>
                    </div>
                    <div style="clear:both;"></div>
                </div>
            </div>
            <?php if ($category_preferences): ?>
                <?php foreach ($category_preferences as $cprf): ?>
                    <button class="btn btn-success btn-outline" type="button" style="margin-right:2%;">
                        <span class="delete_preference fa fa-times" style="font-size: 110%;"></span>
                        <input type="hidden" name="preferences_id[]" value="<?= $cprf->id ?>">
                        <input type="text" name="preferences_name[]" value="<?= $cprf->name ?>" class="input-edit">
                    </button>
                <?php endforeach ?>
            <?php endif ?>
            <div class="area_preferences">
            </div>
        </div>
        <div class="modal-footer">
            <?= form_submit('edit_product_preference_category', lang('submit'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

<script type="text/javascript">
    var variants = [];
    $(document).ready(function(){
        $(".select-tags-preferences").select2({
            tags: variants,
            tokenSeparators: [","],
            multiple: true
        });
    });


    $(document).on('click', '#addPreferences', function (e) {
        e.preventDefault();
        var attrs_val = $('#preferencesInput').val(), attrs;
        attrs = attrs_val.split(',');
        for (var i in attrs) {
            if (attrs[i] !== '') {
                if (!validate_preference_name_exists(attrs[i])) {
                    $('.area_preferences').append('<button class="btn btn-sm btn-success btn-outline" type="button" style="margin-right:2%;">'+
                                                        '<span class="delete_preference fa fa-times" style="font-size: 110%;"></span>  '+
                                                        '<input type="hidden" name="preferences_name[]" value="'+attrs[i]+'">'+
                                                        attrs[i]+
                                                    '</button>');
                } else {
                    command: toastr.warning('Ya existe la preferencia '+attrs[i]+' para la categoría', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                }
            }
        }
        $('#preferencesInput').select2('val', '')
    });

    $(document).on('click', '.delete_preference', function(){
        index = $(this).index('.delete_preference');
        id = $('input[name="preferences_id[]"]').eq(index);
        $('.area_preferences').append('<input type="hidden" name="delete_preference_id[]" value="'+id.val()+'">');
        $(this).parent('.btn').remove();
    });

    function validate_preference_name_exists(name){
        exist = false;
        $('input[name="preferences_name[]"]').each(function(index, input){
            if ($(input).val() == name) {
                exist = true;
            }
        });
        return exist;
    }

</script>

<?= $modal_js ?>