<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <?= admin_form_open("payroll_concepts/update", ["id"=>"edit_type_concept_form"]); ?>
            <?= form_hidden('id', $type_concept_data->id); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h3 class="modal-title" id="myModalLabel"><?= $page_title; ?></h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("payroll_concepts_type"), "concept_type_id"); ?>
                            <?php $topts = [""=>lang('select')]; ?>
                            <?php
                                foreach ($types_concepts as $type_concept) {
                                    $topts[$type_concept->id] = $type_concept->name;
                                }
                            ?>
                            <?= form_dropdown(["name"=>"concept_type_id", "id"=>"concept_type_id", "class"=>"form-control select2", "disabled"=>TRUE, "required"=>TRUE], $topts, $type_concept_data->concept_type_id); ?>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang('payroll_concepts_earned_deducted'), 'earned_deducted'); ?>
                            <?= form_dropdown(["name"=>"earned_deducted", "id"=>"earned_deducted", "class"=>"form-control select2", "disabled"=>TRUE], [1=>lang("payroll_management_earnings"), 2=>lang("payroll_management_deductions")], $type_concept_data->type); ?>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("payroll_concepts_name"), "name"); ?>
                            <?= form_input(["name"=>"name", "id"=>"name", "class"=>"form-control", "required"=>TRUE], $type_concept_data->name); ?>
                        </div>
                    </div>

                    <div class="col-sm-12">
                        <div class="form-group">
                            <?= form_label(lang("payroll_concepts_status", "status")); ?>
                            <?php $sopts = [YES=>lang("active"), NOT=>lang("inactive")]; ?>
                            <?= form_dropdown(["name"=>"status", "id"=>"status", "class"=>"form-control select2", "required"=>TRUE], $sopts, $type_concept_data->status); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <?= form_button(["name"=>"update_button", "id"=>"update_button", "class"=>"btn btn-primary"], lang("save")); ?>
            </div>
        <?= form_close(); ?>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.select2').select2();


        $(document).on('click', '#update_button', function() { save_type_concept(); });
    });

    function save_type_concept()
    {
        $("#edit_type_concept_form").validate({
            ignore: [],
            rules: {
                concept_type_id:"required"
            },
            messages: {
                concept_type_id:"Este campo es obligatorio"
            },
            highlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                } else {
                    elem.addClass(errorClass);
                }
            },
            unhighlight: function (element, errorClass, validClass) {
                var elem = $(element);
                if (elem[0].tagName == "SELECT") {
                    $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                    $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                } else {
                    elem.removeClass(errorClass);
                }
            }
        });

        if ($('#edit_type_concept_form').valid()) {
            $('#edit_type_concept_form').submit();
        }
    }

</script>
