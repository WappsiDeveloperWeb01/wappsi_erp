<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
            <h4 class="modal-title" id="myModalLabel"><?= lang('delete_product_color'); ?></h4>
        </div>
        <?= admin_form_open("system_settings/deleteColor/$color->id", ['data-toggle' => 'validator', 'role' => 'form']); ?>
        <div class="modal-body">
            <div class="form-group">
                <label for="name"><?= $this->lang->line("name"); ?></label>
                <div class="controls">
                    <?= form_input('name', $color->name, 'class="form-control" id="name" readonly'); ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <p><?= lang('r_u_sure'); ?></p>
            <button type="button" class="btn btn-default new-button" onclick="$('#myModal ').modal('hide')"><i class="fas fa-times"></i></button>
            <?= form_button(['name'=>'saveColor', 'id'=>'saveColor', 'type'=>'submit', 'class'=>'btn btn-primary new-button', 'data-toogle-second'=>'tooltip', 'title'=>$this->lang->line('save'), 'content'=>'<i class="fas fa-check"></i>']); ?>
        </div>
    </div>
    <?= form_close(); ?>
</div>

<?= $modal_js ?>

<script>
    $(document).ready(function () {
        $('[data-toggle-second="tooltip"]').tooltip();
    });
</script>