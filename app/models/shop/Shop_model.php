<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Shop_model extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    public function getSpecialPrice() {
        $sp = new stdClass();
        $sp->cgp = ($this->customer && $this->customer->price_group_id) ? "( SELECT {$this->db->dbprefix('product_prices')}.price as price, {$this->db->dbprefix('product_prices')}.product_id as product_id, {$this->db->dbprefix('product_prices')}.price_group_id as price_group_id from {$this->db->dbprefix('product_prices')} WHERE {$this->db->dbprefix('product_prices')}.price_group_id = {$this->customer->price_group_id} ) cgp" : NULL;

        $sp->wgp = ($this->warehouse && $this->warehouse->price_group_id && !$this->customer) ? "( SELECT {$this->db->dbprefix('product_prices')}.price as price, {$this->db->dbprefix('product_prices')}.product_id as product_id, {$this->db->dbprefix('product_prices')}.price_group_id as price_group_id from {$this->db->dbprefix('product_prices')} WHERE {$this->db->dbprefix('product_prices')}.price_group_id = {$this->warehouse->price_group_id} ) wgp" : NULL;

        return $sp;
    }

    public function getSettings() {
        return $this->db->get('settings')->row();
    }

    public function getShopSettings() {
        return $this->db->get('shop_settings')->row();
    }

    public function getCustomerGroup($id) {
        return $this->db->get_where('customer_groups', ['id' => $id])->row();
    }

    public function getPriceGroup($id) {
        return $this->db->get_where('price_groups', ['id' => $id])->row();
    }

    public function getDateFormat($id) {
        return $this->db->get_where('date_format', ['id' => $id], 1)->row();
    }

    public function addCustomer($data) {
        if ($this->db->insert('companies', $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function addWishlist($product_id) {
        $user_id = $this->session->userdata('user_id');
        if (!$this->getWishlistItem($product_id, $user_id)) {
            return $this->db->insert('wishlist', ['product_id' => $product_id, 'user_id' => $user_id]);
        }
        return FALSE;
    }

    public function removeWishlist($product_id) {
        $user_id = $this->session->userdata('user_id');
        return $this->db->delete('wishlist', ['product_id' => $product_id, 'user_id' => $user_id]);
    }

    public function getWishlistItem($product_id, $user_id) {
        return $this->db->get_where('wishlist', ['product_id' => $product_id, 'user_id' => $user_id])->row();
    }

    public function getAllCurrencies() {
        return $this->db->get('currencies')->result();
    }

    public function getNotifications() {
        $date = date('Y-m-d H:i:s', time());
        $this->db->where("from_date <=", $date)
        ->where("till_date >=", $date)->where('scope !=', 2);
        return $this->db->get("notifications")->result();
    }

    public function getAddresses() {
        return $this->db->get_where("addresses", ['company_id' => $this->session->userdata('company_id')])->result();
    }

    public function getCurrencyByCode($code) {
        return $this->db->get_where('currencies', ['code' => $code], 1)->row();
    }

    public function getAllCategories() {
        if ($this->shop_settings->show_categories_without_products == 0) {
            $this->db->select('categories.*')
                    ->join('products', 'products.category_id = categories.id')
                    ->group_by('categories.id')
                    ->where('(categories.parent_id IS NULL or categories.parent_id = 0)')
                    ->where('products.name IS NOT NULL')
                    ->where('categories.hide_ecommerce', 0)
                    ->order_by('name ASC')
                    ;
            return $this->db->get('categories')->result();
        } else {
            $this->db->where('parent_id', NULL)->or_where('parent_id', 0)->order_by('name');
            return $this->db->get("categories")->result();
        }
    }

    public function getSubCategories($parent_id) {
        if ($this->shop_settings->show_categories_without_products == 0) {
            $this->db->select('categories.*')
                    ->join('products', 'products.subcategory_id = categories.id')
                    ->group_by('categories.id')
                    ->where('categories.parent_id', $parent_id)
                    ->where('products.name IS NOT NULL')
                    ->order_by('name ASC');
            return $this->db->get('categories')->result();
        } else {
            $this->db->where('parent_id', $parent_id)->order_by('name');
            return $this->db->get("categories")->result();
        }
    }

    public function getCategoryBySlug($slug) {
        return $this->db->get_where('categories', ['slug' => $slug], 1)->row();
    }

    public function getAllBrands($category_slug = null, $subcategory_slug = null) {
        if ($category_slug || $subcategory_slug) {
            if ($category_slug) {
                $cs = "(
                        SELECT 
                            PR.brand
                        FROM {$this->db->dbprefix('products')} PR 
                            INNER JOIN {$this->db->dbprefix('categories')} CAT ON CAT.id = PR.category_id
                            WHERE CAT.slug = '{$category_slug}'
                            GROUP BY PR.brand
                        ) AS cs";
            }
            if ($subcategory_slug) {
                $scs = "(
                        SELECT 
                            PR.brand
                        FROM {$this->db->dbprefix('products')} PR 
                            INNER JOIN {$this->db->dbprefix('categories')} SUBCAT ON SUBCAT.id = PR.subcategory_id
                            WHERE SUBCAT.slug = '{$subcategory_slug}'
                            GROUP BY PR.brand
                        ) AS scs";
            }
            $this->db->select('brands.*');
            if ($category_slug) {
                $this->db->join($cs, "cs.brand = {$this->db->dbprefix('brands')}.id");
            }
            if ($subcategory_slug) {
                $this->db->join($scs, "scs.brand = {$this->db->dbprefix('brands')}.id");
            }
            return $this->db->get('brands')->result();
        } else{
            $pc = "(SELECT count(*) FROM {$this->db->dbprefix('products')} WHERE {$this->db->dbprefix('products')}.brand = {$this->db->dbprefix('brands')}.id)";
            $this->db->select("{$this->db->dbprefix('brands')}.*, {$pc} AS product_count", false)->order_by('name');
            if ($this->shop_settings->hide0) {
                $this->db->where("{$pc} >", 0);
            }
            return $this->db->get('brands')->result();
        }
    }

    public function getAllOptions($category_slug = null, $subcategory_slug = null) {
        if ($category_slug || $subcategory_slug) {
            if ($category_slug) {
                $cs = "(
                        SELECT 
                            PRV.name
                        FROM {$this->db->dbprefix('product_variants')} PRV
                            INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = PRV.product_id
                            INNER JOIN {$this->db->dbprefix('categories')} CAT ON CAT.id = PR.category_id
                            WHERE CAT.slug = '{$category_slug}'
                            GROUP BY PRV.name
                        ) AS cs";
            }
            if ($subcategory_slug) {
                $scs = "(
                        SELECT 
                            PRV.name
                        FROM {$this->db->dbprefix('product_variants')} PRV
                            INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = PRV.product_id
                            INNER JOIN {$this->db->dbprefix('categories')} SUBCAT ON SUBCAT.id = PR.subcategory_id
                            WHERE SUBCAT.slug = '{$subcategory_slug}'
                            GROUP BY PRV.name
                        ) AS scs";
            }
            $this->db->select('product_variants.*');
            if ($category_slug) {
                $this->db->join($cs, "cs.name = {$this->db->dbprefix('product_variants')}.name");
            }
            if ($subcategory_slug) {
                $this->db->join($scs, "scs.name = {$this->db->dbprefix('product_variants')}.name");
            }
            return $this->db->group_by('product_variants.name')->get('product_variants')->result();
        } else{
            $pc = "(SELECT count(*) FROM {$this->db->dbprefix('products')} WHERE {$this->db->dbprefix('products')}.id = {$this->db->dbprefix('product_variants')}.product_id)";
            $this->db->select("{$this->db->dbprefix('product_variants')}.*, {$pc} AS product_count", false)->order_by('name');
            if ($this->shop_settings->hide0) {
                $this->db->where("{$pc} >", 0);
            }
            return $this->db->group_by('product_variants.name')->get('product_variants')->result();
        }
    }

    public function getBrandBySlug($slug) {
        return $this->db->get_where('brands', ['slug' => $slug], 1)->row();
    }

    public function getUserByEmail($email) {
        return $this->db->get_where('users', ['email' => $email], 1)->row();
    }

    public function getAllPages() {
        $this->db->select('name, slug')->order_by('order_no asc');
        return $this->db->get_where("pages", ['active' => 1])->result();
    }

    public function getPageBySlug($slug) {
        return $this->db->get_where('pages', ['slug' => $slug], 1)->row();
    }

    public function getFeaturedProducts($limit = 16, $promo = TRUE) {

        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, quantity, type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->where('products.featured', 1)
        ->where('products.hide_online_store !=', 1)
        ->limit($limit);
        if ($promo) {
            $this->db->order_by('promotion desc');
        }
        $this->db->order_by('RAND()');
        return $this->db->get("products")->result();
    }

    public function getPromoProducts($limit = 16) {

        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, quantity, type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->where('products.hide_online_store !=', 1)
        ->where('(promotion = 1 AND start_date <= "'.date('Y-m-d').'" AND end_date >= "'.date('Y-m-d').'" )')
        ->limit($limit);
        $this->db->order_by('RAND()');
        return $this->db->get("products")->result();
    }

    public function getNewProducts($limit = 16) {

        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, quantity, type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->where('products.hide_online_store !=', 1)
        ->where('DATEDIFF("' . date('Y-m-d H:i:s') . '", ' . $this->db->dbprefix('products') . '.registration_date) <=', $this->Settings->days_to_new_product)
        ->limit($limit);
        $this->db->order_by('RAND()');
        return $this->db->get("products")->result();
    }

    public function getBestSellingProducts($limit = 16, $days = 30, $max = 10) {


        $this->create_best_selling_temp_table($max, $days);

        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, quantity, type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->join('best_selling', 'best_selling.product_id = products.id', 'inner')
        ->where('products.hide_online_store !=', 1)
        ->limit($limit);
        $this->db->order_by('RAND()');
        return $this->db->get("products")->result();
    }

    public function getProducts($filters = [], $counting = false, $days = 30, $max = 10) {

        $this->site->create_temporary_product_billers_assoc($this->shop_settings->biller);
        $this->db->select("
                            {$this->db->dbprefix('products')}.*,
                            {$this->db->dbprefix('products')}.product_details as details,
                            {$this->db->dbprefix('warehouses_products')}.quantity as quantity
                        ")
        ->join('products_billers_assoc_'.$this->shop_settings->biller.' AS products_billers_assoc', 'products_billers_assoc.product_id = products.id', 'inner')
        ->join('brands', 'brands.id=products.brand', 'left')
        ->from("products")
        ->join('warehouses_products', 'products.id=warehouses_products.product_id', 'left');
        if (isset($filters['options']) && count($filters['options']) > 0) {
            $options_sql = '(';
            foreach ($filters['options'] as $key => $option) {
                if ($key == 0) {
                    $options_sql.=$this->db->dbprefix('product_variants').'.name = "'.$option.'"';
                } else {
                    $options_sql.=' or '.$this->db->dbprefix('product_variants').'.name = "'.$option.'"';
                }
            }
            $options_sql .= ')';
            $this->db->join('product_variants', $this->db->dbprefix('product_variants').'.product_id = '.$this->db->dbprefix('products').'.id AND '.$options_sql, 'inner');
        }
        if (isset($filters['best_selling']) && $filters['best_selling']) {
            $this->create_best_selling_temp_table($max, $days);
            $this->db->join('best_selling', 'best_selling.product_id = products.id', 'inner');
        }
        if ($this->shop_settings->warehouse) {
            $this->db->where('warehouses_products.warehouse_id', $this->shop_settings->warehouse);
        }
        $this->db->group_by('products.id');

        if (!$counting) {
            $this->db->where('products.hide_online_store !=', 1)->limit($filters['limit'], $filters['offset']);
        } else {
            $this->db->where('products.hide_online_store !=', 1);
        }

        if (!empty($filters)) {
            if (isset($filters['promo']) && !empty($filters['promo'])) {
                $today = date('Y-m-d');
                $this->db->where('promotion', 1)->where('start_date <=', $today)->where('end_date >=', $today);
            }
            if (isset($filters['featured']) && !empty($filters['featured'])) {
                $this->db->where($this->db->dbprefix('products').'.featured', 1);
            }
            if (isset($filters['new']) && !empty($filters['new'])) {
                $this->db->where('DATEDIFF("' . date('Y-m-d H:i:s') . '", ' . $this->db->dbprefix('products') . '.registration_date) <=', $this->Settings->days_to_new_product);
            }
            if (isset($filters['query']) && !empty($filters['query'])) {
                $this->db->group_start()->like($this->db->dbprefix('products').'.name', $filters['query'], 'both')->or_like($this->db->dbprefix('products').'.code', $filters['query'], 'both')->group_end();
            }
            if (isset($filters['category']) && !empty($filters['category'])) {
                $this->db->where($this->db->dbprefix('products').'.category_id', $filters['category']['id']);
            }
            if (isset($filters['subcategory']) && !empty($filters['subcategory'])) {
                $this->db->where($this->db->dbprefix('products').'.subcategory_id', $filters['subcategory']['id']);
            }
            if (isset($filters['brand']) && !empty($filters['brand'])) {
                $this->db->where($this->db->dbprefix('products').'.brand', $filters['brand']['id']);
            }
            if (isset($filters['min_price']) && !empty($filters['min_price'])) {
                $this->db->where($this->db->dbprefix('products').'.price >=', $filters['min_price']);
            }
            if (isset($filters['max_price']) && !empty($filters['max_price'])) {
                $this->db->where($this->db->dbprefix('products').'.price <=', $filters['max_price']);
            }
            if ($this->shop_settings->show_product_zero_quantity == 0) {
                $this->db->where($this->db->dbprefix('warehouses_products').'.quantity !=', 0);
            }
            if (isset($filters['in_stock']) && !empty($filters['in_stock'])) {
                $this->db->group_start()->where('warehouses_products.quantity >=', 1)->or_where('type !=', 'standard')->group_end();
            }
            if (isset($filters['in_stock']) && count($filters['brands']) > 0) {
                $brands_sql = '(';
                foreach ($filters['brands'] as $key => $brand) {
                    if ($key == 0) {
                        $brands_sql.='products.brand = '.$brand;
                    } else {
                        $brands_sql.=' or products.brand = '.$brand;
                    }
                }
                $brands_sql .= ')';
                $this->db->where($brands_sql);
            }
            if (isset($filters['sorting']) && !empty($filters['sorting'])) {
                $sort = explode('-', $filters['sorting']);
                $this->db->order_by($sort[0], $this->db->escape_str($sort[1]));
            } else {
                $this->db->order_by('products.name asc');
            }
        } else {
            $this->db->order_by('products.name asc');
        }

        $q = $this->db->get();
        $data = [];
        foreach (($q->result()) as $key => $value) {
            $value->units = false;
            $value->variants = $this->shop_model->getProductOptions($value->id);
            $value->preferences = $this->shop_model->getProductPreferences($value->id);
            if (
                $this->Settings->prioridad_precios_producto == 5 || 
                $this->Settings->prioridad_precios_producto == 7 || 
                $this->Settings->prioridad_precios_producto == 10 || 
                $this->Settings->prioridad_precios_producto == 11
                ) {
                $value->units = $this->site->get_product_units($value->id);
                $cg_arr_units = json_decode($this->customer_group->units);
                if ($value->units) {
                    foreach ($value->units as $key => $unit_pos) {
                        if (in_array($unit_pos->product_unit_id, $cg_arr_units)) {
                            unset($value->units[$key]);
                        }
                    }
                }
            }
            $data[] = $value;
        }

        return $data;
    }

    public function getProductsBrands($filters = [], $days = 30, $max = 10) {

        $this->db->select("
                            products.brand as id,
                            brands.name,
                            brands.slug
                        ")

        ->from("products")
        ->join('warehouses_products', 'products.id = warehouses_products.product_id', 'left')
        ->join('brands', 'brands.id = products.brand', 'left');
        if ($filters['best_selling']) {
            $this->create_best_selling_temp_table($max, $days);
            $this->db->join('best_selling', 'best_selling.product_id = products.id', 'inner');
        }
        $this->db->where('warehouses_products.warehouse_id', $this->shop_settings->warehouse)
        ->group_by('products.brand');

        $this->db->where('products.hide_online_store !=', 1);

        if (!empty($filters)) {
            if (!empty($filters['promo'])) {
                $today = date('Y-m-d');
                $this->db->where('promotion', 1)->where('start_date <=', $today)->where('end_date >=', $today);
            }
            if (!empty($filters['featured'])) {
                $this->db->where($this->db->dbprefix('products').'.featured', 1);
            }
            if (!empty($filters['query'])) {
                $this->db->group_start()->like($this->db->dbprefix('products').'.name', $filters['query'], 'both')->or_like($this->db->dbprefix('products').'.code', $filters['query'], 'both')->group_end();
            }
            if (!empty($filters['category'])) {
                $this->db->where($this->db->dbprefix('products').'.category_id', $filters['category']['id']);
            }
            if (!empty($filters['subcategory'])) {
                $this->db->where($this->db->dbprefix('products').'.subcategory_id', $filters['subcategory']['id']);
            }
            if (!empty($filters['brand'])) {
                $this->db->where($this->db->dbprefix('products').'.brand', $filters['brand']['id']);
            }
            if (!empty($filters['min_price'])) {
                $this->db->where($this->db->dbprefix('products').'.price >=', $filters['min_price']);
            }
            if (!empty($filters['max_price'])) {
                $this->db->where($this->db->dbprefix('products').'.price <=', $filters['max_price']);
            }
            if ($this->shop_settings->show_product_zero_quantity == 0) {
                $this->db->where($this->db->dbprefix('warehouses_products').'.quantity !=', 0);
            }
            if (!empty($filters['in_stock'])) {
                $this->db->group_start()->where('warehouses_products.quantity >=', 1)->or_where('type !=', 'standard')->group_end();
            }
        }
        $this->db->order_by('brands.name asc');
        return $this->db->get()->result();
    }

    public function getProductsOptions($filters = [], $days = 30, $max = 10) {

        $this->db->select("
                            variants.id as id,
                            variants.name
                        ")

        ->from("products")
        ->join('warehouses_products', 'products.id = warehouses_products.product_id', 'left')
        ->join('product_variants', 'product_variants.product_id = products.id')
        ->join('variants', 'variants.name = product_variants.name');
        if ($filters['best_selling']) {
            $this->create_best_selling_temp_table($max, $days);
            $this->db->join('best_selling', 'best_selling.product_id = products.id', 'inner');
        }
        $this->db->where('warehouses_products.warehouse_id', $this->shop_settings->warehouse)
        ->group_by('variants.name');

        $this->db->where('products.hide_online_store !=', 1);

        if (!empty($filters)) {
            if (!empty($filters['promo'])) {
                $today = date('Y-m-d');
                $this->db->where('promotion', 1)->where('start_date <=', $today)->where('end_date >=', $today);
            }
            if (!empty($filters['featured'])) {
                $this->db->where($this->db->dbprefix('products').'.featured', 1);
            }
            if (!empty($filters['query'])) {
                $this->db->group_start()->like($this->db->dbprefix('products').'.name', $filters['query'], 'both')->or_like($this->db->dbprefix('products').'.code', $filters['query'], 'both')->group_end();
            }
            if (!empty($filters['category'])) {
                $this->db->where($this->db->dbprefix('products').'.category_id', $filters['category']['id']);
            }
            if (!empty($filters['subcategory'])) {
                $this->db->where($this->db->dbprefix('products').'.subcategory_id', $filters['subcategory']['id']);
            }
            if (!empty($filters['brand'])) {
                $this->db->where($this->db->dbprefix('products').'.brand', $filters['brand']['id']);
            }
            if (!empty($filters['min_price'])) {
                $this->db->where($this->db->dbprefix('products').'.price >=', $filters['min_price']);
            }
            if (!empty($filters['max_price'])) {
                $this->db->where($this->db->dbprefix('products').'.price <=', $filters['max_price']);
            }
            if ($this->shop_settings->show_product_zero_quantity == 0) {
                $this->db->where($this->db->dbprefix('warehouses_products').'.quantity !=', 0);
            }
            if (!empty($filters['in_stock'])) {
                $this->db->group_start()->where('warehouses_products.quantity >=', 1)->or_where('type !=', 'standard')->group_end();
            }
        }
        $this->db->order_by('variants.name asc');
        $this->db->group_by('product_variants.name');
        return $this->db->get()->result();
    }

    public function getWishlist($no = NULL) {
        $this->db->where('user_id', $this->session->userdata('user_id'));
        return $no ? $this->db->count_all_results('wishlist') : $this->db->get('wishlist')->result();
    }

    public function getProductBySlug($slug) {
        $this->db->select("{$this->db->dbprefix('products')}.*");
        // $sp = $this->getSpecialPrice();
        // if ($sp->cgp) {
        //     $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        // } elseif ($sp->wgp) {
        //     $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        // }
        return $this->db->get_where('products', ['slug' => $slug, $this->db->dbprefix('products').'.hide_online_store !=' => 1], 1)->row();
    }

    public function getProductByID($id) {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, price, quantity, type, promotion, promo_price, start_date, end_date, product_details as details");
        return $this->db->get_where('products', ['id' => $id], 1)->row();
    }

    public function getProductVariants($product_id, $warehouse_id = NULL, $all = NULL) {
        if (!$warehouse_id) { $warehouse_id = $this->shop_settings->warehouse; }
        $wpv = "( SELECT option_id, warehouse_id, quantity from {$this->db->dbprefix('warehouses_products_variants')} WHERE product_id = {$product_id}) FWPV";
        $this->db->select('product_variants.id as id, product_variants.name as name, product_variants.price as price, product_variants.quantity as total_quantity, FWPV.quantity as quantity', FALSE)
            ->join($wpv, 'FWPV.option_id=product_variants.id', 'left')
            //->join('warehouses', 'warehouses.id=product_variants.warehouse_id', 'left')
            ->where('product_variants.product_id', $product_id)
            ->group_by('product_variants.id');

        if (! $this->Settings->overselling && ! $all) {
            $this->db->where('FWPV.warehouse_id', $warehouse_id);
            $this->db->where('FWPV.quantity >', 0);
        }
        return $this->db->get('product_variants')->result_array();
    }

    public function getProductVariantByID($id) {
        return $this->db->get_where('product_variants',['id' => $id])->row();
    }

    public function getProductVariantWarehouseQty($option_id, $warehouse_id) {
        return $this->db->get_where('warehouses_products_variants', array('option_id' => $option_id, 'warehouse_id' => $warehouse_id), 1)->row();
    }

    public function getAddressByID($id) {
        return $this->db->get_where('addresses', ['id' => $id], 1)->row();
    }

    public function addSale($data, $items, $customer, $address)
    {
        if ($this->shop_settings->sale_destination == 1) {
            $cost = $this->site->costing($items);
        }
        if (is_array($customer) && !empty($customer)) {
            $this->db->insert('companies', $customer);
            $data['customer_id'] = $this->db->insert_id();
        }
        if (is_array($address) && !empty($address)) {
            $address['company_id'] = $data['customer_id'];
            $this->db->insert('addresses', $address);
            $data['address_id'] = $this->db->insert_id();
        }

        if ($this->shop_settings->sale_destination == 1) {
            $enc_table = "sales";
            $det_table = "sale_items";
        } else {
            $enc_table = "order_sales";
            $det_table = "order_sale_items";
            $data['order_sale_origin'] = 4;
        }

        $referenceBiller = $this->site->getReferenceBiller($data['biller_id'], $data['document_type_id']);
        if($referenceBiller){
            $reference = $referenceBiller;
        }
        $ref = explode("-", $reference);
        $hyphen = !empty($ref[0]) ? "-" : "";
        $data['reference_no'] = $ref[0].$hyphen.$ref[1];
        $consecutive = $ref[1];
        if ($this->db->insert($enc_table, $data)) {
            $sale_id = $this->db->insert_id();
            $this->site->updateBillerConsecutive($data['document_type_id'], $consecutive+1);
            foreach ($items as $item) {
                $item['sale_id'] = $sale_id;
                $this->db->insert($det_table, $item);
                $sale_item_id = $this->db->insert_id();
                if ($data['sale_status'] == 'completed' && $this->shop_settings->sale_destination == 1) {
                    $item_costs = $this->site->item_costing($item);
                    foreach ($item_costs as $item_cost) {
                        if (isset($item_cost['date']) || isset($item_cost['pi_overselling'])) {
                            $item_cost['sale_item_id'] = $sale_item_id;
                            $item_cost['sale_id'] = $sale_id;
                            $item_cost['date'] = date('Y-m-d', strtotime($data['date']));
                            if(! isset($item_cost['pi_overselling'])) {
                                $this->db->insert('costing', $item_cost);
                            }
                        } else {
                            foreach ($item_cost as $ic) {
                                $ic['sale_item_id'] = $sale_item_id;
                                $ic['sale_id'] = $sale_id;
                                $ic['date'] = date('Y-m-d', strtotime($data['date']));
                                if(! isset($ic['pi_overselling'])) {
                                    $this->db->insert('costing', $ic);
                                }
                            }
                        }
                    }
                }
            }
            // $this->site->syncQuantity($sale_id);
            // $this->sma->update_award_points($data['grand_total'], $data['customer_id'], $data['created_by']);
            return $sale_id;
        }

        return false;
    }

    public function getOrder($clause) {

        if ($this->shop_settings->sale_destination == 1) {
            $enc_table = "sales";
        } else {
            $enc_table = "order_sales";
        }

        if ($this->loggedIn) {
            $this->db->order_by('id desc');
            $sale = $this->db->get_where($enc_table, ['id' => $clause['id']], 1)->row();
            return ($sale->customer_id == $this->session->userdata('company_id')) ? $sale : FALSE;
        } elseif(!empty($clause['hash'])) {
            return $this->db->get_where($enc_table, $clause, 1)->row();
        }
        return FALSE;
    }

    public function getOrders($limit, $offset) {
        if ($this->loggedIn) {
            $this->db->select("sales.*, deliveries.status as delivery_status")
            ->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
            ->order_by('id', 'desc')->limit($limit, $offset);
            return $this->db->get_where('sales', ['customer_id' => $this->session->userdata('company_id')])->result();
        }
        return FALSE;
    }

    public function getOrdersCount() {
        $this->db->where('customer_id', $this->session->userdata('company_id'));
        return $this->db->count_all_results("sales");
    }

    public function getOrderItems($sale_id) {

        if ($this->shop_settings->sale_destination == 1) {
            $det_table = "sale_items";
        } else {
            $det_table = "order_sale_items";
        }

        $this->db->select($det_table.'.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code,  products.second_name as second_name')
            ->join('products', 'products.id='.$det_table.'.product_id', 'left')
            ->join('product_variants', 'product_variants.id='.$det_table.'.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id='.$det_table.'.tax_rate_id', 'left')
            ->group_by($det_table.'.id')
            ->order_by('id', 'asc');

        return $this->db->get_where($det_table, ['sale_id' => $sale_id])->result();
    }

    public function getQuote($clause) {
        if ($this->loggedIn) {
            $this->db->order_by('id desc');
            $sale = $this->db->get_where('quotes', ['id' => $clause['id']], 1)->row();
            return ($sale->customer_id == $this->session->userdata('company_id')) ? $sale : FALSE;
        } elseif(!empty($clause['hash'])) {
            return $this->db->get_where('quotes', $clause, 1)->row();
        }
        return FALSE;
    }

    public function getQuotes($limit, $offset) {
        if ($this->loggedIn) {
            $this->db->order_by('id', 'desc')->limit($limit, $offset);
            return $this->db->get_where('quotes', ['customer_id' => $this->session->userdata('company_id')])->result();
        }
        return FALSE;
    }

    public function getQuotesCount() {
        $this->db->where('customer_id', $this->session->userdata('company_id'));
        return $this->db->count_all_results("quotes");
    }

    public function getQuoteItems($quote_id) {
        $this->db->select('quote_items.*, tax_rates.code as tax_code, tax_rates.name as tax_name, tax_rates.rate as tax_rate, products.image, products.details as details, product_variants.name as variant, products.hsn_code as hsn_code,  products.second_name as second_name')
            ->join('products', 'products.id=quote_items.product_id', 'left')
            ->join('product_variants', 'product_variants.id=quote_items.option_id', 'left')
            ->join('tax_rates', 'tax_rates.id=quote_items.tax_rate_id', 'left')
            ->group_by('quote_items.id')
            ->order_by('id', 'asc');
        return $this->db->get_where('quote_items', ['quote_id' => $quote_id])->result();
    }

    public function getProductComboItems($pid) {
        $this->db->select($this->db->dbprefix('products') . '.id as id, ' . $this->db->dbprefix('products') . '.code as code, ' . $this->db->dbprefix('combo_items') . '.quantity as qty, ' . $this->db->dbprefix('products') . '.name as name, ' . $this->db->dbprefix('combo_items') . '.unit_price as price')->join('products', 'products.code=combo_items.item_code', 'left')->group_by('combo_items.id');
        $q = $this->db->get_where('combo_items', array('product_id' => $pid));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }

            return $data;
        }
        return FALSE;
    }

    public function getProductPhotos($id) {
        $q = $this->db->get_where("product_photos", array('product_id' => $id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
    }

    public function getAllWarehouseWithPQ($product_id, $warehouse_id = NULL) {
        if (!$warehouse_id) { $warehouse_id = $this->shop_settings->warehouse; }
        $this->db->select('' . $this->db->dbprefix('warehouses') . '.*, ' . $this->db->dbprefix('warehouses_products') . '.quantity, ' . $this->db->dbprefix('warehouses_products') . '.rack')
            ->join('warehouses_products', 'warehouses_products.warehouse_id=warehouses.id', 'left')
            ->where('warehouses_products.product_id', $product_id)
            ->where('warehouses_products.warehouse_id', $warehouse_id)
            ->group_by('warehouses.id');
        return $this->db->get('warehouses')->row();
    }

    public function getProductOptionsWithWH($product_id, $warehouse_id = NULL) {
        if (!$warehouse_id) { $warehouse_id = $this->shop_settings->warehouse; }
        $this->db->select($this->db->dbprefix('product_variants') . '.*, ' . $this->db->dbprefix('warehouses') . '.name as wh_name, ' . $this->db->dbprefix('warehouses') . '.id as warehouse_id, ' . $this->db->dbprefix('warehouses_products_variants') . '.quantity as wh_qty')
            ->join('warehouses_products_variants', 'warehouses_products_variants.option_id=product_variants.id', 'left')
            ->join('warehouses', 'warehouses.id=warehouses_products_variants.warehouse_id', 'left')
            ->group_by(['' . $this->db->dbprefix('product_variants') . '.id', '' . $this->db->dbprefix('warehouses_products_variants') . '.warehouse_id'])
            ->order_by('product_variants.id');
        return $this->db->get_where('product_variants', ['product_variants.product_id' => $product_id, 'warehouses.id' => $warehouse_id, 'warehouses_products_variants.quantity !=' => NULL])->result();
    }

    public function getProductOptions($product_id) {
        $q = $this->db->get_where('product_variants', array('product_id' => $product_id));
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function getSaleByID($id) {
        return $this->db->get_where('sales', ['id' => $id])->row();
    }

    public function getCompanyByID($id) {
        return $this->db->get_where('companies', ['id' => $id])->row();
    }

    public function getPaypalSettings() {
        return $this->db->get_where('paypal', ['id' => 1])->row();
    }

    public function getSkrillSettings() {
        return $this->db->get_where('skrill', ['id' => 1])->row();
    }

    public function updateCompany($id, $data = array()) {
        return $this->db->update('companies', $data, ['id' => $id]);
    }

    public function getDownloads($limit, $offset, $product_id = NULL) {
        if ($this->loggedIn) {
            $this->db->select("{$this->db->dbprefix('sale_items')}.product_id, {$this->db->dbprefix('sale_items')}.product_code, {$this->db->dbprefix('sale_items')}.product_name, {$this->db->dbprefix('sale_items')}.product_type")
            ->distinct()
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
            ->where('sales.customer_id', $this->session->userdata('company_id'))
            ->where('sale_items.product_type', 'digital')
            ->order_by('sales.id', 'desc')->limit($limit, $offset);
            if ($product_id) {
                $this->db->where('sale_items.product_id', $product_id);
            }
            return $this->db->get('sales')->result();
        }
        return FALSE;
    }

    public function getDownloadsCount() {
        $this->db->select("{$this->db->dbprefix('sale_items')}.product_id, {$this->db->dbprefix('sale_items')}.product_code, {$this->db->dbprefix('sale_items')}.product_name, {$this->db->dbprefix('sale_items')}.product_type")
        ->distinct()
            ->join('sale_items', 'sales.id=sale_items.sale_id', 'left')
            ->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
            ->where('sales.customer_id', $this->session->userdata('company_id'))
            ->where('sale_items.product_type', 'digital');
        return $this->db->count_all_results("sales");
    }

    public function updateProductViews($id, $views) {
        $views = is_numeric($views) ? ($views+1) : 1;
        return $this->db->update('products', ['views' => $views], ['id' => $id]);
    }

    public function getProductForCart($id) {
        $this->db->select("{$this->db->dbprefix('products')}.*")->where('products.id', $id);
        // $sp = $this->getSpecialPrice();
        // if ($sp->cgp) {
        //     $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        // } elseif ($sp->wgp) {
        //     $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        // }
        $q = $this->db->get('products', 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getOtherProducts($id, $category_id, $brand) {
        $this->db->select("{$this->db->dbprefix('products')}.id as id, {$this->db->dbprefix('products')}.name as name, {$this->db->dbprefix('products')}.code as code, {$this->db->dbprefix('products')}.image as image, {$this->db->dbprefix('products')}.slug as slug, {$this->db->dbprefix('products')}.price, quantity, type, promotion, promo_price, start_date, end_date, b.name as brand_name, b.slug as brand_slug, c.name as category_name, c.slug as category_slug")
        ->join('brands b', 'products.brand=b.id', 'left')
        ->join('categories c', 'products.category_id=c.id', 'left')
        ->where('category_id', $category_id)->where('brand', $brand)
        ->where('products.id !=', $id)->where('products.hide_online_store !=', 1)
        ->order_by('rand()')->limit(4);

        // $sp = $this->getSpecialPrice();
        // if ($sp->cgp) {
        //     $this->db->select("cgp.price as special_price", false)->join($sp->cgp, 'products.id=cgp.product_id', 'left');
        // } elseif ($sp->wgp) {
        //     $this->db->select("wgp.price as special_price", false)->join($sp->wgp, 'products.id=wgp.product_id', 'left');
        // }
        return $this->db->get('products')->result();
    }

    public function getCompanyByEmail($email)
    {
        $q = $this->db->get_where('companies', array('email' => $email), 1);
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function isPromo() {
        $this->db->where('promotion', 1);
        return $this->db->count_all_results("products");
    }

    public function get_address_by_id($id) {
        return $this->db->get_where('addresses', ['id' => $id])->row();
    }

    public function edit_address($id, $data){
        if ($this->db->update('addresses', $data, ['id' => $id])) {
            return true;
        }
        return false;
    }

    public function add_address($data){
        if ($this->db->insert('addresses', $data)) {
            return true;
        }
        return false;
    }

    public function get_main_biller($biller_id = null){
        if ($biller_id) {
            // exit(var_dump($biller_id));
            $q = $this->db->select('companies.id as biller_id, companies.*, biller_data.*')
                          ->join('biller_data', 'biller_data.biller_id = companies.id')
                          ->where('biller_data.branch_type', '2')
                          ->where('companies.id', $biller_id)
                          ->get('companies');
        } else {
            $q = $this->db->select('companies.id as biller_id, companies.*, biller_data.*')
                          ->join('companies', 'companies.id = shop_settings.biller')
                          ->join('biller_data', 'biller_data.biller_id = companies.id')
                          ->get('shop_settings');
        }
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return false;
    }

    public function get_virtual_billers(){
        $q = $this->db->select('companies.id as biller_id, companies.*, biller_data.*')
                      ->join('biller_data', 'biller_data.biller_id = companies.id')
                      ->where('companies.group_name', 'biller')
                      ->where('biller_data.branch_type', '2')
                      ->get('companies');
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_company_principal_address($id){
        $q = $this->db->order_by('id', 'ASC')->limit(1)->get_where('addresses', ['company_id' => $id]);
        if ($q->num_rows() > 0) {
            $q = $q->row();
            return $q;
        }
        return false;
    }

    public function get_ocaccional_customer(){
        $q = $this->db->where('group_name', 'customer')
                      ->like('name', 'ocasional')
                      ->limit(1)
                      ->get('companies');
        if ($q->num_rows() > 0) {
            return $q->row();
        } else {
            return false;
        }
    }

    public function get_item_unit_price($data = false) {
        $unit_id = $data['unit_id'];
        $product_id = $data['product_id'];
        $quantity = $data['quantity'];
        $product = $this->getProductForCart($product_id);
        if ($this->sma->isPromo($product)) {
            return $product->promo_price;
        }
        $category = $this->site->getCategoryById($product->category_id);
        $subcategory = $this->site->getCategoryById($product->subcategory_id);
        $rows = $this->sales_model->getPrecioUnidad($product_id, $quantity, $unit_id);
        // $this->sma->print_arrays($rows);
        if ($rows && $rows->valor_unitario > 0) {
            $valor_p = $rows->valor_unitario;
            if ($this->customer_group) {
                $valor_p = $valor_p + (($valor_p * $this->customer_group->percent) / 100);
            }
            $valor[0] = $valor_p;
            if ($this->Settings->precios_por_unidad_presentacion == 2) {
                $valor[0] = $rows->cantidad > 0 ? $valor_p / $rows->cantidad : $valor_p;
            }
            if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                if ($row->tax_method == 0) {
                    $valor[0] = $this->sma->remove_tax_from_amount($row->tax_rate, $valor[0]);
                }
            }
            if ($this->customer->tax_exempt_customer == 1 && $this->Settings->enable_customer_tax_exemption == 1) {
                if ($row->tax_method == 0) {
                    $valor[0] = $this->sma->remove_tax_from_amount($row->tax_rate, $valor[0]);
                }
            }
            return $valor[0];
        } else{
            return $product->price;
        }
    }
    public function getProductPreferences($product_id){
        $q = $this->db->get_where('product_preferences', ['product_id' => $product_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return FALSE;
    }
    public function get_best_selling_product($days = 30, $max = 10){
        $sql = "SELECT P.* FROM 
                        (
                            SELECT 
                                SUM(SI.quantity) AS qty, 
                                SI.product_id
                            FROM
                                ".$this->db->dbprefix('sale_items')." SI
                            INNER JOIN ".$this->db->dbprefix('sales')." S ON S.id = SI.sale_id
                            WHERE DATEDIFF('".date('Y-m-d')."', S.date) <= ".$days."
                            GROUP BY product_id
                            ORDER BY SUM(quantity) DESC
                            LIMIT ".$max."
                        ) AS max_sales
                INNER JOIN ".$this->db->dbprefix('products')." P ON P.id = max_sales.product_id;";
        $q = $this->db->query($sql);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function get_biller_schedule($biller_id){
        $q = $this->db->get_where('schedule', ['biller_id'=>$biller_id]);
        if ($q->num_rows() > 0) {
            foreach (($q->result()) as $row) {
                $data[] = $row;
            }
            return $data;
        }
        return false;
    }

    public function create_best_selling_temp_table($max, $days){
        $this->db->query('DROP TABLE IF EXISTS '.$this->db->dbprefix('best_selling'));
        $this->db->query('CREATE TEMPORARY TABLE '.$this->db->dbprefix('best_selling').'(product_id INT(11), selled DECIMAL(25,4));');
        $this->db->query('
            INSERT INTO '.$this->db->dbprefix('best_selling').'
            (
                SELECT SI.product_id AS id, SUM(SI.quantity) as selled FROM sma_sale_items SI
                    INNER JOIN sma_sales S ON S.id = SI.sale_id
                WHERE DATEDIFF("'.date('Y-m-d').'", S.date) <= '.$days.'
                GROUP BY SI.product_id
                order by selled DESC
                LIMIT '.$max.'
                ) ');
        $this->db->query('ALTER TABLE `'.$this->db->dbprefix('best_selling').'` 
                            ADD INDEX `pbselling` (`product_id` ASC);');
    }
}
