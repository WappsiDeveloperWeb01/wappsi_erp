<?php

/***********

FORMATO PARA VISUALIZACIÓN DE RECIBOS DE CAJA MÚLTIPLES

************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        //izquierda
        $this->RoundedRect(13, 7, 117, 26, 3, '1234', '');
        $this->inicio_rectangulo = 0;
        $this->fin_rectangulo = 0;
        //izquierda
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,23,9,21.5);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,16,15.5,34);
        }

        $cx = 52;
        $cy = 10;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper(!empty($this->biller->company) ? $this->biller->company : $this->biller->name)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->digito_verificacion > 0 ? '-'.$this->biller->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 26, 3, '1234', '');

        $cx = 132;

        //derecha
        $cy = 12;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Recibo de caja'),'',1,'C');
        $cy +=6;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->encabezado->reference_no),'',1,'C');

        $this->setXY(13, 35);

        $this->inicio_rectangulo = $this->getY();
        $altura_row = 1.5;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(130, $altura_row , $this->sma->utf8Decode(''),'',1,'L');
        $this->Cell(2, 3 , $this->sma->utf8Decode(''),'',0,'L');
        $this->Cell(28, 3 , $this->sma->utf8Decode('Ciudad y Fecha : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(100, 3 , $this->sma->utf8Decode($this->encabezado->city." - ".date('Y-m-d', strtotime($this->encabezado->date))." Hora : ".date('h:i A', strtotime($this->encabezado->date))),'',1,'L');
        $this->Cell(130, $altura_row , $this->sma->utf8Decode(''),'B',1,'L');

        $this->ln($altura_row);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(2, 3 , $this->sma->utf8Decode(''),'',0,'L');
        $this->Cell(28, 3 , $this->sma->utf8Decode('Recibido de: '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(100, 3 , $this->sma->utf8Decode($this->reduceTextToDescription2($this->encabezado->vat_no.($this->encabezado->digito_verificacion ? '-'.$this->encabezado->digito_verificacion : '').", ".$this->encabezado->customer, 53)),'',1,'L');
        $this->Cell(130, $altura_row , $this->sma->utf8Decode(''),'B',1,'L');
        $this->setXY(143, $this->inicio_rectangulo);
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->Cell(66, 12 , $this->sma->utf8Decode(" $ ".$this->sma->formatMoney($this->enc_total_amount)),'LB',1,'C');

        $this->ln($altura_row);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(2, 3 , $this->sma->utf8Decode(''),'',0,'L');
        $this->Cell(28, 3 , $this->sma->utf8Decode('Dirección: '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(166, 3 , $this->sma->utf8Decode($this->encabezado->address),'',1,'L');
        $this->Cell(196, $altura_row , $this->sma->utf8Decode(''),'B',1,'L');

        $this->ln($altura_row);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(2, 3 , $this->sma->utf8Decode(''),'',0,'L');
        $this->Cell(38, 3 , $this->sma->utf8Decode('La suma de (En letras) : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+0.3);
        $this->Cell(2, 3 , $this->sma->utf8Decode(''),'',0,'L');
        $this->Cell(154, 3 , $this->sma->utf8Decode($this->number_convert->convertir($this->enc_total_amount)),'',1,'L');
        $this->Cell(196, $altura_row , $this->sma->utf8Decode(''),'B',1,'L');

        $this->ln($altura_row);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(2, 3 , $this->sma->utf8Decode(''),'',0,'L');
        $this->Cell(38, 3 , $this->sma->utf8Decode('Por concepto de :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(156, 3 , $this->sma->utf8Decode('Pago a facturas : '.$this->reduceTextToDescription2($this->invoice_paids, 80)),'',1,'L');
        if ($this->reduceTextToDescription2($this->invoice_paids, 80, 2)) {
            $this->Cell(196, 3 , $this->sma->utf8Decode($this->reduceTextToDescription2($this->invoice_paids, 120, 2)),'',1,'L');
        }
        $this->Cell(196, $altura_row , $this->sma->utf8Decode(''),'B',1,'L');

        $this->ln($altura_row);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(2, 3 , $this->sma->utf8Decode(''),'',0,'L');
        $this->Cell(38, 3 , $this->sma->utf8Decode('Medio de pago :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(156, 3 , $this->sma->utf8Decode($this->encabezado->paid_by),'',1,'L');
        $this->Cell(196, $altura_row , $this->sma->utf8Decode(''),'B',1,'L');

        $this->ln($altura_row);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(20, 3 , $this->sma->utf8Decode('Código'),'R',0,'C');
        $this->Cell(40, 3 , $this->sma->utf8Decode('Cuenta'),'R',0,'C');
        $this->Cell(92, 3 , $this->sma->utf8Decode('Descripción'),'R',0,'C');
        $this->Cell(22, 3 , $this->sma->utf8Decode('Débitos'),'R',0,'C');
        $this->Cell(22, 3 , $this->sma->utf8Decode('Créditos'),'',1,'C');
        $this->Cell(196, $altura_row , $this->sma->utf8Decode(''),'B',1,'L');

    }

    function Footer()
    {
        // Print centered page number
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text, $length, $type = 1){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > $length) {
            if ($type == 1) {
                $text = substr($text, 0, ($length-5));
                $text.="...";
            } else {
                $text = substr($text, ($length-5));
                $text = substr($text, 0, ($length-5));
                $text.="...";
            }
            return $text;
        }
        if ($type == 1) {
            return $text;
        } else {
            return false;
        }
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}


$pdf = new PDF('P', 'mm', array(216, 279));

$number_convert = new number_convert();
$pdf->number_convert = $number_convert;

$sale_payment = false;
if ($encabezado->supplier_id) {
    $pdf->setTitle($this->sma->utf8Encode('Comprobante egreso'));
} else if ($encabezado->customer_id) {
    $sale_payment = true;
    $pdf->setTitle($this->sma->utf8Encode('Recibo de caja'));
}
$pdf->sale_payment = $sale_payment;

$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->customer = $customer;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->document_type = $document_type;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->Settings = $this->Settings;
$pdf->encabezado = $encabezado;
$pdf->sma = $this->sma;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->biller_logo = $biller_logo;

$invoice_paids = '';
$enc_total_amount = 0;
$total_payment_by_invoice = [];
foreach ($pmnts as $pmnt) {
    if ($pmnt->type == 'discount') {
        continue;
    }
    $total_retentions =
                    ($pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0) + 
                    ($pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0) + 
                    ($pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0) + 
                    ($pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total : 0) + 
                    ($pmnt->rete_ica_assumed == 0 ? $pmnt->rete_autoaviso_total : 0) + 
                    ($pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0);
    $enc_total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
    if (!isset($total_payment_by_invoice[$pmnt->sale_id])) {
        $total_payment_by_invoice[$pmnt->sale_id] = $pmnt->amount;
    } else {
        $total_payment_by_invoice[$pmnt->sale_id] += $pmnt->amount;
    }
    $invoice_paids .= (strlen($invoice_paids) > 0 ? ', ' : '').$pmnt->reference_no;
}
$pdf->enc_total_amount = $enc_total_amount;
$pdf->invoice_paids = $invoice_paids;
$pdf->tipo_regimen = $tipo_regimen;
$pdf->AddPage();

$maximo_footer = 235;

$pdf->SetFont('Arial','',$fuente+1);

$total_amount = 0;
$total_rete_fuente = 0;
$total_rete_iva = 0;
$total_rete_ica = 0;
$total_rete_other = 0;
$total_received = 0;
$total_discount = 0;
$sales_discount = [];
// $this->sma->print_arrays($pmnts);
foreach ($pmnts as $pmnt) {
    if ($pmnt->type == 'discount') {
        if (isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id])) {
            $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] += $pmnt->amount;
        } else {
            $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]= $pmnt->amount;
        }
    } else {
        continue;
    }
}

foreach ($pmnts as $pmnt) {
    if ($pmnt->type == 'discount') {
        continue;
    }
    $sale_discount = isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] : 0;
    $total_rete_fuente += $pmnt->rete_fuente_total;
    $total_rete_iva += $pmnt->rete_iva_total;
    $total_rete_ica += $pmnt->rete_ica_total;
    $total_rete_other += $pmnt->rete_other_total;
    $total_received += ($pmnt->reference_no != NULL ? $pmnt->amount : 0 ) + $sale_discount;
    $total_retentions = $pmnt->rete_fuente_total + $pmnt->rete_iva_total + $pmnt->rete_ica_total + $pmnt->rete_other_total;
    $total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
}
$t_dc_d = 0;
$t_dc_c = 0;
$pdf->ln(1);

if ($contabilidad) {
    foreach ($contabilidad as $cont) {
        if ($pdf->getY() > 240) {
            $pdf->AddPage();
        }
        $columns = [];
        $inicio_fila_X = $pdf->getX();
        $inicio_fila_Y = $pdf->getY();
        $columns[0]['inicio_x'] = $pdf->getX();
        $pdf->Cell(20, 3 , $this->sma->utf8Decode($cont->ledger_code ? $cont->ledger_code : 'No registra'),'',0,'C');
        $columns[0]['fin_x'] = $pdf->getX();
        $columns[1]['inicio_x'] = $pdf->getX();
        $pdf->Cell(40, 3 , $this->sma->utf8Decode($cont->ledger_name ? $pdf->reduceTextToDescription2($cont->ledger_name, 26) : 'No registra'),'',0,'L');
        $columns[1]['fin_x'] = $pdf->getX();
        $columns[2]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
            $pdf->setXY($cX, $cY);
            $pdf->MultiCell(92,3, $this->sma->utf8Decode($cont->narration),'','L');
        $columns[2]['fin_x'] = $cX+92;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+92, $cY);
        $columns[3]['inicio_x'] = $pdf->getX();
        $pdf->Cell(22, 3 , $this->sma->utf8Decode($cont->dc == 'D' ? $this->sma->formatMoney($cont->amount) : 0),'',0,'R');
        $columns[3]['fin_x'] = $pdf->getX();
        $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(22, 3 , $this->sma->utf8Decode($cont->dc == 'C' ? $this->sma->formatMoney($cont->amount) : 0),'',0,'R');
        $columns[4]['fin_x'] = $pdf->getX();
        $fin_fila_X = $pdf->getX();
        $fin_fila_Y = $pdf->getY();
        $ancho_fila = $fin_fila_X - $inicio_fila_X;
        $altura_fila = $fin_altura_fila - $inicio_altura_fila;
        $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
        foreach ($columns as $key => $data) {
            $ancho_column = $data['fin_x'] - $data['inicio_x'];
            if ($altura_fila < 3) {
                $altura_fila = 3;
            }
            $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'R',0,'R');
        }
        $pdf->ln($altura_fila);
        $t_dc_d += $cont->dc == 'D' ? $cont->amount : 0;
        $t_dc_c += $cont->dc == 'C' ? $cont->amount : 0;
    }
    $pdf->Cell(152, 5, $this->sma->utf8Decode('Total'),'TR',0,'R');
    $pdf->Cell(22, 5, $this->sma->utf8Decode($this->sma->formatMoney($t_dc_d)),'TR',0,'R');
    $pdf->Cell(22, 5, $this->sma->utf8Decode($this->sma->formatMoney($t_dc_c)),'T',1,'R');
}

if ($pdf->getY() > 180) {
    $pdf->AddPage();
}

$pdf->fin_rectangulo = $pdf->getY();
$alto_rectangulo = $pdf->fin_rectangulo - $pdf->inicio_rectangulo;
$pdf->RoundedRect(13, $pdf->inicio_rectangulo, 196, $alto_rectangulo, 3, '1234','');
$pdf->ln(5);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Elaborado'),'T',0,'C');
$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Aprobado'),'T',0,'C');
$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Contabilizado'),'T',0,'C');
$pdf->setXY($cx+49, $cy);
$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx, $cy);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Firma y sello recibido:'),'',0,'L');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Cc o Nit :'),'T',1,'L');
$pdf->ln(5);
$cx = $pdf->getX();
$cy = $pdf->getY();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(93, 4 , $this->sma->utf8Decode(lang('created_by').': '), '', 0, 'R');
$pdf->SetFont('Arial','', $fuente);
$pdf->Cell(0, 4 , $this->sma->utf8Decode($created_user->first_name .' '.$created_user->last_name), '', 1, 'L');
$cx = $pdf->getX();
$cy = $pdf->getY();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(93, 4 , $this->sma->utf8Decode(lang('creation_date'). ': '), '', 0, 'R');
$pdf->SetFont('Arial','', $fuente);
$pdf->Cell(0, 4 , $this->sma->utf8Decode($encabezado->payment_date), '', 1, 'L');
$cx = $pdf->getX();
$cy = $pdf->getY();

$pdf->SetFont('Arial','B',$fuente+1);
$pdf->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
$pdf->setXY($cx+185, $cy);
$pdf->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$pdf->PageNo()),'',0,'C');
$download = false;
if ($download || $for_email) {
    if ($download) {
        $pdf->Output("pago_multiple.pdf", "D");
    } else if ($for_email) {
        $pdf->Output("assets/uploads/". $inv->reference_no .".pdf", "F");
    }
} else {
  $pdf->Output("pago_multiple.pdf", "I");
}