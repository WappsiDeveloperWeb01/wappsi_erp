<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>

if ($(window).width() < 1000) {
    var nums = [[10, 25], [10, 25]];
} else {
    var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
}
$(document).ready(function () {
    $.ajax({url:"<?= admin_url('system_settings/getProductsHybridPrices/').$page ?>"}).done(
            function(data){
                $('#SLData tbody').html(data);

                $('#SLData').DataTable({
                    "processing": false,
                    pageLength: 100,
                    responsive: true,
                    oLanguage: <?php echo $dt_lang; ?>
                });
            }
        );
});

</script>
<?php 
    $pages = ceil($cnt_price_groups / 7);
 ?>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-lg-8">
        <h2><?=lang('update_products_hybrid_prices');?></h2>
    </div>
</div>
<!-- /Header -->

<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">
                        <?php if ($this->Settings->ipoconsumo): ?>
                            <div class="row-sm-12">
                                <em><?= lang('tax_included_impoconsumo_details') ?></em>
                            </div>
                        <?php endif ?>
                        <div class="col-lg-3">
                            <label>Páginas : </label>
                            <?php if ($pages > 1): ?>
                                <select id="page_view">
                                    <?php for($i = 0; $i < $pages; $i++) { ?>
                                        <option value="<?= $i ?>" <?= $i == $page ? 'selected="selected"' : '' ?>>Página <?= $i+1 ?></option>
                                    <?php } ?>
                                </select>
                            <?php endif ?>
                        </div>
<!--                         <div class="col-lg-9">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?=admin_url('system_settings/ExportProductsPricesGroups')?>">
                                            <i class="fa fa-file-excel-o"></i> <?=lang('download_xls')?>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div> -->
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?php 
                                $cnt = count($price_groups);
                                $wdth = 75 / $cnt;
                             ?>
                            <!-- <p class="introtext"><?//=lang('list_results');?></p> -->
                            <div class="table-responsive">
                                <?=  
                                admin_form_open_multipart('system_settings/update_products_hybrid_prices');
                                ?>
                                <table id="SLData" class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th style="width: 5%;"><?= lang("code"); ?></th>
                                        <th style="width: 10%;"><?= lang("product_name"); ?></th>
                                        <th style="width: 10%;"><?= lang("unit"); ?></th>
                                        <?php foreach ($price_groups as $pg): ?>
                                            <th style="width: <?= $wdth ?>%"><?= $pg->name ?></th>
                                        <?php endforeach ?>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="4" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th style="width: 5%;"></th>
                                        <th style="width: 10%;"></th>
                                        <th style="width: 10%;"></th>
                                        <?php foreach ($price_groups as $pg): ?>
                                            <th style="width: <?= $wdth ?>%"></th>
                                        <?php endforeach ?>
                                    </tr>
                                    </tfoot>
                                </table>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on('blur', '.pamount',function(e){
        p_id = $(this).data('productid');
        pg_id = $(this).data('pricegroupid');
        unit_id = $(this).data('unitpriceid');
        price = $(this).val();
        if (price > 0) {
            update_price(p_id, pg_id, unit_id, price);
        }

    });

    $(document).on('keypress', '.pamount',function(e){
        if (e.keyCode == 13) {
            p_id = $(this).data('productid');
            pg_id = $(this).data('pricegroupid');
            unit_id = $(this).data('unitpriceid');
            price = $(this).val();
            if (price > 0) {
                update_price(p_id, pg_id, unit_id, price);
            }
        }
    });

    function update_price(p_id, pg_id, unit_id, price){
        $('.pamount').attr('readonly', true);

        $.ajax(
            {
                url:"<?= admin_url('system_settings/updateProductHybridPrice') ?>/"+p_id+"/"+pg_id+"/"+unit_id+"/"+price
            }).done(function(data){
                if (data == 1) {
                    setTimeout(function() {
                        $('.pamount').attr('readonly', false);
                    }, 800);
                    // command: toastr.success('Se actualizó el precio modificado', 'Precio actualizado', { onHidden : function(){  } });
                } else {
                    command: toastr.error('Por favor revise que haya digitado bien el valor, si se trata de algún error contáctese con soporte.', 'No se actualizó', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "4000",
                            "extendedTimeOut": "1000",
                        });
                }
            });
    }

    $('#page_view').on('change', function(){
        location.href = '<?= admin_url("system_settings/update_products_hybrid_prices/") ?>'+$(this).val();
    });
</script>
