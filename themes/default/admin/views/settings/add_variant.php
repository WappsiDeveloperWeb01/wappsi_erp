<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo sprintf(lang('add_variant'), lang('variant')); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/add_variant", $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label for="name"><?php echo $this->lang->line("name"); ?></label>

                <div
                    class="controls"> <?php echo form_input('name', '', 'class="form-control" id="name" required="required"'); ?> </div>
            </div>

            <?php if($this->Settings->management_weight_in_variants): ?>
                <div class="form-group">
                    <label for="weight"><?= lang("weight"); ?></label>
                    <div class="controls"> 
                        <?php echo form_input('weight', '', 'class="form-control" id="weight" required="required"'); ?> 
                    </div>
                </div>
            <?php endif; ?>    
        </div>
        <div class="modal-footer">
            <?php echo form_submit('add_variant', sprintf(lang('add_variant'), lang('variant')), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<?= $modal_js ?>