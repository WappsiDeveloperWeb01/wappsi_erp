<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<?php $v = ""; ?>

<script type="text/javascript">
    $(document).ready(function () {

        <?php if ($this->session->userdata('portfolio_nothing_found')) {
          $this->sma->unset_data('portfolio_nothing_found');
          ?>
          setTimeout(function() {
            window.close();
          }, 800);
        <?php } ?>

        $('#form').hide();

        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <!-- <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a> -->
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/portfolio_report_pdf", "id='form_portfolio' type='POST' target='_blank'"); ?>
                                <div class="row">
                                    <?php
                                $bl[""] = "";
                                $bldata = [];
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    $bldata[$biller->id] = $biller;
                                }
                                // exit(var_dump($this->session->userdata('biller_id')));
                                ?>

                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "biller"); ?>
                                            <select name="biller" class="form-control" id="biller" required="required">
                                                <option value=""><?= lang('all_billers') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>"  <?= (isset($_POST['biller']) && $_POST['biller'] == $biller->id ? "selected" : "") ?> ><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("biller", "biller"); ?>
                                            <select name="biller" class="form-control" id="biller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])) :
                                                    $biller = $bldata[$this->session->userdata('biller_id')];
                                                ?>
                                                    <option value="<?= $biller->id ?>" selected><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("seller", "seller"); ?>
                                            <?php
                                            echo form_dropdown('seller', array('0' => lang("select") . ' ' . lang("biller")), (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="seller" data-placeholder="' . lang("select") . ' ' . lang("biller") . '" class="form-control input-tip select" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                            <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'class="form-control" id="customer" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("customer") . '"'); ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <?= lang("customer_branch", "customerbranch"); ?>
                                            <?php
                                            echo form_dropdown('address', array('0' => lang("select") . ' ' . lang("customer")), (isset($_POST['address']) ? $_POST['address'] : ''), 'id="customerbranch" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip select" style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4">
                                      <?= lang("country", "country"); ?>
                                        <select class="form-control select" name="country" id="country">
                                          <option value="">Seleccione...</option>
                                          <?php foreach ($countries as $row => $country): ?>
                                            <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>"><?= $country->NOMBRE ?></option>
                                          <?php endforeach ?>
                                        </select>
                                      </div>
                                      <div class="form-group col-md-4">
                                          <?= lang("state", "state"); ?>
                                        <select class="form-control select" name="state" id="state">
                                          <option value="">Seleccione país</option>
                                        </select>
                                      </div>
                                </div>
                                <div class="row">
                                      <div class="form-group col-md-4">
                                          <?= lang("city", "city"); ?>
                                        <select class="form-control select" name="city" id="city">
                                          <option value="">Seleccione departamento</option>
                                        </select>
                                        <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>


                                        <label>
                                            <!-- <?= lang('view_total_by') ?></label> -->
                                          <!-- <label>
                                              <input type="checkbox" name="view_customer_address_total" checked="checked" value="1">
                                              <?= lang('view_customer_address_total') ?>
                                          </label>
                                          <label>
                                              <input type="checkbox" name="view_customer_total" checked="checked" value="1">
                                              <?= lang('view_customer_total') ?>
                                          </label> -->
                                          <input type="hidden" name="view_customer_address_total" value="1">
                                          <input type="hidden" name="view_customer_total" value="1">
                                          <label>
                                              <input type="checkbox" name="view_city_total" checked="checked">
                                              <?= lang('view_city_total') ?>
                                          </label><br>
                                          <label>
                                              <input type="checkbox" name="group_by_biller" checked="checked">
                                              <?= lang('group_by_biller') ?>
                                          </label><br>
                                          <label>
                                              <input type="checkbox" name="group_by_seller" checked="checked">
                                              <?= lang('group_by_seller') ?>
                                          </label>

                                      </div>


                                      <div class="form-group col-md-4">
                                          <?= lang("expiration_days_from", "expiration_days_from"); ?>
                                        <select class="form-control select" name="expiration_days_from" id="expiration_days_from">
                                          <option value="1"><?= lang('expiration_days_from_invoice_date') ?></option>
                                          <option value="2"><?= lang('expiration_days_from_due_date') ?></option>
                                        </select>
                                      </div>

                                      <div class="form-group col-md-4">
                                          <?= lang("generate_in", "generate_in"); ?>
                                        <select class="form-control select" name="generate_in" id="generate_in">
                                          <option value="pdf">PDF</option>
                                          <option value="xls">Excel</option>
                                        </select>
                                      </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                      <label><?= lang('cut_off_date') ?></label>
                                      <input type="date" name="end_date" id="end_date" class="form-control" value="<?= date('Y-m-d') ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="controls">
                                      <button class="btn btn-primary" type="button" id="submit_report"><?= lang('submit') ?></button>
                                      <button class="btn btn-danger" type="button" onclick="location.href='<?= admin_url("reports/portfolio") ?>';"><?= lang('cancel') ?></button>
                                    </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {

        $("#form_portfolio").validate({
            ignore: []
        });

        $('#customer').on('change', function(){
            customer = $(this);
            $.ajax({
                url: site.base_url+"sales/getCustomerAddresses",
                type: "get",
                data: {"customer_id" : customer.val()}
            }).done(function(data){
                if (data != false) {
                    $('#customerbranch').html(data).trigger('change.select2');
                } else {
                    $('#customerbranch').select2('val', '').trigger('change.select2');
                }
            });
        });

        $('#biller').on('change', function(){
            biller = $('#biller');
            $.ajax({
                url : site.base_url+"sales/getSellers",
                type : "get",
                data : {"biller_id" : biller.val()}
            }).done(function(data){
                if (data != false) {
                    $('#seller').html(data).trigger('change.select2');
                } else {
                    $('#seller').select2('val', '').trigger('change.select2');
                }
            }).fail(function(data){
                console.log(data);
            });
        });

        <?php if ($this->input->post('customer')) { ?>
        $('#customer').val(<?= $this->input->post('customer') ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "customers/suggestions/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#customer').val(<?= $this->input->post('customer') ?>);
        <?php } ?>

        $('#state').on('change', function(){
          dpto = $('#state option:selected').data('code');
          $.ajax({
            url:"<?= admin_url() ?>customers/get_cities/"+dpto,
          }).done(function(data){
            $('#city').html(data);
            console.log(data);
          }).fail(function(data){
            console.log(data);
          });

        });

        $('#country').on('change', function(){
          dpto = $('#country option:selected').data('code');
          $.ajax({
            url:"<?= admin_url() ?>customers/get_states/"+dpto,
          }).done(function(data){
            $('#state').html(data);
            console.log(data);
          }).fail(function(data){
            console.log(data);
          });

        });

        $('#city').on('change', function(){
          code = $('#city option:selected').data('code');
          $('.postal_code').val(code);
          $('#city_code').val(code);
        });

            // $('#biller').prop('required', true);
            // $('#seller').prop('required', true);

    });
    $(document).on('click', '#submit_report', function(){
      if ($("#form_portfolio").valid()) {
        $("#form_portfolio").submit();
      }
    });
</script>