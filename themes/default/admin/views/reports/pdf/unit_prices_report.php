<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';

class PDF extends HPDF
  {
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Informe de unidades de medida'));
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->setY(7);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->Image($this->Logo,22,6,45,0);
        $this->setXY($cx+76.06, $cy);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Informe de unidades de medida'), '', 1, 'C');
        $this->setXY($cx, $cy+5);
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Fecha impresión : '.date('Y-m-d H:i:s')), '', 1, 'C');
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        $this->ln();
        $this->SetFont('Arial','B', $this->fontsize_title);
        $columns = [];
            $inicio_fila_X = $this->getX();
            $inicio_fila_Y = $this->getY();
        $columns[0]['inicio_x'] = $this->getX();
        $this->cell(30.4 , 3, $this->sma->utf8Decode('Código'), 1, 0, 'C');
        $columns[0]['fin_x'] = $this->getX();
        $columns[1]['inicio_x'] = $this->getX();
        $this->cell(30.4 , 3, $this->sma->utf8Decode('Producto'), 1, 0, 'C');
        $columns[1]['fin_x'] = $this->getX();
        $width = (141.2 / $this->max_units);
        $n_column = 2;
        for ($i = 1; $i <= $this->max_units; $i++) {
            $columns[$n_column]['inicio_x'] = $this->getX();
            $this->cell($width , 3, $this->sma->utf8Decode('Unidad '.$i), 1, 0, 'C');
            $columns[$n_column]['fin_x'] = $this->getX();
            $n_column++;
        }
        $this->ln();
        $this->SetFont('Arial','', $this->fontsize_body);
    }
    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina '.$this->PageNo()." de ".'{nb}'),'',1,'C');
    }
    function crop_text($text, $length = 50){
        if (strlen($text) > $length) {
            $text = substr($text, 0, ($length-5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);
$pdf->fontsize_body = 6;
$pdf->fontsize_title = 8.5;
$pdf->SetFont('Arial','', $pdf->fontsize_body);
$pdf->Logo = base_url('assets/uploads/logos/'.$biller->logo);

$row = 2;
$current_category_name = null;
$category_id = NULL;
$subcategory_id = NULL;
$pdf->sma = $this->sma;
$pdf->max_units = $max_units;
$printed_products = [];

$pdf->AddPage();
foreach ($results as $row) {
    if (!isset($printed_products[$row->product_id])) {
        $printed_products[$row->product_id] = 1;
    } else {
        continue;
    }
    $product_id = $row->product_id;
    $product_name = $row->product_name;
    $product_code = $row->product_code;


    if ($pdf->getY() > 240) {
        $pdf->AddPage();
    }
    $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $pdf->getX();
        $cX = $pdf->getX();
        $cY = $pdf->getY();
        $inicio_altura_fila = $cY;
    $pdf->setXY($cX, $cY+0.7);
    $pdf->MultiCell(30.4, 3, $this->sma->utf8Decode($row->product_code), '', 'L');
    $columns[0]['fin_x'] = $cX+30.4;
        $fin_altura_fila = $pdf->getY();
        $pdf->setXY($cX+30.4, $cY);
    $columns[1]['inicio_x'] = $pdf->getX();
        $cX = $pdf->getX();
        $cY = $pdf->getY();
        $inicio_altura_fila = $cY;
    $pdf->setXY($cX, $cY+0.7);
    $pdf->MultiCell(30.4, 3, $this->sma->utf8Decode($row->product_name), '', 'L');
    $columns[1]['fin_x'] = $cX+30.4;
        $fin_altura_fila = $pdf->getY();
        $pdf->setXY($cX+30.4, $cY);
    $width = (141.2 / $max_units);
    $n_column = 2;

    $columns[2]['inicio_x'] = $pdf->getX();
    $x_un_s = $pdf->getX();
    $y_un_s = $pdf->getY();
$pdf->SetFont('Arial','B', 7.7);
    $pdf->Cell($width, 3, $this->sma->utf8Decode($this->sma->formatMoney($row->main_price)),'BL',0,'C',0);
$pdf->SetFont('Arial','', $pdf->fontsize_body);
    $x_un_e = $pdf->getX();
    $y_un_e = $pdf->getY();
    $columns[2]['fin_x'] = $pdf->getX();
    $pdf->setXY($x_un_s, $y_un_s+3);
    $pdf->Cell($width, 3, $this->sma->utf8Decode($row->main_u_name),'',0,'C',0);
    $pdf->setXY($x_un_e, $y_un_e);

    $num = count($columns);
    for ($i = 1; $i < $max_units; $i++) {
        $num++;
        $unit_price = '-';
        $unit_name = '';
        if (isset($units[$product_id][$i-1])) {
            $unit_price = $units[$product_id][$i-1]->unit_price;
            $unit_name = $units[$product_id][$i-1]->unit_name;
            $unit_id = $units[$product_id][$i-1]->unit_id;
            $columns[$num]['inicio_x'] = $pdf->getX();
    $x_un_s = $pdf->getX();
    $y_un_s = $pdf->getY();
$pdf->SetFont('Arial','B', 7.7);
            $pdf->Cell($width, 3, $this->sma->utf8Decode($this->sma->formatMoney($unit_price)),'BL',0,'C',0);
$pdf->SetFont('Arial','', $pdf->fontsize_body);
    $x_un_e = $pdf->getX();
    $y_un_e = $pdf->getY();
    $pdf->setXY($x_un_s, $y_un_s+3);
    $pdf->Cell($width, 3, $this->sma->utf8Decode($unit_name),'',0,'C',0);
    $pdf->setXY($x_un_e, $y_un_e);
            $columns[$num]['fin_x'] = $pdf->getX();
        } else {
            $columns[$num]['inicio_x'] = $pdf->getX();
            $pdf->Cell($width, 3, $this->sma->utf8Decode('-'),'',0,'C',0);
            $columns[$num]['fin_x'] = $pdf->getX();
        }
    }

    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = $fin_altura_fila - $inicio_altura_fila;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    // exit(var_dump($columns));
    foreach ($columns as $key => $data) {
        $ancho_column = $data['fin_x'] - $data['inicio_x'];
        if ($altura_fila < 6) {
            $altura_fila = 6;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'TLBR',0,'R');
    }
    $pdf->ln($altura_fila);
}
// $pdf->cell(235.02, 3, $this->sma->utf8Decode("Gran total del informe : "), 'T', 0, 'R');
// $pdf->cell(30.8, 3, $this->sma->utf8Decode($this->sma->formatMoney($total_balance)), 'T', 1, 'R');
$descargar = false;
if ($descargar) {
  $pdf->Output("INFORME_UNIDADES_MEDIDA.pdf", "D");
} else {
  $pdf->Output("INFORME_UNIDADES_MEDIDA.pdf", "I");
}