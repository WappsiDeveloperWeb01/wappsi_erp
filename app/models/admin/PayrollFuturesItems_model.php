<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PayrollFuturesItems_model extends CI_Model {

	public function find($data)
	{
		$this->db->where($data);
		$q = $this->db->get('payroll_futures_items');
		return $q->row();
	}


	public function update($data, $filter) {
		$this->db->where($filter);
		if ($this->db->update('payroll_futures_items', $data)) {
			return TRUE;
		}
		return FALSE;
	}

}

/* End of file PayrollFuturesItems_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/PayrollFuturesItems_model.php */