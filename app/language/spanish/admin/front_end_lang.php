<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Language: English
 * Module: Front End Settings
 *
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */


$lang['shop_name']                      = "Nombre de la tienda ";
$lang['page']                           = "Página";
$lang['pages']                          = "Páginas";
$lang['slug']                           = "Nombre en URL";
$lang['order']                          = "Orden";
$lang['menu_order']                     = "Orden en menú";
$lang['body']                           = "Cuerpo";
$lang['edit_page']                      = "Editar página";
$lang['delete_page']                    = "Eliminar página";
$lang['delete_pages']                   = "Eliminar páginas";
$lang['page_added']                     = "Página agregada correctamente";
$lang['page_updated']                   = "Página actualizada correctamente";
$lang['page_deleted']                   = "Página eliminada correctamente";
$lang['pages_deleted']                  = "Páginas eliminadas correctamente";
$lang['order_no']                       = "Orden No.";
$lang['title_required']                 = "El título es requerido";
$lang['slug_required']                  = "El nombre en URL es requerido";
$lang['description_required']           = "La descripción es requerida";
$lang['body_required']                  = "El cuerpo es requerido";
$lang['about_link']                     = "Página de Acerca de";
$lang['terms_link']                     = "Página de Términos y condiciones";
$lang['privacy_link']                   = "Página de Política de privacidad";
$lang['contact_link']                   = "Página de Contacto";
$lang['cookie_link']                    = "Página de Uso de cookies";
$lang['cookie_message']                 = "Mensaje de cookies";
$lang['payment_text']                   = "Texto de pago";
$lang['follow_text']                    = "Texto de seguimiento en redes sociales";
$lang['facebook']                       = "Facebook";
$lang['twitter']                        = "Twitter";
$lang['google_plus']                    = "Google Plus";
$lang['instagram']                      = "Instagram";
$lang['image']                          = "Imagen";
$lang['link']                           = "Enlace";
$lang['caption']                        = "Mensaje";
$lang['show_in_top_menu']               = "Mostrar en menú principal";
$lang['products_page']                  = "Página de productos (Cuadrícula)";
$lang['leave_gap']                      = "Dejar un espacio abajo";
$lang['re_arrange']                     = "Re-organizar productos";
$lang['activate_available_from_quantity'] = 'Activar disponible a partir de cantidad';
$lang['update_promotions_to_store']     = 'Actualización de promociones a la tienda';