<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<style type="text/css">
    .img-responsive {
        max-height: 180px;
    }
</style>
<script type="text/javascript">
    var warehouses = JSON.parse('<?= json_encode($warehouses) ?>');
</script>

<div class="wrapper wrapper-content animated fadeInRight no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <div class="row">
                        <?= admin_form_open_multipart("billers/edit/" . $biller->id, array('class' => 'wizard-big wizard', 'id' => 'edit_biller_form')); ?>
                        <h1><?= lang('basic_data') ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-md-3 col-sm-6 form-group">
                                    <?= form_label(lang('label_type_person'), 'type_person'); ?>
                                    <?php
                                    $types_person_options[""] = lang('select');
                                    foreach ($types_person as $type_person) {
                                        $types_person_options[$type_person->id] = lang($type_person->description);
                                    }
                                    ?>
                                    <?= form_dropdown(['name' => 'type_person', 'id' => 'type_person', 'class' => 'form-control select', 'required' => TRUE], $types_person_options, $biller->type_person); ?>
                                </div>
                                <div class="col-md-3 col-sm-6 form-group">
                                    <?= form_label(lang("label_type_vat_regime"), "type_vat_regime"); ?>
                                    <?php
                                    $types_vat_regime_options[""] = lang('select');
                                    foreach ($types_vat_regime as $type_vat_regime) {
                                        $types_vat_regime_options[$type_vat_regime->id] = lang($type_vat_regime->description);
                                    }
                                    ?>
                                    <?= form_dropdown(["name" => "type_vat_regime", "id" => "type_vat_regime", "class" => "form-control select", "required" => TRUE], $types_vat_regime_options, $biller->tipo_regimen); ?>
                                </div>
                                <div class="col-md-3 col-sm-6 form-group">
                                    <?= form_label(lang('label_document_type'), 'document_type'); ?>
                                    <?php
                                    $document_type_options[""] = lang('select');
                                    foreach ($id_document_types as $document_type) {
                                        $document_type_options[$document_type->codigo_doc] = lang($document_type->nombre);
                                    }
                                    ?>
                                    <?= form_dropdown(['name' => 'document_type', 'id' => 'document_type', 'class' => 'form-control select', 'required' => TRUE], $document_type_options, $biller->tipo_documento); ?>
                                </div>
                                <div class="col-md-3 col-sm-6 form-group legal_person_field" style="display: <?= ($biller->type_person == LEGAL_PERSON) ? 'block' : 'none'; ?>;">
                                    <?= form_label(lang('label_business_name'), 'business_name'); ?>
                                    <?= form_input(['name' => 'business_name', 'id' => 'business_name', 'class' => 'form-control', 'required' => TRUE], $biller->name); ?>
                                </div><!-- Razón social -->
                                <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($biller->type_person == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                    <?= form_label(lang('label_first_name'), 'first_name'); ?>
                                    <?= form_input(['name' => 'first_name', 'id' => 'first_name', 'class' => 'form-control natural_person_field_required', 'required' => TRUE], $biller->first_name); ?>
                                </div>
                                <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($biller->type_person == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                    <?= form_label(lang('label_second_name'), 'second_name'); ?>
                                    <?= form_input(['name' => 'second_name', 'id' => 'second_name', 'class' => 'form-control natural_person_field '], $biller->second_name); ?>
                                </div>
                                <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($biller->type_person == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                    <?= form_label(lang('label_first_lastname'), 'first_lastname'); ?>
                                    <?= form_input(['name' => 'first_lastname', 'id' => 'first_lastname', 'class' => 'form-control natural_person_field natural_person_field_required', 'required' => TRUE], $biller->first_lastname); ?>
                                </div>

                                <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($biller->type_person == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                    <?= form_label(lang('label_second_lastname'), 'second_lastname'); ?>
                                    <?= form_input(['name' => 'second_lastname', 'id' => 'second_lastname', 'class' => 'form-control natural_person_field'], $biller->second_lastname); ?>
                                </div>
                                <div class="col-md-3 col-sm-6 form-group natural_person_field" style="display: <?= ($biller->type_person == NATURAL_PERSON) ? 'block' : 'none'; ?>;">
                                    <?= form_label(lang('label_tradename'), 'trade_name'); ?>
                                    <?= form_input(['name' => 'trade_name', 'id' => 'trade_name', 'class' => 'form-control natural_person_field_required', 'required' => TRUE], $biller->company); ?>
                                </div> <!-- Nombre comercial -->

                                <div class="col-md-3 col-sm-6 form-group legal_person_field">
                                    <?= form_label(lang('label_commercial_register'), 'commercial_register'); ?>
                                    <?php $required = ($this->Settings->fe_technology_provider == FACTURATECH) ? "required" : "" ?>
                                    <?= form_input(['name' => 'commercial_register', 'id' => 'commercial_register', 'class' => 'form-control'], $biller->commercial_register, $required); ?>
                                </div>
                                <div class="col-md-6 col-sm-6 form-group">
                                    <?= form_label(lang("label_types_obligations"), "types_obligations"); ?>
                                    <?php
                                    $types_obligations_options = ['' => lang('select')];
                                    foreach ($types_obligations as $types_obligation) {
                                        $types_obligations_options[$types_obligation->code] = $types_obligation->code . " - " . $types_obligation->description;
                                    }
                                    ?>
                                    <?= form_dropdown(['name' => 'types_obligations[]', 'id' => 'types_obligations', 'class' => 'form-control', 'multiple' => TRUE, 'style' => 'height: auto;', 'required' => TRUE], $types_obligations_options); ?>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <?= lang('great_contributor', 'great_contributor'); ?>
                                    <?php $prioridadOpts = [
                                        '1' => lang('yes'),
                                        '0' => lang('no'),
                                    ]; ?>
                                    <?= form_dropdown('great_contributor', $prioridadOpts, $biller_data->great_contributor, 'class="form-control tip" id="great_contributor"'); ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-3 person juridical_person">
                                    <?= lang("vat_no", "vat_no"); ?>
                                    <?php echo form_input('vat_no', $biller->vat_no, 'class="form-control tip" id="vat_no"'); ?>
                                </div>
                                <div class="form-group col-md-3 person juridical_person" id="contenedor_digito_verificacion">
                                    <?= lang("digito_verificacion", "digito_verificacion"); ?>
                                    <div class="input-group">
                                        <?php echo form_input('digito_verificacion', $biller->digito_verificacion, 'class="form-control tip" id="digito_verificacion" readonly'); ?>
                                        <span class="input-group-addon recalculate_dv" style="cursor:pointer;"><i class="fa fa-refresh"></i></span>
                                    </div>
                                </div>
                                <input type="password" name="password_hidden" style="display:none;">
                                <div class="form-group col-md-3">
                                    <?= lang('branch_type', 'branch_type') ?>
                                    <select class="form-control not_select" id="branch_type" name="branch_type" required>
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="1" <?= $biller_data->branch_type == 1 ? 'selected="selected"' : '' ?>><?= lang('physical') ?></option>
                                        <option value="2" <?= $biller_data->branch_type == 2 ? 'selected="selected"' : '' ?>><?= lang('virtual') ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <?= lang("email_address", "email_address"); ?>
                                    <input type="email" name="email" class="form-control" required="required" id="email_address" value="<?= $biller->email ?>" required />
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("phone", "phone"); ?>
                                    <input type="tel" name="phone" class="form-control" required="required" id="phone" value="<?= $biller->phone ?>" required />
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("country", "country"); ?>
                                    <select class="form-control not_select" name="country" id="country" required>
                                        <option value="">Seleccione...</option>
                                        <?php foreach ($countries as $row => $country) : ?>
                                            <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= ($country->NOMBRE == $biller->country) ? "selected='selected'" : "" ?>><?= $country->NOMBRE ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <?= lang("state", "state"); ?>
                                    <select class="form-control not_select" name="state" id="state" required>
                                        <option value="">Seleccione país</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("city", "city"); ?>
                                    <select class="form-control not_select" name="city" id="city" required>
                                        <option value="">Seleccione departamento</option>
                                    </select>
                                    <?php $city_code = (isset($_POST['city_code'])) ? $_POST['city_code'] : $biller->city_code; ?>
                                    <input type="hidden" name="city_code" id="city_code" value="<?= $city_code ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("address", "address"); ?>
                                    <?php echo form_input('address', $biller->address, 'class="form-control" id="address" required="required"'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <?= lang("zone", "zone"); ?>
                                    <select class="form-control not_select" name="zone" id="zone">
                                        <option value=""><?= lang("select") ?></option>
                                    </select>
                                    <input type="hidden" name="zone_code" id="zone_code" <?= (isset($_POST['zone_code'])) ? "value='" . $_POST['zone_code'] . "'" : "" ?>>
                                </div>

                                <div class="form-group col-md-3">
                                    <?= lang("subzone", "subzone"); ?>
                                    <select class="form-control not_select" name="subzone" id="subzone">
                                        <option value=""><?= lang("select") ?></option>
                                    </select>
                                    <input type="hidden" name="subzone_code" id="subzone_code" <?= (isset($_POST['subzone_code'])) ? "value='" . $_POST['subzone_code'] . "'" : "" ?>>
                                </div>

                                <div class="form-group col-md-3">
                                    <?= lang("postal_code", "postal_code"); ?>
                                    <?php echo form_input('postal_code', $biller->postal_code, 'class="form-control postal_code" id="postal_code"'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <?= lang('pin_code_method', 'pin_code_method') ?>
                                    <select name="pin_code_method" class="form-control" id="pin_code_method">
                                        <option value="1" <?= $biller_data->pin_code_method == 1 ? 'selected' : '' ?>><?= lang('manual_pin_code') ?></option>
                                        <option value="2" <?= $biller_data->pin_code_method == 2 ? 'selected' : '' ?>><?= lang('random_pin_code') ?></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 div_pin_code" style="display:none;">
                                    <?= lang('pin_code', 'pin_code') ?>
                                    <input type="password" name="pin_code" value="<?= (isset($biller_data->pin_code) ? $biller_data->pin_code : '') ?>" class="form-control">
                                </div>


                                <div class="form-group col-md-3 ">
                                    <?= lang("pin_code_request", "pin_code_request"); ?>
                                    <select class="form-control" name="pin_code_request" id="pin_code_request">
                                        <option value="1" <?= $biller_data->pin_code_request == 1 ? "selected='selected'" : "" ?>><?= lang("control_profitability_margin") ?></option>
                                        <option value="2" <?= $biller_data->pin_code_request == 2 ? "selected='selected'" : "" ?>><?= lang("control_sale_actions") ?></option>
                                        <option value="3" <?= $biller_data->pin_code_request == 3 ? "selected='selected'" : "" ?>><?= lang("control_both") ?></option>
                                        <option value="4" <?= $biller_data->pin_code_request == 4 ? "selected='selected'" : "" ?>><?= lang("control_delete_suspend_sale") ?></option>
                                    </select>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("rectangular_logo", "biller_logo"); ?>
                                        <?php
                                        $biller_logos[''] = '';
                                        foreach ($logo_files as $key => $value) {
                                            $biller_logos[$value] = $value;
                                        }
                                        echo form_dropdown('logo', $biller_logos, $biller->logo, 'class="form-control not_select" id="biller_logo" required="required" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div id="logo-con" class="text-center">
                                        <img class="img-responsive center-block" src="<?= base_url('assets/uploads/logos/' . $biller->logo) ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= lang("square_logo", "square_logo"); ?>
                                        <?php
                                        unset($biller_logos);
                                        $biller_logos[''] = '';
                                        foreach ($logo_files as $key => $value) {
                                            $biller_logos[$value] = $value;
                                        }
                                        echo form_dropdown('square_logo', $biller_logos, $biller->logo_square, 'class="form-control not_select" id="square_logo" required="required" '); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div id="square_logo-con" class="text-center">
                                        <img class="img-responsive center-block" src="<?= base_url('assets/uploads/logos/' . $biller->logo_square) ?>" alt="">
                                    </div>
                                </div>
                            </div>
                            <?php $arr_days = [1 => 'Lunes', 2 => 'Martes', 3 => 'Miércoles', 4 => 'Jueves', 5 => 'Viernes', 6 => 'Sábado', 7 => 'Domingo'] ?>
                            <div class="row virtual_schedule col-sm-12" style="display:none;">
                                <hr class="col-sm-11">
                                <div class="row">
                                    <h3 class="col-sm-12">Horario de servicio tienda en línea</h3>
                                </div>
                                <?php foreach ($arr_days as $key => $value) : ?>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <?= lang('week_day', 'week_day') ?>
                                            <input type="hidden" name="week_day[]" value="<?= $key ?>">
                                            <input type="text" name="week_day_text" value="<?= $value ?>" class="form-control" readonly>
                                        </div>
                                        <div class="col-sm-4">
                                            <?= lang('start', 'start') ?>
                                            <input type="time" name="start[]" value="<?= isset($schedule[$key]) ? $schedule[$key]['start'] : "00:00:00" ?>" class="form-control">
                                        </div>
                                        <div class="col-sm-4">
                                            <?= lang('end', 'end') ?>
                                            <input type="time" name="end[]" value="<?= isset($schedule[$key]) ? $schedule[$key]['end'] : "23:59:00" ?>" class="form-control">
                                        </div>
                                    </div>
                                <?php endforeach ?>
                            </div>
                            <?php if ($Settings->financing_module) : ?>
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="affiliate_consecutive"><?= lang("affiliate_consecutive") ?></label>
                                            <?php $affiliate_consecutive = (isset($biller_data) && !empty($biller_data->affiliate_consecutive)) ? $biller_data->affiliate_consecutive : "" ?>
                                            <input class="form-control" type="text" name="affiliate_consecutive" id="affiliate_consecutive" value="<?= $affiliate_consecutive ?>" required>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="old_branch_code"><?= lang("old_branch_code") ?></label>
                                            <?php $old_branch_code = (isset($biller_data) && !empty($biller_data->old_branch_code)) ? $biller_data->old_branch_code : "" ?>
                                            <input class="form-control" type="text" name="old_branch_code" id="old_branch_code" value="<?= $old_branch_code ?>" required>
                                        </div>
                                    </div>
                                </div>
                            <?php endif ?>
                        </section>

                        <h1><?= lang('commercial_data') ?></h1>
                        <section>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <?= lang('prioridad_precios_producto', 'prioridad_precios_producto') ?>
                                    <select class="form-control not_select" id="prioridad_precios_producto" name="prioridad_precios_producto">
                                        <option value=""><?= lang('setted_in_settings') ?></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="biller_default_price_group"><?php echo $this->lang->line("default_group_price"); ?></label>
                                    <?php
                                    $pgs[''] = lang('select') . ' ' . lang('default_group_price');
                                    if ($price_groups != FALSE) {
                                        foreach ($price_groups as $price_group) {
                                            $pgs[$price_group->id] = $price_group->name;
                                        }
                                    }
                                    echo form_dropdown('biller_default_price_group', $pgs, (isset($biller_data->default_price_group) ? $biller_data->default_price_group : ''), 'class="form-control not_select" id="biller_default_price_group" style="width:100%;"');
                                    ?>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="biller_default_customer_id"><?php echo lang('default_customer'); ?></label>
                                    <?php $requiredCustomer = ($this->Settings->force_default_client_in_branch == 1) ? ' required="required" ' : '' ?>
                                    <?= form_input('biller_default_customer_id', (isset($biller_data->default_customer_id) ? $biller_data->default_customer_id : ''), 'id="biller_default_customer_id" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="max-width:100%;"' . $requiredCustomer); ?>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="biller_default_seller_id"><?php echo lang('default_seller'); ?></label>
                                    <?php
                                    $slls[''] = lang('select') . ' ' . lang('default_seller');

                                    if ($sellers != FALSE) {
                                        foreach ($sellers as $seller) {
                                            $slls[$seller['companies_id']] = $seller['name'];
                                        }
                                        // $required = 'required="true"';
                                        $required = '';
                                    } else {
                                        $required = '';
                                    }
                                    echo form_dropdown('biller_default_seller_id', $slls, (isset($biller_data->default_seller_id) ? $biller_data->default_seller_id : ''), 'class="form-control not_select" id="biller_default_seller_id" style="width:100%;"' . $required);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                <?php if ($cost_centers) : ?>
                                    <div class="col-md-3">
                                        <?= lang('cost_center', 'cost_center') ?>
                                        <select name="cost_center" class="form-control not_select" style="width: 100%;">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php foreach ($cost_centers as $cc) : ?>
                                                <option value="<?= $cc->id ?>" <?= $cc->id == $biller_data->default_cost_center_id ? "selected='selected'" : "" ?>> (<?= $cc->code ?>) <?= $cc->name ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                <?php endif ?>
                                <div class="col-md-3">
                                    <?= lang("language", "language"); ?>
                                    <?php
                                    $lang = array(
                                        '' => lang('setted_in_settings'),
                                        'arabic'                    => 'Arabic',
                                        'english'                   => 'English',
                                        'german'                    => 'German',
                                        'portuguese-brazilian'      => 'Portuguese (Brazil)',
                                        'simplified-chinese'        => 'Simplified Chinese',
                                        'spanish'                   => 'Spanish',
                                        'thai'                      => 'Thai',
                                        'traditional-chinese'       => 'Traditional Chinese',
                                        'turkish'                   => 'Turkish',
                                        'vietnamese'                => 'Vietnamese',
                                    );
                                    echo form_dropdown('language', $lang, (isset($biller_data->language) ? $biller_data->language : ''), 'class="form-control tip not_select" id="language" style="width:100%;"');
                                    ?>
                                </div>
                                <div class="col-md-3">
                                    <label class="control-label" for="currency"><?= lang("default_currency"); ?></label>
                                    <div class="controls"> <?php
                                                            $cu[''] = lang('setted_in_settings');
                                                            foreach ($currencies as $currency) {
                                                                $cu[$currency->code] = $currency->name;
                                                            }
                                                            echo form_dropdown('currency', $cu, (isset($biller_data->currency) ? $biller_data->currency : ''), 'class="form-control tip not_select" id="currency" style="width:100%;"');
                                                            ?>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("product_order", "product_order"); ?>
                                    <?php
                                    $opciones_orden_producto = [
                                        "" => lang("default"),
                                        1 => lang("category"),
                                        2 => lang("brand")
                                    ];

                                    echo form_dropdown(["name" => "product_order", "id" => "product_order", "class" => "form-control not_select", "style" => "width: 100%;"], $opciones_orden_producto, (isset($biller_data->product_order) ? $biller_data->product_order : ''));
                                    ?>
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("default_pos_section", "default_pos_section"); ?>
                                    <?php
                                    $ctgr_options[''] = lang('select');
                                    $ctgr_options[997] = lang('featured');
                                    $ctgr_options[998] = lang('promotions');
                                    $ctgr_options[999] = lang('favorites');
                                    foreach ($categories as $catgry) {
                                        $ctgr_options[$catgry->id] = "(" . lang('category') . ") " . $catgry->name;
                                    }
                                    echo form_dropdown(["name" => "default_pos_section", "id" => "default_pos_section", "class" => "form-control", "style" => "width: 100%;"], $ctgr_options, (isset($biller_data->default_pos_section) ? $biller_data->default_pos_section : ''));
                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3 col-sm-3">
                                    <?= lang('concession_status', 'concession_status') ?>
                                    <?php
                                    $concession_status = array(
                                        '0' => lang('inactive'),
                                        '1' => lang('active'),
                                    );
                                    ?>
                                    <?= form_dropdown('concession_status', $concession_status, $biller_data->concession_status, 'required="required" id="concession_status" class="form-control"') ?>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <?= lang('type_concession', 'type_concession') ?>
                                    <?php
                                    $type_concession = array(
                                        '1' => lang('in_invoice'),
                                        '2' => lang('out_invoice'),
                                    );
                                    ?>
                                    <?= form_dropdown('type_concession', $type_concession, $biller_data->type_concession, 'required="required" id="type_concession" class="form-control"') ?>
                                </div>
                                <div class="col-md-3">
                                    <?= lang('min_sale_amount', 'min_sale_amount') ?>
                                    <input type="number" name="min_sale_amount" id="min_sale_amount" class="form-control only_number" value="<?= $biller_data->min_sale_amount ?>">
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label" for="biller_default_affiliate_id"><?php echo lang('default_affiliate'); ?></label>
                                    <?php
                                    $afflls[''] = lang('select') . ' ' . lang('default_affiliate');

                                    if ($affiliates != FALSE) {
                                        foreach ($affiliates as $affiliate) {
                                            $afflls[$affiliate['companies_id']] = $affiliate['name'];
                                        }
                                        // $required = 'required="true"';
                                        $required = '';
                                    } else {
                                        $required = '';
                                    }
                                    echo form_dropdown('biller_default_affiliate_id', $afflls, (isset($biller_data->default_affiliate_id) ? $biller_data->default_affiliate_id : ''), 'class="form-control not_select" id="biller_default_affiliate_id" style="width:100%;"' . $required);
                                    ?>
                                </div>
                            </div>
                            <div class="row">
                                

                                <div class="form-group col-md-8">
                                    <?php
                                    $warehouses_related = $biller_data->warehouses_related != '' ? json_decode($biller_data->warehouses_related) : [];
                                    ?>
                                    <label class="control-label" for="biller_warehouses_related"><?php echo lang('warehouses_related'); ?></label>
                                    <?php
                                    $whs = [];
                                    if ($warehouses != FALSE) {
                                        foreach ($warehouses as $warehouse) {
                                            $whs[$warehouse->id] = $warehouse->name;
                                        }
                                    }
                                    echo form_dropdown('biller_warehouses_related[]', $whs, $warehouses_related, 'class="form-control not_select" id="biller_warehouses_related" style="width:100%;" multiple required');
                                    ?>
                                </div>

                                <div class="form-group col-md-4">
                                    <label class="control-label" for="biller_default_warehouse_id"><?php echo lang('default_warehouse'); ?></label>
                                    <?php
                                    $whs = [];
                                    $whs[''] = lang('select') . ' ' . lang('default_warehouse');
                                    if ($warehouses != FALSE) {
                                        foreach ($warehouses as $warehouse) {
                                            if (in_array($warehouse->id, $warehouses_related)) {
                                                $whs[$warehouse->id] = $warehouse->name;
                                            }
                                        }
                                    }
                                    echo form_dropdown('biller_default_warehouse_id', $whs, (isset($biller_data->default_warehouse_id) ? $biller_data->default_warehouse_id : ''), 'class="form-control not_select" id="biller_default_warehouse_id" style="width:100%;" required');
                                    ?>
                                </div>
                                <div class="col-md-12 concession_code_div" style="display: none;">
                                    <table class="table" style="margin-top: 1%">
                                        <thead>
                                            <tr>
                                                <th><?= lang('category') ?></th>
                                                <th><?= lang('concession_code') ?></th>
                                                <th><?= lang('concession_name') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php foreach ($categories as $category) : ?>
                                                <tr>
                                                    <td><?= ucwords(mb_strtolower($category->name)) ?></td>
                                                    <td><input type="text" name="category_concession_code[<?= $category->id ?>]" value="<?= isset($biller_categories_concession[$category->id]) ? $biller_categories_concession[$category->id]->concession_code : '' ?>" class="form-control concession_input_code"></td>
                                                    <td><input type="text" name="category_concession_name[<?= $category->id ?>]" value="<?= isset($biller_categories_concession[$category->id]) ? $biller_categories_concession[$category->id]->concession_name : '' ?>" class="form-control concession_input_name"></td>
                                                </tr>
                                            <?php endforeach ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?= lang('geographic_coverage_area') ?></h2>
                                </div>

                                <div class="form-group col-md-3">
                                    <?= lang("country", "coverage_country"); ?>
                                    <select class="form-control not_select" name="coverage_country" id="coverage_country">
                                        <option value="">Seleccione...</option>
                                        <?php foreach ($countries as $row => $country) : ?>
                                            <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= (isset($biller_data->coverage_country) && $biller_data->coverage_country ==  $country->NOMBRE ? 'selected="selected"' : '') ?>><?= $country->NOMBRE ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("state", "coverage_state"); ?>
                                    <select class="form-control not_select" name="coverage_state" id="coverage_state">
                                        <option value="">Seleccione país</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("city", "city"); ?>
                                    <select class="form-control not_select" name="coverage_city" id="coverage_city">
                                        <option value="">Seleccione departamento</option>
                                    </select>
                                    <input type="hidden" name="coverage_city_code" id="coverage_city_code">
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang("zone", "coverage_zone"); ?>
                                    <select class="form-control not_select" name="coverage_zone" id="coverage_zone">
                                        <option value=""><?= lang("select") ?></option>
                                    </select>
                                    <input type="hidden" name="coverage_zone_code" id="coverage_zone_code">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?= lang('shipping_management') ?></h2>
                                </div>
                                <div class="form-group col-md-3">
                                    <?= lang('charge_shipping_cost', 'charge_shipping_cost') ?>
                                    <select name="charge_shipping_cost" id="charge_shipping_cost" class="form-control not_select">
                                        <option value=""><?= lang('without_cost') ?></option>
                                        <option value="1" <?= isset($biller_data->charge_shipping_cost) && $biller_data->charge_shipping_cost == 1 ? 'selected="selected"' : '' ?>><?= lang('according_to_location') ?></option>
                                        <option value="2" <?= isset($biller_data->charge_shipping_cost) && $biller_data->charge_shipping_cost == 2 ? 'selected="selected"' : '' ?>><?= lang('according_to_volume_and_weight') ?></option>
                                        <option value="3" <?= isset($biller_data->charge_shipping_cost) && $biller_data->charge_shipping_cost == 3 ? 'selected="selected"' : '' ?>><?= lang('fixed_cost') ?></option>
                                    </select>
                                </div>
                                <div class="form-group col-md-3 shipping_cost" style="display: none;">
                                    <?= lang('shipping_cost', 'shipping_cost') ?>
                                    <input type="text" name="shipping_cost" id="shipping_cost" class="form-control only_number" value="<?= (isset($biller_data->shipping_cost) ? $biller_data->shipping_cost : '') ?>">
                                </div>
                                <div class="form-group col-md-3 max_total_shipping_cost" style="display: none;">
                                    <?= lang('max_total_shipping_cost', 'max_total_shipping_cost') ?>
                                    <input type="text" name="max_total_shipping_cost" id="max_total_shipping_cost" class="form-control only_number" value="<?= (isset($biller_data->max_total_shipping_cost) ? $biller_data->max_total_shipping_cost : '') ?>">
                                </div>
                            </div>
                            <script type="text/javascript">
                            </script>
                        </section>

                        <h1><?= lang('documents') ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?= lang('sales') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[2] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 2) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[4] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 4) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[13] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 13) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[31] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 31) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label><?= lang('modules')[1] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 1) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[3] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 3) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[19] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 19) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[27] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 27) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <label><?= lang('modules')[29] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 29) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label><?= lang('modules')[26] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 26) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[28] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 28) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[37] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 37) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[36] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 36) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[46] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 46) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[53] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 53) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[61] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 61) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[62] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 62) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('purchases') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[5] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 5) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[6] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 6) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[20] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 20) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[32] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 32) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[35] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 35) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[52] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 52) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('expenses') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[21] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 21) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[22] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 22) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[23] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 23) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('imports') ?></h2>
                                </div>

                                <div class="col-md-3">
                                    <label><?= lang('modules')[54] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 54) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label><?= lang('modules')[55] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 55) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('payments') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[14] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 14) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[17] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 17) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[24] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 24) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[30] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 30) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[15] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 15) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[33] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 33) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[34] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 34) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[57] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 57) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[60] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 60) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('orders') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[8] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 8) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[7] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 7) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[9] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 9) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[10] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 10) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('products_and_production') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[11] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 11) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[25] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 25) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[18] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 18) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[38] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 38) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[39] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 39) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[40] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 40) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[41] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 41) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[12] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 12) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[63] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 63) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[64] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 64) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row" style="padding-bottom: 30px;">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('default_document_type_selection') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[1] ?> por Defecto: </label>
                                    <select name="document_type_pos_default" id="document_type_pos_default" style="width: 100%;">
                                        <option value="">Seleccione al menos un documento POS</option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 1) : ?>
                                                    <option value="<?= $dt->id ?>" <?= $dt->id == $biller_data->pos_document_type_default ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[2] ?> por Defecto: </label>
                                    <select name="document_type_detal_default" id="document_type_detal_default" style="width: 100%;">
                                        <option value="">Seleccione al menos un documento DETAL</option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 2) : ?>
                                                    <option value="<?= $dt->id ?>" <?= $dt->id == $biller_data->detal_document_type_default ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[5] ?> por Defecto: </label>
                                    <select name="document_type_purchases_default" id="document_type_purchases_default" style="width: 100%;">
                                        <option value="">Seleccione al menos un documento COMPRAS</option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 5) : ?>
                                                    <option value="<?= $dt->id ?>" <?= $dt->id == $biller_data->purchases_document_type_default ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label>Tipo de documento de ajuste para las preparaciones : </label>
                                    <select name="document_type_preparation_adjustment" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 11) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= $dt->id == $biller_data->preparation_adjustment_document_type_id ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[8] ?> por Defecto: </label>
                                    <select name="document_type_order_sales_default" id="document_type_order_sales_default" style="width: 100%;">
                                        <option value="">Seleccione al menos un documento <?= lang('modules')[8] ?></option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 8) : ?>
                                                    <option value="<?= $dt->id ?>" <?= $dt->id == $biller_data->order_sales_document_type_default ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[3] ?> por Defecto: </label>
                                    <select name="pos_document_type_default_returns" id="pos_document_type_default_returns" style="width: 100%;">
                                        <option value="">Seleccione al menos un documento <?= lang('modules')[3] ?></option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 3) : ?>
                                                    <option value="<?= $dt->id ?>" <?= $dt->id == $biller_data->pos_document_type_default_returns ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <h2><?= lang('warranty') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[56] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 56) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= lang('payroll_electronic') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[43] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 43) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[44] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 44) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label><?= lang('modules')[45] ?> : </label>
                                    <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 45) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <?php if ($this->Settings->hotel_module_management == 1) : ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                        <h2><?= lang('hotel') ?></h2>
                                    </div>
                                    <div class="col-md-3">
                                        <label><?= lang('modules')[47] ?> : </label>
                                        <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                            <?php if ($document_types) : ?>
                                                <?php foreach ($document_types as $dt) : ?>
                                                    <?php if ($dt->module == 47) : ?>
                                                        <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>
                            <?php endif ?>

                            <div class="row">
                                <div class="col-md-12">
                                    <hr>
                                    <h2><?= $this->lang->line('documents_reception') ?></h2>
                                </div>
                                <div class="col-md-3">
                                    <label for=""><?= $this->lang->line('acknowledgment_receipt'); ?></label>
                                    <select name="document_types[]" class="form-control document_types">
                                        <option value=""><?= $this->lang->line('select') ?></option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 48) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for=""><?= $this->lang->line('reception_good_service'); ?></label>
                                    <select name="document_types[]" class="form-control document_types">
                                        <option value=""><?= $this->lang->line('select') ?></option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 49) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for=""><?= $this->lang->line('claim'); ?></label>
                                    <select name="document_types[]" class="form-control document_types">
                                        <option value=""><?= $this->lang->line('select') ?></option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 50) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>

                                <div class="col-md-3">
                                    <label for=""><?= $this->lang->line('express_acceptance'); ?></label>
                                    <select name="document_types[]" class="form-control document_types">
                                        <option value=""><?= $this->lang->line('select') ?></option>
                                        <?php if ($document_types) : ?>
                                            <?php foreach ($document_types as $dt) : ?>
                                                <?php if ($dt->module == 51) : ?>
                                                    <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                <?php endif ?>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>

                            <?php if ($this->Settings->financing_module == 1) { ?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr>
                                        <h2><?= lang('credit_financing_language') ?></h2>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('credit_financing_language'); ?></label>
                                            <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                                <option value=""><?= $this->lang->line('select') ?></option>
                                                <?php if ($document_types) : ?>
                                                    <?php foreach ($document_types as $dt) : ?>
                                                        <?php if ($dt->module == 58) : ?>
                                                            <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><?= "{$this->lang->line('payment_financing_fee')}  {$this->lang->line('credit_financing_language')}"; ?></label>
                                            <select name="document_types[]" class="form-control document_types" multiple="multiple">
                                                <option value=""><?= $this->lang->line('select') ?></option>
                                                <?php if ($document_types) : ?>
                                                    <?php foreach ($document_types as $dt) : ?>
                                                        <?php if ($dt->module == 59) : ?>
                                                            <option value="<?= $dt->id ?>" data-module="<?= $dt->module ?>" <?= isset($document_types_selected[$dt->id]) ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('document_electronic_billing'); ?></label>
                                            <select name="default_electronic_document_for_financing" id="default_electronic_document_for_financing" class="form-control document_types">
                                                <option value=""><?= $this->lang->line('select') ?></option>
                                                <?php if ($document_types) : ?>
                                                    <?php foreach ($document_types as $dt) : ?>
                                                        <?php if ($dt->module == 1) : ?>
                                                            <option value="<?= $dt->id ?>" <?= $dt->id == $biller_data->default_electronic_document_for_financing ? "selected='selected'" : "" ?>> <?= $dt->sales_prefix ?> </option>
                                                        <?php endif ?>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <?= form_hidden('add_biller', '1'); ?>
                        </section>

                        <?php if ($this->Settings->modulary) : ?>
                            <h1><?= lang('accountant_data') ?></h1>
                            <section>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <?= lang('autoica_percentage', 'rete_autoica_percentage') ?>
                                        <input type="number" name="rete_autoica_percentage" id="rete_autoica_percentage" class="form-control" value="<?= $biller_data->rete_autoica_percentage ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('autoica_account', 'rete_autoica_account') ?>
                                        <?php
                                        $lopts[''] = lang('select');
                                        foreach ($ledgers as $ledger) {
                                            $lopts[$ledger->id] = $ledger->code . " - " . $ledger->name;
                                        }
                                        ?>
                                        <?= form_dropdown('rete_autoica_account', $lopts, $biller_data->rete_autoica_account, 'class="form-control" id="rete_autoica_account" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('autoica_account_counterpart', 'rete_autoica_account_counterpart') ?>
                                        <?= form_dropdown('rete_autoica_account_counterpart', $lopts, $biller_data->rete_autoica_account_counterpart, 'class="form-control" id="rete_autoica_account_counterpart" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <?= lang('bomberil_percentage', 'rete_bomberil_percentage') ?>
                                        <input type="number" name="rete_bomberil_percentage" id="rete_bomberil_percentage" class="form-control" value="<?= $biller_data->rete_bomberil_percentage ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('bomberil_account', 'rete_bomberil_account') ?>
                                        <?= form_dropdown('rete_bomberil_account', $lopts, $biller_data->rete_bomberil_account, 'class="form-control" id="rete_bomberil_account" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('bomberil_account_counterpart', 'rete_bomberil_account_counterpart') ?>
                                        <?= form_dropdown('rete_bomberil_account_counterpart', $lopts, $biller_data->rete_bomberil_account_counterpart, 'class="form-control" id="rete_bomberil_account_counterpart" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <?= lang('autoaviso_percentage', 'rete_autoaviso_percentage') ?>
                                        <input type="number" name="rete_autoaviso_percentage" id="rete_autoaviso_percentage" class="form-control" value="<?= $biller_data->rete_autoaviso_percentage ?>">
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('autoaviso_account', 'rete_autoaviso_account') ?>
                                        <?= form_dropdown('rete_autoaviso_account', $lopts, $biller_data->rete_autoaviso_account, 'class="form-control" id="rete_autoaviso_account" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('autoaviso_account_counterpart', 'rete_autoaviso_account_counterpart') ?>
                                        <?= form_dropdown('rete_autoaviso_account_counterpart', $lopts, $biller_data->rete_autoaviso_account_counterpart, 'class="form-control" id="rete_autoaviso_account_counterpart" style="width:100%;"'); ?>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <?= lang('cash_payment_method_account', 'cash_payment_method_account') ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="top" title="Esta configuración se toma tanto en VENTA cómo en COMPRA"></i>
                                        <?= form_dropdown('cash_payment_method_account', $lopts, $biller_data->cash_payment_method_account, 'class="form-control" id="cash_payment_method_account" style="width:100%;"'); ?>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <?= lang('default_credit_payment_method', 'default_credit_payment_method') ?>
                                        <select name="default_credit_payment_method" id="default_credit_payment_method" data-pbnum="1" class="form-control">
                                            <?= $this->sma->paid_opts($biller_data->default_credit_payment_method, false, true, false, false, true); ?>
                                        </select>
                                    </div>
                                </div>
                            </section>
                        <?php endif ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {

        setTimeout(function() {
            $('[data-toggle="tooltip"]').tooltip();
        }, 1000);

        $(document).on('change', '#biller_logo', function() {
            console.log('entra');
            var biller_logo = $(this).val();
            $('#logo-con').html('<img class="img-responsive center-block" src="<?= base_url('assets/uploads/logos') ?>/' + biller_logo + '" alt="">');
        });

        $(document).on('change', '#square_logo', function() {
            console.log('entra');
            var square_logo = $(this).val();
            $('#square_logo-con').html('<img class="img-responsive center-block" src="<?= base_url('assets/uploads/logos') ?>/' + square_logo + '" alt="">');
        });

        $(document).on('change', '#country', function() {
            dpto = $('#country option:selected').data('code');
            $.ajax({
                url: "<?= admin_url() ?>customers/get_states/" + dpto,
            }).done(function(data) {
                $('#state').html(data);
                console.log(data);
            }).fail(function(data) {
                console.log(data);
            });
        });

        $(document).on('change', '#state', function() {
            dpto = $('#state option:selected').data('code');
            $.ajax({
                url: "<?= admin_url() ?>customers/get_cities/" + dpto,
            }).done(function(data) {
                $('#city').html(data);
                console.log(data);
            }).fail(function(data) {
                console.log(data);
            });
        });

        $(document).on('change', '#city', function() {
            code = $('#city option:selected').data('code');
            $('.postal_code').val(code);
            $('#city_code').val(code);
            $.ajax({
                url: "<?= admin_url() . 'customers/get_zones/' ?>" + code
            }).done(function(data) {
                $('#zone').html(data);
            });
        });

        $(document).on('change', '#zone', function() {
            code = $('#zone option:selected').data('code');
            $('.postal_code').val(code);
            $.ajax({
                url: "<?= admin_url() . 'customers/get_subzones/' ?>" + code
            }).done(function(data) {
                $('#subzone').html(data);
            });
        });

        $(document).on('change', '#subzone', function() {
            code = $('#subzone option:selected').data('code');
            $('.postal_code').val(code);
        });

        var form = $("#edit_biller_form");
        var runValid = 1;
        wizard = form.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onStepChanging: function(event, currentIndex, newIndex) {
                if (runValid == 1) {
                    form.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                    return form.valid();
                } else {
                    return true;
                }
            },
            onStepChanged: function(event) {
                $('section:not(:hidden) input[required="required"]').addClass('validate');
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                setTimeout(function() {
                    $('select:not(:hidden)').select2();
                }, 300);
            },
            onFinishing: function(event, currentIndex) {
                if (runValid == 1) {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                } else {
                    return true;
                }
            },
            onFinished: function(event, currentIndex) {
                if (runValid == 1) {
                    form.validate().settings.ignore = ":disabled";
                    if (form.valid()) {
                        $(form).submit();
                    }
                }
            }
        });

        $('section:not(:hidden) select[required="required"]').addClass('validate');
        $('select:not(:hidden)').select2();
        $('#charge_shipping_cost').trigger('change');
        set_ubication();
        set_coverage_ubication();

        $(document).on('change', '#concession_status', function() {
            if ($(this).val() == 0) {
                $('.concession_code_div').fadeOut();
            } else if ($(this).val() == 1) {
                $('.concession_code_div').fadeIn();
            }
        });

        $('.concession_input_code').keyup(function() {
            tr = $(this).parents('tr');
            concession_name = $(tr).find('.concession_input_name');
            if ($(this).val()) {
                $(concession_name).prop('required', true);
            } else {
                $(concession_name).prop('required', false);
            }
        });

        setTimeout(function() {
            $('#restobar_mode_status').trigger('change');
            $('#concession_status').trigger('change');
            $('#pin_code_method').trigger('change');
            $('#vat_no').trigger('change');
        }, 800);
        $('#branch_type').trigger('change');

        <?php if (isset($biller_data->default_customer_id)) : ?>
            $('#biller_default_customer_id').val("<?= $biller_data->default_customer_id ?>").select2({
                <?php if ($biller_data->default_customer_id != 0) { ?>
                    allowClear: true,
                <?php }  ?>

                placeholder: "<?= lang('select') . ' ' . lang('customer') ?>",
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        },
                        error: function() {
                            callback({
                                id: '',
                                text: "<?= lang('select') . ' ' . lang('customer') ?>"
                            })
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: "<?= lang('select') . ' ' . lang('customer') ?>"
                                }]
                            };
                        }
                    }
                }
            });
        <?php else : ?>
            nsCustomer();
        <?php endif ?>
        <?php if (!empty($types_customer_obligations)) { ?>
            var types_customer_obligations_array = [];
            <?php foreach ($types_customer_obligations as $type_customer_obligation) { ?>
                types_customer_obligations_array.push('<?= $type_customer_obligation->types_obligations_id; ?>');
            <?php } ?>
            setTimeout(function() {
                $('select#types_obligations').select2({}).select2('val', types_customer_obligations_array);
            }, 850);
            $(document).on('change', '#document_type', mostrarOcultarDigitoVerificacion);
        <?php } ?>
    });

    $(document).on('click', '.add_document_type', function() {
        $('.no_resolucion').css('display', 'none');
        html = $('.datos-resolucion').html();

        $.ajax({
            url: "<?= admin_url('billers/datos_resolucion') ?>",
            type: "get"
        }).done(function(data) {
            $('.resoluciones').append(data);

            $('#edit_biller_form').bootstrapValidator('destroy');
            $('#edit_biller_form').data('bootstrapValidator', null);
            $('#edit_biller_form').bootstrapValidator();
        });
    });


    $(document).on('click', '#submit_biller', function() {
        $('.bv-hidden-submit').click()
    });

    $(document).on('change', '.document_types', function() {
        $('#document_type_pos_default').html('');
        $('#document_type_detal_default').html('');
        $('#document_type_purchases_default').html('');
        $('#document_type_order_sales_default').html('');

        $('.document_types option:selected').each(function(index, val) {
            option = $(this);
            if (option.data('module') == 1) {
                $('#document_type_pos_default').append("<option value='" + option.val() + "'>" + option.text() + "</option>");
            } else if (option.data('module') == 2) {
                $('#document_type_detal_default').append("<option value='" + option.val() + "'>" + option.text() + "</option>");
            } else if (option.data('module') == 5) {
                $('#document_type_purchases_default').append("<option value='" + option.val() + "'>" + option.text() + "</option>");
            } else if (option.data('module') == 8) {
                $('#document_type_order_sales_default').append("<option value='" + option.val() + "'>" + option.text() + "</option>");
            }
        });
        $('#document_type_pos_default').select2();
        $('#document_type_detal_default').select2();
        $('#document_type_purchases_default').select2();
        $('#document_type_order_sales_default').select2();
    });

    $(document).on('change', '#coverage_country', function() {
        dpto = $('#coverage_country option:selected').data('code');
        $.ajax({
            url: "<?= admin_url() ?>customers/get_states/" + dpto,
        }).done(function(data) {
            $('#coverage_state').html(data);
            console.log(data);
        }).fail(function(data) {
            console.log(data);
        });
    });

    $(document).on('change', '#coverage_state', function() {
        dpto = $('#coverage_state option:selected').data('code');
        $.ajax({
            url: "<?= admin_url() ?>customers/get_cities/" + dpto,
        }).done(function(data) {
            $('#coverage_city').html(data);
            console.log(data);
        }).fail(function(data) {
            console.log(data);
        });
    });

    $(document).on('change', '#coverage_city', function() {
        code = $('#coverage_city option:selected').data('code');
        $('#coverage_city_code').val(code);
        $.ajax({
            url: "<?= admin_url() . 'customers/get_zones/' ?>" + code
        }).done(function(data) {
            $('#coverage_zone').html(data);
        });
    });

    $(document).on('change', '#coverage_zone', function() {
        code = $('#coverage_zone option:selected').data('code');
    });


    $(document).on('change', '#charge_shipping_cost', function() {
        charge_shipping_cost = $(this).val();

        if (charge_shipping_cost == 3) {
            $('.shipping_cost').fadeIn();
            $('#shipping_cost').prop('required', true).prop('min', 1);
        } else {
            $('.shipping_cost').fadeOut();
            $('#shipping_cost').prop('required', false).prop('min', false);
        }

        if (charge_shipping_cost) {
            $('.max_total_shipping_cost').fadeIn();
        } else {
            $('.max_total_shipping_cost').fadeOut();
        }
    });

    function set_ubication() {
        country = $('#country option:selected').data('code');
        state = "<?= $biller->state ?>";
        set_city = "<?= $biller->city ?>";
        $.ajax({
                url: "<?= admin_url() ?>customers/get_states",
                method: "POST",
                data: {
                    "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                    country: country,
                    state: state
                }
            })
            .done(function(data) {
                $('#state').html(data);
                $('#state').select2();
                state = $('#state option:selected').data('code');

                $.ajax({
                        url: "<?= admin_url() ?>customers/get_cities",
                        method: "POST",
                        data: {
                            "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                            state: state,
                            city: set_city
                        }
                    }).done(function(data) {
                        $('#city').html(data);
                        $('#city').select2('val', set_city);

                        <?php if (!empty($biller->location)) { ?>

                            city = $('#city option:selected').data('code');
                            zone = $('#zone option:selected').data('code');

                            $.ajax({
                                url: "<?= admin_url() ?>customers/get_zones",
                                method: "POST",
                                data: {
                                    "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                                    state: state,
                                    city: city
                                }
                            }).done(function(data) {
                                $('#zone').html(data).select2().trigger('change');
                                <?php if (!empty($biller->subzone)) { ?>
                                    zone = $('#zone option:selected').data('code');
                                    $.ajax({
                                        url: "<?= admin_url() ?>customers/get_subzones",
                                        method: "POST",
                                        data: {
                                            "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                                            zone: zone,
                                            subzone: subzone
                                        }
                                    }).done(function(data) {
                                        $('#subzone').html(data).select2();
                                    });
                                <?php } ?>
                            });
                        <?php } ?>

                    })
                    .fail(function(data) {
                        console.log(data.responseText);
                    });

            }).fail(function(data) {
                console.log(data.respnoseText);
            });
    }

    function set_coverage_ubication() {
        country = $('#coverage_country option:selected').data('code');
        state = "<?= $biller_data->coverage_state ?>";
        city = "<?= $biller_data->coverage_city ?>";
        $.ajax({
                url: "<?= admin_url() ?>customers/get_states/" + country + "/" + state
            })
            .done(function(data) {
                $('#coverage_state').html(data);
                $('#coverage_state').select2();
                state = $('#coverage_state option:selected').data('code');
                $.ajax({
                        url: "<?= admin_url() ?>customers/get_cities/" + state + "/" + city
                    }).done(function(data) {
                        $('#coverage_city').html(data);
                        $('#coverage_city').select2('val', city);
                        <?php if (!empty($biller_data->coverage_zone)) { ?>
                            city = $('#coverage_city option:selected').data('code');
                            $.ajax({
                                url: "<?= admin_url() ?>customers/get_zones/" + city + "/" + zone
                            }).done(function(data) {
                                $('#coverage_zone').html(data).select2().trigger('change');
                            });
                        <?php } ?>
                    })
                    .fail(function(data) {
                        console.log(data.responseText);
                    });

            }).fail(function(data) {
                console.log(data.respnoseText);
            });
    }

    $(document).on('change', '#pin_code_method', function() {
        pcm = $(this).val();
        if (pcm == 1) {
            $('.div_pin_code').fadeIn();
        } else {
            $('.div_pin_code').fadeOut();
        }
    });


    $(document).on('change', '#vat_no', function() {
        $('#digito_verificacion').val(calcularDigitoVerificacion($(this).val()));
    });

    $(document).on('click', '.recalculate_dv', function() {
        $('#digito_verificacion').val(calcularDigitoVerificacion($('#vat_no').val()));
    });

    $(document).on('change', '#biller_warehouses_related', function() {

        $('#biller_default_warehouse_id option').remove();
        $('#biller_default_warehouse_id').append($('<option>', {
            value: '',
            text: 'Seleccione'
        }));
        first_warehouse = null;
        if ($('#biller_warehouses_related option:selected').length > 0) {
            $('#biller_warehouses_related option:selected').each(function(index, option) {
                $('#biller_default_warehouse_id').append($('<option>', {
                    value: $(option).val(),
                    text: $(option).text()
                }));
                if (first_warehouse == null) {
                    first_warehouse = $(option).val();
                }
            });
        } else {
            $(warehouses).each(function(index, option) {
                $('#biller_default_warehouse_id').append($('<option>', {
                    value: option.id,
                    text: option.name
                }));
                if (first_warehouse == null) {
                    first_warehouse = option.id;
                }
            });
        }
        setTimeout(function() {
            default_warehouse_id = '<?= $biller_data->default_warehouse_id ? $biller_data->default_warehouse_id : $this->Settings->default_warehouse ?>';
            $('#biller_default_warehouse_id').select2('val', default_warehouse_id ? default_warehouse_id : first_warehouse);
        }, 850);
    });

    $(document).on('change', '#branch_type', function() {
        if ($(this).val() == 2) {
            $('.virtual_schedule').fadeIn();
        } else {
            $('.virtual_schedule').fadeOut();
        }
    });


    $(document).on('change', '#type_person', show_fields_type_person);
    $(document).on('change', '#document_type', mostrarOcultarDigitoVerificacion);
    $(document).on('change', '#technology_provider', function() {
        show_hide_user_password_electronic_billing();
    });
    $('#type_person').trigger('change');
    mostrarOcultarDigitoVerificacion();

    function show_fields_type_person() {
        var type_person = $('#type_person').val();

        if (type_person == '<?= LEGAL_PERSON; ?>') {
            $('.legal_person_field').show();
            $('.natural_person_field').hide();
            $('.natural_person_field_required').prop('required', false);
            $('#document_type').select2('val', 31);

            $('#type_vat_regime').select2('val', <?= COMMON; ?>);

            $('#business_name').attr('required', 'required');

            $('#first_name').removeAttr('required');
            $('#first_lastname').removeAttr('required');
        } else if (type_person == '<?= NATURAL_PERSON; ?>') {
            $('.legal_person_field').hide();
            $('.natural_person_field').show();
            $('.natural_person_field_required').prop('required', true);

            $('#document_type option').each(function() {
                $(this).removeAttr('disabled');
            });

            $('#document_type').select2('val', '');
            $('#type_vat_regime').select2('val', '');

            $('#first_name').attr('required', 'required');
            $('#first_lastname').attr('required', 'required');

            $('#business_name').removeAttr('required');
        } else {
            $('.legal_person_field').hide();
            $('.natural_person_field').hide();
            $('.natural_person_field_required').prop('required', true);
        }
    }

    function mostrarOcultarDigitoVerificacion() {
        var tipo_documento = $('#document_type').val();
        if (tipo_documento == 31) {
            $('#contenedor_digito_verificacion').fadeIn();
            $('#digito_verificacion').attr("required", true);
        } else {
            $('#contenedor_digito_verificacion').fadeOut();
            $('#digito_verificacion').removeAttr("required");
        }
    }

    function nsCustomer() {
        $('#biller_default_customer_id').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function(term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function(data, page) {
                    if (data.results != null) {
                        return {
                            results: data.results
                        };
                    } else {
                        return {
                            results: [{
                                id: '',
                                text: lang.no_match_found
                            }]
                        };
                    }
                }
            }
        });
    }
</script>