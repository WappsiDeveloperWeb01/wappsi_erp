<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <?= admin_form_open('sales_reports/budget_execution', ['id'=>'budgetFilterForm']) ?>
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('year') ?></label>
                                            <?php $year = (isset($_POST["year"])) ? $_POST["year"] : date('Y'); ?>
                                            <input class="form-control" type="number" name="year" id="year" value="<?= $year ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('month') ?></label>
                                            <?php $monthId = (isset($_POST["month"])) ? $_POST["month"] : date('n'); ?>
                                            <select class="form-control" name="month" id="month">
                                                <?php if (!empty($months)) : ?>
                                                    <?php foreach ($months as $month) : ?>
                                                        <option value="<?= $month->id ?>" <?= ($month->id == $monthId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($month->name)) ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="biller_id"><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <select class="form-control select" name="biller_id" id="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php if (!empty($billers)) : ?>
                                                    <?php foreach ($billers as $biller) : ?>
                                                        <option value="<?= $biller->id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($biller->name)) ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php if ($this->Settings->detailed_budget_by_seller == YES) : ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="seller"><?= $this->lang->line('seller') ?></label>
                                                <?php $sellerId = (isset($_POST["seller_id"])) ? $_POST["seller_id"] : ''; ?>
                                                <select class="form-control select" name="seller_id" id="seller_id">
                                                    <option value=""><?= $this->lang->line('alls') ?></option>
                                                    <?php if (!empty($sellers)) : ?>
                                                        <?php foreach ($sellers as $seller) : ?>
                                                            <option value="<?= $seller->id ?>" <?= ($seller->id == $sellerId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($seller->name)) ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <?php if ($this->Settings->detailed_budget_by_seller == YES && $this->Settings->detailed_budget_by_category == YES) : ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="category_id"><?= $this->lang->line('category') ?></label>
                                                <?php $categoryId = (isset($_POST["category_id"])) ? $_POST["category_id"] : ''; ?>
                                                <select class="form-control select" name="category_id" id="category_id">
                                                    <option value=""><?= $this->lang->line('allsf') ?></option>
                                                    <?php if (!empty($categories)) : ?>
                                                        <?php foreach ($categories as $category) : ?>
                                                            <option value="<?= $category->id ?>" <?= ($category->id == $categoryId) ? 'selected' : '' ?>><?= ucfirst(mb_strtolower($category->name)) ?></option>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="see_units" style="margin-top: 25px">
                                                <?php $seeUnits = (isset($_POST["see_units"])) ? $_POST["see_units"] : ''; ?>
                                                <input type="checkbox" name="see_units" id="see_units" <?= (!empty($seeUnits) ? 'checked' : '') ?>> <?= $this->lang->line('see_units') ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="include_credit_notes" style="margin-top: 25px">
                                                <?php $includeCreditNotes = (isset($_POST["include_credit_notes"])) ? $_POST["include_credit_notes"] : ''; ?>
                                                <input type="checkbox" name="include_credit_notes" id="include_credit_notes" <?= (!empty($includeCreditNotes) ? 'checked' : '') ?>> <?= $this->lang->line('include_credit_notes') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-1">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros">
                                        <?= $icon ?>
                                    </button>
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="top" title="Aplicar filtros">
                                        <i class="fas fa-filter fa-lg"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="ibox">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-hover" id="budgetTable">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Sucursal</th>
                                            <th class="text-center">Vendedor</th>
                                            <th class="text-center">Categoria producto</th>

                                            <th class="text-center">Presupuesto <br> <small>ventas</small></th>
                                            <th class="text-center">Ventas</th>
                                            <th class="text-center">Diferencia <br> <small>ventas</small></th>
                                            <th class="text-center">% <br> <small>ventas</small></th>
                                            <th class="text-center">Proyección <br> <small>ventas</small></th>
                                            <?php if (!empty($seeUnits)) : ?>
                                                <th class="text-center">Presupuesto <br> <small>unidades</small></th>
                                                <th class="text-center">Ventas <br> <small>unidades</small></th>
                                                <th class="text-center">Diferencia <br> <small>unidades</small></th>
                                                <th class="text-center">% <br> <small>unidades</small></th>
                                                <th class="text-center">Proyección <br> <small>unidades</small></th>
                                            <?php endif ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        loadDataTables();

        $('[data-toggle="tooltip"]').tooltip();
        $('[data-toggle-second="tooltip"]').tooltip();

        $(document).on('change', '#year, #month', function() { loadBillers()});
        <?php if ($this->Settings->detailed_budget_by_seller == YES) : ?>
            $(document).on('change', '#biller_id', function() { loadSellers($(this).val())});
        <?php endif ?>
        $(document).on('change', '#seller_id', function() { loadCategories($(this).val())});
        $(document).on('click', '#excel', function() { $('.buttons-excel').trigger('click'); });
    });

    function loadDataTables()
    {
        var _tabFilterFill = false;
        var nums = [[10, 25], [10, 25]];

        if ($(window).width() > 1000) {
            nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        $('#budgetTable').dataTable({
            buttons: [{
                extend: 'excel',
                text: 'Exportar',
                title: 'Informe ejecución presupuesto de Ventas de ' + $('#month option:selected').text() + ' de '+ $('#year').val()
            }],
            aaSorting: [[0, "asc"], [1, "asc"], [2, "asc"]],
            aLengthMenu: nums,
            iDisplayLength:  $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"Bp>>',
            sAjaxSource: site.base_url+'sales_reports/getDataTablesBudgetExecution',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?=$this->security->get_csrf_token_name()?>",
                    "value": "<?=$this->security->get_csrf_hash()?>"
                }, {
                    "name": "year",
                    "value": $('#year').val()
                }, {
                    "name": "month",
                    "value": $('#month').val()
                }, {
                    "name": "biller_id",
                    "value": $('#biller_id').val()
                }, {
                    "name": "seller_id",
                    "value": $('#seller_id').val()
                }, {
                    "name": "category_id",
                    "value": $('#category_id').val()
                }, {
                    "name": "see_units",
                    "value": ($('#see_units').is(':checked')) ? 1 : 0
                }, {
                    "name": "include_credit_notes",
                    "value": ($('#include_credit_notes').is(':checked')) ? 1 : 0
                });

                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            aoColumns: [
                {
                    mRender: function (data, type, row) {
                        return (data != null) ? data : '<?= $this->lang->line('allsf') ?>';
                    }
                },
                {
                    mRender: function(data, type, row) {
                        return (data != null) ? data : '<?= $this->lang->line('alls') ?>'
                    }
                },
                {
                    mRender: function(data, type, row) {
                        return (data != null) ? data : '<?= $this->lang->line('allsf') ?>'
                    }
                },
                {
                    className: 'text-right',
                    mRender: function(data, type, row) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    mRender: function(data, type, row) {
                        return formatMoney(data)

                    }
                },
                {
                    className: 'text-right',
                    mRender: function (data, type, row) {
                        return formatMoney(data)
                    }
                },
                {
                    className: 'text-right',
                    mRender: function (data, type, row) {
                        return formatDecimal(data, 2) + ' %'
                    }
                },
                {
                    className: 'text-right',
                    mRender: function (data, type, row) {
                        const currentDate = new Date()
                        const currentYear = currentDate.getFullYear()
                        const currentMonth = currentDate.getMonth() + 1
                        const month = $('#month').val()
                        const year = $('#year').val()

                        if (year < currentYear || (year == currentYear && month < currentMonth)) {
                            return ''
                        } else {
                            return formatDecimal(data, 2) + ' %'
                        }
                    }
                },
                <?php if (!empty($seeUnits)) : ?>
                    {
                        className: 'text-right',
                        mRender: function(data, type, row) {
                            return formatMoney(data)
                        }
                    },
                    {
                        className: 'text-right',
                        mRender: function(data, type, row) {
                            return formatMoney(data)
                        }
                    },
                    {
                        className: 'text-right',
                        mRender: function (data, type, row) {
                            return formatMoney(data)
                        }
                    },
                    {
                        className: 'text-right',
                        mRender: function (data, type, row) {
                            return formatDecimal(data, 2) + ' %'
                        }
                    },
                    {
                        className: 'text-right',
                        mRender: function (data, type, row) {
                            const currentDate = new Date()
                            const currentYear = currentDate.getFullYear()
                            const currentMonth = currentDate.getMonth() + 1
                            const month = $('#month').val()
                            const year = $('#year').val()

                            if (year < currentYear || (year == currentYear && month < currentMonth)) {
                                return ''
                            } else {
                                return formatDecimal(data, 2) + ' %'
                            }
                        }
                    }
                <?php endif ?>
            ],
            fnDrawCallback: function (oSettings) {
                if (oSettings.aoData.length) {
                    $('.actionsButtonContainer').html('<div class="dropdown">'+
                        '<button class="btn btn-primary btn-outline new-button dropdown-toggle center-block" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                        '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                           '<li>'+
                                '<a href="#" id="excel">'+
                                    '<i class="fa fa-file-excel-o"></i> <?=lang('export_to_excel')?>'+
                                '</a>'+
                            '</li>'+
                        '</ul>'+
                    '</div>');
                }

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });

                $('[data-toggle="tooltip"]').tooltip();
            },
            fnFooterCallback: function(row, aData) {
                let totalSales = totalBadgeSales = totalDiff = 0
                let totalUnits = totalBadgeUnits = totalDiffUnits = 0

                console.log(aData);

                $.each(aData, function(index, data) {
                    totalBadgeSales += parseFloat(data[3]);
                    totalSales += parseFloat(data[4]);
                    totalDiff += parseFloat(data[5]);

                    if (typeof data[8] !== 'undefined') {
                        totalBadgeUnits += parseFloat(data[8]);
                        totalUnits += parseFloat(data[9]);
                        totalDiffUnits += parseFloat(data[10]);
                    }
                });

                let days = obtenerDiasEnMes($('#year').val(), $('#month').val())
                let currentDate = new Date();

                let thUnitsFoot = ''
                if (totalBadgeUnits > 0) {
                    thUnitsFoot = `<th class="text-right">${formatMoney(totalBadgeUnits)}</th>
                        <th class="text-right">${formatMoney(totalUnits)}</th>
                        <th class="text-right">${formatMoney(totalDiffUnits)}</th>
                        <th class="text-right">${formatDecimal(((totalUnits / totalBadgeUnits) * 100))} %</th>
                        <th class="text-right">${formatDecimal(((days * totalUnits / currentDate.getDate()) / totalBadgeUnits) * 100)} %</th>`
                }

                $('#budgetTable').append(`<tfoot>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th class="text-right">${formatMoney(totalBadgeSales)}</th>
                        <th class="text-right">${formatMoney(totalSales)}</th>
                        <th class="text-right">${formatMoney(totalDiff)}</th>
                        <th class="text-right">${formatDecimal(((totalSales / totalBadgeSales) * 100))} %</th>
                        <th class="text-right">${formatDecimal(((days * totalSales / currentDate.getDate()) / totalBadgeSales) * 100)} %</th>
                        ${thUnitsFoot}
                    </tr>
                </tfoot>`);
            },
        });
    }

    function obtenerDiasEnMes(año, mes) {
        let ultimoDiaMesSiguiente = new Date(año, mes, 0);
        return ultimoDiaMesSiguiente.getDate();
    }

    function loadBillers()
    {
        var options = '<option value=""><?= $this->lang->line('allsf') ?></option>';
        $('#biller_id').select2('val', '');

        $.ajax({
            type: "post",
            url: site.base_url+'sales_reports/getBranchesFromBudget',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'year': $('#year').val(),
                'month': $('#month').val()
            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(seller => {
                        options += '<option value="'+seller.companies_id+'">'+seller.name+'</option>';
                    });
                }

                $('#biller_id').html(options);
            }
        });
    }

    function loadSellers(billerId)
    {
        var options = '<option value=""><?= $this->lang->line('alls') ?></option>';
        $('#seller_id').select2('val', '');

        $.ajax({
            type: "post",
            url: site.base_url+'sales_reports/getSellerByBillerFromBudget',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'year': $('#year').val(),
                'month': $('#month').val(),
                'billerId': billerId
            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(seller => {
                        options += '<option value="'+seller.id+'">'+seller.name+'</option>';
                    });
                }

                $('#seller_id').html(options);
            }
        });
    }

    function loadCategories(sellerId)
    {
        var options = '<option value=""><?= $this->lang->line('allsf') ?></option>';
        $('#category_id').select2('val', '');

        $.ajax({
            type: "post",
            url: site.base_url+'sales_reports/getCategoriesBySellerFromBudget',
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'year': $('#year').val(),
                'month': $('#month').val(),
                'billerId': $('#biller_id').val(),
                'sellerId': sellerId,
            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(seller => {
                        options += '<option value="'+seller.companies_id+'">'+seller.name+'</option>';
                    });
                }

                $('#category_id').html(options);
            }
        });
    }

</script>