<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// $lang["employees"] = "Empleados";
// $lang["employees_list"] = "Empleados";
// $lang["employees_profession"] = "Profesión";
// $lang["employees_professional_position"] = "Cargo";
// $lang["employees_add"] = "Nuevo empleado";
// $lang["employees_first_name"] = "Primer Nombre";
// $lang["employees_second_name"] = "Segundo Nombre";
// $lang["employees_first_lastname"] = "Primer Apellido";
// $lang["employees_second_lastname"] = "Segundo Apellido";
// $lang["employees_document_type"] = "Tipo Documento";
// $lang["employees_document_number"] = "No. Documento";
// $lang["employees_email"] = "Correo electrónico";
// $lang["employees_phone"] = "Télefono";
// $lang["employees_landline"] = "Télefono fijo";
// $lang["employees_birthday_date"] = "Fecha de nacimiento";
// $lang["employees_gender"] = "Género";
// $lang["employees_marital_status"] = "Estado civil";
// $lang["employees_blood_type"] = "Tipo de sangre";
// $lang["employees_military_card"] = "Libreta Militar";
// $lang["employees_educational_level"] = "Nivel educacional";
// $lang["employees_relationship"] = "Parentesco";
// $lang["employees_regime_type"] = "Tipo régimen";
// $lang["employees_contract_status"] = "Estado contrato";
// $lang["employees_contract_type"] = "Tipo contrato";
// $lang["employees_biller"] = "Sucursal";
// $lang['employees_main_contact'] = 'Contacto principal';

// $lang["employees_saved"] = "Empleado guardado exitosamente";
// $lang["employees_not_saved"] = "Empleado no ha sido guardado";

// $lang["employees_edit"] = "Editar empleado";
// $lang["employees_existing_document_number"] = "El Número de documento ya se encuentra registrado";
// $lang["employees_existing_email"] = "El Correo electrónico ya se encuentra registrado";
// $lang["employees_maximum_contacts_allowed"] = "Se permitirá un máximo de 3 contactos";
// $lang["employees_updated"] = "Empleado actualizado exitosamente";
// $lang["employees_not_updated"] = "Empleado no ha actualizado";

// $lang["employees_add_contract"] = "Agregar contrato";
// $lang["employees_edit_contract"] = "Editar contrato";