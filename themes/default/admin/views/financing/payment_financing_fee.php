<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="no-print notifications_container">
                <?php if (isset($pending_electronic_document_message)) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?= $pending_electronic_document_message; ?>
                    </div>
                <?php } ?>

                <?php if (isset($validationElectronicHist)) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?= $validationElectronicHist; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?= admin_form_open_multipart("financing/add", ['id' => 'addPaymentform']); ?>
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang('date', 'date') ?>
                                <?= form_input(["name" => "date", "id" => "date", "type" => "date", "class" => "form-control", "required" => true, "readonly" => true], date("Y-m-d")) ?>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?= lang("biller", "rcbiller"); ?>
                                <?php
                                $bl = [];
                                foreach ($billers as $biller) {
                                    $bl[$biller->id] = $biller;
                                }
                                ?>
                                <select name="biller" class="form-control" id="rcbiller" required="required">
                                    <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                        <?php foreach ($billers as $biller) : ?>
                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endforeach ?>
                                        <option value="all"><?= lang('allsf') ?></option>
                                    <?php } else { ?>
                                        <?php $biller = $bl[$this->session->userdata('biller_id')]; ?>
                                        <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-group">
                                <?php if ($this->Settings->manual_payment_reference == 1) { ?>
                                    <?= lang("reference_no", "document_type_id"); ?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" name="manual_reference" id="manual_reference" class="form-control" placeholder="<?= lang('type_manual_reference') ?>" required>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <?= lang("reference_no", "document_type_id"); ?>
                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                <?php } ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang('customer', 'creditCustomer') ?>
                                <?= $creditCustomer = (isset($_POST['creditCustomer']) ? $_POST['creditCustomer'] : "") ?>
                                <?= form_input(["name" => "creditCustomer", "id" => "creditCustomer", "class" => "form-control input-tip", "required" => true, "placeholder" => lang("select")], $creditCustomer); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("credit_branch", "credit_branch"); ?>
                                <?= form_dropdown('customerbranch', ['0' => lang("allsf")], (isset($_POST['customerbranch']) ? $_POST['customerbranch'] : ''), 'id="customerbranch" data-placeholder="' . lang("select") . '" class="form-control input-tip select"'); ?>
                            </div>
                        </div>

                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang("credit", "credit"); ?>
                                <?= form_dropdown('credit', ['0' => lang("alls")], (isset($_POST['credit']) ? $_POST['credit'] : ''), 'id="credit" data-placeholder="' . lang("select") . '" class="form-control input-tip select"'); ?>
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            <div class="form-group">
                                <?= lang('value_to_apply', 'value_to_apply') ?>
                                <?= form_input(["name" => "value_to_apply", "id" => "value_to_apply", "class" => "form-control number_mask"]) ?>
                            </div>
                        </div>
                        <?php if (isset($cost_centers)) : ?>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <?= lang('cost_center', 'cost_center_id') ?>
                                    <?php
                                    $ccopts[''] = lang('select');
                                    if ($cost_centers) {
                                        foreach ($cost_centers as $cost_center) {
                                            $ccopts[$cost_center->id] = "(" . $cost_center->code . ") " . $cost_center->name;
                                        }
                                    }
                                    ?>
                                    <?= form_dropdown(["name" => "cost_center_id", "id" => "cost_center_id", "class" => "form-control input-tip select", $ccopts]); ?>
                                </div>
                            </div>
                        <?php endif ?>
                        <div class="col-sm-2">
                            <div class="new-button-container">
                                <button class="btn btn-primary new-button" type="button" id="searchCreditsButton" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                            </div>
                        </div>
                        <div class="col-sm-4" id="containerForValueOfDeposits"></div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table" id="creditInstallmentTable">
                                <thead>
                                    <tr>
                                        <th><?= lang('biller') ?></th>
                                        <th><?= lang('credit') ?></th>
                                        <th><?= lang('fee') ?></th>
                                        <th><?= lang('due_date') ?></th>
                                        <th><?= lang('days_expired') ?></th>
                                        <th><?= lang('default_interest') ?></th>
                                        <th><?= lang('vat_default_interest') ?></th>
                                        <th><?= lang('current_interest') ?></th>
                                        <th><?= lang('vat_current_interest') ?></th>
                                        <th><?= lang('capital') ?></th>
                                        <th><?= lang('fee_value') ?></th>
                                        <th><?= lang('total_to_pay') ?></th>
                                        <th><input type="checkbox" id="mainCheck" checked></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>

                    <hr>

                    <div id="paymentAgreementContainer"></div>

                    <div id="payments">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="totalContainer" style="display: flex; align-items: center; height: 60px;">
                                    <h2 style="left: 0; font-size: 22px;"><?= lang("value_to_apply") ?>: <span id="totalToPay">0</span></h2>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="form-group text-right">
                                    <button type="button" class="btn btn-primary new-button" id="addPaymentMethod" data-toggle="tooltip" data-placement="top" title="<?= lang('add_more_payments') ?>"><i class="fas fa-plus"></i></button>
                                </div>
                            </div>
                        </div>

                        <div class="row" id="paymentsContainer">
                            <div class="col-sm-6">
                                <div class="well well-sm well_1" style="padding: 19px;">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <?= lang("value_to_apply", "amountPaid1"); ?>
                                                        <?= form_input(["name" => "amounPaid[]", "id" => "amountPaid1", "class" => "form-control amountPaid number_mask", "amountPaidPrev" => 0, "number" => "1"]) ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="form-group">
                                                        <?= lang("paying_by", "paid_by_1"); ?>
                                                        <select name="paid_by[]" id="paid_by_1" class="form-control paid_by" number="1" required>
                                                            <?= $this->sma->paid_opts('cash', false, true, false, true); ?>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3" id="totalReceivedContainer1">
                                                    <div class="form-group ngc">
                                                        <?= lang('value_received', 'totalReceived') ?>
                                                        <?= form_input(["name" => "totalReceived[]", "id" => "totalReceived1", "class" => "form-control number_mask totalReceived", "number" => "1"]) ?>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3" id="changeContainer1">
                                                    <div class="form-group">
                                                        <?= lang('change', 'change') ?>
                                                        <?= form_input(["name" => "change[]", "id" => "change1", "class" => "form-control number_mask change", "number" => "1", "readonly" => true]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <?= lang('payment_note', 'paymentNote1'); ?>
                                                        <?= form_textarea(["name" => "paymentNote[]", "id" => "paymentNote1", "class" => "form-control paymentNote"]) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <?= form_hidden('add_payment', '1'); ?>
                            <span class="text-danger negative_alert" style="display: none;"><?= lang('total_received_negative') ?></span>
                        </div>
                        <div class="col-sm-12 text-right">
                            <button type="button" class="btn btn-danger new-button" id="resetButton" data-toggle="tooltip" data-placement="top" title="<?= lang('reset') ?>"><i class="fas fa-undo"></i></button>
                            <button type="button" class="btn btn-success new-button" id="addFeePaymentButton" data-toggle="tooltip" data-placement="top" title="<?= lang('submit') ?>"><i class="fas fa-check"></i></button>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        _consecutivePaymentMethod = 1
        _paymentAmount = 0;
        _paymentToPay = 0;
        _discount = 0

        laodUtilities()
        creditCustomerAutocomplete()

        $(document).on('change', '#rcbiller', function() {
            loadDocumentTypeBranch($(this).val())
        })
        $(document).on('change', '#creditCustomer', function() {
            getCustomerCreditBranch($(this).val())
        })
        $(document).on('change', '#customerbranch', function() {
            getCreditsByCustomerBranch($(this).val())
        })
        $(document).on('click', '#searchCreditsButton', function() {
            searchCredit()
        })
        $(document).on('click', '#addFeePaymentButton', function() {
            addFeePayment()
        })
        $(document).on('click', '#addPaymentMethod', function() {
            addPaymentMethod()
        })
        $(document).on('change', '.amountPaid', function() {
            validateNotExceedValueToPay($(this))
            calculateAmountReceived($(this))
        })
        $(document).on('change', '.paid_by', function() {
            showHideAmountReceived($(this))
            showHideDeposits($(this))
        })
        $(document).on('change', '.totalReceived', function() {
            calculateAmountReceived($(this))
        })
        $(document).on('keydown', '#value_to_apply', function(event) {
            if (event.keyCode === 13) {
                searchCredit()
            }
        })
        $(document).on('click', '#resetButton', function() {
            location.reload();
        })
        $(document).on('click', '#paymentAgreementButton', function() {
            openPaymentAgreementModal()
        });
        $(document).on('change', '#default_interest_pa', function() {
            calculateVATInterestAJAX($(this), 'default')
        })
        $(document).on('change', '#currente_interest_pa', function() {
            calculateVATInterestAJAX($(this), 'current')
        })

        $(document).on('change', '#procredito_commission', function() {
            calculateProcreditoVATCommission($(this).val())
        })

        $(document).on('change', '#procredito_interest', function() {
            calculateProcreditoVATInterest($(this).val())
        })

        $(document).on('click', '#late_fee_discount_button', function() {
            _discount = parseFloat($('#late_fee_discount').val())
            calculateDiscountDistribution(_discount)
        })

        $('#rcbiller', ).trigger('change')
    });

    function laodUtilities() {
        $('[data-toggle="tooltip"]').tooltip()

        set_number_mask()
    }

    function loadIcheck() {
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%'
        })

        $('.installmentId').on('ifChecked', function(event) {
            reducePaymentAmount($(this));

            removeMethodPayments()
            removePaymentAgreement()
        });

        $('.installmentId').on('ifUnchecked', function(event) {
            increasePaymentAmount($(this));

            removeMethodPayments()
            removePaymentAgreement()
        });

        $('#mainCheck').on('ifChecked', function(event) {
            $('.installmentId').iCheck('check')
        })

        $('#mainCheck').on('ifUnchecked', function(event) {
            $('.installmentId').iCheck('uncheck')
        })
    }

    function creditCustomerAutocomplete() {
        $('#creditCustomer').select2({
            minimumInputLength: 1,
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            ajax: {
                url: site.base_url + "financing/creditCustomerSuggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function(term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function(data, page) {
                    if (data != null) {
                        return {
                            results: data
                        };
                    } else {
                        return {
                            results: [{
                                id: '',
                                text: lang.no_match_found
                            }]
                        };
                    }
                }
            }
        });
    }

    function loadDocumentTypeBranch(billerId) {
        localStorage.setItem('rcbiller', billerId);

        $.ajax({
            url: site.base_url + 'billers/getBillersDocumentTypes/59/' + billerId,
            type: 'get',
            dataType: 'JSON'
        }).done(function(data) {
            response = data;

            $('#document_type_id').html(response.options).select2();
            <?php if (!($Owner || $Admin)) { ?>
                $('#rcbiller').select2('readonly', true);
                $('#document_type_id').select2('readonly', true);
            <?php } ?>

            if (response.not_parametrized != "") {
                swal({
                    title: `<?= lang('toast_warning_title') ?>`,
                    text: `Los documentos <b> (${response.not_parametrized}) no están parametrizados </b> en contabilidad`,
                    type: 'warning',
                    showCancelButton: true,
                    showConfirmButton: false,
                    cancelButtonText: "Ok"
                })
            }

            if (response.status == 0) {
                swal({
                    title: `<?= lang('toast_warning_title') ?>`,
                    text: `<?= lang('biller_without_documents_types') ?>`,
                    type: 'warning',
                    showCancelButton: true,
                    showConfirmButton: false,
                    cancelButtonText: "Ok"
                })
            }

            $('#document_type_id').trigger('change');
        });
    }

    function getCustomerCreditBranch(customerId) {
        $.ajax({
            type: "get",
            url: site.base_url + 'financing/getCustomerCreditBranchAjax',
            data: {
                'customerId': customerId
            },
            dataType: "json",
            success: function(response) {
                let options = `<option value=""><?= lang('allsf') ?></option>`

                if (response != null) {
                    response.forEach(biller => {
                        options += `<option value="${biller.id}">${biller.name}</option>`
                    });

                    $('#customerbranch').html(options);
                }
            }
        })
    }

    function getCreditsByCustomerBranch(billerId) {
        let customerId = $('#creditCustomer').val();

        $.ajax({
            type: "get",
            url: site.base_url + 'financing/getCreditsByCustomerBranchAjax',
            data: {
                'customerId': customerId,
                'billerId': billerId,
            },
            dataType: "json",
            success: function(response) {
                let options = `<option value=""><?= lang('alls') ?></option>`

                if (response != null) {
                    response.forEach(credit => {
                        options += `<option value="${credit.id}">${credit.credit_no}</option>`
                    });

                    $('#credit').html(options);
                }
            }
        })
    }

    function searchCredit() {
        if (validateSearchFields()) {
            $.ajax({
                type: "post",
                async: false,
                url: site.base_url + `financing/searchCreditsAjax`,
                data: {
                    "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                    'creditCustomer': $('#creditCustomer').val(),
                    'customerbranch': $('#customerbranch').val(),
                    'credit': $('#credit').val(),
                },
                dataType: "json",
                success: function(response) {
                    let existingReportedCredits = response.existingReportedCredits
                    let showReportedCredits     = response.showReportedCredits
                    let deposits                = response.deposits
                    let credits                 = response.credits

                    if (Object.keys(credits).length !== 0) {
                        let rows = ''

                        if (existingReportedCredits == true) {
                            let notification = `<div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <strong>El cliente consultado tiene Créditos Reportados</strong>
                            </div>`
                            $('.notifications_container').html(notification);
                        }

                        if (showReportedCredits == true) {
                            if (deposits > 0) {
                                let  depositsHtml = `<div class="alert alert-success alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <h4>Anticipos a favor: ${formatMoney(deposits)}</h4>
                                </div>`
                                $('#containerForValueOfDeposits').html(depositsHtml);
                            }
                        }

                        credits.forEach(installment => {
                            let status = ''

                            if (installment.installmentAmount > 0) {
                                if (installment.daysExpired > 0) {
                                    status = 'warning'
                                }
                                if (installment.creditStatus == 2) {
                                    status = 'danger'
                                }

                                if (installment.creditStatus != 2) {
                                    see = true
                                } else if (installment.creditStatus == 2) {
                                    if (showReportedCredits == true) {
                                        see = true
                                    } else {
                                        see = false
                                    }
                                }

                                if (see == true) {
                                    rows += `<tr class="${status}" number="${installment.installmentId}">
                                        <td>${installment.billerName}</td>
                                        <td>${installment.creditNo}</td>
                                        <td>${installment.installmentNo}</td>
                                        <td>${installment.installmentDueDate}</td>
                                        <td>
                                            ${installment.daysExpired}
                                            <input type="hidden" name="daysExpired[${installment.installmentId}]" id="daysExpired${installment.installmentId}" value="${installment.daysExpired}"/>
                                        </td>
                                        <td>
                                            ${formatMoney(installment.defaultInterest)}
                                            <input type="hidden" name="defaultInterest[${installment.installmentId}]" value="${installment.defaultInterest}" id="defaultInterest${installment.installmentId}"/>
                                        </td>
                                        <td>
                                            ${formatMoney(installment.defaultInterestTax)}
                                            <input type="hidden" name="defaultInterestTax[${installment.installmentId}]" value="${installment.defaultInterestTax}" id="defaultInterestTax${installment.installmentId}"/>
                                        </td>
                                        <td>
                                            ${formatMoney(installment.currentInterestAmount)}
                                            <input type="hidden" name="currentInterestAmount[${installment.installmentId}]" value="${installment.currentInterestAmount}" id="currentInterestAmount${installment.installmentId}"/>
                                        </td>
                                        <td>
                                            ${formatMoney(installment.interestTax)}
                                            <input type="hidden" name="interestTax[${installment.installmentId}]" value="${installment.interestTax}" id="interestTax${installment.installmentId}"/>
                                        </td>
                                        <td>
                                            ${formatMoney(installment.capitalAmount)}
                                            <input type="hidden" name="capital[${installment.installmentId}]" value="${installment.capitalAmount}" id="capital${installment.installmentId}"/>
                                        </td>
                                        <td>${formatMoney(installment.installmentAmount)}</td>
                                        <td>
                                            <input name="totalInstallmentAmount[${installment.installmentId}]" id="totalInstallmentAmount${installment.installmentId}" class="form-control number_mask totalInstallmentAmount" readonly
                                                amount="${installment.installmentAmountTotal}"
                                                number="${installment.installmentId}"
                                                value="0"/>
                                        </td>
                                        <td>
                                            <input type="checkbox" class="icheck installmentId" name="installmentId[${installment.installmentId}]" id="installmentId${installment.installmentId}"
                                                number="${installment.installmentId}" />
                                        </td>
                                    </tr>`
                                }

                                if (installment.creditStatus == 2) {
                                    existingReportedCredits = true
                                }
                            }
                        })
                        $('#creditInstallmentTable tbody').html(rows)

                        if (showReportedCredits == true) {
                            if ($('#paymentAgreementButton').length === 0) {
                                let paymentAgreementbutton = `<button class="btn btn-primary new-button" type="button" id="paymentAgreementButton" data-toggle="tooltip" data-placement="bottom" title="Acuerdo de Pago"><i class="fas fa-business-time"></i></button>`
                                $('.new-button-container').append(paymentAgreementbutton);
                            }
                        }

                        paymentDistribution()
                        calculateValueAppliedPortfolio()
                        loadIcheck()

                        set_number_mask()
                    } else {
                        swal({
                            title: `<?= lang('toast_warning_title') ?>`,
                            text: `No se encontró ningún registro con los filtros aplicados.`,
                            type: 'warning',
                            showCancelButton: true,
                            showConfirmButton: false,
                            cancelButtonText: "Ok"
                        }, function(result) {
                            return false
                        })
                    }

                    removePaymentAgreement()
                }
            });
        }
    }

    function validateSearchFields() {
        if ($('#document_type_id').select2('val') == '') {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= sprintf(lang('field_required'), lang('reference_no')) ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                $('#document_type_id').select2('open')
            })

            return false
        } else if ($('#creditCustomer').select2('val') == '') {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= sprintf(lang('field_required'), lang('customer')) ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                $('#creditCustomer').select2('open')
            })

            return false
        } else {
            return true
        }
    }

    function paymentDistribution() {
        _paymentAmount = $('#value_to_apply').val()

        if (_paymentAmount > 0) {
            $(".totalInstallmentAmount").each(function() {
                number = $(this).attr('number')

                if (_paymentAmount > 0) {
                    value = 0
                    quoteAmount = parseFloat($(this).attr('amount'))

                    if (quoteAmount <= _paymentAmount) {
                        value = quoteAmount
                        $(`#installmentId${number}`).iCheck('check');
                        _paymentAmount -= value
                    } else {
                        _paymentAmount = 0
                    }

                    $(this).val(value)
                    _paymentAmount = (_paymentAmount < 0) ? 0 : parseFloat(_paymentAmount.toFixed(2))
                }

            })
        } else {
            loadIcheck()

            $(".totalInstallmentAmount").each(function() {
                number = $(this).attr('number')
                daysExpired = parseFloat($(`#daysExpired${number}`).val())

                if (daysExpired > 0) {
                    $(`#installmentId${number}`).iCheck('check');
                }
            })
        }

    }

    function reducePaymentAmount(element) {
        let number = element.attr('number')
        let totalInstallmentAmount = $(`#totalInstallmentAmount${number}`).attr('amount')

        $(`#totalInstallmentAmount${number}`).val(formatDecimals(totalInstallmentAmount))

        calculateValueAppliedPortfolio()
    }

    function increasePaymentAmount(element) {
        let number = element.attr('number')
        let installmentAmount = parseFloat($(`#totalInstallmentAmount${number}`).val())

        $(`#totalInstallmentAmount${number}`).val(0)

        calculateValueAppliedPortfolio()
    }

    function calculateValueAppliedPortfolio() {
        let totalAmount = 0
        $('.totalInstallmentAmount').each(function() {
            amount = ($(this).val() === null || $(this).val() === '') ? 0 : $(this).val()
            totalAmount += parseFloat(amount);
        })

        _paymentToPay = parseFloat(totalAmount.toFixed(2))

        $('#totalToPay').html(formatMoney(parseFloat(_paymentToPay)))
        $('#value_to_apply').val(parseFloat(0))

        $('#amountPaid1').attr('amountPaidPrev', 0)
        $('#amountPaid1').val(parseFloat(_paymentToPay)).trigger('change')
    }

    function addFeePayment() {
        if (validateFormFields()) {
            $('#addPaymentform').submit()
        }
    }

    function validateFormFields() {
        let totalReceivedEmpty = 0
        let checksSelected = 0
        let paidByEmpty = 0

        $('.installmentId').each(function(index, check) {
            if ($(check).is(':checked')) {
                checksSelected++
            }
        });

        $('select.paid_by').each(function(index, paidBy) {
            if ($(paidBy).val() == '') {
                paidByEmpty++
                paidByEmptyIndex = (index + 1)
            }
        });

        $('.totalReceived').each(function(index, totalReceived) {
            if ($(totalReceived).val() == '') {
                totalReceivedEmpty++
                totalReceivedEmptyIndex = (index + 1)
            }
        });

        if ($('#creditCustomer').val() == '') {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `El pago aún no se encuentra distribuido. Por favor seleccione las cuotas a pagar`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                return false
            })
        } else if (checksSelected == 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('El pago aún no se encuentra distribuido. Por favor seleccione las cuotas a pagar') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                return false
            })
        } else if (paidByEmpty > 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('El campo forma de pago debe ser obligatorio') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                $(`#paid_by_${paidByEmptyIndex}`).select2('open')
            });
        } else if (totalReceivedEmpty > 0 && $(`#paid_by_${totalReceivedEmptyIndex}`).val() == 'cash') {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('El campo Valor recibido debe ser obligatorio') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                $(`#totalReceived${totalReceivedEmptyIndex}`).focus()
            });
        } else if (_paymentToPay > 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('Falta por completar pagos para las cuotas') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            });
        } else {
            return true
        }
    }

    function addPaymentMethod() {
        if (validateAddPaymentMethod()) {
            _consecutivePaymentMethod++

            let ignoreCash = false
            let ignoreDeposit = false

            $('.paid_by').each(function(i, e) {
                if ($(this).val() == 'cash') {
                    ignoreCash = true
                }
                if ($(this).val() == 'deposit') {
                    ignoreDeposit = true
                }
            })

            if (ignoreCash == true) {
                if (ignoreDeposit == true) {
                    options = '<?= $this->sma->paid_opts(null, false, true, true, true, false, false, true); ?>'
                } else {
                    options = '<?= $this->sma->paid_opts(null, false, true, false, true, false, false, true); ?>'
                }
            } else {
                if (ignoreDeposit == true) {
                    options = '<?= $this->sma->paid_opts(null, false, true, true, true, false, false, false); ?>'
                } else {
                    options = '<?= $this->sma->paid_opts(null, false, true, false, true, false, false, false); ?>'
                }
            }

            let paymentMethodHtml = `<div class="col-sm-6">
                <div class="well well-sm well_${_consecutivePaymentMethod}" style="padding: 19px;">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="amountPaid${_consecutivePaymentMethod}"><?= lang("value_to_apply") ?></label>
                                        <input name="amounPaid[]" id="amountPaid${_consecutivePaymentMethod}" class="form-control amountPaid number_mask" number="${_consecutivePaymentMethod}" amountPaidPrev="${_paymentToPay}" value="${_paymentToPay}" />
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                        <label for="paid_by_${_consecutivePaymentMethod}"><?= lang("paying_by") ?></label>
                                        <select name="paid_by[]" id="paid_by_${_consecutivePaymentMethod}" class="form-control paid_by" number="${_consecutivePaymentMethod}">
                                            ${options}
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-3 hide" id="totalReceivedContainer${_consecutivePaymentMethod}">
                                    <div class="form-group ngc">
                                        <label for="totalReceived${_consecutivePaymentMethod}"><?= lang("value_received") ?></label>
                                        <input name="totalReceived[]" id="totalReceived${_consecutivePaymentMethod}" class="form-control number_mask totalReceived" number="${_consecutivePaymentMethod}" />
                                    </div>
                                </div>
                                <div class="col-sm-3 hide" id="changeContainer${_consecutivePaymentMethod}">
                                    <div class="form-group">
                                        <label for="change${_consecutivePaymentMethod}"><?= lang("change") ?></label>
                                        <input name="change[]" id="change${_consecutivePaymentMethod}" class="form-control number_mask change" number="${_consecutivePaymentMethod}" readonly />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label for="payment_note${_consecutivePaymentMethod}"><?= lang("payment_note") ?></label>
                                        <textarea name="paymentNote[]" id="paymentNote${_consecutivePaymentMethod}" class="form-control paymentNote"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`
            $('#paymentsContainer').append(paymentMethodHtml)

            _paymentToPay = 0
            $(`#amountPaid${_consecutivePaymentMethod}`).trigger('change')

            reloadComplementsJs()
        }
    }

    function validateAddPaymentMethod() {
        if (_paymentToPay == 0 || _consecutivePaymentMethod < 1) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('No es posible agregar más formas de pago') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            })
            return false
        } else {
            return true
        }
    }

    function reloadComplementsJs() {
        $('.paid_by').select2('destroy');
        $(".paid_by").select2({
            formatResult: payment_method_icons,
            formatSelection: payment_method_icons,
            escapeMarkup: function(m) {
                return m;
            }
        })

        $('.paymentNote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100
        })

        set_number_mask()
    }

    function validateNotExceedValueToPay(element) {
        let amountToPayPayment = parseFloat(element.val())
        let amounPaidPrev = parseFloat(element.attr('amountPaidPrev'))

        _paymentToPay += amounPaidPrev

        if (amountToPayPayment > _paymentToPay) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `El Valor a pagar del pago no debe exceder el Valor a pagar`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                _paymentToPay -= amounPaidPrev

                element.val(amounPaidPrev)
                element.focus()
            })
            return false
        } else {
            _paymentToPay -= amountToPayPayment
            _paymentToPay = parseFloat(_paymentToPay.toFixed(2))
            element.attr('amountPaidPrev', amountToPayPayment)
        }
    }

    function showHideAmountReceived(element) {
        let number = element.attr('number')
        let methodPayment = element.val()

        if (methodPayment == 'cash') {
            $(`#totalReceivedContainer${number}`).removeClass('hide')
            $(`#changeContainer${number}`).removeClass('hide')
        } else {
            $(`#totalReceivedContainer${number}`).addClass('hide')
            $(`#changeContainer${number}`).addClass('hide')
        }
    }

    function showHideDeposits(element) {
        let methodPayment = element.val()
        let number = element.attr('number')
        let amount = $(`#amountPaid${number}`).val()

        if (methodPayment == "deposit") {
            let customer = $('#creditCustomer').val()

            $.ajax({
                url: site.base_url + `customers/deposit_balance/${customer}/${amount}`,
            }).done(function(data) {
                let balance = $(data).find('#balance').attr('balance')

                $(`#amountPaid${number}`).parents('.row').eq(0).after(data)
                $(`#amountPaid${number}`).val(formatDecimals(balance)).trigger('change')
            });
        } else {
            $(`#amountPaid${number}`).parents(`.well_${number}`).find('.deposit_message').remove()
        }
    }

    function calculateAmountReceived(element) {
        let number = element.attr('number')

        let amountToPayPayment = $(`#amountPaid${number}`).val()
        let amountReceived = $(`#totalReceived${number}`).val()

        let result = ((amountReceived - amountToPayPayment) <= 0) ? 0 : (amountReceived - amountToPayPayment)
        $(`#change${number}`).val(parseFloat(result.toFixed(2)))
    }

    function removeMethodPayments() {
        $('#paymentsContainer .col-sm-6').each(function(index, paymentMethod) {
            if (index != 0) {
                $(this).remove()
                $('.totalReceived').val(0).trigger('change')
            }
        })
    }

    function openPaymentAgreementModal() {
        let capital = 0
        let checksSelected = 0
        let currentInterest = 0
        let defaultInterest = 0
        let defaultInterestTax = 0
        let currentInterestTax = 0
        let totalInstallmentAmount = 0
        let installmentSelected = []
        let procreditoCommission = 0

        $('.installmentId').each(function(index, check) {
            if ($(check).is(':checked')) {
                let number = $(check).attr('number')

                checksSelected++

                defaultInterest += parseFloat($(`#defaultInterest${number}`).val())
                defaultInterestTax += parseFloat($(`#defaultInterestTax${number}`).val())
                currentInterest += parseFloat($(`#currentInterestAmount${number}`).val())
                currentInterestTax += parseFloat($(`#interestTax${number}`).val())
                capital += parseFloat($(`#capital${number}`).val())
                totalInstallmentAmount += parseFloat($(`#totalInstallmentAmount${number}`).val())
            }
        });

        if (validateInstallmentSelected(checksSelected)) {
            let procreditoInterest   = calculateProcreditoInterest(defaultInterest)
            let procreditoCommission = calculateProcreditoCommission(capital)

            if (parseFloat(procreditoCommission.commission) > 0) {
                totalInstallmentAmount += parseFloat(procreditoCommission.commission)
                totalInstallmentAmount += parseFloat(procreditoCommission.VATinterest)
            }
            if (parseFloat(procreditoInterest.interest) > 0) {
                totalInstallmentAmount += parseFloat(procreditoInterest.interest)
                totalInstallmentAmount += parseFloat(procreditoInterest.VATinterest)
            }

            let paymentAgreementContainerHtml = `<div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Configurar Acuerdo de Pago
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="default_interest_pa"><?= lang("default_interest") ?></label>
                                        <input class="form-control number_mask" name="default_interest_pa" id="default_interest_pa" value="${formatDecimals(defaultInterest)}">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="vat_default_interest_pa"><?= lang("vat_default_interest") ?></label>
                                        <input class="form-control number_mask" name="vat_default_interest_pa" id="vat_default_interest_pa" value="${formatDecimals(defaultInterestTax)}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="currente_interest_pa"><?= lang("current_interest") ?></label>
                                        <input class="form-control number_mask" name="currente_interest_pa" id="currente_interest_pa" value="${formatDecimals(currentInterest)}">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="vat_currente_interest_pa"><?= lang("vat_current_interest") ?></label>
                                        <input class="form-control number_mask" name="vat_currente_interest_pa" id="vat_currente_interest_pa" value="${formatDecimals(currentInterestTax)}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="capital_pa"><?= lang("capital") ?></label>
                                        <input class="form-control number_mask" name="capital_pa" id="capital_pa" value="${formatDecimals(capital)}" readonly>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="procredito_commission"><?= lang("procredito_commission") ?></label>
                                        <input class="form-control number_mask" name="procredito_commission" id="procredito_commission" value="${formatDecimals(procreditoCommission.commission)}">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="vat_procredito_commission"><?= lang("vat_procredito_commission") ?></label>
                                        <input class="form-control number_mask" name="vat_procredito_commission" id="vat_procredito_commission" value="${formatDecimals(procreditoCommission.VATinterest)}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="procredito_interest"><?= lang("procredito_interest") ?></label>
                                        <input class="form-control number_mask" name="procredito_interest" id="procredito_interest" value="${formatDecimals(procreditoInterest.interest)}">
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="vat_procredito_interest"><?= lang("vat_procredito_interest") ?></label>
                                        <input class="form-control number_mask" name="vat_procredito_interest" id="vat_procredito_interest" value="${formatDecimals(procreditoInterest.VATinterest)}" readonly>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="late_fee_discount"><?= lang("late_fee_discount") ?></label>
                                        <div class="input-group">
                                            <input class="form-control number_mask" name="late_fee_discount" id="late_fee_discount" value="${formatDecimals(0)}">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary" type="button" id="late_fee_discount_button"><i class="fas fa-calculator"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label for="total_installment_amount_pa"><?= lang("total_to_pay") ?></label>
                                        <input class="form-control number_mask" name="total_installment_amount_pa" id="total_installment_amount_pa" value="${formatDecimals(totalInstallmentAmount)}" readonly>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>`

            $('#paymentAgreementContainer').html(paymentAgreementContainerHtml)

            $('html, body').animate({
                scrollTop: $('#paymentAgreementContainer').offset().top
            }, 1000);

            recalculatePaymentAgreement()
            reloadComplementsJs()
        }
    }

    function calculateProcreditoCommission(capital) {
        let data = false
        if (capital > 0) {
            $.ajax({
                type: "post",
                async: false,
                url: site.base_url+'financing/calculateProcreditoCommissionAJAX',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'capital' : capital
                },
                dataType: "json",
                success: function (response) {
                    data = response;
                }
            });
        }

        return data
    }

    function calculateProcreditoInterest(procreditoInterest) {
        let data = '';
        if (procreditoInterest > 0) {
            $.ajax({
                type: "post",
                async: false,
                url: site.base_url+'financing/calculateProcreditoInterestAJAX',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'procreditoInterest' : procreditoInterest
                },
                dataType: "json",
                success: function (response) {
                    data = response
                }
            })
        }

        return data
    }

    function calculateTotalPaymentAgreement() {
        let totalInstallmentAmount  = parseFloat($('#total_installment_amount_pa').val())

        let procreditoCommission    = parseFloat($('#procredito_commission').val())
        let vatProcreditoCommission = parseFloat($('#vat_procredito_commission').val())
        let procreditoInterest      = parseFloat($('#procredito_interest').val())
        let vatProcreditoInterest   = parseFloat($('#vat_procredito_interest').val())

        total = (totalInstallmentAmount + procreditoCommission + vatProcreditoCommission + procreditoInterest + vatProcreditoInterest);

        $('#total_installment_amount_pa').val(formatDecimals(total));
    }

    function validateInstallmentSelected(checksSelected) {
        if (checksSelected == 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `<?= lang('Por favor seleccione las cuotas del acuerdo de pago') ?>`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                return false
            })
        } else {
            return true
        }
    }

    function removePaymentAgreement() {
        $('#paymentAgreementContainer').html('')
    }

    function calculateVATInterestAJAX(element, type) {
        $.ajax({
            type: "post",
            url: site.base_url + 'financing/calculateVATInterestAJAX',
            data: {
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                'baseValue': element.val(),
                'type': type
            },
            dataType: 'json',
            success: function(response) {
                response
                if (type == 'default') {
                    $('#vat_default_interest_pa').val(response.interestTax)
                } else {
                    $('#vat_currente_interest_pa').val(response.interestTax)
                }

                recalculatePaymentAgreement()
            }
        })
    }

    function recalculatePaymentAgreement() {
        let capital                 = parseFloat($('#capital_pa').val())
        let defaultInterest         = parseFloat($('#default_interest_pa').val())
        let vatDefaultInterest      = parseFloat($('#vat_default_interest_pa').val())
        let currenteInterest        = parseFloat($('#currente_interest_pa').val())
        let vatCurrenteInterest     = parseFloat($('#vat_currente_interest_pa').val())

        let procreditoCommission    = parseFloat($('#procredito_commission').val())
        let vatProcreditoCommission = parseFloat($('#vat_procredito_commission').val())
        let procreditoInterest      = parseFloat($('#procredito_interest').val())
        let vatProcreditoInterest   = parseFloat($('#vat_procredito_interest').val())

        let totalInstallmentAmount  = defaultInterest + vatDefaultInterest + currenteInterest + vatCurrenteInterest + capital
        totalInstallmentAmount += (procreditoCommission + vatProcreditoCommission + procreditoInterest + vatProcreditoInterest)

        $('#total_installment_amount_pa').val(totalInstallmentAmount.toFixed(2))

        _paymentToPay = parseFloat(formatDecimals(totalInstallmentAmount))

        $('#totalToPay').html(formatMoney(parseFloat(_paymentToPay)))
        $('#value_to_apply').val(parseFloat(0))

        $('#amountPaid1').attr('amountPaidPrev', 0)
        $('#amountPaid1').val(parseFloat(_paymentToPay)).trigger('change')

        $('#totalReceived1').val(0).trigger('change')
        removeMethodPayments()
    }

    function calculateProcreditoVATCommission(commission) {
        if (commission > 0) {
            $.ajax({
                type: "post",
                async: false,
                url: site.base_url+'financing/calculateProcreditoVATCommission',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'commission' : commission
                },
                dataType: "json",
                success: function (response) {
                    console.log(response)
                    $('#vat_procredito_commission').val(response.VATinterest)
                }
            });
        } else {
            $('#vat_procredito_commission').val(0)
        }

        recalculatePaymentAgreement()
    }

    function calculateProcreditoVATInterest(interest) {
        if (interest > 0) {
            $.ajax({
                type: "post",
                async: false,
                url: site.base_url+'financing/calculateProcreditoVATInterest',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'interest' : interest
                },
                dataType: "json",
                success: function (response) {
                    $('#vat_procredito_interest').val(response.VATinterest)
                }
            });
        } else {
            $('#vat_procredito_interest').val(0)
        }

        recalculatePaymentAgreement()
    }

    function calculateDiscountDistribution(_discount) {
        if (validateDiscountValue()) {
            calculateBaseAndVatAjax($('#default_interest_pa'), $('#vat_default_interest_pa'))
            calculateBaseAndVatAjax($('#currente_interest_pa'), $('#vat_currente_interest_pa'))
            calculateBaseAndVatAjax($('#procredito_commission'), $('#vat_procredito_commission'))
            calculateBaseAndVatAjax($('#procredito_interest'), $('#vat_procredito_interest'))

            recalculatePaymentAgreement()

            $('#late_fee_discount_button').prop('disabled', true)
            $('#late_fee_discount').val('')
        }
    }

    function calculateBaseAndVatAjax(elementBase, elementVat) {
        let base = parseFloat(elementBase.val())
        let vat  = parseFloat(elementVat.val())
        let total= base + vat

        if (_discount > 0 && (total > 0)) {
            $.ajax({
                type: "post",
                async: false,
                url: site.base_url + 'financing/calculateBaseAndVatAJAX',
                data: {
                    '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                    'amount': _discount,
                    'type': 'default'
                },
                dataType: "json",
                success: function (response) {
                    base -= response.base
                    vat  -= response.vat

                    base = (base < 0) ? 0 : base
                    vat  = (vat < 0) ? 0 : vat

                    elementBase.val(base)
                    elementVat.val(vat)

                    if (_discount <= total) {
                        _discount = 0
                    } else {
                        _discount -= total
                    }
                }
            })
        }
    }

    function validateDiscountValue()
    {
        if (_discount <= 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `Por favor ingrese un valor mayor a cero`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            })

            return false;
        } else {
            return true;
        }
    }
</script>