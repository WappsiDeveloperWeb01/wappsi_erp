<?php

/***********

FORMATO PARA VISUALIZACIÓN DE RECIBOS DE CAJA MÚLTIPLES

************/

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{
    function Header()
    {
        //izquierda
        $this->RoundedRect(13, 7, 117, 26, 3, '1234', '');

        //izquierda
        if ($this->biller_logo == 2) {
            # Teniendo en cuenta que son 13 de margen y empieza en el 16 debe terminar en el 53 por los dos puntos de margen derecho,
            # quedan => 37 de ancho. La altura para que quede centrado y no alterar relacion, se conoce la altura del contenedor 26, 
            # margen superior 7, se calcula la relacion de la imagen para obtener la margen superior del contendor
            $info_img = getimagesize(base_url().'assets/uploads/logos/'.$this->logo);
            $relacion = $info_img[0] / $info_img[1]; // 150 / 150 = > 1
            $alturaRelativa = 37 / $relacion; // 37 / 1 = > 37
            $anchuraRelativa = (((26 - $alturaRelativa) / 2) < 2) ? 23 : 37; // 26-37/2
            $posicionAlto = ((((26 - $alturaRelativa) / 2) + 7) < 8) ? 8 : ((26 - $alturaRelativa) / 2) + 7; // 26 - 37 /2 = > 7 
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo, 16, $posicionAlto, $anchuraRelativa);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,16,16,21.5);
        }

        $cx = 55;
        $cy = 11;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper($this->biller->company != '-' ? $this->biller->company : $this->biller->name)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Nit : '.$this->biller->vat_no.($this->biller->tipo_documento == 6 ? "-".$this->biller->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Teléfonos : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode('Correo : '.$this->biller->email),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 26, 3, '1234', '');

        $cx = 132;

        //derecha
        $cy = 9;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : 'Recibo de caja'),'',1,'C');
        $cy +=6;
        $this->setXY($cx, $cy);
        $this->Cell(77, 10 , $this->sma->utf8Decode('N°. '.$this->inv[0]->origin_reference_no),'',1,'C');

        //izquierda

        $cx = 13;
        $cy = 35;

        $this->setXY($cx, $cy);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Fecha:'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->inv[0]->date),'',1,'L');

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Bodega :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->warehouse->name),'',1,'L');

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(35.1, 4, $this->sma->utf8Decode('Creado por :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(81.9, 4, $this->sma->utf8Decode($this->created_by->first_name." ".$this->created_by->last_name),'',1,'L');

        //derecha

        $cx = 132;
        $cy = 35;
        $this->setXY($cx, $cy);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(18, 4, $this->sma->utf8Decode('Tercero : '),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(0, 4, $this->sma->utf8Decode($this->employee->company),'',1,'L');

        $cx = 132;
        $cy += 4;
        $this->setXY($cx, $cy);

        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->Cell(18, 4, $this->sma->utf8Decode('Nit / CC :'),'',0,'L');
        $this->SetFont('Arial','',$this->fuente+$this->adicional_fuente);
        $this->Cell(0, 4, $this->sma->utf8Decode($this->employee->vat_no),'',1,'L');

        $this->ln(4);

        $this->SetFont('Arial','B',$this->fuente);
        $this->Cell(5, 5, $this->sma->utf8Decode('N°'),'B',0,'C');
        if ($this->Owner || $this->Admin) {
            $this->Cell(28.5, 5, $this->sma->utf8Decode('Código'),'B',0,'C');
            $this->Cell(74.5, 5, $this->sma->utf8Decode('Descripción'),'B',0,'C');
        } else {
            $this->Cell(54, 5, $this->sma->utf8Decode('Código'),'B',0,'C');
            $this->Cell(100, 5, $this->sma->utf8Decode('Descripción'),'B',0,'C');
        }
        $this->Cell(17.5, 5, $this->sma->utf8Decode('Tipo'),'B',0,'C');
        $this->Cell(19.5, 5, $this->sma->utf8Decode('Cantidad'),'B',(($this->Owner || $this->Admin) ? 0 : 1),'C');
        if ($this->Owner || $this->Admin) {
            $this->Cell(25.5, 5, $this->sma->utf8Decode('Costo Unitario'),'B',0,'C');
            $this->Cell(25.5, 5, $this->sma->utf8Decode('Costo Total'),'B',1,'C');
        }

    }

    function Footer()
    {
        // Print centered page number
    }

    function reduceTextToDescription1($text){
        $text="Nota : ".strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 325) {
            $text = substr($text, 0, 320);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function reduceTextToDescription2($text){
        $text=strip_tags($this->sma->decode_html($text));
        if (strlen($text) > 805) {
            $text = substr($text, 0, 800);
            $text.="...";
            return $text;
        }
        return $text;
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$pdf->setTitle($this->sma->utf8Encode('Ajuste de cantidades'));
$fuente = 6.5;
$adicional_fuente = 3;
$color_R = 200;
$color_G = 200;
$color_B = 200;
$number_convert = new number_convert();

$pdf->setFillColor($color_R,$color_G,$color_B);
$pdf->biller = $biller;
$pdf->employee = $employee;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->document_type = $document_type;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->Settings = $this->Settings;
$pdf->sma = $this->sma;
if (isset($cost_center)) {
    $pdf->cost_center = $cost_center;
}

$pdf->logo = $biller->logo_square;
$pdf->biller_logo = 2;
$pdf->tipo_regimen = $tipo_regimen;
$pdf->inv = $inv;
$pdf->warehouse = $warehouse;
$pdf->created_by = $created_by;
// $this->Owner = false;
// $this->Admin = false;
$pdf->Admin = $this->Admin;
$pdf->Owner = $this->Owner;

$pdf->AddPage();

$maximo_footer = 235;

$pdf->SetFont('Arial','',$fuente+1);

$num = 1;

$x_qty = null;
$x_cost = null;
$total_qty = 0;
$total_cost = 0;
// for ($i=0; $i < 99 ; $i++) { 
foreach ($rows[$inv[0]->id] as $row) {

    if ($row->type == 'addition') {
        continue;
    }
    if ($pdf->getY() > 250) {
        $pdf->AddPage();
    }

    $columns = [];
    $inicio_fila_X = $pdf->getX();
    $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $pdf->getX();
        $pdf->Cell(5, 5, $this->sma->utf8Decode($num),'',0,'L');
    $columns[0]['fin_x'] = $pdf->getX();
    $columns[1]['inicio_x'] = $pdf->getX();
        $pdf->Cell((($this->Owner || $this->Admin) ? 28.5 : 54), 5, $this->sma->utf8Decode($row->code),'',0,'L');
    $columns[1]['fin_x'] = $pdf->getX();
    $columns[2]['inicio_x'] = $pdf->getX();
    $cX = $pdf->getX();
    $cY = $pdf->getY();
    $inicio_altura_fila = $cY;
    $pdf->setY($cY+1);
    $pdf->setX($cX);
    if ($this->Owner || $this->Admin) {
        $ancho_cell = 74.5;
    } else {
        $ancho_cell = 100;
    }
        $pdf->MultiCell($ancho_cell, 2.5, $this->sma->utf8Decode($row->name),'','L');
    $columns[2]['fin_x'] = $cX+$ancho_cell;
    $fin_altura_fila = $pdf->getY();
    $pdf->setXY($cX+$ancho_cell, $cY);
    $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(17.5, 5, $this->sma->utf8Decode(($row->type == 'addition') ? lang('destination') : lang('origin')),'',0,'L');
    $columns[4]['fin_x'] = $pdf->getX();
    $columns[5]['inicio_x'] = $pdf->getX();
        $x_qty = $pdf->getX();
        $pdf->Cell(19.5, 5, $this->sma->utf8Decode($this->sma->formatNumber($row->quantity)),'',0,'R');
    $columns[5]['fin_x'] = $pdf->getX();
    if ($this->Owner || $this->Admin) {
        $columns[6]['inicio_x'] = $pdf->getX();
            $pdf->Cell(25.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($row->adjustment_cost > 0 ? ($row->adjustment_cost) : $row->avg_cost)),'',0,'R');
        $columns[6]['fin_x'] = $pdf->getX();
        $columns[7]['inicio_x'] = $pdf->getX();
            $x_cost = $pdf->getX();
            $pdf->Cell(25.5, 5, $this->sma->utf8Decode($this->sma->formatMoney(($row->adjustment_cost > 0 ? $row->adjustment_cost : $row->avg_cost) * $row->quantity)),'',0,'R');
        $columns[7]['fin_x'] = $pdf->getX();
    }
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = ($fin_altura_fila - $inicio_altura_fila)+1;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    foreach ($columns as $key => $data) {
        $ancho_column = ($data['fin_x'] - $data['inicio_x']);
        if ($altura_fila < 5) {
            $altura_fila = 5;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'B',0,'R');
    }
    $pdf->ln($altura_fila);
    $num++;
    $total_qty += $row->quantity;
    $total_cost += (($row->adjustment_cost > 0 ? $row->adjustment_cost : $row->avg_cost) * $row->quantity);

}

foreach ($rows[$inv[0]->id] as $row) {

    if ($row->type == 'subtraction') {
        continue;
    }
    if ($pdf->getY() > 250) {
        $pdf->AddPage();
    }

    $columns = [];
    $inicio_fila_X = $pdf->getX();
    $inicio_fila_Y = $pdf->getY();
    $columns[0]['inicio_x'] = $pdf->getX();
        $pdf->Cell(5, 5, $this->sma->utf8Decode($num),'',0,'L');
    $columns[0]['fin_x'] = $pdf->getX();
    $columns[1]['inicio_x'] = $pdf->getX();
        $pdf->Cell((($this->Owner || $this->Admin) ? 28.5 : 54), 5, $this->sma->utf8Decode($row->code),'',0,'L');
    $columns[1]['fin_x'] = $pdf->getX();
    $columns[2]['inicio_x'] = $pdf->getX();
    $cX = $pdf->getX();
    $cY = $pdf->getY();
    $inicio_altura_fila = $cY;
    $pdf->setY($cY+1);
    $pdf->setX($cX);
    if ($this->Owner || $this->Admin) {
        $ancho_cell = 74.5;
    } else {
        $ancho_cell = 100;
    }
        $pdf->MultiCell($ancho_cell, 2.5, $this->sma->utf8Decode($row->name),'','L');
    $columns[2]['fin_x'] = $cX+$ancho_cell;
    $fin_altura_fila = $pdf->getY();
    $pdf->setXY($cX+$ancho_cell, $cY);
    $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(17.5, 5, $this->sma->utf8Decode(($row->type == 'addition') ? lang('destination') : lang('origin')),'',0,'L');
    $columns[4]['fin_x'] = $pdf->getX();
    $columns[5]['inicio_x'] = $pdf->getX();
        $x_qty = $pdf->getX();
        $pdf->Cell(19.5, 5, $this->sma->utf8Decode($this->sma->formatNumber($row->quantity)),'',0,'R');
    $columns[5]['fin_x'] = $pdf->getX();
    if ($this->Owner || $this->Admin) {
        $columns[6]['inicio_x'] = $pdf->getX();
            $pdf->Cell(25.5, 5, $this->sma->utf8Decode($this->sma->formatMoney($row->adjustment_cost > 0 ? ($row->adjustment_cost) : $row->avg_cost)),'',0,'R');
        $columns[6]['fin_x'] = $pdf->getX();
        $columns[7]['inicio_x'] = $pdf->getX();
            $x_cost = $pdf->getX();
            $pdf->Cell(25.5, 5, $this->sma->utf8Decode($this->sma->formatMoney(($row->adjustment_cost > 0 ? $row->adjustment_cost : $row->avg_cost) * $row->quantity)),'',0,'R');
        $columns[7]['fin_x'] = $pdf->getX();
    }
    $fin_fila_X = $pdf->getX();
    $fin_fila_Y = $pdf->getY();
    $ancho_fila = $fin_fila_X - $inicio_fila_X;
    $altura_fila = ($fin_altura_fila - $inicio_altura_fila)+1;
    $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
    foreach ($columns as $key => $data) {
        $ancho_column = ($data['fin_x'] - $data['inicio_x']);
        if ($altura_fila < 5) {
            $altura_fila = 5;
        }
        $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'B',0,'R');
    }
    $pdf->ln($altura_fila);
    $num++;
    $total_qty += $row->quantity;
    $total_cost += (($row->adjustment_cost > 0 ? $row->adjustment_cost : $row->avg_cost) * $row->quantity);

}
// }

if ($pdf->getY() > 240) {
    $pdf->AddPage();
}


$pdf->ln(5);
$pdf->MultiCell(196, 5 , $this->sma->utf8Decode('Nota : '.strip_tags($this->sma->decode_html($inv[0]->note))),'','L');
$pdf->ln(5);

$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(49, 20, $this->sma->utf8Decode(''),'',0,'R');
$pdf->setXY($cx+4.5, $cy+15);
$pdf->Cell(40, 5, $this->sma->utf8Decode('Recibido por'),'T',0,'C');

$pdf->ln(5);

$cx = $pdf->getX();
$cy = $pdf->getY();
$pdf->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
$pdf->setXY($cx+185, $cy);
$pdf->Cell(7, 7 , $this->sma->utf8Decode('Pagina N° '.$pdf->PageNo()),'',1,'C');
$download = false;
$pdf->Output("factura_venta.pdf", "I");