<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Informe de ordenes de pedido'));
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->setY(7);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->setXY($cx+76.06, $cy);
        $cx = $this->getX();
        $cy = $this->getY();
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Informe de ordenes de pedido'), '', ($this->wh_data ? 0 : 1), 'C');
        $this->setXY($cx, $cy+5);
        $this->SetFont('Arial','B', $this->fontsize_title);
        $this->cell(111.97, 5, $this->sma->utf8Decode('Fecha impresión : '.date('Y-m-d H:i:s')), '', ($this->wh_data ? 0 : 1), 'C');
        $this->SetFont('Arial','B', $this->fontsize_title+4);
        if ($this->wh_data) {
            $this->setXY($cx+111.97, $cy);
            $this->cell(75.97, 5, $this->sma->utf8Decode('Sucursal : '.$this->wh_data->name), '', 1, 'C');
        }
        $this->ln();
        $this->SetFont('Arial','B', $this->fontsize_title);
        $cx = $this->getX();
        $cy = $this->getY();


        $this->cell(30.44, 8, $this->sma->utf8Decode('Categoría'), 'TLR', 0, 'C');
        $this->cell(30.44, 8, $this->sma->utf8Decode('Código'), 'TLR', 0, 'C');
        $this->cell(81.44, 8, $this->sma->utf8Decode('Producto'), 'TLR', 0, 'C');
        $this->cell(16.44, 8, $this->sma->utf8Decode('UM Base'), 'TLR', 0, 'C');
        $this->cell(16.44, 8, $this->sma->utf8Decode('Ordenada'), 'TLR', 0, 'C');
        $this->cell(16.44, 8, $this->sma->utf8Decode('Facturada'), 'TLR', 0, 'C');
        $this->cell(16.44, 8, $this->sma->utf8Decode('Pendiente'), 'TLR', 0, 'C');
        if (!$this->warehouse_setted) {
            $this->cell(26.44, 8, $this->sma->utf8Decode('Existencia'), 'TLR', 0, 'C');
        } else {
            $current_X = $this->getX();
            $current_Y = $this->getY();
            $this->cell(26.44, 4, $this->sma->utf8Decode('Existencia'), 'TLR', 0, 'C');
            $this->setXY($current_X, $current_Y+4);
            $this->cell(13.22, 4, $this->sma->utf8Decode('Bodega'), 'TLR', 0, 'C');
            $this->cell(13.22, 4, $this->sma->utf8Decode('Otras'), 'TLR', 0, 'C');
            $this->setXY($current_X+26.44, $current_Y);
        }
        $this->cell(30.44, 8, $this->sma->utf8Decode('Por comprar'), 'TLR', 1, 'C');
    }
    function Footer()
    {
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode('Impreso por Wappsi © '.date('Y').' Web Apps Innovation SAS | www.wappsi.com'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode('Pagina '.$this->PageNo()." de ".'{nb}'),'',1,'C');
    }
    function get_quantity_to_purchase(){
        $quantity_to_purchase  = ($this->data_row->product_quantity - $this->data_row->quantity_request) * -1;
        $quantity_to_purchase = $quantity_to_purchase > 0 ? $quantity_to_purchase : 0;
        $quantity_to_purchase_process = $quantity_to_purchase > 0 ? $quantity_to_purchase : 0;
        $text_qty = "";
        if (isset($this->data_row->all_units)) {
            foreach ($this->data_row->all_units as $dr_unit) {
                if ($quantity_to_purchase_process > 0 && ($dr_unit->operation_value > 0 || $dr_unit->num == 1)) {
                    if ($dr_unit->num == 1) {
                        $dr_unit->operation_value = 1;
                    }
                    $dr_unit_qty = $quantity_to_purchase_process / $dr_unit->operation_value;
                    if ($dr_unit->operation_value != 1) {
                        $dr_unit_qty = floor($dr_unit_qty);
                    }
                    $quantity_to_purchase_process -= ($dr_unit_qty * $dr_unit->operation_value);
                    if ($dr_unit_qty > 0) {
                        $text_qty .= $this->sma->formatQuantity($dr_unit_qty)." ".$dr_unit->name." | ";
                    }
                }
            }
        }
            
        return trim($text_qty, "| ");
    }
}
$pdf = new PDF('L', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(7, 7);
$pdf->fontsize_body = 7;
$pdf->fontsize_title = 8.5;
$pdf->SetFont('Arial','', $pdf->fontsize_body);
$pdf->Logo = base_url('assets/uploads/logos/'.$biller->logo);
$row = 2;
$current_product_name = null;
$current_category_name = null;
$current_preference = null;
$category_id = NULL;
$subcategory_id = NULL;
$pdf->sma = $this->sma;
$pdf->wh_data = $wh_data;
$pdf->warehouse_setted = $this->input->post('warehouse') ? true : false;
$pdf->AddPage();
$current_reference  = NULL;
$current_zone       = NULL;
$currentReferenceByZone = null;



foreach ($data as $data_row) {
    $data_row->preferences = $data_row->preferences ? $this->sma->print_preference_selection($data_row->preferences, true) : "";
    if ($pdf->getY() > 180) {
        $pdf->AddPage();
    }

    if ($group_by_order && ($current_reference == NULL || $current_reference != $data_row->reference_no)) {
        $current_reference = $data_row->reference_no;
        $pdf->SetFont('Arial','B', $pdf->fontsize_body+3);
        $pdf->SetFillColor(190, 190, 190);
        $pdf->cell(18, 6, $this->sma->utf8Decode($data_row->reference_no), 'LT', 0, 'L', 1);
        $pdf->SetFont('Arial','B', $pdf->fontsize_body+1);
        $name = (trim($data_row->companyName) != trim($data_row->company)) ? "$data_row->companyName ($data_row->company)" : "$data_row->companyName";
        $pdf->cell(247, 6, $this->sma->utf8Decode("$data_row->date - $name - $data_row->direccion - $data_row->zone - $data_row->subzone"), 'TR', 1, 'L', 1);

    } else if ($group_by_zone && ($current_zone == NULL || $current_zone != $data_row->zone)) { 
        $current_zone = $data_row->zone;
        $pdf->SetFont('Arial','B', $pdf->fontsize_body+3);
        $pdf->SetFillColor(190, 190, 190);
        $pdf->cell(30.44, 6, $this->sma->utf8Decode($data_row->zone), 'LT', 0, 'L', 1);
        $pdf->SetFont('Arial','B', $pdf->fontsize_body+1);
        $name = (trim($data_row->companyName) != trim($data_row->company)) ? "$data_row->companyName ($data_row->company)" : "$data_row->companyName";
        $pdf->cell(234.56, 6, $this->sma->utf8Decode("$data_row->date - $name - $data_row->direccion - $data_row->zone - $data_row->subzone"), 'TR', 1, 'L', 1);

    }

    if (!($group_by_order && $group_by_zone)) {
        if ($currentReferenceByZone != $data_row->reference_no) {
            $currentReferenceByZone = $data_row->reference_no;

            $pdf->SetFillColor(230, 230, 230);
            $pdf->SetFont('Arial','B', $pdf->fontsize_body+3);
            $pdf->cell(265, 6, $this->sma->utf8Decode($data_row->reference_no), 'LTR', 1, 'L', 1);
        }
    }

    $pdf->SetFont('Arial','', $pdf->fontsize_body);
    $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();
        $columns[0]['inicio_x'] = $pdf->getX();
    $pdf->cell(30.44, 6, $this->sma->utf8Decode($data_row->category_name), '', 0, 'L');
        $columns[0]['fin_x'] = $pdf->getX();
        $columns[1]['inicio_x'] = $pdf->getX();
    $pdf->cell(30.44, 6, $this->sma->utf8Decode($data_row->product_code), '', 0, 'L');
        $columns[1]['fin_x'] = $pdf->getX();
        $columns[2]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.7);
    $pdf->MultiCell(81.44, 4, $this->sma->utf8Decode($data_row->product_name), '', 'L');
        $columns[2]['fin_x'] = $cX+81.44;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+81.44, $cY);
        $columns[3]['inicio_x'] = $pdf->getX();
    $pdf->cell(16.44, 6, $this->sma->utf8Decode($data_row->main_unit_code), '', 0, 'L');
        $columns[3]['fin_x'] = $pdf->getX();
        $columns[4]['inicio_x'] = $pdf->getX();
    $pdf->cell(16.44, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->quantity_request)), '', 0, 'R');
        $columns[4]['fin_x'] = $pdf->getX();
        $columns[5]['inicio_x'] = $pdf->getX();
    $pdf->cell(16.44, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->quantity_delivered)), '', 0, 'R');
        $columns[5]['fin_x'] = $pdf->getX();
        $columns[6]['inicio_x'] = $pdf->getX();
    $pdf->cell(16.44, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->quantity_request - $data_row->quantity_delivered)), '', 0, 'R');
        $columns[6]['fin_x'] = $pdf->getX();
        $columns[7]['inicio_x'] = $pdf->getX();
    
    if (!$pdf->warehouse_setted) {
        $pdf->cell(26.44, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->product_quantity)), '', 0, 'R');
    } else {
        $pdf->cell(13.22, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->product_quantity)), 'R', 0, 'R');
        $pdf->cell(13.22, 6, $this->sma->utf8Decode($this->sma->formatQuantity($data_row->ow_quantity)), '', 0, 'R');
    }
        
        
        $columns[7]['fin_x'] = $pdf->getX();
    $pdf->data_row = $data_row;
        $columns[8]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.7);
    $pdf->MultiCell(30.44, 4, $this->sma->utf8Decode($pdf->get_quantity_to_purchase()), '','L');
        $columns[8]['fin_x'] = $cX+30.44;
            $fin_altura_fila = $pdf->getY() > $fin_altura_fila ? $pdf->getY() : $fin_altura_fila;
            $pdf->setXY($cX+30.44, $cY);

        $fin_fila_X = $pdf->getX();
        $fin_fila_Y = $pdf->getY();
        $ancho_fila = $fin_fila_X - $inicio_fila_X;
        $altura_fila = $fin_altura_fila - $inicio_altura_fila;
        $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
        foreach ($columns as $key => $data) {
            $ancho_column = $data['fin_x'] - $data['inicio_x'];
            if ($altura_fila < 5) {
                $altura_fila = 5;
            }
            $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'TLBR',0,'R');
        }
        $pdf->ln($altura_fila);
        $current_product_name = $data_row->product_name;
        $current_preference = NULL;
    $row++;
}

$descargar = false;
if ($descargar) {
  $pdf->Output("INFORME_ORDEN_PEDIDOS.pdf", "D");
} else {
  $pdf->Output("INFORME_ORDEN_PEDIDOS.pdf", "I");
}