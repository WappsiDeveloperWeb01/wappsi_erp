<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Método para generar el response para Datatables
 */
#[\AllowDynamicProperties]
class Datatablesphp
{

	public $result = false;
	public $rows;
	public $action;
	public $set_columns;


	function __construct()
	{
        $this->ci =& get_instance();
	}

	public function get_json_response(){
		$data = [];
        $cnt = 0;

	    if ($this->result) {
	    	foreach (($this->result->result()) as $row) {
	            $data[$cnt] = [];
	            foreach ($row as $columna => $valor) {
	                array_push($data[$cnt], $valor);
	            }

	            $row_action = $this->action;
	            foreach ($this->set_columns as $column => $value) {
	            	$row_action = str_replace($column, $row->{$value}, $row_action);
	            }

	            array_push($data[$cnt], $row_action);
	            $cnt++;
	        }
	        $data_r = array(
	                    'sEcho' => 1,
	                    'iTotalRecords' => count($data),
	                    'iTotalDisplayRecords' => count($data),
	                    'aaData' => $data,
	                    );
	    } else {

	        $data_r = array(
	                    'sEcho' => 1,
	                    'iTotalRecords' => 0,
	                    'iTotalDisplayRecords' => 0,
	                    'aaData' => array(),
	                    );
	    }

        return json_encode($data_r);
	}

}