<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_delivery'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open_multipart("sales/edit_delivery/".$delivery->id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="row">
                <?php if ($Owner || $Admin) { ?>
                    <div class="form-group col-sm-6">
                        <?= lang("date", "date"); ?>
                        <?= form_input('date', $delivery->date, 'class="form-control datetime" id="date" required="required"'); ?>
                    </div>
                <?php } ?>
                
                <?php
                    $bl[""] = "";
                    $bldata = [];
                    foreach ($billers as $biller) {
                        if ($biller->id == $inv->biller_id) {
                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                            $bldata[$biller->id] = $biller;
                        }
                    }
                ?>
                <div class="form-group col-sm-6">
                    <?= lang("biller", "delivery_biller"); ?>
                    <select name="biller" class="form-control" id="delivery_biller" required="required" readonly>
                        <?php foreach ($billers as $biller): ?>
                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>" data-autoica="<?= $biller->rete_autoica_percentage ?>"><?= $biller->company ?></option>
                        <?php endforeach ?>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <?= lang("do_reference_no", "document_type_id"); ?>
                    <input type="text" name="do_reference_no" class="form-control" value="<?= $delivery->do_reference_no ?>" readonly>
                </div>
                <div class="form-group col-sm-6">
                    <?= lang("sale_reference_no", "sale_reference_no"); ?>
                    <?= form_input('sale_reference_no', (isset($_POST['sale_reference_no']) ? $_POST['sale_reference_no'] : $inv->reference_no), 'class="form-control tip" id="sale_reference_no" required="required" readonly'); ?>
                </div>
                <input type="hidden" value="<?php echo $inv->id; ?>" name="sale_id"/>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <?= lang("customer", "customer"); ?>
                    <?php echo form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : $customer->name), 'class="form-control" id="customer" required="required" readonly'); ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= lang("delivered_by", "delivered_by"); ?>
                    <?= form_input('delivered_by', $delivery->delivered_by, 'class="form-control" id="delivered_by" required'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <?= lang("received_by", "received_by"); ?>
                    <?= form_input('received_by', $delivery->received_by, 'class="form-control" id="received_by"'); ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= lang("vehicle", "vehicle"); ?>
                    <?= form_input('vehicle', $delivery->vehicle, 'class="form-control" id="vehicle" required'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <?= lang("delivery_time", "delivery_time"); ?>
                    <?= form_input('delivery_time', ($inv->time_1 ? (date('h:ia', strtotime($inv->time_1))."-".date('h:ia', strtotime($inv->time_2))) : ""), 'class="form-control" id="delivery_time" readonly'); ?>
                    <input type="hidden" name="delivery_time_id" value="<?= $inv->delivery_time_id ?>">
                </div>
                <div class="form-group col-sm-6">
                    <?= lang("containers_quantity", "containers_quantity"); ?>
                    <input type="number" name="containers_quantity" type="number" class="form-control only_number" id="containers_quantity"  value="<?= $delivery->containers_quantity ?>" required>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <?= lang("zone", "zone"); ?>
                    <?= form_input('zone', $inv->zone_negocio, 'class="form-control" id="zone" readonly'); ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= lang("subzone", "subzone"); ?>
                    <?= form_input('subzone', $inv->subzone_negocio, 'class="form-control" id="subzone" readonly'); ?>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-sm-6">
                    <?= lang("address", "address"); ?>
                    <?php
                    $av = (isset($delivery->address) && !empty($delivery->address) ? $delivery->address
                        : (empty($address)
                            ? ($customer->address . " " . $customer->city . " " . $customer->state . " " . $customer->postal_code . " " . $customer->country . "<br>".lang('tel').": " . $customer->phone . " Email: " . $customer->email)
                            : ($address->line1.'<br>'.$address->line2.'<br>'.$address->city.' '.$address->city.'<br>'.$address->postal_code.' '.$address->country.'<br>'.lang('tel').': '.$address->phone)
                            )
                        );
                    echo form_textarea('address', $av, 'class="form-control" id="address" required="required"'); ?>
                </div>
                <div class="form-group col-sm-6">
                    <?= lang("note", "note"); ?>
                    <?php echo form_textarea('note', $delivery->note, 'class="form-control" id="note"'); ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_delivery', lang('edit_delivery'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript" charset="UTF-8">
    $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
</script>
<?= $modal_js ?>
<script type="text/javascript" charset="UTF-8">
    $(document).ready(function () {
        $.fn.datetimepicker.dates['sma'] = <?=$dp_lang?>;
        $("#date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date("<?= date('Y-m-d H:i', strtotime($delivery->date)) ?>"));

        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/46/").$inv->biller_id ?>',
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            }
            $('#document_type_id').trigger('change');
        });

        $('#delivery_biller').select2('readonly', true);
    });
</script>
