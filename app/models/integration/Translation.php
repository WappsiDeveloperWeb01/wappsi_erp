<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Translation extends CI_Model
{
    public $tableName;
    public $conn;

    public function __construct()
    {
        parent::__construct();
    }

    public function find($data)
    {
        $this->conn->where($data);
        $q = $this->conn->get($this->tableName);
        return $q->row();
    }

    public function create($data)
    {
        if ($this->conn->insert($this->tableName, $data)) {
            return true;
        }
        return false;
    }

    public function update($data, $id)
    {
        $this->conn->where('id', $id);
        if ($this->conn->update($this->tableName, $data)) {
            return true;
        }
        return false;
    }

    public function delete($id)
    {
        $this->conn->where("id", $id);
        if ($this->conn->delete($this->tableName)) {
            return true;
        }
        return false;
    }
}