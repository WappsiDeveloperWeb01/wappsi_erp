<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <!-- <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/view_adjustment_pos/'.$inv->id) ?>" target="_blank">
                <i class="fa fa-print"></i> POS
            </a> -->
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>
            <div class="text-center" style="margin-bottom:20px;">
                <h3><?= lang('wms_picking') ?> N° <?= $inv->reference_no; ?></h3>
            </div>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-5">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("packing_warehouse"); ?>: <?= $warehouses[$inv->packing_warehouse_id]->name; ?><br>
                    </p>
                    </div>
                    <div class="col-xs-7 text-right order_barcodes">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <label class="table-label"><?= lang("products"); ?> *</label>
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th><?= lang("warehouse"); ?> </th>
                                    <th><?= lang("description"); ?> </th>
                                    <th><?= lang("variant"); ?> </th>
                                    <th><?= lang("quantity"); ?> </th>
                                </tr>
                            </thead>

                            <tbody>
                                <?php
                                $qty = 0;
                                foreach ($rows as $row):
                                ?>
                                    <tr>
                                        <td style="vertical-align:middle;">
                                            <?= $warehouses[$row->warehouse_id]->code; ?>
                                        </td>
                                        <td style="vertical-align:middle;">
                                            <?= $row->product_code.' - '.$row->product_name; ?>
                                        </td>
                                        <td style="vertical-align:middle;">
                                            <?= $row->variant; ?>
                                        </td>
                                        <td class="text-right">
                                            <?= $this->sma->formatQuantity($row->quantity); ?>
                                        </td>
                                    </tr>
                                    <?php
                                    $qty+=$row->quantity;
                                endforeach;
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="3" class="text-right"><?= lang('total') ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($qty); ?></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
                <?php if ($notices): ?>
                    <div class="col-xs-12">
                        <div class="table-responsive">
                        <label class="table-label"><?= lang("notices"); ?> *</label>
                        <table class="table table-bordered table-hover print-table order-table">
                            <thead>
                                <tr>
                                    <th><?= lang("notice"); ?> </th>
                                    <th><?= lang("warehouse"); ?> </th>
                                    <th><?= lang("product_code"); ?> </th>
                                    <th><?= lang("variant"); ?> </th>
                                    <th><?= lang("quantity"); ?> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($notices as $notice): ?>
                                    <tr>
                                        <td><?= $notice->notice ?></td>
                                        <td><?= $notice->warehouse_name ?></td>
                                        <td><?= $notice->product_code.($notice->product_variant_suffix ? $notice->product_variant_suffix : "") ?></td>
                                        <td><?= $notice->product_variant ?></td>
                                        <td class="text-right"><?= $this->sma->formatQuantity($notice->quantity) ?></td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                            </table>
                        </div>
                    </div>
                <?php endif ?>
            </div>
        </div>

        <div class="modal-footer">
            <div class="row">
                <div class="col-xs-12 text-center">
                    <p>
                        <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
