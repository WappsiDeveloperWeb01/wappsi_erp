$(document).ready(function () {
    if (!localStorage.getItem('porref')) {
        localStorage.setItem('porref', '');
    }
    ItemnTotals();
    $('.bootbox').on('hidden.bs.modal', function (e) {
        $('#add_item').focus();
    });
    $('body a, body button').attr('tabindex', -1);
    check_add_item_val();
    if (site.settings.set_focus != 1) {
        $('#add_item').focus();
    }
    // If there is any item in localStorage
    if (localStorage.getItem('poritems')) {
        loadItems();
    }
    // clear localStorage and reload
    $('#reset').click(function (e) {
        bootbox.confirm(lang.r_u_sure, function (result) {
            if (result) {
                if (localStorage.getItem('poritems')) {
                    localStorage.removeItem('poritems');
                }
                if (localStorage.getItem('porref')) {
                    localStorage.removeItem('porref');
                }
                if (localStorage.getItem('porwarehouse')) {
                    localStorage.removeItem('porwarehouse');
                }
                if (localStorage.getItem('pornote')) {
                    localStorage.removeItem('pornote');
                }
                if (localStorage.getItem('pordate')) {
                    localStorage.removeItem('pordate');
                }
                if (localStorage.getItem('document_type_id')) {
                    localStorage.removeItem('document_type_id');
                }
                if (localStorage.getItem('companies_id')) {
                    localStorage.removeItem('companies_id');
                }
                if (localStorage.getItem('porbiller')) {
                    localStorage.removeItem('porbiller');
                }

                $('#modal-loading').show();
                location.reload();
            }
        });
    });

    // save and load the fields in and/or from localStorage
    $('#document_type_id').change(function (e) {
        localStorage.setItem('document_type_id', $(this).val());
    });
    if (document_type_id = localStorage.getItem('document_type_id')) {
        $('#document_type_id').select2('val', document_type_id);
    }
    $('#porwarehouse').change(function (e) {
        localStorage.setItem('porwarehouse', $(this).val());
    });
    if (porwarehouse = localStorage.getItem('porwarehouse')) {
        $('#porwarehouse').select2("val", porwarehouse);
    }
    $('#companies_id').change(function (e) {
        localStorage.setItem('companies_id', $(this).val());
    });
    if (companies_id = localStorage.getItem('companies_id')) {
        $('#companies_id').select2("val", companies_id);
    }
    $('#porbiller').change(function (e) {
        localStorage.setItem('porbiller', $(this).val());
    });
    if (porbiller = localStorage.getItem('porbiller')) {
        $('#porbiller').select2("val", porbiller);
    }
    $('#porstatus').change(function (e) {
        localStorage.setItem('porstatus', $(this).val());
    });
    if (porstatus = localStorage.getItem('porstatus')) {
        $('#porstatus').select2("val", porstatus);
        setTimeout(function() {$('#porstatus').trigger('change')}, 850);
    }

    //$(document).on('change', '#pornote', function (e) {
        $('#pornote').redactor('destroy');
        $('#pornote').redactor({
            buttons: ['formatting', '|', 'alignleft', 'aligncenter', 'alignright', 'justify', '|', 'bold', 'italic', 'underline', '|', 'unorderedlist', 'orderedlist', '|', 'link', '|', 'html'],
            formattingTags: ['p', 'pre', 'h3', 'h4'],
            minHeight: 100,
            changeCallback: function (e) {
                var v = this.get();
                localStorage.setItem('pornote', v);
            }
        });
        if (pornote = localStorage.getItem('pornote')) {
            $('#pornote').redactor('set', pornote);
        }

    // prevent default action upon enter
    $('body').bind('keypress', function (e) {
        if ($(e.target).hasClass('redactor_editor')) {
            return true;
        }
        if (e.keyCode == 13) {
            e.preventDefault();
            return false;
        }
    });


    /* ----------------------
     * Delete Row Method
     * ---------------------- */

    $(document).on('click', '.pordel', function () {
        poritems = JSON.parse(localStorage.getItem('poritems'));
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        delete poritems[item_id];
        row.remove();
        if(poritems.hasOwnProperty(item_id)) { } else {
            localStorage.setItem('poritems', JSON.stringify(poritems));
            loadItems();
            return;
        }
    });

    /* --------------------------
     * Edit Row Quantity Method
     -------------------------- */

    $(document).on("change", '.rquantity', function () {
        poritems = JSON.parse(localStorage.getItem('poritems'));
        var row = $(this).closest('tr');
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(old_row_qty);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        var new_qty = parseFloat($(this).val()),
        item_id = row.attr('data-item-id');
        poritems[item_id].row.qty = new_qty;
        localStorage.setItem('poritems', JSON.stringify(poritems));
        loadItems();
    });

    $(document).on("change", '.rcost', function () {
        poritems = JSON.parse(localStorage.getItem('poritems'));
        var new_cost = parseFloat($(this).val());
        var row = $(this).closest('tr');
        var item_id = row.attr('data-item-id');
        var prev_cost = poritems[item_id].row.cost;
        if (!is_numeric($(this).val()) || parseFloat($(this).val()) < 0) {
            $(this).val(prev_cost);
            bootbox.alert(lang.unexpected_value);
            return;
        }
        poritems[item_id].row.cost = new_cost;
        localStorage.setItem('poritems', JSON.stringify(poritems));
        loadItems();
    });

    $(document).on("change", '.rtype', function () {
        poritems = JSON.parse(localStorage.getItem('poritems'));
        var row = $(this).closest('tr');
        var new_type = $(this).val(),
        item_id = row.attr('data-item-id');
        poritems[item_id].row.type = new_type;
        localStorage.setItem('poritems', JSON.stringify(poritems));
        loadItems();
    });

    $(document).on("change", '.rvariant', function () {
        poritems = JSON.parse(localStorage.getItem('poritems'));
        var row = $(this).closest('tr');
        var new_opt = $(this).val(),
        item_id = row.attr('data-item-id');
        poritems[item_id].row.option = new_opt;
        localStorage.setItem('poritems', JSON.stringify(poritems));
    });


});


/* -----------------------
 * Load Items to table
 ----------------------- */
//aca load

function loadItems() {
    var decimals = null;
    // if (site.settings.rounding == 1) {
    //     decimals = 1;
    // }

    if (localStorage.getItem('poritems')) {
        count = 1;
        an = 1;
        Ttotal = 0;
        $("#porTable tbody").empty();
        poritems = JSON.parse(localStorage.getItem('poritems'));
        sortedItems = (site.settings.item_addition == 1) ? _.sortBy(poritems, function(o){return [parseInt(o.order)];}) : poritems;
        if (comproducts !== undefined) { delete comproducts; }
        var comproducts = [];
        $.each(sortedItems, function () {
            var item = this;
            var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
            item.order = item.order ? item.order : new Date().getTime();
            var product_id = item.row.id, item_qty = item.row.qty, item_price = item.row.price, item_total = item.row.qty * item.row.cost, item_option = item.row.option, item_code = item.row.code, item_serial = item.row.serial, item_name = item.row.name.replace(/"/g, "&#034;").replace(/'/g, "&#039;");
            var type = item.row.type ? item.row.type : '';
            var item_cost = item.row.cost;
            console.log('last cost '+item.row.last_cost);

            var opt = $("<select id=\"poption\" name=\"variant\[\]\" class=\"form-control select rvariant\" />");
            if(item.options !== false) {
                $.each(item.options, function () {
                    if (item.row.option == this.id)
                        $("<option />", {value: this.id, text: this.name, selected: 'selected'}).appendTo(opt);
                    else
                        $("<option />", {value: this.id, text: this.name}).appendTo(opt);
                });
            } else {
                $("<option />", {value: 0, text: 'n/a'}).appendTo(opt);
                opt = opt.hide();
            }
            Ttotal += item_total;

            if (item_total == 0) {
                header_alert('warning', 'Existen productos de destino con costo en 0');
            }
            var row_no = item.id;
            var newTr = $('<tr id="row_' + row_no + '" class="row_' + item_id + '" data-item-id="' + item_id + '"></tr>');
            tr_html = '<td>'+
                        (is_edit ? '<input name="item_id[]" type="hidden" value="' + item.row.adjustment_item_id + '">' : '' ) +
                            '<input name="product_id[]" type="hidden" class="rid" value="' + product_id + '"><input name="product_name[]" type="hidden" value="' + item_name + '"><span class="sname" id="name_' + row_no + '">' + item_code +' - ' + item_name +'</span>'+
                        '</td>';
            tr_html += '<td>'+item.unit.code+'</td>';
            tr_html += '<td>'+lang[item.row.type]+'</td>';
            tr_html += '<td>'+
                            '<input class="form-control text-center rquantity" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="quantity[]" type="text" value="' + formatDecimal(item_qty, decimals) + '" data-id="' + row_no + '" data-item="' + item_id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                        '</td>';
            tr_html += '<td class="text-right">'+formatMoney(item_total)+
                            '<input class="form-control text-center" tabindex="'+((site.settings.set_focus == 1) ? an : (an+1))+'" name="total_cost[]" type="hidden" value="' + item_total + '" readonly>'+
                            '<input name="cost[]" type="hidden" value="'+item_total+'">'+
                            '<input name="unit_cost[]" type="hidden" value="'+item.row.cost+'">'+
                        '</td>'; //unitario
            tr_html += '<td class="text-center"><i class="fa fa-times tip pordel" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i></td>';
            newTr.html(tr_html);
            newTr.prependTo("#porTable");
            count += parseFloat(item_qty);
            an++;
            cproducts = item.composition_products;
            $.each(cproducts, function(index, cproduct){
                cproduct.qty = cproduct.qty * item_qty;
                if (comproducts[cproduct.id] !== undefined) {
                    comproducts[cproduct.id].qty = parseFloat(comproducts[cproduct.id].qty) + parseFloat(cproduct.qty);
                } else {
                    comproducts[cproduct.id] = cproduct;
                }
            });

        });
        var col = 4;
        var tfoot = '<tr id="tfoot" class="tfoot active">'+(site.settings.set_adjustment_cost ? '<th></th>' : '')+
                        '<th colspan="2">Total</th>'+
                        '<th class="text-center">' + formatQuantity2(parseFloat(count) - 1) + '</th>'+
                        '<th class="text-center">' + formatMoney(parseFloat(Ttotal)) + '</th>'+
                        '<th class="text-center"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>'+
                    '</tr>';
        $('#porTable tfoot').html(tfoot);
        $('select.select').select2({minimumResultsForSearch: 7});
        if (an > parseInt(site.settings.bc_fix) && parseInt(site.settings.bc_fix) > 0) {
            $("html, body").animate({scrollTop: $('#sticker').offset().top}, 500);
            $(window).scrollTop($(window).scrollTop() + 1);
        }
        set_page_focus();
        loadCompositionItems(comproducts);
    }
}
var lock_submit = false;
function loadCompositionItems(comproducts){

    var decimals = null;
    // if (site.settings.rounding == 1) {
    //     decimals = 1;
    // }
    $("#porCTable tbody").empty();
    console.log(comproducts);
    count = 1;
    Ttotal = 0;
    $.each(comproducts, function (index, item) {

        if (item === undefined) { return; }

        var trclass = 'class="cproduct_'+item.id+'"';
        var newTr = $('<tr></tr>');
        tr_html = '<td '+trclass+'>'+
                        (is_edit ? '<input name="cp_item_id[]" type="hidden" value="' + item.adjustment_item_id + '">' : '' ) +
                        '<input name="cp_product_id[]" type="hidden" class="rid" value="' + item.id + '">'+
                        '<input name="cp_product_name[]" type="hidden" value="' + item.name + '">'+
                        '<input name="cp_parent_id[]" type="hidden" class="rid" value="' + item.parent_id + '">'+
                        '<span class="sname">' + item.name +'</span>'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+item.unit_code+'</td>';
        tr_html += '<td '+trclass+'>'+lang[item.type]+'</td>';
        tr_html += '<td '+trclass+'>'+
                        '<input class="form-control text-center rquantity" name="cp_quantity[]" type="text" value="' + formatDecimal(item.qty, decimals) + '" readonly>'+
                    '</td>';
        tr_html += '<td '+trclass+'>'+formatMoney((parseFloat(item.price) * parseFloat(item.qty)))+
                        '<input class="form-control text-center" name="cp_cost[]" type="hidden" value="' + (parseFloat(item.price) * parseFloat(item.qty)) + '" readonly>'+
                        '<input name="cp_unit_cost[]" type="hidden" value="'+item.price+'">'+
                    '</td>'; //unitario
        newTr.html(tr_html);
        newTr.prependTo("#porCTable");
        Ttotal += parseFloat(item.price) * parseFloat(item.qty);
        console.log('ttotal '+Ttotal);
        count += parseFloat(item.qty);

        if (item.price == 0) {
            header_alert('warning', 'Existen productos componentes con costo en 0')
        }

    });

    var tfoot = '<tr id="tfoot" class="tfoot active">'+
                    '<th colspan="3">Total</th>'+
                    '<th class="text-center">' + formatQuantity2(parseFloat(count) - 1) + '</th>'+
                    '<th class="text-center">' + formatMoney(parseFloat(Ttotal)) + '</th>'+
                '</tr>';
    $('#porCTable tfoot').html(tfoot);
    lock_submit = false;
    $.each(comproducts, function (index, item) {
        if (item === undefined) { return; }
        $.ajax({
            url : site.base_url+"products/getProductDataByWarehouse/"+item.id +"/"+$('#porwarehouse').val(),
            dataType : "JSON"
        }).done(function(data){
            if (parseFloat(data.quantity) < parseFloat(item.qty)) {
                $('.cproduct_'+item.id).addClass('danger');
                if (site.settings.overselling == 0) {
                    lock_submit = true;
                }
            }

            if (lock_submit == true && site.settings.overselling == 0) {
                $('#add_adjustment').prop('disabled', true);
                header_alert('error', 'El componente ('+item.name+') no tiene suficientes cantidades para la producción', 4);
            }
        });
    });

}

/* -----------------------------
 * Add Purchase Item Function
 * @param {json} item
 * @returns {Boolean}
 ---------------------------- */
function add_adjustment_item(item) {

    poritems = JSON.parse(localStorage.getItem('poritems'));

    if (count == 1) {
        poritems = {};
    }
    if (item == null)
        return;

    var item_id = site.settings.item_addition == 1 ? item.item_id : item.id;
    if (poritems[item_id]) {

        var new_qty = parseFloat(poritems[item_id].row.qty) + 1;
        poritems[item_id].row.base_quantity = new_qty;
        if(poritems[item_id].row.unit != poritems[item_id].row.base_unit) {
            $.each(poritems[item_id].units, function(){
                if (this.id == poritems[item_id].row.unit) {
                    poritems[item_id].row.base_quantity = unitToBaseQty(new_qty, this);
                }
            });
        }
        poritems[item_id].row.qty = new_qty;

    } else {
        poritems[item_id] = item;
    }
    poritems[item_id].order = new Date().getTime();
    localStorage.setItem('poritems', JSON.stringify(poritems));
    loadItems();
    return true;
}

if (typeof (Storage) === "undefined") {
    $(window).bind('beforeunload', function (e) {
        if (count > 1) {
            var message = "You will loss data!";
            return message;
        }
    });
}

$(document).on('change', '#porstatus', function() {
    if ($(this).val() == 'pending') {
        $('#add_adjustment').prop('disabled', true).fadeOut();
        $('#submit_pending').prop('disabled', false).fadeIn();
    } else {
        if (!lock_submit) {
            $('#add_adjustment').prop('disabled', false).fadeIn();
        } else {
            header_alert('error','Debe revisar las cantidades en existencia de los componentes');
        }
        $('#submit_pending').prop('disabled', true).fadeOut();
    }
});

$(document).on('click', '#submit_pending, #add_adjustment', function(){
    if ($('#por_form').valid()) {
        $('#por_form').submit();
    }
});