<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
    $v = "";
    if ($this->input->post('user')) {
        $v .= "&user=" . $this->input->post('user');
    }
    if ($this->input->post('biller_id')) {
        $v .= "&biller_id=" . $this->input->post('biller_id');
    }
    if ($this->input->post('start_date')) {
        $v .= "&start_date=" . $this->input->post('start_date');
    }
    if ($this->input->post('end_date')) {
        $v .= "&end_date=" . $this->input->post('end_date');
    }
?>
<style type="text/css">
    .topborder div {
        border-top: 1px solid #CCC;
    }
    .table td:nth-child(6) {
        text-align: center;
    }
</style>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/serial_register", 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <label for="biller_id"><?= $this->lang->line('serial_no') ?></label>
                                        <?php $serial = (isset($_POST["serial"])) ? $_POST["serial"] : ''; ?>
                                        <?php $serial = (empty($serial)) ? $this->session->userdata('user_pc_serial') : $serial; ?>
                                        <select class="form-control" id="serial" name="serial" required>
                                            <option value=""><?= $this->lang->line('select') ?></option>
                                            <?php foreach ($serials as $serial) : ?>
                                                <option value="<?= $serial->serial ?>" <?= ($serial->serial == $serial) ? 'selected' : '' ?>><?= $serial->serial ?></option>
                                            <?php endforeach ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>
                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                        <div class="col-sm-4 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("start_date", "start_date_dh"); ?>
                                            <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("end_date", "end_date_dh"); ?>
                                            <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh"'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary" id="submit_filter" type="button"><?= lang('submit') ?></button>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                        <li class="dropdown"><a href="#" id="xls" class="tip"><i class="icon fa fa-file-excel-o"></i><?= lang('download_xls') ?></a> </li>
                                        <li class="dropdown"><a href="#" id="image" class="tip"><i class="icon fa fa-file-picture-o"></i><?= lang('save_image') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <table id="registerTable" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover reports-table">
                                        <thead>
                                            <tr>
                                                <th><?= lang('open_time'); ?></th>
                                                <th><?= lang('close_time'); ?></th>
                                                <th><?= lang('user'); ?></th>
                                                <th><?= lang('cash_in_hand'); ?></th>
                                                <th><?= lang('cc_slips'); ?></th>
                                                <th><?= lang('Cheques'); ?></th>
                                                <th><?= lang('total_cash'); ?></th>
                                                <th><?= lang('note'); ?></th>
                                                <th></th>
                                                <th><?= lang('actions'); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                            <tr class="active">
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

<script>
    $(document).ready(function() {
        function total_cash(x) {
            if (x !== null) {
                var y = x.split(' (');
                var z = y[1].split(')');
                return currencyFormat(y[0]) + '<span class="text-success">' + currencyFormat(z[0]) + '</span><span class="text-danger topborder">' + currencyFormat(y[0] - z[0]) + '</span>';
            }
            return '';
        }

        function total_sub(x) {
            if (x !== null) {
                var y = x.split(' (');
                var z = y[0].split(')');
                return y[0] + '<br><span class="text-success">' + z[0] + '</span><span class="text-danger topborder"><div>' + (y[0] - z[0]) + '</div></span>';
            }
            return '';
        }
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
            oTable = $('#registerTable').dataTable({
                "aaSorting": [
                    [0, "desc"]
                ],
                "aLengthMenu": [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
                ],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true,
                'bServerSide': true,
                'sAjaxSource': '<?= admin_url('reports/get_serial_registers/?v=1' . $v) ?>',
                'fnServerData': function(sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
                },
                'fnRowCallback': function(nRow, aData, iDisplayIndex) {
                    var oSettings = oTable.fnSettings();
                    nRow.id = aData[8];
                    nRow.className = "register_link";
                    return nRow;
                },
                "aoColumns": [{
                    "mRender": fld
                }, {
                    "mRender": fld
                }, null, {
                    "mRender": currencyFormat
                }, {
                    "mRender": total_sub
                }, {
                    "mRender": total_sub
                }, {
                    "mRender": total_cash
                }, {
                    "bSortable": false
                }, {
                    "bVisible": false
                }, null]
            }).fnSetFilteringDelay().dtFilter([{
                    column_number: 0,
                    filter_default_label: "[ yyyy-mm-dd HH:mm:ss ]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 1,
                    filter_default_label: "[ yyyy-mm-dd HH:mm:ss ]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 2,
                    filter_default_label: "[<?= lang('user'); ?>]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 3,
                    filter_default_label: "[<?= lang('cash_in_hand'); ?>]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 4,
                    filter_default_label: "[<?= lang('cc_slips'); ?>]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 5,
                    filter_default_label: "[<?= lang('Cheques'); ?>]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 6,
                    filter_default_label: "[<?= lang('total_cash'); ?>]",
                    filter_type: "text",
                    data: []
                },
                {
                    column_number: 7,
                    filter_default_label: "[<?= lang('note'); ?>]",
                    filter_type: "text",
                    data: []
                },
            ], "footer");
        <?php endif ?>

        $('#form').hide();
        $('.toggle_down').click(function() {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function() {
            $("#form").slideUp();
            return false;
        });

    });
</script>
<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#pdf').click(function(event) {
            event.preventDefault();
            window.location.href = "<?= admin_url('reports/get_serial_registers/pdf/?v=1' . $v) ?>";
            return false;
        });
        $('#xls').click(function(event) {
            event.preventDefault();
            window.location.href = "<?= admin_url('reports/get_serial_registers/0/xls/?v=1' . $v) ?>";
            return false;
        });
        $('#image').click(function(event) {
            event.preventDefault();
            html2canvas($('.box'), {
                onrendered: function(canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $('#submit_filter').on('click', function(e) {
            if ($('#form_filter').valid()) {
                $('#filter_action').val(1);
                $('#form_filter').submit();
            }
            // e.preventDefault();
        });
    });

    $(document).ready(function() {
        $("#form_filter").validate({
            ignore: []
        });
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])) : ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                // $('#sales_filter').submit();
            <?php elseif ($_POST['date_records_filter'] != $this->Settings->default_records_filter) : ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);

        <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
            $('#user').prop('required', true);
        <?php endif ?>
    });
</script>