<?php

$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);

require_once $apppath.'vendor/fpdf/fpdf.php';
require_once $apppath.'vendor/number_convert/number_convert.php';

#[\AllowDynamicProperties]
class PDF extends FPDF
{   
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode($this->document_type ? $this->document_type->nombre : lang('quote') ));

        //izquierda
        $this->RoundedRect(13, 7, 117, 29, 3, '1234', '');
        if ($this->biller_logo == 2) {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,18,4,35);
        } else {
            $this->Image(base_url().'assets/uploads/logos/'.$this->logo,18,16,50);
        }
        $cx = 53;
        $cy = 13;
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setXY($cx, $cy);
        $this->Cell(72, 5 , $this->sma->utf8Decode(mb_strtoupper($this->biller->company != '-' ? $this->biller->company : $this->biller->name)),'',1,'L');
        $cy+=5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente+1.5);
        if($this->session->userdata['language'] != 'english'){
            $this->Cell(72, 3 , $this->sma->utf8Decode(lang('biller_document_type_name'). ' : '.$this->biller->vat_no.($this->biller->tipo_documento == 6 ? "-".$this->biller->digito_verificacion : '').", ".$this->tipo_regimen),'',1,'L');
        }
        if($this->session->userdata['language'] == 'english'){
            $this->Cell(72, 3 , $this->sma->utf8Decode(lang('biller_document_type_name'). ' : '.$this->biller->vat_no.($this->biller->tipo_documento == 6 ? "-".$this->biller->digito_verificacion : '')),'',1,'L');
        }
        if ($this->Settings->great_contributor || $this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer) {
            $cy+=3;
            $this->setXY($cx, $cy);
            $this->Cell(72, 3 , $this->sma->utf8Decode(
                                                ($this->Settings->great_contributor ? 'Gran contribuyente '.
                                                    ($this->Settings->fuente_retainer || $this->Settings->iva_retainer || $this->Settings->ica_retainer ? ' - Auto retenedor de ' : '')
                                                     : '').
                                                ($this->Settings->fuente_retainer ? 'Fuente' : '').
                                                ($this->Settings->iva_retainer ? ($this->Settings->fuente_retainer ? ', ' : '').'IVA ' : '').
                                                ($this->Settings->ica_retainer ? ($this->Settings->fuente_retainer || $this->Settings->iva_retainer ? ', ' : '').'ICA ' : '')
                                            ),'',1,'L');
        }
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode($this->biller->address." - ".ucfirst(mb_strtolower($this->biller->city))." - ".ucfirst(mb_strtolower($this->biller->state))),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(lang('phone')). ' : '.$this->biller->phone),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(lang('biller_email_name')). ' : '.$this->biller->email),'',1,'L');
        $cy+=3;
        $this->setXY($cx, $cy);
        $this->Cell(72, 3 , $this->sma->utf8Decode(ucfirst(lang('biller_web_name')). ' : ' .$this->Settings->url_web),'',1,'L');

        //derecha
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*2));
        $this->RoundedRect(132, 7, 77, 29, 3, '1234', '');

        $cx = 134;

        //derecha
        $cy = 13;
        $this->setXY($cx, $cy);
        $this->Cell(77, 3.5 , $this->sma->utf8Decode(ucfirst($this->document_type ? $this->document_type->nombre : lang('quote') )),'',1,'C');
        $cy +=4;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','B',$this->fuente+($this->adicional_fuente*4));
        $this->Cell(77, 10 , $this->sma->utf8Decode($this->factura->reference_no),'',1,'C');
        $cy +=10;
        $this->SetFont('Arial','',$this->fuente);
        $this->setXY($cx-2, $cy);
        $this->MultiCell(77, 3 , $this->sma->utf8Decode(''),'','C');

        //izquierda
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(13, 38, 118, 5, 3, '1', 'DF');
        $this->RoundedRect(13, 43, 118, 21, 3, '4', '');
        $cx = 13;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper(lang('customer_information'))),'B',1,'C');
        $cx += 3;
        $cy += 6;
        $this->setXY($cx, $cy);
        $this->Cell(115, 5 , $this->sma->utf8Decode(mb_strtoupper($this->customer->company != '-' ? $this->customer->company : $this->customer->name)),'',1,'L');
        $cy += 5;
        $this->setXY($cx, $cy);
        $this->SetFont('Arial','',$this->fuente);
        $this->Cell(115, 3 , $this->sma->utf8Decode(lang('customer_document_type')." : ".$this->customer->vat_no.($this->customer->tipo_documento == 6 ? "-".$this->customer->digito_verificacion : '')),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode(lang('address'). " : ".$this->customer->address.", ".(ucfirst(mb_strtolower($this->customer->city)))),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode(lang('customer_phone')." : ".$this->customer->phone),'',1,'L');
        $cy += 3;
        $this->setXY($cx, $cy);
        $this->Cell(115, 3 , $this->sma->utf8Decode(lang('customer_email')." : ".$this->customer->email),'',1,'L');

        //derecha


        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);

        $altura = 5;
        $adicional_altura = 2.3;

        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->RoundedRect(131, 38, 78, 5, 3, '2', 'DF');
        $this->RoundedRect(131, 43, 78, 21, 3, '3', '');
        $cx = 131;
        $cy = 38;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode(mb_strtoupper(lang('quotes').' '.lang('date'))),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode('TOTAL'),'B',1,'C');

        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->date),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->sma->formatMoney($this->factura->grand_total)),'B',1,'C');
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $cy +=5+$adicional_altura;
        $cx -= 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode(mb_strtoupper(lang('expiration_date'))),'LBR',1,'C',1);
        $cx += 39;
        $this->setXY($cx, $cy);
        $this->Cell(39, $altura , $this->sma->utf8Decode(mb_strtoupper(lang('term'))),'LBR',1,'C',1);
        $this->SetFont('Arial','',$this->fuente);
        $cx -= 39;
        $cy += $altura;
        $this->setXY($cx, $cy);
        // $fecha_exp = strtotime("+".$this->factura->payment_term." day", strtotime($this->factura->date));
        // $fecha_exp = date('Y-m-d H:i:s', $fecha_exp);
        // $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($fecha_exp),'BR',1,'C');
        $cx += 39;
        $this->setXY($cx, $cy);
        // $this->Cell(39, $altura+$adicional_altura , $this->sma->utf8Decode($this->factura->payment_term ." ".($this->factura->payment_term > 1 ? "Días" : "Día")),'B',1,'C');
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $this->SetFont('Arial','B',$this->fuente+$this->adicional_fuente);
        // $cy +=5+$adicional_altura;
        // $cx -= 39;
        // $this->setXY($cx, $cy);
        // $this->Cell(78, $altura , $this->sma->utf8Decode('VENDEDOR'),'TLBR',1,'C',1);
        // $this->SetFont('Arial','',$this->fuente);
        // $cy += $altura;
        // $this->setXY($cx, $cy);
        // $this->Cell(78, $altura , $this->sma->utf8Decode(ucwords(mb_strtolower($this->seller->company != '-' ? $this->seller->company : $this->seller->name))),'',1,'C');
        $this->ln(11);

        $this->SetFont('Arial','B',$this->fuente);
        $this->setFillColor($this->color_R,$this->color_G,$this->color_B);
        $current_X = $this->getX();
        $current_Y = $this->getY();
        $pr_name_wdth = 85.4;
        if (!$this->document_type_invoice_format || ($this->document_type_invoice_format && $this->document_type_invoice_format->product_show_code == 1)) {
            $this->Cell(23.2, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        } else{
            $pr_name_wdth += 23.2;
        }
        $this->Cell($pr_name_wdth, 8, $this->sma->utf8Decode(''),'TBLR',0,'C',1);
        $this->Cell(26.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(23.6, 8, $this->sma->utf8Decode(''),'TBL',0,'C',1);
        $this->Cell(26.6, 8, $this->sma->utf8Decode(''),'TBLR',1,'C',1);

        $this->setXY($current_X, $current_Y);

        if (!$this->document_type_invoice_format || ($this->document_type_invoice_format && $this->document_type_invoice_format->product_show_code == 1)) {
            $this->Cell(23.2, 8, $this->sma->utf8Decode(ucfirst(lang('code'))),'',0,'C',0);
        }
        $this->Cell($pr_name_wdth, 8, $this->sma->utf8Decode(ucfirst(lang('product_description'))),'',0,'C',0);
        $this->Cell(26.6, 4, $this->sma->utf8Decode(ucfirst(lang('vr_unit'))),'',0,'C',0);
        $this->Cell(10.6, 8, $this->sma->utf8Decode(ucfirst(lang('tax_label'))),'',0,'C',0);
        $this->Cell(23.6, 8, $this->sma->utf8Decode(ucfirst(lang('quantity')) .' / UM'),'',0,'C',0);
        $this->Cell(26.6, 8, $this->sma->utf8Decode('Subtotal'),'',1,'C',0);

        $this->setXY($current_X, $current_Y+4);

        if (!$this->document_type_invoice_format || ($this->document_type_invoice_format && $this->document_type_invoice_format->product_show_code == 1)) {
            $this->Cell(23.2, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        }
        $this->Cell($pr_name_wdth, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(26.6, 4, $this->sma->utf8Decode('(' .ucfirst(lang('tax_included')). ')'),'',0,'C',0);
        $this->Cell(10.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(23.6, 4, $this->sma->utf8Decode(''),'',0,'C',0);
        $this->Cell(26.6, 4, $this->sma->utf8Decode(''),'',1,'C',0);


    }

    function Footer()
    {
        // Print centered page number
        $this->RoundedRect(13, 248, 196, 17.5, 3, '1234', '');
        $this->SetXY(7, -35);
        $this->Cell(196, 5 , $this->sma->utf8Decode(''),'',1,'C');
        $this->MultiCell(196,3,$this->sma->utf8Decode($this->reduceTextToDescription2($this->description2)), 0, 'L');
        $this->SetXY(7, -14);
        $this->Cell(196, 7 , $this->sma->utf8Decode(lang('printed_by'). ' Wappsi © '.date('Y').' Web Apps Innovation SAS | wappsi te lo simplifica'),'',1,'C');
        $this->SetXY(195, -14);
        $this->Cell(7, 7 , $this->sma->utf8Decode(lang('page'). ' N° '.$this->PageNo()),'',1,'C');

    }

    function reduceTextToDescription1($text){
        if (strlen($text) > 390) {
            $text = substr($text, 0, 385);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function reduceTextToDescription2($text){
        if (strlen($text) > 380) {
            $text = substr($text, 0, 375);
            $text.="...";
            return $text;
        }
        return strip_tags($this->sma->decode_html($text));
    }

    function RoundedRect($x, $y, $w, $h, $r, $corners = '1234', $style = '')
    {
        $k = $this->k;
        $hp = $this->h;
        if($style=='F')
            $op='f';
        elseif($style=='FD' || $style=='DF')
            $op='B';
        else
            $op='S';
        $MyArc = 4/3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m',($x+$r)*$k,($hp-$y)*$k ));

        $xc = $x+$w-$r;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l', $xc*$k,($hp-$y)*$k ));
        if (strpos($corners, '2')===false)
            $this->_out(sprintf('%.2F %.2F l', ($x+$w)*$k,($hp-$y)*$k ));
        else
            $this->_Arc($xc + $r*$MyArc, $yc - $r, $xc + $r, $yc - $r*$MyArc, $xc + $r, $yc);

        $xc = $x+$w-$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-$yc)*$k));
        if (strpos($corners, '3')===false)
            $this->_out(sprintf('%.2F %.2F l',($x+$w)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc + $r, $yc + $r*$MyArc, $xc + $r*$MyArc, $yc + $r, $xc, $yc + $r);

        $xc = $x+$r;
        $yc = $y+$h-$r;
        $this->_out(sprintf('%.2F %.2F l',$xc*$k,($hp-($y+$h))*$k));
        if (strpos($corners, '4')===false)
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-($y+$h))*$k));
        else
            $this->_Arc($xc - $r*$MyArc, $yc + $r, $xc - $r, $yc + $r*$MyArc, $xc - $r, $yc);

        $xc = $x+$r ;
        $yc = $y+$r;
        $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$yc)*$k ));
        if (strpos($corners, '1')===false)
        {
            $this->_out(sprintf('%.2F %.2F l',($x)*$k,($hp-$y)*$k ));
            $this->_out(sprintf('%.2F %.2F l',($x+$r)*$k,($hp-$y)*$k ));
        }
        else
            $this->_Arc($xc - $r, $yc - $r*$MyArc, $xc - $r*$MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3)
    {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1*$this->k, ($h-$y1)*$this->k,
            $x2*$this->k, ($h-$y2)*$this->k, $x3*$this->k, ($h-$y3)*$this->k));
    }
}

$pdf = new PDF('P', 'mm', array(216, 279));
$pdf->AliasNbPages();
$pdf->sma = $this->sma;
$pdf->SetMargins(13, 7);

$number_convert = new number_convert();

$fuente = 8;
$adicional_fuente = 2;

$color_R = 200;
$color_G = 200;
$color_B = 200;

$pdf->setFillColor($color_R,$color_G,$color_B);

$pdf->logo = $biller_logo == 2 ? $biller->logo_square : $biller->logo;
$pdf->biller_logo = $biller_logo;

$pdf->session = $this->session;
$pdf->biller = $biller;
$pdf->Settings = $this->Settings;
$pdf->tipo_regimen = $tipo_regimen;
$pdf->biller = $biller;
$pdf->customer = $customer;
$pdf->document_type = $document_type;
// $pdf->seller = $seller;
$pdf->factura = $inv;
$pdf->fuente = $fuente;
$pdf->adicional_fuente = $adicional_fuente;
$pdf->color_R = $color_R;
$pdf->color_G = $color_G;
$pdf->color_B = $color_B;
$pdf->sma = $sma;
$pdf->document_type_invoice_format = $document_type_invoice_format;
$description1 = $inv->note;
$pdf->description2 = $biller->invoice_footer;

$pdf->AddPage();

$maximo_footer = 242;
$taxes = [];
$plus_font_size = $product_detail_font_size != 0 ? $product_detail_font_size : 0;
$pdf->SetFont('Arial','',$fuente+$plus_font_size);
$taxes = [];
$total_bruto = 0;
$references = [];

    foreach ($rows as $item) {

        $total_bruto += (($item->net_unit_price + ($item->item_discount / $item->quantity)) * $item->quantity) * $trmrate;

        if (!isset($taxes[$item->tax_rate_id])) {
            $taxes[$item->tax_rate_id]['tax'] = $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] = $item->net_unit_price * $item->quantity;
        } else {
            $taxes[$item->tax_rate_id]['tax'] += $item->item_tax;
            $taxes[$item->tax_rate_id]['base'] += $item->net_unit_price * $item->quantity;
        }

        $references[$item->id]['pr_code'] = ($show_code == 1 ? $item->product_code : $item->reference);
        $pr_name = $pdf->sma->reduce_text_length(
                                                    $item->product_name.
                                                    (!is_null($item->variant) ? "( ".$item->variant." )" : '').
                                                    ($pdf->Settings->show_brand_in_product_search ? " - ".$item->brand_name : '').
                                                    (!is_null($item->serial_no) ? " - ".$item->serial_no : '')
                                                    
                                                , 50);
        $references[$item->id]['pr_name'] = $pr_name;
        $references[$item->id]['pr_discount'] = $item->discount;
        $references[$item->id]['pr_individual_discount'] = $item->item_discount / $item->quantity;
        $references[$item->id]['pr_tax'] = $item->tax;
        $references[$item->id]['pr_individual_tax'] = $item->item_tax / $item->quantity;
        $references[$item->id]['unit_price_1'] = ($item->net_unit_price + ($item->item_discount / $item->quantity)) * $trmrate;
        $references[$item->id]['unit_price_2'] = $item->net_unit_price * $trmrate;
        $references[$item->id]['unit_price_3'] = $item->unit_price * $trmrate;
        $references[$item->id]['pr_subtotal'] = $item->subtotal * $trmrate;
        $references[$item->id]['pr_quantity'] = $item->quantity;
        if (isset($item->operator)) {
            if ($item->operator == "*") {
                $references[$item->id]['pr_quantity'] = $item->quantity / $item->operation_value;
            } else if ($item->operator == "/") {
                $references[$item->id]['pr_quantity'] = $item->quantity * $item->operation_value;
            } else if ($item->operator == "+") {
                $references[$item->id]['pr_quantity'] = $item->quantity - $item->operation_value;
            } else if ($item->operator == "-") {
                $references[$item->id]['pr_quantity'] = $item->quantity + $item->operation_value;
            }
        }
        $references[$item->id]['product_unit_code'] = $item->product_unit_code;
    }
    $cnt = 1;
// for ($i=0; $i < 15 ; $i++) {
    foreach ($references as $reference => $data) {
        if ($pdf->getY() > $maximo_footer) {
            $pdf->AddPage();
        }

        $columns = [];
            $inicio_fila_X = $pdf->getX();
            $inicio_fila_Y = $pdf->getY();


        $pr_name_wdth = 85.4;
        if (!$document_type_invoice_format || ($document_type_invoice_format && $document_type_invoice_format->product_show_code == 1)) {
            $columns[0]['inicio_x'] = $pdf->getX();
            $pdf->Cell(23.2, 5, $this->sma->utf8Decode($data['pr_code']),'',0,'C',0);
            $columns[0]['fin_x'] = $pdf->getX();
        } else{
            $pr_name_wdth += 23.2;
        }
        
        $columns[1]['inicio_x'] = $pdf->getX();
            $cX = $pdf->getX();
            $cY = $pdf->getY();
            $inicio_altura_fila = $cY;
        $pdf->setXY($cX, $cY+0.5);
        $pdf->MultiCell($pr_name_wdth,3, $this->sma->utf8Decode($data['pr_name']),'','L');
        $columns[1]['fin_x'] = $cX+$pr_name_wdth;
            $fin_altura_fila = $pdf->getY();
            $pdf->setXY($cX+$pr_name_wdth, $cY);
        $columns[2]['inicio_x'] = $pdf->getX();
        $pdf->Cell(26.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['unit_price_3'])),'',0,'C',0);
        $columns[2]['fin_x'] = $pdf->getX();
        $columns[3]['inicio_x'] = $pdf->getX();
        $pdf->Cell(10.6, 5, $this->sma->utf8Decode($data['pr_tax']),'',0,'C',0);
        $columns[3]['fin_x'] = $pdf->getX();
        $columns[4]['inicio_x'] = $pdf->getX();
        $pdf->Cell(11.8, 5, $this->sma->utf8Decode(($this->sma->formatQuantity($data['pr_quantity'], $qty_decimals))),'',0,'C',0);
        $columns[4]['fin_x'] = $pdf->getX();
        $columns[5]['inicio_x'] = $pdf->getX();
        $pdf->Cell(11.8, 5, $this->sma->utf8Decode($data['product_unit_code']),'',0,'C',0);
        $columns[5]['fin_x'] = $pdf->getX();
        $columns[6]['inicio_x'] = $pdf->getX();
        $pdf->Cell(26.6, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $data['pr_subtotal'])),'',0,'C',0);
        $columns[6]['fin_x'] = $pdf->getX();

            $fin_fila_X = $pdf->getX();
            $fin_fila_Y = $pdf->getY();
            $ancho_fila = $fin_fila_X - $inicio_fila_X;
            $altura_fila = $fin_altura_fila - $inicio_altura_fila;
            $pdf->setXY($inicio_fila_X, $inicio_fila_Y);
            foreach ($columns as $key => $data) {
                $ancho_column = $data['fin_x'] - $data['inicio_x'];
                if ($altura_fila < 5) {
                    $altura_fila = 5;
                }
                $pdf->Cell($ancho_column,$altura_fila, $this->sma->utf8Decode(''),'LBR',0,'R');
            }
            $pdf->ln($altura_fila);
        $cnt++;
    }
// }
$pdf->SetFont('Arial','',$fuente);
if ($pdf->getY() > 185) {
    $pdf->AddPage();
}

$cX_items_finished = $pdf->getX();
$cY_items_finished = $pdf->getY();

//izquierda

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;

$pdf->RoundedRect($current_x, $current_y, 115.6, 12.25, 3, '1234', '');

$pdf->ln(1);


$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->Cell(115.6,5, ($this->session->userdata['language'] != 'english' ? $this->sma->utf8Decode('VALOR (En letras)') : ''),'',1,'L');
$pdf->setX(14);
if ($this->session->userdata['language'] != 'english') {
    $pdf->MultiCell(115.6,3, $this->sma->utf8Decode($number_convert->convertir($inv->grand_total)),0,'L');
}

$pdf->setX(13);

$pdf->setXY($current_x, $current_y+13);

$total = $inv->total;

$pdf->Cell(28.9,5, $this->sma->utf8Decode(ucfirst(lang('tax_type'))),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode(ucfirst(lang('amount_base'))),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode(ucfirst(lang('amount_tax'))),'',0,'C');
$pdf->Cell(28.9,5, $this->sma->utf8Decode(ucfirst(lang('amount_total'))),'',1,'C');

foreach ($taxes as $tax => $arr) {
    $current_x = $pdf->getX();
    $current_y = $pdf->getY();

    $total+=$arr['tax'];

    $pdf->Cell(28.9,3, $this->sma->utf8Decode(isset($taxes_details[$tax]) ? $taxes_details[$tax] : "Exento"),'',1,'C');

    $pdf->setXY($current_x + 28.9, $current_y);
    $pdf->setX($current_x + 28.9);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($arr['base'])),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 2), $current_y);
    $pdf->setX($current_x + (28.9 * 2));
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($arr['tax'])),'',1,'C');

    $pdf->setXY($current_x + (28.9 * 3), $current_y);
    $pdf->setX($current_x + 28.9 * 3);
    $pdf->Cell(28.9,3, $this->sma->utf8Decode($this->sma->formatMoney($total)),'',1,'C');
}

$current_x = $pdf->getX();
$current_y = $pdf->getY() + 1;


//derecha

$pdf->setXY($cX_items_finished+118, $cY_items_finished);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('gross_total'))), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->total + $inv->product_tax))), 'TBR', 1, 'R');
$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('discount'))), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_discount)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('Subtotal'), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, ($inv->total + $inv->product_tax - $inv->order_discount))), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('amount_tax'))), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->order_tax)), 'TBR', 1, 'R');

$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('withholding_tax'))), 'TBLR', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(0), 'TBR', 1, 'R');

if ($inv->shipping > 0) {
    $current_y+=5;
    $pdf->setXY($current_x, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode(ucfirst(lang('shipping'))), 'TBLR', 1, 'L');
    $pdf->setXY($current_x+39.2, $current_y);
    $pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->shipping)), 'TBR', 1, 'R');
}
    
$current_y+=5;
$pdf->setXY($current_x, $current_y);
$pdf->SetFont('Arial','B',$fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode('TOTAL'), 'TBLR', 1, 'R', 1);
$pdf->setXY($current_x+39.2, $current_y);
$pdf->SetFont('Arial','',$fuente);
$pdf->cell(39.2, 5, $this->sma->utf8Decode($this->sma->formatValue($value_decimals, $inv->grand_total)), 'TBR', 1, 'R');


//FIRMAS

//izquierda
$pdf->ln(2);

$current_x = $pdf->getX();
$current_y = $pdf->getY();

$pdf->RoundedRect($current_x, $current_y, 196, 11, 3, '1234', '');

$pdf->ln(1);
$current_x = $pdf->getX();
$current_y = $pdf->getY()+11;

$pdf->MultiCell(196,3, '   '.$this->sma->utf8Decode($pdf->reduceTextToDescription1($description1)),0,'L');

// $pdf->ln();

$pdf->setXY($current_x, $current_y);

$pdf->RoundedRect($current_x, $current_y, 115.6, 15, 3, '1234', '');
$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode(lang('legal_representative')), '', 1, 'L');

//derecha
$pdf->setXY($current_x+115.6, $current_y);
$current_x = $pdf->getX()+2;
$current_y = $pdf->getY();
$pdf->RoundedRect($current_x, $current_y, 78.4, 15, 3, '1234', '');

$pdf->setX($current_x+3);
$pdf->cell(43, 5, $this->sma->utf8Decode(lang('accept_signature_and_stamp_received')), '', 1, 'L');

$pdf->setXY($current_x+3, $current_y+10);
$pdf->cell(39.2, 5, $this->sma->utf8Decode(lang('accept_document_type')), 'T', 1, 'L');
$pdf->setXY($current_x+39.2, $current_y+10);
$pdf->cell(37, 5, $this->sma->utf8Decode(lang('date')), 'T', 1, 'L');


if ($for_email) {
  $pdf->Output(FCPATH."files/".$inv->reference_no.".pdf", "F");
} else {
    if ($for_download) {
      $pdf->Output($inv->reference_no.".pdf", "D");
    } else {
      $pdf->Output($inv->reference_no.".pdf", "I");
    }
}
