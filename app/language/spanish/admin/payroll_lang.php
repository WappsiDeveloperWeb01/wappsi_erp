<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
|
|---------------------------------
| Payroll
|---------------------------------
|
*/
$lang["payroll_management_list"] = "Listado de Nómina";
$lang["payroll_management_add"] = "Nueva nómina";
$lang["payroll_management_month"] = "Mes";
$lang["payroll_management_biweekly"] = "Quincena";
$lang["payroll_management_weekly"] = "Semana";
$lang["biweekly_one"] = "Primera Quincena";
$lang["payroll_management_number_time"] = "Repetición de meses";
$lang["biweekly_two"] = "Segunda Quincena";
$lang["payroll_management_number_employees"] = "No. Empleados";
$lang["payroll_management_earnings"] = "Devengados";
$lang["payroll_management_earning"] = "Devengado";
$lang["payroll_management_deductions"] = "Deducciones";
$lang["payroll_management_deduction"] = "Deducción";
$lang["payroll_management_total_payment"] = "Pago Total";
$lang["payroll_management_payment_date"] = "Fecha de pago";
$lang["payroll_management_in_preparation"] = "En preparación";
$lang["payroll_management_approved"] = "Aprobada";
$lang["payroll_management_paid"] = "Pagada";
$lang["payroll_management_employee"] = "Empleado";
$lang["payroll_management_saved"] = "La nómina fue creada correctamente";
$lang["payroll_management_not_saved"] = "No fue posible crear la nómina";
$lang["payroll_management_existing"] = "Ya se encuentra una nómina registrada con los datos ingresados";
$lang["payroll_management_updated"] = "La nómina se actualizó correctamente";
$lang["payroll_management_not_updated"] = "No fue posible actualizar la nómina";
$lang["payroll_management_state_not_preparation"] = "El estado de la nómina no esta en <strong>Preparación</strong>";
$lang["payroll_management_see_employee_concepts"] = "Conceptos del empleado";
$lang["add_concept"] = "Agregar Novedad";
$lang["payroll_management_saved_item"] = "El concepto fue creado correctamente";
$lang["payroll_management_not_saved_item"] = "No fue posible crear el concepto";
$lang["parroll_management_value"] = "Valor";
$lang["payroll_management_see_details"] = "Ver detalles";
$lang["parroll_management_frequency"] = "Frecuencia";
$lang["parroll_management_once"] = "Unica vez";
$lang["parroll_management_recurrent"] = "Recurrente";
$lang["parroll_management_permanent"] = "Permanente";
$lang["parroll_management_amount_fees"] = "Número de cuotas";
$lang["parroll_management_not_saved_future_concept"] = "No fue posible guardar el concepto";
$lang["payroll_management_no_contracts"] = "No existen contratos dentro de las fechas de la nómina seleccionada";
$lang["payroll_management_total_provisions"] = "Provisiones";
$lang['first_week'] = 'Primera semana';
$lang['second_week'] = 'Segunda semana';
$lang['third_week'] = 'Tercera semana';
$lang['fourth_week'] = 'Cuarta semana';
$lang['fifth_week'] = 'Quinta semana';
$lang['names_weeks'] = [
	1=>'Primera semana',
	2=>'Segunda semana',
	3=>'Tercera semana',
	4=>'Cuarta semana',
	5=>'Quinta semana'
];
$lang['biweekly_names'] = [
	1=>'Primera Quincena',
	2=>'Segunda Quincena'
];
$lang['approve'] = 'Aprobar';
$lang['to_accept'] = 'Aceptar';
$lang['confirmation_window'] = 'Ventana de confirmación';
$lang['are_you_sure_to_approve_the_payroll'] = '¿Esta Seguro de Aprobar la Nómina?';
$lang['approved_payroll'] = 'La nómina ha sido aprobada correctamente';
$lang['not_approved_payroll'] = 'La nómina no ha sido aprobada';
$lang['eliminated_payroll'] = 'La nómina ha sido eliminada correctamente';
$lang['payroll_not_eliminated'] = 'La nómina no pudo ser eliminada';
$lang['payroll_items_not_deleted'] = 'Los items de la nómina no pudieron ser eliminados';
$lang['number_days'] = 'Cantidad de días';
$lang["end_date"] = "Fecha finalización";
$lang["conceptNames"] = [
	PAID_LICENSE => "Licencia Remunerada",
	COMMON_DISABILITY => "Incapacidad Común",
	WORK_DISABILITY => "Incapacidad Laboral",
	PROFESSIONAL_DISABILITY => "Incapacidad Profesional",
	UNEXCUSED_ABSENTEEISM => "Ausentismo no justificado",
	DISCIPLINARY_SUSPENSION => "Suspensión Disciplinaria",
	NOT_PAID_LICENSE=>"Licencia no Remunerada",
	TIME_VACATION=>"Vacaciones en Tiempo",
    FAMILY_ACTIVITY_PERMIT=>"Permiso por Actividad Familiar",
    DUEL_LICENSE=>"Licencia por Luto",
    PAID_PERMIT=>"Permiso Remunerado",
    MATERNITY_LICENSE=>"Licencia de Maternidad",
    PATERNITY_LICENSE=>"Licencia de Paternidad",
    NOT_PAID_PERMIT=>"Permiso no Remunerado",
    LAYOFFS=>"Cesantías",
    LAYOFFS_INTERESTS=>"Interéses de cesantías",
    MONEY_VACATION=>"Vacaciones en Dinero",
    SERVICE_BONUS=>"Prima de Servicios",
	SUNDAY_DISCOUNT=>"Descuento dominical",
];
$lang["branch"] = "Sucursal";
$lang["yes"] = "Si";
$lang["not"] = "No";
$lang["start"] = "Inicio";
$lang["end"] = "Final";
$lang["date"] = "Fecha";
$lang["working_days"] = "Días laborados";
$lang["remove_employee_payroll"] = 'Eliminar empleado';
$lang['employee_removed'] = 'Empleado eliminado correctamente';
$lang['employee_was_not_removed'] = 'Empleado no ha sido eliminado';
$lang["to_pay"] = "Pagar";
$lang["payroll_paid"] = "Nómina pagada";
$lang["payroll_unpaid"] = "Nómina no pagada";
$lang["spreadsheet"] = "Planilla";
$lang["calculationParafiscalAndProvisions"] = "Cálculo parafiscales y provisiones";
$lang["doYouWorkOnADayOff"] = "¿Trabaja en un día libre?";

/*
|
|---------------------------------
| Employees
|---------------------------------
|
*/
$lang["employees"] = "Empleados";
$lang["personal_information"] = "Información personal";
$lang["employees_list"] = "Lista Empleados";
$lang["profession"] = "Profesión";
$lang["employees_professional_position"] = "Cargo";
$lang["employees_add"] = "Agregar Empleado";
$lang["first_name"] = "Primer Nombre";
$lang["second_name"] = "Segundo Nombre";
$lang["first_lastname"] = "Primer Apellido";
$lang["second_lastname"] = "Segundo Apellido";
$lang["document_type"] = "Tipo Documento";
$lang['tipo_documento'] = 'Tipo Documento';
$lang['document_code'] = 'Código documento';
$lang["document_number"] = "No. Documento";
$lang["employees_email"] = "Correo electrónico";
$lang["employees_phone"] = "Celular";
$lang["home_phone"] = "Télefono fijo";
$lang["birthdate"] = "Fecha de nacimiento";
$lang["marital_status"] = "Estado civil";
$lang["blood_type"] = "Tipo de sangre";
$lang["military_card"] = "Libreta Militar";
$lang["educational_level"] = "Nivel educacional";
$lang["relationship"] = "Parentesco";
$lang["regime_type"] = "Tipo régimen";
$lang["gender"] = "Género";
$lang["employees_contract_status"] = "Estado contrato";
$lang["employees_biller"] = "Sucursal";
$lang['employees_main_contact'] = 'Contacto principal';
$lang["employees_saved"] = "Empleado guardado exitosamente";
$lang["employees_not_saved"] = "Empleado no ha sido guardado";
$lang["employees_edit"] = "Editar empleado";
$lang["employees_existing_document_number"] = "El Número de documento ya se encuentra registrado";
$lang["employees_existing_email"] = "El Correo electrónico ya se encuentra registrado";
$lang["employees_maximum_contacts_allowed"] = "Se permitirá un máximo de 3 contactos";
$lang["employees_updated"] = "Empleado actualizado exitosamente";
$lang["employees_not_updated"] = "Empleado no ha actualizado";
$lang["employees_add_contract"] = "Agregar contrato";
$lang["edit_contract"] = "Editar contrato";
$lang['city_code'] = 'Código ciudad';
$lang['contact_name'] = 'Nombre de contacto';
$lang['contact_address'] = 'Dirección de Contacto';
$lang['contact_phone'] = 'Teléfono de Contacto';
$lang['contact_email'] = 'Correo de Contacto';
$lang['remove_employee'] = 'Eliminar empleado';
$lang['eliminated_employee'] = 'Empleado eliminado correctamente';
$lang['eliminated_not_employee'] = 'Empleado no ha sido eliminado';
$lang['existing_employee_on_payroll'] = 'El Empleado ya se encuentra liquidado en una Nómina';
$lang['existing_user_transactions'] = 'Transacciones de usuario existentes en el módulo de Ventas';
$lang["arl_risk_classes"] = "Clase se riesgo";
$lang["contract"] = "Contrato";
$lang["status"] = "Estado";
$lang["social_security"] = "Seguridad Social";
$lang['educational_levels'] = [
	1=>"Básica primaria",
	2=>"Básica Secundaria",
	3=>"Educación Media técnica",
	4=>"Técnico profesional",
	5=>"Tecnología",
	6=>"Profesional Universitario",
	7=>"Especialización",
	8=>"Maestría",
	9=>"Doctorado",
	10=>"Postdoctorado"
];
$lang["free_union"] = "Unión libre";
$lang["married"] = "Casado";
$lang["divorced"] = "Separado";
$lang["single"] = "Soltero";
$lang["widower"] = "Viudo";
$lang['marital_statuses'] = [
	1=>'Unión libre',
	2=>'Casado',
	3=>'Divorciado',
	4=>'Soltero',
	5=>'Viudo'
];
$lang['genders'] = [
	1=>'Masculino',
	2=>'Femenino',
	3=>'Sin definir'
];
$lang['relations'] = [
	1=>"Conyuge",
	2=>"Padre",
	3=>"Madre",
	4=>"Hijo",
	5=>"Hija",
	6=>"Abuelo",
	7=>"Abuela",
	8=>"Otros"
];
$lang['blood_types'] = [
	1=>"A+",
	2=>"A-",
	3=>"B+",
	4=>"B-",
	5=>"O+",
	6=>"O-",
	7=>"AB+",
	8=>"AB-"
];

/*
|
|---------------------------------
| Contracts
|---------------------------------
|
*/
$lang["contract"] = "Contrato";
$lang["contract_list"] = "Lista Contrato";
$lang["payroll_contracts"] = "Contratos";
$lang["area"] = "Área";
$lang["icbf"] = "ICBF";
$lang["sena"] = "SENA";
$lang["bank"] = "Banco";
$lang["internal_code"] = "Código";
$lang["payroll_contracts_status"] = "Estado";
$lang["contracts_salary"] = "Salario";
$lang["payroll_contracts_biller"] = "Sucursal";
$lang["payroll_contracts_employee"] = "Empleado";
$lang["contracts_add"] = "Agregar contrato";
$lang["payroll_contracts_endowment"] = "Dotación";
$lang["eps"] = "Entidad de Salud";
$lang["workday"] = "Jornada laboral";
$lang["contracts_start_date"] = "Fecha inicio";
$lang["payroll_contracts_base_amount"] = "Salario base";
$lang["afp"] = "Entidad de Pensiones";
$lang["withholding_percentage"] = "Retención";
$lang["caja"] = "Caja de compensación";
$lang["payroll_contracts_daily_hours"] = "Horas diarias";
$lang["cesantia"] = "Fondo de Cesantías";
$lang["professional_position"] = "Cargo";
$lang["contracts_end_date"] = "Fecha finalización";
$lang["account_type"] = "Tipo de cuenta";
$lang["account_no"] = "Número de cuenta";
$lang["payroll_contracts_extra_allowance"] = "Auxilio extra";
$lang["payment_method"] = "Método de pago";
$lang["payroll_contracts_name_employee"] = "Nombre empleado";
$lang["contract_type"] = "Tipo de contrato";
$lang["type"] = "Tipo";
$lang["employee_type"] = "Tipo de empleado";
$lang["arl"] = "Entidad de Riesgos laborales";
$lang["payroll_contracts_integral_salary"] = "Salario integral";
$lang["payroll_contracts_social_security"] = "Seguridad social";
$lang["payroll_contracts_savings_account"] = "Cuenta de Ahorros";
$lang["payroll_contracts_current_account"] = "Cuenta corriente";
$lang["contracts_settlement_date"] = "Fecha terminación anticipada";
$lang["payroll_contracts_process_1"] = "Aplicar Procedimiento 1";
$lang["payroll_contracts_process_2"] = "Aplicar Procedimiento 2";
$lang["payroll_contracts_payment_frequency"] = "Periodo de pago";
$lang["payroll_contracts_not_saved"] = "Contrato no fue guardado";
$lang["retired_risk"] = "Alto riesgo de pensión";
$lang["payroll_contracts_saved"] = "Contrato guardado exitosamente";
$lang["trans_allowance"] = "Auxilio de transporte";
$lang["withholding_method"] = "Método de retención";
$lang["payroll_contracts_not_updated"] = "Contrato no fue actualizado";
$lang["payroll_contracts_account_virtual_wallet"] = "Billetera virtual";
$lang["payroll_contracts_updated"] = "Contrato actualizado exitosamente";
$lang["payroll_contracts_existing_internal_code"] = "El código interno ya se encuentra registrado";
$lang["payroll_contracts_no_general_settings"] = "No se ha configurado los parametros generales. Por favor diligencie los datos para continuar.";
$lang["payroll_contracts_edit"] = "Editar contrato";
$lang['mark_as_seller'] = "Marcar como vendedor";
$lang['salary_bonus'] = 'Bonificación constitutiva de Salario';
$lang['non_salary_bonus'] = 'Bonificación no constitutiva de Salario';
$lang['hand_calculation'] = 'Cálculo manual';
$lang['percentage_salary_apprentice_productive'] = 'Porcentaje salario de aprendiz en productiva';
$lang['percentage_salary_apprentice_teaching'] = 'Porcentaje salario de aprendiz en lectiva';
$lang['export_to_excel'] = 'Exportar a excel';

/*
|
|---------------------------------
| Settings
|---------------------------------
|
*/
$lang["payroll_settings"] = "Ajustes de Nómina";
$lang["payroll_general_adjustments"] = "Ajustes generales";
$lang["payroll_grants_exemptions"] = "Auxilios y exoneraciones";
$lang["payroll_bank_data"] = "Datos bancarios";
$lang["work_environment"] = "Entorno de desarrollo";
$lang["payment_frequency"] = "Frecuencia pago de nómina";
$lang["payment_schedule"] = "Liquidar nomina mensual de acuerdo con";
$lang["enable_saturday_vacation"] = "Incluir día sábado en liquidación de vacaciones";
$lang["minimum_salary_value"] = "Valor salario mínimo";
$lang["integral_salary_value"] = "Valor salario integral";
$lang["transportation_allowance_value"] = "Valor subsidio de transporte";
$lang["withholding_base_value"] = "Valor base de retención en UVT";
$lang["prima_payment"] = "Calendario definido para el pago de prima";
$lang["payroll_prima_payment_option_1"] = "Último dia de semestre";
$lang["payroll_prima_payment_option_2"] = "Ultimo día de quincena";
$lang["bonus_payment"] = "Frecuencia de pago de conceptos contrato";
$lang["arl"] = "Entidad administradora de riesgos laborales";
$lang["ccf"] = "Caja de compensación Familiar";
$lang["exempt_parafiscal_health_payments"] = "Se exonerada pago de aportes a parafiscales y salud";
$lang["small_business"] = "Se califica como micro, pequeña o mediana empresa";
$lang["exempt_concepts"] = "Aplicar conceptos a cálculo de vacaciones";
$lang["percentage_contributions"] = "Porcentaje de aportes";
$lang["illness_2days"] = "Auxilio para enfermedad común de 1 a 2 días";
$lang["illness_90days"] = "Auxilio para enfermedad común de 3 a 90 días";
$lang["illness_91"] = "Auxilio para enfermedad común de más de 91";
$lang["bank_id"] = "Banco";
$lang["bank_account_number"] = "No. Cuenta bancaria";
$lang["bank_account_type"] = "Tipo de cuenta";
$lang["payroll_savings_account"] = "Cuenta de ahorro";
$lang["payroll_current_account"] = "Cuenta corriente";
$lang["payroll_account_virtual_wallet"] = "Billetera virtual";
$lang["file_format"] = "Formato Archivo";
$lang["payment_note"] = "Nota de pagos";
$lang["ss_operators"] = "Operador de aportes a seguridad social";
$lang["payroll_settings_saved"] = "Ajuste guardados correctamente";
$lang["payroll_settings_not_saved"] = "Ajuste NO guardados correctamente";
$lang["payment_frequency_change_warning"] = "Advertencia cambio de frecuencia de pago";
$lang["UVT_value"] = "Valor UVT";
$lang["compensation_fund_percentage"] = "% de aportes Caja de compensación";
$lang["pension_percentage"] = "% de aportes a Pensiones";
$lang["high_risk_pension_percentage"] = "% de aportes a Pensiones de alto riesgo";
$lang["generate_payroll_per_branch"] = "Generar nómina por Sucursal";
$lang["weekly_working_hours"] = "Horas laborales semanales";
$lang["set_test_id"] = "Set Test id";
$lang["bonus_limit_percentage"] = "Límite de bonificación";
$lang["layoffs_interests"] = "Porcentaje Liquidación Interes de Cesantías";
$lang["payroll_start_date"] = "Fecha Inicial de Nómina";
$lang["provision_options"] = "Opciones de provisión";
$lang["do_not_provision"] = "No provisionar";
$lang["provision_without_reporting_to_DIAN"] = "Provisionar sin reportar a DIAN";
$lang["provision_and_report_to_DIAN"] = "Provisionar y reportar a DIAN";

/*
|
|---------------------------------
| Concepts
|---------------------------------
|
*/
$lang["payroll_concepts"] = "Conceptos";
$lang["payroll_concepts_name"] = "Nombre";
$lang["payroll_concepts_type"] = "Tipo de concepto";
$lang["payroll_concepts_earned_deducted"] = "Devengado / Deducido";
$lang["payroll_concepts_percentage"] = "Porcentaje";
$lang["payroll_concepts_status"] = "Estado";
$lang["payroll_concepts_add"] = "Nuevo Concepto";
$lang["payroll_concepts_saved"] = "Concepto guardado correctamente";
$lang["payroll_concepts_not_saved"] = "No fue posible guardar el Concepto";
$lang["payroll_concepts_edit"] = "Editar Concepto";
$lang["payroll_concepts_updated"] = "Concepto actualizado correctamente";
$lang["payroll_concepts_not_updated"] = "No fue posible actualizar el Concepto";
$lang["payroll_concepts_disability_days"] = "Cantidad Días de incapacidad";
$lang["payroll_who_covers_disability"] = "¿Quién cubre la incapacidad?";
$lang["employer"] = "Empleador";
$lang["eps_less_than_91_days"] = "EPS entre 3 hasta 91 días";
$lang["eps_more_than_91_days"] = "EPS después de 91 días";
$lang["payment_frequency_concept"] = "Frecuencia de pago";

/*
|
|---------------------------------
| Payroll electronic
|---------------------------------
|
*/
$lang["payroll_electronic"] = "Nómina Electrónica";
$lang["payroll_electronic_list"] = "Listado Nómina Electrónica";
$lang["payroll_electronic_detail"] = "Detalle de Nómina Electrónica";
$lang["not_sent"] = "No enviado";
$lang["pending"] = "Pendiente";
$lang["accepted"] = "Aceptado";
$lang["employee"] = "Empleado";
$lang["send_electronic_payroll"] = "Enviar";
$lang["multiple_send_electronic_payroll"] = "Enviar Múltiples";
$lang["ne_previous_json"] = "Nómina electrónica (.json)";
$lang["nee_previous_json"] = "Documento eliminación (.json)";
$lang["nea_previous_json"] = "Documento ajuste (.json)";
$lang["delete_electronic_payroll_document"] = "Eliminar";
$lang["adjustment_electronic_payroll_document"] = "Ajustar";
$lang["to_affect"] = "Afecta";
$lang["send_delete_electronic_payroll"] = "Enviar documento eliminación";
$lang["payroll_adjustment"] = "Ajuste Novedades de Nómina";
$lang["create_adjustmet"] = "Crear ajuste";
$lang["send_adjustment_electronic_payroll_document"] = "Enviar documento de ajuste";
$lang["check_document_status"] = "Consultar estado documento";
$lang["pay_slip"] = "Comprobante de nomina";
$lang["sunday_discount_for_absenteeism"] = "Aplicar descuento dominical por  ausentismo";
$lang["documentTypesNotExist"] = "No se ha configurado los tipos de documentos para Nómina electrónica";
$lang["existingPayroll"] = "Ya existe una Nomina Electrónica creada para el mes seleccionado";

/*
|
|---------------------------------
| Payroll exports
|---------------------------------
|
*/
$lang['report_type'] = 'Tipo de informe';
$lang["payroll_exports"] = "Exportar";
$lang["include_provisions_social_benefits"] = "Provisiones y prestaciones sociales";
$lang["include_personal_data_employee"] = "Datos personales del empleado";
$lang["biweekly"] = "Quincenal";
$lang["monthly"] = "Mensual";
$lang["summarized"] = "Resumido";
$lang["detailed"] = "Detallado";
$lang["frequency"] = "Frecuencia";