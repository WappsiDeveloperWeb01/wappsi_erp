<?php
require_once APPPATH.'../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Set document properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");


$nit_old = $sucursal_old = $vendedor_old =  '' ; 
$total = $total_30 = $total_60 = $total_90 = $total_120 =  $total_mas = 0;
$total_vendedor = $total_30_vendedor = $total_60_vendedor = $total_90_vendedor = $total_120_vendedor =  $total_mas_vendedor = 0;
$i = $j = 0 ;
$saldo = $saldo_30 = $saldo_60 = $saldo_90 = $saldo_120 =  $saldo_mas = '';
$total_sucursal = $total_30_sucursal = $total_60_sucursal = $total_90_sucursal = $total_120_sucursal =  $total_mas_sucursal = 0;
$prin_ven = $prin_clien = false; 

$j++;
$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(30);

$objPHPExcel->setActiveSheetIndex(0)->getStyle("A1:J1")->getFont()->setBold(true);

$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, 'Vendedor')
			->setCellValue('B'.$j, 'Zona')
			->setCellValue('C'.$j, 'Sucursal')
			->setCellValue('D'.$j, 'Cliente')
			->setCellValue('E'.$j, 'Fecha')
			->setCellValue('F'.$j, 'Consecutivo')
			->setCellValue('G'.$j, 'Consecutivo')
			->setCellValue('H'.$j, 'Subtotal')
			->setCellValue('I'.$j, 'Impuestos')
			->setCellValue('K'.$j, 'Total') 
			->setCellValue('L'.$j, 'Estado') 
			->setCellValue('M'.$j, 'Zona') 
			->setCellValue('N'.$j, 'Barrio') 
			;

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultRowDimension()
			->setRowHeight(16);

$objPHPExcel->setActiveSheetIndex(0)
			->getDefaultColumnDimension()
			->setWidth(16);
$j++;

$sellers = [];
$zones = [];
$subzones = [];
$subtotal_seller = 0;
$total_iva_seller = 0;
$total_seller = 0;
$subtotal_zone = 0;
$total_iva_zone = 0;
$total_zone = 0;
$subtotal_subzone = 0;
$total_iva_subzone = 0;
$total_subzone = 0;
$subtotal = 0;
$total_iva = 0;
$total = 0;

foreach ($sales as $sale) 
{

    if (!isset($sellers[$sale['cod_seller']])) {
		$sellers[$sale['cod_seller']] = 1;
		$zones = [];
		$subzones = [];
        if ($total_subzone != 0 && $input['group_by_subzone'] == 1) {
    		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total Barrio')
				->setCellValue('H'.$j, $subtotal_subzone)
				->setCellValue('I'.$j, $total_iva_subzone)
				->setCellValue('J'.$j, $total_subzone)
				->setCellValue('K'.$j, '');
				$subtotal_subzone = 0;
				$total_iva_subzone = 0;
				$total_subzone = 0;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;
        }
        if ($total_zone != 0 && $input['group_by_zone'] == 1) {
    		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total Zona')
				->setCellValue('H'.$j, $subtotal_zone)
				->setCellValue('I'.$j, $total_iva_zone)
				->setCellValue('J'.$j, $total_zone)
				->setCellValue('K'.$j, '');
				$subtotal_zone = 0;
				$total_iva_zone = 0;
				$total_zone = 0;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;
        }
        if ($total_seller != 0) {
    		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total vendedor')
				->setCellValue('H'.$j, $subtotal_seller)
				->setCellValue('I'.$j, $total_iva_seller)
				->setCellValue('J'.$j, $total_seller)
				->setCellValue('K'.$j, '');
				$subtotal_seller = 0;
				$total_iva_seller = 0;
				$total_seller = 0;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, $sale['nom_seller'])
			->setCellValue('B'.$j, '')
			->setCellValue('C'.$j, '')
			->setCellValue('D'.$j, '')
			->setCellValue('E'.$j, '')
			->setCellValue('F'.$j, '')
			->setCellValue('G'.$j, '')
			->setCellValue('H'.$j, '')
			->setCellValue('I'.$j, '')
			->setCellValue('J'.$j, '')
			->setCellValue('K'.$j, '');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;

    }
    if (!isset($zones[$sale['cod_zone']]) && $input['group_by_zone'] == 1) {
		$zones[$sale['cod_zone']] = 1;
		$subzones = [];
        if ($total_subzone != 0) {
    		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total Barrio')
				->setCellValue('H'.$j, $subtotal_subzone)
				->setCellValue('I'.$j, $total_iva_subzone)
				->setCellValue('J'.$j, $total_subzone)
				->setCellValue('K'.$j, '');
				$subtotal_subzone = 0;
				$total_iva_subzone = 0;
				$total_subzone = 0;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;
        }
        if ($total_zone != 0) {
    		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total Zona')
				->setCellValue('H'.$j, $subtotal_zone)
				->setCellValue('I'.$j, $total_iva_zone)
				->setCellValue('K'.$j, $total_zone)
				->setCellValue('L'.$j, '');
				$subtotal_zone = 0;
				$total_iva_zone = 0;
				$total_zone = 0;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, "Zona : ".(!empty($sale['nom_zone']) ? $sale['nom_zone'] : "No registra"))
			->setCellValue('B'.$j, '')
			->setCellValue('C'.$j, '')
			->setCellValue('D'.$j, '')
			->setCellValue('E'.$j, '')
			->setCellValue('F'.$j, '')
			->setCellValue('G'.$j, '')
			->setCellValue('H'.$j, '')
			->setCellValue('I'.$j, '')
			->setCellValue('J'.$j, '')
			->setCellValue('K'.$j, '');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;

    }
    if (!isset($subzones[$sale['cod_subzone']]) && $input['group_by_subzone'] == 1) {
		$subzones[$sale['cod_subzone']] = 1;
        if ($total_subzone != 0) {
    		$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total Barrio')
				->setCellValue('H'.$j, $subtotal_subzone)
				->setCellValue('I'.$j, $total_iva_subzone)
				->setCellValue('J'.$j, $total_subzone)
				->setCellValue('K'.$j, '');
				$subtotal_subzone = 0;
				$total_iva_subzone = 0;
				$total_subzone = 0;
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;
        }

        $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, "Barrio : ".(!empty($sale['nom_subzone']) ? $sale['nom_subzone'] : "No registra"))
			->setCellValue('B'.$j, '')
			->setCellValue('C'.$j, '')
			->setCellValue('D'.$j, '')
			->setCellValue('E'.$j, '')
			->setCellValue('F'.$j, '')
			->setCellValue('G'.$j, '')
			->setCellValue('H'.$j, '')
			->setCellValue('I'.$j, '')
			->setCellValue('J'.$j, '')
			->setCellValue('K'.$j, '');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
        	$j++;

    }

    $referencia = ($sale['return_sale_ref']!='') ? $sale['return_sale_ref'] : $sale['reference_no'] ;
    $afec = ($sale['return_sale_ref']!='') ? $sale['reference_no'] : '' ;

    if ($tipo == 'Detallado' || !$tipo) {
	    $objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, $sale['nom_seller'])
			->setCellValue('B'.$j, (!empty($sale['nom_zone']) ? $sale['nom_zone'] : "No registra"))
			->setCellValue('C'.$j, $sale['Sucursal'])
			->setCellValue('D'.$j, $sale['customer_name'])
			->setCellValue('E'.$j, $sale['date'])
			->setCellValue('F'.$j, $referencia)
			->setCellValue('G'.$j, $afec)
			->setCellValue('H'.$j, $sale['total'])
			->setCellValue('I'.$j, $sale['total_tax'])
			->setCellValue('J'.$j, $sale['grand_total'])
			->setCellValue('K'.$j, lang($sale['payment_status']))
			->setCellValue('M'.$j, $sale['nom_zone'])
			->setCellValue('N'.$j, $sale['nom_subzone'])
			;
	    	$j++;
    }


    $subtotal_seller += $sale['total'];
    $total_iva_seller += $sale['total_tax'];
    $total_seller += $sale['grand_total'];

    $subtotal_zone += $sale['total'];
    $total_iva_zone += $sale['total_tax'];
    $total_zone += $sale['grand_total'];

    $subtotal_subzone += $sale['total'];
    $total_iva_subzone += $sale['total_tax'];
    $total_subzone += $sale['grand_total'];
    $subtotal += $sale['total'];
    $total_iva += $sale['total_tax'];
    $total += $sale['grand_total'];

    if ($sale === end($sales)) {
    	if ($input['group_by_subzone'] == 1) {
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total Barrio')
				->setCellValue('H'.$j, $subtotal_subzone)
				->setCellValue('I'.$j, $total_iva_subzone)
				->setCellValue('J'.$j, $total_subzone)
				->setCellValue('K'.$j, '');
				$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
				$subtotal_subzone = 0;
				$total_iva_subzone = 0;
				$total_subzone = 0;
			$j++;
    	}

		if ($input['group_by_zone'] == 1) {
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('A'.$j, '')
				->setCellValue('B'.$j, '')
				->setCellValue('C'.$j, '')
				->setCellValue('D'.$j, '')
				->setCellValue('E'.$j, '')
				->setCellValue('F'.$j, '')
				->setCellValue('G'.$j, 'Total Zona')
				->setCellValue('H'.$j, $subtotal_zone)
				->setCellValue('I'.$j, $total_iva_zone)
				->setCellValue('J'.$j, $total_zone)
				->setCellValue('K'.$j, '');
				$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
				$subtotal_zone = 0;
				$total_iva_zone = 0;
				$total_zone = 0;
			$j++;
		}


		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, '')
			->setCellValue('B'.$j, '')
			->setCellValue('C'.$j, '')
			->setCellValue('D'.$j, '')
			->setCellValue('E'.$j, '')
			->setCellValue('F'.$j, '')
			->setCellValue('G'.$j, 'Total vendedor')
			->setCellValue('H'.$j, $subtotal_seller)
			->setCellValue('I'.$j, $total_iva_seller)
			->setCellValue('J'.$j, $total_seller)
			->setCellValue('K'.$j, '');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
			$subtotal_seller = 0;
			$total_iva_seller = 0;
			$total_seller = 0;
		$j++;
    }
}

		$objPHPExcel->setActiveSheetIndex(0)
			->setCellValue('A'.$j, '')
			->setCellValue('B'.$j, '')
			->setCellValue('C'.$j, '')
			->setCellValue('D'.$j, '')
			->setCellValue('E'.$j, '')
			->setCellValue('F'.$j, '')
			->setCellValue('G'.$j, 'Total')
			->setCellValue('H'.$j, $subtotal)
			->setCellValue('I'.$j, $total_iva)
			->setCellValue('J'.$j, $total)
			->setCellValue('K'.$j, '');
			$objPHPExcel->setActiveSheetIndex(0)->getStyle("A".$j.":J".$j)->getFont()->setBold(true);
		$j++;
$objPHPExcel->getActiveSheet()->getStyle('G1:G'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('H1:H'.$j)->getNumberFormat()->setFormatCode('#,##0.00');
$objPHPExcel->getActiveSheet()->getStyle('I1:I'.$j)->getNumberFormat()->setFormatCode('#,##0.00');

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('VENTAS POR VENDEDOR');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);


// Redirect output to a client’s web browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="VENTAS POR VENDEDOR.xls"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
