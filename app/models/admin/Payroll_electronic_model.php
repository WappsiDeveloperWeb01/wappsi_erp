<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_electronic_model extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function find($data)
    {
        $this->db->where($data);
        $q = $this->db->get("payroll_electronic");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_by_id($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("payroll_electronic");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return TRUE;
    }

    public function get_electronic_payroll_employee_by_id($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("payroll_electronic_employee");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return TRUE;
    }

    public function get_electronic_payroll_employee_by_electronic_payroll_id($electronic_payroll_id)
    {
        $this->db->where("electronic_payroll_id", $electronic_payroll_id);
        $q = $this->db->get("payroll_electronic_employee");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return TRUE;
    }

    public function get_items_by_electronic_payroll_employee_id($id)
    {
        $this->db->select("payroll_electronic_items.id,
            payroll_electronic_items.concept_type_id,
            payroll_electronic_items.concept_id,
            payroll_electronic_items.amount,
            payroll_electronic_items.earned_deduction,
            payroll_concepts.percentage,
            payroll_electronic_items.days_quantity,
            payroll_electronic_items.hours_quantity,
            payroll_concepts.name,
            payroll_concept_type.type,
            payroll_electronic_items.description,
            payroll_electronic_items.paid_to_employee");
        $this->db->join("sma_payroll_concepts", "sma_payroll_concepts.id = sma_payroll_electronic_items.concept_id", "inner");
        $this->db->join("payroll_concept_type", "payroll_concept_type.id = payroll_electronic_items.concept_type_id", "inner");
        $this->db->where("electronic_payroll_employee_id", $id);
        $this->db->order_by("payroll_concept_type.type", "asc");
        $this->db->order_by("sma_payroll_concepts.order_json", "asc");
        $this->db->order_by("sma_payroll_concepts.id", "asc");
        $q = $this->db->get("payroll_electronic_items");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_employee_items($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("payroll_electronic_employee");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_company_by_id($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("companies");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_state_data($state_name)
    {
        $this->db->WHERE("DEPARTAMENTO LIKE '$state_name%'");
        $q = $this->db->get("states");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_city_data($state_name, $city_name)
    {
        $this->db->like("DEPARTAMENTO", $state_name);
        $this->db->where("DESCRIPCION LIKE '%$city_name'");
        $q = $this->db->get("cities");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_adjustment_document_by_electronic_payroll_id($electronic_payroll_id)
    {
        $this->db->select("payroll_electronic_employee.*");
        $this->db->join("payroll_electronic_employee", "sma_payroll_electronic_employee.id = payroll_adjustment_documents.electronic_payroll_adjustment_id", "INNER");
        $this->db->where("payroll_adjustment_documents.electronic_payroll_id", $electronic_payroll_id);
        $q = $this->db->get("payroll_adjustment_documents");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_support_documents_by_electronic_payroll_delete_id($electronic_payroll_id)
    {
        $this->db->select("payroll_electronic_employee.*");
        $this->db->join("payroll_electronic_employee", "sma_payroll_electronic_employee.id = payroll_adjustment_documents.electronic_payroll_id", "INNER");
        $this->db->where("payroll_adjustment_documents.electronic_payroll_delete_id", $electronic_payroll_id);
        $q = $this->db->get("payroll_adjustment_documents");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_support_documents_by_electronic_payroll_adjustment_id($electronic_payroll_id)
    {
        $this->db->select("payroll_electronic_employee.*");
        $this->db->join("payroll_electronic_employee", "sma_payroll_electronic_employee.id = payroll_adjustment_documents.electronic_payroll_id", "INNER");
        $this->db->where("payroll_adjustment_documents.electronic_payroll_adjustment_id", $electronic_payroll_id);
        $q = $this->db->get("payroll_adjustment_documents");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function insert_header($data)
    {
        if ($this->db->insert("payroll_electronic", $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function insert_employees($data, $type = SUPPORT_DOCUMENT)
    {
        if ($type == SUPPORT_DOCUMENT) {
            $document_type = $this->get_document_types_by_biller_id(43, $data["biller_id"]);
        } else if ($type == ADJUSTMENT_DOCUMENT) {
            $document_type = $this->get_document_types_by_biller_id(44, $data["biller_id"]);
        } else if ($type == ELIMINATION_DOCUMENT) {
            $document_type = $this->get_document_types_by_biller_id(45, $data["biller_id"]);
        }

        $reference_no = $this->site->getReferenceBiller($data["biller_id"], $document_type->document_type_id);
        $ref = explode("-", $reference_no);

        $data["reference_no"] = $reference_no;
        $data["document_type_id"] = $document_type->document_type_id;
        if ($this->db->insert("payroll_electronic_employee", $data)) {
            $last_query = $this->db->insert_id();
            $consecutive = $ref[1] + 1;

            $this->site->updateBillerConsecutive($document_type->document_type_id, $consecutive);
            return $last_query;
        }

        return FALSE;
    }

    public function get_document_types_by_biller_id($module, $biller_id)
    {
        $this->db->join('documents_types', 'documents_types.id = biller_documents_types.document_type_id');
        $this->db->where("documents_types.module", $module);
        $this->db->where("biller_documents_types.biller_id", $biller_id);
        $this->db->order_by("documents_types.sales_prefix ASC, documents_types.factura_electronica ASC");
        $q = $this->db->get("biller_documents_types");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function insert_items($data)
    {
        if ($this->db->insert_batch("payroll_electronic_items", $data) > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function insert_documents_adjustment($data)
    {
        if ($this->db->insert("payroll_adjustment_documents", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("payroll_electronic", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_employee_items($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("payroll_electronic_employee", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_document_type($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("documents_types", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($id)
    {
        $this->db->where("id", $id);
        if ($this->db->delete("payroll_electronic")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_electronic_payroll_employee_by_electronic_payroll_id($electronic_payroll_id)
    {
        $this->db->where("electronic_payroll_id", $electronic_payroll_id);
        if ($this->db->delete("payroll_electronic_employee")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_electronic_payroll_items_by_electronic_payroll_employee_ids($data)
    {
        $this->db->where_in("electronic_payroll_employee_id", $data);
        $this->db->delete("payroll_electronic_items");
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_relationships_between_documents($data)
    {
        $this->db->where_in("electronic_payroll_id", $data);
        $this->db->delete("payroll_adjustment_documents");
        if ($this->db->affected_rows() > 0) {
            return TRUE;
        }
        return FALSE;
    }
}

/* End of file Payroll_electronic_model.php */
/* Location: ./app/models/Payroll_electronic_model.php */