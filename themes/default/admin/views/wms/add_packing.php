<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
    $picking_notices_html = "<select class='form-control' name='picking_notice[]'><option value=''>".lang('select')."</option>";
    if ($picking_notices) {
        foreach ($picking_notices as $pk_nt) {
            $picking_notices_html .= "<option value='".$pk_nt->id."'>".$pk_nt->text."</option>";
        }
    }
    $picking_notices_html .= "</select>";
 ?>
 <style type="text/css">
     
        .packing_box{
            border-style: solid;
            border-width: 1px;
            border-radius: 5px;
            border-color: #ced0d2;
            padding-top: 10px;
            padding-bottom: 10px;
        }

 </style>
<script>
    var warehouses_data = JSON.parse('<?= json_encode($warehouses) ?>');
    var picking_notices_html = "<?= $picking_notices_html ?>";
    $(document).ready(function(){
        $("#wmspaos_date").datetimepicker({
            format: site.dateFormats.js_ldate,
            fontAwesome: true,
            language: 'sma',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0
        }).datetimepicker('update', new Date());

        $(document).on('change', '#wmspabiller', function (e) {
        $.ajax({
                url:'<?= admin_url("billers/getBillersDocumentTypes/42/") ?>'+$('#wmspabiller').val(),
                type:'get',
                dataType:'JSON'
            }).done(function(data){
                response = data;
                $('#document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
                }
                if (response.status == 0) {
                    $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                    localStorage.setItem('locked_for_biller_resolution', 1);
                }
                $('#document_type_id').trigger('change');
            });
            localStorage.setItem('wmspabiller', $('#wmspabiller').val());
        });
    });

</script>
<?= admin_form_open('wms/add_packing', 'id="picking_form"') ?>
<!-- Header -->
<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $page_title ?></h2>
    </div>
</div>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <?= lang("date", "wmspaos_date"); ?>
                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="wmspaos_date" required="required"'); ?>
                            </div>
                        </div>
                        <?php
                            $bl[""] = lang('select');
                            $bldata = [];
                            foreach ($billers as $biller) {
                                $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                $bldata[$biller->id] = $biller;
                            }
                        ?>
                        <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                            <div class="col-md-4 col-xs-12">
                                <div class="form-group">
                                    <?= lang("biller", "wmspabiller"); ?>
                                    <select name="biller" class="form-control" id="wmspabiller" required>
                                        <option value=""><?= lang('select') ?></option>
                                        <?php foreach ($billers as $biller): ?>
                                          <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="col-md-4 col-xs-12" style="display: none;">
                                <div class="form-group">
                                    <?= lang("biller", "wmspabiller"); ?>
                                    <select name="biller" class="form-control" id="wmspabiller">
                                        <?php if (isset($bldata[$this->session->userdata('biller_id')])):
                                            $biller = $bldata[$this->session->userdata('biller_id')];
                                            ?>
                                            <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                        <?php endif ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="col-md-4">
                            <div class="form-group">
                                <?= lang("reference_no", "slref"); ?>
                                <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <?= lang("picking_reference_no", "wmspapicking_reference"); ?>
                                <?= form_input('picking_reference', (isset($_POST['picking_reference']) ? $_POST['picking_reference'] : ""), 'id="wmspapicking_reference" data-placeholder="' . lang("select") . ' ' . lang("picking_reference_no") . '" required="required" class="form-control input-tip"'); ?>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="form-group">
                                <?= lang("customer", "wmspacustomer"); ?>
                                <?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="wmspacustomer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip"'); ?>
                                <em class="text-danger txt-error" style="display: none;"></em>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <!-- <div class="row">
                        <div class="col-md-8">
                            <label>Verificar producto</label>
                            <input type='text' class='form-control verify_product' placeholder='Ingrese código a verificar'>
                        </div>
                        <div class="col-md-4">
                            <label>Caja de empacado</label>
                            <select class="form-control verify_product_box">
                                
                            </select>
                        </div>
                    </div> -->
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary form-control add_new_box" type="button"> <i class="fa fa-plus"></i> <?= lang('add_packing_box') ?></button>
                        </div>
                        <br>
                        <br>
                        <br>
                        <div class="col-md-12 div_packing_boxes row">
                        </div>
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" id="set_picking"><?= lang('send') ?></button>
                            <button type="button" class="btn btn-danger" id="reset_picking"><?= lang('reset') ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalSetPicking" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
            <span class="sr-only"><?=lang('close');?></span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Selección de productos</h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <table class="table table-condensed table-bordered table-hover" id="tabla_set_picking">
                <thead>
                    <tr>
                        <th><?= lang('product_code') ?></th>
                        <th><?= lang('variant') ?></th>
                        <th><?= lang('required_quantity') ?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <table class="table table-condensed table-bordered table-hover" id="tabla_set_novedades" style="display:none;">
                <thead>
                    <tr>
                        <th><?= lang('warehouse') ?></th>
                        <th><?= lang('product_code') ?></th>
                        <th><?= lang('quantity_notice') ?></th>
                        <th><?= lang('picking_notice') ?></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
            <div class="col-md-12">
                <div class="form-group">
                    <?= lang("packing_warehouse", "wms_packing_warehouse"); ?>
                    <?php
                    $wh[''] = lang('select');
                    if ($packing_warehouses) {
                        foreach ($packing_warehouses as $warehouse) {
                            $wh[$warehouse->id] = $warehouse->name;
                        }
                    }
                    echo form_dropdown('packing_warehouse', $wh, '', 'id="wms_packing_warehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" style="width:100%;" ');
                    ?>
                </div>
            </div>
            <div class="col-md-12">
                <button type="button" class="btn btn-success" id="send_picking"><?= lang('send') ?></button>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /Body -->
<?= form_close() ?>