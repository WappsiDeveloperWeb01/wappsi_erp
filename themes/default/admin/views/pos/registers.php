<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="box no-print">

    <div class="box-content">
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <td><?= lang('user'); ?></td>
                        <td><?= lang('opened_at'); ?></td>
                        <td><?= lang('cash_in_hand'); ?></td>
                        <td width="100px;"><?= lang('actions'); ?></td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    if (!empty($registers)) {
                        foreach ($registers as $register) {
                            echo '<tr>';
                            echo '<td>' . $register->user . '</td><td>' . $this->sma->hrld($register->date) . '</td><td>' . $register->cash_in_hand . '</td>';
                            echo '<td class="text-center" width="50px;">
                                    <a class="btn btn-danger new-button btn-sm" style="padding: 12px" href="' . admin_url('pos/close_register/' . $register->user_id.'/'.$register->id) . '/1" data-target="#myModal" data-toggle="modal" data-toggle-second="tooltip" data-placement="top" title="' . lang('close_register') . '">
                                        <i class="fas fa-times fa-lg" style="font-size: 1.75em"></i>
                                    </a>
                                </td>';
                            echo '</tr>';
                        }
                    } else {
                        echo '<tr>';
                        echo '<td colspan="4">' . lang('all_registers_are_closed') . '</td>';
                        echo '</tr>';
                    }
                    ?>
                    </tbody>
                </table>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('[data-toggle-second="tooltip"]').tooltip();
    });
</script>