<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * Last edited:
 * 8th June 2019
 *
 * Version: 2019.01
 */

$lang['edit_return']                        = "Editar nota crédito";
$lang['delete_return']                      = "Eliminar nota crédito";
$lang['return_sale']                        = "nota crédito de venta";
$lang['return_note']                        = "Nota de nota crédito";
$lang['staff_note']                         = "Nota interna";
$lang['you_will_loss_return_data']          = "Se perderán los datos de la actual nota crédito";
$lang['delete_returns']                     = "Eliminar notas créditos";
$lang['staff_note']                         = "Nota interna";
$lang['return_added']                       = "La nota crédito se ha agregado correctamente";
$lang['return_updated']                     = "La nota crédito se ha actualizado correctamente";
$lang['return_deleted']                     = "La nota crédito se ha eliminado correctamente";
$lang['return_x_edited_older_than_x_days']  = "Las notas créditos de más de %d días no pueden ser editadas";
