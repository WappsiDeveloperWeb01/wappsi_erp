<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    var max_qpr = "<?= isset($max_qpr) ? $max_qpr : 0 ?>"
    var close_year_adjustment = JSON.parse('<?= isset($close_year_adjustment) ? "true" : "false"; ?>');
    if (close_year_adjustment) {
        localStorage.setItem('adjustmet_type', '1');
    }
    $(document).ready(function () {
        if (localStorage.getItem('remove_qals')) {
            if (localStorage.getItem('qaitems')) {
                localStorage.removeItem('qaitems');
            }
            if (localStorage.getItem('qaref')) {
                localStorage.removeItem('qaref');
            }
            if (localStorage.getItem('qawarehouse')) {
                localStorage.removeItem('qawarehouse');
            }
            if (localStorage.getItem('qanote')) {
                localStorage.removeItem('qanote');
            }
            if (localStorage.getItem('qadate')) {
                localStorage.removeItem('qadate');
            }
            localStorage.removeItem('remove_qals');
        }

        <?php if ($adjustment_items) { ?>
        localStorage.setItem('qaitems', JSON.stringify(<?= $adjustment_items; ?>));
        <?php } ?>

        setTimeout(function() {

            <?php if ($warehouse_id) { ?>
            localStorage.setItem('qawarehouse', '<?= $warehouse_id; ?>');
            $('#qawarehouse').select2('val', '<?= $warehouse_id; ?>').select2('readonly', true);
            <?php } ?>

            <?php if (isset($cy_warehouse_id)) { ?>
            localStorage.setItem('qawarehouse', '<?= $cy_warehouse_id; ?>');
            $('#qawarehouse').select2('val', '<?= $cy_warehouse_id; ?>').select2('readonly', true);
            <?php } ?>

        }, 850);



        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('qadate')) {
            $("#qadate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#qadate', function (e) {
            localStorage.setItem('qadate', $(this).val());
        });
        if (qadate = localStorage.getItem('qadate')) {
            $('#qadate').val(qadate);
        }
        <?php } ?>

        $("#add_item").autocomplete({
            source: function (request, response) {
                if (!$('#document_type_id').val() || !$('#companies_id').val() || !$('#adjustmet_type').val()) {
                    var msg = "";

                    if (!$('#document_type_id').val()) {
                        msg += "</br><?= lang('reference_no') ?>";
                    }

                    if (!$('#companies_id').val()) {
                        msg += "</br><?= lang('third') ?>";
                    }

                    if (!$('#adjustmet_type').val()) {
                        msg += "</br><?= lang('adjustmet_type') ?>";
                    }

                    $('#add_item').val('').removeClass('ui-autocomplete-loading');


                    command: toastr.warning('<?=lang('select_above');?> : '+msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    $('#add_item').focus();
                    return false;
                }


                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('products/qa_suggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        biller_id: $('#qabiller').val(),
                        warehouse_id: $('#qawarehouse').val(),
                    },
                    success: function (data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });

            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {

                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item").val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $("#add_item").val('');
                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {

                    $.ajax({
                        url:"<?= admin_url('products/getProductAVGCostByWarehouse'); ?>/"+ui.item.item_id+"/"+$('#qawarehouse').val(),
                    }).done(function(data){

                        if (data != false) {
                            ui.item.row.price = data;
                        }
                        var row = add_adjustment_item(ui.item);
                        if (row){
                            $("#add_item").val('');
                        }

                    }).fail(function(data){
                        console.log(data);
                    });

                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
    });

    function getProductAVGCostByWarehouse(product_id, warehouse_id){

    }

    $(document).on('change', '#qabiller', function (e) {
        $.ajax({
            url:'<?= admin_url("billers/getBillersDocumentTypes/11/") ?>'+$('#qabiller').val(),
            type:'get',
            dataType:'JSON'
        }).done(function(data){
            response = data;
            $('#document_type_id').html(response.options).select2();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "6000",
                "extendedTimeOut": "1000",
                });
            }
            if (response.status == 0) {
                $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                localStorage.setItem('locked_for_biller_resolution', 1);
            }
            $('#document_type_id').trigger('change');
        });
        localStorage.setItem('qabiller', $('#qabiller').val());
        $('#qawarehouse').select2('val', $('#qabiller option:selected').data('warehousedefault')).trigger('change');
    });


</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            $attrib = array('data-toggle' => 'validator', 'role' => 'form', 'id' => 'add_adjustment');
                            echo admin_form_open_multipart("products/add_adjustment", $attrib);
                            ?>
                            <div class="row">
                                <?php if ($Owner || $Admin) { ?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <?= lang("date", "qadate"); ?>
                                            <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="qadate" required="required"'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php
                                    $bl[""] = "";
                                    $bldata = [];
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        $bldata[$biller->id] = $biller;
                                    }
                                ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <?= lang("biller", "qabiller"); ?>
                                            <select name="biller" class="form-control" id="qabiller" required="required">
                                                <?php foreach ($billers as $biller): ?>
                                                  <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <?= lang("biller", "qabiller"); ?>
                                            <select name="biller" class="form-control" id="qabiller">
                                                <?php if (isset($bldata[$this->session->userdata('biller_id')])):

                                                    $biller = $bldata[$this->session->userdata('biller_id')];

                                                    ?>
                                                    <option value="<?= $biller->id ?>" data-customerdefault="<?= $biller->default_customer_id ?>" data-warehousedefault="<?= $biller->default_warehouse_id ?>" data-pricegroupdefault="<?= $biller->default_price_group ?>" data-sellerdefault="<?= $biller->default_seller_id ?>"><?= $biller->company ?></option>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php } ?>

                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?= lang("reference_no", "document_type_id"); ?>
                                        <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?= form_hidden('count_id', $count_id); ?>
                                <?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) { ?>
                                    <div class="col-md-4 col-sm-4">
                                        <div class="form-group">
                                            <?= lang("warehouse", "qawarehouse"); ?>
                                            <?php
                                            $wh[''] = '';
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ($warehouse_id ? $warehouse_id :$Settings->default_warehouse)), 'id="qawarehouse" class="form-control input-tip select" data-placeholder="' . lang("select") . ' ' . lang("warehouse") . '" required="required" '.($warehouse_id ? 'readonly' : '').' style="width:100%;"');
                                            ?>
                                        </div>
                                    </div>
                                    <?php } else {
                                        $warehouse_input = array(
                                            'type' => 'hidden',
                                            'name' => 'warehouse',
                                            'id' => 'qawarehouse',
                                            'value' => $this->session->userdata('warehouse_id'),
                                            );

                                        echo form_input($warehouse_input);
                                    } ?>
                                <div class="col-md-4 col-sm-4">
                                    <div class="form-group">
                                        <?= lang("document", "document") ?>
                                        <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                            data-show-preview="false" class="form-control file">
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= lang('third', 'companies_id') ?>
                                    <?php
                                    $cOptions[''] = lang('select_third');
                                    foreach ($companies as $row => $company) {
                                        $cOptions[$company->id] = $company->name;
                                    }

                                     ?>
                                    <?= form_dropdown('companies_id', $cOptions, '',' id="companies_id" class="form-control select" required style="width:100%;"'); ?>
                                </div>
                            </div>
                            <div class="row">
                                <?php if (isset($cost_centers)): ?>
                                    <div class="col-md-4 col-sm-4 form-group">
                                        <?= lang('cost_center', 'cost_center_id') ?>
                                        <?php
                                        $ccopts[''] = lang('select');
                                        foreach ($cost_centers as $cost_center) {
                                            $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                                        }
                                         ?>
                                         <?= form_dropdown('cost_center_id', $ccopts, '', 'id="cost_center_id" class="form-control input-tip select" required="required" style="width:100%;" '); ?>
                                    </div>
                                <?php endif ?>

                                <div class="col-md-4 col-sm-4 form-group">
                                    <?= lang('adjustmet_type', 'adjustmet_type') ?>
                                    <select id="adjustmet_type" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <option value="1"><?= lang('entry') ?></option>
                                        <option value="2"><?= lang('output') ?></option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">


                                <div class="clearfix"></div>


                                <div class="col-md-12 col-sm-12" id="sticker">
                                    <div class="form-group">
                                            <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . lang("search_product_by_name_code") . '"'); ?>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12" style="overflow-x: auto;">
                                    <div class="form-group">
                                    <!-- <div class="col-md-12 col-sm-12" style="overflow-x: auto; padding-right: 0px !important; padding-left: 0px !important; margin-right: 15px; margin-left: 15px;"> -->
                                        <label class="table-label"><?= lang("products"); ?> *</label>
                                        <table id="qaTable" class="table items  table-bordered table-condensed table-hover">
                                            <thead>
                                            <tr>
                                                <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                <th class=""><?= lang("variant"); ?></th>
                                                <th class=""><?= lang("type"); ?></th>
                                                <?php $restrict_permission_cost = ($Admin || $Owner || $GP['products-cost'] == 1) ? '' : 'display:none;' ?>
                                                <th class="" style="<?= $restrict_permission_cost ?>"><span class="avg_cost"><?= lang("average_cost")." ".lang('with_tax'); ?></span><span class="cost" style="display:none;"><?= lang('cost')." ".lang('without_tax') ?></span></th>
                                                <th class="" style="<?= $restrict_permission_cost ?>"><span class="avg_cost"><?= lang("average_cost")." ".lang('with_tax'); ?></span><span class="cost" style="display:none;"><?= lang('cost')." ".lang('with_tax') ?></span></th>
                                                <th class=""><?= lang("quantity"); ?></th>
                                                <th class=""><?= lang("total"); ?></th>
                                                <?php
                                                if ($Settings->product_serial) {
                                                    echo '<th class="">' . lang("serial_no") . '</th>';
                                                }
                                                ?>
                                                <th style="max-width: 30px !important; text-align: center;">
                                                    <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <?= lang("note", "qanote"); ?>
                                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qanote" style="margin-top: 10px; height: 100px;"'); ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                <div class="col-md-12">
                                    <div id="" class="well well-sm" style="margin-bottom: 0;">
                                        <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                            <tr class="warning">
                                            <td><span class="totals_val text-left" id="titems">0</span></td>
                                        </table>
                                    </div>
                                </div>

                                <div class="col-md-12 col-sm-12">
                                    <div
                                        class="fprom-group"><?php echo form_submit('add_adjustment', lang("submit"), 'id="add_adjustment_submit" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                        <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></div>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="product_name_serialModal"></h2>
                <h3><?= lang('ticket_data') ?></h3>
            </div>
            <div class="modal-body">
                <form id="serialModal_form">
                    <input type="hidden" id="serialModal_product_id">
                    <input type="hidden" id="serialModal_option_id">
                    <div class="col-sm-12 form-group">
                        <?= lang('serial_no', 'serial') ?>
                        <div class="input-group">
                            <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                            <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;">
                                <i class="fa fa-random"></i>
                            </span>
                        </div>
                    </div>
                    <div class="col-sm-12 form-group">
                        <?= lang('meters', 'meters') ?>
                        <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                    </div>
                </form>
            </div>

            <div class="modal-footer">
                <div class="col-sm-12 form-group">
                    <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                    <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).on('submit', '#add_adjustment', function(e){
        if (!$('#companies_id').val()) {
            $('#companies_id').select2('open');
            $('#add_adjustment_submit').prop('disabled', false);
            e.preventDefault();
        }

        if (!$('#document_type_id').val()) {
            $('#document_type_id').select2('open');
            $('#add_adjustment_submit').prop('disabled', false);
            e.preventDefault();
        }


    });

</script>