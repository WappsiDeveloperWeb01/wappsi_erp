<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<script type="text/javascript">
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
    var pexppayment_reference_no;
    var pexppayment_method;
    var biller;
    var filter_user;
    var supplier;
    var category;
    var filtered = 0;
    var filtered_ini_date;

    if (localStorage.getItem('pexp_filter_filtered_ini_date')) {
        filtered_ini_date = localStorage.getItem('pexp_filter_filtered_ini_date');
    }
    <?php if (isset($_POST['start_date'])) : ?>
        localStorage.setItem('pexp_filter_start_date', '<?= $_POST['start_date'] ?>');
        start_date = '<?= $_POST['start_date'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_start_date')) {
            start_date = localStorage.getItem('pexp_filter_start_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['end_date'])) : ?>
        localStorage.setItem('pexp_filter_end_date', '<?= $_POST['end_date'] ?>');
        end_date = '<?= $_POST['end_date'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_end_date')) {
            end_date = localStorage.getItem('pexp_filter_end_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['category'])) : ?>
        localStorage.setItem('pexp_filter_category', '<?= $_POST['category'] ?>');
        category = '<?= $_POST['category'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_category')) {
            category = localStorage.getItem('pexp_filter_category');
        }
    <?php endif ?>

    <?php if (isset($_POST['pexppayment_reference_no'])) : ?>
        localStorage.setItem('pexp_filter_pexppayment_reference_no', '<?= $_POST['pexppayment_reference_no'] ?>');
        pexppayment_reference_no = '<?= $_POST['pexppayment_reference_no'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_pexppayment_reference_no')) {
            ppayment_reference_no = localStorage.getItem('pexp_filter_ppayment_reference_no');
        }
    <?php endif ?>

    <?php if (isset($_POST['pexppayment_method'])) : ?>
        localStorage.setItem('pexp_filter_pexppayment_method', '<?= $_POST['pexppayment_method'] ?>');
        pexppayment_method = '<?= $_POST['pexppayment_method'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_pexppayment_method')) {
            pexppayment_method = localStorage.getItem('pexp_filter_pexppayment_method');
        }
    <?php endif ?>

    <?php if (isset($_POST['biller'])) : ?>
        localStorage.setItem('pexp_filter_biller', '<?= $_POST['biller'] ?>');
        biller = '<?= $_POST['biller'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_biller')) {
            biller = localStorage.getItem('pexp_filter_biller');
        }
    <?php endif ?>

    <?php if (isset($_POST['supplier'])) : ?>
        localStorage.setItem('pexp_filter_supplier', '<?= $_POST['supplier'] ?>');
        supplier = '<?= $_POST['supplier'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_supplier')) {
            supplier = localStorage.getItem('pexp_filter_supplier');
        }
    <?php endif ?>

    <?php if (isset($_POST['filtered'])) : ?>
        localStorage.setItem('pexp_filter_filtered', '<?= $_POST['filtered'] ?>');
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            localStorage.setItem('pexp_filter_filtered_ini_date', '<?= date("Y-m-d H:i:s") ?>');
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_filtered')) {
            filtered = localStorage.getItem('pexp_filter_filtered');
        }
    <?php endif ?>

    <?php if (isset($_POST['filter_user'])) : ?>
        localStorage.setItem('pexp_filter_filter_user', '<?= $_POST['filter_user'] ?>');
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pexp_filter_filter_user')) {
            filter_user = localStorage.getItem('pexp_filter_filter_user');
        }
    <?php endif ?>

    <?php if (isset($_POST['date_records_filter'])) : ?>
        localStorage.setItem('pexp_filter_date_records_filter', '<?= $_POST['date_records_filter'] ?>');
    <?php endif ?>


    $(document).ready(function() {
        function attachment(x) {
            if (x != null) {
                return '<a href="' + site.url + 'assets/uploads/' + x + '" target="_blank"><i class="fa fa-chain"></i></a>';
            }
            return x;
        }

        oTable = $('#EXPData').dataTable({
            "aaSorting": [
                [1, "desc"]
            ],
            "aLengthMenu": [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('purchases/getExpenses'); ?>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "start_date",
                    "value": start_date
                }, {
                    "name": "end_date",
                    "value": end_date
                }, {
                    "name": "pexppayment_reference_no",
                    "value": pexppayment_reference_no
                }, {
                    "name": "pexppayment_method",
                    "value": pexppayment_method
                }, {
                    "name": "biller",
                    "value": biller
                }, {
                    "name": "supplier",
                    "value": supplier
                }, {
                    "name": "user",
                    "value": filter_user
                }, {
                    "name": "category",
                    "value": category
                });
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            "aoColumns": [{
                "bSortable": false,
                "mRender": checkbox
            }, {
                "mRender": fld
            }, null, null, {
                "mRender": currencyFormat
            }, null, null, {
                "bSortable": false,
                "mRender": attachment
            }, {
                "bSortable": false
            }],
            'fnRowCallback': function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "expense_link";
                return nRow;
            },
            "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {
                var total = 0;
                for (var i = 0; i < aaData.length; i++) {
                    total += parseFloat(aaData[aiDisplay[i]][4]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[4].innerHTML = currencyFormat(total);
            }
        }).fnSetFilteringDelay().dtFilter([{
                column_number: 1,
                filter_default_label: "[<?= lang('date'); ?> (yyyy-mm-dd)]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 2,
                filter_default_label: "[<?= lang('reference'); ?>]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 3,
                filter_default_label: "[<?= lang('category'); ?>]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 5,
                filter_default_label: "[<?= lang('note'); ?>]",
                filter_type: "text",
                data: []
            },
            {
                column_number: 6,
                filter_default_label: "[<?= lang('created_by'); ?>]",
                filter_type: "text",
                data: []
            },
        ], "footer");

    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight no-print">

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content" style="display: none;">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <?= admin_form_open('purchases/expenses', ['id' => 'ppayments_filter']) ?>
                                <div class="col-sm-4">
                                    <?= lang('reference_no', 'pexppayment_reference_no') ?>
                                    <select name="pexppayment_reference_no" id="pexppayment_reference_no" class="form-control">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php if ($documents_types) : ?>
                                            <?php foreach ($documents_types as $dt) : ?>
                                                <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['pexppayment_reference_no']) && $_POST['pexppayment_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre . " (" . $dt->sales_prefix . ")" ?></option>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </select>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                    <?php
                                    $biller_selected = '';
                                    $biller_readonly = false;
                                    if ($this->session->userdata('biller_id')) {
                                        $biller_selected = $this->session->userdata('biller_id');
                                        $biller_readonly = true;
                                    }

                                    $bl[""] = lang('select');
                                    foreach ($billers as $biller) {
                                        $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                    }
                                    echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                    ?>
                                </div>
                                <div class="col-sm-4">
                                    <label class="control-label" for="supplier"><?= lang("supplier"); ?></label>
                                    <?php
                                    echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'id="filter_supplier" data-placeholder="' . lang("select") . ' ' . lang("supplier") . '" class="form-control input-tip" style="width:100%;"');
                                    ?>
                                </div>
                                <?php if ($this->Owner || $this->Admin) : ?>
                                    <div class="col-sm-4">
                                        <label><?= lang('user') ?></label>
                                        <select name="filter_user" id="filter_user" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($users) : ?>
                                                <?php foreach ($users as $user) : ?>
                                                    <option value="<?= $user->id ?>"><?= $user->first_name . " " . $user->last_name ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                <?php endif ?>


                                <div class="col-sm-4 form-group">
                                    <?= lang("paying_by", "pexppayment_method"); ?>
                                    <select name="pexppayment_method" id="pexppayment_method" class="form-control">
                                        <?= $this->sma->paid_opts(null, false, true, true, true); ?>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <label class="control-label" for="category"><?= lang("category"); ?></label>
                                    <?php

                                    $bl[""] = lang('select');
                                    foreach ($categories as $category) {
                                        $bl[$category->id] = $category->name;
                                    }
                                    echo form_dropdown('category', $bl, (isset($_POST['category']) ? $_POST['category'] : ''), 'class="form-control" id="category" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("category") . '"');
                                    ?>
                                </div>
                                <hr class="col-sm-11">

                                <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>

                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                        <div class="col-sm-4 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <?= lang('start_date', 'start_date') ?>
                                        <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                    </div>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <?= lang('end_date', 'end_date') ?>
                                        <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control datetime">
                                    </div>
                                </div>

                                <div class="col-sm-12" style="margin-top: 2%;">
                                    <input type="hidden" name="filtered" value="1">
                                    <button type="submit" id="submit-purchases-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    <button type="button" id="submit-purchases-filter-clean" class="btn btn-danger"><span class="fa fa-times"></span> <?= lang('reset') ?></button>
                                </div>
                                <?= form_close() ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if ($Owner) {
        echo admin_form_open('purchases/expense_actions', 'id="action-form"');
    } ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('purchases/add_expense') ?>" data-toggle="modal" data-target="#myModal">
                                            <i class="fa fa-plus-circle"></i> <?= lang('add_expense') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="excel" data-action="reaccount">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('post_expense') ?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                    <?php if ($this->Owner) : ?>
                                        <li>
                                            <a href="#" class="bpo" title="<b><?= $this->lang->line("delete_expenses") ?></b>" data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>" data-html="true" data-placement="left">
                                                <i class="fa fa-trash-o"></i> <?= lang('delete_expenses') ?>
                                            </a>
                                        </li>
                                    <?php endif ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- <p class="introtext"><? //= lang('list_results');
                                                        ?></p> -->
                            <div class="table-responsive">
                                <table id="EXPData" cellpadding="0" cellspacing="0" border="0" class="table table-bordered table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th class="col-xs-2"><?= lang("date"); ?></th>
                                            <th class="col-xs-2"><?= lang("reference"); ?></th>
                                            <th class="col-xs-2"><?= lang("category"); ?></th>
                                            <th class="col-xs-1"><?= lang("amount"); ?></th>
                                            <th class="col-xs-3"><?= lang("note"); ?></th>
                                            <th class="col-xs-2"><?= lang("created_by"); ?></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i>
                                            </th>
                                            <th style="width:100px;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i>
                                            </th>
                                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script type="text/javascript">
    $(document).ready(function() {
        if (supplier != '') {
            $('#filter_supplier').val(supplier).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });

            $('#filter_supplier').trigger('change');
        } else {
            $('#filter_supplier').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "suppliers/getSupplier/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });
        }

        if (filtered !== undefined) {
            setTimeout(function() {
                $('#start_date_dh').val(start_date);
                $('#end_date_dh').val(end_date);
                $('#pexppayment_reference_no').select2('val', pexppayment_reference_no);
                $('#pexppayment_method').select2('val', pexppayment_method);
                $('#biller').select2('val', biller);
                $('#filter_user').select2('val', filter_user);
                // $('.collapse-link').click();
            }, 900);
        }


        if (filtered_ini_date !== undefined) {
            minutos = calcularMinutos(filtered_ini_date, '<?= date("Y-m-d H:i:s") ?>');
            if (minutos >= 10) {
                localStorage.removeItem('pexp_filter_start_date');
                localStorage.removeItem('pexp_filter_end_date');
                localStorage.removeItem('pexp_filter_category');
                localStorage.removeItem('pexp_filter_biller');
                localStorage.removeItem('pexp_filter_filter_user');
                localStorage.removeItem('pexp_filter_supplier');
                localStorage.removeItem('pexp_filter_pexppayment_reference_no');
                localStorage.removeItem('pexp_filter_pexppayment_method');
                localStorage.removeItem('pexp_filter_filtered');
                localStorage.removeItem('pexp_filter_filtered_ini_date');
                localStorage.removeItem('pexp_filter_date_records_filter');
                location.href = '<?= admin_url("purchases/expenses") ?>';
            }
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);


        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>

    });

    $(document).on('click', '#submit-purchases-filter-clean', function() {
        localStorage.removeItem('pexp_filter_start_date');
        localStorage.removeItem('pexp_filter_end_date');
        localStorage.removeItem('pexp_filter_category');
        localStorage.removeItem('pexp_filter_biller');
        localStorage.removeItem('pexp_filter_filter_user');
        localStorage.removeItem('pexp_filter_supplier');
        localStorage.removeItem('pexp_filter_pexppayment_reference_no');
        localStorage.removeItem('pexp_filter_pexppayment_method');
        localStorage.removeItem('pexp_filter_filtered');
        localStorage.removeItem('pexp_filter_filtered_ini_date');
        localStorage.removeItem('pexp_filter_date_records_filter');
        location.href = '<?= admin_url("purchases/expenses") ?>';
    });

    function calcularMinutos(start_date, end_date) {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff / (1000 * 60);
        return minutos;
    }

    function setFilterText() {

        var reference_text = $('#pexppayment_reference_no option:selected').data('dtprefix');
        var pexppayment_method_text = $('#pexppayment_method option:selected').text();
        var biller_text = $('#biller option:selected').text();
        var supplier_text = $('#filter_supplier').select2('data') !== null ? $('#filter_supplier').select2('data').text : '';
        var start_date_text = $('#start_date_dh').val();
        var end_date_text = $('#end_date_dh').val();
        var text = "Filtros configurados : ";

        coma = false;

        if (pexppayment_reference_no != '' && pexppayment_reference_no !== undefined) {
            text += " Tipo documento (" + reference_text + ")";
            coma = true;
        }
        if (pexppayment_method != '' && pexppayment_method !== undefined) {
            text += "Medio de pago (" + pexppayment_method_text + ")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text += coma ? "," : "";
            text += " Sucursal (" + biller_text + ")";
            coma = true;
        }
        if (supplier != '' && supplier !== undefined) {
            text += coma ? "," : "";
            text += " Cliente (" + supplier_text + ")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha de inicio (" + start_date_text + ")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha final (" + end_date_text + ")";
            coma = true;
        }

        $('.text_filter').html(text);

    }


    $(document).ready(function() {
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])) : ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            <?php elseif ($_POST['date_records_filter'] != $this->Settings->default_records_filter) : ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
</script>