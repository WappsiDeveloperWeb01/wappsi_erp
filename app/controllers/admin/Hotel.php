<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->load->library('form_validation');
        $this->load->admin_model('hotel_model');
        $this->load->admin_model('companies_model');
    }

    public function guest_registers($action = NULL)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('guest_registers')));
        $meta = array('page_title' => lang('guest_registers'), 'bc' => $bc);        
        $this->page_construct('hotel/guest_registers', $meta, $this->data);
    }

    public function get_guest_registers(){
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        guests_register.id as id,
                        customer.company,
                        reference_no,
                        date_in,
                        estimated_date_out,
                        date_out,
                        CONCAT(".$this->db->dbprefix('rooms').".room_no, ' - ', ".$this->db->dbprefix('rooms').".room_name) as room,
                        guests_no,
                        nights_no,
                        guests_register.status,
                        total
                    ")
            ->join('rooms', 'rooms.id = guests_register.room_id', 'left')
            ->join('companies as customer', 'customer.id = guests_register.customer_id', 'left')
            ->from('guests_register')
            ;
        $detail_link = anchor('admin/hotel/register_view/$1', '<i class="fa fa-file-text-o"></i> ' . lang('register_detail'), 'target="_blank"');
        $edit_link = anchor('admin/hotel/edit_guest_register/$1', '<i class="fa fa-pencil"></i> ' . lang('edit_guest_register'), '');
        $close_guest_register = anchor('admin/hotel/close_guest_register/$1', '<i class="fas fa-sign-out-alt"></i> ' . lang('close_guest_register'), 'data-toggle="modal" data-target="#myModal"');
        $action = '<div class="text-center"><div class="btn-group text-left">'
                        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                        . lang('actions') . ' <span class="caret"></span></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                '.($this->Owner || $this->Admin || $this->GP['hotel-edit_guest_register'] ? '<li>' . $edit_link . '</li>' : '').'
                                <li>' . $detail_link . '</li>
                                '.($this->Owner || $this->Admin || $this->GP['hotel-close_guest_register'] ? '<li>' . $close_guest_register . '</li>' : '').'
                            </ul>
                        </div></div>';
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function add_guest_register()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('date', lang("date"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('room', lang("room"), 'required');
        $this->form_validation->set_rules('guests_no', lang("guests_no"), 'required');
        $this->form_validation->set_rules('nights_no', lang("nights_no"), 'required');
        $this->form_validation->set_rules('total', lang("total"), 'required');
        $this->form_validation->set_rules('order_sale_document_type_id', lang("order_sale_document_type_id"), 'required');
        if ($this->form_validation->run() == true) {
            $date = $this->sma->fld($this->input->post('date'));
            $biller = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $customer = $this->input->post('customer');
            $room = $this->input->post('room');
            $guests_no = $this->input->post('guests_no');
            $nights_no = $this->input->post('nights_no');
            $total = $this->input->post('total');
            $order_sale_document_type_id = $this->input->post('order_sale_document_type_id');
            $estimated_date_out = date("Y-m-d",strtotime($date."+ ".$nights_no." days"));
            $estimated_date_out = $estimated_date_out." ".date('H:i:s');
            $data = [
                'date_in' => $date,
                'customer_id' => $customer,
                'biller_id' => $biller,
                'room_id' => $room,
                'guests_no' => $guests_no,
                'nights_no' => $nights_no,
                'document_type_id' => $document_type_id,
                'created_by' => $this->session->userdata('user_id'),
                'total' => $total,
                'order_sale_document_type_id' => $order_sale_document_type_id,
                'estimated_date_out' => $estimated_date_out,
                // 'date_out' => $,
                'status' => 1,
            ];

            $guest_document_type = $this->input->post('document_type');
            $guest_vat_no = $this->input->post('vat_no');
            $guest_names = $this->input->post('names');
            $guest_surnames = $this->input->post('surnames');
            $guest_nationality = $this->input->post('nationality');
            $guest_phone = $this->input->post('phone');
            $guest_email = $this->input->post('email');
            $guest_birth_date = $this->input->post('birth_date');
            $guest_action = $this->input->post('action');
            $guest_guest_id = $this->input->post('guest_id');

            $rows = [];

            for ($i=0; $i < count($guest_vat_no) ; $i++) {
                if (!empty($guest_vat_no[$i])) {
                    $row = [
                        'document_type_id' => $guest_document_type[$i],
                        'vat_no' => $guest_vat_no[$i],
                        'names' => $guest_names[$i],
                        'surnames' => $guest_surnames[$i],
                        'nationality' => $guest_nationality[$i],
                        'phone' => $guest_phone[$i],
                        'email' => $guest_email[$i],
                        'birth_date' => $guest_birth_date[$i],
                        'action' => $guest_action[$i],
                        'guest_id' => $guest_guest_id[$i],
                    ];
                    $rows[] = $row;
                }
            }

            if (empty($rows)) {
                $this->session->set_flashdata('error', 'Registro sin huespedes');
                admin_redirect("hotel/add_guest_register");
            }
            // $this->sma->print_arrays($data, $rows);
        }

        if ($this->form_validation->run() == true && $this->hotel_model->add_guest_register($data, $rows)) {
            $this->session->set_userdata('remove_gstls', 1);
            $this->session->set_flashdata('message', 'Registro creado con éxito');
            admin_redirect("hotel/guest_registers");
        } else {
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['rooms'] = $this->hotel_model->get_available_rooms();
            $dts = $this->companies_model->getIDDocumentTypes();
            foreach ($dts as $key => $dt) {
                $dts[$key]->nombre = lang($dt->nombre);
            }
            $this->data['id_document_types'] = $dts;
            $this->data['reference'] = $this->Settings->sales_prefix."-";
            $this->data['slnumber'] = '';
            $this->data['payment_ref'] = '';
            $this->data['prioridad_por_unidad'] = ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? true : false;
            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            $this->data['brands'] = $this->site->getAllBrands();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('hotel'), 'page' => lang('hotel')), array('link' => '#', 'page' => lang('add_guest_register')));
            $meta = array('page_title' => lang('add_guest_register'), 'bc' => $bc);
            $this->page_construct('hotel/add_guest_register', $meta, $this->data);
        }
    }

    public function edit_guest_register($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('date', lang("date"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('room', lang("room"), 'required');
        $this->form_validation->set_rules('guests_no', lang("guests_no"), 'required');
        $this->form_validation->set_rules('nights_no', lang("nights_no"), 'required');
        $this->form_validation->set_rules('total', lang("total"), 'required');
        $this->form_validation->set_rules('order_sale_document_type_id', lang("order_sale_document_type_id"), 'required');
        $inv = $this->hotel_model->get_guests_register($id);
        $rows = $this->hotel_model->get_guests_register_detail($id);
        $this->data['id'] = $id;
        $this->data['inv'] = $inv;
        $this->data['rows'] = $rows;

        if ($inv->status == 0) {
            $this->session->set_flashdata('error', 'El registro no se puede editar');
            admin_redirect("hotel/guest_registers");
        }
        // $this->sma->print_arrays($inv, $rows);
        if ($this->form_validation->run() == true) {
            $date = $this->sma->fld($this->input->post('date'));
            $biller = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $customer = $this->input->post('customer');
            $room = $this->input->post('room');
            $guests_no = $this->input->post('guests_no');
            $nights_no = $this->input->post('nights_no');
            $total = $this->input->post('total');
            $order_sale_document_type_id = $this->input->post('order_sale_document_type_id');
            $estimated_date_out = date("Y-m-d",strtotime($date."+ ".$nights_no." days"));
            $estimated_date_out = $estimated_date_out." ".date('H:i:s');
            $data = [
                'date_in' => $date,
                'customer_id' => $customer,
                'biller_id' => $biller,
                'room_id' => $room,
                'guests_no' => $guests_no,
                'nights_no' => $nights_no,
                'document_type_id' => $document_type_id,
                'created_by' => $this->session->userdata('user_id'),
                'total' => $total,
                'order_sale_document_type_id' => $order_sale_document_type_id,
                'estimated_date_out' => $estimated_date_out,
                'status' => 1,
            ];
            $guest_document_type = $this->input->post('document_type');
            $guest_vat_no = $this->input->post('vat_no');
            $guest_names = $this->input->post('names');
            $guest_surnames = $this->input->post('surnames');
            $guest_nationality = $this->input->post('nationality');
            $guest_phone = $this->input->post('phone');
            $guest_email = $this->input->post('email');
            $guest_birth_date = $this->input->post('birth_date');
            $guest_action = $this->input->post('action');
            $guest_guest_id = $this->input->post('guest_id');
            $rows = [];
            for ($i=0; $i < count($guest_vat_no) ; $i++) {
                if (!empty($guest_vat_no[$i])) {
                    $row = [
                        'document_type_id' => $guest_document_type[$i],
                        'vat_no' => $guest_vat_no[$i],
                        'names' => $guest_names[$i],
                        'surnames' => $guest_surnames[$i],
                        'nationality' => $guest_nationality[$i],
                        'phone' => $guest_phone[$i],
                        'email' => $guest_email[$i],
                        'birth_date' => $guest_birth_date[$i],
                        'action' => $guest_action[$i],
                        'guest_id' => $guest_guest_id[$i],
                    ];
                    $rows[] = $row;
                }
            }
            if (empty($rows)) {
                $this->session->set_flashdata('error', 'Registro sin huespedes');
                admin_redirect("hotel/add_guest_register");
            }
            // $this->sma->print_arrays($data, $rows);
        }
        if ($this->form_validation->run() == true && $this->hotel_model->update_guest_register($id, $data, $rows)) {
            $this->session->set_userdata('remove_gstls', 1);
            $this->session->set_flashdata('message', 'Registro actualizado con éxito');
            admin_redirect("hotel/guest_registers");
        } else {
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['rooms'] = $this->hotel_model->get_available_rooms($inv->room_id);
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->data['reference'] = $this->Settings->sales_prefix."-";
            $this->data['slnumber'] = '';
            $this->data['payment_ref'] = '';
            $this->data['prioridad_por_unidad'] = ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? true : false;
            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            $this->data['brands'] = $this->site->getAllBrands();
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('hotel'), 'page' => lang('hotel')), array('link' => '#', 'page' => lang('edit_guest_register')));
            $meta = array('page_title' => lang('edit_guest_register'), 'bc' => $bc);
            $this->page_construct('hotel/edit_guest_register', $meta, $this->data);
        }
    }

    public function validate_existing_guest(){
        $document_type = $this->input->get('document_type');
        $vat_no = $this->input->get('vat_no');
        $q = $this->db->get_where('guests', ['document_type_id'=>$document_type, 'vat_no'=>$vat_no]);
        if ($q->num_rows() > 0) {
            $q = $q->row();
            $q->birth_date = date('Y-m-d', strtotime($q->birth_date));
            echo json_encode(['response'=>true, 'data'=>$q]);
        } else {
            echo json_encode(['response'=>false]);
        }
    }

    public function guest_register_modal_view($id = null)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->hotel_model->get_guests_register($id);
        $this->data['inv'] = $inv;
        $this->data['rows'] =  $this->hotel_model->get_guests_register_detail($id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->load_view($this->theme . 'hotel/guest_register_modal_view', $this->data);
    }

    public function register_view($id = null)
    {
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->load->library('qr_code');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->hotel_model->get_guests_register($id);
        $document_type = $this->site->getDocumentTypeById($inv->document_type_id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id, false);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['inv'] = $inv;
        $this->data['rows'] =  $this->hotel_model->get_guests_register_detail($id);
        $this->data['settings'] = $this->Settings;
        $this->data['sma'] = $this->sma;
        $this->data['document_type'] = $document_type;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        if ($this->Settings->great_contributor == 1) {
            $this->data['tipo_regimen'] = lang('great_contributor');
        } else {
            $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        }
        $prueba = false;
        $url_format = "hotel/register_view";
        $this->data['document_type_invoice_format'] = false;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
        $this->data['biller_logo'] = 2;
        $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        if (!$prueba) {
            $url_format = "hotel/register_view";
            if ($document_type_invoice_format) {
                $this->data['document_type_invoice_format'] = $document_type_invoice_format;
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
                $this->data['biller_logo'] = $document_type_invoice_format->logo;
                $url_format = $document_type_invoice_format->format_url;
            }
        }
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $this->load_view($this->theme.$url_format, $this->data);
    }

    public function add_guest()
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('document_type_id', lang("document_type_id"), 'required');
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'document_type_id' => $this->input->post('document_type_id'),
                'vat_no' => $this->input->post('vat_no'),
                'names' => $this->input->post('names'),
                'surnames' => $this->input->post('surnames'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'nationality' => $this->input->post('nationality'),
                'birth_date' => $this->input->post('birth_date'),
            );
        } elseif ($this->input->post('add_guest')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run() == true && $saved_guest = $this->hotel_model->add_guest($data))
        {
          $this->session->set_flashdata('message', lang("guest_added"));
          redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->load_view($this->theme . 'hotel/add_guest', $this->data);
        }
    }

    public function guests($action = NULL)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['action'] = $action;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => '#', 'page' => lang('guests')));
        $meta = array('page_title' => lang('guests'), 'bc' => $bc);        
        $this->page_construct('hotel/guests', $meta, $this->data);
    }

    public function get_guests(){
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        ".$this->db->dbprefix("guests").".id as id,
                        ".$this->db->dbprefix("guests").".names,
                        ".$this->db->dbprefix("guests").".surnames,
                        ".$this->db->dbprefix("guests").".nationality,
                        ".$this->db->dbprefix("guests").".phone,
                        ".$this->db->dbprefix("guests").".email,
                        ".$this->db->dbprefix("guests").".birth_date,

                    ")
            ->join('documentypes', 'documentypes.id = guests.document_type_id', 'left')
            ->from('guests')
            ;
        $edit_link = anchor('admin/hotel/edit_guest/$1', '<i class="fa fa-pencil"></i> ' . lang('edit_guest'), 'data-toggle="modal" data-target="#myModal"');
        $action = '<div class="text-center"><div class="btn-group text-left">'
                        . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
                        . lang('actions') . ' <span class="caret"></span></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                            '.($this->Owner || $this->Admin || $this->GP['hotel-edit_guest'] ? '<li>' . $edit_link . '</li>' : '').'
                            </ul>
                        </div></div>';
        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function edit_guest($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('document_type_id', lang("document_type_id"), 'required');
        if (!$guest = $this->hotel_model->get_guest_by_id($id)) {
            $this->session->set_flashdata('error', 'No existe el Huésped');
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'document_type_id' => $this->input->post('document_type_id'),
                'vat_no' => $this->input->post('vat_no'),
                'names' => $this->input->post('names'),
                'surnames' => $this->input->post('surnames'),
                'email' => $this->input->post('email'),
                'phone' => $this->input->post('phone'),
                'nationality' => $this->input->post('nationality'),
                'birth_date' => $this->input->post('birth_date'),
            );
        } elseif ($this->input->post('edit_guest')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run() == true && $this->hotel_model->edit_guest($id, $data))
        {
          // $this->sma->print_arrays($data, $id);
          $this->session->set_flashdata('message', lang("guest_updated"));
          redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['guest'] = $guest;
            $this->data['id'] = $id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->load_view($this->theme . 'hotel/edit_guest', $this->data);
        }
    }

    public function close_guest_register($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('date_out', lang("date_out"), 'required');
        if (!$guest_register = $this->hotel_model->get_guest_register_by_id($id)) {
            $this->session->set_flashdata('error', 'No existe el registro de huespedes');
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($guest_register->status != 1) {
            $this->session->set_flashdata('error', 'El registro ya se encuentra cerrado');
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run() == TRUE) {
            $data = array(
                'date_out' => $this->sma->fld(trim($this->input->post('date_out'))),
                'total' => $this->input->post('total'),
            );
        } elseif ($this->input->post('close_guest_register')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->form_validation->run() == true && $this->hotel_model->close_guest_register($id, $data))
        {
          $this->session->set_flashdata('message', lang("guest_updated"));
          redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['guest_register'] = $guest_register;
            $this->data['id'] = $id;
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['id_document_types'] = $this->companies_model->getIDDocumentTypes();
            $this->load_view($this->theme . 'hotel/close_guest_register', $this->data);
        }
    }

    public function validate_no_in_another_open_register(){
        $document_type = $this->input->get('document_type');
        $vat_no = $this->input->get('vat_no');
        if ($this->hotel_model->validate_no_in_another_open_register($document_type, $vat_no)) {
            echo json_encode(['exists'=>'true']);
        } else {
            echo json_encode(['exists'=>'false']);
        }
    }
}