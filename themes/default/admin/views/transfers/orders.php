<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>

    if (localStorage.getItem('remove_otols')) {
        if (localStorage.getItem('otoitems')) {
                localStorage.removeItem('otoitems');
            }
            if (localStorage.getItem('otoref')) {
                localStorage.removeItem('otoref');
            }
            if (localStorage.getItem('otowarehouse')) {
                localStorage.removeItem('otowarehouse');
            }
            if (localStorage.getItem('otobiller')) {
                localStorage.removeItem('otobiller');
            }
            if (localStorage.getItem('otonote')) {
                localStorage.removeItem('otonote');
            }
            if (localStorage.getItem('otodate')) {
                localStorage.removeItem('otodate');
            }
            if (localStorage.getItem('otoseller')) {
                localStorage.removeItem('otoseller');
            }
        localStorage.removeItem('remove_otols');
    }

    <?php if ($this->session->userdata('remove_otols')) {?>
        if (localStorage.getItem('otoitems')) {
                localStorage.removeItem('otoitems');
            }
            if (localStorage.getItem('otoref')) {
                localStorage.removeItem('otoref');
            }
            if (localStorage.getItem('otowarehouse')) {
                localStorage.removeItem('otowarehouse');
            }
            if (localStorage.getItem('otobiller')) {
                localStorage.removeItem('otobiller');
            }
            if (localStorage.getItem('otonote')) {
                localStorage.removeItem('otonote');
            }
            if (localStorage.getItem('otodate')) {
                localStorage.removeItem('otodate');
            }
            if (localStorage.getItem('otoseller')) {
                localStorage.removeItem('otoseller');
            }
    <?php
        $this->sma->unset_data('remove_tols');
    }
    ?>

    if ($(window).width() < 1000) {
        var nums = [[10, 25], [10, 25]];
    } else {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }
    $(document).ready(function () {
        _tabFilterFill = false;
        oTable = $('#oToData').dataTable({
            "aaSorting": [[1, "desc"], [2, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('transfers/getOrderTransfers') ?>',
            'dom': '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push(
                    {
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    },                    
                    {
                        "name": "biller_id",
                        "value": "<?= isset($_POST['biller_id']) ? $_POST['biller_id'] : '$(\'#biller_id\').val()' ?>"
                    },                    
                    {
                        "name": "warehouse_id",
                        "value": "<?= isset($_POST['warehouse_id']) ? $_POST['warehouse_id'] : '$("#warehouse_id").val()' ?>"
                    },              
                    {
                        "name": "status",
                        "value": "<?= isset($_POST['status']) ? $_POST['status'] : '' ?>"
                    },                    
                    {
                        "name": "accepted",
                        "value": "<?= isset($_POST['accepted']) ? $_POST['accepted'] : '' ?>"
                    }, 
                    {
                        "name": "start_date",
                        "value": "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
                    }, 
                    {
                        "name": "end_date",
                        "value": "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>"
                    }
                );
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            "aoColumns": [
                {"bSortable": false,"mRender": checkbox}, 
                {"mRender": fld}, 
                null, 
                null, 
                null, 
                null, 
                null, 
                {"mRender": row_status}, 
                {"bSortable": false}],
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "order_transfer_link";
                return nRow;
            },
            'fnDrawCallback' : function(){ 
                addButton = '';

                $('.actionsButtonContainer').html(`<a href="<?= admin_url('transfers/add_order') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>
                    <div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                    </ul>
                </div>`);
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            },
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var row_total = 0, tax = 0, gtotal = 0;
                for (var i = 0; i < aaData.length; i++) {
                }
                var nCells = nRow.getElementsByTagName('th');
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('ref_no');?>]", filter_type: "text", data: []},
            {
                column_number: 3,
                filter_default_label: "[<?=lang("warehouse").' ('.lang('from').')';?>]",
                filter_type: "text", data: []
            },
            {column_number: 4, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
        ], "footer");
        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });
    });
</script>


<div class="wrapper wrapper-content  animated fadeInRight  no-printable">
    <div class="row">
        <div class="col-lg-12">
            <div class="no-print notifications_container"></div>
        </div> 
    </div>    
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('transfers/orders', ['id' => 'transfers_filter']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <?php $billerId = (empty($billerId)) && $this->session->userdata('view_right') == 0 ? $this->session->userdata('biller_id') : $billerId; ?>
                                            <select class="form-control" id="biller_id" name="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="warehouse_id"><?= $this->lang->line('warehouse') ?></label>
                                            <?php $warehouse_id = (isset($_POST["warehouse_id"])) ? $_POST["warehouse_id"] : ''; ?>
                                            <select class="form-control" id="warehouse_id" name="warehouse_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($warehouses as $warehouse) : ?>
                                                    <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouse_id) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                                <?php endforeach ?> 
                                            </select>
                                        </div>
                                    </div>  
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="status"><?= $this->lang->line('status') ?></label>
                                            <?php $status = (isset($_POST["status"])) ? $_POST["status"] : ''; ?>
                                            <?php $arrayStatus = array('completed' => lang('completed'), 'pending' => lang('pending')); ?>
                                            <select class="form-control" id="status" name="status">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($arrayStatus as $key => $value) : ?>
                                                    <option value="<?= $key ?>" <?= ($key == $status) ? 'selected' : '' ?>><?= $value ?></option>
                                                <?php endforeach ?> 
                                            </select>
                                        </div>
                                    </div>
                                </div> <!-- row -->
                            </div> <!-- col-sm-11 -->
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                </div>
                            </div>
                        </div>
                    </div> <!-- ibox-title -->
                    <div class="ibox-content" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h4>Filtros avanzados</h4>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-3" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <?= lang('date_records_filter', 'date_records_filter_h') ?>
                                            <select name="date_records_filter" id="date_records_filter_h" class="form-control">
                                                <?= $this->sma->get_filter_options(); ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="date_controls_h">
                                        <div class="col-sm-3" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('start_date', 'start_date_h') ?>
                                            <input type="date" name="start_date" id="start_date_h" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control">
                                        </div>
                                        <div class="col-sm-3" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                            <?= lang('end_date', 'end_date_h') ?>
                                            <input type="date" name="end_date" id="end_date_h" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <button type="submit" id="submit-sales-filter" class="btn btn-primary"><span class="fa fa-search"></span> <?= lang('do_filter') ?></button>
                                    </div>
                                </div>
                            </div>
                        </div>  
                    </div> <!-- ibox-content -->
                </div> <!-- ibox -->
            <?= form_close() ?>    
        </div> <!-- col-sm-12 -->
    </div> <!-- row -->
           

<?php if ($Owner || $Admin || $GP['bulk_actions']) {
    echo admin_form_open('transfers/transfer_actions', 'id="action-form"');
} ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <h4 class="text_filter"></h4>
                            <div class="table-responsive">
                                <table id="oToData" class="table table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("destination_biller"); ?></th>
                                            <th><?= lang("third"); ?></th>
                                            <th><?= lang("ref_no"); ?></th>
                                            <th><?= lang("biller_origin"); ?></th>
                                            <th><?= lang("destination_warehouse"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <th style="width:100px;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check"/>
                                            </th>
                                            <th></th><th></th><th></th><th></th><th></th><th></th><th></th>
                                            <th style="width:100px; text-align: center;"><?= lang("actions"); ?></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<script type="text/javascript">


$(document).ready(function(){
    setTimeout(function() {
        <?php if (!isset($_POST['date_records_filter'])): ?>
            $('#date_records_filter_h').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            $('#transfers_filter').submit();
        <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
            $('#date_records_filter_h').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
        <?php endif ?>
        setFilterText();
        <?php if ($this->session->userdata('biller_id') && $this->session->userdata('view_right') == 0): ?>
            $('#biller_id').trigger('change').select2('readonly', true);
        <?php endif ?>
    }, 150);
    $(document).on('change', '#biller_id', function() { loadWarehouses($(this)); });
    function loadWarehouses(element) {
        if (element.val()) {
            var defaultwh = element.children("option:selected").data('defaultwh');
            $('#warehouse_id').select2('val', defaultwh).trigger('change');
        } else {
            $('#warehouse_id option').each(function(index, option) {
                $(option).attr('disabled', false);
            });
            $('#warehouse_id').select2('val', '').trigger('change');
        }
    };
});

function setFilterText(){
    var start_date_text = $('#start_date_h').val();
    var end_date_text = $('#end_date_h').val();
    var text = "Filtros configurados : ";
    coma = false;
    if (start_date_text != '' && start_date_text !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha de inicio ("+start_date_text+")";
        coma = true;
    }
    if (end_date_text != '' && end_date_text !== undefined) {
        text+= coma ? "," : "";
        text+=" Fecha final ("+end_date_text+")";
        coma = true;
    }
    $('.text_filter').html(text);
}

function receivedStatus(x)
{ 
    y = x.split('-');
    if (y[1] == '0') {
        return '<div class="receivedClass text-center text-default"><a href="'+site.base_url+'/transfers/received/'+y[0]+'" data-toggle="modal" data-target="#myModal"><i style="cursor:pointer;" class="fa fa-times text-default" data-toggle="tooltip" data-placement="top" title="'+lang["no"]+'"> </i></a></div>';
    } else if(y[1] == '1') {
        return '<div class="receivedClass text-center text-info"><i style="cursor:pointer;" class="fa fa-check" data-toggle="tooltip" data-placement="top" title="'+lang["yes"]+'"></i></div>';
    }
}
</script>