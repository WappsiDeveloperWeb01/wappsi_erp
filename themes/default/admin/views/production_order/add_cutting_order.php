<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1, an = 1;
    var type_opt = {'addition': '<?= lang('addition'); ?>', 'subtraction': '<?= lang('subtraction'); ?>'};
    var max_qpr = "<?= isset($max_qpr) ? $max_qpr : 0 ?>";
    var POR_SUGGESTIONS_TYPE = "<?= POR_CUTTING_TYPE ?>";

    <?php if (isset($production_order_id)): ?>
        if (localStorage.getItem('cuitems')) {
            localStorage.removeItem('cuitems');
        }
        if (localStorage.getItem('curef')) {
            localStorage.removeItem('curef');
        }
        if (localStorage.getItem('cuwarehouse')) {
            localStorage.removeItem('cuwarehouse');
        }
        if (localStorage.getItem('cunote')) {
            localStorage.removeItem('cunote');
        }
        if (localStorage.getItem('cudate')) {
            localStorage.removeItem('cudate');
        }
        localStorage.setItem('cuproduction_order', '<?= $production_order_id ?>');
    <?php endif ?>

        if (localStorage.getItem('remove_porls')) {
            if (localStorage.getItem('poritems')) {
                localStorage.removeItem('poritems');
            }
            if (localStorage.getItem('porwarehouse')) {
                localStorage.removeItem('porwarehouse');
            }
            if (localStorage.getItem('pornote')) {
                localStorage.removeItem('pornote');
            }
            if (localStorage.getItem('pordate')) {
                localStorage.removeItem('pordate');
            }
            if (localStorage.getItem('porestimated_date')) {
                localStorage.removeItem('porestimated_date');
            }
            if (localStorage.getItem('document_type_id')) {
                localStorage.removeItem('document_type_id');
            }
            if (localStorage.getItem('poremployee')) {
                localStorage.removeItem('poremployee');
            }
            if (localStorage.getItem('porbiller')) {
                localStorage.removeItem('porbiller');
            }
            localStorage.removeItem('remove_porls');
        }

        <?php if ($this->session->userdata('remove_porls')) { ?>
            if (localStorage.getItem('poritems')) {
                localStorage.removeItem('poritems');
            }
            if (localStorage.getItem('porwarehouse')) {
                localStorage.removeItem('porwarehouse');
            }
            if (localStorage.getItem('pornote')) {
                localStorage.removeItem('pornote');
            }
            if (localStorage.getItem('pordate')) {
                localStorage.removeItem('pordate');
            }
            if (localStorage.getItem('porestimated_date')) {
                localStorage.removeItem('porestimated_date');
            }
            if (localStorage.getItem('document_type_id')) {
                localStorage.removeItem('document_type_id');
            }
            if (localStorage.getItem('poremployee')) {
                localStorage.removeItem('poremployee');
            }
            if (localStorage.getItem('porbiller')) {
                localStorage.removeItem('porbiller');
            }
        <?php $this->sma->unset_data('remove_porls');}
        ?>

    $(document).ready(function () {
        if (localStorage.getItem('remove_culs')) {
            if (localStorage.getItem('cuitems')) {
                localStorage.removeItem('cuitems');
            }
            if (localStorage.getItem('curef')) {
                localStorage.removeItem('curef');
            }
            if (localStorage.getItem('cuwarehouse')) {
                localStorage.removeItem('cuwarehouse');
            }
            if (localStorage.getItem('cunote')) {
                localStorage.removeItem('cunote');
            }
            if (localStorage.getItem('cudate')) {
                localStorage.removeItem('cudate');
            }
            localStorage.removeItem('remove_culs');
        }

        <?php if ($Owner || $Admin) { ?>
        if (!localStorage.getItem('cudate')) {
            $("#cudate").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
        }
        $(document).on('change', '#cudate', function (e) {
            localStorage.setItem('cudate', $(this).val());
        });
        if (cudate = localStorage.getItem('cudate')) {
            $('#cudate').val(cudate);
        }
        $(document).on('change', '#cuestimated_date', function (e) {
            localStorage.setItem('cuestimated_date', $(this).val());
        });
        if (cuestimated_date = localStorage.getItem('cuestimated_date')) {
            $('#cuestimated_date').val(cuestimated_date);
        }
        <?php } ?>
    });

    $(document).on('change', '#cubiller', function (e) {
        $.ajax({
          url:'<?= admin_url("billers/getBillersDocumentTypes/39/") ?>'+$('#cubiller').val(),
          type:'get',
          dataType:'JSON'
        }).done(function(data){
          response = data;
          $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
          if (response.status == 0) {
            $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
            localStorage.setItem('locked_for_biller_resolution', 1);
          }
          $('#document_type_id').trigger('change');
        });
        $('#cuwarehouse').select2('val', $('#cubiller option:selected').data('warehousedefault')).trigger('change');
        localStorage.setItem('cubiller', $('#cubiller').val());
    });


</script>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= lang('add_cutting_order'); ?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="introtext"><?php echo lang('enter_info'); ?></p>
                            <?php
                            $attrib = array('id' => 'add_cutting');
                            echo admin_form_open_multipart("production_order/add_cutting_order/", $attrib);
                            ?>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="row">
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("date", "cudate"); ?>
                                                <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="cudate" required="required" allowtextinput="false"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("making_order", "production_order"); ?>
                                                <input type="hidden" name="production_order_id" value="" id="cuproduction_order" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>" required>
                                                <input type="hidden" name="production_order_reference_no" value="" id="cuproduction_order_reference_no">
                                            </div>
                                        </div>
                                        <!-- <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("cutting_reference_no", "cureference_no"); ?>
                                                <?php echo form_input('reference_no', (isset($_POST['cureference_no']) ? $_POST['cureference_no'] : ""), 'class="form-control" id="cureference_no" required="required"'); ?>
                                            </div>
                                        </div> -->
                                        <div class="col-md-4 col-sm-4">
                                            <div class="form-group">
                                                <?= lang("estimated_date", "cuest_date"); ?>
                                                <?php echo form_input('est_date', (isset($_POST['est_date']) ? $_POST['est_date'] : ""), 'class="form-control input-tip datetime" id="cuest_date" required="required"'); ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cuemployee"><?= sprintf(lang("cutter_num"), 1); ?></label>
                                                <input type="hidden" name="employee_id" value="" id="cuemployee" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cutender1"><?= sprintf(lang("tender_num"), 1); ?></label>
                                                <input type="hidden" name="tender_id" value="" id="cutender" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cupacker1"><?= sprintf(lang("packer_num"), 1); ?></label>
                                                <input type="hidden" name="packer_id" value="" id="cupacker" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>" required>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cuemployee2"><?= sprintf(lang("cutter_num"), 2); ?></label>
                                                <input type="hidden" name="employee_2_id" value="" id="cuemployee2" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cutender2"><?= sprintf(lang("tender_num"), 2); ?></label>
                                                <input type="hidden" name="tender_2_id" value="" id="cutender2" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cupacker2"><?= sprintf(lang("packer_num"), 2); ?></label>
                                                <input type="hidden" name="packer_2_id" value="" id="cupacker2" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cuemployee2"><?= lang("embroiderer"); ?></label>
                                                <input type="hidden" name="embroiderer" value="" id="cuembroiderer" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="cuemployee2"><?= lang("stamper"); ?></label>
                                                <input type="hidden" name="stamper" value="" id="custamper" class="form-control"  style="width: 100%" placeholder="<?= lang("select") ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <?php if (isset($production_order_id)): ?>
                                        <div class="row div_porc" style="display:none;">
                                            <div class="col-md-12">
                                                <hr>
                                                <br>
                                                <h3 class="table-label"><?= lang("making_order"); ?> *</h3>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="">
                                                    <label class="table-label"><?= lang("products"); ?> *</label>
                                                    <table class="table table-bordered table-hover print-table order-table">
                                                        <thead>
                                                            <tr>
                                                                <th><?= lang("description"); ?> </th>
                                                                <th><?= lang("quantity"); ?>    </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                                            <?php $r = 1;
                                                            $tTotal = 0;
                                                            foreach ($por_rows as $row):
                                                                if ($row->production_order_pfinished_item_id != NULL) {
                                                                    continue;
                                                                }
                                                            ?>
                                                                <tr>
                                                                    <td style="vertical-align:middle;">
                                                                        <?= $row->product_code.' - '.$row->product_name; ?>
                                                                    </td>
                                                                    <th class="text-right"><?= $this->sma->formatQuantity($row->quantity); $tTotal+= $row->quantity; ?></th>
                                                                </tr>
                                                                <?php
                                                                $r++;
                                                            endforeach;
                                                            ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th class="text-right"><?= lang('total') ?></th>
                                                                <th class="text-right"><?= $this->sma->formatQuantity($tTotal); ?></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="">
                                                    <label class="table-label"><?= lang("components"); ?> *</label>
                                                    <table class="table table-bordered table-hover print-table order-table">
                                                        <thead>
                                                            <tr>
                                                                <th><?= lang("description"); ?></th>
                                                                <th><?= lang('product'); ?></th>
                                                                <th><?= lang("quantity"); ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php $r = 1;
                                                            $tTotal = 0;
                                                            foreach ($por_rows as $row):
                                                                if ($row->production_order_pfinished_item_id == NULL) {
                                                                    continue;
                                                                }
                                                            ?>
                                                                <tr>
                                                                    <td style="vertical-align:middle;">
                                                                        <?= $row->product_code.' - '.$row->product_name; ?>
                                                                    </td>
                                                                    <td><?= $row->pfinished_name.' - '.$row->pfinished_code.($row->pfinished_variant ? ' - '.$row->pfinished_variant : ''); ?></td>
                                                                    <th class="text-right"><?= $this->sma->formatQuantity($row->quantity); $tTotal+= $row->quantity; ?></th>
                                                                </tr>
                                                                <?php
                                                                $r++;
                                                            endforeach;
                                                            ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <th class="text-right" colspan="2"><?= lang('total') ?></th>
                                                                <th class="text-right"><?= $this->sma->formatQuantity($tTotal); ?></th>
                                                            </tr>
                                                        </tfoot>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-md-12">
                                        <hr>
                                        <br>
                                        <h3 class="table-label"><?= lang("cutting_order"); ?> *</h3>
                                    </div>
                                    <div class="col-md-12 div_cutable">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("pfinished_products_in_cutting_order"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="cuTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th class="col-md-1"><?= lang("type"); ?></th>
                                                        <th class="col-md-2"><?= lang("variant"); ?></th>
                                                        <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                        <th style="max-width: 30px !imcutant; text-align: center;">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="control-group table-group">
                                            <label class="table-label"><?= lang("components"); ?> *</label>
                                            <div class="controls table-controls">
                                                <table id="cuCTable" class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th><?= lang("product_name") . " (" . lang("product_code") . ")"; ?></th>
                                                        <th><?= lang("type"); ?></th>
                                                        <th><?= lang("cutted_quantity"); ?></th>
                                                        <th><?= lang("to_cut_quantity"); ?></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                    <tfoot></tfoot>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <?= lang("document", "document") ?>
                                                <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false"
                                                    data-show-preview="false" class="form-control file">
                                            </div>
                                        </div>
                                        <div class="col-md-12 text-center">
                                            <img alt="User Image" id="previewImage" style="max-width: 100% !important; max-height: 400px;">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                        <div class="col-md-12 col-sm-12">
                                            <div class="form-group">
                                                <?= lang("note", "cunote"); ?>
                                                <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="cunote" style="margin-top: 10px; height: 100px;"'); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="fprom-group">
                                            <button type="button" class="btn btn-primary" id="add_cutting_submit"><?= lang('submit') ?></button>
                                            <!-- <button type="button" class="btn btn-primary" id="add_cutting_order_submit_continue"><?= lang('submit_init_assemble') ?></button> -->
                                            <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" id="scontinue" name="continue" value="0">
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $("#add_cutting").validate({
            ignore: []
        });
    });

    $(document).on('click', '#add_cutting_submit', function(){
        $('#scontinue').val(0);
        if ($('#add_cutting').valid()) {
            $('#add_cutting').submit();
        }
    });

    $(document).on('click', '#add_cutting_order_submit_continue', function(){
        $('#scontinue').val(1);
        if ($('#add_cutting').valid()) {
            $('#add_cutting').submit();
        }
    });
</script>