<?php
$apppath = str_replace("\app", "", APPPATH);
$apppath = str_replace("/app", "", $apppath);
require_once $apppath.'themes/default/admin/views/sales/sale_view_header.php';
class PDF extends HPDF
{
    protected $T128;                                         // Tableau des codes 128
    protected $ABCset = "";                                  // jeu des caractères éligibles au C128
    protected $Aset = "";                                    // Set A du jeu des caractères éligibles
    protected $Bset = "";                                    // Set B du jeu des caractères éligibles
    protected $Cset = "";                                    // Set C du jeu des caractères éligibles
    protected $SetFrom;                                      // Convertisseur source des jeux vers le tableau
    protected $SetTo;                                        // Convertisseur destination des jeux vers le tableau
    protected $JStart = array("A"=>103, "B"=>104, "C"=>105); // Caractères de sélection de jeu au début du C128
    protected $JSwap = array("A"=>101, "B"=>100, "C"=>99); 
    function __construct($orientation='P', $unit='mm', $format='A4') {
        parent::__construct($orientation,$unit,$format);
        $this->T128[] = array(2, 1, 2, 2, 2, 2);           //0 : [ ]               // composition des caractères
        $this->T128[] = array(2, 2, 2, 1, 2, 2);           //1 : [!]
        $this->T128[] = array(2, 2, 2, 2, 2, 1);           //2 : ["]
        $this->T128[] = array(1, 2, 1, 2, 2, 3);           //3 : [#]
        $this->T128[] = array(1, 2, 1, 3, 2, 2);           //4 : [$]
        $this->T128[] = array(1, 3, 1, 2, 2, 2);           //5 : [%]
        $this->T128[] = array(1, 2, 2, 2, 1, 3);           //6 : [&]
        $this->T128[] = array(1, 2, 2, 3, 1, 2);           //7 : [']
        $this->T128[] = array(1, 3, 2, 2, 1, 2);           //8 : [(]
        $this->T128[] = array(2, 2, 1, 2, 1, 3);           //9 : [)]
        $this->T128[] = array(2, 2, 1, 3, 1, 2);           //10 : [*]
        $this->T128[] = array(2, 3, 1, 2, 1, 2);           //11 : [+]
        $this->T128[] = array(1, 1, 2, 2, 3, 2);           //12 : [,]
        $this->T128[] = array(1, 2, 2, 1, 3, 2);           //13 : [-]
        $this->T128[] = array(1, 2, 2, 2, 3, 1);           //14 : [.]
        $this->T128[] = array(1, 1, 3, 2, 2, 2);           //15 : [/]
        $this->T128[] = array(1, 2, 3, 1, 2, 2);           //16 : [0]
        $this->T128[] = array(1, 2, 3, 2, 2, 1);           //17 : [1]
        $this->T128[] = array(2, 2, 3, 2, 1, 1);           //18 : [2]
        $this->T128[] = array(2, 2, 1, 1, 3, 2);           //19 : [3]
        $this->T128[] = array(2, 2, 1, 2, 3, 1);           //20 : [4]
        $this->T128[] = array(2, 1, 3, 2, 1, 2);           //21 : [5]
        $this->T128[] = array(2, 2, 3, 1, 1, 2);           //22 : [6]
        $this->T128[] = array(3, 1, 2, 1, 3, 1);           //23 : [7]
        $this->T128[] = array(3, 1, 1, 2, 2, 2);           //24 : [8]
        $this->T128[] = array(3, 2, 1, 1, 2, 2);           //25 : [9]
        $this->T128[] = array(3, 2, 1, 2, 2, 1);           //26 : [:]
        $this->T128[] = array(3, 1, 2, 2, 1, 2);           //27 : [;]
        $this->T128[] = array(3, 2, 2, 1, 1, 2);           //28 : [<]
        $this->T128[] = array(3, 2, 2, 2, 1, 1);           //29 : [=]
        $this->T128[] = array(2, 1, 2, 1, 2, 3);           //30 : [>]
        $this->T128[] = array(2, 1, 2, 3, 2, 1);           //31 : [?]
        $this->T128[] = array(2, 3, 2, 1, 2, 1);           //32 : [@]
        $this->T128[] = array(1, 1, 1, 3, 2, 3);           //33 : [A]
        $this->T128[] = array(1, 3, 1, 1, 2, 3);           //34 : [B]
        $this->T128[] = array(1, 3, 1, 3, 2, 1);           //35 : [C]
        $this->T128[] = array(1, 1, 2, 3, 1, 3);           //36 : [D]
        $this->T128[] = array(1, 3, 2, 1, 1, 3);           //37 : [E]
        $this->T128[] = array(1, 3, 2, 3, 1, 1);           //38 : [F]
        $this->T128[] = array(2, 1, 1, 3, 1, 3);           //39 : [G]
        $this->T128[] = array(2, 3, 1, 1, 1, 3);           //40 : [H]
        $this->T128[] = array(2, 3, 1, 3, 1, 1);           //41 : [I]
        $this->T128[] = array(1, 1, 2, 1, 3, 3);           //42 : [J]
        $this->T128[] = array(1, 1, 2, 3, 3, 1);           //43 : [K]
        $this->T128[] = array(1, 3, 2, 1, 3, 1);           //44 : [L]
        $this->T128[] = array(1, 1, 3, 1, 2, 3);           //45 : [M]
        $this->T128[] = array(1, 1, 3, 3, 2, 1);           //46 : [N]
        $this->T128[] = array(1, 3, 3, 1, 2, 1);           //47 : [O]
        $this->T128[] = array(3, 1, 3, 1, 2, 1);           //48 : [P]
        $this->T128[] = array(2, 1, 1, 3, 3, 1);           //49 : [Q]
        $this->T128[] = array(2, 3, 1, 1, 3, 1);           //50 : [R]
        $this->T128[] = array(2, 1, 3, 1, 1, 3);           //51 : [S]
        $this->T128[] = array(2, 1, 3, 3, 1, 1);           //52 : [T]
        $this->T128[] = array(2, 1, 3, 1, 3, 1);           //53 : [U]
        $this->T128[] = array(3, 1, 1, 1, 2, 3);           //54 : [V]
        $this->T128[] = array(3, 1, 1, 3, 2, 1);           //55 : [W]
        $this->T128[] = array(3, 3, 1, 1, 2, 1);           //56 : [X]
        $this->T128[] = array(3, 1, 2, 1, 1, 3);           //57 : [Y]
        $this->T128[] = array(3, 1, 2, 3, 1, 1);           //58 : [Z]
        $this->T128[] = array(3, 3, 2, 1, 1, 1);           //59 : [[]
        $this->T128[] = array(3, 1, 4, 1, 1, 1);           //60 : [\]
        $this->T128[] = array(2, 2, 1, 4, 1, 1);           //61 : []]
        $this->T128[] = array(4, 3, 1, 1, 1, 1);           //62 : [^]
        $this->T128[] = array(1, 1, 1, 2, 2, 4);           //63 : [_]
        $this->T128[] = array(1, 1, 1, 4, 2, 2);           //64 : [`]
        $this->T128[] = array(1, 2, 1, 1, 2, 4);           //65 : [a]
        $this->T128[] = array(1, 2, 1, 4, 2, 1);           //66 : [b]
        $this->T128[] = array(1, 4, 1, 1, 2, 2);           //67 : [c]
        $this->T128[] = array(1, 4, 1, 2, 2, 1);           //68 : [d]
        $this->T128[] = array(1, 1, 2, 2, 1, 4);           //69 : [e]
        $this->T128[] = array(1, 1, 2, 4, 1, 2);           //70 : [f]
        $this->T128[] = array(1, 2, 2, 1, 1, 4);           //71 : [g]
        $this->T128[] = array(1, 2, 2, 4, 1, 1);           //72 : [h]
        $this->T128[] = array(1, 4, 2, 1, 1, 2);           //73 : [i]
        $this->T128[] = array(1, 4, 2, 2, 1, 1);           //74 : [j]
        $this->T128[] = array(2, 4, 1, 2, 1, 1);           //75 : [k]
        $this->T128[] = array(2, 2, 1, 1, 1, 4);           //76 : [l]
        $this->T128[] = array(4, 1, 3, 1, 1, 1);           //77 : [m]
        $this->T128[] = array(2, 4, 1, 1, 1, 2);           //78 : [n]
        $this->T128[] = array(1, 3, 4, 1, 1, 1);           //79 : [o]
        $this->T128[] = array(1, 1, 1, 2, 4, 2);           //80 : [p]
        $this->T128[] = array(1, 2, 1, 1, 4, 2);           //81 : [q]
        $this->T128[] = array(1, 2, 1, 2, 4, 1);           //82 : [r]
        $this->T128[] = array(1, 1, 4, 2, 1, 2);           //83 : [s]
        $this->T128[] = array(1, 2, 4, 1, 1, 2);           //84 : [t]
        $this->T128[] = array(1, 2, 4, 2, 1, 1);           //85 : [u]
        $this->T128[] = array(4, 1, 1, 2, 1, 2);           //86 : [v]
        $this->T128[] = array(4, 2, 1, 1, 1, 2);           //87 : [w]
        $this->T128[] = array(4, 2, 1, 2, 1, 1);           //88 : [x]
        $this->T128[] = array(2, 1, 2, 1, 4, 1);           //89 : [y]
        $this->T128[] = array(2, 1, 4, 1, 2, 1);           //90 : [z]
        $this->T128[] = array(4, 1, 2, 1, 2, 1);           //91 : [{]
        $this->T128[] = array(1, 1, 1, 1, 4, 3);           //92 : [|]
        $this->T128[] = array(1, 1, 1, 3, 4, 1);           //93 : [}]
        $this->T128[] = array(1, 3, 1, 1, 4, 1);           //94 : [~]
        $this->T128[] = array(1, 1, 4, 1, 1, 3);           //95 : [DEL]
        $this->T128[] = array(1, 1, 4, 3, 1, 1);           //96 : [FNC3]
        $this->T128[] = array(4, 1, 1, 1, 1, 3);           //97 : [FNC2]
        $this->T128[] = array(4, 1, 1, 3, 1, 1);           //98 : [SHIFT]
        $this->T128[] = array(1, 1, 3, 1, 4, 1);           //99 : [Cswap]
        $this->T128[] = array(1, 1, 4, 1, 3, 1);           //100 : [Bswap]                
        $this->T128[] = array(3, 1, 1, 1, 4, 1);           //101 : [Aswap]
        $this->T128[] = array(4, 1, 1, 1, 3, 1);           //102 : [FNC1]
        $this->T128[] = array(2, 1, 1, 4, 1, 2);           //103 : [Astart]
        $this->T128[] = array(2, 1, 1, 2, 1, 4);           //104 : [Bstart]
        $this->T128[] = array(2, 1, 1, 2, 3, 2);           //105 : [Cstart]
        $this->T128[] = array(2, 3, 3, 1, 1, 1);           //106 : [STOP]
        $this->T128[] = array(2, 1);                       //107 : [END BAR]
        for ($i = 32; $i <= 95; $i++) {                                            // jeux de caractères
            $this->ABCset .= chr($i);
        }
        $this->Aset = $this->ABCset;
        $this->Bset = $this->ABCset;
        for ($i = 0; $i <= 31; $i++) {
            $this->ABCset .= chr($i);
            $this->Aset .= chr($i);
        }
        for ($i = 96; $i <= 127; $i++) {
            $this->ABCset .= chr($i);
            $this->Bset .= chr($i);
        }
        for ($i = 200; $i <= 210; $i++) {                                           // controle 128
            $this->ABCset .= chr($i);
            $this->Aset .= chr($i);
            $this->Bset .= chr($i);
        }
        $this->Cset="0123456789".chr(206);
        for ($i=0; $i<96; $i++) {                                                   // convertisseurs des jeux A & B
            @$this->SetFrom["A"] .= chr($i);
            @$this->SetFrom["B"] .= chr($i + 32);
            @$this->SetTo["A"] .= chr(($i < 32) ? $i+64 : $i-32);
            @$this->SetTo["B"] .= chr($i);
        }
        for ($i=96; $i<107; $i++) {                                                 // contrôle des jeux A & B
            @$this->SetFrom["A"] .= chr($i + 104);
            @$this->SetFrom["B"] .= chr($i + 104);
            @$this->SetTo["A"] .= chr($i);
            @$this->SetTo["B"] .= chr($i);
        }
    }
    function Code128($x, $y, $code, $w, $h) {
        $Aguid = "";                                                                      // Création des guides de choix ABC
        $Bguid = "";
        $Cguid = "";
        for ($i=0; $i < strlen($code); $i++) {
            $needle = substr($code,$i,1);
            $Aguid .= ((strpos($this->Aset,$needle)===false) ? "N" : "O"); 
            $Bguid .= ((strpos($this->Bset,$needle)===false) ? "N" : "O"); 
            $Cguid .= ((strpos($this->Cset,$needle)===false) ? "N" : "O");
        }

        $SminiC = "OOOO";
        $IminiC = 4;

        $crypt = "";
        while ($code > "") {
                                                                                        // BOUCLE PRINCIPALE DE CODAGE
            $i = strpos($Cguid,$SminiC);                                                // forçage du jeu C, si possible
            if ($i!==false) {
                $Aguid [$i] = "N";
                $Bguid [$i] = "N";
            }

            if (substr($Cguid,0,$IminiC) == $SminiC) {                                  // jeu C
                $crypt .= chr(($crypt > "") ? $this->JSwap["C"] : $this->JStart["C"]);  // début Cstart, sinon Cswap
                $made = strpos($Cguid,"N");                                             // étendu du set C
                if ($made === false) {
                    $made = strlen($Cguid);
                }
                if (fmod($made,2)==1) {
                    $made--;                                                            // seulement un nombre pair
                }
                for ($i=0; $i < $made; $i += 2) {
                    $crypt .= chr(strval(substr($code,$i,2)));                          // conversion 2 par 2
                }
                $jeu = "C";
            } else {
                $madeA = strpos($Aguid,"N");                                            // étendu du set A
                if ($madeA === false) {
                    $madeA = strlen($Aguid);
                }
                $madeB = strpos($Bguid,"N");                                            // étendu du set B
                if ($madeB === false) {
                    $madeB = strlen($Bguid);
                }
                $made = (($madeA < $madeB) ? $madeB : $madeA );                         // étendu traitée
                $jeu = (($madeA < $madeB) ? "B" : "A" );                                // Jeu en cours

                $crypt .= chr(($crypt > "") ? $this->JSwap[$jeu] : $this->JStart[$jeu]); // début start, sinon swap

                $crypt .= strtr(substr($code, 0,$made), $this->SetFrom[$jeu], $this->SetTo[$jeu]); // conversion selon jeu

            }
            $code = substr($code,$made);                                           // raccourcir légende et guides de la zone traitée
            $Aguid = substr($Aguid,$made);
            $Bguid = substr($Bguid,$made);
            $Cguid = substr($Cguid,$made);
        }                                                                          // FIN BOUCLE PRINCIPALE

        $check = ord($crypt[0]);                                                   // calcul de la somme de contrôle
        for ($i=0; $i<strlen($crypt); $i++) {
            $check += (ord($crypt[$i]) * $i);
        }
        $check %= 103;
        $crypt .= chr($check) . chr(106) . chr(107);                               // Chaine cryptée complète
        $i = (strlen($crypt) * 11) - 8;                                            // calcul de la largeur du module
        $modul = $w/$i;
        for ($i=0; $i<strlen($crypt); $i++) {                                      // BOUCLE D'IMPRESSION
            $c = $this->T128[ord($crypt[$i])];
            for ($j=0; $j<count($c); $j++) {
                $this->Rect($x,$y,$c[$j]*$modul,$h,"F");
                $x += ($c[$j++]+$c[$j])*$modul;
            }
        }
    }
    function Header()
    {
        $this->SetTitle($this->sma->utf8Decode('Impresión código de barras'));
    }
    function Footer()
    {

    }

    function crop_text($text, $length = 50){
        if (strlen($text) > $length) {
            $text = substr($text, 0, ($length-5));
            $text.="...";
            return strip_tags($this->sma->decode_html($text));
        }
        return strip_tags($this->sma->decode_html($text));
    }
}
$pdf = new PDF('L', 'mm', [50,25]);
$pdf->sma = $this->sma;
$pdf->SetMargins(1, 1);
$pdf->SetAutoPageBreak(false);
$pdf->sma = $this->sma;
$print_row_border = 0;
for ($i=1; $i <= $delivery->containers_quantity; $i++) { 
    $pdf->AddPage();
    $pdf->SetFont('Arial','',5);
    $pdf->Image(base_url().'assets/images/delivery/delivery_logo.png',1,1,13.5);
    $pdf->RoundedRect(15.5, 1, 0, 21.4, 0, '', '');
    $pdf->RoundedRect(15.5, 11.5, 23.5, 0, 0, '', '');
    $pdf->setXY(15.5, 1);

$fontsize = 3;
$fontsize_plus = 1;
$row_height = 1.5;
$row_width = 25;
$max_text_length = 48;

    $cy = 1;
    $pdf->SetFont('Arial','',$fontsize);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode('Cliente'), $print_row_border, 'L');
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus);
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text($customer->company, $max_text_length)), $print_row_border, 0, 'L');
    $pdf->SetFont('Arial','',$fontsize);
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text($customer->name, $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text($inv->direccion_negocio, $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text($inv->phone_negocio, $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text('Vendedor: '.$seller->name, $max_text_length)), $print_row_border, 0, 'L');


$fontsize = 3;
$row_height = 1.7;
$row_width = 10;
$max_text_length = 18;

    $pdf->setXY(15.5, 12.5);
    $cy = 12.5;
    $pdf->SetFont('Arial','',$fontsize);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode('Orden de pedido'), $print_row_border, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text($inv->sale_origin_reference_no, $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->SetFont('Arial','',$fontsize);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text('Factura', $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text($inv->reference_no, $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->SetFont('Arial','',$fontsize);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($pdf->crop_text('Vehículo', $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5, $cy);
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus);
    $pdf->cell($row_width, $row_height, $this->sma->utf8Decode($delivery->vehicle), $print_row_border, 0, 'L');

    //DERECHA


    $row_width_R = 15;
    $max_text_length = 18;


    $pdf->setXY(15.5+$row_width, 12.5);
    $cy = 12.5;
    $pdf->SetFont('Arial','',$fontsize);
    $pdf->cell($row_width_R, $row_height, $this->sma->utf8Decode('Valor'), $print_row_border, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5+$row_width, $cy);
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus);
    $pdf->cell($row_width_R, $row_height, $this->sma->utf8Decode($pdf->crop_text($this->sma->formatMoney($inv->grand_total), $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5+$row_width, $cy);
    $pdf->SetFont('Arial','',$fontsize);
    $pdf->cell($row_width_R, $row_height, $this->sma->utf8Decode($pdf->crop_text('Fecha', $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5+$row_width, $cy);
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus);
    $pdf->cell($row_width_R, $row_height, $this->sma->utf8Decode($pdf->crop_text($pdf->crop_text(date('d/m/Y', strtotime($inv->date)), $max_text_length), $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5+$row_width, $cy);
    $pdf->SetFont('Arial','',$fontsize);
    $pdf->cell($row_width_R, $row_height, $this->sma->utf8Decode($pdf->crop_text('Conductor', $max_text_length)), $print_row_border, 0, 'L');
    $cy += $row_height;
    $pdf->setXY(15.5+$row_width, $cy);
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus);
    $pdf->cell($row_width_R, $row_height, $this->sma->utf8Decode($pdf->crop_text($delivery->delivered_by, $max_text_length)), $print_row_border, 0, 'L');

    $pdf->RoundedRect(41, 1.5   , 8, 7.5, 0, '', '');
    $pdf->setXY(42,2.5);
    $pdf->SetFont('Arial','B',$fontsize-0.8);
    $pdf->cell(6, 3, $this->sma->utf8Decode('ZONA/RUTA'), 'B', 0, 'C');
    $pdf->setXY(41,5.7);
    $pdf->SetFont('Arial','B',2);
    // $pdf->cell(8, 3, $this->sma->utf8Decode($inv->zone_name), 0, 0, 'C');
    $pdf->MultiCell(8,1,$this->sma->utf8Decode($inv->zone_name), 0, 'C');

    $pdf->RoundedRect(41, 9.5   , 8, 4.7, 0, '', '');
    $pdf->setXY(41,9.5);
    $pdf->SetFont('Arial','B',$fontsize-0.8);
    $pdf->cell(8, 3, $this->sma->utf8Decode('FRANJA HORARIA'), 0, 0, 'C');
    $pdf->setXY(41,11.5);
    $pdf->SetFont('Arial','B',$fontsize-0.5);
    $pdf->cell(8, 3, $this->sma->utf8Decode(date('h:ia', strtotime($inv->time_1))."-".date('h:ia', strtotime($inv->time_2))), 0, 0, 'C');

    $pdf->RoundedRect(41, 14.7, 8, 7.5, 0, '', '');
    $pdf->SetFont('Arial','B',$fontsize-0.8);
    $pdf->setXY(42,15.2);
    $pdf->cell(6, 3, $this->sma->utf8Decode('CONTENEDOR'), 'B', 0, 'C');
    $pdf->setXY(41,18.7);
    $pdf->SetFont('Arial','B',$fontsize+$fontsize_plus+1.5);
    $pdf->cell(8, 3, $this->sma->utf8Decode($i." / ".$delivery->containers_quantity), 0, 0, 'C');
}


$pdf->Output("printer_barcodes.pdf", "I");
