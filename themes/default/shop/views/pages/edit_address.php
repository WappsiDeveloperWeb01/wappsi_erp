<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <div class="row">
                <div class="col-md-9">
                    <h2><?= lang('register_form') ?></h2>
                </div>
                <div class="col-md-3">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </button>
                </div>
            </div>
        </div>
        <div class="modal-body row">
            <div class="col-md-12">
                <?php echo form_open("shop/edit_address/".$id, 'autocomplete="off" id="form_edit_address"'); ?>
                    <input type="hidden" name="edit_address" value="1">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <?= lang('name', 'sucursal') ?>
                            <input type="text" name="sucursal" class="form-control" id="sucursal"  value="<?= $address->sucursal ?>" placeholder="<?= lang('sucursal') ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                          <?= lang("country", "country"); ?>
                          <select class="form-control select" name="country" id="country" required>
                            <option value=""><?= lang("select") ?></option>
                            <?php foreach ($countries as $row => $country): ?>
                              <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>" <?= $country->NOMBRE == $address->country ? 'selected="selected"' : '' ?>><?= $country->NOMBRE ?></option>
                            <?php endforeach ?>
                          </select>
                        </div>
                        <div class="form-group col-md-6">
                          <?= lang("state", "state"); ?>
                          <select class="form-control select" name="state" id="state" required>
                            <option value=""><?= lang("select") ?></option>
                          </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                          <?= lang("city", "city"); ?>
                          <select class="form-control select" name="city" id="city" required>
                            <option value=""><?= lang("select") ?></option>
                          </select>
                          <input type="hidden" name="city_code" id="city_code" <?= (isset($_POST['city_code'])) ? "value='".$_POST['city_code']."'" : "" ?>>
                        </div>
                        <div class="col-md-6">
                            <?= lang('address', 'direccion') ?>
                            <input type="text" name="direccion" class="form-control" id="direccion" value="<?= $address->direccion ?>" placeholder="<?= lang('address') ?>" required>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" id="submit_edit_address" class="btn btn-primary"><?= lang('submit') ?></button>
                        </div>
                    </div>
                <?php echo form_close(); ?>
            </div>  
        </div>
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function(){
        $('select').select2();
        $('#country').on('change', function(){
          dpto = $('#country option:selected').data('code');
          $.ajax({
            url:"<?= admin_url() ?>auth/get_states/"+dpto,
          }).done(function(data){
            $('#state').html(data);
            $('#state').select2('val', '');
            $('#city').select2('val', '');
          }).fail(function(data){
            console.log(data.responseText);
          });
        });

        $('#state').on('change', function(){
          dpto = $('#state option:selected').data('code');
          $.ajax({
            url:"<?= admin_url() ?>auth/get_cities/"+dpto,
          }).done(function(data){
            $('#city').html(data);
            $('#city').select2('val', '');
          }).fail(function(data){
            console.log(data.responseText);
          });
        });

        $('#city').on('change', function(){
          code = $('#city option:selected').data('code');
          $('.postal_code').val(code);
          $('#city_code').val(code);
          $.ajax({
            url:"<?= admin_url().'auth/get_zones/' ?>"+code
          }).done(function(data){
            $('#zone').html(data);
          });

        });
        set_ubication();
    });

        

    function set_ubication() {
      country = $('#country option:selected').data('code');
      state = "<?= $address->state ?>";
      city = "<?= $address->city ?>";

      $.ajax({
        url:"<?= admin_url() ?>auth/get_states/"+country+"/"+state
      }).done(function(data) {
        $('#state').html(data);
        $('#state').select2();

        state = $('#state option:selected').data('code');

        $.ajax({
          url:"<?= admin_url() ?>auth/get_cities/"+state+"/"+city
        }).done(function(data) {

          $('#city').html(data);
          $('#city').select2('val', city).trigger('change');

        }).fail(function(data) {
          console.log(data.responseText);
        });
      }).fail(function(data) {
        console.log(data.responseText);
      });
    }

    $('#submit_edit_address').on('click', function(){
        if ($('#form_edit_address').valid()) {
            $('#form_edit_address').submit();
        }
    });
</script>