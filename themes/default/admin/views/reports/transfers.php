<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<?php
    $v = "";
    if ($this->input->post('start_date')) { $v .= "&start_date=" . $this->input->post('start_date'); }
    if ($this->input->post('end_date')) { $v .= "&end_date=" . $this->input->post('end_date'); }
    if ($this->input->post('warehouse_origin')) { $v .= "&warehouse_origin=" . $this->input->post('warehouse_origin'); }
    if ($this->input->post('warehouse_destination')) { $v .= "&warehouse_destination=" . $this->input->post('warehouse_destination'); }
?>
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?php echo admin_form_open("reports/transfers", 'id="form_filter"'); ?>
                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-title">
                        <h5><?= lang('filter') ?></h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">                      
                        <div class="row">
                            <div class="col-sm-3"> 
                                <div class="form-group">
                                    <label for="start_date" class="control-label"> <?= lang('start_date') ?> </label>
                                    <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : date('d/m/Y')), 'class="form-control date" id="start_date" 
                                    type="date" required="required" '); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <label class="control-label" for="end_date"><?= lang("end_date"); ?> </label>
                                    <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : date('d/m/Y')), 'class="form-control date" id="end_date" 
                                    type="date" required="required" '); ?>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="warehouse_origin" class="control-label"><?= lang('warehouse_origin') ?></label>
                                <?php
                                    $warehouse_selected = '';
                                    $who[""] = lang('select');
                                    foreach ($warehouses as $warehouse) { 
                                        $who[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse_origin', $who, (isset($_POST['warehouse_origin']) ? $_POST['warehouse_origin'] : $warehouse_selected), 'class="form-control who"  id="warehouse_origin" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                ?>
                            </div>
                            <div class="col-sm-3">
                                <label for="warehouse_destination" class="control-label"><?= lang('warehouse_destination') ?></label>
                                <?php
                                    $warehouse_d_selected = '';
                                    $whd[""] = lang('select');
                                    foreach ($warehouses as $warehouse) { 
                                        $whd[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse_destination', $whd, (isset($_POST['warehouse_destination']) ? $_POST['warehouse_destination'] : $warehouse_selected), 'class="form-control whd"  id="warehouse_destination" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <input type="hidden" name="filter_action" id="filter_action" value="0">
                                <div class="form-group">
                                    <div class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_filter"'); ?> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php echo form_close(); ?>
        </div>
    </div>

    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <h3>
                                        <?= lang('start_date') ?> : <span><?= !empty($_POST['start_date']) ? $_POST['start_date'] : date('Y-m-d') ?></span>
                                        &nbsp;&nbsp;&nbsp;     
                                        <?= lang('end_date') ?> : <span><?= !empty($_POST['end_date']) ? $_POST['end_date'] : date('Y-m-d') ?></span> 
                                    </h3>
                                    <table id="apData" class="table  table-bordered table-condensed table-hover dfTable reports-table" style="margin-bottom:5px;">
                                        <thead>
                                            <tr class="active">
                                                <th><?= lang("date"); ?></th>
                                                <th><?= lang("reference_no"); ?></th>   
                                                <th><?= lang("warehouse_origin"); ?></th>   
                                                <th><?= lang("warehouse_destination"); ?></th>   
                                                <th><?= lang("created_by"); ?></th>   
                                                <th><?= lang("code"); ?></th>   
                                                <th><?= lang("name"); ?></th>   
                                                <th><?= lang("quantity"); ?></th>   
                                                <th><?= lang("base"); ?></th>   
                                                <th><?= lang("tax"); ?></th>   
                                                <th><?= lang("grand_total"); ?></th>   
                                                <th><?= lang("status"); ?></th>   
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="8" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                            <tr class="active">
                                                <th><?= lang("date"); ?></th>
                                                <th><?= lang("reference_no"); ?></th>   
                                                <th><?= lang("warehouse_origin"); ?></th>   
                                                <th><?= lang("warehouse_destination"); ?></th>   
                                                <th><?= lang("created_by"); ?></th>   
                                                <th><?= lang("code"); ?></th>   
                                                <th><?= lang("name"); ?></th>   
                                                <th><?= lang("quantity"); ?></th>   
                                                <th><?= lang("base"); ?></th>   
                                                <th><?= lang("tax"); ?></th>   
                                                <th><?= lang("grand_total"); ?></th>   
                                                <th><?= lang("status"); ?></th>     
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>
</div>

<script>
    $(document).ready(function() {
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1) : ?>
            var oTable = $('#apData').dataTable({
                "aaSorting": [
                    [1, "asc"]
                ],
                "aLengthMenu": [
                    [10, 25, 50, 100, 500, -1],
                    [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
                ],
                "iDisplayLength": <?= $Settings->rows_per_page ?>,
                'bProcessing': true,
                'bServerSide': true,
                "dom" : '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
                'sAjaxSource': '<?= admin_url('reports/getTrasnfersMovements/?v=1' . $v) ?>',
                'fnServerData': function(sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({
                        'dataType': 'json',
                        'type': 'POST',
                        'url': sSource,
                        'data': aoData,
                        'success': fnCallback
                    });
                },
                'fnRowCallback': function(nRow, aData, iDisplayIndex) {

                },
                "fnDrawCallback": function (oSettings) {
                    $('.actionsButtonContainer').html('<div class="dropdown">'+
                        '<button class="btn btn-primary btn-outline new-button dropdown-toggle center-block" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>'+
                        '<ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">'+
                            '<li>'+
                                '<a href="<?= admin_url('reports/getTrasnfersMovements/xls/?v=1' . $v) ?>" id="xls">'+
                                '<i class="icon fa fa-file-excel-o"></i> <?= lang("download_xls") ?>'+
                                '</a>'+
                            '</li>'+
                        '</ul>'+
                    '</div>');
                    $('[data-toggle-second="tooltip"]').tooltip();
                },
                "aoColumns": [
                    { "bSortable": true },
                    { "bSortable": true },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false },
                    { "bSortable": false, "mRender" : currencyFormat },
                    { "bSortable": false, "mRender" : currencyFormat },
                    { "bSortable": false, "mRender" : currencyFormat },
                    { "bSortable": false, "mRender" : currencyFormat },
                    { "bSortable": false, "mRender": row_status },
                ],
                "fnFooterCallback": function(nRow, aaData, iStart, iEnd, aiDisplay) {

                }
            }).fnSetFilteringDelay().dtFilter([
                
            ], "footer");
        <?php endif ?>

        $('#submit_filter').on('click', function(e) {
            $('#filter_action').val(1);
        });

        $('#warehouse_origin').on('change', function (){
            if ($(this).val() != '') {
                validate_w_selected($(this).val(), 'destination');
            }else{
                $('.whd').find('option').prop('disabled', false);
            }
        });
        if ($('#warehouse_origin').val() != '') {
            validate_w_selected($('#warehouse_origin').val(), 'destination');
        }

        $('#warehouse_destination').on('change', function(){
            if ($(this).val() != '') {
                validate_w_selected($(this).val(), 'origin');
            }else{
                $('.who').find('option').prop('disabled', false);
            }
        });
        if ($('#warehouse_destination').val() != '') {
            validate_w_selected($('#warehouse_destination').val(), 'origin');
        }
    });

    function validate_w_selected(warehouse, label){
        $('#warehouse_'+label).find('option[value="' + warehouse + '"]').prop('disabled', true);
    }

    function formatStylePoint(x){
        return '<div class="text-center"><span class=""><strong>'+ x +'</strong></span></div>';
    }

</script>