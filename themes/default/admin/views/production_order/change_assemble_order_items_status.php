<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <!-- <a class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" href="<?= admin_url('products/view_adjustment_pos/'.$inv->id) ?>" target="_blank">
                <i class="fa fa-print"></i> POS
            </a> -->
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>" alt="<?= $Settings->site_name; ?>">
            </div>
            <div class="text-center" style="margin-bottom:20px;">
                <h3><?= lang('add_receipt_order_to_assemble_order') ?> N° <?= $inv->id ?></h3>
            </div>
            <div class="well well-sm">
                <div class="row bold">
                    <div class="col-xs-5">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?><br>
                        <?= lang("ref"); ?>: <?= $inv->reference_no; ?><br>
                    </p>
                    </div>
                    <div class="col-xs-7 text-right order_barcodes">

                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>

            <div class="col-xs-12">
                <div class="table-responsive">
                    <label class="table-label"><?= lang("pfinished_products_in_assemble_order"); ?> *</label>
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("description"); ?></th>
                                <th><?= lang("variant"); ?></th>
                                <th><?= lang("quantity"); ?></th>
                            </tr>
                        </thead>

                        <tbody>
                            <?php $r = 1;
                            $tTotal = 0;
                            foreach ($por_rows as $row):
                            ?>
                                <tr>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name; ?>
                                    </td>
                                    <th><?= $row->pfinished_variant; ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($row->assembling_quantity); $tTotal+= $row->assembling_quantity; ?></th>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="2" class="text-right"><?= lang('total') ?></th>
                                <th class="text-right"><?= $this->sma->formatQuantity($tTotal); ?></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="col-xs-12">
                <?php
                $attrib = array('id' => 'form_receipt');
                echo admin_form_open_multipart("production_order/change_assemble_order_items_status/".$inv->id, $attrib);
                ?>
                <div class="table-responsive">
                    <label class="table-label"><?= lang("components"); ?> *</label>
                    <table class="table table-bordered table-hover print-table order-table">
                        <thead>
                            <tr>
                                <th><?= lang("description"); ?></th>
                                <th><?= lang("product"); ?></th>
                                <th><?= lang("order_quantity"); ?></th>
                                <th><?= lang("finished_quantity"); ?></th>
                                <th><?= lang("fault_quantity"); ?></th>
                                <th><?= lang("in_process_quantity"); ?></th>
                                <th><?= lang("ok_quantity"); ?></th>
                                <th><?= lang("fault_quantity"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $r = 1;
                            $fqty = 0;
                            $pqty = 0;
                            $ftqty = 0;
                            foreach ($rows as $row):
                            ?>
                                <tr>
                                    <td style="vertical-align:middle;">
                                        <?= $row->product_code.' - '.$row->product_name; ?>
                                    </td>
                                    <td><?= $row->pfinished_name.' - '.$row->pfinished_code.($row->pfinished_variant ? ' - '.$row->pfinished_variant : ''); ?></td>
                                    <th class="text-right"><?= $this->sma->formatQuantity($row->assembling_quantity); ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($row->finished_quantity); $fqty+= $row->finished_quantity; ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($row->fault_quantity); $ftqty+= $row->fault_quantity; ?></th>
                                    <th class="text-right"><?= $this->sma->formatQuantity($row->assembling_quantity - $row->finished_quantity - $row->fault_quantity);  $pqty+= ($row->assembling_quantity - $row->finished_quantity - $row->fault_quantity);?></th>
                                    <th>
                                        <input type="hidden" name="item_id[]" value="<?= $row->id ?>">
                                        <input type="text" name="ok_quantity[]" id="ok_quantity_<?= $row->id ?>" class="form-control text-right ok_quantity" value="<?= ($row->assembling_quantity - $row->finished_quantity - $row->fault_quantity) ?>" onclick="$(this).select();" max="<?= $row->assembling_quantity - $row->finished_quantity - $row->fault_quantity ?>" min="0">
                                        <label class="error" for="ok_quantity_<?= $row->id ?>" style="display:none;"></label>
                                    </th>
                                    <th>
                                        <input type="text" name="fault_quantity[]" id="fault_quantity_<?= $row->id ?>" class="form-control text-right fault_quantity" value="0" onclick="$(this).select();" min="0" max="<?= $row->assembling_quantity - $row->finished_quantity - $row->fault_quantity ?>">
                                        <label class="error" for="fault_quantity_<?= $row->id ?>" style="display:none;"></label>
                                    </th>
                                </tr>
                                <?php
                                $r++;
                            endforeach;
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="text-right" colspan="3"><?= lang('total') ?></th>
                                <th class="text-right"><?= $this->sma->formatQuantity($fqty); ?></th>
                                <th class="text-right"><?= $this->sma->formatQuantity($ftqty); ?></th>
                                <th class="text-right"><?= $this->sma->formatQuantity($pqty); ?></th>
                                <th class="text-right"><span class="total_ok_quantity">0.00</span></th>
                                <th class="text-right"><span class="total_fault_quantity">0.00</span></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <button class="btn btn-primary submit_receipt" type="button"><?= lang('submit') ?></button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>

            <div class="row">
                <div class="col-xs-7">
                    <?php if ($inv->note || $inv->note != "") { ?>
                        <div class="well well-sm">
                            <p class="bold"><?= lang("note"); ?>:</p>
                            <div><?= $this->sma->decode_html($inv->note); ?></div>
                        </div>
                    <?php } ?>
                </div>

                <div class="col-xs-5 pull-right">
                    <div class="well well-sm">
                        <p>
                            <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                            <?= lang("date"); ?>: <?= $this->sma->hrld($inv->date); ?>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function(){
        $(document).on('click', '.submit_receipt', function(){
            if ($('#form_receipt').valid()) {
                $('#form_receipt').submit();
            }
        });
        $(document).on('keyup', '.ok_quantity', function(){
            findex = $('.ok_quantity').index($(this));
            fault_quantity = $('.fault_quantity').eq(findex);
            tt = parseFloat($(this).val()) + parseFloat(fault_quantity.val());
            if (parseFloat(tt) > parseFloat($(this).prop('max'))) {
                $(this).val(0);
                $(this).select();
                command: toastr.warning('La cantidad a entregar en conformidad más en averías, no puede ser mayor a la cantidad de la orden de ensamble', '¡Error!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "7000",
                            "extendedTimeOut": "1000",
                        });
            }
            var total_ok_quantity = 0;
            $.each($('.ok_quantity'), function(index, item){
                total_ok_quantity += parseFloat($(item).val());
            });
            $('.total_ok_quantity').text(formatQuantity2(total_ok_quantity));
        });
        $(document).on('keyup', '.fault_quantity', function(){
            findex = $('.fault_quantity').index($(this));
            ok_quantity = $('.ok_quantity').eq(findex);
            tt = parseFloat($(this).val()) + parseFloat(ok_quantity.val());
            if (parseFloat(tt) > parseFloat($(this).prop('max'))) {
                $(this).val(0);
                $(this).select();
                command: toastr.warning('La cantidad a entregar en averías más en conformidad, no puede ser mayor a la cantidad de la orden de ensamble', '¡Error!', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "7000",
                            "extendedTimeOut": "1000",
                        });
            }
            var total_fault_quantity = 0;
            $.each($('.fault_quantity'), function(index, item){
                total_fault_quantity += parseFloat($(item).val());
            });
            $('.total_fault_quantity').text(formatQuantity2(total_fault_quantity));
        });
        setTimeout(function() {
            $('.ok_quantity').trigger('keyup');
        }, 850);
    });

</script>