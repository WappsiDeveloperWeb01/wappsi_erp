<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    /* #PrRData td:nth-child(5), #PrRData td:nth-child(6), #PrRData td:nth-child(7), #PrRData td:nth-child(8) {
        text-align: right; width: 12%;
    }
    #PrRData td:nth-child(6) {
        font-weight: bold;
    } */
    .table>thead>tr>td.alert, .table>tbody>tr>td.alert, .table>tfoot>tr>td.alert, .table>thead>tr>th.alert, .table>tbody>tr>th.alert, .table>tfoot>tr>th.alert, .table>thead>tr.alert>td, .table>tbody>tr.alert>td, .table>tfoot>tr.alert>td, .table>thead>tr.alert>th, .table>tbody>tr.alert>th, .table>tfoot>tr.alert>th {
        background-color: #ffdaab;
    }
</style>

<?php
    $v = "";
    if ($this->input->post('sproduct')) {
        $v .= "&product=" . $this->input->post('sproduct');
    }
    // if ($this->input->post('variant')) {
    //     $v .= "&variant=" . $this->input->post('variant');
    // }
    if ($this->input->post('category')) {
        $cats_arr = $this->input->post('category');
        $cat_txt = "";
        foreach ($cats_arr as $key => $cat_id) {
            $cat_txt .= $cat_id.",";
        }
        $cat_txt = trim($cat_txt, ",");
        $v .= "&category=" . $cat_txt;
    }
    if ($this->input->post('brand')) {
        $v .= "&brand=" . $this->input->post('brand');
    }
    if ($this->input->post('subcategory')) {
        $v .= "&subcategory=" . $this->input->post('subcategory');
    }
    if ($this->input->post('warehouse')) {
        $v .= "&warehouse=" . $this->input->post('warehouse');
    }
    if ($this->input->post('start_date')) {
        $v .= "&start_date=" . $this->input->post('start_date');
    }
    if ($this->input->post('end_date')) {
        $v .= "&end_date=" . $this->input->post('end_date');
    }
    if ($this->input->post('show_only_invalid')) {
        $v .= "&show_only_invalid=" . $this->input->post('show_only_invalid');
    }
    if ($this->input->post('per_variant')) {
        $v .= "&per_variant=1";
    }
    if ($this->input->post('option_id')) {
        $v .= "&option_id=" . $this->input->post('option_id');
    }
?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                        <?=admin_form_open("reports/products", 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("product", "suggest_product"); ?>
                                        <?= form_input('sproduct', (isset($_POST['sproduct']) ? $_POST['sproduct'] : ""), 'class="form-control" id="suggestProduct"'); ?>
                                        <input type="hidden" name="product" value="<?= isset($_POST['product']) ? $_POST['product'] : "" ?>" id="report_product_id"/>
                                    </div>
                                </div>
                                <!-- <div class="col-sm-12 col-md-2">
                                    <div class="form-group">
                                        <?= lang("variant_code", "suggest_variant"); ?>
                                        <?= form_input('svariant', (isset($_POST['svariant']) ? $_POST['svariant'] : ""), 'class="form-control" id="suggestVariant"'); ?>
                                        <input type="hidden" name="variant" value="<?= isset($_POST['variant']) ? $_POST['variant'] : "" ?>" id="report_variant_id"/>
                                    </div>
                                </div> -->
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("category", "category") ?>
                                        <?php
                                        $cat = [];
                                        foreach ($categories as $category) {
                                            $cat[$category->id] = $category->name;
                                        }
                                        echo form_dropdown('category[]', $cat, '', 'class="form-control select" id="category" style="width:100%" multiple')
                                        ?>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <?= lang("subcategory", "subcategory") ?>
                                        <div class="controls" id="subcat_data"> <?php
                                            echo form_input('subcategory', (isset($_POST['subcategory']) ? $_POST['subcategory'] : ''), 'class="form-control" id="subcategory"  placeholder="' . sprintf(lang('select_category_to_load'), lang('category')) . '"');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("brand", "brand") ?>
                                        <?php
                                        $bt[''] = lang('select').' '.lang('brand');
                                        foreach ($brands as $brand) {
                                            $bt[$brand->id] = $brand->name;
                                        }
                                        echo form_dropdown('brand', $bt, (isset($_POST['brand']) ? $_POST['brand'] : ''), 'class="form-control select" id="brand" placeholder="' . lang("select") . " " . lang("brand") . '" style="width:100%"')
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                        <?php
                                        if ($this->session->userdata('warehouse_id')) {
                                            $wh = [];
                                        } else {
                                            $wh[""] = lang('select').' '.lang('warehouse');
                                        }
                                        foreach ($warehouses as $warehouse) {
                                            if ($this->session->userdata('warehouse_id') && $warehouse->id != $this->session->userdata('warehouse_id')) {
                                                continue;
                                            }
                                            $wh[$warehouse->id] = $warehouse->name;
                                        }
                                        echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                        ?>
                                    </div>
                                </div>

                                <div class="form-group col-md-2">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="control-label" for="seller"><?= sprintf(lang('per_variant'), lang('variant')) ?></label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label>
                                                <input type="checkbox" name="per_variant" id="per_variant" class="form-control">
                                            </label>
                                        </div>

                                        <?php $display = ($this->input->post('per_variant')) ? "block" : "none" ?>
                                        <div class="col-md-9 option_id_div" style="display: <?= $display ?>">
                                            <input type="text" name="option_id" id="option_id" class="form-control" placeholder="<?= lang("select") ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                    <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                    <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                        <?= $this->sma->get_filter_options(); ?>
                                    </select>
                                </div>

                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                        <div class="col-sm-2 form-group">
                                            <?= lang('filter_year', 'filter_year_dh') ?>
                                            <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                    <option value="<?= $key ?>"><?= $key ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang("start_date", "start_date_dh"); ?>
                                            <?= form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh"'); ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= lang("end_date", "end_date_dh"); ?>
                                            <?= form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh"'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <label>
                                        <input type="checkbox" name="show_only_invalid" id="show_only_invalid" <?= isset($_POST['show_only_invalid']) ? 'checked="checked"' : '' ?>>
                                        <?= lang('show_only_invalid') ?>
                                    </label>
                                </div>



                                <div class="col-sm-2 form-group">
                                    <div class="controls">
                                        <button id="submit_filter" class="btn btn-primary new-button" title="<?= lang('search') ?>" data-toggle="tooltip" data-placement="right">
                                            <i class="fa fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            <?= form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <?php
                                        if ($Owner || $Admin || $GP['bulk_actions']) {
                                            echo admin_form_open('products/product_actions', 'id="action-form"');
                                        }
                                    ?>
                                    <table id="PrRData" class="table table-condensed table-hover">
                                        <thead>
                                            <tr class="active">
                                                <th style="min-width:30px; width: 30px; text-align: center;">
                                                    <input class="checkbox checkth" type="checkbox" name="check"/>
                                                </th>
                                                <th><?= lang("product_code"); ?></th>
                                                <th><?= lang("product_name"); ?></th>
                                                <th><?= lang("reference"); ?></th>

                                                <?php if ($this->input->post('per_variant')) { ?>
                                                    <th><?= lang("variant_code"); ?></th>
                                                    <th><?= lang("variant"); ?></th>
                                                <?php } ?>

                                                <th><?= lang("initial_stock"); ?></th>
                                                <th><?= lang("purchased"); ?></th>
                                                <th><?= lang("sold"); ?></th>
                                                <th><?= lang("adjustment_positive"); ?></th>
                                                <th><?= lang("adjustment_negative"); ?></th>
                                                <th><?= lang("transfer_in"); ?></th>
                                                <th><?= lang("transfer_out"); ?></th>
                                                <th><?= lang("stock_in_hand"); ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="6" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
                                        <div style="display: none;">
                                            <input type="hidden" name="form_action" value="" id="form_action"/>
                                            <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
                                        </div>
                                        <?= form_close() ?>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        function spb(x) {
            v = x.split('__');
            return '('+formatQuantity2(v[0])+') <strong>'+formatMoney(v[1])+'</strong>';
        }

        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
            var oTable = $('#PrRData').dataTable({
                aaSorting: [[1, "asc"]],
                aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
                iDisplayLength: <?= $Settings->rows_per_page ?>,
                bProcessing: true,
                bServerSide: true,
                dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
                sAjaxSource: '<?= admin_url('reports/getProductsReport2/?v=1'.$v) ?>',
                fnServerData: function (sSource, aoData, fnCallback) {
                    aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    });
                    $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
                },
                fnRowCallback: function (nRow, aData, iDisplayIndex) {
                    nRow.id = aData[0];
                    if (aData[12] == '0' && '<?= $this->Owner ?>' == '1' && aData[13] != 'combo') {
                        if (aData[14] > 0) {
                            nRow.className = "product_link2 alert";
                        } else {
                            nRow.className = "product_link2 danger";
                        }
                    } else {
                        nRow.className = "product_link2";
                    }
                    return nRow;
                },
                columns: [
                    {"bSortable": false, "mRender": checkbox, "className": "text-center"},
                    { "data": 1, },
                    {
                        "data": 2,
                        "bSortable": false
                    },
                    {
                        "data": 3,
                        "bSortable": false
                    },

                    <?php if ($this->input->post('per_variant')) { ?>
                        {
                            "className": "text-center",
                            "bSortable": false,
                            "data": 4,
                        },
                        {
                            "className": "text-center",
                            "bSortable": false,
                            "data": 5,
                        },
                    <?php } ?>

                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[9])
                            <?php } else { ?>
                                return productMovementFormat(row[7])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[10])
                            <?php } else { ?>
                                return productMovementFormat(row[8])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[11])
                            <?php } else { ?>
                                return productMovementFormat(row[9])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[12])
                            <?php } else { ?>
                                return productMovementFormat(row[10])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[13])
                            <?php } else { ?>
                                return productMovementFormat(row[11])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[14])
                            <?php } else { ?>
                                return productMovementFormat(row[12])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[15])
                            <?php } else { ?>
                                return productMovementFormat(row[13])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                    {
                        mRender: function (data, type, row) {
                            <?php if ($this->input->post('per_variant')) { ?>
                                return productMovementFormat(row[16])
                            <?php } else { ?>
                                return productMovementFormat(row[14])
                            <?php } ?>
                        },
                        className: "text-right",
                        bSortable: false,
                    },
                ],
                // buttons : [{extend:'excel', title:'Afiliados', className:'btnExportarExcel', exportOptions: {columns : [1,2,3,4,5,6,7,8,9,10,12]}}],
                fnDrawCallback: function (oSettings) {
                    $('.actionsButtonContainer').html(`<div class="pull-right dropdown">
                        <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>">
                            <i class="fas fa-ellipsis-v fa-lg"></i>
                        </button>
                        <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>">
                                    <i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="pos" class="tip" title="<?= lang('print_pos') ?>">
                                    <i class="icon fa fa-file-excel-o"></i> <?= lang('print_pos') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="image" class="tip" title="<?= lang('save_image') ?>">
                                    <i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="sync_quantity" data-action="sync_quantity">
                                    <i class="icon fa fa-refresh"></i> <?= lang('sync_quantity') ?>
                                </a>
                            </li>
                        </ul>
                    </div>`);

                    $('[data-toggle-second="tooltip"]').tooltip();
                    $('input[type="checkbox"]').iCheck({
                        checkboxClass: 'icheckbox_flat-blue',
                        increaseArea: '20%'
                    });
                }
            }).fnSetFilteringDelay().dtFilter([
                {column_number: 1, filter_default_label: "[<?=lang('product_code');?>]", filter_type: "text", data: []},
                {column_number: 2, filter_default_label: "[<?=lang('product_name');?>]", filter_type: "text", data: []},
            ], "footer");
            <?php endif ?>


            $('#form').hide();
            $('.toggle_down').click(function () {
                $("#form").slideDown();
                return false;
            });
            $('.toggle_up').click(function () {
                $("#form").slideUp();
                return false;
            });

            // $('#category').select2({allowClear: true, placeholder: "<?= lang('select'); ?>", minimumResultsForSearch: 7}).select2('destroy');
            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>").select2({
                allowClear: true,
                placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>", data: [
                    {id: '', text: '<?= sprintf(lang('select_category_to_load'), lang('category')) ?>'}
                ]
            });
            $('#category').change(function () {
                var v = $(this).val();
                if (v) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: "<?= admin_url('products/getMultipleSubCategories') ?>/" + v,
                        dataType: "json",
                        success: function (scdata) {
                            if (scdata != null) {
                                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_subcategory'), lang('subcategory')) ?>").select2({allowClear: true,
                                    placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                                    data: scdata
                                });
                            } else {
                                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>").select2({allowClear: true,
                                    placeholder: "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>",
                                    data: [{id: '', text: '<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>'}]
                                });
                            }
                        },
                        error: function () {
                            bootbox.alert('<?= lang('ajax_error') ?>');
                        }
                    });
                } else {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>").select2({allowClear: true,
                        placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                        data: [{id: '', text: '<?= sprintf(lang('select_category_to_load'), lang('category')) ?>'}]
                    });
                }
            });
        <?php if (isset($_POST['category']) && !empty($_POST['category'])) { ?>
        var cat_values="<?= $cat_txt; ?>";

        $.each(cat_values.split(","), function(i,e){
            $("#category option[value='" + e + "']").prop("selected", true);
        });
        $.ajax({
            type: "get", async: false,
            url: "<?= admin_url('products/getSubCategories') ?>/" + "<?= $cat_txt ?>",
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_subcategory'), lang('subcategory')) ?>").select2({allowClear: true,
                        placeholder: "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>",
                        data: scdata
                    });
                }
            }
        });
        <?php } ?>

        <?php if ($this->session->userdata('warehouse_id')): ?>
            setTimeout(function() {
                $('#warehouse').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>

        $('input[type="checkbox"]').iCheck({
            checkboxClass: 'icheckbox_flat-blue',
            increaseArea: '20%'
        });

        $(document).on('click', '#pdf', function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getProductsReport2/pdf/?v=1'.$v)?>";
            return false;
        });
        $(document).on('click', '#xls', function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getProductsReport2/0/xls/?v=1'.$v)?>";
            return false;
        });
        $(document).on('click', '#image', function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $(document).on('click', '#pos', function (event) {
            event.preventDefault();
            window.open("<?=admin_url('reports/getProductsReport2Pos/?v=1'.$v)?>", "_blank");
            return false;
        });
        $(document).on('click', '#submit_filter', function (event) {
            // e.preventDefault();
            $('#filter_action').val(1);
            $('#sales_filter').submit();
        });

        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);

        <?php if ($this->input->post('per_variant')): ?>
            $('#per_variant').iCheck('check');
        <?php endif ?>

        $(document).on('ifChecked', '#per_variant', function(){
            $('.option_id_div').fadeIn();
        });
        $(document).on('ifUnchecked', '#per_variant', function(){
            $('.option_id_div').fadeOut();
        });

        autocompleteVariants()
        autocompleteProducts()
    });

    function autocompleteVariants()
    {
        $('#option_id').val("<?= $this->input->post('option_id') ?>").select2({
            minimumInputLength: 1,
            data: [],
            allowClear: true,
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"products/getVariant/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "products/variants_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }

    function autocompleteProducts()
    {
        $('#suggestProduct').val('<?= $this->input->post("sproduct") ?>').select2({
            minimumInputLength: 2,
            data: [],
            placeholder: '<?= lang('select') ?>',
            allowClear: true,
            initSelection: function (element, callback) {
                $.ajax({
                    type: `get`,
                    async: false,
                    url: `${site.base_url}products/getProduct/${$(element).val()}`,
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: `${site.base_url}products/productsSuggestions`,
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    }
                },
                results: function (data, page) {
                    if (data != null) {
                        return {results: data};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    }
</script>
