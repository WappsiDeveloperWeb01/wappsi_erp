<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>
<script type="text/javascript">
    var count = 1,
        an = 1,
        product_variant = 0,
        DT = <?= $Settings->default_tax_rate ?>,
        allow_discount = <?= ($Owner || $Admin || $this->session->userdata('allow_discount')) ? 1 : 0; ?>,
        product_tax = 0,
        invoice_tax = 0,
        total_discount = 0,
        total = 0,
        shipping = 0,
        tax_rates = <?php echo json_encode($tax_rates); ?>;
    var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
    var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');
    var qutype = 2;
    $(document).ready(function() {
        <?php if ($this->input->get('customer')) { ?>
            if (!localStorage.getItem('quitems')) {
                localStorage.setItem('qucustomer', <?= $this->input->get('customer'); ?>);
            }
        <?php } ?>
        <?php if ($Owner || $Admin) { ?>
            if (!localStorage.getItem('qudate')) {
                $("#qudate").datetimepicker({
                    format: site.dateFormats.js_ldate,
                    fontAwesome: true,
                    language: 'sma',
                    weekStart: 1,
                    todayBtn: 1,
                    autoclose: 1,
                    todayHighlight: 1,
                    startView: 2,
                    forceParse: 0
                }).datetimepicker('update', new Date());
            }
            $(document).on('change', '#qudate', function(e) {
                localStorage.setItem('qudate', $(this).val());
            });
            if (qudate = localStorage.getItem('qudate')) {
                $('#qudate').val(qudate);
            }
        <?php } ?>
        $(document).on('change', '#qubiller', function(e) {
            localStorage.setItem('qubiller', $(this).val());
        });
        if (qubiller = localStorage.getItem('qubiller')) {
            $('#qubiller').val(qubiller);
        }
        if (!localStorage.getItem('qutax2')) {
            localStorage.setItem('qutax2', <?= $Settings->purchase_tax_rate; ?>);
        }
        ItemnTotals();
        $("#add_item").autocomplete({
            source: function(request, response) {
                if (!$('#qusupplier').val()) {
                    var msg = "";
                    if (!$('#qusupplier').val()) {
                        msg += "</br><?= lang('supplier') ?>";
                    }
                    $('#add_item').val('').removeClass('ui-autocomplete-loading');
                    command: toastr.warning('<?= lang('select_above'); ?> : ' + msg, '¡Atención!', {
                        "showDuration": "1200",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                    //response('');
                    $('#add_item').focus();
                    return false;
                }
                $.ajax({
                    type: 'get',
                    url: '<?= admin_url('quotes/psuggestions'); ?>',
                    dataType: "json",
                    data: {
                        term: request.term,
                        warehouse_id: $("#quwarehouse").val(),
                        supplier_id: $("#qusupplier").val(),
                        type_quote_purchase: $("#type_quote_purchase").val()
                    },
                    success: function(data) {
                        $(this).removeClass('ui-autocomplete-loading');
                        response(data);
                    }
                });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function(event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function() {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');
                } else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                } else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_match_found') ?>', function() {
                        $('#add_item').focus();
                    });
                    $(this).removeClass('ui-autocomplete-loading');
                    $(this).val('');

                }
            },
            select: function(event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var row = add_invoice_item(ui.item);
                    if (row)
                        $(this).val('');
                } else {
                    bootbox.alert('<?= lang('no_match_found') ?>');
                }
            }
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <?= admin_form_open_multipart("quotes/addqpurchase", array('data-toggle' => 'validator', 'role' => 'form')) ?>
                            <?php if ($Owner || $Admin) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("date", "qudate"); ?>
                                        <?php echo form_input('date', (isset($_POST['date']) ? $_POST['date'] : ""), 'class="form-control input-tip datetime" id="qudate" required="required"'); ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("reference_no", "quref"); ?>
                                    <select name="document_type_id" class="form-control" id="document_type_id" required="required"></select>
                                </div>
                            </div>
                            <?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <?= lang("biller", "qubiller"); ?>
                                        <?php
                                        $bl[""] = "";
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        }
                                        echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $Settings->default_biller), 'id="qubiller" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("biller") . '" required="required" class="form-control input-tip select" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            <?php } else {
                                $biller_input = array(
                                    'type' => 'hidden',
                                    'name' => 'biller',
                                    'id' => 'qubiller',
                                    'value' => $this->session->userdata('biller_id'),
                                );
                                echo form_input($biller_input);
                            } ?>

                            <div class="clearfix"></div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("supplier", "qusupplier"); ?>
                                    <input type="hidden" name="supplier" value="" id="qusupplier" class="form-control" style="width:100%;" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" required='required'>
                                    <input type="hidden" name="supplier_id" value="" id="supplier_id" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="row">
                                    <?php if (count($currencies) > 1) : ?>
                                        <div class="col-md-6 form-group">
                                            <?= lang('currency', 'currency') ?>
                                            <select name="currency" class="form-control" id="currency">
                                                <?php foreach ($currencies as $currency) : ?>
                                                    <option value="<?= $currency->code ?>" data-rate="<?= $currency->rate ?>" <?= $currency->code == $this->Settings->default_currency ? "selected='selected'" : "" ?>><?= $currency->code ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    <?php else : ?>
                                        <input type="hidden" name="currency" value="<?= $this->Settings->default_currency ?>" id="currency">
                                    <?php endif ?>
                                    <div class="col-md-6 form-group trm-control" style="display: none;">
                                        <?= lang('trm', 'trm') ?>
                                        <input type="number" name="trm" id="trm" class="form-control" required="true" step="0.001">
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="type_quote_purchase" id="type_quote_purchase" value="2">

                            <div class="col-md-12" id="sticker">
                                <div class="well well-sm">
                                    <div class="form-group" style="margin-bottom:0;">
                                        <div class="input-group wide-tip">
                                            <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                <i class="fa fa-2x fa-barcode addIcon"></i></a>
                                            </div>
                                            <?php echo form_input('add_item', '', 'class="form-control input-lg" id="add_item" placeholder="' . $this->lang->line("add_expense_to_order") . '"'); ?>
                                            <?php if ($Owner || $Admin || $GP['products-add']) { ?>
                                                <div class="input-group-addon" style="padding-left: 10px; padding-right: 10px;">
                                                    <a href="#" id="addManually" class="tip" title="<?= lang('add_product_manually') ?>"><i class="fa fa-2x fa-plus-circle addIcon" id="addIcon"></i></a>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-12">
                                <div class="control-group table-group">

                                    <div class="controls table-controls">
                                        <table id="quTable" class="table items  table-bordered table-condensed table-hover sortable_table">
                                            <thead>
                                                <tr>
                                                    <th class="col-md-4"><?= lang('product') . ' (' . lang('code') . ' - ' . lang('name') . ')'; ?></th>
                                                    <th class="col-md-1"><?= lang("expense_cost"); ?> (<span class="currency"><?= $default_currency->code ?></span>)</th>
                                                    <th class="col-md-1"><?= lang("quantity"); ?></th>
                                                    <!--  <?php
                                                            if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {
                                                                echo '<th class="col-md-1">' . $this->lang->line("discount") . ' (<span class="currency">' . $default_currency->code . '</span>)  </th>';
                                                            }
                                                            ?> -->
                                                    <?php
                                                    if ($Settings->tax1) {
                                                        echo '<th class="col-md-2">' . $this->lang->line("tax_1") . ' (<span class="currency">' . $default_currency->code . '</span>)  </th>';
                                                        echo '<th class="col-md-2">' . $this->lang->line("tax_2") . ' (<span class="currency">' . $default_currency->code . '</span>)  </th>';
                                                    }
                                                    ?>
                                                    <th><?= lang("subtotal"); ?> (<span class="currency"><?= $default_currency->code ?></span>)
                                                    </th>
                                                    <th style="width: 30px !important; text-align: center;"><i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i></th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                            <tfoot></tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <input type="hidden" name="total_items" value="" id="total_items" required="required" />

                            <div class="col-md-4">
                                <label onclick="$('#rtModal').modal('show');" style="cursor: pointer;"><span class="fa fa-pencil"></span> Retención </label>
                                <input type="text" name="retencion_show" id="retencion_show" class="form-control text-right" readonly>
                                <input type="hidden" name="retencion" id="rete">
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("status", "qustatus"); ?>
                                    <?php $st = array('pending' => lang('pending'), 'sent' => lang('sent'));
                                    echo form_dropdown('status', $st, '', 'class="form-control input-tip" id="qustatus"'); ?>

                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("quote_payment_status", "qu_payment_status"); ?>
                                    <?php $st = array('1' => lang('payment_type_cash'), '0' => lang('due'));
                                    echo form_dropdown('payment_status', $st, '', 'class="form-control input-tip" id="qu_payment_status"'); ?>

                                </div>
                            </div>

                            <div class="col-md-4 div_payment_term" style="display: none;">
                                <div class="form-group">
                                    <?= lang("quote_payment_term", "qu_payment_term"); ?>
                                    <?php echo form_input('payment_term', 1, 'class="form-control input-tip" id="qu_payment_term"'); ?>

                                </div>
                            </div>

                            <div class="col-sm-4 div_paid_by">
                                <div class="form-group">
                                    <?= lang("paying_by", "paid_by_1"); ?>
                                    <select name="paid_by" id="paid_by_1" data-pbnum="1" class="form-control paid_by">
                                        <?= $this->sma->paid_opts(null, true); ?>
                                    </select>
                                </div>
                            </div>


                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang("document", "document") ?>
                                    <input id="document" type="file" data-browse-label="<?= lang('browse'); ?>" name="document" data-show-upload="false" data-show-preview="false" class="form-control file">
                                </div>
                            </div>

                            <div class="row" id="bt">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <?= lang("note", "qunote"); ?>
                                            <?php echo form_textarea('note', (isset($_POST['note']) ? $_POST['note'] : ""), 'class="form-control" id="qunote" style="margin-top: 10px; height: 100px;"'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="col-sm-12">
                                <div class="fprom-group"><?php echo form_submit('add_quote', $this->lang->line("submit"), 'id="add_quote" class="btn btn-primary" style="padding: 6px 15px; margin:15px 0;"'); ?>
                                    <button type="button" class="btn btn-danger" id="reset"><?= lang('reset') ?>
                                </div>
                            </div>


                            <!-- <div id="bottom-total" class="well well-sm" style="margin-bottom: 0;">
                                <table class="table table-bordered table-condensed totals" style="margin-bottom:0;">
                                    <tr class="warning">
                                        <td><?= lang('items') ?> <span class="totals_val pull-right" id="titems">0</span></td>
                                        <?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>
                                        <td colspan="2"><?= lang('order_discount') ?> (<span class="currency"><?= $default_currency->code ?></span>)   <span class="totals_val pull-right" id="tds">0.00</span></td>
                                        <?php } ?>
                                        <?php if ($Settings->tax2) { ?>
                                            <td><?= lang('order_tax') ?> (<span class="currency"><?= $default_currency->code ?></span>)   <span class="totals_val pull-right" id="ttax2">0.00</span></td>
                                        <?php } ?>
                                        <td><?= lang('shipping') ?> (<span class="currency"><?= $default_currency->code ?></span>)   <span class="totals_val pull-right" id="tship">0.00</span></td>
                                        <td><?= lang('grand_total') ?> (<span class="currency"><?= $default_currency->code ?></span>)   <span class="totals_val pull-right" id="gtotal">0.00</span></td>
                                    </tr>
                                </table>
                            </div> -->

                            <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                                            <h4 class="modal-title" id="rtModalLabel"><?= lang('edit_order_tax'); ?></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>Retención</label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <label>Opción</label>
                                                </div>
                                                <div class="col-sm-2">
                                                    <label>Porcentaje</label>
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>Valor</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> Fuente
                                                    </label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                                                    <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_fuente_valor_show" id="rete_fuente_valor_show" class="form-control text-right" readonly>
                                                    <input type="hidden" name="rete_fuente_valor" id="rete_fuente_valor">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                                                    </label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                                                    <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_iva_valor_show" id="rete_iva_valor_show" class="form-control text-right" readonly>
                                                    <input type="hidden" name="rete_iva_valor" id="rete_iva_valor">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                                                    </label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                                                    <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_ica_valor_show" id="rete_ica_valor_show" class="form-control text-right" readonly>
                                                    <input type="hidden" name="rete_ica_valor" id="rete_ica_valor">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>
                                                        <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                                                    </label>
                                                </div>
                                                <div class="col-sm-5">
                                                    <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                                                        <option>Seleccione...</option>
                                                    </select>
                                                    <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                                                    <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                                                </div>
                                                <div class="col-sm-2">
                                                    <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                                                </div>
                                                <div class="col-sm-3">
                                                    <input type="text" name="rete_otros_valor_show" id="rete_otros_valor_show" class="form-control text-right" readonly>
                                                    <input type="hidden" name="rete_otros_valor" id="rete_otros_valor">
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-8 text-right">
                                                    <label>Total : </label>
                                                </div>
                                                <div class="col-md-4 text-right">
                                                    <label id="total_rete_amount" style="display: none;"> 0.00 </label>
                                                    <label id="total_rete_amount_show"> 0.00 </label>
                                                </div>
                                                <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="cancelOrderRete" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                                            <button type="button" id="updateOrderRete" class="btn btn-primary"><?= lang('update') ?></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="prModalLabel"></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group pname-div">
                        <label for="pname" class="col-sm-4 control-label"><?= lang('name') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pname">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pcost" class="col-sm-4 control-label"><?= lang('expense_cost') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pcost" <?= ($Owner || $Admin || $GP['edit_cost']) ? '' : 'readonly'; ?>>
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label class="col-sm-12"><?= lang('tax_1') ?></label>
                            <div class="col-sm-6">
                                <?php
                                $tr[""] = lang('n/a');
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax', $tr, "", 'id="ptax" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>
                            <div class="col-sm-6">
                                <?= form_input('ptax_value', '', 'id="ptax_value" class="form-control"'); ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-12"><?= lang('tax_2') ?></label>
                            <div class="col-sm-6">
                                <?php
                                $tr[""] = lang('n/a');
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('ptax_2', $tr, "", 'id="ptax_2" class="form-control pos-input-tip" style="width:100%;"');
                                ?>
                            </div>

                            <div class="col-sm-6">
                                <?= form_input('ptax_2_value', '', 'id="ptax_2_value" class="form-control"'); ?>
                            </div>
                        </div>
                    <?php } ?>
                    <!-- <div class="form-group">
                        <label for="pquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="pquantity" readonly>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                        <div class="col-sm-8">
                            <div id="punits-div"></div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="poption" class="col-sm-4 control-label"><?= lang('product_option') ?></label>
                        <div class="col-sm-8">
                            <div id="poptions-div"></div>
                        </div>
                    </div> -->
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <!-- <div class="form-group">
                            <label for="pdiscount" class="col-sm-4 control-label"><?= lang('product_discount') ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pdiscount">
                            </div>
                        </div> -->
                    <?php } ?>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:16.66%;"><?= lang('expense_cost'); ?></th>
                            <th style="width:16.66%;"><span id="net_cost"></span></th>
                            <th style="width:16.66%;"><?= lang('expense_tax'); ?></th>
                            <th style="width:16.66%;"><span id="pro_tax"></span></th>
                            <th style="width:16.66%;"><?= lang('total'); ?></th>
                            <th style="width:16.66%;"><span id="pro_total"></span></th>
                        </tr>
                    </table>
                    <input type="hidden" id="punit_cost" value="" />
                    <input type="hidden" id="old_tax" value="" />
                    <input type="hidden" id="old_qty" value="" />
                    <input type="hidden" id="old_cost" value="" />
                    <input type="hidden" id="row_id" value="" />
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="editItem"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i class="fa fa-2x">&times;</i></span><span class="sr-only"><?= lang('close'); ?></span></button>
                <h4 class="modal-title" id="mModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="mcode" class="col-sm-4 control-label"><?= lang('product_code') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcode">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="mname" class="col-sm-4 control-label"><?= lang('product_name') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mname">
                        </div>
                    </div>
                    <?php if ($Settings->tax1) { ?>
                        <div class="form-group">
                            <label for="mtax" class="col-sm-4 control-label"><?= lang('product_tax') ?> *</label>

                            <div class="col-sm-8">
                                <?php
                                $tr[""] = "";
                                foreach ($tax_rates as $tax) {
                                    $tr[$tax->id] = $tax->name;
                                }
                                echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control input-tip select" style="width:100%;"');
                                ?>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mquantity" class="col-sm-4 control-label"><?= lang('quantity') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mquantity">
                        </div>
                    </div>
                    <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
                        <div class="form-group">
                            <label for="mdiscount" class="col-sm-4 control-label"><?= lang('product_discount') ?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="mdiscount">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="form-group">
                        <label for="mcost" class="col-sm-4 control-label"><?= lang('unit_cost') ?> *</label>

                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="mcost">
                        </div>
                    </div>
                    <table class="table table-bordered">
                        <tr>
                            <th style="width:25%;"><?= lang('net_unit_cost'); ?></th>
                            <th style="width:25%;"><span id="mnet_cost"></span></th>
                            <th style="width:25%;"><?= lang('product_tax'); ?></th>
                            <th style="width:25%;"><span id="mpro_tax"></span></th>
                        </tr>
                    </table>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="addItemManually"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $(document).on('change', '#qubiller', function(e) {
            $.ajax({
                url: '<?= admin_url("billers/getBillersDocumentTypes/10/") ?>' + $('#qubiller').val(),
                type: 'get',
                dataType: 'JSON'
            }).done(function(data) {
                response = data;

                $('#document_type_id').html(response.options).select2();
                if (response.not_parametrized != "") {
                    command: toastr.warning('Los documentos <b> (' + response.not_parametrized + ') no están parametrizados </b> en contabilidad', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                }

                if (response.status == 0) {
                    $('.resAlert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
                    localStorage.setItem('locked_for_biller_resolution', 1);
                }
                $('#document_type_id').trigger('change');
            });
        });

        setTimeout(function() {
            $('#qubiller').trigger('change');
        }, 1000);

    });
</script>