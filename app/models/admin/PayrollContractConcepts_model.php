<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PayrollContractConcepts_model extends CI_Model {

    public function find($data)
    {
        $this->db->where($data);
		$q = $this->db->get('payroll_contract_concepts');
		return $q->row();
    }

    public function get($data)
    {
        $this->db->where($data);
		$this->db->join('payroll_concepts', 'concept_id = payroll_concepts.id', 'inner');
		$q = $this->db->get('payroll_contract_concepts');
		return $q->result();
    }

	// public function get_by_contract_id($contract_id)
	// {
	// 	$this->db->where('contract_id', $contract_id);
	// 	$this->db->join('payroll_concepts', 'concept_id = payroll_concepts.id', 'inner');
	// 	$q = $this->db->get('payroll_contract_concepts');
	// 	return $q->result();
	// }

    public function insert($data)
	{
		if ($this->db->insert('payroll_contract_concepts', $data)) {
			return TRUE;
		}
		return FALSE;
	}

	public function insert_batch($data)
	{
		if ($this->db->insert_batch('payroll_contract_concepts', $data) > 0) {
			return TRUE;
		}
		return FALSE;
	}

	public function update($data, $contractId, $employeeId, $conceptId = []) {
		$this->db->where('contract_id', $contractId);
		$this->db->where('employee_id', $employeeId);
        if (!empty($conceptId)) {
    		$this->db->where('concept_id', $conceptId);
        }
		if ($this->db->update('payroll_contract_concepts', $data)) {
			return TRUE;
		}
		return FALSE;
	}

	public function delete($data, $concepts = [])
	{
        if (!empty($concepts)) {
            $this->db->where_not_in("concept_id", $concepts);
        }

        $this->db->where($data);
		if ($this->db->delete('payroll_contract_concepts')) {
			return TRUE;
		}
		return FALSE;
	}
}

/* End of file PayrollContractConcepts_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/PayrollContractConcepts_model.php */