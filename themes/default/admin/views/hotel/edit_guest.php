<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
        </button>
        <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_guest'); ?></h4>
      </div>
      <?= admin_form_open_multipart("hotel/edit_guest/".$id, ['id' => 'add-customer-form']); ?>
        <?= form_hidden('document_code'); ?>
        <div class="modal-body">
          <p><?= lang('enter_info'); ?></p>
          
          <div class="row">
            <div class="form-group col-md-6">
              <?= lang("id_document_type", "id_document_type"); ?>
              <select class="form-control select" name="document_type_id" id="tipo_documento" style="width: 100%;" required="required">
                <option value=""><?= lang("select"); ?></option>
                <?php foreach ($id_document_types as $idt) : ?>
                  <option value="<?= $idt->id; ?>" data-code="<?= $idt->codigo_doc; ?>" <?= $guest->document_type_id == $idt->id ? "selected" : "" ?>><?= lang($idt->nombre); ?></option>
                <?php endforeach ?>
              </select>
            </div>

            <div class="form-group col-md-6">
                <?= lang("vat_no", "vat_no"); ?>
                <?php echo form_input('vat_no', $guest->vat_no, 'class="form-control" id="vat_no" required="required"'); ?>
            </div>
          </div>
            <div class="form-group">
              <div class="row">
                <div class="col-md-6">
                  <label>Nombres</label><span class='input_required'> *</span>
                  <input type="text" name="names" value="<?= $guest->names ?>" class="form-control" required="required">
                </div>
                <div class="col-md-6">
                  <label>Apellidos</label>
                  <input type="text" name="surnames" class="form-control" required="required" value="<?= $guest->surnames ?>">
                </div>
              </div>
            </div>
          <div class="row">
            <div class="form-group col-md-6">
                <?= lang("email_address", "email_address"); ?>
                <input type="email" name="email" class="form-control" id="email_address" required="required" value="<?= $guest->email ?>">
            </div>
            <div class="form-group col-md-6">
                <?= lang("phone", "phone"); ?>
                <input type="tel" name="phone" class="form-control" required="required" id="phone" value="<?= $guest->phone ?>"/>
            </div>
          </div>
          <div class="row">
            <div class="form-group col-md-6">
                <?= lang("nationality", "nationality"); ?>
                <input type="text" name="nationality" class="form-control" id="nationality" required="required" value="<?= $guest->nationality ?>">
            </div>
            <div class="form-group col-md-6">
                <?= lang("birth_date", "birth_date"); ?>
                <input type="date" name="birth_date" class="form-control" id="birth_date" required="required"  value="<?= date('Y-m-d', strtotime($guest->birth_date)) ?>">
            </div>
          </div>
        </div>
        <div class="modal-footer">
            <input type="hidden" name="edit_guest" value="1">
            <button class="btn btn-primary" id="submit_edit_guest" type="button"><?= lang('edit_guest') ?></button>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
  $(document).ready(function (e) {
      $("#add-customer-form").validate({
          ignore: []
      });
      $(document).on('click', '#submit_edit_guest', function(){
        form = $('#add-customer-form');
        if (form.valid()) {
          form.submit();
        }
      });
      $('select.select').select2({minimumResultsForSearch: 7});
    });

    $(document).on('keyup', '#vat_no', function(){
        
    });
</script>
<?= $modal_js ?>