<?php defined('BASEPATH') or exit('No direct script access allowed');
class Sales extends MY_Controller
{
    private $integration = [];
    private $dianStates = [];
    private $saleStatus = [];
    private $paymentStatus = [];
    private $electronicEmailStates = [];

    public function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->lang->admin_load('sales', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('sales_model');
        $this->load->admin_model('products_model');
        $this->load->admin_model('pos_model');
        $this->load->admin_model('quotes_model');
        $this->load->admin_model('settings_model');
        $this->load->admin_model("currency_model");
        $this->load->admin_model("Electronic_billing_model");
        $this->load->admin_model('companies_model');
        $this->load->admin_model('returns_model');
        $this->load->admin_model('TaxSecondary_model');

        $this->load->integration_model('User');
        $this->load->integration_model('Address');
        $this->load->integration_model('Order');
        $this->load->integration_model('OrderDetail');
        $this->load->integration_model('CombinedOrder');
        $this->load->integration_model('Product');
        $this->load->integration_model('ProductVariation');

        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '3072';
        $this->data['logo'] = true;
        $this->paymentStatus = [
            ['id' => 'pending', 'name' => $this->lang->line('pending')],
            ['id' => 'due', 'name' => $this->lang->line('due')],
            ['id' => 'partial', 'name' => $this->lang->line('partial')],
            ['id' => 'paid', 'name' => $this->lang->line('paid')]
        ];
        $this->dianStates = [
            ['id' => "0", 'name' => $this->lang->line('not_sended')],
            ['id' => "1", 'name' => $this->lang->line('pending')],
            ['id' => "2", 'name' => $this->lang->line('acepted')],
            ['id' => "3", 'name' => $this->lang->line('sent')],
            ['id' => "4", 'name' => $this->lang->line('not_sended').' y '. $this->lang->line('pending')]
        ];
        $this->electronicEmailStates = [
            ['id' => "0", 'name' => $this->lang->line('pending')],
            ['id' => "1", 'name' => $this->lang->line('sended')]
        ];
        $this->saleStatus = [
            ['id'=>'pending', 'name'=> $this->lang->line('pending')],
            ['id'=>'partial', 'name'=> $this->lang->line('partial')],
            ['id'=>'enlistment', 'name'=> $this->lang->line('enlistment')],
            ['id'=>'completed', 'name'=> $this->lang->line('invoiced')],
            ['id'=>'sent', 'name'=> $this->lang->line('sent')],
            ['id'=>'delivered', 'name'=> $this->lang->line('delivered')],
            ['id'=>'cancelled', 'name'=> $this->lang->line('cancelled')]
        ];
        if (!$this->session->userdata('register_id')) {
            if ($register = $this->pos_model->registerData($this->session->userdata('user_id'))) {
                $register_data = array('register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date);
                $this->session->set_userdata($register_data);
            }
        }
    }

    public function index($warehouse_id = null)
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }
        if ($this->input->post('dias_vencimiento')) {
            $this->data['dias_vencimiento'] = $this->input->post('dias_vencimiento');
        }
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documents_types'] = $this->site->get_document_types_by_module(2);
        $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
        $this->data['users'] = $this->site->get_all_users();
        $this->data['advancedFiltersContainer'] = FALSE;

        $this->page_construct('sales/index', ['page_title' => lang('sales')], $this->data);
    }

    public function fe_index($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();

        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['users'] = $this->site->get_all_users();
        $this->data['salesOrigins'] = $this->site->getSalesOrigins();
        $this->data['salesCampaigns'] = $this->site->getSalesCampaigns();
        $this->data['paymentsMethods'] = $this->site->getPaymentsMethods();
        $this->data['dianStates'] = json_decode(json_encode($this->dianStates));
        $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['documentsTypes'] = $this->site->getDocumentsTypeByBillerId($this->input->post('biller_id'), [2, 4, 27], YES);
        $this->data['paymentsStatus'] = json_decode(json_encode($this->paymentStatus));
        $this->data['electronicEmailStates'] = json_decode(json_encode($this->electronicEmailStates));

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }
        if ($this->input->post('dias_vencimiento')) {
            $this->data['dias_vencimiento'] = $this->input->post('dias_vencimiento');
        }

        $pending_sales_dian = $this->site->get_pending_sales_dian();
        if ($pending_sales_dian->quantity > 0) {
            $this->data['pending_electronic_document_message'] = sprintf(lang('pending_electronic_document_message'), $this->Settings->days_after_current_date);
        }

        $validationElectronicHist = $this->site->validationElectronicInvoice();
        if ($validationElectronicHist->response == false) {
            $this->data['validationElectronicHist'] = $validationElectronicHist->message;
        }

        if ($this->input->post('warehouse_id') || $this->input->post('payment_status') || $this->input->post('payment_method')
            || $this->input->post('fe_aceptado') || $this->input->post('fe_correo_enviado') || $this->input->post('filter_user')
            || $this->input->post('client')) {
            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $this->page_construct('sales/fe_index', ['page_title' => lang('sales_fe')], $this->data);
    }

    public function getSales($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');
        $dias_vencimiento = $this->input->post('dias_vencimiento');
        if ($this->input->post('start_date')) {
            $start_date = $this->input->post('start_date');
            $start_date = $this->sma->fld($start_date);
        } else {
            $start_date = NULL;
        }
        if ($this->input->post('end_date')) {
            $end_date = $this->input->post('end_date');
            $end_date = $this->sma->fld($end_date);
        } else {
            $end_date = NULL;
        }
        $detal_document_type_id = $this->input->post('detal_reference_no') ? $this->input->post('detal_reference_no') : NULL;
        $biller_id = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $seller_id = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        $customer = $this->input->post('customer') ? $this->input->post('customer') : NULL;
        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : NULL;
        $sale_status = $this->input->post('sale_status') ? $this->input->post('sale_status') : NULL;
        if (!$this->Owner && !$this->Admin && !$warehouse_id) {
            $warehouse_id = $this->session->userdata('warehouse_id');
        }
        if (!$this->Owner && !$this->Admin && !$biller_id) {
            $biller_id = $this->session->userdata('biller_id');
        }
        $detail_link = anchor('admin/sales/saleView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'), 'target="_blank"');
        $order_sale_link = anchor('admin/sales/orderSaleView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('order_sale_details'));
        $duplicate_link = anchor('admin/sales/add?sale_id=$1', '<i class="fa fa-plus-circle"></i> ' . lang('duplicate_sale'));
        $payments_link = anchor('admin/sales/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/payments/add/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), '');
        $packagink_link = anchor('admin/sales/packaging/$1', '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
        $add_delivery_link = anchor('admin/sales/add_delivery/$1', '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $xls_link = anchor('admin/sales/download_xls/$1', '<i class="fa fa-file-excel-o"></i> ' . lang('download_xls'), 'class="sledit"');
        $pdf_link = anchor('admin/sales/saleView/$1/false/true', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));

        $return_link_1 = anchor('admin/sales/return_sale/$1/1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_partial'));
        $return_link_2 = anchor('admin/sales/return_sale/$1/2', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_total'));
        $return_link_3 = anchor('admin/returns/credit_note_other_concepts/$1/3', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_others'));

        $sync_payments = anchor('admin/sales/sync_payments/$1', '<i class="fas fa-file-invoice"></i> ' . lang('sync_payments'));
        $change_payment_method = anchor('admin/sales/change_payment_method/$1', '<i class="fa fa-file-text-o"></i> ' . lang('change_payment_method'), 'data-toggle="modal" data-target="#myModal"');
        $contabilizar_venta = '<a class="reaccount_sale_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> ' . lang('post_sale') . ' </a>';
        $cambiar_estado = '<a class="row_status"><i class="fas fa-retweet"></i> ' . lang('change_status') . ' </a>';

        $return_sale_next_year = false;

        if ($this->Settings->years_database_management == 2 && $this->Settings->close_year_step > 1) {
            $return_sale_next_year = anchor('admin/sales/return_sale_next_year/$1', '<i class="fa fa-money"></i> ' . lang('return_sale_next_year'), 'data-toggle="modal" data-target="#myModal"');
        }

        if (isset($this->GP)) {
            $action = '<div class="text-center"><div class="btn-group text-left">
                <button type="button" class="btn btn-default new-button new-button-sm btn-xs btn-primary dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>' . $detail_link . '</li>';

            if ($this->GP['sales-add']) {
                $action .= '<li>' . $duplicate_link . '</li>';
            }

            if ($this->GP['sales-payments']) {
                $action .= '<li>' . $payments_link . '</li>
                                <li>' . $add_payment_link . '</li>
                                <li>' . $change_payment_method . '</li>';
            }

            if ($this->GP['sales-edit']) {
                $action .= '<li>' . $edit_link . '</li>';
                $action .= '<li>' . $cambiar_estado . '</li>';
            }

            if ($this->GP['sales-return_sales']) {
                $action .= '<li>' . $return_link_1 . '</li>' .
                    '<li>' . $return_link_2 . '</li>' .
                    '<li>' . $return_link_3 . '</li>';
            }

            $action .= '</ul>
                        </div></div>';
        } else {
            $action = '<div class="text-center"><div class="btn-group text-left">
                <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i></button>
                            <ul class="dropdown-menu pull-right" role="menu">
                                <li>' . $detail_link . '</li>
                                <li class="link_edit_sale">' . $edit_link . '</li>
                                <li>' . $duplicate_link . '</li>
                                <li>' . $payments_link . '</li>
                                <li>' . $add_payment_link . '</li>
                                <li>' . $pdf_link . '</li>
                                <li>' . $xls_link . '</li>
                                <li class="link_return_sale">' . $return_link_1 . '</li>
                                <li class="link_return_sale">' . $return_link_2 . '</li>
                                <li class="link_return_sale">' . $return_link_3 . '</li>
                                <li>' . $contabilizar_venta . '</li>
    			                <li>' . $cambiar_estado . '</li>
    			                <li>' . $sync_payments . '</li>
    			                <li>' . $change_payment_method . '</li>
                                <li>' . $add_delivery_link . '</li>' .
                ($return_sale_next_year ? '<li>' . $return_sale_next_year . '</li>' : '') .
                '
                            </ul>
                        </div></div>';
        }
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('sales')}.id as id,
                        DATE_FORMAT({$this->db->dbprefix('sales')}.date, '%Y-%m-%d %T') as date,
                        {$this->db->dbprefix('sales')}.payment_term,
                        sale_origin_reference_no,
                        reference_no,
                        return_sale_ref as affects_to,
                        sales.biller,
                        {$this->db->dbprefix('sales')}.customer,
                        sales.sale_status, sales.grand_total,
                        sales.paid, (grand_total-paid) as balance,
                        sales.payment_status,
                        {$this->db->dbprefix('sales')}.attachment,
                        documents_types.factura_electronica,
                        sales.return_id
                    ")
            ->join('documents_types', 'documents_types.id = sales.document_type_id', 'left')
            ->from('sales')
            ->where('(documents_types.factura_electronica = 0 OR documents_types.factura_electronica IS NULL)')
            ->where('(sales.pos = 0 OR sales.pos IS NULL)');
        if ($warehouse_id) {
            $this->datatables->where('sales.warehouse_id =', $warehouse_id);
        }
        if ($biller_id) {
            $this->datatables->where('sales.biller_id =', $biller_id);
        }
        if ($seller_id) {
            $this->datatables->where('sales.seller_id =', $seller_id);
        }
        if ($user_id) {
            $this->datatables->where('sales.created_by =', $user_id);
        }
        if ($this->input->get('shop') == 'yes') {
            $this->datatables->where('shop', 1);
        } elseif ($this->input->get('shop') == 'no') {
            $this->datatables->where('shop !=', 1);
        }
        if ($this->input->get('delivery') == 'no') {
            $this->datatables->join('deliveries', 'deliveries.sale_id=sales.id', 'left')
                ->where('sales.sale_status', 'completed')->where('sales.payment_status', 'paid')
                ->where("({$this->db->dbprefix('deliveries')}.status != 'delivered' OR {$this->db->dbprefix('deliveries')}.status IS NULL)", NULL);
        }
        if ($this->input->get('attachment') == 'yes') {
            $this->datatables->where('payment_status !=', 'paid')->where('attachment !=', NULL);
        }

        if ($detal_document_type_id) {
            $this->datatables->where('sales.document_type_id =', $detal_document_type_id);
        }

        if ($dias_vencimiento) {
            $this->datatables->where('sales.payment_term', $dias_vencimiento);
        }

        if ($start_date) {
            $this->datatables->where('sales.date >=', $start_date);
        }

        if ($end_date) {
            $this->datatables->where('sales.date <=', $end_date);
        }

        if ($customer) {
            $this->datatables->where('sales.customer_id =', $customer);
        }

        if ($sale_status) {
            $this->datatables->where('sales.sale_status =', $sale_status);
        }

        if ($payment_status) {
            $this->datatables->where('sales.payment_status =', $payment_status);
        }

        $this->datatables->where('pos !=', 1); // ->where('sale_status !=', 'returned');
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
            }
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");
        echo $this->datatables->generate();
    }

    public function getSalesFE($warehouse_id = null)
    {
        $this->sma->checkPermissions('index');

        $detail_link = anchor('admin/sales/saleView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $order_sale_link = anchor('admin/sales/orderSaleView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('order_sale_details'));
        $duplicate_link = anchor('admin/sales/add?sale_id=$1', '<i class="fa fa-plus-circle"></i> ' . lang('duplicate_sale'));
        $payments_link = anchor('admin/sales/payments/$1', '<i class="fa fa-money"></i> ' . lang('view_payments'), 'data-toggle="modal" data-target="#myModal"');
        $add_payment_link = anchor('admin/payments/add/$1', '<i class="fa fa-money"></i> ' . lang('add_payment'), '');
        $packagink_link = anchor('admin/sales/packaging/$1', '<i class="fa fa-archive"></i> ' . lang('packaging'), 'data-toggle="modal" data-target="#myModal"');
        $add_delivery_link = anchor('admin/sales/add_delivery/$1', '<i class="fa fa-truck"></i> ' . lang('add_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit/$1', '<i class="fa fa-edit"></i> ' . lang('edit_sale'), 'class="sledit"');
        $pdf_link = anchor('admin/sales/saleView/$1/false/true', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $xls_link = anchor('admin/sales/download_xls/$1', '<i class="fa fa-file-excel-o"></i> ' . lang('download_xls'), 'class="sledit"');
        $return_link_1 = anchor('admin/sales/return_sale/$1/1', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_partial'));
        $return_link_2 = anchor('admin/sales/return_sale/$1/2', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_total'));
        $return_link_3 = anchor('admin/returns/credit_note_other_concepts/$1/3', '<i class="fa fa-angle-double-left"></i> ' . lang('return_sale_others'));
        $debit_note_link = anchor('admin/debit_notes/add/$1', '<i class="fa fa-files-o"></i> ' . lang('debit_note_label_menu'));
        $sync_payments = anchor('admin/sales/sync_payments/$1', '<i class="fas fa-file-invoice"></i> ' . lang('sync_payments'));
        $change_payment_method = anchor('admin/sales/change_payment_method/$1', '<i class="fa fa-file-text-o"></i> ' . lang('change_payment_method'), 'data-toggle="modal" data-target="#myModal"');
        $contabilizar_venta = '<a class="reaccount_sale_link" data-invoiceid="$1"><i class="fas fa-sync-alt"></i> ' . lang('post_sale') . ' </a>';
        $cambiar_estado = '<a class="row_status"><i class="fas fa-retweet"></i> ' . lang('change_status') . ' </a>';

        $action = '<div class="btn-group text-left">
            <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i>
            </button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li class="divider"></li>
                <li><label style="padding-left: 20px; padding-top: 10px">Otras acciones</label></li>
                <li class="link_edit_sale">' . $edit_link . '</li>
                <li class="print_document">' . $detail_link . '</li>
                <li class="link_invoice_action">' . $duplicate_link . '</li>
                <li class="link_invoice_action">' . $payments_link . '</li>
                <li class="link_invoice_action">' . $add_payment_link . '</li>
                <li class="link_invoice_action">' . $packagink_link . '</li>
                <li class="link_invoice_action">' . $add_delivery_link . '</li>
                <li>' . $pdf_link . '</li>
                <li>' . $xls_link . '</li>
                <li class="link_invoice_action link_return_sale">' . $return_link_1 . '</li>
                <li class="link_invoice_action link_return_sale">' . $return_link_2 . '</li>
                <li class="link_invoice_action link_return_sale">' . $return_link_3 . '</li>
                <li class="link_invoice_action link_debit_note">' . $debit_note_link . '</li>
                <li class="link_invoice_action">' . $contabilizar_venta . '</li>
                <li class="link_invoice_action">' . $cambiar_estado . '</li>
                <li class="link_invoice_action">' . $sync_payments . '</li>
                <li class="link_invoice_action">' . $change_payment_method . '</li>
            </ul>
        </div>';

        $tabFilterData = $this->input->post("tabFilterData");
        $optionFilter = $this->input->post('optionFilter') != 'false' ? $this->input->post('optionFilter') : 'all';
        if (!empty($tabFilterData)) {
                $returns = $this->manageResponseRequest($action, 'returns');
                $notApproved = $this->manageResponseRequest($action, 'notApproved');
                $pendingPayment = $this->manageResponseRequest($action, 'pendingPayment');
                $pendingDian = $this->manageResponseRequest($action, 'pendingDian');
                $pendingDispatch = $this->manageResponseRequest($action, 'pendingDispatch');
                $all = $this->manageResponseRequest($action, FALSE);

            echo json_encode(['returns' => $returns, 'notApproved' => $notApproved, 'pendingPayment' => $pendingPayment, 'pendingDian' => $pendingDian, 'pendingDispatch' => $pendingDispatch, 'all' => $all]);
        } else {
            echo $this->manageResponseRequest($action, $optionFilter);
        }
    }

    public function manageResponseRequest($action, $optionFilter)
    {
        $this->load->library('datatables');

        $shop = $this->input->get('shop');
        $delivery = $this->input->get('delivery');
        $attachment = $this->input->get('attachment');
        $fe_aceptado = $this->input->post('fe_aceptado');
        $fe_recibido = $this->input->post('fe_recibido');
        $tabFilterData = $this->input->post("tabFilterData");
        $dias_vencimiento = $this->input->post('dias_vencimiento');
        $fe_correo_enviado = $this->input->post('fe_correo_enviado');
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $seller_id = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        // $customer = $this->input->post('customer') ? $this->input->post('customer') : NULL;
        $client = $this->input->post('client') ? $this->input->post('client') : NULL;
        $biller_id = $this->input->post('biller_id') ? $this->input->post('biller_id') : NULL;
        $campaign = $this->input->post('campaign') ? $this->input->post('campaign') : NULL;
        $sale_status = $this->input->post('sale_status') ? $this->input->post('sale_status') : NULL;
        $sale_origin = $this->input->post('sale_origin') ? $this->input->post('sale_origin') : NULL;
        $fe_aceptado = $this->input->post('fe_aceptado') ? $this->input->post('fe_aceptado') : NULL;
        $warehouse_id = $this->input->post('warehouse_id') ? $this->input->post('warehouse_id') : NULL;
        $document_type = $this->input->post('document_type') ? $this->input->post('document_type') : NULL;
        $payment_method = $this->input->post('payment_method') ? $this->input->post('payment_method') : NULL;
        $payment_methodName = $this->site->getPaymentMethodById($payment_method);
        $payment_status = $this->input->post('payment_status') ? $this->input->post('payment_status') : NULL;
        $fe_correo_enviado = $this->input->post('fe_correo_enviado') ? $this->input->post('fe_correo_enviado') : NULL;
        $detal_document_type_id = $this->input->post('detal_reference_no') ? $this->input->post('detal_reference_no') : NULL;
        $end_date = ($this->input->post('end_date')) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('end_date')))) : NULL;
        $start_date = ($this->input->post('start_date')) ? date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('start_date')))) : NULL;

        if (!$this->Owner && !$this->Admin && !$biller_id) { $biller_id = $this->session->userdata('biller_id'); }
        if (!$this->Owner && !$this->Admin && !$warehouse_id) { $warehouse_id = $this->session->userdata('warehouse_id'); }

        $this->datatables->select("sales.id as id,
            DATE_FORMAT(" . $this->db->dbprefix('sales') . ".date, '%Y-%m-%d %T') as date,
            sales.payment_term,
            sales.sale_origin_reference_no as sale_origin_reference_no,
            sales.reference_no as reference_no,
            IF(documents_types.module = '1' OR documents_types.module = '2' OR documents_types.module = '5',
                (
                    if(" . $this->db->dbprefix('sales') . ".return_sale_ref IS NULL OR " . $this->db->dbprefix('sales') . ".return_sale_ref = '', " . $this->db->dbprefix('sales') . ".reference_debit_note, CONCAT_WS(', ', " . $this->db->dbprefix('sales') . ".return_sale_ref, " . $this->db->dbprefix('sales') . ".reference_debit_note))
                ),
                (
                    IF(documents_types.module = '3' OR documents_types.module = '4' OR documents_types.module = '6' OR documents_types.module = '22', " . $this->db->dbprefix('sales') . ".return_sale_ref, " . $this->db->dbprefix('sales') . ".reference_debit_note)
                )
            ) AS affects_to,
            sales.biller,
            sales.customer,
            sales.sale_status,
            sales.grand_total,
            sales.paid,
            (grand_total-paid) as balance,
            sales.payment_status,
            sales.attachment,
            sales.return_id,
            sales.fe_aceptado,
            sales.fe_correo_enviado,
            sales.fe_recibido,
            sales.fe_mensaje,
            sales.fe_mensaje_soporte_tecnico,
            documents_types.factura_electronica,
            documents_types.module");
        $this->datatables->from('sales');
        $this->datatables->join('documents_types AS documents_types', 'documents_types.id = ' . $this->db->dbprefix('sales') . '.document_type_id', 'inner');
        $this->datatables->where('documents_types.factura_electronica', '1');
        $this->datatables->where('pos !=', 1);

        if ($document_type) {
            $this->datatables->where('documents_types.id', $document_type);
        }
        if ($sale_origin) {
            $this->datatables->where('sales.sale_origin', $sale_origin);
        }
        if ($campaign) {
            $this->datatables->where('sales.campaigns', $campaign);
        }
        if ($biller_id) {
            $this->datatables->where('sales.biller_id =', $biller_id);
        }
        if ($seller_id) {
            $this->datatables->where('sales.seller_id =', $seller_id);
        }
        if ($warehouse_id) {
            $this->datatables->where('sales.warehouse_id', $warehouse_id);
        }
        if ($payment_status) {
            $this->datatables->where('sales.payment_status', $payment_status);
        }
        if ($payment_method) {
            $this->datatables->where('sales.payment_method', $payment_methodName->code);
        }
        if ($fe_aceptado) {
            $this->datatables->where('sales.fe_aceptado', $fe_aceptado);
        }
        if ($fe_correo_enviado) {
            $this->datatables->where('sales.fe_correo_enviado', $fe_correo_enviado);
        }
        if ($user_id) {
            $this->datatables->where('sales.created_by', $user_id);
        }
        if ($start_date) {
            $this->datatables->where("CAST({$this->db->dbprefix('sales')}.date AS DATE) >=", $start_date);
        }
        if ($end_date) {
            $this->datatables->where("CAST({$this->db->dbprefix('sales')}.date AS DATE) <=", $end_date);
        }
        if ($client) {
            $this->datatables->where('sales.customer_id', $client);
        }
        if ($sale_status) {
            $this->datatables->where('sales.sale_status', $sale_status);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(sales.created_by = '.$this->session->userdata('user_id').' OR seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('sales.created_by', $this->session->userdata('user_id'));
            }
        } else if ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        if ($optionFilter == 'returns') {
            $this->datatables->where('documents_types.module', 4);
        } else if ($optionFilter == 'notApproved') {
            $this->datatables->where('sales.sale_status', 'pending');
        } else if ($optionFilter == 'pendingPayment') {
            $this->datatables->where('sales.payment_status !=', 'paid');
        } else if ($optionFilter == 'pendingDian') {
            $this->datatables->where('sales.fe_aceptado !=', ACCEPTED);
        } else if ($optionFilter == 'pendingDispatch') {
            $this->datatables->join('deliveries', 'deliveries.sale_id = sales.id', 'inner');
        }

        if (!empty($tabFilterData)) {
            return $this->datatables->get_result_num_rows();
        } else {
            return $this->datatables->generate();
        }
    }

    public function modal_view($id = null)
    {
        // if (!($this->Admin || $this->Owner || $this->GP['sales-index'] || $this->GP['reports-sales'] || $this->GP['reports-customers'])) {
        //     die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
        // }

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getSaleByID($id);
        // if (!$this->session->userdata('view_right')) {
        //     $this->sma->view_rights($inv->created_by, true);
        // }
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id, NULL, false, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getReturnsInvoicesBySaleID($id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllReturnsItems($id) : NULL;
        $this->data['seller'] = $this->site->getCompanyByID($inv->seller_id);
        $this->data['affiliate'] = $this->site->getCompanyByID($inv->affiliate_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
        $this->data['tipo_regimen'] = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);

        $payments = $this->sales_model->getPaymentsForSale($id);
        $sale_main_payment_method = 'credit';
        $sale_payments_method = '';
        if ($payments) {
            foreach ($payments as $payment) {
                if ($payment->date == $inv->date) {
                    $sale_main_payment_method = 'counted';
                    $sale_payment_method = $payment->paid_by;
                }
                $sale_payments_method .= $payment->name . ", ";
            }

            $sale_payments_method = trim($sale_payments_method, ", ");
        }

        $this->data['sale_main_payment_method'] = $sale_main_payment_method;
        $this->data['sale_payments_method'] = $sale_payments_method;

        $currency = $this->site->getCurrencyByCode($inv->sale_currency);
        $trmrate = 1;
        if (!empty($inv->sale_currency) && $inv->sale_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->sale_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;

        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }

        $this->load_view($this->theme . 'sales/modal_view', $this->data);
    }

    public function order_modal_view($id = null)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->db->update('order_sales', ['notification_status'=>1], ['id'=>$id]);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getOrderByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by, true);
        }
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        $this->data['rows'] = $this->sales_model->getAllOrderSaleItems($id, NULL, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getOrderSaleByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllOrderSaleItems($inv->return_id) : NULL;

        $this->load_view($this->theme . 'orders/modal_view', $this->data);
    }

    public function view($id = null)
    {
        $this->sma->checkPermissions('index');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['paypal'] = $this->sales_model->getPaypalSettings();
        $this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $this->page_construct('sales/view', $meta, $this->data);
    }

	public function orderView($id = null, $for_email = false)
	{
		$this->sma->checkPermissions('index');
		if ($this->input->get('id')) {
			$id = $this->input->get('id');
		}
		$this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
		$inv = $this->sales_model->getOrderByID($id);
		if (!$this->session->userdata('view_right')) {
			$this->sma->view_rights($inv->created_by);
		}
		$this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
		$this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
		$this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
		$this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
		$this->data['created_by'] = $this->site->getUser($inv->created_by);
		$this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
		$this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
		$this->data['inv'] = $inv;
        $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
		$this->data['rows'] = $this->sales_model->getAllOrderSaleItems($id, NULL, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
		$this->data['return_sale'] = $inv->return_id ? $this->sales_model->getOrderByID($inv->return_id) : NULL;
		$this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllOrderSaleItems($inv->return_id) : NULL;
		$this->data['paypal'] = $this->sales_model->getPaypalSettings();
		$this->data['skrill'] = $this->sales_model->getSkrillSettings();
		$this->data['settings'] = $this->Settings;
		$this->data['seller'] = $this->site->getSellerById($inv->seller_id);
		$this->data['sma'] = $this->sma;
        $this->data['biller_logo'] = 2;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $this->data['tipo_regimen'] = $tipo_regimen && isset($tipo_regimen->description) ? lang($tipo_regimen->description) : '';
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = $tax->name;
        }
        $this->data['taxes_details'] = $taxes_details;
        $this->data['show_code'] = 1;
        $this->data['product_detail_font_size'] = 0;
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
        $prueba = false;
        $url_format = "orders/sale_view";
        $view_tax = true;
        $tax_inc = true;
        $this->data['invoice_format'] = $document_type_invoice_format;
        $this->data['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);
        $this->data['show_document_type_header'] = 1;
        $this->data['show_code'] = 1;
        $this->data['totalize_quantity'] = 0;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
		if ($prueba == false && $inv->module_invoice_format_id != NULL) {
			if ($document_type_invoice_format) {
                $url_format = $document_type_invoice_format->format_url;
				$view_tax = $document_type_invoice_format->view_item_tax ? true : false;
				$tax_inc = $document_type_invoice_format->tax_inc ? true : false;
				$this->data['show_code'] = $document_type_invoice_format->product_show_code;
                $this->data['product_detail_font_size'] = $document_type_invoice_format->product_detail_font_size > 0 ? $document_type_invoice_format->product_detail_font_size : 0;
                $this->data['show_document_type_header'] = $document_type_invoice_format->show_document_type_header;
                $this->data['totalize_quantity'] = $document_type_invoice_format->totalize_quantity;
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
            }
        }
        $this->data['view_tax'] = $view_tax;
        $this->data['tax_inc'] = $tax_inc;
        $this->data['biller_logo'] = 2;
        $this->data['tax_indicator'] = 0;
        $this->data['product_detail_promo'] = 1;
        $this->data['show_award_points'] = 1;
        $this->data['show_product_preferences'] = 1;
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $trmrate = 1;
        if (!empty($inv->sale_currency) && $inv->sale_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->sale_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;
        $ciius_rows = $this->site->get_ciiu_code_by_ids(explode(",", $this->Settings->ciiu_code));
        $ciius = "";
        if ($ciius_rows) {
            foreach ($ciius_rows as $ciiu) {
                $ciius.= $ciiu->code.", ";
            }
        }
        $this->data['ciiu_code'] = trim($ciius, ", ");
        $this->data['signature_root'] = is_file("assets/uploads/signatures/".$this->Settings->digital_signature) ? base_url().'assets/uploads/signatures/'.$this->Settings->digital_signature : false;
        $this->data['for_email'] = false;
        $paymentMethod = $this->sales_model->findPaymentMethod(['code' => $inv->payment_method]);
        $this->data['payment_method'] = $paymentMethod;

        // $this->sma->print_arrays($this->data['rows']);
        if ($for_email) {
            $this->data['for_email'] = true;
            if (strpos($url_format, 'pos_')) {
                $html = $this->load_view($this->theme . $url_format, $this->data, true);
                $this->sma->generate_pos_inv_pdf($html, $inv->reference_no);
            } else {
                $this->load_view($this->theme . $url_format, $this->data, true);
            }
        } else {
            $this->load_view($this->theme . $url_format, $this->data);
        }
    }

    public function saleView($id = null, $internal_download = FALSE, $download = FALSE, $for_email = FALSE,  $quickPrintFromPos = FALSE, $quickPrint = FALSE)
    {
        $this->sma->checkPermissions('index');
        if ($this->session->userdata('print_pos_finalized_invoice') == 1) {
            $this->pos_model->update_print_status($id);
        }
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->load->library('qr_code');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getSaleByID($id);
        $document_type = $this->site->getDocumentTypeById($inv->document_type_id);

        if (empty($inv->resolucion)) {
            $inv->resolucion = $this->site->textoResolucion($document_type);
        }

        $affected_bill = '';
        if (!empty($inv->sale_id)) {
            if (!empty($inv->year_database)) {
                $affected_bill = $this->site->get_past_year_sale($inv->return_sale_ref, $inv->year_database);
                // $resolution_data_referenced_invoice = $this->site->get_past_year_document_type_by_id($reference_invoice->document_type_id, $inv->year_database);
            } else {
                $affected_bill = $this->sales_model->getSaleByID($inv->sale_id);
            }
        }

        $this->data['affected_bill'] = $affected_bill;
        //Declaramos una carpeta temporal para guardar la imagenes generadas
        $dir = 'themes/default/admin/assets/images/qr_code/';
        //Declaramos la ruta y nombre del archivo a generar
        // $filename = $dir . 'test.png';
        $filename = $dir . $inv->reference_no.'.png';
        //Si no existe la carpeta la creamos
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }
        QRcode::png($inv->codigo_qr, $filename, 'L', 3, 0);

        $quickPrintFormatId = $document_type->quick_print_format_id;
        if ($quickPrint == FALSE) {
            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
        } else {
            if (!empty($quickPrintFormatId)) {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($document_type->quick_print_format_id);
            } else {
                $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
            }
        }

        if (!empty($inv->fe_xml) && $inv->technology_provider != SIMBA) {
            $xml_file = base64_decode($inv->fe_xml);

            $xml = new DOMDocument("1.0", "ISO-8859-15");
            $xml->loadXML($xml_file);

            $path = 'files/electronic_billing';
            if (!file_exists($path)) {
                mkdir($path, 0777);
            }
            $xml->save('files/electronic_billing/' . $inv->reference_no . '.xml');

            $positionDate = strpos($xml_file, '<cbc:ValidationDate>');
            $substringDate = substr($xml_file, $positionDate);
            $substringDate2 = substr($substringDate, 20, 10);

            $positionHour = strpos($xml_file, '<cbc:ValidationTime>');
            $substringHour = substr($xml_file, $positionHour);
            $substringHour2 = substr($substringHour, 20, 8);

            $validationDateTime = $substringDate2 .' '. $substringHour2;

            $this->data['validationDateTime'] = $validationDateTime;

            if (file_exists(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml')) {
                unlink(FCPATH. 'files/electronic_billing/'.$inv->reference_no.'.xml');
            }
        } else {
            $this->data['validationDateTime'] = '';
            if (isset($inv->fe_validation_dian) && $inv->fe_validation_dian !== null) {
                $this->data['validationDateTime'] = $inv->fe_validation_dian;
            }
        }

        $items = $this->sales_model->getAllInvoiceItems($id, NULL, false, ($document_type_invoice_format ? $document_type_invoice_format->product_order : NULL));
        $secundaryTaxes = [];
        if (!empty($items)) {
            $secundaryTaxAmount = 0;
            foreach ($items as $item) {
                if (!in_array($item->tax_rate_id, $secundaryTaxes)) {
                    $secondTax = $this->TaxSecondary_model->find($item->tax_rate_2_id);
                    if ($secondTax) {
                        $secundaryTaxAmount += $this->sma->formatDecimal($item->item_tax_2);
                        $secundaryTaxes[$item->tax_rate_2_id]["tax"] = $secundaryTaxAmount;
                        $secundaryTaxes[$item->tax_rate_2_id]["name"] = $secondTax->tax_indicator;
                    }

                }
            }
        }
        $this->data["secundaryTaxes"] = $secundaryTaxes;


        $this->data['quickPrintFormatId'] = $quickPrintFormatId;
        $this->data['quickPrintFromPos'] = $quickPrintFromPos;
        $this->data['internal_download'] = $internal_download;
        $this->data['qr_code'] = $filename;
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getAllCompaniesWithState('biller', $inv->biller_id, false);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $items;
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;
		$this->data['paypal'] = $this->sales_model->getPaypalSettings();
		$this->data['skrill'] = $this->sales_model->getSkrillSettings();
		$this->data['settings'] = $this->Settings;
		$this->data['seller'] = $this->site->getSellerById(!empty($inv->seller_id) ? $inv->seller_id : $this->data['biller']->default_seller_id);
        $this->data['filename'] = $this->site->getFilename($id);
        $ciius_rows = $this->site->get_ciiu_code_by_ids(explode(",", $this->Settings->ciiu_code));
        $ciius = "";
        if ($ciius_rows) {
            foreach ($ciius_rows as $ciiu) {
                $ciius.= $ciiu->code.", ";
            }
        }
        $this->data['ciiu_code'] = trim($ciius, ", ");
		$this->data['sma'] = $this->sma;
		$this->data['document_type'] = $document_type;

        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        if ($this->Settings->great_contributor == 1) {
            $this->data['tipo_regimen'] = lang('great_contributor');
        } else {
            $this->data['tipo_regimen'] = $tipo_regimen && isset($tipo_regimen->description) ? lang($tipo_regimen->description) : '';
        }
        $payments = $this->sales_model->getPaymentsForSale($id);
        $original_payment_method = '';
        $original_payment_method_with_amount = '';
        $total_original_payments_amount = 0;
        $direct_payments_num = 0;
        if ($payments) {
            foreach ($payments as $payment) {
                if ($payment->paid_by == "retencion") {
                    $total_original_payments_amount += $payment->amount;
                }
                if (date('Y-m-d H', strtotime($payment->date)) == date('Y-m-d H', strtotime($inv->date)) && $payment->paid_by != "retencion") {
                    if ($original_payment_method == '') {
                        $original_payment_method = lang($payment->paid_by) . ", ";
                    } else {
                        $original_payment_method .= lang($payment->paid_by) . ", ";
                    }
                    if ($original_payment_method_with_amount == '') {
                        $original_payment_method_with_amount = lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    } else {
                        $original_payment_method_with_amount .= lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    }
                    $direct_payments_num++;
                    $total_original_payments_amount += $payment->amount;
                }
            }
            if ($direct_payments_num > 1) {
                $inv->note .= $original_payment_method_with_amount;
                $original_payment_method = 'Varios, ver nota';
            } else {
            }
        }
        if ($original_payment_method != '') {
            $original_payment_method = trim($original_payment_method, ", ");
        }
        if ($original_payment_method == '' || ($total_original_payments_amount < $inv->grand_total)) {
            $original_payment_method .= ($original_payment_method != '' ? ", " : "") . lang('due') . " " . $inv->payment_term . ($inv->payment_term > 1 ? " Días" : " Día");
        }
        // exit(var_dump($original_payment_method));
        $currencies = $this->site->getAllCurrencies();
        $currencies_names = [];
        if ($currencies) {
            foreach ($currencies as $currency) {
                $currencies_names[$currency->code] = $currency->name;
            }
        }
        $this->data['currencies_names'] = $currencies_names;
        $this->data['sale_payment_method'] = $original_payment_method;
        $prueba = false;
        $url_format = "sales/sale_view";
        $view_tax = true;
        $tax_inc = true;
        $this->data['document_type_invoice_format'] = false;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
        $this->data['biller_logo'] = 2;
        $this->data['show_code'] = 1;
        $this->data['product_detail_promo'] = 1;
        $this->data['show_document_type_header'] = 1;
        $this->data['show_product_preferences'] = 1;
        $this->data['tax_indicator'] = 0;
        $this->data['product_detail_font_size'] = 0;
        if (!$prueba) {
            $url_format = "sales/sale_view";
            if ($inv->sale_status == 'returned') {
                $url_format = "sales/return_sale_view";
            }
            if ($document_type_invoice_format) {
                $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
                $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
                $this->data['biller_logo'] = $document_type_invoice_format->logo;
                $this->data['show_code'] = $document_type_invoice_format->product_show_code;
                $this->data['product_detail_promo'] = $document_type_invoice_format->product_detail_promo;
                $this->data['show_document_type_header'] = $document_type_invoice_format->show_document_type_header;
                $this->data['document_type_invoice_format'] = $document_type_invoice_format;
                $this->data['show_product_preferences'] = $document_type_invoice_format->show_product_preferences;
                $this->data['tax_indicator'] = $document_type_invoice_format->tax_indicator;
                $this->data['product_detail_font_size'] = $document_type_invoice_format->product_detail_font_size > 0 ? $document_type_invoice_format->product_detail_font_size : 0;
				$url_format = $document_type_invoice_format->format_url;
				$view_tax = $document_type_invoice_format->view_item_tax ? true : false;
				$tax_inc = $document_type_invoice_format->tax_inc ? true : false;
    		}
        }

        $this->data['view_tax'] = $view_tax;
        $this->data['tax_inc'] = $tax_inc;
        $taxes = $this->site->getAllTaxRates();
        $taxes_details = [];
        foreach ($taxes as $tax) {
            $taxes_details[$tax->id] = (!$document_type_invoice_format || ($document_type_invoice_format && $document_type_invoice_format->tax_indicator == 1) ? "(" . $tax->tax_indicator . ") " : "") . $tax->name;
        }
        $this->data['taxes_details'] = $taxes_details;
        if ($this->Settings->cost_center_selection == 1) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->customer_id);
        }
        $this->data['signature_root'] = is_file("assets/uploads/signatures/" . $this->Settings->digital_signature) ? base_url() . 'assets/uploads/signatures/' . $this->Settings->digital_signature : false;
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
        $currency = $this->site->getCurrencyByCode($inv->sale_currency);
        $trmrate = 1;
        if (!empty($inv->sale_currency) && $inv->sale_currency != $this->Settings->default_currency) {
            $actual_currency_rate = $currency->rate;
            $trmrate = $actual_currency_rate / $inv->sale_currency_trm;
        }
        $this->data['trmrate'] = $trmrate;
        $this->data['download'] = $download;
        $this->data['for_email'] = $for_email === true ? $for_email : false;

        if ($this->Settings->fe_technology_provider == CADENA) {
            $this->data["technologyProviderLogo"] = "assets/images/cadena_logo.jpeg";
        } else if ($this->Settings->fe_technology_provider == BPM) {
            $this->data["technologyProviderLogo"] = "assets/images/bpm_logo.jpeg";
        } else if ($this->Settings->fe_technology_provider == SIMBA) {
            $this->data["technologyProviderLogo"] = "assets/images/simba_logo.png";
        } else {
            $this->data["technologyProviderLogo"] = "assets/images/delcop_logo.png";
        }
        if ($document_type_invoice_format && $this->config->item('language') != $document_type_invoice_format->language) {
            $this->lang->admin_load('sma', $document_type_invoice_format->language);
            $this->data['other_language'] = true;
        } else {
            $this->lang->admin_load('sma', $this->Settings->language);
        }
        // exit(var_dump($url_format));
        $this->load_view($this->theme . $url_format, $this->data);
    }

    public function orderSaleView($id = null)
    {
        $this->sma->checkPermissions('index');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->db->update('order_sales', ['notification_status'=>1], ['id'=>$id]);
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getSaleByID($id);
        if (!$inv->seller_id) {
            $this->session->set_flashdata('error', lang("invalid_order_sale"));
            admin_redirect("sales");
        }
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getOrderByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['paypal'] = $this->sales_model->getPaypalSettings();
        $this->data['skrill'] = $this->sales_model->getSkrillSettings();
        $this->data['settings'] = $this->Settings;
        $this->data['seller'] = $this->site->getSellerById($inv->seller_id);
        $this->data['sma'] = $this->sma;
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
        $this->load_view($this->theme . 'orders/sale_view', $this->data);
    }

    public function pdf($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getInvoiceByID($id);
        if (!$this->session->userdata('view_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['user'] = $this->site->getUser($inv->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['inv'] = $inv;
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
        $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : NULL;
        //$this->data['paypal'] = $this->sales_model->getPaypalSettings();
        //$this->data['skrill'] = $this->sales_model->getSkrillSettings();

        $name = lang("sale") . "_" . str_replace('/', '_', $inv->reference_no) . ".pdf";
        $html = $this->load_view($this->theme . 'sales/pdf', $this->data, true);
        if (!$this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }

        if ($view) {
            $this->load_view($this->theme . 'sales/pdf', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer, $this->data['biller']->invoice_footer);
        } else {
            $this->sma->generate_pdf($html, $name, false, $this->data['biller']->invoice_footer);
        }
    }

    public function combine_pdf($sales_id)
    {
        $this->sma->checkPermissions('pdf');

        foreach ($sales_id as $id) {

            $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
            $inv = $this->sales_model->getInvoiceByID($id);
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $this->data['barcode'] = "<img src='" . admin_url('products/gen_barcode/' . $inv->reference_no) . "' alt='" . $inv->reference_no . "' class='pull-left' />";
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['payments'] = $this->sales_model->getPaymentsForSale($id);
            $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
            $this->data['user'] = $this->site->getUser($inv->created_by);
            $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
            $this->data['inv'] = $inv;
            $this->data['rows'] = $this->sales_model->getAllInvoiceItems($id);
            $this->data['return_sale'] = $inv->return_id ? $this->sales_model->getInvoiceByID($inv->return_id) : NULL;
            $this->data['return_rows'] = $inv->return_id ? $this->sales_model->getAllInvoiceItems($inv->return_id) : NULL;
            $html_data = $this->load_view($this->theme . 'sales/pdf', $this->data, true);
            if (!$this->Settings->barcode_img) {
                $html_data = preg_replace("'\<\?xml(.*)\?\>'", '', $html_data);
            }

            $html[] = array(
                'content' => $html_data,
                'footer' => $this->data['biller']->invoice_footer,
            );
        }

        $name = lang("sales") . ".pdf";
        $this->sma->generate_pdf($html, $name);
    }

    public function add($quote_id = null, $order = false)
    {
        if (!$this->Owner && !$this->Admin) {
            if ($this->session->userdata('register_cash_movements_with_another_user')) {
                if (!$this->pos_model->registerData($this->session->userdata('register_cash_movements_with_another_user'))) {
                    $this->session->set_flashdata('error', lang('another_user_cash_not_open'));
                    admin_redirect('welcome');
                }
            } else {
                if (!$this->pos_model->registerData($this->session->userdata('user_id'))) {
                    $this->session->set_flashdata('error', lang('register_not_open'));
                    admin_redirect('pos/open_register');
                }
            }
        }
        if ($order != false && $quote_id == null) {
            $this->session->set_flashdata('error', lang("invalid_order_id"));
            admin_redirect('orders');
        }
        $orders_ids = json_decode(urldecode($this->input->get('orders_ids') ? $this->input->get('orders_ids') : ""));
        $pending_sales_dian = $this->site->get_pending_sales_dian();
        if ($pending_sales_dian->quantity > 0) {
            $this->data['pending_electronic_document_message'] = 'Existen documentos electrónicos en estado pendiente ante la DIAN. Por favor es necesario diligenciar dichos documentos';
        }

        $validationElectronicHist = $this->site->validationElectronicInvoice();
        if ($validationElectronicHist->response == false) {
            $this->data['validationElectronicHist'] = $validationElectronicHist->message;
        }

        $fe_pos_sale_id = false;
        if ($this->input->get('fe_pos_sale_id')) {
            $fe_pos_sale_id = $this->input->get('fe_pos_sale_id');
        } else if ($this->input->post('fe_pos_sale_id')) {
            $fe_pos_sale_id = $this->input->post('fe_pos_sale_id');
        }
        if ($fe_pos_sale_id) { //validación de si existe un documento de devolución para POS, ya que la venta POS se devolverá para conventirla en DETAL
            $this->data['fe_pos_sale'] = true;
            $dev_pos_data = $this->site->getSaleByID($fe_pos_sale_id);
            $dev_pos = $this->sales_model->get_biller_document_type($dev_pos_data->biller_id, 3);
            if (!$dev_pos) {
                $this->session->set_flashdata('error', lang("invalid_return_pos_biller_document_type_id"));
                admin_redirect('pos/sales');
            }

            if ($dev_pos_data->return_id > 0 || $dev_pos_data->sale_status == 'returned') { //Si la venta POS ya tiene una devolución no se puede convertir en DETAL
                $this->session->set_flashdata('error', lang("invalid_pos_to_fe"));
                admin_redirect('pos/sales');
            }
        }
        $sid = false;
        $stofe = false;
        if ($this->input->get('suspended_sale')) { //obtención de id de una venta suspendida
            $sid = $this->input->get('suspended_sale');
        }
        if ($this->input->get('suspend_to_fe')) { //obtención de id de una venta suspendida
            $stofe = $this->input->get('suspend_to_fe');
        }
        $this->sma->checkPermissions();
        $sale_id = $this->input->get('sale_id') ? $this->input->get('sale_id') : NULL;
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('seller_id', lang("seller"), 'required');
        if ($this->Settings->cost_center_selection == 1 && $this->Settings->modulary == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if ($order == false) {
            if ($quote = $this->quotes_model->getQuoteByID($quote_id)) {
                if (!$quote->customer_id > 0) {
                    $this->session->set_flashdata('error', lang("invalid_quote"));
                    admin_redirect('sales');
                }
            }
        }

        if ($this->input->post('payment_status') == 'partial') {
            $this->form_validation->set_rules('payment_reference_no', lang("payment_reference_no"), 'required');
        }
        if ($this->Owner || $this->Admin) {
            $date = $this->sma->fld(trim($this->input->post('date') ? $this->input->post('date') : date('Y-m-d H:i:s')));
        }
        if (empty($date) || !$date || !isset($date)) {
            $date = date('Y-m-d H:i:s');
        }
        $date = $this->site->movement_setted_datetime($date);
        $sale_status = $this->input->post('sale_status');
        if ($sale_status == 'completed') {
            $this->form_validation->set_rules('paid_by[]', lang("paying_by"), 'required');
        }

        if ($this->form_validation->run() == true && !$fe_pos_sale_id) {
            $this->site->validate_movement_date($date);
            if ($this->site->invoice_date_outside_allowed_period($date, $this->input->post('document_type_id'))) {
                $this->session->set_flashdata("error", lang("invoice_date_out_of_period"));
            }
            $aiu_tax = $this->input->post('aiu_tax'); // order_tax_aiu_id
            $aiu_tax_base_apply_to = $this->input->post('aiu_tax_base_apply_to'); // order_tax_aiu_base_apply_to
            $aiu_tax_base = $this->input->post('aiu_tax_base'); // order_tax_aiu_base
            $aiu_tax_total = $this->input->post('aiu_tax_total'); // order_tax_aiu_total
            $aiu_admin_percentage = $this->input->post('aiu_admin_percentage'); // aiu_admin_percentage
            $aiu_admin_total = $this->input->post('aiu_admin_total'); // aiu_admin_total
            $aiu_imprev_percentage = $this->input->post('aiu_imprev_percentage'); // aiu_imprev_percentage
            $aiu_imprev_total = $this->input->post('aiu_imprev_total'); // aiu_imprev_total
            $aiu_util_percentage = $this->input->post('aiu_util_percentage'); // aiu_utilidad_percentage
            $aiu_util_total = $this->input->post('aiu_util_total'); // aiu_utilidad_total
            $afecta_aiu = $this->input->post('afecta_aiu'); // aiu_amounts_affects_grand_total
            $aiu_management = $this->input->post('aiu_management'); // aiu_management

            $except_category_taxes = $this->input->post('except_category_taxes') ? true : false;
            $tax_exempt_customer = $this->input->post('tax_exempt_customer') ? true : false;
            $orders_ids = json_decode($this->input->post('orders_ids'));
            $warehouse_id = $this->input->post('warehouse');
            $birthday_year_applied = $this->input->post('birthday_year_applied');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $total_items = $this->input->post('total_items');
            $payment_status = $this->input->post('payment_status');
            $sale_origin = $this->input->post('sale_origin');
            $sale_origin_reference_no = $this->input->post('sale_origin_reference_no');
            $unique_field = $this->input->post('unique_field');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->name) && $customer_details->name != '-' ? $customer_details->name : $customer_details->company;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $order = $this->input->post('order');
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
            $suspended_sale_id = $this->input->post('sid') ? $this->input->post('sid') : null;

            $total = 0;
            $product_tax = 0;
            $ipoconsumo_total = 0;
            $product_discount = 0;
            $digital = FALSE;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;

            //Retenciones
            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_id');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_id');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_id');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_id');

                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->sma->formatDecimal($this->input->post('rete_bomberil_valor'));
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_bomberil_id = $this->input->post('rete_bomberil_id');

                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->sma->formatDecimal($this->input->post('rete_autoaviso_valor'));
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $rete_autoaviso_id = $this->input->post('rete_autoaviso_id');


                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total + $rete_bomberil_total + $rete_autoaviso_total;

                $rete_applied = TRUE;
            } else {
                $rete_applied = FALSE;
                $total_retenciones = 0;
            }
            //Retenciones
            $txt_pprices_changed = false;
            $archivo = false;
            if (isset($_FILES["xls_file"])) {
                $archivo = $_FILES['xls_file']['tmp_name'];
            }
            if ($archivo) {
                $this->load->library('excel');
                $invalids_products = false;
                $objPHPExcel = PHPExcel_IOFactory::load($archivo);
                $hojas = $objPHPExcel->getWorksheetIterator();
                foreach ($hojas as $hoja) {
                    $data = $hoja->toArray();
                    unset($data[0]);
                    foreach ($data as $data_row) {
                        if (empty($data_row[0])) {
                            continue;
                        }
                        $product_excel['code'] = $data_row[0];
                        $product_excel['reference'] = $data_row[1];
                        $product_excel['name'] = $data_row[2];
                        $product_excel['quantity'] = $data_row[3];
                        $product_excel['unit_price'] = $data_row[4];

                        if ($product_excel_data = $this->products_model->get_product_by_code_reference($product_excel)) {
                            $ped_unit = $this->site->getUnitByID($product_excel_data['unit']);
                            $product_excel_data['product_id'] = $product_excel_data['id'];
                            $product_excel_data['tax_rate_id'] = $product_excel_data['tax_rate'];
                            $product_excel_data['real_unit_price'] = $product_excel['unit_price'];
                            $product_excel_data['quantity'] = $product_excel['quantity'];
                            if (!$tax_exempt_customer && $item_fixed = $this->site->fix_item_tax($document_type_id, $product_excel_data, true)) {
                            } else {
                                $item_fixed = false;
                            }
                            $product = array(
                                'product_id' => $product_excel_data['id'],
                                'product_code' => $product_excel_data['code'],
                                'product_name' => $product_excel_data['name'],
                                'product_type' => $product_excel_data['type'],
                                'net_unit_price' => $item_fixed ? $item_fixed['net_unit_price'] : NULL,
                                'unit_price' => $item_fixed ? $item_fixed['unit_price'] : NULL,
                                'quantity' => $product_excel['quantity'],
                                'product_unit_id' => $ped_unit ? $ped_unit->id : NULL,
                                'product_unit_code' => $ped_unit ? $ped_unit->code : NULL,
                                'unit_quantity' => $product_excel['quantity'],
                                'warehouse_id' => $warehouse_id,
                                'item_tax' => ($item_fixed ? $item_fixed['item_tax'] : NULL),
                                'tax_rate_id' => $item_fixed ? $item_fixed['tax_rate_id'] : NULL,
                                'tax' => $item_fixed ? $item_fixed['tax'] : NULL,
                                'subtotal' => $item_fixed ? $item_fixed['subtotal'] : NULL,
                                'real_unit_price' => $item_fixed ? $item_fixed['unit_price'] : NULL,
                                'price_before_tax' => $item_fixed ? $item_fixed['net_unit_price'] : NULL,
                                'price_before_promo' => $item_fixed ? $item_fixed['unit_price'] : NULL,
                                'seller_id'   => $this->input->post('seller_id'),
                            );
                            $products[] = $product;
                            $product_tax += $product['item_tax'];
                            $total += $product['net_unit_price'] * $product['quantity'];
                        } else {
                            $invalids_products = $invalids_products.($invalids_products ? "</br>" : "")." <b>Código</b> ".$product_excel['code']." <b>Referencia</b> ".$product_excel['reference']." <b>Nombre</b> ".$product_excel['name'];
                        }
                    }
                }
                if ($invalids_products) {
                            // exit(var_dump($invalids_products));
                    $this->session->set_userdata('invalid_csv_sale_products', '<h2>Los siguientes productos no existen</h2> : </br></br></br>'.$invalids_products);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
                for ($r = 0; $r < $i; $r++) {
                    $item_id = $_POST['product_id'][$r];
                    $ignore_hide_parameters = $_POST['ignore_hide_parameters'][$r];
                    $item_type = $_POST['product_type'][$r];
                    $item_code = $_POST['product_code'][$r];
                    $item_name = $_POST['product_name'][$r];
                    $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                    $item_net_price = $_POST['net_price'][$r]; /**/
                    $real_unit_price = $_POST['real_unit_price'][$r];
                    $unit_price = $_POST['unit_price'][$r];
                    $item_unit_quantity = $_POST['quantity'][$r];
                    $under_cost_authorized = $_POST['under_cost_authorized'][$r];
                    $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                    $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                    $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1; /**/
                    $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null; /**/
                    $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : 0;
                    $product_tax_rate_2 = isset($_POST['product_tax_rate_2'][$r]) ? $_POST['product_tax_rate_2'][$r] : 0; /**/
                    $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : 0; /**/
                    $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                    $item_unit = isset($_POST['product_unit'][$r]) ? $_POST['product_unit'][$r] : null;
                    $item_quantity = isset($_POST['product_base_quantity'][$r]) ? $_POST['product_base_quantity'][$r] : null;
                    $item_aquantity = isset($_POST['product_aqty'][$r]) ? $_POST['product_aqty'][$r] : null;
                    $item_preferences = isset($_POST['product_preferences_text'][$r]) ? $this->sma->preferences_selection($_POST['product_preferences_text'][$r]) : NULL;
                    $pr_item_discount = isset($_POST['product_discount_val'][$r]) ? $_POST['product_discount_val'][$r] : null; /**/
                    $price_before_promo = isset($_POST['price_before_promo'][$r]) ? $_POST['price_before_promo'][$r] : null; /**/
                    $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : null; /**/

                    $product_gift_card_no = isset($_POST['product_gift_card_no'][$r]) ? $_POST['product_gift_card_no'][$r] : null; /**/
                    $product_gift_card_value = isset($_POST['product_gift_card_value'][$r]) ? $_POST['product_gift_card_value'][$r] : null; /**/
                    $product_gift_card_expiry = isset($_POST['product_gift_card_expiry'][$r]) ? $_POST['product_gift_card_expiry'][$r] : null; /**/
                    $product_seller_id = isset($_POST['product_seller_id'][$r]) ? $_POST['product_seller_id'][$r] : null; /**/

                    if ($item_quantity > $item_aquantity && $this->Settings->overselling == 0 && $item_type != 'service' && $item_type != 'combo') {
                        $this->session->set_flashdata('error', lang("sale_cannot_be_completed_for_item_quantity"));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                    if (($item_code && $item_quantity && (($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2)) > 0) || $ignore_hide_parameters == 1) {
                        $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                        if ($item_type == 'digital') {
                            $digital = TRUE;
                        }
                        $pr_item_tax = $this->sma->formatDecimal((($item_unit_tax_val) * $item_unit_quantity));
                        $pr_item_tax_2 = $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_unit_quantity));
                        $ipoconsumo_total += $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_unit_quantity));
                        $unit = $this->site->getUnitByID($item_unit);
                        $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                        $product_discount += $pr_item_discount;
                        $product_tax += ($pr_item_tax + $pr_item_tax_2);

                        if ($price_before_promo > 0 && $under_cost_authorized == 1) {
                            if (!$txt_pprices_changed) {
                                $txt_pprices_changed .= sprintf(lang('user_sale_pprice_changed'), ($this->session->first_name . " " . $this->session->last_name));
                            }
                            $txt_pprices_changed .= sprintf(lang('product_sale_pprice_changed'), $item_name . " (" . $item_code . ")", $this->sma->formatMoney($price_before_promo), $this->sma->formatMoney($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2));
                        }

                        $product = array(
                            'product_id' => $item_id,
                            'product_code' => $item_code,
                            'product_name' => $item_name,
                            'product_type' => $item_type,
                            'option_id' => $item_option,
                            'net_unit_price' => $item_net_price,
                            'unit_price' => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2),
                            'quantity' => $item_quantity,
                            'product_unit_id' => $unit ? $unit->id : NULL,
                            'product_unit_code' => $unit ? $unit->code : NULL,
                            'unit_quantity' => $item_unit_quantity,
                            'warehouse_id' => $warehouse_id,
                            'item_tax' => $pr_item_tax,
                            'tax_rate_id' => $item_tax_rate,
                            'tax' => $product_tax_rate,
                            'item_tax_2' => $pr_item_tax_2,
                            'tax_rate_2_id' => $item_tax_rate_2,
                            'tax_2' => $product_tax_rate_2,
                            'discount' => $item_discount,
                            'item_discount' => $pr_item_discount,
                            'subtotal' => $this->sma->formatDecimal($subtotal),
                            'serial_no' => $serialModal_serial ? $serialModal_serial : $item_serial,
                            'real_unit_price' => $real_unit_price,
                            'price_before_tax' => $item_net_price + ($pr_item_discount / $item_quantity),
                            'consumption_sales' => $item_unit_tax_val_2,
                            'price_before_promo' => $price_before_promo,
                            'under_cost_authorized' => $under_cost_authorized,
                            'preferences' => $item_preferences,
                            'product_gift_card_no'   => $product_gift_card_no,
                            'product_gift_card_value'   => $product_gift_card_value,
                            'product_gift_card_expiry'   => $product_gift_card_expiry,
                            'seller_id'   => $product_seller_id,
                        );

                        if (!$tax_exempt_customer && $item_fixed = $this->site->fix_item_tax($document_type_id, $product)) {
                            $product_tax -= ($pr_item_tax + $pr_item_tax_2);
                            unset($product);
                            $product = $item_fixed;
                            $product_tax += ($item_fixed['item_tax'] + $item_fixed['item_tax_2']);
                            $item_net_price = $item_fixed['net_unit_price'];
                        }

                        $products[] = ($product + $gst_data);
                        $total += $this->sma->formatDecimal(($item_net_price * $item_quantity));
                    } else {
                        $this->session->set_flashdata('error', lang('invalid_products_data'));
                        redirect($_SERVER["HTTP_REFERER"]);
                    }
                }
                if (empty($products)) {
                    $this->form_validation->set_rules('product', lang("order_items"), 'required');
                }
            } //FIN RECORRIDO EXCEL PRODUCTOS
            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount));
            $actual_currency = $this->input->post('currency');
            $actual_currency_trm = $this->input->post('trm');

            $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
            $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);

            if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                $note .= sprintf(
                    lang('diferent_currency_trm'),
                    $actual_currency,
                    $current_currency->name,
                    $this->sma->formatMoney($actual_currency_trm),
                    $this->Settings->default_currency,
                    $default_currency->name,
                    $this->sma->formatMoney($grand_total)
                );
            }

            if ($except_category_taxes) {
                $note .= lang('except_category_taxes_text');
            }

            $document_type = $this->site->getDocumentTypeById($document_type_id);
            if ($this->site->invoice_date_existing_in_resolution_date_range($date, $document_type) == FALSE) {
                $this->session->set_flashdata("error", lang("invoice_date_out_of_range"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->site->invoice_date_outside_allowed_period($date, $this->input->post('document_type_id'))) {
                $this->session->set_flashdata("error", lang("invoice_date_out_of_period"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $resolucion = "";
            if ($document_type->save_resolution_in_sale == 1) {
                $resolucion = $this->site->textoResolucion($document_type);
            }

            if ($this->input->post('address_id')) {
                $address_id = $this->input->post('address_id');
            } else {
                $address_id = $this->site->getCustomerBranch($customer_id, true);
                if (!$address_id) {
                    $address_id = NULL;
                }
            }

            if (!$this->site->getCustomerBranchByCustomerAndAddresId($customer_id, $address_id)) {
                $this->session->set_flashdata('error', lang("invalid_customer_branch"));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            $address_data = $this->site->getAddressByID($address_id);
            $autorrete_amount = 0;
            $autorrete_perc = 0;

            if ($this->Settings->self_withholding_percentage > 0 && $document_type->key_log == 1) {
                $autorrete_perc_op = $this->Settings->self_withholding_percentage / 100;
                $autorrete_amount = $total * $autorrete_perc_op;
                $autorrete_perc = $this->Settings->self_withholding_percentage;
            }
            $disc_diff = $order_discount - $grand_total;
            $order_discount = (($disc_diff > 0 && $disc_diff < 1) || ($disc_diff > -1 && $disc_diff < 0)) ? $grand_total : $order_discount;
            $disc_diff = $total_discount - $grand_total;
            $total_discount = (($disc_diff > 0 && $disc_diff < 1) || ($disc_diff > -1 && $disc_diff < 0)) ? $grand_total : $total_discount;
            $data = array(
                'date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $this->sma->formatDecimal($grand_total, 2),
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'paid' => 0,
                'seller_id' => $this->input->post('seller_id'),
                'affiliate_id'   => $this->input->post('affiliate_id'),
                'address_id' => $address_id,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'resolucion' => $resolucion,
                'sale_currency' => !empty($actual_currency) ? $actual_currency : $this->Settings->default_currency,
                'sale_currency_trm' => $actual_currency_trm,
                'document_type_id' => $document_type_id,
                'sale_origin' => $sale_origin,
                'sale_origin_reference_no' => $sale_origin_reference_no,
                'consumption_sales' => $ipoconsumo_total,
                'sale_comm_perc' => $address_data ? $address_data->seller_sale_comision : NULL,
                'collection_comm_perc' => $address_data ? $address_data->seller_collection_comision : NULL,
                'self_withholding_amount' => $autorrete_amount,
                'self_withholding_percentage' => $autorrete_perc,
                'payment_status' => 'pending',
                'purchase_order' => $this->input->post('purchase_order'),
                'delivery_time_id' => $this->input->post('delivery_time_id'),
                'delivery_day' => $this->input->post('delivery_day'),
                'technology_provider' => $this->Settings->fe_technology_provider,
                'unique_field' => $unique_field,
                'recurring_sale' => $this->input->post('recurring_sale') ? 1 : 0,
            );

            if ($aiu_management == 1) {
                $data['order_tax_aiu_id'] = $aiu_tax;
                $data['order_tax_aiu_base_apply_to'] = $aiu_tax_base_apply_to;
                $data['order_tax_aiu_base'] = $aiu_tax_base;
                $data['order_tax_aiu_total'] = $aiu_tax_total;
                $data['aiu_admin_percentage'] = $aiu_admin_percentage;
                $data['aiu_admin_total'] = $aiu_admin_total;
                $data['aiu_imprev_percentage'] = $aiu_imprev_percentage;
                $data['aiu_imprev_total'] = $aiu_imprev_total;
                $data['aiu_utilidad_percentage'] = $aiu_util_percentage;
                $data['aiu_utilidad_total'] = $aiu_util_total;
                $data['aiu_amounts_affects_grand_total'] = $afecta_aiu;
                $data['aiu_management'] = $aiu_management;
                $data['grand_total'] += ($afecta_aiu ? ($aiu_admin_total + $aiu_imprev_total + $aiu_util_total) : 0) + $aiu_tax_total;
                $data['total_tax'] += $aiu_tax_total;
            }

            $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
            if ($this->Settings->modulary && $biller_data->rete_autoica_percentage > 0) {
                $autoica_perc_op = $biller_data->rete_autoica_percentage / 100;
                $autoica_amount = $total * $autoica_perc_op;
                $data['rete_autoica_percentage'] = $biller_data->rete_autoica_percentage;
                $data['rete_autoica_total'] = $autoica_amount;
                $data['rete_autoica_account'] = $biller_data->rete_autoica_account;
                $data['rete_autoica_account_counterpart'] = $biller_data->rete_autoica_account_counterpart;
                $data['rete_autoica_base'] = $total;
                if ($biller_data->rete_bomberil_percentage > 0) {
                    $bomberil_perc_op = $biller_data->rete_bomberil_percentage / 100;
                    $bomberil_amount = $autoica_amount * $bomberil_perc_op;
                    $data['rete_bomberil_percentage'] = $biller_data->rete_bomberil_percentage;
                    $data['rete_bomberil_total'] = $bomberil_amount;
                    $data['rete_bomberil_account'] = $biller_data->rete_bomberil_account;
                    $data['rete_bomberil_account_counterpart'] = $biller_data->rete_bomberil_account_counterpart;
                    $data['rete_bomberil_base'] = $autoica_amount;
                }
                if ($biller_data->rete_autoaviso_percentage > 0) {
                    $autoaviso_perc_op = $biller_data->rete_autoaviso_percentage / 100;
                    $autoaviso_amount = $autoica_amount * $autoaviso_perc_op;
                    $data['rete_autoaviso_percentage'] = $biller_data->rete_autoaviso_percentage;
                    $data['rete_autoaviso_total'] = $autoaviso_amount;
                    $data['rete_autoaviso_account'] = $biller_data->rete_autoaviso_account;
                    $data['rete_autoaviso_account_counterpart'] = $biller_data->rete_autoaviso_account_counterpart;
                    $data['rete_autoaviso_base'] = $autoica_amount;
                }
            }
            if ($biller_data->min_sale_amount > 0 && $grand_total < $biller_data->min_sale_amount) {
                $this->session->set_flashdata('error', sprintf(lang('min_sale_amount_error'), lang('sale'), $this->sma->formatMoney($biller_data->min_sale_amount)));
                redirect($_SERVER["HTTP_REFERER"]);
            }

            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }

            $payments = [];

            if ($rete_applied) {
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;
                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_bomberil_id'] = $rete_bomberil_id;
                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_autoaviso_id'] = $rete_autoaviso_id;
                $retencion = array(
                    'date'         => $date,
                    'amount'       => $total_retenciones,
                    'reference_no' => 'retencion',
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'received',
                    'note'         => 'Retenciones',
                );
                $payments[] = $retencion;
            } else {
                $retencion = false;
            }
            // PAGO
            $payment_reference_no = $this->input->post('payment_reference_no');
            $j = count($_POST['amount-paid']);
            $sale_payment_method = NULL;
            if ($j > 0 &&  $sale_status == 'completed') {
                for ($i = 0; $i < $j; $i++) {
                    if ($_POST['due_payment'][$i] == 1) {
                        $pm = $this->site->getPaymentMethodByCode($_POST['paid_by'][$i]);
                        $data['due_payment_method_id'] = $pm->id;
                    } else {
                        if ($_POST['paid_by'][$i] == 'deposit') {
                            if (!$this->site->check_customer_deposit($customer_id, $_POST['amount-paid'][$i])) {
                                $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }
                        if ($_POST['paid_by'][$i] == 'gift_card') {
                            $gc = $this->site->getGiftCardByNO($_POST['gift_card_no'][$i]);
                            $amount_paying = $grand_total >= $gc->balance ? $gc->balance : $grand_total;
                            $gc_balance = $gc->balance - $amount_paying;
                            $payment = array(
                                'date' => $date,
                                'amount' => $this->sma->formatDecimal($amount_paying),
                                'paid_by' => $_POST['paid_by'][$i],
                                'cheque_no' => $_POST['cheque_no'][$i],
                                'cc_no' => $_POST['gift_card_no'][$i],
                                'cc_holder' => $_POST['pcc_holder'][$i],
                                'cc_month' => $_POST['pcc_month'][$i],
                                'cc_year' => $_POST['pcc_year'][$i],
                                'cc_type' => $_POST['pcc_type'][$i],
                                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                'note' => $_POST['payment_note'][$i],
                                'type' => 'received',
                                'gc_balance' => $gc_balance,
                                'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i],
                                'document_type_id' => $payment_reference_no,
                            );
                            $payments[] = $payment;
                        } else {
                            $address_data = $this->site->getAddressByID($address_id);
                            $base_commision = (($_POST['amount-paid'][$i] / $data['grand_total']) * $data['total']);
                            $perc_commision = $address_data->seller_collection_comision / 100;
                            $amount_commision = $base_commision * $perc_commision;
                            $amount_paid = $_POST['amount-paid'][$i];
                            if ($_POST['amount-paid'][$i] == NULL || $_POST['amount-paid'][$i] <= 0) {
                                continue;
                            }
                            if ((float) $this->sma->formatDecimal($data['grand_total'] - $_POST['amount-paid'][$i]) < 1) {
                                $amount_paid = $data['grand_total'];
                            }
                            $payment = array(
                                'date' => $date,
                                'amount' => $amount_paid,
                                'paid_by' => $_POST['paid_by'][$i],
                                'cheque_no' => $_POST['cheque_no'][$i],
                                'cc_no' => $_POST['gift_card_no'][$i],
                                'cc_holder' => $_POST['pcc_holder'][$i],
                                'cc_month' => $_POST['pcc_month'][$i],
                                'cc_year' => $_POST['pcc_year'][$i],
                                'cc_type' => $_POST['pcc_type'][$i],
                                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                'note' => $_POST['payment_note'][$i],
                                'type' => 'received',
                                'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i],
                                'document_type_id' => $payment_reference_no,
                                'comm_base' => $base_commision,
                                'comm_amount' => $amount_commision,
                                'comm_perc' => ($perc_commision * 100),
                                'seller_id' => $data['seller_id'],
                                'document_type_id' => $payment_reference_no,
                            );
                            $payments[] = $payment;
                        }
                        if ($this->Settings->tax_rate_traslate && $data['total_tax'] > 0) {
                            $sale_taxes = [];
                            foreach ($products as $row) {
                                $proporcion_pago = ($_POST['amount-paid'][$i] * 100) / $data['grand_total'];
                                $total_iva = ($row['item_tax']) * ($proporcion_pago / 100);

                                if (!isset($sale_taxes[$row['tax_rate_id']])) {
                                    $sale_taxes[$row['tax_rate_id']] = $total_iva;
                                } else {
                                    $sale_taxes[$row['tax_rate_id']] += $total_iva;
                                }
                            }
                            $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
                            $data_tax_rate_traslate = $data_taxrate_traslate = array(
                                'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->receipt_ledger_id,
                                'sale_taxes' => $sale_taxes,
                            );
                        }
                    }
                    $sale_payment_method = $_POST['paid_by'][$i];
                }
            }
            if ($j > 1) {
                $sale_payment_method = 'mixed';
            }
            $data['payment_method'] = $sale_payment_method == NULL ? 'Credito' : $sale_payment_method;
			if ($_FILES['document']['size'] > 0) {
				$this->load->library('upload');
				$config['upload_path'] = $this->digital_upload_path;
				$config['allowed_types'] = $this->digital_file_types;
				$config['max_size'] = $this->allowed_file_size;
				$config['overwrite'] = false;
				$config['encrypt_name'] = true;
				$this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            $total_payment = 0;
            if (count($payments) > 0) {
                foreach ($payments as $payment) {
                    $total_payment += $payment['amount'];
                }
            }
            $data['grand_total'] = (Double) $this->sma->formatDecimal($data['grand_total']);
            $total_payment = (Double) $this->sma->formatDecimal($total_payment);

            if ($total_payment == $data['grand_total']) {
                $data['payment_status'] = 'paid';
                $data['payment_term'] = 0;
                $data['payment_method_fe'] = CASH;
                $data['portfolio_end_date'] = date('Y-m-d H:i:s');
            } else if ($total_payment != $data['grand_total']) {
                if ($total_payment == 0) {
                    $data['payment_status'] = 'pending';
                } else if ($total_payment > 0) {
                    $data['payment_status'] = 'partial';
                }
                $data['payment_term'] = $this->input->post('sale_payment_term');
                $data['payment_method_fe'] = CREDIT;
                $due_date = $this->input->post('sale_payment_term') ? date('Y-m-d', strtotime('+' . $this->input->post('sale_payment_term') . ' days', strtotime($date))) : null;
                $data['due_date'] = $due_date;

                if ($this->input->post('sale_payment_term') <= 0 && $sale_status == 'completed') {
                    $this->session->set_flashdata('error', lang("payment_term_almost_be_greather_than_zero"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                if (!$this->site->check_customer_credit_limit($customer_id, ($data['grand_total'] - $total_payment))) {
                    $this->session->set_flashdata('error', lang("customer_credit_limit_out_of_balance"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            // $this->sma->print_arrays($data, $products, $orders_ids);
            if (!$this->session->userdata('detal_post_processing')) {
                $this->session->set_userdata('detal_post_processing', 1);
            } else {
                $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
                admin_redirect("sales");
            }
            $txt_pprices_changed = trim($txt_pprices_changed, ', ');
            // exit(var_dump($grand_total));
            if ($grand_total <= 0) {
                $this->session->set_flashdata('error', 'El valor final de la factura no puede ser negativo.');
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($order) {
                $products = array_reverse($products);
            }
		}

        if ($this->form_validation->run() == true) {
            if (!$fe_pos_sale_id) {
                $sale_id = $this->sales_model->addSale($data, $products, $payments, null, (isset($data_tax_rate_traslate) ? $data_tax_rate_traslate : null), $quote_id, $order, $orders_ids);
                if ($suspended_sale_id) {
                    $this->pos_model->deleteBill($suspended_sale_id);
                }
            } else {
                $customer_id = $this->input->post('customer');
                $address_id = $this->input->post('address_id');
                $biller_id = $this->input->post('biller');
                $document_type_id = $this->input->post('document_type_id');
                $sale_origin = $this->input->post('sale_origin');
                $sale_origin_reference_no = $this->input->post('sale_origin_reference_no');
                $note = $this->sma->clear_tags($this->input->post('note'));
                $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
                $seller_id = $this->input->post('seller_id');
                $actual_currency = $this->input->post('currency');
                $actual_currency_trm = $this->input->post('trm');
                $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
                $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);
                if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                    $note .= sprintf(
                        lang('diferent_currency_trm'),
                        $actual_currency,
                        $current_currency->name,
                        $this->sma->formatMoney($actual_currency_trm),
                        $this->Settings->default_currency,
                        $default_currency->name,
                        $this->sma->formatMoney($grand_total)
                    );
                }
                $resolucion = "";
                $document_type = $this->site->getDocumentTypeById($document_type_id);
                if ($document_type->save_resolution_in_sale == 1) {
                    $resolucion = $this->site->textoResolucion($document_type);
                }
                $data = [
                    'customer_id' => $customer_id,
                    'address_id' => $address_id,
                    'biller_id' => $biller_id,
                    'document_type_id' => $document_type_id,
                    'seller_id' => $seller_id,
                    'sale_origin' => $sale_origin,
                    'sale_origin_reference_no' => $sale_origin_reference_no,
                    'note' => $note,
                    'staff_note' => $staff_note,
                    'sale_currency' => !empty($actual_currency) ? $actual_currency : $this->Settings->default_currency,
                    'sale_currency_trm' => $actual_currency_trm,
                    'resolucion' => $resolucion,
                ];

                $sale_id = $this->sales_model->sale_for_fe($fe_pos_sale_id, $data);
            }
            if ($birthday_year_applied) {
                $this->db->update('companies', ['birthday_year_applied' => $birthday_year_applied], ['id'=>$customer_id]);
            }

            /**********************************************************************************************************/
            $resolution_data = $this->site->getDocumentTypeById($this->input->post('document_type_id'));

            if ($resolution_data->factura_electronica == YES) {
                $sale_data = $this->site->getSaleByID($sale_id);

                $invoice_data = [
                    'sale_id' => $sale_id,
                    'biller_id' => $sale_data->biller_id,
                    'customer_id' => $sale_data->customer_id,
                    'reference' => $sale_data->reference_no,
                    'attachment' => $sale_data->attachment
                ];

                if ($resolution_data->factura_contingencia == NOT) {
                    $this->create_document_electronic($invoice_data);
                }
            }
            /**********************************************************************************************************/

            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_userdata('remove_pols', 1);
            $this->session->set_userdata('remove_oslls', 1);

            if ($this->session->userdata('detal_post_processing')) {
                $this->session->unset_userdata('detal_post_processing');
                $this->session->unset_userdata('detal_post_processing_model');
            }

            if (isset($data['sale_status']) && $data['sale_status'] == 'pending') {
                $msg_warn = lang('sale_pending');

                if ($this->Settings->modulary) {
                    $msg_warn .= lang('sale_pending_not_accounted');
                }

                $this->session->set_flashdata('warning', $msg_warn);
            } else {
                $this->session->set_flashdata('message', $this->lang->line("sale_added"));
            }

            if ($fe_pos_sale_id) {
                $this->sales_model->return_sale_for_fe($fe_pos_sale_id);
            }

            if (!empty($txt_pprices_changed)) {
                $this->db->insert('user_activities', [
                    'date' => date('Y-m-d H:i:s'),
                    'type_id' => 1,
                    'table_name' => 'sales',
                    'record_id' => $sale_id,
                    'user_id' => $this->session->userdata('user_id'),
                    'module_name' => $this->m,
                    'description' => $txt_pprices_changed,
                ]);
            }

            $quickPrintFormatId = $resolution_data->quick_print_format_id;
            if (isset($suspended_sale_id) && $suspended_sale_id) {
                if (!empty($quickPrintFormatId)) {
                    admin_redirect("sales/saleView/$sale_id/0/0/0/1/1");
                }
                admin_redirect("pos");
            } else {
                if ($resolution_data->factura_electronica == 1) {
                    $existing_sale = $this->site->getSaleByID($sale_id);
                    if ($existing_sale->fe_aceptado == ACCEPTED && !empty($quickPrintFormatId)) {
                        admin_redirect("sales/saleView/$sale_id/0/0/0/0/1");
                    }
                    admin_redirect("sales/fe_index");
                }
                admin_redirect("sales");
            }
        } else {
            if ($quote_id || $sale_id || $fe_pos_sale_id || $sid) {
                $this->data['is_quote'] = true;
                if ($quote_id && $order == false) {
                    $this->data['quote'] = $this->sales_model->getQuoteByID($quote_id);
                    $items = $this->sales_model->getAllQuoteItems($quote_id);
                } elseif ($quote_id && $order) {
                    $this->data['quote'] = $this->sales_model->getOrderByID($quote_id);
                    $items = $this->site->getAllOrderItems($quote_id);
                } elseif ($sale_id) {
                    $this->data['quote'] = $this->sales_model->getInvoiceByID($sale_id);
                    $items = $this->sales_model->getAllInvoiceItems($sale_id);
                } elseif ($fe_pos_sale_id) {
                    $this->data['quote'] = $this->sales_model->getInvoiceByID($fe_pos_sale_id);
                    $items = $this->sales_model->getAllInvoiceItems($fe_pos_sale_id);
                } elseif ($sid) {
                    $this->data['sid'] = $sid;
                    $this->data['stofe'] = $stofe;
                    $this->data['quote'] = $this->pos_model->getOpenBillByID($sid);
                    // $this->sma->print_arrays($this->data['quote']);
                    $items = $this->pos_model->getSuspendedSaleItems($sid);
                    // exit(var_dump($items));
                }
                $customer = $this->site->getCompanyByID($this->data['quote']->customer_id);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if ($quote_id && $this->Settings->product_order == 1) {
                        if (!isset($max_order_id)) {
                            $max_order_id = $item->id;
                        } else {
                            $max_order_id--;
                        }
                    }
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->name = $this->sma->clean_json_text($row->name);
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->seller_id = $item->seller_id;
                    $row->name = $this->sma->clean_json_text($item->product_name);
                    $row->type = $item->product_type;
                    if ($order) {
                        $row->qty = ($item->quantity_delivered > 0 ? $item->quantity - $item->quantity_delivered  : $item->quantity_to_bill);
                        $row->base_quantity = ($item->quantity_delivered > 0 ? $item->quantity - $item->quantity_delivered  : $item->quantity_to_bill);
                    } else {
                        $row->qty = $item->quantity;
                        $row->base_quantity = $item->quantity;
                    }
                    $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->unit = $item->product_unit_id;
                    $r_item_discount = $item->item_discount / $item->quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                    // $row->unit_price = $row->tax_method ?
                    //     $item->unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity) - $this->sma->formatDecimal($item->item_tax / $item->quantity) :
                    //     $item->unit_price + ($item->item_discount / $item->quantity);
                    $row->unit_price = $item->unit_price - ($row->tax_method ? $this->sma->formatDecimal($item->item_tax / $item->quantity) : ($item->item_tax_2  / $item->quantity));
                    $row->real_unit_price = $item->real_unit_price;
                    $row->prev_unit_price = ($row->tax_method == 1 ? $item->net_unit_price : $item->unit_price) + $r_item_discount;
                    $row->base_unit_price = $row->real_unit_price;

                    $row->diferent_tax_alert = false;
                    $real_tax_rate = null;
                    if ($item->tax_rate_id != $row->tax_rate) {
                        $row->diferent_tax_alert = true;
                        $row->real_tax_rate = $row->tax_rate;
                        $real_tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    }
                    $row->tax_rate = $item->tax_rate_id;
                    if ($customer->tax_exempt_customer == 1 && $this->Settings->enable_customer_tax_exemption == 1) {
                        if ($row->tax_method == 0) {
                            $row->unit_price = $this->sma->remove_tax_from_amount($row->tax_rate, $row->unit_price);
                        }
                        $row->tax_rate = $this->Settings->customer_territorial_decree_tax_rate_id ? $this->Settings->customer_territorial_decree_tax_rate_id : $this->site->get_tax_exempt();
                        $row->tax_exempt_customer = true;
                        $real_tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    }
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $row->product_unit_id_selected = $item->product_unit_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                    $combo_items = false;
                    $row->price_before_tax = $item->price_before_tax;
                    $row->consumption_sale_tax = $item->tax_rate_2_id ? $item->tax_rate_2_id : 0;
                    if ($this->Settings->product_variant_per_serial == 1) {
                        $row->serialModal_serial = $item->serial_no;
                    }
                    if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                        continue;
                    }
                    if ($row->qty <= 0 && $this->Settings->display_all_products == 0 && $row->ignore_hide_parameters != 1) {
                        continue;
                    }
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->get_product_units($row->id);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                        // exit(var_dump($tax_rate));
                    // $ri = $this->Settings->item_addition ? $row->id. ($item->option_id > 0 ? $item->option_id : '') : $c;
                    $ri = $item->id;
                    if ($row->qty == 0) {
                        continue;
                    }
                    if ($this->Settings->product_preferences_management == 1) {
                        $preferences = $this->sales_model->getProductPreferences($item->product_id);
                    } else {
                        $preferences = false;
                    }

                    $gift_card = [];
                    if ($item->product_id == $this->Settings->gift_card_product_id) {
                        $gift_card = [
                            'gc_card_no'=>NULL,
                            'gc_value'=>$row->unit_price,
                            'gc_expiry'=>date("Y-m-d", strtotime("+1 year")),
                        ];
                    }

                    $pr[$ri] = array(
                        'id' => $item->id,
                        'item_id' => $item->id,
                        'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row,
                        'combo_items' => $combo_items,
                        'tax_rate' => $tax_rate,
                        'real_tax_rate' => $real_tax_rate,
                        'units' => $units,
                        'options' => $options,
                        'order' => $item->id,
                        'preferences' => $preferences,
                        'gift_card' => $gift_card,
                        'preferences_text' => $this->sma->print_preference_selection($item->preferences),
                        'preferences_selected' => json_decode($item->preferences ? $item->preferences : ""),
                    );
                    if (isset($max_order_id)) {
                        $pr[$ri]['order'] = $max_order_id;
                    }
                    $c++;
                }
                // $this->sma->print_arrays($pr);
                $this->data['quote_items'] = json_encode($pr);
            } else if ($orders_ids && count($orders_ids) > 0) {
                $this->data['is_quote'] = true;
                $this->data['orders_ids_id'] = $orders_ids[0];
                $customer_orders = NULL;
                $customer_address_orders = NULL;
                $txt_osl_references = '';
                $orders = [];
                $main_order= null;
                $orders_rows = [];
                asort($orders_ids);
                foreach ($orders_ids as $key => $oid) {
                    $orders[$oid]['data'] = $this->sales_model->getOrderByID($oid);
                    $orders[$oid]['rows'] = $this->sales_model->getAllOrderSaleItems($oid);
                    $txt_osl_references.=$orders[$oid]['data']->reference_no.", ";
                    if ($main_order == null) {
                        $main_order = $orders[$oid]['data'];
                    }
                    foreach ($orders[$oid]['rows'] as $orrow) {
                        $orders_rows[] = $orrow;
                    }
                    if ($customer_orders == NULL) {
                        $customer_orders = $orders[$oid]['data']->customer_id;
                    } else if ($customer_orders != NULL && $customer_orders != $orders[$oid]['data']->customer_id) {
                        $this->session->set_flashdata('error', lang("complete_orders_different_customer"));
                        admin_redirect($_SERVER['HTTP_REFERER']);
                    }
                    if ($customer_address_orders == NULL) {
                        $customer_address_orders = $orders[$oid]['data']->address_id;
                    } else if ($customer_address_orders != NULL && $customer_address_orders != $orders[$oid]['data']->address_id) {
                        $this->session->set_flashdata('error', lang("complete_orders_different_customer"));
                        admin_redirect($_SERVER['HTTP_REFERER']);
                    }
                    if ($orders[$oid]['data']->sale_status != 'pending' && $orders[$oid]['data']->sale_status != 'partial') {
                        $this->session->set_flashdata('error', lang("complete_orders_status_error"));
                        admin_redirect($_SERVER['HTTP_REFERER']);
                    }
                }
                $orders_rows = array_merge([], $orders_rows);
                $customer = $this->site->getCompanyByID($customer_orders);
                $c = rand(100000, 9999999);
                // $this->sma->print_arrays($orders_rows);
                foreach ($orders_rows as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->name = $this->sma->clean_json_text($row->name);
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $this->sma->clean_json_text($item->product_name);
                    $row->type = $item->product_type;
                    $row->qty = $item->quantity - $item->quantity_delivered;
                    $row->base_quantity = $item->quantity - $item->quantity_delivered;
                    $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->unit = $item->product_unit_id;
                    $r_item_discount = $item->item_discount / $item->quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));

                    $row->unit_price = $item->unit_price - ($row->tax_method ? $this->sma->formatDecimal($item->item_tax / $item->quantity) : 0);
                    $row->real_unit_price = $row->unit_price;
                    $row->prev_unit_price = $row->unit_price;
                    $row->base_unit_price = $row->unit_price;

                    $row->diferent_tax_alert = false;
                    $real_tax_rate = null;
                    if ($item->tax_rate_id != $row->tax_rate) {
                        $row->diferent_tax_alert = true;
                        $row->real_tax_rate = $row->tax_rate;
                        $real_tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    }
                    $row->tax_rate = $item->tax_rate_id;
                    if ($customer->tax_exempt_customer == 1 && $this->Settings->enable_customer_tax_exemption == 1) {
                        if ($row->tax_method == 0) {
                            $row->unit_price = $this->sma->remove_tax_from_amount($row->tax_rate, $row->unit_price);
                        }
                        $row->tax_rate = $this->Settings->customer_territorial_decree_tax_rate_id ? $this->Settings->customer_territorial_decree_tax_rate_id : $this->site->get_tax_exempt();
                        $row->tax_exempt_customer = true;
                        $real_tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    }
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $row->product_unit_id_selected = $item->product_unit_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                    $combo_items = false;
                    $row->price_before_tax = $item->price_before_tax;
                    $row->consumption_sale_tax = $item->tax_rate_2_id ? $item->tax_rate_2_id : 0;
                    if ($this->Settings->product_variant_per_serial == 1) {
                        $row->serialModal_serial = $item->serial_no;
                    }
                    if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                        continue;
                    }
                    if ($row->qty <= 0 && $this->Settings->display_all_products == 0 && $row->ignore_hide_parameters != 1) {
                        continue;
                    }
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->get_product_units($row->id);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    if ($units) {
                        foreach ($units as $key => $value) {
                            $units[$key]->valor_unitario = $row->unit_price;
                            $units[$key]->valor_unitario_old = $row->unit_price;
                        }
                    }
                        
                        // exit(var_dump($tax_rate));
                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    if ($row->qty == 0) {
                        continue;
                    }
                    if ($this->Settings->product_preferences_management == 1) {
                        $preferences = $this->sales_model->getProductPreferences($item->product_id);
                    } else {
                        $preferences = false;
                    }

                    $gift_card = [];
                    if ($item->product_id == $this->Settings->gift_card_product_id) {
                        $gift_card = [
                            'gc_card_no'=>NULL,
                            'gc_value'=>$row->unit_price,
                            'gc_expiry'=>date("Y-m-d", strtotime("+1 year")),
                        ];
                    }

                    $pr[$ri] = array(
                        'id' => $ri,
                        'item_id' => $row->id,
                        'order' => round(microtime(true) * 1000),
                        'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row,
                        'combo_items' => $combo_items,
                        'tax_rate' => $tax_rate,
                        'real_tax_rate' => $real_tax_rate,
                        'units' => $units,
                        'options' => $options,
                        'preferences' => $preferences,
                        'gift_card' => $gift_card,
                        'preferences_text' => $this->sma->print_preference_selection($item->preferences),
                        'preferences_selected' => json_decode($item->preferences),
                    );
                    $c++;
                }
                // $this->sma->print_arrays($pr);
                $this->data['orders_rows'] = json_encode($pr);
                $this->data['orders'] = $orders;
                $this->data['main_order'] = $main_order;
                $this->data['orders_ids'] = json_encode($orders_ids);
                $this->data['txt_osl_references'] = sprintf(lang('completed_orders_references'), trim($txt_osl_references, ", "));
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['quote_id'] = $quote_id ? $quote_id : $sale_id;
            if ($sale_id) {
                $this->data['is_duplicate'] = true;
            }
            $this->data['fe_pos_sale_id'] = $fe_pos_sale_id;
            $this->data['order'] = $order;
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['reference'] = $this->Settings->sales_prefix . "-";
            $this->data['slnumber'] = '';
            $this->data['payment_ref'] = '';
            $this->data['prioridad_por_unidad'] = ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? true : false;
            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            $this->data['brands'] = $this->site->getAllBrands();

            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale')));
            $meta = array('page_title' => lang('add_sale'), 'bc' => $bc);
            $this->page_construct('sales/add', $meta, $this->data);
        }
    }

    public function edit($id = null)
    {
        $this->sma->checkPermissions();
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getInvoiceByID($id);
        $inv_payments = $this->sales_model->getInvoicePayments($id);
        $sale_has_payments = false;
        if ($inv_payments) {
            $sale_has_payments = true;
        }
        if (!$this->Owner && !$this->Admin && $inv->sale_status != 'pending') {

            $this->session->set_flashdata('error', lang('cannot_edit_sale_completed'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        if ($inv->sale_status == 'returned') {
            $this->session->set_flashdata('error', lang('sale_x_action'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        // else if (date('m', strtotime($inv->date)) != date('m')) {
        //     $this->session->set_flashdata('error', lang('cannot_edit_purchase_diferent_month'));
        //     admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        // }
        if (!$this->session->userdata('edit_right')) {
            $this->sma->view_rights($inv->created_by);
        }
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            }
            if (empty($date) || !$date) {
                $date = date('Y-m-d H:i:s');
            }

            $aiu_tax = $this->input->post('aiu_tax'); // order_tax_aiu_id
            $aiu_tax_base_apply_to = $this->input->post('aiu_tax_base_apply_to'); // order_tax_aiu_base_apply_to
            $aiu_tax_base = $this->input->post('aiu_tax_base'); // order_tax_aiu_base
            $aiu_tax_total = $this->input->post('aiu_tax_total'); // order_tax_aiu_total
            $aiu_admin_percentage = $this->input->post('aiu_admin_percentage'); // aiu_admin_percentage
            $aiu_admin_total = $this->input->post('aiu_admin_total'); // aiu_admin_total
            $aiu_imprev_percentage = $this->input->post('aiu_imprev_percentage'); // aiu_imprev_percentage
            $aiu_imprev_total = $this->input->post('aiu_imprev_total'); // aiu_imprev_total
            $aiu_util_percentage = $this->input->post('aiu_util_percentage'); // aiu_utilidad_percentage
            $aiu_util_total = $this->input->post('aiu_util_total'); // aiu_utilidad_total
            $afecta_aiu = $this->input->post('afecta_aiu'); // aiu_amounts_affects_grand_total
            $aiu_management = $this->input->post('aiu_management'); // aiu_management


            $except_category_taxes = $this->input->post('except_category_taxes') ? true : false;
            $tax_exempt_customer = $this->input->post('tax_exempt_customer') ? true : false;
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            // $sale_origin = $this->input->post('sale_origin');
            // $sale_origin_reference_no = $this->input->post('sale_origin_reference_no');
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->name) && $customer_details->name != '-' ? $customer_details->name : $customer_details->company;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
            $order = $this->input->post('order');
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false && $this->Settings->modulary == 1) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
            $suspended_sale_id = $this->input->post('sid') ? $this->input->post('sid') : null;
            $total = 0;
            $product_tax = 0;
            $ipoconsumo_total = 0;
            $product_discount = 0;
            $digital = FALSE;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            //Retenciones
            if ($this->input->post('rete_applied')) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_fuente_id = $this->input->post('rete_fuente_id');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_iva_id = $this->input->post('rete_iva_id');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_ica_id = $this->input->post('rete_ica_id');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_other_id = $this->input->post('rete_otros_id');

                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;

                $rete_applied = TRUE;
            } else {
                $rete_applied = FALSE;
                $total_retenciones = 0;
            }
            //Retenciones

            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $product_sale_item_id = $_POST['product_sale_item_id'][$r];
                $ignore_hide_parameters = $_POST['ignore_hide_parameters'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $item_net_price = $_POST['net_price'][$r]; /**/
                $real_unit_price = $_POST['real_unit_price'][$r];
                $unit_price = $_POST['unit_price'][$r];
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';

                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1; /**/
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null; /**/
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : 0;
                $product_tax_rate_2 = isset($_POST['product_tax_rate_2'][$r]) ? $_POST['product_tax_rate_2'][$r] : 0; /**/
                $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : 0; /**/

                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = isset($_POST['product_unit'][$r]) ? $_POST['product_unit'][$r] : null;
                $item_quantity = isset($_POST['product_base_quantity'][$r]) ? $_POST['product_base_quantity'][$r] : null;
                $item_aquantity = isset($_POST['product_aqty'][$r]) ? $_POST['product_aqty'][$r] : null;
                $pr_item_discount = isset($_POST['product_discount_val'][$r]) ? $_POST['product_discount_val'][$r] : null; /**/
                $price_before_promo = isset($_POST['price_before_promo'][$r]) ? $_POST['price_before_promo'][$r] : null; /**/
                $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : null; /**/

                if ($sale_status == 'completed' && $item_quantity > $item_aquantity && $this->Settings->overselling == 0 && $item_type != 'service' && $item_type != 'combo') {
                    $this->session->set_flashdata('error', lang("sale_cannot_be_completed_for_item_quantity"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if (($item_code && $item_quantity && (($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2)) > 0) || $ignore_hide_parameters == 1) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;

                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }
                    $pr_item_tax = $this->sma->formatDecimal((($item_unit_tax_val) * $item_unit_quantity));
                    $pr_item_tax_2 = $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_unit_quantity));
                    $ipoconsumo_total += $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_unit_quantity));
                    $unit = $this->site->getUnitByID($item_unit);
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $product_discount += $pr_item_discount;
                    $product_tax += ($pr_item_tax + $pr_item_tax_2);
                    $product = array(
                        'id' => $product_sale_item_id,
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $product_tax_rate,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_rate_2,
                        'tax_2' => $product_tax_rate_2,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $serialModal_serial ? $serialModal_serial : $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'price_before_tax' => $item_net_price + ($pr_item_discount / $item_quantity),
                        'consumption_sales' => $item_unit_tax_val_2,
                        'price_before_promo' => $price_before_promo,
                    );
                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_quantity));
                } else {
                    $this->session->set_flashdata('error', lang('invalid_products_data'));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount));
            $actual_currency = $this->input->post('currency');
            $actual_currency_trm = $this->input->post('trm');

            $current_currency = $this->currency_model->get_currency_by_code($actual_currency);
            $default_currency = $this->currency_model->get_currency_by_code($this->Settings->default_currency);

            if ($actual_currency != $this->Settings->default_currency && $quote_id == null) {
                $note .= sprintf(
                    lang('diferent_currency_trm'),
                    $actual_currency,
                    $current_currency->name,
                    $this->sma->formatMoney($actual_currency_trm),
                    $this->Settings->default_currency,
                    $default_currency->name,
                    $this->sma->formatMoney($grand_total)
                );
            }
            if ($except_category_taxes) {
                $note .= lang('except_category_taxes_text');
            }
            $document_type = $this->site->getDocumentTypeById($document_type_id);
            $resolucion = "";
            if ($document_type->save_resolution_in_sale == 1) {
                $resolucion = $this->site->textoResolucion($document_type);
            }
            if ($this->input->post('address_id')) {
                $address_id = $this->input->post('address_id');
            } else {
                $address_id = $this->site->getCustomerBranch($customer_id, true);
                if (!$address_id) {
                    $address_id = NULL;
                }
            }
            if (!$this->site->getCustomerBranchByCustomerAndAddresId($customer_id, $address_id)) {
                $this->session->set_flashdata('error', lang("invalid_customer_branch"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $address_data = $this->site->getAddressByID($address_id);
            $autorrete_amount = 0;
            $autorrete_perc = 0;
            if ($this->Settings->self_withholding_percentage > 0 && $document_type->key_log == 1) {
                $autorrete_perc_op = $this->Settings->self_withholding_percentage / 100;
                $autorrete_amount = $total * $autorrete_perc_op;
                $autorrete_perc = $this->Settings->self_withholding_percentage;
            }
            $data = array(
                'date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $this->sma->formatDecimal($grand_total, 2),
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'paid' => 0,
                'seller_id' => $this->input->post('seller_id'),
                'address_id' => $address_id,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'resolucion' => $resolucion,
                'sale_currency' => $actual_currency,
                'sale_currency_trm' => $actual_currency_trm,
                'document_type_id' => $document_type_id,
                // 'sale_origin' => $sale_origin,
                // 'sale_origin_reference_no' => $sale_origin_reference_no,
                'consumption_sales' => $ipoconsumo_total,
                'sale_comm_perc' => $address_data ? $address_data->seller_sale_comision : NULL,
                'collection_comm_perc' => $address_data ? $address_data->seller_collection_comision : NULL,
                'self_withholding_amount' => $autorrete_amount,
                'self_withholding_percentage' => $autorrete_perc,

            );

            if ($aiu_management == 1) {
                $data['order_tax_aiu_id'] = $aiu_tax;
                $data['order_tax_aiu_base_apply_to'] = $aiu_tax_base_apply_to;
                $data['order_tax_aiu_base'] = $aiu_tax_base;
                $data['order_tax_aiu_total'] = $aiu_tax_total;
                $data['aiu_admin_percentage'] = $aiu_admin_percentage;
                $data['aiu_admin_total'] = $aiu_admin_total;
                $data['aiu_imprev_percentage'] = $aiu_imprev_percentage;
                $data['aiu_imprev_total'] = $aiu_imprev_total;
                $data['aiu_utilidad_percentage'] = $aiu_util_percentage;
                $data['aiu_utilidad_total'] = $aiu_util_total;
                $data['aiu_amounts_affects_grand_total'] = $afecta_aiu;
                $data['aiu_management'] = $aiu_management;
                $data['grand_total'] += ($afecta_aiu ? ($aiu_admin_total + $aiu_imprev_total + $aiu_util_total) : 0) + $aiu_tax_total;
                $data['total_tax'] += $aiu_tax_total;
            }

            $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
            if ($biller_data->rete_autoica_percentage > 0) {
                $autoica_perc_op = $biller_data->rete_autoica_percentage / 100;
                $autoica_amount = $total * $autoica_perc_op;
                $data['rete_autoica_percentage'] = $biller_data->rete_autoica_percentage;
                $data['rete_autoica_total'] = $autoica_amount;
                $data['rete_autoica_account'] = $biller_data->rete_autoica_account;
                $data['rete_autoica_account_counterpart'] = $biller_data->rete_autoica_account_counterpart;
                $data['rete_autoica_base'] = $total;
                if ($biller_data->rete_bomberil_percentage > 0) {
                    $bomberil_perc_op = $biller_data->rete_bomberil_percentage / 100;
                    $bomberil_amount = $autoica_amount * $bomberil_perc_op;
                    $data['rete_bomberil_percentage'] = $biller_data->rete_bomberil_percentage;
                    $data['rete_bomberil_total'] = $bomberil_amount;
                    $data['rete_bomberil_account'] = $biller_data->rete_bomberil_account;
                    $data['rete_bomberil_account_counterpart'] = $biller_data->rete_bomberil_account_counterpart;
                    $data['rete_bomberil_base'] = $autoica_amount;
                }
                if ($biller_data->rete_autoaviso_percentage > 0) {
                    $autoaviso_perc_op = $biller_data->rete_autoaviso_percentage / 100;
                    $autoaviso_amount = $autoica_amount * $autoaviso_perc_op;
                    $data['rete_autoaviso_percentage'] = $biller_data->rete_autoaviso_percentage;
                    $data['rete_autoaviso_total'] = $autoaviso_amount;
                    $data['rete_autoaviso_account'] = $biller_data->rete_autoaviso_account;
                    $data['rete_autoaviso_account_counterpart'] = $biller_data->rete_autoaviso_account_counterpart;
                    $data['rete_autoaviso_base'] = $autoica_amount;
                }
            }

            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }

            $payments = [];

            if ($rete_applied) {
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_fuente_id'] = $rete_fuente_id;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_iva_id'] = $rete_iva_id;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_ica_id'] = $rete_ica_id;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_other_id'] = $rete_other_id;
                $retencion = array(
                    'date'         => $date,
                    'amount'       => $total_retenciones,
                    'reference_no' => 'retencion',
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'received',
                    'note'         => 'Retenciones',
                    'sale_id'      => $id,
                );
                $payments[] = $retencion;
                $data['payment_status'] = 'partial';
            } else {
                $retencion = false;
            }
            // PAGO
            $payment_reference_no = $this->input->post('payment_reference_no');
            $j = count($_POST['amount-paid']);
            $sale_payment_method = NULL;
            if ($j > 0 && $sale_status == 'completed') {
                for ($i = 0; $i < $j; $i++) {
                    if ($_POST['due_payment'][$i] == 1) {
                        $pm = $this->site->getPaymentMethodByCode($_POST['paid_by'][$i]);
                        $data['due_payment_method_id'] = $pm->id;
                    } else {
                        if ($_POST['paid_by'][$i] == 'deposit') {
                            if (!$this->site->check_customer_deposit($customer_id, $_POST['amount-paid'][$i])) {
                                $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                        }
                        if ($_POST['paid_by'][$i] == 'gift_card') {
                            $gc = $this->site->getGiftCardByNO($_POST['gift_card_no'][$i]);
                            $amount_paying = $grand_total >= $gc->balance ? $gc->balance : $grand_total;
                            $gc_balance = $gc->balance - $amount_paying;
                            $payment = array(
                                'date' => $date,
                                'amount' => $this->sma->formatDecimal($amount_paying),
                                'paid_by' => $_POST['paid_by'][$i],
                                'cheque_no' => $_POST['cheque_no'][$i],
                                'cc_no' => $_POST['gift_card_no'][$i],
                                'cc_holder' => $_POST['pcc_holder'][$i],
                                'cc_month' => $_POST['pcc_month'][$i],
                                'cc_year' => $_POST['pcc_year'][$i],
                                'cc_type' => $_POST['pcc_type'][$i],
                                'note' => $_POST['payment_note'][$i],
                                'type' => 'received',
                                'gc_balance' => $gc_balance,
                                'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i],
                                'document_type_id' => $payment_reference_no,
                                'sale_id'      => $id,
                            );
                            $payments[] = $payment;
                        } else {
                            $address_data = $this->site->getAddressByID($address_id);
                            $base_commision = (($_POST['amount-paid'][$i] / $data['grand_total']) * $data['total']);
                            $perc_commision = $address_data->seller_collection_comision / 100;
                            $amount_commision = $base_commision * $perc_commision;
                            $amount_paid = $_POST['amount-paid'][$i];
                            if ($_POST['amount-paid'][$i] == NULL || $_POST['amount-paid'][$i] <= 0) {
                                continue;
                            }
                            if ((float) $this->sma->formatDecimal($data['grand_total'] - $_POST['amount-paid'][$i]) < 1) {
                                $amount_paid = $data['grand_total'];
                            }
                            $payment = array(
                                'date' => $date,
                                'amount' => $amount_paid,
                                'paid_by' => $_POST['paid_by'][$i],
                                'cheque_no' => $_POST['cheque_no'][$i],
                                'cc_no' => $_POST['gift_card_no'][$i],
                                'cc_holder' => $_POST['pcc_holder'][$i],
                                'cc_month' => $_POST['pcc_month'][$i],
                                'cc_year' => $_POST['pcc_year'][$i],
                                'cc_type' => $_POST['pcc_type'][$i],
                                'note' => $_POST['payment_note'][$i],
                                'type' => 'received',
                                'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i],
                                'document_type_id' => $payment_reference_no,
                                'comm_base' => $base_commision,
                                'comm_amount' => $amount_commision,
                                'comm_perc' => ($perc_commision * 100),
                                'seller_id' => $data['seller_id'],
                                'sale_id'      => $id,
                            );
                            $payments[] = $payment;
                        }

                        if ($this->Settings->tax_rate_traslate && $data['total_tax'] > 0) {
                            $sale_taxes = [];
                            foreach ($products as $row) {
                                $proporcion_pago = ($_POST['amount-paid'][$i] * 100) / $data['grand_total'];
                                $total_iva = ($row['item_tax']) * ($proporcion_pago / 100);

                                if (!isset($sale_taxes[$row['tax_rate_id']])) {
                                    $sale_taxes[$row['tax_rate_id']] = $total_iva;
                                } else {
                                    $sale_taxes[$row['tax_rate_id']] += $total_iva;
                                }
                            }
                            $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
                            $data_tax_rate_traslate = $data_taxrate_traslate = array(
                                'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->receipt_ledger_id,
                                'sale_taxes' => $sale_taxes,
                            );
                        }
                    }
                    $sale_payment_method = $_POST['paid_by'][$i];
                }
            }

            if ($j > 1) {
                $sale_payment_method = 'mixed';
            }

            $data['payment_method'] = $sale_payment_method;

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            $total_payment = 0;
            if (count($payments) > 0) {
                foreach ($payments as $payment) {
                    $total_payment += $payment['amount'];
                }
            }
            if ($sale_status == 'completed') {
                if ($total_payment == $data['grand_total']) {
                    $data['payment_status'] = 'paid';
                    $data['payment_term'] = 0;
                    $data['payment_method_fe'] = CASH;
                } else if ($total_payment < $data['grand_total']) {
                    if ($total_payment == 0) {
                        $data['payment_status'] = 'pending';
                    } else if ($total_payment > 0) {
                        $data['payment_status'] = 'partial';
                    }
                    $data['payment_term'] = $this->input->post('sale_payment_term');
                    $data['payment_method_fe'] = CREDIT;
                    $due_date = $this->input->post('sale_payment_term') ? date('Y-m-d', strtotime('+' . $this->input->post('sale_payment_term') . ' days', strtotime($date))) : null;
                    $data['due_date'] = $due_date;
                    if ($this->input->post('sale_payment_term') <= 0) {
                        $this->session->set_flashdata('error', lang("payment_term_almost_be_greather_than_zero"));
                        admin_redirect($_SERVER['HTTP_REFERER']);
                    }
                }
            }
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateSale($id, $data, $products, $payments)) {
            $txt_changed = ($this->session->first_name . " " . $this->session->last_name)." Hizo los siguientes cambios en la venta ".$inv->reference_no." : ";
            foreach ($inv as $old_inv_key => $old_inv_value) {
                if (isset($data[$old_inv_key]) && $data[$old_inv_key] != $old_inv_value) {
                    $txt_changed .= lang($old_inv_key)." cambió de ".$old_inv_value." a ".$data[$old_inv_key].", ";
                }
            }
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 1,
                'table_name' => 'sales',
                'record_id' => $id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $txt_changed ,
            ]);
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', lang("sale_updated"));
            admin_redirect($inv->pos ? 'pos/sales' : 'sales');
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['inv'] = $this->sales_model->getInvoiceByID($id);
            if ($this->Settings->disable_editing) {
                if ($this->data['inv']->date <= date('Y-m-d', strtotime('-' . $this->Settings->disable_editing . ' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("sale_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $inv_items = $this->sales_model->getAllInvoiceItems($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);

            $warehouse = $this->site->getWarehouseByID($inv->warehouse_id);
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
            $biller = $inv->biller_id ? $this->site->getAllCompaniesWithState('biller', $inv->biller_id) : false;
            // $this->sma->print_arrays($inv_items);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                $row->sale_item_id = $item->id;
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                } else {
                    unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }
                $row->name = $this->sma->clean_json_text($row->name);
                $row->quantity = 0;
                $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->code = $item->product_code;
                $row->name = $this->sma->clean_json_text($item->product_name);
                $row->type = $item->product_type;

                $row->qty = $item->quantity;
                $row->base_quantity = $item->quantity;
                $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                $row->unit = $item->product_unit_id;
                $r_item_discount = $item->item_discount / $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $item->unit_price - ($row->tax_method ? $this->sma->formatDecimal($item->item_tax / $item->quantity) : 0);
                $row->real_unit_price = $item->real_unit_price;
                $row->prev_unit_price = ($row->tax_method == 1 ? $item->net_unit_price : $item->unit_price) + $r_item_discount;
                $row->base_unit_price = $row->unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->serial = '';
                $row->option = $item->option_id;
                $row->product_unit_id_selected = $item->product_unit_id;
                $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                $combo_items = false;
                $row->price_before_tax = $item->price_before_tax;
                $row->consumption_sale_tax = $item->tax_rate_2_id ? $item->tax_rate_2_id : 0;
                if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                    continue;
                }
                if ($row->qty <= 0 && $this->Settings->display_all_products == 0 && $row->ignore_hide_parameters != 1) {
                    continue;
                }
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                // $ri = $this->Settings->item_addition ? $row->id : $c;
                $ri = $item->id;
                if ($row->qty == 0) {
                    continue;
                }
                $pr[$ri] = array(
                    'id' => $item->id, 
                    'item_id' => $item->id, 
                    'label' => $row->name . " (" . $row->code . ")", 
                    'row' => $row, 
                    'combo_items' => $combo_items, 
                    'tax_rate' => $tax_rate, 
                    'units' => $units, 
                    'options' => $options, 
                    'order' => $item->id);
                $c++;
            }
            // $this->sma->print_arrays($pr);
            $this->data['inv_items'] = json_encode($pr);
            $this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['sale_has_payments'] = $sale_has_payments;
            $this->data['id'] = $id;
            //$this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['billers'] = ($this->Owner || $this->Admin || !$this->session->userdata('biller_id')) ? $this->site->getAllCompaniesWithState('biller') : null;
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $Dsellers = $this->site->getSellerByBiller($this->data['inv']->biller_id);
            $sellers = [];
            if ($Dsellers != FALSE) {
                foreach ($Dsellers as $row => $seller) {
                    $sellers[$seller['seller_id']] = $seller['name'];
                }
            }
            $this->data['sellers'] = $sellers;
            $Daffiliates = $this->site->getAffiliateByBiller($this->data['inv']->biller_id);
            $affiliates = [];
            if ($Daffiliates != FALSE) {
                foreach ($Daffiliates as $row => $affiliate) {
                    $affiliates[$affiliate['affiliate_id']] = $affiliate['name'];
                }
            }
            $this->data['affiliates'] = $affiliates;

            $Daddresses = $this->site->getAddressByCustomer($this->data['inv']->customer_id);
            $addresses = [];

            if ($Daddresses != FALSE) {
                foreach ($Daddresses as $row => $address) {
                    $addresses[$address->id] = $address->sucursal;
                }
            }
            $this->data['addresses'] = $addresses;

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('edit_sale')));
            $meta = array('page_title' => lang('edit_sale'), 'bc' => $bc);
            $this->page_construct('sales/edit', $meta, $this->data);
        }
    }

    public function return_sale($id = null, $return_type = 1)
    {
        if (!$this->session->userdata('cash_in_hand')) {
            $this->session->set_flashdata('error', lang("no_data_pos_register"));
            admin_redirect('pos/index');
        }
        $sale = false;
        if ($this->input->get('id') || $this->input->post('id') || $id) {
            if ($this->input->get('id')) {
                $id = $this->input->get('id');
            }
            if ($this->input->post('id')) {
                $id = $this->input->post('id');
            }
            $sale = $this->sales_model->getInvoiceByID($id);
        }

        if ($sale->sale_status != 'completed') {
            $this->session->set_flashdata('error', lang("sale_status_x_competed"));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->Settings->cashier_close != 2) {
            if ($register = $this->pos_model->registerData($this->session->userdata('user_id'))) {
                $register_data = array('register_id' => $register->id, 'cash_in_hand' => $register->cash_in_hand, 'register_open_time' => $register->date);
            } else {
                $this->session->set_flashdata('error', lang('register_not_open'));
                admin_redirect('pos/open_register');
            }
        }
        if (!empty($sale->cufe)) {
            $this->sma->checkPermissions('return_sales_fe');
        } else {
            $this->sma->checkPermissions('return_sales');
        }
        $this->form_validation->set_rules('document_type_id', lang("document_type_id"), 'required');
        if ($this->input->post('amount-paid') && $this->input->post('amount-paid') > 0) {
            $this->form_validation->set_rules('payment_reference_no', lang("payment_reference_no"), 'required');
        }
        if ($this->Settings->cost_center_selection == 1) {
            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
        }
        if ($sale->return_other_concepts == YES) {
            $this->session->set_flashdata('error', lang('return_other_concepts_existing'));
            redirect($_SERVER['HTTP_REFERER']);
        }
        $customer_details = $this->site->getCompanyByID($sale->customer_id);
        $this->data['customer_details'] = $customer_details;
        $address_details = $this->site->getAddressByID($sale->address_id);
        $this->data['address_details'] = $customer_details;

        if ($this->form_validation->run() == true) {
            // $this->allowCreditNote($sale);

            $document_type_id = $this->input->post('document_type_id');
            $total_items = $this->input->post('total_items');
            $paid_by_giftcard = $this->input->post('paid_by_giftcard');
            $referenceBiller = $this->site->getReferenceBiller($sale->biller_id, $document_type_id);
            if ($referenceBiller) {
                $reference = $referenceBiller;
            } else {
                $reference = $this->site->getReference('re');
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            if ($date < $register->date) {
                $this->session->set_flashdata('error', "Fecha de devolución incorrecta, fuera del rango permitido");
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($date < $sale->date) {
                $this->session->set_flashdata('error', "Fecha de devolución incorrecta, es menor a la fecha de la venta");
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $this->site->validate_movement_date($date);
            $return_surcharge = $this->input->post('return_surcharge') ? $this->input->post('return_surcharge') : 0;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $biller_details = $this->site->getCompanyByID($sale->biller_id);
            $biller_cost_center = $this->site->getBillerCostCenter($sale->biller_id);
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center == false) {
                $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $total = 0;
            $product_tax = 0;
            $ipoconsumo = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $sale_item_id = $_POST['sale_item_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $item_net_price = $_POST['net_price'][$r]; /**/
                $real_unit_price = $_POST['real_unit_price'][$r];
                $unit_price = $_POST['unit_price'][$r];
                $item_unit_quantity = (0 - $_POST['quantity'][$r]);
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $product_seller_id = isset($_POST['product_seller_id'][$r]) ? $_POST['product_seller_id'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1; /**/
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null; /**/
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : null;
                $product_tax_rate_2 = isset($_POST['product_tax_rate_2'][$r]) ? $_POST['product_tax_rate_2'][$r] : null; /**/
                $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : null; /**/
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = isset($_POST['product_unit'][$r]) ? $_POST['product_unit'][$r] : NULL;
                $item_quantity = isset($_POST['product_base_quantity'][$r]) ? (0 - $_POST['product_base_quantity'][$r]) : NULL;
                $item_aquantity = isset($_POST['product_aqty'][$r]) ? $_POST['product_aqty'][$r] : NULL;
                $pr_item_discount = isset($_POST['product_discount_val'][$r]) ? $_POST['product_discount_val'][$r] : NULL; /**/
                $product_unit_id_selected = isset($_POST['product_unit_id_selected'][$r]) ? $_POST['product_unit_id_selected'][$r] : NULL; /**/
                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity) && $item_quantity != 0) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }
                    $pr_item_tax = $this->sma->formatDecimal(($item_unit_tax_val * $item_unit_quantity));
                    $pr_item_tax_2 = $this->sma->formatDecimal(($item_unit_tax_val_2 * $item_unit_quantity));
                    $unit = $this->site->getUnitByID($item_unit);
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $product_discount += ($pr_item_discount * $item_quantity);
                    $product_tax += ($pr_item_tax + $pr_item_tax_2);
                    $ipoconsumo += ($pr_item_tax_2);
                    $product_data = $this->site->getProductByID($item_id);
                    $unit_id = NULL;
                    if (!$unit) {
                        $unit = $this->site->getUnitByID($product_data->unit);
                    }
                    if ($unit) {
                        $unit_id = $unit->id;
                    }
                    $product = array(
                        'sale_id' => $id,
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $sale->warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $product_tax_rate,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_rate_2,
                        'tax_2' => $product_tax_rate_2,
                        'discount' => $item_discount,
                        'item_discount' => ($pr_item_discount * $item_quantity),
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'price_before_tax' => $item_net_price + ($pr_item_discount / ($item_quantity * -1)),
                        'consumption_sales' => $pr_item_tax_2,
                        'seller_id' => $product_seller_id,
                        'product_unit_id_selected' => $product_unit_id_selected > 0 ? $product_unit_id_selected : $unit_id,
                    );
                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimalNoRound(($item_net_price * $item_unit_quantity));
                    $si_return[] = array(
                        'id' => $sale_item_id,
                        'sale_id' => $id,
                        'product_id' => $item_id,
                        'option_id' => $item_option,
                        'quantity' => (0 - $item_quantity),
                        'warehouse_id' => $sale->warehouse_id,
                        'paid_by_giftcard' => $paid_by_giftcard,
                    );
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }
            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount') ? $this->input->post('order_discount') : null, ($total));
            $order_discount_not_applied = $this->input->post('order_discount_not_applied');
            if ($order_discount > 0) {
                $order_discount = $order_discount * -1;
            }
            $tip_amount = $this->input->post('tip_amount') ? $this->input->post('tip_amount') * -1 : 0;
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal($product_tax + $order_tax);
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($return_surcharge) - $order_discount) + $tip_amount);
            if ($grand_total >= 0) {
                $this->session->set_flashdata('error', "No se indicó ningún producto a devolver");
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $discount_amount = $this->input->post('discount_amount');
            $discount_ledger_id = $this->input->post('discount_ledger_id');
            $discount_description = $this->input->post('discount_description');
            $document_type = $this->site->getDocumentTypeById($document_type_id);
            $autorrete_amount = 0;
            $autorrete_perc = 0;
            if ($sale->self_withholding_amount > 0 && $this->Settings->self_withholding_percentage > 0 && $document_type->key_log == 1) {
                $autorrete_perc_op = $this->Settings->self_withholding_percentage / 100;
                $autorrete_amount = $total * $autorrete_perc_op;
                $autorrete_perc = $this->Settings->self_withholding_percentage;
            }
            $data = array(
                'date' => $date,
                'sale_id' => $id,
                'reference_no' => $reference,
                'address_id' => $sale->address_id,
                'customer_id' => $sale->customer_id,
                'customer' => $sale->customer,
                'biller_id' => $sale->biller_id,
                'biller' => $sale->biller,
                'warehouse_id' => $sale->warehouse_id,
                'note' => $note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount') ? $this->input->post('order_discount') : null,
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'surcharge' => $this->sma->formatDecimal($return_surcharge),
                'grand_total' => $grand_total,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'return_sale_ref' => $sale->reference_no,
                'sale_currency' => $sale->sale_currency,
                'sale_currency_trm' => $sale->sale_currency_trm,
                'sale_status' => 'returned',
                'pos' => $sale->pos,
                'seller_id' => $sale->seller_id,
                'payment_status' => $sale->payment_status == 'paid' ? 'due' : 'pending',
                'document_type_id' => $document_type_id,
                'shipping' => 0,
                'consumption_sales' => $ipoconsumo,
                'self_withholding_amount' => $autorrete_amount,
                'self_withholding_percentage' => $autorrete_perc,
                'tip_amount' => $tip_amount,
                'total_items' => $total_items,
                'return_order_discount_not_applied' => $order_discount_not_applied,
                'return_apply_order_discount' => $this->input->post('apply_return_order_discount'),
            );
            if ($sale->biller_id > 0 && $sale->rete_autoica_total > 0) {
                $autoica_perc_op = $sale->rete_autoica_percentage / 100;
                $autoica_amount = $total * $autoica_perc_op;
                $data['rete_autoica_percentage'] = $sale->rete_autoica_percentage;
                $data['rete_autoica_total'] = $autoica_amount;
                $data['rete_autoica_account'] = $sale->rete_autoica_account;
                $data['rete_autoica_account_counterpart'] = $sale->rete_autoica_account_counterpart;
                $data['rete_autoica_base'] = $total;
                if ($sale->rete_bomberil_percentage > 0 && $sale->rete_bomberil_total > 0) {
                    $bomberil_perc_op = $sale->rete_bomberil_percentage / 100;
                    $bomberil_amount = $autoica_amount * $bomberil_perc_op;
                    $data['rete_bomberil_percentage'] = $sale->rete_bomberil_percentage;
                    $data['rete_bomberil_total'] = $bomberil_amount;
                    $data['rete_bomberil_account'] = $sale->rete_bomberil_account;
                    $data['rete_bomberil_account_counterpart'] = $sale->rete_bomberil_account_counterpart;
                    $data['rete_bomberil_base'] = $autoica_amount;
                }
                if ($sale->rete_autoaviso_percentage > 0 && $sale->rete_autoaviso_total > 0) {
                    $autoaviso_perc_op = $sale->rete_autoaviso_percentage / 100;
                    $autoaviso_amount = $autoica_amount * $autoaviso_perc_op;
                    $data['rete_autoaviso_percentage'] = $sale->rete_autoaviso_percentage;
                    $data['rete_autoaviso_total'] = $autoaviso_amount;
                    $data['rete_autoaviso_account'] = $sale->rete_autoaviso_account;
                    $data['rete_autoaviso_account_counterpart'] = $sale->rete_autoaviso_account_counterpart;
                    $data['rete_autoaviso_base'] = $autoica_amount;
                }
            }
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }
            if ($this->Settings->cost_center_selection == 0 && $biller_cost_center) {
                $data['cost_center_id'] = $biller_cost_center->id;
            } else if ($this->Settings->cost_center_selection == 1 && $this->input->post('cost_center_id')) {
                $data['cost_center_id'] = $this->input->post('cost_center_id');
            }
            $payment = [];
            $total_retenciones = 0;
            if ($this->input->post('rete_applied') == 1) {
                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');
                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');
                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');
                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');
                $rete_bomberil_percentage = $this->input->post('rete_bomberil_tax');
                $rete_bomberil_total = $this->sma->formatDecimal($this->input->post('rete_bomberil_valor'));
                $rete_bomberil_account = $this->input->post('rete_bomberil_account');
                $rete_bomberil_base = $this->input->post('rete_bomberil_base');
                $rete_autoaviso_percentage = $this->input->post('rete_autoaviso_tax');
                $rete_autoaviso_total = $this->sma->formatDecimal($this->input->post('rete_autoaviso_valor'));
                $rete_autoaviso_account = $this->input->post('rete_autoaviso_account');
                $rete_autoaviso_base = $this->input->post('rete_autoaviso_base');
                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total + $rete_bomberil_total + $rete_autoaviso_total;
                $data['rete_fuente_percentage'] = $rete_fuente_percentage;
                $data['rete_fuente_total'] = $rete_fuente_total;
                $data['rete_fuente_account'] = $rete_fuente_account;
                $data['rete_fuente_base'] = $rete_fuente_base;
                $data['rete_iva_percentage'] = $rete_iva_percentage;
                $data['rete_iva_total'] = $rete_iva_total;
                $data['rete_iva_account'] = $rete_iva_account;
                $data['rete_iva_base'] = $rete_iva_base;
                $data['rete_ica_percentage'] = $rete_ica_percentage;
                $data['rete_ica_total'] = $rete_ica_total;
                $data['rete_ica_account'] = $rete_ica_account;
                $data['rete_ica_base'] = $rete_ica_base;
                $data['rete_other_percentage'] = $rete_other_percentage;
                $data['rete_other_total'] = $rete_other_total;
                $data['rete_other_account'] = $rete_other_account;
                $data['rete_other_base'] = $rete_other_base;
                $data['rete_bomberil_percentage'] = $rete_bomberil_percentage;
                $data['rete_bomberil_total'] = $rete_bomberil_total;
                $data['rete_bomberil_account'] = $rete_bomberil_account;
                $data['rete_bomberil_base'] = $rete_bomberil_base;
                $data['rete_autoaviso_percentage'] = $rete_autoaviso_percentage;
                $data['rete_autoaviso_total'] = $rete_autoaviso_total;
                $data['rete_autoaviso_account'] = $rete_autoaviso_account;
                $data['rete_autoaviso_base'] = $rete_autoaviso_base;
                $data['rete_fuente_id'] = $sale->rete_fuente_id;
                $data['rete_iva_id'] = $sale->rete_iva_id;
                $data['rete_ica_id'] = $sale->rete_ica_id;
                $data['rete_other_id'] = $sale->rete_other_id;
                $data['rete_bomberil_id'] = $sale->rete_bomberil_id;
                $data['rete_autoaviso_id'] = $sale->rete_autoaviso_id;
                $retencion = array(
                    'date'         => $date,
                    'amount'       => ($total_retenciones * -1),
                    'reference_no' => 'retencion',
                    'paid_by'      => 'retencion',
                    'cheque_no'    => '',
                    'cc_no'        => '',
                    'cc_holder'    => '',
                    'cc_month'     => '',
                    'cc_year'      => '',
                    'cc_type'      => '',
                    'created_by'   => $this->session->userdata('user_id'),
                    'type'         => 'received',
                    'note'         => 'Retenciones',
                );
                $payment[] = $retencion;
            }
            $sale_return_balance = ($data['grand_total'] * -1) - $total_retenciones;
            // exit(var_dump($sale_return_balance));
            $total_payments = $total_retenciones;
            $j = isset($_POST['amount-paid']) ? count($_POST['amount-paid']) : 0;
            $sale_payment_method = NULL;
            if ($j > 0 && $sale->paid > 0) {
                $pay_ref = $this->input->post('payment_reference_no');
                $address_data = $this->site->getAddressByID($sale->address_id);
                for ($i = 0; $i < $j; $i++) {
                    if ($_POST['amount-paid'][$i] > 0) {
                        $base_commision = 0;
                        $perc_commision = 0;
                        $amount_commision = 0;
                        if ($address_data) {
                            $base_commision = (($_POST['amount-paid'][$i] / $data['grand_total']) * $data['total']) * -1;
                            $perc_commision = $address_data->seller_collection_comision / 100;
                            $amount_commision = ($base_commision * $perc_commision);
                        }
                        $data_payment = array(
                            'date' => $date,
                            'document_type_id' => $pay_ref,
                            'amount' => -abs($_POST['amount-paid'][$i]),
                            'paid_by' => $_POST['paid_by'][$i],
                            'cheque_no' => $_POST['cheque_no'][$i],
                            'cc_no' => $_POST['pcc_no'][$i],
                            'cc_holder' => $_POST['pcc_holder'][$i],
                            'cc_month' => $_POST['pcc_month'][$i],
                            'cc_year' => $_POST['pcc_year'][$i],
                            'cc_type' => $_POST['pcc_type'][$i],
                            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                            'type' => 'returned',
                            'comm_base' => $base_commision,
                            'comm_amount' => $amount_commision,
                            'comm_perc' => ($perc_commision * 100),
                            'seller_id' => $data['seller_id'],
                            'mean_payment_code_fe' => $_POST['mean_payment_code_fe'][$i]
                        );
                        $data['payment_status'] = $grand_total == -abs($_POST['amount-paid'][$i]) ? 'paid' : 'partial';
                        // exit(var_dump($data['payment_status']));
                        $sale_return_balance -= $_POST['amount-paid'][$i];
                        $total_payments += $_POST['amount-paid'][$i];
                        // $this->sma->print_arrays($_POST['deposit_document_type_id']);
                        if ($_POST['paid_by'][$i] == 'deposit') {
                            if (!isset($_POST['deposit_document_type_id'][$i]) || empty($_POST['deposit_document_type_id'][$i]) || !$_POST['deposit_document_type_id'][$i]) {
                                $this->session->set_flashdata('error', lang("deposit_document_type_id"));
                                redirect($_SERVER["HTTP_REFERER"]);
                            }
                            $data_payment['deposit_document_type_id'] = $_POST['deposit_document_type_id'][$i];
                        }
                        $payment[] = $data_payment;
                        $sale_payment_method = $_POST['paid_by'][$i];
                    }
                }
            }

            if ($j > 1) {
                $sale_payment_method = 'mixed';
            }
            $data['payment_method'] = $sale_payment_method == NULL ? 'Credito' : $sale_payment_method;
            if ($this->input->post('amount-discounted') && $this->input->post('amount-discounted') > 0) {
                foreach ($discount_amount as $index => $ds_amount) {
                    $ds_ledger = $discount_ledger_id[$index];
                    $ds_description = $discount_description[$index];
                    $data_payment = array(
                        'date' => $date,
                        'amount' => $ds_amount * -1,
                        'paid_by' => 'discount',
                        'cheque_no' => NULL,
                        'cc_no' => NULL,
                        'cc_holder' => NULL,
                        'cc_month' => NULL,
                        'cc_year' => NULL,
                        'cc_type' => NULL,
                        'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                        'type' => 'returned',
                        'discount_ledger_id' => $ds_ledger,
                        'note' => $ds_description,
                    );
                    $sale_return_balance -= $ds_amount;
                    $total_payments += $total_retenciones;
                    $payment[] = $data_payment;
                }
            }
            $rc_retentions_data = [];
            if ($this->input->post('rc_retention_type')) {
                $rc_retention_type = $this->input->post('rc_retention_type');
                $rc_retention_total = $this->input->post('rc_retention_total');
                $rc_retention_base = $this->input->post('rc_retention_base');
                $rc_retention_account = $this->input->post('rc_retention_account');
                $rc_retention_percentage = $this->input->post('rc_retention_percentage');
                $rc_rete_data_payment = [];
                foreach ($rc_retention_type as $rc_rete_reference => $rc_rete_type_arr) {
                    foreach ($rc_rete_type_arr as $rcretekey => $rc_rete_type) {

                        $rc_retentions_data[$rc_rete_reference]['rete_'.$rc_rete_type.'_total'] = $rc_retention_total[$rc_rete_reference][$rcretekey];
                        $rc_retentions_data[$rc_rete_reference]['rete_'.$rc_rete_type.'_base'] = $rc_retention_base[$rc_rete_reference][$rcretekey];
                        $rc_retentions_data[$rc_rete_reference]['rete_'.$rc_rete_type.'_account'] = $rc_retention_account[$rc_rete_reference][$rcretekey];
                        $rc_retentions_data[$rc_rete_reference]['rete_'.$rc_rete_type.'_percentage'] = $rc_retention_percentage[$rc_rete_reference][$rcretekey];
                        if (isset($rc_retentions_data[$rc_rete_reference]['total_rete'])) {
                            $rc_retentions_data[$rc_rete_reference]['total_rete'] += $rc_retention_total[$rc_rete_reference][$rcretekey];
                        } else {
                            $rc_retentions_data[$rc_rete_reference]['total_rete'] = $rc_retention_total[$rc_rete_reference][$rcretekey];
                        }

                        if (isset($rc_rete_data_payment[$rc_rete_reference])) {
                            $rc_rete_data_payment[$rc_rete_reference]['amount'] += ($rc_retention_total[$rc_rete_reference][$rcretekey] * -1);
                            $rc_rete_data_payment[$rc_rete_reference]['note'] .= 'rete '.$rc_rete_type.' por '.$this->sma->formatMoney($rc_retention_total[$rc_rete_reference][$rcretekey]).', ';

                            $rc_rete_data_payment[$rc_rete_reference]['rete_'.$rc_rete_type.'_total'] = $rc_retention_total[$rc_rete_reference][$rcretekey];
                            $rc_rete_data_payment[$rc_rete_reference]['rete_'.$rc_rete_type.'_account'] = $rc_retention_account[$rc_rete_reference][$rcretekey];
                            $rc_rete_data_payment[$rc_rete_reference]['rete_'.$rc_rete_type.'_base'] = $rc_retention_base[$rc_rete_reference][$rcretekey];
                            $rc_rete_data_payment[$rc_rete_reference]['rete_'.$rc_rete_type.'_percentage'] = $rc_retention_percentage[$rc_rete_reference][$rcretekey];
                        } else {
                            $rc_rete_data_payment[$rc_rete_reference] = array(
                                'date' => $date,
                                'amount' => $rc_retention_total[$rc_rete_reference][$rcretekey] * -1,
                                'paid_by' => 'cash',
                                'reference_no' => $rc_rete_reference,
                                'cheque_no' => NULL,
                                'cc_no' => NULL,
                                'cc_holder' => NULL,
                                'cc_month' => NULL,
                                'cc_year' => NULL,
                                'cc_type' => NULL,
                                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                                'type' => 'returned',
                                'note' => 'Retención posterior a '.$rc_rete_type.' aplicada en RC '.$rc_rete_reference.' con total de '.$this->sma->formatMoney($rc_retention_total[$rc_rete_reference][$rcretekey]).', ',
                                'rete_'.$rc_rete_type.'_total' =>$rc_retention_total[$rc_rete_reference][$rcretekey],
                                'rete_'.$rc_rete_type.'_account' =>$rc_retention_account[$rc_rete_reference][$rcretekey],
                                'rete_'.$rc_rete_type.'_base' =>$rc_retention_base[$rc_rete_reference][$rcretekey],
                                'rete_'.$rc_rete_type.'_percentage' =>$rc_retention_percentage[$rc_rete_reference][$rcretekey],
                            );
                        }
                        $sale_return_balance -= $rc_retention_total[$rc_rete_reference][$rcretekey];
                        $total_payments += $rc_retention_total[$rc_rete_reference][$rcretekey];
                    }
                }
                if (count($rc_rete_data_payment) > 0) {
                    foreach ($rc_rete_data_payment as $rcdata) {
                        $payment[] = $rcdata;
                    }
                }
                if (count($rc_retentions_data) > 0) {
                    $si_return[0]['rc_retentions_data'] = $rc_retentions_data;
                }
            }
            // $this->sma->print_arrays($si_return);
            $max_to_pay = $sale->paid;
            // - ($sale->rete_fuente_total + $sale->rete_iva_total + $sale->rete_ica_total + $sale->rete_other_total + $sale->rete_bomberil_total + $sale->rete_autoaviso_total);
            if ($sale->payment_status == 'paid') {
                if ($sale_return_balance > 1) {
                    $this->session->set_flashdata('error', 'El monto del pago de la devolución no puede ser menor al total de la devolución');
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($sale_return_balance < -1) {
                    // exit(var_dump($sale_return_balance));
                    $this->session->set_flashdata('error', 'El monto del pago de la devolución no puede ser mayor al total de la devolución');
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else if ($sale->payment_status == 'partial' && ($total_payments > $max_to_pay)) {
                $this->session->set_flashdata('error', 'El monto del pago de la devolución no puede ser mayor a los pagos aplicados a la cartera de esta factura');
                redirect($_SERVER["HTTP_REFERER"]);
            }
            if ($sale_return_balance > 1) {
                $data_payment = array(
                    'date' => $date,
                    'amount' => ($sale_return_balance * -1),
                    'paid_by' => 'due',
                    'cheque_no' => NULL,
                    'cc_no' => NULL,
                    'cc_holder' => NULL,
                    'cc_month' => NULL,
                    'cc_year' => NULL,
                    'cc_type' => NULL,
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'type' => 'returned',
                );
                $data['payment_status'] = 'paid';
                $payment[] = $data_payment;
            }
            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            // $this->sma->print_arrays($data, $products, $payment);
            // Condición que valida el método de pago.
            $data["payment_method_fe"] = $sale->payment_method_fe;
            if (!$this->session->userdata('detal_post_processing')) {
                $this->session->set_userdata('detal_post_processing', 1);
            } else {
                $this->session->set_flashdata('error', 'Se interrumpió el proceso de envío por que se detectó que ya hay uno en proceso, prevención de duplicación.');
                admin_redirect("sales");
            }
            $data = $this->getReferenceInvoice($data, $sale->id);
        }

        if ($this->Settings->cashier_close != 2) {
            if (isset($sale) && isset($data) && ($this->input->post('paid_by') == "cash" && $this->input->post('amount-paid') > 0) && !$this->pos_model->getRegisterState(($this->input->post('amount-paid')))) {
                $this->session->set_flashdata('error', "El dinero en caja no es suficiente para completar la devolución.");
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }

        //Si la factura está a crédito y el valor a devolver es mayor al saldo de la factura, arrojamos error.
        if ($this->form_validation->run() == true && $sale_id = $this->sales_model->addSale($data, $products, $payment, $si_return)) {
            /**********************************************************************************************************/
            $resolution_data = $this->site->getDocumentTypeById($this->input->post('document_type_id'));
            if ($resolution_data->factura_electronica == YES) {
                $sale_data = $this->site->getSaleByID($sale_id);
                $invoice_data = [
                    'sale_id' => $sale_id,
                    'biller_id' => $sale_data->biller_id,
                    'customer_id' => $sale_data->customer_id,
                    'reference' => $sale_data->reference_no,
                    'attachment' => $sale_data->attachment
                ];

                $this->create_document_electronic($invoice_data);
            }
            /**********************************************************************************************************/
            $this->session->set_userdata('remove_resl', 1);

            if ($this->session->userdata('detal_post_processing')) {
                $this->session->unset_userdata('detal_post_processing');
                $this->session->unset_userdata('detal_post_processing_model');
            }

            $this->site->syncSalePayments($data['sale_id']);
            $this->site->updateReference('re');
            $this->session->set_flashdata('message', lang("return_sale_added"));
            if ($sale->pos) {
                if ($document_type->module == 3) {
                    admin_redirect("pos/view/".$sale_id);
                } else {
                    if ($resolution_data->factura_electronica == YES) {
                        admin_redirect("pos/fe_index");
                    }else{
                        admin_redirect("pos/sales");
                    }
                }
            } else {
                if ($resolution_data->factura_electronica == YES) {
                    $existing_sale = $this->site->getSaleByID($sale_id);
                    if ($existing_sale->fe_aceptado == ACCEPTED) {
                        admin_redirect('sales/fe_index/0/' . $sale_id);
                    } else {
                        admin_redirect("sales/fe_index");
                    }
                } else {
                    admin_redirect("sales");
                }
            }
        } else {


            $total_rc_payments = $this->sales_model->get_total_payments_for_sale($id);
            if ($total_rc_payments) {
                $total_rc_rete_amount = $total_rc_payments->fuente_total + $total_rc_payments->iva_total + $total_rc_payments->ica_total + $total_rc_payments->other_total + $total_rc_payments->autoaviso_total + $total_rc_payments->bomberil_total;
            } else {
                $total_rc_rete_amount = 0;
            }
            if ($return_type == 1 && $total_rc_payments && ($total_rc_payments->fuente_total > 0 || $total_rc_payments->iva_total > 0 || $total_rc_payments->ica_total > 0 || $total_rc_payments->other_total > 0)) {
                $this->session->set_flashdata('error', lang('cannot_return_partialy_rc_wh'));
                admin_redirect($_SERVER['HTTP_REFERER']);
            }
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($sale) {
                $this->data['inv'] = $sale;

                $this->data['invoice_resolution_data'] = $this->site->getDocumentTypeById($sale->document_type_id);
                // $this->sma->print_arrays($this->data['invoice_resolution_data']);
                if ($this->data['inv']->sale_status != 'completed') {
                    $this->session->set_flashdata('error', lang("sale_status_x_competed"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            if ($sale && $this->Settings->disable_editing) {
                if ($this->data['inv']->date <= date('Y-m-d', strtotime('-' . $this->Settings->disable_editing . ' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("sale_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $this->data['payment_ref'] = '';
            $this->data['reference'] = '';
            $this->data['return_type'] = $return_type;
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $invalid_gc_return = false;
            $allow_payment = $id ? false : true;
            $amount_discounted = 0;
            $discounts = false;
            $rc_retentions = false;
            $sale_payments = false;
            $total_quantity_available = 0;
            if ($sale) {
                $inv_items = $this->sales_model->getAllInvoiceItems($id);
                $c = rand(100000, 9999999);
                foreach ($inv_items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if ($this->Settings->gift_card_product_id == $item->product_id){
                        $si_gift_card = $this->site->get_giftcard_by_saleitem($item->id);
                        if ($this->sales_model->get_gift_card_movements($si_gift_card->id)) {
                            $invalid_gc_return = true;
                            continue;
                        }
                    }
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->sale_item_id = $item->id;
                    $row->code = $item->product_code;
                    $row->seller_id = $item->seller_id;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->returned_quantity = $item->returned_quantity;
                    if (isset($order) && $order) {
                        $row->qty = $item->quantity_to_bill;
                        $row->base_quantity = $item->quantity_to_bill;
                    } else {
                        if ($return_type == 1) {
                            $row->qty = 0;
                            $row->base_quantity = 0;
                        } else {
                            $row->qty = $item->quantity;
                            $row->base_quantity = $item->quantity - $item->returned_quantity;
                        }
                    }
                    $row->base_unit = isset($row->unit) && $row->unit ? $row->unit : $item->product_unit_id;
                    if ($item->product_unit_id_selected) {
                        $row->product_unit_id_selected = $item->product_unit_id_selected;
                    }
                    $row->unit = $item->product_unit_id;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));

                    $row->unit_price = $item->unit_price - ($row->tax_method ? $this->sma->formatDecimal($item->item_tax / $item->quantity) : 0);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->base_unit_price = $row->unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = $item->serial_no;
                    $row->option = $item->option_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id, true);
                    $combo_items = false;
                    $row->price_before_tax = $item->price_before_tax;
                    $row->consumption_sale_tax = $item->item_tax_2 / $item->quantity;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->item_tax = $item->item_tax / $item->quantity;
                    $row->net_unit_price = $item->net_unit_price;
                    $row->item_discount = $item->item_discount / $item->quantity;
                    $row->oqty = $item->quantity - $item->returned_quantity;
                    $total_quantity_available += $row->oqty;
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->get_product_units($row->id);
                    $ri = $this->Settings->item_addition ? $item->id : $c;
                    if ($row->qty == 0 && $return_type != 1) {
                        continue;
                    }
                    $pr[$item->id.($row->option)] = array('id' => $ri, 'item_id' => $item->id.($row->option), 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'order' => $item->id);
                    $c++;
                }
                // $this->sma->print_arrays($pr);
                $this->data['inv_items'] = json_encode($pr);
                $this->data['id'] = $id;
                if ($sale->payment_status == 'paid' || $sale->payment_status == 'partial') {
                    $allow_payment = true;
                }
                $sale_payments = $this->sales_model->getPaymentsForSale($id);
                $paid_by_giftcard = false;
                $pmnts = [];
                if ($sale_payments) {
                    $cnt_ds = 0;
                    foreach ($sale_payments as $payment) {
                        if ($payment->paid_by == 'gift_card') {
                            $paid_by_giftcard = true;
                        }
                        if ($payment->paid_by != 'discount' && $payment->paid_by != 'retencion') {
                            $pmamnt = $payment->amount - $payment->rc_total_retention;
                            if ($pmamnt > 0) {
                                if (!isset($pmnts[$payment->paid_by])) {
                                    $pmnts[$payment->paid_by] = $payment->amount - $payment->rc_total_retention;
                                } else {
                                    $pmnts[$payment->paid_by] += $payment->amount - $payment->rc_total_retention;
                                }
                            }
                        }
                        if ($payment->rc_total_retention > 0) {
                            if ($payment->rete_fuente_total > 0) {
                                if (isset($rc_retentions[$payment->reference_no]['fuente'])) {
                                    $rc_retentions[$payment->reference_no]['fuente']['total'] += $payment->rete_fuente_total;
                                } else {
                                    $rc_retentions[$payment->reference_no]['fuente']['total'] = $payment->rete_fuente_total;
                                    $rc_retentions[$payment->reference_no]['fuente']['base'] = $payment->rete_fuente_base;
                                    $rc_retentions[$payment->reference_no]['fuente']['account'] = $payment->rete_fuente_account;
                                    $rc_retentions[$payment->reference_no]['fuente']['percentage'] = $payment->rete_fuente_percentage;
                                }
                            }

                            if ($payment->rete_iva_total > 0) {
                                if (isset($rc_retentions[$payment->reference_no]['iva'])) {
                                    $rc_retentions[$payment->reference_no]['iva']['total'] += $payment->rete_iva_total;
                                } else {
                                    $rc_retentions[$payment->reference_no]['iva']['total'] = $payment->rete_iva_total;
                                    $rc_retentions[$payment->reference_no]['iva']['base'] = $payment->rete_iva_base;
                                    $rc_retentions[$payment->reference_no]['iva']['account'] = $payment->rete_iva_account;
                                    $rc_retentions[$payment->reference_no]['iva']['percentage'] = $payment->rete_iva_percentage;
                                }
                            }

                            if ($payment->rete_ica_total > 0) {
                                if (isset($rc_retentions[$payment->reference_no]['ica'])) {
                                    $rc_retentions[$payment->reference_no]['ica']['total'] += $payment->rete_ica_total;
                                } else {
                                    $rc_retentions[$payment->reference_no]['ica']['total'] = $payment->rete_ica_total;
                                    $rc_retentions[$payment->reference_no]['ica']['base'] = $payment->rete_ica_base;
                                    $rc_retentions[$payment->reference_no]['ica']['account'] = $payment->rete_ica_account;
                                    $rc_retentions[$payment->reference_no]['ica']['percentage'] = $payment->rete_ica_percentage;
                                }
                            }

                            if ($payment->rete_other_total > 0) {
                                if (isset($rc_retentions[$payment->reference_no][$payment->rete_other_id])) {
                                    $rc_retentions[$payment->reference_no]['other']['total'] += $payment->rete_other_total;
                                } else {
                                    $rc_retentions[$payment->reference_no]['other']['total'] = $payment->rete_other_total;
                                    $rc_retentions[$payment->reference_no]['other']['base'] = $payment->rete_other_base;
                                    $rc_retentions[$payment->reference_no]['other']['account'] = $payment->rete_other_account;
                                    $rc_retentions[$payment->reference_no]['other']['percentage'] = $payment->rete_other_percentage;
                                }
                            }

                            if ($payment->rete_bomberil_total > 0) {
                                if (isset($rc_retentions[$payment->reference_no][$payment->rete_bomberil_id])) {
                                    $rc_retentions[$payment->reference_no]['bomberil']['total'] += $payment->rete_bomberil_total;
                                } else {
                                    $rc_retentions[$payment->reference_no]['bomberil']['total'] = $payment->rete_bomberil_total;
                                    $rc_retentions[$payment->reference_no]['bomberil']['base'] = $payment->rete_bomberil_base;
                                    $rc_retentions[$payment->reference_no]['bomberil']['account'] = $payment->rete_bomberil_account;
                                    $rc_retentions[$payment->reference_no]['bomberil']['percentage'] = $payment->rete_bomberil_percentage;
                                }
                            }

                            if ($payment->rete_autoaviso_total > 0) {
                                if (isset($rc_retentions[$payment->reference_no][$payment->rete_autoaviso_id])) {
                                    $rc_retentions[$payment->reference_no]['autoaviso']['total'] += $payment->rete_autoaviso_total;
                                } else {
                                    $rc_retentions[$payment->reference_no]['autoaviso']['total'] = $payment->rete_autoaviso_total;
                                    $rc_retentions[$payment->reference_no]['autoaviso']['base'] = $payment->rete_autoaviso_base;
                                    $rc_retentions[$payment->reference_no]['autoaviso']['account'] = $payment->rete_autoaviso_account;
                                    $rc_retentions[$payment->reference_no]['autoaviso']['percentage'] = $payment->rete_autoaviso_percentage;
                                }
                            }
                        }
                        if ($payment->paid_by == 'discount') {
                            $amount_discounted += $payment->amount;
                            $discounts[$cnt_ds]['amount'] = $payment->amount;
                            $discounts[$cnt_ds]['ledger_id'] = $payment->discount_ledger_id;
                            $discounts[$cnt_ds]['description'] = $payment->note;
                            $cnt_ds++;
                        }
                    }
                }
                $this->data['sale_payments'] = $pmnts;
                $this->data['paid_by_giftcard'] = $paid_by_giftcard;
            }

            if ($total_quantity_available <= 0) {
                $this->session->set_flashdata('error', $invalid_gc_return ? sprintf(lang('gift_card_to_return_has_movements'), lang('gift_card')) : lang("sale_totally_returned"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
            $this->data['allow_payment'] = $allow_payment;
            $this->data['total_rc_rete_amount'] = $total_rc_rete_amount;
            $this->data['discounts'] = $discounts;
            $this->data['rc_retentions'] = $rc_retentions;
            $this->data['amount_discounted'] = $amount_discounted;
            $this->data['register'] = $register;
            $this->data['currencies'] = $this->site->getAllCurrencies();
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');

            $this->page_construct('sales/return_sale', ['page_title' => lang('return_sale')], $this->data);
        }
    }

    private function getReferenceInvoice($data, $saleId)
    {
        $referenceInvoice = $this->returns_model->get_sale($saleId);
        if ($referenceInvoice->technology_provider !== $this->Settings->fe_technology_provider) {
            $data['external_invoice_reference'] = $referenceInvoice->reference_no;
            $data['external_invoice_cufe'] = $referenceInvoice->cufe;
            $data['external_invoice_date'] = $referenceInvoice->date;
        }

        return $data;
    }

    private function allowCreditNote($document)
    {
        if (!empty($this->Settings->electronic_billing)) {
            if ($document->technology_provider != $this->Settings->fe_technology_provider) {
                $this->session->set_flashdata('error', lang('No es posible realizar Nota crédito a una factura con un proveedor tecnológico diferente al actual.'));
                admin_redirect('sales/fe_index');
            }
        }
    }

    public function delete($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $inv = $this->sales_model->getInvoiceByID($id);
        if ($inv->sale_status == 'returned') {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("sale_x_action")));
        }

        if ($this->sales_model->deleteSale($id)) {
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("sale_deleted")));
            }
            $this->session->set_flashdata('message', lang('sale_deleted'));
            admin_redirect('welcome');
        }
    }

    public function delete_return($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteReturn($id)) {
            if ($this->input->is_ajax_request()) {
                $this->sma->send_json(array('error' => 0, 'msg' => lang("return_sale_deleted")));
            }
            $this->session->set_flashdata('message', lang('return_sale_deleted'));
            admin_redirect('welcome');
        }
    }

    public function sale_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if ($this->input->post('form_action') != 'mark_all_printed_sale' && (!empty($_POST['val']))) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteSale($id);
                    }
                    $this->session->set_flashdata('message', lang("sales_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'combine') {

                    $html = $this->combine_pdf($_POST['val']);
                } elseif ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('grand_total'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('paid'));
                    $this->excel->getActiveSheet()->SetCellValue('G1', lang('payment_status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sale = $this->sales_model->getInvoiceByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($sale->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sale->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->biller);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sale->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $sale->grand_total);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($sale->paid));
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, lang($sale->payment_status));
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'sales_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } else if ($this->input->post('form_action') == 'post_sale') {
                    $msg = '';
                    foreach ($_POST['val'] as $id) {
                        $msg .= $this->sales_model->recontabilizarVenta($id) . "\n";
                    }
                    if ($this->session->userdata('reaccount_error')) {
                        $this->session->set_flashdata('error', $msg);
                        $this->session->unset_userdata('reaccount_error');
                    } else {
                        $this->session->set_flashdata('message', $msg);
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'sync_payments') {
                    foreach ($_POST['val'] as $id) {
                        $this->site->syncSalePayments($id);
                    }
                    $this->session->set_flashdata('message', lang("sales_payments_synchronized"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'sync_returns') {
                    $inserted = 0;
                    $updated = 0;
                    foreach ($_POST['val'] as $id) {
                        $result = $this->sales_model->syncSaleReturns($id);
                        $inserted += $result['insertadas'];
                        $updated += $result['actualizadas'];
                    }
                    $text = "Costing en  Devoluciones insertados : " . $inserted . ", Costing en  Devoluciones actualizadas : " . $updated;
                    $this->session->set_flashdata('message', $text);
                    redirect($_SERVER["HTTP_REFERER"]);
                } else if ($this->input->post('form_action') == 'mark_printed_sale') {
                    $msg = '';
                    foreach ($_POST['val'] as $id) {
                        $msg .= $this->pos_model->update_print_status($id)."\n";
                    }
                    if ($this->session->userdata('mark_as_printed_error')) {
                        $this->session->set_flashdata('error', $msg);
                        $this->session->unset_userdata('mark_as_printed_error');
                    } else {
                        $this->session->set_flashdata('message', lang('marked_as_printed_succesfully'));
                    }
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else if ($this->input->post('form_action') == 'mark_all_printed_sale') {
                $electronic = $this->input->post('electronic');
                if ($this->sales_model->mark_all_sales_as_printed($electronic)) {
                    $this->session->set_flashdata('message', lang("sales_marked_as_printed"));
                } else {
                    $this->session->set_flashdata('error', lang("sales_not_marked_as_printed"));
                }
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function order_actions()
    {
        if (!$this->Owner && !$this->Admin && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');
        if ($this->form_validation->run() == true) {
            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete');
                    foreach ($_POST['val'] as $id) {
                        // $this->sales_model->deleteSale($id);
                    }
                    $this->session->set_flashdata('message', lang("sales_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'combine') {
                    $html = $this->combine_pdf($_POST['val']);
                } elseif ($this->input->post('form_action') == 'export_excel') {
                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('sales'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('biller'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('order_status'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('grand_total'));
                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sale = $this->sales_model->getOrderSaleByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($sale->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sale->reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sale->biller);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $sale->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, lang($sale->sale_status));
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $sale->grand_total);
                        $row++;
                    }
                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'sales_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                } elseif ($this->input->post('form_action') == 'change_order_status') {
                    $new_order_sale_status = $this->input->post('new_order_sale_status');
                    foreach ($_POST['val'] as $id) {
                        $inv = $this->sales_model->getOrderSaleByID($id);
                        // exit(var_dump($inv));
                        if (
                            ($new_order_sale_status == 'enlistment' && $inv->sale_status == 'pending') ||
                            ($new_order_sale_status == 'pending' && $inv->sale_status == 'enlistment') ||
                            (
                                ($new_order_sale_status == 'sent' || $new_order_sale_status == 'delivered') &&
                                ($inv->sale_status == 'completed' || $inv->sale_status == 'sent' || $inv->sale_status == 'delivered')
                            ) ||
                            ($new_order_sale_status == 'completed' && ($inv->sale_status == 'sent' || $inv->sale_status == 'delivered'))
                        ) {

                            $this->db->update('order_sales', ['sale_status' => $new_order_sale_status], ['id' => $id]);
                        }
                    }
                    $this->session->set_flashdata('message', lang("sales_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                } elseif ($this->input->post('form_action') == 'print_batch') {


                    foreach ($_POST['val'] as $id) {
                        $inv = $this->sales_model->getOrderByID($id);
                        $data[$id]['customer'] = $this->site->getCompanyByID($inv->customer_id);
                        $data[$id]['payments'] = $this->sales_model->getPaymentsForSale($id);
                        $data[$id]['biller'] = $this->site->getCompanyByID($inv->biller_id);
                        $data[$id]['created_by'] = $this->site->getUser($inv->created_by);
                        $data[$id]['updated_by'] = $inv->updated_by ? $this->site->getUser($inv->updated_by) : null;
                        $data[$id]['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
                        $data[$id]['inv'] = $inv;
                        $data[$id]['rows'] = $this->sales_model->getAllOrderSaleItems($id);
                        $data[$id]['return_sale'] = $inv->return_id ? $this->sales_model->getOrderByID($inv->return_id) : NULL;
                        $data[$id]['return_rows'] = $inv->return_id ? $this->sales_model->getAllOrderSaleItems($inv->return_id) : NULL;
                        $data[$id]['paypal'] = $this->sales_model->getPaypalSettings();
                        $data[$id]['skrill'] = $this->sales_model->getSkrillSettings();
                        $data[$id]['settings'] = $this->Settings;
                        $data[$id]['seller'] = $this->site->getSellerById($inv->seller_id);
                        $data[$id]['sma'] = $this->sma;
                        $data[$id]['biller_logo'] = 2;
                        $data[$id]['qty_decimals'] = $this->Settings->decimals;
                        $data[$id]['value_decimals'] = $this->Settings->qty_decimals;
                        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
                        $data[$id]['tipo_regimen'] = lang($tipo_regimen->description);
                        $taxes = $this->site->getAllTaxRates();
                        $taxes_details = [];
                        foreach ($taxes as $tax) {
                            $taxes_details[$tax->id] = $tax->name;
                        }
                        $data[$id]['taxes_details'] = $taxes_details;
                        $data[$id]['show_code'] = 1;
                        $data[$id]['address'] = $this->site->getAddressByID($inv->address_id);
                        $data[$id]['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
                        $data[$id]['invoice_header'] = $this->site->getInvoiceHeader($inv->document_type_id, $inv->reference_no);
                        $view_tax = true;
                        $tax_inc = true;
                        $data[$id]['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);
                        $data[$id]['show_document_type_header'] = 1;
                        if ($inv->module_invoice_format_id != NULL) {
                            $document_type_invoice_format = $this->site->getInvoiceFormatById($inv->module_invoice_format_id);
                            if ($document_type_invoice_format) {
                                $url_format = $document_type_invoice_format->format_url;
                                $view_tax = $document_type_invoice_format->view_item_tax ? true : false;
                                $tax_inc = $document_type_invoice_format->tax_inc ? true : false;
                                $data[$id]['show_code'] = $document_type_invoice_format->product_show_code;
                                $data[$id]['show_document_type_header'] = $document_type_invoice_format->show_document_type_header;
                            }
                        }
                        $data[$id]['view_tax'] = $view_tax;
                        $data[$id]['tax_inc'] = $tax_inc;
                        $data[$id]['document_type_invoice_format'] = false;
                        $data[$id]['qty_decimals'] = $this->Settings->decimals;
                        $data[$id]['value_decimals'] = $this->Settings->qty_decimals;
                        $data[$id]['biller_logo'] = 2;
                        $data[$id]['tax_indicator'] = 0;
                        $data[$id]['product_detail_promo'] = 1;
                        $data[$id]['show_code'] = 1;
                        $data[$id]['show_award_points'] = 1;
                        $data[$id]['show_product_preferences'] = 1;
                        $data[$id]['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
                        $trmrate = 1;
                        if (!empty($inv->sale_currency) && $inv->sale_currency != $this->Settings->default_currency) {
                            $actual_currency_rate = $currency->rate;
                            $trmrate = $actual_currency_rate / $inv->sale_currency_trm;
                        }
                        $data[$id]['trmrate'] = $trmrate;
                        $data[$id]['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);
                        $data[$id]['signature_root'] = is_file("assets/uploads/signatures/".$this->Settings->digital_signature) ? base_url().'assets/uploads/signatures/'.$this->Settings->digital_signature : false;
                        $data[$id]['for_email'] = false;
                    }
                    $this->data['data'] = $data;
                    $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
                    $meta = array('page_title' => lang('view_sales_details'), 'bc' => $bc);
                    $this->load_view($this->theme . 'orders/print_batch_sale_view_3', $this->data);
                } elseif ($this->input->post('form_action') == 'complete_order_sales'){
                    $ids = [];
                    foreach ($_POST['val'] as $id) {
                        $ids[] = $id;
                    }
                    $url_ids = urlencode(json_encode($ids));
                    admin_redirect('sales/add/?orders_ids='.$url_ids);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_sale_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function deliveries()
    {
        $this->sma->checkPermissions();

        $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('deliveries')));
        $meta = array('page_title' => lang('deliveries'), 'bc' => $bc);
        $this->page_construct('sales/deliveries', $meta, $this->data);
    }

    public function getDeliveries()
    {
        $this->sma->checkPermissions('deliveries');

        $detail_link = anchor('admin/sales/view_delivery/$1', '<i class="fa fa-file-text-o"></i> ' . lang('delivery_details'), 'data-toggle="modal" data-target="#myModal"');
        $email_link = anchor('admin/sales/email_delivery/$1', '<i class="fa fa-envelope"></i> ' . lang('email_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit_delivery/$1', '<i class="fa fa-edit"></i> ' . lang('edit_delivery'), 'data-toggle="modal" data-target="#myModal"');
        $pdf_link = anchor('admin/sales/pdf_delivery/$1', '<i class="fa fa-file-pdf-o"></i> ' . lang('download_pdf'));
        $delete_link = "<a href='#' class='po' title='<b>" . lang("delete_delivery") . "</b>' data-content=\"<p>" . lang('r_u_sure') . "</p><a class='btn btn-danger po-delete' href='" . admin_url('sales/delete_delivery/$1') . "'>" . lang('i_m_sure') . "</a> <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash-o\"></i> " . lang('delete_delivery') . "</a>";
        $action = '<div class="text-center"><div class="btn-group text-left">'
            . '<button type="button" class="btn btn-default btn-xs btn-primary dropdown-toggle" data-toggle="dropdown">'
            . lang('actions') . ' <span class="caret"></span></button>
        	<ul class="dropdown-menu pull-right" role="menu">
        		<li>' . $detail_link . '</li>
        		<li>' . $edit_link . '</li>
        		<li>' . $pdf_link . '</li>
        		<li>' . $delete_link . '</li>
        	</ul>
        </div></div>';

        $this->load->library('datatables');
        //GROUP_CONCAT(CONCAT('Name: ', sale_items.product_name, ' Qty: ', sale_items.quantity ) SEPARATOR '<br>')
        $this->datatables
            ->select("deliveries.id as id, date, do_reference_no, sale_reference_no, customer, address, status, attachment")
            ->from('deliveries')
            ->join('sale_items', 'sale_items.sale_id=deliveries.sale_id', 'left')
            ->group_by('deliveries.id');
        $this->datatables->add_column("Actions", $action, "id");

        echo $this->datatables->generate();
    }

    public function pdf_delivery($id = null, $view = null, $save_bufffer = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);

        $this->data['delivery'] = $deli;
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);

        $name = lang("delivery") . "_" . str_replace('/', '_', $deli->do_reference_no) . ".pdf";
        $html = $this->load_view($this->theme . 'sales/pdf_delivery', $this->data, true);
        if (!$this->Settings->barcode_img) {
            $html = preg_replace("'\<\?xml(.*)\?\>'", '', $html);
        }
        if ($view) {
            $this->load_view($this->theme . 'sales/pdf_delivery', $this->data);
        } elseif ($save_bufffer) {
            return $this->sma->generate_pdf($html, $name, $save_bufffer);
        } else {
            $this->sma->generate_pdf($html, $name);
        }
    }

    public function view_delivery($id = null)
    {
        $this->sma->checkPermissions('deliveries');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
        $deli = $this->sales_model->getDeliveryByID($id);
        $sale = $this->sales_model->getInvoiceByID($deli->sale_id);
        if (!$sale) {
            $this->session->set_flashdata('error', lang('sale_not_found'));
            $this->sma->md();
        }
        $this->data['delivery'] = $deli;
        $this->data['biller'] = $this->site->getCompanyByID($sale->biller_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItemsWithDetails($deli->sale_id);
        $this->data['user'] = $this->site->getUser($deli->created_by);
        $this->data['page_title'] = lang("delivery_order");

        $this->load_view($this->theme . 'sales/view_delivery', $this->data);
    }

	public function add_delivery($id = null)
	{
		$this->sma->checkPermissions();
		if ($this->input->get('id')) {
			$id = $this->input->get('id');
		}
		$sale = $this->sales_model->getSaleByID($id);
		if ($sale->sale_status != 'completed') {
			$this->session->set_flashdata('error', lang('status_is_x_completed'));
			$this->sma->md();
		}
		if ($delivery = $this->sales_model->getDeliveryBySaleID($id)) {
			$this->edit_delivery($delivery->id);
		} else {
			$this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
			$this->form_validation->set_rules('customer', lang("customer"), 'required');
			$this->form_validation->set_rules('address', lang("address"), 'required');
			if ($this->form_validation->run() == true) {
				if ($this->Owner || $this->Admin) {
					$date = $this->sma->fld(trim($this->input->post('date')));
				} else {
					$date = date('Y-m-d H:i:s');
				}
				$dlDetails = array(
					'date' => $date,
					'sale_id' => $this->input->post('sale_id'),
					'sale_reference_no' => $this->input->post('sale_reference_no'),
					'customer' => $this->input->post('customer'),
					'address' => $this->input->post('address'),
					'status' => $this->input->post('status'),
					'delivered_by' => $this->input->post('delivered_by'),
                    'received_by' => $this->input->post('received_by'),
					'note' => $this->sma->clear_tags($this->input->post('note')),
					'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
					'biller_id' => $this->input->post('biller'),
                    'document_type_id' => $this->input->post('document_type_id'),
                    'vehicle' => $this->input->post('vehicle'),
                    'delivery_time_id' => $this->input->post('delivery_time_id'),
                    'containers_quantity' => $this->input->post('containers_quantity'),
                    'zone' => $this->input->post('zone'),
                    'subzone' => $this->input->post('subzone'),
					);
			} elseif ($this->input->post('add_delivery')) {
				if ($sale->shop) {
					$this->load->library('sms');
					$this->sms->delivering($sale->id, $dlDetails['do_reference_no']);
				}
				$this->session->set_flashdata('error', validation_errors());
				redirect($_SERVER["HTTP_REFERER"]);
			}
			if ($this->form_validation->run() == true && $del_id = $this->sales_model->addDelivery($dlDetails)) {
                if (!empty($sale->sale_origin_reference_no)) {
                    $this->sales_model->change_order_sale_status_from_delivery($sale->sale_origin_reference_no);
                }
                $this->session->set_flashdata('message', lang("delivery_added"));
                admin_redirect("sales/view_delivery_pdf/" . $del_id);
            } else {
                $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
                $this->data['customer'] = $this->site->getCompanyByID($sale->customer_id);
                $this->data['address'] = $this->site->getAddressByID($sale->address_id);
                $this->data['inv'] = $sale;
                $this->data['do_reference_no'] = ''; //$this->site->getReference('do');
                $this->data['modal_js'] = $this->site->modal_js();
                $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
                $this->load_view($this->theme . 'sales/add_delivery', $this->data);
            }
        }
    }

    public function view_delivery_pdf($id)
    {
        $delivery = $this->sales_model->getDeliveryByID($id);
        $this->data['delivery'] = $delivery;
        $inv = $this->sales_model->getSaleByID($delivery->sale_id);
        $this->data['inv'] = $inv;
        $customer = $this->site->getCompanyByID($inv->customer_id);
        $this->data['customer'] = $customer;
        $seller = $this->site->getCompanyByID($inv->seller_id);
        $this->data['seller'] = $seller;

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('view')));
        $meta = array('page_title' => lang('view_delivery_pdf'), 'bc' => $bc);
        $this->load_view($this->theme . 'sales/view_delivery_pdf', $this->data);
    }

    public function edit_delivery($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        $this->form_validation->set_rules('do_reference_no', lang("do_reference_no"), 'required');
        $this->form_validation->set_rules('sale_reference_no', lang("sale_reference_no"), 'required');
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('address', lang("address"), 'required');

        if ($this->form_validation->run() == true) {

            $dlDetails = array(
                'sale_id' => $this->input->post('sale_id'),
                'customer' => $this->input->post('customer'),
                'address' => $this->input->post('address'),
                'delivered_by' => $this->input->post('delivered_by'),
                'received_by' => $this->input->post('received_by'),
                'note' => $this->sma->clear_tags($this->input->post('note')),
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'vehicle' => $this->input->post('vehicle'),
                'containers_quantity' => $this->input->post('containers_quantity'),
            );

            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
                $dlDetails['date'] = $date;
            }
        } elseif ($this->input->post('edit_delivery')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateDelivery($id, $dlDetails)) {
            $this->session->set_flashdata('message', lang("delivery_updated"));
            admin_redirect("sales/deliveries");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['delivery'] = $this->sales_model->getDeliveryByID($id);
            $sale = $this->sales_model->getSaleByID($this->data['delivery']->sale_id);
            if ($sale->sale_status != 'completed') {
                $this->session->set_flashdata('error', lang('status_is_x_completed'));
                $this->sma->md();
            }

            $this->data['customer'] = $this->site->getCompanyByID($sale->customer_id);
            $this->data['address'] = $this->site->getAddressByID($sale->address_id);
            $this->data['inv'] = $sale;
            $this->data['do_reference_no'] = ''; //$this->site->getReference('do');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->load_view($this->theme . 'sales/edit_delivery', $this->data);
        }
    }

    public function delete_delivery($id = null)
    {
        $this->sma->checkPermissions(null, true);

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deleteDelivery($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("delivery_deleted")));
        }
    }

    public function delivery_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {
                    $this->sma->checkPermissions('delete_delivery');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteDelivery($id);
                    }
                    $this->session->set_flashdata('message', lang("deliveries_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('deliveries'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('date'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('do_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('sale_reference_no'));
                    $this->excel->getActiveSheet()->SetCellValue('D1', lang('customer'));
                    $this->excel->getActiveSheet()->SetCellValue('E1', lang('address'));
                    $this->excel->getActiveSheet()->SetCellValue('F1', lang('status'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $delivery = $this->sales_model->getDeliveryByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $this->sma->hrld($delivery->date));
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $delivery->do_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $delivery->sale_reference_no);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $delivery->customer);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $delivery->address);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, lang($delivery->status));
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(35);

                    $filename = 'deliveries_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_delivery_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function payments($id = null, $start_date = null, $end_date = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->data['payments'] = $this->sales_model->getInvoicePayments($id);
        $this->data['inv'] = $this->sales_model->getInvoiceByID($id);
        $this->data['start_date'] = rawurldecode($start_date ? $start_date : "");
        $this->data['end_date'] = rawurldecode($end_date ? $end_date : "");
        $this->load_view($this->theme . 'sales/payments', $this->data);
    }

    public function payment_note($id = null)
    {
        if (!($this->Admin || $this->Owner || $this->GP['sales-payments'] || $this->GP['reports-payments'])) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . (isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : site_url('welcome')) . "'; }, 10);</script>");
        }
        $payment = $this->sales_model->getPaymentByID($id);
        $inv = $this->sales_model->getInvoiceByID($payment->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['page_title'] = lang("payment_note");

        $this->load_view($this->theme . 'sales/payment_note', $this->data);
    }

    public function email_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $payment = $this->sales_model->getPaymentByID($id);
        $inv = $this->sales_model->getInvoiceByID($payment->sale_id);
        $this->data['biller'] = $this->site->getCompanyByID($inv->biller_id);
        $customer = $this->site->getCompanyByID($inv->customer_id);
        if (!$customer->email) {
            $this->sma->send_json(array('msg' => lang("update_customer_email")));
        }
        $this->data['inv'] = $inv;
        $this->data['payment'] = $payment;
        $this->data['customer'] = $customer;
        $this->data['page_title'] = lang("payment_note");
        $html = $this->load_view($this->theme . 'sales/payment_note', $this->data, TRUE);

        $html = str_replace(array('<i class="fa fa-2x">&times;</i>', 'modal-', '<p>&nbsp;</p>', '<p style="border-bottom: 1px solid #666;">&nbsp;</p>', '<p>' . lang("stamp_sign") . '</p>'), '', $html);
        $html = preg_replace("/<img[^>]+\>/i", '', $html);
        // $html = '<div style="border:1px solid #DDD; padding:10px; margin:10px 0;">'.$html.'</div>';

        $this->load->library('parser');
        $parse_data = array(
            'stylesheet' => '<link href="' . $this->data['assets'] . 'styles/helpers/bootstrap.min.css" rel="stylesheet"/>',
            'name' => $customer->company && $customer->company != '-' ? $customer->company :  $customer->name,
            'email' => $customer->email,
            'heading' => lang('payment_note') . '<hr>',
            'msg' => $html,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_con.html');
        $message = $this->parser->parse_string($msg, $parse_data);
        $subject = lang('payment_note') . ' - ' . $this->Settings->site_name;

        if ($this->sma->send_email($customer->email, $subject, $message)) {
            $this->sma->send_json(array('msg' => lang("email_sent")));
        } else {
            $this->sma->send_json(array('msg' => lang("email_failed")));
        }
    }

    public function add_payment($id = null)
    {
        $this->sma->checkPermissions('payments', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $sale = $this->sales_model->getInvoiceByID($id);
        if ($sale->payment_status == 'paid' && $sale->grand_total == $sale->paid) {
            $this->session->set_flashdata('error', lang("sale_already_paid"));
            $this->sma->md();
        }

        //$this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("reference_no"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            if ($this->input->post('paid_by') == 'deposit') {
                $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
                $customer_id = $sale->customer_id;
                if (!$this->site->check_customer_deposit($customer_id, $this->input->post('amount-paid'))) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $customer_id = null;
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date') . date(':s')));
            } else {
                $date = date('Y-m-d H:i:s');
            }

            $pagadoBB = $this->input->post('amount-paid') + (isset($total_retenciones) ? $total_retenciones : 0);
            $saldoBB = ceil($sale->grand_total - $sale->paid);

            if ($pagadoBB == $saldoBB) {
                $pagadoBB = $sale->grand_total - $sale->paid;
            } else {
                $pagadoBB = $this->input->post('amount-paid');
            }

            // exit("Pagado : ".$pagadoBB.", Balance : ".$saldoBB);

            if ($pagadoBB > ($sale->grand_total - $sale->paid)) {
                $this->session->set_flashdata('error', lang('mount_paid_greather_than_balance'));
                redirect($_SERVER["HTTP_REFERER"]);
            }


            $biller_id = $sale->biller_id;
            $document_type_id = $this->input->post('document_type_id');
            $referenceBiller = $this->site->getReferenceBiller($biller_id, $document_type_id);

            if ($referenceBiller) {
                $reference = $referenceBiller;
            } else {
                $reference = $this->site->getReference('rc');
            }

            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $reference,
                'amount' => $pagadoBB,
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('paid_by') == 'gift_card' ? $this->input->post('gift_card_no') : $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'type' => 'received',
                'document_type_id' => $document_type_id,
            );

            if ($this->Settings->cost_center_selection != 2) {

                if ($sale->cost_center_id > 0) { //SI LA COMPRA YA TIENE CENTRO DE COSTO DEFINIDO

                    $payment['cost_center_id'] = $sale->cost_center_id;
                } else { //SI NO TIENE CENTRO DE COSTO

                    if ($this->Settings->cost_center_selection == 0) { //SI EL CENTRO SE DEFINE POR SUCURSAL

                        $biller_cost_center = $this->site->getBillerCostCenter($biller_id);
                        if ($biller_cost_center) {
                            $payment['cost_center_id'] = $biller_cost_center->id;
                        } else {
                            $this->session->set_flashdata('error', lang("biller_without_cost_center"));
                            admin_redirect($_SERVER["HTTP_REFERER"]);
                        }
                    } else if ($this->Settings->cost_center_selection == 1) { //SI EL CENTRO SE DEFINE ESCOGIÉNDOLO

                        if ($this->input->post('cost_center_id')) {
                            $payment['cost_center_id'] = $this->input->post('cost_center_id');
                        } else {
                            $this->form_validation->set_rules('cost_center_id', lang("cost_center"), 'required');
                        }
                    }
                }
            }

            //IVA PROPORCIONAL

            if ($this->Settings->tax_rate_traslate && $sale->total_tax > 0) {

                $inv_items = $this->sales_model->getAllInvoiceItems($id);

                $sale_taxes = [];

                foreach ($inv_items as $row) {

                    $proporcion_pago = ($pagadoBB * 100) / $sale->grand_total;
                    // $total_iva = ($row->item_tax + $row->item_tax_2) * ($proporcion_pago / 100); //ACTIVAR CUANDO SE DESARROLLE SEGUNDO IMPUESTO PARA VENTAS
                    $total_iva = ($row->item_tax) * ($proporcion_pago / 100);

                    if (!isset($sale_taxes[$row->tax_rate_id])) {
                        $sale_taxes[$row->tax_rate_id] = $total_iva;
                    } else {
                        $sale_taxes[$row->tax_rate_id] += $total_iva;
                    }
                }

                $tax_rate_traslate_ledger_id = $this->site->getPaymentMethodParameter('tax_rate_traslate');
                $data_tax_rate_traslate = $data_taxrate_traslate = array(
                    'tax_rate_traslate_ledger_id' => $tax_rate_traslate_ledger_id->receipt_ledger_id,
                    'sale_taxes' => $sale_taxes,
                );

                // exit(var_dump($data_tax_rate_traslate));
            }

            //IVA PROPORCIONAL

            //Retenciones
            if ($this->input->post('rete_applied') == 1) {

                $rete_fuente_percentage = $this->input->post('rete_fuente_tax');
                $rete_fuente_total = $this->sma->formatDecimal($this->input->post('rete_fuente_valor'));
                $rete_fuente_account = $this->input->post('rete_fuente_account');
                $rete_fuente_base = $this->input->post('rete_fuente_base');

                $rete_iva_percentage = $this->input->post('rete_iva_tax');
                $rete_iva_total = $this->sma->formatDecimal($this->input->post('rete_iva_valor'));
                $rete_iva_account = $this->input->post('rete_iva_account');
                $rete_iva_base = $this->input->post('rete_iva_base');

                $rete_ica_percentage = $this->input->post('rete_ica_tax');
                $rete_ica_total = $this->sma->formatDecimal($this->input->post('rete_ica_valor'));
                $rete_ica_account = $this->input->post('rete_ica_account');
                $rete_ica_base = $this->input->post('rete_ica_base');

                $rete_other_percentage = $this->input->post('rete_otros_tax');
                $rete_other_total = $this->sma->formatDecimal($this->input->post('rete_otros_valor'));
                $rete_other_account = $this->input->post('rete_otros_account');
                $rete_other_base = $this->input->post('rete_otros_base');

                $total_retenciones = $rete_fuente_total + $rete_iva_total + $rete_ica_total + $rete_other_total;

                // $payment['amount'] -= $total_retenciones;

                $retencion = [];
                $retencion['rete_fuente_percentage'] = $rete_fuente_percentage;
                $retencion['rete_fuente_total'] = $rete_fuente_total;
                $retencion['rete_fuente_account'] = $rete_fuente_account;
                $retencion['rete_fuente_base'] = $rete_fuente_base;
                $retencion['rete_iva_percentage'] = $rete_iva_percentage;
                $retencion['rete_iva_total'] = $rete_iva_total;
                $retencion['rete_iva_account'] = $rete_iva_account;
                $retencion['rete_iva_base'] = $rete_iva_base;
                $retencion['rete_ica_percentage'] = $rete_ica_percentage;
                $retencion['rete_ica_total'] = $rete_ica_total;
                $retencion['rete_ica_account'] = $rete_ica_account;
                $retencion['rete_ica_base'] = $rete_ica_base;
                $retencion['rete_other_percentage'] = $rete_other_percentage;
                $retencion['rete_other_total'] = $rete_other_total;
                $retencion['rete_other_account'] = $rete_other_account;
                $retencion['rete_other_base'] = $rete_other_base;
                $retencion['total_retenciones'] = $total_retenciones;

                // exit('Total Fuente : '.$rete_fuente_total."\n Total IVA : ".$rete_iva_total."\n Total ICA : ".$rete_ica_total."\n Total Otros : ".$rete_other_total."\n Total Retención : ".$total_retenciones);
            }
            //Retenciones

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('add_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addPayment($payment, $customer_id, (isset($retencion) ? $retencion : null), (isset($data_tax_rate_traslate) ? $data_tax_rate_traslate : null))) {
            if ($sale->shop) {
                $this->load->library('sms');
                $this->sms->paymentReceived($sale->id, $payment['reference_no'], $payment['amount']);
            }
            $this->session->set_flashdata('message', lang("payment_added"));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($sale->sale_status == 'returned' && $sale->paid == $sale->grand_total) {
                $this->session->set_flashdata('warning', lang('payment_was_returned'));
                $this->sma->md();
            }
            $this->data['inv'] = $sale;

            if ($sale->rete_fuente_total != 0 || $sale->rete_iva_total != 0 || $sale->rete_ica_total != 0 || $sale->rete_other_total != 0) {
                $rete_applied = true;
            } else {
                $rete_applied = false;
            }
            $this->data['rete_applied'] = $rete_applied;
            $this->data['payment_ref'] = ''; //$this->site->getReference('pay');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');

            if ($this->Settings->cost_center_selection != 2 && $sale->cost_center_id == NULL) {
                if ($this->Settings->cost_center_selection == 1) {
                    $this->data['cost_centers'] = $this->site->getAllCostCenters();
                }
            }

            $this->load_view($this->theme . 'sales/add_payment', $this->data);
        }
    }

    public function edit_payment($id = null)
    {
        $this->sma->checkPermissions('edit', true);
        $this->load->helper('security');
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $payment = $this->sales_model->getPaymentByID($id);
        if ($payment->paid_by == 'ppp' || $payment->paid_by == 'stripe' || $payment->paid_by == 'paypal' || $payment->paid_by == 'skrill') {
            $this->session->set_flashdata('error', lang('x_edit_payment'));
            $this->sma->md();
        }
        $this->form_validation->set_rules('reference_no', lang("reference_no"), 'required');
        $this->form_validation->set_rules('amount-paid', lang("amount"), 'required');
        $this->form_validation->set_rules('paid_by', lang("paid_by"), 'required');
        $this->form_validation->set_rules('userfile', lang("attachment"), 'xss_clean');
        if ($this->form_validation->run() == true) {
            if ($this->input->post('paid_by') == 'deposit') {
                $sale = $this->sales_model->getInvoiceByID($this->input->post('sale_id'));
                $customer_id = $sale->customer_id;
                $amount = $this->input->post('amount-paid') - $payment->amount;
                if (!$this->site->check_customer_deposit($customer_id, $amount)) {
                    $this->session->set_flashdata('error', lang("amount_greater_than_deposit"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            } else {
                $customer_id = null;
            }
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = $payment->date;
            }
            $payment = array(
                'date' => $date,
                'sale_id' => $this->input->post('sale_id'),
                'reference_no' => $this->input->post('reference_no'),
                'amount' => $this->input->post('amount-paid'),
                'paid_by' => $this->input->post('paid_by'),
                'cheque_no' => $this->input->post('cheque_no'),
                'cc_no' => $this->input->post('pcc_no'),
                'cc_holder' => $this->input->post('pcc_holder'),
                'cc_month' => $this->input->post('pcc_month'),
                'cc_year' => $this->input->post('pcc_year'),
                'cc_type' => $this->input->post('pcc_type'),
                'note' => $this->input->post('note'),
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
            );

            if ($_FILES['userfile']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $payment['attachment'] = $photo;
            }

            //$this->sma->print_arrays($payment);

        } elseif ($this->input->post('edit_payment')) {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->form_validation->run() == true && $this->sales_model->updatePayment($id, $payment, $customer_id)) {
            $this->session->set_flashdata('message', lang("payment_updated"));
            admin_redirect("sales");
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['payment'] = $payment;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'sales/edit_payment', $this->data);
        }
    }

    public function delete_payment($id = null)
    {
        $this->sma->checkPermissions('delete');

        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }

        if ($this->sales_model->deletePayment($id)) {
            //echo lang("payment_deleted");
            $this->session->set_flashdata('message', lang("payment_deleted"));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function suggestions()
    {
        $term = $this->input->get('term', true);
        $enter = $this->input->get('enter', true);
        $warehouse_id = $this->input->get('warehouse_id', true);
        $customer_id = $this->input->get('customer_id', true);
        $biller_id = $this->input->get('biller_id', true);
        $address_id = $this->input->get('address_id', true);
        $module = $this->input->get('module', true);
        $aiu_management = $this->input->get('aiu_management', true);
        // Wappsi carne
        $this->pos_settings = $this->pos_model->getSetting();
        if ($this->pos_settings->balance_settings == 2) {
            $peso = $this->input->get('peso', true);
        }
        // Termina Wappsi carne
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $warehouse = $this->site->getWarehouseByID($warehouse_id);
        $customer = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows = $this->sales_model->getProductNames($sr, $warehouse_id, $biller_id, $aiu_management, $enter);
        $biller = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : false;
        // $this->sma->print_arrays($rows);
        if ($rows) {
            $r = 0;
            $pr = [];
            foreach ($rows as $row) {
                if ($module == 'pos' && $row->hide_pos == 1) {
                    continue;
                }
                if ($module == 'detal' && $row->hide_detal == 1) {
                    continue;
                }
                $label_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $unit_price_id = null);
                if ($row->tax_method == 0 && $this->Settings->ipoconsumo) {
                    $consumption_sale_tax = $row->consumption_sale_tax;
                    $label_price['new_price'] = $label_price['new_price'] + $consumption_sale_tax;
                }
                $label_price = $label_price ? "(" . $this->sma->formatMoney($label_price['new_price']) . ")" : "";
                $c = mt_rand();
                unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option = false;
                $row->quantity = $this->sma->formatQuantity($row->quantity);
                $row->item_tax_method = $row->tax_method;
                $row->op_pending_quanty = isset($row->op_pending_quanty) ? $row->op_pending_quanty : 0;
                $row->qty = $row->paste_balance_value == 1 ? 0 : 1;
                $row->base_quantity = $row->paste_balance_value == 1 ? 0 : 1;
                // Wappsi carne
                if ($this->pos_settings->balance_settings == 2) {
                    if ($peso > 0) {
                        $row->qty = $peso;
                    }
                    $row->peso = $peso;
                }
                // Termina Wappsi carne
                $row->discount = '0';
                $row->serial = '';
                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                        $option_weight = $opt->weight;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = FALSE;
                    $option_weight = FALSE;
                }
                $row->option = $option_id;

                if (isset($row->variant_selected)) {
                    $row->variant_selected = $row->variant_selected;
                    $row->option = $row->variant_selected;
                    $row->variant_weight = $row->variant_weight_selected;
                }
                if ($this->Settings->product_preferences_management == 1) {
                    $preferences = $this->sales_model->getProductPreferences($row->id);
                } else {
                    $preferences = false;
                }
                $data_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $row->sale_unit);
                $row->price = $data_price['new_price'];
                $row->discount = $data_price['new_discount'] . "%";
                if (!$this->sma->isPromo($row)) {
                    $row->promotion = 0;
                }
                // Wappsi carne
                if ($this->pos_settings->balance_settings == 2) {
                    if ($peso > 0) {
                        $row->base_quantity = $peso;
                    }
                }
                // Termina Wappsi carne
                if ($this->Settings->exclusive_discount_group_customers == 1) {
                    $row->discount = ($customer_group->percent * -1) . "%";
                }
                $row->profitability_margin = $row->profitability_margin;
                $row->base_unit = $row->unit;
                $row->base_unit_price = $row->price;
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->product_unit_id_selected = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                    foreach ($combo_items as $combo_item) {
                        $ci_unit = $this->site->getUnitByID($combo_item->unit);
                        if ($ci_unit->operation_value != 1) {
                            if ($ci_unit->operator == "*") {
                                $combo_item->qty = ($combo_item->qty * $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "/") {
                                $combo_item->qty = ($combo_item->qty / $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "+") {
                                $combo_item->qty = ($combo_item->qty + $ci_unit->operation_value);
                            } else if ($ci_unit->operator == "-") {
                                $combo_item->qty = ($combo_item->qty - $ci_unit->operation_value);
                            }
                        }
                    }
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $cnt_units_prices =  $this->site->get_all_product_units_prices($row->id) ? count($this->site->get_all_product_units_prices($row->id)) + 1 : 1;
                $row->cnt_units_prices = $cnt_units_prices;
                if ($row->tax_method == 1) {
                    $tax = $this->sma->calculateTax($row->tax_rate, $row->price, 0);
                    $row->real_unit_price = $row->price + $tax;
                    $row->unit_price = $row->price + $tax;
                } else {
                    $row->real_unit_price = $row->price;
                    $row->unit_price = $row->price;
                }
                if ($tax_rate) {
                    $row->price_before_tax = $row->real_unit_price / (($tax_rate->rate / 100) + 1);
                } else {
                    $row->price_before_tax = $row->real_unit_price;
                }

                if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                    continue;
                }
                $pr_name = $row->name . " (" . $row->code . ")";
                if ($this->Settings->show_brand_in_product_search) {
                    $pr_name .= " - " . $row->brand_name;
                }
                $ri = $c;
                $category = $this->site->getCategoryById($row->category_id);
                $subcategory = $this->site->getCategoryById($row->subcategory_id);
                $row->except_category_taxes = false;
                if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                    $row->tax_rate = $this->Settings->category_tax_exception_tax_rate_id;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->except_category_taxes = true;
                }
                $hybrid_prices = false;
                if ($this->Settings->prioridad_precios_producto == 11) { //híbrido
                    $hybrid_prices = $this->site->get_all_product_hybrid_prices($row->id);
                    $hybrid_prices = $hybrid_prices[$row->id];
                }
                if (!$this->Settings->ipoconsumo) {
                    $row->consumption_sale_tax = 0;
                }
                $label_quantity = $this->Settings->product_search_show_quantity == 1 ? ' (Cant : ' . $row->quantity . ')' : '';
                $pr[$ri] = array('id' => $ri, 'item_id' => $row->id, 'label' => ucfirst(mb_strtolower($pr_name . $label_price . $label_quantity)), 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'options' => $options, 'preferences' => $preferences, 'category' => $category, 'subcategory' => $subcategory, 'hybrid_prices' => $hybrid_prices);
                $r++;
            }
            if (count($pr) > 0) {
                $products_order = [];
                foreach ($pr as $product) {
                    $products_order[] = $product['label'];
                }
                array_multisort($products_order, SORT_ASC, $pr);
                $this->sma->send_json($pr);
            } else {
                $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
            }
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function iusuggestions()
    {
        $product_id = $this->input->get_post('product_id');
        $unit_price_id = $this->input->get_post('unit_price_id');
        $product_unit_id = $this->input->get_post('product_unit_id');
        $warehouse_id = $this->input->get_post('warehouse_id');
        $unit_quantity = $this->input->get_post('unit_quantity');

        $customer_id = $this->input->get_post('customer_id', true);
        $biller_id = $this->input->get_post('biller_id', true);
        $address_id = $this->input->get_post('address_id', true);

        $unit_price = $this->db->get_where('unit_prices', array('id' => $unit_price_id, 'id_product' => $product_id));
        $unit_price = $unit_price->row();
        $unit = $unit_price ? $this->site->getUnitByID($unit_price->unit_id) : false;
        $warehouse = $this->site->getWarehouseByID($warehouse_id);
        $rows = $this->sales_model->getProductNamesIU($product_id, $warehouse_id, $unit_price_id);

        $customer = $this->site->getCompanyByID($customer_id);
        // exit(var_dump($customer_id));
        // $this->sma->print_arrays($customer);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $biller = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : false;
        $option_id = $this->input->get_post('option_id');

        if ($rows) {
            $r = 0;
            $pr = [];
            foreach ($rows as $row) {

                if ($row->hide_pos == 1) {
                    continue;
                }

                $c = mt_rand();
                unset($row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option = false;
                // $row->quantity = 0;
                $row->item_tax_method = $row->tax_method;
                if ($unit && $unit_price->cantidad > 0) {
                    $unit_price->cantidad = $unit->operator == "*" ? 1 * $unit_price->cantidad : 1 / $unit_price->cantidad;
                }
                $row->qty = (($unit_price && isset($unit_price->cantidad) && $unit_price->cantidad > 0 ? $unit_price->cantidad : 1) * $unit_quantity);
                $row->base_quantity = ((isset($unit_price->cantidad) && $unit_price->cantidad > 0 ? $unit_price->cantidad : 1) * $unit_quantity);

                $row->discount = '0';
                $row->serial = '';
                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = FALSE;
                }
                $row->option = $option_id;
                $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                if ($this->Settings->product_preferences_management == 1) {
                    $preferences = $this->sales_model->getProductPreferences($row->id);
                } else {
                    $preferences = false;
                }
                $data_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $row->sale_unit);
                $row->price = $data_price['new_price'];
                $row->discount = $data_price['new_discount'] . "%";

                if (!$this->sma->isPromo($row)) {
                    $row->promotion = 0;
                }

                $row->real_unit_price = $row->price;
                $row->unit_price = $row->price;
                $row->unit_quantity = $row->qty;

                $row->base_unit = $row->unit;
                $row->base_unit_price = $row->price;
                $row->unit_price_id = $unit_price_id;
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->product_unit_id = (isset($unit_price->unit_id) ? (int) $unit_price->unit_id : $row->unit);
                $row->product_unit_id_selected = $product_unit_id ? $product_unit_id : $row->unit;
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                $tax_rate_2 = false;
                $category = $this->site->getCategoryById($row->category_id);
                $subcategory = $this->site->getCategoryById($row->subcategory_id);
                $row->except_category_taxes = false;
                if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                    $row->tax_rate = $this->Settings->category_tax_exception_tax_rate_id;
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->except_category_taxes = true;
                }

                if (!$this->Settings->ipoconsumo) {
                    $row->consumption_sale_tax = 0;
                }

                $pr = array(
                    'id' => sha1($c . $r), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")",
                    'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'tax_2_rate' => $tax_rate_2, 'units' => $units, 'preferences' => $preferences, 'options' => $options, 'category' => $category, 'subcategory' => $subcategory
                );
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function order_suggestions()
    {
        $term = $this->input->get('term', true);
        $warehouse_id = $this->input->get('warehouse_id', true);
        $customer_id = $this->input->get('customer_id', true);
        $biller_id = $this->input->get('biller_id', true);
        $address_id = $this->input->get('address_id', true);

        // Wappsi carne
        $this->pos_settings = $this->pos_model->getSetting();
        if ($this->pos_settings->balance_settings == 2) {
            $peso = $this->input->get('peso', true);
        }
        // Termina Wappsi carne

        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }

        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];

        $warehouse = $this->site->getWarehouseByID($warehouse_id);
        $customer = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $rows = $this->sales_model->getProductNames($sr, $warehouse_id, $biller_id);
        $biller = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : false;
        if ($rows) {
            $r = 0;
            $pr = [];
            foreach ($rows as $row) {
                if ($row->hide_pos == 1) {
                    continue;
                }
                $c = mt_rand();
                unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                $option = false;
                // $row->quantity = 0;
                $row->item_tax_method = $row->tax_method;
                $row->qty = 1;
                // Wappsi carne
                if ($this->pos_settings->balance_settings == 2) {
                    if ($peso > 0) {
                        $row->qty = $peso;
                    }
                    $row->peso = $peso;
                }
                // Termina Wappsi carne
                $row->discount = '0';
                $row->serial = '';
                $options = $this->sales_model->getProductOptions($row->id, $warehouse_id);
                if ($options) {
                    $opt = $option_id && $r == 0 ? $this->sales_model->getProductOptionByID($option_id) : $options[0];
                    if (!$option_id || $r > 0) {
                        $option_id = $opt->id;
                    }
                } else {
                    $opt = json_decode('{}');
                    $opt->price = 0;
                    $option_id = FALSE;
                }
                $row->option = $option_id;
                $pis = $this->site->getPurchasedItems($row->id, $warehouse_id, $row->option);
                
                $data_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $row->sale_unit);
                $row->price = $data_price['new_price'];
                $row->discount = $data_price['new_discount'] . "%";
                if (!$this->sma->isPromo($row)) {
                    $row->promotion = 0;
                }
                $row->base_quantity = 1;
                // Wappsi carne
                if ($this->pos_settings->balance_settings == 2) {
                    if ($peso > 0) {
                        $row->base_quantity = $peso;
                    }
                }
                // Termina Wappsi carne
                $cnt_units_prices =  $this->site->get_all_product_units_prices($row->id) ? count($this->site->get_all_product_units_prices($row->id)) + 1 : 1;
                $row->cnt_units_prices = $cnt_units_prices;
                $row->base_unit = $row->unit;
                $row->base_unit_price = $row->price;
                $row->unit = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->product_unit_id_selected = $row->sale_unit ? $row->sale_unit : $row->unit;
                $row->comment = '';
                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $warehouse_id);
                }
                $units = $this->site->get_product_units($row->id);
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                if ($row->tax_method == 1) {
                    $tax = $this->sma->calculateTax($row->tax_rate, $row->price, 0);
                    $row->real_unit_price = $row->price + $tax;
                } else {
                    $row->real_unit_price = $row->price;
                }
                $row->price_before_tax = $row->real_unit_price / (($tax_rate->rate / 100) + 1);
                if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                    continue;
                }
                $ri = $this->Settings->item_addition ? $row->id : $c;
                $hybrid_prices = false;
                if ($this->Settings->prioridad_precios_producto == 11) { //híbrido
                    $hybrid_prices = $this->site->get_all_product_hybrid_prices($row->id);
                    $hybrid_prices = $hybrid_prices[$row->id];
                }
                if ($this->Settings->product_preferences_management == 1) {
                    $preferences = $this->sales_model->getProductPreferences($row->id);
                } else {
                    $preferences = false;
                }
                $label_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $unit_price_id = null);
                if ($row->tax_method == 0 && $this->Settings->ipoconsumo) {
                    $consumption_sale_tax = $row->consumption_sale_tax;
                    $label_price['new_price'] = $label_price['new_price'] + $consumption_sale_tax;
                }
                $label_price = $label_price ? "(" . $this->sma->formatMoney($label_price['new_price']) . ")" : "";
                $label_quantity = $this->Settings->product_search_show_quantity == 1 ? ' (Cant : ' . $row->quantity . ')' : '';

                $pr_name = $row->name . " (" . $row->code . ")";
                if ($this->Settings->show_brand_in_product_search) {
                    $pr_name .= " - " . $row->brand_name;
                }
                $pr[$ri] = array(
                    'id' => $ri, 'item_id' => $row->id, 'label' => ucfirst(mb_strtolower($pr_name . $label_price . $label_quantity)), 'category' => $row->category_id,
                    'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'hybrid_prices' => $hybrid_prices, 'preferences' => $preferences
                );
                $r++;
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function gift_cards()
    {
        $this->sma->checkPermissions();

        $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
        $this->data['documents_types'] = $this->site->get_document_types_by_module(53);
        $this->data['users'] = $this->site->get_all_users();

        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('gift_cards')));
        $meta = array('page_title' => lang('gift_cards'), 'bc' => $bc);
        $this->page_construct('sales/gift_cards', $meta, $this->data);
    }

    public function getGiftCards()
    {

        $this->load->library('datatables');

        $start_date = $this->input->post('start_date') ? $this->sma->fld($this->input->post('start_date')) : NULL;
        $end_date = $this->input->post('end_date') ? $this->sma->fld($this->input->post('end_date')) : NULL;
        $expiration_date = $this->input->post('expiration_date') ? $this->sma->fsd($this->input->post('expiration_date')) : NULL;
        $customer = $this->input->post('customer');
        $status = $this->input->post('status');
        $creation_type = $this->input->post('creation_type');
        $user = $this->input->post('user');

        $this->datatables
            ->select(
                    $this->db->dbprefix('gift_cards') . ".id as id,
                    date,
                    creation_type,
                    sale_reference_no,
                    card_no,
                    value,
                    balance,
                    CONCAT(" . $this->db->dbprefix('users') . ".first_name, ' ', " . $this->db->dbprefix('users') . ".last_name) as created_by,
                    customer,
                    expiry,
                    status
                    ", false)
            ->join('users', 'users.id=gift_cards.created_by', 'left')
            ->from("gift_cards");

        if ($start_date) {
            $this->datatables->where('date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('date <=', $end_date);
        }
        if ($customer) {
            $this->datatables->where('customer_id', $customer);
        }
        if ($user) {
            $this->datatables->where('created_by', $user);
        }
        if ($status) {
            $this->datatables->where('status', $status);
        }
        if ($creation_type) {
            $this->datatables->where('creation_type', $creation_type);
        }
        if ($creation_type) {
            $this->datatables->where('creation_type', $creation_type);
        }
        if ($expiration_date) {
            $this->datatables->where('expiry', $expiration_date);
        }

        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                        <div class=\"btn-group text-left\">
                                            <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                Acciones
                                                <span class=\"caret\"></span>
                                            </button>
                                            <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                <li>
                                                    <a href='" . admin_url('sales/view_gift_card_detail/$1') . "' class='tip'  data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-eye\"></i>
                                                        " . lang("view_gift_card_detail") . "
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('sales/view_gift_card/$1') . "' class='tip'  data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-eye\"></i>
                                                        " . sprintf(lang("view_gift_card"), lang('gift_card')) . "
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('sales/topup_gift_card/$1') . "' class='tip' title='' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-dollar\"></i>
                                                        " . sprintf(lang("topup_gift_card"), lang('gift_card')) . "
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('sales/edit_gift_card/$1') . "' class='tip' title='' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-edit\"></i>
                                                        " . sprintf(lang("edit_gift_card"), lang('gift_card')) . "
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href='" . admin_url('sales/cancel_gift_card/$1') . "' class='tip' title='' data-toggle='modal' data-target='#myModal'>
                                                        <i class=\"fa fa-times\"></i>
                                                        " . sprintf(lang("cancel_gift_card"), lang('gift_card')) . "
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>", "id");
        //->unset_column('id');

        echo $this->datatables->generate();
    }

    public function view_gift_card($id = null)
    {
        $this->data['page_title'] = lang('gift_card');
        $gift_card = $this->site->getGiftCardByID($id);
        $this->data['gift_card'] = $this->site->getGiftCardByID($id);
        $this->data['customer'] = $this->site->getCompanyByID($gift_card->customer_id);
        $this->data['topups'] = $this->sales_model->getAllGCTopups($id);
        $this->load_view($this->theme . 'sales/view_gift_card', $this->data);
    }

    public function view_gift_card_detail($id = null)
    {
        $this->data['page_title'] = lang('gift_card');
        $gift_card = $this->site->getGiftCardByID($id);
        $this->data['gift_card'] = $this->site->getGiftCardByID($id);
        $this->data['customer'] = $this->site->getCompanyByID($gift_card->customer_id);
        $this->data['topups'] = $this->sales_model->getAllGCTopups($id);
        $this->data['movements'] = $this->sales_model->get_gift_card_movements($id);
        $this->load_view($this->theme . 'sales/view_gift_card_detail', $this->data);
    }

    public function topup_gift_card($card_id)
    {
        $this->sma->checkPermissions('add_gift_card', true);
        $card = $this->site->getGiftCardByID($card_id);
        $this->form_validation->set_rules('amount', lang("amount"), 'trim|integer|required');

        if ($this->form_validation->run() == true) {
            $data = array(
                'card_id' => $card_id,
                'amount' => $this->input->post('amount'),
                'biller_id' => $this->input->post('biller_id'),
                'document_type_id' => $this->input->post('document_type_id'),
                'cost_center_id' => $this->input->post('cost_center_id'),
                'paid_by' => $this->input->post('paid_by'),
                'date' => date('Y-m-d H:i:s'),
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
            );
            $card_data['balance'] = ($this->input->post('amount') + $card->balance);
            // $card_data['value'] = ($this->input->post('amount')+$card->value);
            if ($this->input->post('expiry')) {
                $card_data['expiry'] = $this->input->post('expiry');
            }
        } elseif ($this->input->post('topup')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $insert_id = $this->sales_model->topupGiftCard($data, $card_data)) {
            $this->session->set_flashdata('message', lang("topup_added"));
            admin_redirect("sales/view_gift_card_topup/".$insert_id);
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['card'] = $card;
            $this->data['page_title'] = lang("topup_gift_card");
            $this->data['billers'] = $this->site->getAllCompanies('biller');
            if ($this->Settings->cost_center_selection == 1) {
                $this->data['cost_centers'] = $this->site->getAllCostCenters();
            }
            $this->load_view($this->theme . 'sales/topup_gift_card', $this->data);
        }
    }

    public function validate_gift_card($no)
    {
        if ($gc = $this->site->getGiftCardByNO($no)) {
            if ($gc->expiry >= date('Y-m-d') && $gc->status == 1) {
                $this->sma->send_json($gc);
            } else {
                $this->sma->send_json(false);
            }
        } else {
            $this->sma->send_json(false);
        }
    }

    public function add_gift_card()
    {
        $this->sma->checkPermissions(false, true);
        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|is_unique[gift_cards.card_no]|required');
        $this->form_validation->set_rules('value', lang("value"), 'required');
        if ($this->form_validation->run() == true) {
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $use_points = $this->input->post('use_points');
            $customer = $customer_details ? $customer_details->company : null;
            $document_type_id = $this->input->post('document_type_id');
            $biller = $this->input->post('biller');
            $data = array(
                'card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'creation_type' => $use_points ? 2 : 4,
                'customer' => $customer,
                'balance' => $this->input->post('value'),
                'paid_by' => $use_points ? NULL : $this->input->post('paid_by'),
                'expiry' => $this->input->post('expiry') ? $this->input->post('expiry') : null,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'biller_id' => !$use_points ? $biller : NULL,
                'document_type_id' => !$use_points ? $document_type_id : NULL,
            );
            $sa_data = array();
            $ca_data = array();
            $ca_points = $this->input->post('ca_points');
            if ($customer_details && $ca_points > 0) {
                if ($customer_details->award_points < $ca_points) {
                    $this->session->set_flashdata('error', lang("award_points_wrong"));
                    admin_redirect("sales/gift_cards");
                }
                $ca_data = array('customer' => $this->input->post('customer'), 'points' => ($customer_details->award_points - $ca_points), 'used_points'=>$ca_points);
            }
            // $this->sma->print_arrays($data, $ca_data, $sa_data);
        } elseif ($this->input->post('add_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->addGiftCard($data, $ca_data, $sa_data)) {
            $this->session->set_flashdata('message', lang("gift_card_added"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['users'] = $this->sales_model->getStaff();
            $this->data['page_title'] = lang("new_gift_card");
            $this->load_view($this->theme . 'sales/add_gift_card', $this->data);
        }
    }

    public function edit_gift_card($id = null)
    {
        $this->sma->checkPermissions(false, true);

        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|required');
        $gc_details = $this->site->getGiftCardByID($id);
        if ($this->input->post('card_no') != $gc_details->card_no) {
            $this->form_validation->set_rules('card_no', lang("card_no"), 'is_unique[gift_cards.card_no]');
        }
        $this->form_validation->set_rules('value', lang("value"), 'required');
        //$this->form_validation->set_rules('customer', lang("customer"), 'xss_clean');

        if ($this->form_validation->run() == true) {
            $gift_card = $this->site->getGiftCardByID($id);
            $customer_details = $this->input->post('customer') ? $this->site->getCompanyByID($this->input->post('customer')) : null;
            $customer = $customer_details ? $customer_details->company : null;
            $data = array(
                'card_no' => $this->input->post('card_no'),
                'value' => $this->input->post('value'),
                'customer_id' => $this->input->post('customer') ? $this->input->post('customer') : null,
                'customer' => $customer,
                'balance' => ($this->input->post('value') - $gift_card->value) + $gift_card->balance,
                'expiry' => $this->input->post('expiry') ? $this->sma->fsd($this->input->post('expiry')) : null,
            );
        } elseif ($this->input->post('edit_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateGiftCard($id, $data)) {
            $this->session->set_flashdata('message', lang("gift_card_updated"));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['gift_card'] = $this->site->getGiftCardByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'sales/edit_gift_card', $this->data);
        }
    }

    public function sell_gift_card()
    {
        $this->sma->checkPermissions('gift_cards', true);
        $error = null;
        $gcData = $this->input->get('gcdata');
        if (empty($gcData[0])) {
            $error = lang("value") . " " . lang("is_required");
        }
        if (empty($gcData[1])) {
            $error = lang("card_no") . " " . lang("is_required");
        }

        $customer_details = (!empty($gcData[2])) ? $this->site->getCompanyByID($gcData[2]) : null;
        $customer = $customer_details ? $customer_details->company : null;
        $data = array(
            'card_no' => $gcData[0],
            'value' => $gcData[1],
            'customer_id' => (!empty($gcData[2])) ? $gcData[2] : null,
            'customer' => $customer,
            'balance' => $gcData[1],
            'expiry' => (!empty($gcData[3])) ? $this->sma->fsd($gcData[3]) : null,
            'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
        );

        if (!$error) {
            if ($this->sales_model->addGiftCard($data)) {
                $this->sma->send_json(array('result' => 'success', 'message' => lang("gift_card_added")));
            }
        } else {
            $this->sma->send_json(array('result' => 'failed', 'message' => $error));
        }
    }

    public function delete_gift_card($id = null)
    {
        $this->sma->checkPermissions();

        if ($this->sales_model->deleteGiftCard($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("gift_card_deleted")));
        }
    }

    public function gift_card_actions()
    {
        if (!$this->Owner && !$this->GP['bulk_actions']) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->form_validation->set_rules('form_action', lang("form_action"), 'required');

        if ($this->form_validation->run() == true) {

            if (!empty($_POST['val'])) {
                if ($this->input->post('form_action') == 'delete') {

                    $this->sma->checkPermissions('delete_gift_card');
                    foreach ($_POST['val'] as $id) {
                        $this->sales_model->deleteGiftCard($id);
                    }
                    $this->session->set_flashdata('message', lang("gift_cards_deleted"));
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                if ($this->input->post('form_action') == 'export_excel') {

                    $this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle(lang('gift_cards'));
                    $this->excel->getActiveSheet()->SetCellValue('A1', lang('card_no'));
                    $this->excel->getActiveSheet()->SetCellValue('B1', lang('value'));
                    $this->excel->getActiveSheet()->SetCellValue('C1', lang('customer'));

                    $row = 2;
                    foreach ($_POST['val'] as $id) {
                        $sc = $this->site->getGiftCardByID($id);
                        $this->excel->getActiveSheet()->SetCellValue('A' . $row, $sc->card_no);
                        $this->excel->getActiveSheet()->SetCellValue('B' . $row, $sc->value);
                        $this->excel->getActiveSheet()->SetCellValue('C' . $row, $sc->customer);
                        $row++;
                    }

                    $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'gift_cards_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    create_excel($this->excel, $filename);
                }
            } else {
                $this->session->set_flashdata('error', lang("no_gift_card_selected"));
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function get_award_points($id = null)
    {
        $this->sma->checkPermissions('index');

        $row = $this->site->getUser($id);
        $this->sma->send_json(array('sa_points' => $row->award_points));
    }

    public function sale_by_csv()
    {
        $this->sma->checkPermissions('csv');
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', lang("upload_file"), 'xss_clean');
        $this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
        $this->form_validation->set_rules('customer', lang("customer"), 'required');
        $this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('sale_status', lang("sale_status"), 'required');
        $this->form_validation->set_rules('payment_status', lang("payment_status"), 'required');

        if ($this->form_validation->run() == true) {

            $reference = $this->input->post('reference_no') ? $this->input->post('reference_no') : $this->site->getReference('so');
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = $this->input->post('sale_status');
            $payment_status = $this->input->post('payment_status');
            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days')) : null;
            $shipping = $this->input->post('shipping') ? $this->input->post('shipping') : 0;
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = $customer_details->company != '-'  ? $customer_details->company : $customer_details->name;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            if (isset($_FILES["userfile"])) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    admin_redirect("sales/sale_by_csv");
                }
                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 1000, ",")) !== false) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);

                $keys = array('code', 'net_unit_price', 'quantity', 'variant', 'item_tax_rate', 'discount', 'serial');
                $final = array();
                foreach ($arrResult as $key => $value) {
                    $value = $this->site->cleanRowCsvImport($value);
                    if (!$final[] = array_combine($keys, $value)) { //VALIDACIÓN NUM CAMPOS NO CORRESPONDE A COLUMNAS
                        $this->session->set_flashdata('error', sprintf(lang('invalid_csv_row'), ($key + 1)));
                        admin_redirect("sales/sale_by_csv");
                        break;
                    }
                }
                $rw = 2;
                foreach ($final as $csv_pr) {

                    if (isset($csv_pr['code']) && isset($csv_pr['net_unit_price']) && isset($csv_pr['quantity'])) {

                        if ($product_details = $this->sales_model->getProductByCode($csv_pr['code'])) {

                            if ($csv_pr['variant']) {
                                $item_option = $this->sales_model->getProductVariantByName($csv_pr['variant'], $product_details->id);
                                if (!$item_option) {
                                    $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $product_details->name . " - " . $csv_pr['variant'] . " ). " . lang("line_no") . " " . $rw);
                                    redirect($_SERVER["HTTP_REFERER"]);
                                }
                            } else {
                                $item_option = json_decode('{}');
                                $item_option->id = null;
                            }

                            $item_id = $product_details->id;
                            $item_type = $product_details->type;
                            $item_code = $product_details->code;
                            $item_name = $product_details->name;
                            $item_net_price = $this->sma->formatDecimal($csv_pr['net_unit_price']);
                            $item_quantity = $csv_pr['quantity'];
                            $item_tax_rate = $csv_pr['item_tax_rate'];
                            $item_discount = $csv_pr['discount'];
                            $item_serial = $csv_pr['serial'];

                            if (isset($item_code) && isset($item_net_price) && isset($item_quantity)) {
                                $product_details = $this->sales_model->getProductByCode($item_code);
                                $pr_discount = $this->site->calculateDiscount($item_discount, $item_net_price);
                                $item_net_price = $this->sma->formatDecimal(($item_net_price - $pr_discount));
                                $pr_item_discount = $this->sma->formatDecimal(($pr_discount * $item_quantity));
                                $product_discount += $pr_item_discount;

                                $tax = "";
                                $pr_item_tax = 0;
                                $unit_price = $item_net_price;
                                $tax_details = ((isset($item_tax_rate) && !empty($item_tax_rate)) ? $this->sales_model->getTaxRateByName($item_tax_rate) : $this->site->getTaxRateByID($product_details->tax_rate));
                                if ($tax_details) {
                                    $ctax = $this->site->calculateTax($product_details, $tax_details, $unit_price);
                                    $item_tax = $ctax['amount'];
                                    $tax = $ctax['tax'];
                                    if (!$product_details || (!empty($product_details) && $product_details->tax_method != 1)) {
                                        $item_net_price = $unit_price - $item_tax;
                                    }
                                    $pr_item_tax = $this->sma->formatDecimal($item_tax * $item_quantity);
                                    if ($this->Settings->indian_gst && $gst_data = $this->gst->calculteIndianGST($pr_item_tax, ($biller_details->state == $customer_details->state), $tax_details)) {
                                        $total_cgst += $gst_data['cgst'];
                                        $total_sgst += $gst_data['sgst'];
                                        $total_igst += $gst_data['igst'];
                                    }
                                }

                                $product_tax += $pr_item_tax;
                                $subtotal = $this->sma->formatDecimal((($item_net_price * $item_quantity) + $pr_item_tax));
                                $unit = $this->site->getUnitByID($product_details->unit);

                                $product = array(
                                    'product_id' => $product_details->id,
                                    'product_code' => $item_code,
                                    'product_name' => $item_name,
                                    'product_type' => $item_type,
                                    'option_id' => $item_option->id,
                                    'net_unit_price' => $item_net_price,
                                    'quantity' => $item_quantity,
                                    'product_unit_id' => $product_details->unit,
                                    'product_unit_code' => $unit->code,
                                    'unit_quantity' => $item_quantity,
                                    'warehouse_id' => $warehouse_id,
                                    'item_tax' => $pr_item_tax,
                                    'tax_rate_id' => $tax_details ? $tax_details->id : null,
                                    'tax' => $tax,
                                    'discount' => $item_discount,
                                    'item_discount' => $pr_item_discount,
                                    'subtotal' => $subtotal,
                                    'serial_no' => $item_serial,
                                    'unit_price' => $this->sma->formatDecimal(($item_net_price + $item_tax)),
                                    'real_unit_price' => $this->sma->formatDecimal(($item_net_price + $item_tax + $pr_discount)),
                                );

                                $products[] = ($product + $gst_data);
                                $total += $this->sma->formatDecimal(($item_net_price * $item_quantity));
                            }
                        } else {
                            $this->session->set_flashdata('error', lang("pr_not_found") . " ( " . $csv_pr['code'] . " ). " . lang("line_no") . " " . $rw);
                            redirect($_SERVER["HTTP_REFERER"]);
                        }
                        $rw++;
                    }
                }
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount));
            $data = array(
                'date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'paid' => 0,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($payment_status == 'paid') {

                $payment = array(
                    'date' => $date,
                    'reference_no' => $this->site->getReference('pay'),
                    'amount' => $grand_total,
                    'paid_by' => 'cash',
                    'cheque_no' => '',
                    'cc_no' => '',
                    'cc_holder' => '',
                    'cc_month' => '',
                    'cc_year' => '',
                    'cc_type' => '',
                    'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                    'note' => lang('auto_added_for_sale_by_csv') . ' (' . lang('sale_reference_no') . ' ' . $reference . ')',
                    'type' => 'received',
                );
            } else {
                $payment = array();
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }

            // $this->sma->print_arrays($data, $products, $payment);
        }

        if ($this->form_validation->run() == true && $this->sales_model->addSale($data, $products, $payment)) {
            $this->session->set_userdata('remove_slls', 1);
            $this->session->set_flashdata('message', lang("sale_added"));
            admin_redirect("sales");
        } else {

            $data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['slnumber'] = $this->site->getReference('so');

            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_sale_by_csv')));
            $meta = array('page_title' => lang('add_sale_by_csv'), 'bc' => $bc);
            $this->page_construct('sales/sale_by_csv', $meta, $this->data);
        }
    }

    public function update_status($id)
    {
        $this->form_validation->set_rules('status', lang("sale_status"), 'required');

        if ($this->form_validation->run() == true) {
            $status = $this->input->post('status');
            $note = $this->sma->clear_tags($this->input->post('note'));
        } elseif ($this->input->post('update')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateStatus($id, $status, $note)) {
            if ($status == 'completed') {
                $msg = $this->sales_model->recontabilizarVenta($id);

                $sale = $this->site->getSaleByID($id);
                $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);

                if ($resolution_data->factura_electronica == 1) {
                    $invoice_data = [
                        'sale_id' => $sale->id,
                        'biller_id' => $sale->biller_id,
                        'customer_id' => $sale->customer_id,
                        'reference' => $sale->reference_no,
                        'attachment' => $sale->attachment
                    ];

                    $this->create_document_electronic($invoice_data);
                }
            } else {
                $msg = '';
            }
            if ($this->session->userdata('reaccount_error')) {
                $this->session->set_flashdata('error', lang('status_updated') . ", " . $msg);
                $this->session->unset_userdata('reaccount_error');
            } else {
                $this->session->set_flashdata('message', lang('status_updated') . ", " . $msg);
            }
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'sales');
        } else {

            $this->data['inv'] = $this->sales_model->getInvoiceByID($id);
            $this->data['returned'] = FALSE;
            if ($this->data['inv']->sale_status == 'returned' || $this->data['inv']->return_id) {
                $this->data['returned'] = TRUE;
            }
            $this->data['completed'] = FALSE;
            if ($this->data['inv']->sale_status == 'completed') {
                $this->data['completed'] = TRUE;
            }
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'sales/update_status', $this->data);
        }
    }

    public function packaging($id)
    {

        $sale = $this->sales_model->getInvoiceByID($id);
        $this->data['returned'] = FALSE;
        if ($sale->sale_status == 'returned' || $sale->return_id) {
            $this->data['returned'] = TRUE;
        }
        $this->data['warehouse'] = $this->site->getWarehouseByID($sale->warehouse_id);
        $items = $this->sales_model->getAllInvoiceItems($sale->id);
        foreach ($items as $item) {
            $packaging[] = array(
                'name' => $item->product_code . ' - ' . $item->product_name,
                'quantity' => $item->quantity . ' ' . $item->product_unit_code,
                'rack' => $this->sales_model->getItemRack($item->product_id, $sale->warehouse_id),
            );
        }
        $this->data['packaging'] = $packaging;
        $this->data['sale'] = $sale;

        $this->load_view($this->theme . 'sales/packaging', $this->data);
    }

    public function preciosUnidad()
    {
        /*
            POSICIONES JSON RESPUESTA
            0. Precio obtenido
            1. operation_value de la unidad
            2. Nuevo descuento por grupo de cliente
            3. Impuesto por decreto territorial
            4. Datos del impuesto parametrizado para el decreto territorial
            5. Precio por promoción
        */
        $id = $this->input->get('id', true);
        $new_qty = $this->input->get('new_qty', true);
        $prioridad = $this->input->get('prioridad', true);
        $customer_id = $this->input->get('customer', true);
        $biller_id = $this->input->get('biller', true);
        $address_id = $this->input->get('address_id', true);
        $biller = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : false;
        $customer = $this->site->getCompanyByID($customer_id);
        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);
        $unit_price_id = $this->input->get('unit_price_id', true);
        $row = $this->site->getProductByID($id);
        $category = $this->site->getCategoryById($row->category_id);
        $subcategory = $this->site->getCategoryById($row->subcategory_id);
        if ($prioridad != 5 && $prioridad != 7 && $prioridad != 10) { //Política de precios no va por unidades
            $data_price = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $unit_price_id);
            $valor_p = $data_price['new_price'];
            $new_discount = $data_price['new_discount'] . "%";
            $valor[0] = $valor_p;
            $valor[2] = $new_discount;
            if ($this->Settings->exclusive_discount_group_customers == 1) {
                $valor[2] = $customer_group->percent . "%";
            }
            if ($prioridad == 11) {
                $unit_data = $this->site->getUnitByID($unit_price_id);
                $valor[1] = $unit_data->operation_value > 0 ? $unit_data->operation_value : 1;
            }
            if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                if ($row->tax_method == 0) {
                    $valor[0] = $this->sma->remove_tax_from_amount($row->tax_rate, $valor[0]);
                }
            }
            if ($customer->tax_exempt_customer == 1 && $this->Settings->enable_customer_tax_exemption == 1) {
                if ($row->tax_method == 0) {
                    $valor[0] = $this->sma->remove_tax_from_amount($row->tax_rate, $valor[0]);
                }
                $valor[3] = $this->Settings->customer_territorial_decree_tax_rate_id ? $this->Settings->customer_territorial_decree_tax_rate_id : $this->site->get_tax_exempt();
                $valor[4] = $this->site->getTaxRateByID($valor[3]);
            }
            if ($this->sma->isPromo($row)) {
                $pbp = $this->site->get_item_price($row, $customer, $customer_group, $biller, $address_id, $unit_price_id, true);
                $valor[5] = $pbp['new_price'];
            }
            $this->sma->send_json($valor);
        } else {
            $rows = $this->sales_model->getPrecioUnidad($id, $new_qty, $unit_price_id);
            // exit(var_dump($unit_price_id));
            // $this->sma->print_arrays($rows);
            if ($rows && $rows->valor_unitario > 0) {
                $valor_p = $rows->valor_unitario;
                if ($customer_group) {
                    $valor_p = $valor_p + (($valor_p * $customer_group->percent) / 100);
                }
                $valor[0] = $valor_p;
                if ($this->Settings->precios_por_unidad_presentacion == 2) {
                    $valor[1] = $rows->cantidad > 0 ? $rows->cantidad : 1;
                }

                if ($this->sma->validate_except_category_taxes($category, $subcategory)) {
                    if ($row->tax_method == 0) {
                        $valor[0] = $this->sma->remove_tax_from_amount($row->tax_rate, $valor[0]);
                    }
                }
                if ($customer->tax_exempt_customer == 1 && $this->Settings->enable_customer_tax_exemption == 1) {
                    if ($row->tax_method == 0) {
                        $valor[0] = $this->sma->remove_tax_from_amount($row->tax_rate, $valor[0]);
                    }
                    $valor[3] = $this->Settings->customer_territorial_decree_tax_rate_id;
                    $valor[4] = $this->site->getTaxRateByID($valor[3]);
                }
                if ($this->sma->isPromo($row)) {
                    $row->price = $row->promo_price;
                    $valor[5] = $valor[0];
                    $valor[0] = $row->price;
                }
                $this->sma->send_json($valor);
            } else {
                if ($customer_group) {
                    $row->price = $row->price + (($row->price * $customer_group->percent) / 100);
                }
                $valor[0] = $row->price;
                if ($this->sma->isPromo($row)) {
                    $row->price = $row->promo_price;
                    $valor[0] = $row->price;
                }
                $this->sma->send_json($valor);
            }
        }
    }

    public function opcionesWithHolding($type)
    {
        $q = $this->sales_model->getWithHolding($type, 'S');
        if ($q !== FALSE) {
            $options = "<option value=''>Seleccione...</option>";
            foreach ($q as $row => $data) {
                $options .= "<option value='" . $data['id'] . "' data-account='" . $data['account_id'] . "' data-apply='" . $data['apply'] . "' data-minbase='" . $data['min_base'] . "' data-percentage='" . $data['percentage'] . "' data-manual_base='" . $data['manual_base'] . "'>" . $data['description'] . " (" . $data['apply'] . ")</option>";
            }
            echo $options;
        } else {
            echo "<option value=''>Sin resultados</option>";
        }
    }

    public function validate_biller_resolution($biller_id, $document_type_id)
    {
        $parametro_porc_aviso = ($this->Settings->resolucion_porc_aviso / 100);
        $parametro_dias_aviso = $this->Settings->resolucion_dias_aviso;

        $biller = $this->db->where('id', $document_type_id)->get('documents_types');
        $mensaje = "";
        $disable = false;
        if ($biller->num_rows() > 0) {
            $biller = $biller->row();
            if ($biller->save_resolution_in_sale == 0) {
                echo FALSE;
                exit();
            }
            $res_actual = $biller->sales_consecutive;
            $res_fin = $biller->fin_resolucion;

            if (($biller->save_resolution_in_sale == 1) && ($res_actual == 0 || $res_fin == 0 || $res_fin == "")) {
                $mensaje = '<span class="fa fa-user"></span> ' . lang('invalid_biller_data') . "<a href='" . admin_url('billers') . "' target='_blank'>Click</a>";
                $disable = true;
            }

            $porc_aviso = $res_fin * $parametro_porc_aviso;
            $porc_aviso = floor($porc_aviso);
            $res_restante = $res_fin - $res_actual;

            if (($biller->save_resolution_in_sale == 1) && $res_restante <= $porc_aviso) {

                if ($res_restante > 0) {
                    $mensaje .= sprintf(lang('biller_resolution_advice'), $res_restante);
                } else if ($res_restante == 0) {
                    $mensaje .= sprintf(lang('biller_resolution_advice'), 'último');
                } else {
                    $mensaje .= sprintf(lang('biller_resolution_finished'), abs($res_restante));
                    $disable = TRUE;
                }
            }

            if (($biller->save_resolution_in_sale == 1) && date('Y-m-d') > $biller->vencimiento_resolucion) { //Ya se cumplió la fecha de vencimiento
                $mensaje.='Ya se venció la fecha de la resolución.';
                $disable = TRUE;
            }

            $fecha1 = new DateTime(date('Y-m-d'));
            $fecha2 = new DateTime($biller->vencimiento_resolucion);
            $diferencia = $fecha1->diff($fecha2);

            $diasDiff = $diferencia->format('%d');
            $mesesDiff = $diferencia->format('%m');
            $añosDiff = $diferencia->format('%Y');

            if (($biller->save_resolution_in_sale == 1) && $añosDiff <= 0 && $mesesDiff <= 0 && $diasDiff <= $parametro_dias_aviso) { //Se avisa por fecha de vencimiento
                $mensaje .= sprintf(lang('biller_resolution_expired'), ($diasDiff + 1));
            }
            if ($mensaje != "") {
                $response = array(
                        'mensaje' => $mensaje,
                        'disable' => $disable
                    );
                exit(json_encode($response));
            }
        }
        exit(FALSE);
    }

    public function getSellers()
    {
        if ($this->input->get('biller_id')) {
            $biller_id = $this->input->get('biller_id');
            $withvatno = $this->input->get('withvatno');
            $sellers = $this->site->getSellerByBiller($biller_id);
            $options = '';
            if ($sellers != FALSE && count($sellers) > 1) {
                $options = "<option value=''>" . lang('select') . "</option>";
            }
            if ($sellers != FALSE) {
                foreach ($sellers as $row => $seller) {
                    $options .= "<option value='" . $seller['companies_id'] . "'>" . ($withvatno ? $seller['vat_no'] . " - " : "") . ucwords(mb_strtolower($seller['name'])) . "</option>";
                }
            } else if ($sellers == FALSE) {
                $options = "<option value=''>" . lang('no_data_available') . "</option>";
            }
            echo $options;
        } else {
            echo FALSE;
        }
    }

    public function getCustomerAddresses()
    {
        if ($this->input->get('customer_id')) {
            $customer_id = $this->input->get('customer_id');
            $addresses = $this->site->getAddressByCustomer($customer_id);
            if ($addresses != FALSE) {
                $options = "<option value=''>" . lang('select') . "</option>";
                $selected = 'selected="selected"';
                if (count($addresses) > 1) {
                    $selected = '';
                }
                foreach ($addresses as $row => $address) {
                    $options .= "<option value='" . $address->id . "' data-sellerdefault='" . $address->customer_address_seller_id_assigned . "' " . $selected . ">" . ucwords(mb_strtolower($address->sucursal)) . "</option>";
                }
            } else {
                $options = "<option value=''>Sin registros</option>";
            }
            echo $options;
        } else {
            echo FALSE;
        }
    }

    public function add_order()
    {
        $order_sale_id = $this->input->get('order_sale_id');
        $guest_register_id = $this->input->get('guest_register_id');
		$this->sma->checkPermissions();
		$sale_id = $this->input->get('sale_id') ? $this->input->get('sale_id') : NULL;
		$this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
		$this->form_validation->set_rules('customer', lang("customer"), 'required');
		$this->form_validation->set_rules('biller', lang("biller"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        $this->form_validation->set_rules('seller_id', lang("seller"), 'required');
        if ($this->Settings->management_order_sale_delivery_time == 1) {
            $this->form_validation->set_rules('delivery_time', lang("delivery_time"), 'required');
        }
        if ($this->form_validation->run() == true) {
            if ($this->Owner || $this->Admin) {
                $date = $this->sma->fld(trim($this->input->post('date')));
            } else {
                $date = date('Y-m-d H:i:s');
            }
            $warehouse_id = $this->input->post('warehouse');
            $customer_id = $this->input->post('customer');
            $biller_id = $this->input->post('biller');
            $total_items = $this->input->post('total_items');
            $sale_status = 'pending';
            $payment_status = 'pending';
            $payment_term = $this->input->post('payment_term');
            $due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
            $shipping = $this->input->post('shipping');
            $customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->name) && $customer_details->name != '-' ? $customer_details->name : $customer_details->company;
            $biller_details = $this->site->getCompanyByID($biller_id);
            $biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
            $note = $this->sma->clear_tags($this->input->post('note'));
            $staff_note = $this->sma->clear_tags($this->input->post('staff_note'));
            $quote_id = $this->input->post('quote_id') ? $this->input->post('quote_id') : null;
            $document_type_id = $this->input->post('document_type_id');
            $total = 0;
            $product_tax = 0;
            $product_discount = 0;
            $digital = FALSE;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            $total_to_bill = 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $item_net_price = $_POST['net_price'][$r];
                $real_unit_price = $_POST['real_unit_price'][$r];
                $unit_price = $_POST['unit_price'][$r];
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1;
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null;
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : 0;
                $product_tax_rate_2 = isset($_POST['product_tax_rate_2'][$r]) ? $_POST['product_tax_rate_2'][$r] : 0;
                $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : 0;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_unit = isset($_POST['product_unit'][$r]) ? $_POST['product_unit'][$r] : null;
                $item_quantity = isset($_POST['product_base_quantity'][$r]) ? $_POST['product_base_quantity'][$r] : null;
                $item_aquantity = isset($_POST['product_aqty'][$r]) ? $_POST['product_aqty'][$r] : null;
                $pr_item_discount = isset($_POST['product_discount_val'][$r]) ? $_POST['product_discount_val'][$r] : null;
                $item_quantity_to_bill = isset($_POST['quantity_bill'][$r]) ? $_POST['quantity_bill'][$r] : null;
                $product_seller_id = isset($_POST['product_seller_id'][$r]) ? $_POST['product_seller_id'][$r] : null; /**/
                $product_preferences = isset($_POST['product_preferences_text'][$r]) ? $this->sma->preferences_selection($_POST['product_preferences_text'][$r]) : NULL;
                $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : NULL;
                $product_current_gold_price = isset($_POST['product_current_gold_price'][$r]) ? $_POST['product_current_gold_price'][$r] : null; /**/
                $total_to_bill += $item_quantity_to_bill;
                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }
                    $pr_item_tax = $this->sma->formatDecimal(($item_unit_tax_val * $item_unit_quantity));
                    $pr_item_tax_2 = $this->sma->formatDecimal(($item_unit_tax_val_2 * $item_unit_quantity));
                    $unit = $this->site->getUnitByID($item_unit);
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $product_discount += $pr_item_discount;
                    $product_tax += ($pr_item_tax + $pr_item_tax_2);
                    $price_before_tax = $item_net_price + ($pr_item_discount / $item_quantity);
                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val + $item_unit_tax_val_2),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $product_tax_rate,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_rate_2,
                        'tax_2' => $product_tax_rate_2,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $serialModal_serial ? $serialModal_serial : $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'quantity_to_bill' => $item_quantity_to_bill,
                        'price_before_tax' => $price_before_tax > 0 ? $price_before_tax : 0,
                        'preferences' => $product_preferences,
                        'seller_id' => $product_seller_id,
                        'current_gold_price' => $product_current_gold_price,
                    );
                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity));
                }
            }
            // $this->sma->print_arrays($product_preferences);
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }
            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount));
            $resolucion = '';
            if ($this->input->post('address_id')) {
                $address_id = $this->input->post('address_id');
            } else {
                $address_id = $this->site->getCustomerBranch($customer_id);
                if (!$address_id) {
                    $this->form_validation->set_rules('address_id', lang("customer_branch"), 'required');
                }
            }
            $payment_status = $this->input->post('payment_status');
            $data = array(
                'date' => $date,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'due_date' => $due_date,
                'paid' => 0,
                'seller_id' => $this->input->post('seller_id'),
                'address_id' => $address_id,
                'created_by' => $this->session->userdata('register_cash_movements_with_another_user') ? $this->session->userdata('register_cash_movements_with_another_user') : $this->session->userdata('user_id'),
                'hash' => hash('sha256', microtime() . mt_rand()),
                'resolucion' => $resolucion,
                'payment_method' =>  $this->input->post('paid_by'),
                'payment_term' => $this->input->post('paid_by') == 'Credito' ? $payment_term : NULL,
                'document_type_id' => $document_type_id,
                'delivery_time_id' => $this->input->post('delivery_time'),
                'delivery_day' => $this->input->post('day_delivery_time'),
			);

            $biller_data = $this->site->getAllCompaniesWithState('biller', $biller_id);
            if ($biller_data->min_sale_amount > 0 && $grand_total < $biller_data->min_sale_amount) {
                $this->session->set_flashdata('error', sprintf(lang('min_sale_amount_error'), lang('order'), $this->sma->formatMoney($biller_data->min_sale_amount)));
                redirect($_SERVER["HTTP_REFERER"]);
            }
			if ($this->Settings->indian_gst) {
				$data['cgst'] = $total_cgst;
				$data['sgst'] = $total_sgst;
				$data['igst'] = $total_igst;
			}
			if ($_FILES['document']['size'] > 0) {
				$this->load->library('upload');
				$config['upload_path'] = $this->digital_upload_path;
				$config['allowed_types'] = $this->digital_file_types;
				$config['max_size'] = $this->allowed_file_size;
				$config['overwrite'] = false;
				$config['encrypt_name'] = true;
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('document')) {
					$error = $this->upload->display_errors();
					$this->session->set_flashdata('error', $error);
					redirect($_SERVER["HTTP_REFERER"]);
				}
				$photo = $this->upload->file_name;
				$data['attachment'] = $photo;
			}
			// $this->sma->print_arrays($data, $products);
		}

        if ($this->form_validation->run() == true && $order_id = $this->sales_model->addOrderSale($data, $products)) {
            if (!empty($this->Settings->email_new_order_sale_notification)) {
                $this->send_order_sale_email($order_id);
            }
            if ($total_to_bill > 0) {
                $this->session->set_userdata('remove_oslls', 1);
                $redirect_order_to = $this->input->post('redirect_order_to');
                if ($redirect_order_to == 'pos') {
                    admin_redirect("pos/index/" . $order_id . "/1/0/1");
                } else if ($redirect_order_to == 'wholesale_pos') {
                    admin_redirect("pos/add_wholesale/" . $order_id . "/0/1");
                } else {
                    admin_redirect("sales/add/" . $order_id . "/1");
                }
            } else {
                $this->session->set_userdata('remove_oslls', 1);
                $this->session->set_flashdata('message', lang("sale_added"));
                admin_redirect("sales/orderView/$order_id");
            }
        } else {
            // exit(var_dump($order_sale_id));
            if ($order_sale_id) {
                $this->data['order_sale'] = $this->sales_model->getOrderByID($order_sale_id);
                $items = $this->site->getAllOrderItems($order_sale_id);
                $c = rand(100000, 9999999);
                foreach ($items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }
                    $row->name = $this->sma->clean_json_text($row->name);
                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }
                    $row->id = $item->product_id;
                    $row->code = $item->product_code;
                    $row->name = $this->sma->clean_json_text($item->product_name);
                    $row->type = $item->product_type;
                    // if ($order) {
                    //     $row->qty = $item->quantity_to_bill;
                    //     $row->base_quantity = $item->quantity_to_bill;
                    // // } else {
                        $row->qty = $item->quantity;
                        $row->base_quantity = $item->quantity;
                    // //     $row->base_quantity = $item->quantity;
                    // // }
                    $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->unit = $item->product_unit_id;
                    $r_item_discount = $item->item_discount / $item->quantity;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));

                    $row->unit_price = $item->unit_price - ($row->tax_method ? $this->sma->formatDecimal($item->item_tax / $item->quantity) : 0);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->prev_unit_price = ($row->tax_method == 1 ? $item->net_unit_price : $item->unit_price) + $r_item_discount;
                    $row->base_unit_price = $row->unit_price;

                    $row->diferent_tax_alert = false;
                    $real_tax_rate = null;
                    if ($item->tax_rate_id != $row->tax_rate) {
                        $row->diferent_tax_alert = true;
                        $row->real_tax_rate = $row->tax_rate;
                        $real_tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    }
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $row->product_unit_id_selected = $item->product_unit_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                    $combo_items = false;
                    $row->price_before_tax = $item->price_before_tax;
                    $row->consumption_sale_tax = $item->tax_rate_2_id ? $item->tax_rate_2_id : 0;
                    if ($this->Settings->product_variant_per_serial == 1) {
                        $row->serialModal_serial = $item->serial_no;
                    }
                    if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                        continue;
                    }
                    if ($row->qty <= 0 && $this->Settings->display_all_products == 0 && $row->ignore_hide_parameters != 1) {
                        continue;
                    }
                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }
                    $units = $this->site->get_product_units($row->id);
                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $ri = $this->Settings->item_addition ? $row->id : $c;
                    if ($row->qty == 0) {
                        continue;
                    }
                    $pr[$ri] = array(
                        'id' => $ri,
                        'item_id' => $row->id,
                        'label' => $row->name . " (" . $row->code . ")",
                        'row' => $row,
                        'combo_items' => $combo_items,
                        'tax_rate' => $tax_rate,
                        'real_tax_rate' => $real_tax_rate,
                        'units' => $units,
                        'options' => $options,
                        'preferences_text' => $item->preferences,
                    );
                    $c++;
                }
                $this->data['order_items'] = json_encode($pr);
            }

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['reference'] = $this->Settings->order_sale_prefix . "-";
            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            $this->data['oslnumber'] = '';
            $this->data['payment_ref'] = '';
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('add_order')));
            $meta = array('page_title' => lang('add_order'), 'bc' => $bc);
            $this->page_construct('orders/add', $meta, $this->data);
        }
    }

    public function orders($warehouse_id = null)
    {
        $this->sma->checkPermissions();

        $this->data['advancedFiltersContainer'] = FALSE;
        $this->data['salesOrigins'] = $this->site->getSalesOrigins();
        $this->data['saleStatus'] = json_decode(json_encode($this->saleStatus));
        $this->data['documentsTypes'] = $this->site->getDocumentsTypeByBillerId($this->input->post('biller_id'), [8]);
        $this->data['documents_types'] = $this->site->get_document_types_by_module(8);
        $this->data['sellers'] = $this->site->getAllCompaniesWithState('seller');
        $this->data['users'] = $this->site->get_all_users();
        $this->data['countries'] = $this->site->getCountries();
        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouse_id'] = $warehouse_id;
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;

            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        } else {
            $this->data['warehouse_id'] = $this->session->userdata('warehouse_id');
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;

            $biller_id = $this->session->userdata('biller_id');
            $this->data['billers'] = $biller_id ? $this->site->getAllCompaniesWithState('biller', $biller_id) : $this->site->getAllCompaniesWithState('biller');
        }

        // $this->data['warehouses'] = $this->site->getAllWarehouses(1);

        if ($this->input->post('document_type') || $this->input->post('warehouse') || $this->input->post('delivery_day')
            || $this->input->post('time_1') || $this->input->post('time_2') || $this->input->post('user') ||
            $this->input->post('country') || $this->input->post('country')) {
                if ($this->input->post('country')) {
                    $this->data['states'] = $this->companies_model->getStatesByCountry($this->input->post('countryCode'));

                    if ($this->input->post('state')) {
                        $this->data['cities'] = $this->companies_model->getCitiesByState($this->input->post('stateCode'));

                        if ($this->input->post('city')) {
                            $this->data['zones'] = $this->companies_model->getZonesByCity($this->input->post('cityCode'));

                            if ($this->input->post('zone')) {
                                $this->data['subzones'] = $this->companies_model->getSubZonesByZones($this->input->post('zoneCode'));
                            }
                        }
                    }
                }

            $this->data['advancedFiltersContainer'] = TRUE;
        }

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->page_construct('orders/index', ['page_title' => $this->lang->line('orders')], $this->data);
    }

    public function getOrders($warehouse_id = null)
    {
        $data = [
            'option_filter' => $this->input->post('option_filter') == 'false' ? FALSE : $this->input->post('option_filter'),
            'get_json' => $this->input->post('get_json') ? $this->input->post('get_json') : FALSE,
        ];

        if ($data['get_json']) {
            $data['option_filter'] = 'pending';
            $pending = $this->get_order_sales_result($data);
            $data['option_filter'] = 'completed';
            $completed = $this->get_order_sales_result($data);
            $data['option_filter'] = 'cancelled';
            $cancelled = $this->get_order_sales_result($data);
            $data['option_filter'] = false;
            $none = $this->get_order_sales_result($data);
            echo json_encode(['none'=>$none, 'pending'=>$pending, 'completed'=>$completed, 'cancelled'=>$cancelled]);
        } else {
            echo $this->get_order_sales_result($data);
        }
	}

    public function get_order_sales_result($data)
    {
        $detal_document_type_id = $this->input->post('detal_reference_no') ? $this->input->post('detal_reference_no') : NULL;
        $delivery_day = $this->input->post('delivery_day') ? $this->sma->fld($this->input->post('delivery_day')) : NULL;
        $option_filter = $this->input->post('option_filter') == 'false' ? false : $this->input->post('option_filter');
        $order_sale_origin = $this->input->post('order_sale_origin') ? $this->input->post('order_sale_origin') : NULL;
        $start_date = ($this->input->post('start_date')) ? $this->sma->fld($this->input->post('start_date')) : NULL;
        $end_date = ($this->input->post('end_date')) ? $this->sma->fld($this->input->post('end_date')) : NULL;
        $sale_status = $this->input->post('sale_status') ? $this->input->post('sale_status') : NULL;
        $warehouse_id = $this->input->post('warehouse') ? $this->input->post('warehouse') : NULL;
        $biller_id = $this->input->post('biller_id') ? $this->input->post('biller_id') : NULL;
        $get_json = $this->input->post('get_json') ? $this->input->post('get_json') : NULL;
        $seller_id = $this->input->post('seller') ? $this->input->post('seller') : NULL;
        $time_1 = $this->input->post('time_1') ? $this->input->post('time_1') : NULL;
        $time_2 = $this->input->post('time_2') ? $this->input->post('time_2') : NULL;
        $client = $this->input->post('client') ? $this->input->post('client') : NULL;
        $user_id = $this->input->post('user') ? $this->input->post('user') : NULL;
        $country = $this->input->post('country') ?? '';
        $subzone = $this->input->post('subzone') ?? '';
        $state = $this->input->post('state') ?? '';
        $city = $this->input->post('city') ?? '';
        $zone = $this->input->post('zone') ?? '';
        $option_filter = $data['option_filter'];
        $get_json = $data['get_json'];

        $detail_link = anchor('admin/sales/orderView/$1', '<i class="fa fa-file-text-o"></i> ' . lang('sale_details'));
        $email_link = anchor('admin/sales/order_sale_email/$1', '<i class="fa fa-file-text-o"></i> ' . lang('send_email'), 'data-toggle="modal" data-target="#myModal"');
        $delete_remaining_quantity = anchor('admin/sales/order_delete_remaining_quantity/$1', '<i class="fa fa-trash"></i> ' . lang('delete_remaining_quantity'), 'data-toggle="modal" data-target="#myModal"');
        $change_status = anchor('admin/sales/change_order_sale_status/$1', '<i class="fa fa-sync"></i> ' . lang('change_order_status'), 'data-toggle="modal" data-target="#myModal"');
        $edit_link = anchor('admin/sales/edit_order/$1', '<i class="fa fa-edit"></i> ' . lang('edit_order'), 'class="sledit"');
        $duplicate_link = anchor('admin/sales/add_order?order_sale_id=$1', '<i class="fa fa-plus-circle"></i> ' . lang('duplicate_order_sale'));
        if ($this->Settings->order_sales_conversion == 1) {
            $convert_link = "
                        <li>
                             <a href='#' class='po' title='' data-placement=\"left\" data-content=\"
                                        <p> <?= lang('where_to_convert') ?> </p>
                                        <a class='btn btn-primary' href='".admin_url('sales/complete_convert_order/3/')."$1"."'> ".lang('convert_to_detal_sale')." </a>
                                        <a class='btn btn-primary' href='".admin_url('sales/complete_convert_order/1/')."$1".'/1/1'."'> ".lang('convert_to_pos_sale')." </a>
                                        <button class='btn po-close'> ".lang('cancel')." </button>\" rel='popover' style=\"width: 100%;\">
                                        <i class='fa fa-download'></i>
                                ".lang('convert_complete')."
                            </a>
                        </li>";
        } else if ($this->Settings->order_sales_conversion == 2) {
            $convert_link = anchor('admin/sales/complete_convert_order/1/$1/1/1', '<i class="fa fa-download"></i> ' . lang('convert_to_pos_sale'), 'class="sledit"');
        } else if ($this->Settings->order_sales_conversion == 3) {
            $convert_link = anchor('admin/sales/complete_convert_order/3/$1', '<i class="fa fa-download"></i> ' . lang('convert_to_detal_sale'), 'class="sledit"');
        }

        $action = '<div class="text-center"><div class="btn-group text-left">
                <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                <i class="fas fa-ellipsis-v fa-lg"></i></button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li>' . $detail_link . '</li>
                    <li>' . $edit_link . '</li>
                    <li>' . $convert_link . '</li>
                    <li>' . $email_link . '</li>
                    <li>' . $delete_remaining_quantity . '</li>
                    <li>' . $duplicate_link . '</li>
                    <li>' . $change_status . '</li>' .
                    "<li>
                        <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure_order_sale') . "</p>
                            <a class='btn btn-danger po-delete' href='" . admin_url('sales/complete_os_wo_sale/') . "/$1'>" . lang('i_m_sure') . "</a>
                            <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash\"></i>" . lang('complete_os_wo_sale') . "
                        </a>
                    </li>" . 
                    "<li>
                        <a href='#' class='tip po' data-content=\"<p>" . lang('r_u_sure_order_sale') . "</p>
                            <a class='btn btn-danger po-delete' href='" . admin_url('sales/cancel_order_sale/') . "/$1'>" . lang('i_m_sure') . "</a>
                            <button class='btn po-close'>" . lang('no') . "</button>\"  rel='popover'><i class=\"fa fa-trash\"></i>" . lang('cancel_order_sale') . "
                        </a>
                    </li>" . '
                </ul>
            </div>
        </div>';

        $this->load->library('datatables');

        $this->datatables->select("
                    {$this->db->dbprefix('order_sales')}.id as id,
                    DATE_FORMAT({$this->db->dbprefix('order_sales')}.date, '%Y-%m-%d %T') as date,
                    CONCAT({$this->db->dbprefix('order_sales')}.reference_no, IF({$this->db->dbprefix('order_sales')}.sale_status = 'pending', ' <i class=\"fa fa-bell\" style=\"color:#428bca;\"></i>', '')) as reference_no,
                    destination_reference_no,
                    biller.name as biller_name,
                    CONCAT(customer.name, ' (', customer.company, ')'),
                    seller.name,
                    sale_status,
                    " . ($this->Settings->management_order_sale_delivery_time == 1 ? "CONCAT({$this->db->dbprefix('delivery_time')}.day, '_', {$this->db->dbprefix('delivery_time')}.time_1, '_', {$this->db->dbprefix('delivery_time')}.time_2) as delivery_timea," : "") . "
                    grand_total,
                    order_sale_origin,
                    {$this->db->dbprefix('order_sales')}.attachment,
                    return_id");
        $this->datatables->from('order_sales');
        $this->datatables->join('delivery_time', 'delivery_time.id = order_sales.delivery_time_id', 'left');
        $this->datatables->join('companies as seller', 'seller.id = order_sales.seller_id', 'left');
        $this->datatables->join('companies as customer', 'customer.id = order_sales.customer_id', 'left');
        $this->datatables->join('companies as biller', 'biller.id = order_sales.biller_id', 'left');

        $skip_cancelled = true;
        if ($option_filter) {
            if ($option_filter == 'pending') {
                $this->datatables->where('(order_sales.sale_status = "pending" OR order_sales.sale_status = "partial" OR order_sales.sale_status = "enlistment")');
            } else if ($option_filter == 'completed') {
                $this->datatables->where('(order_sales.sale_status = "completed" OR order_sales.sale_status = "sent" OR order_sales.sale_status = "delivered")');
            } else if ($option_filter == 'cancelled') {
                $skip_cancelled = false;
                $this->datatables->where('(order_sales.sale_status = "cancelled")');
            } else {
                $skip_cancelled = false;
                if ($sale_status) {
                    $this->datatables->where('order_sales.sale_status =', $sale_status);
                }
            }
        } else {
            $skip_cancelled = false;
            if ($sale_status) {
                $this->datatables->where('order_sales.sale_status', $sale_status);
            }
        }

        if ($warehouse_id) {
            $this->datatables->where('order_sales.warehouse_id', $warehouse_id);
        }
        if ($order_sale_origin) {
            $this->datatables->where('order_sales.order_sale_origin', $order_sale_origin);
        }
        if ($biller_id) {
            $this->datatables->where('order_sales.biller_id', $biller_id);
        }
        if ($seller_id) {
            $this->datatables->where('order_sales.seller_id', $seller_id);
        }
        if ($user_id) {
            $this->datatables->where('order_sales.created_by', $user_id);
        }
        if ($detal_document_type_id) {
            $this->datatables->where('order_sales.document_type_id', $detal_document_type_id);
        }
        if ($start_date) {
            $this->datatables->where('order_sales.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('order_sales.date <=', $end_date);
        }
        if ($client) {
            $this->datatables->where('order_sales.customer_id', $client);
        }
        if ($delivery_day) {
            $this->datatables->where('order_sales.delivery_day', $delivery_day);
        }
        if ($time_1 || $time_2) {
            if ($time_1) {
                $this->datatables->where('delivery_time.time_1', $time_1);
            }
            if ($time_2) {
                $this->datatables->where('delivery_time.time_2', $time_2);
            }
        }
        if ($skip_cancelled) {
            $this->datatables->where('order_sales.sale_status !=', 'cancelled');
        }
        if (!empty($country)) {
            $this->datatables->where('customer.country', $country);
        }
        if (!empty($state)) {
            $this->datatables->where('customer.state', $state);
        }
        if (!empty($city)) {
            $this->datatables->where('customer.city', $city);
        }
        if (!empty($zone)) {
            $this->datatables->where('customer.location', $zone);
        }
        if (!empty($subzone)) {
            $this->datatables->where('customer.subzone', $subzone);
        }

        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->session->userdata('view_right')) {
            if ($this->session->userdata('company_id') || $this->session->userdata('seller_id')) {
                $this->datatables->where('(order_sales.created_by = '.$this->session->userdata('user_id').' OR seller_id = '.($this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id')).')');
            } else {
                $this->datatables->where('order_sales.created_by', $this->session->userdata('user_id'));
            }
        } elseif ($this->Customer) {
            $this->datatables->where('customer_id', $this->session->userdata('user_id'));
        }

        $this->datatables->add_column("Actions", $action, "id");

        if ($get_json) {
            return $this->datatables->get_result_num_rows();
        } else {
            return $this->datatables->generate();
        }
    }

	public function edit_order($id = null, $converting = 'false')
	{
		$this->sma->checkPermissions();
		if ($this->input->get('id')) {
			$id = $this->input->get('id');
		}
		$inv = $this->sales_model->getOrderSaleByID($id);
        if ($inv->sale_status == 'cancelled') {
            $this->session->set_flashdata('error', lang('order_sale_cancelled'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        if ($inv->sale_status == 'completed') {
            $this->session->set_flashdata('error', lang('order_sale_completed'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
		if (!$this->session->userdata('edit_right')) {
			$this->sma->view_rights($inv->created_by);
		}
		$this->form_validation->set_message('is_natural_no_zero', lang("no_zero_required"));
		$this->form_validation->set_rules('customer', lang("customer"), 'required');
		$this->form_validation->set_rules('biller', lang("biller"), 'required');
		$this->form_validation->set_rules('seller_id', lang("seller"), 'required');
		$this->form_validation->set_rules('address_id', lang("customer_branch"), 'required');

        if ($this->form_validation->run() == true) {

			$reference = $inv->reference_no;
			if ($this->Owner || $this->Admin) {
				$date = $this->sma->fld(trim($this->input->post('date')));
			} else {
				$date = $inv->date;
			}
			$warehouse_id = $this->input->post('warehouse');
			$customer_id = $this->input->post('customer');
			$biller_id = $this->input->post('biller');
			$total_items = $this->input->post('total_items');
			$sale_status = 'pending';
			$payment_status = 'pending';
			$payment_term = 0;
			$due_date = $payment_term ? date('Y-m-d', strtotime('+' . $payment_term . ' days', strtotime($date))) : null;
			$shipping = $this->input->post('shipping');;
			$customer_details = $this->site->getCompanyByID($customer_id);
            $customer = !empty($customer_details->name) && $customer_details->name != '-' ? $customer_details->name : $customer_details->company;
			$biller_details = $this->site->getCompanyByID($biller_id);
			$biller = !empty($biller_details->company) && $biller_details->company != '-' ? $biller_details->company : $biller_details->name;
			$note = $this->sma->clear_tags($this->input->post('note'));
			$staff_note = $this->sma->clear_tags($this->input->post('staff_note'));

            $total = 0;
            $total_to_bill = 0;
            $product_tax = 0;
            $product_discount = 0;
            $gst_data = [];
            $total_cgst = $total_sgst = $total_igst = 0;
            $i = isset($_POST['product_code']) ? sizeof($_POST['product_code']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $item_id = $_POST['product_id'][$r];
                $item_type = $_POST['product_type'][$r];
                $item_code = $_POST['product_code'][$r];
                $item_name = $_POST['product_name'][$r];
                $item_option = isset($_POST['product_option'][$r]) && $_POST['product_option'][$r] != 'false' && $_POST['product_option'][$r] != 'null' ? $_POST['product_option'][$r] : null;
                $item_net_price = $_POST['net_price'][$r];
                $real_unit_price = $_POST['real_unit_price'][$r];
                $unit_price = $_POST['unit_price'][$r];
                $item_unit_quantity = $_POST['quantity'][$r];
                $item_serial = isset($_POST['serial'][$r]) ? $_POST['serial'][$r] : '';
                $item_tax_rate = isset($_POST['product_tax'][$r]) ? $_POST['product_tax'][$r] : 1;
                $product_tax_rate = isset($_POST['product_tax_rate'][$r]) ? $_POST['product_tax_rate'][$r] : 1;
                $item_unit_tax_val = isset($_POST['unit_product_tax'][$r]) ? $_POST['unit_product_tax'][$r] : null;
                $item_discount = isset($_POST['product_discount'][$r]) ? $_POST['product_discount'][$r] : null;
                $item_tax_rate_2 = isset($_POST['product_tax_2'][$r]) ? $_POST['product_tax_2'][$r] : 0;
                $product_tax_rate_2 = isset($_POST['product_tax_rate_2'][$r]) ? $_POST['product_tax_rate_2'][$r] : 0;
                $item_unit_tax_val_2 = isset($_POST['unit_product_tax_2'][$r]) ? $_POST['unit_product_tax_2'][$r] : 0;
                $item_unit = $_POST['product_unit'][$r];
                $item_quantity = $_POST['product_base_quantity'][$r];
                $item_aquantity = $_POST['product_aqty'][$r];
                $pr_item_discount = $_POST['product_discount_val'][$r];
                $item_quantity_to_bill = $_POST['quantity_bill'][$r];
                $sale_item_id = $_POST['sale_item_id'][$r];
                $total_to_bill += $item_quantity_to_bill;
                $product_preferences = isset($_POST['product_preferences_text'][$r]) ? $this->sma->preferences_selection($_POST['product_preferences_text'][$r]) : NULL;
                $serialModal_serial = isset($_POST['serialModal_serial'][$r]) ? $_POST['serialModal_serial'][$r] : NULL;
                $ipoconsumo_total = 0;
                if (isset($item_code) && isset($real_unit_price) && isset($unit_price) && isset($item_quantity)) {
                    $product_details = $item_type != 'manual' ? $this->sales_model->getProductByCode($item_code) : null;
                    if ($item_type == 'digital') {
                        $digital = TRUE;
                    }
                    $pr_item_tax = $this->sma->formatDecimal((($item_unit_tax_val) * $item_unit_quantity));
                    $pr_item_tax_2 = $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_unit_quantity));
                    $ipoconsumo_total += $this->sma->formatDecimal((($item_unit_tax_val_2) * $item_unit_quantity));
                    $unit = $this->site->getUnitByID($item_unit);
                    $subtotal = (($item_net_price * $item_unit_quantity) + $pr_item_tax + $pr_item_tax_2);
                    $product_discount += $pr_item_discount;
                    $product_tax += ($pr_item_tax + $pr_item_tax_2);
                    $product = array(
                        'product_id' => $item_id,
                        'product_code' => $item_code,
                        'product_name' => $item_name,
                        'product_type' => $item_type,
                        'option_id' => $item_option,
                        'net_unit_price' => $item_net_price,
                        'unit_price' => $this->sma->formatDecimal($item_net_price + $item_unit_tax_val),
                        'quantity' => $item_quantity,
                        'product_unit_id' => $unit ? $unit->id : NULL,
                        'product_unit_code' => $unit ? $unit->code : NULL,
                        'unit_quantity' => $item_unit_quantity,
                        'warehouse_id' => $warehouse_id,
                        'item_tax' => $pr_item_tax,
                        'tax_rate_id' => $item_tax_rate,
                        'tax' => $product_tax_rate,
                        'item_tax_2' => $pr_item_tax_2,
                        'tax_rate_2_id' => $item_tax_rate_2,
                        'tax_2' => $product_tax_rate_2,
                        'discount' => $item_discount,
                        'item_discount' => $pr_item_discount,
                        'subtotal' => $this->sma->formatDecimal($subtotal),
                        'serial_no' => $serialModal_serial ? $serialModal_serial : $item_serial,
                        'real_unit_price' => $real_unit_price,
                        'quantity_to_bill' => $item_quantity_to_bill,
                        'price_before_tax' => $item_net_price + ($pr_item_discount / $item_quantity),
                        'sale_item_id' => $sale_item_id,
                    );
                    if ($product_preferences) {
                        $product['preferences'] = $product_preferences;
                    }
                    $products[] = ($product + $gst_data);
                    $total += $this->sma->formatDecimal(($item_net_price * $item_unit_quantity));
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("order_items"), 'required');
            } else {
                krsort($products);
            }

            $order_discount = $this->site->calculateDiscount($this->input->post('order_discount'), ($total + $product_tax));
            $total_discount = $this->sma->formatDecimal(($order_discount + $product_discount));
            $order_tax = $this->site->calculateOrderTax($this->input->post('order_tax'), ($total + $product_tax - $order_discount));
            $total_tax = $this->sma->formatDecimal(($product_tax + $order_tax));
            $grand_total = $this->sma->formatDecimal(($total + $total_tax + $this->sma->formatDecimal($shipping) - $order_discount));
            $data = array(
                'date' => $date,
                'reference_no' => $reference,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'biller_id' => $biller_id,
                'biller' => $biller,
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'staff_note' => $staff_note,
                'total' => $total,
                'product_discount' => $product_discount,
                'order_discount_id' => $this->input->post('order_discount'),
                'order_discount' => $order_discount,
                'total_discount' => $total_discount,
                'product_tax' => $product_tax,
                'order_tax_id' => $this->input->post('order_tax'),
                'order_tax' => $order_tax,
                'total_tax' => $total_tax,
                'shipping' => $this->sma->formatDecimal($shipping),
                'grand_total' => $grand_total,
                'total_items' => $total_items,
                'sale_status' => $sale_status,
                'payment_status' => $payment_status,
                'payment_term' => $payment_term,
                'due_date' => $due_date,
                'updated_by' => $this->session->userdata('user_id'),
                'updated_at' => date('Y-m-d H:i:s'),
                'seller_id' => $this->input->post('seller_id'),
                'address_id' => $this->input->post('address_id'),
                'delivery_time_id' => $this->input->post('delivery_time'),
                'delivery_day' => $this->input->post('day_delivery_time'),
            );
            if ($this->Settings->indian_gst) {
                $data['cgst'] = $total_cgst;
                $data['sgst'] = $total_sgst;
                $data['igst'] = $total_igst;
            }

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }
                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
        }

        if ($this->form_validation->run() == true && $this->sales_model->updateOrderSale($id, $data, $products)) {
            $this->session->set_userdata('remove_oslls', 1);
            if ($total_to_bill > 0) {
                $this->session->set_userdata('remove_oslls', 1);
                $redirect_order_to = $this->input->post('redirect_order_to');
                if ($redirect_order_to == 'pos') {
                    admin_redirect("pos/index/" . $id . "/1/0/1");
                } else if ($redirect_order_to == 'wholesale_pos') {
                    admin_redirect("pos/add_wholesale/" . $id . "/0/1");
                } else {
                    admin_redirect("sales/add/" . $id . "/1");
                }
            } else {
                $this->session->set_flashdata('message', lang("sale_updated"));
                admin_redirect('sales/orders');
            }
        } else {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));

            $this->data['inv'] = $this->sales_model->getOrderSaleByID($id);

            if ($this->Settings->disable_editing) {
                if ($this->data['inv']->date <= date('Y-m-d', strtotime('-' . $this->Settings->disable_editing . ' days'))) {
                    $this->session->set_flashdata('error', sprintf(lang("sale_x_edited_older_than_x_days"), $this->Settings->disable_editing));
                    redirect($_SERVER["HTTP_REFERER"]);
                }
            }
            $inv_items = $this->sales_model->getAllOrderSaleItems($id);
            // krsort($inv_items);
            $c = rand(100000, 9999999);
            foreach ($inv_items as $item) {
                $row = $this->site->getProductByID($item->product_id);
                if (!$row) {
                    $row = json_decode('{}');
                    $row->tax_method = 0;
                    $row->quantity = 0;
                } else {
                    unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                }
                $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);
                if ($pis) {
                    $row->quantity = 0;
                    foreach ($pis as $pi) {
                        $row->quantity += $pi->quantity_balance;
                    }
                }
                $row->id = $item->product_id;
                $row->sale_item_id = $item->id;
                $row->code = $item->product_code;
                $row->name = $item->product_name;
                $row->type = $item->product_type;
                $row->base_quantity = $item->quantity;
                $row->base_unit = !empty($row->unit) ? $row->unit : $item->product_unit_id;
                $row->base_unit_price = !empty($row->price) ? $row->price : $item->unit_price;
                $row->unit = $item->product_unit_id;
                $row->qty = $item->quantity;
                $row->delivered_qty = $item->quantity_delivered;
                $row->pending_qty = $item->quantity - $item->quantity_delivered;
                $row->bill_qty = $item->quantity_to_bill > 0 ? $item->quantity_to_bill : $row->pending_qty;
                $row->quantity += $item->quantity;
                $row->discount = $item->discount ? $item->discount : '0';
                $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));
                $row->unit_price = $item->unit_price - ($row->tax_method ? $this->sma->formatDecimal($item->item_tax / $item->quantity) : 0);
                $row->real_unit_price = $item->real_unit_price;
                $row->tax_rate = $item->tax_rate_id;
                $row->product_unit_id_selected = $item->product_unit_id;
                $row->serial = $item->serial_no;
                $row->picking_quantity = $item->picking_quantity;
                $row->seller_id = $item->seller_id;
                $row->packing_quantity = $item->packing_quantity;
                if ($this->Settings->product_variant_per_serial == 1) {
                    $row->serialModal_serial = $item->serial_no;
                }
                $row->option = $item->option_id;
                $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);

                $combo_items = false;
                if ($row->type == 'combo') {
                    $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    $te = $combo_items;
                    foreach ($combo_items as $combo_item) {
                        $combo_item->quantity = $combo_item->qty * $item->quantity;
                    }
                }
                $units = !empty($row->base_unit) ? $this->site->getUnitsByBUID($row->base_unit) : NULL;
                $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                if ($tax_rate) {
                    $row->price_before_tax = $row->real_unit_price / (($tax_rate->rate / 100) + 1);
                } else {
                    $row->price_before_tax = $row->real_unit_price;
                }
                if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                    continue;
                }

                if ($row->quantity <= 0 && $this->Settings->display_all_products == 0 && $row->ignore_hide_parameters != 1) {
                    continue;
                }
                $ri = $this->Settings->item_addition ? $row->id : $c;

                if ($this->Settings->product_preferences_management == 1) {
                    $preferences = $this->sales_model->getProductPreferences($row->id);
                } else {
                    $preferences = false;
                }

                if ($this->Settings->handle_jewerly_products) {
                    if ($row->based_on_gram_value != '0') {
                        $row->current_price_gram = $item->current_gold_price;
                    }
                }

                $pr[$ri] = array(
                    'id' => $ri, 'order' => round(microtime(true) * 1000), 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options, 'preferences' => $preferences
                );
                $c++;
            }

            $this->data['inv_items'] = json_encode($pr);
            // $this->sma->print_arrays($pr);
            $this->data['id'] = $id;
            //$this->data['currencies'] = $this->site->getAllCurrencies();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['tax_rates'] = $this->site->getAllTaxRates();
            $this->data['warehouses'] = $this->site->getAllWarehouses(1);
            $Dsellers = $this->site->getSellerByBiller($this->data['inv']->biller_id);
            $sellers = [];
            if ($Dsellers != FALSE) {
                foreach ($Dsellers as $row => $seller) {
                    $sellers[$seller['seller_id']] = $seller['name'];
                }
            }
            $this->data['sellers'] = $sellers;

            $user_group_name = $this->site->getUserGroup();
            $this->data['user_group_name'] = $user_group_name->name;
            $Daddresses = $this->site->getAddressByCustomer($this->data['inv']->customer_id);
            $addresses = [];

            if ($Daddresses != FALSE) {
                foreach ($Daddresses as $row => $address) {
                    $addresses[$address->id] = $address->sucursal;
                }
            }
            $this->data['addresses'] = $addresses;
            $this->data['converting'] = $converting;

            $this->page_construct('orders/edit', ['page_title' => lang('edit_order')], $this->data);
        }
    }

    public function validate_key_log($doc_type_id)
    {
        if ($doc_type_id) {
            $doc_type = $this->site->getDocumentTypeById($doc_type_id);
            if ($doc_type) {
                $kl = (string) $doc_type->key_log;
                exit($kl);
            }
        }
        echo FALSE;
    }

    public function add_invoice_note($id = null)
    {
        $this->sma->checkPermissions('add', true);

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['invoce_notes'] = $this->site->get_invoice_notes();
        $this->load_view($this->theme . 'sales/add_invoice_note', $this->data);
    }

    public function add_invoice_order_note($id = null)
    {
        $this->sma->checkPermissions('add', true);

        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $this->data['invoce_notes'] = $this->site->get_invoice_notes();
        $this->load_view($this->theme . 'sales/add_invoice_order_note', $this->data);
    }

    public function contabilizarVenta($sale_id = null, $json = null)
    {

        $this->form_validation->set_rules('invoice_id', lang("invoice_id"), 'required');

        if ($this->form_validation->run() == true) {

            $sale_id = $this->input->post('invoice_id');
            $msg = $this->sales_model->recontabilizarVenta($sale_id);
            if ($this->session->userdata('reaccount_error')) {
                $this->session->set_flashdata('error', $msg);
                $this->session->unset_userdata('reaccount_error');
            } else {
                $this->session->set_flashdata('message', $msg);
            }
            admin_redirect($_SERVER["HTTP_REFERER"]);
        } else {

            $sale = $this->sales_model->getSaleByID($sale_id, true);
            $exists = $this->site->getEntryTypeNumberExisting($sale, false, ($sale['sale_status'] == 'returned' ? true : false));

            $payments = $this->sales_model->getInvoicePayments($sale_id);
            $alert = false;
            if ($payments) {
                foreach ($payments as $pmnt) {
                    if ($pmnt->multi_payment && (($pmnt->rete_fuente_total > 0 && !$pmnt->rete_fuente_id) || ($pmnt->rete_iva_total > 0 && !$pmnt->rete_iva_id) || ($pmnt->rete_ica_total > 0 && !$pmnt->rete_ica_id) || ($pmnt->rete_other_total > 0 && !$pmnt->rete_other_id))) {
                        $alert = true;
                    }
                }
            }

            if ($alert) {
                $this->data['alert'] = lang('sale_with_incomplete_payment_data');
            }


            $this->data['exists'] = $exists;
            $this->data['inv'] = $sale;
            $this->load_view($this->theme . 'sales/recontabilizar', $this->data);
        }
    }

    public function sincronizarDevolucionesVentas()
    {

        if ($result = $this->sales_model->syncSaleReturns()) {
            $text = "Costing en  Devoluciones insertados : " . $result['insertadas'] . ", Costing en  Devoluciones actualizadas : " . $result['actualizadas'];
            $this->session->set_flashdata('message', $text);
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function sale_suggestions()
    {
        $rows_start = $this->input->get('rows_start');
        $num_rows = $this->input->get('num_rows');
        $customer_id = $this->input->get('customer_id');
        $address_id = $this->input->get('address_id') && $this->input->get('address_id') != '' ? $this->input->get('address_id') : null;
        $biller_id = $this->input->get('biller_id') && $this->input->get('biller_id')  != '' && $this->input->get('biller_id')  != 'all' ? $this->input->get('biller_id') : null;
        $sale_id = $this->input->get('sale_id') && $this->input->get('sale_id') != '' ? $this->input->get('sale_id') : null;
        $sales = $this->sales_model->getSalesPaymentPending($customer_id, $address_id, $biller_id, $sale_id, $rows_start, $num_rows);
        if ($sales) {
            foreach ($sales as $sale) {
                $sale->sale_selected = false;
                $sale->amount_to_paid = 0;
                $sale->prev_retentions_total = $sale->rete_fuente_total + $sale->rete_iva_total + $sale->rete_ica_total + $sale->rete_other_total;
                $sale->rete_fuente_total = 0;
                $sale->rete_iva_total = 0;
                $sale->rete_ica_total = 0;
                $sale->rete_other_total = 0;
                $sale->rete_bomberil_total = 0;
                $sale->rete_autoaviso_total = 0;
                $sale->rete_autoica_total = 0;
                $rete_fuente_base_paid = $rete_iva_base_paid = $rete_ica_base_paid = $rete_other_base_paid = 0;
                if ($bases = $this->sales_model->get_total_payments_for_sale($sale->id)) {
                    $rete_fuente_base_paid = $bases->fuente_base;
                    $rete_iva_base_paid = $bases->iva_base;
                    $rete_ica_base_paid = $bases->ica_base;
                    $rete_other_base_paid = $bases->other_base;
                }
                $sale->rete_fuente_base_paid = $sale->rete_fuente_base + $rete_fuente_base_paid;
                $sale->rete_ica_base_paid = $sale->rete_ica_base + $rete_ica_base_paid;
                $sale->rete_iva_base_paid = $sale->rete_iva_base + $rete_iva_base_paid;
                $sale->rete_other_base_paid = $sale->rete_other_base + $rete_other_base_paid;
                $sale->address_data = $this->site->getAddressByID($sale->address_id);
                $sale->sale_in_view = false;
                $sale->discount = false;
                $sale->activated_manually = false;
                $sale->deactivated_manually = false;
                $sale->max_date = date('Y-m-d', strtotime($sale->date));
            }
            $this->sma->send_json($sales);
        } else {
            $this->sma->send_json(array('response' => 0));
        }
    }

    public function sync_payments($sale_id)
    {
        $this->site->syncSalePayments($sale_id);
    }

    public function validate_assigned_technology_provider()
    {
        $document_type_id = $this->input->post('document_type_id');

        $assigned = $this->Settings->fe_technology_provider == '0' ? FALSE : TRUE;

        if ($document_type_id) {
            $resolution_data = $this->site->getDocumentTypeById($document_type_id);
            if ($resolution_data->factura_electronica == 1) {
                if ($this->Settings->fe_technology_provider == '0') {
                    echo json_encode(FALSE);
                } else {
                    echo json_encode(TRUE);
                }
            } else {
                echo json_encode(TRUE);
            }
        } else {
            echo json_encode($assigned);
        }
    }

    public function resend_electronic_document($sale_id)
    {
        $sale = $this->site->getSaleByID($sale_id);
        $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);

        if ($resolution_data->factura_electronica == 1) {
            $invoice_data = [
                'sale_id' => $sale->id,
                'biller_id' => $sale->biller_id,
                'customer_id' => $sale->customer_id,
                'reference' => $sale->reference_no,
                'attachment' => $sale->attachment
            ];

            $this->create_document_electronic($invoice_data);
        }

        $existing_sale = $this->site->getSaleByID($sale_id);

        if ($existing_sale->fe_aceptado == ACCEPTED) {
            if ($existing_sale->pos == 1) {
                admin_redirect('pos/fe_index/0/' . $sale_id);
            }else{
                admin_redirect('sales/fe_index/0/' . $sale_id);
            }
        } else {
            if ($existing_sale->pos == 1) {
                admin_redirect('pos/fe_index');
            }else{
                admin_redirect('sales/fe_index');
            }
        }
    }

    private function create_document_electronic($invoice_data)
    {
        if ($_SERVER['SERVER_NAME'] == 'localhost') {
            return true;
            return true;
            return true;
        }
        $invoice_data = (object) $invoice_data;
        $created_file = $this->Electronic_billing_model->send_document_electronic($invoice_data);
        $filename = $this->site->getFilename($invoice_data->sale_id);
        $technologyProvider = $this->Settings->fe_technology_provider;

        if ($created_file->response == 0) {
            $this->session->set_flashdata('error', $created_file->message);
        } else {
            $email_delivery_response = '';

            if (in_array($technologyProvider, [CADENA, BPM, DELCOP, SIMBA])) {
                if ($technologyProvider == SIMBA) {
                    $sale = $this->site->getSaleByID($invoice_data->sale_id);
                    $biller_data = $this->companies_model->getBillerData(['biller_id' => $sale->biller_id]); // <- biller data para validar el consumidor por defecto en la sucursal
                    if ($biller_data->default_customer_id == $sale->customer_id) { // <- Sí, el cliente de la factura es el mismo cliente por defecto de la sucursal, entra aca y no envia correo
                        $this->session->set_flashdata('message', 'Documento electrónico creado correctamente');
                        return true;
                    }
                }
            // if ($technologyProvider == CADENA || $technologyProvider == BPM || $technologyProvider == DELCOP || $technologyProvider == SIMBA) {
                $this->download_fe_xml($invoice_data->sale_id, TRUE);
                $this->download_fe_pdf($invoice_data->sale_id, TRUE);

                if (file_exists('files/electronic_billing/' . $filename . '.xml') && file_exists('files/electronic_billing/' . $filename . '.pdf')) {
                    $zip = new ZipArchive();

                    $zip->open('files/electronic_billing/' . $filename . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
                    $zip->addFile("files/electronic_billing/" . $filename . '.xml', $filename . ".xml");
                    $zip->addFile("files/electronic_billing/" . $filename . '.pdf', $filename . ".pdf");

                    if ($this->Settings->add_individual_attachments == YES) {
                        $sale = $this->site->getSaleByID($invoice_data->sale_id);
                        if ($sale->attachment != '') {
                            $zip->addFile('files/'.$sale->attachment, $sale->attachment);
                        }
                    }

                    $zip->close();

                    unlink('files/electronic_billing/' . $filename . '.xml');
                    unlink('files/electronic_billing/' . $filename . '.pdf');

                    if ($_SERVER['SERVER_NAME'] != 'localhost') {
                        $email_delivery_response = $this->Electronic_billing_model->receipt_delivery($invoice_data, $filename);
                    }
                }
            }
            if (file_exists('files/electronic_billing/' . $filename . '.xml')) {
                unlink('files/electronic_billing/' . $filename . '.xml');
            }
            if (file_exists('files/electronic_billing/' . $filename . '.pdf')) {
                unlink('files/electronic_billing/' . $filename . '.pdf');
            }

            $this->session->set_flashdata('message', 'Documento electrónico creado correctamente <br>' . $email_delivery_response);
        }
    }

    public function download_fe_xml($sale_id, $internal_download = FALSE)
    {
        $sale = $this->site->getSaleByID($sale_id);
        $technologyProvider = $this->Settings->fe_technology_provider;
        $filename = $this->site->getFilename($sale_id);

        if ($technologyProvider == CADENA) {
            if (!empty($sale->fe_xml)) {
                $xml_file = base64_decode($sale->fe_xml);

                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $path = 'files/electronic_billing';
                    if (!file_exists($path)) {
                        mkdir($path, 0777);
                    }

                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            } else {
                $prefix = strstr($sale->reference_no, '-', TRUE);
                $reference = str_replace('-', '', $sale->reference_no);
                $document_type_code = ($sale->total > 0) ? '01' : '92';
                $technology_provider_configuration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

                $curl = curl_init();
                curl_setopt_array($curl, array(
                    CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor=' . $this->Settings->numero_documento . '&id_documento=' . $reference . '&codigo_tipo_documento=' . $document_type_code . '&prefijo=' . $prefix,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_ENCODING => '',
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => TRUE,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => 'GET',
                    CURLOPT_HTTPHEADER => [
                        'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                        'Content-Type: application/json',
                        'Partnership-Id: 901090070'
                    ],
                ));
                $response = curl_exec($curl);
                log_message('debug', 'CADENA FE-Consulta documento: '. $response);
                curl_close($curl);

                $response = json_decode($response);
                if (!empty($response)) {
                    if (isset($response->statusCode)) {
                        if ($response->statusCode == 200) {
                            $updated_sale = $this->site->updateSale([
                                'fe_aceptado' => 2,
                                'fe_mensaje' => 'Documento aceptado por la DIAN',
                                'fe_mensaje_soporte_tecnico' => 'La Factura electrónica ' . $reference . ', ha sido autorizada. Código es estado ' . $response->statusCode,
                                'fe_xml' => $response->document
                            ], $sale_id);

                            if ($updated_sale == TRUE) {
                                $xml_file = base64_decode($response->document);

                                if ($internal_download === TRUE) {
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                                    $xml->loadXML($xml_file);

                                    $path = 'files/electronic_billing';
                                    if (!file_exists($path)) {
                                        mkdir($path, 0777);
                                    }

                                    $xml->save('files/electronic_billing/' . $sale->reference_no . '.xml');
                                } else {
                                    header('Content-Type: application/xml;');
                                    header('Content-Disposition: attachment; filename="' . $sale->reference_no . '.xml"');
                                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                                    $xml->loadXML($xml_file);
                                    echo $xml->saveXML();
                                }
                            } else {
                                $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML.');
                                admin_redirect('sales/fe_index');
                            }
                        } else if ($response->statusCode == 400) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. El prefijo no coincide con el prefijo del id del documento.');
                            admin_redirect('sales/fe_index');
                        } else if ($response->statusCode == 404) {
                            $this->session->set_flashdata('error', 'No fue posible descargar el archivo XML. Registro no encontrado con los parámetros de búsqueda enviados.');
                            admin_redirect('sales/fe_index');
                        }
                    } else {
                        $this->session->set_flashdata('error', 'Se ha perdido la conexión con el Web Services.');
                        admin_redirect('sales/fe_index');
                    }
                } else {
                    $this->session->set_flashdata('error', 'No es posible realizar conexión con Web Services.');
                    admin_redirect('sales/fe_index');
                }
            }

        } else if ($technologyProvider == DELCOP) {
            $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
            $responseToken = $this->Electronic_billing_model->delcopGetAuthorizationoken();
            if ($responseToken->success == FALSE) {
                $this->session->set_flashdata('error', "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $responseToken->error);
                admin_redirect('sales/fe_index');
            }


            $url = ($this->Settings->fe_work_environment == TEST) ? "https://www-prueba.titanio.com.co/PDE/public/api/PDE/descargar" : "https://www.titanio.com.co/PDE/public/api/PDE/descargar";

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => '{"token": "' . $responseToken->token . '", "documentos": [{"transaccion_id": "' . $sale->fe_id_transaccion . '", "tipo_descarga": "1"}]}',
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                ),
            ));

            $response = curl_exec($curl);

            curl_close($curl);

            $response = json_decode($response);

            if ($response->error_id == 0) {
                $sale_data["fe_xml"] = $response->documentos[0]->data;
                $updated_sale = $this->site->updateSale($sale_data, $sale->id);

                $xml_file = base64_decode($response->documentos[0]->data);
                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);

                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                } else {
                    header('Content-Type: application/xml;');
                    header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                    $xml = new DOMDocument("1.0", "ISO-8859-15");

                    $xml->loadXML($xml_file);
                    echo $xml->saveXML();
                }
            }
        } else if ($technologyProvider == BPM) {
            $xml_file = base64_decode($sale->fe_xml);

            if ($internal_download === TRUE) {
                $xml = new DOMDocument("1.0", "ISO-8859-15");
                $xml->loadXML($xml_file);

                $path = 'files/electronic_billing';
                if (!file_exists($path)) {
                    mkdir($path, 0777);
                }

                $xml->save('files/electronic_billing/' . $filename . '.xml');
            } else {
                header('Content-Type: application/xml;');
                header('Content-Disposition: attachment; filename="' . $filename . '.xml"');
                $xml = new DOMDocument("1.0", "ISO-8859-15");

                $xml->loadXML($xml_file);
                echo $xml->saveXML();
            }
        } else if ($technologyProvider == SIMBA) {
            if (!empty($sale->fe_xml)){
                $fe_xml = $sale->fe_xml;
                if ($sale->technology_provider == SIMBA) { // <- Esto se hace necesario ya que, cuando se quiera generar un xml de facturas pasadas generadas con otro proveedor tecnologico, no se debe consultar el xml en simba pues no va a existir
                    $fe_xml = $this->Electronic_billing_model->getXMLSimba($sale_id); // <- Consulta el xml en simba
                }
            }else{
                $typeDocument = $this->site->getTypeElectronicDocument($sale_id);
                if ($typeDocument == INVOICE) {
                    sleep(10);
                }
                if ($typeDocument == CREDIT_NOTE) {
                    sleep(15);
                }
                $fe_xml = $this->Electronic_billing_model->getXMLSimba($sale_id);
            }
            if ($fe_xml) {
                $xml_file = base64_decode($fe_xml);
                if ($internal_download === TRUE) {
                    $xml = new DOMDocument("1.0", "ISO-8859-15");
                    $xml->loadXML($xml_file);
                    $path = 'files/electronic_billing';
                    if (!file_exists($path)) {
                        mkdir($path, 0777);
                    }
                    $xml->save('files/electronic_billing/' . $filename . '.xml');
                }
            }
        }
    }

    public function download_fe_pdf($sale_id, $internal_download = FALSE)
    {
        $technologyProvider = $this->Settings->fe_technology_provider;
        $filename = $this->site->getFilename($sale_id);

        if ($technologyProvider == CADENA || $technologyProvider == BPM || $technologyProvider == SIMBA) {
            $this->saleView($sale_id, $internal_download);
        } else if ($technologyProvider == DELCOP) {
            if ($this->Settings->wappsi_print_format == YES) {
                $this->saleView($sale_id, $internal_download);
            } else {
                $sale = $this->site->getSaleByID($sale_id);
                $resolution_data = $this->site->getDocumentTypeById($sale->document_type_id);
                $response_token = $this->Electronic_billing_model->delcopGetAuthorizationoken();
                if ($response_token->success == FALSE) {
                    $this->session->set_flashdata('error', "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $response_token->error);
                    admin_redirect('sales/fe_index');
                }

                if ($this->Settings->fe_work_environment == TEST) {
                    $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/descargar";
                } else {
                    $url = "https://www.titanio.com.co/PDE/public/api/PDE/descargar";
                }

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL => $url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => "POST",
                    CURLOPT_POSTFIELDS => '{"token": "' . $response_token->token . '", "documentos": [{"transaccion_id": "' . $sale->fe_id_transaccion . '", "tipo_descarga": "2"}]}',
                    CURLOPT_HTTPHEADER => array(
                        "Content-Type: application/json",
                    ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);

                $response = json_decode($response);

                if ($response->error_id == 0) {
                    $xml_file = base64_decode($response->documentos[0]->data);

                    if ($internal_download === TRUE) {
                        $xml = new DOMDocument("1.0", "ISO-8859-15");
                        $xml->loadXML($xml_file);

                        $xml->save('files/electronic_billing/' . $filename . '.pdf');
                    } else {
                        file_put_contents("files/electronic_billing/" . $filename . ".pdf", $xml_file);

                        header("Content-type: application/pdf");
                        header("Content-Disposition: inline; filename=documento.pdf");
                        readfile("files/electronic_billing/" . $filename . ".pdf");
                    }
                }
            }
        }
    }

    public function validate_fe_customer_data($customer_id = NULL)
    {
        if (empty($customer_id)) {
            return FALSE;
            exit();
        }

        $customer = $this->site->getCompanyByID($customer_id);
        $tipo_documento = $customer ? $this->site->get_document_type_by_id($customer->tipo_documento) : NULL;
        $country = $this->site->getCountryByName($customer->country);
        $types_customer_obligations = $this->site->getTypesCustomerObligations($customer_id, ACQUIRER);
        $is_invalid = false;
        $empty_columns = [];

        $required_data = [
            'type_person',
            'vat_no',
            'tipo_regimen',
            'address',
            'country',
            'state',
            'city',
            "email"
        ];

        if ($customer->type_person == LEGAL_PERSON) {
            $required_data[] = "digito_verificacion";
            $required_data[] = "name";
            $required_data[] = "company";
        } else if ($customer->type_person == NATURAL_PERSON) {
            $required_data[] = "first_name";
            $required_data[] = "first_lastname";
        }

        if (strpos($customer->name, 'Ocasional') !== false) {
            exit(lang('fe_customer_cannot_be_occasional'));
        }

        foreach ($required_data as $key => $column) {
            if ($column != "digito_verificacion") {
                if (empty($customer->{$column})) {
                    $empty_columns[] = $column;
                    if ($is_invalid == false) {
                        $is_invalid = true;
                    }
                }
            } else {
                if ($customer->{$column} == "") {
                    $empty_columns[] = $column;
                    if ($is_invalid == false) {
                        $is_invalid = true;
                    }
                }
            }
        }

        if (!$tipo_documento) {
            $empty_columns[] = lang('id_document_type');
        }

        if (!$country || empty($country->codigo_iso)) {
            $empty_columns[] = lang('codigo_iso');
        }

        if ($this->sma->validate_only_number($customer->phone)) {
            $is_invalid = true;
        }

        if ($this->sma->vatNoIsIncorrect($customer->document_code, $customer->vat_no)) {
            $is_invalid = true;
        }

        if (empty($types_customer_obligations)) {
            $is_invalid = TRUE;
            $empty_columns[] = "Responsabilidades";
        }

        if ($customer && $customer->customer_only_for_pos == YES) {
            $is_invalid = TRUE;
            $empty_columns[] = lang('customer_only_for_pos');
        }

        if ($is_invalid) {
            $msg = lang('empty_customer_columns');
            $msg .= ' <a href="' . admin_url('customers/edit/') . $customer_id . '" data-toggle="modal" data-target="#myModal"> ' . lang('clic_x_edit') . ' </a>';
        } else {
            $msg = '';
        }

        echo $msg;
    }

    public function complete_convert_order($where, $order_id)
    {
        $this->sales_model->complete_convert_order($order_id);
        if ($where == 1) {
            admin_redirect("pos/index/" . $order_id . "/1/0/1");
        } else if ($where == 2) {
            admin_redirect("pos/add_wholesale/" . $order_id . "/0/1");
        } else {
            admin_redirect("sales/add/" . $order_id . "/1");
        }
    }

    public function get_order_sales_ftp()
    {
        // $root = dirname(dirname(dirname(__DIR__)))."/assets/import/";
        $root = dirname(dirname(dirname(__DIR__))) . "/assets/wms/import/";
        $directorio = opendir($root);
        $ordenes = [];
        $ordenes_incorrectas = [];
        $invalid_orders = "";
        $cnt = 0;
        $response = [];
        while ($archivo = readdir($directorio)) {
            $arrResult = [];
            if (is_dir($archivo)) {
                // echo "[".$archivo . "]<br />";
            } else {
                if (strpos($archivo, ".csv") === false) {
                    continue;
                }
                $enc_data = explode('_', $archivo);
                $enc_data = $enc_data[1];
                $año = substr($enc_data, 0, 4);
                $mes = substr($enc_data, 4, 2);
                $dia = substr($enc_data, 6, 2);
                $hora = substr($enc_data, 8, 2);
                $minuto = substr($enc_data, 10, 2);
                $segundo = substr($enc_data, 12, 2);
                $date = $año . "-" . $mes . "-" . $dia . " " . $hora . ":" . $minuto . ":" . $segundo;
                $handle = fopen($root . $archivo, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ";")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $cnt_items = 0;
                $total_tax = 0;
                $total_discount = 0;
                $total = 0;
                $pr_qty = 0;
                $grand_total = 0;
                foreach ($arrResult as $key => $producto) {

                    if (count($producto) == 1) {
                        break;
                    }

                    $reference = $producto[0] . "-" . $producto[1];
                    if ($this->sales_model->get_order_sale_by_reference($reference)) {
                        $error = 'Referencia duplicada';
                        if ($invalid_orders != '') {
                            $invalid_orders .= ', ';
                        }
                        $invalid_orders .= $reference . " (" . $error . ")";
                        break;
                    }
                    $document_type = $this->site->get_document_type_by_prefix($producto[0]);
                    $biller = $this->site->get_company_by_field_name('name', $producto[3], 'biller');
                    $warehouse = $this->site->get_warehouse_by_code($producto[4]);
                    $customer = $this->site->get_company_by_field_name('vat_no', $producto[7], 'customer');
                    $address = false;
                    if ($customer) {
                        $address = $this->site->get_address_by_code($producto[8], $customer->id);
                    }

                    if ($biller && $warehouse && $customer && $address && $document_type) {

                        $customer_group = $this->site->getCustomerGroupByID($customer->customer_group_id);

                        if (!isset($ordenes[$cnt]['reference_no']) && !isset($ordenes_incorrectas[$reference])) {
                            $ordenes[$cnt]['file_name'] = $archivo;
                            $ordenes[$cnt]['reference_no'] = $reference;
                            // $ordenes[$cnt]['date'] = $producto[2];
                            $ordenes[$cnt]['date'] = $date;
                            $ordenes[$cnt]['biller_id'] = $biller->id;
                            $ordenes[$cnt]['warehouse_id'] = $warehouse->id;
                            $ordenes[$cnt]['customer_id'] = $customer->id;
                            $ordenes[$cnt]['address_id'] = $address->id;
                            //Nuevos campos
                            $ordenes[$cnt]['product_discount'] = NULL;
                            $ordenes[$cnt]['total_discount'] = NULL;
                            $ordenes[$cnt]['product_tax'] = NULL;
                            $ordenes[$cnt]['total_tax'] = NULL;
                            $ordenes[$cnt]['total'] = NULL;
                            $ordenes[$cnt]['grand_total'] = NULL;
                            $ordenes[$cnt]['total_items'] = NULL;
                            $ordenes[$cnt]['sale_status'] = 'pending';
                            $ordenes[$cnt]['payment_status'] = 'pending';
                            $ordenes[$cnt]['seller_id'] = $address->customer_address_seller_id_assigned;
                            //Campos listos
                            $ordenes[$cnt]['customer'] = $customer->name;
                            $ordenes[$cnt]['biller'] = $biller->name;
                            $ordenes[$cnt]['note'] = $producto[9];
                            $ordenes[$cnt]['staff_note'] = '';
                            $ordenes[$cnt]['order_discount_id'] = NULL;
                            $ordenes[$cnt]['order_discount'] = NULL;
                            $ordenes[$cnt]['order_tax_id'] = NULL;
                            $ordenes[$cnt]['order_tax'] = NULL;
                            $ordenes[$cnt]['shipping'] = NULL;
                            $ordenes[$cnt]['payment_term'] = NULL;
                            $ordenes[$cnt]['due_date'] = NULL;
                            $ordenes[$cnt]['paid'] = 0;
                            $ordenes[$cnt]['created_by'] = $this->session->userdata('user_id');
                            $ordenes[$cnt]['hash'] = hash('sha256', microtime() . mt_rand());
                            $ordenes[$cnt]['resolucion'] = NULL;
                            $ordenes[$cnt]['document_type_id'] = $document_type->id;
                        }

                        if (isset($ordenes[$cnt])) {
                            $product = $this->site->getProductByCode($producto[5]);
                            if ($product) {
                                $ordenes[$cnt]['items'][$cnt_items]['product_id'] = $product->id;
                                $ordenes[$cnt]['items'][$cnt_items]['product_code'] = $producto[5];
                                $ordenes[$cnt]['items'][$cnt_items]['quantity'] = $producto[6];
                                $ordenes[$cnt]['items'][$cnt_items]['quantity_to_bill'] = $producto[6];
                                $ordenes[$cnt]['items'][$cnt_items]['unit_quantity'] = $producto[6];
                                $ordenes[$cnt]['items'][$cnt_items]['warehouse_id'] = $warehouse->id;

                                $pr_price_data = $this->site->get_item_price($product, $customer, $customer_group, $biller, $address->id, $product->sale_unit);
                                $ordenes[$cnt]['items'][$cnt_items]['real_unit_price'] = $pr_price_data['new_price'];
                                $pr_discount_rate = $pr_price_data['new_discount'];
                                $pr_discount_val = 0;
                                $pr_price = $pr_price_data['new_price'];
                                $pr_tax_val = 0;
                                $pr_tax_rate = 0;
                                $tax_details = $this->site->getTaxRateByID($product->tax_rate);
                                $price_before_tax = $pr_price;
                                if ($tax_details->rate > 0) {
                                    $price_before_tax = $pr_price / (1 + ($tax_details->rate / 100));
                                    $pr_tax_val = $pr_price - $price_before_tax;
                                }
                                $pr_discount_val = $price_before_tax * ($pr_discount_rate / 100);
                                $net_unit_price = $price_before_tax - $pr_discount_val;

                                if ($pr_discount_val > 0 && $tax_details->rate > 0) {
                                    $pr_tax_val = $net_unit_price * ($tax_details->rate / 100);
                                }

                                $unit_price = $net_unit_price + $pr_tax_val;

                                $ordenes[$cnt]['items'][$cnt_items]['product_name'] = $product->name;
                                $ordenes[$cnt]['items'][$cnt_items]['product_type'] = $product->type;
                                $ordenes[$cnt]['items'][$cnt_items]['option_id'] = NULL;
                                $ordenes[$cnt]['items'][$cnt_items]['net_unit_price'] = $net_unit_price;
                                $ordenes[$cnt]['items'][$cnt_items]['unit_price'] = $unit_price;
                                $ordenes[$cnt]['items'][$cnt_items]['product_unit_id'] = $product->sale_unit;
                                $ordenes[$cnt]['items'][$cnt_items]['product_unit_code'] = NULL;
                                $ordenes[$cnt]['items'][$cnt_items]['item_tax'] = $pr_tax_val * $producto[6];
                                $ordenes[$cnt]['items'][$cnt_items]['tax_rate_id'] = $product->tax_rate;
                                $ordenes[$cnt]['items'][$cnt_items]['tax'] = $tax_details->rate . "%";
                                $ordenes[$cnt]['items'][$cnt_items]['discount'] = $pr_discount_rate . "%";
                                $ordenes[$cnt]['items'][$cnt_items]['item_discount'] = $pr_discount_val * $producto[6];
                                $ordenes[$cnt]['items'][$cnt_items]['subtotal'] = $unit_price * $producto[6];
                                $ordenes[$cnt]['items'][$cnt_items]['serial_no'] = NULL;
                                // $ordenes[$cnt]['items'][$cnt_items]['real_unit_price'] = $price_before_tax;
                                $ordenes[$cnt]['items'][$cnt_items]['price_before_tax'] = $price_before_tax;
                                $total_tax += $pr_tax_val * $producto[6];
                                $total_discount += $pr_discount_val * $producto[6];
                                $total += $net_unit_price * $producto[6];
                                $grand_total += $unit_price * $producto[6];
                                $pr_qty += $producto[6];
                                $cnt_items++;
                            } else {
                                $cnt_items = 0;
                                unset($ordenes[$cnt]);
                                if (isset($ordenes_incorrectas[$reference])) {
                                    $ordenes_incorrectas[$reference] .= $producto[5] . ", ";
                                } else {
                                    $ordenes_incorrectas[$reference] = 'Productos incorrectos : ' . $producto[5] . ", ";
                                }
                            }
                        }
                    } else {
                        $error = '';

                        if (!$biller) {
                            $error = 'Sucursal inválida';
                        }
                        if (!$warehouse) {
                            if ($error != '') {
                                $error .= ', ';
                            }
                            $error .= 'Bodega inválida';
                        }
                        if (!$customer) {
                            if ($error != '') {
                                $error .= ', ';
                            }
                            $error .= 'Cliente inválido';
                        }
                        if (!$address) {
                            if ($error != '') {
                                $error .= ', ';
                            }
                            $error .= 'Sucursal de cliente inválida o no coincide con el NIT de cliente indicado';
                        }
                        if (!$document_type) {
                            if ($error != '') {
                                $error .= ', ';
                            }
                            $error .= 'Error en con la referencia indicada en el detalle';
                        }

                        if ($invalid_orders != '') {
                            $invalid_orders .= ', ';
                        }

                        $invalid_orders .= $reference . " (" . $error . ")";

                        $cnt_items = 0;
                        unset($ordenes[$cnt]);
                        if (isset($ordenes_incorrectas[$reference])) {
                            $ordenes_incorrectas[$reference] .= $producto[5] . ", ";
                        } else {
                            $ordenes_incorrectas[$reference] = 'Productos incorrectos : ' . $producto[5] . ", ";
                        }

                        break;
                    }
                }

                if ($grand_total > 0 && !isset($ordenes_incorrectas[$reference])) {
                    $ordenes[$cnt]['product_discount'] = $total_discount;
                    $ordenes[$cnt]['total_discount'] = $total_discount;
                    $ordenes[$cnt]['product_tax'] = $total_tax;
                    $ordenes[$cnt]['total_tax'] = $total_tax;
                    $ordenes[$cnt]['total'] = $total;
                    $ordenes[$cnt]['grand_total'] = $grand_total;
                    $ordenes[$cnt]['total_items'] = $pr_qty;
                }
                $cnt++;
            }
        }

        if (count($ordenes) > 0) {
            foreach ($ordenes as $orden) {
                foreach ($orden as $name => $value) {
                    if ($name == 'file_name') {
                        continue;
                    }
                    if ($name != 'items') {
                        $data[$name] = $value;
                    } else {
                        $cnt = 0;
                        foreach ($value as $key => $product) {
                            foreach ($product as $p_field => $p_value) {
                                $items[$cnt][$p_field] = $p_value;
                            }
                            $cnt++;
                        }
                    }
                }
                if ($this->sales_model->addOrderSale($data, $items, [])) {
                    $copy = copy($root . $orden['file_name'], $root . "ok/" . $orden['file_name']);
                    $unlink = unlink($root . $orden['file_name']);
                } else {
                    $cnt_rspns = count($response);
                    $response[$cnt_rspns]['successfully'] = "false";
                    $response[$cnt_rspns]['msg'] = 'No se guardó la orden ' . $orden['reference_no'];
                }
                unset($data, $items);
            }
        } else {
            $cnt_rspns = count($response);
            $response[$cnt_rspns]['successfully'] = "false";
            $response[$cnt_rspns]['msg'] = 'No se encontró ninguna orden válida, por favor verifique los formatos';
        }

        if (count($ordenes_incorrectas) > 0) {
            foreach ($ordenes_incorrectas as $oi_ref => $msg) {
                $cnt_rspns = count($response);
                $response[$cnt_rspns]['successfully'] = "false";
                $response[$cnt_rspns]['msg'] =  "Orden " . $oi_ref . " con " . trim($msg, ', ');
            }
        }

        if ($invalid_orders != '') {
            $cnt_rspns = count($response);
            $response[$cnt_rspns]['successfully'] = "false";
            $response[$cnt_rspns]['msg'] = 'Ordenes invalidas : ' . $invalid_orders;
        }

        if (count($response) == 0) {
            $response[0]['successfully'] = "true";
            $response[0]['msg'] = 'Se completó exitosamente el proceso';
        }

        echo json_encode($response);
    }

    public function send_electronic_invoice_email($document_id = NULL, $email = NULL, $from_change_status_method = FALSE)
    {
        $document_id = !empty($document_id) ? $document_id : $this->input->get("sale_id");
        $email = !empty($email) ? $email : $this->input->get("email");

        $email_delivery_response = '';
        $sale = $this->site->getSaleByID($document_id);
        $filename = $this->site->getFilename($document_id);

        $invoice_data = (object) [
            'sale_id' => $sale->id,
            'biller_id' => $sale->biller_id,
            'customer_id' => $sale->customer_id,
            'reference' => $sale->reference_no
        ];

        if (!empty($email)) {
            $invoice_data->email = $email;
        }

        try {
            $this->download_fe_xml($invoice_data->sale_id, TRUE);
            $this->download_fe_pdf($invoice_data->sale_id, TRUE);
        } catch (Exception $e) {
            log_message("error", "Error al intentar crear el archivo XML o el PDF: " . $e->getMessage());

            $this->session->set_flashdata('message', "No fue posible enviar el correo. El archivo XML o el PDF no fueron generados");
            admin_redirect('sales/fe_index');
        }


        if (file_exists('files/electronic_billing/' . $filename . '.xml') && file_exists('files/electronic_billing/' . $filename . '.pdf')) {
            $zip = new ZipArchive();

            $zip->open('files/electronic_billing/' . $filename . ".zip", ZipArchive::CREATE | ZipArchive::OVERWRITE);
            $zip->addFile("files/electronic_billing/" . $filename . '.xml', $filename . ".xml");
            $zip->addFile("files/electronic_billing/" . $filename . '.pdf', $filename . ".pdf");

            if ($this->Settings->add_individual_attachments == YES) {
                $sale = $this->site->getSaleByID($invoice_data->sale_id);
                if ($sale->attachment != '') {
                    $zip->addFile('files/'.$sale->attachment, $sale->attachment);
                }
            }

            $zip->close();

            unlink('files/electronic_billing/' . $filename . '.xml');
            unlink('files/electronic_billing/' . $filename . '.pdf');

            // if ($_SERVER['SERVER_NAME'] != 'localhost') {
                $email_delivery_response = $this->Electronic_billing_model->receipt_delivery($invoice_data, $filename);
            // }
        } else {
            if (file_exists('files/electronic_billing/' . $filename . '.xml')) {
                unlink('files/electronic_billing/' . $filename . '.xml');
            }
            if (file_exists('files/electronic_billing/' . $filename . '.pdf')) {
                unlink('files/electronic_billing/' . $filename . '.pdf');
            }
            $email_delivery_response = "No fue posible enviar el correo. El archivo XML o el PDF no fueron generados.";
        }

        if ($from_change_status_method == FALSE) {
            if (empty($email)) {
                $this->session->set_flashdata('message', 'Documento electrónico enviado correctamente' . $email_delivery_response);
            } else {
                $this->session->set_flashdata('message', $email_delivery_response);
            }

            admin_redirect('sales/fe_index');
        } else {
            return $email_delivery_response;
        }
    }

    public function validate_is_contingency_invoice()
    {
        $document_id = $this->input->post("document_id");
        $document_data = $this->site->getSaleByID($document_id);
        $resolution_data = $this->site->getDocumentTypeById($document_data->document_type_id);

        echo $resolution_data->factura_contingencia;
    }

    public function get_email_customer_by_sale_id()
    {
        $sale_id = $this->input->post("sale_id");
        $sale = $this->site->getSaleByID($sale_id);
        $customer = $this->sales_model->get_email_customer($sale->customer_id);

        echo json_encode($customer->email);
    }

    public function showPreviousRequest($documentId)
    {
        $document = $this->site->getSaleByID($documentId);
        $resolution = $this->site->getDocumentTypeById($document->document_type_id);

        if ($resolution->factura_electronica == YES) {
            $documentElectronicData = (object) [
                'sale_id' => $document->id,
                'biller_id' => $document->biller_id,
                'customer_id' => $document->customer_id,
                'reference' => $document->reference_no
            ];

            $this->Electronic_billing_model->buildRequestFile($documentElectronicData, TRUE);
        }
    }

    public function document_status($document_id)
    {
        $sale = $this->site->getSaleByID($document_id);
        $response_token = $this->Electronic_billing_model->delcopGetAuthorizationoken();

        if ($response_token->success == FALSE) {
            $response = [
                'response' => FALSE,
                'message' => "Los campos de Usuario o Contraseña DELCOP no son correctos. " . $response_token->error
            ];

            return (object) $response;
        }

        if ($this->Settings->fe_work_environment == TEST) {
            $url = "https://www-prueba.titanio.com.co/PDE/public/api/PDE/detalle";
        } else {
            $url = "https://www.titanio.com.co/PDE/public/api/PDE/detalle";
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => '{"token": "' . $response_token->token . '", "transaccion_id": "' . trim($sale->fe_id_transaccion) . '"}',
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/json"
            ),
        ));

        $response = curl_exec($curl);
        log_message('debug', 'DELCOP consulta de estado'. $response);

        $response = json_decode($response);

        curl_close($curl);

        if ($response->error_id == 0) {
            if ($response->transaccion_id != 0) {
                $document_status = $response->detalleTransaccion->estado_id;
                $acepted_DIAN = strstr($document_status, 'Validado por la DIAN');
                $rejected_DIAN = strstr($document_status, 'Rechazado por la DIAN');
                $sent_DIAN = strstr($document_status, 'Enviado a la DIAN');
                $validated_DELCOP = strstr($document_status, 'Validado por la DIAN');
                $transaction_received = strstr($document_status, 'Transacción recibida,');

                if ($rejected_DIAN != "") {
                    $this->site->updateSale(["fe_aceptado" => 1, "fe_mensaje" => "Transacción con errores detectados en la DIAN.", "fe_mensaje_soporte_tecnico" => $response->error], $sale->id);
                    $this->session->set_flashdata('error', $document_status);
                } else if ($acepted_DIAN != "") {
                    $this->site->updateSale(["fe_aceptado" => 2, "fe_mensaje" => "Documento aceptado por la DIAN", "fe_mensaje_soporte_tecnico" => ""], $sale->id);
                    $this->session->set_flashdata('message', $document_status);
                } else if ($sent_DIAN != "") {
                    $this->site->updateSale(["fe_aceptado" => 3, "fe_mensaje" => "Documento aceptado y enviado a la DIAN", "fe_mensaje_soporte_tecnico" => ""], $sale->id);
                    $this->session->set_flashdata('warning', $document_status);
                } else if ($validated_DELCOP != "") {
                    $this->site->updateSale(["fe_aceptado" => 3, "fe_mensaje" => "Documento aceptado y validado por DELCOP", "fe_mensaje_soporte_tecnico" => ""], $sale->id);
                    $this->session->set_flashdata('warning', $document_status);
                } else if ($transaction_received != "") {
                    if ($response->error != "") {
                        $this->site->updateSale(["fe_aceptado" => 1, "fe_mensaje" => "Transacción recibida por DELCOP con posibles errores.", "fe_mensaje_soporte_tecnico" => $response->error], $sale->id);
                        $this->session->set_flashdata('warning', $document_status);
                    } else {
                        $this->site->updateSale(["fe_aceptado" => 3, "fe_mensaje" => "Transacción recibida por DELCOP.", "fe_mensaje_soporte_tecnico" => "Proceso de validación DELCOP pendiente"], $sale->id);
                        $this->session->set_flashdata('error', $document_status);
                    }
                }

                admin_redirect('sales/fe_index');
            } else {
                $this->site->updateSale(["fe_aceptado" => 1, "fe_mensaje" => "No existe registro del Documento en DELCOP"], $sale->id);

                $this->session->set_flashdata('error', "No existe registro del Documento en DELCOP");
                admin_redirect('sales/fe_index');
            }
        } else {
            $this->site->updateSale(["fe_mensaje" => "Error al consumir: Estado del documentos en DELCOP"], $sale->id);

            $this->session->set_flashdata('error', $response->error_msg);
            admin_redirect('sales/fe_index');
        }
    }

    public function re_sale_suggestions()
    {
        $term = $this->input->get('term', true);
        $biller_id = $this->input->get('biller_id', true);
        $sales = $this->sales_model->get_sale_by_term($term);
        $sale_data = null;
        if ($sales) {
            foreach ($sales as $sale) {
                $inv_items = $this->sales_model->getAllInvoiceItems($sale->id);
                $c = rand(100000, 9999999);
                $pr = [];
                foreach ($inv_items as $item) {
                    $row = $this->site->getProductByID($item->product_id);
                    if (!$row) {
                        $row = json_decode('{}');
                        $row->tax_method = 0;
                    } else {
                        unset($row->cost, $row->details, $row->product_details, $row->image, $row->barcode_symbology, $row->cf1, $row->cf2, $row->cf3, $row->cf4, $row->cf5, $row->cf6, $row->supplier1price, $row->supplier2price, $row->cfsupplier3price, $row->supplier4price, $row->supplier5price, $row->supplier1, $row->supplier2, $row->supplier3, $row->supplier4, $row->supplier5, $row->supplier1_part_no, $row->supplier2_part_no, $row->supplier3_part_no, $row->supplier4_part_no, $row->supplier5_part_no);
                    }

                    $row->quantity = 0;
                    $pis = $this->site->getPurchasedItems($item->product_id, $item->warehouse_id, $item->option_id);

                    if ($pis) {
                        foreach ($pis as $pi) {
                            $row->quantity += $pi->quantity_balance;
                        }
                    }

                    $row->id = $item->product_id;
                    $row->sale_item_id = $item->id;
                    $row->code = $item->product_code;
                    $row->name = $item->product_name;
                    $row->type = $item->product_type;
                    $row->qty = $item->quantity;
                    $row->base_quantity = $item->quantity;
                    $row->base_unit = $row->unit ? $row->unit : $item->product_unit_id;
                    $row->unit = $item->product_unit_id;
                    $row->discount = $item->discount ? $item->discount : '0';
                    $row->price = $this->sma->formatDecimal($item->net_unit_price + $this->sma->formatDecimal($item->item_discount / $item->quantity));

                    $row->unit_price = $item->unit_price - ($row->tax_method ? $this->sma->formatDecimal($item->item_tax / $item->quantity) : 0);
                    $row->real_unit_price = $item->real_unit_price;
                    $row->base_unit_price = $row->unit_price;
                    $row->tax_rate = $item->tax_rate_id;
                    $row->serial = '';
                    $row->option = $item->option_id;
                    $options = $this->sales_model->getProductOptions($row->id, $item->warehouse_id);
                    $combo_items = false;
                    $row->price_before_tax = $item->price_before_tax;
                    // if ($row->price == 0 && $this->Settings->hide_products_in_zero_price && $row->ignore_hide_parameters != 1) {
                    //     continue;
                    // }

                    // if ($row->quantity <= 0 && $this->Settings->display_all_products == 0 && $row->ignore_hide_parameters != 1) {
                    //     continue;
                    // }

                    $tax_rate = $this->site->getTaxRateByID($row->tax_rate);
                    $row->item_tax = $item->item_tax / $item->quantity;
                    $row->net_unit_price = $item->net_unit_price;

                    $row->item_discount = $item->item_discount / $item->quantity;
                    $row->oqty = $item->quantity;

                    if ($row->type == 'combo') {
                        $combo_items = $this->sales_model->getProductComboItems($row->id, $item->warehouse_id);
                    }

                    $units = $this->site->get_product_units($row->id);
                    $ri = $this->Settings->item_addition ? $row->id : $c;

                    $pr[$ri] = array('id' => $ri, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'combo_items' => $combo_items, 'tax_rate' => $tax_rate, 'units' => $units, 'options' => $options);
                    $c++;
                }
                $sale_data[$sale->id]['id'] = $sale->id;
                $sale_data[$sale->id]['label'] = $sale->reference_no;
                $sale_data[$sale->id]['sale'] = $sale;
                $sale_data[$sale->id]['items'] = $pr;

                $sale_data[$sale->id]['resolution_data'] = $this->site->getDocumentTypeById($sale->document_type_id);
            }
        }
        if ($sale_data) {
            echo json_encode($sale_data);
        } else {
            echo json_encode([]);
        }
    }

    public function get_paid_opts()
    {
        $paid_by = $this->input->get('paid_by');
        echo $this->sma->paid_opts($paid_by);
    }

    public function change_status_accepted_DIAN($document_id)
    {
        $sale_data = $this->site->getSaleByID($document_id);

        $document_type = $this->site->getDocumentTypeById($sale_data->document_type_id);
        if ($document_type->module == FACTURA_DETAL) {
            $document_type_code = '01';
        } else if ($document_type->module == NOTA_CREDITO_DETAL) {
            $document_type_code = '91';
        } else if ($document_type->module == NOTA_DEBITO_DETAL) {
            $document_type_code = '92';
        }
        $prefix = strstr($sale_data->reference_no, '-', TRUE);
        $reference = str_replace('-', '', $sale_data->reference_no);
        $technology_provider_configuration = $this->site->getTechnologyProviderConfiguration($this->Settings->fe_technology_provider, $this->Settings->fe_work_environment);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'https://apivp.efacturacadena.com/v1/vp/consulta/documentos?nit_emisor=' . $this->Settings->numero_documento . '&id_documento=' . $reference . '&codigo_tipo_documento=' . $document_type_code . '&prefijo=' . $prefix,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => [
                'efacturaAuthorizationToken: d212df0e-8c61-4ed9-ae65-ec677da9a18c',
                'Content-Type: application/json',
                'Partnership-Id: 901090070'
            ],
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        log_message('debug', 'CADENA FE-Consulta documento: '. $response);

        $response = json_decode($response);

        if (!empty($response)) {
            if (isset($response->statusCode)) {
                if ($response->statusCode == 200) {
                    $updated_sale = $this->site->updateSale([
                        'fe_aceptado' => 2,
                        'fe_mensaje' => 'Documento aceptado por la DIAN',
                        'fe_mensaje_soporte_tecnico' => 'La Factura electrónica ' . $reference . ', ha sido autorizada. Código es estado ' . $response->statusCode,
                        'fe_xml' => $response->document
                    ], $document_id);

                    if ($updated_sale == TRUE) {
                        $customer = $this->sales_model->get_email_customer($sale_data->customer_id);
                        $email_send = $this->send_electronic_invoice_email($document_id, $customer->email, TRUE);

                        $this->session->set_flashdata('message', 'El cambio de estado del Documento fue realizado correctamente. ' . $email_send);
                        admin_redirect('sales/fe_index');
                    } else {
                        $this->session->set_flashdata('error', 'No fue posible cambiar el estado del Documento.');
                        admin_redirect('sales/fe_index');
                    }
                } else if ($response->statusCode == 400) {
                    $this->session->set_flashdata('error', 'No fue posible cambiar el estado del Documento. El prefijo no coincide con el prefijo del id del documento.');
                    admin_redirect('sales/fe_index');
                } else if ($response->statusCode == 404) {
                    $this->session->set_flashdata('error', 'No fue posible cambiar el estado del Documento. Registro no encontrado con los parámetros de búsqueda enviados.');
                    admin_redirect('sales/fe_index');
                }
            } else {
                $this->session->set_flashdata('error', 'Se ha perdido la conexión con el Web Services.');
                admin_redirect('sales/fe_index');
            }
        } else {
            $this->session->set_flashdata('error', 'No es posible realizar conexión con Web Services.');
            admin_redirect('sales/fe_index');
        }
    }

    public function search_product_price_suggestions()
    {
        $term = $this->input->get('term', true);
        $biller_id = $this->input->get('biller_id', true);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $rows = $this->sales_model->getProductNames($sr, NULL, $biller_id);
        if ($rows) {
            $r = 0;
            $pr = [];
            foreach ($rows as $row) {
                $product_unit_prices = NULL;
                $product_price_groups = NULL;
                $hybrid_prices = NULL;
                $hybrid_prices_data = NULL;
                if ($this->Settings->prioridad_precios_producto == 5 || $this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) {
                    $product_unit_prices = $this->products_model->get_product_unit_prices($row->id);
                    $unit_data = $this->site->getUnitByID($row->unit);
                    $punit['id'] = $row->unit;
                    $punit['name'] = $unit_data->name;
                    $punit['valor_unitario'] = $row->price;
                    $product_unit_prices[] = $punit;
                } else if ($this->Settings->prioridad_precios_producto == 11) {
                    $hybrid_prices = $this->site->get_all_product_hybrid_prices($row->id);
                    $hybrid_prices = $hybrid_prices[$row->id];
                    $hybrid_prices_data = [];
                    if ($hybrid_prices) {
                        foreach ($hybrid_prices as $unit_id => $price_groups) {
                            $unit_data = $this->site->getUnitByID($unit_id);
                            $hybrid_prices_data[$unit_id]['unit_name'] = $unit_data->name;
                            foreach ($price_groups as $price_group_id => $precio) {
                                $price_group_data = $this->site->getPriceGroupByID($price_group_id);
                                $hybrid_prices_data[$unit_id]['price_groups'][$price_group_id]['price_group_name'] = $price_group_data->name;
                                $hybrid_prices_data[$unit_id]['price_groups'][$price_group_id]['price_group_value'] = $precio;
                            }
                        }
                    }
                } else {
                    $product_price_groups = $this->products_model->getProductPriceGroups($row->id);
                }
                $pr[$row->id] = array('id' => $row->id, 'label' => ucfirst(mb_strtolower($row->name)), 'row' => $row, 'product_unit_prices' => $product_unit_prices, 'product_price_groups' => $product_price_groups, 'hybrid_prices' => $hybrid_prices_data);
                $r++;
            }
            if (count($pr) > 0) {
                $this->sma->send_json($pr);
            } else {
                $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
            }
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function email($id = null)
    {
        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getInvoiceByID($id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);
            $this->send_sale_mail(1, $id, $to, $subject);
        } elseif ($this->input->post('send_email')) {

            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['subject'] = array(
                'name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('invoice') . ' (' . $inv->reference_no . ') ' . lang('from') . ' ' . $this->Settings->site_name),
            );
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'sales/email', $this->data);
        }
    }

    public function order_sale_email($id = null)
    {
        $this->sma->checkPermissions(false, true);
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getOrderByID($id);
        $this->form_validation->set_rules('to', lang("to") . " " . lang("email"), 'trim|required|valid_email');
        $this->form_validation->set_rules('subject', lang("subject"), 'trim|required');
        $this->form_validation->set_rules('cc', lang("cc"), 'trim|valid_emails');
        $this->form_validation->set_rules('bcc', lang("bcc"), 'trim|valid_emails');
        if ($this->form_validation->run() == true) {
            if (!$this->session->userdata('view_right')) {
                $this->sma->view_rights($inv->created_by);
            }
            $to = $this->input->post('to');
            $subject = $this->input->post('subject');
            if ($this->input->post('cc')) {
                $cc = $this->input->post('cc');
            } else {
                $cc = null;
            }
            if ($this->input->post('bcc')) {
                $bcc = $this->input->post('bcc');
            } else {
                $bcc = null;
            }
            $customer = $this->site->getCompanyByID($inv->customer_id);
            $biller = $this->site->getCompanyByID($inv->biller_id);

            $this->send_sale_mail(2, $id, $to, $subject);
        } elseif ($this->input->post('send_email')) {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->session->set_flashdata('error', $this->data['error']);
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['subject'] = array(
                'name' => 'subject',
                'id' => 'subject',
                'type' => 'text',
                'value' => $this->form_validation->set_value('subject', lang('order_sale') . ' (' . $inv->reference_no . ') '),
            );
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->load_view($this->theme . 'orders/email', $this->data);
        }
    }

    public function send_sale_mail($type, $id, $to, $subject)
    {

        if ($type == 1) {
            $this->saleView($id, false, false, true);
            $inv = $this->sales_model->getInvoiceByID($id);
            $template = file_get_contents('./themes/default/admin/views/email_templates/sale.html');
        } else {
            $this->orderView($id, true);
            $inv = $this->sales_model->getOrderByID($id);
            $template = file_get_contents('./themes/default/admin/views/email_templates/order_sale.html');
        }

        $attachment =  FCPATH . 'files/' . $inv->reference_no . '.pdf';
        $this->load->library('parser');
        $customer = $this->site->getCompanyByID($inv->customer_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);
        $parse_data = array(
            'reference_number' => $inv->reference_no,
            'contact_person' => $customer->name,
            'company' => $customer->company && $customer->company != '-' ? '(' . $customer->company . ')' : '',
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $biller->logo . '" alt="' . ($biller->company != '-' ? $biller->company : $biller->name) . '"/>',
        );
        $email_content = $this->parser->parse_string($template, $parse_data);
        $parse_data = array(
            'email_title' => $subject,
            'email_content' => $email_content,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_template_background.php');
        $message = $this->parser->parse_string($msg, $parse_data);
        try {
            if ($this->sma->send_email($to, $subject, $message, null, null, $attachment)) {
                unlink($attachment);
                $this->session->set_flashdata('message', lang("email_sent"));
                admin_redirect("sales/orders");
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function possible_solution_currency()
    {
        $sale_id = $this->input->get("sale_id");

        $sale_data = $this->sales_model->get_sale_by_id($sale_id);

        if ($sale_data != FALSE) {
            if (empty((float) $sale_data->sale_currency_trm)) {
                $updated_sale = $this->site->updateSale(["sale_currency" => "COP"], $sale_id);
                if ($updated_sale == TRUE) {
                    echo json_encode([
                        "status" => TRUE,
                        "message" => "La moneda fue actualizada correctamente"
                    ]);
                } else {
                    echo json_encode([
                        "status" => FALSE,
                        "message" => "La moneda no fue actualizada"
                    ]);
                }
            } else {
                echo json_encode([
                    "status" => FALSE,
                    "message" => "Por favor comunicarse con el area de soporte técnico"
                ]);
            }
        } else {
            echo json_encode([
                "status" => FALSE,
                "message" => "No fue encontrado los datos de la factura"
            ]);
        }
    }

    public function invoice_date_existing_in_resolution_date_range()
    {
        $date = ($this->input->get('format_the_date') == 1) ? $this->sma->fld($this->input->get("date")) : $this->input->get("date");
        $document_type_id = $this->input->get("document_type_id");
        $document_type = $this->site->getDocumentTypeById($document_type_id);

        if ($this->site->invoice_date_existing_in_resolution_date_range($date, $document_type) == FALSE) {
            echo json_encode(FALSE);
        } else {
            echo json_encode(TRUE);
        }
    }

    public function order_delete_remaining_quantity($id = null)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getOrderByID($id);
        if ($inv->sale_status == 'cancelled') {
            $this->session->set_flashdata('error', lang('order_sale_cancelled'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        $this->form_validation->set_rules('submit', lang("submit"), 'trim|required');
        if ($this->form_validation->run() == true) {
            if ($inv->sale_status == 'partial') {
                $this->sales_model->order_delete_remaining_quantity($id);
                $this->session->set_flashdata('message', lang('completed'));
                redirect($_SERVER["HTTP_REFERER"]);
            } else {
                $this->session->set_flashdata('error', 'Este proceso sólo es válido para ordenes en estado parcial');
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['inv'] = $inv;
            $this->data['rows'] = $this->sales_model->getAllOrderSaleItems($id);
            $this->load_view($this->theme . 'orders/order_delete_remaining_quantity', $this->data);
        }
    }

    public function change_order_sale_status($id = null)
    {
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->getOrderByID($id);
        if ($inv->sale_status == 'cancelled') {
            $this->session->set_flashdata('error', lang('order_sale_cancelled'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'welcome');
        }
        $this->form_validation->set_rules('submit', lang("submit"), 'trim|required');
        if ($this->form_validation->run() == true) {
            $new_order_sale_status = $this->input->post('order_status');
            if ($inv->sale_status != 'cancelled') {
                if (
                    ($new_order_sale_status == 'enlistment' && $inv->sale_status == 'pending') ||
                    ($new_order_sale_status == 'pending' && $inv->sale_status == 'enlistment') ||
                    (
                        ($new_order_sale_status == 'sent' || $new_order_sale_status == 'delivered') &&
                        ($inv->sale_status == 'completed' || $inv->sale_status == 'sent' || $inv->sale_status == 'delivered')
                    ) ||
                    ($new_order_sale_status == 'completed' && ($inv->sale_status == 'sent' || $inv->sale_status == 'delivered'))
                ) {
                    $this->db->update('order_sales', ['sale_status' => $new_order_sale_status, 'last_update' => date('Y-m-d H:i:s')], ['id' => $id]);
                }
            }
            $this->session->set_flashdata('message', lang('completed'));
            redirect($_SERVER["HTTP_REFERER"]);
        } else {
            $this->data['customer'] = $this->site->getCompanyByID($inv->customer_id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['inv'] = $inv;
            $this->data['rows'] = $this->sales_model->getAllOrderSaleItems($id);
            $this->load_view($this->theme . 'orders/change_order_sale_status', $this->data);
        }
    }

    public function send_order_sale_email($id, $redirect = false)
    {
        $this->orderView($id, true);
        $inv = $this->sales_model->getOrderByID($id);
        $template = file_get_contents('./themes/default/admin/views/email_templates/staff_order_sale.html');
        $attachment =  FCPATH . 'files/' . $inv->reference_no . '.pdf';
        $this->load->library('parser');
        $customer = $this->site->getCompanyByID($inv->customer_id);
        $biller = $this->site->getCompanyByID($inv->biller_id);
        $parse_data = array(
            'reference_number' => $inv->reference_no,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url() . 'assets/uploads/logos/' . $biller->logo . '" alt="' . ($biller->company != '-' ? $biller->company : $biller->name) . '"/>',
        );
        $email_content = $this->parser->parse_string($template, $parse_data);
        $parse_data = array(
            'email_title' => $subject,
            'email_content' => $email_content,
            'site_link' => base_url(),
            'site_name' => $this->Settings->site_name,
            'logo' => '<img src="' . base_url('assets/uploads/logos/' . $this->Settings->logo) . '" alt="' . $this->Settings->site_name . '"/>'
        );
        $msg = file_get_contents('./themes/' . $this->Settings->theme . '/admin/views/email_templates/email_template_background.php');
        $message = $this->parser->parse_string($msg, $parse_data);

        $to = $this->Settings->email_new_order_sale_notification;
        $subject = "Nueva orden de pedido " . $inv->reference_no;
        try {
            if ($this->sma->send_email($to, $subject, $message, null, null, $attachment)) {
                unlink($attachment);
                $this->session->set_flashdata('message', lang("email_sent"));
                if ($redirect) {
                    admin_redirect("sales/orders");
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('error', $e->getMessage());
            if ($redirect) {
                redirect($_SERVER["HTTP_REFERER"]);
            }
        }
    }

    public function cancel_order_sale($id)
    {
        if ($this->sales_model->cancel_order_sale($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("order_sale_cancelled")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("order_sale_cancelled_error")));
        }
    }

    public function complete_os_wo_sale($id)
    {
        if ($this->sales_model->complete_os_wo_sale($id)) {
            $this->sma->send_json(array('error' => 0, 'msg' => lang("order_sale_completed_wo_sale")));
        } else {
            $this->sma->send_json(array('error' => 1, 'msg' => lang("order_sale_completed_wo_sale_error")));
        }
    }

    public function get_delivery_times()
    {
        $day = date('N', strtotime($this->input->get('day')));
        $day_date = $this->sma->fsd($this->input->get('day'));
        $address_id = $this->input->get('address_id');
        $address = $this->site->getAddressByID($address_id);
        if ($day) {
            $q = $this->db->get_where('delivery_time', ['day' => $day]);
            if ($q->num_rows() > 0) {
                foreach (($q->result()) as $row) {
                    $row->disabled = false;
                    $row->time_1 = date('h:i a', strtotime($row->time_1));
                    $row->time_2 = date('h:i a', strtotime($row->time_2));
                    if ($this->Settings->delivery_day_max_orders > 0 && !empty($address->location)) {
                        $exists = $this->db->select('*')
                            ->join('addresses', 'addresses.id = order_sales.address_id')
                            ->where('addresses.location', $address->location)
                            ->where('order_sales.delivery_time_id', $row->id)
                            ->where('order_sales.delivery_day', $day_date)
                            ->get('order_sales');
                        if ($exists->num_rows() >= $this->Settings->delivery_day_max_orders) {
                            $row->disabled = true;
                        }
                    }
                    $data[] = $row;
                }
                echo json_encode(['error' => 0, 'days' => $data]);
                exit();
            }
        }
        echo json_encode(['error' => 1, 'days' => false]);
        exit();
    }

    public function export_accountant_notes()
    {
        if ($this->Settings->modulary == 0) {
            $this->session->set_flashdata('warning', 'No puede usar el informe por que no existe modular con contabilidad');
            redirect($_SERVER["HTTP_REFERER"]);
        }
        $this->form_validation->set_rules('module', lang("module"), 'required');
        $this->form_validation->set_rules('export_date', lang("date"), 'required');
        $this->form_validation->set_rules('export_format', lang("export_format"), 'required');
        if ($this->form_validation->run() == true) {
            $module = $this->input->post('module');
            $date = $this->input->post('export_date');
            $export_format = $this->input->post('export_format');
            $data = [
                        'module' => $module,
                        'date' => $date,
                        'export_format' => $export_format,
                    ];
        }

        if ($this->form_validation->run() == true && $message = $this->site->export_accountant_notes($data)) {
            if ($message['error'] == 1) {
                $this->session->set_flashdata('error', $message['msg']);
                redirect($_SERVER["HTTP_REFERER"]);
            }
        } else {
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('sales'), 'page' => lang('sales')), array('link' => '#', 'page' => lang('export_accountant_notes')));
            $meta = array('page_title' => lang('export_accountant_notes'), 'bc' => $bc);
            $this->page_construct('sales/export_accountant_notes', $meta, $this->data);
        }
    }

    public function validate_export_accountant_notes()
    {
        $module = $this->input->get('module');
        $date = $this->input->get('export_date');
        $export_format = $this->input->get('export_format');
        $data = [
                    'module' => $module,
                    'date' => $date,
                    'expfort_format' => $export_format,
                ];
        $response = $this->site->export_accountant_notes_validations($data);
        if (!$response) {
            echo json_encode(['error'=>0]);
        } else {
            echo json_encode($response);
        }
    }

    public function cancel_gift_card($id = null)
    {
        $this->sma->checkPermissions(false, true);
        $this->form_validation->set_rules('card_no', lang("card_no"), 'trim|required');
        //$this->form_validation->set_rules('customer', lang("customer"), 'xss_clean');
        if ($this->form_validation->run() == true) {

            $biller = $this->input->post('biller');

            $data = [
                        'document_type_id' => $this->input->post('document_type_id'),
                        'biller' => $this->input->post('biller'),
                    ];

        } elseif ($this->input->post('cancel_gift_card')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect("sales/gift_cards");
        }

        if ($this->form_validation->run() == true && $this->sales_model->cancel_gift_card($id, $data)) {
            $this->session->set_flashdata('message', sprintf(lang("gift_card_cancelled"), lang('gift_card')));
            admin_redirect("sales/gift_cards");
        } else {
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['gift_card'] = $this->site->getGiftCardByID($id);
            $this->data['id'] = $id;
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['gift_card_movements'] = $this->sales_model->get_gift_card_movements($id);
            $this->load_view($this->theme . 'sales/cancel_gift_card', $this->data);
        }
    }

    public function view_gift_card_topup($id = NULL){
        if ($this->input->get('id')) {
            $id = $this->input->get('id');
        }
        $inv = $this->sales_model->get_gift_card_topup($id);
        $gc = $this->site->getGiftCardByID($inv->card_id);
        $this->data['inv'] = $inv;
        $this->data['gc'] = $gc;
        $this->data['modal'] = false;
        $this->data['biller'] = $this->pos_model->getCompanyByID($inv->biller_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($inv->biller_id);
        $this->data['customer'] = $this->pos_model->getCompanyByID($gc->customer_id);
        $this->data['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);
        $this->data['created_by'] = $this->site->getUser($gc->created_by);
        $this->data['document_type'] = $inv->document_type_id ? $this->site->getDocumentTypeById($inv->document_type_id) : false;
        if ($this->Settings->cost_center_selection != 2) {
            $this->data['cost_center'] = $this->site->getCostCenterByid($inv->cost_center_id);
        }
        $this->load_view($this->theme.'sales/view_gift_card_topup', $this->data);
    }

    public function getDocumentTypes()
    {
        $electronicDocument = ($this->input->post("electronicDocument")) ? $this->input->post("electronicDocument") : 0;
        $modules = $this->input->post("modules");
        $billerId = $this->input->post("billerId");
        $asynchronous = $this->input->post("asynchronous");
        $documentTypes = $this->site->getDocumentsTypeByBillerId($billerId, $modules, $electronicDocument);
        if ($asynchronous = 1) {
            echo json_encode($documentTypes);
        }
    }

    public function getMessage()
    {
        $documentId = $this->input->post('documentId');
        $document = $this->site->getSaleByID($documentId);

        $response = [
            'title' => $document->fe_mensaje,
            'text' => $document->fe_mensaje_soporte_tecnico
        ];

        echo json_encode($response);
    }

    public function download_xls($id = null)
    {
        $this->sma->checkPermissions('index');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $inv = $this->sales_model->getSaleByID($id);
        $customer = $this->site->getCompanyByID($inv->customer_id);
        $rows = $this->sales_model->getAllInvoiceItems($id);

        $this->load->library('excel');
        $this->excel->setActiveSheetIndex(0);
        $this->excel->getActiveSheet()->setTitle(lang('sales'));
        $this->excel->getActiveSheet()->SetCellValue('A1', lang('product_code'));
        $this->excel->getActiveSheet()->SetCellValue('B1', lang('reference'));
        $this->excel->getActiveSheet()->SetCellValue('C1', lang('product_name'));
        $this->excel->getActiveSheet()->SetCellValue('D1', lang('quantity'));
        $this->excel->getActiveSheet()->SetCellValue('E1', lang('price'));
        $nrow = 2;
        foreach ($rows as $row) {
            $this->excel->getActiveSheet()->SetCellValue('A' . $nrow, $row->product_code."");
            $this->excel->getActiveSheet()->SetCellValue('B' . $nrow, $row->product_reference."");
            $this->excel->getActiveSheet()->SetCellValue('C' . $nrow, $row->product_name);
            $this->excel->getActiveSheet()->SetCellValue('D' . $nrow, $row->quantity);
            $this->excel->getActiveSheet()->SetCellValue('E' . $nrow, $row->unit_price);
            $nrow++;
        }
        $this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
        $this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $range = 'A1:A'.$nrow;
        $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
        $range = 'B1:B'.$nrow;
        $this->excel->getActiveSheet()
                    ->getStyle($range)
                    ->getNumberFormat()
                    ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );
        $filename = str_replace("-", "", $inv->reference_no)." - ".$customer->vat_no." - ".str_replace(" ", "", $customer->company);
        $this->load->helper('excel');
        create_excel($this->excel, $filename);
    }

    public function checkStatusBPM($documentId)
    {
        $document = $this->sales_model->getSaleByID($documentId);
        $sentDocument = $this->Electronic_billing_model->getDocumentBPM($document);

        $this->sma->print_arrays($sentDocument);

        $this->session->set_flashdata('error', $response->error_msg);
        admin_redirect('sales/fe_index');
    }

    public function checkStatusSimba($documentId){
        $document = $this->sales_model->getSaleByID($documentId);
        $sentDocument = $this->Electronic_billing_model->getStatusSimba($document);
        if (!$sentDocument['error']) { 
            $this->session->set_flashdata('message', $sentDocument['message']);
        }else{
            $this->session->set_flashdata('warning', $sentDocument['message']);
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

    public function synchronizeFromEcommerce()
    {
        $response = [];
        if ($this->Settings->data_synchronization_to_store == ACTIVE) {
            $integrations = $this->settings_model->getAllIntegrations();
            if (!empty($integrations)) {
                foreach ($integrations as $integration) {
                    $this->integration = $integration;

                    $syncCreateOrders = $this->syncCreateOrders();
                    if (isset($syncCreateOrders->warning) && !empty($syncCreateOrders->warning)) {
                        $warningMessage .= $syncCreateOrders->warning;
                    }
                }

                if (!empty($warningMessage)) {
                    $response['warning'] = $warningMessage;
                }
            }
        }

        echo json_encode($response);
    }

    private function syncCreateOrders()
    {
        $_errorMessage = '';
        $_message = '';
        $response = [];

        $Order = new Order();
        $Order->open($this->integration);
        $orders = $Order->get(['sync'=>NOT]);

        if (!empty($orders)) {
            foreach ($orders as $order) {
                $customer = $this->syncAddCustomers($order);
                $biller = $this->getBillerData();
                $details = $this->getStoreOrderDetailsData($order, $biller);
                $totals = $this->getTotalData($details);

                $data = [
                    'date'              => $order->created_at,
                    'customer_id'       => $customer->id,
                    'customer'          => $customer->name,
                    'document_type_id'  => $biller->document_type_id,
                    'biller_id'         => $biller->id,
                    'biller'            => $biller->name,
                    'warehouse_id'      => $biller->default_warehouse_id,
                    'note'              => '',
                    'staff_note'        => $this->getStoreCombinedOrderData($order),
                    'total'             => $totals->totalBaseValue,
                    'total_discount'    => $order->coupon_discount,
                    'order_discount'    => $order->coupon_discount,
                    'product_tax'       => $totals->totalTaxValue,
                    'total_tax'         => $totals->totalTaxValue,
                    'shipping'          => $order->shipping_cost,
                    'grand_total'       => ($totals->totalBaseValue + $totals->totalTaxValue + $order->shipping_cost),
                    'sale_status'       => 'pending',
                    'created_by'        => $this->session->userdata('user_id'),
                    'total_items'       => count($details),
                    'note'              => $order->coupon_code,
                    'order_sale_origin' => 7,
                    'address_id'        => $this->getOrderAddress($order->shipping_address),
                    'seller_id'         => $biller->default_seller_id,
                    'payment_method'    => $this->getPaymentMethod($order),
                ];

                if (!empty($data) && !empty($details)) {
                    if ($orderId = $this->sales_model->addOrderSale($data, $details)) {
                        $Order->update(['sync' => YES, 'id_wappsi' => $orderId], $order->id);
                        $_message .= $this->site->getnameTypeIntegration($this->integration->type) . ", ";
                    } else {
                        $_errorMessage .= $this->site->getnameTypeIntegration($this->integration->type) . ", ";
                    }
                }
            }

            if (!empty($_message)) {
                $response["message"] = " Sincronizado en: ". rtrim($_message, ", ");
            }
            if (!empty($_errorMessage)) {
                $response["warning"] = " No fue posible sincronizar en: ". rtrim($_errorMessage, ", ");
            }

            $Order->close();
        }

        return (object) $response;
    }

    private function syncAddCustomers($order)
    {
        $userId = $order->user_id;
        $customerId = null;
        $User = new User();
        $User->open($this->integration);
        $storeUser = $User->find(['id' => $userId]);
        if (!empty($storeUser)) {
            if ($storeUser->sync == NOT) {
                $data = [
                    'group_id'            => 3,
                    'group_name'          => 'customer',
                    'type_person'         => NATURAL_PERSON,
                    'name'                => $storeUser->name,
                    'company'             => $storeUser->name,
                    'email'               => $storeUser->email,
                    'phone'               => $storeUser->phone,
                    'commercial_register' => 'XXX',
                    'commercial_register' => '13',
                    'city_code'           => '17005000',
                    'registration_date'   => $storeUser->phone,
                    'registration_date'   => date('Y-m-d H:i:s'),
                ];
                $data = $this->getStoreNameCustomer($data, $storeUser->name);
                $data = $this->getCustomerAddress($order, $data);

                if ($customerId = $this->companies_model->insertCustomer($data)) {
                    $User->update(['sync' => YES, 'id_wappsi' => $customerId], $storeUser->id);

                    $this->syncAddAdresses($storeUser, $customerId);
                }
            } else {
                $customerId = $storeUser->id_wappsi;
                $this->syncAddAdresses($storeUser, $customerId);
            }
        }
        $User->close();

        return (object) ['id' => $customerId, 'name' => $storeUser->name];
    }

    private function getCustomerAddress($order, $data)
    {
        $shippingAddress = $order->shipping_address;
        $shippingAddress = json_decode($shippingAddress);

        $data['address'] = $shippingAddress->address;
        $data['location'] = strtoupper($shippingAddress->city);
        $data['city'] = strtoupper($shippingAddress->city);
        $data['state'] = strtoupper($shippingAddress->state);
        $data['postal_code'] = $shippingAddress->postal_code;
        $data['country'] = strtoupper($shippingAddress->country);
        $data['phone'] = $shippingAddress->phone;

        return $data;
    }

    private function getStoreNameCustomer($data, $name)
    {
        $names = explode(' ',$name);
        if (count($names) == 1) {
            $data['first_name'] = $names[0];
        } else if (count($names) == 2) {
            $data['first_name'] = $names[0];
            $data['first_lastname'] = $names[1];
        } else if (count($names) == 3) {
            $data['first_name'] = $names[0];
            $data['first_lastname'] = $names[1];
            $data['second_lastname'] = $names[2];
        } else if (count($names) == 4) {
            $data['first_name'] = $names[0];
            $data['second_name'] = $names[1];
            $data['first_lastname'] = $names[2];
            $data['second_lastname'] = $names[3];
        }
        return $data;
    }

    private function syncAddAdresses($storeUser, $customerId)
    {
        $Address = new Address();
        $Address->open($this->integration);
        $storeAddresses = $Address->get(['user_id' => $storeUser->id]);
        if (!empty($storeAddresses)) {
            foreach ($storeAddresses as $storeAddress) {
                if ($storeAddress->sync == NOT) {
                    $data = [
                        'company_id'    => $customerId,
                        'direccion'     => $storeAddress->address,
                        'sucursal'      => "principal",
                        'country'       => $storeAddress->country,
                        'state'         => $storeAddress->state,
                        'city'          => $storeAddress->city,
                        'city_code'     => $this->getCodeCity($storeAddress->city),
                        'postal_code'   => $storeAddress->postal_code,
                        'phone'         => $storeAddress->phone,
                        'email'         => $storeUser->email,
                        'updated_at'    => date('Y-m-d H:i:s')
                    ];
                    if ($addressId = $this->companies_model->insertAddress($data)) {
                        $Address->update(['sync' => YES, 'id_wappsi' => $addressId], $storeAddress->id);
                    }
                }
            }
        }

        $Address->close();
    }

    private function getCodeCity($cityName)
    {
        $city = $this->companies_model->getCity(['DESCRIPCION' => $cityName]);
        return $city->CODIGO;
    }

    private function getStoreOrderDetailsData($order, $biller)
    {
        $data = [];
        $OrderDetail = new OrderDetail();
        $OrderDetail->open($this->integration);
        $orderDetails = $OrderDetail->get(['order_id' => $order->id]);

        if (!empty($orderDetails)) {
            $Product = new Product();
            $Product->open($this->integration);

            $ProductVariation = new ProductVariation();
            $ProductVariation->open($this->integration);

            foreach ($orderDetails as $detail) {
                $storeProduct = $Product->find(['id' => $detail->product_id], 'id, name, id_wappsi');
                $product = $this->products_model->getProductDetail($storeProduct->id_wappsi);
                $storeProductVariation = $ProductVariation->find(['id' => $detail->product_variation_id]);

                if (!empty($product)) {
                    $baseValue = $detail->price / (($product->rate / 100) + 1);
                    $taxValue = ($detail->price - $baseValue);
                    $data[] = [
                        'product_id'        => $product->id,
                        'product_code'      => $product->code,
                        'product_name'      => $storeProduct->name,
                        'product_type'      => $product->type,
                        'option_id'         => $storeProductVariation->id_wappsi,
                        'net_unit_price'    => $baseValue,
                        'unit_price'        => $detail->price,
                        'quantity'          => $detail->quantity,
                        'quantity_delivered'=> 0.0000,
                        'warehouse_id'      => $biller->default_warehouse_id,
                        'item_tax'          => ($taxValue * $detail->quantity),
                        'tax_rate_id'       => $product->tax_rate,
                        'tax'               => ((int) $product->rate . " %"),
                        'unit_quantity'     => $detail->quantity,
                        'subtotal'          =>  $detail->total,
                        'real_unit_price'   => $detail->price,
                        'product_unit_id'   => $product->sale_unit,
                        'product_unit_code' => $product->unitCode,
                        'price_before_tax'  => $baseValue,
                    ];
                }

            }
        }

        $ProductVariation->close();
        $OrderDetail->close();
        $Product->close();

        return $data;
    }

    private function getTotalData($details)
    {
        $totalBaseValue = 0;
        $totalTaxValue = 0;

        if (!empty($details)) {
            foreach ($details as $detail) {
                $detail = (object) $detail;

                $totalBaseValue += ($detail->net_unit_price * $detail->quantity);
                $totalTaxValue += $detail->item_tax;
            }
        }

        return (object) ['totalBaseValue' => $totalBaseValue, 'totalTaxValue' => $totalTaxValue];
    }

    private function getBillerData()
    {
        $billerDocumentType = '';
        $billerId = $this->integration->biller_id;
        if (!empty($billerId)) {
            $billerDocumentType = $this->companies_model->getBillerDocumentType(['biller_documents_types.biller_id' => $billerId, 'module' => 8]);
        }
        return $billerDocumentType;
    }

    private function getStoreCombinedOrderData($order)
    {
        $CombinedOrder = new CombinedOrder();
        $CombinedOrder->open($this->integration);
        $combinedOrder = $CombinedOrder->find(['id' => $order->combined_order_id]);
        $CombinedOrder->close();
        return $combinedOrder->code;
    }

    private function getOrderAddress($shippingAddress)
    {
        $shippingAddress = json_decode($shippingAddress);

        $Address = new Address();
        $Address->open($this->integration);
        $storeAddress = $Address->find(['id'=>$shippingAddress->id]);
        $Address->close();
        return $storeAddress->id_wappsi;
    }

    private function getPaymentMethod($order)
    {
        $manualPaymentData = json_decode($order->manual_payment_data);
        $StorePaymentMethod = $manualPaymentData->payment_method;
        $paymentMethod = $this->site->getPaymentMethodByName($StorePaymentMethod);

        return $paymentMethod->code;
    }

    public function change_date($sale_id)
    {
        $inv = $this->sales_model->getSaleByID($sale_id);
        if ($msg = $this->sales_model->update_date($inv)){
            $txt_date_changed = sprintf( lang('user_sale_date_changed'), ($this->session->first_name . " " . $this->session->last_name), $inv->reference_no, $inv->date, date('Y-m-d H:i:s') );
            $this->db->insert('user_activities', [
                'date' => date('Y-m-d H:i:s'),
                'type_id' => 1,
                'table_name' => 'sales',
                'record_id' => $sale_id,
                'user_id' => $this->session->userdata('user_id'),
                'module_name' => $this->m,
                'description' => $txt_date_changed,
            ]);

            if ($this->session->userdata('reaccount_error')) {
                $this->session->set_flashdata('error', $msg);
                $this->session->unset_userdata('reaccount_error');
            } else {
                $this->session->set_flashdata('message', $msg);
            }
            $this->resend_electronic_document($sale_id);
            redirect($_SERVER["HTTP_REFERER"]);
        }else{
            $this->session->set_flashdata('error', lang('error'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function change_payment_method($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('paid_by[]', lang('paid_by'), 'required');
        $inv = $this->pos_model->getInvoiceByID($id);
        if ($this->form_validation->run() == TRUE) {
            $data = [];
            $pm_commision_value = [];
            $pm_commision_value_info = [];
            $pm_commision_changed = [];
            $pm_retefuente_value = [];
            $pm_retefuente_value_info = [];
            $pm_retefuente_changed = [];
            $pm_reteiva_value = [];
            $pm_reteiva_value_info = [];
            $pm_reteiva_changed = [];
            $pm_reteica_value = [];
            $pm_reteica_value_info = [];
            $pm_reteica_changed = [];
            $payments_id = $_POST['payment_id'];
            foreach ($_POST['paid_by'] as $key => $paid_by) {
                $data[$payments_id[$key]] = $paid_by;
                $setted_retcom = $_POST['setted_retcom'][$payments_id[$key]];
                if ($setted_retcom == 1) {
                    $pm_commision_value[$payments_id[$key]] = $_POST['pm_commision_value'][$payments_id[$key]];
                    $pm_commision_value_info[$payments_id[$key]] = $_POST['pm_commision_value_info'][$payments_id[$key]];
                    $pm_commision_changed[$payments_id[$key]] = $_POST['pm_commision_changed'][$payments_id[$key]];
                    $pm_retefuente_value[$payments_id[$key]] = $_POST['pm_retefuente_value'][$payments_id[$key]];
                    $pm_retefuente_value_info[$payments_id[$key]] = $_POST['pm_retefuente_value_info'][$payments_id[$key]];
                    $pm_retefuente_changed[$payments_id[$key]] = $_POST['pm_retefuente_changed'][$payments_id[$key]];
                    $pm_reteiva_value[$payments_id[$key]] = $_POST['pm_reteiva_value'][$payments_id[$key]];
                    $pm_reteiva_value_info[$payments_id[$key]] = $_POST['pm_reteiva_value_info'][$payments_id[$key]];
                    $pm_reteiva_changed[$payments_id[$key]] = $_POST['pm_reteiva_changed'][$payments_id[$key]];
                    $pm_reteica_value[$payments_id[$key]] = $_POST['pm_reteica_value'][$payments_id[$key]];
                    $pm_reteica_value_info[$payments_id[$key]] = $_POST['pm_reteica_value_info'][$payments_id[$key]];
                    $pm_reteica_changed[$payments_id[$key]] = $_POST['pm_reteica_changed'][$payments_id[$key]];
                }
            }
            $ret_com = [
                            'pm_commision_value' => $pm_commision_value,
                            'pm_commision_value_info' => $pm_commision_value_info,
                            'pm_commision_changed' => $pm_commision_changed,
                            'pm_retefuente_value' => $pm_retefuente_value,
                            'pm_retefuente_value_info' => $pm_retefuente_value_info,
                            'pm_retefuente_changed' => $pm_retefuente_changed,
                            'pm_reteiva_value' => $pm_reteiva_value,
                            'pm_reteiva_value_info' => $pm_reteiva_value_info,
                            'pm_reteiva_changed' => $pm_reteiva_changed,
                            'pm_reteica_value' => $pm_reteica_value,
                            'pm_reteica_value_info' => $pm_reteica_value_info,
                            'pm_reteica_changed' => $pm_reteica_changed,
                        ];
        } else if ($this->input->post('change_payment_method')) {
            $this->session->set_flashdata('error', validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            admin_redirect("sales/index");
        }
        if ($this->form_validation->run() == TRUE && $this->pos_model->update_inv_payment_method($id, $data, $inv->customer_id, $ret_com)) {
            $msg = $this->sales_model->recontabilizarVenta($id);
            $this->site->syncSalePayments($id);
            if ($this->session->userdata('reaccount_error')) {
                $this->session->set_flashdata('error', lang("payment_method_updated").", ".$msg);
                $this->session->unset_userdata('reaccount_error');
            } else {
                $this->session->set_flashdata('message', lang("payment_method_updated").", ".$msg);
            }
            if ($inv->factura_electronica == '1') {
                admin_redirect("sales/fe_index");
            }else{
                admin_redirect("sales/index");
            }
        } else {
            $this->data['error'] = validation_errors() ? validation_errors() : $this->session->flashdata('error');
            $this->data['modal_js'] = $this->site->modal_js();
            $this->data['inv'] = $inv;
            $this->data['payments'] = $this->site->getSalePayments($id);
            $this->data['invoice_already_returned'] = false;
            if ($inv->return_sale_total < 0) {
                $this->session->set_flashdata('error', lang('invoice_already_returned'));
                $this->data['invoice_already_returned'] = true;
            }
            $this->load_view($this->theme . 'sales/change_payment_method', $this->data);
        }
    }

    public function printServer()
    {
        $data = [];
        $invoiceToPrint = '';

        if (!empty($this->session->userdata("sale_document_type_id"))) {
            $data[] = $this->session->userdata("sale_document_type_id");
        }
        if (!empty($this->session->userdata("document_type_id"))) {
            $data[] = $this->session->userdata("document_type_id");
        }

        $unprintedDocuments = $this->sales_model->selectUnprintedDocuments($data);
        
        foreach ($unprintedDocuments as $key => $document) {
            if (($document->factura_electronica == 1 && !empty($document->cufe)) || ($document->factura_electronica == 0)) {
                if ($document->printed == 0) {
                    $invoiceToPrint = $document->id;
                    break;
                }
            }
        }

        $this->data["invoiceToPrint"] = $invoiceToPrint;
        $this->data["unprinted_documents"] = $unprintedDocuments;

        $meta = ['page_title' => lang("print_server")];
        $this->page_construct('sales/print_server', $meta, $this->data);
    }
    
    public function get_unprinted_documents()
    {
        $data = [];
        $invoiceToPrint = '';

        if (!empty($this->session->userdata("sale_document_type_id"))) {
            $data[] = $this->session->userdata("sale_document_type_id");
        }
        if (!empty($this->session->userdata("document_type_id"))) {
            $data[] = $this->session->userdata("document_type_id");
        }

        $unprintedDocuments = $this->sales_model->selectUnprintedDocuments($data);
        foreach ($unprintedDocuments as $key => $document) {
            if (($document->factura_electronica == 1 && !empty($document->cufe)) || ($document->factura_electronica == 0)) {
                if ($document->printed == 0) {
                    $invoiceToPrint = $document->id;
                    break;
                }
            }
        }

        echo json_encode([
                "unprinted_documents" =>$unprintedDocuments,
                "invoiceToPrint" => $invoiceToPrint
            ]
        );
    }

    public function printFormat()
    {
        $document_id = $this->input->post('document_id');
        $inv = $this->pos_model->getInvoiceByID($document_id);
        $document_type = $this->site->getDocumentTypeById($inv->document_type_id);

        $url_format = "sales/genericViewHtml";
        $biller_id = $inv->biller_id;
        $customer_id = $inv->customer_id;
        $quickPrintFormatId = $document_type->quick_print_format_id;
        $tipo_regimen = $this->site->get_types_vat_regime($this->Settings->tipo_regimen);
        $print_directly = $this->pos_settings->auto_print == 1 && $this->pos_settings->remote_printing == 4 ? 1 : 0;
        $originalPaymentMethod = $this->getPayments($inv);
        
        $this->data['inv'] = $inv;
        $this->data['show_code'] = 1;
        $this->data['modal'] = FALSE;
        $this->data['biller_logo'] = 2;
        $this->data['tax_indicator'] = 0;
        $this->data['redirect_to_pos'] = FALSE;
        $this->data['product_detail_promo'] = 1;
        $this->data['show_product_preferences'] = 1;
        $this->data['print_directly'] = $print_directly;
        $this->data['pos'] = $this->pos_model->getSetting();
        $this->data['document_type_invoice_format'] = FALSE;
        $this->data['qty_decimals'] = $this->Settings->decimals;
        $this->data['page_title'] = $this->lang->line("invoice");
        $this->data["originalPaymentMethod"] = $originalPaymentMethod;
        $this->data['message'] = $this->session->flashdata('message');
        $this->data['value_decimals'] = $this->Settings->qty_decimals;
        $this->data['tipo_regimen'] = lang($tipo_regimen->description);
        $this->data['created_by'] = $this->site->getUser($inv->created_by);
        $this->data['seller'] = $this->site->getCompanyByID($inv->seller_id);
        $this->data['biller'] = $this->pos_model->getCompanyByID($biller_id);
        $this->data['address'] = $this->site->getAddressByID($inv->address_id);
        $this->data['rows'] = $this->sales_model->getAllInvoiceItems($document_id);
        $this->data['customer'] = $this->pos_model->getCompanyByID($customer_id);
        $this->data['payments'] = $this->sales_model->getPaymentsForSale($document_id);
        $this->data['warehouse'] = $this->site->getWarehouseByID($inv->warehouse_id);
        $this->data['document_type'] = $this->site->getDocumentTypeById($inv->document_type_id);
        $this->data['biller_data'] = $this->pos_model->get_biller_data_by_biller_id($biller_id);
        $this->data['printer'] = $this->pos_model->getPrinterByID($this->pos_settings->printer);
        $this->data['ciiu_code'] = $this->site->get_ciiu_code_by_id($this->Settings->ciiu_code);
        $this->data['invoice_footer'] = $this->site->getInvoiceFooter($inv->document_type_id, $inv->reference_no);
        $this->data['return_sale'] = $inv->return_id ? $this->pos_model->getInvoiceByID($inv->return_id) : NULL;
        $this->data['return_rows'] = $inv->return_id ? $this->pos_model->getAllInvoiceItems($inv->return_id) : NULL;
        $this->data['return_payments'] = $this->data['return_sale'] ? $this->pos_model->getInvoicePayments($this->data['return_sale']->id) : NULL;

        $document_type_invoice_format = $this->site->getInvoiceFormatById($document_type->quick_print_format_id);

        if ($document_type_invoice_format) {
            $this->data['document_type_invoice_format'] = $document_type_invoice_format;
            $this->data['qty_decimals'] = $document_type_invoice_format->qty_decimals;
            $this->data['value_decimals'] = $document_type_invoice_format->value_decimals;
            $this->data['biller_logo'] = $document_type_invoice_format->logo;
            $this->data['tax_indicator'] = $document_type_invoice_format->tax_indicator;
            $this->data['product_detail_promo'] = $document_type_invoice_format->product_detail_promo;
            $this->data['show_code'] = $document_type_invoice_format->product_show_code;
            $this->data['show_product_preferences'] = $document_type_invoice_format->show_product_preferences;
            $view_tax = $document_type_invoice_format->view_item_tax ? TRUE : FALSE;
            $tax_inc = $document_type_invoice_format->tax_inc ? TRUE : FALSE;
            $url_format = $document_type_invoice_format->format_url;
        }

        $totalItems = 0;
        foreach ($this->data['rows'] as $items) {
            $totalItems += $items->quantity;
        }
        $this->generateQr($inv);
        

        if ($inv->total_items == ceil($totalItems)) {
            $html = $this->load_view($this->theme.$url_format, $this->data, TRUE);
            
            $this->pos_model->update_print_status($document_id);
        }
        // $this->load_view($this->theme.$url_format, $this->data, TRUE);

        echo $html;
    }

    function generateQr($inv) {
        $this->load->library('qr_code');

        $dir = "themes/default/admin/assets/images/qr_code/";
        $filename = "$dir$inv->reference_no.png";
        if (!file_exists($dir)) {
            mkdir($dir, 0777);
        }

        QRcode::png($inv->codigo_qr, $filename, 'L', 3, 0);
    }

    function getPayments($inv) {
        $payments = $this->sales_model->getPaymentsForSale($inv->id);
        $original_payment_method = '';
        $original_payment_method_with_amount = '';
        $total_original_payments_amount = 0;
        $direct_payments_num = 0;

        if ($payments) {
            foreach ($payments as $payment) {
                if ($payment->paid_by == "retencion") {
                    $total_original_payments_amount += $payment->amount;
                }
                if (date('Y-m-d H', strtotime($payment->date)) == date('Y-m-d H', strtotime($inv->date)) && $payment->paid_by != "retencion") {
                    if ($original_payment_method == '') {
                        $original_payment_method = lang($payment->paid_by) . ", ";
                    } else {
                        $original_payment_method .= lang($payment->paid_by) . ", ";
                    }
                    if ($original_payment_method_with_amount == '') {
                        $original_payment_method_with_amount = lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    } else {
                        $original_payment_method_with_amount .= lang($payment->paid_by) . " (" . $this->sma->formatMoney($payment->amount) . "), ";
                    }
                    $direct_payments_num++;
                    $total_original_payments_amount += $payment->amount;
                }
            }
            if ($direct_payments_num > 1) {
                $inv->note .= $original_payment_method_with_amount;
                $original_payment_method = 'Varios, ver nota';
            } else {
            }
        }
        if ($original_payment_method != '') {
            $original_payment_method = trim($original_payment_method, ", ");
        }
        if ($original_payment_method == '' || ($total_original_payments_amount < $inv->grand_total)) {
            $original_payment_method .= ($original_payment_method != '' ? ", " : "") . lang('due') . " " . $inv->payment_term . ($inv->payment_term > 1 ? " Días" : " Día");
        }

        return $original_payment_method;
    }

    public function markPrintedAjax() {
        $documentId = $this->input->post('documentId');
        $documentsUpdate = $this->pos_model->update_print_status($documentId);

        echo json_encode($documentsUpdate);
    }

    public function phpinfoE(){
        echo phpinfo();
    }

    public function getAffiliates()
    {
        if ($this->input->get('biller_id')) {
            $biller_id = $this->input->get('biller_id');
            $withvatno = $this->input->get('withvatno');
            $sellers = $this->site->getAffiliateByBiller($biller_id);
            $options = '';
            if ($sellers != FALSE && count($sellers) > 1) {
                $options = "<option value=''>" . lang('select') . "</option>";
            }
            if ($sellers != FALSE) {
                foreach ($sellers as $row => $seller) {
                    $options .= "<option value='" . $seller['companies_id'] . "'>" . ($withvatno ? $seller['vat_no'] . " - " : "") . ucwords(mb_strtolower($seller['name'])) . "</option>";
                }
            } else if ($sellers == FALSE) {
                $options = "<option value=''>" . lang('no_data_available') . "</option>";
            }
            echo $options;
        } else {
            echo FALSE;
        }
    }

    public function create_recurring_orders(){
        if (!$this->Settings->osdt_recurring_sales > 0) {
            $this->session->set_flashdata('error', lang('invalid_osdt'));
            redirect($_SERVER["HTTP_REFERER"]);
        }
        if ($this->sales_model->create_recurring_orders()) {
            $this->session->set_flashdata('message', lang('recurring_sale_orders_created'));
        } else {
            $this->session->set_flashdata('warning', lang('recurring_sale_orders_error'));
        }
        redirect($_SERVER["HTTP_REFERER"]);
    }

}
