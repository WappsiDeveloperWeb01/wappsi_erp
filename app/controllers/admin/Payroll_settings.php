<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_settings extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableElectronicPayroll) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->lang->admin_load('payroll', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('Payroll_settings_model');
        $this->load->admin_model('Payroll_management_model');
        $this->load->admin_model("UserActivities_model");
	}

	public function index()
	{
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $payroll_settings = $this->Payroll_settings_model->get();

        $approved_payroll = $this->Payroll_management_model->get_by_status(APPROVED);
        $payroll_in_preparation = $this->Payroll_management_model->get_by_status(IN_PREPARATION);

        $this->data["payroll_settings"] = $payroll_settings;
        $this->data["arls"]=$this->Payroll_settings_model->get_arl();
        $this->data["ccfs"]=$this->Payroll_settings_model->get_ccf();
        $this->data["banks"]=$this->Payroll_settings_model->get_bank();

		if ($this->Owner) {
			$skipApprovedPayroll = 0;
		} else {
			if ($approved_payroll === FALSE) {
				$skipApprovedPayroll = 0;
			} else {
				$skipApprovedPayroll = count((array) $approved_payroll);
			}
		}

        $this->data['approved_payroll_number'] = $skipApprovedPayroll;
        $this->data['payroll_in_preparation_number'] = ($payroll_in_preparation == FALSE) ? FALSE : TRUE;
        $this->data["ss_operators"]=$this->Payroll_settings_model->get_ss_operators();
        $this->data["payment_frequencies"]=$this->Payroll_settings_model->get_payment_frequency();
        $this->data['options'] = $this->getFrequencyOptions($payroll_settings);

        $this->page_construct('payroll_settings/index', ["page_title"=>lang("payroll_settings")], $this->data);
	}

    private function getFrequencyOptions($payroll_settings)
    {
        $options = [];
        if (!empty($payroll_settings)) {
        	if ($payroll_settings->payment_frequency == BIWEEKLY) {
	        	$options = [
        			BIWEEKLY => 'Cada quincena',
        			MONTHLY => 'En la última quincena'
        		];
	        } else if ($payroll_settings->payment_frequency == MONTHLY) {
	        	$options = [MONTHLY => 'Mensual'];
	        }
        }
        return $options;
    }

	public function save()
	{
        $this->sma->checkPermissions('add', NULL, 'payroll_management');

		$this->form_validation->set_rules("work_environment", lang("work_environment"), "trim|required");
		$this->form_validation->set_rules("payment_frequency", lang("payment_frequency"), "trim|required");
		$this->form_validation->set_rules("payment_schedule", lang("payment_schedule"), "trim|required");
		$this->form_validation->set_rules("enable_saturday_vacation", lang("parroll_enable_saturday_vacation"), "trim|greater_than_equal_to[0]");
		$this->form_validation->set_rules("weekly_working_hours", lang("weekly_working_hours"), "trim|required");
		$this->form_validation->set_rules("minimum_salary_value", lang("minimum_salary_value"), "trim|required|numeric");
		$this->form_validation->set_rules("integral_salary_value", lang("integral_salary_value"), "trim|required|numeric");
		$this->form_validation->set_rules("transportation_allowance_value", lang("transportation_allowance_value"), "trim|required|numeric");
		$this->form_validation->set_rules("withholding_base_value", lang("withholding_base_value"), "trim|required|numeric");
		$this->form_validation->set_rules("prima_payment", lang("prima_payment"), "trim|required");
		$this->form_validation->set_rules("bonus_payment", lang("bonus_payment"), "trim|required");
		$this->form_validation->set_rules("arl", lang("arl"), "trim|required");
		$this->form_validation->set_rules("ccf", lang("ccf"), "trim|required");
		$this->form_validation->set_rules("exempt_parafiscal_health_payments", lang("exempt_parafiscal_health_payments"), "trim|greater_than_equal_to[0]");
		$this->form_validation->set_rules("small_business", lang("small_business"), "trim|greater_than_equal_to[0]");
		$this->form_validation->set_rules("exempt_concepts", lang("exempt_concepts"), "trim|greater_than_equal_to[0]");
		$this->form_validation->set_rules("illness_2days", lang("illness_2days"), "trim|required|numeric");
		$this->form_validation->set_rules("illness_90days", lang("illness_90days"), "trim|required|numeric");
		$this->form_validation->set_rules("illness_91", lang("illness_91"), "trim|required|numeric");
		$this->form_validation->set_rules("bank_id", lang("bank_id"), "trim|required");
		$this->form_validation->set_rules("bank_account_number", lang("bank_account_number"), "trim|required|numeric");
		$this->form_validation->set_rules("bank_account_type", lang("bank_account_type"), "trim|required");
		$this->form_validation->set_rules("file_format", lang("file_format"), "trim|required");
		$this->form_validation->set_rules("ss_operators", lang("ss_operators"), "trim|required");
		$this->form_validation->set_rules("UVT_value", lang("UVT_value"), "trim|required");
		$this->form_validation->set_rules("compensation_fund_percentage", lang("compensation_fund_percentage"), "trim|required");
		$this->form_validation->set_rules("pension_percentage", lang("pension_percentage"), "trim|required");
		$this->form_validation->set_rules("high_risk_pension_percentage", lang("high_risk_pension_percentage"), "trim|required");
        $this->form_validation->set_rules('percentage_salary_apprentice_teaching', lang('percentage_salary_apprentice_teaching'), 'trim|required|greater_than_equal_to[50]');
        $this->form_validation->set_rules('percentage_salary_apprentice_productive', lang('percentage_salary_apprentice_productive'), 'trim|required|greater_than_equal_to[75]');
        $this->form_validation->set_rules('layoffs_interests', lang('layoffs_interests'), 'trim|required');
        $this->form_validation->set_rules('provision_options', lang('provision_options'), 'trim|required');

        if ($this->Owner) {
            $this->form_validation->set_rules('payroll_start_date', lang('payroll_start_date'), 'trim|required');
        }

		if ($this->form_validation->run() == TRUE) {
			$data = [
				"work_environment"=>$this->input->post("work_environment"),
				"payment_frequency"=>$this->input->post("payment_frequency"),
				"payment_schedule"=>$this->input->post("payment_schedule"),
				"weekly_working_hours"=>$this->input->post("weekly_working_hours"),
				"enable_saturday_vacation"=>$this->input->post("enable_saturday_vacation"),
				"prima_payment"=>$this->input->post("prima_payment"),
				"bonus_payment"=>$this->input->post("bonus_payment"),
				"arl"=>$this->input->post("arl"),
				"ccf"=>$this->input->post("ccf"),
				"compensation_fund_percentage" => $this->input->post("compensation_fund_percentage"),
				"pension_percentage" => $this->input->post("pension_percentage"),
				"high_risk_pension_percentage" => $this->input->post("high_risk_pension_percentage"),
				"exempt_parafiscal_health_payments"=>$this->input->post("exempt_parafiscal_health_payments"),
				"small_business"=>$this->input->post("small_business"),
				"exempt_concepts"=>$this->input->post("exempt_concepts"),
				"illness_2days"=>$this->input->post("illness_2days"),
				"illness_90days"=>$this->input->post("illness_90days"),
				"illness_91"=>$this->input->post("illness_91"),
				"bank_id"=>$this->input->post("bank_id"),
				"bank_account_number"=>$this->input->post("bank_account_number"),
				"bank_account_type"=>$this->input->post("bank_account_type"),
				"file_format"=>$this->input->post("file_format"),
				"ss_operators"=>$this->input->post("ss_operators"),
				"UVT_value"=>$this->input->post("UVT_value"),
				"set_test_id"=>$this->input->post("set_test_id"),
                "bonus_limit_percentage"=>$this->input->post("bonus_limit_percentage"),
                'percentage_salary_apprentice_teaching' => $this->input->post('percentage_salary_apprentice_teaching'),
                'percentage_salary_apprentice_productive' => $this->input->post('percentage_salary_apprentice_productive'),
                "sunday_discount_for_absenteeism" => $this->input->post('sunday_discount_for_absenteeism'),
                "layoffs_interests" => $this->input->post('layoffs_interests'),
                "provision_options" => $this->input->post('provision_options'),
			];

            if ($this->Owner) {
                $data["payroll_start_date"] = $this->input->post("payroll_start_date");
            }

			$payroll_in_preparation = $this->Payroll_management_model->get_by_status(IN_PREPARATION);
			if ($payroll_in_preparation == FALSE) {
				$data["minimum_salary_value"] = $this->input->post("minimum_salary_value");
				$data["integral_salary_value"] = $this->input->post("integral_salary_value");
				$data["transportation_allowance_value"] = $this->input->post("transportation_allowance_value");
				$data["withholding_base_value"] = $this->input->post("withholding_base_value");
			} else if (empty($payroll_in_preparation)) {
				$data["minimum_salary_value"] = $this->input->post("minimum_salary_value");
				$data["integral_salary_value"] = $this->input->post("integral_salary_value");
				$data["transportation_allowance_value"] = $this->input->post("transportation_allowance_value");
				$data["withholding_base_value"] = $this->input->post("withholding_base_value");
			}

			// $this->sma->print_arrays($data);

            $payroll_settings_data = $this->Payroll_settings_model->get();
			if ($this->input->post("payroll_settings_id")) {
				$payroll_settings_saved = $this->Payroll_settings_model->update_payroll_settings($data, $this->input->post("payroll_settings_id"));

				$this->check_settings_changes($payroll_settings_data);
			} else {
				$payroll_settings_saved = $this->Payroll_settings_model->insert_payroll_settings($data);
			}

			if ($payroll_settings_saved == TRUE) {
                $this->log_user_activity($payroll_settings_data);

				$this->session->set_flashdata("message", lang("payroll_settings_saved"));
				admin_redirect("payroll_settings");
			} else {
				$this->session->set_flashdata("post", (object) $this->input->post());
				$this->session->set_flashdata("error", lang("payroll_settings_not_saved"));
				admin_redirect($_SERVER["HTTP_REFERER"]);
			}
		} else {
			$this->session->set_flashdata("post", (object) $this->input->post());
			$this->session->set_flashdata("error", validation_errors());
			admin_redirect($_SERVER["HTTP_REFERER"]);
		}
	}

	private function check_settings_changes(stdClass $payroll_settings_data)
	{
		$changes_detected = 0;
		$updated_payroll_settings_data = $this->Payroll_settings_model->get();

		foreach ($updated_payroll_settings_data as $field_name => $field_data) {
			if ($field_name == "pension_percentage" || $field_name == "high_risk_pension_percentage" || $field_name == "compensation_fund_percentage") {
				if ($field_data != $payroll_settings_data->{$field_name}) {
					$changes_detected++;
				}
			}
        }

		if ($changes_detected > 0) {
			$this->session->set_userdata('existing_configuration_data_changes', TRUE);
		}
	}

	public function validate_existing_current_payroll_ajax()
	{
		$payroll_in_preparation = $this->Payroll_management_model->get_by_status(IN_PREPARATION);

		if ($payroll_in_preparation == FALSE) {
			echo json_encode(FALSE);
			exit();
		}

		if (count($payroll_in_preparation) > 0) {
			$id = $payroll_in_preparation->id;
			echo json_encode($id);
		} else {
			echo json_encode(FALSE);
		}
	}

	public function log_user_activity($payroll_settings_data)
	{
        $updated_payroll_settings_data = $this->Payroll_settings_model->get();

        $diff = array_diff_assoc ((array) $payroll_settings_data, (array) $updated_payroll_settings_data);
        if (!empty($diff)) {
            $changes_detected = 0;
            $description_changes_detected = '';
            $arls = $this->Payroll_settings_model->get_arl();
            $ccfs = $this->Payroll_settings_model->get_ccf();
            $banks = $this->Payroll_settings_model->get_bank();
            $boolean_options = [NOT=>lang("no"), YES=>lang("yes")];
            $ss_operators = $this->Payroll_settings_model->get_ss_operators();
            $work_environments = [PRODUCTION=>lang("production"), TEST=>lang("tests")];
            $files_format = [""=>lang("select"), 1=>lang("SAP"), 2=>lang("PAB")];
            $payment_frequencies = $this->Payroll_settings_model->get_payment_frequency();
            $prima_payment = [1=>lang("payroll_prima_payment_option_1"), 2=>lang("payroll_prima_payment_option_2")];
            $frequency_options = $this->Payroll_settings_model->get_frequency_options($payroll_settings_data->payment_frequency);
            $account_types = [1=>lang("payroll_savings_account"), 2=>lang("payroll_current_account"), 3=>lang("payroll_account_virtual_wallet")];
            $updated_frequency_options = $this->Payroll_settings_model->get_frequency_options($updated_payroll_settings_data->payment_frequency);
            $description = 'El usuario '. $this->session->userdata('first_name') .' '. $this->session->userdata('last_name') .' hizo lo siguiente cambios en parámetros generales de nómina: ';

            foreach ($diff as $field_name => $field_data) {
                    if ($field_name == 'work_environment') {
                        $data_from = $work_environments[$field_data];
                        $data_to = $work_environments[$updated_payroll_settings_data->$field_name];
                    } else if ($field_name == 'payment_frequency') {
                        foreach ($payment_frequencies as $payment_frequency) {
                            if ($payment_frequency->id == $field_data) { $data_from = $payment_frequency->period; }
                            if ($payment_frequency->id == $updated_payroll_settings_data->$field_name) { $data_to = $payment_frequency->period; }
                        }
                    } else if ($field_name == 'bonus_payment') {
                        $data_to = $frequency_options[$payroll_settings_data->$field_name];
                        $data_from = $updated_frequency_options[$updated_payroll_settings_data->field_name];
                    } else if ($field_name == 'enable_saturday_vacation' || $field_name == 'exempt_parafiscal_health_payments' || $field_name == 'small_business'){
                        $data_from = $boolean_options[$field_data];
                        $data_to = $boolean_options[$updated_payroll_settings_data->$field_name];
                    } else if ($field_name == 'prima_payment'){
                        $data_from = $prima_payment[$field_data];
                        $data_to = $prima_payment[$updated_payroll_settings_data->$field_name];
                    } else if ($field_name == 'arl') {
                        foreach ($arls as $arl) {
                            if ($arl->id == $field_data) { $data_from = $arl->name; }
                            if ($arl->id == $payroll_settings_data->$field_name) { $data_to = $arl->name; }
                        }
                    } else if ($field_name == 'ccf') {
                        foreach ($ccfs as $ccf) {
                            if ($ccf->id == $field_data) { $data_from = $ccf->name; }
                            if ($ccf->id == $updated_payroll_settings_data->$field_name) { $data_to = $ccf->name; }
                        }
                    } else if ($field_name == 'bank_id') {
                        foreach ($banks as $bank) {
                            if ($bank->id == $field_data) { $data_from = $bank->name; }
                            if ($bank->id == $updated_payroll_settings_data->$field_name) { $data_to = $bank->name; }
                        }
                    } else if ($field_name == 'bank_account_type') {
                        $data_from = $account_types[$field_data];
                        $data_to = $account_types[$updated_payroll_settings_data->$field_name];
                    } else if ($field_name == 'file_format') {
                        $data_from = $files_format[$field_data];
                        $data_to = $files_format[$updated_payroll_settings_data->$field_name];
                    } else if ($field_name == 'ss_operators') {
                        foreach ($ss_operators as $operator) {
                            if ($operator->id == $field_data) { $data_from = $operator->name; }
                            if ($operator->id == $updated_payroll_settings_data->$field_name) { $data_from = $operator->name; }
                        }
                    } else {
                        $data_from = $field_data;
                        $data_to = $updated_payroll_settings_data->$field_name;
                    }

                    $description_changes_detected .= lang($field_name). ' de "'. $data_from .'" a "'. $data_to. '", ';
                    $changes_detected++;
            }

            if (!empty($changes_detected)) {
                $log_user_data = [
                    'date'=>date('Y-m-d H:m:i'),
                    'type_id'=>EDITION,
                    'user_id'=>$this->session->userdata('user_id'),
                    'module_name'=>'payroll',
                    'table_name'=>'payroll_settings',
                    'record_id'=> $payroll_settings_data->id,
                    'description'=>trim($description . $description_changes_detected, ', ')
                ];
                $this->UserActivities_model->insert($log_user_data);
            }
        }
	}
}

/* End of file Payroll_settings.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/controllers/admin/Payroll.php */