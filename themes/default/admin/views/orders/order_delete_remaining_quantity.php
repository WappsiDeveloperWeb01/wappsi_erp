<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog  modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('delete_remaining_quantity'); ?></h4>
            <br>
            <span>Esto proceso reducirá las cantidades igualándolas a las ya facturadas y cambiará el estado de la orden a "Facturado", los productos que no tienen ninguna cantidad facturada, se eliminarán de la orden</span>
        </div>
        <?= admin_form_open("sales/order_delete_remaining_quantity/" . $id); ?>
        <div class="modal-body">
            <p>
                <b><?= lang('reference_no') ?></b> : <?= $inv->reference_no ?> 
                <b><?= lang('status') ?></b> : <?= lang($inv->sale_status) ?>
            </p>
            <table class="table">
                <tr>
                    <th><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                    <th><?= lang("quantity"); ?></th>
                    <th><?= lang("quantity_dispatch"); ?></th>
                    <th><?= lang("quantity_dispatch_pending"); ?></th>
                </tr>
                <?php foreach ($rows as $row): ?>
                    <?php if (($row->quantity - $row->quantity_delivered) > 0): ?>
                        <tr>
                            <td><?= $row->product_code.' - '.$row->product_name . ($row->variant ? ' (' . $row->variant . ')' : ''); ?></td>
                            <td class="text-right"><?= $this->sma->formatQuantity($row->quantity).' '.$row->product_unit_code; ?></td>
                            <td class="text-right"><?= $this->sma->formatQuantity($row->quantity_delivered).' '.$row->product_unit_code; ?></td>
                            <td class="text-right"><?= $this->sma->formatQuantity($row->quantity - $row->quantity_delivered).' '.$row->product_unit_code; ?></td>
                        </tr>
                    <?php endif ?>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="modal-footer">
            <?= form_submit('submit', lang('send'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
<?= $modal_js ?>
