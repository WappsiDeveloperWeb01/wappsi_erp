<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>

<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2>Ordenes de preparación</h2>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table col-lg-12 table-hover table-condensed" id="preparation_order_table">
                                <thead>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Area de preparación</th>
                                    <th>Mesa</th>
                                    <th>Producto</th>
                                    <th>Estado</th>
                                </thead>
                                <tbody>
                                    <?php foreach ($preparation_order as $key => $order) { ?>
                                        <?php
                                            if ($order->state_readiness == ORDERED) { $state_readiness = 'label-success'; }
                                            else if ($order->state_readiness == PREPARATION) { $state_readiness = 'label-warning'; }
                                            else if ($order->state_readiness == CANCELLED) { $state_readiness = 'label-danger'; }
                                            else if ($order->state_readiness == DISPATCHED) { $state_readiness = 'label-primary'; }
                                        ?>
                                        <tr>
                                            <td><?= $order->date; ?></td>
                                            <td><?= $order->time; ?></td>
                                            <td><?= $order->nombre; ?></td>
                                            <td><?= $order->tipo. " ". $order->numero; ?></td>
                                            <td><?= $order->product_name; ?></td>
                                            <td class="text-center">
                                                <span class="payment_status pending_errors label <?= $state_readiness; ?>">Pendiente</span>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <th>Fecha</th>
                                    <th>Hora</th>
                                    <th>Area de preparación</th>
                                    <th>Mesa</th>
                                    <th>Producto</th>
                                    <th>Estado</th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('#preparation_order_table').DataTable();
    });

</script>
