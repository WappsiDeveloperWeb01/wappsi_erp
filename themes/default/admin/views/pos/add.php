<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<?php $this->session->unset_userdata('redirect_pos') ?>
<?php
if ($this->session->userdata('pos_post_processing')) {
    $this->session->unset_userdata('pos_post_processing');
}
 ?>
 <script type="text/javascript">

   <?php
       $showDocTypeInput = TRUE;
       if ($this->pos_settings->express_payment == 0 && $this->pos_settings->activate_electronic_pos == YES && $this->pos_settings->mode_electronic_pos == 2) {
           $showDocTypeInput = FALSE;
       }
   ?>
   var showDocTypeInput = "<?= $showDocTypeInput ?>";
    var user_seller_id = "<?= $this->session->userdata('company_id') ? $this->session->userdata('company_id') : $this->session->userdata('seller_id') ?>";
	var remove_posls = false;
	var is_quote_paid_by = false;
	<?php if ($this->session->userdata('remove_posls')) { ?>
		remove_posls = true;
	<?php $this->sma->unset_data('remove_posls'); ?>
	<?php } ?>
 </script>
	<style type="text/css">
		.line-through {
		    text-decoration: line-through;
		}
		.txt-error {
			color: #ed3439;
		}
		.row_mobile{
			flex-direction: column;
		}
		.input_mobile{
			width : 100% !important;
		}
		input.btn-pos-product_movil#add_item {
			border-bottom : 2px solid #1484c6 !important;
			border-top : 2px solid #1484c6 !important;
			border-left : 4px solid #1484c6 !important;
			border-right : 4px solid #1484c6 !important;
			border-radius : 30px !important;
		}
		.div_hidden_movile{
			display : none;
		}
		.div_container_item_movile{
			height : 60vh !important;
		}
		.value_pay_contenedor{
  			display: flex;
  			align-items: center; /* Centra verticalmente los elementos en el contenedor */
  			gap: 10px; /
		}
		#clear-cash-notes {
			position: initial !important;
		}
	</style>
	<script type="text/javascript">
		localStorage.removeItem('pospaid_by');
		localStorage.removeItem('diferent_tax_alert');
		localStorage.removeItem('diferent_tax_alert_fixed');
		count_suspended_bills = "<?= $count_suspended_bills ?>";
		var is_quote = false;
		var is_duplicate = false;
		var protect_delete = false;
		var protect_delete_pin_request = false;
		var is_order = JSON.parse('<?= isset($order) ? "true" : "false" ?>');
		var user_biller_id = JSON.parse('<?= $this->session->userdata("biller_id") ? $this->session->userdata('biller_id') : 'false' ?>');
	    <?php if (isset($quote_id)) { ?>
			is_quote = true;
	        if (localStorage.getItem('pos_keep_prices_quote_id')) {
	            if (localStorage.getItem('pos_keep_prices_quote_id') != "<?= $quote_id ?>") {
	                localStorage.removeItem('pos_keep_prices');
	                localStorage.removeItem('pos_keep_prices_quote_id');
	            }
	        } else {
	            localStorage.removeItem('pos_keep_prices');
	            localStorage.removeItem('pos_keep_prices_quote_id');
	        }
	    <?php } ?>
		<?php if (!$sid) { ?>
				if (localStorage.getItem('restobar_mode_module') == 1) {
					localStorage.removeItem('restobar_mode_module');
					localStorage.removeItem('positems');
				}
		<?php } ?>
		var is_suspend_sale = false;
		<?php if ($suspend_sale) {?>
			is_suspend_sale = true;
		<?php } ?>
	</script>
	<script type="text/javascript">if(parent.frames.length !== 0){top.location = '<?=admin_url('pos')?>';}</script>
	<link rel="stylesheet" href="<?=$assets?>pos/css/posajax.css" type="text/css"/>
	<link rel="stylesheet" href="<?=$assets?>pos/css/print.css" type="text/css" media="print"/>
	<div class="wrapper wrapper-content  animated fadeInRight no-printable">
			<div class="ibox float-e-margins border-bottom">
							 <div id="content">
						<div class="c1">
							<div class="pos">
								<div id="pos">
									<div id="cp">
										<div id="cpinner">
											<div class="menu-sobre-productos">
												<?php if ($this->pos_settings->default_section != 1001 && $this->pos_settings->default_section != 1002): ?>
													<button type="button" id="open-category" class="btn btn-wappsi-cat btn-outline open-category <?= $this->pos_settings->default_section > 900 ? '' : 'wappsi_cat_active' ?>"><i class="fas fa-th"></i> <span class="tabs_text"><?= lang('categories'); ?></span></button>
													<button type="button" id="open-subcategory" class="btn btn-wappsi-cat btn-outline open-subcategory"><i class="fas fa-th-list"></i> <span class="tabs_text"><?= lang('subcategories'); ?></span></button>
													<button type="button" id="open-brands" class="btn btn-wappsi-cat btn-outline open-brands"><i class="fa-sharp fa-solid fa-flag"></i> <span class="tabs_text"><?= lang('brands'); ?></span></button>
													<button type="button" id="open-promotions" class="btn btn-wappsi-cat btn-outline  <?= $this->pos_settings->default_section == 998 ? 'wappsi_cat_active' : '' ?> open-promotions"><i class="fas fa-tags"></i> <span class="tabs_text"><?= lang('promotions'); ?></span></button>
													<?php if ($this->Settings->big_data_limit_reports == 0): ?>
														<button type="button" id="open-favorites" class="btn btn-wappsi-cat btn-outline  <?= $this->pos_settings->default_section == 999 ? 'wappsi_cat_active' : '' ?> open-favorites"><i class="fa-solid fa-bolt-lightning"></i> <span class="tabs_text"><?= lang('most_selled'); ?></span></button>
													<?php endif ?>
													<button type="button" id="open-featured" class="btn btn-wappsi-cat btn-outline open-featured <?= $this->pos_settings->default_section == 997 ? 'wappsi_cat_active' : '' ?>"><i class="fas fa-star"></i> <span class="tabs_text"><?= lang('featureds'); ?></span></button>
												<?php else: ?>
													<style type="text/css">
														.menu-sobre-productos {
												            display: flex;
												            overflow-x: auto; /* Permitir desplazamiento horizontal */
												            white-space: nowrap; /* Evitar que los botones se envuelvan */
												            align-items: center;
	    														scrollbar-width: none;
	    														justify-content: normal !important;
												        }
												        .btn-wappsi-cat {
												            min-width: 20% !important;
												            font-size: 110% !important;
												            margin-right: 10px; /* Espaciado entre botones */
												            overflow: hidden;
												        }
													</style>
													<script>
													    
													    setTimeout(function() {
													    	const menu = document.querySelector('.menu-sobre-productos');
														    let isDown = false;
														    let startX;
														    let scrollLeft;
														    menu.addEventListener('mousedown', (e) => {
														        isDown = true;
														        menu .classList.add('active');
														        startX = e.pageX - menu.offsetLeft;
														        scrollLeft = menu.scrollLeft;
														    });

														    menu.addEventListener('mouseleave', () => {
														        isDown = false;
														        menu.classList.remove('active');
														    });

														    menu.addEventListener('mouseup', () => {
														        isDown = false;
														        menu.classList.remove('active');
														    });

														    menu.addEventListener('mousemove', (e) => {
														        if (!isDown) return;
														        e.preventDefault();
														        const x = e.pageX - menu.offsetLeft;
														        const walk = (x - startX);
														        menu.scrollLeft = scrollLeft - walk;
														    });
													    }, 1000);
													</script>
													<?php if ($this->pos_settings->default_section == 1001): ?>
															<?php foreach ($categories as $category) {
																if ($category->hide == 1) {
																	continue;
																}
																if ($this->pos_settings->pos_navbar_default_category == $category->id) {
																	echo "<button id=\"category-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn btn-wappsi-cat btn-outline category ".($this->pos_settings->pos_navbar_default_category == $category->id ? 'wappsi_cat_active' : '')."  \" ><span class=\"tabs_text_inverse\">" . $category->code . "</span>
																			<span class=\"tabs_text\">" . $category->name . "</span>
																		</button>";
																}
															} ?>
															<?php foreach ($categories as $category) {
																if ($category->hide == 1) {
																	continue;
																}
																if ($this->pos_settings->pos_navbar_default_category != $category->id) {
																	echo "<button id=\"category-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn btn-wappsi-cat btn-outline category ".($this->pos_settings->pos_navbar_default_category == $category->id ? 'wappsi_cat_active' : '')."  \" ><span class=\"tabs_text_inverse\">" . $category->code . "</span>
																			<span class=\"tabs_text\">" . $category->name . "</span>
																		</button>";
																}
															} ?>
													<?php elseif ($this->pos_settings->default_section == 1002): ?>
															<?php foreach ($allsubcategories as $category) {
																if ($this->pos_settings->pos_navbar_default_category == $category->id) {
																	echo "<button id=\"category-" . $category->id . "\" type=\"button\" data-subcategory='" . $category->id . "' data-category='" . $category->parent_id . "' class=\"btn btn-wappsi-cat btn-outline ".($this->pos_settings->pos_navbar_default_category == $category->id ? 'wappsi_cat_active' : '')." subcategory_navbar \" ><span class=\"tabs_text_inverse\">" . $category->code . "</span>
																			<span class=\"tabs_text\">" . $category->name . "</span>
																		</button>";
																}
																	
															} ?>
															<?php foreach ($allsubcategories as $category) {
																if ($category->hide == 1 || !$category->parent_id) {
																	continue;
																}
																if ($this->pos_settings->pos_navbar_default_category != $category->id) {
																	echo "<button id=\"category-" . $category->id . "\" type=\"button\" data-subcategory='" . $category->id . "' data-category='" . $category->parent_id . "' class=\"btn btn-wappsi-cat btn-outline ".($this->pos_settings->pos_navbar_default_category == $category->id ? 'wappsi_cat_active' : '')." subcategory_navbar \" ><span class=\"tabs_text_inverse\">" . $category->code . "</span>
																		<span class=\"tabs_text\">" . $category->name . "</span>
																	</button>";
																}
																
															} ?>
													<?php endif ?>
												<?php endif ?>
													
											</div>

											<div class="quick-menu">

												<div id="brands-slider">
													<div id="brands-list">
														<?php
														foreach ($brands as $brand) {
															echo "<button id=\"brand-" . $brand->id . "\" type=\"button\" value='" . $brand->id . "' class=\"btn-prni brand btn-white\" >
																	<div class=\"pos-product-img\">
																		<img src=\"assets/uploads/thumbs/" . ($brand->image ? $brand->image : 'no_image.png') . "\" class='img-rounded' loading='lazy' />
																	</div>
																	<div class=\"pos-product-name\">
																		<span>" . $brand->name . "</span>
																	</div>
																</button>";
														}
														?>
													</div>
												</div>
												<div id="favorites-slider">
													<div id="favorites-list">
													<?php
														if ($favorites) {
															foreach ($favorites as $fav) {
																echo "<div class='btn-prni product btn-white' data-code='". $fav->code ."'>
																			<div class='pos-product-img'>
																					<img src=\"" . $this->sma->get_img_url($fav->image, true) . "\" alt=\"" . $fav->name . "\" class='img-rounded' loading='lazy' />
																			</div>
																			<div class='pos-product-name'>
																					" . ucfirst(mb_strtolower(character_limiter($fav->name, 40))) . "
																			</div>
																		</div>";

															}
														} else {
															echo "Sin resultados";
														}
													?>
													</div>
												</div>
												<div id="category-slider">
													<div id="category-list">
													<?php
														foreach ($categories as $category) {
															if ($category->hide == 1) {
																continue;
															}
															echo "<button id=\"category-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni category btn-white\" >
																	<div class=\"pos-product-img\">
																		<img src=\"". $this->sma->get_img_url($category->image, true) . "\" class='img-rounded' loading='lazy' />
																	</div>
																	<div class=\"pos-product-name\">
																		<span>" . $category->name . "</span>
																	</div>
																</button>";
														}
														?>
													</div>
												</div>
												<div id="subcategory-slider">
													<div id="subcategory-list">
														<?php
														if (!empty($subcategories)) {
															foreach ($subcategories as $category) {
																echo "<button id=\"subcategory-" . $category->id . "\" type=\"button\" value='" . $category->id . "' class=\"btn-prni subcategory btn-white\" >
																	<div class=\"pos-product-img\">
																		<img src=\"". $this->sma->get_img_url($category->image, true) . "\" class='img-rounded' loading='lazy' />
																	</div>
																	<div class=\"pos-product-name\">
																		<span>" . $category->name . "</span>
																	</div>
																</button>";
															}

														}
														?>
													</div>
												</div>
												<div id="promotions-slider">
													<div id="promotions-list">
														<?php
														if ($promotions) {
															foreach ($promotions as $promo) {
																echo "<button class='btn-prni product btn-white' data-code='". $promo->code ."' >
																	<div class=\"pos-product-img\">
																		<img src=\"" . $this->sma->get_img_url($promo->image, true) . "\" alt=\"" . $promo->name . "\" class='img-rounded' loading='lazy' />
																	</div>
																	<div class=\"pos-product-name\">
																		<span>" . ucfirst(mb_strtolower(character_limiter($promo->name, 40))) . "</span>
																	</div>
																</button>";
															}
														} else {
															echo "Sin resultados";
														}
														?>
													</div>
												</div>
												<div id="featured-slider">
													<div id="featured-list">
														<?php
														if ($featured) {
															foreach ($featured as $promo) {
																echo "<button class='btn-prni product btn-white' data-code='". $promo->code ."' >
																	<div class=\"pos-product-img\">
																		<img src=\"" . $this->sma->get_img_url($promo->image, true) . "\" alt=\"" . $promo->name . "\" class='img-rounded' loading='lazy' />
																	</div>
																	<div class=\"pos-product-name\">
																		<span>" . ucfirst(mb_strtolower(character_limiter($promo->name, 40))) . "</span>
																	</div>
																</button>";
															}
														} else {
															echo "Sin resultados";
														}
														?>
													</div>
												</div>
												<div id="proContainer">
													<div id="ajaxproducts">
														<div id="item-list">
															<?php // echo $products; ?>
														</div>
													</div>
													<div style="clear:both;"></div>
												</div>
											</div>
										</div>
									</div>

									<?= admin_form_open("pos", array('role' => 'form', 'id' => 'pos-sale-form',  'autocomplete'=>"off"));?>
									<input type="hidden" name="unique_field" value="<?= $this->sma->unique_field() ?>">
									<input type="hidden" name="type_pos" id="type_pos" value="1">
									<?php if (isset($fe_pos_sale_id)) { ?>
										<input type="hidden" name="fe_pos_sale_id" id="fe_pos_sale_id" value="<?= $fe_pos_sale_id ?>">
										<input type="hidden" name="document_type_default_return" id="document_type_default_return" value="<?= $pos_document_type_default_returns[0]->pos_document_type_default_returns ?>">
									<?php } ?>
									<div id="leftdiv">
										<div id="printhead">
											<h4 style="text-transform:uppercase;"><?php echo $Settings->site_name; ?></h4>
											<?php
											echo "<h5 style=\"text-transform:uppercase;\">" . $this->lang->line('order_list') . "</h5>";
											echo $this->lang->line("date") . " " . $this->sma->hrld(date('Y-m-d H:i:s'));
											?>
										</div>
										<!-- Bloque para botones del módulo restobar -->
										<?php if ($this->pos_settings->restobar_mode == YES) { ?>
											<div id="restobar_general_container">
												<?php
													$position_container = "16px";
													$position_first_label = "44px";
													$position_second_label = "98px";
													$position_third_label = "150px";
												?>
												<?php if (isset($restobar_table) && $restobar_table) { ?>
													<?php
														if ($this->pos_settings->restobar_mode == 1) {
															$module_restobar_name = "Pedido para la Mesa";
														} else if ($this->pos_settings->restobar_mode == 2) {
															$module_restobar_name = "Pedido a Domicilio";
														} else if ($this->pos_settings->restobar_mode == 2) {
															$module_restobar_name = "Cliente recoge";
														}
													?>
													<h3 style="color: #616162; font-size: 20px; margin-bottom: 20px; margin-left: 5px;">
                                                        <?= $module_restobar_name .": ".$restobar_table->numero ?>
                                                    </h3>
												<?php } else { ?>
													<div class="restobar_button_container" style="align-items: center; display: flex; justify-content: center; padding: 0 0 10px 0;">
														<?php if ($this->pos_settings->table_service == 1) { ?>
															<button type="button" id="boton_mesa" class="btn btn-wappsi-cat btn-outline" style="flex-grow: 1; margin-right: 5px; width: 33.3%;"><i class="fa fa-th fa-1x" aria-hidden="true"></i> <?= lang('restobar') ?></button>
														<?php } ?>
														<button type="button" id="boton_domicilio" class="btn btn-wappsi-cat btn-outline" style="flex-grow: 1; margin-right: 5px; width: 33.3%;"><i class="fa fa-motorcycle fa-1x" aria-hidden="true"></i> Domicilio</button>
														<button type="button" id="boton_recoger" class="btn btn-wappsi-cat btn-outline" style="flex-grow: 1; margin-right: 5px; width: 33.3%;"><i class="fa fa-shopping-bag fa-1x" aria-hidden="true"></i> Cliente Recoge</button>
													</div>
												<?php } ?>
											</div>
										<?php
											} else {
												$position_container = "14px";
												$position_first_label = "1px";
												$position_second_label = "55px";
												$position_third_label = "107px";
											}
										?>
										<!-- Fin bloque botones del módulo restobar -->
										<div id="left-top">
											<div style="position: absolute; <?=$Settings->user_rtl ? 'right:-9999px;' : 'left:-9999px;';?>"><?php echo form_input('test', '', 'id="test" class="kb-pad"'); ?></div>
											<div class="form-group" style="display: flex;" id="row_form_1">
												<?php if ($Owner || $Admin || !$this->session->userdata('biller_id')) { ?>
													<div <?= ($showDocTypeInput == TRUE) ? 'style="width: 50%"' : 'style="width: 70%"'?> id ="div_posbiller">
														<label class="pos-label-input"><?= lang('biller') ?></label>
														<?php
														foreach ($billers as $biller) {
															$btest = ($biller->company && $biller->company != '-' ? $biller->company : $biller->name);
															$posbillers[] = array('logo' => $biller->logo, 'company' => $btest);
															if ($biller->id == $pos_settings->default_biller) {
																$posbiller = array('logo' => $biller->logo, 'company' => $btest);
															}
														}
														?>
														<select name="posbiller" class="form-control" id="posbiller" style="margin-right: 5px !important;">
															<?php foreach ($billers as $biller): ?>
																<option value="<?= $biller->id ?>"
																	data-customerdefault="<?= $biller->default_customer_id ?>"
																	<?= (isset($_GET['fe_pos_sale_id'])) ? "data-posdocumenttypedefault='" .$biller->pos_document_type_default_fe. "'" : "data-posdocumenttypedefault='" .$biller->pos_document_type_default. "'" ?>
																	data-warehousedefault="<?= $biller->default_warehouse_id ?>"
																	data-pricegroupdefault="<?= $biller->default_price_group ?>"
																	data-sellerdefault="<?= $biller->default_seller_id ?>"
																	data-defaultcategory="<?= $biller->default_pos_section ?>"
																	data-autoica="<?= $biller->rete_autoica_percentage ?>"
																	<?= $biller->id == $this->Settings->default_biller ? 'selected="selected"' : '' ?>>
																	<?= $biller->name ?>
																</option>
															<?php endforeach ?>
														</select>
													</div>
                                     <?php if ($showDocTypeInput == TRUE) : ?>
													<div style="width: 20%; margin-left: 5px !important;" id ="div_doc_type">
														<label class="pos-label-input"><?= lang('document_type_abreviate') ?></label>
											            <select name="doc_type_id" class="form-control" id="doc_type_id"></select>
											            <span class="text-danger" id="doc_type_error" style="display: none;">* No se ha definido un tipo de documento, por favor, seleccione uno.</span>
													</div>
                                                    <?php endif ?>
												<?php } else {
													foreach ($billers as $biller) {
														$btest = ($biller->company && $biller->company != '-' ? $biller->company : $biller->name);
														$posbillers[] = array('logo' => $biller->logo, 'company' => $btest);
														if (isset($this->session->userdata('billers_id_assigned')[$biller->id])) {
                                                            $posbiller_id = $biller->id;
                                                            $posbiller = array('logo' => $biller->logo, 'company' => $btest);
															$posbillerdata[] = $biller;
														}
													}?>
														<div <?= ($showDocTypeInput == TRUE) ? 'style="width: 50%"' : 'style="width: 70%"'?> id ="div_posbiller">
															<label class="pos-label-input"><?= lang('biller') ?></label> <br>
															<select name="posbiller" class="form-control" id="posbiller" style="margin-right: 5px !important;">
																<?php foreach ($posbillerdata as $biller): ?>
																	<option value="<?= $biller->id ?>"
																		data-customerdefault="<?= $biller->default_customer_id ?>"
																		data-warehousedefault="<?= $biller->default_warehouse_id ?>"
																		data-pricegroupdefault="<?= $biller->default_price_group ?>"
																		data-sellerdefault="<?= $biller->default_seller_id ?>"
																		<?= (isset($_GET['fe_pos_sale_id'])) ? "data-posdocumenttypedefault='" .$biller->pos_document_type_default_fe. "'" : "data-posdocumenttypedefault='" .$biller->pos_document_type_default. "'" ?>
																		data-defaultcategory="<?= $biller->default_pos_section ?>"
																		data-autoica="<?= $biller->rete_autoica_percentage ?>"
																		<?= $biller->id == $this->Settings->default_biller ? 'selected="selected"' : '' ?> >
																		<?= $biller->name ?>
																	</option>
																<?php endforeach ?>
															</select>
														</div>
                                                        <?php if ($showDocTypeInput == TRUE) : ?>
														<div style="width: 20%; margin-left: 5px !important;" id ="div_doc_type">
															<label class="pos-label-input"><?= lang('document_type_abreviate') ?></label>
												            <select name="doc_type_id" class="form-control" id="doc_type_id"></select>
											            	<span class="text-danger" id="doc_type_error" style="display: none;">* No se ha definido un tipo de documento, por favor, seleccione uno.</span>
														</div>
                                                        <?php endif ?>
												<?php } ?>
												<div style="width: 30%; margin-left: 5px !important;" id ="div_warehouse">
													<label class="pos-label-input"><?= lang('warehouse') ?></label>
													<?php

													$wh[''] = '';
																foreach ($warehouses as $warehouse) {
																	$wh[$warehouse->id] = $warehouse->name;
																}

													 ?>
													<?php if ($Owner || $Admin || !$this->session->userdata('warehouse_id')) {
															?>
															<?php
																echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : $Settings->default_warehouse), 'id="poswarehouse" class="form-control pos-input-tip" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("warehouse") . '" required="required" ');
																?>
														<?php } else { ?>
															<select name="warehouse" id="poswarehouse" class="form-control">
																<option value="<?= $this->session->userdata('warehouse_id') ?>"><?= $wh[$this->session->userdata('warehouse_id')] ?></option>
															</select>
															<?php
														}
														?>
												</div>
											</div>
											<div class="form-group" style="display: flex;" id="row_form_2">
												<div style="width: 70%; margin-left: 5px !important;" id ="div_customer">
													<label class="pos-label-input"><?= lang('customer') ?></label>
													<?php if ($this->Settings->payment_collection_webservice == 1){ ?>
														<div class="input-group">
													<?php } ?>
													<?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="poscustomer" data-placeholder="' . $this->lang->line("select") . ' ' . $this->lang->line("customer") . '" required="required" class="form-control pos-input-tip"'); ?>
													<?php if ($this->Settings->payment_collection_webservice == 1) { ?>
															<span class="input-group-btn">
																<button class="btn btn-primary" type="button" id="checkQuotaKiowa"><i class="fas fa-search-dollar"></i></button>
															</span>
														</div>
													<?php } ?>
												</div>
												<div style="width:30%; margin-left: 5px !important;" id ="div_customer_branch">
													<label class="pos-label-input"><?= lang('customer_branch') ?></label>
													<select name="customer_branch" id="poscustomer_branch" class="form-control">

							            			</select>
												</div>
												<div class="txt-error"></div>
												<div style="clear:both;"></div>
											</div>
											<div class="form-group" style="" id ="div_seller">
									            <?php if ( $this->Owner || $this->Admin || ($user_group_name != 'seller' && !$this->sma->keep_seller_from_user())): ?>
														<label class="pos-label-input"><?= lang('seller') ?></label>
										                <?php
										                echo form_dropdown('seller', array('0' => lang("select") . ' ' . lang("biller")), (isset($_POST['seller']) ? $_POST['seller'] : ''), 'id="posseller" data-placeholder="' . lang("select") . ' ' . lang("seller") . '" class="form-control input-tip" style="width:100%;"');
										                ?>
										        <?php else: ?>
	                                                <?php if ($this->sma->keep_seller_from_user()): ?>
	                                                    <input type="hidden" name="seller" id="posseller"  value="<?= $this->session->userdata('seller_id') ?>">
	                                                <?php else: ?>
	                                                    <input type="hidden" name="seller" id="posseller"  value="<?= $this->session->userdata('company_id') ?>">
                                                	<?php endif ?>
									            <?php endif ?>
											</div>
											<div class="no-print" style="padding-top: 9px;">
												<div class="form-group" id="ui">
													<?php if ($Settings->allow_advanced_search == "1") : ?>
														<div class="input-group">
														<?= form_input('add_item', '', 'class="form-control pos-tip btn-pos-product" id="add_item" data-placement="top" data-trigger="focus" placeholder="' . $this->lang->line("search_product_by_name_code") . '" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
															<?php if ($Owner || $Admin || $GP['products-add']) { ?>
																<!-- <div class="input-group-addon" style="padding: 2px 8px; border-radius: 5px;">
																	<a href="#" id="addManually">
																		<i class="fa fa-plus-square" id="addIcon" style="font-size: 1.5em;"></i>
																	</a>
																</div> -->
															<?php } ?>
															<?php if ($Settings->allow_advanced_search == "1") : ?>
																<div class="input-group-addon" style="padding: 2px 8px; border-radius: 5px;">
																	<a href="#" data-toggle="modal" data-target="#modal_compatibilidad">
																		<i class="fa fa-search fa-lg" id="addIcon"></i>
																	</a>
																</div>
															<?php endif ?>
														</div>
													<?php else: ?>
														<?= form_input('add_item', '', 'class="form-control pos-tip btn-pos-product" id="add_item" data-placement="top" data-trigger="focus" placeholder="' . $this->lang->line("search_product_by_name_code") . '" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
													<?php endif ?>
													<div style="clear:both;"></div>
												</div>
											</div>
										</div>
										<div id="print">
											<div id="left-middle">
												<div id="product-list">
													<table class="table items  table-bordered table-condensed table-hover sortable_table"
													id="posTable" style="margin-bottom: 0;">
													<thead>
														<tr>
															<th style="width: 5%; text-align: center;">
																<i class="fa fa-pencil" style="opacity:0.5; filter:alpha(opacity=50);"></i>
															</th>
															<th width="40%"><?=lang("product");?>  <span class="product_number"></span></th>
															<th width="15%"><?=lang("price");?></th>
															<th width="15%"><?=lang("qty");?></th>
															<th width="15%"><?=lang("subtotal");?></th>
															<th style="width: 5%; text-align: center;">
																<i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
															</th>
														</tr>
													</thead>
													<tbody>
													</tbody>
												</table>
												<div style="clear:both;"></div>
											</div>
										</div>
										<div style="clear:both;"></div>



										<div id="left-bottom">
											<table id="totalTable">
												<tr class="order_detail">
													<td></td>
													<td style="padding: 5px 10px;"> <?=lang('subtotal');?> </td>
													<td class="text-right" style="padding: 5px 10px;font-weight:bold;"> <span id="subtotal">0.00</span> </td>
												</tr>
												<tr class="order_detail">
													<td></td>
													<td style="padding: 5px 10px;"> <?=lang('tax');?> </td>
													<td class="text-right" style="padding: 5px 10px;font-weight:bold;"> <span id="ivaamount">0.00</span> </td>
												</tr>
												<tr class="order_detail">
													<td></td>
													<td style="padding: 5px 10px;"> <?=lang('total');?> </td>
													<td class="text-right" style="padding: 5px 10px;font-weight:bold;"> <span id="total">0.00</span> </td>
												</tr>

												<?php if($this->pos_settings->withholding_management == 1) {?>
													<tr class="order_detail">
														<td style="text-align:center;">  <a href="#" id="pprete">
																<i class="fa fa-edit"></i>
															</a>
														</td>
														<td style="padding: 5px 10px;">
															<span class="fa fa-minus-square-o"></span> <?= lang('retentions') ?>
														</td>
														<td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;">
															<span id="rete">0.00</span>
														</td>
													</tr>
												<?php } ?>

						                		<?php if ($this->Settings->exclusive_discount_group_customers == 0): ?>
						                			<tr class="order_detail">
														<td style="text-align:center;">  <a href="#" id="ppdiscount">
															<i class="fa fa-edit"></i>
														</a></td>
														<td style="padding: 5px 10px;"><span class="fa fa-minus-square-o"></span> <?=lang('discount');?>
															<?php if ($Owner || $Admin || $this->session->userdata('allow_discount')) { ?>

															<?php } ?>
														</td>
														<td class="text-right" style="padding: 5px 10px;font-weight:bold;">
															<span id="tds">0.00</span>
														</td>
													</tr>
						                		<?php endif ?>

												<?php if ($this->Settings->default_tax_rate2 != 0): ?>
													<tr class="order_detail">
														<td style="text-align:center;">  <a href="#" id="pptax2">
																<i class="fa fa-edit"></i>
															</a>
														</td>
														<td style="padding: 5px 10px;">
															<span class="fa fa-plus-square-o"></span> <?=lang('order_tax');?>
														</td>
														<td class="text-right" style="padding: 5px 10px;font-size: 14px; font-weight:bold;">
															<span id="ttax2">0.00</span>
														</td>
													</tr>
												<?php endif ?>
												<tr class="order_detail tip_row" style="display: none;">
													<td class="text-center"> <a href="#" id="ptip"> <i class="fa fa-plus-square"></i> </a></td>
													<td style="padding: 5px 10px;"> <?= lang('tip_amount') ?> </td>
													<td class="text-right delete_tip_amount" style="padding: 5px 10px;font-weight:bold;"> <i class="fa fa-times"></i> <span id="tips">0.00</span> </td>
												</tr>
												<tr class="order_detail">
													<td style="text-align:center;"><a href="#" id="pshipping"> <i class="fa fa-plus-square"></i> </a></td>
													<td style="padding: 5px 10px;"><?= lang('shipping') ?></td>
													<td class="text-right" style="padding: 5px 10px;font-weight:bold;"><span id="tship">0.00</span></td>
												</tr>
											</table>

										<div class="form-group" style="display: flex; width: 100% !important;">
											<div class="div-btn-payment-bars">
												<button class="btn btn-wappsi-pagar view_order_detail detail_close btn-pos-footer" type="button">
													<span class="fa fa-bars" id="icon_detail"></span>
												</button>
											</div>
											<div class="div-btn-payment-amount" style="margin-left: 5px;">
												<?php if (!$this->Owner && !$this->Admin && $this->session->userdata('cant_finish_pos_invoice') == 1): ?>
													<button type="button" class="btn btn-wappsi-pagar btn-pos-footer" disabled style="font-size: 150% !important">
														<?= $this->pos_settings->express_payment ? "<i class='fas fa-hand-holding-usd'></i> ".lang('pay_with_cash')." " : lang('pay') ?> $ <span id="gtotal">0.00</span>
													</button>
												<?php else: ?>
													<button type="button" class="btn btn-wappsi-pagar btn-pos-footer" id="payment" style="font-size: 150% !important">
														<?= $this->pos_settings->express_payment ? "<i class='fas fa-hand-holding-usd'></i> ".lang('pay_with_cash')." " : lang('pay') ?> $ <span id="gtotal">0.00</span>
													</button>
												<?php endif ?>
											</div>
										</div>

										<div class="clearfix"></div>
										<div id="botbuttons" class="col-xs-12 text-center" style="padding: 0px !important;">
											<input type="hidden" name="biller" id="biller" value="<?= ($Owner || $Admin || !$this->session->userdata('biller_id')) ? $pos_settings->default_biller : $this->session->userdata('biller_id')?>"/>
									<div class="menu-sobre-productos">
                                        <?php if ($this->pos_settings->restobar_mode == 1) { ?>
                                            <div class="btn-pos">
                                                <button type="button" class="btn btn-default btn-pos-footer btn-outline" id="cancelOrder">
                                                    <span class="fas fa-times"></span>
                                                    <span class="no-mobile"><?= lang('exit') ?></span>
                                                </button>
                                            </div>

                                            <?php if (($Owner || $Admin)) { ?>
                                                <div class="btn-pos">
                                                    <button type="button" class="btn btn-wappsi-cancelar btn-pos-footer btn-outline" id="reset">
                                                        <span class="fas fa-times"></span>
                                                        <span class="no-mobile"><?= lang('delete') ?></span>
                                                    </button>
                                                </div>
                                            <?php } else if ($GP["pos-delete_table_order"] == 1) { ?>
                                                <div class="btn-pos">
                                                    <button type="button" class="btn btn-wappsi-cancelar btn-pos-footer btn-outline" id="reset">
                                                        <span class="fas fa-times"></span>
                                                        <span class="no-mobile"><?= lang('delete') ?></span>
                                                    </button>
                                                </div>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <div class="btn-pos">
                                                <button type="button" class="btn btn-wappsi-cancelar btn-pos-footer btn-outline" id="reset">
                                                    <span class="fas fa-times"></span>
                                                    <span class="no-mobile"><?= lang('reset') ?></span>
                                                </button>
                                            </div>
                                        <?php } ?>

										<div class="btn-pos">
											<button type="button" class="btn btn-wappsi-pausar btn-pos-footer btn-outline" id="suspend">
												<?php if ($this->pos_settings->restobar_mode == 1) { ?>
													<span class="fa fa-check"></span>
													<span class="no-mobile">Guardar</span>
												<?php } else { ?>
													<span class="fas fa-pause"></span>
													<span class="no-mobile"><?= lang('suspend') ?></span>
												<?php } ?>
											</button>
										</div>
										<?php if ($this->pos_settings->allow_print_command == 1 && ((isset($sid) && $this->pos_settings->command_only_for_suspended == 1) || $this->pos_settings->command_only_for_suspended == 0)): ?>
											<div class="btn-pos">
												<!-- print_bill -->
												<button type="button" class="btn btn-wappsi-comanda btn-pos-footer btn-outline" id="print_order">
													<span class="fas fa-print"></span>
													<span class="no-mobile"><?= ($this->pos_settings->restobar_mode == 1) ? "Prefactura" : "Comanda";?></span>
												</button>
											</div>
										<?php endif ?>
										<?php if ($this->pos_settings->creation_order_sale == 1): ?>
											<div class="btn-pos">
												<button type="button" class="btn btn-wappsi-pagar btn-pos-footer btn-outline" id="convert_order">
													<span class="far fa-file-alt"></span>
													<span class="no-mobile"><?= lang('order') ?></span>
												</button>
											</div>
										<?php endif ?>
									</div>
								</div>
								<div style="clear:both; height:5px;"></div>
								<div id="num">
									<div id="icon"></div>
								</div>
								<span id="hidesuspend"></span>
								<input type="hidden" name="pos_note" value="" id="pos_note">
								<input type="hidden" name="staff_note" value="" id="staff_note">
								<input type="hidden" name="locator_details" value="" id="locator_details">
								<input type="hidden" name="birthday_year_applied" value="" id="birthday_year_applied">

                <div id="payment-con">
                  <?php for ($i = 1; $i <= 5; $i++) {?>
                    <input type="hidden" name="amount[]" id="amount_val_<?=$i?>" value=""/>
                    <input type="hidden" name="balance_amount[]" id="balance_amount_<?=$i?>" value=""/>
                    <input type="hidden" name="due_payment[]" id="due_payment_<?=$i?>" class="due_payment_<?=$i?>" value=""/>
                    <input type="hidden" name="paid_by[]" id="paid_by_val_<?=$i?>" value="cash"/>
                    <input type="hidden" name="cc_no[]" id="cc_no_val_<?=$i?>" value=""/>
                    <input type="hidden" name="paying_gift_card_no[]" id="paying_gift_card_no_val_<?=$i?>" value=""/>
                    <input type="hidden" name="cc_holder[]" id="cc_holder_val_<?=$i?>" value=""/>
                    <input type="hidden" name="cheque_no[]" id="cheque_no_val_<?=$i?>" value=""/>
                    <input type="hidden" name="cc_month[]" id="cc_month_val_<?=$i?>" value=""/>
                    <input type="hidden" name="cc_year[]" id="cc_year_val_<?=$i?>" value=""/>
                    <input type="hidden" name="cc_type[]" id="cc_type_val_<?=$i?>" value=""/>
                    <input type="hidden" name="cc_cvv2[]" id="cc_cvv2_val_<?=$i?>" value=""/>
                    <input type="hidden" name="payment_note[]" id="payment_note_val_<?=$i?>" value=""/>
                  <?php }
                  ?>
                </div>
                <input name="order_tax" type="hidden" value="<?=$suspend_sale ? $suspend_sale->order_tax_id : ($old_sale ? $old_sale->order_tax_id : $Settings->default_tax_rate2);?>" id="postax2">
                <input name="discount" type="hidden" value="<?=$suspend_sale ? $suspend_sale->order_discount_id : ($old_sale ? $old_sale->order_discount_id : '');?>" id="posdiscount">
                <input name="shipping" type="hidden" value="<?=$suspend_sale ? $suspend_sale->shipping : ($old_sale ? $old_sale->shipping :  '0');?>" id="posshipping">
                <input type="hidden" name="table_id" id="table_id" value="<?= $suspend_sale ? $suspend_sale->table_id : ''; ?>">
                <input type="hidden" name="suspend_sale_id" id="suspend_sale_id" value="<?= $suspend_sale ? $suspend_sale->id : ''; ?>">
                <input type="hidden" name="rpaidby" id="rpaidby" value="cash" style="display: none;"/>
                <input type="hidden" name="gtotal_rete_amount" id="gtotal_rete_amount" value="0">
                <input type="hidden" name="total_items" id="total_items" value="0" style="display: none;"/>
                <input type="hidden" name="payment_term" id="payment_term">
                <input type="hidden" name="document_type_id" id="document_type_id">
                <input type="hidden" name="frequency" id="frequencyHidden">
                <input type="hidden" name="installment" id="installmentHidden">
                <?php if ($this->sma->keep_seller_from_user()): ?>
                	<input type="hidden" name="seller_id" id="seller_id" value="<?= $this->session->userdata('seller_id') ?>">
                <?php else: ?>
                	<input type="hidden" name="seller_id" id="seller_id" value="<?= $this->session->userdata('company_id') ?>">
                <?php endif ?>
                <input type="hidden" name="address_id" id="address_id">
                <input type="hidden" name="cost_center_id" id="cost_center_id">
                <input type="hidden" name="sale_tip_amount" id="sale_tip_amount">
                <input type="hidden" name="restobar_mode_module" id="restobar_mode_module">
                <input type="hidden" name="shipping_in_grand_total" id="shipping_in_grand_total">
                <input type="hidden" name="payment_document_type_id" id="payment_document_type_id">
                <input type="hidden" name="restobar_table" id="restobar_table" value="<?= isset($restobar_table) && $restobar_table ? $restobar_table->id : NULL ?>">
                <input type="submit" id="submit_sale" value="Submit Sale" style="display: none;"/>
                <input type="hidden" name="mean_payment_code_fe" id="mean_payment_code_fe">
                <?php if (isset($quote_id)): ?>
                	<input type="hidden" name="quote_id" id="quote_id" value="<?= $quote_id ?>">
                <?php endif ?>
                <?php if (isset($sid) || isset($quote)): ?>
                	<input type="hidden" name="sale_origin_reference_no" id="sale_origin_reference_no" value="<?= isset($sid) && $suspend_sale ? $suspend_sale->suspend_note.'('.$suspend_sale->id.')' : $quote->reference_no ?>">
                <?php endif ?>
                <?php if (isset($order)): ?>
                	<input type="hidden" name="order" id="order" value="1">
                <?php endif ?>
                <?php if (isset($suspend_sale) && $suspend_sale->origin_order_sale_id): ?>
                	<input type="hidden" name="origin_order_sale_id" id="origin_order_sale_id" value="<?= $suspend_sale->origin_order_sale_id ?>">
                <?php endif ?>
                <input type="hidden" name="except_category_taxes" id="except_category_taxes">
                <input type="hidden" name="tax_exempt_customer" id="tax_exempt_customer">
                <input type="hidden" name="submit_target" id="submit_target">
              </div>
            </div>

					</div>



<!-- retencion -->

        <div class="modal fade in" id="rtModal" tabindex="-1" role="dialog" aria-labelledby="rtModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                  class="fa fa-2x">&times;</i></button>
                  <h4 class="modal-title" id="rtModalLabel"><?=lang('apply_order_retention');?></h4>
                </div>
                <div class="modal-body">
                  <div class="row">
                    <div class="col-sm-2">
                      <label><?= lang('retention')  ?></label>
                    </div>
                    <div class="col-sm-5">
                      <label><?= lang('option')  ?></label>
                    </div>
                    <div class="col-sm-2">
                      <label><?= lang('percentage')  ?></label>
                    </div>
                    <div class="col-sm-3">
                      <label><?= lang('amount')  ?></label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_fuente" class="skip" id="rete_fuente"> <?= lang('') ?>
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_fuente_option" id="rete_fuente_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_fuente_account" id="rete_fuente_account">
                      <input type="hidden" name="rete_fuente_base" id="rete_fuente_base">
                      <input type="hidden" name="rete_fuente_id" id="rete_fuente_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_fuente_tax" id="rete_fuente_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_fuente_valor" id="rete_fuente_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_iva" class="skip" id="rete_iva"> Iva
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_iva_option" id="rete_iva_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_iva_account" id="rete_iva_account">
                      <input type="hidden" name="rete_iva_base" id="rete_iva_base">
                      <input type="hidden" name="rete_iva_id" id="rete_iva_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_iva_tax" id="rete_iva_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_iva_valor" id="rete_iva_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_ica" class="skip" id="rete_ica"> Ica
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_ica_option" id="rete_ica_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_ica_account" id="rete_ica_account">
                      <input type="hidden" name="rete_ica_base" id="rete_ica_base">
                      <input type="hidden" name="rete_ica_id" id="rete_ica_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_ica_tax" id="rete_ica_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_ica_valor" id="rete_ica_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_otros" class="skip" id="rete_otros"> <?= lang('rete_other') ?>
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_otros_option" id="rete_otros_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_otros_account" id="rete_otros_account">
                      <input type="hidden" name="rete_otros_base" id="rete_otros_base">
                      <input type="hidden" name="rete_otros_id" id="rete_otros_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_otros_tax" id="rete_otros_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_otros_valor" id="rete_otros_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_bomberil" class="skip" id="rete_bomberil" disabled> <?= lang('rete_bomberil') ?>
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_bomberil_option" id="rete_bomberil_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_bomberil_account" id="rete_bomberil_account">
                      <input type="hidden" name="rete_bomberil_base" id="rete_bomberil_base">
                      <input type="hidden" name="rete_bomberil_id" id="rete_bomberil_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_bomberil_tax" id="rete_bomberil_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_bomberil_valor" id="rete_bomberil_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-sm-2">
                      <label>
                        <input type="checkbox" name="rete_autoaviso" class="skip" id="rete_autoaviso" disabled> <?= lang('rete_autoaviso') ?>
                      </label>
                    </div>
                    <div class="col-sm-5">
                      <select class="form-control" name="rete_autoaviso_option" id="rete_autoaviso_option" disabled='true'>
                        <option>Seleccione...</option>
                      </select>
                      <input type="hidden" name="rete_autoaviso_account" id="rete_autoaviso_account">
                      <input type="hidden" name="rete_autoaviso_base" id="rete_autoaviso_base">
                      <input type="hidden" name="rete_autoaviso_id" id="rete_autoaviso_id">
                    </div>
                    <div class="col-sm-2">
                      <input type="text" name="rete_autoaviso_tax" id="rete_autoaviso_tax" class="form-control" readonly>
                    </div>
                    <div class="col-sm-3">
                      <input type="text" name="rete_autoaviso_valor" id="rete_autoaviso_valor" class="form-control text-right" readonly>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-8 text-right">
                      <label>Total : </label>
                    </div>
                    <div class="col-md-4 text-right">
                      <label id="total_rete_amount"> 0.00 </label>
                    </div>
                    <input type="hidden" name="rete_applied" id="rete_applied" value="0">
                  </div>


                  <!-- <div class="form-group">
                    <?=lang("order_tax", "order_tax_input");?>
                    <?php
                    $tr[""] = "";
                    foreach ($tax_rates as $tax) {
                      $tr[$tax->id] = $tax->name;
                    }
                    echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                    ?>
                  </div> -->
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
                  <button type="button" id="cancelOrderRete" class="btn btn-danger" data-dismiss="modal"><?= lang('reset') ?></button>
                  <button type="button" id="updateOrderRete" class="btn btn-primary"><?=lang('update')?></button>
                </div>
              </div>
            </div>
          </div>

<!-- retencion -->
					<?php echo form_close(); ?>
				</div>
				<div style="clear:both;"></div>
			</div>
		</div>
		</div>
		<!-- FINAL CONTENT -->
			</div>
	</div>



	<script type="text/javascript">
		$(document).ready(function(){
			$(document).on('click', '#buscar_productos', buscar_productos);
			$(document).on('click', '.agregar_producto', function() { agregar_producto($(this)); });
			$(document).on('keypress', '#producto, #descripcion', function(e){
				if (e.keyCode == 13) {
					$('#buscar_productos').trigger('click');
				}
			});
			$(document).on('click', '#clean_filters_button', function() { clean_filters(); });

			if (localStorage.getItem('restobar_mode_module') == 2 || localStorage.getItem('restobar_mode_module') == 3) {
				<?php if ($this->pos_settings->restobar_mode == 1) { ?>
					$('#suspend').parent('.btn-pos').css('display', 'none');
				<?php } ?>
			}
		});
		function buscar_productos()
		{
			var producto = $('#producto').val();
			var descripcion = $('#descripcion').val();
        	var marca = $('#marca').val();
        	var warehouse = $('#poswarehouse').val();

			if (producto == "")
			{
				command: toastr.error('El campo es obligatorio', 'Mensaje de validación', { onHidden : function(){ $('#producto').focus(); } });
				return false;
			}

			$('#tabla_productos_compatibilidad').DataTable({
				aaSorting: [[1, "asc"]],
				aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
				iDisplayLength: <?= $Settings->rows_per_page ?>,
				bDestroy: true,
				bProcessing: true,
				bServerSide: true,
				sAjaxSource: '<?= admin_url("pos/buscar_productos/") ?>',
				fnServerData: function (sSource, aoData, fnCallback)
				{
					aoData.push({ name: "producto", value: producto });
					aoData.push({ name: "descripcion", value: descripcion });
                	aoData.push({ name: "marca", value: marca });
                	aoData.push({ name: "warehouse", value: warehouse });
					aoData.push({ name: "<?= $this->security->get_csrf_token_name() ?>", value: "<?= $this->security->get_csrf_hash() ?>" });

					$.ajax({dataType: 'json', type: 'POST', 'url': sSource, 'data': aoData, success: fnCallback});
				},
				fnRowCallback: function (nRow, aData, iDisplayIndex) {
	                nRow.setAttribute('data-product_id', aData[6]);
	                nRow.className = "agregar_producto";

	                return nRow;
	            },
				aoColumns: [
                    null,
                    null,
                    null,
                    null,
                    {"mRender" : formatQuantity},
                    {"mRender" : formatQuantity},
                ],
			});
		}

		function clean_filters()
	    {
	        $('#producto').val("");
	        $('#descripcion').val("");
	        $('#tabla_productos_compatibilidad tbody').find('tr').empty();
	        $('#tabla_productos_compatibilidad').DataTable().destroy();
	    }

		function agregar_producto(elemento)
		{
			var product_id = elemento.data('product_id');
			var warehouse_id = $('#poswarehouse').val();
			var customer_id = $('#poscustomer').val();
			var biller_id = $('#posbiller').val();
			var address_id = $('#poscustomer_branch').val();

			$.ajax({
				dataType: 'json',
				type: 'post',
				url: '<?= admin_url("sales/iusuggestions") ?>',
				data: {
					'<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
					'product_id': product_id,
					'warehouse_id': warehouse_id,
					'customer_id': customer_id,
					'biller_id': biller_id,
					'address_id': address_id,
					'unit_quantity': 1
				}
			}).done(function(data) {
				add_invoice_item(data);
				$('#modal_compatibilidad').modal('hide');
			}).error(function (data) {
				console.log(data.responseText);
			})
		}
	</script>

	<div class="modal fade" id="modal_compatibilidad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	        	<span aria-hidden="true"><i class="fa fa-2x">&times;</i></span>
	        	<span class="sr-only"><?=lang('close');?></span>
	        </button>
	        <h4 class="modal-title" id="myModalLabel">Busqueda avanzada</h4>
	      </div>
	      <div class="modal-body">
	        <div class="row">
	        	<form id="formulario_compatibilidad_producto" data-toggle="validator">
		        	<div class="col-sm-3">
		        		<div class="form-group">
	                        <label for="producto">Código / Nombre</label>
	                        <input class="form-control" type="text" name="producto" id="producto" placeholder="Código o nombre a buscar">
		        		</div>
		        	</div>
		        	<div class="col-sm-3">
		        		<div class="form-group">
		        			<label for="descripcion">Descripción</label>
		        			<input class="form-control" type="text" name="descripcion" id="descripcion">
		        		</div>
		        	</div>
	                <div class="col-sm-2">
	                    <div class="form-group">
	                        <label for="marca">Marca</label>
	                        <select class="form-control" name="marca" id="marca">
	                            <option value="">Seleccione...</option>
	                            <?php foreach ($brands as $brand): ?>
	                                <option value="<?= $brand->id ?>"><?= $brand->name ?></option>
	                            <?php endforeach ?>
	                        </select>
	                    </div>
	                </div>
		        	<div class="col-sm-2">
	        			<button class="btn btn-primary" type="button" id="buscar_productos" style="margin-top: 28px; width: 100%;"><i class="fa fa-search"></i> Buscar</button>
		        	</div>
		        	<div class="col-sm-2">
	        			<button class="btn btn-default" type="button" id="clean_filters_button" style="margin-top: 28px; width: 100%;"><i class="fa fa-trash"></i> <?= lang("clear"); ?></button>
		        	</div>
	        	</form>
	        </div>
	        <hr>
	        <div class="row">
	        	<table class="table table-condensed table-bordered table-hover" id="tabla_productos_compatibilidad">
	        		<thead>
	        			<th>Código</th>
	        			<th>Producto</th>
	        			<th>Descripción</th>
	        			<th>Marca</th>
	        			<th>Precio</th>
	        			<th>Cantidad</th>
	        		</thead>
	        		<tbody>

	        		</tbody>
	        	</table>
	        </div>
	      </div>
	    </div>
	  </div>
	</div>

    <div class="modal fade in" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="payModalLabel" aria-hidden="true">
    	<div class="modal-dialog modal-md">
      		<div class="modal-content">
                <div class="modal-body" id="payment_content">
                    <style type="text/css">
                        #paymentModal .select2-choice {
                        min-height: 40px !important;
                        max-height: 40px !important;
                        overflow-y: auto;
                        }
                    </style>
                    <div class="col-sm-12 resAlert"></div>

                    <em class="text-danger margin_alert"><?= lang('msg_error_invalid_margin') ?></em>

                    <?php if (isset($cost_centers)): ?>
                        <div class="col-md-6 form-group">
                            <?= lang('cost_center', 'cost_center') ?>
                            <?php
                            $ccopts[''] = lang('select');
                            foreach ($cost_centers as $cost_center) {
                                $ccopts[$cost_center->id] = "(".$cost_center->code.") ".$cost_center->name;
                            }
                            ?>
                            <?= form_dropdown('cost_center', $ccopts, '', 'id="cost_center" class="form-control input-tip" required="required" style="width:100%;" '); ?>
                        </div>
                    <?php endif ?>

                    <div class="row">
                        <div class="col-sm-12 text-center">
                            <h2 class="title_payment"><?=lang("total_payable");?></h2>
                            <p id="twt" class="txt_payment">0.00</p>
                        </div>
                    </div>

                    <hr>

                    <div class="row">
                        <div class="col-md-12">
                            <?php if (!$showDocTypeInput) : ?>
                            <div class="bg-warning p-xs b-r-sm text-warning text-center" id="limitExceededElertElectronicPos" style="display:none">
                                <i class="fas fa-exclamation-triangle fa-lg">&nbsp; </i>Esta venta se procesará como Factura electrónica.
                            </div>
                            <?php endif ?>

                            <?php if (!$showDocTypeInput) : ?>
                            <div>
                                <label class="pos-label-input"><?= lang('document_type_abreviate') ?></label>
                                <select name="doc_type_id" class="form-control" id="doc_type_id"></select>
                                <span class="text-danger" id="doc_type_error" style="display: none;">* No se ha definido un tipo de documento, por favor, seleccione uno.</span>
                            </div>
                            <?php endif ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12 col-xs-12">
                            <div class="row">
                                <div class="col-md-6 col-xs-12">
                                    <h3 class="title_payment text-right"><?= lang('product_number') ?> : <span class="pmnt_product_number"></span></h3>
                                </div>
                                <div class="col-md-6 col-xs-12">
                                    <h3 class="title_payment"><?= lang('total_items') ?> : <span class="pmnt_total_items"></span></h3>
                                </div>
                            </div>
                        </div>
                        <div id="payments" class="col-sm-12 col-xs-12">
                            <div class="payment">
                                <div class="row row_payment" style="height: 85px;">
                                    <div class="col-sm-6 col-xs-12 input_payment_term_paid_by">
                                        <div class="form-group">
                                            <h3 class="title_payment text-right"><?= lang("payment_method") ?></h3>
                                            <select name="paid_by[]" id="paid_by_1" class="form-control paid_by" style="font-size: 150%; height: 40px;">
                                                <?= $this->sma->paid_opts(); ?>
                                                <?=$pos_settings->paypal_pro ? '<option value="ppp">' . lang("paypal_pro") . '</option>' : '';?>
                                                <?=$pos_settings->stripe ? '<option value="stripe">' . lang("stripe") . '</option>' : '';?>
                                                <?=$pos_settings->authorize ? '<option value="authorize">' . lang("authorize") . '</option>' : '';?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-xs-12 input_payment_term_amount">
										<h3 class="title_payment"><?= lang("amount") ?></h3>
                                        <div class="form-group value_pay_contenedor">
                                            <input name="amount[]" type="text" id="amount_1" class="pa form-control kb-pad1 amount only_number text-right" style="font-size: 150%;height: 40px;padding-right: 12%;" />
											<span class="fa fa-trash text-danger" id="clear-cash-notes"></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-4 col-xs-12 input_payment_term_val" style="display: none;">
                                        <div class="form-group">
                                            <h3 class="title_payment"><?=lang("payment_term");?></h3>
                                            <input type="text" class="pa form-control payment_term_val only_number"  style="font-size: 150%; height: 40px;" >
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-11">
                                        <div class="form-group gc_1" style="display: none;">
                                            <?=lang("gift_card_no", "gift_card_no_1");?>
                                            <input name="paying_gift_card_no[]" type="text" id="gift_card_no_1"
                                            class="pa form-control kb-pad gift_card_no"/>

                                            <div id="gc_details_1"></div>
                                        </div>
                                        <div class="pcc_1" style="display:none;">
                                            <div class="form-group">
                                                <input type="text" id="swipe_1" class="form-control swipe"
                                                placeholder="<?=lang('swipe')?>"/>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <input name="cc_no[]" type="text" id="pcc_no_1"
                                                        class="form-control"
                                                        placeholder="<?=lang('cc_no')?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="form-group">

                                                        <input name="cc_holer[]" type="text" id="pcc_holder_1"
                                                        class="form-control"
                                                        placeholder="<?=lang('cc_holder')?>"/>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <select name="cc_type[]" id="pcc_type_1"
                                                        class="form-control pcc_type"
                                                        placeholder="<?=lang('card_type')?>">
                                                        <option value="Visa"><?=lang("Visa");?></option>
                                                        <option
                                                        value="MasterCard"><?=lang("MasterCard");?></option>
                                                        <option value="Amex"><?=lang("Amex");?></option>
                                                        <option
                                                        value="Discover"><?=lang("Discover");?></option>
                                                    </select>
                                                    <!-- <input type="text" id="pcc_type_1" class="form-control" placeholder="<?=lang('card_type')?>" />-->
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">
                                                    <input name="cc_month[]" type="text" id="pcc_month_1"
                                                    class="form-control"
                                                    placeholder="<?=lang('month')?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">

                                                    <input name="cc_year" type="text" id="pcc_year_1"
                                                    class="form-control"
                                                    placeholder="<?=lang('year')?>"/>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group">

                                                    <input name="cc_cvv2" type="text" id="pcc_cvv2_1"
                                                    class="form-control"
                                                    placeholder="<?=lang('cvv2')?>"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="pcheque_1" style="display:none;">
                                        <div class="form-group"><?=lang("cheque_no", "cheque_no_1");?>
                                            <input name="cheque_no[]" type="text" id="cheque_no_1"
                                            class="form-control cheque_no"/>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div id="multi-payment" class="col-sm-12 col-xs-12"></div>
                        <br>
                        <div class="col-sm-3" style="display: none;">
                            <button type="button" class="btn btn-lg btn-info quick-cash" id="quick-payable">0.00
                            </button>
                        </div>
                    </div>

                    <div class="row" style="padding-bottom: 10px;">
                        <?php
                        foreach (lang('quick_cash_notes') as $cash_note_amount) {
                            echo '<div class="col-md-3 col-xs-12">
                                    <button type="button" class="btn btn-lg btn-success btn-outline quick-cash">' . $cash_note_amount . '</button>
                                </div>';
                        }
                        ?>
                        <!-- <div class="col-sm-3">
                                <button type="button" class="btn btn-lg btn-danger"
                                id="clear-cash-notes"><?=lang('clear');?></button>
                        </div> -->
                    </div>

                    <div class="row" style="padding-bottom: 10px;">
                        <div class="col-md-12 ">
                            <button type="button" class="btn btn-lg btn-primary btn-block addButton"><i
                                class="fa fa-plus"></i> <?=lang('add_more_payments')?></button>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-6 col-xs-12 txt_payment_left">
                            <h3 class="title_payment" style="margin-bottom: -5px;"><?=lang("total_paying");?></h3>
                            <span id="total_paying" class="txt_payment text-primary">0.00</span>
                        </div>
                        <div class="col-sm-6 col-xs-12 txt_payment_right">
                            <h3 class="title_payment" style="margin-bottom: -5px;"><?=lang("balance");?></h3>
                            <span id="balance" class="txt_payment text-success">0.00</span>
                        </div>
                    </div>
                    <?php if ($this->pos_settings->locator_management == 1): ?>
                    	<div class="form-group">
	                        <div class="col-sm-12">
	                            <h3 class="title_payment"><?= lang('locator_details') ?></h3>
	                            <?=form_input('input_locator_details', "", 'id="input_locator_details" class="form-control only_number" maxlength="45"');?>
	                        </div>
	                    </div>
                    <?php endif ?>
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-12 div_payment_reference" style="display: none;">
                                <?= lang('payment_reference_no', 'payment_reference_no') ?>
                                <select name="payment_reference_no" class="form-control" id="payment_reference_no" required="required"></select>
                            </div>
                            <hr class="col-sm-11">
                            <div id="sale_note" class="col-sm-6 text-right">
                                <h3 class="title_payment"><?= lang('sale_note') ?></h3>
                                <?=form_textarea('sale_note', "", 'id="sale_note" class="form-control kb-text skip" style="height: 100px;" maxlength="250"');?>
                            </div>
                            <div class="col-sm-6">
                                <h3 class="title_payment"><?= lang('staff_note') ?></h3>
                                <?=form_textarea('staffnote', isset($suspend_sale->id) ? " Orden No ". $suspend_sale->id : "", 'id="staffnote" class="form-control kb-text skip" style="height: 100px;" maxlength="250"');?>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="error_min_sale_amount" style="display: none;">
                            <br>
                            <em class="text-danger bold"><?= sprintf(lang('min_sale_amount_error'), lang('sale')) ?></em>
                            <br>
                            <br>
                        </div>
                        <?php if ($this->pos_settings->order_print_mode == 1 || $this->pos_settings->order_print_mode == 2): ?>
                            <h4 class="text-warning text-center"><?= lang('product_preparation_pending_advertisement') ?></h4>
                        <?php endif ?>
                        <?php if ($this->session->userdata('print_pos_finalized_invoice') == 2): ?>
                        	<div class="row">
                        		<div class="col-sm-12">
                        			<button type="button" class="btn btn-block btn-lg btn-wappsi-pagar submit-sale"data-submittarget="1"><?=lang('finish_sale')?></button>
                        		</div>
                        		<div class="col-sm-12">
                        			<button type="button" class="btn btn-block btn-lg btn-wappsi-cancelar btn-outline submit-sale"data-submittarget="2"><?=lang('finish_sale')?> </button>
                        		</div>
                        	</div>
                        <?php else: ?>
                        	<button type="button" class="btn btn-block btn-lg btn-wappsi-pagar submit-sale"><?=lang('finish_sale');?></button>
                        <?php endif ?>

                    </div>
                </div>
            </div>
        </div>
	</div>

    <div class="modal" id="cmModal" tabindex="-1" role="dialog" aria-labelledby="cmModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                        <i class="fa fa-2x">&times;</i></span>
                        <span class="sr-only"><?=lang('close');?></span>
                    </button>
                    <h4 class="modal-title" id="cmModalLabel"></h4>
                </div>
                <div class="modal-body" id="pr_popover_content">
                    <div class="form-group">
                        <?= lang('comment', 'icomment'); ?>
                        <?= form_textarea('comment', '', 'class="form-control" id="icomment" style="height:80px;"'); ?>
                    </div>
                    <div class="form-group">
                        <?= lang('ordered', 'iordered'); ?>
                        <?php
                        $opts = array(0 => lang('no'), 1 => lang('yes'));
                        ?>
                        <?= form_dropdown('ordered', $opts, '', 'class="form-control" id="iordered" style="width:100%;"'); ?>
                    </div>
                    <input type="hidden" id="irow_id" value=""/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="editComment"><?=lang('submit')?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="prModal" tabindex="-1" role="dialog" aria-labelledby="prModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                        <h4 class="modal-title" id="prModalLabel"></h4>
                </div>
                <div class="modal-body" id="pr_popover_content">
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="pname" class="col-sm-4 control-label"><?= lang('product_name') ?></label>
                            <div class="col-sm-8">
                                <?= form_input('pname', '', 'id="pname" class="form-control"') ?>
                            </div>
                        </div>
                        <?php if ($Settings->tax1) {
                            ?>
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?=lang('product_tax')?></label>
                                <div class="col-sm-8">
                                    <input type="text" id="ptax_show" class="form-control" readonly>
                                    <input type="hidden" id="ptax">
                                </div>
                            </div>

                            <?php if ($this->Settings->ipoconsumo == 2 || $this->Settings->ipoconsumo == 1): ?>
                            	<div class="form-group row">
		                            <div class="col-sm-4">
		                                <label class="control-label"><?= lang('second_product_tax') ?></label>
		                            </div>
		                            <div class="col-sm-8 ptax2_div">
		                                <input type="text" name="ptax2" id="ptax2" class="form-control" readonly>
		                            </div>
		                            <div class="col-sm-4 ptax2_percentage_div" style="display:none;">
		                                <input type="text" name="ptax2_percentage" id="ptax2_percentage" class="form-control" readonly>
		                            </div>
		                        </div>
                            <?php endif ?>
                        <?php } ?>
                        <?php if ($Settings->product_serial) { ?>
                            <div class="form-group">
                                <label for="pserial" class="col-sm-4 control-label"><?=lang('serial_no')?></label>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control kb-text" id="pserial">
                                </div>
                            </div>
                        <?php } ?>
                        <div class="form-group punit_div">
                            <label for="punit" class="col-sm-4 control-label"><?= lang('product_unit') ?></label>
                            <div class="col-sm-8">
                                <div id="punits-div"></div>
                            </div>
                        </div>
		                <?php if ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10): ?>
		                    <div class="form-group pedit_input_group">
		                        <label for="punit_quantity" class="col-sm-4 control-label"><?= lang('product_unit_quantity') ?></label>

		                        <div class="col-sm-8">
		                            <input type="text" class="form-control" id="punit_quantity" readonly>
		                        </div>
		                    </div>
		                <?php endif ?>
                        <div class="form-group">
                            <label for="pquantity" class="col-sm-4 control-label"><?=lang('quantity')?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control kb-pad" id="pquantity">
                            </div>
                        </div>
						<?php if($this->Settings->handle_jewerly_products): ?>
							<div class="form-group pedit_input_group jewel_grams_div">
								<label for="jewel_grams" class="col-sm-4 control-label"><?= lang('grams') ?></label>
								<div class="col-sm-8">
									<input type="text" class="form-control kb-pad" id="jewel_grams">
								</div>
							</div> 
						<?php endif; ?>	
                        <div class="form-group poptions_div">
                            <label for="poption" class="col-sm-4 control-label"><?=lang('product_option')?></label>
                            <div class="col-sm-8">
                                <div id="poptions-div"></div>
                            </div>
                        </div>
                        <div class="form-group ppreferences_div">
                            <label for="ppreferences" class="col-sm-4 control-label"><?=lang('product_preferences')?></label>
                            <div class="col-sm-8">
                                <div id="ppreferences-div"></div>
                            </div>
                        </div>
                		<?php if ($this->Settings->exclusive_discount_group_customers == 0): ?>
	                        <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) { ?>
	                            <div class="form-group">
	                                <label for="pdiscount" class="col-sm-4 control-label"><?=lang('total_product_discount')?></label>
	                                <div class="col-sm-8">
	                                    <input type="text" class="form-control kb-pad" id="ptdiscount">
	                                </div>
	                            </div>
	                            <div class="form-group">
	                                <label for="pdiscount" class="col-sm-4 control-label"><?=lang('product_discount')?></label>
	                                <div class="col-sm-8">
	                                    <input type="text" class="form-control kb-pad" id="pdiscount">
	                                </div>
	                            </div>
	                        <?php } ?>
                		<?php endif ?>
                        <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "style='display:none;'" : "" ?>>
                            <label for="pprice" class="col-sm-4 control-label"><?= lang('unit_price') ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                            </div>
                        </div>
                        <div class="form-group div_pprice_authorization_code" style="display:none;">
                            <label for="pprice_authorization_code" class="col-sm-4 control-label"><?= lang('authorization_code') ?></label>
                            <div class="col-sm-8">
                                <div class="input-group" >
                                    <input type="password" class="form-control" id="pprice_authorization_code" placeholder="<?= lang('authorization_code_placeholder') ?>">
                                    <span class="input-group-addon" id="check_pprice_autoriz" style="cursor:pointer;"><i class="fas fa-lock-open"></i></span>
                                </div>
                                <input type="hidden" name="under_cost_authorized" id="under_cost_authorized" value="0">
                            </div>
                        </div>
                        <div class="form-group" <?= $this->Settings->precios_por_unidad_presentacion == 2 && ($this->Settings->prioridad_precios_producto == 7 || $this->Settings->prioridad_precios_producto == 10) ? "" : "style='display:none;'" ?>>
                            <label for="pproduct_unit_price" class="col-sm-4 control-label"><?= lang('sale_form_edit_product_unit_price') ?></label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="pproduct_unit_price" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                            </div>
                        </div>
                        <table class="table table-bordered">
                            <tr>
                                <th style="width:25%;"><?=lang('net_unit_price');?></th>
                                <th style="width:25%;"><span id="net_price"></span></th>
                                <th style="width:25%;"><?=lang('product_taxes');?></th>
                                <th style="width:25%;"><span id="pro_tax"></span></th>
                            </tr>
                        </table>
                        <input type="hidden" id="punit_price" value=""/>
                        <input type="hidden" id="old_tax" value=""/>
                        <input type="hidden" id="old_qty" value=""/>
                        <input type="hidden" id="old_price" value=""/>
                        <input type="hidden" id="row_id" value=""/>


                        <div class="panel panel-default">
                            <div class="panel-heading"><?= lang('calculate_unit_price'); ?></div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <label for="pcost" class="col-sm-4 control-label"><?= lang('subtotal_before_tax') ?></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="psubtotal">
                                            <div class="input-group-addon" style="padding: 2px 8px;">
                                                <a id="calculate_unit_price" class="tip" title="<?= lang('calculate_unit_price'); ?>">
                                                    <i class="fa fa-calculator"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="paddprice" class="col-sm-4 control-label"><?=lang('add_price_amount')?></label>

                            <div class="col-sm-8">
                                <input type="text" class="form-control kb-pad" id="paddprice" <?= ($Owner || $Admin || $GP['edit_price']) ? '' : 'readonly'; ?>>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="editItem"><?=lang('submit')?></button>
                    <em class="text-danger error_under_cost" style="display:none;">El precio digitado está por debajo del costo del producto</em>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="mModal" tabindex="-1" role="dialog" aria-labelledby="mModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                        class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span></button>
                        <h4 class="modal-title" id="mModalLabel"><?=lang('add_product_manually')?></h4>
                    </div>
                    <div class="modal-body" id="pr_popover_content">
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="mcode" class="col-sm-4 control-label"><?=lang('product_code')?> *</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control kb-text" id="mcode">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mname" class="col-sm-4 control-label"><?=lang('product_name')?> *</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control kb-text" id="mname">
                                </div>
                            </div>
                            <?php if ($Settings->tax1) {
                                ?>
                                <div class="form-group">
                                    <label for="mtax" class="col-sm-4 control-label"><?=lang('product_tax')?> *</label>

                                    <div class="col-sm-8">
                                        <?php
                                        $tr[""] = "";
                                        foreach ($tax_rates as $tax) {
                                            $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('mtax', $tr, "", 'id="mtax" class="form-control pos-input-tip" style="width:100%;"');
                                        ?>
                                    </div>
                                </div>
                            <?php }
                            ?>
                            <div class="form-group">
                                <label for="mquantity" class="col-sm-4 control-label"><?=lang('quantity')?> *</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control kb-pad" id="mquantity">
                                </div>
                            </div>
                            <?php if ($Settings->product_discount && ($Owner || $Admin || $this->session->userdata('allow_discount'))) {?>
                                <div class="form-group">
                                    <label for="mdiscount"
                                    class="col-sm-4 control-label"><?=lang('product_discount')?></label>

                                    <div class="col-sm-8">
                                        <input type="text" class="form-control kb-pad" id="mdiscount">
                                    </div>
                                </div>
                            <?php }
                            ?>
                            <div class="form-group">
                                <label for="mprice" class="col-sm-4 control-label"><?=lang('unit_price')?> *</label>

                                <div class="col-sm-8">
                                    <input type="text" class="form-control kb-pad" id="mprice">
                                </div>
                            </div>
                            <table class="table table-bordered">
                                <tr>
                                    <th style="width:25%;"><?=lang('net_unit_price');?></th>
                                    <th style="width:25%;"><span id="mnet_price"></span></th>
                                    <th style="width:25%;"><?=lang('product_tax');?></th>
                                    <th style="width:25%;"><span id="mpro_tax"></span></th>
                                </tr>
                            </table>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="addItemManually"><?=lang('submit')?></button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade in" id="sckModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">
                            <i class="fa fa-2x">&times;</i></span><span class="sr-only"><?=lang('close');?></span>
                        </button>
                        <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                            <i class="fa fa-print"></i> <?= lang('print'); ?>
                        </button>
                        <h4 class="modal-title" id="mModalLabel"><?=lang('shortcut_keys')?></h4>
                    </div>
                    <div class="modal-body" id="pr_popover_content">
                        <table class="table table-bordered table-condensed table-hover"
                        style="margin-bottom: 0px;">
                        <thead>
                            <tr>
                                <th><?=lang('shortcut_keys')?></th>
                                <th><?=lang('actions')?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><?=$pos_settings->focus_add_item?></td>
                                <td><?=lang('focus_add_item')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->add_manual_product?></td>
                                <td><?=lang('add_manual_product')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->customer_selection?></td>
                                <td><?=lang('customer_selection')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->add_customer?></td>
                                <td><?=lang('add_customer')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->toggle_category_slider?></td>
                                <td><?=lang('toggle_category_slider')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->toggle_subcategory_slider?></td>
                                <td><?=lang('toggle_subcategory_slider')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->cancel_sale?></td>
                                <td><?=lang('cancel_sale')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->suspend_sale?></td>
                                <td><?=lang('suspend_sale')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->print_items_list?></td>
                                <td><?=lang('print_items_list')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->finalize_sale?></td>
                                <td><?=lang('finalize_sale')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->today_sale?></td>
                                <td><?=lang('today_sale')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->open_hold_bills?></td>
                                <td><?=lang('open_hold_bills')?></td>
                            </tr>
                            <tr>
                                <td><?=$pos_settings->close_register?></td>
                                <td><?=lang('close_register')?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="dsModal" tabindex="-1" role="dialog" aria-labelledby="dsModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </button>
                    <h4 class="modal-title" id="dsModalLabel"><?=lang('edit_order_discount');?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?=lang("order_discount", "order_discount_input");?>
                        <?php echo form_input('order_discount_input', '', 'class="form-control kb-pad" id="order_discount_input"'); ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="updateOrderDiscount" class="btn btn-primary"><?=lang('update')?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="tipModal" tabindex="-1" role="dialog" aria-labelledby="tipModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </button>
                    <h4 class="modal-title" id="tipModalLabel"><?=lang('tip_amount');?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?=lang("tip_amount", "tip_amount_input");?>
                        <?php echo form_input('tip_amount_input', '', 'class="form-control kb-pad" id="tip_amount_input"'); ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="deleteTip" class="btn btn-danger"><i class="fa fa-times"></i></button>
                    <button type="button" id="recalculateTip" class="btn btn-primary"><i class="fa fa-refresh"></i></button>
                    <button type="button" id="updateTip" class="btn btn-primary"><?=lang('update')?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="sModal" tabindex="-1" role="dialog" aria-labelledby="sModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </button>
                    <h4 class="modal-title" id="sModalLabel"><?=lang('shipping');?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?=lang("shipping", "shipping_input");?>
                        <?php echo form_input('shipping_input', '', 'class="form-control kb-pad" id="shipping_input"'); ?>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" id="updateShipping" class="btn btn-primary"><?=lang('update')?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="txModal" tabindex="-1" role="dialog" aria-labelledby="txModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                    <h4 class="modal-title" id="txModalLabel"><?=lang('edit_order_tax');?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?=lang("order_tax", "order_tax_input");?>
                        <?php
                        $tr[""] = "";
                        foreach ($tax_rates as $tax) {
                            $tr[$tax->id] = $tax->name;
                        }
                        echo form_dropdown('order_tax_input', $tr, "", 'id="order_tax_input" class="form-control pos-input-tip" style="width:100%;"');
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <button type="button" id="updateOrderTax" class="btn btn-primary"><?=lang('update')?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="susModal" tabindex="-1" role="dialog" aria-labelledby="susModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                    <span class="modal-title" id="susModalLabel" style="font-size: 150%;"><?=lang('suspend_sale');?></span>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <p><?=lang('type_reference_note');?></p>
                            <div class="form-group">
                                <?=lang("reference_note", "reference_note");?>
                                <?= form_input('reference_note', (!empty($reference_note) ? $reference_note : ''), 'class="form-control kb-text btn-pos-product" id="reference_note"'); ?>
                            </div>

                            <?php if ($this->pos_settings->activate_electronic_pos == YES/*  && $this->pos_settings->mode_electronic_pos == 2 */) : ?>
                                <div class="bg-warning p-xs b-r-sm text-warning text-center" id="limitExceededElertElectronicPosSuspend">
                                    <i class="fas fa-exclamation-triangle fa-lg">&nbsp; </i>Esta venta se procesará como Factura electrónica.
                                </div>
                            <?php endif ?>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="suspend_sale" class="btn btn-primary"><?=lang('submit')?></button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="convertOrderModal" tabindex="-1" role="dialog" aria-labelledby="convertOrderModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                    <span class="modal-title" id="convertOrderModalLabel" style="font-size: 150%;"><?=lang('convert_order');?></span>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 div_convert_order_doc_type">
                            <?=lang('convert_order_doc_type', 'convert_order_doc_type');?>
                            <select name="convert_order_doc_type" class="form-control" id="convert_order_doc_type"></select>
                        </div>
                        <div class="col-md-12 div_convert_order">
                            <?=lang('convert_order_as', 'convert_order_as');?>
                            <select name="convert_order_as" class="form-control" id="convert_order_as">
                                <option value="1"><?= lang('convert_order_as_person') ?></option>
                                <option value="2"><?= lang('convert_order_as_company') ?></option>
                            </select>
                        </div>
                        <div class="col-md-12 div_convert_order_as" style="display:none;">
                            <?=lang('convert_order_employer', 'convert_order_employer');?>
                            <input type="text" name="convert_order_employer" id="convert_order_employer" class="form-control" placeholder="<?= lang('select')." ".lang('customer') ?>" />
                        </div>
                        <div class="col-md-12 div_convert_order_as" style="display:none;">
                            <?=lang('convert_order_employer_branch', 'convert_order_employer_branch');?>
                            <select name="convert_order_employer_branch" id="convert_order_employer_branch" class="form-control">
                                <option value=""><?= lang('select')." ".lang('customer') ?></option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="convert_order_submit" class="btn btn-primary"><?=lang('submit')?></button>
                </div>
            </div>
        </div>
    </div>

    <div id="order_tbl"><span id="order_span"></span>
        <table id="order-table" class="prT table" style="margin-bottom:0;" width="100%"></table>
    </div>
    <div id="bill_tbl"><span id="bill_span"></span>
        <table id="bill-table" width="100%" class="prT table" style="margin-bottom:0;"></table>
        <table id="bill-total-table" class="prT table" style="margin-bottom:0;" width="100%"></table>
        <span id="bill_footer"></span>
    </div>

    <div class="modal fade in" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" aria-hidden="true" data-backdrop="static" data-keyboard="false"></div>

    <div class="modal fade in" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel2" aria-hidden="true" aria-hidden="true" data-backdrop="static" data-keyboard="false"></div>

    <div class="modal fade in" id="cbModal" tabindex="-1" role="dialog" aria-labelledby="cbModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                        <i class="fa fa-2x">&times;</i>
                    </button>
                    <h4 class="modal-title" id="cbModalLabel"><?=lang('customer_branch');?></h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <select name="customer_branch" id="poscustomer_branch" class="form-control">
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="updateCustomerBranch" class="btn btn-primary"><?=lang('update')?></button>
                </div>
            </div>
        </div>
    </div>

    <div id="modal-loading" style="display: none;">
        <div class="blackbg"></div>
        <div class="loader"></div>
    </div>

    <div class="modal fade in" id="sPUModal" tabindex="-1" role="dialog" aria-labelledby="sPUModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="modal-title product_name_spumodal" style="font-size: 150%;"><?=lang('suspended_sales');?></span>
                    <h3><?= lang('unit_prices') ?></h3>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 10%;"></th>
                                <th style="width: 35%;"><?= lang('unit') ?></th>
                                <th style="width: 35%;"><?= lang('price') ?></th>
                                <th style="width: 20%;"><?= lang('quantity') ?></th>
                                <th style="width: 35%;"><?= lang('Factor') ?></th>
                            </tr>
                        </thead>
                    </table>
                    <table class="table" id="unit_prices_table" style="width: 100%;">
                        <tbody>
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <div class="col-sm-3">
                        <?= lang('unit_quantity', 'unit_quantity') ?>
                        <input type="text" name="unit_quantity" id="unit_quantity" class="form-control">
                    </div>
                    <div class="col-sm-9">
                        <button class="btn btn-success send_item_unit_select" type="button"><?= lang('submit') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="pricesChanged" tabindex="-1" role="dialog" aria-labelledby="pricesChangedLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h3><?= lang('product_prices_changed') ?></h3>
                </div>
                <div class="modal-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th style="width: 35%;"><?= lang('product_name') ?></th>
                                <th style="width: 20%;"><?= lang('previous_product_price') ?></th>
                                <th style="width: 35%;"><?= lang('actual_product_price') ?></th>
                            </tr>
                        </thead>
                        <tbody id="prices_changed_table">

                        </tbody>
                    </table>
                </div>

                <div class="modal-footer">
                    <div class="col-sm-12">
                        <button class="btn btn-danger" id="pos_keep_prices" type="button"><?= lang('no_keep_prices') ?></button>
                        <button class="btn btn-success" id="update_prices" type="button"><?= lang('yes_update_prices') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade in" id="serialModal" tabindex="-1" role="dialog" aria-labelledby="serialModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h2 class="product_name_serialModal"></h2>
                    <h3><?= lang('ticket_data') ?></h3>
                </div>
                <div class="modal-body">
                    <form id="serialModal_form">
                        <input type="hidden" id="serialModal_product_id">
                        <input type="hidden" id="serialModal_option_id">
                        <div class="col-sm-12 form-group">
                            <?= lang('serial_no', 'serial') ?>
                            <input type="text" name="serialModal_serial" id="serialModal_serial" class="form-control" required>
                        </div>
                        <div class="col-sm-12 form-group">
                            <?= lang('meters', 'meters') ?>
                            <input type="text" name="serialModal_meters" id="serialModal_meters" class="form-control" required>
                        </div>
                    </form>
                </div>

                <div class="modal-footer">
                    <div class="col-sm-12 form-group">
                        <button class="btn btn-success continue_serial_modal" type="button"><?= lang('continue') ?></button>
                        <button class="btn btn-success send_serial_modal" type="button"><?= lang('submit') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>

	<div class="modal fade in" id="checkQuoteModal" tabindex="-1" role="dialog" aria-labelledby="checkQuoteLabel" aria-hidden="true">
	</div>

<?php $restobar = $this->pos_settings->restobar_mode; ?>
<?php if ($restobar == '1') { ?>
	<?php if (empty($sid)) { ?>
		<?php if($this->pos_settings->order_print_mode == 0) { ?>
			<?= form_open(admin_url('pos/create_suspended_sale'), 'id="formulario_crear_cuenta"'); ?>
				<?php
					if ($this->Owner || $this->Admin) {
			            $branch_id = $this->Settings->default_biller;
			        } else {
			            $branch_id = $this->session->biller_id;
			        }
				?>
				<input type="hidden" name="id_sucursal" id="id_sucursal" value="<?= $branch_id ?>">
			<?= form_close(); ?>
		<?php } ?>
	<?php } ?>
<?php } ?>

<?php unset($Settings->setting_id, $Settings->smtp_user, $Settings->smtp_pass, $Settings->smtp_port, $Settings->update, $Settings->reg_ver, $Settings->allow_reg, $Settings->default_email, $Settings->mmode, $Settings->timezone, $Settings->restrict_calendar, $Settings->restrict_user, $Settings->auto_reg, $Settings->reg_notification, $Settings->protocol, $Settings->mailpath, $Settings->smtp_crypto, $Settings->corn, $Settings->customer_group, $Settings->envato_username, $Settings->purchase_code);?>
	<script type="text/javascript">
	var site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url('/'), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>, pos_settings = <?=json_encode($pos_settings);?>;
	var lang = {
		unexpected_value: '<?=lang('unexpected_value');?>',
		select_above: '<?=lang('select_above');?>',
		r_u_sure: '<?=lang('r_u_sure');?>',
		r_u_sure_suspend_sale: '<?=lang('r_u_sure_suspend_sale');?>',
		bill: '<?=lang('bill');?>',
		order: '<?=lang('order');?>',
		total: '<?=lang('total');?>',
		items: '<?=lang('items');?>',
		discount: '<?=lang('discount');?>',
		order_tax: '<?=lang('order_tax');?>',
		grand_total: '<?=lang('grand_total');?>',
		total_payable: '<?=lang('total_payable');?>',
		rounding: '<?=lang('rounding');?>',
		merchant_copy: '<?=lang('merchant_copy');?>'
	};
	var items_suspended_sale = '<?= isset($inv_items) ? count($inv_items) : 0; ?>'
    var _docTypeOptions = '';
	</script>

	<script type="text/javascript">
		var product_variant = 0, shipping = 0, p_page = 0, per_page = 0, tcp = "<?=$tcp?>", pro_limit = <?= $pos_settings->pro_limit; ?>, brand_id = 0, obrand_id = 0, cat_id = "<?=$pos_settings->default_section?>", ocat_id = "<?=$pos_settings->default_section?>", sub_cat_id = 0, osub_cat_id, count = 1, an = 1, DT = <?=$Settings->default_tax_rate?>, product_tax = 0, invoice_tax = 0, product_discount = 0, order_discount = 0, total_discount = 0, total = 0, total_paid = 0, grand_total = 0, KB = <?=$pos_settings->keyboard?>, tax_rates =<?php echo json_encode($tax_rates); ?>, suspended_sale_items = 0, tip_amount = 0;
		var billers = <?= json_encode($posbillers); ?>,
			biller = <?= json_encode($posbiller); ?>,
			posbiller_id = <?= isset($posbiller_id) ? $posbiller_id : 0 ?>;
		var username = '<?=$this->session->userdata('username');?>',
			order_data = '',
			bill_data = '';
		function widthFunctions(e) {
			var wh = $(window).height(),
			ww = $(window).width(),

			lth = $('#left-top').height(),
			lbh = $('#left-bottom').height();
		}
		$(window).bind("resize", widthFunctions);
		localStorage.setItem('restobar_mode', '<?= $restobar; ?>');
		<?php if ($this->pos_settings->restobar_mode == YES && $this->pos_settings->table_service == YES && $this->pos_settings->order_to_table_default_restobar == YES) { ?>
			<?php if(!isset($sid)) { ?>
				localStorage.setItem('restobar_mode_module', 1);
				$pos_biller = (localStorage.getItem('posbiller') != null) ? localStorage.getItem('posbiller') : $('#posbiller').val();

				$('#myModal').modal({remote: site.base_url + 'pos/restobar_table_grid/'+ $pos_biller});
	        	$('#myModal').modal('show');
			<?php } ?>
		<?php } else { ?>
            // <?php if (isset($restobar_table) && $restobar_table) { ?>
                // localStorage.setItem('restobar_mode_module', 1);
		    // <?php } else { ?>
			    localStorage.setItem('restobar_mode_module', false);
		    // <?php } ?>
		<?php } ?>

		<?php if ($restobar == '1') { ?>
			window.onbeforeunload = function () {
				<?php if (! empty($sid)) { ?>
					if (localStorage.getItem('submit_form')) {
						localStorage.removeItem('submit_form');
					} else {
						if (suspended_sale_items == 0) {
			        		return "¿Seguro que quieres salir?";
						}
					}
		    	<?php } ?>
			};
			$(document).on('click', '#boton_mesa', function() {
				localStorage.setItem('restobar_mode_module', 1);
				$('#restobar_mode_module').val(1);
				$('#myModal').modal({remote: site.base_url + 'pos/restobar_table_grid/'+ $('#posbiller').val()});
            	$('#myModal').modal('show');
            	loadItems();
			});
			$(document).on('click', '#boton_domicilio', function() {
				localStorage.setItem('restobar_mode_module', 2);
				$('#restobar_mode_module').val(2);
				$('#restobar_general_container').html('<h3 style="color: #616162; font-size: 20px; margin-bottom: 20px; margin-left: 5px;">Pedido a Domicilio</h3>');
				$('#suspend').parent('.btn-pos').fadeOut();
				$('#print_order').parent('.btn-pos').fadeOut();
            	loadItems();
			});
			$(document).on('click', '#boton_recoger', function() {
				localStorage.setItem('restobar_mode_module', 3);
				$('#restobar_mode_module').val(3);
				$('#restobar_general_container').html('<h3 style="color: #616162; font-size: 20px; margin-bottom: 20px; margin-left: 5px;">Cliente recoge</h3>');
				$('#suspend').parent('.btn-pos').fadeOut();
				$('#print_order').parent('.btn-pos').fadeOut();
            	loadItems();
			});
		<?php } ?>

var pi = 'amount_1', pa = 2;

		$(document).ready(function () {
			cargar_codigo_forma_pago_fe();
			$('#view-customer').click(function(){
				$('#myModal').modal({remote: site.base_url + 'customers/view/' + $("input[name=customer]").val()});
				$('#myModal').modal('show');
			});
			$(document).on('change', '#paid_by_1', cargar_codigo_forma_pago_fe);

			function cargar_codigo_forma_pago_fe() {
		        var codigo_fe_forma_pago = $('#paid_by_1 option:selected').data('code_fe');
		        $('#mean_payment_code_fe').val(codigo_fe_forma_pago);
		    }
			$('textarea').keydown(function (e) {
				if (e.which == 13) {
					var s = $(this).val();
					$(this).val(s+'\n').focus();
					e.preventDefault();

					return false;
				}
			});
			<?php if ($sid || isset($quote_id)) { ?>
				<?php if (!empty($items)) { ?>

		            positems = JSON.parse('<?= $items ?>');
		            $.each(positems, function(index, item){
		                positems[index].preferences_selected = Object.assign([], positems[index].preferences_selected);
		            });
		            localStorage.setItem('positems', JSON.stringify(positems));

				<?php } else { ?>
					// localStorage.setItem('positems', '');
				<?php } ?>
			<?php } ?>
			<?php if ($oid) { ?>
				<?php if (!empty($items)) { ?>
					positems = JSON.parse('<?= $items ?>');
		            $.each(positems, function(index, item){
		                positems[index].preferences_selected = Object.assign([], positems[index].preferences_selected);
		            });
		            localStorage.setItem('positems', JSON.stringify(positems));
				<?php } ?>
			<?php } ?>
			<?php if ($this->session->userdata('remove_slls') || $this->session->userdata('remove_resl')) {?>
	            if (localStorage.getItem('slitems')) {
	                localStorage.removeItem('slitems');
	            }
	            if (localStorage.getItem('sldiscount')) {
	                localStorage.removeItem('sldiscount');
	            }
	            if (localStorage.getItem('sltax2')) {
	                localStorage.removeItem('sltax2');
	            }
	            if (localStorage.getItem('slref')) {
	                localStorage.removeItem('slref');
	            }
	            if (localStorage.getItem('slshipping')) {
	                localStorage.removeItem('slshipping');
	            }
	            if (localStorage.getItem('slwarehouse')) {
	                localStorage.removeItem('slwarehouse');
	            }
	            if (localStorage.getItem('slnote')) {
	                localStorage.removeItem('slnote');
	            }
	            if (localStorage.getItem('slinnote')) {
	                localStorage.removeItem('slinnote');
	            }
	            if (localStorage.getItem('slcustomer')) {
	                localStorage.removeItem('slcustomer');
	            }
	            if (localStorage.getItem('slbiller')) {
	                localStorage.removeItem('slbiller');
	            }
	            if (localStorage.getItem('slcurrency')) {
	                localStorage.removeItem('slcurrency');
	            }
	            if (localStorage.getItem('sldate')) {
	                localStorage.removeItem('sldate');
	            }
	            if (localStorage.getItem('slsale_status')) {
	                localStorage.removeItem('slsale_status');
	            }
	            if (localStorage.getItem('slpayment_status')) {
	                localStorage.removeItem('slpayment_status');
	            }
	            if (localStorage.getItem('paid_by')) {
	                localStorage.removeItem('paid_by');
	            }
	            if (localStorage.getItem('amount_1')) {
	                localStorage.removeItem('amount_1');
	            }
	            if (localStorage.getItem('paid_by_1')) {
	                localStorage.removeItem('paid_by_1');
	            }
	            if (localStorage.getItem('pcc_holder_1')) {
	                localStorage.removeItem('pcc_holder_1');
	            }
	            if (localStorage.getItem('pcc_type_1')) {
	                localStorage.removeItem('pcc_type_1');
	            }
	            if (localStorage.getItem('pcc_month_1')) {
	                localStorage.removeItem('pcc_month_1');
	            }
	            if (localStorage.getItem('pcc_year_1')) {
	                localStorage.removeItem('pcc_year_1');
	            }
	            if (localStorage.getItem('pcc_no_1')) {
	                localStorage.removeItem('pcc_no_1');
	            }
	            if (localStorage.getItem('cheque_no_1')) {
	                localStorage.removeItem('cheque_no_1');
	            }
	            if (localStorage.getItem('slpayment_term')) {
	                localStorage.removeItem('slpayment_term');
	            }
	            if (localStorage.getItem('othercurrencycode')) {
	                localStorage.removeItem('othercurrencycode');
	            }
	            if (localStorage.getItem('othercurrencytrm')) {
	                localStorage.removeItem('othercurrencytrm');
	            }
	            if (localStorage.getItem('slpayment_term')) {
	                localStorage.removeItem('slpayment_term');
	            }
	            if (localStorage.getItem('slordersale')) {
	                localStorage.removeItem('slordersale');
	            }
	            if (localStorage.getItem('slcustomerspecialdiscount')) {
	                localStorage.removeItem('slcustomerspecialdiscount');
	            }
	            if (localStorage.getItem('posretenciones')) {
	                localStorage.removeItem('posretenciones');
	            }
	            if (localStorage.getItem('keep_prices')) {
	                localStorage.removeItem('keep_prices');
	            }
	            if (localStorage.getItem('keep_prices_quote_id')) {
	                localStorage.removeItem('keep_prices_quote_id');
	            }
	            if (localStorage.getItem('price_updated')) {
	                localStorage.removeItem('price_updated');
	            }
	        <?php
	            $this->sma->unset_data('remove_slls');
	            $this->sma->unset_data('remove_resl');
	        }
	        ?>
			widthFunctions();
			//Configuración localStorage datos precargados
			<?php if ($new_customer_added) { ?>
    			localStorage.setItem('poscustomer', '<?= $new_customer_added ?>');
			<?php } ?>
			<?php if ($suspend_sale) {?>
			if (site.settings.loaded_suspended_sale_validate_prices == 0) {
    			localStorage.setItem('pos_keep_prices', true);
    			localStorage.setItem('pos_keep_prices_quote_id', $('#quote_id').val());
			}
			localStorage.setItem('postax2', '<?=$suspend_sale->order_tax_id;?>');
			localStorage.setItem('posdiscount', '<?=$suspend_sale->order_discount_id;?>');
			localStorage.setItem('poswarehouse', '<?=$suspend_sale->warehouse_id;?>');
			localStorage.setItem('poscustomer', '<?=$suspend_sale->customer_id;?>');
			localStorage.setItem('posseller', '<?=$suspend_sale->seller_id;?>');
			localStorage.setItem('posbiller', '<?=$suspend_sale->biller_id;?>');
			localStorage.setItem('posshipping', '<?=$suspend_sale->shipping;?>');
			<?php }
			?>
			<?php if ($old_sale) {?>
			localStorage.setItem('postax2', '<?=$old_sale->order_tax_id;?>');
			localStorage.setItem('posdiscount', '<?=$old_sale->order_discount_id;?>');
			localStorage.setItem('poswarehouse', '<?=$old_sale->warehouse_id;?>');
			localStorage.setItem('poscustomer', '<?=$old_sale->customer_id;?>');
			localStorage.setItem('posbiller', '<?=$old_sale->biller_id;?>');
			localStorage.setItem('posseller', '<?=$old_sale->seller_id;?>');
			localStorage.setItem('posshipping', '<?=$old_sale->shipping;?>');
			<?php }
			?>
			<?php if ($quote) {?>
			localStorage.setItem('postax2', '<?=$quote->order_tax_id;?>');
			localStorage.setItem('posdiscount', '<?= $quote->total_discount ?>');
			localStorage.setItem('poswarehouse', '<?=$quote->warehouse_id;?>');
			localStorage.setItem('poscustomer', '<?=$quote->customer_id;?>');
			localStorage.setItem('posbiller', '<?=$quote->biller_id;?>');
			localStorage.setItem('posseller', '<?=$quote->seller_id;?>');
			localStorage.setItem('posshipping', '<?=$quote->shipping;?>');
			localStorage.setItem('poscustomer_branch', '<?=$quote->address_id;?>');
			localStorage.setItem('pospaid_by', '<?=isset($quote->payment_method) ? $quote->payment_method : false;?>');
			is_quote_paid_by = '<?=isset($quote->payment_method) ? $quote->payment_method : false;?>';
			<?php }
			?>

			<?php if (isset($duplicate_sale)): ?>
				is_duplicate = true;
				if (site.settings.loaded_suspended_sale_validate_prices == 0) {
	    			localStorage.setItem('pos_keep_prices', true);
	    			localStorage.setItem('pos_keep_prices_quote_id', "<?= $oid ?>");
				}
			<?php endif ?>
			<?php if (isset($order)): ?>
				if (site.settings.loaded_suspended_sale_validate_prices == 0) {
	    			localStorage.setItem('pos_keep_prices', true);
	    			localStorage.setItem('pos_keep_prices_quote_id', "<?= $quote_id ?>");
				}
			<?php endif ?>
			<?php if ($this->session->userdata('biller_id')) { ?>
				// localStorage.setItem('posbiller', '<?=$this->session->userdata('biller_id');?>');
			<?php } ?>
			if (!localStorage.getItem('postax2')) {
			localStorage.setItem('postax2', <?=$Settings->default_tax_rate2;?>);
			}
			$('.select').select2({minimumResultsForSearch: 7});

			$('#poscustomer').val(localStorage.getItem('poscustomer')).select2({
				minimumInputLength: 1,
				data: [],
				initSelection: function (element, callback) {
					$.ajax({
						type: "get", async: false,
						url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
						dataType: "json",
						success: function (data) {
							callback(data[0]);
						}
					});
				},
				ajax: {
					url: site.base_url + "customers/suggestions",
					dataType: 'json',
					quietMillis: 15,
					data: function (term, page) {
						return {
							term: term,
							limit: 10
						};
					},
					results: function (data, page) {
						if (data.results != null) {
							return {results: data.results};
						} else {
							return {results: [{id: '', text: lang.no_match_found}]};
						}
					}
				}
			});
			setTimeout(function() {
				$('#poscustomer').trigger('change');
			}, 850);
			get_customer_branches();
			$('#poscustomer').on('select2-opening', function () {
				if (pos_settings.show_client_modal_on_select == 1) {
					localStorage.setItem('poscustomer_manual_click', 1);
				}
			});
if (KB) {
display_keyboards();
var result = false, sct = '';
$('#poscustomer').on('select2-opening', function () {
	if (pos_settings.show_client_modal_on_select == 1) {
		localStorage.setItem('poscustomer_manual_click', 1);
	}
	sct = '';
	$('.select2-input').addClass('kb-text');
	display_keyboards();
	$('.select2-input').bind('change.keyboard', function (e, keyboard, el) {
		if (el && el.value != '' && el.value.length > 0 && sct != el.value) {
			sct = el.value;
		}
		if(!el && sct.length > 0) {
			$('.select2-input').addClass('select2-active');
			setTimeout(function() {
				$.ajax({
					type: "get",
					async: false,
					url: "<?=admin_url('customers/suggestions')?>/?term=" + sct,
					dataType: "json",
					success: function (res) {
						if (res.results != null) {
							$('#poscustomer').select2({data: res}).select2('open');
							$('.select2-input').removeClass('select2-active');
						} else {
							// bootbox.alert('no_match_found');
							$('#poscustomer').select2('close');
							$('#test').click();
						}
					}
				});
			}, 500);
		}
	});
});
$('#poscustomer').on('select2-close', function () {
	$('.select2-input').removeClass('kb-text');
	$('#test').click();
	$('select, .select').select2('destroy');
	$('select, .select').select2({minimumResultsForSearch: 7});
});
$(document).bind('click', '#test', function () {
	// var kb = $('#test').keyboard().getkeyboard();
	// kb.close();
});
}
localStorage.removeItem('prev_posbiller');
$(document).on('select2-open', '#posbiller', function(){
	localStorage.setItem('prev_posbiller', $(this).val());
});
$(document).on('change', '#posbiller', function(){
	if (!$(this).val()) {
		return false;
	}
	prev_biller = localStorage.getItem('prev_posbiller');
	if (prev_biller == $('#posbiller').val()) {
		return false;
	} else {
		localStorage.setItem('prev_posbiller', $(this).val());
	}
	if (prev_biller === null) {
		prev_biller = $(this).val();
	}
	$('#biller').val($(this).val());
	bdata = billers_data[$(this).val()];
	protect_delete = 0;
	if ((bdata.pin_code_method == 1 && bdata.pin_code) || bdata.pin_code_method == 2) {
		protect_delete = 1;
	}
	protect_delete_pin_request = bdata.pin_code_request;
    var posBillerId = $('#posbiller').val();
    dt_default = $('#posbiller option:selected').data('posdocumenttypedefault');
    arr_docs = [];
    <?php if ($this->session->userdata('pos_document_type_id')): ?>
    	arr_docs.push('<?= $this->session->userdata('pos_document_type_id') ?>');
    <?php endif ?>
    <?php if ($this->session->userdata('fe_pos_document_type_id')): ?>
    	arr_docs.push('<?= $this->session->userdata('fe_pos_document_type_id') ?>');
    <?php endif ?>

        $.ajax({
        url:'<?= admin_url("billers/getBillersDocumentTypesAjax/") ?>',
        type:'POST',
        dataType:'JSON',
        data : {
            "<?=$this->security->get_csrf_token_name()?>" : "<?=$this->security->get_csrf_hash()?>",
            module : 1,
            billerId : posBillerId,
            // arr_docs : arr_docs,
        }
        }).done(function(data){
            response = data;
            _docTypeOptions = response.options;
            optionsHtml = ''
            optionsHtmlFe = ''

            const options = $(response.options)
            $.each(options, function (i, option) {
                if ($(option).data('fe') == 0) {
                    optionsHtml += $(option).prop('outerHTML')
                } else if ($(option).data('fe') == 1) {
                    optionsHtmlFe += $(option).prop('outerHTML')
                }
            })

            <?php if ($this->session->userdata('group_id') == 1 || $this->session->userdata('group_id') == 2) { ?>
                optionsTrue = response.options
            <?php } else { ?>
                optionsTrue = (optionsHtmlFe != '') ? optionsHtmlFe : optionsHtml
            <?php } ?>

            $('#doc_type_id').html(optionsTrue).select2(); // primera llamada en cambio de posbiller
            resizeWindow();
            if (response.not_parametrized != "") {
                command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            }
            if (dt_default && $('#doc_type_id option[value="'+dt_default+'"]').length > 0) {
            // Establecer la opción con valor "2" como valor por defecto
            $('#doc_type_id').select2('val', dt_default);
            }
            $('#doc_type_id').trigger('change');
        if (response.status == 0) {
            $('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>");
        } else {
            $('.resAlert').css('display', 'none');
        }
            $(document).on('change', '#doc_type_id', function(){
                if (site.pos_settings.activate_electronic_pos == 0) {
                    localStorage.setItem('docu_type_selected', $(this).val());
                }
            });
            if (localStorage.getItem('docu_type_selected')) {
                $('#doc_type_id').select2('val', localStorage.getItem('docu_type_selected'));
            }
        });

	$.ajax({
        url:'<?= admin_url("billers/getBillersDocumentTypes/19/") ?>'+$('#posbiller').val(),
        type:'get',
        dataType:'JSON'
    }).done(function(data){
		response = data;
		$('#payment_reference_no').html(response.options).select2();
		if (response.status == 0) {
		$('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('payment_biller_without_documents_types') ?></div></div>");
		}
		if ($('#payment_reference_no option').length > 1) {
			$('.div_payment_reference').fadeIn();
		} else {
      		setTimeout(function() {
      			$('#payment_reference_no').trigger('change');
      		}, 850);
		}
    });
	$.ajax({
	    url : site.base_url+"sales/getSellers",
	    type : "get",
	    data : {"biller_id" : $('#posbiller').val()}
	}).done(function(data){
	    if (data != false) {
	        $('#posseller').html(data).trigger('change');
	        if (posseller = localStorage.getItem('posseller')) {
                $('#posseller').select2('val', posseller).trigger('change');
            } else if (user_seller_id) {
                $('#posseller').select2('val', user_seller_id).trigger('change');
            }
	    } else {
	        $('#posseller').select2('val', '').trigger('change');
	    }
	}).fail(function(data){
	    console.log(data);
	});

	<?php if ($sid || $oid) {?>
		var customer = localStorage.getItem('poscustomer');
		var warehouse = localStorage.getItem('poswarehouse');
		var seller = localStorage.getItem('posseller');
	<?php } else { ?>
		<?php if ($new_customer_added): ?>
			var customer = localStorage.getItem('poscustomer');
		<?php else: ?>
			if (
				(localStorage.getItem('poscustomer') &&
								localStorage.getItem('poscustomer') != $('#posbiller option:selected').data('customerdefault') &&
								localStorage.getItem('posbiller') &&
								localStorage.getItem('posbiller') != posBillerId ) ||
				!localStorage.getItem('poscustomer')
				) {
				var customer = $('#posbiller option:selected').data('customerdefault');
				localStorage.setItem('poscustomer', customer);
			}
			<?php if ($customer): ?>
				if (customer == 0) {
					customer = <?= $customer->id;?>;
					localStorage.setItem('poscustomer', customer);
				}
			<?php endif ?>
		<?php endif ?>
		var warehouse = session_user_data.warehouse_id !== null && session_user_data.warehouse_id > 0 ? session_user_data.warehouse_id : $('#posbiller option:selected').data('warehousedefault');
		var seller = $('#posbiller option:selected').data('sellerdefault');
	<?php } ?>
	var pricegroup = $('#posbiller option:selected').data('pricegroupdefault');
	if (customer != '' && customer !== undefined && $('#poscustomer').val() != customer) {
		$('#poscustomer').val(customer).select2({
				minimumInputLength: 1,
				data: [],
				initSelection: function (element, callback) {
					$.ajax({
						type: "get", async: false,
						url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
						dataType: "json",
						success: function (data) {
							callback(data[0]);
						}
					});
				},
				ajax: {
					url: site.base_url + "customers/suggestions",
					dataType: 'json',
					quietMillis: 15,
					data: function (term, page) {
						return {
							term: term,
							limit: 10
						};
					},
					results: function (data, page) {
						if (data.results != null) {
							return {results: data.results};
						} else {
							return {results: [{id: '', text: lang.no_match_found}]};
						}
					}
				}
				});
        setTimeout(function() {
        	$('#poscustomer').trigger('change');
        	// get_customer_branches();
        }, 1200);
	}
	if (warehouse != '') {
        $('#poswarehouse option').each(function(index, option){
            option = $(this);
            option.prop('disabled', false);
        });
        $('#poswarehouse').select2('val', warehouse);
	}
	if (localStorage.getItem('posseller')) {
		seller = localStorage.getItem('posseller');
	}
	if (seller != '' && !user_seller_id) {
	    setTimeout(function() {
	        $('#posseller').select2('val', seller).trigger('change');
	    }, 800);
	    setTimeout(function() {
	    	if (!$('#posseller').val()) {
	        	$('#posseller').select2('val', seller).trigger('change');
	    	}
	    }, 1500);
	}
	if (site.pos_settings.default_section == 1001) {
		defaultcategory = site.pos_settings.pos_navbar_default_category;
	} else {
		var defaultcategory = $('#posbiller option:selected').data('defaultcategory') > 0 && $('#posbiller option:selected').data('defaultcategory') !== 1 ? $('#posbiller option:selected').data('defaultcategory') : site.pos_settings.default_section;
	}
	if (defaultcategory == 998) {
		$('.open-promotions').addClass('wappsi_cat_active');
		$('.open-category, .open-subcategory, .open-favorites, .open-featured').removeClass('wappsi_cat_active');
	} else if (defaultcategory == 999) {
		$('.open-favorites').addClass('wappsi_cat_active');
		$('.open-category, .open-promotions, .open-subcategory, .open-featured').removeClass('wappsi_cat_active');
	} else if (defaultcategory == 997) {
		$('.open-featured').addClass('wappsi_cat_active');
		$('.open-promotions, .open-subcategory, .open-favorites, .open-category').removeClass('wappsi_cat_active');
	} else if (defaultcategory == 900) {
		$('.open-category').addClass('wappsi_cat_active');
		$('.open-promotions, .open-subcategory, .open-favorites, .open-featured').removeClass('wappsi_cat_active');
	} else {
		$('.open-promotions, .open-subcategory, .open-favorites, .open-featured').removeClass('wappsi_cat_active');
	}
	$.ajax({
		type: "get",
		url: "<?=admin_url('pos/ajaxcategorydata');?>",
		data: {
				category_id: defaultcategory,
				biller_id : $('#posbiller').val(),
				warehouse_id : $('#poswarehouse').val(),
			},
		dataType: "json",
		success: function (data) {
			$('#item-list').empty();
			var newPrs = $('<div></div>');
			newPrs.html(data.products);
			newPrs.appendTo("#item-list");
			$('#subcategory-list').empty();
			var newScs = $('<div></div>');
			newScs.html(data.subcategories);
			newScs.appendTo("#subcategory-list");
			tcp = data.tcp;
			nav_pointer();
		}
	}).done(function () {
		p_page = 'n';
		$('#category-' + cat_id).addClass('active');
		$('#category-' + ocat_id).removeClass('active');
		ocat_id = cat_id;
		$('#modal-loading').hide();
		nav_pointer();
	});

	if (formatDecimal($('#posbiller option:selected').data('autoica')) > 0) {
        $('#rete_ica').prop('disabled', true);
        command: toastr.warning('Sucursal con Auto retención de ICA definida', '¡Atención!', {
            "showDuration": "1200",
            "hideDuration": "1000",
            "timeOut": "12000",
            "extendedTimeOut": "1000",
        });
    } else {
        $('#rete_ica').prop('disabled', false);
    }
    set_biller_warehouses_related();

	positems = localStorage.getItem('positems') ? JSON.parse(localStorage.getItem('positems')) : [];
	if (positems) {
	    $.each(positems, function(id, arr){
	        wappsiPreciosUnidad(positems[id].row.id,id,positems[id].row.qty);
	    });
	}
	localStorage.setItem('posbiller', posBillerId);
	set_payment_method_billers();
}); //fin change posbiller

<?php for ($i = 1; $i <= 5; $i++) {?>
$('#paymentModal').on('change', '#amount_<?=$i?>', function (e) {
	$('#amount_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('blur', '#amount_<?=$i?>', function (e) {
	$('#amount_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#paid_by_<?=$i?>', function (e) {
	$('#paid_by_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#pcc_no_<?=$i?>', function (e) {
	$('#cc_no_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#pcc_holder_<?=$i?>', function (e) {
	$('#cc_holder_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#gift_card_no_<?=$i?>', function (e) {
	$('#paying_gift_card_no_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#pcc_month_<?=$i?>', function (e) {
	$('#cc_month_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#pcc_year_<?=$i?>', function (e) {
	$('#cc_year_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#pcc_type_<?=$i?>', function (e) {
	$('#cc_type_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#pcc_cvv2_<?=$i?>', function (e) {
	$('#cc_cvv2_val_<?=$i?>').val($(this).val());

});
$('#paymentModal').on('change', '#cheque_no_<?=$i?>', function (e) {
	$('#cheque_no_val_<?=$i?>').val($(this).val());
});
$('#paymentModal').on('change', '#payment_note_<?=$i?>', function (e) {
	$('#payment_note_val_<?=$i?>').val($(this).val());
});
<?php }
?>

$(document).on('change', '#frequency', function() {
    getAmountInstallments($(this))
    $('#frequencyHidden').val($(this).val())
})
$(document).on('change', '#installment', function() {
    $('#installmentHidden').val($(this).val())
})

$('#payment').click(function () {
	$('#paid_by_1').select2('readonly', true);
	if (!$('#seller_id').val()) {
		if (!$('#posseller').val()) {
			header_alert('error', 'El vendedor es obligatorio');
			$('#posseller').select2('open');
			return false;
		} else {
			$('#seller_id').val($('#posseller').val());
		}
	}
	if ($('#payment').attr('disabled')) {
		return false;
	}
	set_customer_payment();
	$('#sale_note').trigger('blur');
	if (validate_existing_selected_table()) {
		$('.submit-sale').html('<i class="fa fa-spinner fa-spin"></i>  <?= lang('loading') ?>').attr('disabled', true);
		<?php if ($sid || isset($quote_id)) {?>
			suspend = $('<span></span>');
			suspend.html('<input type="hidden" name="delete_id" id="delete_id" value="<?php echo isset($sid) ? $sid : $quote_id; ?>" />');
			suspend.appendTo("#hidesuspend");
		<?php }
		?>
		if ($('#rete_applied').val()) {
			total_rete_amount = $('#total_rete_amount').text();
			total_rete_amount = parseFloat(formatDecimal(total_rete_amount));
		} else {
			total_rete_amount = 0;
		}
		var twt = formatDecimal((total + invoice_tax) - order_discount + shipping - total_rete_amount + tip_amount);
		if (count == 1) {
			bootbox.alert('<?=lang('x_total');?>');
			return false;
		}
		gtotal = formatDecimal(twt);
		<?php if ($pos_settings->rounding) {?>
			round_total = roundNumber(gtotal, <?=$pos_settings->rounding?>);
			var rounding = formatDecimal(0 - (gtotal - round_total));
			$('#twt').text("$ "+formatMoney(round_total) + ' (' + formatMoney(rounding) + ')');
			$('#quick-payable').text(round_total);
		<?php } else {?>
			$('#twt').text("$ "+formatMoney(gtotal));
			$('#quick-payable').text(gtotal);
		<?php }
		?>
		$('#item_count').text(formatDecimal(count - 1));
		if (site.pos_settings.express_payment == 0) {
        showHideAlertFE($('#subtotal').text());
		}

		if (!$('#document_type_id').val()) {
			if (showDocTypeInput) {
				if (!$('#doc_type_id').val()) {
					header_alert('error', 'Sin tipo de documento de venta configurado, por favor verifique asignación del mismo en sucursal');
					$('#doc_type_id').select2('open');
					return false;
				} else {
					$('#document_type_id').val($('#doc_type_id').val());
				}
			}
				
		}
		$('#paymentModal').appendTo("body").modal('show');
		setTimeout(function() {
			$('#quick-payable').click();
			$('#amount_1').select();
		}, 850);

		//CUSTOMER PAYMENT TERM
		// $.ajax({
		// 	type: "get", async: false,
		// 	url: site.base_url + "pos/payment_term_expire/" + $('#poscustomer').val() + "/1",
		// 	success: function (data) {
		// 		if (data == true) {
		// 			$('.alertPaymentTerm').remove();
		// 			mensaje = "El cliente seleccionado tiene facturas sin pagar y con el plazo de pago vencido.";
		// 			$('.pos').append("<div class=\"alert alert-danger alertPaymentTerm\"><button type=\"button\" class=\"close fa-2x\" data-dismiss=\"alert\">&times;</button>"+mensaje+"</div>");
		// 		}
		// 	}
		// });

		pospaid_by = localStorage.getItem('pospaid_by');

		if (!pospaid_by && pos_settings.express_payment == 0) {
			var msg = "";
			var giftcard_balance = deposit_amount = 0;
			if (localStorage.getItem('poscustomerdepositamount') &&  deposit_payment_method.sale == true) {
				deposit_amount = parseFloat(localStorage.getItem('poscustomerdepositamount'));
				if (deposit_amount > 0) {
					msg += formatMoney(deposit_amount)+" de saldo de anticipos ";
				}
			}
			if (localStorage.getItem('poscustomergiftcardbalance')) {
				giftcard_balance = parseFloat(localStorage.getItem('poscustomergiftcardbalance'));
				if (giftcard_balance > 0) {
					msg += formatMoney(giftcard_balance)+" de saldo de <?= lang('gift_card') ?> ";
				}
			}

			if (deposit_amount > 0 && giftcard_balance > 0) {
				bootbox.dialog({
                    title: "Cliente con Saldo de <?= lang('deposit') ?> o <?= lang('gift_card') ?>",
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    size: 'x2',
                    onEscape: false,
                    backdrop: true,
                    buttons: {
                        deposit: {
                            label: '<?= lang('deposit') ?>',
                            className: 'btn-success btn-full',
                            callback: function(){
                            	if (gtotal > deposit_amount) {
		                       		$('#amount_1').val(deposit_amount).trigger('change');
		                       		$('.addButton').trigger('click');
		                       		$('#amount_2').val(gtotal - deposit_amount).trigger('change');
		                       } else {
		                        	$('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
		                       }
		                       $('#paid_by_1').select2('val', 'deposit').trigger('change');
		                       setTimeout(function() {
		                       		$('#amount_1').trigger('change');
		                       		calculateTotals();
		                        }, 850);
                            }
                        },
                        giftcard: {
                            label: '<?= lang('gift_card') ?>',
                            className: 'btn-success btn-full',
                            callback: function(){
                            	set_giftcard_payments(gtotal);
                            }
                        },
                        cancel: {
                            label: '<?= lang('cancel') ?>',
                            className: 'btn-danger btn-full',
                            callback: function(){
                                $('#amount_1').trigger('change');
		                        $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
		                        setTimeout(function() {
		                       		$('#amount_1').trigger('change');
		                       		calculateTotals();
		                        }, 850);
                            }
                        }
                    }
                });
			} else if (deposit_amount > 0) {
				bootbox.dialog({
                    title: "Cliente con Saldo de <?= lang('deposit') ?>",
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    size: 'x2',
                    onEscape: false,
                    backdrop: true,
                    buttons: {
                        deposit: {
                            label: '<?= lang('deposit') ?>',
                            className: 'btn-success btn-full',
                            callback: function(){
                           		if (gtotal > deposit_amount) {
									$('#amount_1').val(deposit_amount);
									$('.addButton').trigger('click');
								} else {
									$('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
								}
								setTimeout(function() {
									$('#paid_by_1').select2('val', 'deposit').trigger('change');
									$('#amount_1').trigger('change');
									calculateTotals();
								}, 2000);
                            }
                        },
                        cancel: {
                            label: '<?= lang('cancel') ?>',
                            className: 'btn-danger btn-full',
                            callback: function(){
                                $('#amount_1').trigger('change');
		                        $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
		                        setTimeout(function() {
		                       		$('#amount_1').trigger('change');
		                       		calculateTotals();
		                        }, 850);
                            }
                        }
                    }
                });
			} else if (giftcard_balance > 0) {
				bootbox.dialog({
                    title: "Cliente con Saldo de <?= lang('gift_card') ?>",
                    message: "El cliente cuenta con "+msg+", ¿Desea usar este medio de pago para esta venta?",
                    size: 'x2',
                    onEscape: false,
                    backdrop: true,
                    buttons: {
                        giftcard: {
                            label: '<?= lang('gift_card') ?>',
                            className: 'btn-success btn-full',
                            callback: function(){
                            	set_giftcard_payments(gtotal);
                            }
                        },
                        cancel: {
                            label: '<?= lang('cancel') ?>',
                            className: 'btn-danger btn-full',
                            callback: function(){
                                $('#amount_1').trigger('change');
		                        $('#paymentModal').css('display', 'block').css('overflow-y', 'auto');
		                        setTimeout(function() {
		                       		$('#amount_1').trigger('change');
		                       		calculateTotals();
		                        }, 850);
                            }
                        }
                    }
                });
			}
		} else {
			if (pospaid_by) {
				$('#paid_by_1').select2('val', pospaid_by);
			}
		}
		// if (site.pos_settings.mode_electronic_pos == 2) {
		// 	setTimeout(function() {
		// 		dt_default = $('#posbiller option:selected').data('posdocumenttypedefault');
		// 	    if (dt_default && $('#doc_type_id option[value="'+dt_default+'"]').length > 0) {
		// 		  // Establecer la opción con valor "2" como valor por defecto
		// 		  $('#doc_type_id').select2('val', dt_default);
		// 		}
		// 	}, 550);
		// }
		//acá parámetro efectivo express
		if (pos_settings.express_payment == 1) {
			lock_submit = JSON.parse(localStorage.getItem('lock_submit'));
		    lock_submit_by_margin = JSON.parse(localStorage.getItem('lock_submit_by_margin'));
		    lock_submit_by_min_sale_amount = JSON.parse(localStorage.getItem('lock_submit_by_min_sale_amount'));
		    if (lock_submit == false && lock_submit_by_margin == false && lock_submit_by_min_sale_amount == false) {
	       		$('#amount_1').prop('readonly', true);
				setTimeout(function() {
					set_submit_text();
	           		$('#amount_1').trigger('change');
	           		calculateTotals();
	            }, 850);
	            setTimeout(function() {
	           		$('.submit-sale').trigger('click');
	            }, 1200);
		    } else {
	            setTimeout(function() {
	           		$('.submit-sale').trigger('click');
	            }, 850);
		    }
		}
	}
	setTimeout(function() {
		$('#payment_term').val(1);
	}, 1000);
	setTimeout(function() {
	}, 1100);


}); //FIN CLICK #PAYMENT

$('#doc_type_id').on('change', function(){
    if ($(this).val() != '') {
        $('#document_type_id').val($(this).val());
        validar_resolucion_biller();
        validate_key_log();
        validate_fe_customer_data();

        <?php if ($this->pos_settings->activate_electronic_pos == YES && $this->pos_settings->mode_electronic_pos == 2) : ?>
            if ($('#doc_type_id option:selected').data('fe') == 1) {
                $('#limitExceededElertElectronicPos').fadeIn();
            } else {
                $('#limitExceededElertElectronicPos').fadeOut();
            }
        <?php endif ?>
    }
});
function validar_resolucion_biller(){
  $('.alertResolucion').remove();

   lock_submit = JSON.parse(localStorage.getItem('lock_submit'));
   lock_submit_by_margin = JSON.parse(localStorage.getItem('lock_submit_by_margin'));
   lock_submit_by_min_sale_amount = JSON.parse(localStorage.getItem('lock_submit_by_min_sale_amount'));

   if (lock_submit == false && lock_submit_by_margin == false && lock_submit_by_min_sale_amount == false) {
   	$('.submit-sale').fadeIn();
   } else {
   	$('.submit-sale').fadeOut();
   }

  //VALIDAR RESOLUCIÓN
  $.ajax({
    type: "get", async: false,
    url: site.base_url + "sales/validate_biller_resolution/"+$('#posbiller').val()+"/"+$('#doc_type_id').val(),
    success: function (data) {
      if (data != false) {
        // $('.resAlert').css('display', '');

        response = JSON.parse(data);
		// mensaje = response.mensaje;
		// $('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button>"+mensaje+"</div></div>");
        swal({
            title: '<?= $this->lang->line('toast_alert_title') ?>',
            text: response.mensaje,
            type: 'warning',
            showCancelButton: true,
            showConfirmButton: false,
            cancelButtonText: "Ok"
        });

		if (response.disable == true) {
			$('.submit-sale').attr('disabled', true).css('display', 'none');
		}

		// bootbox.alert('<h4>'+mensaje+'</h4>', function(){
		// 		command: toastr.error(mensaje, 'Mensaje de validación', { onHidden : function(){ } });
		// });
      } else {
          $('.resAlert').css('display', 'none');
		  $('#doc_type_error').css('display', 'none');
      }
    }
  });
  //VALIDAR RESOLUCIÓN
}

$('#paymentModal').on('show.bs.modal', function(e) {
	lock_submit = JSON.parse(localStorage.getItem('lock_submit'));
    lock_submit_by_margin = JSON.parse(localStorage.getItem('lock_submit_by_margin'));
    lock_submit_by_min_sale_amount = JSON.parse(localStorage.getItem('lock_submit_by_min_sale_amount'));
	if (pos_settings.express_payment == 0 && lock_submit == false && lock_submit_by_margin == false && lock_submit_by_min_sale_amount == false) {
		setTimeout(function() {
			set_submit_text();
		}, 6000);
	}
	if (lock_submit_by_margin && protect_delete == 1 && (protect_delete_pin_request == 3 || protect_delete_pin_request == 1)) {
		setTimeout(function() {
	        var boxd = bootbox.dialog({
	            title: "<i class='fa fa-key'></i> Código de autorización",
	            message: 'La venta se encuentra bloqueada por que existen márgenes inválidos en los precios de algunos productos, ingrese el código de autorización para desbloquear </br></br><input id="pos_pin" name="pos_pin" type="password" placeholder="Código de autorización" class="form-control"> ',
	            buttons: {
	                success: {
	                    label: "<i class='fas fa-unlock-alt'></i> Desbloquear",
	                    className: "btn-success verify_pin",
	                    callback: function () {
	                        var pos_pin = md5($('#pos_pin').val());
	                        if(pos_pin == biller_pin_code) {
								set_submit_text();
								$('.margin_alert').fadeOut();
								$('#amount_1').trigger('change');
	                        } else {
	                            bootbox.alert('Código de autorización incorrecto');
	                        }
	                    }
	                }
	            }
	        });
		}, 1100);
	} else if (!lock_submit && !lock_submit_by_min_sale_amount && (lock_submit_by_margin && (protect_delete == 1 && protect_delete_pin_request == 2) || !protect_delete)) {
		set_submit_text();
		$('.margin_alert').fadeOut();
		$('#amount_1').trigger('change');
	} else {
		$('#amount_1').focus();
		$('#quick-payable').click();
	}
});
$(document).on('click', '.quick-cash', function () {
if ($('#quick-payable').find('span.badge').length) {
	$('#clear-cash-notes').click();
}
var $quick_cash = $(this);
var amt = $quick_cash.contents().filter(function () {
	return this.nodeType == 3;
}).text();
var th = ',';
var $pi = $('#' + pi);
amt = formatDecimal(amt.split(th).join("")) * 1 + $pi.val() * 1;
$pi.val(formatDecimal(amt)).focus();
var note_count = $quick_cash.find('span');
if (note_count.length == 0) {
	$quick_cash.append('<span class="badge">1</span>');
} else {
	note_count.text(parseInt(note_count.text()) + 1);
}
});
$(document).on('click', '#quick-payable', function () {
$('#clear-cash-notes').click();
$(this).append('<span class="badge">1</span>');
$('#amount_1').val(formatDecimal(grand_total, 2));
});
$(document).on('click', '#clear-cash-notes', function () {
$('.quick-cash').find('.badge').remove();
$('#' + pi).val('0').focus();
});
$(document).on('change, blur', '.gift_card_no', function () {
var cn = $(this).val() ? $(this).val() : '';
var payid = $(this).attr('id'),
id = payid.substr(payid.length - 1);
if (cn != '') {
	$.ajax({
		type: "get", async: false,
		url: site.base_url + "sales/validate_gift_card/" + cn,
		dataType: "json",
		success: function (data) {
			if (data === false) {
				$('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
				bootbox.alert('<?=lang('incorrect_gift_card')?>');
			} else if (data.customer_id !== null && data.customer_id !== $('#poscustomer').val()) {
				$('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
				bootbox.alert('<?=lang('gift_card_not_for_customer')?>');
			} else {
				$('#gc_details_' + id).html('<small>Card No: ' + data.card_no + '<br>'+lang.value+': ' + formatMoney(data.value) + ' - '+lang.gc_balance+': ' + (data.balance > 0 ? formatMoney(data.balance) : '<b class="text-danger">'+formatMoney(data.balance)+'</b>') + (data.balance == 0 ? '<br><p class="text-danger" style="font-size: 110%;font-weight: 800;">'+lang.gc_without_balance+'</p>' : '')+ '</small>');
				$('#gift_card_no_' + id).parent('.form-group').removeClass('has-error');
				//calculateTotals();
				$('#amount_' + id).val(formatDecimal(gtotal >= data.balance ? data.balance : gtotal)).focus();
				$('#amount_' + id).data('mingc', (gtotal >= data.balance ? data.balance : gtotal));
			}
		}
	});
} else if (cn == ''){
	$('#gift_card_no_' + id).parent('.form-group').addClass('has-error');
	bootbox.alert('<?=lang('incorrect_gift_card')?>');
}
});
$(document).on('click', '.addButton', function () {
	// $('#paid_by_1').select2('destroy');
	if (pa <= 5) {
		add_payment_input();
	} else {
		bootbox.alert('<?=lang('max_reached')?>');
		return false;
	}

	setTimeout(function() {
		set_payment_method_billers();
	}, 350);

	if (KB) { display_keyboards(); }
	$('#paymentModal').css('overflow-y', 'scroll');

	$('.deposit_message:last').remove();
});

$(document).on('click', '.close-payment', function () {
	$(this).parent().remove();
	$(this).remove();
	pa--;
	calculateTotals();
});
$(document).on('focus', '.amount', function () {
pi = $(this).attr('id');
calculateTotals();
input = $(this);
rows = input.parents('.row').find('select.paid_by');
paid_by = $(rows[0]).val();
pay_amount = input.val();
customer = $('input[name="customer"]').val();
}).on('blur', '.amount', function () {
calculateTotals();
});

function calculateTotals() {
	var total_paying = 0;
	var balance = parseFloat($('#balance').data('balance'));
	var ia = $(".amount");
	$.each(ia, function (i) {
		var this_amount = formatCNum($(this).val() ? $(this).val() : 0);
		total_paying += parseFloat(this_amount);
	});
	$('#total_paying').text("$ "+formatMoney(total_paying));
		lock_submit = JSON.parse(localStorage.getItem('lock_submit'));
	    lock_submit_by_margin = JSON.parse(localStorage.getItem('lock_submit_by_margin'));
	    lock_submit_by_min_sale_amount = JSON.parse(localStorage.getItem('lock_submit_by_min_sale_amount'));
	    if (lock_submit == false && lock_submit_by_margin == false && lock_submit_by_min_sale_amount == false) {
			setTimeout(function() {
				set_submit_text();
			}, 6000);
		} else {
			$('.submit-sale').fadeOut();
		}
	<?php if ($pos_settings->rounding) {?>
		$('#balance').text("$ "+formatMoney(total_paying - round_total));
		$('#balance_' + pi).val(formatDecimal(total_paying - round_total));
		total_paid = total_paying;
		grand_total = round_total;
		if ((total_paying - round_total) < 0) {
			$('#total_paying').removeClass('text-primary').addClass('text-danger');
		} else {
			$('#total_paying').removeClass('text-danger').addClass('text-primary');
		}
	<?php } else {?>
		$('#balance').text("$ "+formatMoney(total_paying - gtotal));
		$('#balance_' + pi).val(formatDecimal(total_paying - gtotal));
		total_paid = total_paying;
		grand_total = gtotal;
		if ((total_paying - gtotal) < 0) {
			$('#total_paying').removeClass('text-primary').addClass('text-danger');
		} else {
			$('#total_paying').removeClass('text-danger').addClass('text-primary');
		}
	<?php }
	?>
}
$("#add_item").on({
    keypress: function(event) {
    	if (event.key != site.pos_settings.finalize_sale) {
			$('#payment').attr('disabled', true);
    	}
    },
    keydown: function(event) {
    	if (site.pos_settings.enter_to_exact_search == 1 && event.which === 13) {
    		event.preventDefault();
    		if (search_item_XHR == $("#add_item").val()) {
    			search_item_XHR = false;
    			return true;
    		}
    		$.ajax({
				type: 'get',
				url: '<?=admin_url('sales/suggestions');?>',
				dataType: "json",
				data: {
					term: $("#add_item").val(),
					warehouse_id: $("#poswarehouse").val(),
					customer_id: $("#poscustomer").val(),
					biller_id: $("#posbiller").val(),
					address_id: $("#poscustomer_branch").val(),
					module: 'pos',
					enter: true,
					<?php if ($pos_settings->balance_settings == 2) { ?>
						peso: peso
					<?php } ?>
				},
				while:function(){
					$('#payment').prop('disabled', true);
				},
				success: function (data) {
					if ($("#add_item").val().length >= 16 && data[0].id == 0) {
						command: toastr.error('No se encontró ningún producto', 'Mensaje de validación', { onHidden : function(){   } });
						setTimeout(function() {
							if (site.pos_settings.use_barcode_scanner == 1) {
								$("#add_item").val('');
							}
							$('#add_item').focus();
						}, 1200);
					}
					else if (data.length == 1 && data[0].id != 0) {
						item = data[0];
						if (item.row.cnt_units_prices !== undefined && item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && item.row.promotion != 1) {
							var item_id = item.item_id;
					    	var warehouse_id = $('#poswarehouse').val();
							$('.product_name_spumodal').text(item.label);
					        $('#sPUModal').appendTo("body").modal('show');
					    	add_item_unit(item_id, warehouse_id, item.row.sale_unit, item.row.paste_balance_value);
							$("#add_item").val('');
						} else {
							var row = add_invoice_item(item);
							if (row)
							$("#add_item").val('');
						}
						$("#add_item").autocomplete('close');
					}
					else if (data.length == 1 && data[0].id == 0) {
						setTimeout(function() {
							if (site.pos_settings.use_barcode_scanner == 1) {
								$("#add_item").val('');
							}
							$('#add_item').focus();
						}, 1200);
						command: toastr.error('No se encontró ningún producto', 'Mensaje de validación', { onHidden : function(){   } });
					}
		    		$('#payment').prop('disabled', true);
				}
			});
    		return true;
    	}
    	if (event.key != site.pos_settings.finalize_sale) {
			$('#payment').attr('disabled', true);
    	}
    },
    change: function() {
		$('#payment').attr('disabled', true);
    }
});
var search_item_XHR = false;
$("#add_item").autocomplete({
	source: function (request, response) {
		if (!$('#poscustomer').val()) {
			$('#add_item').val('').removeClass('ui-autocomplete-loading');
			bootbox.alert('<?=lang('select_above');?>');
			$('#add_item').focus();
			return false;
		}
		if (localStorage.getItem('balance_label')) {
			localStorage.removeItem('balance_label');
		}
		// Wappsi carne
		<?php if ($pos_settings->balance_settings == 2) { ?>
			var aux = request.term;
			aux = aux.substr(0, 2);
			var peso = 0;
			if(aux == '<?= $pos_settings->balance_label_prefix ?>'){
				localStorage.setItem('balance_label', true);
				aux = request.term;
				request.term = aux.substr(2,5);
				peso = aux.substr(7,5);
				//Se van a revisar si los primeros 2 digitos del paso son 0 para determinar si es 0.peso_bascula
				var parteEntera = peso.substr(0,2);
			  	var parteDecimal = peso.substr(2,3);
	      		peso = parteEntera+"."+parteDecimal;
				peso = parseFloat(peso);
			}
		<?php } ?>
		// Termina Wappsi carne
		search_item_XHR = request.term;
		$.ajax({
			type: 'get',
			url: '<?=admin_url('sales/suggestions');?>',
			dataType: "json",
			data: {
				term: request.term,
				warehouse_id: $("#poswarehouse").val(),
				customer_id: $("#poscustomer").val(),
				biller_id: $("#posbiller").val(),
				address_id: $("#poscustomer_branch").val(),
				module: 'pos',
				// Wappsi carne
				<?php if ($pos_settings->balance_settings == 2) { ?>
					peso: peso
				<?php } ?>
				// Termina Wappsi carne
			},
			while:function(){
				$('#payment').prop('disabled', true);
			},
			success: function (data) {
				$(this).removeClass('ui-autocomplete-loading');
				response(data);
	    		$('#payment').prop('disabled', true);
			}
		});
	},
	minLength: 1,
	autoFocus: false,
	delay: 250,
	response: function (event, ui) {
		if ($(this).val().length >= 16 && ui.content[0].id == 0) {
			command: toastr.error('No se encontró ningún producto', 'Mensaje de validación', { onHidden : function(){   } });
			setTimeout(function() {
				if (site.pos_settings.use_barcode_scanner == 1) {
					$("#add_item").val('');
				}
				$('#add_item').focus();
			}, 1200);
		}
		else if (ui.content.length == 1 && ui.content[0].id != 0) {
			ui.item = ui.content[0];
			$(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
			$(this).autocomplete('close');
		}
		else if (ui.content.length == 1 && ui.content[0].id == 0) {
			setTimeout(function() {
				if (site.pos_settings.use_barcode_scanner == 1) {
					$("#add_item").val('');
				}
				$('#add_item').focus();
			}, 1200);
			command: toastr.error('No se encontró ningún producto', 'Mensaje de validación', { onHidden : function(){   } });
		}
	},
	select: function (event, ui) {
		event.preventDefault();
		if (ui.item.id !== 0) {
			if (ui.item.row.based_on_gram_value == '1' || ui.item.row.based_on_gram_value == '2') {  // <- solo entra cuando el producto tiene valor en Depende del valor de gramo
				const value_based_on_gram = ui.item.row.based_on_gram_value;
				const url = site.base_url + 'pos/curren_price_gram?valuedOnGram='+value_based_on_gram;
				$.ajax({
                    url : url,
                    async : false,
                    dataType: 'json'
                }).done(function(data){		
					if (data['current_price']) {
						current_price_gram = data['current_price'];
						if (localStorage.getItem('gram_current_price')) {
							if (localStorage.getItem('gram_current_price') != current_price_gram) {	
								update_current_gram_price(current_price_gram, '1'); // <- Funcion para que actualice todos los creados
							}
						}else{
							localStorage.setItem('gram_current_price', current_price_gram);
						}
					}
					else if (data['current_price_italian']) {
						current_price_gram = data['current_price_italian'];
						if (localStorage.getItem('current_price_italian')) {
							if (localStorage.getItem('current_price_italian') != current_price_gram) {
								update_current_gram_price(current_price_gram, '2'); // <- Funcion para que actualice todos los creados
							}
						}else{
							localStorage.setItem('current_price_italian', current_price_gram);
						}
					}
                });
				ui.item.row.current_price_gram = current_price_gram;
			}
			if (ui.item.row.option) {
				if (site.settings.management_weight_in_variants == "1") {
            		if (ui.item.row.based_on_gram_value == '1' || ui.item.row.based_on_gram_value == '2' ) {
						const value_based_on_gram = ui.item.row.based_on_gram_value;
                		const url = site.base_url + 'pos/curren_price_gram?valuedOnGram='+value_based_on_gram;
                		$.ajax({
                    		url : url,
                    		async : false,
                    		dataType: 'json'
                		}).done(function(data){
							if (data['current_price']) {
								current_price_gram = data['current_price'];
								if (localStorage.getItem('gram_current_price')) {
									if (localStorage.getItem('gram_current_price') != current_price_gram) {
									// Crear una funcion para que actualice todos los creados
									update_current_gram_price(current_price_gram, '1');
								}
								}else{
									localStorage.setItem('gram_current_price', current_price_gram);
								}
							}
							if (data['current_price_italian']) {
								current_price_gram = data['current_price_italian'];
								if (localStorage.getItem('current_price_italian')) {
									if (localStorage.getItem('current_price_italian') != current_price_gram) {
									// Crear una funcion para que actualice todos los creados
									update_current_gram_price(current_price_gram, '2');
								}
								}else{
									localStorage.setItem('current_price_italian', current_price_gram);
								}
							}
                		});
						ui.item.row.current_price_gram = current_price_gram;
            		}
				}
        	}
			ui.item.is_new = true;
			if (site.settings.gift_card_product_id == ui.item.row.id) {
				var item_id = site.settings.item_addition == 1 ? ui.item.item_id : ui.item.id;
	            $('#gc_value').val('');
	            $('#card_no').val('');
				$('#product_id_gift_card').val(item_id);
				$('#gc_modal').modal('show');
				var row = add_invoice_item(ui.item);
				var myDate = new Date();
	            $('#gc_expiry').val($('#gc_expiry').data('originaldate'));
				if (row)
					$(this).val('');
			} else {
				if (ui.item.row.cnt_units_prices !== undefined && ui.item.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && ui.item.row.promotion != 1) {
					var item_id = ui.item.item_id;
			    	var warehouse_id = $('#poswarehouse').val();
					$('.product_name_spumodal').text(ui.item.label);
			        $('#sPUModal').appendTo("body").modal('show');
			    	add_item_unit(item_id, warehouse_id, ui.item.row.sale_unit, ui.item.row.paste_balance_value);
					$(this).val('');
				} else {
					var row = add_invoice_item(ui.item);
					if (row)
					$(this).val('');
				}
			}
		} else {
			command: toastr.error('No se encontró ningún producto', 'Mensaje de validación', { onHidden : function(){   } });
		}
	}
});

<?php if ($pos_settings->tooltips) { echo '$(".pos-tip").tooltip();'; } ?>
$('#product-list, #category-list, #subcategory-list, #brands-list').perfectScrollbar({suppressScrollX: true});
$('select, .select').select2({minimumResultsForSearch: 7});
$(document).on('click', '.product', function (e) {
	$('#payment').prop('disabled', true);
	if (localStorage.getItem('balance_label')) {
		localStorage.removeItem('balance_label');
	}
	$('#modal-loading').show();
	code = $(this).data('code');
	wh = $('#poswarehouse').val();
	bl = $('#posbiller').val();
	cu = $('#poscustomer').val();
	cub = $('#poscustomer_branch').val();
	$.ajax({
		type: "get",
		url: "<?=admin_url('pos/getProductDataByCode')?>",
		data: {code: code, warehouse_id: wh, customer_id: cu, biller_id : bl, address_id : cub},
		dataType: "json",
		success: function (data) {
			e.preventDefault();
			if (data !== null) {
				if(data.row.cnt_units_prices !== undefined && data.row.cnt_units_prices > 1 && (site.settings.prioridad_precios_producto == 7 || site.settings.prioridad_precios_producto == 10 || site.settings.prioridad_precios_producto == 11) && data.row.promotion != 1)

				 {
					var item_id = data.item_id;
			    	var warehouse_id = $('#poswarehouse').val();
					$('.product_name_spumodal').text(data.label);
			        $('#sPUModal').appendTo("body").modal('show');
			    	add_item_unit(item_id, warehouse_id, data.row.sale_unit, data.row.paste_balance_value);
				} else {
					data.is_new = true;
					var row = add_invoice_item(data);
					if (row)
					$(this).val('');
				}
				$('#modal-loading').hide();
			} else {
				bootbox.alert('<?=lang('no_match_found')?>');
				$('#modal-loading').hide();
				hide_menu_sobre_productos();
				setTimeout(function() {
        			$("#add_item").focus();
				}, 1200);
			}
		}
	});
});

$(document).on('click', '.category', function () {
if (cat_id != $(this).val()) {
	$('#modal-loading').show();
	cat_id = $(this).val();
	$.ajax({
		type: "get",
		url: "<?=admin_url('pos/ajaxcategorydata');?>",
		data: {
				category_id: cat_id,
				biller_id : $('#posbiller').val(),
				warehouse_id : $('#poswarehouse').val()
			},
		dataType: "json",
		success: function (data) {
			$('#item-list').empty();
			var newPrs = $('<div></div>');
			newPrs.html(data.products);
			newPrs.appendTo("#item-list");
			$('#subcategory-list').empty();
			var newScs = $('<div></div>');
			newScs.html(data.subcategories);
			newScs.appendTo("#subcategory-list");
			tcp = data.tcp;
			nav_pointer();
		}
	}).done(function () {
		p_page = 'n';
		$('#category-' + cat_id).addClass('active');
		$('#category-' + ocat_id).removeClass('active');
		ocat_id = cat_id;
		$('#modal-loading').hide();
		nav_pointer();
		hide_menu_sobre_productos();
	});
}
});
$('#category-' + cat_id).addClass('active');

$(document).on('click', '.brand', function () {
if (brand_id != $(this).val()) {
	// $('#open-brands').click();
	$('#modal-loading').show();
	brand_id = $(this).val();
	$.ajax({
		type: "get",
		url: "<?=admin_url('pos/ajaxbranddata');?>",
		data: {brand_id: brand_id},
		dataType: "json",
		success: function (data) {
			$('#item-list').empty();
			var newPrs = $('<div></div>');
			newPrs.html(data.products);
			newPrs.appendTo("#item-list");
			tcp = data.tcp;
			nav_pointer();
		}
	}).done(function () {
		p_page = 'n';
		$('#brand-' + brand_id).addClass('active');
		$('#brand-' + obrand_id).removeClass('active');
		obrand_id = brand_id;
		$('#category-' + cat_id).removeClass('active');
		$('#subcategory-' + sub_cat_id).removeClass('active');
		cat_id = 0; sub_cat_id = 0;
		$('#modal-loading').hide();
		nav_pointer();
		hide_menu_sobre_productos();
	});
}
});

$(document).on('click', '.subcategory', function () {
	if (sub_cat_id != $(this).val()) {
		// $('#open-subcategory').click();
		$('#modal-loading').show();
		sub_cat_id = $(this).val();
		$.ajax({
			type: "get",
			url: "<?=admin_url('pos/ajaxproducts');?>",
			data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
			dataType: "html",
			success: function (data) {
				$('#item-list').empty();
				var newPrs = $('<div></div>');
				newPrs.html(data);
				newPrs.appendTo("#item-list");
			}
		}).done(function () {
			p_page = 'n';
			$('#subcategory-' + sub_cat_id).addClass('active');
			$('#subcategory-' + osub_cat_id).removeClass('active');
			$('#modal-loading').hide();
			hide_menu_sobre_productos();
		});
	}
});

$(document).on('click', '.subcategory_navbar', function () {
	$('#modal-loading').show();
	cat_id = $(this).data('category');
	sub_cat_id = $(this).data('subcategory');
	$.ajax({
		type: "get",
		url: "<?=admin_url('pos/ajaxproducts');?>",
		data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
		dataType: "html",
		success: function (data) {
			$('#item-list').empty();
			var newPrs = $('<div></div>');
			newPrs.html(data);
			newPrs.appendTo("#item-list");
		}
	}).done(function () {
		p_page = 'n';
		$('#subcategory-' + sub_cat_id).addClass('active');
		$('#subcategory-' + osub_cat_id).removeClass('active');
		$('#modal-loading').hide();
		hide_menu_sobre_productos();
		$('.subcategory_navbar').removeClass('wappsi_cat_active');
		$('.subcategory_navbar[data-category="'+cat_id+'"][data-subcategory="'+sub_cat_id+'"]').addClass('wappsi_cat_active');
	});
});
$('#last').click(function () {
	if (p_page == 'n') {
		p_page = 0
	}
	paginas = parseInt(tcp / pro_limit);
	p_page = paginas * pro_limit;
	if (tcp >= pro_limit && p_page < tcp) {
		$('#modal-loading').show();
		$.ajax({
			type: "get",
			url: "<?=admin_url('pos/ajaxproducts');?>",
			data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
			dataType: "html",
			success: function (data) {
				$('#item-list').empty();
				var newPrs = $('<div></div>');
				newPrs.html(data);
				newPrs.appendTo("#item-list");
				nav_pointer();
			}
		}).done(function () {
			$('#modal-loading').hide();
		});
	} else {
		p_page = p_page - pro_limit;
	}
});

$('#next').click(function () {
if (p_page == 'n') {
	p_page = 0
}
p_page = p_page + pro_limit;
if (tcp >= pro_limit && p_page < tcp) {
	$('#modal-loading').show();
	$.ajax({
		type: "get",
		url: "<?=admin_url('pos/ajaxproducts');?>",
		data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
		dataType: "html",
		success: function (data) {
			$('#item-list').empty();
			var newPrs = $('<div></div>');
			newPrs.html(data);
			newPrs.appendTo("#item-list");
			nav_pointer();
		}
	}).done(function () {
		$('#modal-loading').hide();
	});
} else {
	p_page = p_page - pro_limit;
}
});

    $('#previous').click(function () {
        if (p_page == 'n') {
            p_page = 0;
        }
        if (p_page != 0) {
            $('#modal-loading').show();
            p_page = p_page - pro_limit;
            if (p_page == 0) {
                p_page = 'n'
            }
            $.ajax({
                type: "get",
                url: "<?=admin_url('pos/ajaxproducts');?>",
                data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
                dataType: "html",
                success: function (data) {
                    $('#item-list').empty();
                    var newPrs = $('<div></div>');
                    newPrs.html(data);
                    newPrs.appendTo("#item-list");
                    nav_pointer();
                }

            }).done(function () {
                $('#modal-loading').hide();
            });
        }
        });

        $('#first').click(function () {
        if (p_page == 'n') {
            p_page = 0;
        }
        if (p_page != 0) {
            $('#modal-loading').show();
            p_page = 0;
            if (p_page == 0) {
                p_page = 'n'
            }
            $.ajax({
                type: "get",
                url: "<?=admin_url('pos/ajaxproducts');?>",
                data: {category_id: cat_id, subcategory_id: sub_cat_id, per_page: p_page},
                dataType: "html",
                success: function (data) {
                    $('#item-list').empty();
                    var newPrs = $('<div></div>');
                    newPrs.html(data);
                    newPrs.appendTo("#item-list");
                    nav_pointer();
                }

            }).done(function () {
                $('#modal-loading').hide();
            });
        }
    });

    $(document).on('change', '.paid_by', function () {
        var p_val = $(this).val(),
        id = $(this).attr('id'),
        pa_no = id.substr(id.length - 1);
        var due_payment = $(this).find('option:selected').data('duepayment');
        $('#rpaidby').val(p_val);
        if (p_val == 'cash' || p_val == 'other') {
            $('.pcheque_' + pa_no).hide();
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).show();
            $('#amount_' + pa_no).focus();
        } else if (p_val == 'CC' || p_val == 'stripe' || p_val == 'ppp' || p_val == 'authorize') {
            $('.pcheque_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
            $('.pcc_' + pa_no).show();
            $('#swipe_' + pa_no).focus();
        } else if (p_val == 'Cheque') {
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
            $('.pcheque_' + pa_no).show();
            $('#cheque_no_' + pa_no).focus();
        } else {
            $('.pcheque_' + pa_no).hide();
            $('.pcc_' + pa_no).hide();
            $('.pcash_' + pa_no).hide();
        }
        if (p_val == 'gift_card') {
            $('.gc_' + pa_no).show();
            $('.ngc_' + pa_no).hide();
            // $('#gift_card_no_' + pa_no).focus();
        } else {
            $('.ngc_' + pa_no).show();
            $('.gc_' + pa_no).hide();
            $('#gc_details_' + pa_no).html('');
        }
        ipt_index = $('select.paid_by').index($(this));
        input_payment_term_paid_by = $('.input_payment_term_paid_by').eq(ipt_index);
        input_payment_term_amount = $('.input_payment_term_amount').eq(ipt_index);
        input_payment_term_val = $('.input_payment_term_val').eq(ipt_index);
        payment_term_input = $(input_payment_term_val).find('.payment_term_val');
        if (p_val == 'Credito' || due_payment == 1) {
            $(input_payment_term_paid_by).removeClass('col-sm-6');
            $(input_payment_term_amount).removeClass('col-sm-6');
            $(input_payment_term_paid_by).addClass('col-sm-4');
            $(input_payment_term_amount).addClass('col-sm-4');
            $(input_payment_term_val).fadeIn();
            $(payment_term_input).val(1);
            $('#payment_term').val(1);
            $('.addButton').prop('disabled', true);
            $('#amount_' + pa_no).trigger('change');
            $('.due_payment_' + pa_no).val(1);
            $('#payment_term').val(1);
            setTimeout(function() {
                $('.payment_term_val').focus();
            }, 850);
        } else {
            $(input_payment_term_paid_by).removeClass('col-sm-4');
            $(input_payment_term_amount).removeClass('col-sm-4');
            $(input_payment_term_paid_by).addClass('col-sm-6');
            $(input_payment_term_amount).addClass('col-sm-6');
            $(input_payment_term_val).fadeOut();
            $(payment_term_input).val('');
            $('#payment_term').val('');
            $('.addButton').prop('disabled', false);
            $('.due_payment_' + pa_no).val(0);
            $('#payment_term').val(0);
        }

        payment = $(this).closest('.payment');
        paid_by = $(this).val();
        amount_to_pay = payment.find('.amount').val();
        customer = $('#poscustomer').val();
        payment.find('.deposit_message').remove();
        if (paid_by == "deposit") {
            $.ajax({
                url:  site.base_url+"customers/deposit_balance/"+customer+"/"+amount_to_pay,
            }).done(function(data) {
                payment.find('.deposit_message').remove();
                payment.append(data);
            });
        }
        calculateTotals();
        validatePaymentMethodForCreditFinancing($(this), pa_no)
    });
    $(document).on('change', '.payment_term_val', function () {
        $('#payment_term').val($(this).val());
    });

    $(document).on('click', '.submit-sale', function () {
        if (validateQuotaFields() == false) {
            return false
        }

        if (pos_submitted = localStorage.getItem('pos_submitted')) {
            Command: toastr.warning('La factura ya está siendo procesada.', 'Advertencia', {onHidden: function() {
                // setTimeout(function() {
                // 	$('#pos-sale-form').submit();
                // }, 400);
            }});
            return false;
        } else {
            localStorage.setItem('pos_submitted', 1);
        }

        if ($(this).data('submittarget')) {
            $('#submit_target').val($(this).data('submittarget'));
        }
        $('.submit-sale').prop('disabled', true);
        if ($('#doc_type_id').val() == '') {
            $('#doc_type_error').css('display', '');
            $('.submit-sale').prop('disabled', true);
            $('#doc_type_id').select2('focus');
            localStorage.removeItem('pos_submitted');
            return false;
        } else {
            $('#doc_type_error').css('display', 'none');
        }
        if (due_payment_method.sale == false && (total_paid == 0 || formatDecimal(total_paid, 2) < formatDecimal(grand_total, 2))) {
            bootbox.alert('<?=sprintf(lang('due_method_disabled'), lang('sale'));?>');
            localStorage.removeItem('pos_submitted');
            return false;
        }
        if ((total_paid == 0 || formatDecimal(total_paid, 2) < formatDecimal(grand_total, 2)) && $('#paid_by_1').val() != 'Credito') {
            bootbox.prompt({
                title: "<?=lang('paid_l_t_payable');?>",
                placeholder: 'Días de plazo para el pago',
                inputType: 'text',
                callback: function (result) {
                    if (result == null) {
                        $('#payment_term').val(0);
                    } else {
                        if (result < 0) { result = 0 }
                        $('#payment_term').val(result);
                        $('#pos_note').val(localStorage.getItem('posnote'));
                        $('#staff_note').val(localStorage.getItem('staffnote'));
                        $('#locator_details').val(localStorage.getItem('locator_details'));
                        $('.submit-sale').text('<?=lang('loading');?>').attr('disabled', true);
                        $('#pos-sale-form').submit();
                    }
                }
            });
            localStorage.removeItem('pos_submitted');
            return false;
        } else {
            <?php if (! empty($sid)) { ?>
                localStorage.setItem('submit_form', 1);
            <?php } ?>
            $('#pos_note').val(localStorage.getItem('posnote'));
            $('#staff_note').val(localStorage.getItem('staffnote'));
            $('#locator_details').val(localStorage.getItem('locator_details'));
            $(this).text('<?=lang('loading');?>').attr('disabled', true);
            if (localStorage.getItem('restobar_mode_module')) {
                localStorage.removeItem('restobar_mode_module');
            }
            <?php if (! empty($sid)) { ?>
                <?php if ($this->pos_settings->restobar_mode == YES) { ?>
                    <?php if ($pos_settings->order_print_mode == 0) { ?>
                        var products_in_preparation = 0;
                        var products_ids = [];
                        $('.rstate_readiness').each(function(index, el) {
                            if ($(this).val() < 3) {
                                products_ids.push($(this).val());
                                products_in_preparation++;
                            }
                        });
                        if (products_in_preparation > 0) {
                            bootbox.confirm('¿Existen productos en preparación, desea marcarlos como despachados y finalizar la venta?', function(result){
                                if (result == true) {
                                    $.ajax({
                                        url: site.base_url+'pos/dispatch_all_products_order',
                                        type: 'POST',
                                        dataType: 'HTML',
                                        data: {
                                            '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                                            'suspend_sale_id': <?= $sid; ?>
                                        },
                                    })
                                    .done(function(data) {
                                        console.log('Cambio de estado de productos de la mesa: '+ data);
                                        if (data == true) {
                                            Command: toastr.success('Productos despachados correctamente.', 'Mensaje de validación', {onHidden: function() {
                                                setTimeout(function() {
                                                    $('#pos-sale-form').submit();
                                                }, 400);
                                            }});
                                        }
                                    })
                                    .fail(function(data) {
                                        console.log('Error'+ data);
                                    });
                                } else {
                                    $('#paymentModal').modal('hide');
                                }
                            });
                        } else {
                            setTimeout(function() {
                                $('#pos-sale-form').submit();
                            }, 400);
                        }
                    <?php } else { ?>
                        $.ajax({
                            url: site.base_url+'pos/dispatch_all_products_order',
                            type: 'POST',
                            dataType: 'HTML',
                            data: {
                                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                                'suspend_sale_id': <?= $sid; ?>
                            },
                        })
                        .done(function(data) {
                            if (data == true) {
                                Command: toastr.success('Productos despachados correctamente.', 'Mensaje de validación', {onHidden: function() {
                                    setTimeout(function() {
                                        $('#pos-sale-form').submit();
                                    }, 400);
                                }});
                            }

                        })
                        .fail(function(data) {
                            console.log("error");
                        });
                    <?php } ?>
                <?php } else { ?>
                    setTimeout(function() {
                        $('#pos-sale-form').submit();
                    }, 400);
                <?php } ?>
            <?php } else { ?>
                setTimeout(function() {
                    $('#pos-sale-form').submit();
                }, 400);
            <?php } ?>
        }
    });

    $('#suspend').click(function () {
        if (validate_existing_selected_table()) {
            if (count <= 1) {
                bootbox.alert('<?=lang('x_suspend');?>');
                return false;
            } else {
                <?php if ($restobar == '1') { ?>
                    if ($('#restobar_mode_module').val() == 1) {
                        if ($('#table_id').val() == '') {
                            Command: toastr.error('Para continuar por favor seleccione la mesa', '¡Error!', {onHidden: function() { $('#boton_mesa').click(); }});
                            return false;
                        }
                    }
                    <?php if (!isset($suspend_sale) || isset($suspend_sale->suspend_note) && $suspend_sale->suspend_note == '') { ?>
                        $('#susModal').modal();
                        setTimeout(function() {
                            $('#reference_note').focus();
                        }, 1000);
                    <?php } else { ?>
                        $('#reference_note').val('<?= (isset($suspend_sale->suspend_note) ? $suspend_sale->suspend_note : ''); ?>');
                        suspend_sale();
                    <?php } ?>
                <?php } else { ?>
                    $('#susModal').modal();
                    setTimeout(function() {
                        $('#reference_note').focus();
                    }, 1000);
                <?php } ?>
            }
        }
    });

    $('#susModal').on('show.bs.modal', function (e) {
        var subtotal = $('#subtotal').text();
        <?php if ($this->pos_settings->activate_electronic_pos == YES /*&& $this->pos_settings->mode_electronic_pos == 2*/) : ?>
            if (formatDecimal(subtotal) >= formatDecimal('<?= $this->pos_settings->uvt_value ?>')) {
                $('#limitExceededElertElectronicPosSuspend').css('display', 'block');
            } else {
                $('#limitExceededElertElectronicPosSuspend').css('display', 'none');
            }
        <?php endif ?>
    })

	$('#suspend_sale').click(function () { suspend_sale() });
	$('#convert_order_submit').click(function () { convert_order_submit() });

	function convert_order_submit() {
		ref = $('#convert_order_doc_type').val();
		convert_order_as = $('#convert_order_as').val();

		if (convert_order_as == 2) {
			if (!$('#convert_order_as').val()) {
				Command: toastr.error('El campo <?= lang('convert_order_as') ?> es Obligatorio.', 'Validación de formulario.', {onHidden: function() {  }});
						return false;
			}
			if ($('#convert_order_as').val() == 2 && !$('#convert_order_employer_branch').val()) {
				Command: toastr.error('El campo <?= lang('convert_order_employer_branch') ?> es Obligatorio.', 'Validación de formulario.', {onHidden: function() {  }});
						return false;
			}
		}
		if (!ref) {
			Command: toastr.error('El campo <?= lang('convert_order_doc_type') ?> es Obligatorio.', 'Validación de formulario.', {onHidden: function() {  }});
			return false;
		} else {
			suspended_sale_items = 1;
			suspend = $('<span></span>');
			<?php if ($sid) {?>
				suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" />'+
							 '<input type="hidden" name="convert_to_order" value="yes" />'+
							 '<input type="hidden" name="convert_order_as" value="'+$('#convert_order_as').val()+'"/>'+
							 '<input type="hidden" name="convert_order_employer_branch" value="'+$('#convert_order_employer_branch').val()+'"/>'+
							 '<input type="hidden" name="convert_order_employer" value="'+$('#convert_order_employer').val()+'"/>'+
							 '<input type="hidden" name="convert_order_doc_type" value="' + $('#convert_order_doc_type').val() + '" />');
			<?php } else {?>
				suspend.html('<input type="hidden" name="convert_to_order" value="yes" />'+
							 '<input type="hidden" name="convert_order_as" value="'+$('#convert_order_as').val()+'"/>'+
							 '<input type="hidden" name="convert_order_employer_branch" value="'+$('#convert_order_employer_branch').val()+'"/>'+
							 '<input type="hidden" name="convert_order_employer" value="'+$('#convert_order_employer').val()+'"/>'+
							 '<input type="hidden" name="convert_order_doc_type" value="' + $('#convert_order_doc_type').val() + '" />'); //ACÁ SUSPEND
			<?php } ?>
			suspend.appendTo("#hidesuspend");
			$('#total_items').val(count - 1);
			$('#pos-sale-form').submit();
		}
	}
	function suspend_sale() {
		$('#suspend_sale').prop('disabled', true);
		ref = $('#reference_note').val();
		if (!ref || ref == '') {
			bootbox.alert('<?=lang('type_reference_note');?>');
			$('#suspend_sale').prop('disabled', false);
			return false;
		} else {
			suspended_sale_items = 1;
			suspend = $('<span></span>');
			<?php if ($sid) {?>
				suspend.html('<input type="hidden" name="delete_id" value="<?php echo $sid; ?>" /><input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />');
			<?php } else {?>
				suspend.html('<input type="hidden" name="suspend" value="yes" /><input type="hidden" name="suspend_note" value="' + ref + '" />'); //ACÁ SUSPEND
			<?php } ?>
			suspend.appendTo("#hidesuspend");
			$('#total_items').val(count - 1);
			<?php if ($this->pos_settings->restobar_mode == YES) { ?>
				if (pos_settings.order_print_mode == 0) {
					if ($('#posseller').val() == '') {
						Command: toastr.error('El campo del Vendedor es Obligatorio.', 'Validación de formulario.', {onHidden: function() { $('#posseller').focus() }});
						return false;
					}
					$.ajax({
						url: '<?= admin_url("pos"); ?>',
						type: 'POST',
						dataType: 'JSON',
						data: $('#pos-sale-form').serialize()
					})
					.done(function(response) {
						if (response != '') {
							window.location.href = '<?= admin_url('pos'); ?>';

						}
					})
					.fail(function(response) {
						console.log(response.responseText);
					});
				} else {
					$('#pos-sale-form').submit();
				}
			<?php } else { ?>
				$('#pos-sale-form').submit();
			<?php } ?>
		}
	}
});

$(document).ready(function () {
	$('#print_order').click(function () {
		$('#loading').fadeIn()
		var v = "?biller_id="+$('#posbiller').val();
			v+= "&customer_id="+$('#poscustomer').val();
			v+= "&seller_id="+$('#posseller').val();
			v+= "&customer_branch_id="+$('#poscustomer_branch').val();
			v+= "&doc_type_id="+$('#document_type_id').val();

		<?php if ($sid) { ?>
			v+= "&suspend_sale_id=<?= $sid ?>";
		<?php } ?>

		url = site.base_url+"pos/print_order"+v;
		newWin = window.open(url, 'blank');
		setTimeout(function() {
			newWin.close();
			$('#loading').fadeOut()
		}, 4000);

	});
	$('#print_bill').click(function () {
		if (count == 1) {
			bootbox.alert('<?=lang('x_total');?>');
			return false;
		}
		<?php if ($pos_settings->remote_printing != 1) { ?>
			printBill();
		<?php } else { ?>
			Popup($('#bill_tbl').html());
		<?php } ?>
	});
	$(document).on('change', '#convert_order_as', function(){
		if ($(this).val() == 2) {
			$('.div_convert_order_as').fadeIn();
		} else {
			$('.div_convert_order_as').fadeOut();
		}
	});
	$('#convert_order').click(function () {

		if ($('#posbiller option:selected').data('customerdefault') == $('#poscustomer').val()) {
			Command: toastr.error('No se puede generar orden de pedido para el cliente por defecto de la sucursal.', '¡Error!', {onHidden: function(){

			}});
		} else {
			$('#convertOrderModal').modal('show');
			if (customer_data.customer_employer_id) {
				$.ajax({
					url:"<?= admin_url('sales/getCustomerAddresses') ?>/",
			        type: "get",
			        data: {"customer_id" : customer_data.customer_employer_id}
				}).done(function(data){
					$('#convert_order_employer_branch').html(data).select2();
					$('#convert_order_employer_branch').trigger('change');
				});
				//ACA CUSTOMER EMPLOYER
				$('#convert_order_employer').val(customer_data.customer_employer_id).select2({
					minimumInputLength: 1,
					data: [],
					initSelection: function (element, callback) {
						$.ajax({
							type: "get", async: false,
							url: "<?=admin_url('customers/getCustomer')?>/" + $(element).val(),
							dataType: "json",
							success: function (data) {
								callback(data[0]);
							}
						});
					},
					ajax: {
						url: site.base_url + "customers/suggestions",
						dataType: 'json',
						quietMillis: 15,
						data: function (term, page) {
							return {
								term: term,
								limit: 10
							};
						},
						results: function (data, page) {
							if (data.results != null) {
								return {results: data.results};
							} else {
								return {results: [{id: '', text: lang.no_match_found}]};
							}
						}
					}
				});
				$.ajax({
					url:"<?= admin_url('sales/getCustomerAddresses') ?>/",
			        type: "get",
			        data: {"customer_id" : customer_data.customer_employer_id}
				}).done(function(data){
					$('#convert_order_employer_branch').html(data).select2();
				});
				// $('.div_convert_order').fadeIn();
			} else {
				// $('.div_convert_order').fadeOut();

				$('#convert_order_employer').select2({
		            minimumInputLength: 1,
		            ajax: {
		                url: site.base_url + "customers/suggestions",
		                dataType: 'json',
		                quietMillis: 15,
		                data: function (term, page) {
		                    return {
		                        term: term,
		                        limit: 10
		                    };
		                },
		                results: function (data, page) {
		                    if (data.results != null) {
		                        return {results: data.results};
		                    } else {
		                        return {results: [{id: '', text: lang.no_match_found}]};
		                    }
		                }
		            }
		        });
			}

			$.ajax({
		        url:'<?= admin_url("billers/getBillersDocumentTypes/8/") ?>'+$('#posbiller').val(),
		        type:'get',
		        dataType:'JSON'
		    }).done(function(data){
				response = data;
				$('#convert_order_doc_type').html(response.options).select2();
				if (response.status == 0) {
					$('.resAlert').append("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('payment_biller_without_documents_types') ?></div></div>");
				}
				if ($('select#convert_order_doc_type option').length == 1) {
						$('.div_convert_order_doc_type').fadeOut();
				}
		    });
		}

	});

    $(document).on('click', '#cancelOrder', function() {
        swal({
            title: "¿Está seguro de salir sin guardar?",
            text: "Este proceso no eliminará la Órden",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Cancelar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                window.location.href = site.base_url + 'pos'
            }
        });
        clean_localstorage();
    });

    if (site.pos_settings.default_section == 1002) {
    	setTimeout(function() {
    		$('.subcategory_navbar[data-subcategory="'+site.pos_settings.pos_navbar_default_category+'"]').trigger('click');
    	}, 1200);
    }

});
$(document).on('change', '#convert_order_employer', function(){
	cempl = $(this).val();
	$.ajax({
		url:"<?= admin_url('sales/getCustomerAddresses') ?>/",
        type: "get",
        data: {"customer_id" : cempl}
	}).done(function(data){
		$('#convert_order_employer_branch').html(data).select2();
	});
});
$(function () {
	$(".alert:not(.header_alert)").effect("shake");
	setTimeout(function () {
	$(".alert:not(.header_alert)").hide('blind', {}, 500)
	}, 15000);
	<?php if ($pos_settings->display_time) {?>
	var now = new moment();
	$('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
	setInterval(function () {
		var now = new moment();
		$('#display_time').text(now.format((site.dateFormats.js_sdate).toUpperCase() + " HH:mm"));
	}, 1000);
	<?php }
	?>
});
<?php if ($pos_settings->remote_printing == 1) { ?>
function Popup(data) {
	var mywindow = window.open('', 'sma_pos_print', 'height=500,width=300');
		mywindow.document.write('<html><head><title>Print</title>');
		mywindow.document.write('<link rel="stylesheet" href="<?=$assets?>styles/helpers/bootstrap.min.css" type="text/css" />');
		mywindow.document.write('</head><body >');
		mywindow.document.write(data);
		mywindow.document.write('</body></html>');
		mywindow.print();
		mywindow.close();
		return true;
	}
<?php }
?>
</script>
<?php
if ( ! $pos_settings->remote_printing) {
?>
<script type="text/javascript">
	var order_printers = <?= json_encode($order_printers); ?>;
	function printOrder() {
		$.each(order_printers, function() {
			var socket_data = { 'printer': this,
			'logo': (biller && biller.logo ? biller.logo : ''),
			'text': order_data };
			$.get('<?= admin_url('pos/p/order'); ?>', {data: JSON.stringify(socket_data)});
		});
		return false;
	}
	function printBill() {
		var socket_data = {
			'printer': <?= json_encode($printer); ?>,
			'logo': (biller && biller.logo ? biller.logo : ''),
			'text': bill_data
		};
		$.get('<?= admin_url('pos/p'); ?>', {data: JSON.stringify(socket_data)});
		return false;
	}
</script>
<?php
} elseif ($pos_settings->remote_printing == 2) {
?>
<script src="<?= $assets ?>js/socket.io.min.js" type="text/javascript"></script>
<script type="text/javascript">
	socket = io.connect('http://localhost:6440', {'reconnection': false});
	function printBill() {
		if (socket.connected) {
			var socket_data = {'printer': <?= json_encode($printer); ?>, 'text': bill_data};
			socket.emit('print-now', socket_data);
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}
	var order_printers = <?= json_encode($order_printers); ?>;
	function printOrder() {
		if (socket.connected) {
			$.each(order_printers, function() {
				var socket_data = {'printer': this, 'text': order_data};
				socket.emit('print-now', socket_data);
			});
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}
</script>
<?php
} elseif ($pos_settings->remote_printing == 3) {
?>
<script type="text/javascript">
	var order_printers = <?= $pos_settings->local_printers ? "''" : json_encode($order_printers); ?>;
	function printOrder() {
		if (socket.readyState == 1) {

			if (order_printers == '') {

				var socket_data = { 'printer': false, 'order': true,
				'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
				'text': order_data };
				socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));

			} else {

				$.each(order_printers, function() {
					var socket_data = { 'printer': this,
					'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
					'text': order_data };
					socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));
				});

			}
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}

	function printBill() {
		if (socket.readyState == 1) {
			var socket_data = {
				'printer': <?= $pos_settings->local_printers ? "''" : json_encode($printer); ?>,
				'logo': (biller && biller.logo ? site.url+'assets/uploads/logos/'+biller.logo : ''),
				'text': bill_data
			};
			socket.send(JSON.stringify({type: 'print-receipt', data: socket_data}));
			return false;
		} else {
			bootbox.alert('<?= lang('pos_print_error'); ?>');
			return false;
		}
	}
</script>
<?php
}
?>
<script type="text/javascript">
$('.sortable_table tbody').sortable({
	containerSelector: 'tr'
});
//ocultar barra lateral izquierda
if (screen.width > 800) {
	$('.pace-done').addClass('mini-navbar');
}
</script>
<?php
$s2_lang_file = read_file('./assets/config_dumps/s2_lang.js');
foreach (lang('select2_lang') as $s2_key => $s2_line) {
$s2_data[$s2_key] = str_replace(array('{', '}'), array('"+', '+"'), $s2_line);
}
$s2_file_date = $this->parser->parse_string($s2_lang_file, $s2_data, true);
?>
<script type="text/javascript" charset="UTF-8"><?=$s2_file_date?></script>
<div id="ajaxCall"><i class="fa fa-spinner fa-pulse"></i></div>
<?php
if (isset($print) && !empty($print)) {
/* include FCPATH.'themes'.DIRECTORY_SEPARATOR.$Settings->theme.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'pos'.DIRECTORY_SEPARATOR.'remote_printing.php'; */
include 'remote_printing.php';
}
?>
<script type="text/javascript">

	function get_customer_branches(){
		if ($('#poscustomer').val()) {
			$.ajax({
				url:"<?= admin_url('sales/getCustomerAddresses') ?>/",
		        type: "get",
		        data: {"customer_id" : $('#poscustomer').val()}
			}).done(function(data){
				$('#poscustomer_branch').html(data).select2();
				$('#poscustomer_branch').trigger('change');
				if ($('#poscustomer_branch').val()) {
					$('#address_id').val($('#poscustomer_branch').val());
				}
				resizeWindow();
			});
		}
	}
	$('#poscustomer_branch').on('change', function(){
		$('#address_id').val($(this).val());
	});
	$('#set-customer-branch').on('click', function(){
    	get_customer_branches();
	});
	$('#poscustomer_branch').on('change', function(){
		cb = $('#poscustomer_branch').val();
		localStorage.setItem('poscustomer_branch', cb);
		$('#address_id').val(cb);
		if (!localStorage.getItem('posseller')) {
            setTimeout(function() {
                seller = $('#poscustomer_branch option:selected').data('sellerdefault');
                if (seller && !user_seller_id) {
                    $('#posseller').select2('val', seller).trigger('change');
                }
            }, 1300);
        }
	});
    function add_item_unit(item_id, warehouse_id, sale_unit, paste_balance_value){
        var ooTable = $('#unit_prices_table').dataTable({
            // aaSorting: [[1, "asc"]],
            aLengthMenu: [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            bProcessing: true, 'bServerSide': true,
            sAjaxSource: site.base_url+"pos/itemSelectUnit/"+item_id+"/"+warehouse_id+"/"+$('#poscustomer').val(),
            "bDestroy": true,
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                name: "<?= $this->security->get_csrf_token_name() ?>",
                value: "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback });
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                var oSettings = ooTable.fnSettings();

                nRow.setAttribute('data-itemunitid', aData[4]);
                nRow.setAttribute('data-productid', aData[5]);
                nRow.setAttribute('data-productunitid', aData[6]);
                nRow.className = "add_item_unit";

                return nRow;
            },
            aoColumns: [
                {bSortable: false, "mRender": radio_2},
                {bSortable: false},
                {bSortable: false, className : 'text-right'},
                {bSortable: false, className : 'text-right'},
                {bVisible: false},
                {bVisible: false},
                {bVisible: false},
                {bSortable: false, className : 'text-right'}
            ],
            initComplete: function(settings, json) {
            	setTimeout(function() {
                    $('.add_item_unit[data-productunitid="'+sale_unit+'"]').find('input.select_auto_2 ').iCheck('check').focus();

                    if (site.pos_settings.balance_settings == 1 && paste_balance_value == 1) {
				            navigator.clipboard.readText()
				            .then(text => {
				                value = parseFloat(text);
				                if (value > 0) {
				                	$('#unit_quantity').val(value);
				                }
				                navigator.clipboard.writeText('').then(() => {
				                    console.log("Text copied to clipboard");
				                  }).catch(error => {
				                    console.log("Error copying text to clipboard:", error);
				                  });
				            })
				            .catch(err => {
				              console.log('Error reading from clipboard:', err);
				            });
				        }


                }, 450);
            	$('#sPUModal').modal('show');
            }
            }).dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('name');?>]", filter_type: "text", mdata: []},
            {column_number: 1, filter_default_label: "[<?=lang('price');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('quantity');?>]", filter_type: "text", data: []},
            ], "footer");

            $('#unit_prices_table thead').remove();
    }
    $(document).on('click', '.add_item_unit', function() {

        var product_id = $(this).data('productid');
        var unit_price_id = $(this).data('itemunitid');

        var unit_data = {
                            'product_id' : product_id,
                            'unit_price_id' : unit_price_id,
                        };

        localStorage.setItem('unit_data', JSON.stringify(unit_data));

        $('#unit_quantity').select();
        // $('#unit_quantity').val(1).select();

    });
    $(document).on('keyup', '#unit_quantity', function(e){
    	var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
        var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');
        var productunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productunitid');
        var warehouse_id = $('#poswarehouse').val();
        var unit_quantity = $('#unit_quantity').val();

        if (e.keyCode == 13) {
            if (unit_data = JSON.parse(localStorage.getItem('unit_data'))) {
            	unit_data = {
                            'product_id' : productid,
                            'unit_price_id' : itemunitid,
            				'product_unit_id': productunitid,
                        };
                localStorage.removeItem('unit_data');
                var warehouse_id = $('#poswarehouse').val();
                var unit_quantity = $(this).val();
                $.ajax({
                    url:site.base_url+"sales/iusuggestions",
                    type:"get",
                    data:{
	            		'product_id' : unit_data.product_id,
	            		'unit_price_id' : unit_data.unit_price_id,
	                	'product_unit_id' : unit_data.product_unit_id,
	            		'warehouse_id' : warehouse_id,
	            		'unit_quantity' : unit_quantity,
	            		'biller_id' : $('#posbiller').val(),
	            		'customer_id' : $('#poscustomer').val(),
	            		'address_id' : $('#poscustomer_branch').val(),
	            	}
                }).done(function(data){
                    add_invoice_item(data);
                    $('#sPUModal').modal('hide');
                });
            } else {
                $('#unit_quantity').val(1).select();
            }
        }
    });
    $(document).on('click', '.send_item_unit_select', function(){
        var itemunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('itemunitid');
        var productid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productid');
        var productunitid = $('input[name="radio_2"]:checked').closest('.add_item_unit').data('productunitid');
			var unit_data = {
                           'product_id' : productid,
                           'unit_price_id' : itemunitid,
         						'product_unit_id': productunitid,
                        };
        var warehouse_id = $('#poswarehouse').val();
        var unit_quantity = $('#unit_quantity').val();
        $.ajax({
            url:site.base_url+"sales/iusuggestions",
            type:"get",
            data:{
            		'product_id' : unit_data.product_id,
            		'unit_price_id' : unit_data.unit_price_id,
                	'product_unit_id' : unit_data.product_unit_id,
            		'warehouse_id' : warehouse_id,
            		'unit_quantity' : unit_quantity,
            		'qty' : unit_quantity,
            		'base_qty' : unit_quantity,
            		'biller_id' : $('#posbiller').val(),
            		'customer_id' : $('#poscustomer').val(),
            		'address_id' : $('#poscustomer_branch').val(),
            	}
        }).done(function(data){
            add_invoice_item(data);
            $('#sPUModal').modal('hide');
        });
	});
	$('#sPUModal').on('shown.bs.modal', function () {
    	// $('#unit_prices_table_filter .input-xs').focus();
    	$('#unit_prices_table_length').remove();
    	$('#unit_prices_table_filter').remove();
    	$('#unit_prices_table_info').remove();
    	$('#unit_prices_table_paginate').remove();
    	$('#unit_quantity').val(1);
	});
    $(document).on('hide.bs.modal', '#sPUModal', function (e) {
      e.stopPropagation() // stops modal from being shown
    });
    $(document).on('ifClicked', '.select_auto_2', function(e){
		var index = $('.select_auto_2').index($(this));
		$('.add_item_unit').eq(index).trigger('click');
	});
	$(document).on('keyup', '.select_auto_2', function(e){
		if (e.keyCode == 13) {
			$(this).closest('.add_item_unit').trigger('click');
		}
	});


	function validate_existing_selected_table() {
		<?php if ($this->pos_settings->restobar_mode == YES) { ?>
			<?php if (!isset($sid)) { ?>
				<?php if ($this->pos_settings->table_service == YES) { ?>
					if (localStorage.getItem('restobar_mode_module') == 1) {
						Command: toastr.error('No se ha seleccionado la mesa para el pedido.', 'Mensaje de Validación', {onHidden: function(){
							$('#boton_mesa').trigger('click');
						}});
						return false;
					} else {
						return true;
					}
				<?php } else  { ?>
					if (localStorage.getItem('restobar_mode_module') == false || localStorage.getItem('restobar_mode_module') == 'false') {
						Command: toastr.error('No se ha seleccionado el tipo de orden.', 'Mensaje de Validación');

						return false;
					}
					return true;
				<?php } ?>
			<?php } else { ?>
				return true;
			<?php } ?>
		<?php } else { ?>
			return true;
		<?php } ?>
	}
	$(document).ready(function() {
	    $(document).on('click', '.table_button', function () {
	    	select_table($(this).data('table_id'), $(this).data('table_number'), $(this).data('table_status'), $(this).data('table_suspended_bill_id'), $('#posbiller').val());
	    });

		$(document).on('click', '#checkQuotaKiowa', function () { checkQuotaKiowa() } )
	});
	function select_table(id, number, status, suspended_bill_id, biller_id) {
        if (status == 1) {
            $('#formulario_crear_cuenta #id_mesa').val(id);
            $('#formulario_crear_cuenta #numero_mesa').val(number);
            $('#formulario_crear_cuenta #id_sucursal').val(biller_id);

            $('#formulario_crear_cuenta').submit();
        } else {
            if (status == 2) {
                Command: toastr.error('La mesa se encuentra: No disponible', 'Mensaje del sistema.');
            } else if (status == 3) {
                if (suspended_bill_id != '') {
                    window.location.href = site.base_url +'pos/index/'+ suspended_bill_id;
                }
            } else if (status == 4) {
                Command: toastr.error('La mesa se encuentra: Reservada', 'Mensaje del sistema.');
            }
        }
    }

    function showHideAlertFE(subtotal)
    {
        <?php if ($this->pos_settings->activate_electronic_pos == YES && $this->pos_settings->mode_electronic_pos == 2) : ?>
            if (formatDecimal(subtotal) >= formatDecimal('<?= $this->pos_settings->uvt_value ?>')) {
                $("#doc_type_id option").each(function(){
                    if ($(this).data('fe') == 0) {
                        $(this).remove();
                    } else {
                    	if (!$('#doc_type_id').val()) {
                        	$('#doc_type_id').select2('val', $(this).val());
                    	}
                    }
                });
            } else {
                var index = 0;
                $('#doc_type_id').html(_docTypeOptions);
                $("#doc_type_id option").each(function(){
                    if ($(this).data('fe') == 1) {
						if ($(this).attr('selected')) {
							index = $(this).val();
						}
                        $(this).removeAttr('selected');
                    } else {
                        index = $(this).val();
                    }
                });

                if (!$('#doc_type_id').val()) {
                	posdocumenttypedefault = $('#posbiller option:selected').data('posdocumenttypedefault');
                	$('#doc_type_id').select2('val', (posdocumenttypedefault ? posdocumenttypedefault : index));
                }
            }
            $('#doc_type_id').trigger('change');
        <?php endif ?>
    }

    function add_payment_input(set_balance = true) {
        let ia = $(".amount");
        if (set_balance) {
            let total_paying = 0;
            $.each(ia, function (i) {
                let this_amount = formatCNum($(this).val() ? $(this).val() : 0);
                total_paying += parseFloat(this_amount);
            });
            balance = gtotal - total_paying;
            balance = balance < 0 ? 0 : balance;
        }
        $('#pcc_type_1').select2('destroy');
        $('#pcc_type_1, #pcc_type_' + pa).select2({minimumResultsForSearch: 7});

        // if (balance > 0) {
            $('#paid_by_1').select2('destroy');
            $('#pcc_type_1').select2('destroy');
            var phtml = $('#payments').html(),
            update_html = phtml.replace(/_1/g, '_' + pa);
            pi = 'amount_' + pa;
            $('#multi-payment').append(update_html);
            $('#pcc_type_1, #pcc_type_' + pa).select2({minimumResultsForSearch: 7});
            $('.row_payment').eq(pa-1).append('<button type="button" class="close close-payment"><i class="fa fa-2x">&times;</i></button>');
            var ppa = pa;
            $('#paid_by_'+ppa).select2({
                formatResult: payment_method_icons,
                formatSelection: payment_method_icons,
                escapeMarkup: function (m) {
                return m;
                }
            });
            if (set_balance) {
                read_card();
                setTimeout(function() {
                    var total_paying = 0;
                    var ia = $(".amount");
                    $.each(ia, function (i) {
                        var this_amount = formatCNum($(this).val() ? $(this).val() : 0);
                        total_paying += parseFloat(this_amount);
                    });
                    balance = gtotal - total_paying;
                    balance = balance < 0 ? 0 : balance;
                    $('#amount_'+ppa).val(parseFloat(balance)).select();
                }, 600);
            }
            pa++;
        // }
	}

	// this section is for converting pocs to posfe
	$(document).ready(function(){
		<?php if (isset($_GET['fe_pos_sale_id']) && $_GET['fe_pos_sale_id'] != '' ) { ?>
			localStorage.setItem('pos_to_posfe_transformation', 1);
			$('#posbiller').prop('disabled', true);
			$('.view_order_detail').prop('disabled', true);
			$('#suspend').prop('disabled', true);
			$('#pos-sale-form #leftdiv #left-top .no-print').hide();
			$('#open-category').prop('disabled', true);
			$('#open-subcategory').prop('disabled', true);
			$('#open-brands').prop('disabled', true);
			$('#open-promotions').prop('disabled', true);
			$('#open-favorites').prop('disabled', true);
			$('#open-featured').prop('disabled', true);
			$('#posseller').prop('disabled', true);
			<?php if(isset($fe_pos_sale_id)) { ?>
				localStorage.removeItem('locked_for_biller_fe_data');
			<?php } ?>
			<?php if (!isset($fe_pos_sale_id)) { ?> // this section when document type is not assigned
				localStorage.setItem('locked_for_biller_fe_data', 1);
			<?php }?>
		<?php } ?>
		<?php if(!isset($_GET['fe_pos_sale_id'])) { ?>
			if (localStorage.getItem('pos_to_posfe_transformation')) {
				localStorage.removeItem('pos_to_posfe_transformation');
			}
		<?php }?>
	})

	function checkQuotaKiowa()
	{
		swal({
			title: 'Consultando Cupo Disponible en Kiowa...',
			text: 'Por favor, espere un momento.',
			type: 'info',
			showCancelButton: false,
			showConfirmButton: false,
			closeOnClickOutside: false,
		});
		$.ajax({
			type: 'post',
			url: site.base_url+'Pos/checkQuotaKiowa',
			data: {
				'<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
				'customerId' : $('#poscustomer').val()
			},
			dataType: 'json',
			success: function (response) {
				if (response.status == true) {
					let modal = response.data

					$('#checkQuoteModal').html(modal)
					$('#checkQuoteModal').modal('show')
				}

				swal.close()
			}
		});
	}

    // $(document).ready(function() {
    //     $(document).on('change', '#frequency', function() {
    //         getAmountInstallments($(this))
    //     });
    // })



    function validatePaymentMethodForCreditFinancing(element, pa_no)
    {
        let paymentMethodId = element.val()
        let paymentMethodForCreditFinancing = `<?= $this->Settings->payment_method_for_credit_financing ?>`
        let enableForCreditFinancing = `<?= $this->Settings->enable_for ?>`
        let financingModule = `<?= $this->Settings->financing_module ?>`

        if (financingModule == 1 && (paymentMethodForCreditFinancing == null || paymentMethodForCreditFinancing == '')) {
            swal({
                title: '<?= $this->lang->line('toast_alert_title') ?>',
                text: '<?= lang('payment_method_financing_without_configuration') ?>',
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            });

            return false;
        } else {
            if (paymentMethodId == paymentMethodForCreditFinancing) {
                $(`#amount_${pa_no}`).prop('readonly', true)

                if ($('#amountInstallementContainer').length == 0) {
                    let amountInstallementContainerHtml = `<div id="amountInstallementContainer" class="row">
                        <div class="col-sm-6">
                            <h3 for="frequency" class="title_payment text-right"><?= lang('frequency') ?></h3>
                            <select class="form-control" name="frequency" id="frequency" required>
                                <option value="">Seleccionar</option>
                                <option value="1">Quncenal</option>
                                <option value="2">Mensual</option>
                            </select>
                        </div>
                        <div class="col-sm-6">
                            <h3 for="installments" class="title_payment"><?= lang('installments') ?></h3>
                            <select class="form-control" name="installment" id="installment" required>
                                <option value="">Seleccionar</option>
                            </select>
                        </div>
                    </div>`
                    element.parents('.row_payment').append(amountInstallementContainerHtml)
                }
            } else {
                $('#amountInstallementContainer').remove()
            }
        }
    }

    function getAmountInstallments(element)
    {
        let installmentsOptionHtml = '<option value="">Seleccionar</option>'
        if (element.val() == 1) {
            installmentsOptionHtml += `<option value="6">6</option>
            <option value="9">9</option>
            <option value="12">12</option>
            <option value="18">18</option>`
        } else if (element.val() == 2) {
            installmentsOptionHtml += `<option value="1">1</option>
            <option value="3">3</option>
            <option value="5">5</option>
            <option value="6">6</option>
            <option value="9">9</option>`
        }

        $('#installment').html(installmentsOptionHtml);
    }

    function validateQuotaFields()
    {
        let paymentMethodForCreditFinancing = `<?= $this->Settings->payment_method_for_credit_financing ?>`
        let financingModule = `<?= $this->Settings->financing_module ?>`
        let  validateInputs = false;

        if (financingModule) {
            $('select.paid_by').each(function(i) {
                if ($(this).val() == paymentMethodForCreditFinancing) {
                    validateInputs = true
                }
            });

            if (validateInputs == true) {
                if ($('#frequency').val() == '') {
                    swal({
                        title: '<?= $this->lang->line('toast_alert_title') ?>',
                        text: `<?= sprintf(lang('field_required'), lang('frequency')) ?>`,
                        type: 'warning',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    });
                    return false
                } else if ($('#installment').val() == '') {
                    swal({
                        title: '<?= $this->lang->line('toast_alert_title') ?>',
                        text: `<?= sprintf(lang('field_required'), lang('installments')) ?>`,
                        type: 'warning',
                        showCancelButton: true,
                        showConfirmButton: false,
                        cancelButtonText: "Ok"
                    });
                    return false
                } else {
                    return true
                }
            } else {
                return true
            }
        } else {
            return true
        }
    }
</script>