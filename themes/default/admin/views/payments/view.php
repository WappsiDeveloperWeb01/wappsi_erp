<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg" id="modalMultiPayment">
    <div class="modal-content">
        <div class="modal-header no-print">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
              <i class="fa fa-2x">&times;</i>
          </button>
          <a class="btn btn-xs btn-default no-print pull-right" href="<?= admin_url('payments/multi_payment_view/').$enc->reference_no ?>" target="blank"> <i class="fa fa-print"></i> <?= lang('print') ?></a>
        </div>
        <div class="modal-body row" id="printesto">
            <style type="text/css">
              @media print {
                .title{
                  display: initial;
                }
                table > tbody > tr > td {
                  font-size: 80%;
                }
              }

              .border {
                border-style: solid;
                border-width: 1px;
              }

              .no-right {
                border-right-width: 0px;
              }

              .no-left {
                border-left-width: 0px;
              }

              .no-sides {
                border-left-width: 0px;
                border-right-width: 0px;
              }

              span {
                font-size: 120%;
              }
            </style>
            <div class="row">
              <div class="col-xs-6">
                  <img src="<?= base_url().'assets/uploads/logos/' . $biller->logo; ?>" alt="<?= $biller->company != '-' ? $biller->company : $biller->name; ?>">
              </div>
              <div class="col-xs-6">
                <span><b><?= $document_type ? $document_type->nombre : lang('invoice_register') ?></span></b><br>
                <span><?= lang('reference_no') ?> : <?= $enc->reference_no ?></span>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12" style="padding-top: 23px;">
                <span><b><?= lang('third') ?></b> :<br> <?= $enc->customer ?></span>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-3">
                <span><b><?= lang('vat_no') ?></b> :<br> <?= $enc->vat_no ?></span>
              </div>
              <div class="col-xs-3">
                <span><b><?= lang('date') ?></b> :<br> <?= $enc->date ?></span>
              </div>
              <div class="col-xs-3">
                <span><b><?= lang('paid_by') ?></b> :<br> <?= $enc->paid_by ?></span>
              </div>
              <div class="col-xs-3">
                <?php 
                  $enc_total_amount = 0;
                  foreach ($pmnts as $pmnt) {
                      if ($pmnt->type == 'discount') {
                          continue;
                      }
                      $total_retentions = 
                    (!isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0) + 
                    (!isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0) + 
                    (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0) + 
                    (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total : 0) + 
                    (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_autoaviso_total : 0) + 
                    (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0);
                      $enc_total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
                  }
                 ?>
                <span><b><?= lang('total') ?></b> :<br> <?= $this->sma->formatMoney($enc_total_amount) ?></span>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-3">
                <span><b><?= lang('payment_date') ?></b> :<br> <?= $enc->payment_date ?></span>
              </div>
              <div class="col-xs-3">
                <span><b><?= lang('consecutive_payment') ?></b> :<br> <?= $enc->consecutive_payment ?></span>
              </div>
            </div>

            <div class="col-xs-12">
              <span><b><?= lang('note') ?></b> :<br> <?= $enc->note ?></span>
            </div>

            <hr class="col-xs-11">

            <div class="col-xs-12">
              <table class="table col-xs-12" style="width: 100%;">
                <thead>
                  <tr>
                    <th><?= lang('affects_to') ?></th>
                    <th><?= lang('value') ?></th>
                    <th><?= lang('rete_fuente') ?></th>
                    <th><?= lang('rete_iva') ?></th>
                    <th><?= lang('rete_ica') ?></th>
                    <th><?= lang('rete_other') ?></th>
                    <th><?= lang('total_discount') ?></th>
                    <th><?= lang('amount_paid') ?></th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                  $total_amount = 0;
                  $total_rete_fuente = 0;
                  $total_rete_iva = 0;
                  $total_rete_ica = 0;
                  $total_rete_other = 0;
                  $total_discount = 0;
                  $total_received = 0;
                   ?>
                  <?php if ($pmnts): ?>
                    <?php
                    $sales_discount = []; 
                    foreach ($pmnts as $pmnt) {
                      if ($pmnt->type == 'discount') {
                        if (isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id])) {
                          $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] += $pmnt->amount;
                        } else {
                          $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] = $pmnt->amount;
                        }
                      } else {
                        continue;
                      }
                    }
                     ?>
                    <?php foreach ($pmnts as $pmnt): ?>
                      <?php if($pmnt->type == 'discount') { continue; } ?>
                      <?php 
                          $sale_discount = isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] : 0;
                          $total_discount += $sale_discount;
                          $total_rete_fuente += !isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0;
                          $total_rete_iva += !isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0;
                          $total_rete_ica += !isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0;
                          $total_rete_other += (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0) + (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total + $pmnt->rete_autoaviso_total : 0);
                          $total_received += ($pmnt->reference_no != NULL ? $pmnt->amount : 0 ) + $sale_discount;
                          $total_retentions =
                            (!isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0) + 
                            (!isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0) + 
                            (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0) + 
                            (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total : 0) + 
                            (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_autoaviso_total : 0) + 
                            (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0);
                          $total_amount += ($pmnt->reference_no != NULL ? $pmnt->amount - $total_retentions : $pmnt->amount);
                       ?>
                      <tr>
                        <td><?= $pmnt->reference_no != NULL ? $pmnt->reference_no : $pmnt->note ?></td>
                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->reference_no != NULL ? $pmnt->amount + (isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] : 0) : 0) ?></td>
                        <td class="text-right"><?= $this->sma->formatMoney(!isset($pmnt->rete_fuente_assumed) || $pmnt->rete_fuente_assumed == 0 ? $pmnt->rete_fuente_total : 0) ?></td>
                        <td class="text-right"><?= $this->sma->formatMoney(!isset($pmnt->rete_iva_assumed) || $pmnt->rete_iva_assumed == 0 ? $pmnt->rete_iva_total : 0) ?></td>
                        <td class="text-right"><?= $this->sma->formatMoney(!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_ica_total : 0) ?></td>
                        <?php $rete_other = (!isset($pmnt->rete_other_assumed) || $pmnt->rete_other_assumed == 0 ? $pmnt->rete_other_total : 0) + (!isset($pmnt->rete_ica_assumed) || $pmnt->rete_ica_assumed == 0 ? $pmnt->rete_bomberil_total + $pmnt->rete_autoaviso_total : 0); ?>
                        <td class="text-right"><?= $this->sma->formatMoney($rete_other) ?></td>
                        <td class="text-right"><?= $this->sma->formatMoney(isset($sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id]) ? $sales_discount[$pmnt->sale_id ? $pmnt->sale_id : $pmnt->purchase_id] * -1 : 0) ?></td>
                        <td class="text-right"><?= $this->sma->formatMoney($pmnt->amount - $total_retentions) ?></td>
                      </tr>
                    <?php endforeach ?>
                  <?php endif ?>
                    <tr>
                      <th><?= lang('total') ?></th>
                      <td class="text-right"><?= $this->sma->formatMoney($total_received) ?></td>
                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_fuente) ?></td>
                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_iva) ?></td>
                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_ica) ?></td>
                      <td class="text-right"><?= $this->sma->formatMoney($total_rete_other) ?></td>
                      <td class="text-right"><?= $this->sma->formatMoney(($total_discount * -1)) ?></td>
                      <td class="text-right"><?= $this->sma->formatMoney($total_amount) ?></td>
                    </tr>
                </tbody>
              </table>
            </div>

            <hr class="col-xs-11"></hr>
              
            <div class="col-xs-3 border no-right">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p style="border-bottom: 1px solid #666;">&nbsp;</p>
              <p style="text-align: center;"><?= lang('elaborated') ?></p>
            </div>
            <div class="col-xs-3 border no-sides">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p style="border-bottom: 1px solid #666;">&nbsp;</p>
              <p style="text-align: center;"><?= lang('approved') ?></p>
            </div>
            <div class="col-xs-3 border no-left">
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p style="border-bottom: 1px solid #666;">&nbsp;</p>
              <p style="text-align: center;"><?= lang('accounted') ?></p>
            </div>
            <div class="col-xs-3 border">
              <p><?= lang('signature_and_received') ?> :</p>
              <p>&nbsp;</p>
              <p style="border-bottom: 1px solid #666;">&nbsp;</p>
              <p><?= lang('vat_no_2') ?> :</p>
            </div>

        </div>
        <?php if ($created_user): ?>
          <div class="modal-footer">
            <div class="col-xs-12">
              <div class="text-center">
                <p><?= lang("created_by"); ?>: <?= $created_user->first_name. ' ' . $created_user->last_name ?> </p>
              </div>
            </div>    
          </div>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });

    function printDiv(nombreDiv) {
      var divToPrint=document.getElementById(nombreDiv);
      var newWin=window.open('','Print-Window');
      var header = $('head').html();
      newWin.document.open();
      newWin.document.write('<html><head>'+header+'</head><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');
      newWin.document.close();
      setTimeout(function(){newWin.close();},10);
    }
</script>
