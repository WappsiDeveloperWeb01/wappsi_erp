<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('edit_price_group'); ?></h4>
        </div>
        <?php $attrib = array('data-toggle' => 'validator', 'role' => 'form');
        echo admin_form_open("system_settings/edit_price_group/" . $id, $attrib); ?>
        <div class="modal-body">
            <p><?= lang('enter_info'); ?></p>

            <div class="form-group">
                <label class="control-label" for="name"><?php echo $this->lang->line("group_name"); ?></label>
                <?php echo form_input('name', $price_group->name, 'class="form-control" id="name" required="required"'); ?>
            </div>
            <div class="form-group">
                <label>
                    <input type="checkbox" name="price_group_base" id="price_group_base" <?= $price_group->price_group_base == 1 ? 'checked' : '' ?>>
                    <?= lang('price_group_base') ?>
                </label>
            </div>
            <div class="form-group">
                <label>
                    <input type="checkbox" name="minimun_list" id="minimun_list" <?= $price_group->minimun_list == 1 ? 'checked' : '' ?>>
                    <?= lang('minimun_list') ?>
                </label>
            </div>
        </div>
        <div class="modal-footer">
            <?php echo form_submit('edit_price_group', lang('edit_price_group'), 'class="btn btn-primary"'); ?>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>

<script type="text/javascript">
    
    $(document).on('ifChecked', '#price_group_base', function(){
        command: toastr.warning('Si ya existe otro grupo marcado cómo base, se desmarcará para asignarle dicho parámetro a esta lista.', '¡Atención!', {
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "4000",
            "extendedTimeOut": "1000",
        });
    });

</script>

<?= $modal_js ?>