<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css">
    .col-sm-3, .col-md-2, .col-sm-1 {
        padding-right: 5px;
        padding-left: 5px;
    }

    .iradio_square-blue {
        margin-top: 5px;
    }
</style>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row" id="sortable-view">
        <div class="col-lg-12">
            <div class="ibox ">
                <div class="ibox-content">
                    <?= admin_form_open_multipart("employees/save", ["class" => "wizard-big wizard", "id" => "add_employee_form"]); ?>
                        <?= form_hidden("add_contract", "0"); ?>
                        <h1><?= lang('basic_data') ?></h1>
                        <section>
                            <div class="row">
                                <div class="col-md-3 text-center">
                                    <span class="fileinput-info"><i class="fa fa-info-circle fa-lg" data-html="true" data-toggle="popover" data-placement="bottom" data-content="<small><b>Formato: </b>.jpg, .png<br><b>Dimensión: </b>800px*800px<br><b>Peso max: </b> 2MB</small>"></i></span>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new img-thumbnail" style="width: 140px; height: 140px;"></div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 140px; max-height: 140px;  margin-bottom: 5px;"></div>
                                        <div>
                                            <span class="btn btn-primary btn-file btn-sm">
                                                <span class="fileinput-new">Seleccionar</span>
                                                <span class="fileinput-exists">Cambiar</span>
                                                <input type="file" name="employee_photo" id="employee_photo" accept=".jpg,.png,.jpeg">
                                            </span>
                                            <a href="#" class="btn btn-default btn-sm fileinput-exists" data-dismiss="fileinput">Borrar</a>
                                        </div>
                                    </div>
                                    <label id="employee_photo-error" class="error" for="employee_photo" style="display: none;"></label>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("first_name"), "first_name"); ?>
                                        <?php $first_name = $this->session->userdata("first_name"); ?>
                                        <?= form_input(["name"=>"first_name", "id"=>"first_name", "class"=>"form-control", "required"=>TRUE], $first_name); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("second_name"), "second_name"); ?>
                                        <?= form_input(["name"=>"second_name", "id"=>"second_name", "class"=>"form-control"]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("first_lastname"), 'first_lastname'); ?>
                                        <?= form_input(["name"=>"first_lastname", "id"=>"first_lastname", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("second_lastname"), 'second_lastname'); ?>
                                        <?= form_input(["name"=>"second_lastname", "id"=>"second_lastname", "class"=>"form-control"]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("document_type"), 'tipo_documento'); ?>
                                        <select class="form-control not_select" name="tipo_documento" id="tipo_documento" required="required">
                                            <option value=""><?= lang("select"); ?></option>
                                            <?php foreach ($document_types as $document_type): ?>
                                                <?php $selected = ($document_type->id == 3)?"selected":""; ?>
                                                <option value="<?= $document_type->id ?>" data-code="<?= $document_type->codigo_doc; ?>" <?= $selected; ?>><?= lang($document_type->nombre); ?></option>
                                            <?php endforeach ?>
                                            <?= form_hidden("document_code", ""); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("document_number"), 'vat_no'); ?>
                                        <?= form_input(["name"=>"vat_no", "id"=>"vat_no", "class"=>"form-control tip", "required"=>TRUE]); ?>
                                        <?= form_hidden("digito_verificacion", ""); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("educational_level"), "educational_level"); ?>
                                        <?php $bopts = [""=>lang("select"), 1=>"Básica primaria", 2=>"Básica Secundaria", 3=>"Educación Media técnica", 4=>"Técnico profesional", 5=>"Tecnología", 6=>"Profesional Universitario", 7=>"Especialización", 8=>"Maestría", 9=>"Doctorado", 10=>"Postdoctorado"]; ?>
                                        <?= form_dropdown(["name"=>"educational_level", "id"=>"educational_level", "class"=>"form-control not_select", "required"=>TRUE], $bopts); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("profession"), 'profession'); ?>
                                        <?= form_input(["name"=>"profession", "id"=>"profession", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("marital_status"), 'marital_status'); ?>
                                        <?php $mopts = [""=>lang("select"), 1=>lang("free_union"), 2=>lang("married"), 3=>lang("divorced"), 4=>lang("single"), 5=>lang("widower")]; ?>
                                        <?= form_dropdown(["name"=>"marital_status", "id"=>"marital_status", "class"=>"form-control not_select", "required"=>TRUE], $mopts); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("blood_type"), "blood_type"); ?>
                                        <?php $bopts = [""=>lang("select"), 1=>"A+", 2=>"A-", 3=>"B+", 4=>"B-", 5=>"O+", 6=>"O-", 7=>"AB+", 8=>"AB-"]; ?>
                                        <?= form_dropdown(["name"=>"blood_type", "id"=>"blood_type", "class"=>"form-control not_select", "required"=>TRUE], $bopts); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("birthdate"), 'birthdate'); ?>
                                        <?= form_input(["name"=>"birthdate", "id"=>"birthdate", "type"=>"date", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("gender"), 'gender'); ?>
                                        <?php $gopts = [""=>lang("select"), MALE=>lang("male"), FEMALE=>lang("female"), UNDEFINED_SEX=>lang('undefined_sex')]; ?>
                                        <?= form_dropdown(["name"=>"gender", "id"=>"gender", "class"=>"form-control not_select", "required"=>TRUE], $gopts); ?>
                                    </div>
                                </div>

                                <div class="col-md-3" id="military_card_container">
                                    <div class="form-group">
                                        <?= form_label(lang("military_card"), 'military_card'); ?>
                                        <?= form_input(["name"=>"military_card", "id"=>"military_card", "class"=>"form-control"]); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("country").' '.lang('residency'), 'country'); ?>
                                        <select class="form-control not_select" name="country" id="country" required>
                                            <option value=""><?= lang("select"); ?></option>
                                            <?php foreach ($countries as $country): ?>
                                                <option value="<?= $country->NOMBRE ?>" data-code="<?= $country->CODIGO ?>"><?= $country->NOMBRE ?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <?= form_hidden("country_code", ""); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("state").' '.lang('residency'), "state"); ?>
                                        <select class="form-control not_select" name="state" id="state" required>
                                            <option value=""><?= lang("select"); ?></option>
                                        </select>
                                        <?= form_hidden("state_code", ""); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("city").' '.lang('residency'), "city"); ?>
                                        <select class="form-control not_select" name="city" id="city" required>
                                            <option value=""><?= lang("select"); ?></option>
                                        </select>
                                        <?= form_hidden("city_code", ""); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("address"), 'address'); ?>
                                        <?= form_input(["name"=>"address", "id"=>"address", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("postal_code"), 'postal_code'); ?>
                                        <?= form_input(["name"=>"postal_code", "id"=>"postal_code", "class"=>"form-control postal_code"]); ?>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("employees_email"), 'email'); ?>
                                        <?= form_input(["name"=>"email", "id"=>"email", "type"=>"email", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("employees_phone"), 'phone'); ?>
                                        <?= form_input(["name"=>"phone", "id"=>"phone", "type"=>"tel", "class"=>"form-control", "required"=>TRUE]); ?>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label(lang("home_phone"), 'home_phone'); ?>
                                        <?= form_input(["name"=>"home_phone", "id"=>"home_phone", "type"=>"tel", "class"=>"form-control"]); ?>
                                    </div>
                                </div>
                            </div>
                        </section>

                        <h1><?= lang('contacts_data') ?></h1>
                        <section>
                            <div id="contact_data_container">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <?= form_label(lang("name"), "contact_name"); ?>
                                            <?= form_input(["name"=>"contact_name[]", "id"=>"contact_name", "class"=>"form-control", "required"=>TRUE]); ?>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <?= form_label(lang("relationship"), "relationship") ?>
                                            <?php $ropts = [""=>lang("select"), 1=>"Conyuge", 2=>"Padre", 3=>"Madre", 4=>"Hijo", 5=>"Hija", 6=>"Abuelo", 7=>"Abuela", 8=>"Otros"]; ?>
                                            <?= form_dropdown(["name"=>"relationship[]", "id"=>"relationship", "class"=>"form-control not_select", "required"=>TRUE], $ropts); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= form_label(lang("address"), "contact_address"); ?>
                                            <?= form_input(["name"=>"contact_address[]", "id"=>"contact_address", "class"=>"form-control", "required"=>TRUE]); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group">
                                            <?= form_label(lang("phone"), "contact_phone"); ?>
                                            <?= form_input(["name"=>"contact_phone[]", "id"=>"contact_phone", "type"=>"tel", "class"=>"form-control", "required"=>TRUE]); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <?= form_label(lang("email"), "contact_email"); ?>
                                            <?= form_input(["name"=>"contact_email[]", "id"=>"contact_email", "type"=>"email", "class"=>"form-control"]); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-1">
                                        <div class="form-group text-center">
                                            <?= form_label(lang('employees_main_contact'), 'main_contact'); ?>
                                            <br>
                                            <?= form_radio(['name'=>'main_contact', 'id'=>'main_contact_0', 'required'=>TRUE], 0, TRUE); ?>
                                        </div>
                                    </div>

                                    <div class="col-sm-1 text-center">
                                        <?= form_button(["type"=>"button", "class"=>"btn btn-default new-button remove_contact_fields tip", "disabled"=>TRUE, "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang("delete"), "style"=>"margin-top: 29px;"], '<i class="fa fa-trash"></i>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 text-right">
                                    <hr>
                                    <?= form_button(["id"=>"add_contact_fields", "type"=>"button", "class"=>"btn btn-primary new-button tip", "data-toggle"=>"tooltip", "data-placement"=>"top", "title"=>lang("add")], '<i class="fa fa-plus"></i>'); ?>
                                </div>
                            </div>
                        </section>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        _number_contact_fields = 1;
        _consecutive_contact_fields = 1;

        $('[data-toggle="tooltip"]').tooltip();
        $("[data-toggle=popover]").popover();

        load_wizard_configuration();

        $('#remove_contact_fields').fadeOut();
        $(document).on('change', '#vat_no', function(){ validate_existing_document_number(); });
        $(document).on('change', '#country', function() { load_states(); });
        $(document).on('change', '#state', function(){ load_cities(); });
        $(document).on('change', '#city', function(){ load_zones(); });
        $(document).on('change', '#gender', function(){ enable_disable_military_card_input(); });
        $(document).on('change', '#email', function(){ validate_existing_email(); });
        $(document).on('click', '#add_contact_fields', function(){ add_contact_fields();});
        $(document).on('click', '.remove_contact_fields', function(){ remove_contact_fields($(this)); });
        $(document).on('click', '#add_contract', function(){ $('input[name="add_contract"]').val(1); $("#add_employee_form").steps("finish"); });
        $(document).on('click', '#cancel_button', function(){ window.location = '<?= admin_url("employees"); ?>'; });

        $('#employee_photo').change(function() {
            $('#employee_photo').removeData('imageWidth');
            $('#employee_photo').removeData('imageHeight');
            var file = this.files[0];
            var tmpImg = new Image();
            tmpImg.src=window.URL.createObjectURL( file );
            tmpImg.onload = function() {
                width = tmpImg.naturalWidth,
                height = tmpImg.naturalHeight;
                $('#employee_photo').data('imageWidth', width);
                $('#employee_photo').data('imageHeight', height);
            }
        });

        if (sessionStorage.getItem('country') != '') { $('#country').trigger('change'); }
    });

    function load_wizard_configuration()
    {
        var form = $("#add_employee_form");
        wizard = form.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
            labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onInit: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();

                var cancel_button = $("<li>").attr("aria-disabled",false).append('<a id="cancel_button"><i class="fas fa-times fa-lg" data-toggle="tooltip" title="Cancelar" data-placement="top"></i></a>');
                $(document).find(".actions ul").prepend(cancel_button);

                load_temporary_data();
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                $.validator.addMethod("filesize_max", function(value, element, param){
                    return this.optional(element) || (element.files[0].size <= param)
                }, 'El tamaño de la imágen debe ser menor a {0} KB');

                $.validator.addMethod('ratio_aspecto', function(value, element, param) {
                    if (this.optional(element)) {
                        return true;
                    }

                    function gcd(a, b) {
                        if (b > a) {
                            temp = a;
                            a = b;
                            b = temp
                        }
                        while (b != 0) {
                            m = a % b;
                            a = b;
                            b = m;
                        }
                        return a;
                    }

                    function ratio(x, y) {
                        c = gcd(x, y);
                        return "" + (x / c) + ":" + (y / c)
                    }

                    var width = $(element).data('imageWidth');
                    var height = $(element).data('imageHeight');
                    var ratio = ratio(width, height);

                    if (ratio == param) {
                        return true;
                    }

                    return false;
                },'Por favor ingresar una imágen cuadrada');

                $.validator.addMethod('resolution', function(value, element, param){
                    if (this.optional(element)) {
                        return true;
                    }

                    var width = $(element).data('imageWidth');
                    var height = $(element).data('imageHeight');

                    if (width <= param && height <= param) {
                        if (width == height) {
                            return true;
                        }
                    }

                }, 'El resolución de la imágen debe ser de {0}px * {0}px o menor');

                form.validate({
                    rules: {
                        employee_photo: {
                            accept: "image/png,image/jpeg",
                            filesize_max : 512000,
                            ratio_aspecto:"1:1",
                            resolution:"800"
                        },
                        educational_level:"required",
                        country:"required",
                        state:"required",
                        gender:"required"
                    },
                    messages: {
                        educational_level: "Este campo es obligatorio",
                        country: "Este campo es obligatorio",
                        state: "Este campo es obligatorio",
                        gender: "Este campo es obligatorio"
                    },
                    highlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").addClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").addClass('mierroArrow');
                        } else {
                            elem.addClass(errorClass);
                        }
                    },
                    unhighlight: function (element, errorClass, validClass) {
                        var elem = $(element);
                        if (elem[0].tagName == "SELECT") {
                            $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                            $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                        } else {
                            elem.removeClass(errorClass);
                        }
                    }
                }).settings.ignore = ":disabled,:hidden:not(.validate)";

                create_temporary_data(currentIndex);
                return form.valid();
            },
            onStepChanged: function(event, currentIndex)
            {
                $('section:not(:hidden) select[required="required"]').addClass('validate');
                $('select:not(:hidden)').select2();

                // if(currentIndex == 1) {
                //     var create_button = $("<li>").attr("aria-disabled",false).append('<a id="add_contract" style="background: #fff;color: #1484c6;border: 1px solid #1484c6">Guardar y crear Contrato</a>');

                //     $(document).find(".actions ul").append(create_button);
                // }else{
                //   $(".actions").find("#add_contract").remove();
                // }

                create_temporary_data(currentIndex);
            },
            onFinishing: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                return form.valid();
            },
            onFinished: function (event, currentIndex)
            {
                form.validate().settings.ignore = ":disabled";
                if(form.valid()){
                    swal({
                        title: 'Creando nuevo empleado...',
                        text: 'Por favor, espere un momento.',
                        type: 'info',
                        showCancelButton: false,
                        showConfirmButton: false,
                        closeOnClickOutside: false,
                    });

                    $(form).submit();
                }
            }
        });

        $(document).on("select2-opening", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror")) {
                $(".select2-drop ul").addClass("mierror");
            } else {
                $(".select2-drop ul").removeClass("mierror");
            }
        });

        $(document).on("select2-close", function (arg) {
            var elem = $(arg.target);
            if ($("#s2id_" + elem.attr("id") + " a.select2-choice").hasClass("mierror") && $("#" + elem.attr("id")).val() != '') {
                $("#s2id_" + elem.attr("id") + " a.select2-choice").removeClass('mierror');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');
                $("#s2id_" + elem.attr("id") + " span.select2-arrow, u.select2-results").removeClass('mierroArrow');

                $("#" + elem.attr("id") + "-error").css('display', 'none');
            }
        });
    }

    function load_temporary_data()
    {
        $('#first_name').val(sessionStorage.getItem('first_name'));
        $('#second_name').val(sessionStorage.getItem('second_name'));
        $('#first_lastname').val(sessionStorage.getItem('first_lastname'));
        $('#second_lastname').val(sessionStorage.getItem('second_lastname'));
        $('#tipo_documento').select2('val', sessionStorage.getItem('tipo_documento'));
        $('#vat_no').val(sessionStorage.getItem('vat_no'));
        $('#educational_level').select2('val', sessionStorage.getItem('educational_level'));
        $('#profession').val(sessionStorage.getItem('profession'));
        $('#marital_status').select2('val', sessionStorage.getItem('marital_status'));
        $('#blood_type').select2('val', sessionStorage.getItem('blood_type'));
        $('#birthdate').val(sessionStorage.getItem('birthdate'));
        $('#gender').select2('val', sessionStorage.getItem('gender'));
        $('#military_card').val(sessionStorage.getItem('military_card'));
        $('#country').select2('val', sessionStorage.getItem('country'));
        $('#state').select2('val', sessionStorage.getItem('state'));
        $('#city').select2('val', sessionStorage.getItem('city'));
        $('#address').val(sessionStorage.getItem('address'));
        $('#postal_code').val(sessionStorage.getItem('postal_code'));
        $('#email').val(sessionStorage.getItem('email'));
        $('#phone').val(sessionStorage.getItem('phone'));
        $('#home_phone').val(sessionStorage.getItem('home_phone'));


        $('#contact_name').val(sessionStorage.getItem('contact_name'));
        $('#relationship').val(sessionStorage.getItem('relationship'));
        $('#contact_address').val(sessionStorage.getItem('contact_address'));
        $('#contact_phone').val(sessionStorage.getItem('contact_phone'));
        $('#contact_email').val(sessionStorage.getItem('contact_email'));
    }

    function create_temporary_data(tab_index)
    {
        if (tab_index == 0) {
            sessionStorage.setItem("first_name", $('#first_name').val());
            sessionStorage.setItem("second_name", $('#second_name').val());
            sessionStorage.setItem("first_lastname", $('#first_lastname').val());
            sessionStorage.setItem("second_lastname", $('#second_lastname').val());
            sessionStorage.setItem("tipo_documento", $('#tipo_documento').val());
            sessionStorage.setItem("vat_no", $('#vat_no').val());
            sessionStorage.setItem("educational_level", $('#educational_level').val());
            sessionStorage.setItem("profession", $('#profession').val());
            sessionStorage.setItem("marital_status", $('#marital_status').val());
            sessionStorage.setItem("blood_type", $('#blood_type').val());
            sessionStorage.setItem("birthdate", $('#birthdate').val());
            sessionStorage.setItem("gender", $('#gender').val());
            sessionStorage.setItem("military_card", $('#military_card').val());
            sessionStorage.setItem("country", $('#country').val());
            sessionStorage.setItem("state", $('#state').val());
            sessionStorage.setItem("city", $('#city').val());
            sessionStorage.setItem("address", $('#address').val());
            sessionStorage.setItem("postal_code", $('#postal_code').val());
            sessionStorage.setItem("email", $('#email').val());
            sessionStorage.setItem("phone", $('#phone').val());
            sessionStorage.setItem("home_phone", $('#home_phone').val());
        }

        if (tab_index == 1) {
            // _contacts = [];
            // _relationship = [];
            // _address = [];
            // _phone = [];
            // _email = [];

            sessionStorage.setItem("contact_name", $('#contact_name').val());
            sessionStorage.setItem("relationship", $('#relationship').val());
            sessionStorage.setItem("contact_address", $('#contact_address').val());
            sessionStorage.setItem("contact_phone", $('#contact_phone').val());
            sessionStorage.setItem("contact_email", $('#contact_email').val());

            // $('#contact_data_container').find('.row').each(function(i) {
            //     index = (i == 0) ? '' : i;
            //     if ($('#contact_name'+index).val() != '') { _contacts.push($('#contact_name'+index).val()); }
            //     if ($('#relationship'+index).val() != '') { _relationship.push($('#relationship'+index).val()); }
            //     if ($('#contact_address'+index).val() != '') { _address.push($('#contact_address'+index).val()); }
            //     if ($('#contact_phone'+index).val() != '') { _phone.push($('#contact_phone'+index).val()); }
            //     if ($('#contact_email'+index).val() != '') { _email.push($('#contact_email'+index).val()); }
            // });

            // if (_contacts.length > 0) {
            //     sessionStorage.setItem("contact_name", JSON.stringify(_contacts));
            //     sessionStorage.setItem("contact_relationship", JSON.stringify(_relationship));
            //     sessionStorage.setItem("contact_address", JSON.stringify(_address));
            //     sessionStorage.setItem("contact_phone", JSON.stringify(_phone));
            //     sessionStorage.setItem("contact_email", JSON.stringify(_email));
            // }
        }
    }

    function validate_existing_document_number()
    {
        var document_number = $("#vat_no").val();

        if (document_number != '') {
            $.ajax({
                url: '<?= admin_url("employees/validate_existing_document_number") ?>',
                type: 'GET',
                dataType: 'json',
                data: {
                    'document_number': document_number
                },
            })
            .done(function(data) {
                if (data == true) {
                    command:toastr.error('<?= lang("employees_existing_document_number"); ?>', '<?= lang("toast_error_title") ?>', {onHidden:function() { $("#vat_no").val(''); $("#vat_no").focus(); }});
                } else {
                    get_check_digit();
                }
            })
            .fail(function(data) {
                command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
            });
        }
    }

    function get_check_digit()
    {
        var check_digit = '';
        var document_number = $("#vat_no").val();
        var document_code = $("#tipo_documento option:selected").data('code');

        if (document_code == '<?= NIT; ?>') {
            if (site.settings.get_companies_check_digit == 1) {
                check_digit = calculate_check_digit(document_number);
                $('input[name="digito_verificacion"]').val(check_digit);
            }
        }

        $('input[name="digito_verificacion"]').val(check_digit);
        $('input[name="document_code"]').val(document_code);
    }

    function validate_existing_email() {
        var email = $("#email").val();
        if (email != '') {
            $.ajax({
                url: '<?= admin_url("employees/validate_existing_email") ?>',
                type: 'GET',
                dataType: 'json',
                data: {
                    'email': email
                },
            })
            .done(function(data) {
                if (data == true) {
                    command:toastr.error('<?= lang("employees_existing_email"); ?>', '<?= lang("toast_error_title") ?>', {onHidden:function() { $("#email").val(''); $("#email").focus(); }});
                }
            })
            .fail(function(data) {
                command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
            });
        }
    }

    function load_states()
    {
        var country_code = $('#country option:selected').data('code');
        $('input[name="country_code"]').val(country_code);

        $.ajax({
            url:"<?= admin_url() ?>employees/get_states/"+country_code,
        }).done(function(data){
            $('#state').html(data);

            if (sessionStorage.getItem('state') != '') {
                $('#state').select2('val', sessionStorage.getItem('state'));
                $('#state').trigger('change');
            }
        }).fail(function(data) {
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function load_cities()
    {
        state_code = $('#state option:selected').data('code');
        $('input[name="state_code"]').val(state_code);

        $.ajax({
            url:"<?= admin_url() ?>employees/get_cities/"+state_code,
        }).done(function(data){
            $('#city').html(data);

            if (sessionStorage.getItem('state') != '') {
                $('#city').select2('val', sessionStorage.getItem('city'));
            }
        }).fail(function(data){
            command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function() { console.log(data.responseText); }});
        });
    }

    function load_zones()
    {
        city_code = $('#city option:selected').data('code');
        $('input[name="city_code"]').val(city_code);

        code_country = $('#country option:selected').data('code');
        postal_code = '';
        if (city_code) {
          postal_code = city_code.toString().replace(code_country, '');
          $('#postal_code').val(postal_code);
        }

        $.ajax({
            url:"<?= admin_url().'employees/get_zones/' ?>"+city_code
        }).done(function(data){
            $('#zone').html(data);
        });
    }

    function enable_disable_military_card_input()
    {
        var gender = $('#gender').val();
        if(gender != ''){
            if(gender == '<?= MALE ?>' || gender == '<?= UNDEFINED_SEX ?>'){
                $('#military_card').prop("disabled", false);
            }else{
                $('#military_card').prop("disabled", true);
            }
        }else{
            $('#military_card').prop("disabled", false);
        }
    }

    function add_contact_fields()
    {
        if(_number_contact_fields < 3){
            $.ajax({
                dataType: 'HTML',
                url:"<?= admin_url() ?>employees/get_contact_fields/"+_consecutive_contact_fields,
            }).done(function(data){
                $('#contact_data_container').append(data);
                $('[data-toggle="tooltip"]').tooltip();
                $('select:not(:hidden)').select2();
                $(input).iCheck({ radioClass :'iradio_square-blue' });

                _number_contact_fields++;
                _consecutive_contact_fields++;
            }).fail(function(data){
                command:toastr.error('<?= lang("ajax_error"); ?>', '<?= lang("toast_error_title"); ?>', {onHidden: function(){ console.log(data.reponseText); }});
            });
        }else{
            command: toastr.error('<?= lang("employees_maximum_contacts_allowed"); ?>', '<?= lang("toast_alert_title") ?>');
        }
    }

    function remove_contact_fields(element)
    {
        element.parent().parent('.row').remove()
        _number_contact_fields--;

        $('input[name="main_contact"]').each(function(index, el) {
            $('#'+$(this).prop('id')).iCheck('check');
        });
    }

    function  calculate_check_digit(myNit)
    {
        var vpri, x, y, z;

        // Se limpia el Nit
        myNit = myNit.replace ( /\s/g, "" ) ; // Espacios
        myNit = myNit.replace ( /,/g,  "" ) ; // Comas
        myNit = myNit.replace ( /\./g, "" ) ; // Puntos
        myNit = myNit.replace ( /-/g,  "" ) ; // Guiones

        // Se valida el nit
        if(isNaN(myNit)){
            console.log ("El nit/cédula '" + myNit + "' no es válido(a).") ;
            return "" ;
        };

        vpri = new Array(16) ;
        z = myNit.length ;

        vpri[1]  =  3 ;
        vpri[2]  =  7 ;
        vpri[3]  = 13 ;
        vpri[4]  = 17 ;
        vpri[5]  = 19 ;
        vpri[6]  = 23 ;
        vpri[7]  = 29 ;
        vpri[8]  = 37 ;
        vpri[9]  = 41 ;
        vpri[10] = 43 ;
        vpri[11] = 47 ;
        vpri[12] = 53 ;
        vpri[13] = 59 ;
        vpri[14] = 67 ;
        vpri[15] = 71 ;

        x = 0 ;
        y = 0 ;

        for(var i=0; i<z; i++){
            y = (myNit.substr(i, 1));
            x += (y * vpri[z-i]) ;
        }

        y = x % 11;

        return (y > 1) ? 11 - y : y;
    }
</script>