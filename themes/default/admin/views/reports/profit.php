<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-md modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>

            <span class="modal-title" style="font-size: 150%;"><?=lang('day_profit');?></span>
            <h3><?= lang('date')." : ".$date ?></h3>
        </div>
        <div class="modal-body">
            <!-- <p><?= lang('unit_and_net_tip'); ?></p> -->
            <div class="form-group">
                <?php
                $opts[] = lang('all_billers');
                foreach ($billers as $biller) {
                    $opts[$biller->id] = $biller->name;
                }
                ?>
                <?= form_dropdown('biller', $opts, set_value('biller', $swh), 'class="form-control select" id="biller"'); ?>
            </div>
            <div class="table-responsive">
            <table width="100%" class="stable">
                <tr>
                    <td style="border-bottom: 1px solid #EEE;"><h4><?= lang('subtotal_before_tax'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"><h4>
                            <span><?= $this->sma->formatMoney($costing->net_sales); ?></span></h4>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #EEE;"><h4><?= lang('tax'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"><h4>
                            <span><?= $this->sma->formatMoney($costing->item_tax); ?></span></h4>
                    </td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('order_discount'); ?></h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"><h4>
                            <span><?php $discount = $discount ? $discount->order_discount : 0; echo $this->sma->formatMoney($discount); ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td style="border-bottom: 1px solid #EEE;"><h4><?= lang('products_cost_before_tax'); ?></h4></td>
                    <td style="text-align:right; border-bottom: 1px solid #EEE;"><h4>
                            <span><?= $this->sma->formatMoney($costing->cost); ?></span>
                        </h4></td>
                </tr>
                <tr>
                    <td width="300px;" style="font-weight:bold;"><h4><strong><?= lang('profit'); ?></strong></h4>
                    </td>
                    <td style="text-align:right;"><h4>
                            <span><strong><?= $this->sma->formatMoney($costing->net_sales - $costing->cost); ?></strong></span>
                        </h4></td>
                </tr>
                <?php 
                $margin = (($costing->net_sales - $costing->cost) / ($costing->net_sales > 0 ? $costing->net_sales : 1)) * 100;
                 ?>
                <tr>
                    <td width="300px;" style="font-weight:bold;"><h4><strong><?= lang('profit_margin'); ?></strong></h4>
                    </td>
                    <td style="text-align:right;"><h4>
                            <span><strong><?= $this->sma->formatDecimal($margin, 2); ?> %</strong></span>
                        </h4></td>
                </tr>
                <?php if (isset($returns->total)) { ?>
                <tr>
                    <td width="300px;" style="font-weight:bold;"><h4><strong><?= lang('return_sales'); ?></strong></h4>
                    </td>
                    <td style="text-align:right;"><h4>
                            <span><strong><?= $this->sma->formatMoney($costing->net_return_sales); ?></strong></span>
                        </h4></td>
                </tr>
                <?php } ?>
                <tr>
                    <td style="border-bottom: 1px solid #DDD;"><h4><?= lang('expenses'); ?></h4></td>
                    <td style="text-align:right;border-bottom: 1px solid #DDD;"><h4>
                            <span><?php $expense = $expenses ? $expenses->total : 0; echo $this->sma->formatMoney($expense); ?></span>
                        </h4></td>
                </tr>
            </table>
            </div>
        </div>
    </div>

</div>

<script>
    $(document).ready(function() {
        $('#biller').select2({minimumResultsForSearch: 7});
        $('#biller').change(function(e) {
            var wh = $(this).val();
            $.get('<?= admin_url('reports/profit/'.$date); ?>/'+wh+'/1', function(data) {
                $('#myModal').empty().html(data);
                $('#biller').select2({minimumResultsForSearch: 7});
            });
        });
    });
</script>
