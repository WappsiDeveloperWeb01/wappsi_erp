<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<style type="text/css" media="screen">
    #PRData td:nth-child(7) { text-align: right; }

    <?php if($Owner || $Admin || $this->session->userdata('show_cost')) { ?>
        #PRData td:nth-child(9) { text-align: right; }
    <?php } if($Owner || $Admin || $this->session->userdata('show_price')) { ?>
        #PRData td:nth-child(8) { text-align: right; }
    <?php } ?>
</style>

<script>
    var oTable;

    $(document).ready(function ()
    {

        $("#form_recosting").validate({
            ignore: []
        });


        $(document).on('click', '#clean_filters_button', function() { clean_filters(); })
        if ($(window).width() < 1000) {
            var nums = [[10, 25], [10, 25]];
        } else {
            var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
        }

        oTable = $('#PRData').dataTable({
            "aaSorting": [[2, "asc"], [3, "asc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('products/getRecostingProductRegister/'.$product_id); ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({"name": "<?= $this->security->get_csrf_token_name() ?>", "value": "<?= $this->security->get_csrf_hash() ?>"});
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "product_link";

                return nRow;
            },
            "aoColumns": [
                null,
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": decimalFormat},
            ]
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 2, filter_default_label: "[<?=lang('code');?>]", filter_type: "text", data: []},
        ], "footer");
    });

</script>

<div class="row wrapper border-bottom white-bg page-heading no-print">
    <div class="col-lg-8">
        <h2>
            <?= lang('recosting_registers_view'); ?>
        </h2>
    </div>
</div>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <h3><?= lang('product')." : ".$product->code." - ".$product->name ?></h3>
                            <hr>
                            <div class="table-responsive">
                                <table id="PRData" class="table table-bordered table-condensed table-hover">
                                    <thead>
                                        <tr class="primary">
                                            <th><?= lang("start_date") ?></th>
                                            <th><?= lang("end_date") ?></th>
                                            <th><?= lang("warehouse") ?></th>
                                            <th><?= lang("calculated_avg_cost") ?></th>
                                            <th><?= lang("calculated_quantity") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="5" class="dataTables_empty"><?= lang('loading_data_from_server'); ?></td>
                                        </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>