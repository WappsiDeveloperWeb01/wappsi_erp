<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Header -->

<style type="text/css">
    .widget {
        min-height: 100px;
    }
</style>


<div class="row wrapper border-bottom white-bg page-heading">
    <div class="col-lg-8">
        <h2><?= $name; ?></h2>
        <!-- <ol class="breadcrumb">
            <?php
            foreach ($bc as $b) {
                if ($b['link'] === '#') {
                    echo '<li> <strong>' . $b['page'] . '</strong> </li>';
                } else {
                    echo '<li><a href="' . $b['link'] . '">' . $b['page'] . '</a></li>';
                }
            }
            ?>
        </ol> -->
    </div>
</div>
<!-- /Header -->
<!-- Body -->
<div class="wrapper wrapper-content  animated fadeInRight col-sm-12">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                    <div class="row">
                        <?php if ($this->Admin || $this->Owner || $this->GP['production_order-packing_orders']): ?>
                            <div class="col-md-2" style="padding-bottom: 2%">
                                <button onclick="window.open('<?= admin_url('production_order/packing_orders') ?>', '_self');" class="btn btn-success btn-outline"  style="width: 100%; height: 80px;">
                                    <h3 class="font-bold">Recepción mercancia</h3>
                                </button>
                            </div>
                        <?php endif ?>
                        <div class="col-md-2">
                            <button onclick="" class="btn btn-success btn-outline"  style="width: 100%; height: 80px;">
                                <h3 class="font-bold">Proceso de Picking</h3>
                            </button>
                        </div>
                        <div class="col-md-2">
                            <button onclick="" class="btn btn-success btn-outline"  style="width: 100%; height: 80px;">
                                <h3 class="font-bold">Proceso de Packing</h3>
                            </button>
                        </div>
                        <div class="col-md-2">
                            <button onclick="" class="btn btn-success btn-outline"  style="width: 100%; height: 80px;">
                                <h3 class="font-bold">Reubicar mercancía</h3>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->
<script language="javascript">
    $(document).ready(function () {


    });
</script>