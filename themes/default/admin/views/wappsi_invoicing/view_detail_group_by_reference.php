<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
$tipo_mov[1] = 'Activacion Modulos ';
$tipo_mov[2] = 'Plan Usuarios ';
$tipo_mov[3] = 'Plan Sucursales ';
$tipo_mov[4] = 'Documentos electronicos ';
$tipo_mov[5] = 'Plan Nómina ';
$tipo_mov[6] = 'Servicios recurrentes ';
$tipo_mov[7] = 'Productos recurrentes ';
$tipo_mov[8] = 'Productos y servicios NO recurrentes';
$tipo_mov[9] = 'Activación Sucursal Adicional';

 ?>
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <h4 class="modal-title" id="myModalLabel"><?php echo lang('wappsi_invoicing_detail_reference'); ?></h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3">
                    <label><?= lang('date') ?> : </label><br>
                    <span><?= $rows[0]->fecha ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('reference_no') ?> : </label><br>
                    <span><?= $rows[0]->reference_no ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('due_date') ?> : </label><br>
                    <span><?= $rows[0]->vencimiento ?></span>
                </div>
                <div class="col-md-3">
                    <?php 
                    $pm_st[0] = 'Pendiente';
                    $pm_st[1] = 'Pagado';
                    $pm_st[3] = 'Mora';
                     ?>
                    <label><?= lang('payment_status') ?> : </label><br>
                    <span><?= $pm_st[$rows[0]->estado_pago] ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label><?= lang('payment_reference_no') ?> : </label><br>
                    <span><?= $rows[0]->pago_reference_no ?></span>
                </div>
                <div class="col-md-3">
                    <label><?= lang('payment_date') ?> : </label><br>
                    <span><?= $rows[0]->fecha_pago ?></span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <label>Detalle:</label>
                    <table class="table">
                        <thead>
                            <tr>
                                <th><?= lang("movement_type"); ?></th>
                                <th><?= lang("description"); ?></th>
                                <th><?= lang("frecuency"); ?></th>
                                <th><?= lang("unit_quantity"); ?></th>
                                <th><?= lang("quantity"); ?></th>
                                <th><?= lang("net_price"); ?></th>
                                <th><?= lang("tax"); ?></th>
                                <th><?= lang("unit_price"); ?></th>
                                <th><?= lang("grand_total"); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $grand_total = 0; ?>
                            <?php foreach ($rows as $row): ?>
                                <tr>
                                    <td><?= $tipo_mov[$row->tipo_mov] ?></td>
                                    <td><?= $row->descripcion ?></td>
                                    <td class="text-right"><?= $this->sma->formatQuantity($row->frecuencia); ?></td>
                                    <td class="text-right"><?= $this->sma->formatQuantity($row->cantidad_unidad); ?></td>
                                    <td class="text-right"><?= $this->sma->formatQuantity($row->cantidad_facturada); ?></td>
                                    <td class="text-right"><?= $this->sma->formatMoney($row->valor_neto); ?></td>
                                    <td class="text-right"><?= $this->sma->formatMoney($row->valor_iva); ?></td>
                                    <td class="text-right"><?= $this->sma->formatMoney($row->valor_total); ?></td>
                                    <td class="text-right"><?= $this->sma->formatMoney($row->valor_total * $row->cantidad_facturada); ?></td>
                                </tr>
                                <?php $grand_total += $row->valor_total * $row->cantidad_facturada; ?>
                            <?php endforeach ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="8" class="text-right">
                                    Total
                                </th>
                                <th class="text-right">
                                    <?= $this->sma->formatMoney($grand_total); ?>
                                </th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<?= $modal_js ?>