<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php

$v = "";
if ($this->input->post('product')) {
    $v .= "&product=" . $this->input->post('product');
}
if ($this->input->post('reference_no')) {
    $v .= "&reference_no=" . $this->input->post('reference_no');
}
if ($this->input->post('supplier')) {
    $v .= "&supplier=" . $this->input->post('supplier');
}
if ($this->input->post('warehouse')) {
    $v .= "&warehouse=" . $this->input->post('warehouse');
}
if ($this->input->post('user')) {
    $v .= "&user=" . $this->input->post('user');
}
if ($this->input->post('start_date')) {
    $v .= "&start_date=" . $this->input->post('start_date');
}
if ($this->input->post('end_date')) {
    $v .= "&end_date=" . $this->input->post('end_date');
}
if ($this->input->post('detailed_purchases')) {
    $v .= "&detailed_purchases=" . $this->input->post('detailed_purchases');
}
if (!is_null($this->input->post('purchase_evaluated'))) {
    $v .= "&purchase_evaluated=" . $this->input->post('purchase_evaluated');
}

?>
<script type="text/javascript">
    $(document).ready(function () {
        <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        oTable = $('#PoRData').dataTable({
            "aaSorting": [[0, "desc"]],
            "aLengthMenu": [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?= lang('all') ?>"]],
            "iDisplayLength": <?= $Settings->rows_per_page ?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('reports/getPurchasesReport/?v=1' . $v) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {

                <?php if ($this->input->post('detailed_purchases')): ?>
                    nRow.id = aData[13];
                <?php else: ?>
                    nRow.id = aData[11];
                <?php endif ?>
                nRow.className = "purchase_link2";
                return nRow;
            },
            "aoColumns": [
                        {"mRender": fld},
                        null,
                        null,
                        null,
                        <?php if ($this->input->post('detailed_purchases')): ?>
                            {"bSearchable": false, "mRender": pqFormat},
                            {"mRender": pqFormat},
                        <?php endif ?>
                        {"mRender": currencyFormat},
                        {"mRender": currencyFormat},
                        {"mRender": currencyFormat},
                        {"mRender": currencyFormat},
                        <?php if ($this->input->post('detailed_purchases')): ?>
                            {"bVisible": false},
                            {"bVisible": false},
                            {"bVisible": false},
                        <?php else: ?>
                            {"mRender": currencyFormat},
                            {"mRender": currencyFormat},
                            {"mRender": row_status}
                        <?php endif ?>
                        <?php if ($this->Settings->manage_purchases_evaluation == YES) : ?> , {
                                "className": "text-center",
                                render: function(data) {
                                    if (data == 1) {
                                        return '<i class="fa fa-check fa-lg text-success"></i>';
                                    } else {
                                        return '<i class="fa fa-ban fa-lg text-danger"></i>';
                                    }
                                }
                            },
                        <?php endif ?>
                    ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var tt1 = 0, tt2 = 0, tt3 = 0, tt4 = 0, tt5 = 0;
                for (var i = 0; i < aaData.length; i++) {
                    tt1 += parseFloat(aaData[aiDisplay[i]][4]);
                    tt2 += parseFloat(aaData[aiDisplay[i]][5]);
                    tt3 += parseFloat(aaData[aiDisplay[i]][6]);
                    tt4 += parseFloat(aaData[aiDisplay[i]][7]);
                    tt5 += parseFloat(aaData[aiDisplay[i]][8]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[4].innerHTML = currencyFormat(parseFloat(tt1));
                nCells[5].innerHTML = currencyFormat(parseFloat(tt2));
                nCells[6].innerHTML = currencyFormat(parseFloat(tt3));
                nCells[7].innerHTML = currencyFormat(parseFloat(tt4));
                nCells[8].innerHTML = currencyFormat(parseFloat(tt5));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 0, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 1, filter_default_label: "[<?=lang('ref_no');?>]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('warehouse');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('supplier');?>]", filter_type: "text", data: []},
            {column_number: 8, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
        ], "footer");
        <?php endif ?>
    });

    $(document).ready(function () {
        $('#form').hide();
        <?php if ($this->input->post('supplier')) { ?>
        $('#supplier').val(<?= $this->input->post('supplier') ?>).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url + "suppliers/suggestions/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data.results[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $('#supplier').val(<?= $this->input->post('supplier') ?>);
        <?php } ?>
        $('.toggle_down').click(function () {
            $("#form").slideDown();
            return false;
        });
        $('.toggle_up').click(function () {
            $("#form").slideUp();
            return false;
        });
    });
</script>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <h5><?= lang('filter') ?></h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-down"></i>
                        </a>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <?php echo admin_form_open("reports/purchases", 'id="form_filter"'); ?>
                            <input type="hidden" name="filter_action" id="filter_action" value="0">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("product", "suggest_product"); ?>
                                            <?php echo form_input('sproduct', (isset($_POST['sproduct']) ? $_POST['sproduct'] : ""), 'class="form-control" id="suggest_product"'); ?>
                                            <input type="hidden" name="product" value="<?= isset($_POST['product']) ? $_POST['product'] : "" ?>" id="report_product_id"/>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="reference_no"><?= lang("reference_no"); ?></label>
                                            <?php echo form_input('reference_no', (isset($_POST['reference_no']) ? $_POST['reference_no'] : ""), 'class="form-control tip" id="reference_no"'); ?>

                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="user"><?= lang("created_by"); ?></label>
                                            <?php
                                            $us[""] = lang('select').' '.lang('user');
                                            foreach ($users as $user) {
                                                $us[$user->id] = $user->first_name . " " . $user->last_name;
                                            }
                                            echo form_dropdown('user', $us, (isset($_POST['user']) ? $_POST['user'] : ""), 'class="form-control" id="user" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("user") . '"');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang("supplier", "supplier"); ?>
                                            <?php echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ""), 'class="form-control" id="supplier"'); ?> </div>
                                    </div>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <label class="control-label" for="warehouse"><?= lang("warehouse"); ?></label>
                                            <?php
                                            $wh[""] = lang('select').' '.lang('warehouse');
                                            foreach ($warehouses as $warehouse) {
                                                $wh[$warehouse->id] = $warehouse->name;
                                            }
                                            echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ""), 'class="form-control" id="warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"');
                                            ?>
                                        </div>
                                    </div>
                                    <div class="col-sm-4" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="date_controls_dh">
                                        <?php if ($this->Settings->big_data_limit_reports == 1): ?>
                                            <div class="col-sm-4 form-group">
                                                <?= lang('filter_year', 'filter_year_dh') ?>
                                                <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value): ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        <?php endif ?>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("start_date", "start_date_dh"); ?>
                                                <?php echo form_input('start_date', (isset($_POST['start_date']) ? $_POST['start_date'] : ""), 'class="form-control datetime" id="start_date_dh"'); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <?= lang("end_date", "end_date_dh"); ?>
                                                <?php echo form_input('end_date', (isset($_POST['end_date']) ? $_POST['end_date'] : ""), 'class="form-control datetime" id="end_date_dh"'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">

                                    <?php if ($this->Settings->manage_purchases_evaluation == 1): ?>
                                        <div class="col-sm-4">
                                            <?= lang('purchase_evaluated', 'purchase_evaluated') ?>
                                            <select name="purchase_evaluated" id="purchase_evaluated" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <option value="1" <?= isset($_POST['purchase_evaluated']) && $_POST['purchase_evaluated'] == '1' ? 'selected="selected"' : '' ?>><?= lang('yes') ?></option>
                                                <option value="0" <?= isset($_POST['purchase_evaluated']) && $_POST['purchase_evaluated'] == '0' ? 'selected="selected"' : '' ?>><?= lang('no') ?></option>
                                            </select>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-4">
                                        <div class="form-group">
                                            <?= lang('detailed_purchases', 'detailed_purchases'); ?>
                                            <br>
                                            <label>
                                                <input type="checkbox" name="detailed_purchases" id="detailed_purchases" class="form-control" <?= isset($_POST['detailed_purchases']) ? 'checked="checked"' : '' ?>>
                                                <?= lang('detail_by_products') ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div
                                        class="controls"> <?php echo form_submit('submit_report', $this->lang->line("submit"), 'class="btn btn-primary" id="submit_filter"'); ?> </div>
                                </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php if (isset($_POST['filter_action']) && $_POST['filter_action'] == 1): ?>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-content">
                        <!-- <p class="introtext"><?php //echo lang('enter_info'); ?></p> -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                        <li class="dropdown"><a href="#" id="xls" class="tip" title="<?= lang('download_xls') ?>"><i class="icon fa fa-file-excel-o"></i> <?= lang('download_xls') ?></a></li>
                                        <li class="dropdown"><a href="#" id="image" class="tip" title="<?= lang('save_image') ?>"><i class="icon fa fa-file-picture-o"></i> <?= lang('save_image') ?></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <!-- <p class="introtext"><?//= lang('customize_report'); ?></p> -->
                                <div class="table-responsive">
                                    <table id="PoRData"
                                        class="table table-bordered table-hover table-condensed reports-table">
                                        <thead>
                                        <tr>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("warehouse"); ?></th>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <?php if ($this->input->post('detailed_purchases')): ?>
                                                <th><?= lang("product"); ?></th>
                                                <th><?= lang("product_qty"); ?></th>
                                            <?php endif ?>
                                            <th><?= lang("subtotal"); ?></th>
                                            <th><?= lang("taxes"); ?></th>
                                            <th><?= lang("order_discount"); ?></th>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <?php if ($this->Settings->manage_purchases_evaluation == 1): ?>
                                                <th><?= lang("purchase_evaluation"); ?></th>
                                            <?php endif ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                            <td colspan="9" class="dataTables_empty"><?= lang('loading_data_from_server') ?></td>
                                        </tr>
                                        </tbody>
                                        <tfoot class="dtFilter">
                                        <tr class="active">
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("warehouse"); ?></th>
                                            <th><?= lang("supplier_name"); ?></th>
                                            <?php if ($this->input->post('detailed_purchases')): ?>
                                                <th><?= lang("product"); ?></th>
                                                <th><?= lang("product_qty"); ?></th>
                                            <?php endif ?>
                                            <th><?= lang("subtotal"); ?></th>
                                            <th><?= lang("taxes"); ?></th>
                                            <th><?= lang("order_discount"); ?></th>
                                            <th><?= lang("grand_total"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("balance"); ?></th>
                                            <th><?= lang("status"); ?></th>
                                            <?php if ($this->Settings->manage_purchases_evaluation == 1): ?>
                                                <th><?= lang("purchase_evaluation"); ?></th>
                                            <?php endif ?>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endif ?>

</div>

<script type="text/javascript" src="<?= $assets ?>js/html2canvas.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#pdf').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getPurchasesReport/pdf/?v=1'.$v)?>";
            return false;
        });
        $('#xls').click(function (event) {
            event.preventDefault();
            window.location.href = "<?=admin_url('reports/getPurchasesReport/0/xls/?v=1'.$v)?>";
            return false;
        });
        $('#image').click(function (event) {
            event.preventDefault();
            html2canvas($('.table-responsive'), {
                onrendered: function (canvas) {
                    openImg(canvas.toDataURL());
                }
            });
            return false;
        });
        $('#submit_filter').on('click', function(e){
            // e.preventDefault();
            $('#filter_action').val(1);
            $('#sales_filter').submit();
        });
    });

    $(document).ready(function(){
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])): ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#sales_filter').submit();
            <?php elseif($_POST['date_records_filter'] != $this->Settings->default_records_filter): ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
        <?php if ($this->session->userdata('warehouse_id')): ?>
            setTimeout(function() {
                $('#warehouse').select2('val', '<?= $this->session->userdata('warehouse_id') ?>').select2('readonly', true).trigger('change');
            }, 850);
        <?php endif ?>
    });
</script>