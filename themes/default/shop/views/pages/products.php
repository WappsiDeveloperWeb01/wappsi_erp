<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style type="text/css">
    .checkbox-div{
        padding-left: 12%;
    }
</style>
<section class="page-contents">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row">
                    <div class="col-sm-3 col-md-2 no-mobile">
                        <div id="sticky-con">
                            <div class="margin-top-md">
                                <h4 class="title text-bold"><span><?= lang('filters'); ?></span></h4>
                                <ul class="list-group">
                                    <?php
                                    if (isset($filters['category']) && !empty($filters['category'])) {
                                        ?>
                                        <li class="list-group-item">
                                            <span class="close reset_filters_category">&times;</span>
                                            <?= $filters['category']->name; ?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                    <?php
                                    if (isset($filters['brand']) && !empty($filters['brand'])) {
                                        ?>
                                        <li class="list-group-item">
                                            <span class="close reset_filters_brand">&times;</span>
                                            <?= $filters['brand']->name; ?>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                            </div>
                            <div class="margin-bottom-xl">
                                <h5 class="title text-bold"><span><?= lang('availability'); ?></span></h5>
                                <div class="checkbox-div">
                                    <label for="in-stock">
                                        <input type="checkbox" id="in-stock" class="skip">
                                        <span></span>
                                        <?= lang('in_stock'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="margin-bottom-xl">
                                <h5 class="title text-bold"><span><?= lang('featured'); ?></span></h5>
                                <div class="checkbox-div">
                                    <label for="featured">
                                        <input type="checkbox" id="featured" class="skip" <?= $this->input->get('featured') == 'yes' ? ' checked' : ''; ?>>
                                        <span> <?= lang('featured'); ?></span>
                                    </label>
                                </div>
                            </div>
                            <?php if ($isPromo) { ?>
                            <div class="margin-bottom-xl">
                                <h5 class="title text-bold"><span><?= lang('promotions'); ?></span></h5>
                                <div class="checkbox-div">
                                    <label for="promotions">
                                        <input type="checkbox" id="promotions" class="skip" <?= $this->input->get('promo') == 'yes' ? ' checked' : ''; ?>>
                                        <span></span>
                                        <?= lang('promotions'); ?>
                                    </label>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="margin-bottom-xl">
                                <h5 class="title text-bold"><span><?= lang('price_range'); ?></span></h5>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <input type="text" name="min-price" id="min-price" value="" placeholder="Min" class="form-control"></input>
                                    </div>
                                    <div class="col-xs-6">
                                        <input type="text" name="max-price" id="max-price" value="" placeholder="Max" class="form-control"></input>
                                    </div>
                                </div>
                            </div>

                            <div class="margin-bottom-xl">
                                <h5 class="title text-bold"><span><?= lang('brand'); ?></span></h5>
                                <div class="brands_div">
                                    <?php if ($brands): ?>
                                        <?php foreach ($brands as $brand): ?>
                                            <div class="checkbox-div">
                                                <label>
                                                    <input type="checkbox" name="brand_filter[]" class="brand_filter skip" id="<?= $brand->slug ?>" value="<?= $brand->id ?>">
                                                    <span></span>
                                                    <?= $brand->name ?>
                                                </label>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>

                            <div class="margin-bottom-xl">
                                <h5 class="title text-bold"><span><?= lang('product_variants'); ?></span></h5>
                                <div class="options_div">
                                    <?php if ($options): ?>
                                        <?php foreach ($options as $option): ?>
                                            <div class="checkbox-div">
                                                <label>
                                                    <input type="checkbox" name="option_filter[]" class="option_filter skip" id="<?= $option->id ?>" value="<?= $option->name ?>">
                                                    <span></span>
                                                    <?= $option->name ?>
                                                </label>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>

                        <?php
                        if ($side_featured) {
                            ?>
                            <h4 class="margin-top-md title text-bold">
                                <span><?= lang('featured'); ?></span>
                                <div class="pull-right">
                                    <div class="controls pull-right hidden-xs">
                                        <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-example"
                                        data-slide="prev"></a>
                                        <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-example"
                                        data-slide="next"></a>
                                    </div>
                                </div>
                            </h4>

                            <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                <!-- Wrapper for slides -->
                                <div class="carousel-inner">
                                    <?php
                                    $r = 0;
                                    foreach ($side_featured as $fp) {
                                        ?>
                                        <div class="item <?= empty($r) ? 'active' : ''; ?>">
                                            <div class="featured-products">
                                                <div class="product" style="z-index: 1;">
                                                    <div class="details" style="transition: all 100ms ease-out 0s;">
                                                        <?php
                                                        if ($fp->promotion) {
                                                            ?>
                                                            <span class="badge badge-right theme"><?= lang('promo'); ?></span>
                                                            <?php
                                                        }
                                                        ?>
                                                        <img src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                        <div class="image_overlay"></div>
                                                        <div class="btn btn-sm add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart"></i> <?= lang('add_to_cart'); ?></div>
                                                        <div class="stats-container">
                                                            <span class="product_price">
                                                                <?php
                                                                if ($fp->promotion) {
                                                                    echo '<del class="text-red">'.$this->sma->convertMoney(isset($fp->special_price) && !empty($fp->special_price) ? $fp->special_price : $fp->price).'</del><br>';
                                                                    echo $this->sma->convertMoney($fp->promo_price);
                                                                } else {
                                                                    echo $this->sma->convertMoney(isset($fp->special_price) && !empty($fp->special_price) ? $fp->special_price : $fp->price);
                                                                }
                                                                ?>
                                                            </span>
                                                            <span class="product_name">
                                                                <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                                                            </span>
                                                            <a href="<?= site_url('category/'.$fp->category_slug); ?>" class="link"><?= $fp->category_name; ?></a>
                                                            <?php
                                                            if ($fp->brand_name) {
                                                                ?>
                                                                <span class="link">-</span>
                                                                <a href="<?= site_url('brand/'.$fp->brand_slug); ?>" class="link"><?= $fp->brand_name; ?></a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $r++;
                                    }
                                    ?>
                                </div>
                            </div>
                            <?php
                        }
                        ?>

                        </div>
                    </div>

                    <div class="col-sm-9 col-md-10">
                        <div id="loading">
                            <div class="wave">
                                <div class="rect rect1"></div>
                                <div class="rect rect2"></div>
                                <div class="rect rect3"></div>
                                <div class="rect rect4"></div>
                                <div class="rect rect5"></div>
                            </div>
                        </div>
                        <div id="grid-selector">
                            <div id="grid-menu" class="hidden-xs hidden-sm">
                                <span><?= lang('view') ?>:</span>
                                <ul>
                                    <li class="two-col active" style="font-size: 125%;"><i class="fa fa-th-large"></i></li>
                                    <li class="three-col" style="font-size: 125%;"><i class="fa fa-th"></i></li>
                                </ul>
                            </div>
                            <div id="grid-sort">
                                <?= lang('sort') ?>:
                                <select id="select_sorting">
                                    <option id="name-asc" class="sorting active"><i class="fa fa-sort-alpha-asc"></i> <?= lang('name_asc') ?></option>
                                    <option id="name-desc" class="sorting"><i class="fa fa-sort-alpha-desc"></i> <?= lang('name_desc') ?></option>
                                    <option id="price-asc" class="sorting"><i class="fa fa-sort-numeric-asc"></i> <?= lang('price_asc') ?></option>
                                    <option id="price-desc" class="sorting"><i class="fa fa-sort-numeric-desc"></i>  <?= lang('price_desc') ?></option>
                                </select>
                            </div>
                            <span class="page-info"></span>
                        </div>

                        <div class="clearfix"></div>
                        <div class="row">
                            <div id="results" class="grid"></div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div id="pagination" class="pagination-right" style="width:100%;"></div>
                            </div>
                            <div class="col-md-12">
                                <span class="page-info line-height-xl hidden-xs hidden-sm"></span>
                            </div>

                            <div class="mobile">
                                <?php
                                if ($side_featured) {
                                    ?>
                                    <h4 class="margin-top-md title text-bold">
                                        <span><?= lang('featured'); ?></span>
                                        <div class="pull-right">
                                            <div class="controls pull-right hidden-xs">
                                                <a class="left fa fa-chevron-left btn btn-xs btn-default" href="#carousel-example"
                                                data-slide="prev"></a>
                                                <a class="right fa fa-chevron-right btn btn-xs btn-default" href="#carousel-example"
                                                data-slide="next"></a>
                                            </div>
                                        </div>
                                    </h4>

                                    <div id="carousel-example" class="carousel slide" data-ride="carousel">
                                        <!-- Wrapper for slides -->
                                        <div class="carousel-inner">
                                            <?php
                                            $r = 0;
                                            foreach ($side_featured as $fp) {
                                                ?>
                                                <div class="item <?= empty($r) ? 'active' : ''; ?>">
                                                    <div class="featured-products">
                                                        <div class="product" style="z-index: 1;">
                                                            <div class="details" style="transition: all 100ms ease-out 0s;">
                                                                <?php
                                                                if ($fp->promotion) {
                                                                    ?>
                                                                    <span class="badge badge-right theme"><?= lang('promo'); ?></span>
                                                                    <?php
                                                                }
                                                                ?>
                                                                <img src="<?= base_url('assets/uploads/'.$fp->image); ?>" alt="">
                                                                <div class="image_overlay"></div>
                                                                <div class="btn btn-sm add-to-cart" data-id="<?= $fp->id; ?>"><i class="fa fa-shopping-cart"></i> <?= lang('add_to_cart'); ?></div>
                                                                <div class="stats-container">
                                                                    <span class="product_price">
                                                                        <?php
                                                                        if ($fp->promotion) {
                                                                            echo '<del class="text-red">'.$this->sma->convertMoney(isset($fp->special_price) && !empty($fp->special_price) ? $fp->special_price : $fp->price).'</del><br>';
                                                                            echo $this->sma->convertMoney($fp->promo_price);
                                                                        } else {
                                                                            echo $this->sma->convertMoney(isset($fp->special_price) && !empty($fp->special_price) ? $fp->special_price : $fp->price);
                                                                        }
                                                                        ?>
                                                                    </span>
                                                                    <span class="product_name">
                                                                        <a href="<?= site_url('product/'.$fp->slug); ?>"><?= $fp->name; ?></a>
                                                                    </span>
                                                                    <a href="<?= site_url('category/'.$fp->category_slug); ?>" class="link"><?= $fp->category_name; ?></a>
                                                                    <?php
                                                                    if ($fp->brand_name) {
                                                                        ?>
                                                                        <span class="link">-</span>
                                                                        <a href="<?= site_url('brand/'.$fp->brand_slug); ?>" class="link"><?= $fp->brand_name; ?></a>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $r++;
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

</section>