<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php if ($modal) { ?>

<style type="text/css">
    @media print {

      #wrapper {
        padding: 0px !important;
        margin: 0px !important;
      }
      #receiptData {
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-body{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-content{
        padding: 0px !important;
        margin: 0px !important;
      }
      .modal-dialog{
        padding: 0px !important;
        margin: 0px !important;
      }

    }
</style>

<div class="modal-dialog no-modal-header" role="document"><div class="modal-content"><div class="modal-body">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
    <?php
} else {
    ?><!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?=$page_title . " " . lang("no") . " " . $deposit->id;?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
        </style>
    </head>

    <body>
        <?php
            } ?>
        <div id="wrapper">
            <div id="receiptData">
                <div>
                    <div class="text-center">
                        <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                        <h3 style="text-transform:uppercase;"><?=$biller->company != "-" ? $biller->company : $biller->name;?></h3>
                        <?php
                        echo "<p>" . $biller->address . " " . $biller->city . " " . $biller->postal_code . " " . $biller->state . " " . $biller->country .
                        "<br>" . lang("tel") . ": " . $biller->phone;

                        echo "<br>";

                        echo "NIT : ".$biller->vat_no.($biller->digito_verificacion != "" ? '-'.$biller->digito_verificacion : '');
                        echo '</p>';
                        ?>
                        <p class="text-center"><b><?= $tipo_regimen ?></b></p>
                        <h4 style="font-weight:bold;"><?=  $document_type ? $document_type->nombre : lang('sale_invoice');?></h4>
                    </div>
                    <?php
                    echo "<p>" .lang("date") . ": " . $this->sma->hrld($deposit->date) . "<br>";
                    echo lang("sale_reference") . ": " . $deposit->reference_no . "<br>";
                    echo "<p>";
                    echo lang("customer") . ": " . ($customer->company && $customer->company != '-' ? $customer->company : $customer->name) . "<br>";
                    if ($this->pos_settings->customer_details && $customer->vat_no != "222222222222") {
                        if ($customer->vat_no != "-") {
                            if ($customer->tipo_documento == 6) {
                                echo  lang("vat_no") . ": " . $customer->vat_no."-".$customer->digito_verificacion." <br>";
                            } else {
                                echo  lang("vat_no") . ": " . $customer->vat_no." <br>";
                            }
                        }
                        echo lang("tel") . ": " . $customer->phone . "<br>";
                        echo lang("address") . ": " . $customer->address . "<br>";
                        echo $customer->city ." ".$customer->state." ".$customer->country ."<br>";
                        echo $customer->email ."<br>";
                    }
                    echo "</p>";
                    ?>

                    <div style="clear:both;"></div>
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th style="text-align: center; width: 60%">Nombre</th>
                                <th style="text-align: center;">Precio</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center"><?= lang('deposit_amount') ?></td>
                                <td class="text-right"><?= $this->sma->formatMoney($deposit->amount) ?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <?php
        if( ! $modal) {
            ?>
            <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
            <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
            <?php
        }
        ?>
        <script type="text/javascript">
            $(document).ready(function () {
                window.print();
            });
        </script>

    <?php if($modal) { ?>
        </div>
    <?php } else { ?>
    </body>
</html>
    <?php } ?>
