<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="margin-top: 0;">
                <i class="fa fa-lg">&times;</i>
            </button>
            <h2 class="modal-title" id="myModalLabel">
                <?= lang("table_change") ?>
            </h2>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-10 col-sm-offset-1">
                    <?= admin_form_open_multipart("pos/restobar_update_table/", [
                        "data-toggle" => "validator",
                        "role"        => "form",
                        "id"          => "tableChangeForm"
                    ]) ?>
                    <input type="hidden" name="tableToChange" id="tableToChange" value="<?= $tableToChange->id ?>">
                    <input type="hidden" name="order" id="order" value="<?= $order->id ?>">
                    <div class="form-group">
                        <label for="table"><?= lang("mesa") ?></label>
                        <select class="form-control" name="table" id="table">
                            <?php if (!empty($tables)) { ?>
                                <?php foreach ($tables as $table) { ?>
                                    <option value="<?= $table->id ?>">
                                        <?= lang("mesa") ."  $table->numero" ?>
                                    </option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary new-button" id="tableChangeButton"
                type="button"
                title="Actualizar"
                data-toggle="tooltip">
                <i class="fa fa-check"></i>
            </button>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip()
        $(document).one('click', '#tableChangeButton', function (e) { confirmTableChange() })
    });

    function confirmTableChange() {
        swal({
            title: "¿Está seguro de Cambiar la mesa?",
            text: "Se liberará la mesa anterior",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Si, Cambiar!",
            closeOnConfirm: false
        }, function (is_confirm) {
            if (is_confirm) {
                $("#tableChangeForm").submit();
            }
        });
    }
</script>
