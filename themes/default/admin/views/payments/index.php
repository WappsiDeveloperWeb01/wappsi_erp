<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content animated fadeInRight no-print">
    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('payments/index', ['id' => 'ppayments_filter']) ?>
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-title">
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="row">
                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang('reference_no', 'p2payment_reference_no') ?>
                                        <select name="p2payment_reference_no" id="p2payment_reference_no" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <?php if ($documents_types) : ?>
                                                <?php foreach ($documents_types as $dt) : ?>
                                                    <option value="<?= $dt->id ?>" data-dtprefix="<?= $dt->sales_prefix ?>" <?= isset($_POST['p2payment_reference_no']) && $_POST['p2payment_reference_no'] == $dt->id ? 'selected="selected"' : '' ?>><?= $dt->nombre . " (" . $dt->sales_prefix . ")" ?></option>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="biller"><?= lang("biller"); ?></label>
                                        <?php
                                        $biller_selected = '';
                                        $biller_readonly = false;
                                        if ($this->session->userdata('biller_id')) {
                                            $biller_selected = $this->session->userdata('biller_id');
                                            $biller_readonly = true;
                                        }

                                        $bl[""] = lang('select');
                                        foreach ($billers as $biller) {
                                            $bl[$biller->id] = $biller->company != '-' ? $biller->company : $biller->name;
                                        }
                                        echo form_dropdown('biller', $bl, (isset($_POST['biller']) ? $_POST['biller'] : $biller_selected), 'class="form-control" id="biller" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("biller") . '"');
                                        ?>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <label class="control-label" for="customer"><?= lang("customer"); ?></label>
                                        <?= form_input('customer', (isset($_POST['customer']) ? $_POST['customer'] : ""), 'id="filter_customer" data-placeholder="' . lang("select") . ' ' . lang("customer") . '" class="form-control input-tip" style="width:100%;"');?>
                                    </div>
                                </div>

                                <?php if ($this->Owner || $this->Admin) : ?>
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= lang('user') ?></label>
                                            <select name="filter_user" id="filter_user" class="form-control">
                                                <option value=""><?= lang('select') ?></option>
                                                <?php if ($users) : ?>
                                                    <?php foreach ($users as $user) : ?>
                                                        <option value="<?= $user->id ?>" <?= (isset($_POST['filter_user']) && $_POST['filter_user'] == $user->id) ? 'selected' : '' ?>><?= $user->first_name . " " . $user->last_name ?></option>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                <?php endif ?>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("paying_by", "p2payment_method"); ?>
                                        <select name="p2payment_method" id="p2payment_method" class="form-control">
                                            <?= $this->sma->paid_opts(null, false, true, false, true); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-sm-2">
                                    <div class="form-group">
                                        <?= lang("filter_by", "filter_by"); ?>
                                        <select name="filter_by" id="filter_by" class="form-control">
                                            <option value=""><?= lang('select') ?></option>
                                            <option value="1" <?= isset($_POST['filter_by']) && $_POST['filter_by'] == 1 ? 'selected="selected"' : '' ?>><?= lang('date') ?></option>
                                            <option value="2" <?= isset($_POST['filter_by']) && $_POST['filter_by'] == 2 ? 'selected="selected"' : '' ?>><?= lang('payment_date') ?></option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-1 col-without-padding text-center">
                            <div class="new-button-container">
                                <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                <button class="btn btn-primary new-button" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                <button class="btn btn-danger new-button" id="submit-purchases-filter-clean" type="button" data-toggle="tooltip" data-placement="bottom" title="<?= lang('reset') ?>"><i class="fa fa-times"></i></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ibox-content" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                    <div class="form-group">
                                        <?= lang('date_records_filter', 'date_records_filter_dh') ?>
                                        <select name="date_records_filter" id="date_records_filter_dh" class="form-control">
                                            <?= $this->sma->get_filter_options(); ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="date_controls_dh">
                                    <?php if ($this->Settings->big_data_limit_reports == 1) : ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <?= lang('filter_year', 'filter_year_dh') ?>
                                                <select name="filter_year" id="filter_year_dh" class="form-control" required>
                                                    <?php foreach ($this->filter_year_options as $key => $value) : ?>
                                                        <option value="<?= $key ?>"><?= $key ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <div class="form-group">
                                            <?= lang('start_date', 'start_date') ?>
                                            <input type="text" name="start_date" id="start_date_dh" value="<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>" class="form-control datetime">
                                        </div>
                                    </div>

                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?>>
                                        <div class="form-group">
                                            <?= lang('end_date', 'end_date') ?>
                                            <input type="text" name="end_date" id="end_date_dh" value="<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>" class="form-control datetime">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?= form_close() ?>
        </div>
    </div>

    <?php if ($Owner || $Admin || $GP['bulk_actions']) {
        echo admin_form_open('payments/payment_actions', 'id="action-form"');
    }
    ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="SLData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkft" type="checkbox" name="check" />
                                            </th>
                                            <th><?= lang("reference_no"); ?></th>
                                            <th><?= lang("date"); ?></th>
                                            <th><?= lang("payment_date"); ?></th>
                                            <th><?= lang("affects_to"); ?></th>
                                            <th><?= lang("consecutive_payment"); ?></th>
                                            <th><?= lang("customer_name"); ?></th>
                                            <th><?= lang("paid_by"); ?></th>
                                            <th><?= lang("paid"); ?></th>
                                            <th><?= lang("applied"); ?></th>
                                            <th><?= lang("payment_status"); ?></th>
                                            <th></th>
                                            <th><?= lang("actions") ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action" />
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php }
?>

<script>
    var start_date = "<?= $this->filtros_fecha_inicial ?>";
    var end_date = "<?= $this->filtros_fecha_final ?>";
    var p2payment_reference_no;
    var p2payment_method;
    var biller;
    var filter_user;
    var customer;
    var filtered = 0;
    var filtered_ini_date;

    if (localStorage.getItem('pp2_filter_filtered_ini_date')) {
        filtered_ini_date = localStorage.getItem('pp2_filter_filtered_ini_date');
    }
    <?php if (isset($_POST['start_date'])) : ?>
        localStorage.setItem('pp2_filter_start_date', '<?= $_POST['start_date'] ?>');
        start_date = '<?= $_POST['start_date'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_start_date')) {
            start_date = localStorage.getItem('pp2_filter_start_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['end_date'])) : ?>
        localStorage.setItem('pp2_filter_end_date', '<?= $_POST['end_date'] ?>');
        end_date = '<?= $_POST['end_date'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_end_date')) {
            end_date = localStorage.getItem('pp2_filter_end_date');
        }
    <?php endif ?>

    <?php if (isset($_POST['p2payment_reference_no'])) : ?>
        localStorage.setItem('pp2_filter_p2payment_reference_no', '<?= $_POST['p2payment_reference_no'] ?>');
        p2payment_reference_no = '<?= $_POST['p2payment_reference_no'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_p2payment_reference_no')) {
            ppayment_reference_no = localStorage.getItem('pp2_filter_ppayment_reference_no');
        }
    <?php endif ?>

    <?php if (isset($_POST['p2payment_method'])) : ?>
        localStorage.setItem('pp2_filter_p2payment_method', '<?= $_POST['p2payment_method'] ?>');
        p2payment_method = '<?= $_POST['p2payment_method'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_p2payment_method')) {
            p2payment_method = localStorage.getItem('pp2_filter_p2payment_method');
        }
    <?php endif ?>

    <?php if (isset($_POST['biller'])) : ?>
        localStorage.setItem('pp2_filter_biller', '<?= $_POST['biller'] ?>');
        biller = '<?= $_POST['biller'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_biller')) {
            biller = localStorage.getItem('pp2_filter_biller');
        }
    <?php endif ?>

    <?php if (isset($_POST['customer'])) : ?>
        localStorage.setItem('pp2_filter_customer', '<?= $_POST['customer'] ?>');
        customer = '<?= $_POST['customer'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_customer')) {
            customer = localStorage.getItem('pp2_filter_customer');
        }
    <?php endif ?>

    <?php if (isset($_POST['filtered'])) : ?>
        localStorage.setItem('pp2_filter_filtered', '<?= $_POST['filtered'] ?>');
        filtered = '<?= $_POST['filtered'] ?>';
        if (filtered_ini_date === undefined) {
            localStorage.setItem('pp2_filter_filtered_ini_date', '<?= date("Y-m-d H:i:s") ?>');
            filtered_ini_date = '<?= date("Y-m-d H:i:s") ?>';
        }
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_filtered')) {
            filtered = localStorage.getItem('pp2_filter_filtered');
        }
    <?php endif ?>

    <?php if (isset($_POST['filter_user'])) : ?>
        localStorage.setItem('pp2_filter_filter_user', '<?= $_POST['filter_user'] ?>');
        filter_user = '<?= $_POST['filter_user'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_filter_user')) {
            filter_user = localStorage.getItem('pp2_filter_filter_user');
        }
    <?php endif ?>

    <?php if (isset($_POST['filter_by'])) : ?>
        localStorage.setItem('pp2_filter_filter_by', '<?= $_POST['filter_by'] ?>');
        filter_by = '<?= $_POST['filter_by'] ?>';
    <?php else : ?>
        if (localStorage.getItem('pp2_filter_filter_by')) {
            filter_by = localStorage.getItem('pp2_filter_filter_by');
        }
    <?php endif ?>

    <?php if (isset($_POST['date_records_filter'])) : ?>
        localStorage.setItem('pp2_filter_date_records_filter', '<?= $_POST['date_records_filter'] ?>');
    <?php endif ?>

    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }
    $(document).ready(function() {
        oTable = $('#SLData').dataTable({
            "aaSorting": [
                [1, "desc"]
            ],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?= $Settings->rows_per_page ?>,
            'bProcessing': true,
            'bServerSide': true,
            'sAjaxSource': '<?= admin_url('payments/getPayments/1') ?>',
            'dom': '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            'fnServerData': function(sSource, aoData, fnCallback) {
                aoData.push({
                        "name": "<?= $this->security->get_csrf_token_name() ?>",
                        "value": "<?= $this->security->get_csrf_hash() ?>"
                    }, {
                        "name": "start_date",
                        "value": start_date
                    }, {
                        "name": "end_date",
                        "value": end_date
                    }, {
                        "name": "p2payment_reference_no",
                        "value": p2payment_reference_no
                    }, {
                        "name": "p2payment_method",
                        "value": p2payment_method
                    }, {
                        "name": "biller",
                        "value": biller
                    }, {
                        "name": "customer",
                        "value": customer
                    }, {
                        "name": "user",
                        "value": filter_user
                    }, {
                        "name": "filter_by",
                        "value": filter_by
                    }

                );
                $.ajax({
                    'dataType': 'json',
                    'type': 'POST',
                    'url': sSource,
                    'data': aoData,
                    'success': fnCallback
                });
            },
            'fnRowCallback': function(nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "multipayment_link";
                return nRow;
            },
            fnDrawCallback: function() {
                $('.actionsButtonContainer').html(`<a href="<?= admin_url('payments/add') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>
                <div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                        <?php if ($this->Owner || $this->Admin) : ?> 
                            <li>
                                <a href="#" id="post_sale" data-action="post_multi_payments_1"><i class="fa fa-plus-circle"></i> <?= lang('post_multi_payments') ?></a>
                            </li>
                        <?php endif ?>
                        <li>
                            <a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a>
                        </li>
                    </ul>
                </div>`);
                
                $('[data-toggle-second="tooltip"]').tooltip();
                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });
            },
            "aoColumns": [{
                    "mRender": checkbox
                },
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                {
                    "mRender": currencyFormat,
                    "bSearchable": false
                },
                {
                    "mRender": currencyFormat,
                    "bSearchable": false
                },
                {
                    "mRender": pay_status
                },
                {
                    "bSortable": false,
                    "mRender": attachment
                },
                {
                    "bSortable": false
                }
            ]
        });
    });

    <?php if ($this->session->userdata('remove_multipayments')) { ?>
        if (localStorage.getItem('customer')) {
            localStorage.removeItem('customer');
        }
        if (localStorage.getItem('sales')) {
            localStorage.removeItem('sales');
        }
        if (localStorage.getItem('customer')) {
            localStorage.removeItem('customer');
        }
        if (localStorage.getItem('purchases')) {
            localStorage.removeItem('purchases');
        }
        if (localStorage.getItem('payment_distribution')) {
            localStorage.removeItem('payment_distribution');
        }
        if (localStorage.getItem('payment_date')) {
            localStorage.removeItem('payment_date');
        }
        if (localStorage.getItem('consecutive_payment')) {
            localStorage.removeItem('consecutive_payment');
        }
    <?php
        $this->sma->unset_data('remove_multipayments');
    }
    ?>

    $(document).ready(function() {
        if (customer != '') {
            $('#filter_customer').val(customer).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "customers/getCustomer/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });

            $('#filter_customer').trigger('change');
        } else {
            $('#filter_customer').select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function(element, callback) {
                    $.ajax({
                        type: "get",
                        async: false,
                        url: site.base_url + "customers/getcustomer/" + $(element).val(),
                        dataType: "json",
                        success: function(data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "customers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function(term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function(data, page) {
                        if (data.results != null) {
                            return {
                                results: data.results
                            };
                        } else {
                            return {
                                results: [{
                                    id: '',
                                    text: lang.no_match_found
                                }]
                            };
                        }
                    }
                }
            });
        }

        if (filtered_ini_date !== undefined) {
            minutos = calcularMinutos(filtered_ini_date, '<?= date("Y-m-d H:i:s") ?>');
            if (minutos >= 10) {
                localStorage.removeItem('pp2_filter_start_date');
                localStorage.removeItem('pp2_filter_end_date');
                localStorage.removeItem('pp2_filter_biller');
                localStorage.removeItem('pp2_filter_filter_user');
                localStorage.removeItem('pp2_filter_customer');
                localStorage.removeItem('pp2_filter_p2payment_reference_no');
                localStorage.removeItem('pp2_filter_p2payment_method');
                localStorage.removeItem('pp2_filter_filtered');
                localStorage.removeItem('pp2_filter_filtered_ini_date');
                localStorage.removeItem('pp2_filter_date_records_filter');
                localStorage.removeItem('pp2_filter_filter_by');
                location.href = '<?= admin_url("payments/index") ?>';
            }
        }

        setTimeout(function() {
            setFilterText();
        }, 1500);


        <?php if ($biller_readonly) { ?>
            setTimeout(function() {
                $('#biller').select2('readonly', true);
            }, 1500);
        <?php } ?>

    });

    $(document).on('click', '#submit-purchases-filter-clean', function() {
        localStorage.removeItem('pp2_filter_start_date');
        localStorage.removeItem('pp2_filter_end_date');
        localStorage.removeItem('pp2_filter_biller');
        localStorage.removeItem('pp2_filter_filter_user');
        localStorage.removeItem('pp2_filter_customer');
        localStorage.removeItem('pp2_filter_p2payment_reference_no');
        localStorage.removeItem('pp2_filter_p2payment_method');
        localStorage.removeItem('pp2_filter_filtered');
        localStorage.removeItem('pp2_filter_filtered_ini_date');
        localStorage.removeItem('pp2_filter_date_records_filter');
        localStorage.removeItem('pp2_filter_filter_by');
        location.href = '<?= admin_url("payments/index") ?>';
    });

    function calcularMinutos(start_date, end_date) {
        var fecha1 = new Date(start_date);
        var fecha2 = new Date(end_date);
        var diff = fecha2.getTime() - fecha1.getTime();
        var minutos = diff / (1000 * 60);
        return minutos;
    }

    function setFilterText() {

        var reference_text = $('#p2payment_reference_no option:selected').data('dtprefix');
        var p2payment_method_text = $('#p2payment_method option:selected').text();
        var biller_text = $('#biller option:selected').text();
        var customer_text = $('#filter_customer').select2('data') !== null ? $('#filter_customer').select2('data').text : '';
        var start_date_text = $('#start_date_dh').val();
        var end_date_text = $('#end_date_dh').val();
        var text = "Filtros configurados : ";

        coma = false;

        if (p2payment_reference_no != '' && p2payment_reference_no !== undefined) {
            text += " Tipo documento (" + reference_text + ")";
            coma = true;
        }
        if (p2payment_method != '' && p2payment_method !== undefined) {
            text += "Medio de pago (" + p2payment_method_text + ")";
            coma = true;
        }
        if (biller != '' && biller !== undefined) {
            text += coma ? "," : "";
            text += " Sucursal (" + biller_text + ")";
            coma = true;
        }
        if (customer != '' && customer !== undefined) {
            text += coma ? "," : "";
            text += " Cliente (" + customer_text + ")";
            coma = true;
        }
        if (start_date != '' && start_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha de inicio (" + start_date_text + ")";
            coma = true;
        }
        if (end_date != '' && end_date !== undefined) {
            text += coma ? "," : "";
            text += " Fecha final (" + end_date_text + ")";
            coma = true;
        }

        $('.text_filter').html(text);

    }

    $(document).ready(function() {
        setTimeout(function() {
            <?php if (!isset($_POST['date_records_filter'])) : ?>
                $('#date_records_filter_dh').select2('val', "<?= $this->Settings->default_records_filter ?>").trigger('change');
                $('#ppayments_filter').submit();
            <?php elseif ($_POST['date_records_filter'] != $this->Settings->default_records_filter) : ?>
                $('#date_records_filter_dh').select2('val', "<?= $_POST['date_records_filter'] ?>").trigger('change');
            <?php endif ?>
        }, 150);
    });
</script>