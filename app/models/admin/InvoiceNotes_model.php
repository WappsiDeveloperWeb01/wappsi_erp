<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class InvoiceNotes_model extends CI_Model {

 	public function get_by_id($id)
 	{
 		$this->db->select("*");
 		$this->db->from("invoice_notes");
 		$this->db->where("id", $id);
 		$q = $this->db->get();

 		return $q->row();
 	}

 	public function insert($data)
 	{
 		if ($this->db->insert("invoice_notes", $data)) {
 			return TRUE;
 		}

 		return FALSE;
 	}

 	public function update($data, $id)
 	{
 		$this->db->where("id", $id);
 		if ($this->db->update("invoice_notes", $data)) {
 			return TRUE;
 		}

 		return FALSE;
 	}

}

/* End of file InvoiceNotes_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/InvoiceNotes_model.php */