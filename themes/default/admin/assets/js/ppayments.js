var rows_start = 0;
var num_rows = 100;
var max_num_rows = 100;

$(document).ready(function(){
    remove_localstorage_data();
    localStorage.removeItem('poretenciones');
    $('#date').prop('max', max_input_date);
    $('#date').prop('min', min_input_date);
    localStorage.removeItem('total_conceptos');
    if (localStorage.getItem('rcbiller') || biller_id) {
        $('#rcbiller').select2('val', (localStorage.getItem('rcbiller') ? localStorage.getItem('rcbiller') : biller_id));
        if (biller_id && ppayment_edit == false) {
            $('#rcbiller').select2('readonly', true)
        }
    }
    if (localStorage.getItem('supplier') || supplier_id) {
        supplier = localStorage.getItem('supplier') ? localStorage.getItem('supplier') : supplier_id;
        $('#supplier').val(supplier).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"suppliers/getsupplier/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "suppliers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10,
                        support_document : 'all',
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
        if (localStorage.getItem('purchases')) {
            loadItems();
        }
        if (supplier_id) {
            if (ppayment_edit == false) {
                $('#supplier').select2('readonly', true);
            }
            $.ajax({
                url : site.base_url+"suppliers/get_supplier_deposit_balance/"+supplier_id,
                dataType : "JSON"
            }).done(function(data){
                if (data.deposit_amount > 0) {
                    bootbox.alert('El proveedor seleccionado tiene <b>$'+formatMoney(data.deposit_amount)+'</b> en saldo de anticipos');
                }

                if (data.deposit_amount > 0 || data.cxc_portfolio > 0) {
                    bhtml = "";
                    if (data.deposit_amount > 0) {
                        bhtml+="<h4>Anticipos</h4><p>El cliente seleccionado tiene <b>$"+formatMoney(data.deposit_amount)+"</b> en saldo de anticipos</p>";
                    }
                    if (data.cxc_portfolio > 0) {
                        bhtml+="<h4>Cartera por cobrar</h4><p>Existe un saldo de <b>$"+formatMoney(data.cxc_portfolio)+"</b> en cartera por cobrar para este tercero</p>";
                    }
                    bootbox.alert(bhtml);
                }
            });
        }
        if (purchase_id == false) {
            $('#supplier').trigger('change');
        }
    } else {
        nssupplier();
    }
    if (localStorage.getItem('payment_distribution')) {
        $('#payment_distribution').val(localStorage.getItem('payment_distribution'));
    }
    if (purchase_id) {
        localStorage.removeItem('payment_distribution_applied');
        $.ajax({
            url: site.base_url+"purchases/purchase_suggestions",
            type: "GET",
            data : {
                'supplier_id' :  supplier_id,
                'biller_id' :  biller_id,
                'purchase_id' :  purchase_id,
                'rows_start' :  rows_start,
                'num_rows' :  num_rows,
                'ptype' : $('#purchase_type').val(),
            },
            dataType: "json",
        }).done(function(data){
            $('#amount_1').val(0);
            $('#base').val(0);
            localStorage.removeItem('purchases');
            localStorage.setItem('purchases', JSON.stringify(data));
            loadItems();
            $('.enable_payment').trigger('click');
        }).fail(function(){
            loadItems();
        });
    }
    if (pp_reference_no) {
        localStorage.removeItem('payment_distribution_applied');
        $.ajax({
            url: site.base_url+"purchases/purchase_suggestions",
            type: "GET",
            data : {
                'supplier_id' :  supplier_id,
                'biller_id' :  biller_id,
                'pp_reference_no' :  pp_reference_no,
                'rows_start' :  rows_start,
                'num_rows' :  num_rows,
                'ptype' : $('#purchase_type').val(),
            },
            dataType: "json",
        }).done(function(data){
            $('#amount_1').val(0);
            $('#base').val(0);
            localStorage.removeItem('purchases');
            localStorage.setItem('purchases', JSON.stringify(data));
            loadItems();
            $('.enable_payment').trigger('click');
        }).fail(function(){
            loadItems();
        });
    }
    if (payment_date = localStorage.getItem('payment_date')) {
        $('#payment_date').val(payment_date);
    }
    if (consecutive_payment = localStorage.getItem('consecutive_payment')) {
        $('#consecutive_payment').val(consecutive_payment);
    }
    if ((purchase_id || ppayment_edit) && ptype) {
        $('#purchase_type').select2('val', ptype).trigger('change');
        if (ppayment_edit == false) {
            $('#purchase_type').select2('readonly', true);
        }
    }
    if (ppayment_edit && document_type_id) {
        $('#document_type_id').select2('val', document_type_id).trigger('change');
        if (ppayment_edit == false) {
            $('#document_type_id').select2('readonly', true);
        }
    }
    if (ppayment_edit && payment_date) {
        $('#payment_date').val(payment_date);
        if (ppayment_edit == false) {
            $('#payment_date').prop('readonly', true);
        }
    }
    if (ppayment_edit && ppdate) {
        $("#date").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                startDate: min_input_date,
                endDate: max_input_date,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', (ppdate));
        if (ppayment_edit == false) {
            $('#date').prop('readonly', true);
        }
    } else {
        $("#date").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                startDate: min_input_date,
                endDate: max_input_date,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', new Date());
    }
    $('#rcbiller').trigger('change');
    $('#paid_by_1').trigger('change');
});

$(document).on('keyup', function(e){
    $('#ajaxCall').fadeIn();
    if (e.keyCode == 13) {
        setTimeout(function() {
            $('#add_payment').prop('disabled', false);
            $('#ajaxCall').fadeOut();
        }, 800);
    }
});

// clear localStorage and reload
$(document).on('keyup', function(e){
    $('#ajaxCall').fadeIn();
    if (e.keyCode == 13) {
        setTimeout(function() {
            $('#add_payment').prop('disabled', false);
            $('#ajaxCall').fadeOut();
        }, 800);
    }
});

// clear localStorage and reload
$('#reset').click(function (e) {
    bootbox.confirm(lang.r_u_sure, function (result) {
        remove_localstorage_data();
        $('#modal-loading').show();
        // location.reload();
        window.location.href = site.base_url+"payments/padd";
    });
});

$('#add_payment').click(function(){

});

function remove_localstorage_data(){
    if (localStorage.getItem('purchases')) {
        localStorage.removeItem('purchases');
    }
    if (localStorage.getItem('supplier')) {
        localStorage.removeItem('supplier');
    }
    if (localStorage.getItem('payment_distribution')) {
        localStorage.removeItem('payment_distribution');
    }
    if (localStorage.getItem('payment_date')) {
        localStorage.removeItem('payment_date');
    }
    if (localStorage.getItem('consecutive_payment')) {
        localStorage.removeItem('consecutive_payment');
    }
    if (localStorage.getItem('rcbiller')) {
        localStorage.removeItem('rcbiller');
    }
    if (localStorage.getItem('payment_distribution_applied')) {
        localStorage.removeItem('payment_distribution_applied');
    }
}

function nssupplier() {
    $('#supplier').select2('destroy');
    if (!ptype) {
        ptype = $('#purchase_type').val();
    }
    $('#supplier').select2({
        minimumInputLength: 1,
        data: [],
        initSelection: function (element, callback) {
          $.ajax({
            type: "get", async: false,
            url: site.base_url+"suppliers/getsupplier/" + $(element).val(),
            dataType: "json",
            success: function (data) {
              callback(data[0]);
            }
          });
        },
        ajax: {
          url: site.base_url+"suppliers/suggestions",
          dataType: 'json',
          quietMillis: 15,
          data: function (term, page) {
            return {
              term: term,
              limit: 10,
              ptype : 7,
              support_document : 'all',
            };
          },
          results: function (data, page) {
            if (data.results != null) {
              return {results: data.results};
            } else {
              return {results: [{id: '', text: lang.no_match_found}]};
            }
          }
        }
    });
}
$(document).on('change', '#purchase_type', function(){
    $('#rcbiller').trigger('change');
    setTimeout(function() {
        nssupplier();
    }, 850);
});
$(document).on('change', '#supplier', function(){
    rows_start = 0;
    localStorage.setItem('supplier', $(this).val());
    localStorage.removeItem('payment_distribution_applied');
    $('#amount_1').val(formatMoney(0));
    $('#total_rete_amount_2').val(formatMoney(0));
    $('#total_other_concepts').val(formatMoney(0));
    $('#total_received').val(formatMoney(0));
    if (purchases = localStorage.getItem('purchases')) {
        localStorage.removeItem('purchases');
    }
    $.ajax({
            url: site.base_url+"purchases/purchase_suggestions",
            type: "GET",
            data : {
                'supplier_id' :  $(this).val(),
                'biller_id' :  $('#rcbiller').val(),
                'rows_start' :  rows_start,
                'num_rows' :  num_rows,
                'ptype' : $('#purchase_type').val(),
            },
        dataType: "json",
    }).done(function(data){
        if (data.response !== undefined && data.response == 0) {
            command: toastr.warning('El proveedor seleccionado no tiene cuentas por pagar pendientes', 'Sin resultados', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            localStorage.removeItem('purchases');
            loadItems();
        } else {
            $('#amount_1').val(0);
            $('#base').val(0);
            localStorage.removeItem('purchases');
            localStorage.setItem('purchases', JSON.stringify(data));
            loadItems();
        }
    }).fail(function(){
        loadItems();
    });
    var supplier_validate_min_base_retention = false;
    $.ajax({
        url : site.base_url+"suppliers/get_supplier_by_id/"+$(this).val()+"/"+$('#rcbiller').val(),
        dataType : "JSON"
    }).done(function(data){
        if (data.supplier_validate_min_base_retention == 0) {
            supplier_validate_min_base_retention = true;
        }
        localStorage.setItem('supplier_validate_min_base_retention', supplier_validate_min_base_retention);
        $('#supplier_portfolio').val(formatMoney(data.portfolio));
    });
    $.ajax({
        url : site.base_url+"suppliers/get_supplier_deposit_balance/"+$(this).val(),
        dataType : "JSON"
    }).done(function(data){
        if (data.deposit_amount > 0 || data.cxc_portfolio > 0) {
            bhtml = "";
            if (data.deposit_amount > 0) {
                 bhtml+='El proveedor seleccionado tiene <b>$'+formatMoney(data.deposit_amount)+'</b> en saldo de anticipos';
            }
            if (data.cxc_portfolio > 0) {
                bhtml+="<h4>Cartera por cobrar</h4><p>Existe un saldo de <b>$"+formatMoney(data.cxc_portfolio)+"</b> en cartera por cobrar para este tercero</p>";
            }
            bootbox.alert(bhtml);
        }
    });
});

$(document).on('click, change', '.enable_payment', function(){
    var check = $(this);
    var purchases = JSON.parse(localStorage.getItem('purchases'));
    var row = check.closest('tr');
    row_id = row.attr('data-item-id');
    purchases[row_id].purchase_selected = check.prop('checked');
    var reset_rete = false;
    if (!check.prop('checked')) {
        purchases[row_id].amount_to_paid = 0;
        reset_rete = true;
        purchases[row_id].activated_manually = false;
        purchases[row_id].deactivated_manually = true;
        delete purchases[row_id].amount_to_paid_manually_changed;
        //$('.retencion_'+purchases[row_id].id).prop('check', false).trigger('change');
    } else {
        purchases[row_id].amount_to_paid = purchases[row_id].grand_total - purchases[row_id].paid;
        purchases[row_id].activated_manually = true;
        purchases[row_id].deactivated_manually = false;
    }
    localStorage.removeItem('purchases');
    localStorage.setItem('purchases', JSON.stringify(purchases));
    if (reset_rete) {
        reiniciar_retencion(row_id, true);
    } else {
        loadItems();
    }
});

$(document).on('click, change', '.enable_retencion', function(){
    var check = $(this);
    var row = check.closest('tr');
    row_id = row.attr('data-item-id');
    if (check.prop('checked')) {
    } else {
        reiniciar_retencion(row_id, true);
    }
});

$(document).on('change', '.amount', function(){
  var val = 0;
  var purchases = JSON.parse(localStorage.getItem('purchases'));
  var rowP = $(this).closest('tr');
  row_idP = rowP.attr('data-item-id');
  $('.amount').each(function(index){
    // console.log('readonly = '+$(this).prop('readonly'));
    var row = $(this).closest('tr');
    row_id = row.attr('data-item-id')
    if (!$(this).prop('readonly')) {
      var max = $(this).data('totalpaid');
      if ($(this).val() >= 0 && $(this).val() <= max) {
        val += parseInt($(this).val());
      } else {
        $(this).val(max);
        val += parseFloat(max);
      }
    }
    if (purchases[row_id].amount_to_paid != $(this).val()) {
        purchases[row_id].amount_to_paid_manually_changed = true;
    }
    purchases[row_id].amount_to_paid = $(this).val();
  });
  setTimeout(function() {
    // $('#total_amount_paid').val(val);
    $('#amount_1').val(val);
    $('#base').val(val);
    localStorage.removeItem('purchases');
    localStorage.setItem('purchases', JSON.stringify(purchases));
    // loadItems();
    reiniciar_retencion(row_idP);
  }, 800);
});
$(document).on('change', '.paid_by', function () {
    var p_val = $(this).val();
    localStorage.setItem('paid_by', p_val);
    $('#rpaidby').val(p_val);
    if (p_val == 'cash' ||  p_val == 'other') {
        $('.pcheque_1').hide();
        $('.pcc_1').hide();
        $('.pcash_1').show();
        $('#payment_note_1').focus();
    } else if (p_val == 'CC') {
        $('.pcheque_1').hide();
        $('.pcash_1').hide();
        $('.pcc_1').show();
        $('#pcc_no_1').focus();
    } else if (p_val == 'Cheque') {
        $('.pcc_1').hide();
        $('.pcash_1').hide();
        $('.pcheque_1').show();
        $('#cheque_no_1').focus();
    } else {
        $('.pcheque_1').hide();
        $('.pcc_1').hide();
        $('.pcash_1').hide();
    }
    if (p_val == 'gift_card') {
        $('.gc').show();
        $('.ngc').hide();
        $('#gift_card_no').focus();
    } else {
        $('.ngc').show();
        $('.gc').hide();
        $('#gc_details').html('');
    }
});


$(document).on('click, change', '.check_all', function(){

    var input = $(this);
    var status = input.prop('checked') ? true : false;

    var purchases = JSON.parse(localStorage.getItem('purchases'));

    $('.enable_payment').each(function(index){

        var check = $(this);
        var row = check.closest('tr');
        row_id = row.attr('data-item-id');

        if (status) {

            $(this).prop('checked', status);

        } else {

            delete purchases[row_id].total_rete_amount;
            delete purchases[row_id].fuente_option;
            delete purchases[row_id].iva_option;
            delete purchases[row_id].ica_option;
            delete purchases[row_id].otros_option;
            delete purchases[row_id].id_rete_fuente;
            delete purchases[row_id].id_rete_iva;
            delete purchases[row_id].id_rete_ica;
            delete purchases[row_id].id_rete_otros;
            purchases[row_id].rete_fuente_percentage = 0;
            purchases[row_id].rete_ica_percentage = 0;
            purchases[row_id].rete_iva_percentage = 0;
            purchases[row_id].rete_other_percentage = 0;
            purchases[row_id].rete_fuente_base = 0;
            purchases[row_id].rete_ica_base = 0;
            purchases[row_id].rete_iva_base = 0;
            purchases[row_id].rete_other_base = 0;
            purchases[row_id].rete_fuente_total = 0;
            purchases[row_id].rete_ica_total = 0;
            purchases[row_id].rete_iva_total = 0;
            purchases[row_id].rete_other_total = 0;
            purchases[row_id].rete_fuente_account = 0;
            purchases[row_id].rete_ica_account = 0;
            purchases[row_id].rete_iva_account = 0;
            purchases[row_id].rete_other_account = 0;

            $(this).removeAttr('checked');
        }

        var purchase_id = $(this).data('purchaseid');

        if ($(this).prop('checked')) {
            var input = $('.purchase_id_'+purchase_id);
            var amount = input.data('totalpaid');
            input.prop('readonly', false).val(amount);
            purchases[row_id].amount_to_paid = amount;
            purchases[row_id].purchase_selected = true;
            purchases[row_id].activated_manually = true;
        } else {
            $(this).removeProp('checked');
            var input = $('.purchase_id_'+purchase_id);
            var amount = input.data('totalpaid');
            input.prop('readonly', true).val(0);
            purchases[row_id].amount_to_paid = 0;
        }


        purchases[row_id].purchase_selected = status;

    });

    localStorage.removeItem('purchases');
    localStorage.setItem('purchases', JSON.stringify(purchases));

    loadItems();

});

$(document).on('click', '.edit', function(){

    reset_retention_window();

    itemid = $(this).data('item-id');
    purchases = JSON.parse(localStorage.getItem('purchases'));
    purchase = purchases[itemid];

    $('#rtModalLabel').text("Factura N° "+purchase.reference_no);
    $('#rtModalLabel2').text(purchase.supplier);

    if (purchase.rete_fuente_base_paid > 0 || purchase.rete_iva_base_paid > 0 || purchase.rete_ica_base_paid > 0 || purchase.rete_other_base_paid > 0) {
        $('#rete_otros').prop('disabled', true);
    } else {
        $('#rete_otros').prop('disabled', false);
    }

    $('#suggested_total').html(0);
    $('#suggested_tax').html(0);
    $('#suggested_subtotal').html(0);

    if (purchase.purchase_selected) {

        $('#rete_base_subtotal').val(0).prop('readonly', true);
        $('#rete_base_tax').val(0).prop('readonly', true);
        $('#rete_base_total').val(0).prop('readonly', true);

        $('#rete_applied').val(itemid);

        if (rete_total = purchase.total_rete_amount) {
            retenciones = {
                    'id_rete_fuente' : purchase.id_rete_fuente,
                    'id_rete_iva' : purchase.id_rete_iva,
                    'id_rete_ica' : purchase.id_rete_ica,
                    'id_rete_otros' : purchase.id_rete_otros,
                    'id_rete_bomberil' : purchase.id_rete_bomberil,
                    'id_rete_autoaviso' : purchase.id_rete_autoaviso,
                    'rete_fuente_assumed' : purchase.rete_fuente_assumed,
                    'rete_iva_assumed' : purchase.rete_iva_assumed,
                    'rete_ica_assumed' : purchase.rete_ica_assumed,
                    'rete_other_assumed' : purchase.rete_other_assumed,
                };

            recalcular_retenciones(retenciones);
        }

        if (discount = purchase.discount) {
            recalcular_descuento(discount);
        }

        $('#rtModal').appendTo("body").modal('show');
    }

});

$(document).on('blur', '#payment_distribution', function(){
    var val = parseFloat($(this).val());
    if (val > 0) {
        localStorage.setItem('payment_distribution', val);
    } else {
        localStorage.removeItem('payment_distribution');
        localStorage.removeItem('payment_distribution_applied');
    }
    loadItems();
});
//aca load
function loadItems(){
    $('#table_purchases tbody').empty();
    var amount1 = 0;
    if (localStorage.getItem('purchases')) {
        var purchases = JSON.parse(localStorage.getItem('purchases'));
        var max_date = null;
        var total_received = 0;
        var total_receipted = 0;
        var total_rete_amount = 0;
        var disable_submit = false;
        var count = 0;
        var max = max_num_rows;
        var payment_distribution = localStorage.getItem('payment_distribution') ? parseFloat(localStorage.getItem('payment_distribution')) : 0;
        var exists_payment_distribution = localStorage.getItem('payment_distribution') ? true : false;
        var total_discounted = 0;
        var payment_distribution_applied = localStorage.getItem('payment_distribution_applied');
        $.each(purchases, function(index){
            count++;
            if (count == (max + 1)) {
                return false;
            }
            purchases[index].purchase_in_view = true;
            purchase = this;

            if (exists_payment_distribution && purchase.deactivated_manually === false && payment_distribution > 0) {
                purchase_balance = (purchase.amount_to_paid > 0 ? purchase.amount_to_paid : (purchase.grand_total - purchase.paid));
                    if (purchase_balance <= payment_distribution) {
                        payment_distribution -= purchase_balance;
                        purchase.amount_to_paid = purchase_balance;
                        purchase.purchase_selected = true;
                    } else if (purchase_balance > payment_distribution) {
                        purchase.amount_to_paid = payment_distribution;
                        payment_distribution = 0;
                        purchase.purchase_selected = true;
                    }
            } else {
                if (!purchase.activated_manually){
                    purchase.purchase_selected = false;
                    purchase.amount_to_paid = 0;
                }
                if (purchase.activated_manually === true && exists_payment_distribution && payment_distribution <= 0) {
                    purchase.amount_to_paid = 0;
                }
            }
            var purchase_selected = purchase.purchase_selected;
            if (purchase_selected) {
                if (max_date == null || max_date != null && purchase.max_date > max_date) {
                    max_date = purchase.max_date;
                }
            }
            var purchase_total_rete_amount = 0;
            var invalid_retention = false;
            total_rete_amount += formatDecimal(purchase.total_rete_amount);
            purchase_total_rete_amount = formatDecimal(purchase.total_rete_amount);

            max_amount_to_paid_without_retention = formatDecimal(purchase.grand_total * 0.9);
            advertising = false;
            if ((formatDecimal(purchase.amount_to_paid) + formatDecimal(purchase.paid)) > max_amount_to_paid_without_retention) {
                if(purchase_total_rete_amount < 1){
                    advertising = true;
                }
            }

            if (formatDecimal(purchase.amount_to_paid) < formatDecimal(purchase.total_rete_amount)) {
                // disable_submit = true;
                // invalid_retention = true;
            }
            var tr_html = $('<tr data-item-id="'+index+'"></tr>');
            var discounted = 0;
            if (purchase.discount) {
                var discount_html = '';
                discount = purchase.discount;
                arr_apply = discount.discount_apply_to;
                arr_amount = discount.discount_amount;
                arr_calculated = discount.discount_amount_calculated;
                arr_ledger_id = discount.discount_ledger_id;
                $.each(arr_apply, function(index, val){
                    index_d = index+1;
                    apply_to = val;
                    amount = arr_amount[index];
                    amount_cal = arr_calculated[index];
                    ledger_id = arr_ledger_id[index];
                    if (amount_cal > 0) {
                        discount_html +=
                                        "<input type='hidden' name='discount_purchase_id[]' value='"+purchase.id+"'>"+
                                        "<input type='hidden' name='discount_purchase_reference[]' value='"+purchase.reference_no+"'>"+
                                        "<input type='hidden' name='discount_apply_to[]' value='"+apply_to+"'>"+
                                        "<input type='hidden' name='discount_amount[]' value='"+amount+"'>"+
                                        "<input type='hidden' name='discount_amount_calculated[]' value='"+amount_cal+"'>"+
                                        "<input type='hidden' name='discount_ledger_id[]' value='"+ledger_id+"'>"
                                        ;
                        discounted += parseFloat(amount_cal);
                        total_discounted += parseFloat(amount_cal);
                    }
                });
            }

            commision = false;
            if (parseFloat(purchase.collection_comm_perc) > 0) {
                perc_commision = parseFloat(purchase.collection_comm_perc) / 100;
                if (perc_commision > 0 && purchase.amount_to_paid > 0) {
                    base_rete = (purchase.amount_to_paid / (purchase.grand_total - purchase.prev_retentions_total)) * purchase.prev_retentions_total;
                    base_commision = (((
                                        (
                                            parseFloat(purchase.amount_to_paid) +
                                            parseFloat(base_rete) +
                                            parseFloat(purchase.rete_fuente_total) +
                                            parseFloat(purchase.rete_iva_total) +
                                            parseFloat(purchase.rete_ica_total) +
                                            parseFloat(purchase.rete_other_total)
                                        )
                                        / purchase.grand_total) * purchase.total) - discounted);
                    commision = base_commision * perc_commision;
                }
            }

            inv_total_discounted = (formatDecimal(tra = purchase.total_rete_amount ? purchase.total_rete_amount : 0) + parseFloat(discounted));
            if ((purchase.amount_to_paid + inv_total_discounted) > (purchase.grand_total - purchase.paid)) {
                purchase.amount_to_paid -= inv_total_discounted;
                if (exists_payment_distribution) {
                    payment_distribution += inv_total_discounted;
                }
            }

            total_received += parseFloat(purchase.amount_to_paid) + parseFloat(inv_total_discounted);
            tbody = "<td>"+purchase.reference_no+"</td>"+
                    "<td>"+(purchase.return_purchase_ref != null ? purchase.return_purchase_ref : '')+"</td>"+
                    "<td>"+purchase.date+"</td>"+
                    "<td>"+(purchase.due_date ? purchase.due_date : purchase.date)+"</td>"+
                    "<td class='text-right'>"+formatMoney(purchase.grand_total)+"</td>"+
                    "<td class='text-right'>"+
                        (purchase.paid > 0 ? "<a href='"+site.base_url+"purchases/payments/"+purchase.id+"' data-toggle='modal' data-target='#myModal'> <i class='fa fa-eye'></i> "+formatMoney(purchase.paid)+"</a>" :
                        formatMoney(purchase.paid))+
                    "</td>"+
                    "<td class='text-right'>"+formatMoney(purchase.grand_total - purchase.paid < 0 ? 0 : purchase.grand_total - purchase.paid)+"</td>"+
                    "<td>"+
                        "<div class='input-group'>"+
                          "<input type='hidden' name='purchase_id["+index+"]' value='"+purchase.id+"'>"+

                          (advertising ? "<span class='input-group-addon' style='cursor:pointer;' data-toggle='tooltip' data-placement='bottom' title='EL valor a pagar es mayor al 90% del total de la factura, después no le será posible aplicarle retención.'><i class='text-danger fa fa-exclamation-triangle'></i></span>" : "")+

                          "<input type='text' name='amount["+index+"]' value='"+
                          (
                            purchase_selected ? 
                                (purchase.amount_to_paid >= 0 || exists_payment_distribution) ?
                                purchase.amount_to_paid : 
                                (purchase.grand_total - purchase.paid) 
                                : 0)+"' data-totalpaid='"+(purchase.grand_total - purchase.paid)+"' class='amount form-control number_mask purchase_id_"+purchase.id+" text-right' "+(purchase_selected ? "" : "readonly='true'")+" >"+
                        "</div>"+
                    "</td>"+
                    "<td>"+
                        "<div class='input-group'>"+
                            "<input type='hidden' name='purchase_id_rete_fuente["+index+"]' value='"+formatDecimal(purchase.id_rete_fuente)+"'>"+
                            "<input type='hidden' name='purchase_id_rete_iva["+index+"]' value='"+formatDecimal(purchase.id_rete_iva)+"'>"+
                            "<input type='hidden' name='purchase_id_rete_ica["+index+"]' value='"+formatDecimal(purchase.id_rete_ica)+"'>"+
                            "<input type='hidden' name='purchase_id_rete_bomberil["+index+"]' value='"+formatDecimal(purchase.id_rete_bomberil)+"'>"+
                            "<input type='hidden' name='purchase_id_rete_autoaviso["+index+"]' value='"+formatDecimal(purchase.id_rete_autoaviso)+"'>"+
                            "<input type='hidden' name='purchase_id_rete_other["+index+"]' value='"+formatDecimal(purchase.id_rete_otros)+"'>"+

                            "<input type='hidden' name='purchase_rete_fuente_total["+index+"]' value='"+purchase.rete_fuente_total+"'>"+
                            "<input type='hidden' name='purchase_rete_iva_total["+index+"]' value='"+purchase.rete_iva_total+"'>"+
                            "<input type='hidden' name='purchase_rete_ica_total["+index+"]' value='"+purchase.rete_ica_total+"'>"+
                            "<input type='hidden' name='purchase_rete_bomberil_total["+index+"]' value='"+purchase.rete_bomberil_total+"'>"+
                            "<input type='hidden' name='purchase_rete_autoaviso_total["+index+"]' value='"+purchase.rete_autoaviso_total+"'>"+
                            "<input type='hidden' name='purchase_rete_other_total["+index+"]' value='"+purchase.rete_other_total+"'>"+

                            "<input type='hidden' name='purchase_rete_fuente_percentage["+index+"]' value='"+purchase.rete_fuente_percentage+"'>"+
                            "<input type='hidden' name='purchase_rete_iva_percentage["+index+"]' value='"+purchase.rete_iva_percentage+"'>"+
                            "<input type='hidden' name='purchase_rete_ica_percentage["+index+"]' value='"+purchase.rete_ica_percentage+"'>"+
                            "<input type='hidden' name='purchase_rete_bomberil_percentage["+index+"]' value='"+purchase.rete_bomberil_percentage+"'>"+
                            "<input type='hidden' name='purchase_rete_autoaviso_percentage["+index+"]' value='"+purchase.rete_autoaviso_percentage+"'>"+
                            "<input type='hidden' name='purchase_rete_other_percentage["+index+"]' value='"+purchase.rete_other_percentage+"'>"+

                            "<input type='hidden' name='purchase_rete_fuente_base["+index+"]' value='"+purchase.rete_fuente_base+"'>"+
                            "<input type='hidden' name='purchase_rete_iva_base["+index+"]' value='"+purchase.rete_iva_base+"'>"+
                            "<input type='hidden' name='purchase_rete_ica_base["+index+"]' value='"+purchase.rete_ica_base+"'>"+
                            "<input type='hidden' name='purchase_rete_bomberil_base["+index+"]' value='"+purchase.rete_bomberil_base+"'>"+
                            "<input type='hidden' name='purchase_rete_autoaviso_base["+index+"]' value='"+purchase.rete_autoaviso_base+"'>"+
                            "<input type='hidden' name='purchase_rete_other_base["+index+"]' value='"+purchase.rete_other_base+"'>"+

                            "<input type='hidden' name='purchase_rete_fuente_account["+index+"]' value='"+purchase.rete_fuente_account+"'>"+
                            "<input type='hidden' name='purchase_rete_iva_account["+index+"]' value='"+purchase.rete_iva_account+"'>"+
                            "<input type='hidden' name='purchase_rete_ica_account["+index+"]' value='"+purchase.rete_ica_account+"'>"+
                            "<input type='hidden' name='purchase_rete_bomberil_account["+index+"]' value='"+purchase.rete_bomberil_account+"'>"+
                            "<input type='hidden' name='purchase_rete_autoaviso_account["+index+"]' value='"+purchase.rete_autoaviso_account+"'>"+
                            "<input type='hidden' name='purchase_rete_other_account["+index+"]' value='"+purchase.rete_other_account+"'>"+

                            "<input type='hidden' name='purchase_rete_fuente_assumed["+index+"]' value='"+purchase.rete_fuente_assumed+"'>"+
                            "<input type='hidden' name='purchase_rete_iva_assumed["+index+"]' value='"+purchase.rete_iva_assumed+"'>"+
                            "<input type='hidden' name='purchase_rete_ica_assumed["+index+"]' value='"+purchase.rete_ica_assumed+"'>"+
                            "<input type='hidden' name='purchase_rete_other_assumed["+index+"]' value='"+purchase.rete_other_assumed+"'>"+

                            "<input type='hidden' name='purchase_rete_fuente_assumed_account["+index+"]' value='"+purchase.rete_fuente_assumed_account+"'>"+
                            "<input type='hidden' name='purchase_rete_iva_assumed_account["+index+"]' value='"+purchase.rete_iva_assumed_account+"'>"+
                            "<input type='hidden' name='purchase_rete_ica_assumed_account["+index+"]' value='"+purchase.rete_ica_assumed_account+"'>"+
                            "<input type='hidden' name='purchase_rete_bomberil_assumed_account["+index+"]' value='"+purchase.rete_bomberil_assumed_account+"'>"+
                            "<input type='hidden' name='purchase_rete_autoaviso_assumed_account["+index+"]' value='"+purchase.rete_autoaviso_assumed_account+"'>"+
                            "<input type='hidden' name='purchase_rete_other_assumed_account["+index+"]' value='"+purchase.rete_other_assumed_account+"'>"+

                            (invalid_retention ? "<span class='input-group-addon edit text-danger' data-item-id='"+index+"' style='cursor:pointer;' ata-toggle='tooltip' data-placement='bottom' title='El valor de la retención es mayor al monto a pagar.'><i class='fa fa-exclamation-triangle'></i></span>" : "<span class='input-group-addon edit' data-item-id='"+index+"' style='cursor:pointer;'><i class='fa fa-pencil'></i></span>")+
                            (purchase.discount ? discount_html : '')+

                            "<input type='hidden' name='purchase_commision_base["+index+"]' value='"+(commision ? base_commision : null)+"'>"+
                            "<input type='hidden' name='purchase_commision_percentage["+index+"]' value='"+(commision ? perc_commision * 100 : null)+"'>"+
                            "<input type='hidden' name='purchase_commision_amount["+index+"]' value='"+(commision ? formatDecimals(commision) : null)+"'>"+
                            "<input type='hidden' name='purchase_seller_id["+index+"]' value='"+purchase.seller_id+"'>"+

                            "<input type='text' class='form-control text-right' value='"+inv_total_discounted+"' readonly>"+
                            "<span class='input-group-addon'>"+
                                "<input type='checkbox' class='retencion_"+purchase.id+" enable_retencion' data-purchaseid='"+purchase.id+"' "+(tra = purchase.total_rete_amount ? "checked" : "")+">"+
                            "</span>"+
                        "</div>"+
                    "</td>"+
                    "<td>"+
                        "<input type='checkbox' name='row_selected["+index+"]' class='enable_payment' data-purchaseid='"+purchase.id+"'  "+(purchase_selected ? 'checked="true"' : '')+">"+
                    "</td>";
            tr_html.html(tbody);
            tr_html.appendTo('#table_purchases tbody');
            // total_receipted = total_received - total_rete_amount - total_discounted;
            total_receipted = total_received;
            amount1 = total_received - (parseFloat(total_rete_amount) + parseFloat(total_discounted));
            $('#amount_1').val(formatMoney(amount1));
            $('#total_rete_amount_2').val(formatMoney(total_rete_amount + total_discounted));
            $('#total_received').val(formatMoney(total_receipted));
            $('#base').val(total_received);
        });
        
        if (parseFloat(payment_distribution) < parseFloat(localStorage.getItem('payment_distribution'))) {
            localStorage.setItem('payment_distribution_applied', 1);
        }

        localStorage.removeItem('purchases');
        localStorage.setItem('purchases', JSON.stringify(purchases));

        if (formatDecimal(total_receipted) < 0) {
            disable_submit = true;
            $('.negative_alert').css('display', '');
            Command: toastr.error('El monto total recibido está en negativo.', 'Error', {onHidden : function(){}})
        } else {
            $('.negative_alert').css('display', 'none');
        }

        if (disable_submit) {
            $('#add_payment').prop('disabled', true);
        } else {
            $('#add_payment').prop('disabled', false);
        }

        if (payment_distribution > 0) {
            $('#deposit_amount_text').val(formatMoney(payment_distribution));
            $('#deposit_amount').val(payment_distribution);
            $('#deposit').fadeIn();
            command: toastr.warning('Se habilitó una forma para indicar si el saldo de este monto se registrará o no cómo un anticipo para el cliente', 'Se detectó saldo del monto a distribuir', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                    onclick: function () {
                        $("html, body").animate({ scrollTop: $('#deposit')[0].scrollHeight}, 1000)
                    }
                });
            $('.set_deposit_on').iCheck('check');
        } else {
            $('#deposit_amount_text').val(formatMoney(0));
            $('#deposit_amount').val(0);
            $('#deposit').fadeOut();
            $('.set_deposit_off').iCheck('check');
        }

        set_number_mask();
        $('#paid_by_1').trigger('change');
        if (max_date != null) {
            $('input[name="date"]').prop('min', max_date);
        }
    }

    if (total_conceptos = localStorage.getItem('total_conceptos')) {
        amount1 += formatDecimal(total_conceptos);
        $('#total_other_concepts').val(formatMoney(total_conceptos));
        $('#amount_1').val(formatMoney(amount1));
    }
}

// JS Retenciones

    $(document).on('click', '#rete_fuente', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
             $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/FUENTE"
            }).done(function(data){
                $('#rete_fuente_option').html(data);
                $('#rete_fuente_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_fuente_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_fuente_option option:selected').data('apply');
            $('#rete_fuente_option').select2('val', '').attr('disabled', true).select().trigger('change');
            $('#rete_fuente_tax').val('');
            $('#rete_fuente_valor_show').val('');
            setReteTotalAmount($('#rete_fuente_valor').val(), '-');
            $('#rete_fuente_valor').val('');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_iva', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/IVA"
            }).done(function(data){
                $('#rete_iva_option').html(data);
                $('#rete_iva_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_iva_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_iva_option option:selected').data('apply');
            $('#rete_iva_option').select2('val', '').attr('disabled', true).select().trigger('change');
            $('#rete_iva_tax').val('');
            $('#rete_iva_valor_show').val('');
            setReteTotalAmount($('#rete_iva_valor').val(), '-');
            $('#rete_iva_valor').val('');
            resetBase(apply);
        }
    });

    $(document).on('click', '#rete_ica', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/ICA"
            }).done(function(data){
                $('#rete_ica_option').html(data);
                $('#rete_ica_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_ica_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_ica_option option:selected').data('apply');
            $('#rete_ica_option').select2('val', '').attr('disabled', true).select().trigger('change');
            $('#rete_ica_tax').val('');
            $('#rete_ica_valor_show').val('');
            setReteTotalAmount($('#rete_ica_valor').val(), '-');
            reset_rete_bomberil();
            $('#rete_ica_valor').val('');
            resetBase(apply);
        }
    });
    $(document).on('click', '#rete_otros', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/OTRA"
            }).done(function(data){
                $('#rete_otros_option').html(data);
                $('#rete_otros_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_otros_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_otros_option option:selected').data('apply');
            $('#rete_otros_option').select2('val', '').attr('disabled', true).select().trigger('change');
            $('#rete_otros_tax').val('');
            $('#rete_otros_valor_show').val('');
            setReteTotalAmount($('#rete_otros_valor').val(), '-');
            $('#rete_otros_valor').val('');
            resetBase(apply);
        }
    });
    $(document).on('click', '#rete_bomberil', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/BOMB"
            }).done(function(data){
                $('#rete_bomberil_option').html(data);
                $('#rete_bomberil_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_bomberil_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_bomberil_option option:selected').data('apply');
            $('#rete_bomberil_option').select2('val', '').attr('disabled', true).select().trigger('change');
            $('#rete_bomberil_tax').val('');
            $('#rete_bomberil_valor_show').val('');
            setReteTotalAmount($('#rete_bomberil_valor').val(), '-');
            $('#rete_bomberil_valor').val('');
            resetBase(apply);
        }
    });
    $(document).on('click', '#rete_autoaviso', function(){
        var check = $(this);
        $('#updateOrderRete').prop('disabled', true);
        if (check.is(':checked')) {
            $.ajax({
                url: site.base_url+"purchases/opcionesWithHolding/TABL"
            }).done(function(data){
                $('#rete_autoaviso_option').html(data);
                $('#rete_autoaviso_option').attr('disabled', false).select();
                if (selected = check.data('selected')) {
                    $('#rete_autoaviso_option').select2('val', selected).trigger('change');
                }
                setReteTotalAmount(0, '+');
                // $('#updateOrderRete').prop('disabled', false);
            }).fail(function(data){
                console.log(data);
            });
        } else {
            apply = $('#rete_autoaviso_option option:selected').data('apply');
            $('#rete_autoaviso_option').select2('val', '').attr('disabled', true).select().trigger('change');
            $('#rete_autoaviso_tax').val('');
            $('#rete_autoaviso_valor_show').val('');
            setReteTotalAmount($('#rete_autoaviso_valor').val(), '-');
            $('#rete_autoaviso_valor').val('');
            resetBase(apply);
        }
    });
    var rete_fuente_amount = 0;
    var rete_ica_amount = 0;
    var rete_iva_amount = 0;
    var rete_otros_amount = 0;

    $(document).on('change', '#rete_fuente_option, #rete_iva_option, #rete_ica_option, #rete_otros_option, #rete_bomberil_option, #rete_autoaviso_option', function(){
        rete_purchase_option_changed($(this).prop('id'));
    });

    function getReteAmount(apply){
        amount = 0;
        if (apply == "ST") {
            amount = $('#rete_base_subtotal').val();
        } else if (apply == "TX") {
            amount = $('#rete_base_tax').val();
        } else if (apply == "TO") {
            amount = $('#rete_base_total').val();
        } else if (apply == "IC") {
            amount = $('#rete_ica_valor').val();
        }
        return amount;
    }

    function setReteTotalAmount(amount, action){
        var purchases = JSON.parse(localStorage.getItem('purchases'));
        var purchase = purchases[$('#rete_applied').val()];
        rtotal_rete = 
                    ($('#rete_fuente_assumed').is(':checked') ? 0 : formatDecimal($('#rete_fuente_valor').val())) + 
                    ($('#rete_iva_assumed').is(':checked') ? 0 : formatDecimal($('#rete_iva_valor').val())) + 
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_ica_valor').val())) + 
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_bomberil_valor').val())) + 
                    ($('#rete_ica_assumed').is(':checked') ? 0 : formatDecimal($('#rete_autoaviso_valor').val())) + 
                    ($('#rete_otros_assumed').is(':checked') ? 0 : formatDecimal($('#rete_otros_valor').val()));
        $('#total_rete_amount').text(formatMoney(rtotal_rete));
        // if (formatDecimal(rtotal_rete) > formatDecimal(purchase.amount_to_paid)) {
        //     $('.advise_invalid_retention').css('display', '');
        //     $('#updateOrderRete').prop('disabled', true);
        // } else {
            $('.advise_invalid_retention').css('display', 'none');
            $('#updateOrderRete').prop('disabled', false);
        // }
    }

    function resetBase(apply){
        if (apply == "ST") {
            $('#rete_base_subtotal').val(0).prop('readonly', true);
        } else if (apply == "TX") {
            $('#rete_base_tax').val(0).prop('readonly', true);
        } else if (apply == "TO") {

        }
        total = formatDecimal($('#rete_base_subtotal').val()) + formatDecimal($('#rete_base_tax').val());
        $('#rete_base_total').val(total);
    }

    function setMaxBaseAllowed(apply, rete, item_id){
        var purchases = JSON.parse(localStorage.getItem('purchases'));
        var purchase = purchases[item_id];
        var base_paid = 0;
        var total = 0;
        if (rete == 'fuente') {
            base_paid = purchase.rete_fuente_base_paid;
        } else if (rete == 'iva') {
            base_paid = purchase.rete_iva_base_paid;
        } else if (rete == 'ica') {
            base_paid = purchase.rete_ica_base_paid;
        } else if (rete == 'other') {
            base_paid = purchase.rete_other_base_paid;
        }
        if (base_paid > 0 && !isToastShowing) {
            command: toastr.warning('La compra ya tiene retenciones aplicadas, por favor verifique', 'Retenciones ya aplicadas', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
        }
        if (apply == "ST") {
            max_val = purchase.total - base_paid;
            $('#rete_base_subtotal').prop('max', max_val).prop('readonly', false);
            if ($('#rete_base_subtotal').val() == 0) {
                $('#rete_base_subtotal').val(max_val);
            }
            if (max_val == 0) {
                base_subtotal = 0;
            } else if ( (formatDecimal(purchase.amount_to_paid) + formatDecimal(purchase.paid)) == formatDecimal(purchase.grand_total)) {
                base_subtotal = max_val;
            } else {
                base_subtotal = ((purchase.amount_to_paid / purchase.grand_total) * purchase.total);
            }
            $('#suggested_subtotal').html(formatMoney(base_subtotal));
        } else if (apply == "TX") {
            max_val = purchase.total_tax - base_paid;
            $('#rete_base_tax').prop('max', max_val).prop('readonly', false);
            if ($('#rete_base_tax').val() == 0) {
                $('#rete_base_tax').val(max_val);
            }
            if (max_val == 0) {
                base_iva = 0;
            } else if (formatDecimal(purchase.amount_to_paid + purchase.paid) == formatDecimal(purchase.grand_total)) {
                base_iva = max_val;
            } else {
                base_iva = ((purchase.amount_to_paid / purchase.grand_total) * purchase.total_tax);
            }
            $('#suggested_tax').html(formatMoney(base_iva));
        } else if (apply == "TO") {
            if ($('#rete_base_subtotal').prop('readonly')) {
                max_val = purchase.total - base_paid;
                $('#rete_base_subtotal').prop('max', max_val).prop('readonly', false);
                if ($('#rete_base_subtotal').val() == 0) {
                    $('#rete_base_subtotal').val(max_val);
                }
                if (max_val == 0) {
                    base_subtotal = 0;
                } else if ( (formatDecimal(purchase.amount_to_paid) + formatDecimal(purchase.paid)) == formatDecimal(purchase.grand_total)) {
                    base_subtotal = max_val;
                } else {
                    base_subtotal = ((purchase.amount_to_paid / purchase.grand_total) * purchase.total);
                }
                $('#suggested_subtotal').html(formatMoney(base_subtotal));
            }

            if ($('#rete_base_subtotal').prop('readonly')) {
                max_val = purchase.total_tax - base_paid;
                $('#rete_base_tax').prop('max', max_val).prop('readonly', false);
                if ($('#rete_base_tax').val() == 0) {
                    $('#rete_base_tax').val(max_val);
                }
                if (max_val == 0) {
                    base_iva = 0;
                } else if (formatDecimal(purchase.amount_to_paid + purchase.paid) == formatDecimal(purchase.grand_total)) {
                    base_iva = max_val;
                } else {
                    base_iva = ((purchase.amount_to_paid / purchase.grand_total) * purchase.total_tax);
                }
                $('#suggested_tax').html(formatMoney(base_iva));
            }
            base_total = formatDecimal($('#suggested_subtotal').html()) + formatDecimal($('#suggested_tax').html());
            $('#suggested_total').html(formatMoney(base_total));
        }
        total = formatDecimal($('#rete_base_subtotal').val()) + formatDecimal($('#rete_base_tax').val());
        //console.log("Apply : "+ apply + " total : "+total+" Max total : "+max_val);
        $('#rete_base_total').val(total);
    }

    $(document).on('keyup', '#rete_base_subtotal, #rete_base_tax, #rete_base_total', function(){
        var purchases = JSON.parse(localStorage.getItem('purchases'));
        var purchase = purchases[$('#rete_applied').val()];
        var rete_base = $(this);
        var val = formatDecimal(rete_base.val());
        var max = formatDecimal(rete_base.prop('max'));
        var min = formatDecimal(rete_base.prop('min') ? rete_base.prop('min') : 0);
        if (purchase.amount_to_paid == purchase.grand_total) {
            rete_base.val(max);
            Command: toastr.error('No es posible modificar la base por que se está pagando el total de la factura', 'Error', {onHidden : function(){}})
        } else {
            if (val < min || val > max) {
                rete_base.val(max);
            }
            debbug_retenciones();
        }
    });

    $(document).on('focus', '#rete_base_subtotal, #rete_base_tax, #rete_base_total', function(){
        $(this).select();
    });

    $(document).on('click', '#updateOrderRete', function () {

        $('#ajaxCall').fadeIn();
        itemid = $('#rete_applied').val();
        purchases = JSON.parse(localStorage.getItem('purchases'));
        purchases[itemid].rete_fuente_percentage = $('#rete_fuente_tax').val();
        purchases[itemid].rete_ica_percentage = $('#rete_ica_tax').val();
        purchases[itemid].rete_bomberil_percentage = $('#rete_bomberil_tax').val();
        purchases[itemid].rete_autoaviso_percentage = $('#rete_autoaviso_tax').val();
        purchases[itemid].rete_iva_percentage = $('#rete_iva_tax').val();
        purchases[itemid].rete_other_percentage = $('#rete_otros_tax').val();

        purchases[itemid].rete_fuente_base = $('#rete_fuente_base').val();
        purchases[itemid].rete_ica_base = $('#rete_ica_base').val();
        purchases[itemid].rete_bomberil_base = $('#rete_bomberil_base').val();
        purchases[itemid].rete_autoaviso_base = $('#rete_autoaviso_base').val();
        purchases[itemid].rete_iva_base = $('#rete_iva_base').val();
        purchases[itemid].rete_other_base = $('#rete_otros_base').val();

        purchases[itemid].rete_fuente_total = $('#rete_fuente_valor').val();
        purchases[itemid].rete_ica_total = $('#rete_ica_valor').val();
        purchases[itemid].rete_bomberil_total = $('#rete_bomberil_valor').val();
        purchases[itemid].rete_autoaviso_total = $('#rete_autoaviso_valor').val();
        purchases[itemid].rete_iva_total = $('#rete_iva_valor').val();
        purchases[itemid].rete_other_total = $('#rete_otros_valor').val();

        purchases[itemid].rete_fuente_account = $('#rete_fuente_account').val();
        purchases[itemid].rete_ica_account = $('#rete_ica_account').val();
        purchases[itemid].rete_bomberil_account = $('#rete_bomberil_account').val();
        purchases[itemid].rete_autoaviso_account = $('#rete_autoaviso_account').val();
        purchases[itemid].rete_iva_account = $('#rete_iva_account').val();
        purchases[itemid].rete_other_account = $('#rete_otros_account').val();

        purchases[itemid].fuente_option = $('#rete_fuente_option option:selected').data('percentage');
        purchases[itemid].iva_option = $('#rete_iva_option option:selected').data('percentage');
        purchases[itemid].ica_option = $('#rete_ica_option option:selected').data('percentage');
        purchases[itemid].bomberil_option = $('#rete_bomberil_option option:selected').data('percentage');
        purchases[itemid].autoaviso_option = $('#rete_autoaviso_option option:selected').data('percentage');
        purchases[itemid].otros_option = $('#rete_otros_option option:selected').data('percentage');

        purchases[itemid].id_rete_fuente = $('#rete_fuente_option').val();
        purchases[itemid].id_rete_iva = $('#rete_iva_option').val();
        purchases[itemid].id_rete_ica = $('#rete_ica_option').val();
        purchases[itemid].id_rete_bomberil = $('#rete_bomberil_option').val();
        purchases[itemid].id_rete_autoaviso = $('#rete_autoaviso_option').val();
        purchases[itemid].id_rete_otros = $('#rete_otros_option').val();

        purchases[itemid].rete_fuente_assumed = $('#rete_fuente_assumed').is(':checked');
        purchases[itemid].rete_iva_assumed = $('#rete_iva_assumed').is(':checked');
        purchases[itemid].rete_ica_assumed = $('#rete_ica_assumed').is(':checked');
        purchases[itemid].rete_other_assumed = $('#rete_otros_assumed').is(':checked');

        purchases[itemid].rete_fuente_assumed_account = $('#rete_fuente_option option:selected').data('assumedaccount');
        purchases[itemid].rete_iva_assumed_account = $('#rete_iva_option option:selected').data('assumedaccount');
        purchases[itemid].rete_ica_assumed_account = $('#rete_ica_option option:selected').data('assumedaccount');
        purchases[itemid].rete_bomberil_assumed_account = $('#rete_bomberil_option option:selected').data('assumedaccount');
        purchases[itemid].rete_autoaviso_assumed_account = $('#rete_autoaviso_option option:selected').data('assumedaccount');
        purchases[itemid].rete_other_assumed_account = $('#rete_otros_option option:selected').data('assumedaccount');

        purchases[itemid].total_rete_amount = $('#total_rete_amount').text();

        var discount_apply_to = [];
        var discount_amount = [];
        var discount_amount_calculated = [];
        var discount_ledger_id = [];
        $('select.discount_apply_to').each(function(index, select){
            index_d = $(select).data('index');
            d_discount_apply_to = $(select).val();
            d_discount_amount = $('.discount_amount[data-index="'+index_d+'"]').val();
            d_discount_amount_calculated = $('.discount_amount_calculated[data-index="'+index_d+'"]').val();
            d_discount_ledger_id = $(select).find('option:selected').data('ledgerid');
            if (d_discount_amount_calculated > 0) {
                discount_apply_to.push(d_discount_apply_to);
                discount_amount.push(d_discount_amount);
                discount_amount_calculated.push(d_discount_amount_calculated);
                discount_ledger_id.push(d_discount_ledger_id);
            }
        });

        if (discount_apply_to.length > 0 || discount_amount.length > 0 || discount_amount_calculated.length > 0 || discount_ledger_id.length > 0 ) {
            purchases[itemid].discount = {discount_apply_to, discount_amount, discount_amount_calculated, discount_ledger_id};
        } else {
            purchases[itemid].discount = false;
        }

        localStorage.removeItem('purchases');
        localStorage.setItem('purchases', JSON.stringify(purchases));

        loadItems();

        reset_retention_window();

        $('#rtModal').modal('hide');
        $('#ajaxCall').fadeOut();
    });

     $(document).on('click', '#cancelOrderRete', function(){
        // localStorage.removeItem('retenciones');
        // loadItems();
        reset_retention_window();
        $('#updateOrderRete').trigger('click');
     });

    function recalcular_retenciones(retenciones){
        console.log(retenciones);
        if (retenciones != null) {
            if (retenciones.id_rete_fuente > 0) {
                if (!$('#rete_fuente').is(':checked')) {
                    $('#rete_fuente').attr('data-selected', retenciones.id_rete_fuente).trigger('click');
                }
            }
            if (retenciones.id_rete_iva > 0) {
                if (!$('#rete_iva').is(':checked')) {
                    $('#rete_iva').attr('data-selected', retenciones.id_rete_iva).trigger('click');
                }
            }
            if (retenciones.id_rete_ica > 0) {
                if (!$('#rete_ica').is(':checked')) {
                    $('#rete_ica').attr('data-selected', retenciones.id_rete_ica).trigger('click');
                }
            }
            if (retenciones.id_rete_otros > 0) {
                if (!$('#rete_otros').is(':checked')) {
                    $('#rete_otros').attr('data-selected', retenciones.id_rete_otros).trigger('click');
                }
            }

            setTimeout(function() {
                $('#rete_autoaviso').attr('disabled', false);
                $('#rete_bomberil').attr('disabled', false);
                if (retenciones.id_rete_bomberil > 0) {
                    if (!$('#rete_bomberil').is(':checked')) {
                        $('#rete_bomberil').attr('data-selected', retenciones.id_rete_bomberil).trigger('click');
                    }
                }
                if (retenciones.id_rete_autoaviso > 0) {
                    if (!$('#rete_autoaviso').is(':checked')) {
                        $('#rete_autoaviso').attr('data-selected', retenciones.id_rete_autoaviso).trigger('click');
                    }
                }
            }, 1500);
            if (retenciones.rete_fuente_assumed != false) {
                if (!$('#rete_fuente_assumed').is(':checked')) {
                    $('#rete_fuente_assumed').trigger('click');
                }
            }
            if (retenciones.rete_iva_assumed != false) {
                if (!$('#rete_iva_assumed').is(':checked')) {
                    $('#rete_iva_assumed').trigger('click');
                }
            }
            if (retenciones.rete_ica_assumed != false) {
                if (!$('#rete_ica_assumed').is(':checked')) {
                    $('#rete_ica_assumed').trigger('click');
                }
            }
            if (retenciones.rete_other_assumed != false) {
                if (!$('#rete_otros_assumed').is(':checked')) {
                    $('#rete_otros_assumed').trigger('click');
                }
            }
        }
    }


    function debbug_retenciones(){

        if ($('#rete_fuente').is(':checked')) {
            $('#rete_fuente_option').trigger('change');
        }

        if ($('#rete_iva').is(':checked')) {
            $('#rete_iva_option').trigger('change');
        }

        if ($('#rete_ica').is(':checked')) {
            $('#rete_ica_option').trigger('change');
        }

        if ($('#rete_otros').is(':checked')) {
            $('#rete_otros_option').trigger('change');
        }

    }

    function reiniciar_retencion(row_id, by_checkbox = false){

        var purchases = JSON.parse(localStorage.getItem('purchases'));

        delete purchases[row_id].total_rete_amount;

        delete purchases[row_id].fuente_option;
        delete purchases[row_id].iva_option;
        delete purchases[row_id].ica_option;
        delete purchases[row_id].otros_option;
        delete purchases[row_id].id_rete_fuente;
        delete purchases[row_id].id_rete_iva;
        delete purchases[row_id].id_rete_ica;
        delete purchases[row_id].id_rete_otros;

        purchases[row_id].rete_fuente_percentage = 0;
        purchases[row_id].rete_ica_percentage = 0;
        purchases[row_id].rete_bomberil_percentage = 0;
        purchases[row_id].rete_autoaviso_percentage = 0;
        purchases[row_id].rete_iva_percentage = 0;
        purchases[row_id].rete_other_percentage = 0;

        purchases[row_id].rete_fuente_base = 0;
        purchases[row_id].rete_ica_base = 0;
        purchases[row_id].rete_bomberil_base = 0;
        purchases[row_id].rete_autoaviso_base = 0;
        purchases[row_id].rete_iva_base = 0;
        purchases[row_id].rete_other_base = 0;

        purchases[row_id].rete_fuente_total = 0;
        purchases[row_id].rete_ica_total = 0;
        purchases[row_id].rete_bomberil_total = 0;
        purchases[row_id].rete_autoaviso_total = 0;
        purchases[row_id].rete_iva_total = 0;
        purchases[row_id].rete_other_total = 0;

        purchases[row_id].rete_fuente_account = 0;
        purchases[row_id].rete_ica_account = 0;
        purchases[row_id].rete_bomberil_account = 0;
        purchases[row_id].rete_autoaviso_account = 0;
        purchases[row_id].rete_iva_account = 0;
        purchases[row_id].rete_other_account = 0;

        delete purchases[row_id].discount;
        if (by_checkbox) {
            delete purchases[row_id].amount_to_paid_manually_changed;
        }
        if (purchases[row_id].amount_to_paid_manually_changed === undefined) {
            purchases[row_id].amount_to_paid = purchases[row_id].grand_total - purchases[row_id].paid;
        }
        $('#cancelOrderRete').trigger('click');

        localStorage.removeItem('purchases');
        localStorage.setItem('purchases', JSON.stringify(purchases));
        loadItems();
    }

$(document).on('click', '.add_concept', function(){
    $.ajax({
        url : site.base_url+"payments/addConcept",
        type : 'get',
        data : {
            ptype : 2
        }
    }).done(function(data){
        $(data).appendTo('#table_concepts tbody');
        $('.select2').select2('destroy');
        $('.select2').select2();
        actualizarConceptos();
        set_concepto_supplier();
    })
});

$(document).on('click', '.delete_row', function(){
    tr = $(this).closest('tr');
    tr.remove();
    actualizarConceptos();
});

$(document).on('change', '.type_mov, .concepto_amount', function(){
    var val = $(this).val();
    if (val < 0) {
        $(this).val(0);
    }
    actualizarConceptos();
});

$(document).on('keyup', '.concepto_amount', function(){
    var val = $(this).val();

    this.value = this.value.replace(/[^0-9],[^0-9]/g,'');

    if (val < 0) {
        $(this).val(0);
    }

});

function actualizarConceptos(){

    localStorage.removeItem('total_conceptos');
    var total = 0;
    $('select[name="type_mov[]"]').each(function(index){
        var type = $(this);
        var amount = $('input[name="concepto_amount[]"]').eq(index);
        if (type.val() == "+") {
            total += formatDecimal(amount.val());
        } else {
            total -= formatDecimal(amount.val());
        }
    });
    localStorage.setItem('total_conceptos', total);
    loadItems();
}

$(document).on('click', '#add_payment', function(){
    var num_checkeds = 0;
    $('.enable_payment').each(function(index, check){
        if($(check).is(':checked')){
            num_checkeds++;
        }
    });
    if (num_checkeds == 0) {
        bootbox.confirm({
            message: "Aún no ha seleccionado ninguna compra, por lo que este comprobante de egreso no afectará las cuentas por pagar",
            buttons: {
                confirm: {
                    label: 'Continuar',
                    className: 'btn-success send-submit-sale btn-full'
                },
                cancel: {
                    label: 'Cancelar',
                    className: 'btn-danger btn-full'
                }
            },
            callback: function (result) {
                if (result) {
                    var submit = true;
                    if ($('select[name="type_mov[]"]').length > 0) {
                        $('select[name="type_mov[]"]').each(function(index){
                            type_mov = $(this);
                            if (type_mov.val() == '') {
                                type_mov.select2('open');
                                submit = false;
                                return false;
                            }
                            description = $('input[name="concepto_description[]"]').eq(index);
                            if (description.val() == '') {
                                description.focus();
                                submit = false;
                                return false;
                            }
                            amount = $('input[name="concepto_amount[]"]').eq(index);
                            if (amount.val() == '') {
                                amount.focus();
                                submit = false;
                                return false;
                            }
                            if (site.settings.modulary == 1) {
                                ledger = $('select[name="ledger[]"]').eq(index);
                                if (ledger.val() == '') {
                                    ledger.select2('open');
                                    submit = false;
                                    return false;
                                }
                            }
                            if (site.settings.cost_center_selection == 0) {
                                biller_concepto = $('select[name="biller_concepto[]"]').eq(index);
                                if (biller_concepto.val() == '') {
                                    biller_concepto.select2('open');
                                    submit = false;
                                    return false;
                                }
                            } else if (site.settings.cost_center_selection == 1) {
                                cost_center_id_concepto = $('select[name="cost_center_id_concepto[]"]').eq(index);
                                if (cost_center_id_concepto.val() == '') {
                                    cost_center_id_concepto.select2('open');
                                    submit = false;
                                    return false;
                                }
                            }
                        });
                    }
                    if (submit && site.settings.cost_center_selection == 1) {
                        if ($('#cost_center_id').val() == '') {
                            $('#cost_center_id').select2('open');
                            submit = false;
                        }
                    }
                    if (!$('#add_multi_payment_purchases').valid()) {
                        submit = false;
                    }
                    if (submit) {
                        $('#add_multi_payment_purchases').submit();
                        $('#add_payment').prop('disabled', true);
                        $('#reset').prop('disabled', true);
                        remove_localstorage_data();
                        setTimeout(function() {
                            location.href = site.base_url+'payments/pindex';
                        }, 1300);
                    }
                }
            }
        });
    } else {
        var submit = true;
        if ($('select[name="type_mov[]"]').length > 0) {
            $('select[name="type_mov[]"]').each(function(index){
                type_mov = $(this);
                if (type_mov.val() == '') {
                    type_mov.select2('open');
                    submit = false;
                    return false;
                }
                description = $('input[name="concepto_description[]"]').eq(index);
                if (description.val() == '') {
                    description.focus();
                    submit = false;
                    return false;
                }
                amount = $('input[name="concepto_amount[]"]').eq(index);
                if (amount.val() == '') {
                    amount.focus();
                    submit = false;
                    return false;
                }
                if (site.settings.modulary == 1) {
                    ledger = $('select[name="ledger[]"]').eq(index);
                    if (ledger.val() == '') {
                        ledger.select2('open');
                        submit = false;
                        return false;
                    }
                }
                if (site.settings.cost_center_selection == 0) {
                    biller_concepto = $('select[name="biller_concepto[]"]').eq(index);
                    if (biller_concepto.val() == '') {
                        biller_concepto.select2('open');
                        submit = false;
                        return false;
                    }
                } else if (site.settings.cost_center_selection == 1) {
                    cost_center_id_concepto = $('select[name="cost_center_id_concepto[]"]').eq(index);
                    if (cost_center_id_concepto.val() == '') {
                        cost_center_id_concepto.select2('open');
                        submit = false;
                        return false;
                    }
                }
            });
        }
        if (submit && site.settings.cost_center_selection == 1) {
            if ($('#cost_center_id').val() == '') {
                $('#cost_center_id').select2('open');
                submit = false;
            }
        }
        if (!$('#add_multi_payment_purchases').valid()) {
            submit = false;
        }
        if (submit) {
            $('#add_multi_payment_purchases').submit();
            $('#add_payment').prop('disabled', true);
            $('#reset').prop('disabled', true);
            remove_localstorage_data();
            setTimeout(function() {
                location.href = site.base_url+'payments/pindex';
            }, 1300);
        }
    }
});


$(document).on('click', '#debug', function(){
    var purchases = JSON.parse(localStorage.getItem('purchases'));
    var purchases_debugged = [];
    var selected = 0;
    $.each(purchases, function(index){
        if (this.purchase_selected == true) {
            purchases_debugged.push(this);
            selected++;
        }
    });
    num_rows = max_num_rows - selected;
    if (num_rows == 0) {
        return false;
    }
    rows_start+= ((max_num_rows + 1) - selected);
    $.ajax({
        url : site.base_url+"purchases/purchase_suggestions",
        type : "GET",
        dataType: "JSON",
        data : {
            'supplier_id' :  $('#supplier').val(),
            'address_id' :  $('#address_id').val(),
            'biller_id' :  $('#rcbiller').val(),
            'rows_start' :  rows_start,
            'num_rows' :  num_rows,
            'ptype' : $('#purchase_type').val(),
        },
    }).done(function(data){
        if (data.response !== undefined && data.response == 0) {
            command: toastr.warning('Ya no se encontraron más compras pendientes para el proveedor', 'Sin resultados', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
        } else {
            $.each(data, function(index){
                purchases_debugged.push(this);
            });
        }
        setTimeout(function() {
            localStorage.removeItem('purchases');
            localStorage.setItem('purchases', JSON.stringify(purchases_debugged));
            loadItems();
        }, 850);
    }).fail(function(){
        loadItems();
    });
    
});

$(document).on('change', '.discount_apply_to', function(){
    var form_select = $(this);
    var apply_to = form_select.val();
    $('.discount_apply_to').each(function(index, select){
        if (form_select.index('.discount_apply_to') != index) {
            $.each($(select).find('option'), function(index, option){
                if ($(option).val() == apply_to && apply_to != '') {
                    $(option).prop('disabled', true);
                }
            });
        }
    });
});

$(document).on('blur, change', '.discount_amount', function(){
    var input = $(this);
    var purchases = JSON.parse(localStorage.getItem('purchases'));
    if (input.val() != '') {
        var index = input.data('index');
        var purchase_id = $('#rete_applied').val();
        var purchase = purchases[purchase_id];
        var apply_to = $('.discount_apply_to[data-index="'+index+'"]').val();
        var base_discount = getAmountToDiscount(apply_to, purchase_id);
        var item_discount = 0;
        var ds = input.val();
        if (ds.indexOf("%") !== -1) {
            var pds = ds.split("%");
            if (!isNaN(pds[0])) {
                item_discount = formatDecimal(parseFloat(((base_discount) * parseFloat(pds[0])) / 100));
            } else {
                item_discount = parseFloat(ds);
            }
        } else {
            item_discount = parseFloat(ds);
        }
        $('.discount_amount_calculated[data-index="'+index+'"]').val(formatDecimals(item_discount));
        set_total_calculated_discount(purchase);
    }
});

function getAmountToDiscount(apply, item_id){
    var purchases = JSON.parse(localStorage.getItem('purchases'));
    var purchase = purchases[item_id];
    var base_discount = 0;
    if (apply == "ST") {
        base_discount = parseFloat(purchase.total) + parseFloat(purchase.total_discount);
    } else if (apply == "TX") {
        // base_discount = ((purchase.amount_to_paid / purchase.grand_total) * purchase.total_tax);
        base_discount = purchase.total_tax;
    } else if (apply == "TO") {
        base_discount = purchase.grand_total;
    } else if (apply == "PST") {
        base_discount = ((purchase.amount_to_paid / purchase.grand_total) * purchase.total);
    } else if (apply == "PTO") {
        base_discount = ((purchase.amount_to_paid / purchase.grand_total) * purchase.grand_total);
    }
    return base_discount;
}

function recalcular_descuento(discount){
    arr_apply = discount.discount_apply_to;
    arr_amount = discount.discount_amount;
    arr_calculated = discount.discount_amount_calculated;
    $.each(arr_apply, function(index, val){
        index_d = index+1;
        apply_to = val;
        amount = arr_amount[index];
        $('.discount_apply_to[data-index="'+index_d+'"]').select2('val', apply_to).trigger('change');
        $('.discount_amount[data-index="'+index_d+'"]').val(amount).trigger('change');
    });
}

function reset_retention_window(){
    $('#rete_fuente').prop('checked', true).trigger('click');
    $('#rete_iva').prop('checked', true).trigger('click');
    $('#rete_ica').prop('checked', true).trigger('click');
    $('#rete_otros').prop('checked', true).trigger('click');


    $('#rete_fuente_assumed').prop('checked', true).trigger('click');
    $('#rete_iva_assumed').prop('checked', true).trigger('click');
    $('#rete_ica_assumed').prop('checked', true).trigger('click');
    $('#rete_otros_assumed').prop('checked', true).trigger('click');

    $('.discount_apply_to').each(function(index, select){
        $(select).find('option').prop('disabled', false);
    });
    $('.discount_apply_to').select2('val', '').trigger('change');
    $('.discount_amount').val(0).trigger('change');
    $('#total_discount').val(0);
    $('#updateOrderRete').prop('disabled', false);
}

function set_total_calculated_discount(purchase){
    var total = 0;
    $('#updateOrderRete').prop('disabled', true);
    $('.discount_amount_calculated').each(function(index, input){
        if ($(input).val() > 0) {
            total += parseFloat($(input).val());
        }
    });
        if (total > 0) {
            // if (total <= purchase.amount_to_paid) {
                $('#total_discount').val(formatDecimals(total));
                $('#updateOrderRete').prop('disabled', false);
            // } else {
            //     command: toastr.warning('El total de los descuentos es mayor al valor pagado', 'Inconsistencia en descuentos', {
            //         "showDuration": "500",
            //         "hideDuration": "1000",
            //         "timeOut": "6000",
            //         "extendedTimeOut": "1000",
            //     });
            // }
        }
}


$('#payment_date').on('change', function(){
    if ($(this).val() != '') {
        localStorage.setItem('payment_date', $(this).val());
    }
});

$('#consecutive_payment').on('change', function(){
    if ($(this).val() != '') {
        localStorage.setItem('consecutive_payment', $(this).val());
    }
});


$(document).on('change', '#rcbiller', function(){
    localStorage.setItem('rcbiller', $(this).val());
    if ($('#purchase_type').val() == 2) {
        var document_type = 24;
    } else {
        var document_type = 17;
    }
  $.ajax({
    url: site.base_url+'billers/getBillersDocumentTypes/'+document_type+'/'+$('#rcbiller').val(),
    type:'get',
    dataType:'JSON'
  }).done(function(data){
    response = data;
    $('#document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
    if (response.status == 0) {
      $('.repurchasert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
    }
      $('#document_type_id').trigger('change');
  });
  $.ajax({
    url: site.base_url+'billers/getBillersDocumentTypes/15/'+$('#rcbiller').val(),
    type:'get',
    dataType:'JSON'
  }).done(function(data){
    response = data;
    $('#deposit_document_type_id').html(response.options).select2();
    if (response.not_parametrized != "") {
        command: toastr.warning('Los documentos <b> ('+response.not_parametrized+') no están parametrizados </b> en contabilidad', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    }
    if (response.status == 0) {
      $('.repurchasert').html("<div class='panel panel-warning alertResolucion'><div class='panel-heading'><button type='button' class='close fa-2x' data-dismiss='alert'>&times;</button><?= lang('biller_without_documents_types') ?></div></div>").css('display', '');
    }
    $('#deposit_document_type_id').trigger('change');
  });
  if ($('#supplier').val() && purchase_id == false) {
    $('#supplier').trigger('change');
  }
});


$(document).on('ifChecked', '.set_deposit_on', function(){
    $('.div_deposit_data').fadeIn();
});

$(document).on('ifUnchecked', '.set_deposit_on', function(){
    $('.div_deposit_data').fadeOut();
});

$(document).on('change', '#paid_by_1', function(){
    payment = $(this).closest('.well');
    paid_by = $(this).val();
    amount_to_pay = parseFloat(formatDecimal($('#amount_1').val()));
    supplier = $('#supplier').val();
    if (paid_by == "deposit") {
        $.ajax({
            url:  site.base_url+"suppliers/deposit_balance/"+supplier+"/"+amount_to_pay,
        }).done(function(data) {
            payment.find('.deposit_message').remove();
            payment.append(data);
        });
    }

    if (paid_by == 'cash') {
        $('#div_affects_register').css('display', '');
    } else {
        $('#div_affects_register').css('display', 'none');
    }

});

$(document).on('change', '#manual_reference', function(){
    manual_reference = $(this).val();
    prefix = $('#document_type_id option:selected').text();
    $.ajax({
        url : site.base_url+"payments/validate_payment_reference",
        type : "get",
        data : {
            "prefix" : prefix,
            "consecutive" : manual_reference,
        }
    }).done(function(data){
        if (data == 1) {
            command: toastr.error('El consecutivo '+prefix+'-'+manual_reference+' ya está registrado', 'Consecutivo duplicado', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
            $('#manual_reference').val('').focus();
        }
    });
});

$(document).on('click', '#rete_fuente_assumed, #rete_iva_assumed, #rete_ica_assumed, #rete_otros_assumed', function(e){
    if ($(this).is(':checked')) {
        localStorage.setItem('assumed_click', 1);
    } else {
        localStorage.setItem('assumed_click', 2);
    }
    rete = $(this).prop('id').replace('_assumed', '');
    $('#'+rete+'_option').trigger('change');
});


function set_concepto_supplier(){
    $('.concepto_supplier').select2({
        minimumInputLength: 1,
        data: [],
        initSelection: function (element, callback) {
          $.ajax({
            type: "get", async: false,
            url: site.base_url+"suppliers/getsupplier/" + $(element).val(),
            dataType: "json",
            success: function (data) {
              callback(data[0]);
            }
          });
        },
        ajax: {
          url: site.base_url+"suppliers/suggestions",
          dataType: 'json',
          quietMillis: 15,
          data: function (term, page) {
            return {
              term: term,
              limit: 10,
              ptype : 7,
              support_document : 'all',
            };
          },
          results: function (data, page) {
            if (data.results != null) {
              return {results: data.results};
            } else {
              return {results: [{id: '', text: lang.no_match_found}]};
            }
          }
        }
    });
}

function reset_rete_bomberil(){
    $('#rete_bomberil').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_autoaviso').prop('disabled', true).prop('checked', false).trigger('click');
    $('#rete_bomberil_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_autoaviso_option').select2('val', '').prop('disabled', true).trigger('change');
    $('#rete_bomberil_tax').val('');
    $('#rete_bomberil_valor').val('');
    $('#rete_autoaviso_tax').val('');
    $('#rete_autoaviso_valor').val('');
}

$(document).on('change', '#date', function(){
    min_date = $(this).prop('min');
    sel_date = moment($(this).val(),'DD/MM/YYYY HH:mm:ss a').format('YYYY-MM-DD');
    if (sel_date < min_date) {
        command: toastr.error('La fecha seleccionada para el Comprobante, no puede ser menor a la de las facturas a pagar', 'Fecha CE incorrecta', {
            "showDuration": "500",
            "hideDuration": "1000",
            "timeOut": "6000",
            "extendedTimeOut": "1000",
        });
        $("#date").datetimepicker('update', new Date());
    }
});