<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->load->library('form_validation');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('products_model');
        $this->load->admin_model('dashboard_model');
    }

    public function index()
    {
        $name = 'Calendario';
        $url = 'admin/calendar';

        $dashboard = $this->site->get_dashboard_by_id($this->session->userdata('dashboard_id'));

        if (!empty($dashboard)) {
            $url = $dashboard->url;
            $name = $dashboard->name;

            if ($url == 'admin/calendar') {
                redirect($url);
            }

            $this->data['name'] = $name;
            $this->data['billers'] = $this->companies_model->getAllBillerCompanies();

            $total_customers = [];
            $customers_data = $this->companies_model->getAllCustomerCompanies();
            foreach ($customers_data as $customer_data) {
                if ($customer_data->status == ACTIVE) {
                    $total_customers[] = $customer_data;
                }
            }
            $this->data["total_customers"] = count($total_customers);

            $total_products = [];
            $products_data = $this->products_model->getAllProducts();
            foreach ($products_data as $product_data) {
                if ($product_data->discontinued != YES) {
                    $total_products[] = $product_data;
                }
            }
            $this->data["total_products"] = count($total_products);
            // $this->drop_tables_type_tables();

            $this->page_construct($url, ['page_title' => $name], $this->data);
        } else {
            redirect($url);
        }
    }

    public function update_view()
    {
    	$message = '';
    	$status = TRUE;

        $this->dashboard_model->delete_view_if_exist("sales_view");
        $this->dashboard_model->delete_table_if_exist("sales_view");
        $sale_view_created = $this->dashboard_model->create_sales_view();

        $this->dashboard_model->delete_view_if_exist("sales_customers_view");
        $this->dashboard_model->delete_table_if_exist("sales_customers_view");
        $sales_customers_view_created = $this->dashboard_model->create_sales_customers_view();

        $this->dashboard_model->delete_view_if_exist("sales_document_type_view");
        $this->dashboard_model->delete_table_if_exist("sales_document_type_view");
        $sales_document_type_view_created = $this->dashboard_model->create_sales_document_type_view();

        $this->dashboard_model->delete_view_if_exist("sales_tax_view");
        $this->dashboard_model->delete_table_if_exist("sales_tax_view");
        $sales_tax_view_created = $this->dashboard_model->create_sales_tax_view();

        $this->dashboard_model->delete_view_if_exist("sales_mean_payments_view");
        $this->dashboard_model->delete_table_if_exist("sales_mean_payments_view");
        $sales_mean_payments_view_created = $this->dashboard_model->create_sales_mean_payments_view();

        $this->dashboard_model->delete_view_if_exist("sales_products_view");
        $this->dashboard_model->delete_table_if_exist("sales_products_view");
        $sales_products_view_created = $this->dashboard_model->create_sales_products_view();

        $this->dashboard_model->delete_view_if_exist("sales_sellers_view");
        $this->dashboard_model->delete_table_if_exist("sales_sellers_view");
        $sales_sellers_view_created = $this->dashboard_model->create_sales_sellers_view();

        $this->dashboard_model->delete_view_if_exist("sales_brands_view");
        $this->dashboard_model->delete_table_if_exist("sales_brands_view");
        $sales_brands_view_created = $this->dashboard_model->create_sales_brands_view();

        $this->dashboard_model->delete_view_if_exist("sales_categories_view");
        $this->dashboard_model->delete_table_if_exist("sales_categories_view");
        $sales_categories_view_created = $this->dashboard_model->create_sales_categories_view();

        if ($sale_view_created == FALSE) { $message .= "<li> Ventas </li>"; }
        if ($sales_document_type_view_created == FALSE) { $message .= "<li> Ventas por tipos de documentos </li>"; }
        if ($sales_tax_view_created == FALSE) { $message .= "<li> Ventas por tarifa de impuestos </li>"; }
        if ($sales_mean_payments_view_created == FALSE) { $message .= "<li> Ventas por medios de pago </li>"; }
        if ($sales_customers_view_created == FALSE) { $message .= "<li> Ventas por clientes </li>"; }
        if ($sales_products_view_created == FALSE) { $message .= "<li> Ventas por productos </li>"; }
        if ($sales_sellers_view_created == FALSE) { $message .= "<li> Ventas por vendedor </li>"; }
        if ($sales_brands_view_created == FALSE) { $message .= "<li> Ventas por marca de productos </li>"; }
        if ($sales_categories_view_created == FALSE) { $message .= "<li> Ventas por categoría de productos </li>"; }

        if (!empty($message)) {
            $message = "La información de las siguientes tablas no pudde ser cargada: <ul>". $message ."</ul>";
            $status = FALSE;
        } else {
            $message = "Información actualizada correctamente";
        }

        echo json_encode(["status"=>$status, "message"=>$message]);
    }

    public function search_sales_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

        $month = $this->input->post('month');
        $quartes = $this->input->post('quarte');
        $quarte_id = $this->input->post('quarte_id');
        $branch_office = $this->input->post('branch_office');
        $previous_month = $this->input->post('previous_month');
        $previous_quarte = $this->input->post('previous_quarte');

        $months = [];
        $day_array = [];
        $tax_array = [];
        $cost_array = [];
        $total_array = [];
        $discount_array = [];
        $sub_total_array = [];
        $return_total_array = [];
        $cost_subtotal_array = [];
        $previous_sales_data = [];
        $previous_tax_array = 0;
        $previous_total_array = [];
        $previous_discount = 0;
        $previous_subtotal = 0;
        $previous_return_total = 0;
        $previous_cost_subtotal = 0;
        $amount_total_sales = 0;

        if (!empty($quartes)) {
            foreach ($quartes as $month_data) {
                $months[] = $month_data['number'];
            }
        }

        $sales_data = $this->dashboard_model->get_sales($months, $month, $branch_office);

        if ($quarte_id > 1 || $month > 1) {
            $previous_months = [];

            if (!empty($previous_quarte)) {
                foreach ($previous_quarte as $month_data) {
                    $previous_months[] = $month_data['number'];
                }
            }

            $previous_sales_data = $this->dashboard_model->get_sales($previous_months, $previous_month, $branch_office);
        }

        if (!empty($previous_sales_data)) {
            $existing_previous_day = [];
            for ($i=1; $i <= 31; $i++) {
                foreach ($previous_sales_data as $previous_sales) {
                    if (!in_array($previous_sales->day, $existing_previous_day)) {
                        if ($i == $previous_sales->day) {
                            $existing_previous_day[] = $previous_sales->day;
                            $previous_total_array[] = $previous_sales->total;
                            $previous_tax_array += $previous_sales->tax;
                            $previous_discount += $previous_sales->discount;
                            $previous_subtotal += $previous_sales->subtotal;
                            $previous_return_total += $previous_sales->return_total;
                            $previous_cost_subtotal += $previous_sales->cost_subtotal;
                        } else {
                            $previous_total_array[] = "0";
                        }
                        break;
                    }
                }
            }
        }

        $existing_day = [];
        for ($i=1; $i <= 31; $i++) {
            foreach ($sales_data as $sales) {
                if (!in_array($sales->day, $existing_day)) {
                    if ($i == $sales->day) {
                        $existing_day[] = $sales->day;
                        $total_array[] = $sales->total;
                        $cost_array[] = $sales->cost_total;
                    } else {
                        $total_array[] = "0";
                        $cost_array[] = "0";
                    }
                    break;
                }
            }
        }

        foreach ($sales_data as $sales) {
            $tax_array[] = $sales->tax;
            $discount_array[] = $sales->discount;
            $sub_total_array[] = $sales->subtotal;
            $return_total_array[] = $sales->return_total;
            $cost_subtotal_array[] = $sales->cost_subtotal;
            $amount_total_sales += $sales->amountSales;
        }

        echo json_encode([
            'tax_array'=>$tax_array,
            'cost_array'=>$cost_array,
            'total_array'=>$total_array,
            'discount_array'=>$discount_array,
            'sub_total_array'=>$sub_total_array,
            'return_total_array'=>$return_total_array,
            'cost_subtotal_array'=>$cost_subtotal_array,
            "amount_total_sales"=>$amount_total_sales,
            'previous_total_array'=>$previous_total_array,
            'previous_tax_array'=>$previous_tax_array,
            'previous_discount'=>$previous_discount,
            'previous_subtotal'=>$previous_subtotal,
            'previous_return_total'=>$previous_return_total,
            'previous_cost_subtotal'=>$previous_cost_subtotal
        ]);
    }

    public function search_sales_budget_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_budget") == FALSE) {
            echo json_encode([]);
            exit();
        }

        $month = $this->input->post('month');
        $quartes = $this->input->post('quarte');
        $quarte_id = $this->input->post('quarte_id');
        $branch_office = $this->input->post('branch_office');
        $previous_month = $this->input->post('previous_month');
        $previous_quarte = $this->input->post('previous_quarte');

        $previous_sale_budget_data = [];

        foreach ($quartes as $month_data) {
            $months[] = $month_data['number'];
        }

        $sale_budget_data = $this->dashboard_model->get_sales_budget($months, $month, $branch_office);

        if ($quarte_id > 1 || $month > 1) {
            $previous_months = [];

            if (!empty($previous_quarte)) {
                foreach ($previous_quarte as $month_data) {
                    $previous_months[] = $month_data['number'];
                }
            }

            $previous_sale_budget_data = $this->dashboard_model->get_sales_budget($previous_months, $previous_month, $branch_office);
        }

        echo json_encode([
            "sale_budget_amount"=>$sale_budget_data,
            "previous_sale_budget_data"=>$previous_sale_budget_data
        ]);
    }

    public function search_invoices_type_data()
    {

        $month = $this->input->post('month');
        $quartes = $this->input->post('quarte');
        $branch_office = $this->input->post('branch_office');

        foreach ($quartes as $month_data) {
            $months[] = $month_data['number'];
        }

        $invoices_pos = $this->dashboard_model->get_invoices_type($months, $month, $branch_office, 1);
        $invoices_pos_fe = $this->dashboard_model->get_invoices_type($months, $month, $branch_office, 1, 1);
        $invoices_detal = $this->dashboard_model->get_invoices_type($months, $month, $branch_office, 0);
        $invoices_detal_fe = $this->dashboard_model->get_invoices_type($months, $month, $branch_office, 0, 1);
        $invoices_total = $this->dashboard_model->get_invoices_type($months, $month, $branch_office, 2, 2);

        echo json_encode([
            "invoices_pos"=>$invoices_pos,
            "invoices_pos_fe"=>$invoices_pos_fe,
            "invoices_detal"=>$invoices_detal,
            "invoices_detal_fe"=>$invoices_detal_fe,
            "invoices_total"=>$invoices_total
        ]);
    }

    public function search_customers_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_customers_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

    	$month = $this->input->post("month");
        $quartes = $this->input->post("quarte");
        $branch_office = $this->input->post("branch_office");
        $customers_search_option = $this->input->post("customers_search_option");
        $limit = $this->input->post("limit");

        foreach ($quartes as $month_data) {
            $months[] = $month_data["number"];
        }

        $customers_data = $this->dashboard_model->get_customers($months, $month, $branch_office, $customers_search_option, $limit);
        $customers2_data = $this->dashboard_model->get_customers($months, $month, $branch_office, $customers_search_option, 0);

        $names_array = [];
        $total_sales_array = [];
        $number_transactions_array = [];

        if (!empty($customers_data)) {
	        foreach ($customers_data as $customer) {
        		$names_array[] = ucwords($customer->name);
        		$total_sales_array[] = $customer->subtotal;
        		$number_transactions_array[] = $customer->number_transactions;
	        }
        }

        echo json_encode([
        	"names_array"=>$names_array,
        	"total_sales_array"=>$total_sales_array,
        	"number_transactions_array"=>$number_transactions_array,
            "total_customers"=>count($customers2_data)
        ]);
    }

    public function search_products_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_products_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

    	$month = $this->input->post('month');
        $quartes = $this->input->post('quarte');
        $branch_office = $this->input->post('branch_office');
        $products_search_option = $this->input->post("products_search_option");
        $limit = $this->input->post("limit");
        $groupBy = $this->input->post("groupBy");

        foreach ($quartes as $month_data) {
            $months[] = $month_data['number'];
        }

        $products_data = $this->dashboard_model->get_products($months, $month, $branch_office, $products_search_option, $groupBy, $limit);
        $products2_data = $this->dashboard_model->get_products($months, $month, $branch_office, $products_search_option, $groupBy, 0);

        $names_array = [];
        $total_sales_array = [];
        $number_transactions_array = [];

        if (!empty($products_data)) {
	        foreach ($products_data as $product) {
        		$names_array[] = ucwords($product->name);
        		$total_sales_array[] = $product->subtotal;
        		$number_transactions_array[] = $product->number_transactions;
	        }
        }

        echo json_encode([
        	"names_array"=>$names_array,
        	"total_sales_array"=>$total_sales_array,
        	"number_transactions_array"=>$number_transactions_array,
            "total_products"=>count($products2_data)
        ]);
    }

    public function search_sellers_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_sellers_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

    	$month = $this->input->post('month');
        $quartes = $this->input->post('quarte');
        $branch_office = $this->input->post('branch_office');
        $sellers_search_option = $this->input->post("sellers_search_option");
        $limit = $this->input->post("limit");

        foreach ($quartes as $key => $month_data) {
            $months[] = $month_data['number'];
        }

        $sellers_data = $this->dashboard_model->get_sellers($months, $month, $branch_office, $sellers_search_option, $limit);

        $names_array = [];
        $total_sales_array = [];
        $number_transactions_array = [];

        if (!empty($sellers_data)) {
	        foreach ($sellers_data as $key => $sellers) {
        		$names_array[] = ucwords($sellers->name);
        		$total_sales_array[] = $sellers->subtotal;
        		$number_transactions_array[] = $sellers->number_transactions;
	        }
        }

        echo json_encode([
        	"names_array"=>$names_array,
        	"total_sales_array"=>$total_sales_array,
        	"number_transactions_array"=>$number_transactions_array
        ]);
    }

    public function search_brands_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_brands_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

    	$month = $this->input->post('month');
        $quartes = $this->input->post('quarte');
        $branch_office = $this->input->post('branch_office');
        $brands_search_option = $this->input->post('brands_search_option');
        $limit = $this->input->post('limit');

        foreach ($quartes as $key => $month_data) {
            $months[] = $month_data['number'];
        }

        $brands_data = $this->dashboard_model->get_brands($months, $month, $branch_office, $brands_search_option, $limit);

        $names_array = [];
        $total_sales_array = [];
        $number_transactions_array = [];

        if (!empty($brands_data)) {
	        foreach ($brands_data as $key => $brand) {
        		$names_array[] = ucwords($brand->name);
        		$total_sales_array[] = $brand->subtotal;
        		$number_transactions_array[] = $brand->number_transactions;
	        }
        }

        echo json_encode([
        	"names_array"=>$names_array,
        	"total_sales_array"=>$total_sales_array,
        	"number_transactions_array"=>$number_transactions_array
        ]);
    }

    public function search_categories_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_categories_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

        $month = $this->input->post("month");
        $quartes = $this->input->post("quarte");
        $branch_office = $this->input->post("branch_office");
        $categories_search_option = $this->input->post("categories_search_option");
        $limit = $this->input->post("limit");

        foreach ($quartes as $key => $month_data) {
            $months[] = $month_data["number"];
        }

        $categories_data = $this->dashboard_model->get_categories($months, $month, $branch_office, $categories_search_option, $limit);

        $names_array = [];
        $total_sales_array = [];
        $number_transactions_array = [];

        if (!empty($categories_data)) {
            foreach ($categories_data as $key => $category) {
                $names_array[] = ucwords($category->name);
                $total_sales_array[] = $category->subtotal;
                $number_transactions_array[] = $category->number_transactions;
            }
        }

        echo json_encode([
            "names_array"=>$names_array,
            "total_sales_array"=>$total_sales_array,
            "number_transactions_array"=>$number_transactions_array
        ]);
    }

    public function search_taxes_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_tax_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

    	$month = $this->input->post('month');
        $quartes = $this->input->post('quarte');
        $branch_office = $this->input->post('branch_office');

        foreach ($quartes as $month_data) {
            $months[] = $month_data['number'];
        }

        $taxes_data = $this->dashboard_model->get_tax($months, $month, $branch_office);
        $names_array = [];
        $total_sales_array = [];

        if (!empty($taxes_data)) {
	        foreach ($taxes_data as $tax) {
        		$names_array[] = ucwords($tax->tax_name);
    			$total_sales_array[] = $tax->total_tax;
	        }
        }

        echo json_encode([
        	"names_array"=>$names_array,
        	"total_sales_array"=>$total_sales_array,
        ]);
    }

    public function search_payment_means_data()
    {
        if ($this->dashboard_model->check_view_exist("sales_mean_payments_view") == FALSE) {
            echo json_encode([]);
            exit();
        }

    	$month = $this->input->post('month');
    	$quartes = $this->input->post('quarte');
        $branch_office = $this->input->post('branch_office');

        foreach ($quartes as $key => $month_data) {
            $months[] = $month_data['number'];
        }

        $categories_data = $this->dashboard_model->get_payment_means($months, $month, $branch_office);

        $names_array = [];
        $total_sales_array = [];

        if (!empty($categories_data)) {
	        foreach ($categories_data as $key => $category) {
        		$names_array[] = ucwords($category->name);
        		$total_sales_array[] = $category->subtotal;
	        }
        }

        echo json_encode([
        	"names_array"=>$names_array,
        	"total_sales_array"=>$total_sales_array
        ]);
    }
}