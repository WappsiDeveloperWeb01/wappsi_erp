<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12 table-responsive">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="pull-right dropdown">
                                        <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                        <ul class="dropdown-menu m-t-xs" role="menu" aria-labelledby="dLabel">
                                            <li>
                                                <a onclick="$('#form-price').submit();"><?= lang('submit') ?></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <?php
                            echo admin_form_open_multipart("system_settings/update_price_margin", ['id' => 'form-price']);
                            ?>
                            <div class="row">
                                <div class="col-sm-4">
                                    <?= lang('cost_type', 'cost_type') ?>
                                    <select name="cost_type" id="cost_type" class="form-control">
                                        <option value="1"><?= lang('product_cost') ?></option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <?= lang('price_group_type', 'price_group_type') ?>
                                    <select name="price_group_type" id="price_group_type" class="form-control">
                                        <option value="1"><?= lang('price_group_base') ?></option>
                                    </select>
                                </div>

                                <div class="col-sm-4">
                                    <?= lang('price_rounding', 'price_rounding') ?>
                                    <select name="price_rounding" id="price_rounding" class="form-control">
                                        <option value=""><?= lang('no_rounding') ?></option>
                                        <option value="1"><?= lang('price_rounding_integer') ?></option>
                                        <option value="2"><?= lang('price_rounding_50') ?></option>
                                        <option value="3"><?= lang('price_rounding_100') ?></option>
                                        <option value="4"><?= lang('price_rounding_1000') ?></option>
                                        <option value="5"><?= lang('price_rounding_10000') ?></option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <div class="row">


                                <div class="col-lg-12">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><input class="checkbox checkth" type="checkbox" name="check"/></th>
                                                <th><?= lang('name') ?></th>
                                                <th><?= lang('type') ?></th>
                                                <th><?= lang('profitability_margin_price_base') ?></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($categories): ?>
                                                <?php foreach ($categories as $cat): ?>
                                                    <tr>
                                                        <td><input class="checkbox multi-select input-xs category" type="checkbox" name="category[]" value="<?= $cat->id ?>" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;" <?= $cat->profitability_margin == 0 ? 'disabled="disabled"' : '' ?>></td>
                                                        <th><?= $cat->name ?></th>
                                                        <th><?= lang('category') ?></th>
                                                        <th><?= $cat->profitability_margin ?></th>
                                                    </tr>
                                                    <?php if ($subcategories[$cat->id]): ?>
                                                        <?php foreach ($subcategories[$cat->id] as $subcat): ?>
                                                            <tr>
                                                                <td><input class="checkbox multi-select input-xs subcategory" data-category="<?= $cat->id ?>" type="checkbox" name="subcategory[<?= $cat->id ?>][]" value="<?= $subcat->id ?>" style="position: absolute; top: -20%; left: -20%; display: block; width: 140%; height: 140%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;" <?= $subcat->profitability_margin == 0 ? 'disabled="disabled"' : '' ?>></td>
                                                                <td><?= $subcat->name ?></td>
                                                                <td><?= lang('subcategory') ?></td>
                                                                <td><?= $subcat->profitability_margin ?></td>
                                                            </tr>
                                                        <?php endforeach ?>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <?php echo form_close(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        setTimeout(function() {
            $('#cost_type').select2('readonly', true);
            $('#price_group_type').select2('readonly', true);
        }, 1200);

        $('.category').on('ifChecked', function(){
            console.log('entra');
            var category = $(this).val();
            $('.subcategory').each(function(index, subcategory){
                if ($(subcategory).data('category') == category) {
                    $(subcategory).iCheck('check');
                }
            });
        });
        $('.category').on('ifUnchecked', function(){
            var category = $(this).val();
            $('.subcategory').each(function(index, subcategory){
                if ($(subcategory).data('category') == category) {
                    $(subcategory).iCheck('uncheck');
                }
            });
        });
    });
</script>