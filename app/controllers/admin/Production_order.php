<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*

    1. Orden de producción
    2. Orden de corte
    3. Orden de ensamble
    4. Orden de empaque

Cada siguiente paso depende del anterior, si no existe una orden del anterior paso, el paso actual no se puede realizar.

*/
class Production_order extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }
        $this->lang->admin_load('products', $this->Settings->user_language);
        $this->load->library('form_validation');
        $this->load->admin_model('settings_model');
        $this->load->admin_model('companies_model');
        $this->load->admin_model('production_order_model');
        $this->load->admin_model('products_model');
        $this->digital_upload_path = 'files/';
        $this->upload_path = 'assets/uploads/';
        $this->thumbs_path = 'assets/uploads/thumbs/';
        $this->image_types = 'gif|jpg|jpeg|png|tif';
        $this->digital_file_types = 'zip|psd|ai|rar|pdf|doc|docx|xls|xlsx|ppt|pptx|gif|jpg|jpeg|png|tif|txt';
        $this->allowed_file_size = '1024';
        $this->popup_attributes = array('width' => '900', 'height' => '600', 'window_name' => 'popup', 'menubar' => 'yes', 'scrollbars' => 'yes', 'status' => 'no', 'resizable' => 'yes', 'screenx' => '0', 'screeny' => '0');
    }

    function index($warehouse_id = NULL)
    {
        $this->sma->checkPermissions();

        if ($this->Owner || $this->Admin || !$this->session->userdata('warehouse_id')) {
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['warehouse'] = $warehouse_id ? $this->site->getWarehouseByID($warehouse_id) : null;
        } else {
            $this->data['warehouses'] = null;
            $this->data['warehouse'] = $this->session->userdata('warehouse_id') ? $this->site->getWarehouseByID($this->session->userdata('warehouse_id')) : null;
        }

        $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
        $this->data['employees'] = $this->site->getAllCompaniesWithState('employee');
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('making_order')));
        $meta = array('page_title' => lang('making_order'), 'bc' => $bc);
        $this->page_construct('production_order/index', $meta, $this->data);
    }

    public function getProductionOrders($warehouse_id = NULL)
    {
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date')." 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:00" : NULL;
        $employee = $this->input->post('employee') ? $this->input->post('employee') : NULL;
        $biller = $this->input->post('biller') ? $this->input->post('biller') : NULL;
        $product = $this->input->post('product') ? $this->input->post('product') : NULL;
        $only_cutting_pending = $this->input->post('only_cutting_pending') ? $this->input->post('only_cutting_pending') : NULL;
        $this->load->library('datatables');
        $today_date = date('Y-m-d H:i:s');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('production_order')}.id AS id,
                        date,
                        DATEDIFF('{$today_date}', date) AS expiration,
                        reference_no,
                        employee.name AS employee_name,
                        pfinished_product.code AS product_code,
                        CONCAT(por_pfinished.quantity, '_', SUM({$this->db->dbprefix('production_order_detail')}.quantity)) AS quantity,
                        CONCAT(ciq_pfinished_complete_cutting.complete_cutting_quantity, '_', cutting.cutting_quantity) AS cutting_quantity,

                        IF(por_pfinished.quantity = ciq_pfinished_complete_cutting.complete_cutting_quantity, 'completed', 
                            IF(ciq_pfinished_complete_cutting.complete_cutting_quantity > 0, 'in_process', 'pending')
                        ) as production_order_cutted_status,

                        CONCAT(ciq_pfinished_complete_assemble.complete_assembling_quantity, '_', (assemble.assembling_quantity - assemble.finished_quantity  - assemble.fault_quantity)) AS assembling_quantity,
                        CONCAT(ciq_pfinished_complete_assemble.complete_assembling_quantity_completed, '_', assemble.finished_quantity) AS assembling_finished_quantity,
                        CONCAT(ciq_pfinished_complete_packing.complete_packing_quantity, '_', (packing.packing_quantity - packing.finished_quantity  - packing.fault_quantity)) AS packing_quantity,
                        CONCAT(ciq_pfinished_complete_packing.complete_packing_quantity_completed, '_', packing.finished_quantity) AS packing_finished_quantity,
                        {$this->db->dbprefix('production_order')}.status,
                        IF({$this->db->dbprefix('production_order_notices')}.id IS NOT NULL, CONCAT('fa-bell', '_', {$this->db->dbprefix('production_order')}.id, '_', 1, '_', {$this->db->dbprefix('production_order_notices')}.id), '') as notification_icon,
                        IF({$this->db->dbprefix('production_order_notices')}.id IS NOT NULL, 'display:none;', '') AS
                        por_notif_style
                    ")
            ->from("production_order")
            ->join("{$this->db->dbprefix('production_order_detail')} AS por_pfinished", "por_pfinished.production_order_id = {$this->db->dbprefix('production_order')}.id AND por_pfinished.production_order_pfinished_item_id IS NULL", "inner")
            ->join("{$this->db->dbprefix('production_order_detail')}", "{$this->db->dbprefix('production_order_detail')}.production_order_id = {$this->db->dbprefix('production_order')}.id AND {$this->db->dbprefix('production_order_detail')}.production_order_pfinished_item_id IS NOT NULL", "inner")
            ->join("{$this->db->dbprefix('products')} pfinished_product", "pfinished_product.id = por_pfinished.product_id", "inner")
            ->join("{$this->db->dbprefix('products')}", "{$this->db->dbprefix('products')}.id = {$this->db->dbprefix('production_order_detail')}.product_id", "inner")
            ->join("{$this->db->dbprefix('companies')} AS employee", "employee.id = {$this->db->dbprefix('production_order')}.employee_id", "inner")
            ->join("{$this->db->dbprefix('production_order_notices')}", "production_order_notices.register_id = {$this->db->dbprefix('production_order')}.id AND production_order_notices.register_type = 1 AND production_order_notices.status = 1", "left")
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(cutting_composition_complete_quantity)) AS complete_cutting_quantity,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.cutting_quantity) AS pieces_cutting_quantity,
                                (SUM(CD.cutting_quantity) / CI.quantity) AS cutting_composition_complete_quantity
                        FROM
                            {$this->db->dbprefix('cutting_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS cutting_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_cutting", "ciq_pfinished_complete_cutting.production_order_pfinished_item_id = por_pfinished.id", "left")
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity)) AS complete_assembling_quantity,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity_completed)) AS complete_assembling_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_assembling_quantity,
                                (SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS assemble_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_assembling_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS assemble_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS assemble_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_assemble", "ciq_pfinished_complete_assemble.production_order_pfinished_item_id = por_pfinished.id", "left")
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            MIN(packing_composition_complete_quantity) AS complete_packing_quantity,
                            MIN(packing_composition_complete_quantity_completed) AS complete_packing_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_packing_quantity,
                                (SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS packing_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_packing_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS packing_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('packing_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY POD.id) AS packing_table
                        GROUP BY production_order_pfinished_item_id) ciq_pfinished_complete_packing", "ciq_pfinished_complete_packing.production_order_pfinished_item_id = por_pfinished.id", "left")
            ->join("(SELECT 
                            pfinished_POD.id as pfinished_POD_id,
                            SUM(CD.cutting_quantity) as cutting_quantity,
                            COUNT( DISTINCT CD.product_id) AS cutting_comp_qty
                        FROM
                            {$this->db->dbprefix('cutting_detail')} CD
                                INNER JOIN
                            {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                                INNER JOIN
                            {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD_id
                        ) cutting", "cutting.pfinished_POD_id = por_pfinished.id", "left")
            ->join("(SELECT 
                            pfinished_POD.id as pfinished_POD_id,
                            SUM(CD.assembling_quantity) AS assembling_quantity,
                            SUM(CD.finished_quantity) AS finished_quantity,
                            SUM(CD.fault_quantity) AS fault_quantity,
                            COUNT( DISTINCT CD.product_id) AS assembling_comp_qty
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD.id) assemble", "assemble.pfinished_POD_id = por_pfinished.id", "left")
            ->join("(SELECT 
                            pfinished_POD.id as pfinished_POD_id,
                            SUM(CD.packing_quantity) AS packing_quantity,
                            SUM(CD.finished_quantity) AS finished_quantity,
                            SUM(CD.fault_quantity) AS fault_quantity,
                            COUNT( DISTINCT CD.product_id) AS packing_comp_qty
                        FROM
                            {$this->db->dbprefix('packing_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY pfinished_POD.id) packing", "packing.pfinished_POD_id = por_pfinished.id", "left")
            ;
        if ($product) {
            $this->datatables->where('pfinished_product.id', $product);
        }
        if ($warehouse_id) {
            $this->datatables->where('production_order.warehouse_id', $warehouse_id);
        }
        if ($start_date) {
            $this->datatables->where('production_order.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('production_order.date <=', $end_date);
        }
        if ($biller) {
            $this->datatables->where('production_order.biller_id', $biller);
        }
        if ($employee) {
            $this->datatables->where('production_order.employee_id', $employee);
        }


        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('production_order.created_by', $this->session->userdata('user_id'));
        }
        $this->datatables->group_by("{$this->db->dbprefix('production_order')}.id");

        if ($only_cutting_pending) {
            $this->datatables->having('production_order_cutted_status != "completed"');
        }
        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                    <div class=\"btn-group text-left\">
                                                        <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                            Acciones
                                                            <span class=\"caret\"></span>
                                                        </button>
                                                        <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                            ".(($this->Owner || $this->Admin || $this->GP['production_order-edit']) ? 
                                                                "<li>
                                                                    <a href=\"".admin_url('production_order/edit/')."$1\"><i class='fas fa-edit'></i> ".lang('edit_making_order')." </a>
                                                                </li>"
                                                                :
                                                                "")."
                                                            ".(($this->Owner || $this->Admin || $this->GP['production_order-add_cutting_order']) ? 
                                                                "<li>
                                                                    <a target='_blank' href=\"".admin_url('production_order/add_cutting_order/')."$1\"><i class='fas fa-sync-alt'></i> ".lang('add_cutting_order')." </a>
                                                                </li>"
                                                                :
                                                                "")."
                                                                <li>
                                                                    <a class='por_add_notif' data-registerid='$1' data-registertype='1' style='$3'><i class='fas fa-bell'></i> ".lang('add_new_notification')." </a>
                                                                </li>
                                                            
                                                        </ul>
                                                    </div>
                                                </div>", "id, reference_no, por_notif_style");
        $this->datatables->unset_column("por_notif_style");
        // <li>
        //     <a target='_blank' href=\"".admin_url('production_order/sync_production_order_status/')."$2\"><i class='fas fa-sync-alt'></i> ".lang('sync_production_order_status')." </a>
        // </li>
        echo $this->datatables->generate();
    }

    public function add(){
        $this->sma->checkPermissions();
        // $this->form_validation->set_rules('warehouse', lang("warehouse"), 'required');
        $this->form_validation->set_rules('employee_id', lang("employee"), 'required');
        $this->form_validation->set_rules('document_type_id', lang("document_type"), 'required');
        if ($this->form_validation->run() == true) {
            $continue = $this->input->post('continue');

            $creation_date = $this->sma->fld($this->input->post('creation_date'));
            $estimated_date = $this->sma->fld($this->input->post('estimated_date'));
            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $document_type_id = $this->input->post('document_type_id');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            for ($r = 0; $r < $i; $r++) {
                $product_id = $_POST['product_id'][$r];
                $quantity = $_POST['quantity'][$r];
                $variant = $_POST['variant'][$r];
                $products[] = array(
                        'product_id' => $product_id,
                        'quantity' => $quantity,
                        'option_id' => $variant,
                        'status' => 'pending',
                    );
            }
            $j = isset($_POST['cp_product_id']) ? sizeof($_POST['cp_product_id']) : 0;
            for ($r = 0; $r < $j; $r++) {
                $product_id = $_POST['cp_product_id'][$r];
                $quantity = $_POST['cp_quantity'][$r];
                $production_order_pfinished_item_id = $_POST['cp_production_order_pfinished_item_id'][$r];
                if ($quantity > 0) {
                    $products[] = array(
                            'production_order_pfinished_item_id' => $production_order_pfinished_item_id,
                            'product_id' => $product_id,
                            'quantity' => $quantity,
                            'status' => 'pending',
                        );
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            }
            $data = array(
                'date' => $creation_date,
                'est_date' => $estimated_date,
                // 'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'employee_id' => $this->input->post('employee_id'),
                'biller_id' => $biller_id,
                'document_type_id' => $document_type_id,
                'status' => 'pending',
                );
        }
        if ($this->form_validation->run() == true && $por_id = $this->production_order_model->addProductionOrder($data, $products)) {
            $this->session->set_userdata('remove_porls', 1);
            $this->session->set_flashdata('message', lang("making_order_added"));
            if ($continue == 1) {
                admin_redirect('production_order/add_cutting_order/'.$por_id);
            }
            admin_redirect('production_order');
        } else {
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('production_order'), 'page' => lang('making_order')), array('link' => '#', 'page' => lang('add_making_order')));
            $meta = array('page_title' => lang('add_making_order'), 'bc' => $bc);
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['employees'] = $this->site->getAllCompanies($this->Settings->companies_for_adjustments_transfers);
            $last_order = $this->production_order_model->get_last_employee_order();
            $this->data['last_employee'] = $last_order ? $last_order->employee_id : false;
            $this->page_construct('production_order/add', $meta, $this->data);
        }
    }

    public function edit($por_id){
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('employee_id', lang("employee"), 'required');
        $por = $this->production_order_model->get_production_order_by_id($por_id);
        if (!$por_id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $rows = $this->production_order_model->get_poritems($por_id);
        if ($this->form_validation->run() == true) {
            $continue = $this->input->post('continue');
            $creation_date = $this->sma->fld($this->input->post('creation_date'));
            $estimated_date = $this->sma->fld($this->input->post('estimated_date'));
            $warehouse_id = $this->input->post('warehouse');
            $biller_id = $this->input->post('biller');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $i = isset($_POST['product_id']) ? sizeof($_POST['product_id']) : 0;
            $qty_changed = false;
            for ($r = 0; $r < $i; $r++) {
                $product_id = $_POST['product_id'][$r];
                $quantity = $_POST['quantity'][$r];
                $original_quantity = $_POST['por_original_qty'][$r];
                $por_item_id = $_POST['por_item_id'][$r];
                if ($quantity != $original_quantity) {
                    $qty_changed = true;
                }
                $products[] = array(
                        'id' => $por_item_id,
                        'product_id' => $product_id,
                        'quantity' => $quantity,
                        'status' => 'pending',
                    );
            }
            $j = isset($_POST['cp_product_id']) ? sizeof($_POST['cp_product_id']) : 0;
            for ($r = 0; $r < $j; $r++) {
                $product_id = $_POST['cp_product_id'][$r];
                $quantity = $_POST['cp_quantity'][$r];
                $production_order_pfinished_item_id = $_POST['cp_production_order_pfinished_item_id'][$r];
                $por_item_id = $_POST['cp_por_item_id'][$r];
                if ($quantity > 0) {
                    $products[] = array(
                            'id' => $por_item_id,
                            'production_order_pfinished_item_id' => $production_order_pfinished_item_id,
                            'product_id' => $product_id,
                            'quantity' => $quantity,
                            'status' => 'pending',
                        );
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            }
            $data = array(
                    'id'=>$por_id,
                    'date' => $creation_date,
                    'est_date' => $estimated_date,
                    'note' => $note,
                    'created_by' => $this->session->userdata('user_id'),
                    'employee_id' => $this->input->post('employee_id'),
                    'biller_id' => $biller_id,
                    'status' => $por->status == 'completed' && $qty_changed == true ? 'in_process' : $por->status,
                );
            // $this->sma->print_arrays($data, $products);
        }
        if ($this->form_validation->run() == true && $this->production_order_model->updateProductionOrder($data, $products)) {
            $this->session->set_userdata('remove_porls', 1);
            $this->session->set_flashdata('message', lang("making_order_updated"));
            if ($continue == 1) {
                admin_redirect('production_order/add_cutting_order/'.$por_id);
            }
            admin_redirect('production_order');
        } else {
            $por_items = [];
            foreach ($rows as $item) {
                if ($item->production_order_pfinished_item_id == NULL) {
                    $item->name = $item->product_name;
                    $item->code = $item->product_code;
                    $item->qty = $item->quantity;
                    $item->option = $item->option_id > 0 ? $item->option_id : false;
                    $por_items[$item->id] = [
                                    "id" => $item->id,
                                    "item_id" => $item->product_id,
                                    "label" => $item->product_name." (".$item->product_code.")",
                                    "options" => $this->site->getProductVariants($item->product_id),
                                    "order" => 5,
                                    "row" => $item,
                                    "value" => $item->product_name." (".$item->product_code.")",
                                   ];
                }
            }

            foreach ($rows as $item) {
                if ($item->production_order_pfinished_item_id != NULL) {
                    $por_items[$item->production_order_pfinished_item_id]['composition_products'][] = [
                                    "code" => $item->product_code,
                                    "id" => $item->product_id,
                                    "por_item_id" => $item->id,
                                    "name" => $item->product_name,
                                    "price" => 0,
                                    "qty" => $item->quantity / $por_items[$item->production_order_pfinished_item_id]['row']->quantity,
                                    "quantity" => $item->quantity,
                                    "type" => $item->type,
                                   ];
                }
            }
            if ($this->Settings->item_addition == 1) {
                $por_items_2 = [];
                foreach ($por_items as $key_por_item => $por_item) {
                    $por_items_2[$por_item['item_id']] = $por_item;
                }
            }
            $this->data['inv'] = $por;
            $this->data['rows'] = json_encode(isset($por_items_2) ? $por_items_2 : $por_items);
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('production_order'), 'page' => lang('making_order')), array('link' => '#', 'page' => lang('edit_making_order')));
            $meta = array('page_title' => lang('edit_making_order'), 'bc' => $bc);
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['employees'] = $this->site->getAllCompaniesWithState('employee');
            $last_order = $this->production_order_model->get_last_employee_order();
            $this->data['last_employee'] = $last_order ? $last_order->employee_id : false;
            $this->page_construct('production_order/edit', $meta, $this->data);
        }
    }

    public function suggestions()
    {
        $term = $this->input->get('term', true);
        if (strlen($term) < 1 || !$term) {
            die("<script type='text/javascript'>setTimeout(function(){ window.top.location.href = '" . admin_url('welcome') . "'; }, 10);</script>");
        }
        $analyzed = $this->sma->analyze_term($term);
        $sr = $analyzed['term'];
        $option_id = $analyzed['option_id'];
        $rows = $this->production_order_model->getSuggestions($sr);
        if ($rows) {
            foreach ($rows as $row) {
                $row->qty = 1;
                $options = $this->products_model->getProductOptions($row->id);
                $row->option = $option_id;
                $row->serial = '';
                $row->price = $this->sma->remove_tax_from_amount($row->tax_rate, $row->price);
                $unit = $this->products_model->getUnitById($row->unit);
                $composition_products = $this->products_model->getProductComboItems($row->id);
                $row->last_cost = $row->cost;
                if ($composition_products) {
                    $total_cost = 0;
                    foreach ($composition_products as $cproduct) {
                        $total_cost += ($cproduct->price * $cproduct->qty);
                    }
                    $row->cost = $total_cost;
                }
                $c = sha1(uniqid(mt_rand(), true));
                $pr[] = array('id' => $c, 'item_id' => $row->id, 'label' => $row->name . " (" . $row->code . ")", 'row' => $row, 'options' => $options, 'composition_products' => $composition_products, 'unit' => $unit);
            }
            $this->sma->send_json($pr);
        } else {
            $this->sma->send_json(array(array('id' => 0, 'label' => lang('no_match_found'), 'value' => $term)));
        }
    }

    public function modal_view($id)
    {
        // $this->sma->checkPermissions('adjustments', TRUE);
        $por = $this->production_order_model->get_production_order_by_id($id);
        if (!$id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $por;
        $this->data['rows'] = $this->production_order_model->get_poritems($id);
        $this->data['created_by'] = $this->site->getUser($por->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($por->warehouse_id);
        $this->load_view($this->theme.'production_order/modal_view', $this->data);
    }

    public function production_order_suggestions(){
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $POR_SUGGESTIONS_TYPE = $this->input->get('POR_SUGGESTIONS_TYPE', TRUE);
        $rows['results'] = $this->production_order_model->get_production_order_suggestions($term, $limit, $POR_SUGGESTIONS_TYPE);
        $this->sma->send_json($rows);
    }

    public function cutting_order_suggestions(){
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $POR_SUGGESTIONS_TYPE = $this->input->get('POR_SUGGESTIONS_TYPE', TRUE);
        $rows['results'] = $this->production_order_model->get_cutting_order_suggestions($term, $limit, $POR_SUGGESTIONS_TYPE);
        $this->sma->send_json($rows);
    }

    public function assemble_order_suggestions(){
        if ($this->input->get('term')) {
            $term = $this->input->get('term', TRUE);
        }
        $limit = $this->input->get('limit', TRUE);
        $POR_SUGGESTIONS_TYPE = $this->input->get('POR_SUGGESTIONS_TYPE', TRUE);
        $rows['results'] = $this->production_order_model->get_assemble_order_suggestions($term, $limit, $POR_SUGGESTIONS_TYPE);
        $this->sma->send_json($rows);
    }

    public function get_production_order_items($por_id){
        $por = $this->production_order_model->get_production_order_by_id($por_id);
        $por->date = date('d/m/Y H:i', strtotime($por->date));
        $por->est_date = date('d/m/Y H:i', strtotime($por->est_date));
        $rows = $this->production_order_model->get_poritems($por_id);
        $this->sma->send_json(['por' => $por, 'rows' => $rows]);
    }

    public function get_cutting_order_items($cutting_id, $por_id = NULL){

        $cutting_id = $cutting_id > 0 ? $cutting_id : false ;
        $por_id = $por_id > 0 ? $por_id : false ;

        $cuttings_id = $this->input->get('cuttings_id');
        $rows = $this->production_order_model->get_cutting_items_for_assemble($cutting_id, $por_id, $cuttings_id);
        $spr = [];
        $production_order_id = NULL;
        // $this->sma->print_arrays($rows);
        foreach ($rows as $row) {
            if (!isset($spr[$row->por_product_id][$row->por_option_id])) {
                if (!$production_order_id) {
                    $production_order_id = $row->production_order_id;
                }
                $cutted_quantity = $this->production_order_model->get_cutted_items_for_assemble($row->por_product_id, $row->production_order_id, $row->por_option_id);
                $spr[$row->por_product_id][$row->por_option_id] = [
                                'product_id' => $row->por_product_id,
                                'product_name' => $row->por_product_name,
                                'product_code' => $row->por_product_code,
                                'quantity' => $row->por_product_quantity,
                                'type' => $row->por_product_type,
                                'variant' => $row->por_variant,
                                'cutted_quantity' => $cutted_quantity,
                            ];
            }
        }
        foreach ($spr as $spr_row) {
            foreach ($spr_row as $spr_row_option) {
                $por_rows[] = $spr_row_option;
            }
        }
        $por_date = NULL;
        $por_est_date = NULL;
        if ($por_id) {
            $por = $this->production_order_model->get_production_order_by_id($por_id);
            $por_date = date('d/m/Y H:i', strtotime($por->date));
            $por_est_date = date('d/m/Y H:i', strtotime($por->est_date));
        }
        $this->sma->send_json(['rows'=>$rows, 'por_rows'=>$por_rows, 'production_order_id'=>$production_order_id, 'por_date'=>$por_date, 'por_est_date'=>$por_est_date]);
    }

    public function get_assemble_order_items($assemble_id, $por_id = NULL, $cutting_id = NULL){

        $assemble_id = $assemble_id > 0 ? $assemble_id : false ;
        $cutting_id = $cutting_id > 0 ? $cutting_id : false ;
        $por_id = $por_id > 0 ? $por_id : false ;

        $assembles_id = $this->input->get('assembles_id');

        $rows = $this->production_order_model->get_assemble_items_for_packing($assemble_id, $por_id, $cutting_id, $assembles_id);
        $spr = [];
        $production_order_id = NULL;
        $biller_id = NULL;
        $warehouse_id = NULL;
        $cutting_id = NULL;
        foreach ($rows as $row_id => $row) {
            if (!isset($spr[$row->por_product_id][$row->por_option_id])) {
                if (!$production_order_id) {
                    $production_order_id = $row->production_order_id;
                }
                if (!$cutting_id) {
                    $cutting_id = $row->cutting_id;
                }

                if (!$warehouse_id) {
                    $warehouse_id = $row->warehouse_id;
                }

                if (!$biller_id) {
                    $biller_id = $row->biller_id;
                }
                $assembled_quantity = $this->production_order_model->get_assembled_items_for_packing($row->por_product_id, $row->production_order_id, $row->por_option_id);
                if ($assembled_quantity) {
                    $spr[$row->por_product_id][$row->por_option_id] = [
                                    'product_id' => $row->por_product_id,
                                    'product_name' => $row->por_product_name,
                                    'product_code' => $row->por_product_code,
                                    'quantity' => $row->por_product_quantity,
                                    'type' => $row->por_product_type,
                                    'variant' => $row->por_variant,
                                    'assembled_quantity' => $assembled_quantity,
                                    'ciq_num' => $row->ciq_num,
                                ];

                if ($assembled_quantity == 0) {
                    $rows[$row_id]->no_packing = true;
                }

                } else {
                    unset($rows[$row_id]);
                }
            }
        }
        $por_rows = [];
        foreach ($spr as $spr_row) {
            foreach ($spr_row as $spr_row_option) {
                $por_rows[] = $spr_row_option;
            }
        }
        $this->sma->send_json([
                                'rows'=>$rows,
                                'por_rows'=>$por_rows,
                                'production_order_id'=>$production_order_id,
                                'cutting_id'=>$cutting_id,
                                'biller_id'=>$biller_id,
                                'warehouse_id'=>$warehouse_id,
                            ]);
    }

    public function get_production_order($id = NULL)
    {
        $row = $this->production_order_model->get_production_order_by_id($id);
        $date = date('d/m/Y H:i', strtotime($row->date));
        $est_date = date('d/m/Y H:i', strtotime($row->est_date));
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->reference_no, 'date' => $date, 'est_date' => $est_date)));
    }

    public function get_cutting_order($id = NULL)
    {
        $row = $this->production_order_model->get_cutting_order_by_id($id);
        // $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->reference_no)));
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->id)));
    }

    public function get_assemble_order($id = NULL)
    {
        $row = $this->production_order_model->get_assemble_order_by_id($id);
        $this->sma->send_json(array(array('id' => $row->id, 'text' => $row->id)));
    }

    public function add_cutting_order($production_order_id = NULL){
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('employee_id', lang("cutter_1"), 'required');
        // $this->form_validation->set_rules('reference_no', lang("cutting_reference_no"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');
        $this->form_validation->set_rules('est_date', lang("estimated_date"), 'required');
        if ($this->form_validation->run() == true) {
            if ($production_order_id == NULL) {
                $production_order_id = $this->input->post('production_order_id');
            }
            $por = $this->production_order_model->get_production_order_by_id($production_order_id);
            $continue = $this->input->post('continue');
            $date = $this->sma->fld($this->input->post('date'));
            $est_date = $this->sma->fld($this->input->post('est_date'));
            if (($date < $por->date && $date > $por->est_date) || ($est_date < $por->date && $est_date > $por->est_date)) {
                $this->session->set_flashdata('error', lang("dates_outside_the_allowed_range"));
                admin_redirect('production_order/add_cutting_order');
            }
            $reference_no = $this->input->post('reference_no');
            $employee_id = $this->input->post('employee_id');
            $employee_2_id = $this->input->post('employee_2_id');
            $tender_id = $this->input->post('tender_id');
            $tender_2_id = $this->input->post('tender_2_id');
            $packer_id = $this->input->post('packer_id');
            $packer_2_id = $this->input->post('packer_2_id');
            $embroiderer_id = $this->input->post('embroiderer');
            $stamper_id = $this->input->post('stamper');
            $production_order_reference_no = $this->input->post('production_order_reference_no');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $j = isset($_POST['cp_product_id']) ? sizeof($_POST['cp_product_id']) : 0;
            for ($r = 0; $r < $j; $r++) {
                $to_cut_quantity = $_POST['to_cut_quantity'][$r];
                $cp_production_order_pid = $_POST['cp_production_order_pid'][$r];
                $cp_product_id = $_POST['cp_product_id'][$r];
                $cp_production_order_pfinished_option_id = $_POST['cp_production_order_pfinished_option_id'][$r];
                if ($to_cut_quantity > 0) {
                    $products[] = array(
                            'production_order_pid' => $cp_production_order_pid,
                            'cutting_quantity' => $to_cut_quantity,
                            'product_id' => $cp_product_id,
                            'option_id' => $cp_production_order_pfinished_option_id,
                            'status' => 'pending',
                        );
                }
            }

            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            }

            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'est_date' => $est_date,
                'employee_id' => $employee_id,
                'employee_2_id' => $employee_2_id,
                'tender_id' => $tender_id,
                'tender_2_id' => $tender_2_id,
                'packer_id' => $packer_id,
                'packer_2_id' => $packer_2_id,
                'embroiderer_id' => $embroiderer_id,
                'stamper_id' => $stamper_id,
                'status' => 'pending',
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'production_order_id' => $production_order_id,
                'production_order_reference_no' => $production_order_reference_no,
                );

            if ($_FILES['document']['size'] > 0) {
                $this->load->library('upload');
                $config['upload_path'] = $this->upload_path;
                $config['allowed_types'] = $this->digital_file_types;
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = false;
                $config['encrypt_name'] = true;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('document')) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect($_SERVER["HTTP_REFERER"]);
                }

                $photo = $this->upload->file_name;
                $data['attachment'] = $photo;
            }
            // $this->sma->print_arrays($data, $products);
        }
        if ($this->form_validation->run() == true && $cutting_id = $this->production_order_model->addCuttingOrder($data, $products, $continue)) {
            $this->session->set_userdata('remove_culs', 1);
            $this->session->set_flashdata('message', lang("cutting_order_added"));
            // if ($continue == 1) {
            //     admin_redirect('production_order/add_assemble_order/'.$cutting_id);
            // }
            admin_redirect('production_order/cutting_orders');
        } else {
            if ($production_order_id) {
                $this->data['production_order_id'] = $production_order_id;
                $por = $this->production_order_model->get_production_order_by_id($production_order_id);
                $this->data['por_inv'] = $por;
                $this->data['por_rows'] = $this->production_order_model->get_poritems($production_order_id);
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('production_order/cutting_orders'), 'page' => lang('cutting_order')), array('link' => '#', 'page' => lang('add_cutting_order')));
            $meta = array('page_title' => lang('add_cutting_order'), 'bc' => $bc);
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->page_construct('production_order/add_cutting_order', $meta, $this->data);
        }
    }

    public function cutting_orders()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('cutting_orders')));
        $meta = array('page_title' => lang('cutting_orders'), 'bc' => $bc);
        $this->page_construct('production_order/cutting_orders', $meta, $this->data);
    }

    public function getCuttingOrders()
    {
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date')." 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:00" : NULL;
        $product = $this->input->post('product') ? $this->input->post('product') : NULL;
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('cutting')}.id AS id,
                        date,
                        DATEDIFF('".date('Y-m-d H:i:s')."', date) AS expiration,
                        {$this->db->dbprefix('cutting')}.id AS reference_no,
                        companies.name AS supplier_name,
                        ".($this->Settings->production_order_one_reference_limit == 1 ? "pfinished_product.code AS product_code, " : "")."
                        production_order_reference_no,
                        SUM(COALESCE({$this->db->dbprefix('cutting_detail')}.cutting_quantity, 0)) AS order_pieces,
                        SUM(COALESCE({$this->db->dbprefix('cutting_detail')}.finished_quantity, 0)) AS finished_pieces,
                        {$this->db->dbprefix('cutting')}.status,
                        SUM(COALESCE(assemble.assembling_quantity, 0) -  COALESCE(assemble.finished_quantity, 0)) AS in_assembling,
                        CONCAT(
                            COALESCE(complete_cutting_quantity, 0) - 
                            (
                                COALESCE(ciq_pfinished_complete_assemble.complete_assembling_quantity, 0) +
                                COALESCE(ciq_pfinished_complete_assemble.complete_assembling_quantity_completed, 0)
                            ),
                            '_',
                            (
                                SUM(COALESCE({$this->db->dbprefix('cutting_detail')}.finished_quantity, 0)) - 
                                (SUM(COALESCE(assemble.assembling_quantity, 0)))
                            )
                        ) AS for_assemble,
                        CONCAT(
                                COALESCE(ciq_pfinished_complete_assemble.complete_assembling_quantity_completed, 0) , '_' ,
                                SUM(COALESCE(assemble.finished_quantity, 0))
                                ) AS assembled_quantity,
                        IF(
                            SUM(COALESCE(assemble.assembling_quantity, 0)) = 0,
                            'pending', 
                            IF(
                                (SUM(COALESCE({$this->db->dbprefix('cutting_detail')}.cutting_quantity, 0)) - SUM(COALESCE(assemble.finished_quantity, 0))) = 0, 
                                'completed', 
                                'in_process'
                            )
                        ) AS assemble_status,
                        {$this->db->dbprefix('cutting')}.attachment as attachment,
                        (IF(SUM(COALESCE(assemble.assembling_quantity, 0)) = 0,'',IF((SUM(COALESCE({$this->db->dbprefix('cutting_detail')}.cutting_quantity, 0)) - SUM(COALESCE(assemble.finished_quantity, 0))) = 0,'style=\"display:none;\"',''))) as display_link
                    ", false)
            ->from('cutting')
            ->join('users', 'users.id=cutting.created_by', 'left')
            ->join('cutting_detail', 'cutting_detail.cutting_id=cutting.id', 'left')
            ->join('companies', 'companies.id=cutting.employee_id', 'left');
            $this->datatables->join('production_order_detail', 'production_order_detail.id=cutting_detail.production_order_pid', 'left');
            $this->datatables->join('production_order_detail por_pfinished', 'por_pfinished.id=production_order_detail.production_order_pfinished_item_id', 'left');
            $this->datatables->join('products pfinished_product', 'pfinished_product.id=por_pfinished.product_id', 'left')
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            cutting_id,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(cutting_composition_complete_quantity)) AS complete_cutting_quantity,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                CD.cutting_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.cutting_quantity) AS pieces_cutting_quantity,
                                (SUM(CD.cutting_quantity) / CI.quantity) AS cutting_composition_complete_quantity
                        FROM
                            {$this->db->dbprefix('cutting_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY CD.cutting_id, product_id) AS cutting_table
                        GROUP BY cutting_id) ciq_pfinished_complete_cutting", "ciq_pfinished_complete_cutting.production_order_pfinished_item_id = por_pfinished.id AND ciq_pfinished_complete_cutting.cutting_id = cutting.id", "left")
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            cutting_id,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity)) AS complete_assembling_quantity,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity_completed)) AS complete_assembling_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                cutting_detail.cutting_id,
                                POD.production_order_pfinished_item_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_assembling_quantity,
                                (SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS assemble_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_assembling_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS assemble_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('cutting_detail')} cutting_detail ON cutting_detail.id = CD.cutting_id_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY cutting_id, product_id) AS assemble_table
                        GROUP BY cutting_id) ciq_pfinished_complete_assemble", "ciq_pfinished_complete_assemble.production_order_pfinished_item_id = por_pfinished.id AND ciq_pfinished_complete_assemble.cutting_id = cutting.id", "left")
            ->join("(SELECT 
                        pfinished_POD.id as pfinished_POD_id,
                        CD.cutting_id_pid,
                        SUM(CD.assembling_quantity) AS assembling_quantity,
                        SUM(CD.finished_quantity) AS finished_quantity,
                        SUM(CD.fault_quantity) AS fault_quantity,
                        COUNT( DISTINCT CD.product_id) AS assembling_comp_qty
                    FROM
                        {$this->db->dbprefix('assemble_detail')} CD
                    INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                    INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                    GROUP BY CD.cutting_id_pid) assemble", "assemble.pfinished_POD_id = por_pfinished.id AND assemble.cutting_id_pid = cutting_detail.id", "left");
        $this->datatables->group_by("cutting.id");

        if ($product) {
            $this->datatables->where('pfinished_product.id', $product);
        }
        if ($start_date) {
            $this->datatables->where('cutting.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('cutting.date <=', $end_date);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('cutting.created_by', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                    <div class=\"btn-group text-left\">
                                                        <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                            Acciones
                                                            <span class=\"caret\"></span>
                                                        </button>
                                                        <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                                ".(($this->Owner || $this->Admin || $this->GP['production_order-add_assemble_order']) ? 
                                                                "<li $2>
                                                                    <a target='_blank' href=\"".admin_url('production_order/add_assemble_order/')."$1\"><i class='fas fa-sync-alt'></i> ".lang('add_assemble_order')." </a>
                                                                </li>"
                                                                :
                                                                "")."
                                                        </ul>
                                                    </div>
                                                </div>", "id, display_link");
        $this->datatables->unset_column("display_link");
        echo $this->datatables->generate();
        // <li>
        //     <a class=\"por_status\"><i class=\"fas fa-plus-square\"></i> ".lang('add_por_receipt')." </a>
        // </li>
    }

    public function modal_order_cutting_view($id)
    {
        $por = $this->production_order_model->get_cutting_order_by_id($id);
        if (!$id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $por;
        $rows = $this->production_order_model->get_cuttingitems($id);
        $por_pid_setted = [];
        $por_rows = [];
        foreach ($rows as $row) {
            if (!isset($por_pid_setted[$row->production_order_pfinished_item_id])) {
                $por_pid_setted[$row->production_order_pfinished_item_id] = 1;
                $por_rows[] = $this->production_order_model->get_poritems($row->production_order_id, $row->production_order_pfinished_item_id);
            }
        }
        $this->data['rows'] = $rows;
        $this->data['por_rows'] = $por_rows;
        $this->data['created_by'] = $this->site->getUser($por->created_by);
        $this->data['receipts'] = $this->production_order_model->get_receipts($por->id, POR_CUTTING_TYPE);
        // $this->sma->print_arrays($this->data['receipts']);
        $this->load_view($this->theme.'production_order/modal_order_cutting_view', $this->data);
    }

    public function change_cutting_order_items_status($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('ok_quantity[]', lang("ok_quantity"), 'required');

        if ($this->form_validation->run() == true) {

            $items_id = $this->input->post('item_id');
            $ok_quantity = $this->input->post('ok_quantity');
            $fault_quantity = $this->input->post('fault_quantity');

            $data = [];

            for ($i=0; $i < count($items_id) ; $i++) {
                if ($ok_quantity[$i] > 0 || $fault_quantity[$i] > 0) {
                    $data[] = [
                                'detail_id' => $items_id[$i],
                                'ok_quantity' => $ok_quantity[$i],
                                'fault_quantity' => $fault_quantity[$i],
                                'created_by' => $this->session->userdata('user_id'),
                                'type' => POR_CUTTING_TYPE,
                                ];
                }
            }

            if (count($data) == 0) {
                $this->session->set_flashdata('error', 'Cantidades en 0');
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'cutting_orders');
            }

            // $this->sma->print_arrays($data);

        } elseif ($this->input->post('ok_quantity')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'cutting_orders');
        }

        if ($this->form_validation->run() == true && $this->production_order_model->add_receipt($data)) {
            $this->session->set_flashdata('message', lang("receipt_added"));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'cutting_orders');
        }

        $por = $this->production_order_model->get_cutting_order_by_id($id);
        if (!$id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $por;
        $rows = $this->production_order_model->get_cuttingitems($id);
        $por_pid_setted = [];
        $por_rows = [];
        foreach ($rows as $row) {
            if (!isset($por_pid_setted[$row->production_order_pfinished_item_id])) {
                $por_pid_setted[$row->production_order_pfinished_item_id] = 1;
                $por_rows[] = $this->production_order_model->get_poritems($row->production_order_id, $row->production_order_pfinished_item_id);
            }
        }
        $this->data['rows'] = $rows;
        $this->data['por_rows'] = $por_rows;
        $this->data['created_by'] = $this->site->getUser($por->created_by);
        $this->load_view($this->theme.'production_order/change_cutting_order_items_status', $this->data);
    }

    public function add_assemble_order($cutting_id = NULL, $cuttings_selection = false){
        $this->sma->checkPermissions();
        $cutting_id = $cutting_id == 'null' ? NULL : $cutting_id;
        $this->form_validation->set_rules('supplier_id', lang("supplier"), 'required');
        // $this->form_validation->set_rules('reference_no', lang("assemble_reference_no"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');
        $this->form_validation->set_rules('est_date', lang("estimated_date"), 'required');
        if ($this->form_validation->run() == true) {
            if ($cutting_id == NULL && $this->input->post('cutting_order_id')) {
                $cutting_id = $this->input->post('cutting_order_id');
                $por_cutting = $this->production_order_model->get_cutting_order_by_id($cutting_id);
                if ($por_cutting->status != 'completed') {
                    $this->session->set_flashdata('error', lang("cutting_order_incompleted_status"));
                    admin_redirect('production_order/cutting_orders');
                }
            }
            $por = $this->production_order_model->get_production_order_by_id($this->input->post('production_order_id'));
            $continue = $this->input->post('continue');
            $cuttings_ids = $this->input->post('cuttings_ids');
            $date = $this->sma->fld($this->input->post('date'));
            $est_date = $this->sma->fld($this->input->post('est_date'));
            if (($date < $por->date && $date > $por->est_date) || ($est_date < $por->date && $est_date > $por->est_date)) {
                $this->session->set_flashdata('error', lang("dates_outside_the_allowed_range"));
                admin_redirect('production_order/add_cutting_order');
            }
            $reference_no = $this->input->post('reference_no');
            $supplier_id = $this->input->post('supplier_id');
            $production_order_reference_no = $this->input->post('production_order_reference_no');
            $cutting_reference_no = $this->input->post('cutting_order_reference_no');
            $note = $this->sma->clear_tags($this->input->post('note'));

            $j = isset($_POST['cp_product_id']) ? sizeof($_POST['cp_product_id']) : 0;
            for ($r = 0; $r < $j; $r++) {
                $to_assemble_quantity = $_POST['to_assemble_quantity'][$r];
                $cp_cutting_detail_id = $_POST['cp_cutting_detail_id'][$r];
                $cp_production_order_pid = $_POST['cp_production_order_pid'][$r];
                $cp_production_order_option_id = $_POST['cp_production_order_option_id'][$r];
                $cp_product_id = $_POST['cp_product_id'][$r];
                if ($to_assemble_quantity > 0) {
                    $products[] = array(
                            'cutting_id_pid' => $cp_cutting_detail_id,
                            'production_order_pid' => $cp_production_order_pid,
                            'assembling_quantity' => $to_assemble_quantity,
                            'option_id' => $cp_production_order_option_id,
                            'product_id' => $cp_product_id,
                            'status' => 'pending',
                        );
                }
            }
            if (empty($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            }
            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'est_date' => $est_date,
                'supplier_id' => $supplier_id,
                'status' => 'pending',
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'cutting_id' => $cutting_id,
                'cutting_reference_no' => $cutting_reference_no,
                'production_order_reference_no' => $production_order_reference_no,
                );
            // $this->sma->print_arrays($data, $products);
        }
        if ($this->form_validation->run() == true && $assemble_id = $this->production_order_model->addAssembleOrder($data, $products, $continue, $cuttings_ids)) {
            $this->session->set_userdata('remove_assls', 1);
            $this->session->set_flashdata('message', lang("assemble_order_added"));
            if ($continue == 1) {
                admin_redirect('production_order/add_packing_order/'.$assemble_id);
            }
            admin_redirect('production_order/assemble_orders');
        } else {
            if ($cutting_id) {
                $this->data['cutting_order_id'] = $cutting_id;
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('production_order/assemble_orders'), 'page' => lang('assemble_order')), array('link' => '#', 'page' => lang('add_assemble_order')));

            if ($cuttings_selection == 'true') {
                $this->data['cuttings_ids'] = $this->session->userdata('cuttings_ids');
            }
            if ($this->session->userdata('cuttings_ids')) {
                $this->session->unset_userdata('cuttings_ids');
            }
            $meta = array('page_title' => lang('add_assemble_order'), 'bc' => $bc);
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            $this->page_construct('production_order/add_assemble_order', $meta, $this->data);
        }
    }

    public function assemble_orders()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('assemble_orders')));
        $meta = array('page_title' => lang('assemble_orders'), 'bc' => $bc);
        $this->page_construct('production_order/assemble_orders', $meta, $this->data);
    }

    public function getAssembleOrders()
    {
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date')." 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:00" : NULL;
        $product = $this->input->post('product') ? $this->input->post('product') : NULL;
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('assemble')}.id as id,
                        date,
                        DATEDIFF('".date('Y-m-d H:i:s')."', date) as expiration,
                        {$this->db->dbprefix('assemble')}.id as reference_no,
                        companies.name as supplier_name,
                        ".($this->Settings->production_order_one_reference_limit == 1 ? "pfinished_product.code as product_code, " : "")."
                        production_order_reference_no,
                        cutting_id as cutting_reference_no,
                        SUM({$this->db->dbprefix('assemble_detail')}.assembling_quantity) as order_pieces,
                        SUM({$this->db->dbprefix('assemble_detail')}.finished_quantity) as finished_pieces,
                        {$this->db->dbprefix('assemble')}.status,
                        CONCAT(
                                COALESCE(ciq_pfinished_complete_packing.complete_packing_quantity, 0)
                                , '_',
                                SUM(COALESCE(packing.packing_quantity, 0) - COALESCE(packing.finished_quantity, 0))
                            ) as in_packing,
                        CONCAT(
                            (
                                COALESCE(ciq_pfinished_complete_assemble.complete_assembling_quantity_completed, 0) - 
                                (
                                    COALESCE(ciq_pfinished_complete_packing.complete_packing_quantity, 0) +
                                    COALESCE(ciq_pfinished_complete_packing.complete_packing_quantity_completed, 0)
                                )
                            ), '_',
                            SUM({$this->db->dbprefix('assemble_detail')}.finished_quantity) - SUM(COALESCE(packing.packing_quantity, 0))
                        ) for_packing,
                        CONCAT(COALESCE(ciq_pfinished_complete_packing.complete_packing_quantity_completed,0), '_', SUM(COALESCE(packing.finished_quantity, 0))) as packed_quantity,
                        IF(SUM(COALESCE(packing.packing_quantity, 0)) = 0, 'pending',
                            IF(
                                (SUM({$this->db->dbprefix('assemble_detail')}.assembling_quantity) - SUM(packing.finished_quantity)) = 0, 
                                'completed', 
                                'in_process'
                            )
                        ) as packing_status
                    ")
            ->from('assemble')
            ->join('users', 'users.id=assemble.created_by', 'left')
            ->join('assemble_detail', 'assemble_detail.assemble_id=assemble.id', 'left')
            ->join('companies', 'companies.id=assemble.supplier_id', 'left');
            $this->datatables->join('production_order_detail', 'production_order_detail.id=assemble_detail.production_order_pid', 'left');
            $this->datatables->join('production_order_detail por_pfinished', 'por_pfinished.id=production_order_detail.production_order_pfinished_item_id', 'left');
            $this->datatables->join('products pfinished_product', 'pfinished_product.id=por_pfinished.product_id', 'left')
                ->join("(SELECT 
                            production_order_pfinished_item_id,
                            assemble_id,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity)) AS complete_assembling_quantity,
                            IF(COUNT(DISTINCT product_id)!= pfinished_num_composition_items, 0, MIN(assemble_composition_complete_quantity_completed)) AS complete_assembling_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (SELECT 
                                POD.production_order_id,
                                POD.production_order_pfinished_item_id,
                                CD.assemble_id,
                                POD.product_id,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.assembling_quantity) AS pieces_assembling_quantity,
                                (SUM(CD.assembling_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS assemble_composition_complete_quantity,
                                (SUM(CD.finished_quantity) / CI.quantity) AS assemble_composition_complete_quantity_completed
                        FROM
                            {$this->db->dbprefix('assemble_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        INNER JOIN (SELECT 
                            COUNT(CI.id) AS num_composition_items, CI.product_id
                        FROM
                            {$this->db->dbprefix('combo_items')} CI
                        GROUP BY CI.product_id) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY CD.assemble_id, product_id) AS assemble_table
                        GROUP BY assemble_id) ciq_pfinished_complete_assemble", "ciq_pfinished_complete_assemble.production_order_pfinished_item_id = por_pfinished.id AND ciq_pfinished_complete_assemble.assemble_id = assemble.id", "left")
                ->join("(SELECT 
                            production_order_pfinished_item_id,
                            assemble_id,
                            MIN(packing_composition_complete_quantity) AS complete_packing_quantity,
                            MIN(packing_composition_complete_quantity_completed) AS complete_packing_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (
                            SELECT 
                                POD.production_order_id,
                                POD.product_id,
                                assemble_detail.assemble_id,
                                POD.production_order_pfinished_item_id,
                                CD.assemble_id_pid,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_packing_quantity,
                                (SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS packing_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_packing_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS packing_composition_complete_quantity_completed
                            FROM
                                {$this->db->dbprefix('packing_detail')} CD
                            INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                            INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                            INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                            INNER JOIN {$this->db->dbprefix('assemble_detail')} assemble_detail ON assemble_detail.id = CD.assemble_id_pid
                            INNER JOIN (SELECT 
                                COUNT(CI.id) AS num_composition_items, CI.product_id
                            FROM
                                {$this->db->dbprefix('combo_items')} CI
                            GROUP BY CI.product_id
                        ) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY assemble_id, product_id) AS packing_table
                        GROUP BY assemble_id) ciq_pfinished_complete_packing", "ciq_pfinished_complete_packing.production_order_pfinished_item_id = por_pfinished.id AND ciq_pfinished_complete_packing.assemble_id = assemble.id ", "left")
            ->join("(SELECT 
                            pfinished_POD.id as pfinished_POD_id,
                            CD.assemble_id_pid,
                            SUM(CD.packing_quantity) AS packing_quantity,
                            SUM(CD.finished_quantity) AS finished_quantity,
                            SUM(CD.fault_quantity) AS fault_quantity,
                            COUNT( DISTINCT CD.product_id) AS packing_comp_qty
                        FROM
                            {$this->db->dbprefix('packing_detail')} CD
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                        INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                        GROUP BY CD.assemble_id_pid) packing", "packing.pfinished_POD_id = por_pfinished.id AND packing.assemble_id_pid = assemble_detail.id", "left");

        if ($product) {
            $this->datatables->where('pfinished_product.id', $product);
        }
        $this->datatables->group_by("assemble.id");
        if ($start_date) {
            $this->datatables->where('assemble.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('assemble.date <=', $end_date);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('assemble.created_by', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                    <div class=\"btn-group text-left\">
                                                        <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                            Acciones
                                                            <span class=\"caret\"></span>
                                                        </button>
                                                        <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                            ".(($this->Owner || $this->Admin || $this->GP['production_order-change_assemble_order_items_status']) ? 
                                                                    "<li>
                                                                        <a class=\"por_status\"><i class=\"fas fa-plus-square\"></i> ".lang('add_por_receipt')." </a>
                                                                    </li>"
                                                                    :
                                                            "")."
                                                            ".(($this->Owner || $this->Admin || $this->GP['production_order-add_packing_order']) ? 
                                                                    "<li>
                                                                        <a target='_blank' href=\"".admin_url('production_order/add_packing_order/')."$1\"><i class='fas fa-sync-alt'></i> ".lang('add_packing_order')." </a>
                                                                    </li>"
                                                                    :
                                                            "")."
                                                        </ul>
                                                    </div>
                                                </div>", "id");

        echo $this->datatables->generate();
    }

    public function modal_order_assemble_view($id)
    {
        $por = $this->production_order_model->get_assemble_order_by_id($id);
        if (!$id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $por;
        $rows = $this->production_order_model->get_assembleitems($id);
        $por_pid_setted = [];
        $por_rows = [];
        foreach ($rows as $row) {
            if (!isset($por_pid_setted[$row->production_order_pfinished_item_id])) {
                $por_pid_setted[$row->production_order_pfinished_item_id] = 1;
                $por_rows[] = $this->production_order_model->get_poritems($row->production_order_id, $row->production_order_pfinished_item_id);
            }
        }
        $this->data['rows'] = $rows;
        $this->data['por_rows'] = $por_rows;
        $this->data['created_by'] = $this->site->getUser($por->created_by);
        $this->data['receipts'] = $this->production_order_model->get_receipts($por->id, POR_ASSEMBLE_TYPE);
        // $this->sma->print_arrays($this->data['receipts']);
        $this->load_view($this->theme.'production_order/modal_order_assemble_view', $this->data);
    }

    public function change_assemble_order_items_status($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('ok_quantity[]', lang("ok_quantity"), 'required');

        if ($this->form_validation->run() == true) {

            $items_id = $this->input->post('item_id');
            $ok_quantity = $this->input->post('ok_quantity');
            $fault_quantity = $this->input->post('fault_quantity');

            $data = [];

            for ($i=0; $i < count($items_id) ; $i++) {
                if ($ok_quantity[$i] > 0 || $fault_quantity[$i] > 0) {
                    $data[] = [
                                'detail_id' => $items_id[$i],
                                'ok_quantity' => $ok_quantity[$i],
                                'fault_quantity' => $fault_quantity[$i],
                                'created_by' => $this->session->userdata('user_id'),
                                'type' => POR_ASSEMBLE_TYPE,
                                ];
                }
            }

            if (count($data) == 0) {
                $this->session->set_flashdata('error', 'Cantidades en 0');
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'assemble_orders');
            }

            // $this->sma->print_arrays($data);

        } elseif ($this->input->post('ok_quantity')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'assemble_orders');
        }

        if ($this->form_validation->run() == true && $this->production_order_model->add_receipt($data)) {
            $this->session->set_flashdata('message', lang("receipt_added"));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'assemble_orders');
        }

        $por = $this->production_order_model->get_assemble_order_by_id($id);
        if (!$id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $por;
        $rows = $this->production_order_model->get_assembleitems($id);
        $por_pid_setted = [];
        $por_rows = [];
        foreach ($rows as $row) {
            if (!isset($por_pid_setted[$row->production_order_pfinished_item_id][$row->option_id])) {
                $por_pid_setted[$row->production_order_pfinished_item_id][$row->option_id] = 1;
                $por_rows[] = $row;
            }
        }
        $this->data['rows'] = $rows;
        $this->data['por_rows'] = $por_rows;
        $this->data['created_by'] = $this->site->getUser($por->created_by);
        $this->load_view($this->theme.'production_order/change_assemble_order_items_status', $this->data);
    }

    public function add_packing_order($assemble_order_id = NULL, $assembles_selection = false){
        $assemble_order_id = $assemble_order_id == 'null' ? NULL : $assemble_order_id;
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('supplier_id', lang("supplier"), 'required');
        $this->form_validation->set_rules('date', lang("date"), 'required');
        $this->form_validation->set_rules('est_date', lang("estimated_date"), 'required');
        if ($assemble_order_id) {
            $por_assemble = $this->production_order_model->get_assemble_order_by_id($assemble_order_id);
            if ($por_assemble->status != 'completed') {
                // $this->session->set_flashdata('error', lang("assemble_order_incompleted_status"));
                // admin_redirect('production_order/assemble_orders');
            }
        }
        if ($this->form_validation->run() == true) {
            $continue = $this->input->post('continue');
            $date = $this->sma->fld($this->input->post('date'));
            $est_date = $this->sma->fld($this->input->post('est_date'));
            $reference_no = $this->input->post('reference_no');
            $supplier_id = $this->input->post('supplier_id');
            $biller_id = $this->input->post('biller');
            $warehouse_id = $this->input->post('warehouse');
            $production_order_reference_no = $this->input->post('production_order_reference_no');
            $assemble_reference_no = $this->input->post('assemble_order_reference_no');
            $assemble_id = $this->input->post('assemble_order_id');
            $assembles_ids = $this->input->post('assembles_ids');
            $adjustment_doc_type_id = $this->input->post('adjustment_document_type_id');
            $note = $this->sma->clear_tags($this->input->post('note'));
            $j = isset($_POST['cp_product_id']) ? sizeof($_POST['cp_product_id']) : 0;

            $pfinished_ciq_num = null;
            $pfinished_composition_items = [];

            for ($r = 0; $r < $j; $r++) {
                $to_packing_quantity = $_POST['to_packing_quantity'][$r];
                $cp_assemble_detail_id = $_POST['cp_assemble_detail_id'][$r];
                $cp_production_order_pid = $_POST['cp_production_order_pid'][$r];
                $cp_production_order_option_id = $_POST['cp_production_order_option_id'][$r];
                $cp_product_id = $_POST['cp_product_id'][$r];
                $item_composition_qty = $this->input->post('item_composition_qty');
                $item_pfinished_id = $this->input->post('item_pfinished_id');
                $item_product_id = $this->input->post('item_product_id');
                $pfinished_product_id = $this->input->post('item_pfinished_product_id');
                $pfinished_option_id = $this->input->post('item_pfinished_option_id');
                $blocked_composition_data = [];
                $pfinished_ciq_num[$pfinished_product_id[$r]][$pfinished_option_id[$r]] = $_POST['cp_pfinished_ciq_num'][$r];
                $pfinished_composition_items[$pfinished_product_id[$r]][$pfinished_option_id[$r]][] = $cp_product_id;
                if ($to_packing_quantity > 0) {
                    $products[] = array(
                            'assemble_id_pid' => $cp_assemble_detail_id,
                            'production_order_pid' => $cp_production_order_pid,
                            'packing_quantity' => $to_packing_quantity,
                            'product_id' => $cp_product_id,
                            'option_id' => $cp_production_order_option_id,
                            'status' => 'pending',
                        );
                    if (!isset($composition_data[$pfinished_product_id[$r]][$pfinished_option_id[$r]]) && !isset($blocked_composition_data[$pfinished_product_id[$r]][$pfinished_option_id[$r]])) {
                        $comp_qty = $to_packing_quantity / $item_composition_qty[$r];
                        if ($comp_qty != floor($comp_qty)) {
                            $this->session->set_flashdata('error', 'Algunas de las cantidades no completan la unidad de uno de los productos finalizados');
                            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
                        }
                        $composition_data[$pfinished_product_id[$r]][$pfinished_option_id[$r]] = $to_packing_quantity / $item_composition_qty[$r];
                    } else {
                        if ($composition_data[$pfinished_product_id[$r]][$pfinished_option_id[$r]] != ($to_packing_quantity / $item_composition_qty[$r]) && !isset($blocked_composition_data[$pfinished_product_id[$r]][$pfinished_option_id[$r]])) {
                            unset($composition_data[$pfinished_product_id[$r]][$pfinished_option_id[$r]]);
                            $blocked_composition_data[$pfinished_product_id[$r]][$pfinished_option_id[$r]] = 1;
                        }
                    }
                }
            }
            foreach ($pfinished_ciq_num as $pfinished_id => $pfinished_options) {
                foreach ($pfinished_options as $pfinished_option_id => $pfinished_ciq_num_arr) {
                    if (count($pfinished_composition_items[$pfinished_id][$pfinished_option_id]) != $pfinished_ciq_num_arr) {
                        $this->session->set_flashdata('error', 'Algunas de las cantidades no completan la unidad de uno de los productos finalizados');
                        admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
                    }
                }
            }
            // $this->sma->print_arrays($pfinished_ciq_num, $pfinished_composition_items);
            if (count($blocked_composition_data) > 0) {
                $this->session->set_flashdata('error', 'Hay error con los componentes, algunos no completan el producto terminado');
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
            }
            if (!isset($products)) {
                $this->form_validation->set_rules('product', lang("products"), 'required');
            }
            $data = array(
                'date' => $date,
                'reference_no' => $reference_no,
                'biller_id' => $biller_id,
                'warehouse_id' => $warehouse_id,
                'est_date' => $est_date,
                'supplier_id' => $supplier_id,
                'status' => 'pending',
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'assemble_id' => $assemble_id,
                'assemble_reference_no' => $assemble_reference_no,
                'production_order_reference_no' => $production_order_reference_no,
                'adjustment_doc_type_id' => $adjustment_doc_type_id,
                );
            $productsNames = [];
            if (isset($composition_data)) {
                foreach ($composition_data as $key_pfinished_product_id => $arr_pfinished_option_id) {
                    foreach ($arr_pfinished_option_id as $key_pfinished_option_id => $quantity) {
                        $pfinished_data = $this->site->getProductByID($key_pfinished_product_id);
                        $adjustment_products[] = array(
                            'product_id' => $key_pfinished_product_id,
                            'type' => 'addition',
                            'quantity' => $quantity,
                            'warehouse_id' => $warehouse_id,
                            'option_id' => $key_pfinished_option_id,
                            'serial_no' => NULL,
                            'avg_cost' => $pfinished_data->cost,
                            'adjustment_cost' => ($pfinished_data->cost),
                          );
                        $productsNames[$key_pfinished_product_id] = $pfinished_data->name;
                    }
                }
            }
                
            $adjustment_data = array(
                'date' => date('Y-m-d H:i:s'),
                'warehouse_id' => $warehouse_id,
                'note' => $note,
                'created_by' => $this->session->userdata('user_id'),
                'companies_id' => $supplier_id,
                'biller_id' => $biller_id,
                'document_type_id' => $adjustment_doc_type_id,
                'origin_reference_no' => $reference_no,
                'type_adjustment' => 3,
              );
            // $this->sma->print_arrays($adjustment_data, $adjustment_products);
        }
        if ($this->form_validation->run() == true && $packing_id = $this->production_order_model->addPackingOrder($data, $products, $continue, $assembles_ids)) {
            if ($continue) {
                $adjustment_data['origin_reference_no'] = $packing_id;
                if ($this->products_model->addAdjustment($adjustment_data, $adjustment_products, $productsNames)) {
                    $this->production_order_model->syncProductionOrderStatus($production_order_reference_no);
                }
            }
            $this->session->set_userdata('remove_pckls', 1);
            $this->session->set_flashdata('message', lang("packing_order_added"));
            admin_redirect('production_order/packing_orders');
        } else {
            if ($assemble_order_id) {
                $this->data['assemble_order_id'] = $assemble_order_id;
            }
            $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('production_order/packing_orders'), 'page' => lang('packing_order')), array('link' => '#', 'page' => lang('add_packing_order')));
            $meta = array('page_title' => lang('add_packing_order'), 'bc' => $bc);
            $this->data['warehouses'] = $this->site->getAllWarehouses();
            $this->data['billers'] = $this->site->getAllCompaniesWithState('biller');
            $this->data['error'] = (validation_errors() ? validation_errors() : $this->session->flashdata('error'));
            if ($assembles_selection == 'true') {
                $this->data['assembles_ids'] = $this->session->userdata('assembles_ids');
            }
            if ($this->session->userdata('assembles_ids')) {
                // $this->session->unset_userdata('assembles_ids');
            }
            $this->page_construct('production_order/add_packing_order', $meta, $this->data);
        }
    }

    public function packing_orders()
    {
        $this->sma->checkPermissions();
        $this->data['error'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('error');
        $bc = array(array('link' => base_url(), 'page' => lang('home')), array('link' => admin_url('products'), 'page' => lang('products')), array('link' => '#', 'page' => lang('packing_orders')));
        $meta = array('page_title' => lang('packing_orders'), 'bc' => $bc);
        $this->page_construct('production_order/packing_orders', $meta, $this->data);
    }

    public function getPackingOrders()
    {
        $start_date = $this->input->post('start_date') ? $this->input->post('start_date')." 00:00:00" : NULL;
        $end_date = $this->input->post('end_date') ? $this->input->post('end_date')." 23:59:00" : NULL;
        $product = $this->input->post('product') ? $this->input->post('product') : NULL;
        $this->load->library('datatables');
        $this->datatables
            ->select("
                        {$this->db->dbprefix('packing')}.id as id,
                        {$this->db->dbprefix('packing')}.date,
                        DATEDIFF('".date('Y-m-d H:i:s')."', {$this->db->dbprefix('packing')}.date) as expiration,
                        {$this->db->dbprefix('packing')}.id as reference_no,
                        GROUP_CONCAT(AI.id, '_', {$this->db->dbprefix('warehouses')}.name, '_', AI.AI_qty, '_', {$this->db->dbprefix('adjustments')}.reference_no) as adjustments_qty,
                        companies.name as supplier_name,
                        packing.production_order_reference_no,
                        cutting.id as cutting_reference_no,
                        assemble_id as assemble_reference_no,
                        ".($this->Settings->production_order_one_reference_limit == 1 ? "pfinished_product.code as product_code, " : "")."
                        ciq_pfinished_complete_packing.complete_packing_quantity as order_pieces,
                        ciq_pfinished_complete_packing.complete_packing_quantity_completed as finished_pieces,
                        {$this->db->dbprefix('packing')}.status
                    ")
            ->from('packing')
            ->join('users', 'users.id=packing.created_by', 'left')
            ->join('packing_detail', 'packing_detail.packing_id=packing.id', 'left')
            ->join('companies', 'companies.id=packing.supplier_id', 'left')
            ->join('production_order_detail', 'production_order_detail.id=packing_detail.production_order_pid', 'left')
            ->join('production_order_detail por_pfinished', 'por_pfinished.id=production_order_detail.production_order_pfinished_item_id', 'left')
            ->join('products pfinished_product', 'pfinished_product.id=por_pfinished.product_id', 'left')
            ->join('assemble', 'assemble.id=packing.assemble_id', 'left')
            ->join('cutting', 'cutting.id=assemble.cutting_id', 'left')
            ->join("(SELECT 
                            production_order_pfinished_item_id,
                            packing_id,
                            MIN(packing_composition_complete_quantity) AS complete_packing_quantity,
                            MIN(packing_composition_complete_quantity_completed) AS complete_packing_quantity_completed,
                            COUNT(production_order_pfinished_item_id),
                            pfinished_num_composition_items
                        FROM
                            (
                            SELECT 
                                POD.production_order_id,
                                POD.product_id,
                                CD.packing_id,
                                POD.production_order_pfinished_item_id,
                                CD.assemble_id_pid,
                                CIQ.num_composition_items AS pfinished_num_composition_items,
                                CI.quantity AS composition_quantity,
                                SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) AS pieces_packing_quantity,
                                (SUM(CD.packing_quantity - CD.finished_quantity - CD.fault_quantity) / CI.quantity) AS packing_composition_complete_quantity,
                                SUM(CD.finished_quantity) AS pieces_packing_quantity_completed,
                                (SUM(CD.finished_quantity) / CI.quantity) AS packing_composition_complete_quantity_completed
                            FROM
                                {$this->db->dbprefix('packing_detail')} CD
                            INNER JOIN {$this->db->dbprefix('production_order_detail')} POD ON POD.id = CD.production_order_pid
                            INNER JOIN {$this->db->dbprefix('products')} PR ON PR.id = POD.product_id
                            INNER JOIN {$this->db->dbprefix('production_order_detail')} pfinished_POD ON pfinished_POD.id = POD.production_order_pfinished_item_id
                            INNER JOIN (SELECT 
                                COUNT(CI.id) AS num_composition_items, CI.product_id
                            FROM
                                {$this->db->dbprefix('combo_items')} CI
                            GROUP BY CI.product_id
                        ) CIQ ON CIQ.product_id = pfinished_POD.product_id
                        INNER JOIN {$this->db->dbprefix('combo_items')} CI ON CI.product_id = pfinished_POD.product_id
                            AND CI.item_code = PR.code
                        GROUP BY packing_id, product_id) AS packing_table
                        GROUP BY packing_id) ciq_pfinished_complete_packing", "ciq_pfinished_complete_packing.production_order_pfinished_item_id = por_pfinished.id AND ciq_pfinished_complete_packing.packing_id = packing.id ", "left")
            ->join('adjustments', 'adjustments.origin_reference_no = packing.id ', 'left')
            ->join("(
                    SELECT AI.id,  AI.adjustment_id, AI.product_id, AI.warehouse_id, SUM(AI.quantity) AS AI_qty FROM {$this->db->dbprefix('adjustment_items')} AI
                    GROUP BY AI.adjustment_id, AI.product_id
                    ) AI", 'AI.adjustment_id = adjustments.id AND AI.product_id = por_pfinished.product_id', 'left')
            ->join('warehouses', 'warehouses.id = AI.warehouse_id','left')
            ;
        if ($product) {
            $this->datatables->where('pfinished_product.id', $product);
        }
        $this->datatables->group_by("packing.id");
        if ($start_date) {
            $this->datatables->where('packing.date >=', $start_date);
        }
        if ($end_date) {
            $this->datatables->where('packing.date <=', $end_date);
        }
        if (!$this->Customer && !$this->Supplier && !$this->Owner && !$this->Admin && !$this->Admin && !$this->session->userdata('view_right')) {
            $this->datatables->where('packing.created_by', $this->session->userdata('user_id'));
        }
        $this->datatables->add_column("Actions", "<div class=\"text-center\">
                                                    <div class=\"btn-group text-left\">
                                                        <button type=\"button\" class=\"btn btn-default btn-xs btn-primary dropdown-toggle\" data-toggle=\"dropdown\">
                                                            Acciones
                                                            <span class=\"caret\"></span>
                                                        </button>
                                                        <ul class=\"dropdown-menu pull-right\" role=\"menu\">
                                                            ".(($this->Owner || $this->Admin || $this->GP['production_order-change_packing_order_items_status']) ? 
                                                                    "<li>
                                                                        <a class=\"por_status\"><i class=\"fas fa-plus-square\"></i> ".lang('add_por_receipt')." </a>
                                                                    </li>"
                                                                    :
                                                            "")."
                                                            <li>
                                                                <a  href='admin/products/print_barcodes/?pckorder=$1'><i class='fa fa-print'></i> ".lang('print_barcodes')." </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>", "id");

        echo $this->datatables->generate();
    }

    public function modal_order_packing_view($id)
    {
        $por = $this->production_order_model->get_packing_order_by_id($id);
        if (!$id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $this->data['inv'] = $por;
        $rows = $this->production_order_model->get_packingitems($id);
        $por_pid_setted = [];
        $por_rows = [];
        foreach ($rows as $row) {
            if (!isset($por_pid_setted[$row->production_order_pfinished_item_id][$row->option_id])) {
                $por_pid_setted[$row->production_order_pfinished_item_id][$row->option_id] = 1;
                // $por_rows[] = $this->production_order_model->get_poritems($row->production_order_id, $row->production_order_pfinished_item_id);
                $por_row = $this->production_order_model->get_poritems($row->production_order_id, $row->production_order_pfinished_item_id);
                $por_row->packed_quantity = $this->production_order_model->get_packed_items($por_row->product_id, $por_row->production_order_id, $row->option_id);
                $variant_data = $this->site->get_product_variant_by_id($row->option_id);
                $por_row->variant = $variant_data->name.$variant_data->suffix;
                $por_rows[] = $por_row;
            }
        }
        $this->data['rows'] = $rows;
        $this->data['por_rows'] = $por_rows;
        $this->data['created_by'] = $this->site->getUser($por->created_by);
        $this->data['receipts'] = $this->production_order_model->get_receipts($por->id, POR_PACKING_TYPE);
        // $this->sma->print_arrays($this->data['receipts']);
        $this->load_view($this->theme.'production_order/modal_order_packing_view', $this->data);
    }

    public function change_packing_order_items_status($id)
    {
        $this->sma->checkPermissions();
        $this->form_validation->set_rules('ok_quantity[]', lang("ok_quantity"), 'required');
        $por = $this->production_order_model->get_packing_order_by_id($id);
        $this->data['inv'] = $por;
        if ($this->form_validation->run() == true) {
            $items_id = $this->input->post('item_id');
            $ok_quantity = $this->input->post('ok_quantity');
            $warehouse = $this->input->post('warehouse');
            $fault_quantity = $this->input->post('fault_quantity');
            $item_composition_qty = $this->input->post('item_composition_qty');
            $item_pfinished_id = $this->input->post('item_pfinished_id');
            $item_product_id = $this->input->post('item_product_id');
            $pfinished_product_id = $this->input->post('item_pfinished_product_id');
            $pfinished_option_id = $this->input->post('item_pfinished_option_id');
            $data = [];
            $composition_data = [];
            $blocked_composition_data = [];
            for ($i=0; $i < count($items_id) ; $i++) {
                if ($ok_quantity[$i] > 0 || $fault_quantity[$i] > 0) {
                    $data[] = [
                                'detail_id' => $items_id[$i],
                                'ok_quantity' => $ok_quantity[$i],
                                'fault_quantity' => $fault_quantity[$i],
                                'created_by' => $this->session->userdata('user_id'),
                                'packing_warehouse_id' => $warehouse,
                                'type' => POR_PACKING_TYPE,
                                ];
                    if (!isset($composition_data[$pfinished_product_id[$i]][$pfinished_option_id[$i]]) && !isset($blocked_composition_data[$pfinished_product_id[$i]][$pfinished_option_id[$i]])) {
                        $comp_qty = $ok_quantity[$i] / $item_composition_qty[$i];
                        if ($comp_qty != floor($comp_qty)) {
                            $this->session->set_flashdata('error', 'Algunas de las cantidades no completan la unidad de uno de los productos finalizados');
                            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
                        }
                        $composition_data[$pfinished_product_id[$i]][$pfinished_option_id[$i]] = $comp_qty;
                    } else {
                        if ($composition_data[$pfinished_product_id[$i]][$pfinished_option_id[$i]] != ($ok_quantity[$i] / $item_composition_qty[$i]) && !isset($blocked_composition_data[$pfinished_product_id[$i]][$pfinished_option_id[$i]])) {
                            unset($composition_data[$pfinished_product_id[$i]][$pfinished_option_id[$i]]);
                            $blocked_composition_data[$pfinished_product_id[$i]][$pfinished_option_id[$i]] = 1;
                        }
                    }
                }
            }
            if (count($blocked_composition_data) > 0) {
                $this->session->set_flashdata('error', 'Hay error con los componentes, algunos no completan el producto terminado');
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
            }
            $productsNames = [];
            // $this->sma->print_arrays($composition_data);
            foreach ($composition_data as $key_pfinished_product_id => $arr_pfinished_option_id) {
                foreach ($arr_pfinished_option_id as $key_pfinished_option_id => $quantity) {
                    $pfinished_data = $this->site->getProductByID($key_pfinished_product_id);
                    $adjustment_products[] = array(
                        'product_id' => $key_pfinished_product_id,
                        'type' => 'addition',
                        'quantity' => $quantity,
                        'warehouse_id' => $warehouse,
                        'option_id' => $key_pfinished_option_id,
                        'serial_no' => NULL,
                        'avg_cost' => $pfinished_data->cost,
                        'adjustment_cost' => ($pfinished_data->cost),
                      );
                    $productsNames[$key_pfinished_product_id] = $pfinished_data->name;
                }
            }
            $adjustment_data = array(
                'date' => date('Y-m-d H:i:s'),
                'warehouse_id' => $warehouse,
                'note' => $por->note,
                'created_by' => $this->session->userdata('user_id'),
                'companies_id' => $por->supplier_id,
                'biller_id' => $por->biller_id,
                'document_type_id' => $por->adjustment_doc_type_id,
                'origin_reference_no' => $por->id,
                'type_adjustment' => 3,
            );
            if (count($data) == 0) {
                $this->session->set_flashdata('error', 'Cantidades en 0');
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
            }
            // $this->sma->print_arrays($data);
        } elseif ($this->input->post('ok_quantity')) {
            $this->session->set_flashdata('error', validation_errors());
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
        }
        if ($this->form_validation->run() == true && $this->production_order_model->add_receipt($data)) {
            if ($this->products_model->addAdjustment($adjustment_data, $adjustment_products, $productsNames)) {
                $this->production_order_model->syncProductionOrderStatus($por->reference_no);
                $this->session->set_flashdata('message', lang("receipt_added"));
                admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'packing_orders');
            }
        }
        if (!$id || !$por) {
            $this->session->set_flashdata('error', lang('adjustment_not_found'));
            $this->sma->md();
        }
        $rows = $this->production_order_model->get_packingitems($id);
        $por_pid_setted = [];
        $por_rows = [];

        foreach ($rows as $row) {
            if (!isset($por_pid_setted[$row->production_order_pfinished_item_id][$row->option_id])) {
                $por_pid_setted[$row->production_order_pfinished_item_id][$row->option_id] = 1;
                // $por_rows[] = $this->production_order_model->get_poritems($row->production_order_id, $row->production_order_pfinished_item_id);
                $por_row = $this->production_order_model->get_poritems($row->production_order_id, $row->production_order_pfinished_item_id);
                $por_row->packing_quantity = $this->production_order_model->get_packed_items($por_row->product_id, $por_row->production_order_id, $row->option_id);
                $por_row->option_id = $row->option_id;
                $variant_data = $this->site->get_product_variant_by_id($row->option_id);
                $por_row->variant = $variant_data->name.$variant_data->suffix;
                $por_rows[] = $por_row;
            }
        }

        // $this->sma->print_arrays($rows);
        $this->data['rows'] = $rows;
        $this->data['por_rows'] = $por_rows;
        $this->data['created_by'] = $this->site->getUser($por->created_by);
        $this->data['warehouses'] = $this->site->getAllWarehouses();
        $this->load_view($this->theme.'production_order/change_packing_order_items_status', $this->data);
    }

    public function modal_receipts_view($receipt_id, $type, $reference_no)
    {
        $receipt = $this->production_order_model->get_receipts(NULL,$type,$receipt_id);
        $this->data['receipts'] = $receipt;
        $this->data['created_by'] = $this->site->getUser($receipt[0]->created_by);
        $this->data['type'] = $type;
        $this->data['reference_no'] = $reference_no;
        $this->load_view($this->theme.'production_order/modal_receipts_view', $this->data);
    }

    public function sync_production_order_status($reference_no)
    {
       $this->production_order_model->syncProductionOrderStatus($reference_no);
    }
    public function actions(){

        if ($this->input->post('val')) {
            $sql = "";
            foreach ($this->input->post('val') as $key => $value) {
                $sql.=$value.", ";
            }
            $sql = trim($sql, ", ");
            if ($this->input->post('form_action') == 'convert_to_assemble') {
                $ssql = "SELECT production_order_id FROM sma_cutting WHERE id IN (".$sql.") GROUP BY production_order_id";
                $q = $this->db->query($ssql);
                $data = [];
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $data[] = $row;
                    }
                }
                if (count($data) > 1) {
                    $this->session->set_flashdata('error', 'Los cortes seleccionados deben ser de una misma orden de confección');
                    admin_redirect('production_order/cutting_orders');
                } else {
                    $this->session->set_userdata('cuttings_ids', $sql);
                    admin_redirect('production_order/add_assemble_order/null/true');
                }
            } else if ($this->input->post('form_action') == 'convert_to_packing') {
                $ssql = "SELECT production_order_reference_no FROM sma_assemble WHERE id IN (".$sql.") GROUP BY production_order_reference_no";
                $q = $this->db->query($ssql);
                $data = [];
                if ($q->num_rows() > 0) {
                    foreach (($q->result()) as $row) {
                        $data[] = $row;
                    }
                }
                if (count($data) > 1) {
                    $this->session->set_flashdata('error', 'Los ensambles seleccionados deben ser de una misma orden de confección');
                    admin_redirect('production_order/assemble_orders');
                } else {
                    $this->session->set_userdata('assembles_ids', $sql);
                    admin_redirect('production_order/add_packing_order/null/true');
                }
            }
        }
    }

    public function add_new_notification($register_type, $register_id)
    {
        $this->form_validation->set_rules('description', lang("description"), 'required');
        if ($this->form_validation->run() == true) {
            $data = [
                        'detail' => $this->input->post('description'),
                        'register_type' => $register_type,
                        'register_id' => $register_id,
                        'created_by' => $this->session->userdata('user_id'),
                        'status' => 1,
                    ];
        }
        if ($this->form_validation->run() == true && $this->production_order_model->add_notification($data)) {
            $this->session->set_flashdata('message', lang('notificaton_added'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'production_order');
        } else if ($this->input->post('description')) { 
            $this->session->set_flashdata('error', (validation_errors()) ? validation_errors() : $this->session->flashdata('error'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'production_order');
        }
        if ($register_type == 1) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        } else if ($register_type == 2) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        } else if ($register_type == 3) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        } else if ($register_type == 4) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        }
        $this->data['created_by'] = $this->site->getUser($this->data['inv']->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($this->data['inv']->warehouse_id);
        $this->data['register_type'] = $register_type;
        $this->data['register_id'] = $register_id;
        $this->load_view($this->theme.'production_order/add_new_notification', $this->data);
    }

    public function edit_notification($register_id, $notification_id)
    {
        $notif_data = $this->production_order_model->get_notification_by_id($notification_id);
        $this->form_validation->set_rules('description', lang("description"), 'required');
        if ($this->form_validation->run() == true) {
            $data = [
                        'detail' => $this->input->post('description'),
                        'status' => $this->input->post('status'),
                    ];
        }
        if ($this->form_validation->run() == true && $this->production_order_model->update_notification($notification_id, $data)) {
            $this->session->set_flashdata('message', lang('notificaton_updated'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'production_order');
        } else if ($this->input->post('description')) { 
            $this->session->set_flashdata('error', (validation_errors()) ? validation_errors() : $this->session->flashdata('error'));
            admin_redirect(isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : 'production_order');
        }
        if ($notif_data->register_type == 1) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        } else if ($notif_data->register_type == 2) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        } else if ($notif_data->register_type == 3) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        } else if ($notif_data->register_type == 4) {
            $this->data['inv'] = $this->production_order_model->get_production_order_by_id($register_id);
            $this->data['rows'] = $this->production_order_model->get_poritems($register_id);
        }
        $this->data['created_by'] = $this->site->getUser($this->data['inv']->created_by);
        $this->data['warehouse'] = $this->site->getWarehouseByID($this->data['inv']->warehouse_id);
        $this->data['register_type'] = $notif_data->register_type;
        $this->data['register_id'] = $register_id;
        $this->data['notif_data'] = $notif_data;
        $this->load_view($this->theme.'production_order/edit_notification', $this->data);
    }
}