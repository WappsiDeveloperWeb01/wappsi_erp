<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= lang('adjustment_pos') ?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
        </style>
    </head>
    <body>
        <div id="wrapper">
            <div id="receiptData">
                <div class="row bold">
                    <div class="text-center bold">
                        <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                        <h3><?= lang('adjustment_pos') ?></h3>
                        <br>
                    </div>
                    <div class="col-xs-12">
                    <p class="bold">
                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv[0]->date); ?><br>
                        <?= lang("ref"); ?>: <?= $inv[0]->origin_reference_no; ?><br>
                    </p>
                    </div>
                    <div class="clearfix"></div>
                    <?php if (isset($cost_center) && $cost_center): ?>
                        <div class="col-xs-6">
                            <p class="bold">
                                <?= lang("cost_center"); ?> : <?= $cost_center->name ?> ( <?= $cost_center->code ?> )
                            </p>
                        </div>
                    <?php endif ?>
                </div>
                <div>
                    <div class="text-center">
                        <div class="table-responsive">
                            <table class="table table-bordered table-hover print-table order-table">
                                <thead>
                                    <tr>
                                        <th><?= lang("description"); ?></th>
                                        <th><?= lang("type"); ?></th>
                                        <th><?= lang("quantity"); ?></th>
                                    </tr>
                                </thead>

                                <tbody>
                                <?php $r = 1;
                                foreach ($rows[$inv[0]->id] as $row):
                                ?>
                                    <?php 
                                    if ($row->type == 'addition') {
                                        continue;
                                    }
                                     ?>
                                    <tr>
                                        <td style="vertical-align:left;">
                                            <?= $row->code.' - '.$row->name; ?>
                                            <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                        </td>
                                        <th><?= ($row->type == 'addition') ? lang('destination') : lang('origin') ?></th>
                                        <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatNumber($row->quantity); ?></td>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                                ?>
                                <?php $r = 1;
                                foreach ($rows[$inv[0]->id] as $row):
                                ?>
                                    <?php 
                                    if ($row->type == 'subtraction') {
                                        continue;
                                    }
                                     ?>
                                    <tr>
                                        <td style="vertical-align:left;">
                                            <?= $row->code.' - '.$row->name; ?>
                                            <?= $row->serial_no ? '<br>' . $row->serial_no : ''; ?>
                                        </td>
                                        <th><?= ($row->type == 'addition') ? lang('destination') : lang('origin') ?></th>
                                        <td style="width: 80px; text-align:center; vertical-align:middle;"><?= $this->sma->formatNumber($row->quantity); ?></td>
                                    </tr>
                                    <?php
                                    $r++;
                                endforeach;
                                ?>
                                </tbody>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">
                                <?php if ($inv[0]->note || $inv[0]->note != "") { ?>
                                    <div class="well well-sm">
                                        <p class="bold"><?= lang("note"); ?>:</p>
                                        <div><?= $this->sma->decode_html($inv[0]->note); ?></div>
                                    </div>
                                <?php } ?>
                            </div>

                            <div class="col-xs-12 pull-right">
                                <div class="well well-sm">
                                    <p>
                                        <?= lang("created_by"); ?>: <?= $created_by->first_name . ' ' . $created_by->last_name; ?> <br>
                                        <?= lang("date"); ?>: <?= $this->sma->hrld($inv[0]->date); ?>
                                    </p>
                                    <?php if ($inv[0]->updated_by) { ?>
                                    <p>
                                        <?= lang("updated_by"); ?>: <?= $updated_by->first_name . ' ' . $updated_by->last_name;; ?><br>
                                        <?= lang("update_at"); ?>: <?= $this->sma->hrld($inv[0]->updated_at); ?>
                                    </p>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
<script type="text/javascript">
    site = <?=json_encode(array('url' => base_url(), 'base_url' => admin_url(), 'assets' => $assets, 'settings' => $Settings, 'dateFormats' => $dateFormats))?>;
</script>
<script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
<script type="text/javascript">
        
    $(document).ready(function(){
        window.print();
        setTimeout(function() {
            window.close();
        }, 800);
    });

</script>