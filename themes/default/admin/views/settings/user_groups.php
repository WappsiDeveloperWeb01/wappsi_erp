<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <!-- <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li><a href="<?= admin_url('system_settings/create_group'); ?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <?= lang('add_group') ?></a></li>
                                    <li><a href="#" id="excelProducts" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                                    <li class="divider"></li>
                                    <li><a href="#" id="deleteGroups" data-action="delete"><i class="fa fa-trash-o"></i> <?= lang('delete_groups') ?></a></li>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="GPData" class="table table-hover reports-table">
                                    <thead>
                                        <tr>
                                            <th style="min-width:30px; width: 30px; text-align: center;">
                                                <input class="checkbox checkth" type="checkbox" name="check" />
                                            </th>
                                            <th><?= $this->lang->line("group_id"); ?></th>
                                            <th><?= $this->lang->line("group_name"); ?></th>
                                            <th><?= $this->lang->line("group_description"); ?></th>
                                            <th style="width:45px;"><?= $this->lang->line("actions"); ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($groups as $group) { ?>
                                            <tr>
                                                <td>
                                                    <div class="text-center"><input class="checkbox multi-select" type="checkbox" name="val[]" value="<?= $group->id ?>" /></div>
                                                </td>
                                                <td><?= $group->id; ?></td>
                                                <td><?= $group->name; ?></td>
                                                <td><?= $group->description; ?></td>
                                                <td style="text-align:center;">
                                                    <div class="btn-group text-left">
                                                        <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones">
                                                            <i class="fas fa-ellipsis-v fa-lg"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                            <li class="link_edit_sale">
                                                                <a class="tip" href="<?= admin_url('system_settings/permissions/' . $group->id) ?>"><i class="fa fa-tasks"></i> <?= $this->lang->line("change_permissions") ?></a>
                                                            </li>
                                                            <li class="print_document">
                                                                <a class="tip" data-toggle="modal" data-target="#myModal" href="<?= admin_url('system_settings/edit_group/' . $group->id) ?>"><i class="fa fa-edit"></i> <?= $this->lang->line("edit_group") ?></a>
                                                            </li>
                                                            <li class="link_invoice_action">
                                                                <?= '<a href="#" class="tip po" data-content="<p>' . lang('r_u_sure') . '</p><a class=\'btn btn-danger\' href=\'' . admin_url('system_settings/delete_group/' . $group->id) . '\'>' . lang('i_m_sure') . '</a> <button class=\'btn po-close\'>' . lang('no') . '</button>"><i class="fa fa-trash-o"></i> ' . $this->lang->line("change_permissions") . '</a>'; ?>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#GPData').dataTable({
            aaSorting: [
                [0, "asc"]
            ],
            aLengthMenu: [
                [10, 25, 50, 100, 500, -1],
                [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
            ],
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            iDisplayLength: <?= $Settings->rows_per_page ?>,
            oTableTools: {
                sSwfPath: "assets/media/swf/copy_csv_xls_pdf.swf",
                aButtons: ["csv", {
                    sExtends: "pdf",
                    sPdfOrientation: "landscape",
                    sPdfMessage: ""
                }, "print"]
            },
            aoColumns: [{
                bSortable: false
            }, null, null, null, {
                bSortable: false
            }],
            fnDrawCallback: function (oSettings) {
                $('.actionsButtonContainer').html(`<div class="pull-right dropdown">
                    <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button>
                    <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                        <li><a href="<?= admin_url('system_settings/create_group'); ?>" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus"></i> <?= lang('add_group') ?></a></li>
                        <li><a href="#" id="excelProducts" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a></li>
                        <li class="divider"></li>
                        <li><a href="#" id="deleteGroups" data-action="delete"><i class="fa fa-trash-o"></i> <?= lang('delete_groups') ?></a></li>
                    </ul>
                </div>`);
            }
        });
    });
</script>