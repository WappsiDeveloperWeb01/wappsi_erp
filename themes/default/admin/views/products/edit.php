<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
if (!empty($variants)) {
    foreach ($variants as $variant) {
        $vars[] = addslashes($variant->name);
    }
} else {
    $vars = array();
}
$preferences = array();
$taxes_json = [];
foreach ($tax_rates as $tax) {
    $taxes_json[$tax->id] = $tax;
}
?>
<style type="text/css">
    .img-deleting {
        border-style: dashed;
        border-width: 3px;
        border-color: #f27f8a;
    }
</style>
<script type="text/javascript">
    $(document).ready(function(){
        $("#edit_form").validate({
            ignore: []
        });
        var form = $(".wizard-big");
        var runValid = 1;
        setTimeout(function() {
            $('section[role="tabpanel"][aria-hidden="false"]').find('input[tabindex="1"]').focus();
            $('section[role="tabpanel"][aria-hidden="false"]').find('select[tabindex="1"]').focus();
            $('input[type="checkbox"]').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                increaseArea: '20%'
            });
        }, 550);
        wizard = form.steps({
            headerTag: "h1",
            titleTemplate: "#title#",
            enableAllSteps : true,
            bodyTag: "section",
            transitionEffect: "slideLeft",
            autoFocus: true,
             labels: {
                cancel: "Cancelar",
                current: "Paso actual:",
                pagination: "Paginación",
                finish: '<i class="fa-solid fa-check" data-toggle="tooltip" title="Guardar" data-placement="top"></i>',
                next: '<i class="fa-solid fa-angles-right" data-toggle="tooltip" title="Siguiente" data-placement="top"></i>',
                previous: '<i class="fa-solid fa-angles-left"  data-toggle="tooltip" title="Anterior" data-placement="top"></i>',
                loading: "Loading ..."
            },
            onStepChanging: function (event, currentIndex, newIndex)
            {
                if (runValid == 1) {
                  $('#edit_form').validate().settings.ignore = ":disabled,:hidden:not(.validate)";
                  return $('#edit_form').valid();
                } else {
                  return true;
                }
            },
            onStepChanged: function(event) {
              $('section:not(:hidden) select[required="required"]').addClass('validate');
              setTimeout(function() {
                $('select:not(:hidden)').select2();
              }, 300);
              $('section[role="tabpanel"][aria-hidden="false"]').find('input[tabindex="1"]').focus();
              $('section[role="tabpanel"][aria-hidden="false"]').find('select[tabindex="1"]').focus();

              $('.skip').iCheck('destroy');
            },
            onFinishing: function (event, currentIndex)
            {
                if (runValid == 1) {
                  $('#edit_form').validate().settings.ignore = ":disabled";
                  return $('#edit_form').valid();
                } else {
                  return true;
                }
            },
            onFinished: function (event, currentIndex)
            {
              if (runValid == 1) {
                $('#edit_form').validate().settings.ignore = ":disabled";
                if ($('#product_has_multiple_units').is(':checked') && $('input[name="product_unit_id"]').length == 0) {
                    header_alert('error', '¡Atención! Indicó que maneja multiples unidades, pero no definió ninguna.');
                    $('#steps-uid-0-t-1').trigger('click');
                    setTimeout(function() {
                        $('select[name="units"]').select2('open');
                    }, 850);
                } else {
                    if ($('#edit_form').valid()) {
                      $($('#edit_form')).submit();
                    }
                }
              }
            }
        });

        $('#edit_form ul[role="tablist"] li[role="tab"] a').attr('tabindex', -1);
        $("#add_item").autocomplete({
            source: function (request, response) {
              $.ajax({
                type: 'get',
                url: '<?=admin_url('products/suggestions');?>',
                dataType: "json",
                data: {
                  term: request.term,
                  ptype: $("#type").val(),
                },
                success: function (data) {
                  $(this).removeClass('ui-autocomplete-loading');
                  response(data);
                }
              });
            },
            minLength: 1,
            autoFocus: false,
            delay: 5,
            response: function (event, ui) {
                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    bootbox.alert('<?= lang('no_product_found') ?>', function () {
                        $('#add_item').focus();
                    });
                    $(this).val('');

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    if (ui.item.code == '<?= $product->code ?>') {
                        command: toastr.error('No puede llamar el mismo producto para pertenecer a los componentes', 'Error en componentes', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
                        $(this).val('');
                    } else {
                        var row = add_product_item(ui.item);
                        if (row) {
                            $(this).val('');
                            $('#add_item').removeAttr('required');
                            $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
                        }
                    }
                } else {
                    bootbox.alert('<?= lang('no_product_found') ?>');
                }
            }
        });
        $("#add_preference").autocomplete({
            source: function (request, response) {
            var ps = "";
            $.each($('input[name="preference_id[]"]'), function(index, input){
                ps+=$(input).val()+",";
            });
              $.ajax({
                type: 'get',
                url: '<?=admin_url('products/preferences_suggestions/'.$product->id);?>',
                dataType: "json",
                data: {
                  term: request.term,
                  ps: ps,
                  preference_category : $("#preference_category").val(),
                },
                success: function (data) {
                  $(this).removeClass('ui-autocomplete-loading');
                  response(data);
                }
              });
            },
            minLength: 1,
            autoFocus: false,
            delay: 250,
            response: function (event, ui) {

                if ($(this).val().length >= 16 && ui.content[0].id == 0) {
                    //audio_error.play();
                    command: toastr.error('No se encontró preferencia', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "8000",
                        "extendedTimeOut": "1000",
                    });
                    setTimeout(function() {
                        $('#add_preference').focus();
                    }, 850);
                }
                else if (ui.content.length == 1 && ui.content[0].id != 0) {
                    ui.item = ui.content[0];
                    $(this).data('ui-autocomplete')._trigger('select', 'autocompleteselect', ui);
                    $(this).autocomplete('close');
                    $(this).removeClass('ui-autocomplete-loading');
                }
                else if (ui.content.length == 1 && ui.content[0].id == 0) {
                    //audio_error.play();
                    command: toastr.error('No se encontró preferencia', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "8000",
                        "extendedTimeOut": "1000",
                    });
                        $('#add_preference').focus();

                }
            },
            select: function (event, ui) {
                event.preventDefault();
                if (ui.item.id !== 0) {
                    var add = true;
                    $.each($('input[name="preference_id[]"]'), function(index, input){
                        if ($(input).val() == ui.item.id) {
                            add = false;
                        }
                    });
                    if (add) {
                        $('#preferencesTable').show().append('<tr>'+
                                                                '<td>'+
                                                                    '<input type="hidden" name="preference_id[]" value="' + ui.item.id + '">'+
                                                                    '<span>' + ui.item.name + '</span>'+
                                                                '</td>'+
                                                                '<td class="text-center">'+
                                                                    '<i class="fa fa-times delAttr"></i>'+
                                                                '</td>'+
                                                            '</tr>');
                        $("#add_preference").val('');
                    }
                } else {
                    //audio_error.play();
                    command: toastr.error('No se encontró preferencia', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "8000",
                        "extendedTimeOut": "1000",
                    });
                    setTimeout(function() {
                        $('#add_preference').focus();
                    }, 850);
                }
            }
        });

        setTimeout(function() {
            $('#sale_tax_rate_2_id').trigger('change');
            $('#purchase_tax_rate_2_id').trigger('change');
            $('#cost').trigger('change');
        }, 1200);
    });
    var tax_rates = JSON.parse('<?= json_encode($taxes_json) ?>');
    $(document).ready(function () {
        $('.gen_slug').change(function(e) {
            getSlug($(this).val(), 'products');
        });
        $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>").select2({
            placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>", data: [
                {id: '', text: '<?= sprintf(lang('select_category_to_load'), lang('category')) ?>'}
            ]
        });

        $("#second_level_subcategory_id").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_subcategory_to_load'), lang('subcategory')) ?>").select2({
            placeholder: "<?= sprintf(lang('select_subcategory_to_load'), lang('subcategory')) ?>", data: [
                {id: '', text: '<?= sprintf(lang('select_subcategory_to_load'), lang('subcategory')) ?>'}
            ]
        });
        $('#category').change(function () {
            var v = $(this).val();
            var suggested_rate = $('#category option:selected').data('suggestedrate');
            $('#modal-loading').show();

            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubCategories') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            scdata.push({id: '', text: '<?= sprintf(lang('select_subcategory'), lang('subcategory')) ?>'});
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_subcategory'), lang('subcategory')) ?>").select2({
                                placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                                minimumResultsForSearch: 7,
                                data: scdata
                            }).trigger('change');
                        } else {
                            $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>").select2({
                                placeholder: "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>",
                                minimumResultsForSearch: 7,
                                data: [{id: '', text: '<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>'}]
                            });
                        }
                        setTimeout(function() {

                            $("#subcategory").trigger('change');
                        }, 850);
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            } else {
                $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>").select2({
                    placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                    minimumResultsForSearch: 7,
                    data: [{id: '', text: '<?= sprintf(lang('select_category_to_load'), lang('category')) ?>'}]
                });
            }

            if (suggested_rate) {
                $('#tax_rate').select2('val', suggested_rate);
            } else {
                $('#tax_rate').select2('val', "<?= $product->tax_rate ?>");
            }

            $('#modal-loading').hide();
        });
        $('#subcategory').change(function () {
            var v = $(this).val();
            $('#modal-loading').show();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubCategoriesSecondLevel') ?>/" + v,
                    dataType: "json",
                    success: function (scdata) {
                        if (scdata != null) {
                            scdata.push({id: '', text: '<?= lang('select') ?>'});
                            $("#second_level_subcategory_id").select2("destroy").empty().attr("placeholder", "<?= lang('select') ?>").select2({
                                placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                                minimumResultsForSearch: 7,
                                data: scdata
                            });
                        } else {
                            $("#second_level_subcategory_id").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>").select2({
                                placeholder: "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>",
                                minimumResultsForSearch: 7,
                                data: [{id: '', text: '<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>'}]
                            });
                        }
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                        $('#modal-loading').hide();
                    }
                });
            }
            $('#modal-loading').hide();
        });
        $('#code').bind('keypress', function (e) {
            if (e.keyCode == 13) {
                e.preventDefault();
                return false;
            }
        });
        $('#price').trigger('change');
    });
</script>

<?= admin_form_open_multipart("products/edit/" . $product->id, ['id'=>'edit_form','class'=>'']); ?>
<input type="hidden" name="deleted_images" id="deleted_images">
<input type="hidden" name="deleted_main_image" id="deleted_main_image" value="0">
<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row" id="sortable-view">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="wizard-big wizard">
                        <h1  tabindex="-1"><?= lang('main_data') ?></h1>
                        <section>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <?= lang("product_type", "type") ?>
                                    <?php
                                    $opts = array('standard' => lang('standard'), 'combo' => lang('combo'), 'digital' => lang('digital'), 'service' => lang('service'), 'raw' => lang('raw'), 'subproduct' => lang('subproduct'), 'pfinished' => lang('pfinished'));
                                    echo form_dropdown('type', $opts, (isset($_POST['type']) ? $_POST['type'] : ($product ? $product->type : '')), 'class="form-control" '.$this->sma->set_tabindex().' id="type" required="required"');
                                    ?>
                                </div>
                                <div class="form-group all col-md-3">
                                    <?= lang("product_code", "code") ?> <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="bottom" title="<?= lang('you_scan_your_barcode_too') ?>"></i>
                                    <div class="input-group">
                                        <?= form_input('code', (isset($_POST['code']) ? $_POST['code'] : ($product ? $product->code : '')), 'class="form-control validate_code" data-module="product" data-id="'.$product->id.'" id="code"  required="required"'.($product->product_code_consecutive == 1 ? "readonly" : "").$this->sma->set_tabindex()) ?>
                                        <?php if ($product->product_code_consecutive == 0): ?>
                                            <span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('random_code_text') ?>">
                                                <i class="fa fa-random"></i>
                                            </span>
                                            <span class="input-group-addon pointer" id="consecutive_code" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('consecutive_code_text') ?>">
                                                <b>123</b>
                                            </span>
                                        <?php else: ?>
                                            <span class="input-group-addon pointer" style="padding: 1px 10px;" data-toggle="tooltip" title="<?= lang('code_blocked_by_consecutive') ?>">
                                                <i class="fa-solid fa-ban"></i>
                                            </span>
                                        <?php endif ?>

                                    </div>
                                    <input type="hidden" name="product_code_consecutive" class="code_consecutive_setted">
                                </div>
                                <div class="form-group all col-md-6">
                                    <?= lang("product_name", "name") ?>
                                    <?= form_input('name', (isset($_POST['name']) ? $_POST['name'] : ($product ? $product->name : '')), 'class="form-control gen_slug" id="name" required="required"'.$this->sma->set_tabindex()); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group all col-md-3">
                                    <?= lang("reference", "reference") ?>
                                    <?= form_input('reference', (isset($_POST['reference']) ? $_POST['reference'] : ($product ? $product->reference : '')), 'class="form-control" id="reference"'.$this->sma->set_tabindex()); ?>
                                </div>
                                <div class="form-group all col-md-3">
                                    <?= lang('slug', 'slug'); ?>
                                    <?= form_input('slug', set_value('slug', ($product ? $product->slug : '')), 'class="form-control tip" id="slug" required="required"'.$this->sma->set_tabindex()); ?>
                                </div>
                                <div class="form-group all col-md-3">
                                    <?= lang('second_name', 'second_name'); ?>
                                    <?= form_input('second_name', set_value('second_name', ($product ? $product->second_name : '')), 'class="form-control tip" id="second_name"'.$this->sma->set_tabindex()); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group all col-md-3">
                                    <?= lang("barcode_symbology", "barcode_symbology") ?>
                                    <?php
                                    $bs = array('code25' => 'Code25', 'code39' => 'Code39', 'code128' => 'Code128', 'ean8' => 'EAN8', 'ean13' => 'EAN13', 'upca' => 'UPC-A', 'upce' => 'UPC-E');
                                    echo form_dropdown('barcode_symbology', $bs, (isset($_POST['barcode_symbology']) ? $_POST['barcode_symbology'] : ($product ? $product->barcode_symbology : 'code128')), 'class="form-control select" id="barcode_symbology" required="required" style="width:100%;"'.$this->sma->set_tabindex());
                                    ?>
                                </div>
                                <div class="form-group all col-md-3">
                                    <?= lang("brand", "brand") ?>
                                    <?php
                                    $br[''] = "";
                                    foreach ($brands as $brand) {
                                        $br[$brand->id] = $brand->name;
                                    }
                                    echo form_dropdown('brand', $br, (isset($_POST['brand']) ? $_POST['brand'] : ($product ? $product->brand : '')), 'class="form-control select validate" id="brand" placeholder="' . lang("select") . " " . lang("brand") . '" style="width:100%" required="required"'.$this->sma->set_tabindex())
                                    ?>
                                </div>
                                <div class="form-group all col-md-3">
                                    <?= lang("category", "category") ?>
                                    <select name="category" class="form-control select validate" id="category" placeholder="<?= lang('select') . lang('category') ?>" required="required" style="width:100%" <?= $this->sma->set_tabindex() ?>>
                                        <option value=""><?= lang('select') ?></option>
                                        <?php foreach ($categories as $category): ?>
                                            <option value="<?= $category->id ?>" data-suggestedrate="<?= $category->suggested_rate ?>" <?= $category->id == $product->category_id ? "selected" : "" ?>><?= $category->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group all col-md-3">
                                    <?= lang("subcategory", "subcategory") ?>
                                    <div class="controls" id="subcat_data"> <?php
                                        echo form_input('subcategory', ($product ? $product->subcategory_id : ''), 'class="form-control" id="subcategory"  placeholder="' . sprintf(lang('select_category_to_load'), lang('category')) . '"'.$this->sma->set_tabindex());
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group all col-md-3">
                                    <?= lang("second_level_subcategory_id", "second_level_subcategory_id") ?>
                                    <div class="controls" id="subcat_data"> <?php
                                        echo form_input('second_level_subcategory_id', ($product ? $product->second_level_subcategory_id : ''), 'class="form-control" id="second_level_subcategory_id"  placeholder="' . sprintf(lang('select_category_to_load'), lang('category')) . '"'.$this->sma->set_tabindex());
                                        ?>
                                    </div>
                                </div>
                                <?php if ($Settings->tax1) { ?>
                                    <div class="form-group all col-md-3">
                                        <?= lang('product_tax', "tax_rate") ?>
                                        <?php
                                        $tr[""] = "";
                                        foreach ($tax_rates as $tax) {
                                            $tr[$tax->id] = $tax->name;
                                        }
                                        echo form_dropdown('tax_rate', $tr, (isset($_POST['tax_rate']) ? $_POST['tax_rate'] : ($product ? $product->tax_rate : $Settings->default_tax_rate)), 'class="form-control select" id="tax_rate" placeholder="' . lang("select") . ' ' . lang("product_tax") . '" style="width:100%"'.$this->sma->set_tabindex());
                                        ?>
                                    </div>
                                    <div class="form-group all col-md-3">
                                        <?= lang("tax_method", "tax_method") ?>
                                        <?php

                                        $current_tax_method = $this->Settings->tax_method != 2 ? $this->Settings->tax_method : NULL;

                                        if ($current_tax_method == NULL) {
                                            if ($product) {
                                                $current_tax_method = $product->tax_method;
                                            } else if (isset($_POST['tax_method'])) {
                                                $current_tax_method = $_POST['tax_method'];
                                            }
                                        }


                                        $tm = array('0' => lang('inclusive'), '1' => lang('exclusive'));
                                        echo form_dropdown('tax_method', $tm, $current_tax_method, 'class="form-control select" id="tax_method" placeholder="' . lang("select") . ' ' . lang("tax_method") . '" style="width:100%"'.$this->sma->set_tabindex())
                                        ?>
                                    </div>
                                <?php } ?>
                                <div class="form-group standard col-md-3">
                                    <?= lang("alert_quantity", "alert_quantity") ?>
                                    <div
                                        class="input-group"> <?= form_input('alert_quantity', (isset($_POST['alert_quantity']) ? $_POST['alert_quantity'] : ($product ? $this->sma->formatDecimal($product->alert_quantity) : '')), 'class="form-control tip" id="alert_quantity"') ?>
                                        <span class="input-group-addon">
                                        <input type="checkbox" name="track_quantity" id="inlineCheckbox1"
                                               value="1" <?= ($product ? (!empty($product->track_quantity) ? 'checked="checked"' : '') : 'checked="checked"') ?> <?= $this->sma->set_tabindex() ?>>
                                    </span>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group all col-md-3">
                                    <?php
                                    $color_options[''] = lang('select');
                                    if ($colors) {
                                        foreach ($colors as $color) {
                                            $color_options[$color->id] = $color->name;
                                        }
                                    }
                                    if ($materials) {
                                        foreach ($materials as $material) {
                                            $material_options[$material->id] = $material->name;
                                        }
                                    }
                                    $material_options[''] = lang('select');
                                     ?>
                                    <?= lang("color", "color") ?>
                                    <?php $productColorId = !empty($product->color_id) ? $product->color_id : ''; ?>
                                    <?= form_dropdown('color', $color_options, $productColorId, 'class="form-control select" id="color" placeholder="' . lang("select") . ' ' . lang("color") . '" style="width:100%"'.$this->sma->set_tabindex()) ?>
                                </div>
                                <div class="form-group all col-md-3">
                                    <?= lang("material", "material") ?>
                                    <?php $productMaterialId = !empty($product->material_id) ? $product->material_id : ''; ?>
                                    <?= form_dropdown('material', $material_options, $productMaterialId, 'class="form-control select" id="material" placeholder="' . lang("select") . ' ' . lang("material") . '" style="width:100%"'.$this->sma->set_tabindex()) ?>
                                </div>
                                <div class="form-group one_unit col-md-3">
                                    <?= lang('warranty_time', 'warranty_time'); ?>
                                    <input type="number" name="warranty_time" id="warranty_time" class="form-control" value="<?= $product->warranty_time ?>">
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <?= form_label($this->lang->line("tags"), "tags") ?>
                                        <?php
                                            $tagsOptions = [];
                                            if (!empty($tags)) {
                                                foreach ($tags as $tag) {
                                                    $tagsOptions[$tag->id] = $tag->description;
                                                }
                                            }
                                            $productTags = (!empty($product->tags)) ? json_decode($product->tags) : [];
                                        ?>
                                        <?= form_dropdown(["name"=>"tags[]", "id"=>"tags", "class"=>"form-control select", "multiple"=>TRUE, "tabindex"=>$this->sma->set_tabindex(false)], $tagsOptions, $productTags) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <?php if ($this->pos_settings->balance_settings == 1): ?>
                                    <div class="form-group all col-md-3">
                                        <?= lang("paste_balance_value", "paste_balance_value") ?>
                                        <?php
                                        $balance_options = array('0' => lang('no'), '1' => lang('yes'));
                                        echo form_dropdown('paste_balance_value', $balance_options, $product->paste_balance_value, 'class="form-control select" id="paste_balance_value" placeholder="' . lang("select") . ' ' . lang("paste_balance_value") . '" style="width:100%"'.$this->sma->set_tabindex())
                                        ?>
                                    </div>
                                <?php endif ?>
                                <?php if ($this->Settings->aiu_management == 1): ?>
                                    <div class="form-group all col-md-3">
                                        <?= lang("aiu_product", "aiu_product") ?>
                                        <?php
                                        $balance_options = array('0' => lang('no'), '1' => lang('yes'));
                                        echo form_dropdown('aiu_product', $balance_options, $product->aiu_product, 'class="form-control select" id="aiu_product" placeholder="' . lang("select") . ' ' . lang("aiu_product") . '" style="width:100%"'.$this->sma->set_tabindex())
                                        ?>
                                    </div>
                                <?php endif ?>
                            </div>
                            <?php if ($this->Owner): ?>
                                <div class="row">
                                    <div class="col-md-3">
                                        <?= lang('wappsi_invoicing_product', 'wappsi_invoicing') ?>
                                        <?php
                                        $wappsi_invoicing_options = array('0' => lang('no'), '1' => lang('yes'));
                                        echo form_dropdown('wappsi_invoicing', $wappsi_invoicing_options, $product->wappsi_invoicing, 'class="form-control select" id="wappsi_invoicing" placeholder="' . lang("select") . ' ' . lang("wappsi_invoicing") . '" style="width:100%"'.$this->sma->set_tabindex())
                                        ?>
                                    </div>
                                    <div class="col-md-3 div_wappsi_invoicing" style="display:none;">
                                        <?= lang('wappsi_invoicing_type', 'wappsi_invoicing_type') ?>
                                        <?php
                                        $wappsi_invoicing_type_options = [
                                            1 => lang('wp_type_1'),
                                            2 => lang('wp_type_2'),
                                            3 => lang('wp_type_3'),
                                            4 => lang('wp_type_4'),
                                            5 => lang('wp_type_5'),
                                            6 => lang('wp_type_6'),
                                            7 => lang('wp_type_7'),
                                            8 => lang('wp_type_8'),
                                            9 => lang('wp_type_9'),
                                        ];
                                        echo form_dropdown('wappsi_invoicing_type', $wappsi_invoicing_type_options, $product->wappsi_invoicing_type, 'class="form-control select" id="wappsi_invoicing_type" placeholder="' . lang("select") . ' ' . lang("wappsi_invoicing_type") . '" style="width:100%"'.$this->sma->set_tabindex())
                                        ?>
                                    </div>
                                    <div class="col-md-3 div_wappsi_invoicing" style="display:none;">
                                        <?= lang('wappsi_invoicing_quantity', 'wappsi_invoicing_quantity') ?>
                                        <?php echo form_input('wappsi_invoicing_quantity', $product->wappsi_invoicing_quantity, 'class="form-control ttip" id="wappsi_invoicing_quantity" placeholder="' . $this->lang->line("wappsi_invoicing_quantity") . '"'.$this->sma->set_tabindex()); ?>
                                    </div>
                                    <div class="col-md-3 div_wappsi_invoicing" style="display:none;">
                                        <?= lang('wappsi_invoicing_frecuency', 'wappsi_invoicing_frecuency') ?>
                                        <select name="wappsi_invoicing_frecuency" id="wappsi_invoicing_frecuency" class="form-control" <?= $this->sma->set_tabindex(); ?>>
                                            <option value=""><?= lang('select') ?></option>
                                            <option value="30" <?= $product->wappsi_invoicing_frecuency == 30 ? 'selected' : '' ?>><?= lang('monthly') ?></option>
                                            <option value="365" <?= $product->wappsi_invoicing_frecuency == 365 ? 'selected' : '' ?>><?= lang('annually') ?></option>
                                        </select>
                                    </div>
                                    <div class="form-group one_unit col-md-3">
                                        <?= lang('birthday_discount', 'birthday_discount'); ?>
                                        <input type="text" name="birthday_discount" id="birthday_discount" class="form-control" value="<?= $product->birthday_discount ?>" pattern="\d+%">
                                    </div>
                                </div>
                            <?php endif ?>
                            <div class="row">
                                <div class="form-group all col-md-6">
                                    <?= lang("product_details", "product_details") ?>
                                    <?= form_textarea('product_details', (isset($_POST['product_details']) ? $_POST['product_details'] : ($product ? $product->product_details : '')), 'class="form-control" id="product_details"'.$this->sma->set_tabindex()); ?>
                                </div>
                                <div class="form-group all col-md-6">
                                    <?= lang("product_details_for_invoice", "details") ?>
                                    <?= form_textarea('details', (isset($_POST['details']) ? $_POST['details'] : ($product ? $product->details : '')), 'class="form-control" id="details"'.$this->sma->set_tabindex()); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="combo" style="display:none;">
                                        <div class="form-group">
                                            <?= lang("add_product", "add_item") . ' (' . lang('not_with_variants') . ')'; ?>
                                            <?php echo form_input('add_item', '', 'class="form-control ttip" id="add_item" data-placement="top" data-trigger="focus" data-bv-notEmpty-message="' . lang('please_add_items_below') . '" placeholder="' . $this->lang->line("add_item") . '"'.$this->sma->set_tabindex()); ?>
                                        </div>
                                        <div class="control-group table-group">
                                            <label class="table-label title-text-ci" for="combo"><?= lang("combo_products"); ?></label>
                                            <div class="controls table-controls">
                                                <table id="prTable"
                                                       class="table items  table-bordered table-condensed table-hover">
                                                    <thead>
                                                    <tr>
                                                        <th class=""><?= lang('product') . ' (' . lang('code') .' - '.lang('name') . ')'; ?></th>
                                                        <th class=""><?= lang("unit_cost"); ?></th>
                                                        <th class=""><?= lang("unit"); ?></th>
                                                        <th class=""><?= lang("quantity"); ?></th>
                                                        <th class=""><?= lang("total"); ?></th>
                                                        <th class="">
                                                            <i class="fa fa-trash-o" style="opacity:0.5; filter:alpha(opacity=50);"></i>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody></tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <?php if ($custom_fields): ?>
                                        <hr class="col-md-11">
                                        <?php foreach ($custom_fields as $id => $arr): ?>
                                            <?php
                                            $cf = $arr['data'];
                                            $cf_values = isset($arr['values']) ? $arr['values'] : NULL;
                                            ?>
                                            <div class="col-md-3 form-group">
                                                <input type="hidden" name="custom_field_code[]" value="<?= $cf->cf_code ?>">
                                                <label><?= $cf->cf_name ?></label>
                                                <?php if ($cf->cf_type == 'select' || $cf->cf_type == 'multiple'): ?>
                                                    <select name="custom_field_value[]" class="form-control" <?= $cf->cf_type == 'multiple' ? "multiple" : "" ?> <?= $this->sma->set_tabindex() ?>>
                                                        <?php if ($cf_values): ?>
                                                        <option value="">Seleccione...</option>
                                                        <?php foreach ($cf_values as $cfv): ?>
                                                            <?php
                                                                $selected = "";
                                                                if ($cf->cf_type == 'multiple') {
                                                                    $selected = strpos($product->{$cf->cf_code}, $cfv->cf_value) !== false ? "selected" : "";
                                                                } else {
                                                                    $selected = $product->{$cf->cf_code} == $cfv->cf_value ? "selected" : "";
                                                                }
                                                            ?>
                                                            <option value="<?= $cfv->cf_value ?>" <?= $selected ?>><?= $cfv->cf_value ?></option>
                                                        <?php endforeach ?>
                                                        <?php else: ?>
                                                        <option value="">Sin valores para el campo</option>
                                                        <?php endif ?>
                                                    </select>
                                                <?php endif ?>
                                                <?php if ($cf->cf_type == 'date' || $cf->cf_type == 'text' || $cf->cf_type == 'input'): ?>
                                                    <input type="<?= $cf->cf_type ?>" name="custom_field_value[]" class="form-control" <?= $product->{$cf->cf_code} ? "value='".$product->{$cf->cf_code}."'" : "" ?> <?= $this->sma->set_tabindex() ?>>
                                                <?php endif ?>
                                            </div>
                                        <?php endforeach ?>
                                    <?php endif ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="well">
                                        <?php if ($warehouses_products): ?>
                                            <h4><?= lang('product_location') ?></h4>
                                            <?php foreach ($warehouses_products as $warehouse): ?>
                                                <div class="form-group">
                                                    <label><?= $warehouse->name ?></label> <em><?= lang('quantity') ?> : <?= $this->sma->formatQuantity($warehouse->quantity) ?></em>
                                                    <?php
                                                    $wh_location = explode(" / ", $warehouse->warehouse_location ?? '');
                                                     ?>
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <input type="text" name="warehouse_zone[<?= $warehouse->id ?>]" class="form-control" placeholder="<?= lang('warehouse_zone') ?>" value="<?= isset($wh_location[0]) ? $wh_location[0] : '' ?>" data-toggle="tooltip" title="<?= lang('warehouse_zone') ?>" <?= $this->sma->set_tabindex() ?>>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" name="warehouse_rack[<?= $warehouse->id ?>]" class="form-control" placeholder="<?= lang('warehouse_rack') ?>" value="<?= isset($wh_location[1]) ? $wh_location[1] : '' ?>" data-toggle="tooltip" title="<?= lang('warehouse_rack') ?>" <?= $this->sma->set_tabindex() ?>>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <input type="text" name="warehouse_level[<?= $warehouse->id ?>]" class="form-control" placeholder="<?= lang('warehouse_level') ?>" value="<?= isset($wh_location[2]) ? $wh_location[2] : '' ?>" data-toggle="tooltip" title="<?= lang('warehouse_level') ?>" <?= $this->sma->set_tabindex() ?>>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h1  tabindex="-1"><?= lang('units') ?></h1>
                        <section>
                            <?php $this->sma->reset_tabindex(); ?>
                            <div class="row">
                                <div class="form-group standard col-md-3" style="margin-top: 1.8%;">
                                    <label>
                                        <input type="checkbox" name="product_has_multiple_units" id="product_has_multiple_units" <?= $this->sma->set_tabindex() ?>>
                                        <?= lang('product_has_multiple_units') ?>
                                    </label>
                                </div>
                                <div class="form-group one_unit col-md-3">
                                    <?= lang('product_unit', 'unit'); ?>
                                    <select name="unit" class="form-control tip" id="unit" required <?= $this->sma->set_tabindex() ?>>
                                        <option value=""><?= lang('select') ?></option>
                                        <?php foreach ($units as $unt): ?>
                                            <?php if ($unt->base_unit) {
                                                continue;
                                            } ?>
                                            <option value="<?= $unt->id ?>" data-operationvalue="<?= $unt->operation_value ?>" <?= $product && $product->unit ==  $unt->id ? 'selected="selected"' : '' ?>><?= $unt->name." (".$unt->code.")" ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                                <div class="form-group standard col-md-3 div_default_sale_unit">
                                    <?= lang('default_sale_unit', 'default_sale_unit'); ?>
                                    <?php
                                    $uopts[''] = lang('select').' '.lang('unit');
                                    foreach ($subunits as $sunit) {
                                        $uopts[$sunit->id] = $sunit->name .' ('.$sunit->code.')';
                                    }
                                    ?>
                                    <?= form_dropdown('default_sale_unit', $uopts, $product->sale_unit, 'class="form-control" id="default_sale_unit" style="width:100%;"'.$this->sma->set_tabindex()); ?>
                                </div>
                                <div class="form-group standard col-md-3 div_default_purchase_unit">
                                    <?= lang('default_purchase_unit', 'default_purchase_unit'); ?>
                                    <?= form_dropdown('default_purchase_unit', $uopts, $product->purchase_unit, 'class="form-control" id="default_purchase_unit" style="width:100%;"'.$this->sma->set_tabindex()); ?>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group one_unit col-md-3">
                                    <?= lang('sale_min_qty', 'sale_min_qty'); ?>
                                    <input type="number" name="sale_min_qty" id="sale_min_qty" class="form-control" value="<?= ($product->sale_min_qty && $product->sale_min_qty > 0) ? $product->sale_min_qty : 1 ?>" <?= $this->sma->set_tabindex() ?>>
                                </div>
                                <div class="form-group one_unit col-md-3">
                                    <?= lang('sale_max_qty', 'sale_max_qty'); ?>
                                    <input type="number" name="sale_max_qty" id="sale_max_qty" class="form-control" value="<?= $product->sale_max_qty ?>" <?= $this->sma->set_tabindex() ?>>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group multiple_units col-md-12" style="display: none;">
                                    <?php
                                    $unit_options = [];
                                    foreach ($units as $pg) {
                                         $unit_options[$pg->id] = "(".$pg->code.") ".$pg->name.", Factor: ".$this->sma->formatQuantity($pg->operation_value > 0 ? $pg->operation_value : 1);
                                    }
                                    ?>
                                     <div class="input-group">
                                        <select class="form-control" name="units" multiple <?= $this->sma->set_tabindex() ?>>
                                            <?php foreach ($unit_options as $id => $val): ?>
                                                <option value="<?= $id ?>"> <?= $val ?> </option>
                                            <?php endforeach ?>
                                        </select>
                                         <div class="input-group-addon" style="padding: 2px 5px;">
                                             <a href="#" id="add_item_units">
                                                <i class="fa fa-2x fa-plus-circle"></i>
                                            </a>
                                         </div>
                                     </div>
                                    <table class="table" id="table_units">
                                        <thead>
                                            <tr>
                                                <th style="width: 3.88%;">Base</th>
                                                <th style="width: 3.88%;"><?= lang('sale') ?></th>
                                                <th style="width: 3.88%;"><?= lang('purchases') ?></th>
                                                <th style="width: 21.66%;"><?= lang('name') ?></th>
                                                <th style="width: 15.66%; <?= $this->Settings->prioridad_precios_producto == 11 ? "display: none;" : "" ?>"><?= lang('base_unit') ?></th>
                                                <th style="width: 15.66%;"><?= lang('factor') ?></th>
                                                <?php if ($this->Settings->prioridad_precios_producto == 7): ?>
                                                    <th style="width: 21.66%;"><?= lang('margin_update_price') ?></th>
                                                <?php endif ?>
                                                <?php if ($this->Settings->prioridad_precios_producto == 11): ?>
                                                    <th style="width: 10%;"><?= lang('price_group') ?></th>
                                                    <th style="width: 23.66%;"><?= lang('product_unit_price') ?></th>
                                                <?php else: ?>
                                                    <th style="width: 33.66%;"><?= lang('product_unit_price') ?></th>
                                                <?php endif ?>
                                                <th style="width: 1.66%;"><?= lang('active') ?></th>
                                                <th style="width: 1.66%;"></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php if ($this->Settings->prioridad_precios_producto == 11): ?>
                                                <?php foreach ($units as $unit): ?>
                                                    <?php foreach ($price_groups as $key => $pg): ?>
                                                        <?php
                                                            if (!isset($hybrid_prices[$product->id][$unit->id])) {
                                                                continue;
                                                            }
                                                         ?>
                                                        <tr class='row_id_<?= $unit->id ?> row_unit'>
                                                            <td>
                                                                <input type='radio' name='product_unit_id' value='<?= $unit->id ?>' data-operationvalue='<?= $unit->operation_value ?>' class="skip" <?= $unit->id == $product->unit ? 'checked="checked"' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                                            </td>
                                                            <td>
                                                                <input type='radio' name='multiple_default_sale_unit' value='<?= $unit->id ?>' data-operationvalue='<?= $unit->operation_value ?>' class="skip" <?= $unit->id == $product->sale_unit ? 'checked="checked"' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                                            </td>
                                                            <td>
                                                                <input type='radio' name='multiple_default_purchase_unit' value='<?= $unit->id ?>' data-operationvalue='<?= $unit->operation_value ?>' class="skip" <?= $unit->id == $product->purchase_unit ? 'checked="checked"' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                                            </td>
                                                            <td>
                                                                <?= $unit->name ?>
                                                            </td>
                                                            <td style="display: none;">

                                                            </td>
                                                            <td>
                                                                <?= $this->sma->formatDecimal($unit->operation_value) ?>
                                                            </td>
                                                            <th><?= $pg->name ?></th>
                                                            <td>
                                                                <input type="text"
                                                                    name="hybrid_price[<?= $unit->id ?>][<?= $pg->id ?>]" class="form-control not_only_number pg_price"
                                                                    value="<?= isset($hybrid_prices[$product->id][$unit->id][$pg->id]) ? $this->sma->formatDecimal($hybrid_prices[$product->id][$unit->id][$pg->id] + ($this->Settings->ipoconsumo ? $product->consumption_sale_tax : 0)) : 0 ?>"
                                                                     data-pgbase="<?= isset($pg->price_group_base) ? $pg->price_group_base : null ?>" data-unitid="<?= $unit->id ?>"
                                                                     <?= $this->sma->set_tabindex() ?>
                                                                     <?= (isset($pg->price_group_base) && $pg->price_group_base && $this->Settings->allow_zero_price_products == 0) ? " min='1' " : "" ?>
                                                                     >
                                                            </td>
                                                            <td>
                                                                <?php if ($unit->id != $product->unit): ?>
                                                                    <input type="checkbox" name="unit_status"  class="form-control" <?= $this->sma->set_tabindex() ?>>
                                                                <?php endif ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($unit->id != $product->unit): ?>
                                                                    <span class='fa fa-times' id='delete_unit' data-rowid='<?= $unit->id ?>'></span>
                                                                <?php endif ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach ?>
                                                <?php endforeach ?>
                                            <?php else: ?>
                                                <?php $cnt = 0;
                                                if (isset($product_unit_prices) && $product_unit_prices): ?>
                                                    <?php foreach ($product_unit_prices as $product_unit_price): ?>
                                                        <tr class='row_id_<?= $product_unit_price->unit_id ?> row_unit'>
                                                            <td>
                                                                <input type='radio' name='product_unit_id' value='<?= $product_unit_price->unit_id ?>' data-operationvalue='<?= $product_unit_price->unit_operation_value ?>' class="skip" <?= $product_unit_price->unit_id == $product->unit ? 'checked="checked"' : '' ?> <?= $product_unit_price->unit_operation_value > 1 ? 'disabled' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                                            </td>
                                                            <td>
                                                                <input type='radio' name='multiple_default_sale_unit' value='<?= $product_unit_price->unit_id ?>' data-operationvalue='<?= $product_unit_price->unit_operation_value ?>' class="skip" <?= $product_unit_price->unit_id == $product->sale_unit ? 'checked="checked"' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                                            </td>
                                                            <td>
                                                                <input type='radio' name='multiple_default_purchase_unit' value='<?= $product_unit_price->unit_id ?>' data-operationvalue='<?= $product_unit_price->unit_operation_value ?>' class="skip" <?= $product_unit_price->unit_id == $product->purchase_unit ? 'checked="checked"' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                                            </td>
                                                            <td>
                                                                <input type='hidden' class="m_unit_id" name='unit_id[<?= $cnt ?>]' value='<?= $product_unit_price->unit_id ?>' <?= $this->sma->set_tabindex() ?>>
                                                                <input type='hidden' name='unit_operation_value[<?= $cnt ?>]' value='<?= $product_unit_price->unit_operation_value ?>' <?= $this->sma->set_tabindex() ?>>
                                                                <?= $product_unit_price->unit_name ?>
                                                            </td>
                                                            <td>
                                                                <?= $product_unit_price->parent_unit_name ?>
                                                            </td>
                                                            <td>
                                                                <?= $product_unit_price->unit_operation_value > 0 ? $product_unit_price->unit_operation_value : 1 ?>
                                                            </td>
                                                            <?php if ($this->Settings->prioridad_precios_producto == 7): ?>
                                                                <td>
                                                                    <?php if ($product_unit_price->unit_id == $product->unit): ?>
                                                                        <input type='text' class='not_only_number form-control' name='main_unit_margin_update_price' value='<?= $this->sma->formatDecimals($product->main_unit_margin_update_price) ?>' <?= $this->sma->set_tabindex() ?>>
                                                                    <?php else: ?>
                                                                        <input type='text' class='not_only_number form-control' name='margin_update_price[<?= $cnt ?>]' value='<?= $this->sma->formatDecimals($product_unit_price->margin_update_price) ?>' <?= $this->sma->set_tabindex() ?>>
                                                                    <?php endif ?>
                                                                </td>
                                                            <?php endif ?>
                                                            <td>
                                                                <?php if ($this->Settings->prioridad_precios_producto != 10): ?>
                                                                    <?php
                                                                    $sale_consumption_tax = 0;
                                                                    if ($this->Settings->ipoconsumo) {
                                                                        $sale_consumption_tax = $product->consumption_sale_tax;
                                                                        if ($this->Settings->precios_por_unidad_presentacion == 2) {
                                                                           if ($product_unit_price->unit_operator) {
                                                                               if ($product_unit_price->unit_operator == "*") {
                                                                                   $sale_consumption_tax = $sale_consumption_tax * $product_unit_price->unit_operation_value;
                                                                               } else if ($product_unit_price->unit_operator == "/") {
                                                                                   $sale_consumption_tax = $sale_consumption_tax / $product_unit_price->unit_operation_value;
                                                                               }
                                                                           }
                                                                        }
                                                                    }
                                                                     ?>
                                                                    <input type='text' class='not_only_number form-control unit_price' name='unit_price[<?= $cnt ?>]' value='<?= $product_unit_price->valor_unitario > 0 ? $product_unit_price->valor_unitario + ($this->Settings->ipoconsumo && $product->tax_method == 0 ? $sale_consumption_tax : 0) : '' ?>' <?= $product_unit_price->unit_id == $product->unit ? 'readonly' : '' ?> required <?= $this->sma->set_tabindex() ?>>
                                                                    <?php if ($product->tax_method == 0): ?>
                                                                        <input type='hidden' class='not_only_number form-control unit_ipoconsumo'  data-operationvalue='<?= $product_unit_price->unit_operation_value ?>' data-operator='<?= $product_unit_price->unit_operator ?>' name='unit_ipoconsumo[<?= $cnt ?>]' value="<?= $sale_consumption_tax ?>" required <?= $this->sma->set_tabindex() ?>>
                                                                    <?php endif ?>
                                                                <?php else: ?>
                                                                    <?= $product_unit_price->price_group_name ?>
                                                                <?php endif ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($product_unit_price->unit_id != $product->unit): ?>
                                                                    <input type="checkbox" name="unit_status[<?= $cnt ?>]"  class="form-control" <?= $product_unit_price->status == 1 ? 'checked' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                                                <?php endif ?>
                                                            </td>
                                                            <td>
                                                                <?php if ($product_unit_price->unit_id != $product->unit): ?>
                                                                    <span class='fa fa-times' id='delete_unit' data-rowid='<?= $product_unit_price->unit_id ?>'></span>
                                                                <?php endif ?>
                                                            </td>
                                                        </tr>
                                                        <?php $cnt++; ?>
                                                    <?php endforeach ?>
                                                <?php endif ?>
                                            <?php endif ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>
                        <h1  tabindex="-1"><?= lang('product_cost_price') ?></h1>
                        <section>
                            <?php $this->sma->reset_tabindex(); ?>

                            <?php if ($this->Owner || $this->Admin || $this->GP['products-price']): ?>
                                <div class="form-group all col-md-3" style="display:none;">
                                    <?= lang("product_price", "price") ?>
                                    <?= form_input('price', (($product ? $this->sma->formatDecimal(($product->price + ($product->tax_method == 0 ? $product->consumption_sale_tax : 0))) : '')), 'class="form-control tip" id="price"'.($this->Settings->disable_product_price_under_cost == 1 ? ' r ' : '').($this->Settings->prioridad_precios_producto != 5 && $this->Settings->prioridad_precios_producto != 7 ? 'readonly' : '').' style="cursor: pointer !important;"'.$this->sma->set_tabindex()) ?>
                                </div>
                            <?php endif ?>

                            <?php if ($this->Owner || $this->Admin || $this->GP['system_settings-update_products_group_prices']): ?>
                                <div class="row">

                                    <div class="col-sm-12">
                                        <h3 class="modal-title">Precios</h3>
                                    </div>

                                    <?php if($this->Settings->management_weight_in_variants == '1' || $this->Settings->handle_jewerly_products == 1 ): ?>
                                        <div class="form-group  col-md-3">
                                            <?= form_label(lang('based_on_gram_value'), 'based_on_gram_value') ?>
                                            <?= form_dropdown([ 'name'=>'based_on_gram_value', 
                                                                'id'=>'based_on_gram_value', 
                                                                'class'=>'form-control'], 
                                                                [0 =>lang('no'), 1=>lang('current_gold_price'), 2=> lang('current_italian_gold_price')], $product->based_on_gram_value) ?>
                                        </div>
                                        <div class="form-group col-md-3 jewel_weight">
                                            <?= form_label(lang('jewel_weight'), 'jewel_weight') ?>
                                            <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="bottom" title="<?= lang('weight_of_the_jewel_in_grams') ?>"></i>
                                            <input  type="number" 
                                                    name="jewel_weight" 
                                                    id="jewel_weight" 
                                                    class="form-control not_only_number" value="<?= $product->jewel_weight  ?>" <?= $this->sma->set_tabindex() ?>
                                            >
                                        </div>
                                    <?php endif; ?>

                                    <?php
                                    $pg_setted = [];
                                    foreach ($price_groups as $pga) {
                                        $pg_setted[$pga->id] = 1;
                                    }
                                    ?>
                                    <?php foreach ($product_price_groups as $key => $pg): ?>
                                        <?php
                                        if (!isset($pg_setted[$pg->id])) {
                                            continue;
                                        }
                                        ?>
                                        <div class="col-sm-3">
                                            <label><?= $pg->name.(isset($pg->price_group_base) && $pg->price_group_base == 1 ? ' (Lista base)' : null) ?></label>
                                            <?php if (isset($pg->price_group_base) && $pg->price_group_base == 1): ?>
                                                <i class="fa fa-exclamation-circle" data-toggle="tooltip" data-placement="bottom" title="<?= lang('tax_included_impoconsumo_details') ?>"></i>
                                            <?php endif ?>
                                            <?php
                                            $sale_consumption_tax = 0;
                                            if ($this->Settings->ipoconsumo) {
                                                $sale_consumption_tax = $product->consumption_sale_tax;
                                                if ($this->Settings->precios_por_unidad_presentacion == 2) {
                                                   if (isset($pg->unit_operator) && $pg->unit_operator) {
                                                       if ($pg->unit_operator == "*") {
                                                           $sale_consumption_tax = $sale_consumption_tax * $pg->unit_operation_value;
                                                       } else if ($pg->unit_operator == "/") {
                                                           $sale_consumption_tax = $sale_consumption_tax / $pg->unit_operation_value;
                                                       }
                                                   }
                                                }
                                            }
                                            // $sale_consumption_tax = 0;
                                            if (isset($pg->price_group_base) && $pg->price_group_base == 1){
                                                $_price_base = $pg->price + ($product->tax_method == 0 ? $sale_consumption_tax : 0);
                                            }
                                             ?>
                                            <input type="number" name="pg[]" data-pgbase="<?= isset($pg->price_group_base) ? $pg->price_group_base : null ?>" <?= (isset($pg->price_group_base) && $pg->price_group_base == 1 ? 'required="required"' : null) ?> class="form-control not_only_number pg_price" value="<?= floatval($pg->price + ($product->tax_method == 0 ? $sale_consumption_tax : 0)) ?>" <?= $this->sma->set_tabindex() ?>
                                            <?= (isset($pg->price_group_base) && $pg->price_group_base && $this->Settings->allow_zero_price_products == 0) ? " min='1' " : "" ?>
                                            >
                                            <input type="hidden" name="pg_id[]" value="<?= isset($pg->id) ? $pg->id : null ?>">
                                            <input type="hidden" name="pg_ipoconsumo[]" class="pg_ipoconsumo" data-pgoperator="<?= isset($pg->unit_operator) ? $pg->unit_operator : ''; ?>" data-pgoperationvalue="<?= isset($pg->unit_operation_value) ? $pg->unit_operation_value : ''; ?>" value="<?= $sale_consumption_tax ?>">
                                        </div>
                                    <?php endforeach ?>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                </div>
                            <?php endif ?>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <input type="checkbox" class="checkbox" value="1" name="promotion" id="promotion" <?= $this->input->post('promotion') ? 'checked="checked"' : ''; ?> <?= $this->sma->set_tabindex() ?>>
                                    <label for="promotion" class="padding05">
                                        <?= lang('promotion'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="row well well-sm" id="promo" <?= $product->promotion ? '' : ' style="display:none;"'; ?>>
                                <div class="form-group col-sm-3">
                                    <?= lang('promo_price', 'promo_price'); ?>
                                    <?= form_input('promo_price', set_value('promo_price', $product->promo_price ? $this->sma->formatDecimal($product->promo_price) : ''), 'class="form-control tip" id="promo_price"'.$this->sma->set_tabindex()); ?>
                                </div>
                                <div class="form-group col-sm-3">
                                    <?= lang('start_date', 'start_date'); ?>
                                    <input type="text" name="start_date" id="start_date" value="<?= $product->start_date ? $product->start_date : '' ?>" class="form-control" <?= $this->sma->set_tabindex() ?>>
                                </div>
                                <div class="form-group col-sm-3">
                                    <?= lang('end_date', 'end_date'); ?>
                                    <input type="text" name="end_date" id="end_date" value="<?= $product->end_date ? $product->end_date : '' ?>" class="form-control" <?= $this->sma->set_tabindex() ?>>
                                </div>
                            </div>

                            <?php if ($this->Owner || $this->Admin || $this->GP['products-cost']): ?>
                                <div class="row">
                                    <hr>
                                    <div class="col-sm-12">
                                        <h3 class="modal-title">Costos</h3>
                                    </div>
                                    <div class="form-group standard col-md-3">
                                        <?= lang("avg_cost", "avg_cost") ?>
                                        <?= form_input('avg_cost', ($product->avg_cost != '' ? $product->avg_cost : 0), 'class="form-control" disabled="true" '.$this->sma->set_tabindex()) ?>
                                    </div>
                                    <div class="form-group standard col-md-3">
                                        <?= lang("last_cost", "cost") ?>
                                        <input type="number" name="cost" id="cost" class="form-control" value="<?= $this->sma->formatDecimal($product->cost +  + ($product->tax_method == 0 ? $product->consumption_purchase_tax : 0)) ?>"  <?= $this->Settings->lock_cost_field_in_product == 1 ? 'readonly' : '' ?> <?= $this->Settings->product_crud_validate_cost == 1 ? 'required min="1"' : '' ?> <?= $this->sma->set_tabindex() ?>>
                                    </div>
                                    <div class="form-group all col-md-3">
                                        <?= lang("profitability_margin", "profitability_margin") ?>
                                        <div class="input-group">
                                            <?= form_input('profitability_margin', (isset($_POST['profitability_margin']) ? $_POST['profitability_margin'] : ($product ? $this->sma->formatDecimal($product->profitability_margin) : '')), 'class="form-control tip" id="profitability_margin" readonly') ?>
                                            <span class="input-group-addon" data-toggle="modal" data-backdrop="static" href="#modalPricesMargin">
                                                <i class="fa fa-percent"></i>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            <?php endif ?>
                            <div class="row">

                                <?php if ($this->Settings->ipoconsumo > 0): ?>
                                    <hr>
                                    <?php
                                    if ($product->consumption_sale_tax > 0 && is_null($product->sale_tax_rate_2_id)) {
                                        $product->sale_tax_rate_2_id = 1;
                                    }
                                    if ($product->consumption_purchase_tax > 0 && is_null($product->purchase_tax_rate_2_id)) {
                                        $product->purchase_tax_rate_2_id = 1;
                                    }

                                     ?>
                                    <?php if ($this->Settings->ipoconsumo == 2 || $this->Settings->ipoconsumo == 1): ?>
                                        <div class="col-sm-3">
                                            <?= lang('purchase_second_tax', 'purchase_second_tax') ?>
                                            <select name="purchase_tax_rate_2_id" id="purchase_tax_rate_2_id" class="form-control" <?= $this->sma->set_tabindex() ?>>
                                                <option value=""><?= lang('select') ?></option>
                                                <?php foreach ($second_taxes as $st): ?>
                                                    <option value="<?= $st->id ?>" data-valuetype="<?= $st->value_type ?>" <?= $st->id == $product->purchase_tax_rate_2_id ? "selected" : "" ?>><?= $st->code." - ".$st->description ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 purchase_second_tax_div" <?= $product->purchase_tax_rate_2_id ? '' : 'style="display:none;"' ?> >
                                            <?= lang('purchase_second_tax_value', 'consumption_purchase_tax') ?>
                                            <div class="row">
                                                <div class="col-sm-12 purchase_second_tax_div_2">
                                                    <input type="text" name="consumption_purchase_tax" id="consumption_purchase_tax" class="form-control" value="<?= $product->consumption_purchase_tax ?>" data-prevval="<?= $product->consumption_purchase_tax ?>" <?= $this->sma->set_tabindex() ?>>
                                                </div>
                                                <div class="col-sm-6 purchase_second_tax_div_3" style="display:none;">
                                                    <input type="text" name="purchase_tax_rate_2_percentage" id="purchase_tax_rate_2_percentage" class="form-control" value="<?= $product->purchase_tax_rate_2_percentage ?>" data-prevval="<?= $product->purchase_tax_rate_2_percentage ?>" pattern="\d+%" placeholder="Ingrese %" <?= $this->sma->set_tabindex() ?>>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                    <?php if ($this->Settings->ipoconsumo == 3 || $this->Settings->ipoconsumo == 1): ?>
                                        <div class="col-sm-3">
                                            <?= lang('sale_second_tax', 'sale_second_tax') ?>
                                            <select name="sale_tax_rate_2_id" id="sale_tax_rate_2_id" class="form-control" <?= $this->sma->set_tabindex() ?>>
                                                <option value=""><?= lang('select') ?></option>
                                                <?php foreach ($second_taxes as $st): ?>
                                                    <?php if ($st->id != 1) : ?>
                                                        <option value="<?= $st->id ?>" data-valuetype="<?= $st->value_type ?>" <?= $st->id == $product->sale_tax_rate_2_id ? "selected" : "" ?>><?= $st->code." - ".$st->description ?></option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                        <div class="col-sm-3 sale_second_tax_div" <?= $product->sale_tax_rate_2_id ? '' : 'style="display:none;"' ?> >
                                            <?= lang('sale_second_tax_value', 'consumption_sale_tax') ?>
                                            <div class="row">
                                                <div class="col-sm-4 sale_second_tax_milliliters">
                                                    <div class="form-group">
                                                        <label for="sale_tax_rate_2_milliliters"><small id="sale_tax_rate_2_milliliters_label"><?= lang('milliliters') ?></small></label>
                                                        <input type="number" name="sale_tax_rate_2_milliliters" id="sale_tax_rate_2_milliliters" class="form-control" value="<?= $product->sale_tax_rate_2_milliliters ?>" data-prevval="<?= $product->sale_tax_rate_2_milliliters ?>" <?= $this->sma->set_tabindex() ?>>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4 sale_second_tax_degrees">
                                                    <div class="form-group">
                                                        <label for="sale_tax_rate_2_degrees"><small><?= lang('degrees') ?></small></label>
                                                        <input type="number" name="sale_tax_rate_2_degrees" id="sale_tax_rate_2_degrees" class="form-control" value="<?= $product->sale_tax_rate_2_degrees ?>" data-prevval="<?= $product->sale_tax_rate_2_degrees ?>" <?= $this->sma->set_tabindex() ?>>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4 sale_second_tax_div_3" style="display:none;">
                                                    <div class="form-group">
                                                        <label for="sale_tax_rate_2_percentage"><small><?= lang('fee') ?></small></label>
                                                        <input type="text" name="sale_tax_rate_2_percentage" id="sale_tax_rate_2_percentage" class="form-control" value="<?= $product->sale_tax_rate_2_percentage ?>" data-prevval="<?= $product->sale_tax_rate_2_percentage ?>" pattern="\d+%" <?= $this->sma->set_tabindex() ?>>
                                                    </div>
                                                </div>

                                                <div class="col-sm-4 sale_second_tax_nominal">
                                                    <div class="form-group">
                                                        <label for="sale_tax_rate_2_nominal"><small><?= lang('fee') ?></small></label>
                                                        <select class="form-control not_select" name="sale_tax_rate_2_nominal" id="sale_tax_rate_2_nominal" <?= $this->sma->set_tabindex() ?>></select>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 sale_second_tax_div_2">
                                                    <div class="form-group">
                                                        <label for="consumption_sale_tax"><small><?= lang('tax') ?></small></label>
                                                        <input type="text" name="consumption_sale_tax" id="consumption_sale_tax" class="form-control" value="<?= floatval($product->consumption_sale_tax) ?>" data-prevval="<?= floatval($product->consumption_sale_tax) ?>" <?= $this->sma->set_tabindex() ?> readonly>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                <?php endif ?>
                            </div>
                            <hr class="col-sm-11">
                            <div class="row">
                                <?php
                                if (filter_var($product->file, FILTER_VALIDATE_URL) === FALSE) {
                                    $file = $product->file;
                                    $file_link = '';
                                } else {
                                    $file_link = $product->file;
                                    $file = '';
                                }
                                ?>
                                <div class="standard">
                                    <div class="form-group col-sm-12">
                                        <?= lang("supplier", "supplier") ?>
                                        <button type="button" class="btn btn-primary btn-outline btn-xs" id="addSupplier" style="border-radius: 13px !important;"><i class="fa fa-plus"></i>
                                        </button>
                                    </div>
                                    <div id="supplier-con">
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <?php
                                                echo form_input('supplier', (isset($_POST['supplier']) ? $_POST['supplier'] : ''), 'class="form-control ' . ($product ? '' : 'suppliers') . '" id="' . ($product && ! empty($product->supplier1) ? 'supplier1' : 'supplier') . '" placeholder="' . lang("select") . ' ' . lang("supplier") . '"'.$this->sma->set_tabindex());
                                                ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <?= form_input('supplier_date', (isset($_POST['supplier_date']) ? $_POST['supplier_date'] : ""), 'class="form-control tip" id="supplier_date" placeholder="' . lang('supplier_date') . '" readonly'.$this->sma->set_tabindex()); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <?= form_input('supplier_part_no', (isset($_POST['supplier_part_no']) ? $_POST['supplier_part_no'] : ""), 'class="form-control tip" id="supplier_part_no" placeholder="' . lang('supplier_part_no') . '"'.$this->sma->set_tabindex()); ?>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                            <div class="form-group">
                                                <?= form_input('supplier_price', (isset($_POST['supplier_price']) ? $_POST['supplier_price'] : ""), 'class="form-control tip" id="supplier_price" placeholder="' . lang('supplier_price') . '"'.$this->sma->set_tabindex()); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="ex-suppliers"></div>
                                </div>
                            </div>
                        </section>
                        <h1  tabindex="-1"><?= lang('variants') ?></h1>
                        <section>
                            <?php $this->sma->reset_tabindex(); ?>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="standard">
                                        <div class="clearfix"></div>
                                        <div id="attrs"></div>
                                            <?php
                                            if ($product_options) { ?>
                                            <table class="table table-bordered table-condensed"
                                                   style="<?= $this->input->post('attributes') || $product_options ? '' : 'display:none;'; ?> margin-top: 10px;">
                                                <thead>
                                                <tr class="active">
                                                    <th><?= lang('name') ?></th>
                                                    <th><?= lang('warehouse') ?></th>
                                                    <th><?= lang('quantity') ?></th>
                                                    <th><?= lang('price_addition') ?></th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <?php
                                                    foreach ($product_options as $option) {
                                                        echo '<tr>
                                                                <td class="col-xs-3">
                                                                    <input type="hidden" name="attr_id[]" value="' . $option->id . '">
                                                                    <span>' . $option->name . '</span>
                                                                </td>
                                                                <td class="code text-center col-xs-3">
                                                                    <span>' . $option->wh_name . '</span>
                                                                </td>
                                                                <td class="quantity text-center col-xs-2">
                                                                    <span>' . $this->sma->formatQuantity($option->wh_qty) . '</span>
                                                                </td>
                                                                <td class="price text-right col-xs-2">' . $this->sma->formatMoney($option->price) . '
                                                                </td>
                                                            </tr>';
                                                    }
                                                ?>
                                                </tbody>
                                            </table>
                                            <?php
                                            }  ?>
                                            <?php if ($product_variants) { ?>
                                                <h3 class="bold"><?=lang('product_variants');?></h3>
                                                <table id = "attrTable" class="table table-bordered table-condensed" style="margin-top: 10px;">
                                                    <thead>
                                                        <tr class="active">
                                                            <th class="" style="width:15%;"><?= lang('name') ?></th>
                                                            <?php if ($this->Settings->set_product_variant_suffix == 1): ?>
                                                                <th class=""><?= lang('suffix') ?></th>
                                                            <?php endif ?>
                                                            <th class="" style="width:15%;"><?= lang('price') ?></th>
                                                            <th class="" style="width:15%;"><?= lang('minimun_price') ?></th>
                                                            <th class="" style="width:15%;"><?= lang('code') ?></th>
                                                            <th style="width:15%;" ><?= lang('weight') ?></th>
                                                            <th class="" style="width:15%;" colspan="2"><?= lang('image') ?></th>
                                                            <th class="" style="width:5%;"><?= lang('status') ?></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                <?php
                                                $pv_setted = [];
                                                foreach ($product_variants as $pv) {
                                                    $full_price = $_price_base + $pv->price;
                                                    $pv_setted[$pv->name] = 1;
                                                     echo '<tr>
                                                                <td>
                                                                    <input type="text"  class="form-control select_pv" name="p_variant_name['.$pv->id.']" id="select_pv_'.$pv->id.'" value="'.$pv->name.'">
                                                                </td>'.
                                                                ($this->Settings->set_product_variant_suffix == 1 ?
                                                                    '<td style="width:30%;">
                                                                        <input type="text" name="old_attr_suffix['.$pv->id.']" class="form-control" value="' . $pv->suffix . '" '.$this->sma->set_tabindex().'>
                                                                    </td>'
                                                                    : ''
                                                                ).
                                                                '
                                                                <td class="price text-right" style="display:none">
                                                                    <input name="p_variant_price[' . $pv->id . ']" value="' . $this->sma->formatDecimal($pv->price) . '" class="form-control"'.$this->sma->set_tabindex().'>
                                                                </td>
                                                                 <td class="price text-right">
                                                                    <input name="p_variant_fullPrice[' . $pv->id . ']" value="' . $this->sma->formatDecimal($full_price) . '" class="form-control"'.$this->sma->set_tabindex().'>
                                                                </td>
                                                                <td class="price text-right">
                                                                    <input name="p_variant_minimumPrice[' . $pv->id . ']" value="' . $this->sma->formatDecimal($pv->minimun_price) . '" class="form-control"'.$this->sma->set_tabindex().'>
                                                                </td>
                                                                <td class="text-left form-group">
                                                                    <div class="input-group">
                                                                        <input name="p_variant_code[' . $pv->id . ']" value="' . $pv->code . '" class="form-control validate_code" data-module="variant" data-id="'.$pv->id.'" '.''.$this->sma->set_tabindex().'>'
                                                                        .'<span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;" data-toggle="tooltip" title="'.lang('random_code_text').'">
                                                                            <i class="fa fa-random"></i>
                                                                        </span>
                                                                        <span class="input-group-addon pointer" id="consecutive_code" style="padding: 1px 10px;" data-toggle="tooltip" title="'.lang('consecutive_code_text').'">
                                                                            <b>123</b>
                                                                        </span>'.
                                                                    '</div>
                                                                    <input type="hidden" name="p_variant_code_consecutive[' . $pv->id . ']" class="form-control code_consecutive_setted">
                                                                </td>
                                                                <td class="price text-right">
                                                                    <input name="p_weight[' . $pv->id . ']" value="' . $this->sma->formatDecimal($pv->weight) . '" class="form-control"'.$this->sma->set_tabindex().'>
                                                                </td>
                                                                <td>
                                                                <input type="file" data-browse-label="'.lang('browse').'" name="p_variant_image_' . $pv->id . '" data-show-upload="false" data-show-preview="false" accept="image/*" class="form-control file">
                                                                </td>
                                                                <td style="width:8%;">
                                                                    <img src="'.base_url() . 'assets/uploads/' . $pv->image.'" style="width: 70px;">
                                                                </td>
                                                                <td>
                                                                    <label class="status_switch">
                                                                      <input name="p_variant_status[' . $pv->id . ']" type="checkbox" class="skip" ' . ($pv->status == 1 ? 'checked' : '') .'>
                                                                      <span class="status_slider"></span>
                                                                    </label>
                                                                </td>
                                                            </tr>';
                                                }
                                                ?>
                                                </tbody>
                                                </table>
                                                <?php
                                            }
                                            ?>
                                            <div class="form-group">
                                                <input type="checkbox" class="checkbox" name="attributes" id="attributes" <?= $product->attributes ? 'checked="checked"' : ''; ?>  <?= $this->sma->set_tabindex() ?> >
                                                <label for="attributes" class="padding05"><?= sprintf(lang('add_more_variants'), lang('variants')); ?></label>
                                                <?= lang('eg_sizes_colors'); ?>
                                            </div>
                                            <div id="attr-con" <?= $this->input->post('attributes') || $product->attributes ? '' : 'style="display:none;"'; ?>>
                                                <div class="form-group" id="ui" style="margin-bottom: 0;">

                                                        <select class="form-control" name="attributesInput" id="attributesInput" multiple <?= $this->sma->set_tabindex() ?>>
                                                            <?php if ($variants): ?>
                                                                <?php foreach ($variants as $variant): ?>
                                                                    <?php

                                                                    if (isset($pv_setted[$variant->name])) {
                                                                        continue;
                                                                    }

                                                                     ?>
                                                                    <option data-weight="<?= $variant->weight ?>" value="<?= $variant->name ?>"><?= $variant->name ?></option>
                                                                <?php endforeach ?>
                                                            <?php endif ?>
                                                        </select>
                                                        <div class="input-group-addon" style="padding: 2px 5px; display: none;">
                                                            <a href="#" id="addAttributes">
                                                                <i class="fa fa-2x fa-plus-circle" id="addIcon"></i>
                                                            </a>
                                                        </div>
                                                    <div style="clear:both;"></div>
                                                </div>
                                                <div class="table-responsive">
                                                    <?php if ($this->Settings->set_product_variant_suffix == 1): ?>
                                                        <table id="attrSuffixTable" class="table" style="<?= $this->input->post('attributes') || $product_options ? '' : 'display:none;'; ?>margin-bottom: 0; margin-top: 10px;">
                                                            <thead>
                                                                <tr>
                                                                    <th style="width:50%;"><?= lang('name') ?></th>
                                                                    <th style="width:50%;"><?= lang('suffix') ?></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    <?php endif ?>
                                                    <table id="attrTable" class="table table-bordered table-condensed" style="margin-bottom: 0; margin-top: 10px;">
                                                        <thead>
                                                            <tr class="active">
                                                                <th class="" style="width:15%;"><?= lang('name') ?></th>
                                                                <?php if ($this->Settings->set_product_variant_suffix == 1): ?>
                                                                    <th class=""><?= lang('suffix') ?></th>
                                                                <?php endif ?>
                                                                <th class="" style="width:15%;"><?= lang('price') ?></th>
                                                                <th class="" style="width:15%;"><?= lang('minimun_price') ?></th>
                                                                <th class="" style="width:15%;"><?= lang('code') ?></th>
                                                                <th style="width:15%;" ><?= lang('weight') ?></th>
                                                                <th class="" style="width:15%;" colspan="2"><?= lang('image') ?></th>
                                                                <th class="" style="width:5%;"><?= lang('status') ?></th>
                                                            </tr>
                                                        </thead>
                                                        <tbody><?php
                                                            if ($this->input->post('attributes') && isset($_POST['attr_name'])) {
                                                                $a = sizeof($_POST['attr_name']);
                                                                for ($r = 0; $r <= $a; $r++) {
                                                                    if (isset($_POST['attr_name'][$r]) && (isset($_POST['attr_warehouse'][$r]) || isset($_POST['attr_quantity'][$r]))) {
                                                                        echo '<tr class="attr">
                                                                        <td><input type="hidden" name="attr_name[]" value="' . $_POST['attr_name'][$r] . '"><span>' . $_POST['attr_name'][$r] . '</span></td>
                                                                        <td class="code text-center"><input type="hidden" name="attr_warehouse[]" value="' . (isset($_POST['attr_warehouse'][$r]) ? $_POST['attr_warehouse'][$r] : '') . '"><input type="hidden" name="attr_wh_name[]" value="' . (isset($_POST['attr_wh_name'][$r]) ? $_POST['attr_wh_name'][$r] : '') . '"><span>' . (isset($_POST['attr_wh_name'][$r]) ? $_POST['attr_wh_name'][$r] : '') . '</span></td>
                                                                        <td class="quantity text-center"><input type="hidden" name="attr_quantity[]" value="' . $_POST['attr_quantity'][$r] . '"><span>' . $_POST['attr_quantity'][$r] . '</span></td>
                                                                        <td class="price text-right"><input type="hidden" name="attr_price[]" value="' . $_POST['attr_price'][$r] . '"><span>' . $_POST['attr_price'][$r] . '</span></span></td><td class="text-center"><i class="fa fa-times delAttr"></i></td>
                                                                    </tr>';
                                                                }
                                                            }
                                                        }
                                                        ?></tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class=" well well-sm">
                                        <div class="form-group row">
                                            <div class="col-sm-3">
                                                <input type="checkbox" class="checkbox" name="preferences" id="preferences" <?= $this->input->post('preferences')  || $product_preferences ? 'checked="checked"' : ''; ?><?= $this->sma->set_tabindex() ?>>
                                                <label for="preferences" class="padding05"><?= lang('product_has_preferences'); ?></label>
                                            </div>
                                            <div class="col-sm-3 div_preference_category" style="display:none;">
                                                <?php if ($preferences_categories): ?>
                                                    <?= lang('preference_category', 'preference_category') ?>
                                                    <select id="preference_category" class="form-control"<?= $this->sma->set_tabindex() ?>>
                                                        <option value=""><?= lang('select') ?></option>
                                                        <?php foreach ($preferences_categories as $pfc): ?>
                                                            <option value="<?= $pfc->id ?>"><?= $pfc->name ?></option>
                                                        <?php endforeach ?>
                                                    </select>
                                                <?php endif ?>
                                            </div>
                                        </div>
                                        <div class="" id="preferences-con" style="<?= $this->input->post('preferences') || $product_preferences ? '' : 'display:none;'; ?>">
                                            <div class="form-group" id="ui" style="margin-bottom: 0;">
                                                <div class="form-group" id="ui" style="margin-bottom: 0;">
                                                    <?php echo form_input('add_preference', '', 'class="form-control" id="add_preference" placeholder="' . $this->lang->line("enter_preferences") . '"'.$this->sma->set_tabindex()); ?>
                                                </div>
                                                <div style="clear:both;"></div>
                                            </div>
                                            <div class="table-responsive">
                                                <table id="preferencesTable" class="table table-bordered table-condensed"
                                                       style="<?= $this->input->post('preferences') || $product_preferences ? '' : 'display:none;'; ?>margin-bottom: 0; margin-top: 10px;">
                                                    <thead>
                                                        <tr class="active">
                                                            <th><?= lang('name') ?></th>
                                                            <th><i class="fa fa-times preferences-remove-all"></i></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                            if (($this->input->post('preferences') && isset($_POST['preference_name'])) || $product_preferences) {

                                                                if ($this->input->post('preferences') && isset($_POST['preference_name'])) {
                                                                    $a = sizeof($_POST['preference_name']);
                                                                } else if ($product_preferences) {
                                                                    $a = sizeof($product_preferences);
                                                                }

                                                                for ($r = 0; $r <= $a; $r++) {
                                                                    if (isset($_POST['preference_name'][$r])) {
                                                                        echo '<tr>
                                                                                    <td>
                                                                                        <input type="hidden" name="preference_id[]" value="' . $_POST['preference_id'][$r] . '">
                                                                                        <span>' . $_POST['preference_name'][$r] . '</span>
                                                                                    </td>
                                                                                    <td class="text-center">
                                                                                        <i class="fa fa-times delPreference"></i>
                                                                                    </td>
                                                                               </tr>';
                                                                    } else if (isset($product_preferences[$r])) {
                                                                        echo '<tr>
                                                                                    <td>
                                                                                        <input type="hidden" name="preference_id[]" value="' . $product_preferences[$r]->preference_id . '">
                                                                                        <input type="hidden" name="preference_name[]" value="' .$product_preferences[$r]->name . '">
                                                                                        <span>' . $product_preferences[$r]->prf_cat_name ." - ".$product_preferences[$r]->name . '</span>
                                                                                    </td>
                                                                                    <td class="text-center">
                                                                                        <i class="fa fa-times delPreference"></i>
                                                                                    </td>
                                                                               </tr>';
                                                                    }
                                                                }
                                                            }
                                                        ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <h1  tabindex="-1"><?= lang('visualization_data') ?></h1>
                        <section>
                            <?php $this->sma->reset_tabindex(); ?>
                            <div class="row">
                                <?php if ($Settings->invoice_view == 2) { ?>
                                    <div class="form-group col-md-3">
                                        <?= lang('hsn_code', 'hsn_code'); ?>
                                        <?= form_input('hsn_code', set_value('hsn_code', ($product ? $product->hsn_code : '')), 'class="form-control" id="hsn_code"'.$this->sma->set_tabindex()); ?>
                                    </div>
                                <?php } ?>
                                <div class="form-group digital col-md-3" style="display:none;">
                                    <?= lang("digital_file", "digital_file") ?>
                                    <input id="digital_file" type="file" data-browse-label="<?= lang('browse'); ?>" name="digital_file" data-show-upload="false"
                                           data-show-preview="false" class="form-control file">
                                </div>
                                <div class="form-group digital col-md-3" style="display:none;">
                                    <?= lang('file_link', 'file_link'); ?>
                                    <?= form_input('file_link', $file_link, 'class="form-control" id="file_link"'); ?>
                                </div>
                                <div class="form-group col-md-3">
                                    <input name="featured" type="checkbox" class="checkbox" id="featured" value="1" <?= empty($product->featured) ? '' : 'checked="checked"' ?> <?= $this->sma->set_tabindex() ?>/>
                                    <label for="featured" class="padding05"><?= lang('featured') ?></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <input name="hide_pos" type="checkbox" class="checkbox" id="hide_pos" value="1" <?= empty($product->hide_pos) ? '' : 'checked="checked"' ?><?= $this->sma->set_tabindex() ?>/>
                                    <label for="hide_pos" class="padding05"><?= lang('hide_pos') ?></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <input name="hide_detal" type="checkbox" class="checkbox" id="hide_detal" value="1" <?= empty($product->hide_detal) ? '' : 'checked="checked"' ?><?= $this->sma->set_tabindex() ?>/>
                                    <label for="hide_detal" class="padding05"><?= lang('hide_detal') ?></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <input name="hide_online_store" type="checkbox" class="checkbox" id="hide_online_store" value="1" <?= empty($product->hide_online_store) ? '' : 'checked="checked"' ?><?= $this->sma->set_tabindex() ?>/>
                                    <label for="hide_online_store" class="padding05"><?= lang('hide_online_store') ?></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <input name="discontinued" type="checkbox" class="checkbox" id="discontinued" value="1" <?= empty($product->discontinued) ? '' : 'checked="checked"' ?><?= $this->sma->set_tabindex() ?>/>
                                    <label for="discontinued" class="padding05"><?= lang('product_discontinued') ?></label>
                                </div>
                                <div class="form-group col-md-3">
                                    <input type="checkbox" class="checkbox" value="1" name="ignore_hide_parameters" id="ignore_hide_parameters" <?= $product->ignore_hide_parameters ? 'checked="checked"' : ''; ?><?= $this->sma->set_tabindex() ?>>
                                    <label for="ignore_hide_parameters" class="padding05">
                                        <?= lang('ignore_hide_parameters'); ?>
                                    </label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <?= lang('product_exclusive_billers', 'billers'); ?>
                                    <select class="form-control select" name="billers[]" id="billers" multiple="multiple" <?= $this->sma->set_tabindex() ?>>
                                      <?php foreach ($billers as $biller) : ?>
                                        <option value="<?= $biller->id ?>" <?= isset($billers_selected[$biller->id]) ? "selected" : "" ?>><?= $biller->company; ?></option>
                                      <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                            <input type="hidden" name="billers_changed" id="billers_changed" value="0">
                            <div class="row">
                                <div class="form-grou col-sm-12 row">
                                    <h2><?= lang('quantity_per_warehouses') ?></h2>
                                </div>
                                <?php
                                    $whp_qty = [];
                                    if (!empty($warehouses_products)) {
                                        foreach ($warehouses_products as $whp) {
                                            $whp_qty[$whp->id]['min_quantity'] = $whp->min_quantity;
                                            $whp_qty[$whp->id]['max_quantity'] = $whp->max_quantity;
                                        }
                                    }
                                ?>
                                <?php foreach ($warehouses as $warehouse): ?>
                                    <div class="form-grou col-sm-4 row">
                                        <div class="col-sm-12">
                                            <h3><?= $warehouse->name." (".$warehouse->code.")" ?></h3>
                                        </div>
                                        <div class="col-sm-6">
                                            <label><?= lang('minimum') ?></label>
                                            <input type="text" name="wh_min[<?= $warehouse->id ?>]" class="form-control not_only_number" value="<?= isset($whp_qty[$warehouse->id]) && $whp_qty[$warehouse->id]['min_quantity'] > 0 ? $whp_qty[$warehouse->id]['min_quantity'] : 0 ?>"<?= $this->sma->set_tabindex() ?>>
                                        </div>
                                        <div class="col-sm-6">
                                            <label><?= lang('maximum') ?></label>
                                            <input type="text" name="wh_max[<?= $warehouse->id ?>]" class="form-control not_only_number" value="<?= isset($whp_qty[$warehouse->id]) && $whp_qty[$warehouse->id]['max_quantity'] > 0 ? $whp_qty[$warehouse->id]['max_quantity'] : 0 ?>"<?= $this->sma->set_tabindex() ?>>

                                        </div>
                                    </div>
                                <?php endforeach ?>

                            </div>
                            <!-- <div class="form-group">
                                <button type="button" class="btn btn-primary" id="edit_product">
                                    <?= lang('edit_product') ?>

                                </button>
                            </div> -->
                        </section>
                        <h1  tabindex="-1"><?= lang('images') ?></h1>
                        <section>
                            <?php $this->sma->reset_tabindex(); ?>
                            <div class="row">
                                <div class="form-group all col-md-4 main_image_input">
                                    <?= lang("product_image", "product_image") ?>
                                    <input id="product_image" type="file" data-browse-label="<?= lang('browse'); ?>" name="product_image" data-show-upload="false"
                                           data-show-preview="false" accept="image/*" class="form-control file">
                                </div>

                            </div>
                            <div class="row">
                                <?php if ($product->image): ?>
                                    <div class="col-md-12">
                                        <label>Imágen principal</label>
                                        <br>
                                        <img src="<?= base_url() . 'assets/uploads/' . $product->image ?>" class="main_image" style="width: 300px;">
                                        <button class="btn btn-danger btn-sm delete_main_image" type="button"><i class="fa fa-trash-o"></i></button>
                                    </div>
                                <?php endif ?>
                                <hr class="col-sm-11">
                                <div class="form-group all col-md-4 gallery_images_input">
                                    <?= lang("product_gallery_images", "images") ?>
                                    <input id="images" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile[]" multiple="true" data-show-upload="false"
                                           data-show-preview="false" class="form-control file" accept="image/*">
                                </div>

                                <div class="col-md-12">
                                    <label>Galería de imágenes</label>
                                </div>
                                <div class="gallery_preview">
                                    <?php if ($images && count($images) > 0): ?>
                                        <?php $cnt = 1; ?>
                                        <?php foreach ($images as $img): ?>
                                            <div class="col-md-4">
                                                <label>Imágen <?= $cnt ?> </label>
                                                <br>
                                                <img src="<?= base_url() . 'assets/uploads/' . $img->photo ?>" style="width: 300px;" class="gallery_images" data-imgnum="<?= $cnt ?>">
                                                <button class="btn btn-danger btn-sm delete_image" type="button" data-imgnum="<?= $cnt ?>" data-phid="<?= $img->id ?>"><i class="fa fa-trash-o"></i></button>
                                            </div>
                                            <?php $cnt++; ?>
                                        <?php endforeach ?>
                                        <script type="text/javascript">
                                            var num_gallery_images = "<?= $cnt ?>";
                                        </script>
                                    <?php else: ?>
                                        <script type="text/javascript">
                                            var num_gallery_images = 1;
                                        </script>
                                    <?php endif ?>
                                </div>
                            </div>
                        </section>
                        <h1  tabindex="-1"><?= lang('shipping') ?></h1>
                        <section>
                            <?php $this->sma->reset_tabindex(); ?>
                            <div class="form-group col-md-3">
                                <?= lang('weight', 'weight'); ?>
                                <?= form_input('weight', set_value('weight', ($product ? $product->weight : '')), 'class="form-control tip" id="weight"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('height', 'height'); ?>
                                <?= form_input('height', set_value('height', ($product ? $product->height : '')), 'class="form-control tip" id="height"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('width', 'width'); ?>
                                <?= form_input('width', set_value('width', ($product ? $product->width : '')), 'class="form-control tip" id="width"'); ?>
                            </div>
                            <div class="form-group col-md-3">
                                <?= lang('length', 'length'); ?>
                                <?= form_input('length', set_value('length', ($product ? $product->length : '')), 'class="form-control tip" id="length"'); ?>
                            </div>
                        </section>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($this->Settings->prioridad_precios_producto != 5 && $this->Settings->prioridad_precios_producto != 7): ?>
    <div class="modal fade" id="modalPrices" tabindex="-1" role="dialog" aria-labelledby="modalPricesLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
            <button type="button" class="btn btn-primary" id="submit_prices"><?= lang('submit') ?></button>
          </div>
        </div>
      </div>
    </div>
<?php endif ?>
<?= form_close(); ?>

<div class="modal fade" id="modalPricesMargin" tabindex="-1" role="dialog" aria-labelledby="modalPricesMarginLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="modalPricesMarginLabel">Márgen por listas de precios</h4>
      </div>
      <div class="modal-body row">
        <?php foreach ($product_price_groups as $pg): ?>
            <div class="col-sm-6">
                <label><?= $pg->name ?></label>
                <input type="text" class="form-control pg_margins" readonly>
            </div>
        <?php endforeach ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?= lang('cancel') ?></button>
        <button type="button" class="btn btn-primary" data-dismiss="modal"><?= lang('submit') ?></button>
      </div>
    </div>
  </div>
</div>


<script type="text/javascript">
        var items = {};
    $(document).ready(function () {
        $('.select_pv').each(function(index, select){
            select_variant($(select).prop('id'), $(select).val());
        });
        $('.skin-1').addClass('mini-navbar');
        $("#start_date").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', "<?= date('d/m/Y H:i', strtotime($product->start_date ? $product->start_date : date('Y-m-d H:i:s'))) ?>");

        $("#end_date").datetimepicker({
                format: site.dateFormats.js_ldate,
                fontAwesome: true,
                language: 'sma',
                weekStart: 1,
                todayBtn: 1,
                autoclose: 1,
                todayHighlight: 1,
                startView: 2,
                forceParse: 0
            }).datetimepicker('update', "<?= date('d/m/Y H:i', strtotime($product->end_date ? $product->end_date : date('Y-m-d 23:59:59'))) ?>");

        $('[data-toggle="tooltip"]').tooltip();
        $('form[data-toggle="validator"]').bootstrapValidator({ excluded: [':disabled'] });
        var audio_success = new Audio('<?= $assets ?>sounds/sound2.mp3');
        var audio_error = new Audio('<?= $assets ?>sounds/sound3.mp3');

        $('#edit_form').validate({
          ignore: []
        });
        <?php
        if($combo_items) {
            echo '
                var ci = '.json_encode($combo_items).';
                $.each(ci, function() { add_product_item(this); });
                ';
        }
        ?>
        <?=isset($_POST['cf']) ? '$("#extras").iCheck("check");': '' ?>
        $(document).on('ifChecked', '#extras', function () {
            $('#extras-con').slideDown();
        });
        $(document).on('ifUnchecked', '#extras', function () {
            $('#extras-con').slideUp();
        });

        <?= isset($_POST['promotion']) || $product->promotion ? '$("#promotion").iCheck("check");': '' ?>
        $(document).on('ifChecked', '#promotion', function (e) {
            $('#promo').slideDown();
        });
        $(document).on('ifUnchecked', '#promotion', function (e) {
            $('#promo').slideUp();
        });

        <?php if ($this->Settings->product_variant_per_serial == 0): ?>
            $(document).on('ifChecked', '.attributes', function (event) {
                $('#options_' + $(this).attr('id')).slideDown();
            });
            $(document).on('ifUnchecked', '.attributes', function (event) {
                $('#options_' + $(this).attr('id')).slideUp();
            });
        <?php endif ?>

        //$('#cost').removeAttr('required');
        $(document).on('change', '#type', function () {
            $('#unit option').each(function(index, option){
                $(option).prop('disabled', false);
            });
            var t = $(this).val();
            if (t !== 'standard' && t !== 'raw' && t !== 'subproduct' && t !== 'pfinished' && t !== 'combo') {
                $('.standard').slideUp();
                $('#unit').attr('disabled', true);
                $('#cost').attr('disabled', true);
                $('#track_quantity').iCheck('uncheck');
            } else if (t === 'combo') {
                $('.standard').slideUp();
                $('#cost').attr('disabled', true);
                $('#track_quantity').iCheck('uncheck');
                $('#unit option').each(function(index, option){
                    if ($(option).data('operationvalue') > 0) {
                        $(option).prop('disabled', true);
                    } else {
                        if ($(option).val() == "<?= $product->unit ?>") {
                            $('#unit').select2('val', $(option).val()).trigger('change');
                        }
                    }
                });
            } else {
                $('.standard').slideDown();
                $('#track_quantity').iCheck('check');
                $('#unit').attr('disabled', false);
                $('#cost').attr('disabled', false);
            }

            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#file_link').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'file_link');
            } else {
                $('.digital').slideDown();
                $('#file_link').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'file_link');
            }
            if (t !== 'combo' && t !== 'pfinished' && t !== 'subproduct') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideDown();
            }
            if (t == 'standard' || t == 'combo') {
                $('.standard_combo').slideDown();
            } else {
                $('.standard_combo').slideUp();
            }

            if (t == 'subproduct' || t == 'pfinished') {
                $('.title-text-ci').text("<?= lang('production_products') ?>");
            } else if (t == 'combo') {
                $('.title-text-ci').text("<?= lang('combo_products') ?>");
            }

        });


        $('#add_item').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');



        $(document).on('change', '.rquantity, .rprice', function () {
            var quantity = $(this).val();
            var item_id = $(this).data('item');
            if (items[item_id]) {
                items[item_id].qty = quantity;
            }
            loadItems();
        });

        $(document).on('change', '.combo_item_unit', function(){
            var unit = $(this).val();
            var item_id = $(this).data('item');
            var option_selected = $(this).find(':selected');
            var operator = $(option_selected).data('operator');
            var operator_value = $(option_selected).data('operatorvalue');
            if (items[item_id]) {
                items[item_id].unit = unit;
            }
            loadItems();
        });

        $(document).on('click', '.del', function () {
            var id = $(this).attr('id');
            delete items[id];
            $(this).closest('#row_' + id).remove();
            loadItems();
        });

        var su = 2;
        $(document).on('click', '#addSupplier', function () {
            if (su <= 5) {
                $('#supplier_1').select2('destroy');
                $('#supplier_1').select2('destroy');
                var html =
                        '<div style="clear:both;height:5px;">'+
                        '</div>'+
                        '<div class="col-sm-3">'+
                            '<div class="form-group">'+
                                '<input type="hidden" name="supplier_' + su + '", class="form-control" id="supplier_' + su + '" placeholder="<?= lang("select") . ' ' . lang("supplier") ?>" style="width:100%;display: block !important;" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-3">'+
                            '<div class="form-group">'+
                                '<input type="text" name="supplier_' + su + '_date" class="form-control tip" id="supplier_' + su + '_date" placeholder="<?= lang('supplier_date') ?>" readonly />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-3">'+
                            '<div class="form-group">'+
                                '<input type="text" name="supplier_' + su + '_part_no" class="form-control tip" id="supplier_' + su + '_part_no" placeholder="<?= lang('supplier_part_no') ?>" />'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-sm-3">'+
                            '<div class="form-group">'+
                                '<input type="text" name="supplier_' + su + '_price" class="form-control tip" id="supplier_' + su + '_price" placeholder="<?= lang('supplier_price') ?>" />'+
                            '</div>'+
                        '</div>';
                $('#ex-suppliers').append(html);
                var sup = $('#supplier_' + su);
                suppliers(sup);
                su++;
            } else {
                bootbox.alert('<?= lang('max_reached') ?>');
                return false;
            }
        });


        var variants = <?=json_encode($vars);?>;
        var preferences = <?=json_encode($preferences);?>;
        $(".select-tags-preferences").select2({
            tags: preferences,
            tokenSeparators: [","],
            multiple: true
        });

        $(document).on('ifChecked', '#attributes', function (e) {
            $.ajax({
                url:site.base_url+"products/validate_add_variantes/<?= $product->id ?>"
            }).done(function(data){
                if (data) {
                    $('#attr-con').slideDown();
                } else {
                    swal(
                      '<?= lang('toast_warning_title') ?>',
                      '<?= lang('can_not_add_product_variants') ?>',
                      'warning'
                    );
                    $('input[id="attributes"]').removeAttr('checked').iCheck('update');
                }
            });
        });

        $(document).on('ifUnchecked', '#attributes', function (e) {
            $.ajax({
                url:site.base_url+"products/validate_add_variantes/<?= $product->id ?>"
            }).done(function(data){
                if (data) {
                    <?php if ($this->Settings->product_variant_per_serial == 0): ?>
                        $(".select-tags").select2("val", "");
                        $('.attr-remove-all').trigger('click');
                    <?php endif ?>
                    $('#attr-con').slideUp();
                } else {
                    swal(
                      '<?= lang('toast_warning_title') ?>',
                      '<?= lang('can_not_unset_product_variants') ?>',
                      'warning'
                    );
                    $('input[id="attributes"]').prop('checked', 'checked').iCheck('update');
                }
            });
        });
        $(document).on('change', '#attributesInput', function (e) {
            setTimeout(function() {
                $('#addAttributes').trigger('click');
                $('#attributesInput').select2('val', '');
            }, 500);
        });
        $(document).on('click', '#addAttributes',function (e) {
            e.preventDefault();
            let _valorListaBase = parseFloat($('input[name="pg[]"][data-pgbase="1"]').val()); 
            attrs = $('#attributesInput').val(), attrs;
            let _selectedWeight = $('#attributesInput').find('option:selected').data('weight');
            for (var i in attrs) {
                if (attrs[i] !== '') {

                    if (site.settings.set_product_variant_suffix == 1) {
                        $('#attrSuffixTable').show().append('<tr class="attr_suffix_row" data-attrname="'+attrs[i]+'">'+
                            '<td>'+
                                attrs[i]+
                            '</td>'+
                            '<td>'+
                                '<input type="text" name="attr_suffix[]" class="form-control">'+
                            '</td>'+
                        '</tr>');
                    }

                    $('#attrTable').show().append('<tr class="attr">'+
                                                    '<td>'+
                                                        '<input type="hidden" name="attr_name[]" value="' + attrs[i] + '">'+
                                                        '<input type="hidden" name="attr_warehouse[]" value="">'+
                                                        '<input type="hidden" name="attr_quantity[]" value="0">'+
                                                        '<span>' + attrs[i] + '</span>'+
                                                    '</td>'+
                                                    '<td class="price text-right" style="display:none" >'+
                                                        '<input type="text" name="attr_price[]" class="form-control" value="0">'+
                                                    '</td>'+
                                                    '<td class="price text-right">'+ //precio
                                                        '<input type="number" name="attr_full_price[]" class="form-control" value="'+_valorListaBase+'">'+
                                                    '</td>'+
                                                    '<td class="price text-right">'+ //precio minimo
                                                        '<input type="number" name="attr_minimun_price[]" class="form-control" value="0">'+
                                                    '</td>'+
                                                    '<td class="text-left form-group">'+
                                                        '<div class="input-group">'+
                                                            '<input name="attr_code[]"  class="form-control validate_code" data-module="variant">'+
                                                            '<span class="input-group-addon pointer" id="random_num" style="padding: 1px 10px;" data-toggle="tooltip" title="'+lang.random_code_text+'">'+
                                                                '<i class="fa fa-random"></i>'+
                                                            '</span>'+
                                                            '<span class="input-group-addon pointer" id="consecutive_code" style="padding: 1px 10px;" data-toggle="tooltip" title="'+lang.consecutive_code_text+'">'+
                                                                '<b>123</b>'+
                                                            '</span>'+
                                                        '</div>'+
                                                        '<input type="hidden" name="attr_code_consecutive[]" class="form-control code_consecutive_setted">'+
                                                    '</td>'+

                                                    '<td class="price text-right">'+
                                                        '<input type="text" name="attr_weight[]" class="form-control" value="' + _selectedWeight + '">'+
                                                    '</td>'+

                                                    '<td class="text-right">'+
                                                        '<input type="file" data-browse-label="<?= lang('browse') ?>" name="attr_image[]" data-show-upload="false" data-show-preview="false" accept="image/*" class="form-control file">'+
                                                    '</td>'+
                                                    '<td class="text-center">'+
                                                        '<i class="fa fa-times delAttr"></i>'+
                                                    '</td>'+
                                                '</tr>');
                    $('input[type="file"]').fileinput();
                }
            }
        });

        $(document).on('change', 'input[name="attr_full_price[]"]', function() {
            var fullPrice = parseFloat($(this).val());  // Obtiene el valor del input actual
            var valorListaBase = parseFloat($('input[name="pg[]"][data-pgbase="1"]').val()); // Obtiene el valor de la Lista Base
            if (fullPrice < valorListaBase) {  // Valida si el precio actual es menor que la Lista Base
                header_alert('error', `El precio ingresado no puede ser inferior al precio base establecido.` );
                $(this).val(valorListaBase); // Opcional: cambiar el valor automáticamente al de la lista base
            }else{
                let _overPrice = fullPrice - valorListaBase;
                $(this).closest('tr').find('input[name="attr_price[]"]').val(parseFloat(_overPrice));
            }
        });

        $(document).on('change', 'input[name^="p_variant_fullPrice"]', function() {
            console.log('Cambio detectado en el precio total');

            var fullPrice = parseFloat($(this).val());  // Obtiene el valor del input actual
            var valorListaBase = parseFloat($('input[name="pg[]"][data-pgbase="1"]').val()); // Obtiene el valor de la Lista Base

            if (fullPrice < valorListaBase) {  // Valida si el precio actual es menor que la Lista Base
                header_alert('error', 'El precio ingresado no puede ser inferior al precio base establecido.');
                $(this).val(valorListaBase); // Opcional: cambiar el valor automáticamente al de la lista base
            } else {
                let _overPrice = fullPrice - valorListaBase;
                $(this).closest('tr').find('input[name^="p_variant_price"]').val(parseFloat(_overPrice));
            }
        });

        $(document).on('change', 'input[name="attr_minimun_price[]"]', function() {
            var minPrice = parseFloat($(this).val());  // Obtiene el precio mínimo ingresado
            var fullPrice = parseFloat($(this).closest('tr').find('input[name="attr_full_price[]"]').val());  // Obtiene el precio completo de la misma fila
            if (minPrice < 0) {
                header_alert('error', 'El precio mínimo no puede ser inferior a cero.');
                $(this).val(fullPrice);  // Restablece el valor al precio completo
            }
            if (minPrice > fullPrice) {
                header_alert('error', 'El precio mínimo no puede ser mayor que el precio.');
                $(this).val(fullPrice);  // Restablece el valor al precio completo
            }
        });

        $(document).on('change', 'input[name="pg[]"][data-pgbase="1"]', function() { // si cambia la base entramos a cambiar los valores de la tabla edicion variantes
            let newBasePrice = parseFloat($(this).val());
            $('#attrTable tbody tr').each(function() {
                let row = $(this);
                let variantPrice = parseFloat(row.find('input[name^="p_variant_price["]').val()) || 0; // sobreprecio
                let fullPriceActual = parseFloat(row.find('input[name^="p_variant_fullPrice["]').val()) || 0; // precio full
                let minimumPrice = parseFloat(row.find('input[name^="p_variant_minimumPrice["]').val()) || 0; // minimo precio
                let fullPrice = newBasePrice + variantPrice; // <- nueva lista base + sobreprecio
                // console.log("variantPrice", variantPrice);
                // console.log("fullPriceActual", fullPriceActual);
                // console.log("minimumPrice", minimumPrice);
                // console.log("fullPrice", fullPrice);
                if (fullPriceActual != fullPrice) {
                    row.find('input[name^="p_variant_fullPrice["]').val(parseFloat(fullPrice)); // Actualiza el precio total
                    row.find('input[name^="p_variant_price["]').val(parseFloat(0)); // Actualiza el bobreprecio
                    if (minimumPrice < fullPrice) {  // Validar que el precio mínimo no sea mayor que el precio total
                        row.find('input[name^="p_variant_minimumPrice["]').val(minimumPrice.toFixed(2)); // Actualiza el precio mínimo
                    } else {
                        row.find('input[name^="p_variant_minimumPrice["]').val(parseFloat(0)); 
                    }
                }
            });
        });

        $(document).on('ifChecked', '#preferences', function (e) {
            $('#preferences-con').slideDown();
            $('.div_preference_category').fadeIn();
        });
        $(document).on('ifUnchecked', '#preferences', function (e) {
            $(".select-tags-preferences").select2("val", "");
            $('.preferences-remove-all').trigger('click');
            $('#preferences-con').slideUp();
            $('.div_preference_category').fadeOut();
        });

        $(document).on('click', '.delAttr', function () {
            $(this).closest("tr").remove();
            delete_variant_suffix();
        });
        $(document).on('click', '.delPreference', function () {
            $(this).closest("tr").remove();
        });
        $(document).on('click', '.attr-remove-all', function () {
            $('#attrTable tbody').empty();
            $('#attrTable').hide();
            delete_variant_suffix();
        });
        $(document).on('click', '.preferences-remove-all', function () {
            $('#preferencesTable tbody').empty();
            $('#preferencesTable').hide();
        });
        var row, warehouses = <?= json_encode($warehouses); ?>;
        // $(document).on('click', '.attr td:not(:last-child)', function () {
        //     row = $(this).closest("tr");
        //     $('#aModalLabel').text(row.children().eq(0).find('span').text());
        //     $('#awarehouse').select2("val", (row.children().eq(1).find('input').val()));
        //     $('#aquantity').val(row.children().eq(2).find('span').text());
        //     $('#aprice').val(row.children().eq(3).find('span').text());
        //     $('#aModal').appendTo('body').modal('show');
        // });

        $(document).on('click', '#updateAttr', function () {
            var wh = $('#awarehouse').val(), wh_name;
            $.each(warehouses, function () {
                if (this.id == wh) {
                    wh_name = this.name;
                }
            });
            row.children().eq(1).html('<input type="hidden" name="attr_warehouse[]" value="' + wh + '"><input type="hidden" name="attr_wh_name[]" value="' + wh_name + '"><span>' + wh_name + '</span>');
            row.children().eq(2).html('<input type="hidden" name="attr_quantity[]" value="' + ($('#aquantity').val() ? $('#aquantity').val() : 0) + '"><span>' + $('#aquantity').val() + '</span>');
            row.children().eq(3).html('<input type="hidden" name="attr_price[]" value="' + $('#aprice').val() + '"><span>' + currencyFormat($('#aprice').val()) + '</span>');

            $('#aModal').modal('hide');
        });

        setTimeout(function() {
            $('#type').trigger('change');
            $('#wappsi_invoicing').trigger('change');
        }, 800);

        $(document).on('change', '#billers', function(){
            $('#billers_changed').val(1);
        });

        $('#based_on_gram_value').trigger('change');

    });

    <?php if ($product) { ?>
    $(document).ready(function () {
        $(document).on('click', '#enable_wh', function () {
            var whs = $('.wh');
            $.each(whs, function () {
                $(this).val($('#v' + $(this).attr('id')).val());
            });
            $('#warehouse_quantity').val(1);
            $('.wh').attr('disabled', false);
            $('#show_wh_edit').slideDown();
        });
        $(document).on('click', '#disable_wh', function () {
            $('#warehouse_quantity').val(0);
            $('#show_wh_edit').slideUp();
        });
        $('#show_wh_edit').hide();
        $('.wh').attr('disabled', true);
        var t = "<?=$product->type?>";

            if (t !== 'standard' && t !== 'raw' && t !== 'subproduct' && t !== 'pfinished' && t !== 'combo') {
                $('.standard').slideUp();
                $('#unit').attr('disabled', true);
                $('#cost').attr('disabled', true);
                $('#track_quantity').iCheck('uncheck');
            } else if (t === 'combo') {
                $('.standard').slideUp();
                $('#cost').attr('disabled', true);
                $('#track_quantity').iCheck('uncheck');
                $('#unit option').each(function(index, option){
                    if ($(option).data('operationvalue') > 0) {
                        $(option).prop('disabled', true);
                    }
                });
            } else {
                $('.standard').slideDown();
                $('#track_quantity').iCheck('check');
                $('#unit').attr('disabled', false);
                $('#cost').attr('disabled', false);
            }

            if (t !== 'digital') {
                $('.digital').slideUp();
                $('#file_link').removeAttr('required');
                $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'file_link');
            } else {
                $('.digital').slideDown();
                $('#file_link').attr('required', 'required');
                $('form[data-toggle="validator"]').bootstrapValidator('addField', 'file_link');
            }
            if (t !== 'combo' && t !== 'pfinished' && t !== 'subproduct') {
                $('.combo').slideUp();
            } else {
                $('.combo').slideDown();
            }
            if (t == 'standard' || t == 'combo') {
                $('.standard_combo').slideDown();
            } else {
                $('.standard_combo').slideUp();
            }

        $('#add_item').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'add_item');
        //$("#code").parent('.form-group').addClass("has-error");
        //$("#code").focus();
        $("#product_image").parent('.form-group').addClass("text-warning");
        $("#images").parent('.form-group').addClass("text-warning");
        $.ajax({
            type: "get", async: false,
            url: "<?= admin_url('products/getSubCategories') ?>/" + <?= $product->category_id ?>,
            dataType: "json",
            success: function (scdata) {
                if (scdata != null) {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('select_subcategory'), lang('subcategory')) ?>").select2({
                        placeholder: "<?= sprintf(lang('select_category_to_load'), lang('category')) ?>",
                        minimumResultsForSearch: 7,
                        data: scdata
                    }).trigger('change');
                } else {
                    $("#subcategory").select2("destroy").empty().attr("placeholder", "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>").select2({
                        placeholder: "<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>",
                        minimumResultsForSearch: 7,
                        data: [{id: '', text: '<?= sprintf(lang('no_subcategory'), lang('subcategory')) ?>'}]
                    });
                }
            }
        });
        var supplier_prices = JSON.parse('<?= $this->sma->utf8Encode(json_encode($supplier_prices)) ?>');

        if (Object.keys(supplier_prices).length == 0) {
                suppliers($('#supplier' + 1));
        }
        var supplier_price_num = 1;
        $.each(supplier_prices, function(index, arr){
            if (supplier_price_num == 1) {
                select_supplier('supplier1', arr.supplier_id);
                $('#supplier_date').val(arr.date);
                $('#supplier_price').val(arr.price);
                $('#supplier_part_no').val(arr.part_no);
                supplier_price_num++;
            } else {
                $('#addSupplier').click();
                select_supplier('supplier_'+supplier_price_num, arr.supplier_id);
                $('#supplier_'+supplier_price_num+'_date').val(arr.date);
                $('#supplier_'+supplier_price_num+'_price').val(arr.price);
                $('#supplier_'+supplier_price_num+'_part_no').val(arr.part_no);
                supplier_price_num++;
            }
        });
        // $('#supplier1').addClass('rsupplier');
        function select_supplier(id, v) {
            $('#' + id).val(v).select2({
                minimumInputLength: 1,
                data: [],
                initSelection: function (element, callback) {
                    $.ajax({
                        type: "get", async: false,
                        url: "<?= admin_url('suppliers/getSupplier') ?>/" + $(element).val(),
                        dataType: "json",
                        success: function (data) {
                            callback(data[0]);
                        }
                    });
                },
                ajax: {
                    url: site.base_url + "suppliers/suggestions",
                    dataType: 'json',
                    quietMillis: 15,
                    data: function (term, page) {
                        return {
                            term: term,
                            limit: 10
                        };
                    },
                    results: function (data, page) {
                        if (data.results != null) {
                            return {results: data.results};
                        } else {
                            return {results: [{id: '', text: lang.no_match_found}]};
                        }
                    }
                }
            });
        }
    });
    <?php } ?>
    $(document).ready(function () {
        $('#enable_wh').trigger('click');
        $(document).on('change', '#unit', function(e) {
            var v = $(this).val();
            if (v) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: "<?= admin_url('products/getSubUnits') ?>/" + v,
                    dataType: "json",
                    success: function (data) {
                        $('#default_sale_unit').select2("destroy").empty().select2({minimumResultsForSearch: 7});
                        $('#default_purchase_unit').select2("destroy").empty().select2({minimumResultsForSearch: 7});
                        $.each(data, function () {
                            $("<option />", {value: this.id, text: this.name+' ('+this.code+')'}).appendTo($('#default_sale_unit'));
                            $("<option />", {value: this.id, text: this.name+' ('+this.code+')'}).appendTo($('#default_purchase_unit'));
                        });
                        $('#default_sale_unit').select2('val', v);
                        $('#default_purchase_unit').select2('val', v);
                    },
                    error: function () {
                        bootbox.alert('<?= lang('ajax_error') ?>');
                    }
                });
            } else {
                $('#default_sale_unit').select2("destroy").empty();
                $('#default_purchase_unit').select2("destroy").empty();
                $("<option />", {value: '', text: '<?= lang('select_unit_first') ?>'}).appendTo($('#default_sale_unit'));
                $("<option />", {value: '', text: '<?= lang('select_unit_first') ?>'}).appendTo($('#default_purchase_unit'));
                $('#default_sale_unit').select2({minimumResultsForSearch: 7}).select2('val', '');
                $('#default_purchase_unit').select2({minimumResultsForSearch: 7}).select2('val', '');
            }
        });
        $('#digital_file').removeAttr('required');
        $('form[data-toggle="validator"]').bootstrapValidator('removeField', 'digital_file');


        $(document).on('change', '#default_sale_unit', function(){
            var v = $(this).val();
            $('#default_purchase_unit').select2('val', v);
        });

        setTimeout(function() {
            $('#default_purchase_unit').select2('readonly', true);
            if (site.settings.tax_method != 2) {
                $('#tax_method').select2('readonly', true);
            }
            $('#product_has_multiple_units').iCheck('<?= $product->has_multiple_units ? "check" : "uncheck" ?>').trigger('change');
            $('.pg_price').first().trigger('change');
        }, 1000);
    });

    $(document).on('change', '.pg_price', function(){
        pg_prices_arr = [];
        $('.pg_price').each(function(index, input){
            var base = $(input).data('pgbase');
            if (pg_prices_arr[$(input).val()] === undefined) {
                pg_prices_arr[$(input).val()] = 1;
            } else {
                pg_prices_arr[$(input).val()]++;
            }
            if (base == 1) {
                $('#price').val($(input).val());
            }
            if (parseFloat($(input).val()) <= parseFloat($('#cost').val())) {
                command: toastr.error('El precio definido en la lista ('+parseFloat($(input).val())+') es menor al costo del producto', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "8000",
                    "extendedTimeOut": "1000",
                });
            }
        });
        setTimeout(function() {
            $(pg_prices_arr).each(function(pg_price_index, pg_price_val){
                if (pg_price_val === undefined){
                    return;
                }
                if (pg_price_val > 1) {
                    command: toastr.warning('Existen dos o más listas con el mismo precio', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "8000",
                        "extendedTimeOut": "1000",
                    });
                }
            });
        }, 850);
        // $('#price').trigger('change');
        $('#modalPrices').modal('hide');
    });

    $(document).on('click', '#add_item_units',function (e) {
        units_selected = $('.m_unit_id');
        tax_method = $('#tax_method').val();
        consumption_sale_tax = $('#consumption_sale_tax').val();
        num_units = units_selected.length;
        us_arr = [];
        $.each(units_selected, function(index, unit){
            us_arr[$(unit).val()] = index;
        });
        e.preventDefault();
        attrs = $('select[name="units"]').val();
        $(attrs).each(function(index, attr){
            if (us_arr[attr] == undefined) {
                $.ajax({
                    url : "<?= admin_url('products/add_unit_selected/') ?>"+attr+"/"+num_units+"/"+tax_method+"/"+consumption_sale_tax
                }).done(function(data){
                    $(data).appendTo('#table_units tbody');
                    $('select[name="units"] option:selected').remove();
                    $('select[name="units"]').select2('val', []);
                    $('input[type="checkbox"]').not('.skip').iCheck({
                        checkboxClass: 'icheckbox_square-blue',
                        radioClass: 'iradio_square-blue',
                        increaseArea: '20%'
                    });
                });
            } else {
                command: toastr.warning('La unidad seleccionada ya está agregada', 'Unidad duplicada', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
            num_units++;
        });
    });

    $(document).on('click', '#delete_unit', function(){
        row_id = $(this).data('rowid');
        $.ajax({
            url:"<?= admin_url('products/delete_product_unit/').$product->id.'/' ?>"+row_id,
            dataType : "JSON"
        }).done(function(data){
            if (data.status == 1) {
                command: toastr.success(data.message, 'Correcto', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
                $('.row_id_'+row_id).remove();
            } else {
                command: toastr.error(data.message, 'Error', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            }
        });

    });

    $(document).on('ifChecked', '#product_has_multiple_units', function(){
        $('.one_unit').css('display', 'none');
        $('#unit').prop('required', false);
        $('input[name="product_unit_id"]').prop('required', true);
        $('.multiple_units').css('display', '');
        $('.div_default_sale_unit').fadeOut();
        $('.div_default_purchase_unit').fadeOut();
        $('#default_sale_unit').select2('readonly', true);
    });

    $(document).on('ifUnchecked', '#product_has_multiple_units', function(){
        $('.one_unit').css('display', '');
        $('#unit').prop('required', true);
        $('input[name="product_unit_id"]').prop('required', false);
        $('.multiple_units').css('display', 'none');
        $('#default_sale_unit').select2('readonly', false);
        $('.div_default_sale_unit').fadeIn();
        $('.div_default_purchase_unit').fadeIn();
    });

    $(document).on('click', 'input[name="product_unit_id"]', function(){
        $('#unit').select2('val', $(this).val()).trigger('change');
        row = $(this).parents('.row_unit');
        unit_price = row.find('.unit_price');
        index_upr = $('.unit_price').index(unit_price);
        $.each($('.unit_price'), function(index, upr){
            if (index != index_upr) {
                $(upr).prop('readonly', false).prop('required', true);
            }
        });

        $(unit_price).prop('readonly', true).prop('required', false).val($('#price').val());
        command: toastr.warning('Usted ha cambiado la unidad del producto, por lo que dicha unidad tomará el valor actual del producto', 'Atención', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "6000",
                    "extendedTimeOut": "1000",
                });
    });
    $(document).on('change', '#cost, #price, #tax_rate, #tax_method', function(){
        var cost = parseFloat($('#cost').val());
        var price = parseFloat($('#price').val());
        var tax_rate = $('#tax_rate').val();
        var tax_method = $('#tax_method').val();
        var category_suggested_tax_rate = $('#category option:selected').data('suggestedrate');
        if (category_suggested_tax_rate && category_suggested_tax_rate != tax_rate) {
            bootbox.alert('Está asignando una tarifa diferente a la sugerida por la categoría', function () {
                        $('#add_item').focus();
                    });
        }
        if (cost > 0 && price > 0 && tax_rate > 0 && tax_method == 0) {
            var pr_tax = tax_rates[tax_rate];
            pr_tax_val = 0;
            if (ptax = calculateTax(pr_tax, price, tax_method)) {
                pr_tax_val = ptax[0];
            }
            var net_price = price - pr_tax_val;
            if (ptax = calculateTax(pr_tax, cost, tax_method)) {
                pr_tax_val = ptax[0];
            }
            var net_cost = cost - pr_tax_val;
        } else {
            var net_price = price;
            var net_cost = cost;
        }
        if (net_price > 0 && net_cost > 0) {
            var margin = formatDecimals((((net_price - net_cost) / net_price) * 100), 2);
            $('#profitability_margin').val(margin);
            if (margin <= 0) {
                command: toastr.error('El precio del producto es similar o menor al costo.', '¡Atención!', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "8000",
                    "extendedTimeOut": "1000",
                });
            }
        }
        if (price > 0) {
            if(set_ipoconsumo_values($('#consumption_sale_tax').val(), $('#consumption_sale_tax').val(), $('#tax_method').val(), false)){
                $('.unit_price[readonly]').val(price);
            }
        }
    });
    $(document).on('change', '#cost', function(){
        if ($('#purchase_tax_rate_2_id').val() > 0 && $('#purchase_tax_rate_2_id option:selected').data('valuetype') == 2) {
            calculated_second_tax = calculate_second_tax($('#purchase_tax_rate_2_percentage').val(), parseFloat($('#cost').val()), $('#tax_rate').val(), $('#tax_method').val(), true);
            $('#consumption_purchase_tax').val(calculated_second_tax[1]);
        }
    });
    var old_pg_price = 0;
    $(document).on('focus', '.pg_price', function(){
        old_pg_price = $(this).val();
    }).on('change', '.pg_price', function(){
        pg_price = $(this);
        index = pg_price.index($('.pg_price'));
        pg_margin = $('.pg_margins').eq(index);
        pg_base = pg_price.data('pgbase');
        if (pg_base == 1) {
            $('#price').val(pg_price.val()).trigger('change');
        }
        var cost = parseFloat($('#cost').val());
        var price = parseFloat(pg_price.val());
        var tax_rate = $('#tax_rate').val();
        var tax_method = $('#tax_method').val();
        if (cost >= price && site.settings.disable_product_price_under_cost == 1) {
            pg_price.val(old_pg_price).select();
            command: toastr.error('El precio para la lista no puede estar por debajo del costo del producto', 'Precio incorrecto', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
            return false;
        }
        if (cost > 0 && price > 0 && tax_rate > 0 && tax_method == 0) {
            var pr_tax = tax_rates[tax_rate];
            pr_tax_val = 0;
            if (ptax = calculateTax(pr_tax, price, tax_method)) {
                pr_tax_val = ptax[0];
            }
            var net_price = price - pr_tax_val;
            if (ptax = calculateTax(pr_tax, cost, tax_method)) {
                pr_tax_val = ptax[0];
            }
            var net_cost = cost - pr_tax_val;
        } else {
            var net_price = price;
            var net_cost = cost;
        }

        if (net_price > 0 && net_cost > 0) {
            var margin = formatDecimals((((net_price - net_cost) / net_price) * 100), 2);
            pg_margin.val(margin);
        }

    });
    $(document).on('click', '#edit_product', function(){
        if ($('#edit_form').valid()) {
            $('#edit_form').submit();
        } else {
            command: toastr.error('Aún hay campos obligatorios sin diligenciar.', '¡Atención!', {
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "8000",
                "extendedTimeOut": "1000",
            });
        }
    });
</script>

<div class="modal" id="aModal" tabindex="-1" role="dialog" aria-labelledby="aModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><i
                            class="fa fa-2x">&times;</i></span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="aModalLabel"><?= lang('add_product_manually') ?></h4>
            </div>
            <div class="modal-body" id="pr_popover_content">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="awarehouse" class="col-sm-4 control-label"><?= lang('warehouse') ?></label>
                        <div class="col-sm-8">
                            <?php
                            $wh[''] = '';
                            foreach ($warehouses as $warehouse) {
                                $wh[$warehouse->id] = $warehouse->name;
                            }
                            echo form_dropdown('warehouse', $wh, '', 'id="awarehouse" class="form-control"');
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aquantity" class="col-sm-4 control-label"><?= lang('quantity') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aquantity">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="aprice" class="col-sm-4 control-label"><?= lang('price') ?></label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="aprice">
                        </div>
                    </div>

                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="updateAttr"><?= lang('submit') ?></button>
            </div>
        </div>
    </div>
</div>


<script type="text/javascript">

    $(document).on('change', '.unit_price', function(){
        set_ipoconsumo_values($('#consumption_sale_tax').val(), $('#consumption_sale_tax').val(), $('#tax_method').val(), false);
    });

    $(document).on('change', '.pg_price', function(){
        set_ipoconsumo_values($('#consumption_sale_tax').val(), $('#consumption_sale_tax').val(), $('#tax_method').val(), false);
    });

    $(document).on('change', '#sale_tax_rate_2_percentage', function(){
        set_ipoconsumo_values($('#consumption_sale_tax').val(), $(this).val(), $('#tax_method').val(), true);
    });

    $(document).on('change', '#consumption_sale_tax', function(){
        prev_val = parseFloat($(this).data('prevval'));
        new_val = parseFloat($(this).val());
        tax_method = $('#tax_method').val();

        if (new_val != prev_val && tax_method == 0) {
            bootbox.confirm({
                message: "El valor de 2° impuesto para venta ha cambiado, ¿Actualizar todos los precios para incluir el nuevo 2° impuesto?",
                buttons: {
                    confirm: {
                        label: 'Si, actualizar precios',
                        className: 'btn-success send-submit-sale btn-full'
                    },
                    cancel: {
                        label: 'No, mantener precios',
                        className: 'btn-danger btn-full'
                    }
                },
                callback: function (result) {
                    if (result) {
                        if (set_ipoconsumo_values(prev_val, new_val, tax_method, true)) {
                            $(this).attr('data-prevval', new_val);
                        }
                    } else {
                        if (set_ipoconsumo_values(prev_val, new_val, tax_method, false)) {
                            $(this).data('prevval', new_val);
                        }
                    }
                }
            });
        }
    });

    $(document).on('change', '#purchase_tax_rate_2_percentage', function(){
        set_ipoconsumo_values($('#consumption_purchase_tax').val(), $(this).val(), $('#tax_method').val(), true, false);
    });

    $(document).on('change', '#consumption_purchase_tax', function(){
        prev_val = $(this).data('prevval');
        new_val = $(this).val();
        tax_method = $('#tax_method').val();

        if (new_val != prev_val && tax_method == 0) {
            bootbox.confirm({
                message: "El valor de 2° impuesto para compra ha cambiado, ¿Actualizar el costo para incluir el nuevo 2° impuesto?",
                buttons: {
                    confirm: {
                        label: 'Si, actualizar costo',
                        className: 'btn-success send-submit-sale btn-full'
                    },
                    cancel: {
                        label: 'No, mantener costo',
                        className: 'btn-danger btn-full'
                    }
                },
                callback: function (result) {
                    if (result) {
                        if (set_ipoconsumo_values(prev_val, new_val, tax_method, true, false)) {
                            $(this).data('prevval', new_val);
                        }
                    } else {
                        if (set_ipoconsumo_values(prev_val, new_val, tax_method, false, false)) {
                            $(this).data('prevval', new_val);
                        }
                    }
                }
            });
        }
    });


function set_ipoconsumo_values(prev_val, new_val, tax_method, update_prices, sales = true )
{
    new_val =
            $((sales ? '#sale_tax_rate_2_id' : '#purchase_tax_rate_2_id')+' option:selected').data('valuetype') == 1 ?
                $((sales ? '#consumption_sale_tax' : '#consumption_purchase_tax')).val() :
                $((sales ? '#sale_tax_rate_2_percentage' : '#purchase_tax_rate_2_percentage')).val()
            ;
    price = (sales ? $('#price').val() : $('#cost').val());
    calculated_second_tax = calculate_second_tax(new_val, parseFloat(price), $('#tax_rate').val(), $('#tax_method').val(), true);
    calculated_second_tax_val = calculated_second_tax[1] > 0 ? calculated_second_tax[1] : (sales ? '<?= $product->consumption_sale_tax ?>' : '<?= $product->consumption_purchase_tax ?>');
    $((sales ? '#consumption_sale_tax' : '#consumption_purchase_tax')).val(calculated_second_tax_val);
}

function delete_variant_suffix(){
    setTimeout(function() {
        $('.attr_suffix_row').each(function(index, asrow){
            var rdelete = true;
            $('input[name="attr_name[]"]').each(function(index, input){
                if ($(input).val() == $(asrow).data('attrname')) {
                    rdelete = false;
                }
            });
            if (rdelete == true) {
                $(asrow).remove()
            }
        });
    }, 450);
}

var prev_src = $('.main_image').prop('src');
$(document).on("change", "#product_image", function(){
    $('.main_image').prop('src', prev_src);
    input = this;
    var file, img;
    var _URL = window.URL || window.webkitURL;
    if ((file = this.files[0])) {
        img = new Image();
        var objectUrl = _URL.createObjectURL(file);
        img.onload = function () {
            _URL.revokeObjectURL(objectUrl);
            if (this.height > site.settings.iheight) {
                command: toastr.warning('La imagen no puede tener más de '+site.settings.iheight+'px de altura.', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
                return false;
            } else {
                var reader = new FileReader();
                reader.onload = function (e) {
                  $('.main_image').attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        };
        img.src = objectUrl;
    }
});


$(document).on('click', '.gallery_images_input > .fileinput-remove', function(){
    $('.new_img_added').remove();
});
$(function() {
    var imagesPreview = function(input, placeToInsertImagePreview) {
        $('.new_img_added').remove();
        if (input.files) {
            var filesAmount = input.files.length;
            for (i = 0; i < filesAmount; i++) {
                var reader = new FileReader();
                num = num_gallery_images;
                reader.onload = function(event) {
                    html = '<div class="col-md-4 new_img_added">'+
                        '<label>Imágen '+num+' </label>'+
                        '<br>'+
                        '<img style="width: 300px;" class="gallery_images" data-imgnum="'+num+'" src="'+event.target.result+'">'+
                    '</div>';
                    $(html).appendTo('.gallery_preview');
                    num++;
                }
                reader.readAsDataURL(input.files[i]);
            }
        }
    };
    $('#images').on('change.bs.fileinput', function() {
        imagesPreview(this, '.gallery_preview');
    });
});

$(document).on('click', '.delete_image', function(){
    if ($(this).hasClass('btn-outline')) {
        $(this).removeClass('btn-outline');
        $('img[data-imgnum="'+$(this).data('imgnum')+'"]').removeClass('img-deleting');
        $(this).parent().find('i.fa').removeClass('fa-times').addClass('fa-trash-o');
    } else {
        $(this).addClass('btn-outline');
        $('img[data-imgnum="'+$(this).data('imgnum')+'"]').addClass('img-deleting');
        $(this).parent().find('i.fa').removeClass('fa-trash-o').addClass('fa-times');
    }
    var ids = "";
    $.each($('.delete_image'), function(index, button){
        if ($(button).hasClass('btn-outline')) {
            ids += $(button).data('phid')+", ";
        }
    });
    $('#deleted_images').val(ids);
});

$(document).on('click', '.delete_main_image', function(){
    if ($(this).hasClass('btn-outline')) {
        $(this).removeClass('btn-outline');
        $('.main_image').removeClass('img-deleting');
        $(this).parent().find('i.fa').removeClass('fa-times').addClass('fa-trash-o');
        $('#deleted_main_image').val(0);
    } else {
        $(this).addClass('btn-outline');
        $('.main_image').addClass('img-deleting');
        $(this).parent().find('i.fa').removeClass('fa-trash-o').addClass('fa-times');
        $('#deleted_main_image').val(1);
    }
});


function add_product_item(item)
{
    if (item == null) {
        return false;
    }
    item_id = item.id;
    if (items[item_id]) {
        items[item_id].qty = (parseFloat(items[item_id].qty) + 1).toFixed(2);
    } else {
        items[item_id] = item;
    }
    loadItems();
    return true;
}

function loadItems()
{

    var pp = 0;
    var total_cost_total = 0;
    var total_items = 0;
    var decimales = null;

    // if ($('#type').val() != 'combo' && site.settings.rounding == 1) {
    //     decimales = 2;
    // }

    $("#prTable tbody").empty();
    var advice_pitems_invalid_cost = false;
    $.each(items, function () {
        var row_no = this.id;
        var newTr = $('<tr id="row_' + row_no + '" class="item_' + this.id + '" data-item-id="' + row_no + '"></tr>');
        var units = this.units;
        var unit_selected = units[this.unit];
        var total_cost = formatDecimal(this.qty * (unit_selected.operation_value > 0 ? unit_selected.operation_value : 1)) * formatDecimal(this.price);
        total_cost_total += total_cost;
        advice_pitems_invalid_cost = this.price <= 0 ? true : false;

        tr_html = '<td>'+
                        '<input name="combo_item_id[]" type="hidden" value="' + this.id + '">'+
                        '<input name="combo_item_name[]" type="hidden" value="' + this.name + '">'+
                        '<input name="combo_item_code[]" type="hidden" value="' + this.code + '">'+
                        '<span id="name_' + row_no + '">' + this.code + ' - ' + this.name + '</span>'+
                    '</td>';
        tr_html += '<td class="text-center">'+
                        '<input class="form-control text-center rprice" name="combo_item_price[]" type="text" value="' + formatDecimal(this.price) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="combo_item_price_' + row_no + '" onClick="this.select();" style="display:none;">'+
                        '<span>' + formatDecimal(this.price) + '</span>'+
                    '</td>';

        tr_html += '<td>'+
                        '<select name="combo_item_unit[]" class="form-control combo_item_unit" data-item="' + this.id + '">';
            $.each(units, function(){
                unit_option = this;
                tr_html += '<option value="'+unit_option.id+'" data-operator="'+unit_option.operator+'" data-operatorvalue="'+unit_option.operation_value+'" '+(unit_selected.id == unit_option.id ? 'selected="selected"' : "")+' >'+unit_option.code+'</option>';
            });
                tr_html += '</select>'+
                    '</td>';

        tr_html += '<td>'+
                        '<input class="form-control text-center rquantity" name="combo_item_quantity[]" type="text" value="' + formatDecimal(this.qty, decimales) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="quantity_' + row_no + '" onClick="this.select();">'+
                    '</td>';
        tr_html += '<td class="text-center">'+
                        '<input class="form-control text-center" name="combo_item_total_price[]" type="text" value="' + formatQuantity2(total_cost) + '" data-id="' + row_no + '" data-item="' + this.id + '" id="total_price_' + row_no + '" onClick="this.select();" style="display:none;">'+
                        '<span>' + formatQuantity2(total_cost) + '</span>'+
                    '</td>';
        tr_html += '<td class="text-center">'+
                        '<i class="fa fa-times tip del" id="' + row_no + '" title="Remove" style="cursor:pointer;"></i>'+
                    '</td>';
        newTr.html(tr_html);
        newTr.prependTo("#prTable");
        pp += formatQuantity2(parseFloat(this.price)*parseFloat(this.qty), 2);
        total_items++;
    });
    if (advice_pitems_invalid_cost) {
        header_alert('warning', 'Existen productos del combo con costo inválido o en cero');
    }
    var newTr = $('<tr id="" class="" data-item-id=""></tr>');
    tr_html = '<th> </th>'+
                '<th> </th>'+
                '<th> </th>'+
                '<th> </th>'+
                '<th class="text-right"> '+ formatQuantity2(total_cost_total) +' </th>'+
                '<th> </th>';
    newTr.html(tr_html);
    newTr.appendTo("#prTable");
    $('#cost').val(total_cost_total);
}

$(document).on('change', '#aiu_product', function(){
    if ($(this).val() == 1) {
        $('#type').select2('val', 'service').select2('readonly', true).trigger('change');
        $('#tax_rate').select2('val', 1).select2('readonly', true).trigger('change');
    } else {
        $('#type').select2('val', 'standard').select2('readonly', false).trigger('change');
        $('#tax_rate').select2('readonly', false).trigger('change');
    }
});
$(document).on('change', '#wappsi_invoicing', function(){
    if ($(this).val() == 1) {
        $('.div_wappsi_invoicing').fadeIn();
    } else {
        $('.div_wappsi_invoicing').fadeOut();
    }
});

$(document).on('change', '#sale_tax_rate_2_id', function(){
    valuetype = $('#sale_tax_rate_2_id option:selected').data('valuetype');

    if ($(this).val()) {
        $('.sale_second_tax_div').fadeIn();
        $('#consumption_sale_tax').prop('required', true);
    } else {
        $('.sale_second_tax_div').fadeOut();
        $('#consumption_sale_tax').prop('required', false);

        $('#consumption_sale_tax').val(0);
        $('#sale_tax_rate_2_milliliters').val(0);
        $('#sale_tax_rate_2_nominal').select2('val', '');
        $('#sale_tax_rate_2_degrees').val(0);
    }

    if (valuetype == 2) {
        if ($(this).val() == 5) {
            $('.sale_second_tax_milliliters').fadeOut()
            $('.sale_second_tax_degrees').fadeOut()
            $('.sale_second_tax_nominal').fadeOut()
            $('.sale_second_tax_div_3').fadeOut();

            $('.sale_second_tax_div_2').removeClass('col-sm-6').addClass('col-sm-12');
        } else {
            $('.sale_second_tax_milliliters').fadeOut();
            $('.sale_second_tax_degrees').fadeOut();
            $('.sale_second_tax_nominal').fadeOut();
            $('.sale_second_tax_div_3').fadeIn();

            $('.sale_second_tax_div_2').removeClass('col-sm-12').addClass('col-sm-6');
            $('.sale_second_tax_div_3').removeClass('col-sm-4').addClass('col-sm-6');
        }
    } else {
        $('.sale_second_tax_div_2').removeClass('col-sm-6').addClass('col-sm-12');
        $('.sale_second_tax_div_3').removeClass('col-sm-6').addClass('col-sm-4');

        $('.sale_second_tax_div_3').fadeOut();

        $('#sale_tax_rate_2_milliliters_label').text('<?= lang('milliliters') ?>');

        if ($(this).val() == 4) {
            $('.sale_second_tax_milliliters').fadeIn()
            $('.sale_second_tax_degrees').fadeOut()
            $('.sale_second_tax_nominal').fadeIn()

            $('.sale_second_tax_milliliters').removeClass('col-sm-4').addClass('col-sm-6');
            $('.sale_second_tax_nominal').removeClass('col-sm-4').addClass('col-sm-6');

            $('#sale_tax_rate_2_milliliters_label').text('<?= lang('grams') ?>');
        } else if ($(this).val() == 1) {
            $('.sale_second_tax_milliliters').removeClass('col-sm-6').addClass('col-sm-4');
            $('.sale_second_tax_nominal').removeClass('col-sm-6').addClass('col-sm-4');

            $('.sale_second_tax_milliliters').fadeIn()
            $('.sale_second_tax_degrees').fadeIn()
            $('.sale_second_tax_nominal').fadeIn()
        } else if ($(this).val() == 2) {
            $('.sale_second_tax_milliliters').fadeIn()
            $('.sale_second_tax_degrees').fadeOut()
            $('.sale_second_tax_nominal').fadeIn()

            $('.sale_second_tax_milliliters').removeClass('col-sm-4').addClass('col-sm-6');
            $('.sale_second_tax_nominal').removeClass('col-sm-4').addClass('col-sm-6');
        } else if ($(this).val() == 5) {
            $('.sale_second_tax_milliliters').fadeOut()
            $('.sale_second_tax_degrees').fadeOut()
            $('.sale_second_tax_nominal').fadeOut()
            $('.sale_second_tax_div_3').fadeOut();

            $('.sale_second_tax_div_2').removeClass('col-sm-6').addClass('col-sm-12');
        } else {
            $('.sale_second_tax_degrees').fadeOut()
        }
    }

    mandatorySecondTaxFields($(this).val())
    getSecondaryTaxValues($(this).val());
});

$(document).on('change', '#purchase_tax_rate_2_id', function(){
    valuetype = $('#purchase_tax_rate_2_id option:selected').data('valuetype');
    // $('#sale_tax_rate_2_id').val($(this).val()).trigger('change');
    if ($(this).val()) {
        $('.purchase_second_tax_div').fadeIn();
        $('#consumption_purchase_tax').prop('required', true);
    } else {
        $('.purchase_second_tax_div').fadeOut();
        $('#consumption_purchase_tax').prop('required', false);
    }
    if (valuetype == 2) {
        if ($(this).val() == 5) {
            $('.purchase_second_tax_div_3').fadeOut();
            $('.purchase_second_tax_div_2').removeClass('col-sm-6').addClass('col-sm-12');
            $('#purchase_tax_rate_2_percentage').prop('required', false);
        } else {
            $('.purchase_second_tax_div_2').removeClass('col-sm-12').addClass('col-sm-6');
            $('.purchase_second_tax_div_3').fadeIn();
            $('#consumption_purchase_tax').prop('readonly', true);
            $('#purchase_tax_rate_2_percentage').prop('required', true);
        }
    } else {
        $('#purchase_tax_rate_2_percentage').val('');
        $('.purchase_second_tax_div_2').removeClass('col-sm-6').addClass('col-sm-12');
        $('.purchase_second_tax_div_3').fadeOut();
        $('#consumption_purchase_tax').prop('readonly', false);
        $('#purchase_tax_rate_2_percentage').prop('required', false);
    }
});

function getSecondaryTaxValues(idTaxSecundary)
{
    if (typeof idTaxSecundary !== 'undefined') {
        $.ajax({
            type: "post",
            url: site.base_url + 'products/getSecondaryTaxValuesAjax',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'idTaxSecundary' : idTaxSecundary
            },
            dataType: "json",
            success: function (response) {
                if (response) {
                    if (['1', '2','4'].includes(idTaxSecundary)) {
                        let productNominal = '<?= (!empty($product->sale_tax_rate_2_nominal)) ? $product->sale_tax_rate_2_nominal : 0 ?>'
                        productNominal = (idTaxSecundary == 4) ? productNominal : parseInt(productNominal)
                        productNominal = (productNominal != 0) ? productNominal : ''

                        $('#sale_tax_rate_2_nominal').html(`<option value=""><?= lang('select') ?></option>`)
                        response.forEach(element => {
                            let value = (idTaxSecundary == 4) ? (element.value * 47065).toFixed(2) : parseInt(element.value)

                            $('#sale_tax_rate_2_nominal').append(`<option value="${value}">${value}</option>`)
                        });

                        $('#sale_tax_rate_2_nominal').val(productNominal).trigger('change')
                    } else if (idTaxSecundary == 3) {
                        response.forEach(element => {
                            $('#sale_tax_rate_2_percentage').val(parseInt(element.value) + '%').trigger('change')
                        });
                    }
                }
            }
        })
    }
}

$(document).on('change', '#sale_tax_rate_2_nominal, #sale_tax_rate_2_milliliters, #sale_tax_rate_2_degrees', function() {
    let taxRate2Id = $('#sale_tax_rate_2_id').val();
    let nominal = $('#sale_tax_rate_2_nominal').val() || 0
    let milliliters = $('#sale_tax_rate_2_milliliters').val() || 0
    let degrees = $('#sale_tax_rate_2_degrees').val() || 0

    if (taxRate2Id == 1) {
        if (milliliters > 750) {
            tax = parseInt((milliliters * nominal) / 750) * degrees
        } else {
            tax = nominal * degrees
        }
    } else {
        nominal = (taxRate2Id == 2) ? (nominal / 100) : nominal
        tax = nominal * milliliters
    }

    if (nominal && milliliters) {
        $('#consumption_sale_tax').val((tax).toFixed(2)).trigger('change')
    } else {
        $('#consumption_sale_tax').val((tax).toFixed(2)).trigger('change')
    }
})
function select_variant(id, v) {
    $('#' + id).val(v).select2({
        minimumInputLength: 1,
        data: [],
        initSelection: function (element, callback) {
            $.ajax({
                type: "get", async: false,
                url: "<?= admin_url('products/getOption') ?>/" + $(element).val(),
                dataType: "json",
                success: function (data) {
                    callback(data[0]);
                }
            });
        },
        ajax: {
            url: site.base_url + "products/options_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function blockSecondTax()
{
    let movementsExistingProducts = '<?= $movementsExistingProducts ?>'
    if (movementsExistingProducts == true) {
        $('#purchase_tax_rate_2_id').select2('readonly', true);

        $('#purchase_tax_rate_2_percentage').prop('readonly', true);
        $('#sale_tax_rate_2_milliliters').prop('readonly', true);
        $('#sale_tax_rate_2_percentage').prop('readonly', true);
        $('#consumption_purchase_tax').prop('readonly', true);
        $('#sale_tax_rate_2_degrees').prop('readonly', true);
        $('#sale_tax_rate_2_nominal').prop('readonly', true);
    }
}

function mandatorySecondTaxFields(secondTaxId)
{
    $('#consumption_sale_tax').prop('readonly', true);

    if (secondTaxId == 1) {
        $('#sale_tax_rate_2_milliliters').prop('required', true);
        $('#sale_tax_rate_2_degrees').prop('required', true);

        $('#sale_tax_rate_2_nominal').prop('required', true);

        $('#consumption_sale_tax').prop('required', true);
    } else if (secondTaxId == 2) {
        $('#sale_tax_rate_2_milliliters').prop('required', true);
        $('#sale_tax_rate_2_nominal').prop('required', true);
        $('#sale_tax_rate_2_degrees').prop('required', false);

        $('#consumption_sale_tax').prop('required', true);
    } else if (secondTaxId == 3) {
        $('#sale_tax_rate_2_nominal').prop('required', false);
        $('#sale_tax_rate_2_milliliters').prop('required', false);
        $('#sale_tax_rate_2_percentage').prop('required', true);
        $('#sale_tax_rate_2_degrees').prop('required', false);

        $('#consumption_sale_tax').prop('required', true);
    } else  if (secondTaxId == 4) {
        $('#sale_tax_rate_2_milliliters').prop('required', true);
        $('#sale_tax_rate_2_nominal').prop('required', true);

        $('#consumption_sale_tax').prop('required', true);
    } else if (secondTaxId == 5) {
        $('#consumption_sale_tax').prop('readonly', false);

        $('#sale_tax_rate_2_milliliters').prop('required', false);
        $('#sale_tax_rate_2_degrees').prop('required', false);
        $('#sale_tax_rate_2_nominal').prop('required', false);
    } else {

        $('#consumption_sale_tax').prop('readonly', false);

        $('#sale_tax_rate_2_milliliters').prop('required', false);
        $('#sale_tax_rate_2_degrees').prop('required', false);
        $('#sale_tax_rate_2_nominal').prop('required', false);
    }
}

$(document).on('change', '#based_on_gram_value', function(){
    if ($(this).val() == '0') {
        $('.jewel_weight').css('display', 'none');
        $('#jewel_weight').attr('required', false);
    }else{
        $('.jewel_weight').css('display', 'block');
        $('#jewel_weight').attr('required', true);
    }
    $('#jewel_weight').trigger('change');
});

$(document).on('change', '#jewel_weight', function(){ // <- cambio de peso de joya
    const type = $('#based_on_gram_value').val(); // <- tipo de oro
    if (type == 1) { // <- oro nacional 
        const priceBased = $(this).val() * site.pos_settings.current_gold_price; // <- calcular precio base
        const priceMinimun =  $(this).val() * site.pos_settings.minimun_gold_price; // <- calcular precio base
        if (typeof priceBased === 'number' && !isNaN(priceBased)) { // <- validar que el valor de la operacion sea numerico
            $('#price').val(priceBased.toFixed(4));
            $('input[data-pgbase="1"][name="pg[]"]').val(priceBased.toFixed(4)); // <- actualizar el valor de la lista base
        }
        if (typeof priceMinimun === 'number' && !isNaN(priceMinimun)) { // <- validar que el valor de la operacion sea numerico
            $('input[name="pg[]"][data-pgbase!="1"]').each(function() {
                $(this).val(priceMinimun.toFixed(4));
            });
        }    
    }else if(type == 2) {
        const priceBased = $(this).val() * site.pos_settings.current_italian_gold_price; // <- calcular precio base
        const priceMinimun =  $(this).val() * site.pos_settings.minimun_italian_gold_price; // <- calcular precio base
        if (typeof priceBased === 'number' && !isNaN(priceBased)) { // <- validar que el valor de la operacion sea numerico
            $('#price').val(priceBased.toFixed(4));
            $('input[data-pgbase="1"][name="pg[]"]').val(priceBased.toFixed(4)); // <- actualizar el valor de la lista base
        }
        if (typeof priceMinimun === 'number' && !isNaN(priceMinimun)) { // <- validar que el valor de la operacion sea numerico
            $('input[name="pg[]"][data-pgbase!="1"]').each(function() {
                $(this).val(priceMinimun.toFixed(4));
            });
        }    
    }
})

</script>