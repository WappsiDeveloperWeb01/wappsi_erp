<?php
    $total = 0;
    $total_cartera = 0;
?>
<html>
    <head>
        <meta charset="utf-8">
        <title>Recibo de caja <?= $encabezado->reference_no ?></title>
        <base href="<?=base_url()?>"/>
        <meta http-equiv="cache-control" content="max-age=0"/>
        <meta http-equiv="cache-control" content="no-cache"/>
        <meta http-equiv="expires" content="0"/>
        <meta http-equiv="pragma" content="no-cache"/>
        <link rel="shortcut icon" href="<?=$assets?>images/icon.png"/>
        <link rel="stylesheet" href="<?=$assets?>styles/theme.css" type="text/css"/>
        <style type="text/css" media="all">
            body { color: #000; }
            #wrapper { max-width: 480px; margin: 0 auto; padding-top: 20px; }
            .btn { border-radius: 0; margin-bottom: 5px; }
            .bootbox .modal-footer { border-top: 0; text-align: center; }
            h3 { margin: 5px 0; }
            .order_barcodes img { float: none !important; margin-top: 5px; }
            .text-center { text-align: center ; }
            table { width: 100% ; }
            @media print {
                .no-print { display: none; }
                #wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
                .no-border { border: none !important; }
                .border-bottom { border-bottom: 1px solid #ddd !important; }
                table tfoot { display: table-row-group; }
            }
            .rectangular_logo{
                max-width: 150px;
                max-height: 150px;
            }
            table > tbody > tr > th ,
            table > tbody > tr > td ,
            table > tbody > tr > td > h4,
            table > tbody > tr > td > h4 > span
            {
                font-size: 14px !important;
            }
            table > tbody > tr > th ,
            table > tbody > tr > td
             {
                padding-left:3.5px !important;
            }
        </style>
    </head>

    <body id="closeRegisterPrint">
        <div id="wrapper">
            <div id="receiptData" style="text-align:center;">
                <?= !empty($biller->logo) ? '<img class="rectangular_logo" src="'.base_url('assets/uploads/logos/'.$biller->logo).'" alt="">' : ''; ?>
                
                <h4 style="text-transform:uppercase;"><strong><?= $biller->name ?></strong> </h4>
                <?php
                    echo "NIT : ".$biller->vat_no.($biller->digito_verificacion != "" ? '-'.$biller->digito_verificacion : '');
                    echo "<p>" . $biller->address .
                    // "<br>" . lang("tel") . ": " . $biller->phone .
                    "<br>" . $biller->city . ",  " . $biller->state; 
                    echo "<br>";
                    echo '</p>';
                    ?>
                    <!-- <p style="text-align:center;"><b><?= $tipo_regimen ?></b></p> -->
            </div>

            <div>
                <div class="col-sm-12 text-center" >
                    <h4 style="font-weight:bold; text-transform:uppercase;"><?= $document_type->nombre ?></h4>
                </div>
                <?php
                    echo "<p><strong>" .lang("date"). ": </strong>  &emsp; &nbsp; &nbsp;" .$this->sma->hrld($encabezado->date). "<br>";
                    echo "<strong>" .lang("sale_no_ref"). ": </strong>  &emsp; " .$encabezado->reference_no. "<br>";
                    echo "<strong>" .lang("customer"). ": </strong>  &nbsp; &emsp;" .$customer->name. "<br>";
                    echo "<strong>" .lang("vat_no"). ": </strong> &nbsp; &emsp; " . $customer->vat_no . "<br>";
                    echo "<strong>" .lang("phone"). ": </strong> &emsp;" . $customer->phone . "<br>";
                    echo "<strong>" .lang("paid"). ": </strong> &nbsp;  &emsp;" . $encabezado->paid_by . "<br>";

                ?>
            </div>    
        <div class="">
            <table width="100%" class="stable tableView">
                <tr>
                    <th style="width: 50%;">Afecta a</th>
                    <th style="width: 10%; text-align: center;">Valor</th>
                    <th style="width: 13.3%; text-align: center;">Saldo</th>
                </tr>

                <?php foreach($pmnts as $pm): 
                    $user_cr = $pm->user_cr_name;
                    $saldo = $pm->invoice_affected_balance;
                    if ($pm->sale_id == NULL) {
                        $saldo = 0;
                    }
                    if ($pm->reference_no != NULL) {
                        $total += $pm->amount;
                        $total_cartera += $pm->amount;
                        $str_ref = $pm->reference_no;
                    }else{
                        $total += $pm->amount;
                        $str_ref = 'Otros conceptos';
                    }
                ?>
                    <tr>
                        <td style="border-bottom: 1px solid #DDD;"><h4><?= $str_ref ?></h4></td>
                        <td style="text-align:right; border-bottom: 1px solid #DDD;" ><?= $this->sma->formatMoney($pm->amount) ?></span></td>
                        <td style="text-align:right; border-bottom: 1px solid #DDD;" ><h4><span><?= $this->sma->formatMoney($pm->invoice_affected_balance) ?></span></h4></td>
                    </tr>
                <?php endforeach; ?>
                
                <tr>
                    <th style="border-bottom: 1px solid #DDD;"> <?= lang('total_received') ?>  </th>
                    <th style="text-align:right; border-bottom: 1px solid #DDD;"> <?= $this->sma->formatMoney($total_cartera) ?> </th>
                    <th style="text-align:right; border-bottom: 1px solid #DDD;"></th>
                </tr>
                <tr>
                    <th style="border-bottom: 1px solid #DDD;"> <?= lang('total') ?> </th>
                    <th style="text-align:right; border-bottom: 1px solid #DDD;" ><?= $this->sma->formatMoney($total) ?></th>
                    <th style="text-align:right; border-bottom: 1px solid #DDD;"></th>
                </tr>
            </table>
            <br>
            <?php
            echo "<strong>" .lang('note'). " : </strong>" .$encabezado->note . " <br><br> ";
            ?>
            <p style="text-align: center;"><?= lang('elaborated') . " : " .$user_cr  ?> </p>

        </div>

        <?php if (!isset($cron_job)): ?>
            <div class="modal-footer no-print">
                <div>
                <button  type="button" class="btn btn-block btn-primary " onclick="imprimir_factura_pos();">
                    <i class="fa fa-print"></i> <?= lang('print'); ?>
                </button>
                </div>
                <button style="with:100%" type="button" class="btn btn-block btn btn-warning" onclick="window.location.href = '<?= admin_url().'payments/index' ?>';">
                    <i class="fa fa-arrow-circle-left"></i> <?= lang('go_back'); ?>
                </button>
 
            </div>
        <?php endif ?>
        <script type="text/javascript" src="<?= $assets ?>js/jquery-2.0.3.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="<?= $assets ?>js/custom.js"></script>
        </body>
    </html>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        // close_register
        $("input[name='close_register']").click(function( event ){
            event.preventDefault();
            window.onafterprint = function(e){
                $(window).off('mousemove', window.onafterprint);
                console.log('Print Dialog Closed..');
                $('.bv-form').submit();
            };
            window.print();
            setTimeout(function(){
                $(window).one('mousemove', window.onafterprint);
            }, 1);
        });

        $(document).on('click', '.po', function (e) {
            e.preventDefault();
            $('.po').popover({
                html: true,
                placement: 'left',
                trigger: 'manual'
            }).popover('show').not(this).popover('hide');
            return false;
        });
        $(document).on('click', '.po-close', function () {
            $('.po').popover('hide');
            return false;
        });
        $(document).on('click', '.po-delete', function (e) {
            var row = $(this).closest('tr');
            e.preventDefault();
            $('.po').popover('hide');
            var link = $(this).attr('href');
            $.ajax({
                type: "get", url: link,
                success: function (data) {
                    row.remove();
                    addAlert(data, 'success');
                },
                error: function (data) {
                    addAlert('Failed', 'danger');
                }
            });
            return false;
        });
        setTimeout(function() {
            imprimir_factura_pos();
        }, 1200);
    });

    function imprimir_factura_pos()
    {
      $('.without_close').css('display', '');
      var divToPrint=document.getElementById('closeRegisterPrint');
      var newWin=window.open('','Print-Window');
      var header = $('head').html();
      newWin.document.open();
      newWin.document.write('<head>'+header+'</head><body onload="window.print()" style="height:auto !important; min-height:50% !important;">'+divToPrint.innerHTML+'</body>');
      newWin.document.close();
      setTimeout(function(){
            newWin.close();
        },1000);
    }
</script>
<?php 
    unset($def_color);
 ?>
