<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .ob {
        list-style: none;
        padding: 0;
        margin: 0;
        margin-top: 10px;
    }
    .ob li {
        width: 49%;
        margin: 0 10px 10px 0;
        float: left;
    }
    @media only screen and (max-width: 799px) {
        .ob li {
            width: 100%;
        }
    }
    .ob li .btn {
        width: 100%;
    }
    .ob li:nth-child(2n+2) {
        margin-right: 0;
    }
    .li_preferences_selection, .product_preference_select 
    {
        width: 100% !important;
    }

.product_preference_select {
    border-style: unset;
    background-color: #F2F4F4;
    padding: 1%;
    text-align: left;
}

.product_preference_select > .iradio_square-blue {
    float: right !important;
}

</style>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header modal-primary">
            <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button> -->
            <span class="modal-title" id="susModalLabel" style="font-size: 150%;"><?= "(".$pdata->code.") ".$pdata->name ?></span>
            <h3><?=lang('product_preference_selection');?></h3>
        </div>
        <div class="modal-body" style="padding-bottom:0; max-height: 600px !important; overflow: auto;">
            <div class="form-group" id="ui3">
                <div class="input-group">
                    <?php
                        if(!isset($bill_reference)){
                            $bill_reference = "";
                        }
                    ?>
                    <?php echo form_input('search_reference', $bill_reference, 'class="form-control pos-tip btn-pos-product" id="search_reference" data-placement="top" data-trigger="focus" placeholder="'.lang('key_word').'" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
                    <input type="hidden" name="pv_id" id="pv_id">
                    <div class="input-group-addon" style="padding: 2px 8px;border:none !important;">
                        <a id="pv_search" data-placement="bottom" data-html="true" data-toggle="ajax" tabindex="-1"> <i class="fa fa-search" id="xxd" style="font-size: 1.5em;"></i> </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div class="html_con"><?= $html ?></div>
            <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
            <em class="loading_text" style="display:none;">Cargando...</em>
            <button class="btn btn-primary set_product_preference"><?= lang('submit') ?></button>
            <button class="btn btn-danger unset_product_preference"><?= lang('cancel') ?></button>
        </div>
    </div>
</div>
<script>
    
pref_arritems = JSON.parse(localStorage.getItem('<?= $arritems_name ?>'));
pref_arritems_id = '<?= $arritems_id ?>';
pref_arritems_name = '<?= $arritems_name ?>';

    $('#myModal').ready(function () {
        $('#pv_search').on('click', function (e) {
            var reference = $('#search_reference').val();
            var destino = "<?= admin_url('pos/product_preferences_selection/'.$product_id.'/'.$arritems_id.'/null') ?>/"+reference;
            $("#pv_search").attr("href", destino);
        });
        setTimeout(function() {
            // $('#search_reference').focus();
            $('.radio_pref').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%'
            });
        }, 850);
        setTimeout(function() {
            set_preferences_checked();
        }, 950);
    });
    

    function validate_min_selected(){
        var disable = false;
        $.each($('.head_cat'), function(index, li){
            if ($(li).data('pcat_req') > 0) {
                cat_length = $('li[class="li_preferences_selection body_cat"][data-pcat="'+$(li).data('pcat')+'"]').find('.btn-outline').length;
                if (cat_length == 0) {
                    $('em[class="text-danger cat_error"][data-pcat="'+$(li).data('pcat')+'"]').fadeIn();
                    disable = true;
                } else {
                    $('em[class="text-danger cat_error"][data-pcat="'+$(li).data('pcat')+'"]').fadeOut();
                }
            }
        });
        return disable;
    }
    function set_preferences_checked(){
        arritems = JSON.parse(localStorage.getItem('<?= $arritems_name ?>'));
        console.log(arritems);
        if (arritems["<?= $arritems_id ?>"].preferences_selected) {
            $.each(arritems["<?= $arritems_id ?>"].preferences_selected, function(index, value){
                if (value !== null) {
                    $.each(value, function(index2, value2){
                        $('.product_preference_select[data-pfid="'+value2+'"]').trigger('click');
                    })
                }
            });
        }
    }
    function validate_close_modal(){
        if ($('.head_cat[data-pcat_req="1"]').length > 0) {
            if (validate_min_selected()) {
                var boxd = bootbox.dialog({
                    title: "<i class='fa fa-times'></i> Eliminar producto",
                    message: 'El producto requiere de selección de preferencias, ¿Desea eliminarlo o continuar con la selección? ',
                    buttons: {
                        success: {
                            label: "<i class='fa fa-tick'></i> Continuar",
                            className: "btn-success",
                            callback: function () {
                                setTimeout(function() {
                                    $('#myModal').modal('show');
                                }, 850);
                            }
                        },
                        cancel: {
                            label: "<i class='fa fa-trash'></i> Eliminar",
                            className: 'btn-danger',
                            callback: function () {
                                arritems = JSON.parse(localStorage.getItem('<?= $arritems_name ?>'));
                                delete arritems["<?= $arritems_id ?>"];
                                localStorage.setItem('<?= $arritems_name ?>', JSON.stringify(arritems));
                                loadItems();
                                $('#myModal').modal('hide');
                                $('#myModal').empty();
                            }
                        }
                    }
                });
            }
        } else {
            $('#myModal').modal('hide');
            $('#myModal').empty();
        }
    }

    
$(document).on('keydown', function(e){
    if (e.key == 'Escape') {
        validate_close_modal();
    }
});
</script>

<?= $modal_js ?>