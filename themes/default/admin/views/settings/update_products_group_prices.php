<?php defined('BASEPATH') or exit('No direct script access allowed'); ?>

<?php $pages = ceil($cnt_price_groups / 10); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <?php if (!empty($billers) && ($this->Owner || $this->Admin)) { ?>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label><?= lang('biller') ?></label>
                                    <select class="form-control" id="biller_id">
                                        <option value=""><?= lang('select') ?></option>
                                        <?php foreach ($billers as $biller) : ?>
                                            <option value="<?= $biller->id ?>"><?= $biller->name ?></option>
                                        <?php endforeach ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row">
                        <?php if ($this->Settings->ipoconsumo) : ?>
                            <div class="col-md-12">
                                <em><?= lang('tax_included_impoconsumo_details') ?></em>
                            </div>
                        <?php endif ?>
                        <div class="col-md-3">
                            <label>Páginas : </label>
                            <?php if ($pages > 1) : ?>
                                <select id="page_view">
                                    <?php for ($i = 0; $i < $pages; $i++) { ?>
                                        <option value="<?= $i ?>" <?= $i == $page ? 'selected="selected"' : '' ?>>Página <?= $i + 1 ?></option>
                                    <?php } ?>
                                </select>
                            <?php endif ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <?php
                            $cnt = count($price_groups);
                            $wdth = 75 / $cnt;
                            ?>
                            <div class="table-responsive">
                                <?= admin_form_open_multipart('system_settings/update_products_group_prices'); ?>
                                <table id="SLData" class="table table-hover">
                                    <thead>
                                        <tr>
                                            <th><?= lang("code"); ?></th>
                                            <th><?= lang("product_name"); ?></th>
                                            <?php foreach ($price_groups as $pg) : ?>
                                                <th style="width: <?= $wdth ?>%"><?= $pg->name ?></th>
                                            <?php endforeach ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td colspan="4" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    if ($(window).width() < 1000) {
        var nums = [
            [10, 25],
            [10, 25]
        ];
    } else {
        var nums = [
            [10, 25, 50, 100, 500, -1],
            [10, 25, 50, 100, 500, "<?= lang('all') ?>"]
        ];
    }
    $(document).ready(function() {
        get_table_data();
    });
    $(document).on('change', '#biller_id', function() {
        get_table_data();
    });
    var ajax = 0;
    var table;

    function get_table_data() {
        if (ajax == 1) {
            table.destroy();
        } else {
            ajax = 1;
        }
        $.ajax({
            url: "<?= admin_url('system_settings/getProductsPricesGroups/') . $page ?>",
            data: {
                biller_id: $('#biller_id').val(),
            }
        }).done(
            function(data) {
                $('#SLData tbody').html(data);
                table = $('#SLData').DataTable({
                    processing: false,
                    pageLength: 100,
                    responsive: true,
                    dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
                    oLanguage: <?php echo $dt_lang; ?>
                });
            }
        );
    }

    $(document).on('blur', '.pamount', function(e) {
        p_id = $(this).data('productid');
        pg_id = $(this).data('pricegroupid');
        price = $(this).val();
        if ((site.settings.allow_zero_price_products == 1 && price >= 0) || (site.settings.allow_zero_price_products == 0 && price > 0)) {
            update_price(p_id, pg_id, price);
        } else {
            header_alert('error', 'Porfavor revise el precio ingresado, negativo o en 0 sin permisos de registrar así');
        }

    });

    $(document).on('keypress', '.pamount', function(e) {
        if (e.keyCode == 13) {
            p_id = $(this).data('productid');
            pg_id = $(this).data('pricegroupid');
            price = $(this).val();
            if ((site.settings.allow_zero_price_products == 1 && price >= 0) || (site.settings.allow_zero_price_products == 0 && price > 0)) {
                update_price(p_id, pg_id, price);
            } else {
                header_alert('error', 'Porfavor revise el precio ingresado; Está en negativo o en 0 sin permisos de registrar así');
            }
        }
    });

    function update_price(p_id, pg_id, price) {
        $('.pamount').attr('readonly', true);

        $.ajax({
            url: "<?= admin_url('system_settings/updateProductPriceGroup') ?>/" + p_id + "/" + pg_id + "/" + price
        }).done(function(data) {
            if (data == 1) {
                setTimeout(function() {
                    $.each($('.pamount'), function(index, input) {
                        if ($(input).data('blocked') == undefined) {
                            $(input).attr('readonly', false);
                        }
                    });
                }, 800);
                // command: toastr.success('Se actualizó el precio modificado', 'Precio actualizado', { onHidden : function(){  } });
            } else {
                command: toastr.error('Por favor revise que haya digitado bien el valor, si se trata de algún error contáctese con soporte.', 'No se actualizó', {
                    "showDuration": "500",
                    "hideDuration": "1000",
                    "timeOut": "4000",
                    "extendedTimeOut": "1000",
                });
            }
        });
    }

    $('#page_view').on('change', function() {
        location.href = '<?= admin_url("system_settings/update_products_group_prices/") ?>' + $(this).val();
    });
</script>