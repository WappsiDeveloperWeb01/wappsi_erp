<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-print">
<?= admin_form_open_multipart("products/update_csv_recosting_registers", "id='csv_recosting_form'") ?>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-sm-4 form-group">
                                    <?= lang('end_date', 'rc_end_date') ?>
                                    <input type="date" name="end_date" id="rc_end_date" class="form-control" required>
                                </div>
                                <div class="col-sm-4 form-group">
                                    <?= lang('warehouse', 'rc_warehouse') ?>
                                    <?php
                                    $wh[""] = lang('select');
                                    foreach ($warehouses as $warehouse) {
                                        $wh[$warehouse->id] = $warehouse->name;
                                    }
                                    echo form_dropdown('warehouse', $wh, (isset($_POST['warehouse']) ? $_POST['warehouse'] : ''), 'class="form-control" id="rc_warehouse" data-placeholder="' . $this->lang->line("select") . " " . $this->lang->line("warehouse") . '"  required');
                                    ?>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <button type="button" name="boton_name" id="submit_recosting" class="btn btn-primary"> <?= lang("send"); ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12 form-group">
                            <div class="clearfix"></div>
                            <div class="well well-sm">
                                <a href="<?php echo $this->config->base_url(); ?>assets/csv/sample_recosting_registers.csv"
                                class="btn btn-primary pull-right"><i class="fa fa-download"></i> <?= lang('download_sample_file') ?></a>
                                <span class="text-warning"><?php echo $this->lang->line("csv1"); ?></span><br>
                                <?php echo $this->lang->line("csv2"); ?> <span class="text-info">(<?= lang("product_code") . ', ' . lang("avg_cost") . ', ' . lang("quantity"); ?> )</span><br>
                            </div>
                        </div>
                        <div class="col-md-6 form-group">
                            <?= lang("csv_file", "csv_file") ?>
                            <input id="csv_file" type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" required="required"
                                    data-show-upload="false" data-show-preview="false" class="form-control file">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?= form_close() ?>

<script type="text/javascript">
    $(document).on('click', '#submit_recosting', function(){
        if ($('#csv_recosting_form').valid()) {
            $('#csv_recosting_form').submit();
        }
    });
</script>