<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script>
    if (localStorage.getItem('keep_prices')) {
        localStorage.removeItem('keep_prices');
    }
    if (localStorage.getItem('pos_keep_prices')) {
        localStorage.removeItem('pos_keep_prices');
    }
    if (localStorage.getItem('pos_wholesale_keep_prices')) {
        localStorage.removeItem('pos_wholesale_keep_prices');
    }
    if (localStorage.getItem('keep_prices_quote_id')) {
        localStorage.removeItem('keep_prices_quote_id');
    }
    if (localStorage.getItem('pos_keep_prices_quote_id')) {
        localStorage.removeItem('pos_keep_prices_quote_id');
    }
    if (localStorage.getItem('pos_wholesale_keep_prices_quote_id')) {
        localStorage.removeItem('pos_wholesale_keep_prices_quote_id');
    }
    if ($(window).width() < 1000) {
        var nums = [[10, 25], [10, 25]];
    } else {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }
    $(document).ready(function () {
        oTable = $('#QUData').dataTable({
            "aaSorting": [[1, "desc"], [2, "desc"]],
            "aLengthMenu": nums,
            "iDisplayLength": $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            'bProcessing': true, 'bServerSide': true,
            'sAjaxSource': '<?= admin_url('quotes/getQuotes'. ($warehouse_id ? '/' . $warehouse_id : '')) ?>',
            'fnServerData': function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "index_type",
                    "value": "customers"
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            'fnRowCallback': function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "quote_link";
                return nRow;
            },
            "aoColumns": [
                            {"bSortable": false,"mRender": checkbox},
                            {"mRender": fld},
                            null,
                            null,
                            null,
                            null,
                            {"bVisible" : false},
                            {"mRender": currencyFormat},
                            {"mRender": row_status},
                            {"bVisible" : false},
                            {"bSortable": false,"mRender": attachment},
                            {"bSortable": false}
                        ],
            "fnFooterCallback": function (nRow, aaData, iStart, iEnd, aiDisplay) {
                var tt1 = 0;
                for (var i = 0; i < aaData.length; i++) {
                    tt1 += sumListQuantitys(aaData[aiDisplay[i]][7]);
                }
                var nCells = nRow.getElementsByTagName('th');
                nCells[6].innerHTML = currencyFormat(parseFloat(tt1));
            }
        }).fnSetFilteringDelay().dtFilter([
            {column_number: 1, filter_default_label: "[<?=lang('date');?> (yyyy-mm-dd)]", filter_type: "text", data: []},
            {column_number: 2, filter_default_label: "[<?=lang('reference_no');?>]", filter_type: "text", data: []},
            {column_number: 3, filter_default_label: "[<?=lang('destination_reference_no');?>]", filter_type: "text", data: []},
            {column_number: 4, filter_default_label: "[<?=lang('biller');?>]", filter_type: "text", data: []},
            {column_number: 5, filter_default_label: "[<?=lang('customer_name');?>]", filter_type: "text", data: []},
            {column_number: 8, filter_default_label: "[<?=lang('status');?>]", filter_type: "text", data: []},
        ], "footer");
        <?php if($this->session->userdata('remove_quls')) { ?>
        if (localStorage.getItem('quitems')) {
            localStorage.removeItem('quitems');
        }
        if (localStorage.getItem('qudiscount')) {
            localStorage.removeItem('qudiscount');
        }
        if (localStorage.getItem('qutax2')) {
            localStorage.removeItem('qutax2');
        }
        if (localStorage.getItem('qushipping')) {
            localStorage.removeItem('qushipping');
        }
        if (localStorage.getItem('quref')) {
            localStorage.removeItem('quref');
        }
        if (localStorage.getItem('quwarehouse')) {
            localStorage.removeItem('quwarehouse');
        }
        if (localStorage.getItem('qusupplier')) {
            localStorage.removeItem('qusupplier');
        }
        if (localStorage.getItem('qunote')) {
            localStorage.removeItem('qunote');
        }
        if (localStorage.getItem('qucustomer')) {
            localStorage.removeItem('qucustomer');
        }
        if (localStorage.getItem('qubiller')) {
            localStorage.removeItem('qubiller');
        }
        if (localStorage.getItem('qucurrency')) {
            localStorage.removeItem('qucurrency');
        }
        if (localStorage.getItem('qudate')) {
            localStorage.removeItem('qudate');
        }
        if (localStorage.getItem('qustatus')) {
            localStorage.removeItem('qustatus');
        }
        if (localStorage.getItem('retenciones')) {
            localStorage.removeItem('retenciones');
        }
        if (localStorage.getItem('qutype')) {
            localStorage.removeItem('qutype');
        }
        if (localStorage.getItem('othercurrency')) {
            localStorage.removeItem('othercurrency');
        }
        if (localStorage.getItem('othercurrencycode')) {
            localStorage.removeItem('othercurrencycode');
        }
        if (localStorage.getItem('othercurrencytrm')) {
            localStorage.removeItem('othercurrencytrm');
        }
        <?php $this->sma->unset_data('remove_quls'); } ?>
    });

</script>

<?php if ($this->Owner || $this->Admin || $this->GP['bulk_actions']) {
    echo admin_form_open('quotes/quote_actions', 'id="action-form"');
} ?>

<div class="wrapper wrapper-content  animated fadeInRight  no-print">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="pull-right dropdown">
                                <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> <?= lang('actions') ?> <span class="caret"></span> </button>
                                <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                                    <li>
                                        <a href="<?= admin_url('quotes/add') ?>">
                                            <i class="fa fa-plus-circle"></i> <?= lang('add_quote') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="excel" data-action="export_excel_detail">
                                            <i class="fa fa-file-excel-o"></i> <?= lang('export_all_detail_to_excel') ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" id="combine" data-action="combine">
                                            <i class="fa fa-file-pdf-o"></i> <?=lang('combine_to_pdf')?>
                                        </a>
                                    </li>
                                    <li class="divider"></li>
                                   <!--  <li>
                                        <a href="#" class="bpo" title="<b><?= $this->lang->line("delete_quotes") ?></b>"
                                            data-content="<p><?= lang('r_u_sure') ?></p><button type='button' class='btn btn-danger' id='delete' data-action='delete'><?= lang('i_m_sure') ?></a> <button class='btn bpo-close'><?= lang('no') ?></button>"
                                            data-html="true" data-placement="left"><i class="fa fa-trash-o"></i> <?= lang('delete_quotes') ?>
                                        </a>
                                    </li> -->
                                </ul>
                            </div>
                            <?php if (!empty($warehouses)) { ?>
                                <div class="pull-right dropdown">
                                    <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> Almacenes <span class="caret"></span> </button>
                                    <ul class="dropdown-menu pull-right" class="tasks-menus" role="menu" aria-labelledby="dLabel">
                                        <li><a href="<?= admin_url('quotes') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                                        <li class="divider"></li>
                                        <?php
                                        foreach ($warehouses as $warehouse) {
                                            echo '<li><a href="' . admin_url('quotes/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                        }
                                        ?>
                                    </ul>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="QUData" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("destination_reference_no"); ?></th>
                                        <th><?= lang("biller"); ?></th>
                                        <th><?= lang("customer_name"); ?></th>
                                        <th><?= lang("supplier_name"); ?></th>
                                        <th><?= lang("total"); ?></th>
                                        <th><?= lang("status"); ?></th>
                                        <th><?= lang("type"); ?></th>
                                        <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                        <th style="width:115px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="10"
                                            class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                    </tr>
                                    </tbody>
                                    <tfoot class="dtFilter">
                                    <tr class="active">
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th style="min-width:30px; width: 30px; text-align: center;"><i class="fa fa-chain"></i></th>
                                        <th style="width:115px; text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /Body -->

<?php if ($this->Owner || $this->Admin || $this->GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>