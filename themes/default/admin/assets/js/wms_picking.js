$(document).ready(function(){
    $("#picking_form").validate({
        ignore: []
    });
    $(document).on('click', '#reset_picking', function(){
        clean_picking_localstorage();
        location.reload();
    });

	$(document).on('change', '#wmspicustomer', function(){
		localStorage.setItem('wmspicustomer', $(this).val());
        if ($('#wmspios_reference').val() == "") {
            $.ajax({
                url : site.base_url+"wms/customer_order_sales",
                type : "GET",
                dataType: "JSON",
                data : {
                    'customer_id' :  $('#wmspicustomer').val(),
                    'biller_id' :  $('#wmspibiller').val(),
                }
            }).done(function(data){
                if (data.response !== undefined && data.response == 0) {
                    command: toastr.warning('El cliente seleccionado no tiene cartera pendiente', 'Sin resultados', {
                            "showDuration": "500",
                            "hideDuration": "1000",
                            "timeOut": "6000",
                            "extendedTimeOut": "1000",
                        });
                    localStorage.removeItem('wms_order_sales');
                    loadItems();
                } else {
                    localStorage.removeItem('wms_order_sales');
                    localStorage.setItem('wms_order_sales', JSON.stringify(data));
                    if (Object.keys(data).length == 1) {
                        $.each(data, function(os_id, os){
                            data[os_id].cancelled = false;
                            $.each(os.rows, function(os_item_id, os_item){
                                data[os_id].rows[os_item_id].cancelled = false;
                            });
                        });
                    }
                    loadItems();
                }
            }).fail(function(){
                loadItems();
            });
        }
	});

    $(document).on('change', '#wmspios_reference', function(){
        localStorage.setItem('wmspios_reference', $(this).val());
        $.ajax({
            url : site.base_url+"wms/order_sale_by_reference",
            type : "GET",
            dataType: "JSON",
            data : {
                'order_sale_reference' :  $('#wmspios_reference').val(),
            }
        }).done(function(data){
            if (data.response !== undefined && data.response == 0) {
                command: toastr.warning('El cliente seleccionado no tiene cartera pendiente', 'Sin resultados', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "6000",
                        "extendedTimeOut": "1000",
                    });
                localStorage.removeItem('wms_order_sales');
                loadItems();
            } else {
                localStorage.removeItem('wms_order_sales');
                if (Object.keys(data).length == 1) {
                    $.each(data, function(os_id, os){
                        data[os_id].cancelled = false;
                        $.each(os.rows, function(os_item_id, os_item){
                            data[os_id].rows[os_item_id].cancelled = false;
                        });
                    });
                }
                localStorage.setItem('wms_order_sales', JSON.stringify(data));
                loadItems();
            }
        }).fail(function(){
            loadItems();
        });
    });

    if (wmspibiller = localStorage.getItem('wmspibiller')) {
        $('#wmspibiller').select2('val', wmspibiller).trigger('change');
    }
    if (wmspios_reference = localStorage.getItem('wmspios_reference')) {
        $('#wmspios_reference').val(wmspios_reference).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"wms/getOrderSale/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "wms/order_sale_suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        biller_id: $('#wmspibiller').val(),
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        }).trigger('change');
    } else {
        nsOrderSale();
    }

    if (wmspicustomer = localStorage.getItem('wmspicustomer')) {
        nsCustomer(wmspicustomer);
    } else {
        nsCustomer();
    }

    $(document).on('click', '.inactive_row', function(){
        item_id = $(this).data('itemid');
        osid = $(this).data('osid');
        wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
        wms_order_sales[osid].rows[item_id].quantity = 0;
        wms_order_sales[osid].rows[item_id].cancelled = true;
        localStorage.setItem('wms_order_sales', JSON.stringify(wms_order_sales));
        loadItems();
    });

    $(document).on('click', '.active_row', function(){
        item_id = $(this).data('itemid');
        osid = $(this).data('osid');
        wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
        wms_order_sales[osid].rows[item_id].quantity = wms_order_sales[osid].rows[item_id].old_quantity;
        wms_order_sales[osid].rows[item_id].cancelled = false;
        localStorage.setItem('wms_order_sales', JSON.stringify(wms_order_sales));
        loadItems();
    });

    $(document).on('click', '#set_picking', function(){
        set_picking();
    });

    $(document).on('change', '.rquantity', function(){
        item_id = $(this).data('itemid');
        osid = $(this).data('osid');
        wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
        if (!is_numeric($(this).val()) || $(this).val() > (wms_order_sales[osid].rows[item_id].old_quantity - wms_order_sales[osid].rows[item_id].quantity_delivered)) {
            $(this).val((wms_order_sales[osid].rows[item_id].old_quantity - wms_order_sales[osid].rows[item_id].quantity_delivered));
            bootbox.alert(lang.unexpected_value);
            return;
        }
        wms_order_sales[osid].rows[item_id].quantity = $(this).val();
        localStorage.setItem('wms_order_sales', JSON.stringify(wms_order_sales));
        loadItems();
    });

    $(document).on('click', '.picking_pending', function(){
        tr = $(this).closest('tr');
        console.log(tr);
        item_id = $(tr).data('itemid');
        osid = $(tr).data('osid');
        wh_order = $(tr).data('wh_order');
        wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
        wms_order_sales[osid].rows[item_id].warehouses_products[wh_order].picked = true;
        localStorage.setItem('wms_order_sales', JSON.stringify(wms_order_sales));
        set_picking();
    });

    $(document).on('click', '.picking_completed', function(){
        tr = $(this).closest('tr');
        item_id = $(tr).data('itemid');
        osid = $(tr).data('osid');
        wh_order = $(tr).data('wh_order');
        wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
        wms_order_sales[osid].rows[item_id].warehouses_products[wh_order].picked = false;
        localStorage.setItem('wms_order_sales', JSON.stringify(wms_order_sales));
        set_picking();
    });

    $(document).on('change', '.pquantity', function(){
        tr = $(this).closest('tr');
        item_id = $(tr).data('itemid');
        osid = $(tr).data('osid');
        wh_order = $(tr).data('wh_order');
        minqty = $(this).data('minqty');
        wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
        item = wms_order_sales[osid].rows[item_id].warehouses_products[wh_order];
        if (!is_numeric($(this).val()) || $(this).val() > parseFloat(minqty)) {
            $(this).val(parseFloat(minqty));
            command: toastr.warning('El valor indicado no puede ser mayor a <b>'+minqty+'</b>', '¡Atención!', {
                        "showDuration": "500",
                        "hideDuration": "1000",
                        "timeOut": "4000",
                        "extendedTimeOut": "1000",
                    });
            return;
        }
        wms_order_sales[osid].rows[item_id].warehouses_products[wh_order].manual_to_pick_qty = $(this).val();
        localStorage.setItem('wms_order_sales', JSON.stringify(wms_order_sales));
        set_picking();
    });
    localStorage.removeItem('wms_novedades');

    $(document).on('click', '#send_picking', function(){
        if ($('#wms_packing_warehouse').val()) {
            $('#modalSetPicking').modal('hide');
            setTimeout(function() {
                bootbox.confirm({
                    message: "¿Realmente desea envíar la selección?",
                    buttons: {
                        confirm: {
                            label: 'Si, enviar selección',
                            className: 'btn-success send-submit-sale btn-full'
                        },
                        cancel: {
                            label: 'No, quiero revisar',
                            className: 'btn-danger btn-full'
                        }
                    },
                    callback: function (result) {
                        if (result) {
                            if ($('#picking_form').valid()) {
                                $('#picking_form').submit();
                            }
                        } else {
                            setTimeout(function() {
                                $('#modalSetPicking').modal('show');
                            }, 1200);
                        }
                    }
                });
            }, 1200);
        } else{
            $('#wms_packing_warehouse').select2('open');
        }
    });


    $(document).on('click', '.active_order', function(){
        osid = $(this).data('osid');
        wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
        items = wms_order_sales[osid].rows;
        if (wms_order_sales[osid].cancelled == true) {
            wms_order_sales[osid].cancelled = false;
            $.each(items, function(index,item){
                items[index].cancelled = false;
            });
        } else if (wms_order_sales[osid].cancelled == false) {
            wms_order_sales[osid].cancelled = true;
            $.each(items, function(index,item){
                items[index].cancelled = true;
            });
        }
        wms_order_sales[osid].rows = items;
        localStorage.setItem('wms_order_sales', JSON.stringify(wms_order_sales));
        loadItems();
    });
});

function nsCustomer(customer = null) {
    if (customer == null) {
        $('#wmspicustomer').select2({
            minimumInputLength: 1,
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });
    } else {
        $('#wmspicustomer').val(customer).select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get", async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0]);
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        }).trigger('change');
    }
}

function nsOrderSale(){
    $('#wmspios_reference').select2({
        minimumInputLength: 1,
        width: '100%',
        ajax: {
            url: site.base_url + "wms/order_sale_suggestions",
            dataType: 'json',
            quietMillis: 15,
            data: function (term, page) {
                return {
                    term: term,
                    limit: 10,
                };
            },
            results: function (data, page) {
                if (data.results != null) {
                    return {results: data.results};
                } else {
                    return {results: [{id: '', text: lang.no_match_found}]};
                }
            }
        }
    });
}

function clean_picking_localstorage(){
    localStorage.removeItem('wmspicustomer');
    localStorage.removeItem('wmspibiller');
    localStorage.removeItem('wmspios_reference');
    localStorage.removeItem('wms_order_sales');
}

function loadItems(){
    $("#wmspi_table tbody").empty();
    if (wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'))) {
        $.each(wms_order_sales, function(os_id, os){
            if ($('#wmspicustomer').val() == '' || ($('#wmspicustomer').val() != '' && $('#wmspicustomer').val() != os.customer)) {
                nsCustomer(os.customer);
            }
            var actual_order_ref = null;
            $.each(os.rows, function(os_item_index, os_item){
                tr_html = "";
                tr_html2 = "";
                var newTr2 = null;
                var newTr = $('<tr class="row_'+os_item.id+' order_item_'+os_item.sale_id+'" data-osid="'+os_item.sale_id+'" '+(os.cancelled == true ? 'style="display:none;"' : '')+'></tr>');
                if (actual_order_ref == null || actual_order_ref != os_item.reference_no) {
                    var newTr2 = $('<tr class="active_order" data-osid="'+os_item.sale_id+'"></tr>');
                    tr_html2 = '<td colspan="4">'+
                            '<span>'+(os.cancelled == true ? '<i class="fa fa-chevron-right"></i>' : '<i class="fa fa-chevron-down"></i>')+" "+os_item.reference_no+'</span>'+
                          '</td>';
                    actual_order_ref = os_item.reference_no;
                }
                tr_html += '<td>'+
                            '<span>'+os_item.reference_no+'</span>'+
                          '</td>';
                tr_html += '<td>'+
                            '<span>'+os_item.product_code+(os_item.variant_suffix !== null && os_item.variant_suffix !== undefined ? os_item.variant_suffix : '')+'</span>'+
                          '</td>';
                tr_html += '<td>'+
                            '<input type="text" name="quantity[]" class="form-control rquantity" data-itemid="'+os_item_index+'" data-osid="'+os_id+'" value="'+(os_item.cancelled === undefined || os_item.cancelled == false ? formatDecimal(os_item.quantity - os_item.quantity_delivered) : 0)+'" '+(os_item.cancelled === undefined || os_item.cancelled == false ? '' : 'readonly')+'>'+
                          '</td>';
                tr_html += '<td class="text-center">'+
                            (os_item.cancelled === undefined || os_item.cancelled == false
                            ?
                            '<i class="fa fa-times inactive_row text-danger" data-itemid="'+os_item_index+'" data-osid="'+os_id+'" style="cursor:pointer;"></i>'
                            :
                            '<i class="fa fa-check active_row text-success" data-itemid="'+os_item_index+'" data-osid="'+os_id+'" style="cursor:pointer;"></i>'
                            )+
                          '</td>';
                if (newTr2) {
                    newTr2.html(tr_html2);
                    newTr2.appendTo("#wmspi_table");
                }
                newTr.html(tr_html);
                newTr.appendTo("#wmspi_table");
            });
        });
    }
}

function set_picking(){
    $("#tabla_set_picking tbody").empty();
    if (wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'))) {
        $('#modalSetPicking').modal({
            backdrop: 'static',
            keyboard: false});
        $('#modalSetPicking').modal('show');
        var wh_final_picking = [];
        $.each(wms_order_sales, function(os_id, os){
            $.each(os.rows, function(os_item_index, os_item){
                if (os_item.cancelled === undefined || os_item.cancelled == false) {
                    if (os_item.warehouses_products !== undefined) {
                        delete to_pick_item;
                        to_pick_item = {};
                        os_item_to_pick_qty = parseFloat(os_item.quantity - os_item.quantity_delivered);
                        $.each(os_item.warehouses_products, function(wh_pv_id, wh_pv){
                            var to_pick_qty = 0;
                            if (parseFloat(wh_pv.quantity) > parseFloat(os_item_to_pick_qty)) {
                                to_pick_qty = os_item_to_pick_qty;
                                os_item_to_pick_qty = 0;
                            } else if (parseFloat(wh_pv.quantity) <= parseFloat(os_item_to_pick_qty)) {
                                to_pick_qty = parseFloat(wh_pv.quantity);
                                os_item_to_pick_qty -= parseFloat(wh_pv.quantity);
                            }

                            to_pick_item['option_id'] = os_item.option_id;
                            to_pick_item['product_id'] = os_item.product_id;
                            to_pick_item['product_name'] = os_item.product_name;
                            to_pick_item['product_code'] = os_item.product_code;
                            to_pick_item['variant_name'] = os_item.variant_name;
                            to_pick_item['variant_suffix'] = os_item.variant_suffix;
                            to_pick_item['warehouse_id'] = wh_pv.warehouse_id;
                            to_pick_item['id'] = os_item.id;
                            to_pick_item['sale_id'] = os_item.sale_id;

                            to_pick_item['picked'] = wh_pv.picked == true ? true : false;
                            to_pick_item['to_pick_qty'] = parseFloat(to_pick_qty);
                            to_pick_item['manual_to_pick_qty'] = parseFloat(wh_pv.manual_to_pick_qty !== undefined ? wh_pv.manual_to_pick_qty : 0);
                            to_pick_item['wh_available_qty'] = parseFloat(wh_pv.quantity);
                            to_pick_item['os_id'] = os_id;
                            to_pick_item['os_item_index'] = os_item_index;
                            if (wh_final_picking[wh_pv.order] === undefined) {
                                wh_final_picking[wh_pv.order] = [];
                                wh_final_picking[wh_pv.order]['warehouse_id'] = wh_pv.warehouse_id;
                                wh_final_picking[wh_pv.order]['warehouses'] = [];
                            }
                            wh_final_picking[wh_pv.order]['warehouses'].push(JSON.stringify(to_pick_item));
                        });
                    }
                }     
            });
        });
        actual_wh_id = null;
        wh_actual = 0;
        var wms_novedades = [];
        $.each(wh_final_picking, function(){
            wh_final = wh_final_picking[wh_actual];
            if (wh_final !== undefined) {
                wh_final_id = wh_final.warehouse_id;
                if (actual_wh_id == null || actual_wh_id != wh_final_id) {
                    var newTr = $('<tr></tr>');
                    tr_html = '<th colspan="4">'+
                                '<h3>'+(warehouses_data[wh_final_id] ? warehouses_data[wh_final_id].name : 'AA')+'</h3>'+
                              '</th>';
                    newTr.html(tr_html);
                    newTr.appendTo("#tabla_set_picking");
                    actual_wh_id = wh_final_id;
                }
                $.each(wh_final.warehouses, function(wh_os_item_index, wh_os_item){
                    wh_os_item = JSON.parse(wh_os_item);

                    if (wh_os_item.manual_to_pick_qty > 0 && wh_os_item.manual_to_pick_qty < parseFloat(wh_os_item.to_pick_qty)) {
                        wms_novedades.push(wh_os_item.os_id+"_"+wh_os_item.os_item_index+"_"+wh_actual+"_"+(parseFloat(wh_os_item.to_pick_qty) - wh_os_item.manual_to_pick_qty)+"_"+wh_os_item.product_id+"_"+wh_os_item.option_id);
                    }

                    if (wh_os_item.to_pick_qty > 0) {
                        var newTr = $('<tr data-itemid="'+wh_os_item.os_item_index+'" data-osid="'+wh_os_item.os_id+'" data-wh_order="'+wh_actual+'"></tr>');
                        tr_html = '<td>'+
                                    '<input type="hidden" name="order_sale_item_id[]" value="'+wh_os_item.id+'">'+
                                    '<input type="hidden" name="order_sale_id[]" value="'+wh_os_item.sale_id+'">'+
                                    '<input type="hidden" name="option_id[]" value="'+wh_os_item.option_id+'">'+
                                    '<input type="hidden" name="product_id[]" value="'+wh_os_item.product_id+'">'+
                                    '<input type="hidden" name="product_code[]" value="'+wh_os_item.product_code+'">'+
                                    '<input type="hidden" name="product_name[]" value="'+wh_os_item.product_name+'">'+
                                    '<input type="hidden" name="warehouse_id[]" value="'+wh_final_id+'">'+
                                    '<span>'+wh_os_item.product_code+(wh_os_item.variant_suffix !== null && wh_os_item.variant_suffix !== undefined ? wh_os_item.variant_suffix : '')+'</span>'+
                                  '</td>';
                        tr_html += '<td>'+
                                    '<span>'+(wh_os_item.variant_name !== null && wh_os_item.variant_name !== undefined ? wh_os_item.variant_name : '')+'</span>'+

                                  '</td>';
                        tr_html += '<td>'+
                                    '<input type="text" name="required_quantity[]" class="form-control pquantity" value="'+formatDecimal(wh_os_item.manual_to_pick_qty > 0 ? wh_os_item.manual_to_pick_qty : wh_os_item.to_pick_qty)+'" data-minqty="'+formatDecimal(wh_os_item.to_pick_qty)+'" '+(wh_os_item.picked === undefined || wh_os_item.picked == false ? '' : 'readonly')+'>'+
                                  '</td>';
                        tr_html += '<td class="text-center">'+
                                    (wh_os_item.picked === undefined || wh_os_item.picked == false
                                    ?
                                    '<i class="fas fa-dolly-flatbed picking_pending text-default" style="cursor:pointer;"></i>'
                                    :
                                    '<i class="fas fa-clipboard-check picking_completed text-success"  style="cursor:pointer;"></i>'
                                    )+
                                  '</td>';
                        newTr.html(tr_html);
                        newTr.appendTo("#tabla_set_picking");
                    }
                });
            }
            wh_actual++;
        });

        if (wms_novedades.length > 0) {
            // order_sale_id, item_id, warehouse, quantity, product_id, option_id
            $("#tabla_set_novedades tbody").empty();
            $.each(wms_novedades, function(nindex, novedad){
                narr = novedad.split("_");
                osid = narr[0];
                item_id = narr[1];
                wms_order_sales = JSON.parse(localStorage.getItem('wms_order_sales'));
                item = wms_order_sales[osid].rows[item_id];
                warehouse = warehouses_data[item.warehouses_products[narr[2]].warehouse_id];
                var newTr = $('<tr></tr>');
                tr_html = '<td>'+
                            '<input type="hidden" name="notice_product_id[]" value="'+narr[4]+'">'+
                            '<input type="hidden" name="notice_option_id[]" value="'+narr[5]+'">'+
                            '<input type="hidden" name="notice_quantity[]" value="'+narr[3]+'">'+
                            '<input type="hidden" name="notice_warehouse_id[]" value="'+item.warehouses_products[narr[2]].warehouse_id+'">'+
                            '<span>'+warehouse.code+'</span>'+
                          '</td>';
                tr_html += '<td>'+
                            '<span>'+item.product_code+(item.variant_suffix !== null && item.variant_suffix !== undefined ? item.variant_suffix : '')+'</span>'+
                          '</td>';
                tr_html += '<td>'+
                            ''+narr[3]+
                          '</td>';
                tr_html += '<td>'+
                            picking_notices_html+
                          '</td>';
                newTr.html(tr_html);
                newTr.appendTo("#tabla_set_novedades");
            });
            $('#tabla_set_novedades').fadeIn();
        } else {
            $('#tabla_set_novedades').fadeOut();
        }
    }
}