<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<style>
    .ob {
        list-style: none;
        padding: 0;
        margin: 0;
        margin-top: 10px;
    }
    .ob li {
        width: 49%;
        margin: 0 10px 10px 0;
        float: left;
    }
    @media only screen and (max-width: 799px) {
        .ob li {
            width: 100%;
        }
    }
    .ob li .btn {
        width: 100%;
    }
    .ob li:nth-child(2n+2) {
        margin-right: 0;
    }
</style>
<div class="modal-dialog modal-md">
    <div class="modal-content">
        <div class="modal-header modal-primary">
            <button type="button" class="close" aria-hidden="true"><i class="fa fa-2x">&times;</i>
            </button>
            <span class="modal-title" id="susModalLabel" style="font-size: 150%;"><?= "(".$pdata->code.") ".$pdata->name ?></span>
            <h3><?=lang('product_variant_selection');?></h3>
        </div>
        <div class="modal-body" style="padding-bottom:0; max-height: 600px !important; overflow: auto;">
            <div class="form-group" id="ui3">
                <div class="input-group">
                    <?php
                        if(!isset($bill_reference)){
                            $bill_reference = "";
                        }
                    ?>
                    <?php echo form_input('search_reference', $bill_reference, 'class="form-control pos-tip btn-pos-product" id="search_reference" data-placement="top" data-trigger="focus" placeholder="'.lang('key_word').'" title="' . $this->lang->line("au_pr_name_tip") . '"'); ?>
                    <input type="hidden" name="pv_id" id="pv_id">
                    <div class="input-group-addon" style="padding: 2px 8px;border:none !important;">
                        <a id="pv_search" data-placement="bottom" data-html="true" data-toggle="ajax" tabindex="-1"> <i class="fa fa-search" id="xxd" style="font-size: 1.5em;"></i> </a>
                    </div>
                </div>
                <div style="clear:both;"></div>
            </div>
            <div class="html_con"><?= $html ?></div>
            <div class="clearfix"></div>
        </div>
        <div class="modal-footer">
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#pv_search').on('click', function (e) {
            var reference = $('#search_reference').val();
            var destino = "<?= admin_url('pos/product_variants_selection/'.$product_id.'/'.$positems_id) ?>/"+$('#poswarehouse').val()+"/"+reference;
            $("#pv_search").attr("href", destino);
        });
        setTimeout(function() {
            $('#search_reference').focus();
        }, 800);
        positems = JSON.parse(localStorage.getItem('positems'));
        if (positems["<?= $positems_id ?>"].row.option > 0) {
            $('.product_variant_select[data-pvid="'+positems["<?= $positems_id ?>"].row.option+'"]').removeClass('btn-outline');
            $('#pv_id').val(positems["<?= $positems_id ?>"].row.option);
        }
    });
    $('.bootbox-close-button').on('click', function(e){
        $('#myModal').modal('hide');
        e.preventDefault();
    });
    $('#search_reference').on('keypress', function(e){
        if (e.keyCode == 13) {
            $('#pv_search').click();
        }
    });
    $('.product_variant_select').on('click', function(e){
        let current_price_gram = 0;
        p_index = $($(this)).index('.product_variant_select');
        $(this).removeClass('btn-outline');
        pv_id = $(this).data('pvid');
        positems = JSON.parse(localStorage.getItem('positems'));
        positems["<?= $positems_id ?>"].row.option = pv_id;
        if (site.settings.management_weight_in_variants == "1") {
            if (positems["<?= $positems_id ?>"].row.based_on_gram_value == '1' || positems["<?= $positems_id ?>"].row.based_on_gram_value == '2') {
                const value_based_on_gram = positems["<?= $positems_id ?>"].row.based_on_gram_value;
                const url = site.base_url + 'pos/curren_price_gram?valuedOnGram='+value_based_on_gram;
                $.ajax({
                    url : url,
                    async : false,
                    dataType: 'json'
                }).done(function(data){
                    if (data['current_price']) {
						current_price_gram = data['current_price'];
						if (localStorage.getItem('gram_current_price')) {
							if (localStorage.getItem('gram_current_price') != current_price_gram) {
								// Crear una funcion para que actualice todos los creados
								update_current_gram_price(current_price_gram, '1');
							}
						}else{
							localStorage.setItem('gram_current_price', current_price_gram);
						}
					}
					if (data['current_price_italian']) {
						current_price_gram = data['current_price_italian'];
						if (localStorage.getItem('current_price_italian')) {
							if (localStorage.getItem('current_price_italian') != current_price_gram) {
								// Crear una funcion para que actualice todos los creados
								update_current_gram_price(current_price_gram, '2');
							}
						}else{
							localStorage.setItem('current_price_italian', current_price_gram);
						}
					}
                });
            }
        }
        positems["<?= $positems_id ?>"].row.current_price_gram = current_price_gram;
        positems["<?= $positems_id ?>"].row.variant_weight = $(this).data('weigth');
        new_item = positems["<?= $positems_id ?>"];
        new_item_id = (site.settings.item_addition == 1 ? new_item.item_id : new_item.id) + (new_item.row.option && new_item.row.option > 0 ? new_item.row.option.toString() : '');
        delete positems["<?= $positems_id ?>"];
        positems[new_item_id] = new_item;
        localStorage.setItem('positems', JSON.stringify(positems));
        loadItems();
        $('#myModal').modal('hide');
        if (positems[new_item_id].preferences) {
            setTimeout(function() {
                $('#myModal').modal({remote: site.base_url + 'pos/product_preferences_selection/' + "<?= $product_id ?>" + "/" + "<?= $positems_id ?>"});
                $('#myModal').modal('show');
            }, 850);
        }
    });
    $(document).on('keyup', '#myModal', function(e){
        if (e.keyCode == 27) {
            close_modal();
        }
    });
    $(document).on('click', '.close', function(){
        close_modal();
    });
    function close_modal(){
        $('#myModal').modal('hide');
        positems = JSON.parse(localStorage.getItem('positems'));
        delete positems["<?= $positems_id ?>"];
        localStorage.setItem('positems', JSON.stringify(positems));
        loadItems();
    }

</script>
