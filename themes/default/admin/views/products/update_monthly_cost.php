<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <?php if (isset($products_with_costing) || isset($products_without_costing) || isset($invalid_codes_txt)): ?>
                <div class="ibox float-e-margins border-bottom">
                    <div class="ibox-title">
                        <h5>Ver errores</h5>
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-down"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content" style="display: none;">
                        <?php if (isset($invalid_codes_txt)): ?>
                            <div class="panel panel-danger">
                              <div class="panel-heading">
                                  <?= lang('invalid_codes') ?>
                              </div>
                              <div class="panel-body">
                                <?= $invalid_codes_txt ?>
                              </div>
                            </div>
                        <?php endif ?>

                        <?php if (isset($products_with_costing)): ?>
                            <div class="panel panel-danger">
                              <div class="panel-heading">
                                  <?= lang('product_with_costing_is_not_updating_code_details') ?>
                              </div>
                              <div class="panel-body">
                                <?= $products_with_costing ?>
                              </div>
                            </div>
                        <?php endif ?>

                        <?php if (isset($products_without_costing)): ?>
                            <div class="panel panel-success">
                              <div class="panel-heading">
                                  <?= lang('product_without_costing_is_not_updating_code_details') ?>
                              </div>
                              <div class="panel-body">
                                <?= $products_without_costing ?>
                              </div>
                            </div>
                        <?php endif ?>
                    </div>
                </div>
            <?php endif ?>

            <div class="ibox">
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="title-action">
                                <a href="<?= admin_url('products/syncProductCostingByMonth') ?>" class="btn btn-primary">
                                    <span class="fa fa-refresh"></span>  <?= lang('sync_products_costs') ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    <?= admin_form_open_multipart("products/update_monthly_cost", array('class' => 'form', 'data-toggle' => 'validator', 'role' => 'form')) ?>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <?= lang('month', 'month') ?>
                                    <select name="month" id="month" class="form-control" required>
                                        <?php for ($i=1; $i <= 12 ; $i++) { ?>
                                            <option value="<?= $i ?>" <?= $i == date('m') ? 'selected="selected"' : '' ?> ><?=  lang('months')[$i]  ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="csv_file"><?= lang("upload_file"); ?></label>
                                    <input type="file" data-browse-label="<?= lang('browse'); ?>" name="userfile" class="form-control file" data-show-upload="false" data-show-preview="false" id="csv_file" required="required"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <?php echo form_submit('import', $this->lang->line("import"), 'class="btn btn-primary"'); ?>
                            </div>
                        </div>
                    <?= form_close(); ?>
                </div>
            </div>
        </div>
    </div>
</div>