<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="wrapper wrapper-content  animated fadeInRight no-printable">
     <div class="row">
        <div class="col-lg-12">
            <div class="no-print notifications_container">
                <?php if (isset($pending_electronic_document_message)) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?= $pending_electronic_document_message; ?>
                    </div>
                <?php } ?>

                <?php if (isset($validationElectronicHist)) { ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <?= $validationElectronicHist; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <?= admin_form_open('pos/fe_index', ['id'=>'posFilterForm']) ?>
                <div class="ibox">
                    <div class="ibox-title">
                        <div class="row">
                            <div class="col-sm-11">
                                <div class="row">
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for=""><?= $this->lang->line('date_records_filter') ?></label>
                                            <select class="form-control" name="date_records_filter" id="date_records_filter_dh">
                                                <?= $this->sma->get_filter_options($_POST["date_records_filter"]); ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('biller') ?></label>
                                            <?php $billerId = (isset($_POST["biller_id"])) ? $_POST["biller_id"] : ''; ?>
                                            <?php $billerId = (empty($billerId)) ? $this->session->userdata('biller_id') : $billerId; ?>
                                            <select class="form-control" id="biller_id" name="biller_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($billers as $biller) : ?>
                                                    <option value="<?= $biller->id ?>" data-defaultwh="<?= $biller->default_warehouse_id ?>" <?= ($biller->id == $billerId) ? 'selected' : '' ?>><?= $biller->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('document_type') ?></label>
                                            <?php $documentTypeId = (isset($_POST["document_type"])) ? $_POST["document_type"] : ''; ?>
                                            <select class="form-control" id="document_type" name="document_type">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($documentsTypes as $documentType) : ?>
                                                    <option value="<?= $documentType->id ?>" <?= ($documentType->id == $documentTypeId) ? 'selected' : 'no' ?>><?= $documentType->sales_prefix." - ".$documentType->nombre ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('origin') ?></label>
                                            <?php $saleOrigin = (isset($_POST["sale_origin"])) ? $_POST["sale_origin"] : ''; ?>
                                            <select class="form-control" id="sale_origin" name="sale_origin">
                                                <option value=""><?= lang('allsf') ?></option>
                                                <?php foreach ($salesOrigins as $origin) : ?>
                                                    <?php if ($origin->id != 5) : ?>
                                                        <option value="<?= $origin->id ?>" <?= ($origin->id == $saleOrigin) ? 'selected' : '' ?>><?= $origin->name ?></option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label for="client"><?= $this->lang->line('customer') ?></label>
                                            <?php $client = (isset($_POST["client"])) ? $_POST["client"] : ''; ?>
                                            <input type="text" class="form-control input-tip" name="client" id="client" value="<?= $client ?>" placeholder="<?= lang('alls') ?>">
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <label><?= $this->lang->line('seller') ?></label>
                                        <?php $sellerId = (isset($_POST["seller"])) ? $_POST["seller"] : ''; ?>
                                        <select class="form-control" name="seller" id="seller">
                                            <?php $sellerRestricted = $this->sma->seller_restricted(); ?>

                                            <?php if (!$sellerRestricted): ?>
                                                <option value=""><?= lang('select') ?></option>
                                            <?php endif ?>

                                            <?php if ($sellers): ?>
                                                <?php foreach ($sellers as $seller): ?>
                                                    <?php if (!$sellerRestricted||($sellerRestricted && $seller->id == $sellerRestricted)): ?>
                                                        <?php if(!$sellerRestricted) { ?>
                                                            <?php $sellerSelected = ($seller->id == $sellerId) ? "selected" : "" ?>
                                                        <?php } else { ?>
                                                            <?php $sellerSelected = ($sellerRestricted && $seller->id == $sellerRestricted) ? "selected='selected'" : "" ?>
                                                        <?php } ?>

                                                        <option value="<?= $seller->id ?>" <?= $sellerSelected ?>><?= $seller->company ?></option>
                                                    <?php endif ?>
                                                <?php endforeach ?>
                                            <?php endif ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-1 col-without-padding text-center">
                                <div class="new-button-container">
                                    <?php $icon = ($advancedFiltersContainer == TRUE) ? '<i class="fa fa-lg fa-chevron-up"></i>' : '<i class="fa fa-lg fa-chevron-down"></i>' ?>
                                    <button class="btn btn-primary btn-outline new-button collapse-link" type="button" data-toggle="tooltip" data-placement="bottom" title="Más filtros"><?= $icon ?></button>
                                    <button class="btn btn-primary new-button" id="salesFilterSubmit" type="submit" data-toggle="tooltip" data-placement="bottom" title="Aplicar filtros"><i class="fas fa-filter fa-lg"></i></button>
                                    <button class="btn btn-danger new-button" type="button" onclick="window.location.href = `${site.base_url}pos/fe_index`" data-toggle="tooltip" data-placement="bottom" title="<?= lang('reset') ?>"><i class="fa fa-times"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-content" id="advancedFiltersContainer" style="display: <?= ($advancedFiltersContainer == TRUE) ? 'block' : 'none' ?>;">
                        <div class="row">
                            <div class="col-sm-12">
                                <h3>Filtros avanzados</h3>
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="warehouse_id"><?= $this->lang->line('warehouse') ?></label>
                                            <?php $warehouseId = (isset($_POST["warehouse_id"])) ? $_POST["warehouse_id"] : ''; ?>
                                            <select class="form-control" id="warehouse_id" name="warehouse_id">
                                                <option value=""><?= $this->lang->line('allsf') ?></option>
                                                <?php foreach ($warehouses as $warehouse) : ?>
                                                    <option value="<?= $warehouse->id ?>" <?= ($warehouse->id == $warehouseId) ? 'selected' : '' ?>><?= $warehouse->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="payment_status"><?= lang('payment_status') ?></label>
                                            <?php $paymentStatus = (isset($_POST["payment_status"])) ? $_POST["payment_status"] : ''; ?>
                                            <select name="payment_status" id="payment_status" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($paymentsStatus as $status) : ?>
                                                    <option value="<?= $status->id ?>" <?= ($status->id == $paymentStatus) ? 'selected' : '' ?>><?= $status->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label><?= $this->lang->line('originalPaymentMethod') ?></label>
                                            <?php $paymentMethod = (isset($_POST["payment_method"])) ? $_POST["payment_method"] : ''; ?>
                                            <select class="form-control" id="payment_method" name="payment_method">
                                                <option value=""><?= $this->lang->line('alls') ?></option>
                                                <?php foreach ($paymentsMethods as $method) : ?>
                                                    <option value="<?= $method->code ?>" <?= ($method->code == $paymentMethod) ? 'selected' : '' ?>><?= $method->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="fe_aceptado"><?= $this->lang->line('dianStatus') ?></label>
                                            <?php $feAceptado = (isset($_POST["fe_aceptado"])) ? $_POST["fe_aceptado"] : ''; ?>
                                            <select name="fe_aceptado" id="fe_aceptado" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($dianStates as $dianStatus) : ?>
                                                    <option value="<?= $dianStatus->id ?>" <?= ($dianStatus->id == $feAceptado) ? 'selected' : '' ?>><?= $dianStatus->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-sm-2">
                                        <div class="form-group">
                                            <label for="fe_correo_enviado"><?= $this->lang->line('sended_email') ?></label>
                                            <?php $feCorreoEnviado = (isset($_POST["fe_correo_enviado"])) ? $_POST["fe_correo_enviado"] : ''; ?>
                                            <select name="fe_correo_enviado" id="fe_correo_enviado" class="form-control">
                                                <option value=""><?= lang('alls') ?></option>
                                                <?php foreach ($electronicEmailStates as $emailStates) : ?>
                                                    <option value="<?= $emailStates->id ?>" <?= ($emailStates->id == $feCorreoEnviado) ? 'selected' : '' ?>><?= $emailStates->name ?></option>
                                                <?php endforeach ?>
                                            </select>
                                        </div>
                                    </div>

                                    <?php if ($this->Owner || $this->Admin): ?>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label for="user"><?= $this->lang->line('user') ?></label>
                                                <?php $userId = (isset($_POST["user"])) ? $_POST["user"] : ''; ?>
                                                <select class="form-control" name="user" id="user">
                                                    <option value=""><?= lang('alls') ?></option>
                                                    <?php foreach ($users as $user): ?>
                                                        <option value="<?= $user->id ?>" <?= ($user->id == $userId) ? 'selected' : '' ?>><?= $user->first_name." ".$user->last_name ?></option>
                                                    <?php endforeach ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php endif ?>
                                </div>

                                <div class="row date_controls_dh">
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for="start_date_dh"><?= $this->lang->line('start_date') ?></label>
                                            <?php $startDate = isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>
                                            <input class="form-control datetime" type="text" name="start_date" id="start_date_dh" value="<?= $startDate ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-2" <?= $this->hide_date_range ? 'style="display:none;"' : '' ?> >
                                        <div class="form-group">
                                            <label for="end_date_dh"><?= $this->lang->line('end_date') ?></label>
                                            <?php $endDate = isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>
                                            <input class="form-control datetime" type="text" name="end_date" id="end_date_dh" value="<?= $endDate ?>" class="form-control datetime">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?= form_close() ?>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins border-bottom">
                <div class="ibox-content">
                    <div class="box-content">

                    <div class="row">
                <?php if ($Owner || $Admin ||  $GP['bulk_actions']) {
                    echo admin_form_open('pos/pos_actions', 'id="action-form"');
                } ?>
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table id="POSData" class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th style="min-width:30px; width: 30px; text-align: center;">
                                            <input class="checkbox checkft" type="checkbox" name="check"/>
                                        </th>
                                        <th><?= lang("date"); ?></th>
                                        <th><?= lang("payment_term"); ?></th>
                                        <th><?= lang("reference_origin"); ?></th>
                                        <th><?= lang("reference_no"); ?></th>
                                        <th><?= lang("affects_to"); ?></th>
                                        <th><?= lang("biller"); ?></th>
                                        <th><?= lang("customer_name"); ?></th>
                                        <th><?= lang("grand_total"); ?></th>
                                        <th><?= lang("paid"); ?></th>
                                        <th><?= lang("balance"); ?></th>
                                        <th><?= lang("sale_status"); ?></th>
                                        <th><?= lang("payment_status"); ?></th>
                                        <th><?= lang("acepted_DIAN"); ?></th>
                                        <th><?= lang("sended_email"); ?></th>
                                        <th></th>
                                        <th></th>
                                        <th style="text-align:center;"><?= lang("actions"); ?></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td colspan="11" class="dataTables_empty"><?= lang("loading_data"); ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if ($Owner || $Admin || $GP['bulk_actions']) { ?>
    <div style="display: none;">
        <input type="hidden" name="form_action" value="" id="form_action"/>
        <input type="hidden" name="electronic" value="" id="electronic"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
    <?= form_close() ?>
<?php } ?>

<div class="modal fade" id="confirmation_email_modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="confirmation_email_form">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"> <i class="fa fa-question-circle"></i> Confirmación de Correo Electrónico</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label for="confirmation_email">Correo electrónico</label>
                                <input class="form-control" type="email" name="confirmation_email" id="confirmation_email">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="send_electronic_invoice_email">Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var _tabFilterFill = false;
    var nums = [[10, 25], [10, 25]];

    if ($(window).width() > 1000) {
        var nums = [[10, 25, 50, 100, 500, -1], [10, 25, 50, 100, 500, "<?=lang('all')?>"]];
    }

    $(document).ready(function () {
        oTable = $('#POSData').dataTable({
            aaSorting: [[1, "desc"], [3, "desc"]],
            aLengthMenu: nums,
            iDisplayLength: $(window).width() < 1000 ? 10 : <?=$Settings->rows_per_page?>,
            bProcessing: true,
            bServerSide: true,
            dom: '<"row" <"col-sm-7 additionalControlsContainer"><"col-sm-3"f><"col-sm-1"l><"col-sm-1 actionsButtonContainer">t<"col-sm-6"i><"col-sm-6"p>>',
            sAjaxSource: '<?= admin_url('pos/getSalesFE?warehouse_id='.($warehouse_id ? $warehouse_id : '').'&dias_vencimiento='.(isset($dias_vencimiento) ? $dias_vencimiento : '')) ?>',
            fnServerData: function (sSource, aoData, fnCallback) {
                aoData.push({
                    "name": "<?= $this->security->get_csrf_token_name() ?>",
                    "value": "<?= $this->security->get_csrf_hash() ?>"
                }, {
                    "name": "date_records_filter",
                    "value": $('#date_records_filter').val()
                }, {
                    "name": "biller_id",
                    "value": $('#biller_id').val()
                }, {
                    "name": "document_type",
                    "value": $('#document_type').val()
                }, {
                    "name": "sale_origin",
                    "value": $('#sale_origin').val()
                }, {
                    "name": "campaign",
                    "value": $('#campaign').val()
                }, {
                    "name": "seller",
                    "value": $('#seller').val()
                }, {
                    "name": "client",
                    "value": $('#client').val()
                }, {
                    "name": "warehouse_id",
                    "value": $('#warehouse_id').val()
                }, {
                    "name": "payment_status",
                    "value": $('#payment_status').val()
                }, {
                    "name": "payment_method",
                    "value": $('#payment_method').val()
                } , {
                    "name" : "fe_aceptado",
                    "value" : $('#fe_aceptado').val()
                } , {
                    "name" : "fe_correo_enviado",
                    "value" : $('#fe_correo_enviado').val()
                }, {
                    "name" : "user",
                    "value" : $('#user').val()
                }, {
                    "name": "start_date",
                    "value": "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
                }, {
                    "name": "end_date",
                    "value": "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>"
                }, {
                    "name": "optionFilter",
                    "value": option_filter
                });
                $.ajax({'dataType': 'json', 'type': 'POST', 'url': sSource, 'data': aoData, 'success': fnCallback});
            },
            fnRowCallback: function (nRow, aData, iDisplayIndex) {
                var oSettings = oTable.fnSettings();
                nRow.id = aData[0];
                nRow.className = "posFeRow";
                return nRow;
            },
            aoColumns: [
                {
                    "bSortable": false,
                    "mRender": checkbox
                },
                {"mRender": fld},
                null,
                null,
                null,
                null,
                null,
                null,
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": currencyFormat},
                {"mRender": row_status},
                {"mRender": pay_status},
                {
                    mRender:  function (data, type, row) {
                        return renderStatusDian(data, row[0]);
                    }
                },
                {"mRender": renderSendedEmail},
                {"bVisible": false},
                {"bVisible": false},
                {
                    mRender: function (data, type, row) {
                        return renderActionButton(data, row[13], row[0], row[15], row[11], row[16], row[1]);
                    },
                    bSortable: false
                }
            ],
            fnDrawCallback: function (oSettings) {
                var addSaleButton = postSaleButton = '';
                if (!_tabFilterFill) {
                    $('.additionalControlsContainer').html('<div class="wizard">' +
                        '<div class="steps clearfix">' +
                            '<ul role="tablist">' +
                                '<li role="tab" class="first current index_list" aria-disabled="false"  aria-selected="true" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="all" href="#">'+
                                        '<span class="all_span">0</span><br><?= lang('allsf') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done  index_list" aria-disabled="false" style="width:16.6%;">' +
                                    '<a class="wizard_index_step" data-optionfilter="returns" href="#" aria-controls="edit_biller_form-p-0">' +
                                        '<span class="returns_span">0</span><br><?= lang('returns') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="notApproved" href="#">' +
                                        '<span class="notApproved_span">0</span><br><?= lang('notApproved') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingPayment" href="#">'+
                                        '<span class="pendingPayment_span">0</span><br><?= lang('pendingPayment') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingDian" href="#">'+
                                        '<span class="pendingDian_span">0</span><br><?= lang('pendingDian') ?>' +
                                    '</a>' +
                                '</li>' +
                                '<li role="tab" class="done index_list" aria-disabled="false" aria-selected="false" style="width:16.6%;">' +
                                    '<a  class="wizard_index_step" data-optionfilter="pendingDispatch" href="#">'+
                                        '<span class="pendingDispatch_span">0</span><br><?= lang('pendingDispatch') ?>' +
                                    '</a>' +
                                '</li>' +
                            '</ul>' +
                        '</div>' +
                    '</div>').addClass('col-without-padding');

                    _tabFilterFill = true;
                }

                <?php if ($this->Owner || $this->Admin || $this->GP['bulk_actions']): ?>
                    let addButton = ''
                    let changeStatesDelcop = ''
                    let changeStatesSimba = '';

                    <?php if ($this->Owner || $this->Admin || $this->GP['sales-add']): ?>
                        addButton = '<a href="<?= admin_url('pos') ?>" class="btn btn-primary new-button pull-right" data-toggle-second="tooltip" data-placement="top" title="Agregar"><i class="fas fa-plus fa-lg"></i></a>';
                        if (fe_technology_provider == '2') {
                            changeStatesDelcop = '<li><a id="changeStatesDelcop"><i class="fas fa-toggle-off"></i> <?= lang('changeStatesDelcop') ?></a></li>';
                        }
                        if (fe_technology_provider == '3') {
                            changeStatesSimba = '<li><a id="changeStatesSimba"><i class="fas fa-toggle-off"></i> <?= lang('changeStatesSimba') ?></a></li>';
                        }
                    <?php endif ?>

                    pdfButton = '';
                    <?php if ($this->Owner || $this->Admin || $this->GP['bulk_actions']): ?>
                        pdfButton = '<li><a href="#" id="post_sale" data-action="post_sale"><i class="fa fa-file-pdf-o"></i> <?= lang('post_sale') ?></a></li>';
                    <?php endif ?>

                    $('.actionsButtonContainer').html(`${addButton}
                    <div class="pull-right dropdown">
                        <button class="btn btn-primary btn-outline new-button dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="<?= lang('actions') ?>"><i class="fas fa-ellipsis-v fa-lg"></i></button> 
                        <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                            <li>
                                <a href="#" id="excel" data-action="export_excel"><i class="fa fa-file-excel-o"></i> <?= lang('export_to_excel') ?></a>
                            </li>
                            ${pdfButton}
                            ${changeStatesDelcop}
                            ${changeStatesSimba}
                            <li>
                                <a href="#" id="mark_printed_sale" data-action="mark_printed_sale">
                                    <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_printed_sale') ?>
                                </a>
                            </li>
                            <li>
                                <a href="#" id="mark_all_printed_sale" data-action="mark_all_printed_sale" data-electronic="1">
                                    <i class="fa-solid fa-check-to-slot"></i> <?= lang('mark_all_printed_sale') ?>
                                </a>
                            </li>
                        </ul>
                    </div>`);
                    <?php if (!empty($warehouses)) { ?>
                        /* <div class="pull-right dropdown">
                            <button data-toggle="dropdown" class="btn btn-outline btn-success dropdown-toggle pull-right"> Almacenes <span class="caret"></span> </button>
                            <ul class="dropdown-menu m-t-xs pull-right" role="menu" aria-labelledby="dLabel">
                                <li><a href="<?= admin_url('pos/sales') ?>"><i class="fa fa-building-o"></i> <?= lang('all_warehouses') ?></a></li>
                                <li class="divider"></li>
                                <?php
                                foreach ($warehouses as $warehouse) {
                                    echo '<li><a href="' . admin_url('pos/sales/' . $warehouse->id) . '"><i class="fa fa-building"></i>' . $warehouse->name . '</a></li>';
                                }
                                ?>
                            </ul>
                        </div> */
                    <?php } ?>

                <?php endif ?>

                $('[data-toggle-second="tooltip"]').tooltip();

                $('input[type="checkbox"]').iCheck({
                    checkboxClass: 'icheckbox_flat-blue',
                    increaseArea: '20%'
                });

                loadDataTabFilters();
            }
        });

        $(document).on('click', '.email_confirmation', function () { confirmation_email($(this).data("sale_id")); });
        $(document).on('click', '#send_electronic_invoice_email', function () { send_electronic_invoice_email(); });
        $(document).on('click', '.pending_errors', function () { openErrorModal($(this).data('sale_id')); });
        $(document).on('change', '#date_records_filter_dh', function() { showHidefilterContent($(this)) });
        $(document).on('click', '.show_pending_files', function() { show_pending_files(); });
        $(document).on('click', '#changeStatesDelcop', function() { changeStatesDelcop() })
        $(document).on('click', '#changeStatesSimba', function() { changeStatesSimba() });
        $('#confirmation_email_modal').on('hidden.bs.modal', function (e) {
            $("#confirmation_email_modal #send_electronic_invoice_email").html('Enviar').attr('disabled', false);
        });

        $('#client').val('<?= $client ?>').select2({
            minimumInputLength: 1,
            data: [],
            initSelection: function (element, callback) {
                $.ajax({
                    type: "get",
                    async: false,
                    url: site.base_url+"customers/getCustomer/" + $(element).val(),
                    dataType: "json",
                    success: function (data) {
                        callback(data[0])
                    }
                });
            },
            ajax: {
                url: site.base_url + "customers/suggestions",
                dataType: 'json',
                quietMillis: 15,
                data: function (term, page) {
                    return {
                        term: term,
                        limit: 10
                    };
                },
                results: function (data, page) {
                    if (data.results != null) {
                        return {results: data.results};
                    } else {
                        return {results: [{id: '', text: lang.no_match_found}]};
                    }
                }
            }
        });

        $(document).on('change', '#biller_id', function() { loadDocumentsType($(this)); loadWarehouses($(this)); });

        $('[data-toggle="tooltip"]').tooltip();
    });

    function show_pending_files()
    {
        $('#fe_aceptado').select2('val', '4');

        $('#salesFilterSubmit').trigger('click');
    }

    function renderStatusDian(document_status, sale_id)
    {
        if (document_status == '0') {
            return '<div class="text-center"><span class="payment_status pending_errors label label-danger" data-sale_id="'+ sale_id +'">'+lang["not_sent"]+'</span></div>';
        } else if(document_status == '1') {
            return '<div class="text-center"><span class="payment_status pending_errors label label-warning" data-sale_id="'+ sale_id +'">'+lang["pending"]+'</span></div>';
        } else if(document_status == '2') {
            return '<div class="text-center"><span class="payment_status label label-primary">'+lang["accepted"]+'</span></div>';
        } else if (document_status == '3') {
            return '<div class="text-center"><span class="payment_status pending_errors label label-success" data-sale_id="'+ sale_id +'">'+lang["sent"]+'</span></div>';
        }
    }

    function renderSendedEmail(sended_email)
    {
        if (sended_email == '0') {
            return '<div class="text-center text-danger"><i class="" data-toggle="tooltip" data-placement="top" title="'+lang["pending"]+'"></i></div>';
        } else if(sended_email == '1') {
            return '<div class="text-center text-info"><i class="fa fa-check"data-toggle="tooltip" data-placement="top" title="'+lang["sent"]+'"></i></div>';
        } else {
            return ''
        }
    }

    function renderActionButton(actions_button, accepted_dian, sale_id, document_type, sale_status, electronic_document, dateInv)
    {
        var actions = $(actions_button);
        var fe_technology_provider = '<?= $this->pos_settings->technology_provider_id ?>';

        if (fe_technology_provider == false) {
            if (document_type == false) {
                actions.find('.dropdown-menu').find('.link_return_sale').remove();
            }
        } else {
            if (electronic_document == 1) {
                actions.find('.dropdown-menu').prepend('<li class="divider"></li>');

                if (document_type == FACTURA_POS || document_type == FACTURA_DETAL) {
                    path_show_previous_XML = 'admin/pos/showPreviousRequest/'+sale_id;
                    actions.find('.dropdown-menu').prepend('<li><a href="'+ path_show_previous_XML +'" target="_blank"><i class="fa fa-file-code-o"></i> Mostrar Request previo</a></li>');

                    if (fe_technology_provider == DELCOP) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/documentStatusDELCOP/'+sale_id+'"><i class="fa fa-info"></i> Estado del documento</a></li>');
                    }

                    if (sale_status == "completed" && fe_technology_provider == 3) { // <- simba tecnology provider
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusSimba/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }

                    if (accepted_dian == '0' || accepted_dian == '1') {
                        actions.find('.dropdown-menu').find('.print_document').remove();
                        actions.find('.dropdown-menu').find('.link_return_sale').remove();
                        actions.find('.dropdown-menu').find('.link_debit_note').remove();

                        if (sale_status == 'completed') {
                            const ActualDay = new Date();
                            ActualDay.setHours(0, 0, 0, 0);
                            const inputDateInv = new Date(dateInv);
                            inputDateInv.setHours(0, 0, 0, 0);
                            if (ActualDay.getTime() !== inputDateInv.getTime()) {
                                actions.find('.dropdown-menu').prepend('<li class="change_date_electronic_document" data-document_id="'+ sale_id +'"><a href="admin/sales/change_date/'+sale_id+'"><i class="fas fa-refresh"></i> Cambiar fecha y reenviar factura</a></li>');
                            }
                            actions.find('.dropdown-menu').prepend('<li class="resendElectronicDocument" data-document_id="'+ sale_id +'"><a href="admin/pos/resendElectronicDocument/'+sale_id+'"><i class="far fa-paper-plane"></i> Reenviar factura</a></li>');
                        }
                    } else if (accepted_dian == '2') {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/saleView/'+sale_id+'/0/0/0/0/1" target="_blank"><i class="fa fa-print"></i> Impresión tirilla</a></li>');
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/downloadFeXml/'+sale_id+'"><i class="far fa-file-code"></i> '+lang['xml_download']+'</a></li>');
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/downloadFePdf/'+sale_id+'" target="_blank"><i class="far fa-file-pdf"></i> '+lang['pdf_download']+'</a></li>');
                        actions.find('.dropdown-menu').prepend('<li><a class="email_confirmation" data-sale_id="'+sale_id+'"><i class="fa fa-envelope-o"></i> '+lang['send_FE']+'</a></li>');

                    } else if (accepted_dian == '3') {
                        if (fe_technology_provider == CADENA) {
                            actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Cambiar estado</a></li>');
                        }
                    }

                    actions.find('.dropdown-menu').prepend('<li><label style="padding-left: 20px; padding-top: 10px">Facturación electrónica</label></li>');
                } else if (document_type == NOTA_CREDITO_POS || document_type == NOTA_CREDITO_DETAL) {
                    if (fe_technology_provider == DELCOP) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/documentStatusDELCOP/'+sale_id+'"><i class="far fa-info"></i> Estado del documento</a></li>');
                    }

                    if (sale_status == 'returned' && fe_technology_provider == 3) { // <- simba tecnology provider
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/sales/checkStatusSimba/'+sale_id+'"><i class="fa fa-info"></i> Consultar Estado</a></li>');
                    }

                    path_show_previous_XML = 'admin/pos/showPreviousRequest/'+sale_id;
                    actions.find('.dropdown-menu').prepend('<li><a href="'+ path_show_previous_XML +'" target="_blank"><i class="fa fa-file-code-o"></i> Mostrar Request previo</a></li>');

                    if (accepted_dian == '0' || accepted_dian == '1') {
                        actions.find('.dropdown-menu').find('.print_document').remove();
                        actions.find('.dropdown-menu').find('.link_return_sale').remove();

                        if (sale_status == 'returned') {
                            const ActualDay = new Date();
                            ActualDay.setHours(0, 0, 0, 0);
                            const inputDateInv = new Date(dateInv);
                            inputDateInv.setHours(0, 0, 0, 0);
                            if (ActualDay.getTime() !== inputDateInv.getTime()) {
                                actions.find('.dropdown-menu').prepend('<li class="change_date_electronic_document" data-document_id="'+ sale_id +'"><a href="admin/sales/change_date/'+sale_id+'"><i class="fas fa-refresh"></i> Cambiar fecha y reenviar Nota crédito</a></li>');
                            }
                            actions.find('.dropdown-menu').prepend('<li class="resendElectronicDocument" data-document_id="'+ sale_id +'"><a href="admin/pos/resendElectronicDocument/'+sale_id+'"><i class="far fa-paper-plane"></i> Reenviar Nota crédito</a></li>');
                        }
                    } else if (accepted_dian == '2') {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/downloadFeXml/'+sale_id+'"><i class="far fa-file-code"></i> '+lang['xml_download']+'</a></li>');
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/downloadFePdf/'+sale_id+'" target="_blank"><i class="far fa-file-pdf"></i> '+lang['pdf_download']+'</a></li>');
                        actions.find('.dropdown-menu').prepend('<li><a class="email_confirmation" data-sale_id="'+sale_id+'"><i class="fa fa-envelope-o"></i> '+lang['send_FE']+'</a></li>');

                        if (fe_technology_provider == CADENA) {
                            actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Cambiar estado</a></li>');
                        }
                    } else if (accepted_dian == '3') {
                        if (fe_technology_provider == CADENA) {
                            actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Cambiar estado</a></li>');
                        }
                    }

                    actions.find('.dropdown-menu').prepend('<li><label style="padding-left: 20px; padding-top: 10px">Facturación electrónica</label></li>');
                    actions.find('.dropdown-menu').find('.link_invoice_action').remove();
                } else if (document_type == NOTA_DEBITO_POS || document_type == NOTA_DEBITO_DETAL) {
                    if (fe_technology_provider == DELCOP) {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/documentStatusDELCOP/'+sale_id+'"><i class="fa fa-info"></i> Estado del documento</a></li>');
                    }

                    path_show_previous_XML = 'admin/pos/showPreviousRequest/'+sale_id;
                    actions.find('.dropdown-menu').prepend('<li><a href="'+ path_show_previous_XML +'" target="_blank"><i class="far fa-file-code"></i> Mostrar Request previo</a></li>');

                    if (accepted_dian == '0' || accepted_dian == '1') {
                        actions.find('.dropdown-menu').find('.print_document').remove();
                        actions.find('.dropdown-menu').find('.link_return_sale').remove();

                        actions.find('.dropdown-menu').prepend('<li class="resendElectronicDocument" data-document_id="'+ sale_id +'"><a href="admin/pos/resendElectronicDocument/'+sale_id+'"><i class="far fa-paper-plane"></i> Reenviar Nota débito</a></li>');
                    } else if (accepted_dian == '2') {
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/downloadFeXml/'+sale_id+'"><i class="far fa-file-code"></i> '+lang['xml_download']+'</a></li>');
                        actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/downloadFePdf/'+sale_id+'" target="_blank"><i class="far fa-file-pdf"></i> '+lang['pdf_download']+'</a></li>');
                        actions.find('.dropdown-menu').prepend('<li><a class="email_confirmation" data-sale_id="'+sale_id+'"><i class="fa fa-envelope-o"></i> '+lang['send_FE']+'</a></li>');

                    } else if (accepted_dian == '3') {
                        if (fe_technology_provider == CADENA) {
                            actions.find('.dropdown-menu').prepend('<li><a href="admin/pos/change_status_accepted_DIAN/'+sale_id+'"><i class="fa fa-info"></i> Cambiar estado</a></li>');
                        }
                    }

                    actions.find('.dropdown-menu').prepend('<li><label style="padding-left: 20px; padding-top: 10px">Facturación electrónica</label></li>');
                }
            }
        }

        var str = actions.prop('outerHTML');
        return str;
    }

    function confirmation_email(sale_id)
    {
        $.ajax({
            url: '<?= admin_url("sales/get_email_customer_by_sale_id"); ?>',
            type: 'POST',
            dataType: 'JSON',
            data: {
                '<?= $this->security->get_csrf_token_name(); ?>': '<?= $this->security->get_csrf_hash(); ?>',
                'sale_id': sale_id
            },
        })
        .done(function(data) {
            if (data != '') {
                $('#confirmation_email_modal #confirmation_email').val(data);
                $("#confirmation_email_modal #send_electronic_invoice_email").val(sale_id);

                $('#confirmation_email_modal').modal({backdrop: 'static'});
            }
        })
        .fail(function(data) {
            console.log(data.responseText);
        });
    }

    function send_electronic_invoice_email()
    {
        sale_id = $("#confirmation_email_modal #send_electronic_invoice_email").val();
        email = $("#confirmation_email_modal #confirmation_email").val();

        if (!(/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))) {
            Command: toastr.error('Por favor ingrese un dirección de correo electrónico válido', '<?= lang('toast_error_title') ?>', {onHidden: function() { $('#confirmation_email').focus(); }});
            return false;
        }

        $("#confirmation_email_modal #send_electronic_invoice_email").html('<i class="fa fa-spinner fa-pulse fa-fw"></i> enviando...').attr('disabled', true);

        window.location.href='<?= admin_url("pos/send_electronic_invoice_email"); ?>'+'/'+sale_id+'/'+email;
    }

    function openErrorModal(documentId)
    {
        $.ajax({
            type: "post",
            url: "<?= admin_url('pos/getMessage') ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'documentId': documentId
            },
            dataType: "json",
            success: function (response) {
                swal({
                    title: response.title,
                    text: response.text,
                    type: 'info',
                    showCancelButton: true,
                    showConfirmButton: false,
                    cancelButtonText: "Ok"
                });
            }
        });
    }

    function loadDataTabFilters()
    {
        $.ajax({
            type: 'post',
            url: '<?= admin_url('pos/getSalesFE'); ?>',
            data: {
                tabFilterData: 1,
                user: $('#user').val(),
                seller: $('#seller').val(),
                optionFilter: option_filter,
                biller_id: $('#biller_id').val(),
                campaign: $('#campaign').val(),
                sale_origin: $('#sale_origin').val(),
                fe_aceptado: $('#fe_aceptado').val(),
                warehouse_id: $('#warehouse_id').val(),
                document_type: $('#document_type').val(),
                payment_status: $('#payment_status').val(),
                payment_method: $('#payment_method').val(),
                fe_correo_enviado: $('#fe_correo_enviado').val(),
                date_records_filter: $('#date_records_filter').val(),
                "<?= $this->security->get_csrf_token_name() ?>": "<?= $this->security->get_csrf_hash() ?>",
                end_date: "<?= isset($_POST['end_date']) ? $_POST['end_date'] : $this->filtros_fecha_final ?>",
                start_date: "<?= isset($_POST['start_date']) ? $_POST['start_date'] : $this->filtros_fecha_inicial ?>"
            },
            dataType: 'json'
        }).done(function(data) {
            $('.all_span').text(data.all);
            $('.notApproved_span').text(data.notApproved);
            $('.pendingDian_span').text(data.pendingDian);
            $('.pendingDispatch_span').text(data.pendingDispatch);
            $('.pendingPayment_span').text(data.pendingPayment);
            $('.returns_span').text(data.returns);
        });
    }

    function showHidefilterContent(element)
    {
        var advancedFiltersContainer = '<?= $advancedFiltersContainer ? 1 : 0 ?>';

        var dateFilter = element.val();
        if (dateFilter != '') {
            if (dateFilter == 5) {
                $('#advancedFiltersContainer').fadeIn();
                $('.new-button-container .collapse-link').html('<i class="fa fa-lg fa-chevron-up"></i>');
            }
        }
    }

    function loadDocumentsType(element)
    {
        var billerId = element.val();
        var options = '<option value=""><?= $this->lang->line('allsf') ?></option>';

        $.ajax({
            type: "post",
            url: "<?= admin_url("sales/getDocumentTypes") ?>",
            data: {
                '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                'billerId': element.val(),
                'asynchronous': 1,
                'electronicDocument': 1,
                'modules'       : [1, 3, 26],

            },
            dataType: "json",
            success: function (response) {
                if (response != false) {
                    response.forEach(documentType => {
                        options += '<option value="'+documentType.id+'">'+documentType.sales_prefix+' - '+documentType.nombre+'</option>';
                    });
                }

                $('#document_type').html(options);
            }
        });
    }

    function loadWarehouses(element) {
        if (element.val()) {
            var defaultwh = element.children("option:selected").data('defaultwh');
            $('#warehouse_id').select2('val', defaultwh).trigger('change');
        } else {
            $('#warehouse_id option').each(function(index, option) {
                $(option).attr('disabled', false);
            });
            $('#warehouse_id').select2('val', '').trigger('change');
        }
    }

    function changeStatesDelcop()
    {
        swal({
            title: "Consulta de Estado de Documento Electrónico",
            text: "¿Está seguro de realizar este proceso?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            let checksSelected = [];
            $('.checkbox').each(function(i, e) {
                if ($(e).is(':checked')) {
                    checksSelected.push($(this).val())
                }
            })

            if (validateDocumentsSelected(checksSelected)) {
                swal({
                    title: 'Consultado documentos electrónicos...',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });

                $.ajax({
                    type: 'post',
                    url: site.base_url+'pos/changeStatesDelcopAjax',
                    data: {
                        '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                        documents : checksSelected
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response);
                        if (response.status == true) {
                            location.reload();
                        } else {
                            swal({
                                title: `<?= lang('toast_warning_title') ?>`,
                                text: response.message,
                                type: 'warning',
                                showCancelButton: true,
                                showConfirmButton: false,
                                cancelButtonText: "Ok"
                            }, function(result) {
                                location.reload();
                            })
                        }
                    }
                });
            }
        });
    }

    function changeStatesSimba()
    {
        swal({
            title: "Consulta de Estado de Documento Electrónico",
            text: "¿Está seguro de realizar este proceso?",
            type: "info",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonText: "No",
            confirmButtonText: "¡Sí!",
            closeOnConfirm: false
        }, function () {
            let checksSelected = [];
            $('.checkbox').each(function(i, e) {
                if ($(e).is(':checked')) {
                    checksSelected.push($(this).val())
                }
            })

            if (validateDocumentsSelected(checksSelected)) {
                swal({
                    title: 'Consultado documentos electrónicos...',
                    text: 'Por favor, espere un momento',
                    type: 'info',
                    showCancelButton: false,
                    showConfirmButton: false,
                    closeOnClickOutside: false,
                });

                $.ajax({
                    type: 'post',
                    url: site.base_url+'pos/changeStatesSimbaAjax',
                    data: {
                        '<?= $this->security->get_csrf_token_name() ?>': '<?= $this->security->get_csrf_hash() ?>',
                        documents : checksSelected
                    },
                    dataType: 'json',
                    success: function (response) {
                        console.log(response);
                        if (response.status == true) {
                            location.reload();
                        } else {
                            swal({
                                title: `<?= lang('toast_warning_title') ?>`,
                                text: response.message,
                                type: 'warning',
                                showCancelButton: true,
                                showConfirmButton: false,
                                cancelButtonText: "Ok"
                            }, function(result) {
                                location.reload();
                            })
                        }
                    }
                });
            }
        });
    }

    function validateDocumentsSelected(checksSelected) {
        if (Object.keys(checksSelected).length === 0) {
            swal({
                title: `<?= lang('toast_warning_title') ?>`,
                text: `No ha selecionado ningún documento para realizar el proceso.`,
                type: 'warning',
                showCancelButton: true,
                showConfirmButton: false,
                cancelButtonText: "Ok"
            }, function(result) {
                return false
            })
        } else {
            return true
            }
    }

</script>