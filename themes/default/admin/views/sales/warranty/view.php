<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <?= admin_form_open("warranty/createActivity/$warranty->id", ['id'=>'add_deposit']); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-2x">&times;</i></button>
                <h3 class="modal-title" id="addDepositLabel"> <?= lang('warranty'); ?> </h3>
                <br>
                <h2>
                    <small>
                        <strong><?= "$warranty->productName ( $warranty->reference_no )" ?></strong>
                        <span class="pull-right badge <?= "badge-{$status[$warranty->status]->color}" ?>"><?= $status[$warranty->status]->name ?></span>
                    </small>
                </h2>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="row">
                            <div class="col-sm-6">
                                <p><strong>Fecha:</strong> <?= $warranty->date ?></p>
                                <p><strong>Sucursal:</strong> <?= $warranty->billerName ?></p>
                                <p><strong>Factura:</strong> <?= $warranty->reference_no ?></p>
                            </div>
                            <div class="col-sm-6">
                                <p><strong>Cliente:</strong> <?= $warranty->customerName ?></p>
                                <p><strong>Motivo de devolución:</strong> <?= $warranty->reason ?></p>
                                <p><strong>Factura:</strong> <?= $warranty->referenceNoSale ?></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <p><strong>Detalle:</strong> <?= $warranty->warranty_details ?></p>
                            </div>
                            <div class="col-sm-6">
                                <p><strong>Estado del producto:</strong> <?= $warranty->product_status ?></p>
                            </div>
                        </div>
                        <hr>
                    </div>

                    <div class="col-sm-12">
                        <?php if (!empty($activities)) { ?>
                            <?php foreach ($activities as $activity) { ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p><strong>Fecha:</strong> <?= $activity->date ?></p>
                                        <p><strong>Actividad:</strong> <?= $activity->descriptionActivity ?></p>
                                    </div>
                                    <div class="col-sm-6">
                                        <p><strong>Estado:</strong> <?= $status[$activity->status]->name ?></p>
                                        <p><strong>Descripción:</strong> <?= $activity->description ?></p>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?= form_close(); ?>
    </div>
</div>
<?= $modal_js ?>

<script>
    $(document).ready(function () {
    });
</script>
