<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Payroll_electronic extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->loggedIn) {
            $this->session->set_userdata('requested_page', $this->uri->uri_string());
            $this->sma->md('login');
        }

        if (!$this->enableElectronicPayroll) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        if ($this->Customer || $this->Supplier) {
            $this->session->set_flashdata('warning', lang('access_denied'));
            admin_redirect($_SERVER["HTTP_REFERER"]);
        }

        $this->Payroll_settings = $this->site->get_payroll_setting();

        $this->lang->admin_load('payroll', $this->Settings->user_language);
        $this->load->library('form_validation');

        $this->load->admin_model("Employees_model");
        $this->load->admin_model("Payroll_concepts_model");
        $this->load->admin_model("Payroll_contracts_model");
        $this->load->admin_model("Payroll_electronic_model");
        $this->load->admin_model("Payroll_management_model");
    }

    public function index()
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->page_construct("payroll_electronic/index", ["page_title" => lang("payroll_electronic_list")], $this->data);
    }

    public function get_datatables()
    {
        $this->load->library('datatables');
        $this->datatables->select("id, creation_date, month, employee_no, earnings, deductions, total_payment, status");
        $this->datatables->from("payroll_electronic");
        $this->datatables->add_column("Actions", '<div class="text-center"><div class="btn-group text-left">
        <button type="button" class="btn btn-default new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="Acciones"><i class="fas fa-ellipsis-v fa-lg"></i></button>
            <ul class="dropdown-menu pull-right" role="menu">
                <li><a href="'. admin_url('payroll_electronic/see/$1/$2') .'"><i class="fa fa-eye"></i> ' . lang('payroll_management_see_details') . '</a></li>
                <li class="delete_electronic_payroll"><a href="'. admin_url('payroll_electronic/delete/$1') .'"><i class="fa fa-trash"></i> '. lang("delete") .'</a></li>
            </ul>
        </div>', "id, month");
        echo $this->datatables->generate();
    }

    public function see($id)
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $payrollElectronic = $this->Payroll_electronic_model->get_by_id($id);
        $months = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"];
        $title = lang("payroll_electronic_detail") . " <small>".$months[($payrollElectronic->month - 1)]."</small>";
        $this->data["electronic_payroll_id"] = $id;

        $this->page_construct("payroll_electronic/see", ["page_title" => $title], $this->data);
    }

    public function get_electronic_payroll_items_datatables($electronic_payrolll_id)
    {
        $this->load->library('datatables');
        $this->datatables->select($this->db->dbprefix("payroll_electronic_employee") .".id AS payroll_electronic_employee_id,
                                electronic_payroll_id,
                                employee_id,
                                creation_date,
                                reference_no,
                                (SELECT
                                    GROUP_CONCAT(pee1.reference_no)
                                FROM
                                    ". $this->db->dbprefix("payroll_adjustment_documents") ."
                                    INNER JOIN ". $this->db->dbprefix("payroll_electronic_employee") ." pee1 ON (pee1.id = ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_adjustment_id)
                                WHERE
                                    ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_id = ". $this->db->dbprefix("payroll_electronic_employee") .".id) AS support_document_adjustment,

                                (SELECT
                                    GROUP_CONCAT(pee1.reference_no)
                                FROM
                                    ". $this->db->dbprefix("payroll_adjustment_documents") ."
                                    INNER JOIN ". $this->db->dbprefix("payroll_electronic_employee") ." pee1 ON (pee1.id = ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_delete_id)
                                WHERE
                                    ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_id = ". $this->db->dbprefix("payroll_electronic_employee") .".id) AS support_document_elimination,

                                (SELECT
                                    GROUP_CONCAT(pee2.reference_no)
                                FROM
                                    ". $this->db->dbprefix("payroll_adjustment_documents") ."
                                    INNER JOIN ". $this->db->dbprefix("payroll_electronic_employee") ." pee2 ON pee2.id = ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_id
                                WHERE
                                    ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_adjustment_id = ". $this->db->dbprefix("payroll_electronic_employee") .".id) AS adjustment_document,

                                (SELECT
                                    GROUP_CONCAT(pee2.reference_no)
                                FROM
                                    ". $this->db->dbprefix("payroll_adjustment_documents") ."
                                    INNER JOIN ". $this->db->dbprefix("payroll_electronic_employee") ." pee2 ON pee2.id = ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_id
                                WHERE
                                    ". $this->db->dbprefix("payroll_adjustment_documents") .".electronic_payroll_delete_id = ". $this->db->dbprefix("payroll_electronic_employee") .".id) AS elimination_document,
                                LCASE(companies.name) AS employee_name,
                                earneds AS earned,
                                deductions AS deduction,
                                payment_total,
                                payroll_electronic_employee.status AS status,
                                type,
                                warnings,
                                contract_id");
        $this->datatables->from("payroll_electronic_employee");
        $this->datatables->join("companies companies", "companies.id = ". $this->db->dbprefix("payroll_electronic_employee") .".employee_id", "inner");
        $this->datatables->where("electronic_payroll_id", $electronic_payrolll_id);
        $this->datatables->add_column("Actions", '<div class="text-center">
            <div class="btn-group text-left">
                <button type="button" class="btn btn-default btn-outline new-button new-button-sm btn-xs dropdown-toggle" data-toggle="dropdown" data-toggle-second="tooltip" data-placement="top" title="'.lang('actions').'">
                    <i class="fas fa-ellipsis-v fa-lg"></i>
                </button>
                <ul class="dropdown-menu pull-right" role="menu">
                    <li class="see_employee_concepts">
                        <a href="'. admin_url('payroll_electronic/see_employee_concepts/$2/$3/$4') .'" data-toggle="modal" data-target="#myModal"><i class="fa fa-eye"></i> '. lang('payroll_management_see_details') .'</a>
                    </li>
                    <li class="divider"></li>
                    <li class="ne_previous_json">
                        <a href="'. admin_url('payroll_electronic/previous_json/$2/$3/$4') .'" target="_blank"><i class="fa fa-file-code-o"></i> '. lang('ne_previous_json') .'</a>
                    </li>
                    <li class="pay_slip">
                        <a href="'. admin_url('payroll_electronic/pay_slip/$2/$3/$4') .'" target="_blank"><i class="fa fa-file-pdf-o"></i> '. lang('pay_slip') .'</a>
                    </li>
                    <li class="send_electronic_payroll">
                        <a href="'. admin_url('payroll_electronic/send_electronic_payroll/$1/$2/$3/$4') .'"><i class="fa fa-paper-plane"></i> '. lang('send_electronic_payroll') .'</a>
                    </li>
                    <li class="adjustment_electronic_payroll_document">
                        <a href="'. admin_url('payroll_electronic/adjustment_electronic_payroll_document/$1/$2/$3/$4') .'"><i class="fa fa-pencil"></i> '. lang('adjustment_electronic_payroll_document') .'</a>
                    </li>
                    <li class="send_adjustment_electronic_payroll_document">
                        <a href="'. admin_url('payroll_electronic/send_adjustment_electronic_payroll_document/$1/$2/$3/$4') .'"><i class="fa fa-paper-plane"></i> '. lang('send_adjustment_electronic_payroll_document') .'</a>
                    </li>
                    <li class="nea_previous_json">
                        <a href="'. admin_url('payroll_electronic/nea_previous_json/$2/$3/$4') .'" target="_blank"><i class="fa fa-file-code-o"></i> '. lang('nea_previous_json') .'</a>
                    </li>
                    <li class="send_delete_electronic_payroll">
                        <a href="'. admin_url('payroll_electronic/send_delete_electronic_payroll_document/$1/$2/$3') .'"><i class="fa fa-paper-plane"></i> '. lang('send_delete_electronic_payroll') .'</a>
                    </li>
                    <li class="delete_electronic_payroll_document">
                        <a href="'. admin_url('payroll_electronic/delete_electronic_payroll_document/$1/$2/$3') .'"><i class="fa fa-trash"></i> '. lang('delete_electronic_payroll_document') .'</a>
                    </li>
                    <li class="nee_previous_json">
                        <a href="'. admin_url('payroll_electronic/nee_previous_json/$2/$3') .'" target="_blank"><i class="fa fa-file-code-o"></i> '. lang('nee_previous_json') .'</a>
                    </li>
                    <li class="check_document_status">
                        <a href="'. admin_url('payroll_electronic/check_document_status/$1/$2/$3/$4') .'"><i class="fa fa-search"></i> '. lang('check_document_status') .'</a>
                    </li>
                </ul>
            </div>', "electronic_payroll_id, payroll_electronic_employee_id, employee_id, contract_id");
        echo $this->datatables->generate();
    }

    public function see_employee_concepts(int $payroll_id, int $employee_id, int $contract_id)
    {
        $this->sma->checkPermissions('index', NULL, 'payroll_management');

        $this->data["payroll_id"] = $payroll_id;
        $this->data["employee_id"] = $employee_id;
        $this->data["page_title"] = lang("payroll_management_see_employee_concepts");
        $this->data["employee_data"] = $this->Employees_model->get_by_id($employee_id, $contract_id);

        $this->load_view($this->theme . "payroll_electronic/see_employee_concepts", $this->data);
    }

    public function get_employee_concepts_datatables($electronic_payroll_employee_id, $employee_id)
    {
        $this->load->library('datatables');
        $this->datatables->select("payroll_concepts.name,
                            IF({$this->db->dbprefix('payroll_electronic_items')}.concept_type_id = ".OVERTIME_OR_SURCHARGES.", hours_quantity, days_quantity) AS time_amount,
                            IF(earned_deduction = 1, amount , '') AS earned,
                            IF(earned_deduction = 2, amount , '') AS deduction,
                            payroll_electronic_items.concept_id,
                            IF({$this->db->dbprefix('payroll_electronic_items')}.concept_type_id = ".OVERTIME_OR_SURCHARGES.", hours_quantity, days_quantity) AS time_amount,
                            IF({$this->db->dbprefix('payroll_electronic_items')}.concept_type_id = ".OVERTIME_OR_SURCHARGES.", 'horas', 'días') AS time_unit,
                            payroll_electronic_items.paid_to_employee");
        $this->datatables->from("payroll_electronic_items");
        $this->datatables->join("payroll_concepts", "payroll_concepts.id = payroll_electronic_items.concept_id", "inner");
        $this->datatables->where(["payroll_electronic_items.electronic_payroll_employee_id" => $electronic_payroll_employee_id, "payroll_electronic_items.employee_id" => $employee_id]);
        $this->datatables->order_by("earned_deduction", "desc");
        $this->datatables->order_by("payroll_electronic_items.concept_id", "ASC");

        echo $this->datatables->generate();
    }

    public function send_electronic_payroll($electronic_payroll_id, $electronic_payroll_employee_id, $employee_id, $contract_id) // reutilizar este método
    {
        $this->sma->checkPermissions('send', NULL, 'payroll_management');

        $json = $this->create_json($electronic_payroll_employee_id, $employee_id, $contract_id);

        $response = $this->consume_web_services($json);

        $result = $this->management_response($response, $electronic_payroll_id, $electronic_payroll_employee_id);

        $this->redirect_reponse($result, $electronic_payroll_id);
    }

    private function create_json($electronic_payroll_employee_id, $employee_id, $contract_id)
    {
        $employee = $this->Employees_model->get_by_id($employee_id, $contract_id);
        $bank = $this->Payroll_electronic_model->get_company_by_id($employee->bank);
        $biller = $this->Payroll_electronic_model->get_company_by_id($employee->biller_id);
        $biller_state = $this->Payroll_electronic_model->get_state_data($biller->state);
        $electronic_payroll_employee = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_id($electronic_payroll_employee_id);
        $electronic_payroll = $this->Payroll_electronic_model->get_by_id($electronic_payroll_employee->electronic_payroll_id);
        $state = $this->Payroll_electronic_model->get_state_data($this->Settings->departamento);
        $electronic_payroll_items = $this->Payroll_electronic_model->get_items_by_electronic_payroll_employee_id($electronic_payroll_employee_id);
        $city = $this->Payroll_electronic_model->get_city_data($this->Settings->departamento, $this->Settings->ciudad);
        $generation_date = $electronic_payroll_employee->creation_date;
        $generation_time = $electronic_payroll_employee->creation_time."-05:00";
        $admission_date = $employee->start_date;
        $initial_settlement_date = date("Y-m-d", strtotime($electronic_payroll->year."-".$electronic_payroll->month."-01"));
        $final_settlement_date = date("Y-m-t", strtotime($electronic_payroll->year."-".$electronic_payroll->month));

        $reference_no = $electronic_payroll_employee->reference_no;
        $ref = explode("-", $reference_no);

        $data = [
            "Tipo" => "1",
            "Novedad" => [
                "Novedad" => "false",
                "CUNENov" => ""
            ],
            "Periodo" => [
                "FechaIngreso" => $admission_date,
                "FechaLiquidacionInicio" => $initial_settlement_date,
                "FechaLiquidacionFin" => $final_settlement_date,
                "TiempoLaborado" => $this->calculate_global_worked_days($admission_date, $final_settlement_date),
                "FechaGen"=>$generation_date
            ],
            "NumeroSecuenciaXML" => [
                "CodigoTrabajador" => $employee->employee_id,
                "Prefijo" => $ref[0],
                "Consecutivo" => $ref[1],
                "Numero" => $ref[0].$ref[1]
            ],
            "LugarGeneracionXML" => [
                "Pais"=> $this->Settings->codigo_iso,
                "DepartamentoEstado"=> substr($state->CODDEPARTAMENTO, -2),
                "MunicipioCiudad"=> substr($city->CODIGO, -5),
                "Idioma"=> "es"
            ],
            "InformacionGeneral" => [
                "Version" => "V1.0: Documento Soporte de Pago de Nómina Electrónica",
                "Ambiente" => $this->Payroll_settings->work_environment,
                "TipoXML" => "102",
                "FechaGen" => $generation_date,
                "HoraGen" => $generation_time,
                "PeriodoNomina" => $this->Payroll_settings->payment_frequency,
                "TipoMoneda" => "COP"
            ],
            "Empleador" => [
                "RazonSocial" => $this->Settings->razon_social,
                "NIT" => $this->Settings->numero_documento,
                "DV" => $this->Settings->digito_verificacion,
                "Pais" => $this->Settings->codigo_iso,
                "DepartamentoEstado" => substr($state->CODDEPARTAMENTO, -2),
                "MunicipioCiudad" => substr($city->CODIGO, -5),
                "Direccion" => $this->Settings->direccion
            ],
            "Trabajador" => [
                "TipoTrabajador" => $employee->employee_type,
                "SubTipoTrabajador" => "00",
                "AltoRiesgoPension" => ($employee->retired_risk == YES) ? "true" : "false",
                "TipoDocumento" => $employee->document_code,
                "NumeroDocumento" => $employee->vat_no,
                "PrimerApellido" => $employee->first_lastname,
                "SegundoApellido" => $employee->second_lastname,
                "PrimerNombre" => $employee->first_name,
                "LugarTrabajoPais" => "CO",
                "LugarTrabajoDepartamentoEstado" => substr($biller_state->CODDEPARTAMENTO, -2),
                "LugarTrabajoMunicipioCiudad" => substr($biller->city_code, -5),
                "LugarTrabajoDireccion" => $biller->address,
                "SalarioIntegral" => ($employee->integral_salary == YES) ? "true" : "false",
                "TipoContrato" => $employee->contract_type,
                "Sueldo" => $this->sma->formatDecimal($employee->base_amount, 2)
            ],
            "Pago" => [
                "Forma" => "1",
                "Metodo" => $employee->payment_method
            ],
            "FechasPagos" => [
                "FechaPago" => [
                    $final_settlement_date
                ]
            ]
        ];

        if ($employee->settlement_date != "0000-00-00" || $employee->settlement_date != NULL) {
        } else {
            $data["Periodo"]["FechaRetiro"] = $employee->settlement_date;
        }

        if ($employee->payment_method == WIRE_TRANSFER) {
            $data["Pago"]["Banco"] = $bank->name;
            $data["Pago"]["TipoCuenta"] = ($employee->account_type == 1) ? lang("payroll_contracts_savings_account") : lang("payroll_contracts_current_account");
            $data["Pago"]["NumeroCuenta"] = $employee->account_no;
        }

        $total_earneds = 0;
        $total_deductions = 0;
        foreach ($electronic_payroll_items as $item) {
            if ($item->earned_deduction == EARNED) {
                if ($item->concept_id == SALARY) {
                    $data["Devengados"]["Basico"] = [
                        "DiasTrabajados" => $item->days_quantity,
                        "SueldoTrabajado" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == TRANSPORTATION_ALLOWANCE) {
                    $data["Devengados"]["Transporte"][] = [
                        "AuxilioTransporte" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == PER_DIEM_CONSTITUTES_SALARY) {
                    $data["Devengados"]["Transporte"][] = [
                        "ViaticoManuAlojS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == DAYTIME_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(DAYTIME_OVERTIME);
                    $data["Devengados"]["HEDs"]["HED"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == NIGHT_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(NIGHT_OVERTIME);
                    $data["Devengados"]["HENs"]["HEN"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == NIGHT_SURCHARGE) {
                    $concept = $this->Payroll_concepts_model->get_by_id(NIGHT_SURCHARGE);
                    $data["Devengados"]["HRNs"]["HRN"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_DAYTIME_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_DAYTIME_OVERTIME);
                    $data["Devengados"]["HEDDFs"]["HEDDF"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_DAYTIME_SURCHARGE) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_DAYTIME_SURCHARGE);
                    $data["Devengados"]["HRDDFs"]["HRDDF"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_NIGHT_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_NIGHT_OVERTIME);
                    $data["Devengados"]["HENDFs"]["HENDF"][]= [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_NIGHT_SURCHARGE) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_NIGHT_SURCHARGE);
                    $data["Devengados"]["HRNDFs"]["HRNDF"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == MATERNITY_LICENSE || $item->concept_id == PATERNITY_LICENSE) {
                    $data["Devengados"]["Licencias"]["LicenciaMP"][] = [
						"Cantidad" => $item->days_quantity,
						"Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == PAID_LICENSE || $item->concept_id == FAMILY_ACTIVITY_PERMIT || $item->concept_id == DUEL_LICENSE || $item->concept_id == PAID_PERMIT) {
                    $data["Devengados"]["Licencias"]["LicenciaR"][] = [
						"Cantidad" => $item->days_quantity,
						"Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == DISCIPLINARY_SUSPENSION || $item->concept_id == UNEXCUSED_ABSENTEEISM || $item->concept_id == NOT_PAID_LICENSE || $item->concept_id == NOT_PAID_PERMIT || $item->concept_id == SUNDAY_DISCOUNT) {
                    $data["Devengados"]["Licencias"]["LicenciaNR"][] = [
                        "FechaInicio" => date("Y-m-d", strtotime($item->start_date)),
                        "FechaFin" => date("Y-m-d", strtotime($item->end_date)),
						"Cantidad" => $item->days_quantity
                    ];
                }

                if ($item->concept_id == COMMON_DISABILITY || $item->concept_id == PROFESSIONAL_DISABILITY || $item->concept_id == WORK_DISABILITY) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Devengados"]["Incapacidades"]["Incapacidad"][] = [
						"Cantidad" => $item->days_quantity,
						"Tipo" => $concept->dian_code,
						"Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == COMMISSIONS) {
                    $data["Devengados"]["Comisiones"]["Comision"] = [
						$this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == BEARING_ALLOWANCE) {
                    $data["Devengados"]["Auxilios"]["Auxilio"][] = [
						"AuxilioNS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == TIME_VACATION) {
                    $data["Devengados"]["Vacaciones"]["VacacionesComunes"][] = [
                        "Cantidad" => $item->days_quantity,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                // if ($item->concept_id == MONEY_VACATION) {
                //     $data["Devengados"]["Vacaciones"]["VacacionesCompensadas"][] = [
                //         "Cantidad" => $item->days_quantity,
                //         "Pago" => $this->sma->formatDecimal($item->amount, 2)
                //     ];
                // }

                if ($item->concept_id == BONUSES_CONSTITUTING_SALARY) {
                    $data["Devengados"]["Bonificaciones"]["Bonificacion"][] = [
                        "BonificacionS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == BONUSES_NOT_CONSTITUTING_SALARY) {
                    $data["Devengados"]["Bonificaciones"]["Bonificacion"][] = [
                        "BonificacionNS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }
                
                if ($this->Payroll_settings->provision_options == 3) {
                    if ($item->concept_id == MONEY_VACATION) {
                        $data["Devengados"]["Vacaciones"]["VacacionesCompensadas"][] = [
                            "Cantidad" => $item->days_quantity,
                            "Pago" => $this->sma->formatDecimal($item->amount, 2)
                        ];
                    }

                    if ($item->concept_id == SERVICE_BONUS) {
                        $data["Devengados"]["Primas"] = [
                            "Cantidad" => $item->days_quantity,
                            "Pago" => $this->sma->formatDecimal($item->amount, 2),
                        ];
                    }

                    if ($item->concept_id == LAYOFFS) {
                        $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                        $data["Devengados"]["Cesantias"] = [
                            "Pago" => $this->sma->formatDecimal($item->amount, 2),
                        ];
                    }

                    if ($item->concept_id == LAYOFFS_INTERESTS) {
                        if (isset($data["Devengados"]["Cesantias"])) {
                            $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                            $data["Devengados"]["Cesantias"]["Porcentaje"] = $concept->percentage;
                            $data["Devengados"]["Cesantias"]["PagoIntereses"] = $this->sma->formatDecimal($item->amount, 2);
                        }
                    }
                } else {
                    if ($item->concept_id == MONEY_VACATION) {
                        if ($item->paid_to_employee == 1) {
                            $data["Devengados"]["Vacaciones"]["VacacionesCompensadas"][] = [
                                "Cantidad" => ($item->paid_to_employee == 1) ? $item->days_quantity : 0,
                                "Pago" => $this->sma->formatDecimal($item->amount, 2)
                            ];
                        }
                    }

                    if ($item->concept_id == SERVICE_BONUS) {
                        if ($item->paid_to_employee == 1) {
                            $data["Devengados"]["Primas"] = [
                                "Cantidad" => $item->days_quantity,
                                "Pago" => $this->sma->formatDecimal($item->amount, 2),
                            ];
                        }
                    }

                    if ($item->concept_id == LAYOFFS) {
                        if ($item->paid_to_employee == 1) {
                            $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                            $data["Devengados"]["Cesantias"] = [
                                "Pago" => (($item->paid_to_employee == 1) ? $this->sma->formatDecimal($item->amount, 2) : 0)
                            ];
                        }

                    }
                    
                    if ($item->concept_id == LAYOFFS_INTERESTS) {
                        $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                        if (isset($data["Devengados"]["Cesantias"])) {
                            $data["Devengados"]["Cesantias"]["Porcentaje"] = $concept->percentage;
                            $data["Devengados"]["Cesantias"]["PagoIntereses"] =  (($item->paid_to_employee == 1) ? $this->sma->formatDecimal($item->amount, 2) : 0);
                        } else {
                            $data["Devengados"]["Cesantias"] = [
                                "Pago" => 0,
                                "Porcentaje" => $concept->percentage,
                                "PagoIntereses" => (($item->paid_to_employee == 1) ? $this->sma->formatDecimal($item->amount, 2) : 0),
                            ];
                        }
                    }
                }

                if ($item->concept_id == OTHER_ACCRUED_CONSTITUENTS_SALARY || $item->concept_id == OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    if ($item->concept_id == OTHER_ACCRUED_CONSTITUENTS_SALARY) {
                        $data["Devengados"]["OtrosConceptos"]["OtroConcepto"][] = [
                            "DescripcionConcepto" => $item->description,
                            "ConceptoS" => $this->sma->formatDecimal($item->amount, 2),
                        ];
                    }

                    if ($item->concept_id == OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY) {
                        $data["Devengados"]["OtrosConceptos"]["OtroConcepto"][] = [
                            "DescripcionConcepto" => $item->description,
                            "ConceptoNS" => $this->sma->formatDecimal($item->amount, 2),
                        ];
                    }
                }

                if ($item->concept_id == ENDOWMENT) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Devengados"]["Dotacion"] = $this->sma->formatDecimal($item->amount, 2);
                }

                if ($item->concept_id == COMPENSATION) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Devengados"]["Indemnizacion"] = $this->sma->formatDecimal($item->amount, 2);
                }

                $total_earneds += $item->amount;
            }

            if ($item->earned_deduction == DEDUCTION) {
                if ($employee->employee_type == APPRENTICES_SENA_LECTIVA || $employee->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
                    $data["Deducciones"]["Salud"] = [
                        "Porcentaje" => "4.00",
                        "Deduccion" => $this->sma->formatDecimal(0, 2)
                    ];
                }

                if ($item->concept_id == HEALTH_RATE || $item->concept_id == HEALTH_RATE_LESS_THAN) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Deducciones"]["Salud"] = [
                        "Porcentaje" => $concept->percentage,
				        "Deduccion" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($employee->employee_type == PRE_PENSION_OF_ENTITY_IN_LIQUIDATION || $employee->employee_type == PRE_PENSIONED_WITH_VOLUNTARY_CONTRIBUTION_TO_HEALTH ||
                    $employee->employee_type == APPRENTICES_SENA_LECTIVA || $employee->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
                    $data["Deducciones"]["FondoPension"] = [
                        "Porcentaje" => "4.00",
                        "Deduccion" => $this->sma->formatDecimal(0, 2)
                    ];
                }

                if ($item->concept_id == PENSION_RATE || $item->concept_id == HIGH_RISK_PENSION_RATE) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Deducciones"]["FondoPension"] = [
                        "Porcentaje" => $concept->percentage,
                        "Deduccion" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == DRAFT_CREDIT) {
                    $data["Deducciones"]["Libranzas"]["Libranza"][] = [
                        "Descripcion" => $item->description,
				        "Deduccion" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_2 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_4 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_6 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_8 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_2) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Deducciones"]["FondoSP"] = [
                        "Porcentaje" => $concept->percentage,
                        "DeduccionSP" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == WITHHOLDING) {
                    $data["Deducciones"]["RetencionFuente"] = $this->sma->formatDecimal($item->amount, 2);
                }


                if ($item->concept_id == OTHER_DEDUCTIONS) {
                    $data["Deducciones"]["OtrasDeducciones"]["OtraDeduccion"][] = $this->sma->formatDecimal($item->amount, 2);
                }

                if ($item->concept_id == COMPANY_LOAN) {
                    $data["Deducciones"]["Deuda"] = $this->sma->formatDecimal($item->amount, 2);
                }

                if ($item->concept_id == FISCAL_SEIZURES) {
                    $data["Deducciones"]["EmbargoFiscal"] = $this->sma->formatDecimal($item->amount, 2);
                }

                $total_deductions += $item->amount;
            }
        }

        if (!isset($data['Deducciones'])) {
            if ($employee->employee_type == APPRENTICES_SENA_LECTIVA || $employee->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
                $data["Deducciones"]["Salud"] = [
                    "Porcentaje" => "4.00",
                    "Deduccion" => $this->sma->formatDecimal(0, 2)
                ];
            }

            if ($employee->employee_type == PRE_PENSION_OF_ENTITY_IN_LIQUIDATION || $employee->employee_type == PRE_PENSIONED_WITH_VOLUNTARY_CONTRIBUTION_TO_HEALTH ||
                $employee->employee_type == APPRENTICES_SENA_LECTIVA || $employee->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
                $data["Deducciones"]["FondoPension"] = [
                    "Porcentaje" => "4.00",
                    "Deduccion" => $this->sma->formatDecimal(0, 2)
                ];
            }
        }

        $data["DevengadosTotal"] = strval($this->sma->formatDecimal($total_earneds, 2));
        $data["DeduccionesTotal"] = strval($this->sma->formatDecimal($total_deductions, 2));
        $data["ComprobanteTotal"] = $this->sma->formatDecimal(($total_earneds - $total_deductions), 2);

        return $data;
    }

    private function consume_web_services($json)
    {
        if ($this->Payroll_settings->work_environment == TEST) {
            $http_header = [
                'Content-Type: application/json',
                'nominaAuthorizationToken: e71ddfbe-9b39-4fb9-afd7-875c02c8a6ec',
                'nitAlianza: 901090070',
                'Set-Test-Id: '. $this->Payroll_settings->set_test_id
            ];
            $url = 'https://apine.efacturacadena.com/staging/ne/documentos/proceso/habilitacion';
        } else {
            $http_header = [
                'Content-Type: application/json',
                'nominaAuthorizationToken: 4b71310c-ebd7-42cb-9e51-293680850bbe',
                'nitAlianza: 901090070'
            ];
            // $url = 'https://apine.efacturacadena.com/staging/ne/documentos/proceso/sincrono';
            $url = 'https://apine.efacturacadena.com/v1/ne/documentos/proceso/sincrono';
        }

        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS => json_encode($json, JSON_UNESCAPED_UNICODE),
            CURLOPT_HTTPHEADER => $http_header
        ]);

        $responseCURL = curl_exec($curl);

        log_message('debug', 'CADENA NE - Envio: '. $responseCURL);

        $response = json_decode($responseCURL);
        curl_close($curl);

        return $response;
    }

    private function management_response($response, $electronic_payroll_id, $electronic_payroll_employee_id) 
    {
        if ($response->statusCode == "200") {
            $data["statusCode"] = $response->statusCode;
            $data["trackId"] = $response->trackId;
            $data["cune"] = $response->cune;
            $data["statusMessage"] = $response->statusMessage;
            $data["statusDescription"] = $response->statusDescription;
            $data["status"] = ACCEPTED;
            $data["generation_date"] = date("Y-m-d");
            $data["errorMessage"] = "";
            $data["errorReason"] = "";
            if (!empty($response->warnings)) {
                $warnings_data = "";
                foreach ($response->warnings as $warnings) {
                    $warnings_data .= $warnings ."\n";
                }
            }
            $data["warnings"] = $warnings_data;
        } else if ($response->statusCode == "400" || $response->statusCode == "401" || $response->statusCode == "504") {
            $data["statusCode"] = $response->statusCode;
            $data["errorMessage"] = $response->errorMessage;
            $data["errorReason"] = $response->errorReason;
            $data["statusMessage"] = "";
            $data["statusDescription"] = "";
            $data["status"] = PENDING;
        } else if ($response->statusCode == "409") {
            $data["statusCode"] = $response->statusCode;
            $data["statusMessage"] = $response->statusMessage;
            $data["statusDescription"] = $response->statusDescription;
            $data["errorMessage"] = "";
            $data["errorReason"] = "";
            if (!empty($response->warnings)) {
                $warnings_data = "";
                foreach ($response->warnings as $warnings) {
                    $warnings_data .= $warnings ."\n";
                }
            }
            $data["warnings"] = $warnings_data;
            $data["status"] = PENDING;
        // } else if ($response->statusCode == "500") {
        } else {
            $data["statusCode"] = $response->statusCode;
            $data["trackId"] = $response->trackId;
            $data["cune"] = $response->cune;
            $data["statusMessage"] = $response->statusMessage;
            $data["statusDescription"] = $response->statusDescription;
            $data["errorMessage"] = "";
            $data["errorReason"] = "";
            if (!empty($response->warnings)) {
                $warnings_data = "";
                foreach ($response->warnings as $warnings) {
                    $warnings_data .= $warnings ."\n";
                }
            }
            $data["warnings"] = $warnings_data;
            $data["status"] = PENDING;
        }

        if ($this->Payroll_electronic_model->update_employee_items($data, $electronic_payroll_employee_id)) {
            $this->change_header_state($electronic_payroll_id);
        }

        return $data;
    }

    private function redirect_reponse($result, $electronic_payroll_id)
    {
        if ($result["statusCode"] == "200") {
            $this->session->set_flashdata("message", $result["statusMessage"]);
        } else {
            if ($result["statusCode"] == "409" || $result["statusCode"] == "500") {
                $this->session->set_flashdata("error", $result["statusMessage"]);
            } else {
                $this->session->set_flashdata("error", $result["errorMessage"]);
            }
        }

        admin_redirect('payroll_electronic/see/'.$electronic_payroll_id);
    }

    public function previous_json($electronic_payroll_employee_id, $employee_id, $contract_id)
    {
        $json = $this->create_json($electronic_payroll_employee_id, $employee_id, $contract_id, FALSE);
        echo '<pre>';
            print_r(json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        echo '</pre>';
    }

    private function change_header_state($electronic_payroll_id)
    {
        // $pending = 0;
        $accepted = TRUE;
        $electronic_payroll_employees = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_electronic_payroll_id($electronic_payroll_id);
        foreach ($electronic_payroll_employees as $payroll_employee) {
            // if ($payroll_employee->status != NOT_SENT) {
            //     $pending++;
            // }

            if ($payroll_employee->status != ACCEPTED) {
                $accepted = FALSE;
            }
        }

        if (/* $pending > 0 &&  */$accepted == TRUE) {
            $data["status"] = ACCEPTED;
        } if (/* $pending > 0 &&  */$accepted == FALSE) {
            $data["status"] = PENDING;
        }

        $this->Payroll_electronic_model->update($data, $electronic_payroll_id);
    }

    public function calculate_global_worked_days($start_date, $end_date)
    {
        $date1 = new DateTime(date("Y-m-d", strtotime($start_date)));
        $date2 = new DateTime(date("Y-m-d", strtotime($end_date)));
        $diff = $date1->diff($date2);

        return strval(($diff->y * 360) + ($diff->m * 30) + $diff->d);
    }

    public function get_electronic_payroll_information()
    {
        $id = $this->input->get("id");
        $electronic_payroll_employee = $this->Payroll_electronic_model->get_employee_items($id);
        $warnings = (!empty($electronic_payroll_employee->warnings)) ? " ". $electronic_payroll_employee->warnings : "";

        if ($electronic_payroll_employee->statusCode == "200" || $electronic_payroll_employee->statusCode == "409" || $electronic_payroll_employee->statusCode == "500") {
            $response = [
                "statusCode" => $electronic_payroll_employee->statusCode,
                "messageTitle" => $electronic_payroll_employee->statusMessage,
                "messageDescription" => $electronic_payroll_employee->statusDescription . $warnings,
            ];
        } else {
            $response = [
                "statusCode" => $electronic_payroll_employee->statusCode,
                "messageTitle" => $electronic_payroll_employee->errorMessage,
                "messageDescription" => $electronic_payroll_employee->errorReason . $warnings,
            ];
        }

        echo json_encode($response);
    }

    public function delete($id)
    {
        $this->sma->checkPermissions('delete', NULL, 'payroll_management');

        $electronic_payroll = $this->Payroll_electronic_model->get_by_id($id);
        $month = $electronic_payroll->month;
        $year = $electronic_payroll->year;
        $biller_id = $electronic_payroll->biller_id;

        if ($this->delete_electronic_payroll_employee($id, $biller_id)) {
            if ($this->Payroll_electronic_model->delete($id) == TRUE) {
                $this->disapprove_payroll($year, $month, $biller_id);
            }

            $this->session->set_flashdata("message", "Nómina electrónica eliminada.");
        }

        redirect($_SERVER["HTTP_REFERER"]);
    }

    private function delete_electronic_payroll_employee($electronic_payroll_id, $biller_id)
    {
        $support_documents = [];
        $electronic_payroll_ids = [];
        $electronic_payroll_employees = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_electronic_payroll_id($electronic_payroll_id);
        foreach ($electronic_payroll_employees as $electronic_payroll_employee) {
            if (!in_array($electronic_payroll_employee->id, $electronic_payroll_ids)) {
                array_push($electronic_payroll_ids, $electronic_payroll_employee->id);
            }
            if ($electronic_payroll_employee->type == SUPPORT_DOCUMENT) {
                array_push($support_documents, $electronic_payroll_employee);
            }
        }

        $this->delete_relationships_between_documents($electronic_payroll_ids);

        if ($this->delete_electronic_payroll_items($electronic_payroll_ids) == TRUE) {
            if ($this->Payroll_electronic_model->delete_electronic_payroll_employee_by_electronic_payroll_id($electronic_payroll_id) == TRUE) {
                $number_electronic_payroll_employees = count($support_documents);
                $this->reverse_consecutive($biller_id, $number_electronic_payroll_employees);
                return TRUE;
            }
            return FALSE;
        }
        return FALSE;
    }

    private function delete_electronic_payroll_items($electronic_payroll_ids)
    {
        if($this->Payroll_electronic_model->delete_electronic_payroll_items_by_electronic_payroll_employee_ids($electronic_payroll_ids) == TRUE) {
            return TRUE;
        }
        return FALSE;
    }

    private function delete_relationships_between_documents($electronic_payroll_ids)
    {
        if ($this->Payroll_electronic_model->delete_relationships_between_documents($electronic_payroll_ids)) {
            return TRUE;
        }
        return FALSE;
    }

    private function disapprove_payroll($year, $month, $biller_id)
    {
        $this->sma->checkPermissions('approve', NULL, 'payroll_management');

        $payroll = $this->Payroll_management_model->get_last_payroll($year, $month, NULL, $biller_id);
        if ($this->Payroll_management_model->update(['status' => IN_PREPARATION], $payroll->id) == TRUE) {
            $this->un_apply_future_elements($payroll->id);
        }
    }

    private function un_apply_future_elements($payroll_id)
    {
        $future_items_ids = [];
        $payroll_items = $this->Payroll_management_model->get_payroll_items($payroll_id);
        foreach ($payroll_items as $item) {
            if (!empty($item->future_item_id) && !in_array($item->future_item_id, $future_items_ids)) {
                $future_items_ids[] = $item->future_item_id;
            }
        }

        if (!empty($future_items_ids)) {
            foreach ($future_items_ids as $future_item_id) {
                $this->Payroll_management_model->update_future_item(['applied' => NOT], $future_item_id);
            }
        }
    }

    private function reverse_consecutive($biller_id, $number_electronic_payroll_employees)
    {
        $document_type = $this->Payroll_electronic_model->get_document_types_by_biller_id(43, $biller_id);
        $new_consecutive = ($document_type->sales_consecutive - $number_electronic_payroll_employees);
        $new_consecutive = ($new_consecutive <= 0) ? 1: $new_consecutive;

        if ($this->Payroll_electronic_model->update_document_type(["sales_consecutive"=>$new_consecutive], $document_type->id)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_electronic_payroll_document($electronic_payroll_id, $reference_electronic_payroll_employee_id, $employee_id)
    {
        $reference_electronic_payroll = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_id($reference_electronic_payroll_employee_id);
        $electronic_payroll_id = $reference_electronic_payroll->electronic_payroll_id;

        $document_type = $this->Payroll_electronic_model->get_document_types_by_biller_id(45, $reference_electronic_payroll->biller_id);
        if ($document_type == FALSE) {
            $this->session->set_flashdata("error", "No se encuentra asociado prefijos para eliminación de nómina electrónica.");
            redirect($_SERVER["HTTP_REFERER"]);
        }

        $data = [
            "electronic_payroll_id" => $electronic_payroll_id,
            "creation_date" => date("Y-m-d"),
            "creation_time" => date("H:i:s"),
            "biller_id" => $reference_electronic_payroll->biller_id,
            "employee_id" => $reference_electronic_payroll->employee_id,
            "contract_id" => $reference_electronic_payroll->contract_id,
            "earneds" => -abs($reference_electronic_payroll->earneds),
            "deductions" => -abs($reference_electronic_payroll->deductions),
            "payment_total" => -abs($reference_electronic_payroll->payment_total),
            "type" => ELIMINATION_DOCUMENT
        ];
        $electronic_payroll_employee_id = $this->Payroll_electronic_model->insert_employees($data, ELIMINATION_DOCUMENT);
        if ($electronic_payroll_employee_id != FALSE) {
            $data_elimination = [
                "electronic_payroll_id" => $reference_electronic_payroll->id,
                "electronic_payroll_delete_id" => $electronic_payroll_employee_id
            ];
            $elimination_saved = $this->Payroll_electronic_model->insert_documents_adjustment($data_elimination);
            if ($elimination_saved == TRUE) {
                $this->send_delete_electronic_payroll_document($electronic_payroll_id, $electronic_payroll_employee_id, $employee_id);
            }
        } else {
            $this->session->set_flashdata("error", "El documento de eliminación no pudo ser creado");
            redirect($_SERVER["HTTP_REFERER"]);
        }
    }

    public function send_delete_electronic_payroll_document($electronic_payroll_id, $electronic_payroll_employee_id, $employee_id)
    {
        $this->sma->checkPermissions('send', NULL, 'payroll_management');

        $json = $this->create_delete_json($electronic_payroll_employee_id);

        $response = $this->consume_web_services($json);

        $result = $this->management_response($response, $electronic_payroll_id, $electronic_payroll_employee_id);

        $this->redirect_reponse($result, $electronic_payroll_id);
    }

    private function create_delete_json($electronic_payroll_employee_id)
    {
        $delete_electronic_payroll = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_id($electronic_payroll_employee_id);
        $electronic_payroll = $this->Payroll_electronic_model->get_support_documents_by_electronic_payroll_delete_id($electronic_payroll_employee_id);
        $reference_no_electronic_payroll = explode("-", $electronic_payroll->reference_no);
        $reference_no_delete_electronic_payroll = explode("-", $delete_electronic_payroll->reference_no);
        $state = $this->Payroll_electronic_model->get_state_data($this->Settings->departamento);
        $city = $this->Payroll_electronic_model->get_city_data($this->Settings->departamento, $this->Settings->ciudad);
        $generation_date = date("Y-m-d");
        $generation_time = date("H:i:s-05:00");

        $data = [
            "Tipo" => "2",
            "TipoNota" => "2",
            "Eliminar" => [
                "EliminandoPredecesor" => [
                    "NumeroPred" => $reference_no_electronic_payroll[0].$reference_no_electronic_payroll[1],
                    "CUNEPred" => $electronic_payroll->cune,
                    "FechaGenPred" => $electronic_payroll->creation_date
                ],
                "NumeroSecuenciaXML" => [
                    "Prefijo" => $reference_no_delete_electronic_payroll[0],
                    "Consecutivo" => $reference_no_delete_electronic_payroll[1],
                    "Numero" => $reference_no_delete_electronic_payroll[0].$reference_no_delete_electronic_payroll[1],
                ],
                "LugarGeneracionXML" => [
                    "Pais"=> $this->Settings->codigo_iso,
                    "DepartamentoEstado"=> substr($state->CODDEPARTAMENTO, -2),
                    "MunicipioCiudad"=> substr($city->CODIGO, -5),
                    "Idioma"=> "es"
                ],
                "InformacionGeneral" => [
                    "Version" => "V1.0: Nota de Ajuste de Documento Soporte de Pago de Nómina Electrónica",
                    "Ambiente" => $this->Payroll_settings->work_environment,
                    "TipoXML" => "103",
                    "FechaGen" => $generation_date,
                    "HoraGen" => $generation_time
                ],
                "Empleador" => [
                    "RazonSocial" => $this->Settings->razon_social,
                    "NIT" => $this->Settings->numero_documento,
                    "DV" => $this->Settings->digito_verificacion,
                    "Pais" => $this->Settings->codigo_iso,
                    "DepartamentoEstado" => substr($state->CODDEPARTAMENTO, -2),
                    "MunicipioCiudad" => substr($city->CODIGO, -5),
                    "Direccion" => $this->Settings->direccion
                ],
            ]
        ];

        return $data;
    }

    public function nee_previous_json($electronic_payroll_employee_id, $employee_id)
    {
        $json = $this->create_delete_json($electronic_payroll_employee_id, $employee_id);
        echo '<pre>';
            print_r(json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        echo '</pre>';
    }

    public function adjustment_electronic_payroll_document($electronic_payroll_id, $electronic_payroll_employee_id, $employee_id, $contract_id)
    {
        // $relations_document_electronic_payroll = $this->Payroll_electronic_model->get_adjustment_document_by_electronic_payroll_id($electronic_payroll_employee_id);
        // if ($relations_document_electronic_payroll !== FALSE) {
        //     $this->session->set_flashdata("error", "La nómina ya se encuentra asociada a una documento de ajuste");
        //     redirect($_SERVER["HTTP_REFERER"]);
        // }

        $electronic_payroll = $this->Payroll_electronic_model->get_by_id($electronic_payroll_id);
        $grouped_payroll = $this->Payroll_management_model->get_payroll_grouped_by_month($electronic_payroll->year, $electronic_payroll->month);

        $this->data["employee_id"] = $employee_id;
        $this->data["contract_id"] = $contract_id;
        $this->data["grouped_payroll"] = $grouped_payroll;
        $this->data["electronic_payroll"] = $electronic_payroll;
        $this->data["electronic_payroll_employee_id"] = $electronic_payroll_employee_id;
        $this->page_construct("payroll_electronic/adjusment", ["page_title" => lang("payroll_adjustment")], $this->data);
    }

    public function get_payroll_items_for_adjusment_datatables($year, $month, $employee_id, $contract_id)
    {
        $grouped_payroll = $this->Payroll_management_model->get_payroll_grouped_by_month($year, $month);

        $this->load->library('datatables');
        $this->datatables->select("payroll_items.id AS id,
                                payroll.month,
                                payroll.number,
                                payroll_concepts.name,
                                payroll_items.description,
                                IF(payroll_concept_type = ".OVERTIME_OR_SURCHARGES.", hours_quantity, days_quantity) AS time_amount,
                                IF(payroll_concept_type = ".OVERTIME_OR_SURCHARGES.", 'horas', 'días') AS time_unit,
                                IF(earned_deduction = 1, amount , '') AS earned,
                                IF(earned_deduction = 2, amount , '') AS deduction,
                                payroll_concept_type.automatic");
        $this->datatables->from("payroll_items");
        $this->datatables->join("payroll_concepts", "payroll_concepts.id = payroll_items.payroll_concept_id", "inner");
        $this->datatables->join("payroll_concept_type", "payroll_concept_type.id = payroll_concepts.concept_type_id", "inner");
        $this->datatables->join("payroll", "payroll.id = payroll_items.payroll_id", "inner");
        $this->datatables->where("payroll_items.employee_id", $employee_id);
        $this->datatables->where("payroll_items.contract_id", $contract_id);
        $this->datatables->where("payroll_items.payroll_id IN (". $grouped_payroll->ids . ")");
        $this->datatables->where_not_in("payroll_items.payroll_concept_type", [ORDINARY, TRANSPORTATION_ALLOWANCE_TYPE, DEDUCTIONS_BY_LAW]);
        $this->datatables->order_by("earned_deduction", "DESC");
        $this->datatables->order_by("payroll_items.payroll_concept_id", "ASC");
        $this->datatables->add_column("Actions", '<div class="text-center"><div class="btn-group text-left">
                                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">'. lang("actions") . '<span class="caret"></span></button>
                                                        <ul class="dropdown-menu pull-right" role="menu">
                                                            <li class="remove_novelty">
                                                                <a href="'. admin_url('payroll_management/remove_novetly/$1') .'"><i class="fa fa-trash"></i> '. lang('delete') .'</a>
                                                            </li>
                                                        </ul>
                                                  </div>', "id");
        echo $this->datatables->generate();
    }

    public function add_novetly($payroll_id, $employee_id, $contract_id)
    {
        $this->sma->checkPermissions('add_novelty', NULL, 'payroll_management');

        $employee = $this->Employees_model->get_by_id($employee_id, $contract_id);
        $this->data["payroll_id"] = $payroll_id;
        $this->data['employee_data'] = $employee;
        $this->data["page_title"] = lang("add_concept");
        $this->data["concepts"] = $this->Payroll_concepts_model->get_not_automatic();
        $this->data["payroll_data"] = $this->Payroll_management_model->get_by_id($payroll_id);
        $this->data["contract_data"] = $this->Payroll_contracts_model->get_by_id($employee->contract_id);

        $this->load_view($this->theme . "payroll_electronic/add_novetly", $this->data);
    }

    public function save_adjustment($electronic_payroll_id, $electronic_payroll_employee_id, $contract_id)
    {
        $this->sma->checkPermissions('add_novelty', NULL, 'payroll_management');

        $electronic_payroll = $this->Payroll_electronic_model->get_by_id($electronic_payroll_id);
        $electronic_payroll_employee = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_id($electronic_payroll_employee_id);
        $grouped_payroll = $this->Payroll_management_model->get_payroll_grouped_by_month($electronic_payroll->year, $electronic_payroll->month);
        $payroll_items_data = $this->Payroll_management_model->get_payroll_items_by_payroll_id($grouped_payroll->ids, $electronic_payroll_employee->employee_id, $contract_id);
        $biller_id = $electronic_payroll_employee->biller_id;
        $employees = [];

        foreach ($payroll_items_data as $item) { $employees[$item->employee_id][] = $item; }

        foreach ($employees as $employee_id => $employee_items) {
            $earneds = 0;
            $deductions = 0;
            foreach ($employee_items as $item) {
                if ($item->earned_deduction == EARNED) {
                    $earneds += $item->amount;
                } else {
                    $deductions += $item->amount;
                }
            }
            $total_payment = $earneds - $deductions;

            $data = [
                "electronic_payroll_id" => $electronic_payroll_id,
                "creation_date" => date("Y-m-d"),
                "creation_time" => date("H:i:s"),
                "biller_id" => $biller_id,
                "employee_id" => $employee_id,
                "contract_id" => $contract_id,
                "earneds" => $earneds,
                "deductions" => $deductions,
                "payment_total" => $total_payment,
                "status" => NOT_SENT,
                "type" => ADJUSTMENT_DOCUMENT
            ];

            $electronic_payroll_employee_adjustment_id = $this->Payroll_electronic_model->insert_employees($data, ADJUSTMENT_DOCUMENT);
            if ($electronic_payroll_employee_adjustment_id != FALSE) {
                if ($this->generates_electronic_payroll_items_data($electronic_payroll_employee_adjustment_id, $employee_items, $biller_id) == TRUE) {
                    $data_adjusment = [
                        "electronic_payroll_id" => $electronic_payroll_employee_id,
                        "electronic_payroll_adjustment_id" => $electronic_payroll_employee_adjustment_id
                    ];
                    $adjustment_saved = $this->Payroll_electronic_model->insert_documents_adjustment($data_adjusment);
                    if ($adjustment_saved == TRUE) {
                        $this->send_adjustment_electronic_payroll_document($electronic_payroll_id, $electronic_payroll_employee_adjustment_id, $employee_id, $contract_id);
                    }
                }
            }
        }
    }

    private function generates_electronic_payroll_items_data($electronic_payroll_employee_id, $employee_items_data, $biller_id)
    {
        $salary = 0;
        $pension_rate = 0;
        $hr_pension_rate = 0;
        $days_quantity_ta = 0;
        $bearing_allowance = 0;
        $days_quantity_salary = 0;
        $days_quantity_health = 0;
        $health_rate_less_than = 0;
        $days_quantity_pension = 0;
        $transportation_allowance = 0;
        $days_quantity_high_risk_pension = 0;

        $per_diem_constitutes_salary = $days_quantity_pdcs = 0;

        foreach ($employee_items_data as $item) {
            $employee_id = $item->employee_id;
            $concept_id = $item->payroll_concept_id;
            $concept_type_id = $item->payroll_concept_type;
            if ($concept_type_id == BONUSES_CONSTITUTING_SALARY || $concept_type_id == SALARY_PAYMENTS ||
                $concept_type_id == PAID_LICENSE_TYPE || $concept_type_id == OVERTIME_OR_SURCHARGES ||
                $concept_type_id == DISABILITIES || $concept_type_id == NOT_PAID_LICENSE_TYPE ||
                $concept_type_id == NON_SALARY_PAYMENTS || $concept_type_id == FOOD_BOND_TYPE ||
                $concept_type_id == BUSINESS_LOANS || $concept_type_id == TIME_VACATION_TYPE ||
                $concept_type_id == MONEY_VACATION_TYPE || $concept_type_id == SERVICE_BONUS_TYPE ||
                $concept_type_id == LAYOFFS_TYPE) {
                $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $item->amount, $concept_id, $concept_type_id, $item->earned_deduction, $biller_id, $item->days_quantity, $item->hours_quantity,  $item->start_date, $item->end_date, $item->description, $item->paid_to_employee);
            } else {
                if ($concept_id == SALARY) {
                    $salary += $item->amount;
                    $concept_id_salary = $concept_id;
                    $concept_type_id_salary = $concept_type_id;
                    $days_quantity_salary += $item->days_quantity;
                }

                if ($concept_id == TRANSPORTATION_ALLOWANCE) {
                    $concept_id_ta = $concept_id;
                    $concept_type_id_ta = $concept_type_id;
                    $transportation_allowance += $item->amount;
                    $days_quantity_ta += $item->days_quantity;
                }

                if ($concept_id == PER_DIEM_CONSTITUTES_SALARY) {
                    $concept_id_pdcs = $concept_id;
                    $concept_type_id_pdcs = $concept_type_id;
                    $per_diem_constitutes_salary += $item->amount;
                    $days_quantity_pdcs += $item->days_quantity;
                }

                if ($concept_id == HEALTH_RATE_LESS_THAN || $concept_id == HEALTH_RATE) {
                    $concept_id_hrlt = $concept_id;
                    $concept_type_id_hrlt = $concept_type_id;
                    $health_rate_less_than += $item->amount;
                    $days_quantity_health += $item->days_quantity;
                }

                if ($concept_id == PENSION_RATE) {
                    $concept_id_pr = $concept_id;
                    $concept_type_id_pr = $concept_type_id;
                    $pension_rate += $item->amount;
                    $days_quantity_pension += $item->days_quantity;
                }

                if ($concept_id == HIGH_RISK_PENSION_RATE) {
                    $concept_id_hrpr = $concept_id;
                    $concept_type_id_hrpr = $concept_type_id;
                    $hr_pension_rate += $item->amount;
                    $days_quantity_high_risk_pension += $item->days_quantity;
                }

                if ($concept_id == BEARING_ALLOWANCE) {
                    $concept_id_ba = $concept_id;
                    $concept_type_id_ba = $concept_type_id;
                    $bearing_allowance += $item->amount;
                }

                if ($concept_id == FISCAL_SEIZURES) {
                    $concept_id_fs = $concept_id;
                    $concept_type_id_fs = $concept_type_id;
                    $fiscal_seizures += $item->amount;
                }
            }
        }

        if ($salary >= 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $salary, $concept_id_salary, $concept_type_id_salary, EARNED, $biller_id, $days_quantity_salary);
        }

        if (!empty($transportation_allowance)) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $transportation_allowance, $concept_id_ta, $concept_type_id_ta, EARNED, $biller_id, $days_quantity_ta);
        }

        if ($per_diem_constitutes_salary > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $per_diem_constitutes_salary , $concept_id_pdcs, $concept_type_id_pdcs, EARNED, $biller_id, $days_quantity_pdcs);
        }

        if ($health_rate_less_than > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $health_rate_less_than, $concept_id_hrlt, $concept_type_id_hrlt, DEDUCTION, $biller_id, $days_quantity_health);
        }

        if ($pension_rate > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $pension_rate, $concept_id_pr, $concept_type_id_pr, DEDUCTION, $biller_id, $days_quantity_pension);
        }

        if ($hr_pension_rate > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $hr_pension_rate, $concept_id_hrpr, $concept_type_id_hrpr, DEDUCTION, $biller_id, $days_quantity_high_risk_pension);
        }

        if ($bearing_allowance > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $bearing_allowance, $concept_id_ba, $concept_type_id_ba, EARNED, $biller_id);
        }

        if ($fiscal_seizures > 0) {
            $data[] = $this->get_electronic_payroll_concept_data($electronic_payroll_employee_id, $employee_id, $fiscal_seizures, $concept_id_fs, $concept_type_id_fs, DEDUCTION, $biller_id);
        }

        if(!empty($data)) {
            $items_saved = $this->Payroll_electronic_model->insert_items($data);
            if ($items_saved == TRUE) {
                return TRUE;
            }
            return FALSE;
        }

        return FALSE;
    }

    private function get_electronic_payroll_concept_data($id, $employee_id, $amount, $concept_id, $concept_type_id, $earned_deduction, $biller_id, $days_quantity = NULL, $hours_quantity = NULL, $start_date = NULL, $end_date = NULL, $description = NULL, $paid_to_employee = 0)
    {
        $data = [
            "creation_date" => date("Y-m-d"),
            "electronic_payroll_employee_id" => $id,
            "employee_id" => $employee_id,
            "concept_type_id" => $concept_type_id,
            "concept_id" => $concept_id,
            "earned_deduction" => $earned_deduction,
            "amount" => $amount,
            "start_date" => (!empty($start_date)) ? $start_date : "0000-00-00",
            "end_date" => (!empty($end_date)) ? $end_date : "0000-00-00",
            "days_quantity" => (!empty($days_quantity)) ? $days_quantity : 0,
            "hours_quantity" => (!empty($hours_quantity)) ? $hours_quantity : 0.00,
            "biller_id" => $biller_id
        ];

        return $data;
    }

    public function send_adjustment_electronic_payroll_document($electronic_payroll_id, $electronic_payroll_employee_id, $employee_id, $contract_id)
    {
        $this->sma->checkPermissions('send', NULL, 'payroll_management');

        $json = $this->create_adjustmet_json($electronic_payroll_employee_id, $employee_id, $contract_id);

        $response = $this->consume_web_services($json);

        $result = $this->management_response($response, $electronic_payroll_id, $electronic_payroll_employee_id);

        $this->redirect_reponse($result, $electronic_payroll_id);
    }

    public function nea_previous_json($electronic_payroll_employee_id, $employee_id, $contract_id)
    {
        $json = $this->create_adjustmet_json($electronic_payroll_employee_id, $employee_id, $contract_id);
        echo '<pre>';
            print_r(json_encode($json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
        echo '</pre>';
    }

    public function create_adjustmet_json($electronic_payroll_employee_id, $employee_id, $contract_id)
    {
        $this->sma->checkPermissions('add', NULL, 'payroll_management');

        $employee = $this->Employees_model->get_by_id($employee_id, $contract_id);
        $bank = $this->Payroll_electronic_model->get_company_by_id($employee->bank);
        $biller = $this->Payroll_electronic_model->get_company_by_id($employee->biller_id);
        $biller_state = $this->Payroll_electronic_model->get_state_data($biller->state);
        $electronic_payroll_employee = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_id($electronic_payroll_employee_id);
        $electronic_payroll = $this->Payroll_electronic_model->get_by_id($electronic_payroll_employee->electronic_payroll_id);
        $state = $this->Payroll_electronic_model->get_state_data($this->Settings->departamento);
        $electronic_payroll_items = $this->Payroll_electronic_model->get_items_by_electronic_payroll_employee_id($electronic_payroll_employee_id);
        // $biller_city = $this->Payroll_electronic_model->get_city_data($biller->state, $biller->city);
        $city = $this->Payroll_electronic_model->get_city_data($this->Settings->departamento, $this->Settings->ciudad);
        $electronic_payroll_adjustment = $this->Payroll_electronic_model->get_support_documents_by_electronic_payroll_adjustment_id($electronic_payroll_employee_id);
        $reference_no_electronic_payroll = explode("-", $electronic_payroll_adjustment->reference_no);

        $generation_date = $electronic_payroll_employee->creation_date;
        $admission_date = $employee->start_date;
        $generation_time = date("H:i:s-05:00");
        $initial_settlement_date = date("Y-m-d", strtotime($electronic_payroll->year."-".$electronic_payroll->month."-01"));
        $final_settlement_date = date("Y-m-t", strtotime($electronic_payroll->year."-".$electronic_payroll->month));
        $reference_no = $electronic_payroll_employee->reference_no;
        $ref = explode("-", $reference_no);

        $data = [
            "Tipo" => "2",
            "TipoNota" => "1",
            "Reemplazar" => [
                "ReemplazandoPredecesor" => [
                    "NumeroPred" => $reference_no_electronic_payroll[0].$reference_no_electronic_payroll[1],
                    "CUNEPred" => $electronic_payroll_adjustment->cune,
                    "FechaGenPred" => $electronic_payroll_adjustment->creation_date
                ],
                "Periodo" => [
                    "FechaIngreso" => $admission_date,
                    "FechaLiquidacionInicio" => $initial_settlement_date,
                    "FechaLiquidacionFin" => $final_settlement_date,
                    "TiempoLaborado" => $this->calculate_global_worked_days($admission_date, $final_settlement_date),
                    "FechaGen"=>$generation_date
                ],
                "NumeroSecuenciaXML" => [
                    "CodigoTrabajador" => $employee->employee_id,
                    "Prefijo" => $ref[0],
                    "Consecutivo" => $ref[1],
                    "Numero" => $ref[0].$ref[1]
                ],
                "LugarGeneracionXML" => [
                    "Pais"=> $this->Settings->codigo_iso,
                    "DepartamentoEstado"=> substr($state->CODDEPARTAMENTO, -2),
                    "MunicipioCiudad"=> substr($city->CODIGO, -5),
                    "Idioma"=> "es"
                ],
                "InformacionGeneral" => [
                    "Version" => "V1.0: Nota de Ajuste de Documento Soporte de Pago de Nómina Electrónica",
                    "Ambiente" => $this->Payroll_settings->work_environment,
                    "TipoXML" => "103",
                    "FechaGen" => $generation_date,
                    "HoraGen" => $generation_time,
                    "PeriodoNomina" => $this->Payroll_settings->payment_frequency,
                    "TipoMoneda" => "COP"
                    /*"TRM" => "Se debe colocar la tasa de cambio de la moneda utilizada en el documento en el Campo “TipoMoneda” a Pesos Colombianos.(0.1)"*/
                ],
                // "Notas": [
                //     "Texto libre, relativo al documento",
                //     "Texto libre, relativo al documento",
                //     "Texto libre, relativo al documento(0.N)"
                // ],
                "Empleador" => [
                    "RazonSocial" => $this->Settings->razon_social,
                    "NIT" => $this->Settings->numero_documento,
                    "DV" => $this->Settings->digito_verificacion,
                    "Pais" => $this->Settings->codigo_iso,
                    "DepartamentoEstado" => substr($state->CODDEPARTAMENTO, -2),
                    "MunicipioCiudad" => substr($city->CODIGO, -5),
                    "Direccion" => $this->Settings->direccion
                ],
                "Trabajador" => [
                    "TipoTrabajador" => $employee->employee_type,
                    "SubTipoTrabajador" => "00",
                    "AltoRiesgoPension" => ($employee->retired_risk == YES) ? "true" : "false",
                    "TipoDocumento" => $employee->document_code,
                    "NumeroDocumento" => $employee->vat_no,
                    "PrimerApellido" => $employee->first_lastname,
                    "SegundoApellido" => $employee->second_lastname,
                    "PrimerNombre" => $employee->first_name,
                    "LugarTrabajoPais" => "CO",
                    "LugarTrabajoDepartamentoEstado" => substr($biller_state->CODDEPARTAMENTO, -2),
                    "LugarTrabajoMunicipioCiudad" => substr($biller->city_code, -5),
                    "LugarTrabajoDireccion" => $biller->address,
                    "SalarioIntegral" => ($employee->integral_salary == YES) ? "true" : "false",
                    "TipoContrato" => $employee->contract_type,
                    "Sueldo" => $employee->base_amount
                ],
                "Pago" => [
                    "Forma" => "1",
                    "Metodo" => $employee->payment_method
                ],
                "FechasPagos" => [
                    "FechaPago" => [
                        date("Y-m-t")
                    ]
                ]
            ]
        ];

        if ($employee->settlement_date != "0000-00-00" || $employee->settlement_date != NULL) {
        } else {
            $data["Reemplazar"]["Periodo"]["FechaRetiro"] = $employee->settlement_date;
        }

        if ($employee->payment_method == WIRE_TRANSFER) {
            $data["Reemplazar"]["Pago"]["Banco"] = $bank->name;
            $data["Reemplazar"]["Pago"]["TipoCuenta"] = ($employee->account_type == 1) ? lang("payroll_contracts_savings_account") : lang("payroll_contracts_current_account");
            $data["Reemplazar"]["Pago"]["NumeroCuenta"] = $employee->account_no;
        }

        $total_earneds = 0;
        $total_deductions = 0;
        foreach ($electronic_payroll_items as $item) {
            if ($item->earned_deduction == EARNED) {
                if ($item->concept_id == SALARY) {
                    $data["Reemplazar"]["Devengados"]["Basico"] = [
                        "DiasTrabajados" => $item->days_quantity,
                        "SueldoTrabajado" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == TRANSPORTATION_ALLOWANCE) {
                    $data["Reemplazar"]["Devengados"]["Transporte"][] = [
                        "AuxilioTransporte" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == PER_DIEM_CONSTITUTES_SALARY) {
                    $data["Reemplazar"]["Devengados"]["Transporte"][] = [
                        "ViaticoManuAlojS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == DAYTIME_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(DAYTIME_OVERTIME);
                    $data["Reemplazar"]["Devengados"]["HEDs"]["HED"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == NIGHT_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(NIGHT_OVERTIME);
                    $data["Reemplazar"]["Devengados"]["HENs"]["HEN"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == NIGHT_SURCHARGE) {
                    $concept = $this->Payroll_concepts_model->get_by_id(NIGHT_SURCHARGE);
                    $data["Reemplazar"]["Devengados"]["HRNs"]["HRN"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_DAYTIME_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_DAYTIME_OVERTIME);
                    $data["Reemplazar"]["Devengados"]["HEDDFs"]["HEDDF"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_DAYTIME_SURCHARGE) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_DAYTIME_SURCHARGE);
                    $data["Reemplazar"]["Devengados"]["HRDDFs"]["HRDDF"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_NIGHT_OVERTIME) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_NIGHT_OVERTIME);
                    $data["Reemplazar"]["Devengados"]["HENDFs"]["HENDF"][]= [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == HOLIDAY_NIGHT_SURCHARGE) {
                    $concept = $this->Payroll_concepts_model->get_by_id(HOLIDAY_NIGHT_SURCHARGE);
                    $data["Reemplazar"]["Devengados"]["HRNDFs"]["HRNDF"][] = [
                        "Cantidad" => $item->hours_quantity,
                        "Porcentaje" => $concept->percentage,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == MATERNITY_LICENSE || $item->concept_id == PATERNITY_LICENSE) {
                    $data["Reemplazar"]["Devengados"]["Licencias"]["LicenciaMP"][] = [
						"Cantidad" => $item->days_quantity,
						"Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == PAID_LICENSE || $item->concept_id == FAMILY_ACTIVITY_PERMIT || $item->concept_id == DUEL_LICENSE || $item->concept_id == PAID_PERMIT) {
                    $data["Reemplazar"]["Devengados"]["Licencias"]["LicenciaR"][] = [
						"Cantidad" => $item->days_quantity,
						"Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == DISCIPLINARY_SUSPENSION || $item->concept_id == UNEXCUSED_ABSENTEEISM || $item->concept_id == NOT_PAID_LICENSE || $item->concept_id == NOT_PAID_PERMIT || $item->concept_id == SUNDAY_DISCOUNT) {
                    $data["Reemplazar"]["Devengados"["Licencias"]]["LicenciaNR"][] = [
						"Cantidad" => $item->days_quantity,
						"Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == COMMON_DISABILITY || $item->concept_id == PROFESSIONAL_DISABILITY || $item->concept_id == WORK_DISABILITY) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Reemplazar"]["Devengados"]["Incapacidades"]["Incapacidad"][] = [
						"Cantidad" => $item->days_quantity,
						"Tipo" => $concept->dian_code,
						"Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == COMMISSIONS) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Reemplazar"]["Devengados"]["Comisiones"]["Comision"] = [
						$this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == BEARING_ALLOWANCE) {
                    $data["Reemplazar"]["Devengados"]["Auxilios"]["Auxilio"][] = [
						"AuxilioNS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == TIME_VACATION) {
                    $data["Reemplazar"]["Devengados"]["Vacaciones"]["VacacionesComunes"][] = [
                        "Cantidad" => $item->days_quantity,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == MONEY_VACATION) {
                    $data["Reemplazar"]["Devengados"]["Vacaciones"]["VacacionesCompensadas"][] = [
                        "Cantidad" => $item->days_quantity,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == BONUSES_CONSTITUTING_SALARY) {
                    $data["Reemplazar"]["Devengados"]["Bonificaciones"]["Bonificacion"][] = [
                        "BonificacionS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == BONUSES_NOT_CONSTITUTING_SALARY) {
                    $data["Reemplazar"]["Devengados"]["Bonificaciones"]["Bonificacion"][] = [
                        "BonificacionNS" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == SERVICE_BONUS) {
                    $data["Reemplazar"]["Devengados"]["Primas"] = [
                        "Cantidad" => $item->days_quantity,
                        "Pago" => $this->sma->formatDecimal($item->amount, 2),
                    ];
                }

                if ($item->concept_id == LAYOFFS) {
                    $data["Reemplazar"]["Devengados"]["Cesantias"] = [
                        "Pago" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == LAYOFFS_INTERESTS) {
                    if (isset($data["Reemplazar"]["Devengados"]["Cesantias"])) {
                        $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                        $data["Reemplazar"]["Devengados"]["Cesantias"]["Porcentaje"] = $concept->percentage;
                        $data["Reemplazar"]["Devengados"]["Cesantias"]["PagoIntereses"] = $item->amount;
                    }
                }

                if ($item->concept_id == OTHER_ACCRUED_CONSTITUENTS_SALARY || $item->concept_id == OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    if ($item->concept_id == OTHER_ACCRUED_CONSTITUENTS_SALARY) {
                        $data["Reemplazar"]["Devengados"]["OtrosConceptos"]["OtroConcepto"][] = [
                            "DescripcionConcepto" => $item->description,
                            "ConceptoS" => $this->sma->formatDecimal($item->amount, 2),
                        ];
                    }

                    if ($item->concept_id == OTHER_ACCRUED_NOT_CONSTITUENTS_SALARY) {
                        $data["Reemplazar"]["Devengados"]["OtrosConceptos"]["OtroConcepto"][] = [
                            "DescripcionConcepto" => $item->description,
                            "ConceptoNS" => $this->sma->formatDecimal($item->amount, 2),
                        ];
                    }
                }

                if ($item->concept_id == ENDOWMENT) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Reemplazar"]["Devengados"]["Dotacion"] = $this->sma->formatDecimal($item->amount, 2);
                }

                if ($item->concept_id == COMPENSATION) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Reemplazar"]["Devengados"]["Indemnizacion"] = $this->sma->formatDecimal($item->amount, 2);
                }

                $total_earneds += $item->amount;
            }

            if ($item->earned_deduction == DEDUCTION) {
                if ($employee->employee_type == APPRENTICES_SENA_LECTIVA || $employee->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
                    $data["Reemplazar"]["Deducciones"]["Salud"] = [
                        "Porcentaje" => "4.00",
                        "Deduccion" => $this->sma->formatDecimal(0, 2)
                    ];
                }

                if ($item->concept_id == HEALTH_RATE || $item->concept_id == HEALTH_RATE_LESS_THAN) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Reemplazar"]["Deducciones"]["Salud"] = [
                        "Porcentaje" => $concept->percentage,
				        "Deduccion" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($employee->employee_type == PRE_PENSION_OF_ENTITY_IN_LIQUIDATION || $employee->employee_type == PRE_PENSIONED_WITH_VOLUNTARY_CONTRIBUTION_TO_HEALTH ||
                    $employee->employee_type == APPRENTICES_SENA_LECTIVA || $employee->employee_type == APPRENTICES_SENA_PRODUCTIVE) {
                    $data["Reemplazar"]["Deducciones"]["FondoPension"] = [
                        "Porcentaje" => "4.00",
                        "Deduccion" => $this->sma->formatDecimal(0, 2)
                    ];
                }

                if ($item->concept_id == PENSION_RATE || $item->concept_id == HIGH_RISK_PENSION_RATE) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Reemplazar"]["Deducciones"]["FondoPension"] = [
                        "Porcentaje" => $concept->percentage,
                        "Deduccion" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == DRAFT_CREDIT) {
                    $data["Reemplazar"]["Deducciones"]["Libranzas"]["Libranza"][] = [
                        "Descripcion" => $item->description,
				        "Deduccion" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == COMPANY_LOAN) {
                    $data["Reemplazar"]["Deducciones"]["Deuda"] = $this->sma->formatDecimal($item->amount, 2);
                }

                if ($item->concept_id == FISCAL_SEIZURES) {
                    $data["Reemplazar"]["Deducciones"]["EmbargoFiscal"] = $this->sma->formatDecimal($item->amount, 2);
                }

                if ($item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_2 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_4 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_6 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_1_8 ||
                $item->concept_id == SOLIDARITY_FUND_EMPLOYEES_2) {
                    $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
                    $data["Reemplazar"]["Deducciones"]["FondoSP"] = [
                        "Porcentaje" => $concept->percentage,
                        "DeduccionSP" => $this->sma->formatDecimal($item->amount, 2)
                    ];
                }

                if ($item->concept_id == WITHHOLDING) {
                    $data["Reemplazar"]["Deducciones"]["RetencionFuente"] = $this->sma->formatDecimal($item->amount, 2);
                }

                $total_deductions += $item->amount;
            }
        }

        $data["Reemplazar"]["DevengadosTotal"] = strval($this->sma->formatDecimal($total_earneds, 2));
        $data["Reemplazar"]["DeduccionesTotal"] = strval($this->sma->formatDecimal($total_deductions, 2));
        $data["Reemplazar"]["ComprobanteTotal"] = $this->sma->formatDecimal(($total_earneds - $total_deductions), 2);

        return $data;
    }

    public function check_document_status($electronic_payroll_id, $electronic_payroll_employee_id, $employee_id, $contract_id)
    {
        $electronic_payroll_employee = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_id($electronic_payroll_employee_id);
        $employee = $this->Employees_model->get_by_id($employee_id, $contract_id);

        $payroll_reference_no = str_replace("-", "", $electronic_payroll_employee->reference_no);
        $xml_type = ($electronic_payroll_employee->type == "0") ? "102" : "103";
        $nit_employer = $this->Settings->numero_documento;
        $document_employee = $employee->vat_no;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apine.efacturacadena.com/v1/ne/consulta/documentos?empleadorNit=".$nit_employer."&numeroNomina=".$payroll_reference_no."&tipoXml=".$xml_type."&trabajadorNumeroDocumento=".$document_employee,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'nominaAuthorizationToken: 4b71310c-ebd7-42cb-9e51-293680850bbe',
                'nitAlianza: 901090070'
            ),
        ));

        $response = json_decode(curl_exec($curl));
        curl_close($curl);

        // $this->sma->print_arrays($response);

        if ($response->statusCode == "200") {
            $data["statusCode"] = $response->statusCode;
            $data["trackId"] = $response->trackId;
            $data["cune"] = $response->cune;
            $data["statusMessage"] = $response->statusMessage;
            $data["statusDescription"] = $response->statusDescription;
            $data["status"] = ACCEPTED;
            $data["errorMessage"] = "";
            $data["errorReason"] = "";
        } else if ($response->statusCode == "400" || $response->statusCode == "404") {
            $data["statusCode"] = $response->statusCode;
            $data["errorMessage"] = $response->errorMessage;
            $data["errorReason"] = $response->errorReason;
            $data["statusMessage"] = "";
            $data["statusDescription"] = "";
            $data["status"] = PENDING;
        }

        if ($this->Payroll_electronic_model->update_employee_items($data, $electronic_payroll_employee_id)) {
            $this->change_header_state($electronic_payroll_id);

            if ($data["statusCode"] == "200") {
                $this->session->set_flashdata("message", $data["statusMessage"]);
            } else {
                $this->session->set_flashdata("error", $data["errorMessage"]);
            }
        }

        admin_redirect($_SERVER["HTTP_REFERER"]);
    }

    public function pay_slip($electronicPayrollEmployeeId, $employeeId, $contractId)
    {
        $this->load->library('qr_code');

        $paidToEmployeeArray = [];
        $conceptsPaidEmployee = [];
        $electronicPayrollItems = [];
        $electronicPayrollEmployee = $this->Payroll_electronic_model->get_electronic_payroll_employee_by_id($electronicPayrollEmployeeId);
        $electronicPayroll = $this->Payroll_electronic_model->get_by_id($electronicPayrollEmployee->electronic_payroll_id);
        $payrollItems = $this->Payroll_electronic_model->get_items_by_electronic_payroll_employee_id($electronicPayrollEmployeeId);
        $concepts = $this->Payroll_concepts_model->get();

        if (!empty($payrollItems)) {
            foreach ($payrollItems as $item) {
                $id = $item->id;
                $concept_id = $item->concept_id;
                $paidToEmployee = $item->paid_to_employee;
                if (($concept_id == MONEY_VACATION || $concept_id == SERVICE_BONUS || $concept_id == LAYOFFS || $concept_id == LAYOFFS_INTERESTS) && $paidToEmployee == NOT) {
                    $conceptsPaidEmployee[] = $item;
                } else {
                    $electronicPayrollItems[] = $item;
                }
            }
        }

        $biller = $this->site->getAllCompaniesWithState('biller', $electronicPayrollEmployee->biller_id);
        $employee = $this->Employees_model->get_by_id($employeeId, $contractId);
        $paymentMeans = $this->Payroll_contracts_model->get_payment_means();
        foreach ($paymentMeans as $paymentMean) {
            if ($paymentMean->code == $employee->payment_method) {
                $paymentMethod = $paymentMean->name;
            }
        }

        $electronicPayrollFilesPath = 'themes/default/admin/assets/images/electronic_payroll/';
        if (!file_exists($electronicPayrollFilesPath)) {
            mkdir($electronicPayrollFilesPath, 0777);
        }
        $cune = $electronicPayrollEmployee->cune;
        $QRImageName = $electronicPayrollFilesPath.$cune.'.png';
        QRcode::png("https://catalogo‐vpfe.dian.gov.co/document/searchqr?documentkey=".$cune, $QRImageName);

        $months = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

        $imagePath = 'assets/uploads/logos/'.$biller->logo;
        $this->site->getExistingImage($imagePath);

        // foreach ($electronicPayrollItems as $key => $item) {
        //     if ($item->concept_id == LAYOFFS_INTERESTS && $item->paid_to_employee == YES) {
        //         $concept = $this->Payroll_concepts_model->get_by_id($item->concept_id);
        //         $paidToEmployeeArray['layoffsAmount'] = $this->sma->formatDecimal(($item->amount / $item->days_quantity / ($concept->percentage / 100) * 360), 2);
        //     }
        // }

        if ($electronicPayrollEmployee->type == 0) {
            $documentName = "Documento Soporte de Pago de Nómina Electrónica";
        } else if ($electronicPayrollEmployee->type == 1) {
            $documentName = "Documento Ajuste de Nómina Electrónica";
        } else {
            $documentName = "Documento Eliminación de Nómina Electrónica";
        }

        $this->data['cune'] = $cune;
        $this->data['concepts'] = $concepts;
        $this->data['imagePath'] = $imagePath;
        $this->data['QRImageName'] = $QRImageName;
        $this->data['billerName'] = $biller->name;
        $this->data['company'] = $biller->company;
        $this->data['documentName'] = $documentName;
        $this->data['billerEmail'] = $biller->email;
        $this->data['billerPhone'] = $biller->phone;
        $this->data['pageTitle'] = lang('pay_slip');
        $this->data['paymentMethod'] = $paymentMethod;
        $this->data['employeeName'] = $employee->name;
        $this->data['billerAddress'] = $biller->address;
        $this->data['employeeEmail'] = $employee->email;
        $this->data['employeePhone'] = $employee->phone;
        $this->data['employeeVatNo'] = $employee->vat_no;
        $this->data['employeeAddress'] = $employee->address;
        $this->data['employeeBaseAmount'] = $employee->base_amount;
        $this->data['electronicPayrollItems'] = $electronicPayrollItems;
        $this->data['billerLocation'] = $biller->city.', '.$biller->state;
        $this->data['numeroDocumento'] = $this->Settings->numero_documento;
        $this->data['defaultCurrency'] = $this->Settings->default_currency;
        $this->data['employeeLocation'] = $employee->city.', '.$employee->state;
        $this->data['employeeProfessionalPositionName'] = $employee->professional_position_name;
        $this->data['ivaRetainer'] = ($this->Settings->iva_retainer == NOT) ? "No somos" : "Somos";
        $this->data['icaRetainer'] = ($this->Settings->ica_retainer == NOT) ? "No somos" : "Somos";
        $this->data['fuenteRetainer'] = ($this->Settings->fuente_retainer == NOT) ? "No somos" : "Somos";
        $this->data['greatContributor'] = ($this->Settings->great_contributor == NOT) ? "No somos" : "Somos";
        $this->data['referenceNo'] = $electronicPayrollEmployee->reference_no;
        $this->data['generationDate'] = date("d/m/Y", strtotime($electronicPayrollEmployee->generation_date));
        $this->data['period'] = $months[$electronicPayroll->month - 1]. ' de '. $electronicPayroll->year;
        // $this->data['paidToEmployeeArray'] = $paidToEmployeeArray;
        $this->data['conceptsPaidEmployee'] = $conceptsPaidEmployee;

        $this->load_view($this->theme. 'payroll_electronic/pay_slip.php', $this->data);
    }

    /* traer los id de nomina empleado
        obtener la información necesario para pasarla al método.
        el método que consume el webservices se debe modificar para bifurcar procesos
        crear un método que administre la respuesta para el envío individual
        crear un método para administrar la respuesta masiva.
    */
    public function multiple_send_electronic_payroll()
    {
        $this->sma->checkPermissions('send', NULL, 'payroll_management');

        $message = "";
        $payrollEmployees = $this->input->post("payrolls");
        foreach ($payrollEmployees as $payrollEmployeeId) {
            $payrollEmployee = $this->Payroll_electronic_model->get_employee_items($payrollEmployeeId);

            $employeeId = $payrollEmployee->employee_id;
            $contractId = $payrollEmployee->contract_id;
            $payrollId  = $payrollEmployee->electronic_payroll_id;

            $json = $this->create_json($payrollEmployeeId, $employeeId, $contractId);

            $response = $this->consume_web_services($json);

            $result = $this->management_response($response, $payrollId, $payrollEmployeeId);

            if ($result["statusCode"] != "200") {
                $message += "{$payrollEmployee->reference_no}, ";
            }
        }

        if (!empty($message)) {
            $message = "Las siguientes Nóminas no pudieron ser Enviadas con Éxito: ".trim($message, ', ');
        }

        echo json_encode([
            "message" => $message
        ]);
    }
}

/* End of file Payroll_electronic.php */
/* Location: ./app/controllers/Payroll_electronic.php */