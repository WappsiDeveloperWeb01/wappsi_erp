<?php

use Zend\Validator\Date;

defined('BASEPATH') OR exit('No direct script access allowed');

class Payroll_management_model extends CI_Model {

	public function __construct() {
        parent::__construct();
    }

    public function get()
    {
    	$q = $this->db->get("payroll");
    	if ($q->num_rows() > 0) {
    		return $q->row();
    	}
        return FALSE;
    }

    public function get_by_id($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("payroll");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_existing_employee_payroll($employee_id)
    {
        $this->db->where('employee_id', $employee_id);
        if ($this->db->count_all_results('payroll_items') > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function getExistingContractInPreparationPayroll($contractId)
    {
        $this->db->where('contract_id', $contractId);
        $this->db->where('payroll.status', IN_PREPARATION);
        $this->db->join('payroll', 'payroll.id = payroll_items.payroll_id', 'inner');
        if ($this->db->count_all_results('payroll_items') > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function get_payroll_items($payroll_id)
    {
        $this->db->where("payroll_id", $payroll_id);
        $this->db->join("companies", "companies.id = payroll_items.employee_id", "inner");
        if ($q = $this->db->get("payroll_items")) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_payroll_items_by_payroll_id_employee_id(int $payroll_id, int $employee_id, int $contract_id)
    {
        $this->db->select("payroll_items.id,
            payroll_items.payroll_concept_type,
            payroll_items.payroll_concept_id,
            payroll_items.amount,
            payroll_items.earned_deduction,
            payroll_concepts.percentage,
            payroll_items.days_quantity,
            payroll_items.contract_id,
            payroll_concepts.name,
            payroll_concept_type.type,
            payroll_items.paid_to_employee,
            payroll_items.applied");
        $this->db->where("payroll_id", $payroll_id);
        $this->db->where("employee_id", $employee_id);
        $this->db->where("contract_id", $contract_id);
        $this->db->join("companies", "companies.id = payroll_items.employee_id", "inner");
        $this->db->join("payroll_concepts", "payroll_concepts.id = payroll_items.payroll_concept_id", "inner");
        $this->db->join("payroll_concept_type", "payroll_concept_type.id = payroll_items.payroll_concept_type", "inner");
        $this->db->order_by("payroll_concept_type.type", "asc");
        $this->db->order_by("sma_payroll_concepts.order_json", "asc");
        $this->db->order_by("sma_payroll_concepts.id", "asc");
        if ($q = $this->db->get("payroll_items")) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_payroll_items_by_month_employee_id(int $payroll_id = NULL, int $employee_id, int $month, int $number = NULL, int $contract_id = NULL)
    {
        $this->db->select("payroll_items.id, payroll_items.payroll_concept_type, payroll_items.payroll_concept_id, payroll_items.amount, payroll_items.earned_deduction, payroll_concepts.percentage, payroll_items.days_quantity, payroll_items.contract_id");
        $this->db->join("companies", "companies.id = payroll_items.employee_id", "inner");
        $this->db->join("payroll_concepts", "payroll_concepts.id = payroll_items.payroll_concept_id", "inner");
        $this->db->join("payroll", "payroll.id = payroll_items.payroll_id", "inner");
        if (!empty($payroll_id)) { $this->db->where("payroll.id", $payroll_id); }
        $this->db->where("payroll.month", $month);
        $this->db->where("employee_id", $employee_id);
        $this->db->where("payroll_items.contract_id", $contract_id);
        if (!empty($number)) { $this->db->where("number", $number); }
        if ($q = $this->db->get("payroll_items")) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_existing_payroll($year = NULL, $month = NULL, $number = NULL)
    {
        if (!empty($year)) { $this->db->where("year", $year); }
        if (!empty($month)) { $this->db->where("month", $month); }
        if (!empty($number)) { $this->db->where("number", $number); }

        $q = $this->db->get("payroll");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_last_payroll($year = NULL, $month = NULL, $number = NULL, $biller_id = NULL)
    {
        if (!empty($year)) { $this->db->where("year", $year); }
        if (!empty($month)) { $this->db->where("month", $month); }
        if (!empty($number)) { $this->db->where("number", $number); }
        if (!empty($biller_id)) { $this->db->where("biller_id", $biller_id); }

        $this->db->order_by("year", "DESC");
        $this->db->order_by("month", "DESC");
        $this->db->order_by("number", "DESC");
        $this->db->limit(1);

        $q = $this->db->get("payroll");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_weeks_by_month($month)
    {
        $this->db->where("year", date("Y"));
        $this->db->where("month", $month);
        $q = $this->db->get("payroll_weeks");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_billers()
    {
        $this->db->order_by("company", "ASC");
        $this->db->order_by("id", "ASC");

        $q = $this->db->where("group_name", "biller")->get("companies");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_payroll_items_by_id($id)
    {
        $this->db->where("id", $id);
        $q = $this->db->get("payroll_items");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_payroll_items_by_data($payroll_id, $employee_id, $concept_type_id, $concept_id, $contract_id)
    {
        $this->db->where(["payroll_id"=>$payroll_id, "contract_id"=>$contract_id, "employee_id"=>$employee_id, "payroll_concept_type"=>$concept_type_id, "payroll_concept_id"=>$concept_id]);
        $q = $this->db->get("payroll_items");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_payroll_future_items_by_id($id)
    {
        $this->db->where('id', $id);
        $q = $this->db->get('payroll_futures_items');
        return $q->row();
    }

    public function get_payroll_future_items_by_employee_id($contractId, $employee_id)
    {
        $COMPANY_LOAN = COMPANY_LOAN;
        $q = $this->db->query("SELECT
                            `sma_payroll_futures_items`.`id`,
                            `sma_payroll_futures_items`.`employee_id`,
                            `concept_type_id`,
                            `concept_id`,
                            `amount`,
                            `fee_amount`,
                            `fee_no`,
                            `start_date`,
                            `due_date`,
                            `sma_payroll_concept_type`.`type`,
                            `sma_payroll_futures_items`.`from_contract`,
                            `sma_payroll_futures_items`.`term_no`
                        FROM
                            `sma_payroll_futures_items`
                                INNER JOIN
                            `sma_payroll_concept_type` ON `sma_payroll_concept_type`.`id` = `sma_payroll_futures_items`.`concept_type_id`
                        WHERE
                            `employee_id` = '{$employee_id}' AND `contract_id` = '{$contractId}' AND `applied` = 0 AND `concept_id` != {$COMPANY_LOAN}
                        UNION

                        SELECT
                            `sma_payroll_futures_items`.`id`,
                            `sma_payroll_futures_items`.`employee_id`,
                            `concept_type_id`,
                            `concept_id`,
                            `amount`,
                            `fee_amount`,
                            `fee_no`,
                            `start_date`,
                            `due_date`,
                            `sma_payroll_concept_type`.`type`,
                            `sma_payroll_futures_items`.`from_contract`,
                            `sma_payroll_futures_items`.`term_no`
                        FROM
                            `sma_payroll_futures_items`
                                INNER JOIN
                            `sma_payroll_concept_type` ON `sma_payroll_concept_type`.`id` = `sma_payroll_futures_items`.`concept_type_id`
                        WHERE
                            `employee_id` = '{$employee_id}' AND `contract_id` = '{$contractId}' AND `applied` = 0 AND `concept_id` = {$COMPANY_LOAN}
                        GROUP BY
                            `sma_payroll_futures_items`.`loan_code`
                        ORDER BY
                            `fee_no` ASC");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function find_payroll_future_items($data)
    {
        $this->db->where($data);
        $q = $this->db->get('payroll_futures_items');
        return $q->result();
    }

    public function get_payroll_future_items_by_data($payroll_id)
    {
        $this->db->where('payroll_id', $payroll_id);
        $q = $this->db->get('payroll_futures_items');
        return $q->result();
    }

    public function get_week_month($month)
    {
        $this->db->where('month', $month);
        $q = $this->db->get('payroll_weeks');
        return $q->result();
    }

    public function get_by_status($status)
    {
        $this->db->where('status', $status);
        $q = $this->db->get('payroll');
        if ($q->num_rows() > 0) {
            if ($q->num_rows() > 1) {
                return $q->result();
            }
            return $q->row();
        } else {
            return FALSE;
        }
    }

    public function get_payroll_grouped_by_month($year, $month)
    {
        $this->db->select("GROUP_CONCAT(ID) AS ids");
        $this->db->where("year", $year);
        $this->db->where("month", $month);
        $q = $this->db->get("sma_payroll");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function getMinimumMaximumPayrollDate($payrollIds)
    {
        $this->db->select("MIN(start_date) AS start_date, 
            MAX(end_date) AS end_date");
        $this->db->where("id IN (". $payrollIds .")");
        $q = $this->db->get("sma_payroll");
        if ($q->num_rows() > 0) {
            return $q->row();
        }
        return FALSE;
    }

    public function get_payroll_items_by_payroll_id($payroll_ids, $employee_id = NULL, $contract_id = NULL)
    {
        $this->db->where("payroll_id IN (". $payroll_ids .")");
        $this->db->where_not_in("payroll_items.payroll_concept_id", [LOAN_DISBURSEMENT]);
        if (!empty($employee_id)) {
            $this->db->where("payroll_items.employee_id", $employee_id);
        }
        if (!empty($contract_id)) {
            $this->db->where("payroll_items.contract_id", $contract_id);
        }
        $this->db->order_by("employee_id, payroll_concept_id");
        $q = $this->db->get("payroll_items");
        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function get_payroll_items_to_PDF($payrollId)
    {
        $this->db->select("
            companies.vat_no AS documentNumber,
            companies.name,
            payroll_contracts.base_amount AS baseSalary,
            SUM(".$this->db->dbprefix("payroll_items").".days_quantity) AS daysQuantity,
            SUM(".$this->db->dbprefix("payroll_items").".amount) AS salary,
            COALESCE((SELECT
                SUM(pii.amount)
            FROM
                sma_payroll_items pii
                    INNER JOIN
                sma_payroll_concept_type pcti ON pcti.id = pii.payroll_concept_type
            WHERE
                pii.payroll_id = $payrollId
                    AND sma_payroll_items.contract_id = pii.contract_id
                    AND pcti.id = ".TRANSPORTATION_ALLOWANCE_TYPE."
                    AND pii.contract_id), 0) AS transportSubsidy,
            COALESCE((SELECT
                SUM(pii.amount)
            FROM
                sma_payroll_items pii
                    INNER JOIN
                sma_payroll_concept_type pcti ON pcti.id = pii.payroll_concept_type
            WHERE
                pii.payroll_id = $payrollId
                    AND sma_payroll_items.contract_id = pii.contract_id
                    AND pcti.id = ".OVERTIME_OR_SURCHARGES."
                    AND pii.contract_id), 0) AS overtimeSurcharges,
            COALESCE((SELECT
                SUM(pii.amount)
            FROM
                sma_payroll_items pii
                    INNER JOIN
                sma_payroll_concept_type pcti ON pcti.id = pii.payroll_concept_type
            WHERE
                pii.payroll_id = $payrollId
                    AND sma_payroll_items.contract_id = pii.contract_id
                    AND pcti.id = ".SALARY_PAYMENTS."
                    AND pii.contract_id), 0) AS salaryPayments,
            COALESCE((SELECT
                SUM(pii.amount)
            FROM
                sma_payroll_items pii
                    INNER JOIN
                sma_payroll_concept_type pcti ON pcti.id = pii.payroll_concept_type
            WHERE
                pii.payroll_id = $payrollId
                    AND sma_payroll_items.contract_id = pii.contract_id
                    AND pcti.id = ".NON_SALARY_PAYMENTS."
                    AND pii.contract_id), 0) AS notSalaryPayments,
            COALESCE((SELECT
                SUM(pii.amount)
            FROM
                sma_payroll_items pii
            WHERE
                pii.payroll_id = $payrollId
                    AND sma_payroll_items.contract_id = pii.contract_id
                    AND pii.contract_id
                    AND pii.payroll_concept_id IN (".HEALTH_RATE.", ".HEALTH_RATE_LESS_THAN.")), 0) AS Health,
            COALESCE((SELECT
                    SUM(pii.amount)
                FROM
                    sma_payroll_items pii
                WHERE
                    pii.payroll_id = $payrollId
                        AND sma_payroll_items.contract_id = pii.contract_id
                        AND pii.contract_id
                        AND pii.payroll_concept_id IN (".PENSION_RATE.", ".HIGH_RISK_PENSION_RATE.")), 0) AS Pension,
            COALESCE((SELECT
                SUM(pii.amount)
            FROM
                sma_payroll_items pii
                    INNER JOIN
                sma_payroll_concept_type pcti ON pcti.id = pii.payroll_concept_type
            WHERE
                pii.payroll_id = $payrollId
                    AND sma_payroll_items.contract_id = pii.contract_id
                    AND pii.contract_id
                    AND pcti.id NOT IN (".DEDUCTIONS_BY_LAW.")
                    AND pcti.type = ".DEDUCTION."), 0) AS otherDeductions
        ");
        $this->db->where("payroll_id", $payrollId);
        $this->db->join("companies", "companies.id = payroll_items.employee_id", "inner");
        $this->db->join("payroll_contracts", "payroll_contracts.id = payroll_items.contract_id", "inner");
        $this->db->join("payroll_concept_type", "payroll_concept_type.id = payroll_items.payroll_concept_type", "inner");
        $this->db->where_in("payroll_concept_type.id", [ORDINARY, DISABILITIES, PAID_LICENSE_TYPE, TIME_VACATION_TYPE]);
        $this->db->group_by("companies.id");
        $this->db->order_by("companies.name");
        if ($q = $this->db->get("payroll_items")) {
            return $q->result();
        }
        return FALSE;
    }

    public function getSocialSecurityToPDF($payrollId)
    {
        $this->db->select('SUM('. $this->db->dbprefix('payroll_social_security_parafiscal') .'.amount) AS amount, percentage, description');
        $this->db->where('payroll_id', $payrollId);
        $this->db->where_in('description', ['Pensión', 'Salud']);
        $this->db->or_where("description LIKE '%CLASE%'");
        $this->db->group_by('description, percentage');
        $q = $this->db->get('payroll_social_security_parafiscal');

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getHealthPensionEmployee($payrollId)
    {
        $this->db->select("REPLACE(". $this->db->dbprefix("payroll_concepts") .".name, 'Trabajador', '') AS description, percentage, SUM(amount) AS amount");
        $this->db->join("payroll_concepts", "payroll_concepts.id = payroll_items.payroll_concept_id", "INNER");
        $this->db->where_in("payroll_concept_id", [26, 28, 30, 32]);
        $this->db->group_by("payroll_concept_id");

        if ($q = $this->db->get("payroll_items")) {
            return $q->result();
        }
        return FALSE;
    }

    public function getParaficalToPDF($payrollId)
    {
        $this->db->select("description, percentage, SUM(amount) AS amount");
        $this->db->where("payroll_id", $payrollId);
        $this->db->group_by("description , percentage");
        $this->db->having("description LIKE '%Caja%'");
        $this->db->or_having("description LIKE '%SENA%'");
        $this->db->or_having("description LIKE '%ICBF%'");
        if ($q = $this->db->get("payroll_social_security_parafiscal")) {
            return $q->result();
        }
        return FALSE;
    }

    public function getProvisionsToPDF($payrollId)
    {
        $this->db->select("name AS description, ". $this->db->dbprefix("payroll_provisions") .".percentage, SUM(amount) AS amount");
        $this->db->join("payroll_concepts", "payroll_concepts.id = payroll_provisions.concept_id", "INNER");
        $this->db->where("payroll_id", $payrollId);
        $this->db->group_by("concept_id , ". $this->db->dbprefix("payroll_provisions") .".percentage");
        if ($q = $this->db->get("payroll_provisions")) {
            return $q->result();
        }
        return FALSE;
    }

    public function getPayrollToExcel($data)
    {
        $this->db->select("p.id,
            p.year,
            p.month,
            p.number,
            pc.id contractId,
            c.vat_no,
            pc.companies_id,
            c.name,
            c.address,
            c.phone,
            c.email,
            pc.contract_type,
            ptc.description contractTypeDescription,
            s.name AS sucursal,
            pa.name AS area,
            ppp.name AS professionalPositions,
            pc.base_amount,
            IF(trans_allowance = 1,
                'Aplica',
                'No aplica') AS transAllowance");
        $this->db->from("payroll p");
        $this->db->join("payroll_items pi", "pi.payroll_id = p.id");
        $this->db->join("payroll_contracts pc", "pc.id = pi.contract_id");
        $this->db->join("companies c", "c.id = pc.companies_id");
        $this->db->join("payroll_types_contracts ptc", "ptc.id = pc.contract_type");
        $this->db->join("companies s", "s.id = pc.biller_id");
        $this->db->join("payroll_areas pa", "pa.id = pc.area");
        $this->db->join("payroll_professional_position ppp", "ppp.id = pc.professional_position");
        $this->db->group_by(["p.id", "p.year", "p.month", "pc.id"]);
        $this->db->order_by("p.id, p.year, p.month, c.name");

        if (!empty($data['date_records_filter'])) {
            $this->db->where('DATE(p.start_date) >= ', DateTime::createFromFormat("d/m/Y H:i", $data['start_date'])->format('Y-m-d'));
            $this->db->where('DATE(p.end_date) <= ', DateTime::createFromFormat("d/m/Y H:i", $data['end_date'])->format('Y-m-d'));
        }

        if (!empty($data["biller_id"])) {
            $this->db->where("p.biller_id", $data["biller_id"]);
        }

        if (!empty($data["area"])) {
            $this->db->where("pa.id", $data["area"]);

            if (!empty($data["professional_position"])) {
                $this->db->where("ppp.id", $data["professional_position"]);
            }
        }

        if (!empty($data['frequency'])) {
            $this->db->where('p.frequency ', $data['frequency']);
        }

        if (!empty($data['status'])) {
            $this->db->where('p.status ', $data['status']);
        }

        if (!empty($data['employee_id'])) {
            $this->db->where('pi.employee_id ', $data['employee_id']);
        }



        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return FALSE;
    }

    public function getPayrollItems($data)
    {
        $this->db->select("pi.earned_deduction,
            pi.payroll_concept_id,
            SUM(pi.amount) amount,
            SUM(pi.days_quantity) days,
            SUM(pi.hours_quantity) hours,
            pi.paid_to_employee");
        $this->db->from("payroll p");
        $this->db->join("payroll_items pi", "pi.payroll_id = p.id");
        $this->db->where("pi.payroll_id", $data["payroll_id"]);
        $this->db->where("pi.employee_id", $data["employee_id"]);
        $this->db->where("pi.contract_id", $data["contract_id"]);
        $this->db->group_by("pi.payroll_concept_id");
        $this->db->order_by("pi.earned_deduction , pi.payroll_concept_id");

        $q = $this->db->get();

        if ($q->num_rows() > 0) {
            return $q->result();
        }
        return false;
    }

    public function getAmountFrequenciesPayroll($data)
    {
        $this->db->select("year, month, frequency");
        if (!empty($data['dateRecordsFilter'])) {
            $this->db->where("start_date >=", $data["start_date"]);
            $this->db->where("end_date <=", $data["end_date"]);
        }
        $q = $this->db->get("payroll");

        if ($q->num_rows() > 0) {
            return $q->result();
        }

        return FALSE;
    }

    public function insert($data)
    {
        if ($this->db->insert("payroll", $data)) {
            return $this->db->insert_id();
        }
        return FALSE;
    }

    public function insert_item($data)
    {
        if ($this->is_assoc($data)) {
            if ($this->db->insert("payroll_items", $data)) {
                return $this->db->insert_id();
            }
            return FALSE;
        } else {
            if ($this->db->insert_batch('payroll_items', $data) > 0) {
                return $this->db->insert_id();
            }
            return FALSE;
        }
    }

    private function is_assoc( $array ) {
        return array_keys( $array ) !== range( 0, count($array) - 1 );
    }

    public function insert_items($data)
    {
        if ($this->db->insert_batch("payroll_items", $data) > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function insert_future_items($data)
    {
        if ($this->db->insert_batch("payroll_futures_items", $data) > 0) {
            return TRUE;
        }
        return FALSE;
    }

    public function update($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("payroll", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_item($data, $id)
    {
        $this->db->where("id", $id);
        if ($this->db->update("payroll_items", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function update_future_item($data, $id, $excludeContract = false)
    {
        $this->db->where("id", $id);
        if ($excludeContract) {
            $this->db->where("from_contract", NOT);
        }

        if ($this->db->update("payroll_futures_items", $data)) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_item($id)
    {
        $this->db->where("id", $id);
        if ($this->db->delete("payroll_items")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_items($payroll_id)
    {
        $this->db->where("payroll_id", $payroll_id);
        if ($this->db->delete("payroll_items")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_items_by_employee_id($payroll_id, $employee_id, $contract_id, $concept_id = null)
    {
        $this->db->where("payroll_id", $payroll_id);
        $this->db->where("employee_id", $employee_id);
        $this->db->where("contract_id", $contract_id);
        if (!empty($concept_id)) {
            $this->db->where("payroll_concept_id", $concept_id);
        }
        if ($this->db->delete("payroll_items")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete($id)
    {
        $this->db->where("id", $id);
        if ($this->db->delete("payroll")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_future_item($future_item_id)
    {
        $this->db->where('id', $future_item_id);
        if ($this->db->delete('payroll_futures_items')) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_social_security_parafiscal($payroll_id)
    {
        $this->db->where("payroll_id", $payroll_id);
        if ($this->db->delete("payroll_social_security_parafiscal")) {
            return TRUE;
        }
        return FALSE;
    }

    public function delete_provisions($payroll_id)
    {
        $this->db->where("payroll_id", $payroll_id);
        if ($this->db->delete("sma_payroll_provisions")) {
            return TRUE;
        }
        return FALSE;
    }
}

/* End of file Payroll_management_model.php */
/* Location: .//C/xampp/htdocs/wappsi_comercial/app/models/admin/Payroll_management_model.php */