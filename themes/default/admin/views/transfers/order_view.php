<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="modal-dialog modal-lg no-modal-header">
    <div class="modal-content">
        <div class="modal-body">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                <i class="fa fa-2x">&times;</i>
            </button>
            <button type="button" class="btn btn-xs btn-default no-print pull-right" style="margin-right:15px;" onclick="window.print();">
                <i class="fa fa-print"></i> <?= lang('print'); ?>
            </button>
            <?php if ($logo) { ?>
            <div class="text-center" style="margin-bottom:20px;">
                <img src="<?= base_url() . 'assets/uploads/logos/' . $Settings->logo; ?>"
                alt="<?= $Settings->site_name; ?>">
            </div>
            <?php } ?>
            <div class="row">
                <h2 class="text-center"><?= lang('transfer') ?></h2>
                <div class="col-xs-6">
                    <h3>
                        <?= lang("date"); ?>: <?= $this->sma->hrld($transfer->date); ?>
                        <br>
                        <?= lang("ref_no"); ?>: <?= $transfer->reference_no; ?>
                    </h3>
                </div>
            </div>
            <div class="clearfix"></div>

            <div class="row">
                <div class="col-xs-12">
                    <?= lang($this->Settings->companies_for_adjustments_transfers); ?>:<br/>
                    <h3 style="margin-top:10px;"><?= $seller->name ; ?></h3>
                    <?= lang("to"); ?>:<br/>
                    <h3 style="margin-top:10px;"><?= $biller->name . " ( " . $biller->company . " )"; ?></h3>
                    <h3 style="margin-top:10px;"><?= $warehouse->name . " ( " . $warehouse->code . " )"; ?></h3>
                    <?= "<p>" . $warehouse->phone . "</p><p>" . $warehouse->email . "<br>" . $warehouse->address . "</p>";
                    ?>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-bordered table-hover order-table">
                    <thead>
                        <?php $print_normal = false; ?>
                        <?php if ($print_normal): //dejar por si se quiere parametrizar ?>
                            <tr>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("no"); ?></th>
                                <th style="vertical-align:middle;"><?= lang("description"); ?></th>
                                <?php if ($Settings->indian_gst) { ?>
                                    <th><?= lang("hsn_code"); ?></th>
                                <?php } ?>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("quantity"); ?></th>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("unit_cost"); ?></th>
                                <?php if ($Settings->tax1) {
                                    echo '<th style="text-align:center; vertical-align:middle;">' . lang("tax") . '</th>';
                                } ?>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("subtotal"); ?></th>
                            </tr>
                        <?php else: ?>
                            <tr>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("no"); ?></th>
                                <th style="vertical-align:middle;"><?= lang("description"); ?></th>
                                <?php if ($Settings->product_serial): ?>
                                    <th style="text-align:center; vertical-align:middle;"><?= lang("serial_no"); ?></th>
                                <?php endif ?>
                                <th style="text-align:center; vertical-align:middle;"><?= lang("quantity"); ?></th>
                            </tr>
                        <?php endif ?>
                    </thead>

                    <tbody>
                        <?php $r = 1;
                        $total_qty = 0;
                        foreach ($rows as $row): ?>
                            <tr>
                                <td style="text-align:center; width:25px;"><?= $r; ?></td>
                                <td style="text-align:left;">
                                    <?= $row->code.' - '.$row->name . ($row->color ? ' (' . $row->color . ')' : ''); ?>
                                    <?= $row->second_name ? '<br>' . $row->second_name : ''; ?>
                                </td>
                                <td style="text-align:center; width:80px; "><?= $this->sma->formatQuantity($row->quantity)?></td>
                            </tr>
                        <?php $r++;
                        endforeach; ?>
                    </tbody>
                    <tfoot>
                        <?php $col = $Settings->indian_gst ? 4 : 3;
                        if ($Settings->tax1) {
                            $col += 1;
                        } 

                        if (!$print_normal) {
                            $col = 0;
                        }

                        ?>
                    </tfoot>
                </table>
            </div>

            <div class="row">
                <div class="col-xs-12">
                    <?php if ($transfer->note || $transfer->note != "" || $print_normal == false ) { ?>
                    <div class="well well-sm">
                        <p class="bold"><?= lang("observation"); ?>:</p>

                        <div><?= $this->sma->decode_html($transfer->note); ?></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="buttons">
                <div class="btn-group btn-group-justified">
                    <?php if ($transfer->status != 'completed'): ?>
                        <div class="btn-group">
                            <a href='#' class='po btn btn-primary' title='' data-placement="top" data-content="
                                        <p> <?= lang('r_u_sure') ?> </p>
                                        <a class='btn btn-primary' href='<?= admin_url('transfers/add/').$transfer->id ?>'> <?= lang('i_m_sure') ?> </a>
                                        <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover' style="width: 100%;">
                                        <i class='fa fa-download'></i>
                                <?= lang('end_with_quantity') ?>
                            </a>
                        </div>
                        <div class="btn-group">
                            <a href='#' class='po btn btn-default' title='' data-placement="top" data-content="
                                        <p> <?= lang('r_u_sure') ?> </p>
                                        <a class='btn btn-primary' href='<?= admin_url('transfers/end_order/').$transfer->id ?>'> <?= lang('i_m_sure') ?> </a>
                                        <button class='btn po-close'> <?= lang('cancel') ?> </button>" rel='popover' style="width: 100%;">
                                        <i class='fa fa-download'></i>
                                <?= lang('end_without_quantity') ?>
                            </a>
                        </div>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready( function() {
        $('.tip').tooltip();
    });
</script>
